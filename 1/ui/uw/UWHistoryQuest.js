//程序名称：UWHistoryQuest.js
//程序功能：既往问题件查询
//创建日期：2012-10-19
//创建人  ：张成轩
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug = "0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var cflag = ""; // 问题件操作位置 1.核保
var canReplyFlag = false;

// 提交，保存按钮对应操作
function submitForm() {
	var i = 0;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;

	fm.submit(); // 提交
}

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

// 显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}

function manuchkspecmain() {
	fm.submit();
}

function query() {
	// 初始化表格
	initQuestGrid();
	initContent();

	// 书写SQL语句
	k++;
	var strSQL = "";
	var ifreply = fm.IfReply.value;
	var tOPos = fm.OperatePos.value;
	var tOperatePos = fm.Flag.value;
	var tContNo = fm.ContNo.value;

	if (tOperatePos == "") {
		alert("操作位置传输失败!");
		return "";
	}

	if (tOperatePos == "1" || tOperatePos == "3" || tOperatePos == "5") {
		if (tContNo == "") {
			alert("保单号不能为空!");
			return "";
		}
	}

	strSQL = "select ContNo,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,state,serialno from lcissuepol where "
			+ k
			+ "="
			+ k
			+ " "
			+ " and OperatePos in ('0','1','5')"
			+ " and backobjtype = '3'"
			+ " and IsueManageCom like '"
			+ manageCom
			+ "%%'"
			+ getWherePart('ContNo', 'ContNo')
			+ getWherePart('BackObjType', 'BackObj')
			+ getWherePart('ManageCom', 'ManageCom')
			+ getWherePart('IsueManageCom', 'OManageCom')
			+ getWherePart('OperatePos', 'OperatePos')
			+ getWherePart('IssueType', 'Quest');

	// 查询SQL，返回结果字符串
	turnPage1.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

	// 判断是否查询成功
	if (turnPage1.strQueryResult == false) {
		// alert("没有问题件");
		return "";
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);

	// 设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	turnPage1.pageDisplayGrid = QuestGrid;

	// 保存SQL语句
	turnPage1.strQuerySql = strSQL;

	// 设置查询起始位置
	turnPage1.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet,
			turnPage1.pageIndex, MAXSCREENLINES);

	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
	return true;
}

function queryone(parm1, parm2) {
	k++;

	var strSQL = "";
	var tcho;
	var tOperatePos = fm.Flag.value;

	var row = QuestGrid.getSelNo();
	tPos = QuestGrid.getRowColDataByName(row - 1, "OperatePos");
	tQuest = QuestGrid.getRowColDataByName(row - 1, "IssueType");
	tSerialNo = QuestGrid.getRowColDataByName(row - 1, "SerialNo");

	fm.BackObj.value = QuestGrid.getRowColDataByName(row - 1, "BackObj");
	fm.OperatePos.value = tPos;

	fm.all('HideOperatePos').value = tPos;
	fm.all('HideQuest').value = tQuest;
	fm.all('HideSerialNo').value = tSerialNo;
	fm.all('SerialNo').value = tSerialNo;
	tContNo = fm.ContNo.value;
	if (tPos == "") {
		alert("请选择问题件!");
		return "";
	}

	if (tContNo == "") {
		alert("保单号不能为空！");
		return "";
	}
	if (tQuest == "") {
		alert("问题件不能为空！");
		return "";
	}
	if (tSerialNo == "") {
		alert("问题件编码不能为空！");
		return "";
	}

	if (tOperatePos == "16") {

		strSQL = "select issuecont,replyresult,issuetype,OperatePos from lcgrpissuepol where "
				+ k
				+ "="
				+ k
				+ " "
				+ " and grpcontno = (select grpcontno from lccont where contno = '"
				+ tContNo + "')";
	} else {
		strSQL = "select issuecont,replyresult,issuetype,OperatePos from lcissuepol where "
				+ k
				+ "="
				+ k
				+ " "
				+ getWherePart('ContNo', 'ContNo')
				+ getWherePart('issuetype', 'HideQuest')
				+ getWherePart('OperatePos', 'HideOperatePos')
				+ getWherePart('SerialNo', 'HideSerialNo');
	}

	// 查询SQL，返回结果字符串
	turnPage1.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

	// 判断是否查询成功
	if (!turnPage1.strQueryResult) {
		alert("没有录入过问题键！");
		return "";
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);

	var returnstr = "";
	var tcont = "";
	var treply = "";
	var ttype = "";
	var tOperatePos = "";
	var n = turnPage1.arrDataCacheSet.length;
	if (n > 0) {
		m = turnPage1.arrDataCacheSet[0].length;
		if (m > 1) {
			tcont = turnPage1.arrDataCacheSet[0][0];
			treply = turnPage1.arrDataCacheSet[0][1];
			ttype = turnPage1.arrDataCacheSet[0][2];
			tOperatePos = turnPage1.arrDataCacheSet[0][3];
		} else {
			alert("没有录入过问题键！");
			return "";
		}

	} else {
		alert("没有录入过问题键！");
		return "";
	}

	if (tcont == "") {
		alert("没有录入过问题键！");
		return "";
	}

	fm.all('Content').value = tcont;

	if (treply == "") {
		canReplyFlag = true;
	} else {
		canReplyFlag = false;
	}

	fm.all('Type').value = ttype;
	fm.all('OperatePos').value = tOperatePos;
	return returnstr;
}

function input() {
	cContNo = fm.ContNo.value; // 保单号码

	window.open("./QuestInputMain.jsp?ContNo1=" + cContNo + "&Flag=" + cflag,
			"window1");

}

function quickReply() {
	if (QuestGrid.getSelNo() == "0") {
		alert("请先选择一条问题件！");
		return;
	}
	reply();
}

function QuestQuery(tContNo, tFlag) {
	// 初始化表格
	var i, j, m, n;

	// 书写SQL语句
	k++;
	var strSQL = "select code,cont from ldcodemod where " + k + "=" + k
			+ " and codetype = 'Question'";

	// 查询SQL，返回结果字符串
	turnPage1.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

	// 判断是否查询成功
	if (!turnPage1.strQueryResult) {
		alert("没有问题件描述");
		return "";
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);

	var returnstr = "";
	var n = turnPage1.arrDataCacheSet.length;
	if (n > 0) {
		for (i = 0; i < n; i++) {
			m = turnPage1.arrDataCacheSet[i].length;
			if (m > 0) {
				for (j = 0; j < m; j++) {
					if (i == 0 && j == 0) {
						returnstr = "0|^" + turnPage1.arrDataCacheSet[i][j];
					}
					if (i == 0 && j > 0) {
						returnstr = returnstr + "|"
								+ turnPage1.arrDataCacheSet[i][j];
					}
					if (i > 0 && j == 0) {
						returnstr = returnstr + "^"
								+ turnPage1.arrDataCacheSet[i][j];
					}
					if (i > 0 && j > 0) {
						returnstr = returnstr + "|"
								+ turnPage1.arrDataCacheSet[i][j];
					}

				}
			} else {
				alert("查询失败!!");
				return "";
			}
		}
	} else {
		alert("查询失败!");
		return "";
	}
	fm.Quest.CodeData = returnstr;
	return "";
}

/**
 * 查询发送问题件
 */
var turnPage2 = new turnPageClass();
function queryQuestLetters(cContNo) {
	var tStrSql = " select "
			+ " lopm.PrtSeq, lopm.MakeDate, lopm.DoneDate, "
			+ " (select edm.MakeDate from Es_Doc_Main edm where edm.DocCode = lopm.PrtSeq and edm.SubType ='TB22'), "
			+ " (case lopm.StateFlag when '0' then '未打印' else '已打印' end) "
			+ " From LOPrtManager lopm " + " where 1 = 1 "
			+ " and Code = '85' " + " and OtherNo = '" + cContNo + "' ";
	turnPage2.queryModal(tStrSql, SendQuestGrid);
}

function showPics() {
	var tSel = SendQuestGrid.getSelNo() - 1;

	if (tSel == -1 || tSel == null) {
		alert("请先选择一条记录，再点击明细查询按钮。");
		return false;
	}

	var tReturnDate = SendQuestGrid.getRowColData(tSel, 4);
	if (tReturnDate == "") {
		alert("该问题件尚未回销。");
	}

	var tPrtSeq = SendQuestGrid.getRowColData(tSel, 1);

	var tStrSql = " select edm.DocId, edm.DocCode, edm.BussType, edm.SubType "
			+ " from Es_Doc_Main edm " + " where 1 = 1 "
			+ " and edm.DocCode = '" + tPrtSeq + "' ";

	var tEasyWay = "1";
	var tDocID = "";
	var tDocCode = "";
	var tBussTpye = "";
	var tSubType = "";

	var tResult = easyExecSql(tStrSql);
	if (tResult) {
		tDocID = tResult[0][0];
		tDocCode = tResult[0][1];
		tBussTpye = tResult[0][2];
		tSubType = tResult[0][3];
	}

	var tUrl = "../common/EasyScanQuery/EasyScanQuery.jsp" + "?EASYWAY="
			+ tEasyWay + "&DocID=" + tDocID + "&DocCode=" + tDocCode
			+ "&BussTpye=" + tBussTpye + "&SubTpye=" + tSubType;
	window.open(tUrl, "fraPic");
}
