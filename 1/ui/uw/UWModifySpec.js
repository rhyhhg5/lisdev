//程序名称：UWModifySpec.js
//程序功能：特约查询
//创建日期：2002-09-24 11:10:36
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();

function queryClick() {
	// 书写SQL语句
  var strSql = "select b.prtno, b.proposalno, b.polno, a.speccontent, a.operator, a.modifydate, a.modifytime "
             + " from lcspec a, lcpol b where a.polno=b.polno and a.specno="
             + "(select max(c.specno) from lcspec c where c.polno=b.polno)"
             + getWherePart('b.prtno', 'PrtNo')
             + getWherePart('b.polno', 'PolNo');

  turnPage.queryModal(strSql, QuestGrid); 
  
  fm.Content.value = "";
}


function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
	if(fm.all(parm1).all('InpQuestGridSel').value == '1' ) {
	  var index = fm.all(parm1).all('QuestGridNo').value - 1;
	  fm.Content.value = turnPage.arrDataCacheSet[index][3];
	  
	  fm.PolNo2.value = turnPage.arrDataCacheSet[index][2];
  }
}

function insertClick() { 
  if (trim(fm.Content.value) == "") {
    alert("必须填写特约内容！");
    return;
  }
  
  if (trim(fm.PolNo2.value) == "") {
    alert("必须填写保单号！");
    return;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {   
  showInfo.close();         
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}
