<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpReInsureInit.jsp
//程序功能：人工核保未过原因查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
  String tGrpContNo = "";
  String tGrpPolNo = "";
  tGrpContNo = request.getParameter("GrpContNo");
%>                            

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 

}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在GrpReInsureInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(tGrpContNo, tGrpPolNo)
{
  try
  {

	initReInsureGrid();
  
	initRiskInfoGrid();
	
	initHide(tGrpContNo, tGrpPolNo);
	
	initReInsureAuditGrid();		
	
  }
  catch(re)
  {
    alert("GrpReInsureInit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
  }
  	QueryRiskInfo();
  	ReInsureAudit();
  	QueryReInsureAudit();
   var tmSql="select uwidea from LCReInsurUWIdea where polno = '"+tGrpContNo+"' and uwno =(select max(uwno) from LCReInsurUWIdea where  PolNo='"+tGrpContNo+"')";
   var arr=easyExecSql(tmSql);	
   if(arr){
   	fm.ApplyRemark.value=arr[0][0];
   	}
   var ttSql="select uwidea from LCReInsurUWIdea where polno = '"+tGrpContNo+"'||'BJ' and uwno =(select max(uwno) from LCReInsurUWIdea where  PolNo='"+tGrpContNo+"'||'BJ')";
   var arr=easyExecSql(ttSql);	
   if(arr){
   	fm.BJRemark.value=arr[0][0];
   	}
   var aaSql = "select State from LCReInsurTask where PolNo ='"+tGrpContNo+"' and uwno =(select max(uwno) from LCReInsurUWIdea where  PolNo='"+tGrpContNo+"')";
   var arr=easyExecSql(aaSql);	
   if(arr){
   	  if(arr[0][0]=="1"){
   	fm.LFState1No.value=arr[0][0];
   	fm.LFState1.value="未临分";
      }
    else if(arr[0][0]=="2"){
   	fm.LFState1No.value=arr[0][0];
   	fm.LFState1.value="已临分";    	
    	}  
   	}
   	
   var abSql = "select State from LCReInsurTask where PolNo ='"+tGrpContNo+"'||'BJ' and uwno =(select max(uwno) from LCReInsurUWIdea where  PolNo='"+tGrpContNo+"'||'BJ')";
   var arr=easyExecSql(abSql);	
   if(arr){
   	  if(arr[0][0]=="1"){
   	fm.LFStateNo.value=arr[0][0];
   	fm.LFState.value="未临分";
      }
    else if(arr[0][0]=="2"){
   	fm.LFStateNo.value=arr[0][0];
   	fm.LFState.value="已临分";    	
    	}  
   	}    	      	
}

// 责任信息列表的初始化
function initReInsureGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      	
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保单位";    	//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种代码";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="再保规则";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     


      ReInsureGrid = new MulLineEnter( "fm" , "ReInsureGrid" ); 
      //这些属性必须在loadMulLine前
      ReInsureGrid.mulLineCount = 0;   
      ReInsureGrid.displayTitle = 1;
      ReInsureGrid.locked = 1;
      ReInsureGrid.hiddenPlus = 1;
      ReInsureGrid.hiddenSubtraction = 1;
      ReInsureGrid.loadMulLine(iArray);  
      }
      
      catch(ex)
      {
        alert(ex);
      }
}



// 责任信息列表的初始化
function initRiskInfoGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保单位";    	//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种代码";         			//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="险种名称";                  //列名
      iArray[3][1]="80px";                  //列宽
      iArray[3][2]=100;                     //列最大值
      iArray[3][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="最高职业类别";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[5]=new Array();
      iArray[5][0]="最高保额";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="保费";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="再保任务状态";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="险种号";         		//列名
      iArray[8][1]="20px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][21]="GrpPolNo";  


      RiskInfoGrid = new MulLineEnter( "fm" , "RiskInfoGrid" ); 
      //这些属性必须在loadMulLine前
      RiskInfoGrid.mulLineCount = 0;   
      RiskInfoGrid.displayTitle = 1;
      RiskInfoGrid.locked = 1;
      RiskInfoGrid.hiddenPlus = 1;
      RiskInfoGrid.canSel = 1;
      RiskInfoGrid.hiddenSubtraction = 1;
      RiskInfoGrid.loadMulLine(iArray);  
      }
      
      catch(ex)
      {
        alert(ex);
      }
}
function initReInsureAuditGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种名称";    	//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种代码";         			//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="发送序号";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="发送人";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="审核意见";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="发送时间";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="附件路径";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="有无附件";         		//列名
      iArray[8][1]="50px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      ReInsureAuditGrid = new MulLineEnter( "fm" , "ReInsureAuditGrid" ); 
      //这些属性必须在loadMulLine前
      ReInsureAuditGrid.mulLineCount = 0;   
      ReInsureAuditGrid.displayTitle = 1;
      ReInsureAuditGrid.locked = 1;
      ReInsureAuditGrid.canSel = 1;
      ReInsureAuditGrid.hiddenPlus = 1;
      ReInsureAuditGrid.hiddenSubtraction = 1;
      ReInsureAuditGrid.loadMulLine(iArray);  
      }
      
      catch(ex)
      {
        alert(ex);
      }
}
function initHide(tGrpContNo, tGrpPolNo)
{
	fm.all('GrpContNo').value=tGrpContNo;
}

</script>


