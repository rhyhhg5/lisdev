//程序名称：ContInfoModify.js
//程序功能：新契约信息修改
//创建日期：2002-09-24 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();


function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	// 书写SQL语句
	var strSql = "select l.ContNo,l.PrtNo,l.PolApplyDate,l.CValiDate,l.AppntName,getUniteCode(l.agentcode),l.ManageCom, "
	                + "l.SaleChnl, l.AgentCom "
	                +" from LCCont l where 1=1 "
    				+" and conttype='1'"
    				+" and appflag in ('0','9')"
    				+" and uwflag not in ('a','1','8')"
    				+ " and l.manageCom like '"+manageCom+"%%'"
    				+ getWherePart( 'l.PrtNo','PrtNo' )	    			
    				+ getWherePart( 'l.AppntName' ,'AppntName' )
    				+ getWherePart( 'l.InsuredName','InsuredName' )
    				+ getWherePart( 'getUniteCode(l.agentcode)','AgentCode' )
    				+ getWherePart( 'l.PolApplyDate','PolApplyDate' ) 
    				+ getWherePart( 'l.CValiDate','CValiDate' )
    				+ getWherePart("l.SaleChnl","SaleChnlCode")
                    + getWherePart("l.AgentCom","AgentComBank")	
    				+ " and managecom like '" + manageCom + "%%'";  //集中权限管理体现
	turnPage.pageLineNum = 50;
	turnPage.strQueryResult  = easyQueryVer3(strSql);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合查询条件的记录");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  fm.querySql.value = strSql;

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {   
  
  showInfo.close(); 
  window.focus();       
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}

//初始化合同信息
function showContInfo()
{
	var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cProposalContNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	var cprtno = PolGrid.getRowColData(checkFlag - 1, 2); 	
  	
    urlStr = "./ProposalInfoModifyMain.jsp?ContNo="+cProposalContNo+"&prtNo="+cprtno ,"status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";   
  
    window.open(urlStr);
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
   // alert(showInfo.fm);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}

function downloadClick()
{
    if(PolGrid.mulLineCount==0)
    {
        alert("列表中没有数据可下载");
    }
    var oldAction = fm.action;
    fm.action = "ContInfoModifyDownLoad.jsp";
    fm.submit();
    fm.action = oldAction;
}