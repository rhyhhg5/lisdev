<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：UWManuRReportChk.jsp
	//程序功能：新契约人工核保发送核保通知书报告录入
	//创建日期：2002-06-19 11:10:36
	//创建人  ：WHN
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.workflowengine.*"%>
<%
	//输出参数
	CErrors tError = null;
	CErrors cError = null;
	String FlagStr = "Succ";
	String Content = "";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	System.out.println("\n\n");
	if (tG == null) {
		out.println("session has expired");
		return;
	}

	//接收信息
	TransferData tTransferData = new TransferData();
	String tContNo = request.getParameter("ContNo");
	String tPrtNo = request.getParameter("PrtNoHide");
	System.out.println("Contno:" + tContNo);
	System.out.println("tPrtNo:" + tPrtNo);
	boolean flag = true;
	if (!tContNo.equals("") && !tPrtNo.equals("")) {
		//准备公共传输信息
		tTransferData.setNameAndValue("ContNo", tContNo);
		tTransferData.setNameAndValue("PrtNo", tPrtNo);
	} else {
		flag = false;
		Content = "数据不完整!";
	}
	System.out.println("flag:" + flag);
	try {
		if (flag == true) {
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add(tTransferData);
			tVData.add(tG);

			// 数据传输
			UWPrepareIssueCopyBL tUWPrepareIssueCopyBL = new UWPrepareIssueCopyBL();
			if (!tUWPrepareIssueCopyBL.submitData(tVData, ""))//执行保全核保生调工作流节点0000000004
			{
				int n = tUWPrepareIssueCopyBL.mErrors.getErrorCount();
				Content = " 发送问题件通知书失败，原因是: "
						+ tUWPrepareIssueCopyBL.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			PubSubmit ps = new PubSubmit();
			if (!ps.submitData(tUWPrepareIssueCopyBL.getResult(), ""))//执行保全核保生调工作流节点0000000004
			{
				int n = ps.mErrors.getErrorCount();
				Content = " 发送问题件通知书失败，原因是: "
						+ ps.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			MMap map = (MMap)tUWPrepareIssueCopyBL.getResult().getObjectByObjectName("MMap", 0);
			LCIssuePolSet mLCIssuePolSet = (LCIssuePolSet)map.getObjectByObjectName("LCIssuePolSet", 0);
			String serialno = mLCIssuePolSet.get(1).getSerialNo();
			if("Succ".equals(FlagStr)){
				Content = "发送问题件通知书成功！";
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		Content = Content.trim() + ".提示：异常终止!";
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");
</script>
</html>
