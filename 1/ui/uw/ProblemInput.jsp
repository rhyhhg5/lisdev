<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%> 
<%
//程序名称：QuestInput.jsp
//程序功能：问题件录入
//创建日期：2005-03-19 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="ProblemInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <title>核保问题件录入</title>
  <%@include file="ProblemInputInit.jsp"%>
</head>
<body  onload="initForm('<%=tContNo%>','<%=tFlag%>');" >
  <form method=post name=fm target="fraSubmit" action= "./QuestInputChk.jsp">
	<table>
		<tr>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
			</td>
			<td class= titleImg>问题件信息</td>
		</tr>
	</table>
    <table class= common>
    	<TR  class= common>
          <TD  class= title>
            返回对象  
          </TD>
          <TD  class= input>
            <Input class=codeNo name=BackObj verify="返回对象|notnull&code:BackObj"  ondblclick= "showCodeListEx('BackObj',[this,BackObjName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('BackObj',[this,BackObjName],[0,1],null,null,null,1);" ><input class=codename name=BackObjName readonly=true >
          </TD> 

         <TD id=Obj1 class= title>
            问题对象  
          </TD>
          <TD id=Obj2 class= input>
            <Input class=codeno  name=QuestionObj verify="问题对象|notnull&code:QuestionObj" ondblClick="showCodeListEx('QuestionObj',[this,QuestionObjName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('QuestionObj',[this,QuestionObjName],[0,1],null,null,null,1);" onFocus= "easyQueryClickSingle();" ><input class=codename name=QuestionObjName readonly=true >
          </TD> 
        </tr>
        <tr class=common>
          <Input  name=QuestionObjNo  type=hidden >
        </TR>
        <tr class=common>
          <TD  class= title>
            问题字段  
          </TD>
          <TD  class= input>
            <Input class=code name=QuestionFiedeObj verify="问题字段|notnull&code:issueerrfield" ondblclick="return showCodeList('issueerrfield',[this,QuestionFieldValue,SqlObj],[0,1,2],null,null,null,1);" onkeyup="return showCodeListKey('issueerrfield',[this,QuestionFieldValue,SqlObj],[0,1,2],null,null,null,1);">
          </TD>
          <TD class= input  >
            <Input class= common name=QuestionFieldValue>
          </TD> 
          <TD  class= title>
            原填写内容
          </td>
          <Input name=SqlObj type=hidden>
          <TD class= input  >
            <Input class= common name=OldFieldValue verify="原填写内容|len<=20">
          </TD> 
        </TR>
		<tr class=common>
    	<TD class= title>
    	  问题件内容
    	</TD>
         <TD  class= input>
            <Input class=code name=Quest verify="问题件内容|notnull&code:issuecontent" ondblclick="return showCodeList('issuecontent',[this,Content],[0,1]);" onkeyup="return showCodeListKey('issuecontent',[this,Content],[0,1]);">
          </TD>
          <TD class= input  colspan=4>
            <Input class= common3 name=Content>
          </TD>         
    	</tr>   	
    </table>
    <table common =class>
		<input type= "button" name= "sure" class= cssButton  align=right value="增  加" onClick="submitForm();">
		<input type= "button" name= "sure" class= cssButton  align=right value="删  除" onClick="deleteClick();">
		<input type= "button" id="SendIssueID" name= "SendIssueButton" class= cssButton  align=right value="发问题件通知书 " onClick="SendIssue();">
        <input class="cssButton" type="button" id="btnQuestBack" name="btnQuestBack" value="问题件回退" onclick="questBack();" />
	</table>
  <p> 
    <!--读取信息-->
    <input type= "hidden" name= "Flag" value="">
    <input type= "hidden" name= "ContNo" value= "">
    <input type= "hidden" name= "MissionID" value= "">
  </p>
  	<table>
		<tr>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
			</td>
			<td class= titleImg>问题件</td>
		</tr>
	</table>
	<Div  id= "divIssue" style= "display: ''">
        <table  class= common>
            <tr  class= common>
                <td text-align: left colSpan=1>
                    <span id="spanIssueGrid" >
                    </span>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" class= Common name= SubNoticeMissionID  value= "">  
    <input type="hidden" name= "PrtNoHide" value= "">
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="SerialNo" name="SerialNo">
    <input type=hidden id="fmAction" name="fmAction">
</form>
  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>