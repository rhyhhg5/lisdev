<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InsuredDiseaseQueryInit.jsp
//程序功能：人工核保被保人信息
//创建日期：2006-11-17 11:18
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
    out.println("session has expired");
    return;
  }

String strOperator = globalInput.Operator;
%>

<script language="JavaScript">


function initForm() {
  try {
      initDiseaseGrid();
      easyQueryClick();
    } catch(re) {
      alert("在InsuredDiseaseQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
    }
}


// 保单信息列表的初始化
function initDiseaseGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="风险代码";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="疾病简述";         		//列名
      iArray[2][1]="240px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="风险描述";         		//列名
      iArray[3][1]="240px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="风险等级";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="观察期";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="核保原则";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="险类风险";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="体检标识";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="病历资料";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="疾病问卷";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
      //这些属性必须在loadMulLine前
      DiseaseGrid.mulLineCount = 0;
      DiseaseGrid.displayTitle = 1;
      DiseaseGrid.locked = 1;
      DiseaseGrid.canSel = 0;
      DiseaseGrid.hiddenPlus = 1;
      DiseaseGrid.hiddenSubtraction = 1;
      DiseaseGrid.loadMulLine(iArray);

      DiseaseGrid.selBoxEventFuncName = "easyQueryDisease";

      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
    } catch(ex) {
      alert(ex);
    }
}

</script>
