<%@page contentType="text/html;charset=gb2312"%>
<jsp:directive.page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"/>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//GrpSendRReportSave.jsp
	//程序功能：新契约人工核保发送核保通知书报告录入
	//创建日期：2005-08-5 
	//创建人  ：cuiwei
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	if (tG == null) {
		System.out.println("页面失效,请重新登陆");
		FlagStr = "Fail";
		Content = "页面失效,请重新登陆";
	} else {
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		tLOPRTManagerSchema.setPrtSeq(request.getParameter("PrtSeq"));
		tLOPRTManagerSchema.setCode(request.getParameter("CodeType"));
		System.out.println("删除单证："+request.getParameter("PrtSeq"));
		//接收信息
		boolean flag = true;

		try {
			
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLOPRTManagerSchema);
		tVData.add(tG);

		HeathIssueManagerBL tHeathIssueManagerBL = new HeathIssueManagerBL();
		if (tHeathIssueManagerBL.submitData(tVData, "DELETE") == false) {
			
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail") {
			tError = tHeathIssueManagerBL.mErrors;
			if (!tError.needDealError()) {
				Content = "单证删除成功！";
				FlagStr = "Succ";
			} else {
				Content = " 单证删除失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	
		} catch (Exception e) {
			e.printStackTrace();
			Content = Content.trim() + ".提示：异常终止!";
		}

	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
