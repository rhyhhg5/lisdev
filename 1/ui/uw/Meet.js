//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		//tMissionID = PolGrid.getRowColData(tSel-1,10);
		//tSubMissionID = PolGrid.getRowColData(tSel-1,11);
		//tContNo = PolGrid.getRowColData(tSel-1,12);
		tMissionID = "";
		tSubMissionID = "";
		tContNo = "";
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "CONFIRM";
		//fmSave.target = "../f1print";
		fmSave.action="./MeetF1PSave.jsp?RePrintFlag="+RePrintFlag;
		fmSave.submit();
	}
}

function printPolpdf()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
	}
	else
	{
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);		
		//tMissionID = PolGrid.getRowColData(tSel-1,10);
		//tSubMissionID = PolGrid.getRowColData(tSel-1,11);
		//tContNo = PolGrid.getRowColData(tSel-1,12);
		tMissionID = "";
		tSubMissionID = "";
		tContNo = "";
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "PRINT";
		fmSave.action = "../uw/PrintPDFSave.jsp?Code=004&RePrintFlag="+RePrintFlag;
		fmSave.submit();
}
}

//打印保全面见通知书（PDF）
function printLPPolpdf()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
	}
	else
	{
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);		
		//tMissionID = PolGrid.getRowColData(tSel-1,10);
		//tSubMissionID = PolGrid.getRowColData(tSel-1,11);
		//tContNo = PolGrid.getRowColData(tSel-1,12);
		tMissionID = "";
		tSubMissionID = "";
		tContNo = "";
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "PRINT";
		var showStr="正在准备打印数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fmSave.target = "fraSubmit";
		fmSave.action = "../uw/PDFPrintSave.jsp?Code=04&OtherNo="+PolGrid.getRowColData(tSel-1,12)+"&OldPrtSeq="+tPrtSeq+"&OtherNoType=02";
		fmSave.submit();
}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
    // 初始化表格
    initPolGrid();
    
    var agentGroupSQL = "";//中介公司
    if(fm.agentGroup.value != null && fm.agentGroup.value != "")
        agentGroupSQL = "and b.AgentGroup in (select AgentGroup from LABranchGroup bg where bg.AgentGroup = '" + fm.agentGroup.value + "') ";
    
    var stateFlagSQL = "";//打印状态
    if(RePrintFlag != "1")
        stateFlagSQL = "and a.StateFlag = '0' ";
    
    //2014-11-4  杨阳
	//把新的业务员编码转成旧的
    var agentCodesql ="";
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		agentCodesql=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	
    // 书写SQL语句
    var strSQL = "SELECT a.PrtSeq, b.PrtNo, b.AppntName, c.Name, (  select getUniteCode(b.Agentcode) from dual), b.CvaliDate, "
        + "a.Makedate, b.ManageCom, b.PrtNo, c.Operator, c.Contente, a.otherno "
        + "FROM LOPRTManager a, LCCont b, LCRReport c "
        + "WHERE a.Code = '04' and b.UWFlag not in ('1','8','a') "
        + "and a.OtherNo = b.ContNo "
        + "and c.PrtSeq = a.PrtSeq and a.OtherNo = c.ContNo "
        + getWherePart('b.PrtNo', 'PrtNo')
        + getWherePart('b.ContNo', 'ContNo')
        + getWherePart('b.ManageCom', 'ManageCom', 'like')
        + getWherePart('b.AppntName','AppntName')
        + getWherePart('a.Makedate','MakeDate')
        + getWherePart('b.CvaliDate','CvaliDate')
        + agentGroupSQL
        + stateFlagSQL
        + agentCodesql
        ;
		
        //添加连接查询，查找LPRReport中的契调数据 add by Houyd 20140126
    strSQL += "union SELECT a.PrtSeq, b.PrtNo, b.AppntName, c.Name,(  select getUniteCode(b.Agentcode) from dual), b.CvaliDate, "
        + "a.Makedate, b.ManageCom, b.PrtNo, c.Operator, c.Contente, a.otherno "
        + "FROM LOPRTManager a, LCCont b, LPRReport c "
        + "WHERE a.Code = '04' and b.UWFlag not in ('1','8','a') "
        + "and a.OtherNo = b.ContNo "
        + "and c.PrtSeq = a.PrtSeq and a.OtherNo = c.ContNo "
        + getWherePart('b.PrtNo', 'PrtNo')
        + getWherePart('b.ContNo', 'ContNo')
        + getWherePart('b.ManageCom', 'ManageCom', 'like')
        + getWherePart('b.AppntName','AppntName')
        + getWherePart('a.Makedate','MakeDate')
        + getWherePart('b.CvaliDate','CvaliDate')
        + agentGroupSQL
        + agentCodesql
        ;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
        alert("没有要打印的契调通知书！");
        return false;
    }
    
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
    
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    
    //保存SQL语句
    turnPage.strQuerySql     = strSQL;
    
    //设置查询起始位置
    turnPage.pageIndex       = 0;
    
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //tArr = chooseArray(arrDataSet,[0,1,3,4])
    //调用MULTILINE对象显示查询结果
    
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    //displayMultiline(tArr, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}


function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  	fm.agentGroup.value = arrResult[0][0];
  }
}

//下拉框选择后执行   zhangjianbao   2007-11-16
function afterCodeSelect(cName, Filed)
{	
  if(cName=='statuskind')
  {
    saleChnl = fm.saleChannel.value;
	}
}

//选择营销机构前执行   zhangjianbao   2007-11-16
function beforeCodeSelect()
{
	if(saleChnl == "") alert("请先选择销售渠道");
}

//体检通知书清单下载
function downloadList()
{
    if(fm.querySql.value != "" && PolGrid.mulLineCount == 0)
    {
      alert("没有需要下载的数据");
      return false;
    }
    fm.target = '_blank';
    fm.action = "MeetDownload.jsp";
    fm.submit();
}

function printPolpdfNew()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
	}
	else
	{
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);		
		//tMissionID = PolGrid.getRowColData(tSel-1,10);
		//tSubMissionID = PolGrid.getRowColData(tSel-1,11);
		//tContNo = PolGrid.getRowColData(tSel-1,12);
		tMissionID = "";
		tSubMissionID = "";
		tContNo = "";
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "PRINT";
		var showStr="正在准备打印数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fmSave.target = "fraSubmit";
		fmSave.action = "../uw/PDFPrintSave.jsp?Code=04&OtherNo="+PolGrid.getRowColData(tSel-1,12)+"&OldPrtSeq="+tPrtSeq;
		fmSave.submit();
}
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
