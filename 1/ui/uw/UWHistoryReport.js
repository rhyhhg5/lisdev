//程序名称：UWHistoryReport.js
//程序功能：既往契调件查询
//创建日期：2012-10-19
//创建人  ：张成轩
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mDebug="0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
var cflag = "";  //问题件操作位置 1.核保
var mCustomerNo ="";

var contState = "C";

function initContState(){
	var sql = "select 1 from lccont where proposalcontno='" 
			+ contNo 
			+ "' union all "
			+ "select 2 from lbcont where proposalcontno='" 
			+ contNo 
			+ "' and edorno not like 'xb%'";
	var rs = easyExecSql(sql);
	if(rs){
		if(rs[0][0] == "2") {
			// 退保保单
			contState="B";
		} else if(rs[0][0] == "1") {
			contState="C";
		}
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

// 查询按钮
function easyQueryClick()
{
  var strsql = "";
  var tPrtSeq="";
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<LCRReportGrid.mulLineCount; i++) 
  {
    if (LCRReportGrid.getSelNo(i)) 
    { 
      checkFlag = LCRReportGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) 
  {
    tPrtSeq = LCRReportGrid.getRowColData(checkFlag - 1, 1);
    var	tContNo = LCRReportGrid.getRowColData(checkFlag - 1, 2);
    var	tcustomer = LCRReportGrid.getRowColData(checkFlag - 1, 11);
    fm.all('PrtSeq').value=tPrtSeq;
    fm.all('customer').value=tcustomer;
  }
  else 
  {
    alert("请先选择一条契调回销信息！"); 
    return;
  }
  
  if(tContNo != "")
  {
    strsql = "select b.RReportItemCode,a.name,b.RReportItemName,b.RRITEMCONTENT,b.RRITEMRESULT from lcrreport a,lcrreportitem b where a.contno = '"+tContNo+"' and a.PrtSeq='"+tPrtSeq+"' and a.contno=b.contno and a.PrtSeq=b.PrtSeq";
    //alert(strsql);

    //查询SQL，返回结果字符串
    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult)
    {
      alert("未查到该客户的契调项目明细报告信息！");
      initQuestGrid();
      return true;
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = QuestGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strsql;

    //设置查询起始位置
    turnPage.pageIndex       = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    //divAddDelButton.style.display = "";
    var strsqlre = "select contente,replycontente from LCRReport where "
             + " ProposalContNo = '"+ tContNo + "'"
             + " and PrtSeq = '"+ tPrtSeq + "'";
    var arrreport = easyExecSql(strsqlre);
    if(arrreport)
    {
      fm.Contente.value=arrreport[0][0];
      fm.ReplyContente.value=arrreport[0][1];
    }
    //************
   if(fm.LoadFlag.value!=1)
   {
     divRReportButton.style.display = "";
   } 
  }
  return true;
}

// 查询按钮
function easyQueryChoClick(parm1,parm2)
{
  // 书写SQL语句
  k++;
  var tContNo = "";
  var strSQL = "";
	
  if(fm.all(parm1).all('InpQuestGridSel').value == '1' )
  {
    //当前行第1列的值设为：选中
    tContNo = fm.all(parm1).all('QuestGrid1').value;
    tSerialNo = fm.all(parm1).all('QuestGrid5').value;
    
  }

  if (tContNo != "")
  {
    strSQL = "select a.name,b.RReportItemName,a.RReportReason,,from LCRReport a,LCRReportResult b where contno = '"+tContNo+"' and prtseq = '"+tSerialNo+"'";
  }

  var strsql= "select Contente from LCRReport where 1=1"
              +" 	and ContNo = '"+tContNo+"'";


  var arrReturn = new Array();
  arrReturn = easyExecSql(strsql);

  if(arrReturn!=null)
  {

    fm.all('Contente').value = arrReturn[0][0];
  }

  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
    return "";

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = RReportGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  var strSql = "select RReportResult,ICDCode from LCRReportResult where contno='"+tContNo+"' and customerno='"+mCustomerNo+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    turnPage.queryModal(strSql,RReportResultGrid);
  }

  var strSql = "select ReplyContente from LCRReport where contno='"+tContNo+"' and customerno='"+mCustomerNo+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    fm.ReplyContente.value = arr[0][0];
  }
  return true;
}

function QueryCustomerList(tContNo)
{
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select appntno, appntname from l" + contState + "appnt where contno = (select contno from l" + contState + "cont where proposalcontno = '" + tContNo + "')";
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++)
    {
      j = i + 1;
      tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][1] + "|" + turnPage.arrDataCacheSet[i][0];
    }
  }
  strsql = "select insuredno, name from l" + contState + "insured where contno = (select contno from l" + contState + "cont where proposalcontno = '" + tContNo + "') and not insuredno in (select appntno from l" + contState + "appnt where contno = '" + tContNo + "')";
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != null && turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    n = turnPage.arrDataCacheSet.length;
    for (i = 0; i < n; i++)
    {
      j = i + m + 1;
      tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][1] + "|" + turnPage.arrDataCacheSet[i][0];
    }
  }

  return tCodeData;
}

function showSelectRecord(tContNo)
{
  easyQueryClick(tContNo, fm.all("CustomerNo").value);
}

function afterCodeSelect(cCodeName, Field)
{
  if (cCodeName == "CustomerName")
  {
    showSelectRecord(fm.all("ContNo").value);
  }
}

function easyQueryLCRRClick(tContNo)
{
	// 初始化表格
	initLCRReportGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select c.PrtSeq,a.ContNo,a.Name,c.makedate,b.PrtNo,b.PolApplyDate,b.CValiDate,a.AppntName,b.AgentCode,a.ManageCom,a.customerno,a.Appntno,'','',a.replyoperator,a.replydate from LCRReport a ,l" + contState + "cont b ,LOPRTManager c where 1=1 and a.proposalcontno=b.proposalcontno and a.PrtSeq=c.PrtSeq  "
	                 + "and a.ManageCom like '"+manageCom+"%%'"
	                 + "and a.proposalcontno = '"+tContNo+"'"
				           + " order by a.makedate, a.maketime"
				 ;	 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有找到相关的数据！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = LCRReportGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}