//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  
  //if(!verifyInput()) return false;
  var prtno = "";
  var tSel = PolGrid.getSelNo();  
  var tSel1 = ContGrid.getSelNo();
	
	if((tSel == 0 || tSel == null) && (tSel1 == 0 || tSel1 == null) )
	{
			alert("请选择一条记录")
			return;
	}	
else if(tSel == 0 || tSel == null ) 
	{
		prtno = ContGrid.getRowColData(tSel1 - 1,1);
	}
  else if(tSel1 == 0 || tSel1 == null)
  {
  	prtno = PolGrid.getRowColData(tSel - 1,1);
  }
  else
	{
		alert("不能同时选择两条记录")
		return;
	}

	
  fm.PrtNo1.value = prtno;
 
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function queryPol()
{
  //调用查询投保单的文件
	 window.open("./QuestionAdditionalRiskQuery.html");
}
function easyQueryClick()
{

	var strSQL = "";		
	// 书写SQL语句
       	strSQL = " select prtno,polapplydate,cvalidate,appntname,agentcode,managecom from lccont "
       	    + " where contno in (select otherno from loprtmanager where stateflag='3' and (current date - formakedate) > 3 )"
       	    + " and uwflag not in ('a','1','8')"
       	   
       		//	+ getWherePart('LCCont.CvaliDate','CvaliDate')
       			//+ getWherePart('LCCont.AppntName','AppntName')
			    	//+ getWherePart('LCCont.PolApplyDate','PolApplyDate')
       			;

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  // alert("没有该保单！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 //tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function easyQueryClick1()
{

	var strSQL = "";		
	
	// 书写SQL语句
       	strSQL = " select prtno,polapplydate,cvalidate,grpname,getUniteCode(AgentCode),managecom from LCGrpcont where 1 = 1"
       	       + " and uwflag not in ('a','1')"
       	       + " and appflag in ('0','9')"       	          	   
       			   + getWherePart('PrtNo','PrtNo')   
       			   + getWherePart('polapplydate','PolApplyDate')
       			   + getWherePart('cvalidate','CvaliDate')
       			   + getWherePart('grpname','AppntName')
       			   + getWherePart('getUniteCode(AgentCode)','AgentCode')   	
       			   + getWherePart('managecom','ManageCom')
       			   + " and managecom like '" + comcode + "%%'";  //集中权限管理体现
       			   ;
  if(tCardFlag == "1")
	{
	   strSQL = strSQL + " and cardflag = '0'";		
	}
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("该保单不存在，或者该保单已经撤单！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 //tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}
