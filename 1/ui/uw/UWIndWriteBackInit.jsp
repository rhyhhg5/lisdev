<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWIndWirteBackInit.jsp
//程序功能：人工核保通过之后,如果险种核保结论为变更承保将会自动发送核保通知书,及待客会回复
//创建日期：2006-5-29 11:14
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）

function initInpBox()
{ 
  try
  {  
  }
  catch(ex)
  {
    alert("在UWIndWirteBackInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!原因是:"+ex.message);
  }      
}
function initForm()
{
  try
  {
  	initInpBox();
  	initProposalGrid();
  	queryProposalInfo();
  }
  catch(re)
  {
    alert("UWIndWirteBackInit.jsp-->InitForm函数中发生异常:初始化界面错误!原因是:"+re.message);
  }
}

function initProposalGrid()
{                               
   var iArray = new Array();
   
   try
   {
   iArray[0]=new Array();
   iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
   iArray[0][1]="30px";            		//列宽
   iArray[0][2]=30;            			//列最大值
   iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

   iArray[1]=new Array();
   iArray[1][0]="被保险人";         		//列名
   iArray[1][1]="100px";            		//列宽
   iArray[1][2]=100;            			//列最大值
   iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


   iArray[2]=new Array();
   iArray[2][0]="产品序号";         		//列名
   iArray[2][1]="50px";            		//列宽
   iArray[2][2]=100;            			//列最大值
   iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

   iArray[3]=new Array();
   iArray[3][0]="险种代码";         		//列名
   iArray[3][1]="100px";            		//列宽
   iArray[3][2]=200;            			//列最大值
   iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

   iArray[4]=new Array();
   iArray[4][0]="档次";         		//列名
   iArray[4][1]="60px";            		//列宽
   iArray[4][2]=80;            			//列最大值
   iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

   iArray[5]=new Array();
   iArray[5][0]="保额";         		//列名
   iArray[5][1]="60px";            		//列宽
   iArray[5][2]=80;            			//列最大值
   iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[6]=new Array();
   iArray[6][0]="基本保费";         		//列名
   iArray[6][1]="60px";            		//列宽
   iArray[6][2]=80;            			//列最大值
   iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[7]=new Array();
   iArray[7][0]="风险保费";         		//列名
   iArray[7][1]="60px";            		//列宽
   iArray[7][2]=80;            			//列最大值
   iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[8]=new Array();
   iArray[8][0]="核保结论";         		//列名
   iArray[8][1]="140px";            		//列宽
   iArray[8][2]=80;            			//列最大值
   iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[9]=new Array();
   iArray[9][0]="客户意见";         		//列名
   iArray[9][1]="60px";            		//列宽
   iArray[9][2]=80;            			//列最大值
   iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[10]=new Array();
   iArray[10][0]="PolNo";         		//列名
   iArray[10][1]="60px";            		//列宽
   iArray[10][2]=80;            			//列最大值
   iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[11]=new Array();
   iArray[11][0]="ContNo";         		//列名
   iArray[11][1]="60px";            		//列宽
   iArray[11][2]=80;            			//列最大值
   iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
   
   ProposalGrid = new MulLineEnter( "fm" , "ProposalGrid" ); 
   //这些属性必须在loadMulLine前
   ProposalGrid.mulLineCount = 0;   
   ProposalGrid.displayTitle = 1;
   //ProposalGrid.locked = 1;
   ProposalGrid.canSel = 1;
   ProposalGrid.hiddenPlus = 1;
   ProposalGrid.hiddenSubtraction = 1;
   ProposalGrid.loadMulLine(iArray);       
   //ProposalGrid.selBoxEventFuncName = "showInsuredInfo";
   //这些操作必须在loadMulLine后面
   //PolAddGrid.setRowColData(1,1,"asdf");
   }
   catch(ex)
   {
     alert(ex);
   }
}
</script>