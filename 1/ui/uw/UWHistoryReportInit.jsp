<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：RReportQueryInit.jsp
//程序功能：既往契调件查询
//创建日期：2012-10-19
//创建人  ：张成轩
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox(tContNo,tFlag)
{ 
try
  {                                   
    fm.all('ContNo').value = tContNo;
    fm.all('IfReply').value = '';
    if (tFlag == "5")
    {
    	fm.all('BackObj').value = '1';
    }
    else
    {
    	fm.all('BackObj').value = '';
    }
    
    fm.all('ManageCom').value = '';
    fm.all('OManageCom').value = '';
    
    if (tFlag == "5")
    {
    	fm.all('OperatePos').value = '5';
    }
    else if(tFlag == "3"||tFlag == "1")
    {
    	fm.all('OperatePos').value = '1';	    
    }
    else if(tFlag == "4")
    {
    	fm.all('OperatePos').value = '1';
    }
    else
    {
    	fm.all('OperatePos').value = '';
    }
    fm.all('Content').value = '';
    fm.all('ReplyResult').value = '';
  }
  catch(ex)
  {
    alert("在RReportQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}

function initForm(tContNo,tLoadFlag)
{
  try
  {	
  	initContState();
	initLCRReportGrid();
	initQuestGrid();
		
	initHide(tContNo,tLoadFlag);
	fm.all("CustomerName").CodeData = QueryCustomerList(tContNo);
	//initRReportGrid();
	//initRReportResultGrid();
  //easyQueryClick(tContNo);
  easyQueryLCRRClick(tContNo);	
  }
  catch(re)
  {
    alert("UWSubInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 责任信息列表的初始化
function initQuestGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="契约调查项目编码";    	//列名
      iArray[1][1]="180px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="契约调查对象";         			//列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="契约调查项目";         			//列名
      iArray[3][1]="200px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
                           
      iArray[4]=new Array();
      iArray[4][0]="补充说明";         			//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
      
      iArray[5]=new Array();
      iArray[5][0]="契约调查结果";         			//列名
      iArray[5][1]="200px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许  
      
      QuestGrid = new MulLineEnter( "fm" , "QuestGrid" ); 
      //这些属性必须在loadMulLine前                            
      QuestGrid.mulLineCount = 0;
      QuestGrid.displayTitle = 1;
      QuestGrid.canSel = 1;
      QuestGrid.hiddenPlus = 1;
      QuestGrid.hiddenSubtraction = 1;
      
      QuestGrid.loadMulLine(iArray);
      
      //QuestGrid. selBoxEventFuncName = "easyQueryChoClick";
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 责任信息列表的初始化
function initRReportGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="契约调查对象";         			//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="契约调查项目";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
                           
      iArray[3]=new Array();
      iArray[3][0]="补充说明";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	
			iArray[4]=new Array();
      iArray[4][0]="生调结果";         		//列名
      iArray[4][1]="160px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="ICDName";
      iArray[4][9]="生调结论|len<=120";
      iArray[4][18]=300;

      iArray[5]=new Array();
      iArray[5][0]="ICD编码";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][4]="ICDCode";
      iArray[5][9]="ICD编码|len<=20";
      iArray[5][15]="ICDName";
      iArray[5][17]="1";
      iArray[5][18]=300;
			
      
      RReportGrid = new MulLineEnter( "fm" , "RReportGrid" ); 
      //这些属性必须在loadMulLine前                            
      RReportGrid.mulLineCount = 0;
      RReportGrid.hiddenPlus = 1;
      RReportGrid.hiddenSubtraction = 1;
      RReportGrid.displayTitle = 1;
      RReportGrid.canChk = 0;
      RReportGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //HealthGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 责任信息列表的初始化
function initRReportResultGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="生调结果";         		//列名
      iArray[1][1]="260px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="ICDName";
      iArray[1][9]="生调结论|len<=120";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="ICD编码";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ICDCode";
      iArray[2][9]="ICD编码|len<=20";
      iArray[2][15]="ICDName";
      iArray[2][17]="1";
      iArray[2][18]=300;
     

      RReportResultGrid = new MulLineEnter( "fm" , "RReportResultGrid" ); 
      //这些属性必须在loadMulLine前                            
      RReportResultGrid.mulLineCount = 0;
      RReportResultGrid.displayTitle = 1;
      RReportResultGrid.canChk = 0;
      RReportResultGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //DisDesbGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initHide(tContNo,tLoadFlag)
{
	fm.all('ContNo').value=tContNo;
	fm.all('LoadFlag').value=tLoadFlag;		
}

function initCodeData(tContNo)
{
	if (tFlag == "5")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员";
		fm.all('OperatePos').CodeData = "0|^5|复核";
	}
	if (tFlag == "1")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|机构^3|保户";
		fm.all('OperatePos').CodeData = "0|^1|核保";
	}
	if (tFlag == "2")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员";
		fm.all('OperatePos').CodeData = "0|^5|复核^1|核保";
	}
	if (tFlag == "3")
	{
		fm.all('BackObj').CodeData = "0|^2|机构^3|保户";
		fm.all('OperatePos').CodeData = "0|^1|核保";
	}
	if (tFlag == "4")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|机构^3|保户";
		fm.all('OperatePos').CodeData = "0|^1|核保";
	}	
}

// 
function initLCRReportGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[1]=new Array();
      iArray[1][0]="契调编号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="合同号";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=170;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="被保险人";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="下发时间";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="印刷号";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="申请时间";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="生效时间";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
 
      iArray[8]=new Array();
      iArray[8][0]="投保人";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[9]=new Array();
      iArray[9][0]="业务员代码";         		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="管理机构";         		//列名
      iArray[10][1]="80px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="被保险人编码";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="投保人编码";         		//列名
      iArray[12][1]="80px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="PEResult";         		//列名
      iArray[13][1]="80px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="PEState";         		//列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[15]=new Array();
      iArray[15][0]="回销人员";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许      

      iArray[16]=new Array();
      iArray[16][0]="回销日期";         		//列名
      iArray[16][1]="80px";            		//列宽
      iArray[16][2]=100;            			//列最大值
      iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      LCRReportGrid = new MulLineEnter( "fm" , "LCRReportGrid" ); 
      //这些属性必须在loadMulLine前
      LCRReportGrid.mulLineCount = 1;   
      LCRReportGrid.displayTitle = 1;
      LCRReportGrid.locked = 1;
      LCRReportGrid.canSel = 1;
      LCRReportGrid.canChk = 0;
      LCRReportGrid.hiddenSubtraction = 1;
      LCRReportGrid.hiddenPlus = 1;
      
      LCRReportGrid.loadMulLine(iArray);  
      LCRReportGrid.selBoxEventFuncName = "easyQueryClick";
      
      

      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>


