<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LCPENoticeSave.jsp
//程序功能：
//创建日期：2005-06-28 19:20:10
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LCPENoticeSchema tLCPENoticeSchema   = new LCPENoticeSchema();
  OLCPENoticeUI tOLCPENoticeUI   = new OLCPENoticeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tContNo = request.getParameter("ContNo");
	String tInsureNo = request.getParameter("InsureNo");
	String tHospital = request.getParameter("Hospital");
	String tPrtNo =  request.getParameter("PrtNo");

	String tEDate = request.getParameter("EDate");
	String tNote = request.getParameter("Note");
    
    
   tLCPENoticeSchema.setBookingDate(request.getParameter("BookingDate"));   
   tLCPENoticeSchema.setBookingTime(request.getParameter("BookingTime"));
   if(request.getParameter("PEState").equals("")||request.getParameter("PEState")==null)   
   {tLCPENoticeSchema.setPEState("01");}
  else{tLCPENoticeSchema.setPEState(request.getParameter("PEState"));}           
   tLCPENoticeSchema.setProposalContNo(request.getParameter("ContNo"));           
   tLCPENoticeSchema.setPrtSeq(request.getParameter("PrtSeq"));           
    
    

	
	System.out.println("ContNo:"+tContNo);
	System.out.println("Hospital:"+tHospital);
	System.out.println("note:"+tNote);

	System.out.println("insureno:"+tInsureNo);
	System.out.println("EDATE:"+tEDate);
	
	boolean flag = true;



	if ( tHospital.equals("") )
	{
		Content = "体检资料信息录入不完整!";
		FlagStr = "Fail";
		flag = false;
		System.out.println("111");
		
	}
	else
	{
		System.out.println("222");
	    //体检资料一
		    tLCPENoticeSchema.setContNo(tContNo);	    		
	    	tLCPENoticeSchema.setPEAddress(tHospital);
	    	tLCPENoticeSchema.setPEDate(tEDate);
	    
	    	tLCPENoticeSchema.setRemark(tNote);
	    	tLCPENoticeSchema.setCustomerNo(tInsureNo); 
    } 
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLCPENoticeSchema);
  	tVData.add(tG);
    tOLCPENoticeUI.submitData(tVData,"UPDATE||MAIN");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLCPENoticeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
