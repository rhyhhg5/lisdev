//程序名称：UWHistoryNotifyInput.js

var showInfo;
var mDebug="0";
var flag;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;

function easyQueryClickSingle()
{
	// 书写SQL语句
	var strsql = "";
	var tContNo = "";

	tContNo = fm.ContNo.value;
  tInsuredNo = fm.InsureNo.value;

  if(tContNo != "" )
  { 
		strSQL = "select b.name ,a.ImpartVer , a.ImpartCode,a.ImpartDetailContent,a.DiseaseContent,a.StartDate,a.EndDate,Prover,a.CurrCondition,a.IsProved from LCCustomerImpartDetail a ,lcinsured b  where a.CustomerNo=b.insuredno and a.contno=b.contno  and CustomerNoType='I' and a.contno='"+tContNo+"'" ;	
  	// 查询SQL，返回结果字符串
  	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);  
  	
  	// 判断是否查询成功
  	if (!turnPage.strQueryResult) {
  	  return "";
  	}
  	
  	// 查询成功则拆分字符串，返回二维数组
  	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  	// 设置初始化过的MULTILINE对象
  	turnPage.pageDisplayGrid = ImpartDetailGrid;    
  	// 保存SQL语句
  	turnPage.strQuerySql = strSQL; 
  	// 设置查询起始位置
  	turnPage.pageIndex = 0;  
  	// 在查询结果数组中取出符合页面显示大小设置的数组
  	
  	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 200);
  	
  	// 调用MULTILINE对象显示查询结果
  	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	}
  return true;
}
function fillDisDesbGrid()
{
	// 书写SQL语句
	var strsql = "";
	var tContNo = "";

	tContNo = fm.ContNo.value;
  tInsuredNo = fm.InsureNo.value;

  // turnPage = new turnPageClass();
  if(tContNo != "" )
  { 
		strSQL = "select customerno ,name , disdesb,disresult,icdcode,SerialNo,(select ICDName from LDDisease where ICDCode=LCDiseaseResult.icdcode),Remark from LCDiseaseResult where contno='"+tContNo+"' order by SerialNo asc" ;	
  	// 查询SQL，返回结果字符串
  	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);  
  	
  	// 判断是否查询成功
  	if (!turnPage.strQueryResult) {
  	  return "";
  	}
  	
  	// 查询成功则拆分字符串，返回二维数组
  	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  	// 设置初始化过的MULTILINE对象
  	turnPage.pageDisplayGrid = DisDesbGrid;    
  	// 保存SQL语句
  	turnPage.strQuerySql = strSQL; 
  	// 设置查询起始位置
  	turnPage.pageIndex = 0;  
  	// 在查询结果数组中取出符合页面显示大小设置的数组
  	
  	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 200);
  	
  	// 调用MULTILINE对象显示查询结果
  	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	}
  return true;
	}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strsql = "";
	var tContNo = "";
	var tEdorNo = "";
	tContNo = fm.ContNo.value;
	tInsuredNo = fm.InsureNo.value;
  if(tContNo != "" && tInsuredNo != "")
  {
	strsql = "select peitemcode,peitemname,freepe from LCPENoticeItem where 1=1"
				 + " and ContNo = '"+ tContNo + "'"
				 + " and PrtSeq in (select distinct prtseq from lcpenotice where contno = '"+ tContNo + "'"
				 + " and customerno = '"+ tInsuredNo + "')";
				 				 
	
  // 查询SQL，返回结果字符串
  turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);  
  
  // 判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }
  
  // 查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  // 设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = HealthGrid;    
          
  // 保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  // 设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  // 在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  // 调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
  return true;
}


// 无体检资料查询按钮
function easyQueryClickInit()
{
	fm.action= "./UWManuHealthQuery.jsp";
	fm.submit();
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				HealthGrid.setRowColData( i, j+1, arrResult[i][j] );
				
			} // end of for
		} // end of for
		// alert("result:"+arrResult);
	} // end of if
}

function initInsureNo(tPrtNo)
{
	var i,j,m,n;
	var returnstr;
	
	var strSql = "select InsuredNo,name from lcinsured where 1=1 "
	       + " and Prtno = '" +tPrtNo + "' union all "
	       + "select InsuredNo,name from lbinsured where 1=1 "
	       + " and Prtno = '" +tPrtNo + "'";
					;
	// 查询SQL，返回结果字符串
  
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  
  // 判断是否查询成功
  if (turnPage.strQueryResult == "")
  {
    alert("查询失败！");
    return "";
  }
  
  // 查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;
  // alert("N:"+n);
  if (n > 0)
  {
  	for( i = 0;i < n; i++)
  	{
  		m = turnPage.arrDataCacheSet[i].length;
  		// alert("M:"+m);
  		if (m > 0)
  		{
  			for( j = 0; j< m; j++)
  			{
  				if (i == 0 && j == 0)
  				{
  					returnstr = "0|^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i == 0 && j > 0)
  				{
  					returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j == 0)
  				{
  					returnstr = returnstr+"^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j > 0)
  				{
  					returnstr = returnstr+"|"+turnPage.arrDataCacheSet[i][j];
  				}
  				
  			}
  		}
  		else
  		{
  			alert("查询失败!!");
  			return "";
  		}
  	}
}
else
{
	alert("查询失败!");
	return "";
}
  fm.InsureNo.CodeData = returnstr;
  return returnstr;
}

function afterCodeSelect(tCodeName,tObject)
{
	if(tCodeName == "InsureNo")
	{
		var InsuredNo = tObject.value;
		var strSql = "select a.DisDesb,a.DisResult,a.ICDCode,a.SerialNo,b.icdname,a.remark from LCDiseaseResult a,LDUWAddFee b where "
								+" a.ProposalContNo='"+fm.ContNo.value+"' and a.icdcode=b.icdcode and  a.CustomerNo='"+InsuredNo
								+"' and  a.DiseaseSource='1'";
		turnPage.queryModal(strSql,DisDesbGrid);
	}
}

var querySpanNum;
function querySpanNo(parm1,parm2)
 {
	querySpanNum = fm.all( parm1 ).all('DisDesbGridNo').value;
	showInfo=window.open("./LDUWAddFeeQuery.html");
}