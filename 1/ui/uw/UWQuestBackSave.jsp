<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.cbcheck.*"%>

<%
System.out.println("UWQuestBackSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

CErrors tError = null;
String tAction = "";
String tOperate = "";

GlobalInput tG = (GlobalInput)session.getValue("GI");

String tMissionId = null;
String tSubMissionId = null;
String tContNo = null;

tMissionId = request.getParameter("MissionID");
tSubMissionId = request.getParameter("SubMissionId");
tContNo = request.getParameter("ContNo");

TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("MissionID", tMissionId);
tTransferData.setNameAndValue("SubMissionId", tSubMissionId);
tTransferData.setNameAndValue("ContNo", tContNo);

VData tVData = new VData();
tVData.add(tG);
tVData.add(tTransferData);

UWQuestBackUI tUWQuestBackUI = new UWQuestBackUI();

if (!tUWQuestBackUI.submitData(tVData, "submit"))
{
    Content = " 回退失败! 原因是: " + tUWQuestBackUI.mErrors.getLastError();
    FlagStr = "Fail";
}
else
{
    Content = " 回退成功！";
    FlagStr = "Succ";
}

System.out.println("UWQuestBackSave.jsp End ...");
%>

<html>
<script language="javascript">
    parent.fraInterface.afterQuestBackSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
