<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：EdorUWManuHealth.jsp
//程序功能：保全体检资料录入
//创建日期：2005-09-07
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<!--<SCRIPT src="./EdorUWManuHealth.js"></SCRIPT>-->
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="./EdorUWManuHealthInit.jsp"%>
	<title> 保全体检录入 </title>
</head>
<body onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./UWManuHealthChk.jsp">
    <div id="divHealthInfo" style="display: ''">
	    <table>
	    	<tr>
		      <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divHealthInfo);"></td>
		    	<td class=titleImg>体检基本信息</td>
		    </tr>
	    </table>
	    <table class= common>
	    	<TR class= common>
	    		<TD class= title>保全受理号</TD>
					<TD class= input> 
						<Input class="readonly" name=EdorNo readonly > 
					</TD>
					<TD class= title>打印状态</TD>
					<TD class= input> 
						<Input class="readonly" name=PrintFlag readonly > 
					</TD>
					<TD class= title>体检人</TD>
					<TD class= input> 
						<Input class=codeno name=InsureNo ondblClick="showCodeListEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" onFocus= "easyQueryClickSingle();"><input class=codename name=InsureNoName readonly=true >
					</TD>
	    	</TR>
				<TR  class= common>
				  <TD class= title>体检日期</TD>
				  <TD class= input>  
				     <Input class="coolDatePicker" dateFormat="short" name=EDate verify="体检日期|date" >
				  </TD>
				  <TD class= title>体检医院</TD>
				  <TD class= input> 
				  	<Input class= 'codeNo' name=Hospital verify="体检医院|notnull" ondblclick="return showCodeList('lhhospitnamehtuw',[this,HospitName],[0,1],null,fm.HospitName.value,'HospitName',1,350);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitnameht',[this,HospitName],[0,1],null,fm.HospitName.value,'HospitName',1,350)	; "><Input  class=codename  name=HospitName >
				  </TD>
				  <TD  class= title>体检状态</TD>
				  <td class=common>
				  	<Input class=codeno name=PEState   ondblclick="return showCodeList('pecode',[this,PEStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('pecode',[this,PEStateName],[0,1],null,null,null,1);"><input class=codename name=PEStateName  elementtype=nacessary> 
				  </TD> 
				</TR>
			</table>
		</Div>
		<div id="divHealthItem" style="display: ''">
		  <table>
	    	<tr>
	    	  <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHealthItem);"></td>
	    		<td class= titleImg>体检项目录入</td>                           
	    	</tr>	
			</table>
			<table class=common >
				<TR class=common>
					<TD  class= title>套餐</TD>
					<TD  class= input colspan="7">
						<Input class=code name=TestGrpCode   ondblclick="return showCodeList('testgrpname',[this,GrpTestCode],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('testgrpname',[this,GrpTestCode],[0,1]);">  
					</TD>
			</table>
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1 >
						<span id="spanHealthGrid">
						</span> 
					</td>
				</tr>
			</table>
	    <Div id= "divPage123" align=center style= "display: 'none' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	 		</Div>
    </div>
		<table class=common>
			<TR class= common> 
				<TD class= common colspan="7"> 体检原因 </TD>
			</TR>
			<TR class= common>
				<TD  class= common>
					<textarea name="Note" cols="100%" rows="3" class="common" >
					</textarea>
				</TD>
			</TR>
		</table>
    <div id="divAddPeItem" style="display: ''">
    	<INPUT type= "button" name= "sure" value="确  认" class=CssButton onclick="submitForm()">	
  	</div>		
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>