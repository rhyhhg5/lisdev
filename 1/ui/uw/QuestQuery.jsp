<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：QuestQuery.jsp
//程序功能：问题件查询
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head >
<title>问题件查询 </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="QuestQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="QuestQueryInit.jsp"%>
  
</head>
<body  onload="initForm('<%=tContNo%>','<%=tFlag%>','<%=tMissionID%>','<%=tSubMissionID%>');" >
  <form method=post name=fm target="fraSubmit" action="./QuestQueryChk.jsp">
    <table class=common >
    	<TR  class= common>
        <!--  <TD  class= title>
            合同投保单号码
          </TD>
          <TD  class= input>-->
            <Input class=common name=ContNo  type="hidden">
          <TD  class= title>
            返回对象
          </TD>
          <TD  class= input>
            <Input class=codeNo name=BackObj verify="返回对象|code:BackObj" ondblclick= "showCodeListEx('BackObj',[this,BackObjName],[0,1]);" onkeyup="showCodeListKeyEx('BackObj',[this,BackObjName],[0,1]);" ><input class=codename name=BackObjName readonly=true >
          </TD>
          <TD  class= title>
            操作位置
          </TD>
          <TD  class= input>
            <Input class=codeNo name=OperatePos verify="操作位置|code:OperatePos" ondblclick= "showCodeListEx('OperatePos',[this,'OperatePosName'],[0,1]);" onkeyup="showCodeListKeyEx('OperatePos',[this,'OperatePosName'],[0,1]);" ><input class=codename name=OperatePosName readonly=true >
          </TD>
        </TR>

    	<TR  class= common>
          <TD  class= title>  管理机构  </TD>
          <TD  class= input>  <Input class=codeNo name=ManageCom verify="管理机构|code:station" ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >  </TD>
          <TD  class= title>   问题件选择  </TD>
          <TD  class= input>  <Input class=codeNo name=Quest verify="问题件选择|code:Quest" ondblclick= "showCodeListEx('Quest',[this,'QuestName'],[0,1],null,null,null,null,500);" onkeyup="showCodeListKeyEx('Quest',[this,'QuestName'],[0,1],null,null,null,null,500);" ><input class=codename name=QuestName readonly=true >  </TD>
          <TD  class= title>  是否回复 </TD>
          <TD  class= input> <Input class=codeNo name=IfReply value="N" verify="是否回复|code:YesNo" ondblclick="return showCodeList('YesNo',[this,IfReplyName],[0,1]);" onkeyup="return showCodeListKey('YesNo',[this,IfReplyName],[0,1]);"><input class=codename name=IfReplyName readonly=true > </TD>
        </TR>

    	<TR  class= common>       
          <TD  class= title >
            问题件所在管理机构
          </TD>
          <TD  class= input >
            <Input class=codeNo name=OManageCom verify="问题件所在管理机构|code:station" ondblclick="return showCodeList('station',[this,OManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,OManageComName],[0,1]);"><input class=codename name=OManageComName readonly=true >
          </TD>
          <TD  class= input>
          <Input  type= "hidden" class= Common name = SerialNo >
          </TD>
          <TR>
          <TD  class= title>
            <input type="button" class=cssButton name= "Query" class= Common value="查  询" onClick= "query()">
          </TD>
          </TD>
      </TR>
    </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);">
    		</td>
    		<td class= titleImg>
    			 问题件列表：
    		</td>
    	</tr>
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanQuestGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>
      
  <table width="80%" height="25%" class= common>
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 问题件内容 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= title><textarea name="Content" cols="135" rows="8" class="common" readonly></textarea></TD>
    </TR>
  </table>
  <div id= "divUWreport" style= "display: ''">
  <table width="80%" height="25%" class= common>
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 问题件回复 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= title><textarea name="ReplyResult" cols="135" rows="8" class="common" readonly></textarea></TD>
    </TR>
  </table>
  </div>
  
  <p> 
    <!--读取信息-->
    <input type= "hidden" name= "Flag" value="">
    <input type= "hidden" name= "ContNoHide" value= "">
    <input type= "hidden" name= "QuesFlag" value="">
    <input type= "hidden" name= "Type" value="">
    <input type= "hidden" name= "HideOperatePos" value="">
    <input type= "hidden" name= "HideSerialNo" value="">
    <input type= "hidden" name= "HideQuest" value="">
    <input type= "hidden" name= "MissionID" value="">
    <input type= "hidden" name= "SubMissionID" value="">
  </p>
</form>
<div id = "divButton">
	<input class= cssButton type= "button" name= "sure" value="回复问题" class= Common onClick="reply()">
  <input class= cssButton type= "button" name= "sure" value="回复完毕" class= Common onClick="replySave()">
</div>
<div id = "divModiButton">
	<input type= "button" name= "sure" value="修改问题件打印标记" class= cssButton onClick="IfPrint()">
	<INPUT VALUE=" 返  回 " class= cssButton TYPE=button onclick="parent.close();">
</div>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>