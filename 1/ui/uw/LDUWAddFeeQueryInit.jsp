<%
//程序名称：LDDiseaseQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-19 14:47:24
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ICDCode').value = "";
    fm.all('ICDName').value = "";
//    fm.all('ICDSubjectClass').value = "";
  }
  catch(ex) {
    alert("在LDUWAddFeeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDDiseaseGrid();  
  }
  catch(re) {
    alert("LDUWAddFeeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDDiseaseGrid;
function initLDDiseaseGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="疾病分类代码";   
		iArray[1][1]="35px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
		
		iArray[2]=new Array(); 
		iArray[2][0]="疾病分类名称";   
		iArray[2][1]="120px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="疾病严重程度";   
		iArray[3][1]="40px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="风险等级";   
		iArray[4][1]="60px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
//		iArray[5]=new Array(); 
//		iArray[5][0]="疾病分类代码";   
//		iArray[5][1]="30px";   
//		iArray[5][2]=20;        
//		iArray[5][3]=0;
    
    LDDiseaseGrid = new MulLineEnter( "fm" , "LDDiseaseGrid" ); 
    //这些属性必须在loadMulLine前

    LDDiseaseGrid.mulLineCount = 0;   
    LDDiseaseGrid.displayTitle = 1;
    LDDiseaseGrid.hiddenPlus = 1;
    LDDiseaseGrid.hiddenSubtraction = 1;
    LDDiseaseGrid.canSel = 1;
    LDDiseaseGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    LDDiseaseGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDiseaseGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
