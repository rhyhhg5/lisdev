<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： FillIssue.jsp
//程序功能：
//创建日期：2007-10-30
//创建人  ：张建宝
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%		
    System.out.println("in to FillIssue.jsp");	
    boolean operFlag = true;
		String FlagStr = "";
		String Content = "";
		String[] title = {"印刷号", "申请日期", "生效日期", "投保人", "总保费", "业务员代码", "业务员名称", 
			"营销机构代码", "营销机构名称", "下发日期", "回销日期", "是否已打", "打印次数", "管理机构"};
		XmlExport xmlExport = null;   
		GlobalInput tG = (GlobalInput)session.getValue("GI");

		String sql = request.getParameter("querySql");
		
    TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("sql", sql);	//查询的 SQL 
		tTransferData.setNameAndValue("vtsName", "FillIssue.vts");//模板名
		tTransferData.setNameAndValue("printerName", "printer");//打印机名
		tTransferData.setNameAndValue("title", title);//表头
		tTransferData.setNameAndValue("tableName", "BB");//表名
		
		VData tVData = new VData();
    tVData.addElement(tG);
		tVData.addElement(tTransferData);
          
    PrintList printList = new PrintList(); 
    if(!printList.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = printList.mErrors.getError(0).errorMessage;
    }
    else
    {    
			VData result = printList.getResult();			
	  	xmlExport=(XmlExport)result.getObjectByObjectName("XmlExport",0);

	  	if(xmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
		}
		//System.out.println(operFlag);
		if (operFlag==true)
		{
  		ExeSQL exeSQL = new ExeSQL();
      //获取临时文件名
      String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
      String strFilePath = exeSQL.getOneValue(strSql);
      String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
      //获取存放临时文件的路径
      //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
      //String strRealPath = exeSQL.getOneValue(strSql);
      String strRealPath = application.getRealPath("/").replace('\\','/');
      String strVFPathName = strRealPath + "//" +strVFFileName;
      
      CombineVts tcombineVts = null;	
      
      String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
    	tcombineVts = new CombineVts(xmlExport.getInputStream(),strTemplatePath);
    
    	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    	tcombineVts.output(dataStream);
        	
    	//把dataStream存储到磁盘文件
    	System.out.println("存储文件到"+strVFPathName);
    	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	    System.out.println("==> Write VTS file to disk ");
            
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
		}
		else
		{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%= Content %>");
	top.close();
</script>
</html>
<%
  	}
%>