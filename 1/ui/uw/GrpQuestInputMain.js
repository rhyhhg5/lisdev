//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			window.open("./AppGrpPolQuery.jsp");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
var queryBug = 1;
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	if (contNo==null||contNo=='')
		contNo = "00000000000000000000";
		
	//alert(contNo);
	// 书写SQL语句
	var strSQL = "";
	var strSQL = "select GrpProposalNo,PrtNo,RiskCode,GrpName from LCGrpPol where " + queryBug + "=" + queryBug
    				 + " and AppFlag='0' "                 
    				 + " and Operator ='" + operator + "'"
    				 + " and ManageCom like '" + manageCom + "%%'"    				   				 
 				   	 + " and approveFlag = '1'"
    				 + " and uwflag not in ('1','4','9')"
    				 + " and riskcode in(select riskcode from lmriskapp where subriskflag='M')";

//				 + getWherePart( 'RiskVersion' );
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		//HZM 到此修改
		GrpPolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpPolGrid.loadMulLine(GrpPolGrid.arraySave);		
		//HZM 到此修改
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//GrpPolGrid.delBlankLine();
	} // end of if
}

/*********************************************************************
 *  团体单的问题件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
	  var i = 0;
	  var checkFlag = 0;
	  var cGrpProposalNo = "";
	  var cflag = "2";
	  
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1); 
	        //alert(cProposalNo);
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	    return false;
	  }
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cGrpProposalNo+"&Flag="+cflag,"window1");
	
}


/*********************************************************************
 *  复核修改完成后确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ModifyMakeSure(){
	}//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			window.open("./AppGrpPolQuery.jsp");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
var queryBug = 1;
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	if (contNo==null||contNo=='')
		contNo = "00000000000000000000";
		
	//alert(contNo);
	// 书写SQL语句
	var strSQL = "";
	var strSQL = "select GrpProposalNo,PrtNo,RiskCode,GrpName from LCGrpPol where " + queryBug + "=" + queryBug
    				 + " and AppFlag='0' "                 
    				 + " and Operator ='" + operator + "'"
    				 + " and ManageCom like '" + manageCom + "%%'"    				   				 
 				   	 + " and approveFlag = '1'"
    				 + " and uwflag not in ('1','4','9')"
    				 + " and riskcode in(select riskcode from lmriskapp where subriskflag='M')";

//				 + getWherePart( 'RiskVersion' );
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		//HZM 到此修改
		GrpPolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpPolGrid.loadMulLine(GrpPolGrid.arraySave);		
		//HZM 到此修改
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//GrpPolGrid.delBlankLine();
	} // end of if
}

/*********************************************************************
 *  团体单的问题件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
	  var i = 0;
	  var checkFlag = 0;
	  var cGrpProposalNo = "";
	  var cflag = "2";
	  
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1); 
	        //alert(cProposalNo);
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	    return false;
	  }
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cGrpProposalNo+"&Flag="+cflag,"window1");
	
}


/*********************************************************************
 *  复核修改完成后确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ModifyMakeSure(){
	}