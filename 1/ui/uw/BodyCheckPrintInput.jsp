<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var RePrintFlag = "<%=request.getParameter("RePrintFlag")%>";
	var saleChnl = "";//销售渠道   zhangjianbao   2007-11-16
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="BodyCheckPrintInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BodyCheckPrintInit.jsp"%>
  <title>打印承保体检通知书 </title>
</head>
<body  onload="initForm();" >
  <form action="./BodyCheckPrintQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>  投保单号码   </TD>
          <TD  class= input> --> <Input class= common name=ContNo  type="hidden">
          <TD  class= title>  印刷号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码"> </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
         </TR> 
         <TR  class= common>
         	<TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
          <TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CvaliDate verify="生效日期|date"> </TD>
          <TD  class= title> 下发日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=MakeDate verify="下发日期|date">   </TD>
         </TR> 
         <TR>
         	<TD  class= title> 申请日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=PolApplyDate verify="申请日期|date">   </TD>
          <TD  class= title> 是否回销</TD>
          <TD  class= input><Input class="codeNo" name=SelType ondblclick="return showCodeList('takebackstate',[this,SelTypeName],[0,1]);" onkeyup="return showCodeListKey('takebackstate',[this,SelTypeName],[0,1]);"><input class=codename name=SelTypeName readonly=true ><input class=readonly name=SelTypeName2 value="" readonly=true >  </TD>
          <TD  class= title> 是否已打</TD>
          <TD  class= input><Input class="codeNo" name=StateFlag ondblclick="return showCodeList('printstate',[this,StateFlagName],[0,1]);" onkeyup="return showCodeListKey('printstate',[this,StateFlagName],[0,1]);"><input class=codename name=StateFlagName readonly=true ><input class=readonly name=StateFlagName2 value="未打印" readonly=true >  </TD>
         </TR>
       <tr>
         	<TD  class= title> 销售渠道 </TD>
          <TD  class= input><Input class="codeNo" name=saleChannel ondblclick="return showCodeList('statuskind',[this,saleChannelName],[0,1]);" onkeyup="return showCodeListKey('statuskind',[this,saleChannelName],[0,1]);"><input class=codename name=saleChannelName readonly=true ></TD>
          <TD  class= title> 营销机构 </TD>
          <TD  class= input><Input class="codeNo" name=branchAttr readonly ondblclick="beforeCodeSelect();return showCodeList('branchattrandagentgroup',[this,branchAttrName,agentGroup],[0,1,2],null,saleChnl,'saleChannel',1,200);"><input class=codename name=branchAttrName readonly=true ></TD>
      </tr>
    </table>
    <input type=hidden name="agentGroup">
    <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();">
    <input value="清单下载" class= CssButton type=button onclick="downloadList();">
    　　<font color="#ff0000">*</font> 清单中包含体检件下发人和体检原因
  </form>
  <form action="./BodyCheckPrintSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont1);">
    		</td>
    		<td class= titleImg>
    			 承保体检信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCCont1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<INPUT VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  	<div id= "printnotice" style= "display: ''" align = left>
  		<table>
  			<tr>
  				<td>
  	         <p>
               <INPUT VALUE="打印承保体检通知书(vts)" class= CssButton TYPE=hidden onclick="printPol();">
               <INPUT VALUE="查看体检通知书" class= CssButton TYPE=hidden onclick="printvts();">
               <INPUT VALUE="打印承保体检通知书" class= CssButton TYPE=hidden onclick="printPolpdf();">
               <INPUT VALUE="打印承保体检通知书pdf" class= CssButton TYPE=button onclick="printPolpdfNew();">
  	         </p>
  	      </td>
  	     </tr>
  	  </table>  	
    </div>
    <div id= "reprintnotice" style= "display: 'none'" align = left>
  		<table>
  			<tr>
  				<td>
  	         <p>
               <INPUT VALUE="发送重打" class= CssButton TYPE=button onclick="printPol();">
               <INPUT VALUE="查看体检通知书" class= CssButton TYPE=hidden onclick="printvts();">
               <INPUT VALUE="打印承保体检通知书" class= CssButton TYPE=hidden onclick="printPolpdf();">
               <INPUT VALUE="打印承保体检通知书pdf" class= CssButton TYPE=button onclick="printPolpdfNew();">
  	         </p>
  	      </td>
  	     </tr>
  	  </table>
    </div>
    <br><br>
   <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
        <tr  class= common>
          <td  class= title>印刷号</td>
          <td  class= input>  
            <Input class= common name=PrtNo1 > 
          </td>
          <td  class= title>体检通知书号</td>
          <td  class= input>  
            <Input class= common name=PrtSeq1 > 
          </td>
          <td  class= title>保全受理号</td>
          <td  class= input>  
            <Input class= common name=EdorNo >
          </td>
        </tr>
        <tr  class= common>
          <td  class= title>被保人客户号</td>
          <td  class= input>  
            <Input class= common name=InsuredNo1 >  
          </td>   
          <td  class= title>管理机构</td>
          <td  class= input>  
            <Input class="codeNo" name=ManageCom1 ondblclick="return showCodeList('station',[this,ManageComName1], [0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName1], [0,1]);"><Input class="codeName" name=ManageComName1 readonly >
          </td> 
          <td  class= title>下发日期</td>
          <td  class= input>  
            <Input class= common name=SendDate> 
          </td> 
         <!--
          <td  class= title>打印状态</td>
          <td  class= input>  
            <Input class="codeNo" name=HealthPrintFlag CodeData="0|^0|未打印^1|已打印" value="0" ondblclick="return showCodeListEx('HealthPrintFlag',[this,HealthPrintFlagName], [0,1]);" onkeyup="return showCodeListEx('HealthPrintFlag',[this,HealthPrintFlagName], [0,1]);"><Input class="codeName" name=HealthPrintFlagName readonly value="未打印">   
          </td>
          -->
        </tr>     
    </table>
    <input value="查  询" class= cssButton type=button onclick="queryHealthPrint();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHealth);">
    		</td>
    		<td class= titleImg>
    			 体检通知书信息
    		</td>
    	</tr>
    </table>
  	<div id="divHealth" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanHealthGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<div id= "divPage" align=center style= "display: 'none' ">
  			<input value="首  页" class=cssButton type=button onclick="turnPage2.firstPage();"> 
        <input value="上一页" class=cssButton type=button onclick="turnPage2.previousPage();"> 					
        <input value="下一页" class=cssButton type=button onclick="turnPage2.nextPage();"> 
        <input value="尾  页" class=cssButton type=button onclick="turnPage2.lastPage();"> 
      </div>					
  	</div>
  	<p>
      <input value="打印保全体检通知书" class=cssButton type=button onclick="printNotice();"> 
  	</p>
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="ContNo" name="ContNo">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  	<input type=hidden id="MissionID" name="MissionID">
  	<input type=hidden id="SubMissionID" name="SubMissionID">
  	<input type=hidden id="PrtNo" name="PrtNo">
  	<input type="hidden" name="querySql">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html> 
