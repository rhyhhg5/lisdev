<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：StopInsuredChk.jsp
//程序功能：暂停被保人
//创建日期：2005-01-19 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  if(tG == null) 
  {
		System.out.println("session has expired");
		return;
  }
     
 	// 投保单列表

  String tPrtNo = request.getParameter("PrtNoHidden");
  String tContNo = request.getParameter("ContNoHidden");
	System.out.println("ContNo="+tContNo);
	System.out.println("PrtNo="+tPrtNo);
	System.out.println("MissionID="+request.getParameter("MissionID"));
	System.out.println("SubMissionID="+request.getParameter("SubMissionID"));
	
  TransferData mTransferData = new TransferData();
  mTransferData.setNameAndValue("MissionID",request.getParameter("MissionID"));  
  mTransferData.setNameAndValue("SubMissionID",request.getParameter("SubMissionID"));
  mTransferData.setNameAndValue("PrtNo",tPrtNo);
  mTransferData.setNameAndValue("ContNo",tContNo);  
	
try{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( mTransferData );
		tVData.add( tG );
		
		// 数据传输
		TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
		if (tTbWorkFlowUI.submitData(tVData,"0000001147") == false)
		{
			int n = tTbWorkFlowUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			{
			  System.out.println("Error: "+tTbWorkFlowUI.mErrors.getError(i).errorMessage);
			  Content = " 恢复暂停被保人失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
			}
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tTbWorkFlowUI.mErrors;
		    System.out.println("tError.getErrorCount:"+tError.getErrorCount());
		    if (!tError.needDealError())
		    {                          
		    	Content = " 恢复暂停被保人成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 恢复暂停被保人失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
					}
		    	FlagStr = "Fail";
		    }
		}
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
  
%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
