<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChoiceAutoQuestSave.jsp
//程序功能：
//创建日期：2005-05-24 10:05:39
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LCGrpIssuePolSet tLCGrpIssuePolSet   = new LCGrpIssuePolSet();
  ChoiceAutoQuestUI tChoiceAutoQuestUI   = new ChoiceAutoQuestUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tLoadFlag= request.getParameter("LoadFlag");
  System.out.println("LoadFlag"+tLoadFlag);
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  if(transact.equals("INSERT||QUEST"))
  {
  	String[] tIssueCode    = request.getParameterValues("GrpQuestGrid1");
  	String[] tIssueName    = request.getParameterValues("GrpQuestGrid2");
  	String[] tIssueRuleCode   = request.getParameterValues("GrpQuestGrid5");
  	String[] tIssueRuleCode1   = request.getParameterValues("GrpQuestGrid3");
  	String[] tBackObjType  = request.getParameterValues("GrpQuestGrid7");
		String[] tBackObj      = request.getParameterValues("GrpQuestGrid6");
		String[] tIssueCont    = request.getParameterValues("GrpQuestGrid8");
		String[] tNeedPrint =request.getParameterValues("GrpQuestGrid9");
		String[] tGrpContNo    = request.getParameterValues("GrpQuestGrid10");
		String[] tSerialNo     = request.getParameterValues("GrpQuestGrid11");
		String[] tprtseq     = request.getParameterValues("GrpQuestGrid13");
		String[] tstate     = request.getParameterValues("GrpQuestGrid12");
		String[] tOperator     = request.getParameterValues("GrpQuestGrid14");
    String mGrpContNo = request.getParameter("GrpContNo");

  	for(int i=0;i<tIssueCode.length;i++)
  	{
  		if(tIssueCode[i].equals(""))
  			break;
			LCGrpIssuePolSchema tLCGrpIssuePolSchema = new LCGrpIssuePolSchema();
			tLCGrpIssuePolSchema.setGrpContNo(mGrpContNo);
			tLCGrpIssuePolSchema.setProposalGrpContNo(mGrpContNo);
			tLCGrpIssuePolSchema.setErrField(tIssueCode[i]);
			if(tIssueRuleCode[i].equals(""))
			tLCGrpIssuePolSchema.setQuestionObj(tIssueRuleCode1[i]);
			else
			tLCGrpIssuePolSchema.setQuestionObj(tIssueRuleCode[i]);	
			tLCGrpIssuePolSchema.setIssueType("2");
			tLCGrpIssuePolSchema.setIssueCont(tIssueCont[i]);
			tLCGrpIssuePolSchema.setSerialNo(tSerialNo[i]);
			tLCGrpIssuePolSchema.setNeedPrint(tNeedPrint[i]);
			tLCGrpIssuePolSchema.setOperatePos("5");
			tLCGrpIssuePolSchema.setBackObjType(tBackObjType[i]);
			System.out.println("555555"+tBackObjType[i]);
			tLCGrpIssuePolSchema.setBackObj(tBackObj[i]);
			tLCGrpIssuePolSchema.setErrFieldName(tIssueName[i]);
			tLCGrpIssuePolSchema.setPrtSeq(tprtseq[i]);
			tLCGrpIssuePolSchema.setState(tstate[i]);
			tLCGrpIssuePolSchema.setOperator(tOperator[i]);
			tLCGrpIssuePolSet.add(tLCGrpIssuePolSchema);
  	}
  }else if(transact.equals("UPDATE||MAIN"))
  	{
  	String tIssueCode    = request.getParameter("IssueCode");
  	String tIssueName    = request.getParameter("IssueName");
  	String tIssueRuleCode   = request.getParameter("IssueRuleCode");
  	String tIssueRuleCode1   = request.getParameter("IssueRuleCode1");
  	String tBackObjType  = request.getParameter("BackObj");
		String tBackObj      = request.getParameter("BackObjType");
		String tIssueCont    = request.getParameter("IssueCont");
		String tNeedPrint =request.getParameter("NeedPrint");
		String tGrpContNo    = request.getParameter("GrpContNo");
		String tSerialNo     = request.getParameter("SerialNo");
    String mGrpContNo = request.getParameter("GrpContNo");
    String tprtseq = request.getParameter("prtseq");
    String tstate = request.getParameter("state");
    String tOperator = request.getParameter("Operator");
			LCGrpIssuePolSchema tLCGrpIssuePolSchema = new LCGrpIssuePolSchema();
			tLCGrpIssuePolSchema.setGrpContNo(mGrpContNo);
			tLCGrpIssuePolSchema.setProposalGrpContNo(mGrpContNo);
			tLCGrpIssuePolSchema.setErrField(tIssueCode);
			if(tIssueRuleCode.equals(""))
			tLCGrpIssuePolSchema.setQuestionObj(tIssueRuleCode1);
			else
			tLCGrpIssuePolSchema.setQuestionObj(tIssueRuleCode);	
			tLCGrpIssuePolSchema.setIssueType("2");
			tLCGrpIssuePolSchema.setIssueCont(tIssueCont);
			tLCGrpIssuePolSchema.setSerialNo(tSerialNo);
			tLCGrpIssuePolSchema.setNeedPrint(tNeedPrint);
			tLCGrpIssuePolSchema.setOperatePos("5");
			tLCGrpIssuePolSchema.setBackObjType(tBackObjType);
			tLCGrpIssuePolSchema.setBackObj(tBackObj);
			tLCGrpIssuePolSchema.setErrFieldName(tIssueName);
			tLCGrpIssuePolSchema.setPrtSeq(tprtseq);
			tLCGrpIssuePolSchema.setState(tstate);
			tLCGrpIssuePolSchema.setOperator(tOperator);
			tLCGrpIssuePolSet.add(tLCGrpIssuePolSchema);
  	}else if(transact.equals("REPLY||MAIN"))
  		{
  	String tIssueCode    = request.getParameter("IssueCode");
  	String tIssueName    = request.getParameter("IssueName");
  	String tIssueRuleCode   = request.getParameter("IssueRuleCode");
  	String tIssueRuleCode1   = request.getParameter("IssueRuleCode1");
  	String tBackObjType  = request.getParameter("BackObj");
		String tBackObj      = request.getParameter("BackObjType");
		String tIssueCont    = request.getParameter("IssueCont");
		String tNeedPrint =request.getParameter("NeedPrint");
		String tGrpContNo    = request.getParameter("GrpContNo");
		String tSerialNo     = request.getParameter("SerialNo");
    String mGrpContNo = request.getParameter("GrpContNo");
    String tprtseq = request.getParameter("prtseq");
    String tstate = request.getParameter("state");
    String tOperator = request.getParameter("Operator");
			LCGrpIssuePolSchema tLCGrpIssuePolSchema = new LCGrpIssuePolSchema();
			tLCGrpIssuePolSchema.setGrpContNo(mGrpContNo);
			tLCGrpIssuePolSchema.setProposalGrpContNo(mGrpContNo);
			tLCGrpIssuePolSchema.setErrField(tIssueCode);
			if(tIssueRuleCode.equals(""))
			tLCGrpIssuePolSchema.setQuestionObj(tIssueRuleCode1);
			else
			tLCGrpIssuePolSchema.setQuestionObj(tIssueRuleCode);	
			tLCGrpIssuePolSchema.setIssueType("2");
			tLCGrpIssuePolSchema.setIssueCont(tIssueCont);
			tLCGrpIssuePolSchema.setSerialNo(tSerialNo);
			tLCGrpIssuePolSchema.setNeedPrint(tNeedPrint);
			tLCGrpIssuePolSchema.setOperatePos("5");
			tLCGrpIssuePolSchema.setBackObjType(tBackObjType);
			tLCGrpIssuePolSchema.setBackObj(tBackObj);
			tLCGrpIssuePolSchema.setErrFieldName(tIssueName);
			tLCGrpIssuePolSchema.setPrtSeq(tprtseq);
			tLCGrpIssuePolSchema.setState(tstate);
			tLCGrpIssuePolSchema.setOperator(tOperator);
			tLCGrpIssuePolSet.add(tLCGrpIssuePolSchema);  		
  		}
  try
  {
  // 准备传输数据 VData
  	TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("LoadFlag",tLoadFlag);
  	System.out.println("LoadFlag is : " + request.getParameter("LoadFlag"));
  	VData tVData = new VData();
  	System.out.println(tLCGrpIssuePolSet.encode());
	  tVData.add(tLCGrpIssuePolSet);
  	tVData.add(tG);
  	tVData.addElement(tTransferData);
  	System.out.println(transact);
    tChoiceAutoQuestUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tChoiceAutoQuestUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
