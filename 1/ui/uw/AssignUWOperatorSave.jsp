
<%
	//程序功能：
	//创建日期：2002-11-25
	//创建人  ：Kevin
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page contentType="text/html;charset=GBK"%>
<%
	GlobalInput globalInput = new GlobalInput();
	String FlagStr = "";
	String Content = "";
	if ((GlobalInput) session.getValue("GI") == null) {

		System.out.println("页面失效,请重新登陆");
		FlagStr = "Fail";
		Content = "页面失效,请重新登陆";

	} else {
		globalInput.setSchema((GlobalInput) session.getValue("GI"));
		String tModifyUWOperator = request
		.getParameter("mModifyUWOperator");
		String tPiType =request.getParameter("PiType");
		String tAction = request.getParameter("mAction");
		String strOperation = request.getParameter("fmtransact");
		String tActiveId[]=request.getParameterValues("PolGrid12");
		String tOtherno[] = request.getParameterValues("PolGrid1");
		String tPrtNo[] = request.getParameterValues("PolGrid2");
		String tContType[] = request.getParameterValues("PolGrid10");
		String tChk[] = request.getParameterValues("InpPolGridChk");

		LCContSet tLCContSet = new LCContSet();
		LPEdorItemSet tLPEdorItemSet=new LPEdorItemSet();
        LLCaseSet tLLCaseSet=new LLCaseSet();
        LLCaseSet tLLCaseSets=new LLCaseSet();
		boolean flag = false;
		int feeCount = tPrtNo.length;
		System.out.println("feecout:" + feeCount);

		String   lc="";
		String   lp="";
		String   ll="";
		String   lls="";
		for (int i = 0; i < feeCount; i++) {
			if (!tOtherno.equals("") && tChk[i].equals("1")) {

				//契约
				if(tActiveId[i].equals("0000001100"))
				{
					lc="0000001100";
					 System.out.println("PrtNo:" + i + ":" + tOtherno[i]);
				       LCContSchema tLCContSchema = new LCContSchema();
				       tLCContSchema.setPrtNo(tOtherno[i]);
				       tLCContSchema.setContType(tContType[i]);
				       tLCContSet.add(tLCContSchema);
				       flag = true;
				}
//				保全
				if(tActiveId[i].equals("0000001180"))
				{
					lp="0000001180";
					 System.out.println("Otherno:" + i + ":" + tOtherno[i]);
				       LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
				       tLPEdorItemSchema.setEdorNo(tOtherno[i]);
				       tLPEdorItemSet.add(tLPEdorItemSchema);
				       flag = true;
				}
//				理赔
				if(tActiveId[i].equals("0000001182"))
				{
					ll="0000001182";
					 System.out.println("Otherno:" + i + ":" + tOtherno[i]);
				    LLCaseSchema tLLcaseSchema=new LLCaseSchema();
				    tLLcaseSchema.setCaseNo(tOtherno[i]);
				    tLLCaseSet.add(tLLcaseSchema);
				       flag = true;
				}
//				理赔续保
				if(tActiveId[i].equals("0000001181"))
				{
					lls="0000001181";
					 System.out.println("Otherno:" + i + ":" + tOtherno[i]);
					 System.out.println("lls:" + i + ":" +lls);
				    LLCaseSchema tLLcaseSchemas=new LLCaseSchema();
				    tLLcaseSchemas.setCaseNo(tOtherno[i]);
				    tLLCaseSets.add(tLLcaseSchemas);
				       flag = true;
				}

		      
			}
		}

		VData vData = new VData();
		vData.add(tModifyUWOperator);
		vData.add(tAction);
		vData.add(globalInput);
		vData.add(tLCContSet);
		vData.add(tLPEdorItemSet);
		vData.add(tLLCaseSet);
		vData.add(tLLCaseSets);
		vData.add(tPiType);
		vData.add(tOtherno);
		vData.add(lc);
		vData.add(lp);
		vData.add(ll);
        vData.add(lls);
		AssignUWOperatorBL tQyModifyUWOperatorBL = new AssignUWOperatorBL();
		try {
			if (!tQyModifyUWOperatorBL.submitData(vData, strOperation)) {
		         if (tQyModifyUWOperatorBL.mErrors.needDealError()) {
			           Content = tQyModifyUWOperatorBL.mErrors.getFirstError();
		         } else {
			           Content = "保存失败，但是没有详细的原因";
		         }
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Content = ex.getMessage();
		}
		if (Content.equals("")) {
			FlagStr = "Succ";
			Content = "保存成功";
		} else {
			FlagStr = "Fail";
		}
	}
	
%>
<html>      
	<script language="javascript">
	
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>
