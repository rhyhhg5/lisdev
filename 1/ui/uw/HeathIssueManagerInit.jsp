
<jsp:directive.page import="com.sinosoft.lis.pubfun.GlobalInput"/>
<%
	//程序名称：MeetInit.jsp
	//程序功能：
	//创建日期：2003-03-04
	//创建人  ：Kevin
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput) session.getValue("GI");
	String strManageCom = globalInput.ComCode;

%>

<script language="JavaScript">

var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {
  	fm.reset();                                   
  }
  catch(ex)
  {
    alert("在MeetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
    try
    {
        initInpBox();
        initPolGrid();

        fm.all('ManageCom').value = <%=strManageCom%>;
        if(fm.all('ManageCom').value==86){
            fm.all('ManageCom').readOnly=false;
        }
        else{
            fm.all('ManageCom').readOnly=true;
        }
        if(fm.all('ManageCom').value!=null)
        {
            var arrResult = easyExecSql("select Name from LDCom where ComCode = '" + fm.all('ManageCom').value + "'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('ManageComName').value=arrResult[0][0];
            }
        }
        //下发日期段默认为三个月
        var arrResult = easyExecSql("select Current Date - 3 Month, Current Date from dual");
        if (arrResult != null) {
            fm.all('startMakeDate').value=arrResult[0][0];
            fm.all('endMakeDate').value=arrResult[0][1];
        }
        fm.all('CodeType').value="0";
        fm.all('CodeTypeName').value="问题件";

    }
    catch(re)
    {
        alert("MeetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

// 投保单信息列表的初始化
function initPolGrid()
{
    var iArray = new Array();

    try {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            	//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="单证";         		//列名
        iArray[1][1]="0px";            	//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[2]=new Array();
        iArray[2][0]="单证类型";         		//列名
        iArray[2][1]="50px";            	//列宽
        iArray[2][2]=100;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[3]=new Array();
        iArray[3][0]="单证通知号";         		//列名
        iArray[3][1]="100px";            	//列宽
        iArray[3][2]=100;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[4]=new Array();
        iArray[4][0]="印刷号";       		//列名
        iArray[4][1]="80px";            	//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[5]=new Array();
        iArray[5][0]="申请日期";       		//列名
        iArray[5][1]="80px";            	//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[6]=new Array();
        iArray[6][0]="生效日期";       		//列名
        iArray[6][1]="80px";            	//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[7]=new Array();
        iArray[7][0]="投保人";       		//列名
        iArray[7][1]="60px";            	//列宽
        iArray[7][2]=100;            			//列最大值
        iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[8]=new Array();
        iArray[8][0]="单证对象";       		//列名
        iArray[8][1]="60px";            	//列宽
        iArray[8][2]=100;            			//列最大值
        iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[9]=new Array();
        iArray[9][0]="业务员代码";         	//列名
        iArray[9][1]="75px";            	//列宽
        iArray[9][2]=100;            			//列最大值
        iArray[9][3]=0;

        iArray[10]=new Array();
        iArray[10][0]="业务员名称";         	//列名
        iArray[10][1]="75px";            	//列宽
        iArray[10][2]=100;            			//列最大值
        iArray[10][3]=0;

        iArray[11]=new Array();
        iArray[11][0]="下发日期";        //列名
        iArray[11][1]="80px";            	//列宽
        iArray[11][2]=200;            			//列最大值
        iArray[11][3]=0; 									//是否允许输入,1表示允许，0表示不允许

        iArray[12]=new Array();
        iArray[12][0]="回销日期";        //列名
        iArray[12][1]="80px";            	//列宽
        iArray[12][2]=200;            			//列最大值
        iArray[12][3]=0; 									//是否允许输入,1表示允许，0表示不允许

        iArray[13]=new Array();
        iArray[13][0]="是否已打";        //列名
        iArray[13][1]="50px";            	//列宽
        iArray[13][2]=200;            			//列最大值
        iArray[13][3]=0; 									//是否允许输入,1表示允许，0表示不允许

        iArray[14]=new Array();
        iArray[14][0]="管理机构";         	//列名
        iArray[14][1]="60px";            	//列宽
        iArray[14][2]=100;            			//列最大值
        iArray[14][3]=0;
        

        PolGrid = new MulLineEnter( "fmSave" , "PolGrid" );
        //这些属性必须在loadMulLine前
        PolGrid.mulLineCount = 0;
        PolGrid.displayTitle = 1;
        PolGrid.canSel = 1;
        PolGrid.hiddenPlus = 1;
        PolGrid.hiddenSubtraction = 1;
        PolGrid.locked = 1;
        PolGrid.loadMulLine(iArray);

    } catch(ex) {
        alert(ex);
    }
}

function initIssueGrid()
{                              
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="100px";         			//列宽
    iArray[1][2]=10;          			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="印刷号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="100px";         			//列宽
    iArray[2][2]=10;          			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="问题件通知书号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="100px";         			//列宽
    iArray[3][2]=10;          			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="保全受理号";         	//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="100px";         			//列宽
    iArray[4][2]=10;          			    //列最大值
    iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="业务员代码";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[5][1]="80px";         			//列宽
    iArray[5][2]=10;          			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="下发日期";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[6][1]="80px";         			//列宽
    iArray[6][2]=10;          			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="打印状态";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[7][1]="80px";         			//列宽
    iArray[7][2]=10;          			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    IssueGrid = new MulLineEnter("fmSave", "IssueGrid"); 
    //这些属性必须在loadMulLine前                            
    IssueGrid.mulLineCount = 0;
    IssueGrid.displayTitle = 1;
    IssueGrid.hiddenPlus = 1;   
    IssueGrid.hiddenSubtraction = 1;
    IssueGrid.canSel =1;
    IssueGrid.canChk = 0;
    IssueGrid.selBoxEventFuncName ="onClickedPrint" ;
    IssueGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>
