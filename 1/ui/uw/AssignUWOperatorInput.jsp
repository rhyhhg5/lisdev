<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI"); 
tGI.ManageCom = "86";

%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; 	//记录管理机构
    var comcode = "<%=tGI.ComCode%>";		//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="AssignUWOperatorInput.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="AssignUWOperatorInit.jsp" %>
<title>维护保单核保师</title>
</head>
<body onload="initForm();">
	<form action="./AssignUWOperatorSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入保单查询条件：</td>
			</tr>
		</table>
		<table class="common" align=center>
			<tr class="common">
				<td class="title">管理机构</td>
				<td class="input"><Input class=codeNo name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,'1 and #1#=#1# and length(trim(comcode))=4 ','1');" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></td>
				<td class="title">客户姓名</td>
				<td class="input"><input class="common" name="AppntName" /></td>
				<td class="title" style="display:none;">印刷号</td>
				<td class="input" style="display:none;"><input class="common" name="PrtNo" /></td>
			</tr>
			<tr class="common">
				<td class="title">核保件类型</td>
				<td class="input"><Input class=codeno name=PiType value="1" CodeData="0|^1|全部^2|契约^3|保全^4|理赔" ondblclick="return showCodeListEx('PiType',[this,PiTypeName],[0,1],null,null,null,1);" ><input class=codename name=PiTypeName value="全部" readonly=true ></td>
				<td class="title">业务号</td>
				<td class="input"><input class="common" name="Otherno" /></td>
			</tr>
		</table>
		<input class="cssButton" type="button" value="查询保单" onclick="easyQueryClick();" />
		<INPUT  type="hidden" class=Common name=querySql >
		
		<table>
			<tr>
				<td class=common>
					<IMG src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divLCPol1);">
				</td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>

			<table align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		 </table>
		</div>
		
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmOperator" name="fmOperator">
		<input type=hidden id="mContType" name="mContType">
		<input type=hidden id="mSQL" name="mSQL">
		<input type=hidden id="mAction" name="mAction">
		
		<table>
			<tr>
				<td class= titleImg>分配核保师信息</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>分配给核保师</TD>
				<TD class= input>
					<Input class= 'codeno' name=mModifyUWOperator  verify="分配给核保师|len<=10&code:uwusercode" ondblclick="return showCodeList('uwusercode',[this,UserCodeName],[0,1],null,'1 and #1# = #1# and (comcode = #'+fm.all('ManageCom').value+'# or comcode = #'+fm.all('ManageCom').value.substring(0,2)+'# or comcode = #'+fm.all('ManageCom').value.substring(0,4)+'# )','1',1);" onkeyup="return showCodeListKey('uwusercode',[this,UserCodeName],[0,1],null,null,null,null,1);"><input  class="codename" name="UserCodeName" > 
				</TD>
				<TD class= title></TD>
				<TD class= input><Input class= readonly readOnly=true name=mOperator></TD>
			</TR>
		</table>
		<INPUT VALUE="分配核保师" name=modifybtn class="cssButton" TYPE=button onclick="modifyUWOperator();">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>