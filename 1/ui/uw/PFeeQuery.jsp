<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PFeeQuery.jsp
//程序功能：个人充帐查询
//创建日期：2002-12-16
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="PFeeQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PFeeQueryInit.jsp"%>
  <title>个人冲帐 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./PFeeQueryChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入交费查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class= common name=PayNo >
          </TD>
          <TD  class= title>
            应收/实收编号
          </TD>
          <TD  class= input>
            <Input class= common name=IncomeNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            应收/实收编号类型
          </TD>
          <TD  class= input>
      	 		<Input class=code name=IncomeType verify="应收/实收编号类型" CodeData="0|^0|合同号^1|集体保单号^2|个人保单号^3|批改号" ondblClick="showCodeListEx('FeeIncomeType',[this],[0,1,2,3]);" onkeyup="showCodeListKeyEx('FeeIncomeType',[this],[0,1,2,3]);">          
            <!--Input class=common name=IncomeType -->
          </TD>          
          <TD  class= title>
            交费日期
          </TD>
          <TD  class= input>
          	<Input class= "coolDatePicker" dateFormat="short" name=PayDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <!--Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"-->
            <Input class="code" name=MngCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
          </TD>
          <TD  class= title>
            代理人编码 
          </TD> 
          <TD  class= input>
            <Input class="code" name=AgentCode verify="代理人编码|notnull&code:AgentCode" ondblclick="return showCodeList('AgentCode',[this], [0], null, MngCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this], [0], null, MngCom, 'ManageCom');">
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="费用明细" TYPE=button onclick="getQueryDetail();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAPay1);">
    		</td>
    		<td class= titleImg>
    			 费用信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJAPay1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     <!-- <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 	
     --> 
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      <INPUT VALUE="个人冲帐" TYPE=button onclick="submitForm();">				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
