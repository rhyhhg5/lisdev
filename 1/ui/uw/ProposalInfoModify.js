//程序名称：ContInfoModify.js
//程序功能：新契约信息修改
//创建日期：2002-09-24 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();



//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {   
  
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
 
}

//初始化界面信息
function easyQueryClick()
{
    //初始化投保人信息
    var contSql = " select PrtNo, ManageCom, AgentCode, SaleChnl, AgentCom, AgentGroup,getUniteCode(agentcode) from LCCont where "
	            + " PrtNo = '"+prtNo+"'"
	            ;
    
    turnPage.strQueryResult = easyQueryVer3(contSql, 1, 0, 1);
    if (!turnPage.strQueryResult) {
        alert("未查到合同信息");
        return;
    }
    var arrSelected = new Array();
    arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.PrtNo1.value    = arrSelected[0][0];
    fm.ManageCom.value = arrSelected[0][1];
    fm.AgentCode.value = arrSelected[0][2];
    fm.SaleChnl.value  = arrSelected[0][3];
    fm.AgentCom.value  = arrSelected[0][4];
    fm.AgentGroup.value= arrSelected[0][5];
    fm.GroupAgentCode.value = arrSelected[0][6];
    if(arrSelected[0][3] == "03" || arrSelected[0][3] == "04")
    {
        fm.all("AgentComTitleID").style.display = "";
        fm.all("AgentComInputID").style.display = "";
        if(arrSelected[0][3] == "03") agentComCode = "agentcombrief";//中介
        if(arrSelected[0][3] == "04") agentComCode = "agentcombank";//银行
    }
    
	var strsql  = " select appntname,idtype,idno,appntno,addressNo,appntno from lcappnt where "              
               +" PrtNo = '"+prtNo+"'"
               ;

    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 0, 1);
    if (!turnPage.strQueryResult) {
        alert("未查到投保人信息");    
        return;
    }
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.AppntName.value = turnPage.arrDataCacheSet[0][0];
    fm.AppntIDType.value = turnPage.arrDataCacheSet[0][1];
    fm.AppntIDNo.value = turnPage.arrDataCacheSet[0][2];
    fm.AppntNo.value = turnPage.arrDataCacheSet[0][5];
    var tAppntNo = turnPage.arrDataCacheSet[0][3];
    var tAddressNo = turnPage.arrDataCacheSet[0][4];
    
  
    var appntAdressSql = " select PostalAddress,ZipCode,mobile,HomePhone from LCAddress where CustomerNo = '"+tAppntNo+"'"
                     + " and addressno = '"+tAddressNo+"'";
    turnPage.strQueryResult = easyQueryVer3(appntAdressSql, 1, 0, 1);  
    if (!turnPage.strQueryResult) {
        alert("未查到投保人地址信息");          
    }               
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.AppntPostalAddress.value = turnPage.arrDataCacheSet[0][0];
    fm.AppntZipCode.value = turnPage.arrDataCacheSet[0][1];
    fm.Mobile.value = turnPage.arrDataCacheSet[0][2];
    fm.HomePhone.value = turnPage.arrDataCacheSet[0][3];                   
  
    //初始化被保险人信息
    if(ContNo!=null&&ContNo!="") {
        var strSQL ="select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo from LCInsured where ContNo='"+ContNo+"' order by SequenceNo";
        turnPage.queryModal(strSQL,InsuredGrid);
    
        showAllCodeName(); 
    }
}


/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人详细信息，填入被保人信息表
 *  返回值：  无
 *********************************************************************
 */
function getInsuredDetail(parm1,parm2) {
	divLCBnf1.style.display = "none";
	
  fm.SequenceNo.value=fm.all(parm1).all('InsuredGrid6').value;
  if(fm.SequenceNo.value=="1") {   
    fm.InsuredSequencename.value="第一被保险人资料";
  }
  if(fm.SequenceNo.value=="2") {    
    fm.InsuredSequencename.value="第二被保险人资料";
  }
  if(fm.SequenceNo.value=="3") { 
    fm.InsuredSequencename.value="第三被保险人资料";
  }
  var InsuredNo=fm.all(parm1).all('InsuredGrid1').value;
	 var tsql = "select insuredno,name,idtype,idno,addressNo from lcinsured where "              
               + " PrtNo = '"+prtNo+"'"
               + " and insuredno = '"+InsuredNo+"'";
fm.InsuredName.value = tsql;
  turnPage.strQueryResult = easyQueryVer3(tsql, 1, 0, 1);
  if (!turnPage.strQueryResult) {
      alert("未查到被保险人信息");    
      return;
    }
    
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  fm.InsuredNo.value       = turnPage.arrDataCacheSet[0][0];
  fm.InsuredName.value     = turnPage.arrDataCacheSet[0][1];
  fm.InsuredIDType.value   = turnPage.arrDataCacheSet[0][2];
  fm.InsuredIDNo.value     = turnPage.arrDataCacheSet[0][3];
  var tInsuredNo = fm.InsuredNo.value;
   
  getInsuredPolInfo();
  showAllCodeName(); 

}

/*********************************************************************
 *  获得被保人险种信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getInsuredPolInfo() {
  initPolGrid();
  var InsuredNo=fm.all("InsuredNo").value;
 
  //险种信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select PolNo,RiskCode,Prem,Amnt,insuredname from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'";
    //alert(strSQL);
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}


/*********************************************************************
 *  获险种的受益人信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getBnfDetail(parm1) {
	

  divLCBnf1.style.display = "";
  divBnfButton.style.display = "";
  var InsuredNo=fm.all("InsuredNo").value;
  //var PolNo=fm.all(parm1).all('PolGrid1').value;
  //fm.PolNo.value = PolNo;
 
  //险种信息初始化
  if(ContNo!=null&&ContNo!="") 
  {
    var strSQL ="select (select riskcode from lcpol where polno=LCBnf.polno),(select insuredname from lcpol where polno=LCBnf.polno and conttype='1'),Name,IDType,IDNo,RelationToInsured,BnfLot,BnfGrade,BnfType,polno from LCBnf where ContNo='"+ContNo+"' and InsuredNo='"+InsuredNo+"'  ";
    //alert(strSQL);
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = BnfGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}

//修改投保人信息
function modifyAppnt()
{
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

   fm.action = "./ProposalAppntInfoModifyChk.jsp";
   fm.submit();
}

//修改投保人信息
function modifyInsured()
{
	var tSel = InsuredGrid.getSelNo();

  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择一条记录。" );
    return;
  }
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

   
   fm.action = "./ProposalInsuredInfoModifyChk.jsp";
   fm.submit();
}

//修改投保人信息
function modifyBnf()
{
	 var tSel = PolGrid.getSelNo();	 
   if( tSel == 0 || tSel == null )
   {
     alert( "请先选择一条险种信息" );
     return;
   }
	 var tRiskCode = PolGrid.getRowColData(tSel-1,2);	
	 var tSql = "select 1 from LMRiskEdorItem where edorcode='BC' and riskcode='"+tRiskCode+"'";
	 var arr = easyExecSql(tSql); 
	 if(!arr)
	 {
	    alert("该险种不允许添加受益人!")
	    return; 	
	 }
	
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

   
   fm.action = "./ProposalBnfInfoModifyChk.jsp";
   fm.submit();
}
function AddOne()
{
			// alert(111); 
	 var tSel = PolGrid.getSelNo();	 
   if( tSel == 0 || tSel == null )
   {
     alert( "请先选择一条险种信息" );
     return;
   }
	 var tRiskCode = PolGrid.getRowColData(tSel-1,2);	
	 var tSql = "select 1 from LMRiskEdorItem where edorcode='BC' and riskcode='"+tRiskCode+"'";
	 var arr = easyExecSql(tSql); 
	 if(!arr)
	 {
	    alert("该险种不允许添加受益人!")
	    initBnfGrid();	
	 }
	 //alert(PolGrid.getSelNo());
	// alert(PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2));
	// alert(PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1));
	var i = BnfGrid.mulLineCount;
	if(i>0){         
	//	 alert(i);                                       
 	if (i == 1)                                                                    
  	{                                                                              
  	  fm.BnfGrid1.value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);      
  	  fm.BnfGrid10.value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  	  fm.BnfGrid2.value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 5);                                                                                                                                                        
  	}                                                                              
    else                                                                           
  	{                                                                              
  	  fm.BnfGrid1[i-1].value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2); 
  	  fm.BnfGrid10[i-1].value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);                                          
			fm.BnfGrid2[i-1].value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 5);  	
  	}
  }   
}

//双击事件后执行
function afterCodeSelect(cCodeName, Field)
{
    if(cCodeName == "LCSaleChnl")
	{
	    if(Field.value == "03" || Field.value == "04")
	    {
	        fm.all("AgentComTitleID").style.display = "";
	        fm.all("AgentComInputID").style.display = "";
	        if(Field.value == "03") agentComCode = "agentcombrief";//中介
	        if(Field.value == "04") agentComCode = "agentcombank";//银行
	    }
	    else
	    {
	        fm.all("AgentComTitleID").style.display = "none";
	        fm.all("AgentComInputID").style.display = "none";
	    }
	    fm.AgentCom.value = "";
        //fm.AgentName.value = "";
	    fm.AgentCode.value = "";
        fm.AgentGroup.value = "";
	}
    
    //银行网点选中后自动带出银代专员信息
    if (cCodeName == "agentcombank")
    {
        fm.AgentCode.value = "";
        //fm.AgentName.value = "";
        fm.AgentGroup.value = "";
        if(fm.SaleChnl.value == "04")
        {
            var agentCodeSql = "select a.AgentCode, b.Name, b.AgentGroup,b.groupagentcode from LAComToAgent a left join LAAgent b "
                      + "on a.AgentCode = b.AgentCode where AgentCom='" 
                      + fm.AgentCom.value 
                      + "' and RelaType='1' and b.BranchType = '3' and b.BranchType2 = '01'";
            var arr = easyExecSql(agentCodeSql);
            if(arr){
                fm.GroupAgentCode.value = arr[0][3];
                fm.AgentCode.value = arr[0][0];
                //fm.AgentName.value = arr[0][1];
                fm.AgentGroup.value = arr[0][2];
            }
        }
    }
    
    //中介
    if (cCodeName == "agentcombrief")
    {
        fm.AgentCode.value = "";
        //fm.AgentName.value = "";
        fm.AgentGroup.value = "";
        if(fm.SaleChnl.value == "03")
        {
            var agentCodeSql = "select a.AgentCode, b.Name, b.AgentGroup,b.groupagentcode from LAComToAgent a left join LAAgent b "
                      + "on a.AgentCode = b.AgentCode where AgentCom='" 
                      + fm.AgentCom.value 
                      + "' and RelaType='1' and b.BranchType = '2' and b.BranchType2 = '02'";
            var arr = easyExecSql(agentCodeSql);
            if(arr){
                fm.GroupAgentCode.value = arr[0][3];
                fm.AgentCode.value = arr[0][0];
                //fm.AgentName.value = arr[0][1];
                fm.AgentGroup.value = arr[0][2];
            }
        }
    }
}

//查询业务员信息
function queryAgentCode()
{
	var saleChnl = fm.SaleChnl.value;
	var agentCom = fm.AgentCom.value;
    var branchType = 1;
    
    var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
        + "&SaleChnl=" + saleChnl
        + "&AgentCom=" + agentCom
        + "&branchtype=" + branchType;
    var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
    if(arrResult!=null)
    {
  	    fm.AgentCode.value = arrResult[0][0];
  	    fm.AgentGroup.value = arrResult[0][1];
  	    fm.GroupAgentCode.value = arrResult[0][95];
    }
}

//修改投保人信息
function modifyCont()
{
    //销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
    //销售渠道为中介时中介机构的校验
    if(!checkAgentCom())
    {
        return false;
    }
    if(fm.AgentGroup.value == null || fm.AgentGroup.value == "")
    {
        alert("请双击业务员输入框，查询业务员信息！");
        return false;
    }
    
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
    fm.action = "./ProposalContInfoModify.jsp";
    fm.submit();
}

//销售渠道和业务员的校验
function checkSaleChnl()
{
    var agentc = trim(fm.GroupAgentCode.value);
    if(agentc==""||agentc==null){
        alert("业务员未录入！");
        return false;
    }
	var saleChnl = fm.SaleChnl.value;
	var agentCode = fm.AgentCode.value;
    
	var check = easyExecSql("select 1 from LAAgent a, LDCode1 b where CodeType = 'salechnl' and Code = '" 
	    + saleChnl + "' and AgentCode = '" + agentCode + "' AND BranchType = Code1 and BranchType2 = CodeName");
	if(!check)
	{
        alert("销售渠道和业务员不相符!");
        return false;
	}
	return true;
}

//销售渠道为中介代理或银行代理时中介机构的校验
function checkAgentCom()
{
	var saleChnl = fm.SaleChnl.value;
	var agentCom = fm.AgentCom.value;
	
	if(saleChnl == "03" || saleChnl == "04")
	{
	    if(agentCom == null || agentCom == "")
	    {
	        alert("中介机构为空，请填写！");
	        return false;
	    }
	    else
	    {
            var result = easyExecSql("select 1 from LAComToAgent where AgentCom = '" + agentCom + "' and AgentCode = '" + fm.AgentCode.value + "'");
            if(!result)
            {
                return confirm("中介机构和业务员不匹配！");
            }
	    }
	}
	return true;
}