//程序名称：GrpUWInsuCont.js
//程序功能：团体既往理陪函数
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

/*********************************************************************
 *  该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

var turnPage  = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

function easyQueryI()
{
  // 书写SQL语句
  var arrRs;
  var strSQL = "select GrpName";
  strSQL    += "  from LDGrp";
  strSQL    += " where CustomerNo='"+mAppntNo+"'";

  fm.all("GrpNo").value=mAppntNo;

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //查询成功设置画面上的数据
  if(turnPage.strQueryResult)
  {
    arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.all("GrpName").value=arrRs[0][0];
  }

  strSQL  = "select ProposalGrpContNo";
  strSQL += "  from LCGrpCont";
  strSQL += " where GrpContNo = '" + mGrpContNo + "'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //查询成功设置画面上的数据
  if(turnPage.strQueryResult)
  {
    arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.all("GrpPolNo").value=arrRs[0][0];
  }
}

/*********************************************************************
 *  查询保障信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function easyQuery()
{
    var tStrSql = ""
        + " select sumInfo.GrpContNo, sumInfo.ProposalGrpContNo, sumInfo.PrtNo, "
        + " sumInfo.Peoples2, sumInfo.SumActuPayMoney, "
        + " nvl(sum(sumInfo.ClaimCount), 0) ClaimCount, "
        + " nvl(sum(sumInfo.ClaimPay), 0) ClaimPay, "
        + " nvl(sum(sumInfo.ClaimRate), 0) ClaimRate, "
        + " nvl(sum(sumInfo.UnsettledPeoples), 0) UnsettledPeoples, "
        + " nvl(sum(sumInfo.UnsettledPay), 0) UnsettledPay, "
        + " nvl(sum(sumInfo.DeclinePeoples), 0) DeclinePeoples, "
        + " nvl(sum(sumInfo.DeclinePay), 0) DeclinePay, "
        + " sumInfo.ContState, sumInfo.CValidate, sumInfo.CInValidate, '0' WTFlag "
        + " from "
        + " ( "
        + " select tmp.GrpContNo, tmp.ProposalGrpContNo, tmp.PrtNo, "
        + " tmp.Peoples2, tmp.CValidate, tmp.CInValidate, tmp.ContState, "
        + " tmp.SumActuPayMoney, "
        + " tmp.RealPay ClaimPay, "
        + " tmp.CustomerNoCount ClaimCount, "
        + " nvl(double(Div(tmp.RealPay, tmp.SumActuPayMoney)), 0) ClaimRate, "
        + " 0 UnsettledPay, "
        + " 0 UnsettledPeoples, "
        + " 0 DeclinePay, "
        + " 0 DeclinePeoples "
        + " from "
        + " ( "
        + " select TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, TmpContInfo.ContState, "
        + " TmpContInfo.SumActuPayMoney, "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) RealPay "
        + " from "
        + " ( "
        + " select lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, "
        + " lgc.Peoples2, lgc.CValidate, lgc.CInValidate, "
        + " CodeName('stateflag', lgc.StateFlag) ContState, "
        + " sum(ljap.SumActuPayMoney) SumActuPayMoney "
        + " from LCGrpCont lgc "
        + " inner join LJAPay ljap on ljap.IncomeNo = lgc.GrpContNo and ljap.IncomeType = '1' "
        + " where 1 = 1 "
        + " and lgc.AppntNo = '" + mAppntNo + "' "
        + " and lgc.AppFlag = '1' "
        + " and lgc.SignDate is not null "
        + " and ljap.ConfDate is not null "
        + " group by lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, "
        + " lgc.Peoples2, lgc.CValidate, lgc.CInValidate, lgc.StateFlag "
        + " ) as TmpContInfo "
        + " left join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo and llcd.GiveType in ('1', '2', '3', '4', '5') "
        + " left join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1', '4', '5') and llc.RgtState in ('11', '12') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, "
        + " TmpContInfo.ContState, TmpContInfo.SumActuPayMoney "
        + " ) as tmp "
        + " union all "
        + " select tmp.GrpContNo, tmp.ProposalGrpContNo, tmp.PrtNo, "
        + " tmp.Peoples2, tmp.CValidate, tmp.CInValidate, tmp.ContState, "
        + " tmp.SumActuPayMoney, "
        + " 0 ClaimPay, "
        + " 0 ClaimCount, "
        + " 0 ClaimRate, "
        + " tmp.RealPay UnsettledPay, "
        + " tmp.CustomerNoCount UnsettledPeoples, "
        + " 0 DeclinePay, "
        + " 0 DeclinePeoples "
        + " from "
        + " ( "
        + " select TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, TmpContInfo.ContState, "
        + " TmpContInfo.SumActuPayMoney, "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) RealPay "
        + " from "
        + " ( "
        + " select lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, "
        + " lgc.Peoples2, lgc.CValidate, lgc.CInValidate, "
        + " CodeName('stateflag', lgc.StateFlag) ContState, "
        + " sum(ljap.SumActuPayMoney) SumActuPayMoney "
        + " from LCGrpCont lgc "
        + " inner join LJAPay ljap on ljap.IncomeNo = lgc.GrpContNo and ljap.IncomeType = '1' "
        + " where 1 = 1 "
        + " and lgc.AppntNo = '" + mAppntNo + "' "
        + " and lgc.AppFlag = '1' "
        + " and lgc.SignDate is not null "
        + " and ljap.ConfDate is not null "
        + " group by lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, "
        + " lgc.Peoples2, lgc.CValidate, lgc.CInValidate, lgc.StateFlag "
        + " ) as TmpContInfo "
        + " inner join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo "
        + " inner join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1', '4', '5') and llc.RgtState in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, "
        + " TmpContInfo.ContState, TmpContInfo.SumActuPayMoney "
        + " ) as tmp "
        + " union all "
        + " select tmp.GrpContNo, tmp.ProposalGrpContNo, tmp.PrtNo, "
        + " tmp.Peoples2, tmp.CValidate, tmp.CInValidate, tmp.ContState, "
        + " tmp.SumActuPayMoney, "
        + " 0 ClaimPay, "
        + " 0 ClaimCount, "
        + " nvl(double(Div(tmp.RealPay, tmp.SumActuPayMoney)), 0) ClaimRate, "
        + " 0 UnsettledPay, "
        + " 0 UnsettledPeoples, "
        + " tmp.DeclineAmnt DeclinePay, "
        + " tmp.CustomerNoCount DeclinePeoples "
        + " from "
        + " ( "
        + " select TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, TmpContInfo.ContState, "
        + " TmpContInfo.SumActuPayMoney, "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) RealPay, "
        + " nvl(sum(llcd.DeclineAmnt), 0) DeclineAmnt "
        + " from "
        + " ( "
        + " select lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, "
        + " lgc.Peoples2, lgc.CValidate, lgc.CInValidate, "
        + " CodeName('stateflag', lgc.StateFlag) ContState, "
        + " sum(ljap.SumActuPayMoney) SumActuPayMoney "
        + " from LCGrpCont lgc "
        + " inner join LJAPay ljap on ljap.IncomeNo = lgc.GrpContNo and ljap.IncomeType = '1' "
        + " where 1 = 1 "
        + " and lgc.AppntNo = '" + mAppntNo + "' "
        + " and lgc.AppFlag = '1' "
        + " and lgc.SignDate is not null "
        + " and ljap.ConfDate is not null "
        + " group by lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, "
        + " lgc.Peoples2, lgc.CValidate, lgc.CInValidate, lgc.StateFlag "
        + " ) as TmpContInfo "
        + " inner join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo and llcd.GiveType in ('2', '3') "
        + " inner join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1') and llc.RgtState in ('11', '12') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, "
        + " TmpContInfo.ContState, TmpContInfo.SumActuPayMoney "
        + " ) as tmp "
        + " ) as sumInfo "
        + " group by sumInfo.GrpContNo, sumInfo.ProposalGrpContNo, sumInfo.PrtNo, "
        + " sumInfo.Peoples2, sumInfo.CValidate, sumInfo.CInValidate, sumInfo.ContState, "
        + " sumInfo.SumActuPayMoney "
        + " union all "
        + " select sumInfo.GrpContNo, sumInfo.ProposalGrpContNo, sumInfo.PrtNo, "
        + " sumInfo.Peoples2, sumInfo.SumActuPayMoney, "
        + " nvl(sum(sumInfo.ClaimCount), 0) ClaimCount, "
        + " nvl(sum(sumInfo.ClaimPay), 0) ClaimPay, "
        + " nvl(sum(sumInfo.ClaimRate), 0) ClaimRate, "
        + " nvl(sum(sumInfo.UnsettledPeoples), 0) UnsettledPeoples, "
        + " nvl(sum(sumInfo.UnsettledPay), 0) UnsettledPay, "
        + " nvl(sum(sumInfo.DeclinePeoples), 0) DeclinePeoples, "
        + " nvl(sum(sumInfo.DeclinePay), 0) DeclinePay, "
        + " sumInfo.ContState, sumInfo.CValidate, sumInfo.CInValidate, '1' WTFlag "
        + " from "
        + " ( "
        + " select tmp.GrpContNo, tmp.ProposalGrpContNo, tmp.PrtNo, "
        + " tmp.Peoples2, tmp.CValidate, tmp.CInValidate, tmp.ContState, "
        + " tmp.SumActuPayMoney, "
        + " tmp.RealPay ClaimPay, "
        + " tmp.CustomerNoCount ClaimCount, "
        + " nvl(double(Div(tmp.RealPay, tmp.SumActuPayMoney)), 0) ClaimRate, "
        + " 0 UnsettledPay, "
        + " 0 UnsettledPeoples, "
        + " 0 DeclinePay, "
        + " 0 DeclinePeoples "
        + " from "
        + " ( "
        + " select TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, TmpContInfo.ContState, "
        + " TmpContInfo.SumActuPayMoney, "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) RealPay "
        + " from "
        + " ( "
        + " select lbgc.GrpContNo, lbgc.ProposalGrpContNo, lbgc.PrtNo, "
        + " lbgc.Peoples2, lbgc.CValidate, lbgc.CInValidate, "
        + " CodeName('stateflag', lbgc.StateFlag) ContState, "
        + " sum(ljap.SumActuPayMoney) SumActuPayMoney "
        + " from LBGrpCont lbgc "
        + " inner join LJAPay ljap on ljap.IncomeNo = lbgc.GrpContNo and ljap.IncomeType = '1' "
        + " where 1 = 1 "
        + " and lbgc.AppntNo = '" + mAppntNo + "' "
        + " and lbgc.AppFlag = '1' "
        + " and lbgc.SignDate is not null "
        + " and ljap.ConfDate is not null "
        + " group by lbgc.GrpContNo, lbgc.ProposalGrpContNo, lbgc.PrtNo, "
        + " lbgc.Peoples2, lbgc.CValidate, lbgc.CInValidate, lbgc.StateFlag "
        + " ) as TmpContInfo "
        + " left join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo and llcd.GiveType in ('1', '2', '3', '4', '5') "
        + " left join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1', '4', '5') and llc.RgtState in ('11', '12') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, "
        + " TmpContInfo.ContState, TmpContInfo.SumActuPayMoney "
        + " ) as tmp "
        + " union all "
        + " select tmp.GrpContNo, tmp.ProposalGrpContNo, tmp.PrtNo, "
        + " tmp.Peoples2, tmp.CValidate, tmp.CInValidate, tmp.ContState, "
        + " tmp.SumActuPayMoney, "
        + " 0 ClaimPay, "
        + " 0 ClaimCount, "
        + " 0 ClaimRate, "
        + " tmp.RealPay UnsettledPay, "
        + " tmp.CustomerNoCount UnsettledPeoples, "
        + " 0 DeclinePay, "
        + " 0 DeclinePeoples "
        + " from "
        + " ( "
        + " select TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, TmpContInfo.ContState, "
        + " TmpContInfo.SumActuPayMoney, "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) RealPay "
        + " from "
        + " ( "
        + " select lbgc.GrpContNo, lbgc.ProposalGrpContNo, lbgc.PrtNo, "
        + " lbgc.Peoples2, lbgc.CValidate, lbgc.CInValidate, "
        + " CodeName('stateflag', lbgc.StateFlag) ContState, "
        + " sum(ljap.SumActuPayMoney) SumActuPayMoney "
        + " from LBGrpCont lbgc "
        + " inner join LJAPay ljap on ljap.IncomeNo = lbgc.GrpContNo and ljap.IncomeType = '1' "
        + " where 1 = 1 "
        + " and lbgc.AppntNo = '" + mAppntNo + "' "
        + " and lbgc.AppFlag = '1' "
        + " and lbgc.SignDate is not null "
        + " and ljap.ConfDate is not null "
        + " group by lbgc.GrpContNo, lbgc.ProposalGrpContNo, lbgc.PrtNo, "
        + " lbgc.Peoples2, lbgc.CValidate, lbgc.CInValidate, lbgc.StateFlag "
        + " ) as TmpContInfo "
        + " inner join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo "
        + " inner join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1', '4', '5') and llc.RgtState in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, "
        + " TmpContInfo.ContState, TmpContInfo.SumActuPayMoney "
        + " ) as tmp "
        + " union all "
        + " select tmp.GrpContNo, tmp.ProposalGrpContNo, tmp.PrtNo, "
        + " tmp.Peoples2, tmp.CValidate, tmp.CInValidate, tmp.ContState, "
        + " tmp.SumActuPayMoney, "
        + " 0 ClaimPay, "
        + " 0 ClaimCount, "
        + " nvl(double(Div(tmp.RealPay, tmp.SumActuPayMoney)), 0) ClaimRate, "
        + " 0 UnsettledPay, "
        + " 0 UnsettledPeoples, "
        + " tmp.DeclineAmnt DeclinePay, "
        + " tmp.CustomerNoCount DeclinePeoples "
        + " from "
        + " ( "
        + " select TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, TmpContInfo.ContState, "
        + " TmpContInfo.SumActuPayMoney, "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) RealPay, "
        + " nvl(sum(llcd.DeclineAmnt), 0) DeclineAmnt "
        + " from "
        + " ( "
        + " select lbgc.GrpContNo, lbgc.ProposalGrpContNo, lbgc.PrtNo, "
        + " lbgc.Peoples2, lbgc.CValidate, lbgc.CInValidate, "
        + " CodeName('stateflag', lbgc.StateFlag) ContState, "
        + " sum(ljap.SumActuPayMoney) SumActuPayMoney "
        + " from LBGrpCont lbgc "
        + " inner join LJAPay ljap on ljap.IncomeNo = lbgc.GrpContNo and ljap.IncomeType = '1' "
        + " where 1 = 1 "
        + " and lbgc.AppntNo = '" + mAppntNo + "' "
        + " and lbgc.AppFlag = '1' "
        + " and lbgc.SignDate is not null "
        + " and ljap.ConfDate is not null "
        + " group by lbgc.GrpContNo, lbgc.ProposalGrpContNo, lbgc.PrtNo, "
        + " lbgc.Peoples2, lbgc.CValidate, lbgc.CInValidate, lbgc.StateFlag "
        + " ) as TmpContInfo "
        + " inner join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo and llcd.GiveType in ('2', '3') "
        + " inner join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1') and llc.RgtState in ('11', '12') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ProposalGrpContNo, TmpContInfo.PrtNo, "
        + " TmpContInfo.Peoples2, TmpContInfo.CValidate, TmpContInfo.CInValidate, "
        + " TmpContInfo.ContState, TmpContInfo.SumActuPayMoney "
        + " ) as tmp "
        + " ) as sumInfo "
        + " group by sumInfo.GrpContNo, sumInfo.ProposalGrpContNo, sumInfo.PrtNo, "
        + " sumInfo.Peoples2, sumInfo.CValidate, sumInfo.CInValidate, sumInfo.ContState, "
        + " sumInfo.SumActuPayMoney "
        + " order by GrpContNo "
        + " with ur "
        ;
        
    turnPage1.pageDivName = "divInsuGridPage";
    turnPage1.queryModal(tStrSql, InsuGrid);

    // 判断是否查询成功
    if (!turnPage1.strQueryResult)
    {
        alert("没有既往理赔信息！");
        return false;
    }

    return true;
}

/*********************************************************************
 *  既往询价信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAskApp() 
{
  window.open("../uw/GrpUWAskCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showRejiggerApp()
{
  window.open("../uw/GrpUWRejiggerCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往理陪信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsuApp()
{
  window.open("../uw/GrpUWInsuCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保障信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showEnsureCont()
{
  window.open("../uw/GrpUWEnsureCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往核保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showUWHistoryCont()
{
  window.open("../uw/GrpUWHistoryCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往承保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSignContH()
{
    window.open("../uw/GrpUWHistoryCont.jsp?pmGrpContNo=" + mGrpContNo + "&pmAppntNo=" + mAppntNo + "", "window2");
}



function GoBack()
{
	top.close();
}


function queryClaimDetail()
{
    var tRow = InsuGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = InsuGrid.getRowData(tRow);
    
    var tGrpContNo = tRowDatas[0];
    var tWTFlag = tRowDatas[15];
    
    var tStrSql = ""
        + " select TmpContInfo.ContPlanCode, TmpContInfo.ContPlanName, "
        + " TmpContInfo.RiskCode, TmpContInfo.RiskName, TmpContInfo.Prem, "
        + " TmpContInfo.GetDutyName, nvl(TmpContInfo.PeoplesCount, 0), "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) ClaimPay "
        + " from "
        + " ( "
        + " select distinct lccp.GrpContNo, lccp.ContPlanCode, lccp.ContPlanName, lccpr.RiskCode, "
        + " (select lmr.RiskName from LMRisk lmr where lmr.RiskCode = lccpr.RiskCode) RiskName, "
        + " lmdgr.GetDutyCode, lmdgr.GetDutyName, "
        + " lccp.Peoples2 PeoplesCount, "
        + " (select nvl(sum(lcp.Prem), 0) from LCPol lcp where lcp.GrpContNo = lccp.GrpContNo and lcp.RiskCode = lccpr.RiskCode and lcp.ContPlanCode = lccp.ContPlanCode) Prem "
        + " from LCContPlan lccp "
        + " inner join LCContPlanRisk lccpr on lccp.GrpContNo = lccpr.GrpContNo and lccp.ContPlanCode = lccpr.ContPlanCode"
        + " inner join LCContPlanDutyParam lccpdp on lccpdp.GrpContNo = lccp.GrpContNo and lccpdp.ContPlanCode = lccpr.ContPlanCode and lccpr.RiskCode = lccpdp.RiskCode "
        + " inner join LMDutyGetRela lmdgr on lmdgr.DutyCode = lccpdp.DutyCode "
        + " where 1 = 1 "
        + " and '0' = '" + tWTFlag + "' "
        + " and lccp.GrpContNo = '" + tGrpContNo + "' "
        + " and lccp.ContPlanCode not in ('11') "
        + " ) as TmpContInfo "
        + " left join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo and llcd.GiveType in ('1', '2', '3', '4', '5') and TmpContInfo.GetDutyCode = llcd.GetDutyCode "
        + " left join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1', '4', '5') and llc.RgtState in ('11', '12') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ContPlanCode, TmpContInfo.ContPlanName, "
        + " TmpContInfo.RiskCode, TmpContInfo.RiskName, "
        + " TmpContInfo.GetDutyName, TmpContInfo.PeoplesCount, "
        + " TmpContInfo.Prem "
        + " union all "
        + " select TmpContInfo.ContPlanCode, TmpContInfo.ContPlanName, "
        + " TmpContInfo.RiskCode, TmpContInfo.RiskName, TmpContInfo.Prem, "
        + " TmpContInfo.GetDutyName, nvl(TmpContInfo.PeoplesCount, 0), "
        + " count(distinct llc.CustomerNo) CustomerNoCount, "
        + " nvl(sum(llcd.RealPay), 0) ClaimPay "
        + " from "
        + " ( "
        + " select distinct lbcp.GrpContNo, lbcp.ContPlanCode, lbcp.ContPlanName, lbcpr.RiskCode, "
        + " (select lmr.RiskName from LMRisk lmr where lmr.RiskCode = lbcpr.RiskCode) RiskName, "
        + " lmdgr.GetDutyCode, lmdgr.GetDutyName, "
        + " lbcp.Peoples2 PeoplesCount, "
        + " (select nvl(sum(lbp.Prem), 0) from LBPol lbp where lbp.GrpContNo = lbcp.GrpContNo and lbp.RiskCode = lbcpr.RiskCode and lbp.ContPlanCode = lbcp.ContPlanCode) Prem "
        + " from LBContPlan lbcp "
        + " inner join LBContPlanRisk lbcpr on lbcp.GrpContNo = lbcpr.GrpContNo and lbcp.ContPlanCode = lbcpr.ContPlanCode"
        + " inner join LBContPlanDutyParam lbcpdp on lbcpdp.GrpContNo = lbcp.GrpContNo and lbcpdp.ContPlanCode = lbcpr.ContPlanCode and lbcpr.RiskCode = lbcpdp.RiskCode "
        + " inner join LMDutyGetRela lmdgr on lmdgr.DutyCode = lbcpdp.DutyCode "
        + " where 1 = 1 "
        + " and '1' = '" + tWTFlag + "' "
        + " and lbcp.GrpContNo = '" + tGrpContNo + "' "
        + " and lbcp.ContPlanCode not in ('11') "
        + " ) as TmpContInfo "
        + " left join LLClaimDetail llcd on llcd.GrpContNo = TmpContInfo.GrpContNo and llcd.GiveType in ('1', '2', '3', '4', '5') and TmpContInfo.GetDutyCode = llcd.GetDutyCode "
        + " left join LLCase llc on llc.CaseNo = llcd.CaseNo and llc.RgtType in ('1', '4', '5') and llc.RgtState in ('11', '12') "
        + " where 1 = 1 "
        + " group by TmpContInfo.GrpContNo, TmpContInfo.ContPlanCode, TmpContInfo.ContPlanName, "
        + " TmpContInfo.RiskCode, TmpContInfo.RiskName, "
        + " TmpContInfo.GetDutyName, TmpContInfo.PeoplesCount, "
        + " TmpContInfo.Prem "
        + " order by ContPlanCode, ContPlanName, RiskCode, RiskName, GetDutyName "
        + " with ur "
        ;
        
    turnPage2.pageDivName = "divInsuDetailGridPage";
    turnPage2.queryModal(tStrSql, InsuDetailGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有既往投保明细信息！");
        return false;
    }
    
    return true;    
}