<%
//程序名称：GrpQuestInputInit.jsp
//程序功能：
//创建日期：2005-01-19 14:47:23
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<%
    String GrpContNo = "";
    String BatchNo = "";
    if(request.getParameter("GrpContNo")!=null)
    {
      GrpContNo = request.getParameter("GrpContNo");
    }
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.GrpContNo.value="<%=GrpContNo%>";
  	fm.LoadFlag.value="<%=loadflag%>";
  }  
  catch(ex)
  {
    alert("在GrpQuestInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    if(this.loadflag == "1")
    {
    LoadFlag1.style.display="";
    GrpQuestBack.style.display=""; 
    GrpQuest11.style.display="";        	
    }
    if(this.loadflag == "3")
    {
    LoadFlag2.style.display="";       	
    }
		 if(this.loadflag == "4")
    { 
    	LoadFlag3.style.display="";      	
    }if(this.loadflag == "5"){
    GrpQuestBack.style.display="";     
    }
    initInpBox();
    initGrpQuestGrid();
    initGrpQuestBackGrid();
    initQuest();
   // alert();
    initQuestBack();
    //alert(this.loadflag);


  }
  catch(re)
  {
    alert("GrpQuestInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpQuestGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";              //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";              //列宽
    iArray[0][3]=0;                   //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="编号";              //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="0px";              //列宽
    iArray[1][3]=0;                   //是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="数据项";      //列名
    iArray[2][1]="133px";                //列宽
    iArray[2][3]=2;                   //是否允许输入,1表示允许，0表示不允许
    iArray[2][4]="lcgrpissue";
    //iArray[2][12]="1|2";                                      
    //iArray[2][13]="0|1"; 
    iArray[2][5]="1|2";
    iArray[2][6]="0|1";

    iArray[3]=new Array();
    iArray[3][0]="错误编码";          //列名
    iArray[3][1]="55px";              //列宽
    iArray[3][3]=2;               //是否允许输入,1表示允许，0表示不允许
    iArray[3][10] = "IssueDescription";                       
    iArray[3][11] = "0|^A|未填^B|填写错误^C|规则错误";        
    iArray[3][12]="3|4";                                      
    iArray[3][13]="0|1";                                      
    iArray[3][19]="1";        

    iArray[4]=new Array();
    iArray[4][0]="错误类型";          //列名
    iArray[4][1]="55px";              //列宽
    iArray[4][3]=1;               //是否允许输入,1表示允许，0表示不允许
                                

		//2006-03-01 闫少杰 增加规则编码的处理
    iArray[5]=new Array();
    iArray[5][0]="规则编码";          //列名
    iArray[5][1]="0px";              //列宽
    iArray[5][3]=3;               		//是否允许输入,1表示允许，0表示不允许
    iArray[5][4]="IssueRuleCode";			//规则编码
    iArray[5][5]="3|5|6|7|8";
    iArray[5][6]="0|1|2|3|4";
    iArray[5][15]="编号"							//该字段的查询依赖第一列的值
    iArray[5][17]="1";								//该字段的查询依赖第一列的值
    iArray[5][19]="1";								//强制刷新数据


    iArray[6]=new Array();
    iArray[6][0]="下发对象";          //列名
    iArray[6][1]="75px";              //列宽
    iArray[6][3]=2;               //是否允许输入,1表示允许，0表示不允许
    iArray[6][10] = "OperateLocation";
    iArray[6][11] = "0|^0|投保人^1|操作员";
    iArray[6][12]="6|7";
    iArray[6][13]="1|0";
   // iArray[6][19]="1";

    iArray[7]=new Array();                                             
    iArray[7][0]="下发对象名称";          //列名                           
    iArray[7][1]="0px";              //列宽                            
    iArray[7][3]=1;               //是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="问题件内容";          //列名
    iArray[8][1]="395px";             //列宽
    iArray[8][3]=1;               //是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="打印标记";          //列名
    iArray[9][1]="50px";             //列宽
    iArray[9][3]=2;               //是否允许输入,1表示允许，0表示不允许\
//    iArray[9][9] = "打印标记|NOTNULL";
    iArray[9][10] = "yesno";
    iArray[9][11] = "0|^Y|是^N|否";
    //iArray[9][12]="9|12";
    //iArray[9][13]="0|1";
    
    iArray[10]=new Array();
    iArray[10][0]="团体合同号";           //列名
    iArray[10][1]="80px";             //列宽
    iArray[10][3]=3;                //是否允许输入,1表示允许，0表示不允许

    iArray[11]=new Array();
    iArray[11][0]="流水号";           //列名
    iArray[11][1]="80px";             //列宽
    iArray[11][3]=3;                //是否允许输入,1表示允许，0表示不允许

    iArray[12]=new Array();
    iArray[12][0]="所属状态";           //列名
    iArray[12][1]="80px";             //列宽
    iArray[12][3]=3;                //是否允许输入,1表示允许，0表示不允许
    
    iArray[13]=new Array();                                              
    iArray[13][0]="打印批次号";           //列名                           
    iArray[13][1]="80px";             //列宽                             
    iArray[13][3]=3;                //是否允许输入,1表示允许，0表示不允许

    iArray[14]=new Array();                                              
    iArray[14][0]="操作员";           //列名                             
    iArray[14][1]="60px";             //列宽                             
    iArray[14][3]=0;                //是否允许输入,1表示允许，0表示不允许
    
    GrpQuestGrid = new MulLineEnter( "fm" , "GrpQuestGrid" );
    //这些属性必须在loadMulLine前
    GrpQuestGrid.mulLineCount = 0;
    GrpQuestGrid.displayTitle = 1;
    GrpQuestGrid.hiddenPlus = 0;
    GrpQuestGrid.hiddenSubtraction = 0;
    GrpQuestGrid.canChk = 0;
    GrpQuestGrid.canSel = 1;
    GrpQuestGrid.addEventFuncName="Addline";
    //GrpQuestGrid.delEventFuncName="Delline";
    //GrpQuestGrid.selBoxEventFuncName ="getdetail";
    GrpQuestGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
function initGrpQuestBackGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";              //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";              //列宽
    iArray[0][3]=0;                   //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="编号";              //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="0px";              //列宽
    iArray[1][3]=0;                   //是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="数据项";      //列名
    iArray[2][1]="133px";                //列宽
    iArray[2][3]=0;                   //是否允许输入,1表示允许，0表示不允许
 

    iArray[3]=new Array();
    iArray[3][0]="错误编码";          //列名
    iArray[3][1]="55px";              //列宽
    iArray[3][3]=0;               //是否允许输入,1表示允许，0表示不允许


    iArray[4]=new Array();
    iArray[4][0]="错误类型";          //列名
    iArray[4][1]="55px";              //列宽
    iArray[4][3]=1;               //是否允许输入,1表示允许，0表示不允许
                                

		//2006-03-01 闫少杰 增加规则编码的处理
    iArray[5]=new Array();
    iArray[5][0]="规则编码";          //列名
    iArray[5][1]="0px";              //列宽
    iArray[5][3]=3;               		//是否允许输入,1表示允许，0表示不允许


    iArray[6]=new Array();
    iArray[6][0]="下发对象";          //列名
    iArray[6][1]="75px";              //列宽
    iArray[6][3]=0;               //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="下发对象名称";          //列名
    iArray[7][1]="0px";              //列宽
    iArray[7][3]=0;               //是否允许输入,1表示允许，0表示不允许



    iArray[8]=new Array();
    iArray[8][0]="问题件内容";          //列名
    iArray[8][1]="395px";             //列宽
    iArray[8][3]=1;               //是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="回销标记";          //列名
    iArray[9][1]="50px";             //列宽
    iArray[9][3]=0;               //是否允许输入,1表示允许，0表示不允许

    
    iArray[10]=new Array();
    iArray[10][0]="团体合同号";           //列名
    iArray[10][1]="80px";             //列宽
    iArray[10][3]=3;                //是否允许输入,1表示允许，0表示不允许

    iArray[11]=new Array();
    iArray[11][0]="流水号";           //列名
    iArray[11][1]="80px";             //列宽
    iArray[11][3]=3;                //是否允许输入,1表示允许，0表示不允许

    iArray[12]=new Array();
    iArray[12][0]="打印批单号";           //列名
    iArray[12][1]="80px";             //列宽
    iArray[12][3]=3;                //是否允许输入,1表示允许，0表示不允许

    iArray[13]=new Array();
    iArray[13][0]="回销员";           //列名
    iArray[13][1]="60px";             //列宽
    iArray[13][3]=0;                //是否允许输入,1表示允许，0表示不允许
    
    iArray[14]=new Array();
    iArray[14][0]="操作员";           //列名
    iArray[14][1]="60px";             //列宽
    iArray[14][3]=0;                //是否允许输入,1表示允许，0表示不允许
    
    GrpQuestBackGrid = new MulLineEnter( "fm" , "GrpQuestBackGrid" );
    //这些属性必须在loadMulLine前
    GrpQuestBackGrid.mulLineCount = 1;
    GrpQuestBackGrid.displayTitle = 1;
    GrpQuestBackGrid.hiddenPlus = 1;
    GrpQuestBackGrid.hiddenSubtraction = 1;
    GrpQuestBackGrid.canChk = 0;
    GrpQuestBackGrid.canSel = 1;
    //GrpQuestGrid.selBoxEventFuncName ="getdetail";
    GrpQuestBackGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>
