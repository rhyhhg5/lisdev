<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalContInfoModify.jsp
//程序功能：新契约保单信息修改
//创建日期：2008-6-12
//创建人  ：zhangjianbao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    GlobalInput tG = new GlobalInput();

	tG=(GlobalInput)session.getValue("GI");
	System.out.println("operator:"+tG.Operator);
	System.out.println("ManageCom:"+tG.ManageCom);
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
	TransferData tTransferData = new TransferData();
	String tPrtNo      = request.getParameter("PrtNo1");
	String tContNo     = request.getParameter("ContNo");
	String tSaleChnl   = request.getParameter("SaleChnl");
	String tAgentCom   = request.getParameter("AgentCom");
	String tAgentCode  = request.getParameter("AgentCode");
	String tAgentGroup = request.getParameter("AgentGroup");

	boolean flag = true;
	if (!tContNo.equals(""))
	{
		//准备公共传输信息
		tTransferData.setNameAndValue("PrtNo",tPrtNo);
		tTransferData.setNameAndValue("ContNo",tContNo);
		tTransferData.setNameAndValue("SaleChnl",tSaleChnl);
		tTransferData.setNameAndValue("AgentCom",tAgentCom);
		tTransferData.setNameAndValue("AgentCode",tAgentCode);
		tTransferData.setNameAndValue("AgentGroup",tAgentGroup);
		System.out.println("Contno:"+tContNo);
	}
	else
	{
		flag = false;
		Content = "数据不完整!";
	}
	System.out.println("flag:"+flag);
    try
    {
  	    if (flag == true)
  	    {
		    // 准备传输数据 VData
		    VData tVData = new VData();
		    tVData.add( tTransferData);
		    tVData.add( tG );

		    // 数据传输
		    ModifyContInfo tModifyContInfo = new ModifyContInfo();

		    if (!tModifyContInfo.submitData(tVData, request.getParameter("operate")))
		    {
			    Content = " 新契约修改保单信息失败，原因是: " + tModifyContInfo.mErrors.getError(0).errorMessage;
			    FlagStr = "Fail";
		    }
		    //如果在Catch中发现异常，则不从错误类中提取错误信息
		    if (FlagStr == "Fail")
		    {
		        tError = tModifyContInfo.mErrors;
		        if (!tError.needDealError())
		        {
		    	    Content = " 新契约修改保单信息成功! ";
		    	    FlagStr = "Succ";
		        }
		        else
		        {
		    	    Content = " 新契约修改保单信失败，原因是:" + tError.getError(0).errorMessage;
		    	    FlagStr = "Fail";
		        }
		    }
	    }
    }
    catch(Exception e)
    {
    	e.printStackTrace();
    	Content = Content.trim()+".提示：异常终止!";
    }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
