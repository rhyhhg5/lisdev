<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
//程序名称：GrpUWInsuCont.jsp
//程序功能：既往理赔查询
//创建日期：2005-04-26
//创建人  ： LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String tAppntNo = request.getParameter("pmAppntNo");    
    String tGrpContNo = request.getParameter("pmGrpContNo");
%>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <script src="../common/javascript/Common.js" ></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
  
    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">
    
    <title>既往理赔信息</title>
  
    <script src="GrpUWInsuCont.js"></script>
    <%@include file="GrpUWInsuContInit.jsp"%>
  
</head>

<body onload="initForm();">
<form method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCont);">
    		</td>
    		<td class= titleImg>
    			团体基本信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrpCont" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title8>
            团体客户号码:
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly="readonly" name="GrpNo" />
          </TD>
          <TD  class= title8>
            团体名称:
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly="readonly" name="GrpName" />
          </TD>
          <!--TD  class= title8>
            团体保障号:
          </TD>
          <TD  class= input8-->
            <Input class="readonly" readonly="readonly" name=GrpPolNo type="hidden" />
          <!--/TD-->
        </TR>
    </table>
    </DIV>
    
    <br />

    <table>
        <tr>
            <td class="common">
                <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divInsu);" />
            </td>
            <td class="titleImg">既往理赔信息</td>
        </tr>
    </table>
    <div id="divInsu" style="display: ''" align="center">
        <table class="common">
            <tr class="common">
                <td>
                    <span id="spanInsuGrid"></span> 
                </td>
            </tr>
        </table>
        
        <div id="divInsuGridPage" align="center" style="display:'none'">
            <input class="cssbutton" type="button" value="首  页" onclick="turnPage1.firstPage();" /> 
            <input class="cssbutton" type="button" value="上一页" onclick="turnPage1.previousPage();" />                    
            <input class="cssbutton" type="button" value="下一页" onclick="turnPage1.nextPage();" /> 
            <input class="cssbutton" type="button" value="尾  页" onclick="turnPage1.lastPage();" />
        </div>
    </div>
    
    <br />

    <table>
        <tr>
            <td class=common>
                <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick= "showPage(this, divInsuDetail);" />
            </td>
            <td class= titleImg>既往理赔详细信息</td>
        </tr>
    </table>    
    <div id="divInsu" style="display: ''" align="center">
        <table class="common">
            <tr class="common">
                <td>
                    <span id="spanInsuDetailGrid"></span> 
                </td>
            </tr>
        </table>
        
        <div id="divInsuDetailGridPage" align="center" style="display:'none'">
            <input class="cssbutton" type="button" value="首  页" onclick="turnPage2.firstPage();" /> 
            <input class="cssbutton" type="button" value="上一页" onclick="turnPage2.previousPage();" />                    
            <input class="cssbutton" type="button" value="下一页" onclick="turnPage2.nextPage();" /> 
            <input class="cssbutton" type="button" value="尾  页" onclick="turnPage2.lastPage();" />
        </div>
    </div>
    
    <br />
    
    <!-- 既往理赔部分（分析图） -->
    <div id="h" style= "display: 'none'">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpPic);">
    		</td>
    		<td class= titleImg>
    			 既往理赔分析图
    		</td>
    	</tr>
    </table>
	  <Div  id= "divLPGrpPic" style= "display: ''" align = center>
	  </Div>
	</div>
    <HR>
      <!-- <INPUT VALUE = "既往询价信息" Class="cssButton" TYPE=button onclick= "showAskApp();"> -->
      <input value="既往承保信息" class="cssButton" type="button" onclick="showSignContH();" />
      <INPUT VALUE = "既往投保信息" Class="cssButton"  TYPE=button onclick= "showEnsureCont();">
      <INPUT VALUE = "既往保全信息" Class="cssButton" TYPE=button onclick= "showRejiggerApp();">
      <INPUT VALUE = "既往理赔信息" Class="cssButton" TYPE=button onclick= "showInsuApp();">
    <HR>
	  
  	<div id = "divUWAgree" style = "display: ''">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="GoBack();">
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>