//程序名称：MasterCenter.js
//程序功能：管理中心
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var cflag = "4";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function initQuery() {
	// 初始化表格
	//initPolGrid();
	 
	// 书写SQL语句
	/*
	var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName from LCPol where " + ++queryBug + "=" + queryBug
    				 + " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pi')=1 "
    				 + " and AppFlag='0' "                 //个人保单
    				 + " and GrpPolNo='" + grpPolNo + "'"  //集体单号，必须为20个0
    				 + " and ContNo='" + contNo + "'"      //内容号，必须为20个0
    				 + " and ManageCom like '" + comcode + "%%'"
    				 + " and uwflag <> '0'"
    				 //+ " and uwflag = '7'"
    				 + " and approveflag = '2'"
    				 //+ " and polno in (select proposalno from lcuwmaster where printflag in ('1','2'))"
    				 + " and PolNo in (select distinct proposalno from lcissuepol where ReplyResult is null and backobjtype = '4')";
  */
  var	strSql = "select LWMission.MissionProp1,LWMission.MissionProp2,LWMission.MissionProp3,LWMission.MissionID,LWMission.SubMissionID from LWMission where 1=1"
				 + " and LWMission.ActivityID = '0000001020'"
				 + " and LWMission.ProcessID = '0000000003'"
  //alert(strSql);
	turnPage.queryModal(strSql, PolGrid);
}

var mSwitch = parent.VD.gVSwitch;
function modifyClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cPolNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cPolNo );
  	
    urlStr = "./ProposalMain.jsp?LoadFlag=3";
    window.open(urlStr);
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

function QuestQuery()
{
	  var i = 0;
	  var checkFlag = 0;
	  var cContNo = "";
	  var cMissionID = "";
	  var cSubMissionID = "";
	  
	  for (i=0; i<PolGrid.mulLineCount; i++) {
	    if (PolGrid.getSelNo(i)) { 
	      checkFlag = PolGrid.getSelNo();
	      break;
	    }
	  }
	  
	  if (checkFlag > 0) { 
	  	cContNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
	  	cMissionID = PolGrid.getRowColData(checkFlag - 1, 4); 	
	  	cSubMissionID = PolGrid.getRowColData(checkFlag - 1, 5); 	
	  	window.open("../uw/QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+cflag+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");
	  }
	  else {
	    alert("请先选择一条保单信息！"); 	    
	  }
}


