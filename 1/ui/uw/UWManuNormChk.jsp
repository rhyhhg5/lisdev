<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  //程序名称：UWManuNormChk.jsp
  //程序功能：人工核保最终结论录入保存
  //创建日期：2002-06-19 11:10:36
  //创建人  ：WHN
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = null;
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  if (tG == null) {
    out.println("session has expired");
    return;
  }
  // 投保单列表
  LCContSet tLCContSet = new LCContSet();
  LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
  LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
  String tUWFlag = request.getParameter("uwState");
  String tUWIdea = request.getParameter("UWIdea");
  //String tvalidate = request.getParameter("UWDelay");
  String tMissionID = request.getParameter("MissionID");
  String tActivityID = request.getParameter("ActivityID");
  String tPrtNo = request.getParameter("PrtNo");
  String tSubMissionID = request.getParameter("SubConfirmMissionID");
  String tSendFlag = request.getParameter("SendFlag");
  String tYesOrNo = request.getParameter("YesOrNo");
  String tPSendUpFlag = request.getParameter("PSendUpFlag");
  String tSendType = request.getParameter("SendType");
  String tContNo = "";
  tContNo = request.getParameter("ContNo");
  System.out.println("UWIDEA:" + tUWIdea);
  System.out.println("ContNo:" + tContNo);
  System.out.println("uwflag+" + tUWFlag);
  System.out.println("SendType+" + tSendType);
  System.out.println("PSendUpFlag+" + tPSendUpFlag);
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("ContNo", tContNo);
  tTransferData.setNameAndValue("PrtNo", tPrtNo);
  tTransferData.setNameAndValue("UWFlag", tUWFlag);
  tTransferData.setNameAndValue("UWIdea", tUWIdea);
 
  tTransferData.setNameAndValue("NeedSendNotice", request.getParameter("NeedSendNotice"));
  tTransferData.setNameAndValue("ActivityID", tActivityID);
  tTransferData.setNameAndValue("PSendUpFlag", tPSendUpFlag);
  tTransferData.setNameAndValue("SendType", tSendType);
  boolean flag = false;
  if (!tContNo.equals("")) {
    LCContSchema tLCContSchema = new LCContSchema();
    LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
  //	    tLCContSchema.setPolNo( tContNo);
    tLCContSchema.setProposalContNo(tContNo);
    tLCContSchema.setUWFlag(tUWFlag);
    tLCContSchema.setRemark(tUWIdea);
    tLCCUWMasterSchema.setProposalContNo(tContNo);
    //tLCCUWMasterSchema.setPostponeDay(tvalidate);
    tLCCUWMasterSchema.setUWIdea(tUWIdea);
    tLCCUWMasterSchema.setSugPassFlag(tUWFlag);
    tLCCUWMasterSchema.setSugUWIdea(tUWIdea);
    tLCContSet.add(tLCContSchema);
    tLCCUWMasterSet.add(tLCCUWMasterSchema);
    tTransferData.setNameAndValue("LCCUWMasterSchema", tLCCUWMasterSchema);
    flag = true;
  }
  else {
    FlagStr = "Fail";
    Content = "号码传输失败!";
  }
  try {
	   //by gzh MissionID可能为空
	  if(tMissionID == null || "".equals(tMissionID)){
	  		String tSQL="select missionid from lwmission where MissionProp1 = '"+tPrtNo+"' ";
	  		tMissionID = new ExeSQL().getOneValue(tSQL);
	  }
	  //tMissionID = "";
	  if("".equals(tMissionID)){
			//System.out.println("MissionID为空");
			LDCodeSchema tLDCodeSchema = new LDCodeSchema();
			tLDCodeSchema.setCodeType("missionidnull");
			tLDCodeSchema.setCode(tPrtNo);
			tLDCodeSchema.setCodeName("MissionID");
			tLDCodeSchema.setCodeAlias(PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			tLDCodeSchema.setComCode(tG.ManageCom);
			tLDCodeSchema.setOtherSign("");
			
			Exception ex =new Exception("人工核保时，MissionID获取失败！时间："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			MMap aMMap = new MMap();
			aMMap.put(tLDCodeSchema,SysConst.DELETE_AND_INSERT);
			VData data = new VData();
        	data.add(aMMap);
	        PubSubmit p = new PubSubmit();
	        if (!p.submitData(data, ""))
	        {
	            System.out.println("提交数据失败");
	            throw 	ex;	
	        }
			PubFun.sendMessage("13811995848","保单[印刷号"+tPrtNo+"]的MissionID为空！时间:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			PubFun.sendMessage("13581948194","保单[印刷号"+tPrtNo+"]的MissionID为空！时间:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			PubFun.sendMessage("15810663099","保单[印刷号"+tPrtNo+"]的MissionID为空！时间:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
	  		
		    throw 	ex;	
	  }
	  else  if(tSubMissionID == null || "".equals(tSubMissionID)){
	  		 String strsql = "select submissionid from lwmission where activityid = '0000001110' and missionid = '"+tMissionID+"'";
	         tSubMissionID = new ExeSQL().getOneValue(strsql);
	  }
	  //tSubMissionID = "";
	  if("".equals(tSubMissionID)){
	  
	  		LDCodeSchema tLDCodeSchema = new LDCodeSchema();
			tLDCodeSchema.setCodeType("missionidnull");
			tLDCodeSchema.setCode(tPrtNo);
			tLDCodeSchema.setCodeName("SubMissionID");
			tLDCodeSchema.setCodeAlias(PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			tLDCodeSchema.setComCode(tG.ManageCom);
			tLDCodeSchema.setOtherSign("");
			
			Exception ex =new Exception("人工核保时，SubMissionID获取失败！时间："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			MMap aMMap = new MMap();
			aMMap.put(tLDCodeSchema,SysConst.DELETE_AND_INSERT);
			VData data = new VData();
        	data.add(aMMap);
	        PubSubmit p = new PubSubmit();
	        if (!p.submitData(data, ""))
	        {
	            System.out.println("提交数据失败");
	            throw 	ex;	
	        }
	  		//System.out.println("SubMissionID为空,时间："++PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
	  		PubFun.sendMessage("13811995848","保单[印刷号"+tPrtNo+"]的SubMissionID为空！时间:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			PubFun.sendMessage("13581948194","保单[印刷号"+tPrtNo+"]的SubMissionID为空！时间:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
			PubFun.sendMessage("15810663099","保单[印刷号"+tPrtNo+"]的SubMissionID为空！时间:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
	        throw 	ex;
	  }
	   tTransferData.setNameAndValue("MissionID", tMissionID);
  	   tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
	  //by gzh end
	  
  
    if (flag == true) {
      // 准备传输数据 VData
      VData tVData = new VData();
      tVData.add(tLCContSet);
      tVData.add(tLCCUWMasterSet);
      tVData.add(tTransferData);
      tVData.add(tG);
      String tUpUserCode = request.getParameter("UWPopedomCode");
      tLCUWSendTraceSchema.setOtherNo(tContNo);
      tLCUWSendTraceSchema.setOtherNoType("1"); //个险
      tLCUWSendTraceSchema.setUWFlag(tUWFlag);
      tLCUWSendTraceSchema.setUWIdea(tUWIdea);
      tLCUWSendTraceSchema.setYesOrNo(tYesOrNo);
      tLCUWSendTraceSchema.setSendType(tSendType);
      tTransferData.setNameAndValue("LCUWSendTraceSchema", tLCUWSendTraceSchema);
      tVData.add(tTransferData);
      UWSendTraceAllUI tUWSendTraceAllUI = new UWSendTraceAllUI();
      if (tUWSendTraceAllUI.submitData(tVData, "submit") == false) {
        int n = tUWSendTraceAllUI.mErrors.getErrorCount();
        for (int i = 0; i < n; i++)
          Content = "核保失败，原因是: " + tUWSendTraceAllUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
      }
      else {
        FlagStr = "Succ";
        int m = tUWSendTraceAllUI.mErrors.getErrorCount();
        if (m > 0) {
          Content = "核保失败，原因是: " + tUWSendTraceAllUI.mErrors.getError(0).errorMessage;
          FlagStr = "Fail";
        }
        if (FlagStr.equals("Succ")) {
          Content = " 上报核保成功！ ";
          String tUWSendFlag;
          String tUserCode;
          TransferData mTransferData = new TransferData();
          mTransferData = (TransferData) tUWSendTraceAllUI.getResult().getObjectByObjectName("TransferData", 0);
          tUWSendFlag = (String) mTransferData.getValueByName("SendFlag");
          tUserCode = (String) mTransferData.getValueByName("UserCode");
          tSendType = (String) mTransferData.getValueByName("SendType");
          System.out.println("tUWSendFlag=========" + tUWSendFlag);
          if (tUWSendFlag.equals("9")) { //核保下报
            Content = "核保已下发给核保师：" + tUserCode;
          }
          else if (tUWSendFlag.equals("1") && tSendType.equals("1")) {
            Content = "核保已上报给核保师：" + tUserCode;
          }
          else if (tUWSendFlag.equals("1") && tSendType.equals("2")) {
            Content = "人工核保操作成功！";
          }
          if (tUWSendFlag.equals("0") || (tUWSendFlag.equals("0") && tYesOrNo.equals("Y"))) {
            // 数据传输
            if (tUWFlag.equals("b")) {
              UWManuChgPlanConclusionChkUI tUWManuChgPlanConclusionChkUI = new UWManuChgPlanConclusionChkUI();
              if (tUWManuChgPlanConclusionChkUI.submitData(tVData, "") == false) {
                int n = tUWManuChgPlanConclusionChkUI.mErrors.getErrorCount();
                for (int i = 0; i < n; i++)
                  Content = " 承保计划变更原因录入失败，原因是: " + tUWManuChgPlanConclusionChkUI.mErrors.getError(0).errorMessage;
                FlagStr = "Fail";
              }
              else {
                Content = " 承保计划变更成功！";
                FlagStr = "Succ";
              }
            }
            else if (tUWFlag.equals("a") || tUWFlag.equals("1") || tUWFlag.equals("4") || tUWFlag.equals("8") || tUWFlag.equals("9")) {
              FlagStr = "Fail";
              TransferData nTransferData = new TransferData();
              nTransferData.setNameAndValue("ContNo", tContNo);
              nTransferData.setNameAndValue("PrtNo", tPrtNo);
              nTransferData.setNameAndValue("UWFlag", tUWFlag);
              nTransferData.setNameAndValue("UWIdea", tUWIdea);
              nTransferData.setNameAndValue("MissionID", tMissionID);
              System.out.println(request.getParameter("NeedSendNotice"));
              nTransferData.setNameAndValue("NeedSendNotice", request.getParameter("NeedSendNotice"));
              nTransferData.setNameAndValue("SubMissionID", tSubMissionID);
              tVData.clear();
              tVData.add(tLCContSet);
              tVData.add(nTransferData);
              tVData.add(tLCCUWMasterSet);
              tVData.add(tG);
              TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
              if (tTbWorkFlowUI.submitData(tVData, "0000001110") == false) {
                int n = tTbWorkFlowUI.mErrors.getErrorCount();
                for (int i = 0; i < n; i++)
                  Content = " 人工核保失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
                FlagStr = "Fail";
              }
              //如果在Catch中发现异常，则不从错误类中提取错误信息
              if (FlagStr == "Fail") {
                tError = tTbWorkFlowUI.mErrors;
                if (!tError.needDealError()) {
                  Content = " 人工核保操作成功!";
                  FlagStr = "Succ";
                }
                else {
                  FlagStr = "Fail";
                }
              }
            }
          }
          //人核成功，pad出单实时收费
//           if(FlagStr=="Succ"){
//               tVData.clear();
//               TransferData pTransferData = new TransferData();
//               pTransferData.setNameAndValue("ContNo", tContNo);
//               pTransferData.setNameAndValue("PrtNo", tPrtNo);
//               pTransferData.setNameAndValue("MissionID", tMissionID);
//               pTransferData.setNameAndValue("SubMissionID", tSubMissionID);
//               tVData.add(pTransferData);
//               tVData.add(tG);
//               UWPadFeeUI tUWPadFeeUI = new UWPadFeeUI();
//               tUWPadFeeUI.submitData(tVData, "Fee");
//           }
        }
      }
      System.out.println("flag=" + FlagStr);
      System.out.println("Content=" + Content);
    }
  }
  catch (Exception e) {
    e.printStackTrace();
    Content = Content.trim() + " 提示：异常终止!";
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit3("<%=FlagStr%>","<%=Content%>");	
</script>
</html>
