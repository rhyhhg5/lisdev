<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FeeRateSave.jsp
//程序功能：
//创建日期：2005-12-12 21:34
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
//接收信息，并作校验处理。
//输入参数
LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
FeeRateUI tFeeRateUI   = new FeeRateUI();
//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String tNum[] = request.getParameterValues("FeeRateGridNo");
String tRiskCode[] = request.getParameterValues("FeeRateGrid1");            
String tDisCount[] = request.getParameterValues("FeeRateGrid3");          

String tGrpContNo = request.getParameter("GrpContNo");
//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = "UPDATE||FEERATE";
int tGridCount = 0;
if(tNum!=null)
 tGridCount = tNum.length;
for(int n=0 ; n<tGridCount ; n++){
	LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
	tLCGrpPolSchema.setGrpContNo(tGrpContNo);
	tLCGrpPolSchema.setRiskCode(tRiskCode[n]);
	System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&:tDisCount[n]"+tDisCount[n]);
	tLCGrpPolSchema.setStandbyFlag3(tDisCount[n]);
	
	tLCGrpPolSet.add(tLCGrpPolSchema);
}
try
{
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tLCGrpPolSet);
  tVData.add(tG);
  tFeeRateUI.submitData(tVData,transact);
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
  tError = tFeeRateUI.mErrors;
  if (!tError.needDealError())
  {
    Content = " 保存成功! ";
    FlagStr = "Success";
  }
  else
  {
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

//添加各种预处理
%>
<%=Content%>
   <html>
   <script language="javascript">
                    parent.fraInterface.afterSubmit1("<%=FlagStr%>","<%=Content%>");
</script>
</html>
