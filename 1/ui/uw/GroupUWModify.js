//程序名称：GroupUWAuto.js
//程序功能：集体自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

//var showInfo = "";
var mDebug="0";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保

//自动核保提交，
function submitForm()
{
	var tSel = GrpGrid.getSelNo();
	var cGrpPolNo = "";
	var cGrpPrtNo = "";
	if( tSel != null && tSel != 0 )
	{
		cGrpPolNo = GrpGrid.getRowColData( tSel - 1, 1 );
	  cGrpPrtNo = GrpGrid.getRowColData( tSel - 1, 2 );
    }
	
	if( cGrpPolNo == null || cGrpPolNo == ""|| cGrpPrtNo == null || cGrpPrtNo == "" )
		alert("请选择一张集体投保单后，再进行自动核保操作");
	else
	{
		window.open( "./GroupUWAutoDetailMain.jsp?ProposalGrpContNo=" + cGrpPolNo + "&PrtNo=" + cGrpPrtNo );
	}
  
}

function SetSpecialFlag()
{
    var tSel = GrpGrid.getSelNo();
	var cGrpPolNo = "";
	var cGrpPrtNo = "";
	if( tSel != null && tSel != 0 )
	{
		cGrpPolNo = GrpGrid.getRowColData( tSel - 1, 1 );
	    cGrpPrtNo = GrpGrid.getRowColData( tSel - 1, 2 );
    }
	
	if( cGrpPolNo == null || cGrpPolNo == ""|| cGrpPrtNo == null || cGrpPrtNo == "" )
		alert("请选择一张集体投保单后，再进行设置特殊标志操作");
	else
	{
		window.open( "./GroupUWSetSpecialFlag.jsp?GrpProposalNo=" + cGrpPolNo + "&GrpPrtNo=" + cGrpPrtNo );
	}
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
	
  parent.fraMain.rows = "0,0,0,0,*";
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}

function easyQueryClick()
{
	// 初始化表格
	
	initGrpGrid();
	if(!verifyInput2())
	return false;
	
	// 书写SQL语句
	var strSQL = "";

		strSQL = "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop7,lwmission.missionprop4,getUniteCode(lwmission.missionprop5),lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
				 + " and activityid = '0000002005' "
				 + " and processid = '0000000004'"
				 + getWherePart('missionprop1','ProposalGrpContNo')
				 + getWherePart('getUniteCode(missionprop5)','AgentCode')
				 + getWherePart('missionprop4','ManageCom','like')
				 + getWherePart('missionprop6','AgentGroup')
				 + getWherePart('missionprop7','Name')
				 + getWherePart('missionprop9','GrpNo')				 
				 + "order by lwmission.missionprop2"
				 ;	 
			
      //alert(strSQL);
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有需要自动核保集体单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function GrpUWModify(){
	  var i = 0;
	  var checkFlag = 0;
	  var cGrpProposalNo = "";
	  var cGrpPrtNo="";
	  var cflag = "2";
	  
	  for (i=0; i<GrpGrid.mulLineCount; i++) {
	    if (GrpGrid.getSelNo(i)) { 
	      checkFlag = GrpGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpGrid.getRowColData(checkFlag - 1, 1);
	  	cGrpPrtNo = GrpGrid.getRowColData(checkFlag - 1, 2);
	  	fm.GrpContNo.value = cGrpProposalNo;
	  	fm.MissionID.value = GrpGrid.getRowColData(checkFlag - 1, 6);
	  	fm.SubMissionID.value = GrpGrid.getRowColData(checkFlag - 1, 7);
	  }
	  else {
	    alert("请先选择一条有效的保单信息！"); 
	    return false;
	  }
	  if( cGrpPrtNo == null || trim(cGrpPrtNo)=='' || cGrpProposalNo == null || trim(cGrpProposalNo)== '' )
	  {
	    alert("请先选择一条有效的保单信息！"); 
	    return false;
	  }
	  if(fm.tUWIdea.value==""){
	  	alert("请录入核保订正意见");
	  	return false;
	  	}
	  var exeSQL="select * from lcgcuwmaster where PROPOSALGRPCONTNO ='"+trim(cGrpProposalNo)+"' and operator='"+operator+"'";
	  var arrResult = easyExecSql(exeSQL);
	  if (!arrResult) {
	  	alert("您不是最后的核保人，不能进行核保订正操作！");
      return;
    }
    
    // by gzh 
      var tRICSQL="select 1 from lwmission where missionprop2 = '"+cGrpPrtNo+"' and activityid = '0000002011'";
	  var RICResult = easyExecSql(tRICSQL);
	  if (RICResult) {
	  	alert("该保单正在进行再保审核，不能进行核保订正！");
      	return;
      }
      
      var tRISQL="select 1 from lwmission where missionprop2 = '"+cGrpPrtNo+"' and activityid = '0000002012'";
	  var RIResult = easyExecSql(tRISQL);
	  if (RIResult) {
	  	alert("该保单正在进行总公司核保审核，不能进行核保订正！");
      	return;
      }
	  
	//如果银行在途不允许核保订正
	  var sql = "select cansendbank from ljspay where otherno ='"+cGrpPrtNo+"'";
	  var sql2 = "select paymode from lcgrpcont where prtno ='"+cGrpPrtNo+"'";
	  var cansendbank = easyExecSql(sql);
	  var paymode = easyExecSql(sql2);
	  if (paymode == 4) {
		  if (cansendbank != "1" || cansendbank == "") {
			 alert("缴费方式为转账银行的保单，只有在银行锁定状态下才可以核保订正！");
			 return false;
		}
	  }
	  
	  if(!confirm("该单已下发缴费通知书，请先通知财务暂停到帐处理，继续操作吗？")){
        return;
	  	}
	 fm.ProposalGrpContNo.value = cGrpProposalNo ;
//	 fm.PrtNo.value = cGrpPrtNo ;
	 fm.submit();
	 
}

/*********************************************************************
 *  集体投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	//showInfo.close();
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	if( FlagStr == "Fail" )
	{             
		
		showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		// 刷新查询结果
		showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		easyQueryClick_noAlert();
	}
}


function easyQueryClick_noAlert()
{
	// 初始化表格
	
	initGrpGrid();

	// 书写SQL语句
	var strSQL = "";

		strSQL = "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop7,lwmission.missionprop4,getUniteCode(lwmission.missionprop5),lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
				 + " and activityid = '0000002005' "
				 + " and processid = '0000000004'"
				 + getWherePart('missionprop1','ProposalGrpContNo')
				 + getWherePart('getUniteCode(missionprop5)','AgentCode')
				 + getWherePart('missionprop4','ManageCom')
				 + getWherePart('missionprop6','AgentGroup')
				 + getWherePart('missionprop9','GrpNo')
				 + "order by lwmission.missionprop2"
				 ;	 
			
      //alert(strSQL);
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("没有需要自动核保集体单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup2"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}
