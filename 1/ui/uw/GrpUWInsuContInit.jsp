<%
//程序名称：GrpUWInsuAppInit.jsp
//程序功能：
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script src="../common/javascript/Common.js"></script>

<script language="JavaScript">
var mGrpContNo = "<%=tGrpContNo%>";
var mAppntNo = "<%=tAppntNo%>";


//画面初始化
function initForm()
{
  try
  {
		initInsuGrid();
		initInsuDetailGrid();
		easyQueryI();
		easyQuery();
  }
  catch(re)
  {
    alert("程序名称：GrpUWInsuInit.jsp-->InitForm1函数中发生异常:初始化界面错误!");
  }
}


function initInsuGrid()
{
    var iArray = new Array();
    
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="保险合同号";    				//列名
        iArray[1][1]="120px";            			//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[2]=new Array();
        iArray[2][0]="集体投保单号码";         //列名
        iArray[2][1]="100px";                   //列宽
        iArray[2][2]=100;                       //列最大值
        iArray[2][3]=3;                         //是否允许输入,1表示允许，0表示不允许
        
        iArray[3]=new Array();
        iArray[3][0]="保单印刷号码";         //列名
        iArray[3][1]="100px";                   //列宽
        iArray[3][2]=100;                       //列最大值
        iArray[3][3]=3;                         //是否允许输入,1表示允许，0表示不允许
        
        iArray[4]=new Array();
        iArray[4][0]="参保人数";    				//列名
        iArray[4][1]="60px";            			//列宽
        iArray[4][2]=80;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[5]=new Array();
        iArray[5][0]="实收保费";		    	//列名
        iArray[5][1]="80px";            		//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[6]=new Array();
        iArray[6][0]="赔付人数";       			//列名
        iArray[6][1]="60px";            		//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[7]=new Array();
        iArray[7][0]="赔付金额";	       		//列名
        iArray[7][1]="80px";            		//列宽
        iArray[7][2]=100;            			//列最大值
        iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
        
        iArray[8]=new Array();
        iArray[8][0]="赔付率";	         		//列名
        iArray[8][1]="40px";            		//列宽
        iArray[8][2]=100;            			//列最大值
        iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[9]=new Array();
        iArray[9][0]="未决人数";         			//列名
        iArray[9][1]="60px";            		//列宽
        iArray[9][2]=100;            			//列最大值
        iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[10]=new Array();
        iArray[10][0]="未决赔款";         			//列名
        iArray[10][1]="80px";            		//列宽
        iArray[10][2]=100;            			//列最大值
        iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[11]=new Array();
        iArray[11][0]="拒赔人数";         			//列名
        iArray[11][1]="60px";            		//列宽
        iArray[11][2]=100;            			//列最大值
        iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[12]=new Array();
        iArray[12][0]="拒赔金额";         			//列名
        iArray[12][1]="80px";            		//列宽
        iArray[12][2]=100;            			//列最大值
        iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[13]=new Array();
        iArray[13][0]="保单状态";         			//列名
        iArray[13][1]="60px";            		//列宽
        iArray[13][2]=100;            			//列最大值
        iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[14]=new Array();
        iArray[14][0]="保单生效日期";         			//列名
        iArray[14][1]="80px";            		//列宽
        iArray[14][2]=100;            			//列最大值
        iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[15]=new Array();
        iArray[15][0]="保单失效日期";         			//列名
        iArray[15][1]="80px";            		//列宽
        iArray[15][2]=100;            			//列最大值
        iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        	
        iArray[16]=new Array();
        iArray[16][0]="退保标记";         			//列名
        iArray[16][1]="60px";            		//列宽
        iArray[16][2]=100;            			//列最大值
        iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        
        InsuGrid = new MulLineEnter("fm", "InsuGrid");
        
        InsuGrid.mulLineCount = 0;
        InsuGrid.displayTitle = 1;
        InsuGrid.locked = 1;
        InsuGrid.canSel = 1;
        InsuGrid.hiddenPlus = 1;
        InsuGrid.hiddenSubtraction = 1;
        InsuGrid.selBoxEventFuncName = "queryClaimDetail";
        InsuGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}


function initInsuDetailGrid()
  {                               
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保障计划";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="人员类别";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="投保险种代码";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="投保险种名称";         		//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="险种保费";              //列名
      iArray[5][1]="80px";                  //列宽
      iArray[5][2]=200;                     //列最大值
      iArray[5][3]=0; 

      iArray[6]=new Array();
      iArray[6][0]="责任项目";         		//列名
      iArray[6][1]="150px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="参保人数";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;
      
      iArray[8]=new Array();
      iArray[8][0]="赔付人数";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
      
      iArray[9]=new Array();
      iArray[9][0]="赔付金额";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;
      
      iArray[10]=new Array();
      iArray[10][0]="赔付率";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
        
      InsuDetailGrid = new MulLineEnter("fm", "InsuDetailGrid");
      //这些属性必须在loadMulLine前
      InsuDetailGrid.mulLineCount = 0;   
      InsuDetailGrid.displayTitle = 1;
      InsuDetailGrid.hiddenPlus = 1;
      InsuDetailGrid.hiddenSubtraction = 1;
      InsuDetailGrid.locked = 1;
      InsuDetailGrid.canSel = 0;       
      InsuDetailGrid.loadMulLine(iArray);  
 
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>