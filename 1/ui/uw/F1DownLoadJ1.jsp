<%@ page import="java.io.*"%><%@
page import="com.sinosoft.utility.*"%><%@
page import="com.f1j.ss.*"%><%@
page import="java.util.*" %><%@
page import="com.sinosoft.lis.f1print.AccessVtsFile"%><%
try {
	// Get a name of VTSFile
	//String strVFPathName = (String)session.getValue("RealPath");	
	//session.removeAttribute("RealPath");
	String strVFPathName = request.getParameter("RealPath");

	// Load VTS file to buffer
	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	AccessVtsFile.loadToBuffer(dataStream,strVFPathName);

	byte[] bArr = dataStream.toByteArray(); 
	InputStream ins = new ByteArrayInputStream(bArr);

	com.f1j.ss.BookModelImpl bm = new com.f1j.ss.BookModelImpl();		
	if( ins != null )
	{
		// Now, reload data file from mem
		bm.read(ins, new com.f1j.ss.ReadParams());
		// Write Excel file to client
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment; filename=data.xls");
		OutputStream ous = response.getOutputStream();
		bm.write(ous, new com.f1j.ss.WriteParams(com.f1j.ss.BookModelImpl.eFileExcel97));
		ous.flush();
	}
	else
	{
		System.out.println("There is not any data stream!");
	}
}
catch(java.net.MalformedURLException urlEx)
{
	urlEx.printStackTrace();
}
catch(java.io.IOException ioEx)
{
	ioEx.printStackTrace();
}
catch(Exception ex)
{
	ex.printStackTrace();
}
session.putValue("RealPath", null);
%>