<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：GrpReInsureInput.jsp
//程序功能：再保审核功能
//创建日期：2006-11-19 11:10:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <!--<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>-->
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GrpReInsureInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpReInsureInit.jsp"%>
  <title>再保信息</title>
</head>
<body  onload="initForm('<%=tGrpContNo%>', '<%=tGrpPolNo%>');">
  <form method=post name=fm target="fraSubmit" action= "./UpLodeSave.jsp" ENCTYPE="multipart/form-data">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty1);">
    		</td>
    		<td class= titleImg>
    			 已申请临分业务
    		</td>
    	</tr>
    	 </table>  	
		<tr>
		<td class=common>
    	 	状态：
    	</td>
    <TD class=input><Input class=codeno name=LFState1No  CodeData="0|^1|未临分|^2|已临分" ondblclick="return showCodeListEx('LFState1No',[this,LFState1],[0,1]);" onkeyup="return showCodeListKeyEx('LFState1No',[this,LFState1],[0,1]);"><input class=codename name=LFState1 readonly=true ></TD> 
    </tr>	  	
    	 <table>
      <TR>
        <TD  class= common>
         <textarea name="ApplyRemark" cols="100" rows="2" class="common"  verify="临分说明|len<=1000"></textarea>
        </TD>  
        </tr> 
         </table>      	    	  	
		    <TR>
		      <TD  width='8%' style="font:9pt">
		        上传附件：
		      </TD>     
		      <TD  width='80%'>
		        <Input  type="file"　width="50%" name=FileName1 class= common>
		        <INPUT VALUE="上载附件" class=cssButton TYPE=hidden onclick="ReInsureUpload1();">
		      </td>
         <td class="common" align="right">
         	<input value="信息保存" class="CssButton" type="button" onclick="ApplyChk();" >
          </td>
         <td class="common" align="right">
         	<input value="下载附件" class="CssButton" type="button" onclick="LFDownLoad();" >
          </td>                    		      
		    </tr> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty1);">
    		</td>
    		<td class= titleImg>
    			 未申请临分业务
    		</td>
    	</tr>
    	 </table>		 
<table>		     	
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty1);">
    		</td>
    		<td class= titleImg>
    			 再保规则
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCReInsure" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanReInsureGrid" >
					</span> 
				</td>
			</tr>
		  </table>
		  <table class = common>  
		  <td class = common align="middle">
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 
      <INPUT type= "hidden" name= "ProposalNoHide" value= "">				
      <INPUT type= "hidden" name= "GrpPolNo" value= "">
      <INPUT type= "hidden" name= "GrpContNo" value= "">
      </td>
      <td class="common" align="right"><input value="运行再保规则" class="CssButton" type="button" onclick="AutoReInsure();" ></td>
      </table>
	</Div>

  <table>  	
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty1);">
    	   	</td>
    		  <td class= titleImg>
    			 险种信息 
    	   	</td>
    	</tr>
  </table>
	<Div  id= "divLCRiskInfo" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					  <span id="spanRiskInfoGrid" >
					  </span> 
				    </td>
			  </tr>
		</table>   
  </div>       
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty1);">
    		</td>
    		<td class= titleImg>
    			 核保说明 
    		</td>
    	</tr>
    </table>
    <table class = common>  
      <TR class = common >
        <TD  class= common>
         <textarea name="Remark" cols="105" rows="2" class="common"  verify="核保说明|len<=1000"></textarea>
        </TD>  
        </TR>    
    </table>
    <table>
		    <TR>
		      <TD  width='8%' style="font:9pt">
		        上传附件
		      </TD>     
		      <TD  width='30%'>
		        <Input  type="file"　width="50%" name=FileName class= common>
		        <INPUT VALUE="上载附件" class=cssButton TYPE=hidden onclick="ReInsureUpload();">
		      </td>
        <td class="common" align="middle"><input value="发送再保审核" class="CssButton" type="button" onclick="SendUWReInsure();" ></td>		      	      
		    </TR>    	
    </table>      	
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty1);">
    		</td>
    		<td class= titleImg>
    			 再保审核任务 
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCReInsureAudit" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanReInsureAuditGrid" >
					</span> 
				</td>
			</tr>
		</table>
  </div>  
		<table class = common>  
		    <TR class = common >
		      <td class = common align="right">
			  <INPUT VALUE="下 载" class=cssButton TYPE=button onclick="DownLoad();">
		      </td>
		    </TR>    
		</table>
		<tr>
		<td class=common>
    	 	状态：
    	</td>
    	<td>
    <TD class=input><Input class=codeno name=LFStateNo  CodeData="0|^1|未临分|^2|已临分" ondblclick="return showCodeListEx('LFStateNo',[this,LFState],[0,1]);" onkeyup="return showCodeListKeyEx('LFStateNo',[this,LFState],[0,1]);"><input class=codename name=LFState readonly=true ></TD> 
    	</td>
    </tr>	  	
    	 <table>
      <TR>
        <TD  class= common>
         <textarea name="BJRemark" cols="100" rows="2" class="common"  verify="办结说明|len<=1000"></textarea>
        </TD>  
        </tr> 
       </table>
     <tr>
         <td class="common" >
         	<input value="信息保存" class="CssButton" type="button" onclick="BJChk();" >
          </td> 
<!--			<table class = common>  
		    <TR class = common >  -->
        <td class = common valign="bottom">
          <input type="hidden" class= Common name= FilePath value= "">
      		<INPUT VALUE="办 结" class=cssButton TYPE=button onclick="ReInsureOver();">
        </td>       	
<!--      </TR>       
		</table>		-->
          
         </tr>      	 
                  	    	
  </form>  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
<!--	<form action="./UpLodeSave.jsp" method=post name=fmImport target="fraSubmit" ENCTYPE="multipart/form-data">
		    <TR>
		      <TD  width='8%' style="font:9pt">
		        上传附件
		      </TD>     
		      <TD  width='80%'>
		        <Input  type="file"　width="50%" name=FileName class= common>
		        <INPUT VALUE="上载附件" class=cssButton TYPE=hidden onclick="ReInsureUpload();">
		      </td>
        <td class="common" align="right"><input value="发送再保审核" class="CssButton" type="button" onclick="SendUWReInsure();" ></td>		      	      
		    </TR>
	</form>-->
</body>
</html>