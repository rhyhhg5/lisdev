
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：核保既往保全查询-团单
//程序功能：
//创建日期：2006-1-6
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <script src="GrpUWRejiggerApp.js"></script>
  <%@include file="GrpUWRejiggerAppInit.jsp"%>


  <title>保全历史资料 </title>
</head>

<body  onload="initForm();">
<form name=fm action="" target=fraSubmit method=post>
    <table>
        <tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCont);">
            </td>
            <td class= titleImg>
                团体基本信息
            </td>
        </tr>
    </table>
    <Div  id= "divGrpCont" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title8>
            团体客户号
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=GrpNo >
          </TD>
          <TD  class= title8>
            投保单位名称
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=GrpName>
          </TD>
          <!--TD  class= title8>
            团体保障号:
          </TD>
          <TD  class= input8-->
            <Input class="readonly" readonly name=GrpPolNo type="hidden">
          <!--/TD-->
        </TR>
    </table>
    </DIV>
    
    <br />
    
    <table>			
    		<tr>
    		  <td width="17"> 
    		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divpostPurview);"> 
    		  </td>
    		  <td width="185" class= titleImg>保全历史资料</td>
    		</tr>
    	</table>
        
        <div id="divpostPurview" style="display: ''">
        	<table  class= common>
        		<tr  class= common>
        	  		<td text-align: left colSpan=1>
        				<span id="spanbqViewGrid" >
        				</span> 
        		  	</td>
        		</tr>
        	</table>
            
            <div id="divbqViewGridPage" align="center" style="display:'none'">
                <input class="cssbutton" type="button" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input class="cssbutton" type="button" value="上一页" onclick="turnPage1.previousPage();" />                    
                <input class="cssbutton" type="button" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input class="cssbutton" type="button" value="尾  页" onclick="turnPage1.lastPage();" />
            </div>
        </div>
        
  		<input type=hidden id="fmtransact" name="fmtransact" value="">
    
	<Input name="Query" class=cssButton type=Button value="查看批单" onclick="queryBQScanner();">

    <hr />
    <div>
        <!-- <INPUT VALUE = "既往询价信息" Class="cssButton" TYPE=button onclick= "showAskApp();"> -->
        <input value="既往承保信息" class="cssButton" type="button" onclick="showSignContH();" />
        <input value="既往投保信息" class="cssButton" type="button" onclick= "showEnsureCont();" />
        <input value="既往保全信息" class="cssButton" type="button" onclick= "showRejiggerApp();" />
        <input value="既往理赔信息" class="cssButton" type="button" onclick= "showInsuApp();" />
    </div>
    <hr />
    
    <div id = "divUWAgree" style = "display: ''">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="GoBack();">
    </div>
    
	</form>
	
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>

