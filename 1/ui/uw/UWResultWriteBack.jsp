<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：UWResultWriteBack.jsp
//程序功能：核保通知书回复
//创建日期：2005-9-26 17:06
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var ContNo = "<%=request.getParameter("ContNo")%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var customers = "";                   //生调人选
  var tLoadFlag = "<%=request.getParameter("LoadFlag")%>";
</script>
<head >
<title>核保通知书回复 </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="UWResultWriteBackInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UWResultWriteBackInit.jsp"%>
  
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
	  <Div id="divLDClassInfo1" style="display:''">
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanUWResultGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
		</div>
	<table class=common>
		<TR  class= common>
		  <TD  class= title> 客户意见  </TD>
		  <TD  class= input>  <Input class="code" name=CustomerIdea value= "1" CodeData= "0|^0|不同意^1|同意" ondblClick="showCodeListEx('Grade',[this,''],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('Grade',[this,''],[0,1],null,null,null,1);">  </TD>
		  <TD  class= title >  核保结论 </TD>
		  <TD  class= input > <Input class=common name=PassFlag > </TD>  
		  <TD  class= input> <INPUT id="savebutton" VALUE="确  认" class=cssButton TYPE=button onclick="submitForm();"></TD>  
		  <TD  class= input> </TD>  
		</TR>
	</table>
	<table class=common>
		<tr>
			<TD height="24"  class= title>
				核保意见
			</TD>
			<td></td>
		</tr>
		<tr>
			<TD  class= input> <textarea name="UWIdea" cols="100%" rows="5" witdh=120% class="common"></textarea></TD>
			<td></td>
		</tr>
	</table>
	<input type="hidden" name="PolNo">
	<input type="hidden" name="LoadFlag">
	<input type="hidden" name="fmtransact">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>