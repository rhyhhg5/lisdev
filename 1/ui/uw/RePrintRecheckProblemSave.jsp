<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.LOPRTManagerSchema"%>
<%@page import="com.sinosoft.lis.vschema.LOPRTManagerSet"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.io.*"%>
<%
  System.out.println("--------------------PDFRePrintReCheckUI start------------------");
  CError cError = new CError();
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  
  String PrtSeq = request.getParameter("PrtSeq");//打印单证流水号
  String OtherNo = request.getParameter("OtherNo");//其它号码
  String Code = request.getParameter("Code");//其它号码
 
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  
  VData tVData = new VData();
  CErrors mErrors = new CErrors();
  
  LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
  
  tLOPRTManagerSchema.setPrtSeq(PrtSeq);
  tLOPRTManagerSchema.setOtherNo(OtherNo);
  tLOPRTManagerSchema.setCode(Code);
  tVData.addElement(tLOPRTManagerSchema);
  tVData.addElement(tG);
  
  PDFRePrintReCheckUI tPDFRePrintReCheckUI = new PDFRePrintReCheckUI();
  if(operFlag==true){
    if(!tPDFRePrintReCheckUI.submitData(tVData,"UPDATE")) {           
      FlagStr = "Fail";
      Content = tPDFRePrintReCheckUI.mErrors.getFirstError().toString();
    }
    else {
        if( tPDFRePrintReCheckUI.mErrors.getErrorCount()>0){
           Content = tPDFRePrintReCheckUI.mErrors.getFirstError().toString();
           FlagStr = "PrintError"; 
        }else{ 
           Content = "申请成功！";
           FlagStr = "Succ"; 
        }         
    }
   }
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

