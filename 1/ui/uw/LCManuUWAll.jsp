<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ManuUWAll.jsp.
//程序功能：个单人工核保
//创建日期：2005-1-28 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tFlag = "";
	tFlag = request.getParameter("type");
%>
<html>
<%	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operFlag = "1";		//区分团险和个险的标志
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
	var spanCode = false;
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LCManuUWAll.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="LCManuUWAllInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./ManuUWAllChk.jsp"> 
  <div  id= "queryPage" style= "display: ''">
    <table class=common>
      <tr class=common>
        <td class=title>管理机构</td>
        <td class=input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><input class=codename name=ManageComName readonly=true ></td>
        <td class=title>销售渠道</td>
        <td class=input><input class=codeNo name=SaleChnl verify="业务员代码|code:LCSaleChnl" ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true ></td>
        <td class=title>业务员代码</td>
        <td class=input><Input class=common name="AgentName" verify="业务员代码|len<=40&int"></td>
      </tr>
      <tr class=common>
        <TD class=title>印刷号</TD>
        <TD class=input><Input class=common name="PrtNo" verify="印刷号码"></TD>
        <TD class=title>投保人姓名</TD>
        <TD class=input><Input class=common name="AppntName" verify="投保人|len<=20"></TD>
        <TD class=title>被保险人姓名</TD>
        <TD class=input><Input class=common name="InsuredName" verify="被保险人|len<=20"></TD>
      </tr>
      <tr class=common>
        <TD class=title>申请日期</TD>
        <TD class=input><Input class=coolDatePicker name="ApplyDate" verify="申请日期|date"></TD>
        <TD class=title>生效日期</TD>
        <TD class=input><Input class=coolDatePicker name="CValiDate" verify="申请日期|date"></TD>
        <TD class=title>客户回复状态</TD>
        <TD class=input><Input class=codeNo name="Receive" verify="客户回复状态|code:Receive" CodeData="0^1|未发通知书^2|已发通知书^3|已回复" ondblclick="showCodeListEx('Receive',[this,ReceiveName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Receive',[this,ReceiveName],[0,1],null,null,null,1);"><input class=codename name=ReceiveName readonly=true ></TD>
      </tr>
    </table>
  </div>

    <div id=DivLCContInfo STYLE="display:''"> 
        <table>
            <tr>
                <td class=common>
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onClick="showPage(this,divLCPol1);">
                </td>
                <td class= titleImg>核保申请</td>
            </tr>   
        </table>
    </div>
    <div style="display:''">
        <table class="common">
            <tr>
                <td class="title">保单渠道</td>
                <td class="input"><Input class=codeNo value="00" readonly=true name=ContSaleChnlType verify="保单渠道|notnull&code:contsalechnltype" ondblclick="return showCodeList('contsalechnltype',[this,ContSaleChnlTypeName],[0,1],null,null,null,1);" ><input class=codename name=ContSaleChnlTypeName readonly=true elementtype="nacessary"></td>
                <td class="title">&nbsp;</td>
                <td class="input">&nbsp;</td>
                <td class="title">&nbsp;</td>
                <td class="input">&nbsp;</td>
            </tr>
        </table>
    </div>
    <div id="divApplyButton" style="display:''">      
        <table class="common">
            <tr>
                <td class="title">申请件数</td>
                <td class="input"><input class="common" name="ApplyNo" verify="申请件数|len<=10&int" /></td>
                <td class="title"><input class="cssButton" id="riskbutton" value="申  请" type="button" onClick="ApplyUW();" /></td>
                <td class="input">(每次申请最多为30件)</td>
                <td class="title">待申请件数</td>
                <td class="input"><input class="readonly" readonly="readonly" name="UWNo" /></td>
            </tr>
        </table>
    </div>
  
   <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 待核保保单：
    		</td>
    		<td class=common>    		
    		 	<INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
    		</td>
    	</tr>  	
    </table>
    </Div>
         <Div  id= "divLCPol1" align=center style= "display: ''" align = left>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		<Div id= "div1" align=center style= "display: '' ">
	  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    	</div>
    	</table>    
    </div>	
    		<table>
			<tr>
		    <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEdorList);">
				</td>
				<td class= titleImg>
					 保全待核受理
				</td>
			</tr>  	
		</table>
		<Div  id= "divEdorList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanEdorGrid">
					</span> 
				</td>
			</tr>
		</Div>
	    <Div id= "divEdor" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage3.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage3.lastPage();">
	 		</Div>
	 		
	 		<table>
			<tr>
		    <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divNoReplyEdorList);">
				</td>
				<td class= titleImg>
					 保全核保待回复
				</td>
			</tr>  	
		</table>
		<Div  id= "divNoReplyEdorList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanNoReplyEdorGrid">
					</span> 
				</td>
			</tr>
		</Div>
	    <Div id= "divEdorNoReply" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage5.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage5.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage5.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage5.lastPage();">
	 		</Div>
	 		
	 		
	 		<div id="WriteBackDiv" align=left  style= "display: ''">
		    <table>
		    	<tr>
		        	<td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
		    		</td>
		    		<td class= titleImg>
		    			 待回复保单：
		    		</td>
		    	</tr>  	
		    </table>
		  </div>
    </Div>
         <Div  id= "divLCPol1" style= "display: ''" align=center>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanUpReportPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    </div>
   
    </Div>
     <Div id= "div1" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
  </Div>
  
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 既往核保信息：
    		</td>
    		<td class=common>
    			<INPUT CLASS=cssbutton VALUE="查　询" TYPE=button onclick="queryHistory();"> 
    		</td>
    	</tr>  	
    </table>
    </Div>
         <Div  id= "divLCPol1" style= "display: ''" align=center>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanHistoryUwGrid" >
  					</span> 
  			  	</td>
  			</tr>
    </div>
   
    </Div>
     <Div id= "div1" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </Div>
 		</div>
 	</br>	
 	</br>
 	<table class=common>
      <tr class=common>
        <TD class=title>受理工单号</TD>
        <TD class=input><Input class=common name="EdorNo" ></TD>
        <TD class=title>受理人</TD>
        <TD class=input><Input class=common name="ShouLiRen" ></TD>
      </tr>
      </table>
 	 <table>
			<tr>
		    <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOldEdorList);">
				</td>
				<td class= titleImg>
					 既往保全核保信息
				</td>
			  <td class=common>
    			<INPUT CLASS=cssbutton VALUE="查　询" TYPE=button onclick="queryOldEdorList();"> 
    		</td>
			</tr>  	
		</table>
		<Div  id= "divOldEdorList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1 >
					<span id="spanOldEdorGrid">
					</span> 
				</td>
			</tr>
	    <Div id= "divOldEdor" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage4.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage4.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage4.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage4.lastPage();">
	 		</Div>
	 		
	 		<div align=left  style= "display: ''">
	 			<table>
			<tr>
		  	  <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondLPList);">
				</td>
				<td class= titleImg>
					理赔二次核保
				</td>
			</tr>  	
		</table>
		</Div>
		<Div  id= "divSecondLPList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanSecondLPGrid">
					</span> 
				</td>
			</tr>
	    	<Div id= "divSecondLP" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage6.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage6.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage6.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage6.lastPage();">
	 		</Div>
	 	</Div>
	 	
	 	<div align=left  style= "display: ''">	
		<table class=common>
      <tr class=common>
        <TD class=title>案件号</TD>
        <TD class=input><Input class=common name="CaseNo" ></TD>
        <TD class=title>受理人</TD>
        <TD class=input><Input class=common name="Maker" ></TD>
      </tr>
      </table>
 	 <table>
			<tr>
		    <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOldEdorList);">
				</td>
				<td class= titleImg>
					 既往理赔二次核保信息
				</td>
			  <td class=common>
    			<INPUT CLASS=cssbutton VALUE="查　询" TYPE=button onclick="queryLPTwoOldEdorList();"> 
    		</td>
			</tr>  	
		</table>
		<Div  id= "divLPTwoOldEdorList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1 >
					<span id="spanLPTwoOldEdorGrid">
					</span> 
				</td>
			</tr>
			</Div>
	    <Div id= "divLPOldEdor" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage10.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage10.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage10.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage10.lastPage();">
	 		</Div>
	 		
	 		<table>
			<tr>
		  	  <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPXBList);">
				</td>
				<td class= titleImg>
					理赔续保待受理
				</td>
			</tr>  	
		</table>
		<Div  id= "divLPXBList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanLPXBGrid">
					</span> 
				</td>
			</tr>
	    	<Div id= "divLPXB" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage7.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage7.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage7.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage7.lastPage();">
	 		</Div>
	 	</Div>
	 	<table>
			<tr>
		    <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divNoReplyEdorList);">
				</td>
				<td class= titleImg>
					 理赔续保待回复
				</td>
			</tr>  	
		</table>
		<Div  id= "divLPNoReplyEdorList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanLPNoReplyEdorGrid">
					</span> 
				</td>
			</tr>
		</Div>
	    <Div id= "divLPNoReplyEdor" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage8.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage8.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage8.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage8.lastPage();">
	 		</Div>
	 		
	 		<table class=common>
      <tr class=common>
        <TD class=title>案件号</TD>
        <TD class=input><Input class=common name="EdorNoLP" ></TD>
        <TD class=title>经办人</TD>
        <TD class=input><Input class=common name="ShouLiRenLP" ></TD>
      </tr>
      </table>
 	 <table>
			<tr>
		    <td class=common>
		       <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOldEdorList);">
				</td>
				<td class= titleImg>
					 既往理赔续保信息
				</td>
			  <td class=common>
    			<INPUT CLASS=cssbutton VALUE="查　询" TYPE=button onclick="queryLPOldEdorList();"> 
    		</td>
			</tr>  	
		</table>
		<Div  id= "divLPOldEdorList" style= "display: ''" align=center>
			<tr class=common>
				<td text-align: left colSpan=1 >
					<span id="spanLPOldEdorGrid">
					</span> 
				</td>
			</tr>
			</Div>
	    <Div id= "divLPOldEdor" align=center style= "display: ''">
		    <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage9.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage9.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage9.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage9.lastPage();">
	 		</Div>
	 			 		
  <span id="spanCode"  style="display: none; position:absolute; slategray" onclick="return ;	if(spanCode) showCodeList('bank',[ManageCom],null,null,null,null,1); spanCode=false;" onkeyup="return showCodeListKey('bank',[this],null,null,null,null,1);"></span>	
	<Input type=hidden name="ApplyType" >
</form>
</body>
</html>
