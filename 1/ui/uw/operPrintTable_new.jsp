<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.workflow.ask.*"%>
<%@page import="com.sinosoft.workflowengine.*"%>
<%@page import="java.util.*"%>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");
  // 输出参数
  String FlagStr = "";
  String Content = "";
  //   String PrintNo=(String)session.getValue("PrintNo");
  //   String tCode = (String)session.getValue("Code");
  //   String tPrtNo = (String)session.getValue("PrtNo");
  //   String tContNo = (String)session.getValue("ContNo");
  //   String tMissionID = (String)session.getValue("MissionID");
  //   String tSubMissionID = (String)session.getValue("SubMissionID");
  String PrintNo = request.getParameter("PrintNo");
  String tMissionID = request.getParameter("MissionID");
  String tSubMissionID = request.getParameter("SubMissionID");
  String tCode = request.getParameter("Code");
  String tContNo = request.getParameter("ContNo");
  String tPrtNo = request.getParameter("PrtNo");
  String tStartNo = request.getParameter("StartNo"); //ZB
  String tCertifyCode = request.getParameter("CertifyCode"); //ZB
  String tRePrintFlag = request.getParameter("RePrintFlag"); //ZB
  System.out.println("ZB--> RePrintFlag:" + tRePrintFlag); //ZB
  String tIncomeType = request.getParameter("IncomeType"); //wanglong
  String tGrpContNo = (String) session.getValue("GrpContNo");
  System.out.println("put session value,ContNo2:" + tContNo);
  System.out.println("tMissionID:" + tMissionID);
  System.out.println("PrintNo:" + PrintNo);
  System.out.println("tCode:" + tCode);
  System.out.println("tPrtNo:" + tPrtNo);
  if (PrintNo == null || tCode == null) {
    FlagStr = "Succ";
    Content = "打印数据完毕！";
  }
  else {
    if (tCode.equals("03") || tCode.equals("04") || tCode.equals("05") || tCode.equals("14") || tCode.equals("85")) {
      //准备数据
      //准备公共传输信息
      String tOperate = new String();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("PrtSeq", PrintNo);
      tTransferData.setNameAndValue("Code", tCode);
      tTransferData.setNameAndValue("ContNo", tContNo);
      tTransferData.setNameAndValue("PrtNo", tPrtNo);
      tTransferData.setNameAndValue("MissionID", tMissionID);
      tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
      if (tCode.equals("03"))
        tOperate = "0000001106"; //打印体检通知书任务节点编码
      if (tCode.equals("04"))
        tOperate = "0000001108"; //打印面见通知书任务节点编码
      if (tCode.equals("05"))
        tOperate = "0000001107"; //打印核保通知书任务节点编码
      if (tCode.equals("14"))
        tOperate = "0000001017"; //打印业务员通知书任务节点编码
      if (tCode.equals("85"))
        tOperate = "0000001023"; //打印业务员通知书任务节点编码
      VData tVData = new VData();
      tVData.add(tTransferData);
      tVData.add(tGI);
      // 数据传输
      TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
      if (!tTbWorkFlowUI.submitData(tVData, tOperate)) { //执行保全核保生调工作流节点0000000004
        int n = tTbWorkFlowUI.mErrors.getErrorCount();
        Content = " 更新打印数据失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
        System.out.println(Content);
        FlagStr = "Fail";
      }
      else {
        FlagStr = "Succ";
        Content = "更新打印数据成功！";
      }
      System.out.println("Print:" + FlagStr);
    }
    else if (tCode.equals("61") || tCode.equals("64") || tCode.equals("65")) {
      //准备数据
      System.out.println("开始打印后台处理");
      //准备公共传输信息
      String tOperate = new String();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("PrtSeq", PrintNo);
      tTransferData.setNameAndValue("Code", tCode);
      tTransferData.setNameAndValue("GrpContNo", tContNo);
      tTransferData.setNameAndValue("PrtNo", tPrtNo);
      tTransferData.setNameAndValue("MissionID", tMissionID);
      tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
      if (tCode.equals("61"))
        tOperate = "0000006015"; //打印询价确定通知书任务节点编码
      if (tCode.equals("64"))
        tOperate = "0000006008"; //团体询价补充材料通知书
      if (tCode.equals("65"))
        tOperate = "0000006021"; //团单询价跟踪通知书
      VData tVData = new VData();
      tVData.add(tTransferData);
      tVData.add(tGI);
      // 数据传输
      AskWorkFlowUI tAskWorkFlowUI = new AskWorkFlowUI();
      if (!tAskWorkFlowUI.submitData(tVData, tOperate)) { //执行保全核保生调工作流节点0000000004
        int n = tAskWorkFlowUI.mErrors.getErrorCount();
        Content = " 更新打印数据失败，原因是: " + tAskWorkFlowUI.mErrors.getError(0).errorMessage;
        System.out.println(Content);
        FlagStr = "Fail";
      }
      else {
        FlagStr = "Succ";
        Content = "更新打印数据成功！";
      }
      System.out.println("Print:" + FlagStr);
    }
    else if (tCode.equals("06") || tCode.equals("76")) {
      System.out.println("开始打印后台处理");
      if (!PrintNo.equals("")) {
        ExeSQL tExeSQL = new ExeSQL();
        String strSql = "update loprtmanager set StateFlag='" + 1 + "' where Prtseq='" + PrintNo + "'";
        System.out.println("执行SQL语句:" + strSql);
        if (!tExeSQL.execUpdateSQL(strSql)) {
          FlagStr = "Fail";
          Content = "打印失败，请重新打印！";
        }
        else {
          FlagStr = "Nothing";
          Content = "打印数据完毕！";
        }
      }
    }
    else if (tCode.equals("07") || tCode.equals("11") || tCode.equals("17") || tCode.equals("18") || tCode.equals("19")) {
      System.out.println("开始打印后台处理");
      if (!PrintNo.equals("")) {
        ExeSQL tExeSQL = new ExeSQL();
        String strSql = "update loprtmanager set StateFlag='" + 1 + "' where Prtseq='" + PrintNo + "'";
        System.out.println("执行SQL语句:" + strSql);
        if (!tExeSQL.execUpdateSQL(strSql)) {
          FlagStr = "Fail";
          Content = "打印失败，请重新打印！";
        }
        else {
          FlagStr = "Nothing";
          Content = "打印数据完毕！";
        }
      }
    }
    else if (tCode.equals("16")) {
      System.out.println("开始打印后台处理");
      if (!PrintNo.equals("")) {
        ExeSQL tExeSQL = new ExeSQL();
        String strSql = "update loprtmanager set StateFlag='1' where Prtseq='" + PrintNo + "'";
        System.out.println("执行SQL语句:" + strSql);
        if (!tExeSQL.execUpdateSQL(strSql)) {
          FlagStr = "Fail";
          Content = "打印失败，请重新打印！";
        }
        else {
          FlagStr = "Nothing";
          Content = "打印数据完毕！";
        }
      }
    }
    else if (tCode.equals("75")) {
      System.out.println("开始打印后台处理");
      if (!PrintNo.equals("")) {
        ExeSQL tExeSQL = new ExeSQL();
        String strSql = "update loprtmanager set StateFlag='1' where otherno='" + PrintNo + "' and code = '75'";
        System.out.println("执行SQL语句:" + strSql);
        if (!tExeSQL.execUpdateSQL(strSql)) {
          FlagStr = "Fail";
          Content = "打印失败，请重新打印！";
        }
        else {
          FlagStr = "Nothing";
          Content = "打印数据完毕！";
        }
      }
    }
    else if (tCode.equals("99")) {
      System.out.println("不下发单证");
      FlagStr = "Nothing";
      Content = "打印数据完毕！";
    }
    else if (tCode.equals("35")) {
      System.out.println("come in the print FeeInvoice");
      System.out.println("ZB FlagStr: " + FlagStr);
      System.out.println("come into Succ.......................");
      LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
      tLOPRTManager2Schema.setOtherNo(PrintNo);
      tLOPRTManager2Schema.setOtherNoType("05");
      tLOPRTManager2Schema.setCode("35");
      System.out.println(tIncomeType);
      System.out.println(tRePrintFlag);
      System.out.println(tLOPRTManager2Schema.getOtherNo());
      VData tVData = new VData();
      tVData.add(tGI);
      tVData.add(tLOPRTManager2Schema);
      tVData.add(tRePrintFlag);
      tVData.add(tIncomeType); //wanglong
      PrintManager2BL tPrintManager2BL = new PrintManager2BL();
      if (!tPrintManager2BL.submitData(tVData, "REQUEST")) {
        FlagStr = "Fail:" + tPrintManager2BL.mErrors.getFirstError().toString();
      }
      else {
        FlagStr = "Succ";
        Content = "更新打印数据成功！";
        //session.putValue("PrintNo",null);
        //session.putValue("PrintQueue",null);
      }
      System.out.println("Print:" + FlagStr);
      
      // 重打发票时，发票号也要自增，即重打也进行发票核销。
      //if (FlagStr.equals("Succ") && !tRePrintFlag.equals("1")) {
      if (FlagStr.equals("Succ")) {
        /*=========核销==========*/
        try {
          Content = " 人工核销处理成功 ";
          FlagStr = "Succ";
          String auditType = "HANDAUDIT";
          LZCardSet setLZCard = new LZCardSet();
          LZCardSchema schemaLZCard = new LZCardSchema();
          schemaLZCard.setCertifyCode(tCertifyCode.trim());
          schemaLZCard.setStartNo(tStartNo.trim());
          schemaLZCard.setEndNo(tStartNo.trim());
          schemaLZCard.setStateFlag("5"); //对发票核销StateFlag置为"6"
          setLZCard.add(schemaLZCard);
          VData vData = new VData();
          vData.addElement(tGI);
          vData.addElement(setLZCard);
          vData.addElement(auditType);
          Hashtable hashParams = new Hashtable();
          hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
          vData.addElement(hashParams);
          // 数据传输
          AuditCancelBL tCertTakeBackUI = new AuditCancelBL();
          if (!tCertTakeBackUI.submitData(vData, "INSERT")) {
            Content = " 保存失败，原因是: " + tCertTakeBackUI.mErrors.getFirstError();
            FlagStr = "Fail";
          }
          else {
            Content = " 核销成功! ";
            FlagStr = "Succ";
            vData = tCertTakeBackUI.getResult();
            String strTakeBackNo = (String) vData.getObjectByObjectName("String", 0);
            session.setAttribute("TakeBackNo", strTakeBackNo);
            session.setAttribute("State", CertStatBL.PRT_STATE);
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
          Content = FlagStr + " 保存失败，原因是:" + ex.getMessage();
          FlagStr = "Fail";
        }
      }
      /*===================*/
    }
    else {
      LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
      tLOPRTManagerSchema.setPrtSeq(PrintNo);
      VData tVData = new VData();
      tVData.add(tLOPRTManagerSchema);
      tVData.add(tGI);
      PrintManagerBL tPrintManagerBL = new PrintManagerBL();
      if (!tPrintManagerBL.submitData(tVData, "PRINT")) {
        FlagStr = "Fail:" + tPrintManagerBL.mErrors.getFirstError().toString();
      }
      else {
        FlagStr = "Nothing";
        Content = "更新打印数据成功！";
        //session.putValue("PrintNo",null );
      }
      System.out.println("Print:" + FlagStr);
    }
  }
%>
<html>
<script language="javascript">
window.returnValue="<%=FlagStr%>";
window.close();
</script>
</html>
