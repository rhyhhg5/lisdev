//程序名称：UWBack.js
//程序功能：核保订正
//创建日期：2003-03-27 15:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  initForm();
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	if(!verifyInput2())
	return false;
	// 书写SQL语句
	k++;
	var strSQL = "";
	/*
	strSQL = "select ProposalNo,PrtNo,RiskCode,RiskVersion,AppntName,InsuredName from LCPol where "+k+" = "+k
				 + " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pg')=1 "
				 + " and AppFlag='0'"          //承保
				 + " and UWFlag in ('1','2','4','9','a')"  //核保最终状态
				 + " and LCPol.ApproveFlag = '9' "
				 + " and grppolno = '00000000000000000000'"
				 + " and contno = '00000000000000000000'"
				 + " and polno = mainpolno"
				 + getWherePart( 'ProposalNo' )
				 + getWherePart( 'ManageCom' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'AgentGroup' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + " and ManageCom like '" + comcode + "%%'";
				*/
	var tagentcode="";
	if(fm.AgentCode.value!=""){
		var magentcode=easyExecSql(" select getAgentCode('"+fm.AgentCode.value+"') from dual");
		tagentcode=" and a.MissionProp3='"+magentcode+"'";
	}	
     	strSQL = "select a.missionprop1,a.missionprop5,a.missionprop7,a.missionprop2,a.Missionid,a.submissionid, a.ActivityID "
     	       + "  from lwmission a,lccont b where 1=1"
   			     + " and activityid = '" + tActivityID + "' "
   			     + " and processid = '" + tProcessID + "' "
   			     + " and a.MissionProp5=b.prtno and b.appflag='0'  "
   			     //+ getWherePart('lwmission.MissionProp1','ProposalNo')
			       + getWherePart('a.MissionProp2','ManageCom','like')
			       + tagentcode
			       + getWherePart('a.MissionProp5','PrtNo')
			       +" and a.MissionProp2 like '"+manageCom+"%%'"
					;
				
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有待核保订正的个单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function back()
{
var checkFlag = 0;
	for (i=0; i<PolGrid.mulLineCount; i++)
	{
		if (PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
  if(checkFlag){
	  	var	tPrtNo = PolGrid.getRowColData(checkFlag - 1, 2);
	  	if(tPrtNo == null || tPrtNo == "" || tPrtNo =="null"){
	  		alert("请查询保单！");
	  		return false;
	  	}
	  	var tSQL = "select 1 From lccont where prtno = '"+tPrtNo+"' and PayMethod = '01' ";
	  	var arrResult = easyExecSql(tSQL);
	  	if(arrResult){
	  		alert("该单从投保人帐户缴纳首期保费，无法进行核保订正！");
	  		return false;
	  	}
	  	var i = 0;
	  	var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		//showSubmitFrame(mDebug);
		fm.submit(); //提交
	}else{
	 	alert("请先选择保单信息！");
	 	return false;
	}
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}


//选保单时候记录投保单号
// 查询按钮
function easyQueryAddClick(parm1,parm2)
{

	var tProposalNo = "";
	var tPrtNo = "";
	var tMission = "";
	var tSubMission = "";
	var tInsuredName = "";
	var tAppntName = "";
	if(fm.all(parm1).all('InpPolGridSel').value == '1' )
	{
		//当前行第1列的值设为：选中
   		tProposalNo = fm.all(parm1).all('PolGrid1').value;
   		tPrtNo = fm.all(parm1).all('PolGrid2').value;
   		tAppntName = fm.all(parm1).all('PolGrid3').value;
   		tInsuredName = fm.all(parm1).all('PolGrid4').value;
   		tMission = fm.all(parm1).all('PolGrid5').value;
   		tSubMission = fm.all(parm1).all('PolGrid6').value;
   		tActivityID = fm.all(parm1).all('PolGrid7').value;
  	}
  	
  	if (tProposalNo == "")
  	{
  		alert("请选择投保单！");
  	}
  	
  	fm.ProposalNoHide.value = tProposalNo;
	  fm.PrtNo.value = tPrtNo;
	  fm.AppntName.value = tAppntName;
	  fm.InsuredName.value = tInsuredName;
	  fm.MissionId.value = tMission;
	  fm.SubMissionId.value = tSubMission;
	  fm.ActivityID.value = tActivityID;

	return true;
}

function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }

}
    