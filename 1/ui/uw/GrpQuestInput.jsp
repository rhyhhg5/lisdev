<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GrpQuestInput.jsp
//程序功能：团体问题件录入
//创建日期：2005-06-13 14:47:23
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<%
  String tMissionID = "";
  String tSubMissionID = "";
  String loadflag="";
  tMissionID = request.getParameter("MissionID");
  tSubMissionID = request.getParameter("SubMissionID");
  loadflag=request.getParameter("loadflag");
  %>
<script>
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";
	var loadflag=	"<%=loadflag%>";
</script>  
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="GrpQuestInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpQuestInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./GrpQuestSave.jsp" method=post name=fm target="fraSubmit">
  	  <DIV id=DivLCContButton STYLE="display:''">
  <table id="table">
  		<tr>
  			<td class="titleImg">已录入问题件列表
  			</td>
  		</tr>
  </table>
</DIV>
    <Div  id= "GrpQuest" style= "display: ''">
      <table>
          <tr  class= common>
            <td text-align: left >
          		<span id="spanGrpQuestGrid" >
          		</span>
        		</td>
      		</tr>
     </table>
     <Div id="LoadFlag1" style="display:'none'">
		  <INPUT CLASS=cssbutton VALUE="保存问题件" TYPE=button onclick="submitForm();">
		   <INPUT CLASS=cssbutton VALUE="修改问题件" TYPE=button onclick="updateClick();">
		   <INPUT CLASS=cssbutton VALUE="回退" TYPE=button onclick="Back();">
		  <INPUT CLASS=cssbutton VALUE="打印问题件" TYPE=button onclick="SendIssueNotice();">
		  </Div>
     <Div id="LoadFlag2" style="display:'none'">
		  <INPUT CLASS=cssbutton VALUE="保存问题件" TYPE=button onclick="submitForm();">
		   <INPUT CLASS=cssbutton VALUE="修改问题件" TYPE=button onclick="updateClick();">
		   <INPUT CLASS=cssbutton VALUE="回退" TYPE=button onclick="Back();">
		  </Div>
		  <Div id="LoadFlag3" style="display:'none'">
		   <INPUT CLASS=cssbutton VALUE="保存问题件" TYPE=button onclick="submitForm();">
		   <INPUT CLASS=cssbutton VALUE="修改问题件" TYPE=button onclick="updateClick();">
		  </Div>			  
		  <Div id= "divPage" align=center style= "display: 'none' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		  </Div>
		<Div id="GrpQuestBack" style="display:'none'">  
    <table id="table1">
  		<tr>
  			<td class="titleImg">已打印问题件列表
  			</td>
  		</tr>
  </table>
    <Div  id= "GrpQuest1" style= "display: ''">
      <table>
          <tr  class= common>
            <td text-align: left >
          		<span id="spanGrpQuestBackGrid" >
          		</span>
        		</td>
      		</tr>
     </table>
     </DIV>    
		  <INPUT CLASS=cssbutton VALUE="扫描件查询" TYPE=button onclick="EasyScanQuery();">
		  <span  id= "GrpQuest11" style= "display: 'none'">
		   <INPUT CLASS=cssbutton VALUE="回销问题件" TYPE=button onclick="Reply();">
		  </span>
      <input type=hidden name=GrpContNo>
      <input type=hidden name=SerialNo>
      <input type=hidden name=IssueCode>
      <input type=hidden name=IssueName>
      <input type=hidden name=IssueRuleCode>
      <input type=hidden name=IssueRuleCode1 >
      <input type=hidden name=BackObjType>
      <input type=hidden name=BackObj>
      <input type=hidden name=IssueCont>
      <input type=hidden name=NeedPrint>
      <input type=hidden name=fmtransact>
      <input type=hidden name=state>
      <input type=hidden name=prtseq>
      <input type=hidden  name=GUWState>
      <input type=hidden name=GUWIdea>
      <input type=hidden name="MissionID" value="">
			<input type=hidden name="SubMissionID" value="">
			<input type=hidden name="LoadFlag" value="">
			<input type=hidden name="Operator" value="">
	 </Div>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>