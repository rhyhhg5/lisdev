//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var ContNo;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  //var testPol = PolGrid.getRowColData();	
  //alert(tSel);
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    //showSubmitFrame(mDebug);

		//arrReturn = getQueryResult();
		//ContNo=arrReturn[0][0];
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tMissionID = PolGrid.getRowColData(tSel-1,13);
		tSubMissionID = PolGrid.getRowColData(tSel-1,14);
		tPrtNo = PolGrid.getRowColData(tSel-1,12);
		tContNo = PolGrid.getRowColData(tSel-1,15);
		//alert(ContNo);
		
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "../f1print";
		if(RePrint==1){
			fmSave.target = "fraSubmit";
			}
		fmSave.action="./UWResultSave.jsp?RePrintFlag="+RePrint;
		fmSave.submit();
		showInfo.close();
		
	}
}
//pdf提交，保存按钮对应操作
function printPolpdf()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  //var testPol = PolGrid.getRowColData();	
  //alert(tSel);
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
	}
	else
	{
		arrReturn = getQueryResult();
			if( null == arrReturn ) {
			  alert("无效的数据");
		    return;
		   }
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		//arrReturn = getQueryResult();
		//ContNo=arrReturn[0][0];
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tMissionID = PolGrid.getRowColData(tSel-1,13);
		tSubMissionID = PolGrid.getRowColData(tSel-1,14);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		tContNo = PolGrid.getRowColData(tSel-1,15);
		//alert(ContNo);
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "fraSubmit";
		fmSave.action="./PrintPDFSave.jsp?Code=005&RePrintFlag="+RePrint;
		fmSave.submit();
		showInfo.close();
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");	
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
    tContNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?ContNo="+tContNo;
}


// 查询按钮
function easyQueryClick()
{

	initPolGrid();
	// 书写SQL语句
	if(!verifyInput2())
	return false;
	var strSQL = "";	
	var tSeltype="";
	if (fm.all('SelType').value=="0")
			{
				tSeltype="and a.PrtSeq not in (select doccode from es_doc_main where subtype='TB21' )";
			}
	else if(fm.all('SelType').value=="1")
			{
				tSeltype="and a.PrtSeq in (select doccode from es_doc_main where subtype='TB21' )";
			}
	else
			{
				tSeltype="";
			}
	var tStateFlag="";
	if(fm.all('StateFlag').value=="null"||fm.all('StateFlag').value==null||fm.all('StateFlag').value=="")
 	 		{
    			tStateFlag="";
 	 		}
  else{
			if (fm.all('StateFlag').value=="0")
	  			{
		  				tStateFlag=" and a.stateflag='0' ";
	  			}
	  	else if (fm.all('StateFlag').value=="9")
	  			{
		 	 				tStateFlag="";
	  			}
	  	else
	  			{
	  			tStateFlag=" and a.stateflag<>'0' ";
	  			}
			}	
	 //2014-11-4  杨阳
	//把新的业务员编码转成旧的
    var agentCodesql ="";
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		agentCodesql=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	
	// 书写SQL语句
	 strSQL = "select a.PrtSeq,b.prtno,b.polapplydate,b.CvaliDate,b.AppntName,b.prem,(select getUniteCode(b.AgentCode) from dual),b.managecom,a.makedate,"
		      + "'',''"
		      + ",'','','',b.contno from loprtmanager a,LCCont b where 1=1 "
					+ " and a.code='05'"
                    + " and b.uwflag in ('4', '9') "
					+ " and a.otherno=b.contno "
					+ " and a.ManageCom like '" + fm.ManageCom.value + "%' "
	        //修改查询语句    zhangjianbao    2007-10-30
		      + getWherePart('b.AgentGroup', 'agentGroup') 
	        + getWherePart('b.prtno', 'PrtNo') 
					//+ getWherePart('b.UWFlag', 'UWFlag') 
	        + getWherePart('a.otherno', 'ContNo') 
					//+ getWherePart('b.ManageCom', 'ManageCom', 'like')//2007-10-22
//					+ getWherePart('b.AgentCode','AgentCode')
			    //***** 修改查询条件为时间段  zhangjianbao  2007-10-30 *****
			    //+ getWherePart('a.Makedate','MakeDate')
			    + getWherePart('a.Makedate + 1 day','MakeStartDate', '>')
			    + getWherePart('a.Makedate - 1 day','MakeEndDate', '<')
			    //**********************************************************
			    + getWherePart('b.AppntName','AppntName')
			    + getWherePart('b.CvaliDate','CvaliDate')
			    + getWherePart('b.polapplydate','PolApplyDate')
			    + " and a.stateflag='0' "
			    + agentCodesql
			    //增加按营销机构排序    zhangjianbao    2007-10-30
			    + " order by agentGroup with ur "
					;
	if(RePrint=="1")
	{
		strSQL = "select a.PrtSeq,b.prtno,b.polapplydate,b.CvaliDate,b.AppntName,b.prem,(select getUniteCode(b.AgentCode) from dual),b.managecom,a.makedate,"
		      + "(select makedate from es_doc_main where doccode=a.PrtSeq and subtype='TB21'),(case when a.stateflag='0' then '未打' else '已打' end) "
		      + ",'','','',b.contno from loprtmanager a,LCCont b where 1=1 "
					+ " and a.code='05'"
					+ " and a.otherno=b.contno "
					+ " and a.ManageCom like '" + fm.ManageCom.value + "%' "
					+ " and not exists (select 1 from es_doc_main where a.PrtSeq=doccode and subtype='TB21')"
	            //修改查询语句zhangjianbao    2007-10-30
		        + getWherePart('b.AgentGroup', 'agentGroup') 
	            + getWherePart('b.prtno', 'PrtNo') 
				//+ getWherePart('b.UWFlag', 'UWFlag') 
	            + getWherePart('a.otherno', 'ContNo') 
//			    + getWherePart('b.AgentCode','AgentCode')
			    //***** 修改查询条件为时间段  zhangjianbao  2007-10-30 *****
			    //+ getWherePart('a.Makedate','MakeDate')
			    + getWherePart('a.Makedate + 1 day','MakeStartDate', '>')
			    + getWherePart('a.Makedate - 1 day','MakeEndDate', '<')
			    //**********************************************************
			    + getWherePart('b.AppntName','AppntName')
			    + getWherePart('b.CvaliDate','CvaliDate')
			    + getWherePart('b.polapplydate','PolApplyDate')
			    + tSeltype
			    + tStateFlag
			    + agentCodesql
			    //增加按营销机构排序    zhangjianbao    2007-10-30
			    + " order by agentGroup with ur "
					;
	}
	
	fm.querySql.value = strSQL;//2007-10-22 11:41:45
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的核保通知书！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 	arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 	//tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  	fm.agentGroup.value = arrResult[0][0];
  }
}

//下载投保单信息清单	2007-10-22
function printList()
{
	if(!verifyInput2())
	return false;
	var strSQL = "";	
	var tSeltype="";
	if (fm.all('SelType').value=="0")
	{
		tSeltype="and a.PrtSeq not in (select doccode from es_doc_main where subtype='TB21' )";
	}
	else if(fm.all('SelType').value=="1")
	{
		tSeltype="and a.PrtSeq in (select doccode from es_doc_main where subtype='TB21' )";
	}
	var tStateFlag="";
	if(fm.all('StateFlag').value=="null"||fm.all('StateFlag').value==null||fm.all('StateFlag').value=="")
	{
		tStateFlag="";
	}
  else
  {
		if (fm.all('StateFlag').value=="0")
		{
			tStateFlag=" and a.stateflag='0' ";
		}
		else if (fm.all('StateFlag').value=="9")
		{
			tStateFlag="";
		}
		else
		{
			tStateFlag=" and a.stateflag<>'0' ";
		}
	}	
	
	//2014-11-4  杨阳
	//把新的业务员编码转成旧的
    var agentCodesql ="";
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		agentCodesql=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	
	strSQL = "select b.prtno,b.polapplydate,b.CvaliDate,b.AppntName,b.prem,(select getUniteCode(b.AgentCode) from dual),b.managecom,a.makedate,"
		      + "'',''"
		      + ",'','','',b.contno from loprtmanager a,LCCont b where 1=1 "
					+ " and a.code='05'"
					+ " and a.otherno=b.contno "
					+ " and b.ManageCom like '" + fm.ManageCom.value + "%' "
		      + getWherePart('b.AgentGroup', 'agentGroup') 
	        + getWherePart('b.prtno', 'PrtNo') 
	        + getWherePart('a.otherno', 'ContNo') 
//					+ getWherePart('b.AgentCode','AgentCode')
			    + getWherePart('a.Makedate + 1 day','MakeStartDate', '>')
			    + getWherePart('a.Makedate - 1 day','MakeEndDate', '<')
			    + getWherePart('b.AppntName','AppntName')
			    + getWherePart('b.CvaliDate','CvaliDate')
			    + getWherePart('b.polapplydate','PolApplyDate')
			    + " and a.stateflag='0' "
			    + agentCodesql
			    + " order by agentGroup with ur "
					;
	if(RePrint=="1")
	{
		strSQL = "select b.prtno,b.polapplydate,b.CvaliDate,b.AppntName,b.prem,(select getUniteCode(b.AgentCode) from dual),b.managecom,a.makedate,"
		      + "(select makedate from es_doc_main where doccode=a.PrtSeq and subtype='TB21'),"
		      + "(case when a.stateflag='0' then '未打' else '已打' end), "
		      + "branchattr,name from loprtmanager a,LCCont b,LABranchGroup c "
		      + "where a.code='05' and a.otherno=b.contno and b.ManageCom like '" + fm.ManageCom.value + "%' and b.agentgroup = c.agentgroup "
		      + getWherePart('b.AgentGroup', 'agentGroup') 
	        + getWherePart('b.prtno', 'PrtNo') 
	        + getWherePart('a.otherno', 'ContNo') 
//					+ getWherePart('b.AgentCode','AgentCode')
			    + getWherePart('a.Makedate + 1 day','MakeStartDate', '>')
			    + getWherePart('a.Makedate - 1 day','MakeEndDate', '<')
			    + getWherePart('b.AppntName','AppntName')
			    + getWherePart('b.CvaliDate','CvaliDate')
			    + getWherePart('b.polapplydate','PolApplyDate')
			    + tSeltype
			    + tStateFlag
			    + agentCodesql
			    + " order by c.agentGroup with ur "
					;
	}
	
	fm.querySql.value = strSQL;
	
	fm.action = "PrintUWResult.jsp";
	fm.target = "_blank";
	fm.submit();
}

function afterCodeSelect(cName, Filed)
{	
  if(cName=='statuskind')
  {
    saleChnl = fm.saleChannel.value;
	}
}

function beforeCodeSelect()
{
	if(saleChnl == "") alert("请先选择销售渠道");
}

function newprintPolpdf()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  //var testPol = PolGrid.getRowColData();	
  //alert(tSel);
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
	}
	else
	{
		arrReturn = getQueryResult();
			if( null == arrReturn ) {
			  alert("无效的数据");
		    return;
		   }
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		//arrReturn = getQueryResult();
		//ContNo=arrReturn[0][0];
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tMissionID = PolGrid.getRowColData(tSel-1,13);
		tSubMissionID = PolGrid.getRowColData(tSel-1,14);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		tContNo = PolGrid.getRowColData(tSel-1,15);
		//alert(ContNo);
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		//fmSave.fmtransact.value = "PRINT";
		fmSave.target = "fraSubmit";
		fmSave.action="./PDFPrintSave.jsp?Code=05&OtherNo=" + tContNo + "&PrtSeq=" + tPrtSeq;
		fmSave.submit();
		showInfo.close();
	}
}
////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}