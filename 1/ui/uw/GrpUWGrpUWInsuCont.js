//程序名称：GrpUWInsuCont.js
//程序功能：团体既往理陪函数
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

/*********************************************************************
 *  该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

var turnPage = new turnPageClass();

//取的客户团体的名称
function easyQueryI()
{
	// 书写SQL语句
	var strSQL = "select GrpName";
	var arrRs;
	strSQL    += "  from LDGrp";
	strSQL    += " where CustomerNo='"+mAppntNo+"'";
	
	fm.all("GrpNo").value=mAppntNo;
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	
	//查询成功设置画面上的数据
	if(turnPage.strQueryResult)
	{
	  arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
	  fm.all("GrpName").value=arrRs[0][0];
	}
}

function GoBack()
{
	top.close();
}