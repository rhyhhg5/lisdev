<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GrpFeeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*,com.sinosoft.lis.db.LCContPlanDB"%>
<%
//接收信息，并作校验处理。
//输入参数
LCContPlanFactorySchema tLCContPlanFactorySchema = new LCContPlanFactorySchema();
LCContPlanFactorySet tLCContPlanFactorySet = new LCContPlanFactorySet();

LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
LCContPlanFactoryUI tLCContPlanFactoryUI = new LCContPlanFactoryUI();

//输出参数
CErrors tError = null;
String tRearStr = "";
String tRela = "";
String FlagStr = "Fail";
String Content = "";

//全局变量
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
System.out.println("tG.ComCode:"+tG.ComCode);
System.out.println("tG.ManageCom:"+tG.ManageCom);
System.out.println("tG.Operator:"+tG.Operator);
System.out.println("begin ...");

String tOperate="INSERT||MAIN";
String GrpContNo = request.getParameter("GrpContNo");	//集体合同号码
String ProposalGrpContNo = request.getParameter("ProposalGrpContNo");	//集体投保单号码
String tQFlag = request.getParameter("OperFlag");	//操作编码
System.out.println("GrpContNo=="+GrpContNo);
System.out.println("operFlag=="+tQFlag);
System.out.println("ProposalGrpContNo="+ProposalGrpContNo);
if(tQFlag.equals("risk"))
{
		String arrCount[] = null;
		String tContPlanCode[]= null;	//计划编码
		String tRiskCode[]= null;	//险种编码
		String tFactoryType[]= null;	//要素类型
		String tOtherNo[]= null;	//目标类型
		String tFactory[] = null;	//要素计算编码
		String tCalRemark[] = null;	//要素内容
		String tParams[]= null;	//要素值
		String tContPlanName[] = null;	//计划名称
		String tRiskVersion[]= null;	//险种版本
		String tFactoryName[]= null ;	//计算编码名称
		String tMainRiskCode[]= null;	//主险编码
		String tMainRiskVersion[]= null;	//主险版本
		String tFactoryCode = "";
		String tFactorySubCode = "";
		String tPlanType="" ;
		//取得险种管理费明细
		int lineCount = 0;
		String tRiskFlag = request.getParameter("RiskFlag");
		if(tRiskFlag.equals("UW"))
		{
				arrCount = request.getParameterValues("RiskFactorGridNo");
				if (arrCount != null){
					tContPlanCode = request.getParameterValues("RiskFactorGrid1");	//计划编码
					tRiskCode = request.getParameterValues("RiskFactorGrid2");	//险种编码
					tFactoryType= request.getParameterValues("RiskFactorGrid3");	//要素类型
					tOtherNo = request.getParameterValues("RiskFactorGrid4");	//目标类型
					tFactory = request.getParameterValues("RiskFactorGrid5");	//要素计算编码
					tCalRemark = request.getParameterValues("RiskFactorGrid6");	//要素内容
					tParams = request.getParameterValues("RiskFactorGrid7");	//要素值
					tContPlanName = request.getParameterValues("RiskFactorGrid10");	//计划名称
					tRiskVersion = request.getParameterValues("RiskFactorGrid11");	//险种版本
					tFactoryName = request.getParameterValues("RiskFactorGrid12");	//计算编码名称
					tMainRiskCode = request.getParameterValues("RiskFactorGrid13");	//主险编码
					tMainRiskVersion = request.getParameterValues("RiskFactorGrid14");	//主险版本
					tFactoryCode = "";
					tFactorySubCode = "";
					tPlanType = "4";
				}
		}
		else if(tRiskFlag.equals("MP"))
		{
				arrCount = request.getParameterValues("MPRiskFactorGridNo");
				if (arrCount != null){
					tContPlanCode = request.getParameterValues("MPRiskFactorGrid1");	//计划编码
					tRiskCode = request.getParameterValues("MPRiskFactorGrid2");	//险种编码
					tFactoryType = request.getParameterValues("MPRiskFactorGrid3");	//要素类型
					tOtherNo= request.getParameterValues("MPRiskFactorGrid4");	//目标类型
					tFactory = request.getParameterValues("MPRiskFactorGrid5");	//要素计算编码
					tCalRemark = request.getParameterValues("MPRiskFactorGrid6");	//要素内容
					tParams = request.getParameterValues("MPRiskFactorGrid7");	//要素值
					tContPlanName = request.getParameterValues("MPRiskFactorGrid10");	//计划名称
					tRiskVersion = request.getParameterValues("MPRiskFactorGrid11");	//险种版本
					tFactoryName = request.getParameterValues("MPRiskFactorGrid12");	//计算编码名称
					tMainRiskCode = request.getParameterValues("MPRiskFactorGrid13");	//主险编码
					tMainRiskVersion = request.getParameterValues("MPRiskFactorGrid14");	//主险版本
					tFactoryCode = "";
					tFactorySubCode = "";
					tPlanType = "5";
				}		
		}
			lineCount = arrCount.length; //行数
			
			for(int i=0;i<lineCount;i++){
				tLCContPlanFactorySchema = new LCContPlanFactorySchema();
			
				tLCContPlanFactorySchema.setGrpContNo(GrpContNo);
				tLCContPlanFactorySchema.setProposalGrpContNo(ProposalGrpContNo);
				tLCContPlanFactorySchema.setContPlanCode(tContPlanCode[i]);
				tLCContPlanFactorySchema.setRiskCode(tRiskCode[i]);
				tLCContPlanFactorySchema.setFactoryType(tFactoryType[i]);
				tLCContPlanFactorySchema.setOtherNo(tOtherNo[i]);
				tFactoryCode = tFactory[i].substring(0,6);
				tFactorySubCode = tFactory[i].substring(6);
				System.out.println(tFactory[i]+"****"+tFactoryCode+"****"+tFactorySubCode);
				tLCContPlanFactorySchema.setFactoryCode(tFactoryCode);
				tLCContPlanFactorySchema.setFactorySubCode(tFactorySubCode);
				tLCContPlanFactorySchema.setCalRemark(tCalRemark[i]);
				tLCContPlanFactorySchema.setParams(tParams[i]);
				tLCContPlanFactorySchema.setContPlanName(tContPlanName[i]);
				tLCContPlanFactorySchema.setRiskVersion(tRiskVersion[i]);<input type="password" >
				tLCContPlanFactorySchema.setFactoryName(tFactoryName[i]);
				tLCContPlanFactorySchema.setMainRiskCode(tMainRiskCode[i]);
				tLCContPlanFactorySchema.setMainRiskVersion(tMainRiskVersion[i]);
				tLCContPlanFactorySchema.setPlanType(tPlanType);     //4--为核保定价,5--产品定价
				tLCContPlanFactorySet.add(tLCContPlanFactorySchema);
				System.out.println("记录"+i+"放入Set！");
			}
	
		System.out.println("end ...");
		
		// 准备传输数据 VData
		VData tVData = new VData();
		FlagStr="";
		
		tVData.add(tG);
		tVData.addElement(tLCContPlanFactorySet);
		tVData.addElement(ProposalGrpContNo);
		tVData.addElement(GrpContNo);
		
		try{
			System.out.println("this will save the data!!!");
			tLCContPlanFactoryUI.submitData(tVData,tOperate);
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		
		if (!FlagStr.equals("Fail")){
			tError = tLCContPlanFactoryUI.mErrors;
			if (!tError.needDealError()){
				Content = " 保存成功! ";
				FlagStr = "Succ";
			}
			else{
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
}
else if(tQFlag.equals("duty"))
{
		System.out.println("begin ...");
		SaveAskDutyFactorUI tSaveAskDutyFactorUI = new SaveAskDutyFactorUI();
		TransferData mTransferData = new TransferData();
		
		String tRiskCode = request.getParameter("RiskCode");
		String tContPlanCode = request.getParameter("ContPlanCode");
		String tDutyCode = request.getParameter("DutyCode");
		String tDutyFlag = request.getParameter("DutyFlag");
		String arrCount1[]= null;
		String tFactorCode[]= null ;
		String tFactorName[] = null;
		String tFactorValue[]= null;
		String tChooseFlag[] = null;
		String tPlanType ="" ;
		System.out.println("tRiskCode=="+tRiskCode);
		System.out.println("tContPlanCode=="+tContPlanCode);
		System.out.println("tDutyCode=="+tDutyCode);
					  System.out.println("========"+tDutyFlag);
		//保险计划要素信息
		int lineCount1 = 0;
		if(tDutyFlag.equals("UW"))
		{
			  arrCount1 = request.getParameterValues("DutyFactorGridNo");

				if (arrCount1 != null){
					tFactorCode = request.getParameterValues("DutyFactorGrid1");	//险种编码
					tFactorName = request.getParameterValues("DutyFactorGrid2");	//要素名称
					tFactorValue = request.getParameterValues("DutyFactorGrid3");	//责任编码
					tChooseFlag = request.getParameterValues("DutyFactorGrid4");	//责任编码
					tPlanType = "4";
				}	
		}
		else if(tDutyFlag.equals("MP"))
		{
				arrCount1 = request.getParameterValues("MPDutyFactorGridNo");
				if (arrCount1 != null){
					tFactorCode = request.getParameterValues("MPDutyFactorGrid1");	//险种编码
					tFactorName = request.getParameterValues("MPDutyFactorGrid2");	//要素名称
					tFactorValue = request.getParameterValues("MPDutyFactorGrid3");	//责任编码
					tChooseFlag = request.getParameterValues("MPDutyFactorGrid4");	//责任编码
					tPlanType ="5";
				}			
		}
		
			lineCount1 = arrCount1.length; //行数
			
			for(int i=0;i<lineCount1;i++){
				LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
				
				tLCContPlanDutyParamSchema.setGrpContNo(GrpContNo);
				tLCContPlanDutyParamSchema.setProposalGrpContNo(GrpContNo);
				tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
				tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
				tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
				tLCContPlanDutyParamSchema.setDutyCode(tDutyCode);
				System.out.println("------"+tFactorCode[i]);
				tLCContPlanDutyParamSchema.setCalFactor(tFactorCode[i]);
				tLCContPlanDutyParamSchema.setCalFactorType(tChooseFlag[i]);
				tLCContPlanDutyParamSchema.setCalFactorValue(tFactorValue[i]);
				System.out.println("---------hyq=="+tPlanType);
				tLCContPlanDutyParamSchema.setPlanType(tPlanType);          //4--为核保定价,5--产品定价
				tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
			}
		
				// 准备传输数据 VData
		VData tVData = new VData();
		FlagStr="";
		
		mTransferData.setNameAndValue("LCContPlanDutyParamSet",tLCContPlanDutyParamSet);
		mTransferData.setNameAndValue("GrpContNo",GrpContNo);
		mTransferData.setNameAndValue("RiskCode",tRiskCode);
		tVData.add(tG);
		tVData.add(mTransferData);
		
		try{
			System.out.println("this will save the data!!!");

			tSaveAskDutyFactorUI.submitData(tVData,"submit");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		
		if (!FlagStr.equals("Fail")){
			tError = tSaveAskDutyFactorUI.mErrors;
			if (!tError.needDealError()){
				Content = " 保存成功! ";
				FlagStr = "Succ";
			}
			else{
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
		System.out.println("end ...");
}else if(tQFlag.equals("remark"))//“保存”按钮点击事件处理
{
	String tRiskFlag = request.getParameter("RiskFlag");
	      LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
        String planType = request.getParameter("PlanTypeNew");
        String grpContNo = request.getParameter("GrpContNoNew");
        String planCode= request.getParameter("PlanCodeNew");
        
        tLCContPlanSchema.setGrpContNo(grpContNo);
        tLCContPlanSchema.setContPlanCode(planCode);
        tLCContPlanSchema.setPlanType(planType);
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        tLCContPlanDB.setSchema(tLCContPlanSchema);
        boolean hasInfo = tLCContPlanDB.getInfo();
	if(tRiskFlag.trim().equals("UW")){//来自人工核保的操作

        if(!hasInfo){
            System.out.println("========================ERR-hasInfo=====================");
        }else{
        		String remark = request.getParameter("UWIdea");
            tLCContPlanSchema = tLCContPlanDB.getSchema();
            tLCContPlanSchema.setRemark2(remark);

        }
   }else if(tRiskFlag.trim().equals("MP")){//来自产品定价的操作
   		  if(!hasInfo){
            System.out.println("========================ERR-hasInfo=====================");
        }else{
        		String remark = request.getParameter("MPIdea");
            tLCContPlanSchema = tLCContPlanDB.getSchema();
            tLCContPlanSchema.setRemark2(remark);

        }
   }
            VData tVData = new VData();
            tVData.addElement(tLCContPlanSchema);
            tVData.addElement(tG);
            LCContPlanNewUI tLCContPlanNewUI = new LCContPlanNewUI();
						try{
	            if(tLCContPlanNewUI.submitData(tVData,"UPDATE")){
	            	
	            }else{
	                System.out.println("+++++++++++++++++++++++ERR+++++++++++++++++");
	            }
	            FlagStr = "Succ";
	            
            }catch(Exception ex){
            	FlagStr = "Fail";
            }
						if (!FlagStr.equals("Fail")){
							tError = tLCContPlanNewUI.mErrors;
							if (!tError.needDealError()){
								Content = " 保存成功! ";
								FlagStr = "Succ";
							}
							else{
								Content = " 保存失败，原因是:" + tError.getFirstError();
								FlagStr = "Fail";
							}
						}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>