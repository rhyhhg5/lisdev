<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWManuHealthChk.jsp
//程序功能：承保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  CErrors cError = null;
  String FlagStr = "Succ";
  String Content = "";
  
  GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");	  
  
	LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
	LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
	
	String tContNo = request.getParameter("ContNo");
	String tInsureNo = request.getParameter("InsureNo");
	String tHospital = request.getParameter("Hospital");
	String tPrtNo =  request.getParameter("PrtNo");
	String tIfEmpty = request.getParameter("IfEmpty");
	String tEDate = request.getParameter("EDate");
	String tNote = request.getParameter("Note");
	String tTestGrpCode = request.getParameter("GrpTestCode");
	String tSerialNo[] = request.getParameterValues("HealthGridNo");
	String thealthcode[] = request.getParameterValues("HealthGrid1");
	String thealthname[] = request.getParameterValues("HealthGrid2");
	String mIfEmpty[] = request.getParameterValues("HealthGrid3");
	//String mIfEmpty[] = request.getParameterValues("HealthGrid3");
	
	//针对中文编码，进行转换。
    for(int a = 0; a < mIfEmpty.length; a++)
    {
        String tmpStrmIfEmpty = StrTool.GBKToUnicode(mIfEmpty[a]);
        if(tmpStrmIfEmpty.equals("Y") || tmpStrmIfEmpty.equals("是"))
        {
            tIfEmpty = "Y";
            break;
        }
        else if(tmpStrmIfEmpty.equals("N") || tmpStrmIfEmpty.equals("否"))
        {
            tIfEmpty = "N";
        }
        else
        {
            tIfEmpty = "";
        }
    }
	//---------------------------------
	
	boolean flag = true;
	int ChkCount = 0;
	if(tSerialNo != null)
	{		
		ChkCount = tSerialNo.length;
	}
	System.out.println("count:"+ChkCount);
	if (ChkCount == 0  || tIfEmpty.equals(""))
	{
		Content = "体检资料信息录入不完整!";
		FlagStr = "Fail";
		flag = false;
		System.out.println("111");
	}
	else
	{
    tLCPENoticeSchema.setContNo(tContNo);	    		
  	tLCPENoticeSchema.setPEAddress(tHospital);
  	tLCPENoticeSchema.setPEDate(tEDate);
  	tLCPENoticeSchema.setPEBeforeCond(tIfEmpty);
  	tLCPENoticeSchema.setRemark(tNote);
  	tLCPENoticeSchema.setCustomerNo(tInsureNo);

  	if (ChkCount > 0)
  	{
	    for (int i = 0; i < ChkCount; i++)
			{
				if (!thealthcode[i].equals(""))
				{
	  			LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();
	  			tLCPENoticeItemSchema.setContNo(tContNo);
				  tLCPENoticeItemSchema.setPEItemCode( thealthcode[i]);
				  tLCPENoticeItemSchema.setPEItemName( thealthname[i]);
				  tLCPENoticeItemSchema.setFreePE("N");	
				  tLCPENoticeItemSchema.setTestGrpCode(tTestGrpCode);   	    
		    	tLCPENoticeItemSet.add( tLCPENoticeItemSchema );
		      System.out.println("i:"+i);
    		  System.out.println("healthcode:"+thealthcode[i]);	    	
		   		flag = true;
				}
			}
			    
		}
		else
		{
			Content = "传输数据失败!";
			flag = false;
		}
	}
	
	String tMissionID = request.getParameter("MissionID");
	
	TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("MissionID",tMissionID );
	
	System.out.println("flag:"+flag);
  if (flag == true)
  {
    VData tVData = new VData();
    tVData.add(tGlobalInput);
    tVData.add(tLCPENoticeSchema);
    tVData.add(tLCPENoticeItemSet);
    tVData.add(tTransferData);
    
		//数据传输
		UWHealthSaveUI ui   = new UWHealthSaveUI();
		if (ui.submitData(tVData,"") == false)
		{
			Content = ui.mErrors.getErrContent();
			FlagStr = "Fail";
		}
	  else
	  {
			Content = "保存成功";
			FlagStr = "Succ";
	  }
	} 
	
	Content = PubFun.changForHTML(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
