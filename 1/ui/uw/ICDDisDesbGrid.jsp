<!--本文件为疾病告知整理的MulLine初始化-->

<script language="JavaScript">

//本文件可能在核保时和国际业务录入被保人时调用，
//若在国际业务录入被保人时，不需要显示被保人号和被保人名字，
//displayControl用来控制是否显示，true: 显示：
var displayControl = true;
try
{
  //intlContInsuredInput在国际业务录入被保人页面中定义为1
  if(intlContInsuredInput == 1)
  {
    displayControl = false;
  }
}
catch(ex)
{}

<!--责任信息列表的初始化-->
function initDisDesbGrid()
{                              
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="客户号码";    	//列名
    iArray[1][1]=(displayControl ? "60px" : "0px");            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="insuredno"; 
    iArray[1][5]="1|2";
    iArray[1][6]="0|1";
    iArray[1][15]="ContNo"; 
    iArray[1][16]=fm.all('ContNO').value; 
    iArray[1][19]="1"; 
    
    
    iArray[2]=new Array();
    iArray[2][0]="客户姓名";    	//列名
    iArray[2][1]=(displayControl ? "60px" : "0px");            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=(displayControl ? 0 : 3);              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[3]=new Array();
    iArray[3][0]="疾病症状";    	//列名
    iArray[3][1]="260px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[4]=new Array();
    iArray[4][0]="疾病结论";         		//列名
    iArray[4][1]="260px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    //iArray[2][4]="ICDName";
    //iArray[2][9]="疾病结论|len<=120";
    //iArray[2][18]=300;
  
    //iArray[3]=new Array();
    //iArray[3][0]="ICD编码";         		//列名
    //iArray[3][1]="100px";            		//列宽
    //iArray[3][2]=60;            			//列最大值
    //iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    //iArray[3][4]="ICDCode";
  	//iArray[3][5]="3|5"; 
  	//iArray[3][6]="0|1"; 
    //iArray[3][9]="ICD编码|len<=20";
    //iArray[3][15]="ICDName";
    //iArray[3][17]="2";
    //iArray[3][18]=300;
    
    iArray[5]=new Array();
    iArray[5][0]="ICD编码";         		//列名
    iArray[5][1]="100px";            		//列宽
    iArray[5][2]=60;            			//列最大值
    iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][7]="querySpanNo"; 
    iArray[5][9]="ICD编码|len<=20";
    iArray[5][15]="ICDName";
    iArray[5][17]="2";
    iArray[5][18]=300;
   
   	iArray[6]=new Array();
    iArray[6][0]="内部子序列号";    	//列名
    iArray[6][1]="260px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="标准名称";    	//列名
    iArray[7][1]="100px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="风险代码";    	//列名
    iArray[8][1]="100px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][4]="diseasecode"; 
    iArray[8][5]="8|9|5"; 
  	iArray[8][6]="0|1|2";
    iArray[8][9]="ICD编码|len<=20";
    iArray[8][15]="ICDName";
    iArray[8][17]="9";
    iArray[8][18]=300;
    
    iArray[9]=new Array();
    iArray[9][0]="风险描述";    	//列名
    iArray[9][1]="100px";            		//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="目前情况";    	//列名
    iArray[10][1]="100px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=1;              			//是否允许输入,1表示允许，0表示不允许
  
    DisDesbGrid = new MulLineEnter( "fm" , "DisDesbGrid" ); 
    //这些属性必须在loadMulLine前                            
    DisDesbGrid.mulLineCount = 0;
    DisDesbGrid.displayTitle = 1;
    DisDesbGrid.canChk = 0;
  	DisDesbGrid.hiddenSubtraction=0; 
    DisDesbGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //DisDesbGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex);
  }
}

</script>

<table>
	<tr>
    <td class=common>
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);">
    </td>
		<td class= titleImg>疾病结果</td>                            
	</tr>	
</table>
<Div  id= "divUWDis" style= "display: ''">
  <table  class= common>
 		<tr  class= common>
	  	<td text-align: left colSpan=1 >
		    <span id="spanDisDesbGrid">
		    </span> 
	  	</td>
	  </tr>
	</table>
  <!--在国际业务中，告知整理列表的id定义为divPage4-->	
  <Div id= "divPage4" align="center" style= "display: 'none' ">
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage4.firstPage();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage4.previousPage();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage4.nextPage();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage4.lastPage();"> 
	</Div>
</div>