<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：UWHistoryInput.jsp
	//程序功能：既往核保查询
	//创建日期：2012-10-11
	//创建人:  张成轩
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tPrtNo = "";
	String tContNo = "";
	String tMissionID = "";
	String tSubMissionID = "";
	String tActivityID = "";
	String tLoadFlag = "";
	tPrtNo = request.getParameter("PrtNo");
	tContNo = request.getParameter("ContNo");
	tMissionID = request.getParameter("MissionID");
	tSubMissionID = request.getParameter("SubMissionID");
	tActivityID = request.getParameter("ActivityID");
	tLoadFlag = request.getParameter("LoadFlag");
	session.putValue("ContNo", tContNo);
%>
<html>
	<%
		//个人下个人
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput) session.getValue("GI");
	%>
	<script>
	var ContNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";
	var tActivityID = "<%=tActivityID%>";
	var tLoadFlag = "<%=tLoadFlag%>";
	var PrtNo = "<%=tPrtNo%>";
	var uwgrade = "";
	var appgrade = "";
	var uwpopedomgrade = "";
	var codeSql = "code";
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="UWHistoryInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="UWHistoryInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit" action="./ManuUWCho.jsp">
			<!-- 保单查询条件 -->
			<div id="divSearch">


			</div>
			<Input type="hidden" name=State value="">
			<INPUT type="hidden" name="Operator" value="">
			<!--合同信息-->
			<DIV id=DivLCContButton STYLE="display: 'none'">
				<table id="table1">
					<tr>
						<td>
							<img src="../common/images/butExpand.gif" style="cursor: hand;"
								OnClick="showPage(this,DivLCCont);">
						</td>
						<td class="titleImg">
							管理信息
						</td>
					</tr>
				</table>
			</DIV>
			<div id="DivLCCont" STYLE="display: 'none'">
				<table class="common" id="table2">
					<tr CLASS="common">
						<td CLASS="title">
							印刷号码
						</td>
						<td CLASS="input" COLSPAN="1">
							<input NAME="PrtNo" VALUE CLASS="readonly" readonly TABINDEX="-1"
								MAXLENGTH="14">
						</td>
						<td CLASS="title">
							申请日期
						</td>
						<td CLASS="input" COLSPAN="1">
							<input NAME="PolApplyDate" VALUE CLASS="readonly" readonly
								TABINDEX="-1" MAXLENGTH="14">
						</td>
						<td CLASS="title">
							生效日期
						</td>
						<td CLASS="input" COLSPAN="1">
							<input NAME="CValiDate" VALUE CLASS="readonly" readonly
								TABINDEX="-1" MAXLENGTH="14">
						</td>
						<td CLASS="title">
							管理机构
						</td>
						<td CLASS="input" COLSPAN="1">
							<input NAME="ManageCom_ch" MAXLENGTH="10" CLASS="readonly"
								readonly>
							<input type=hidden name=ManageCom>
						</td>
					</tr>
					<tr CLASS="common">
						<td CLASS="title">
							销售渠道
						</td>
						<td CLASS="input" COLSPAN="1">
							<input NAME="SaleChnl_ch" CLASS="readonly" readonly MAXLENGTH="2">
							<input type=hidden name=SaleChnl>
						</td>
						<td CLASS="title">
							营销机构
						</td>
						<td CLASS="input" COLSPAN="1">
							<input NAME="AgentGroup_ch" CLASS="readonly" readonly
								MAXLENGTH="2">
							<input type=hidden name=AgentGroup>
						</td>
						<td CLASS="title">
							业务员代码
						</td>
						<td CLASS="input" COLSPAN="1">
							<input CLASS="readonly" readonly name=AgentCode>
						</td>
						<td CLASS="title">
							业务员
						</td>
						<td CLASS="input" COLSPAN="3">
							<input NAME="AgentCode_ch" MAXLENGTH="10" CLASS="readonly"
								readonly>
						</td>
					</tr>
					<tr class=common>
						<td CLASS="title">
							特别约定
						</td>
						<td CLASS="input" COLSPAN="7">
							<input NAME="Remark" CLASS="readonly4" readonly MAXLENGTH="255"
								COLSPAN="7">
						</td>
					</tr>
				</table>
				<input NAME="ProposalContNo" type=hidden CLASS="readonly" readonly
					TABINDEX="-1" MAXLENGTH="20">
				<input NAME="AgentGroup" type=hidden CLASS="readonly" readonly
					TABINDEX="-1" MAXLENGTH="12">
				<input NAME="AgentCode1" type=hidden MAXLENGTH="10" CLASS="readonly"
					readonly>
				<input NAME="AgentCom" type=hidden CLASS="readonly" readonly>
				<input NAME="AgentType" type=hidden CLASS="readonly" readonly>
			</div>
			<div id=DivLCSendTrance STYLE="display: 'none'">
			</div>
			<DIV id=DivLCAppntIndButton STYLE="display: 'none'">
				<!-- 投保人信息部分 -->
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
								OnClick="showPage(this,DivLCAppntInd);">
						</td>
						<td class=titleImg>
							投保人信息
						</td>
					</tr>
				</table>
			</DIV>

			<DIV id=DivLCAppntInd STYLE="display: 'none'">
				<table class=common>
					<TR class=common>
						<TD class=title COLSPAN="1">
							姓名
						</TD>
						<TD class=input>
							<Input CLASS="readonly" readonly name=AppntName>
						</TD>
						<TD class=title>
							性别
						</TD>
						<TD class=input COLSPAN="1">
							<Input name=AppntSex_ch CLASS="readonly">
							<input type=hidden name=AppntSex>
						</TD>
						<TD class=title COLSPAN="1">
							年龄
						</TD>
						<TD class=input COLSPAN="1">
							<input CLASS="readonly" readonly name="AppntBirthday">
						</TD>

						<TD class=title COLSPAN="1">
							职业
						</TD>
						<TD class=input COLSPAN="1">
							<input CLASS="readonly" readonly name="OccupationCode_ch">
							<input type=hidden name=OccupationCode>
						</TD>
					</tr>
					<tr class=common>
						<TD class=title>
							国籍
						</TD>
						<TD class=input COLSPAN="1">
							<input CLASS="readonly" readonly name="NativePlace_ch">
							<input type=hidden name=NativePlace>
						</TD>
						<TD class=title>
							联系地址
						</TD>
						<TD class=input COLSPAN="1">
							<input CLASS="readonly" readonly name="AddressNo_ch">
							<input type=hidden name=AddressNo>
						</TD>

						<TD class=title>
							VIP标记
						</TD>
						<TD class=input COLSPAN="1">
							<Input class="readonly" readonly name=VIPValue>

						</TD>
						<TD class=title>
							黑名单标记
						</TD>
						<TD class=input COLSPAN="3">
							<Input class="readonly" readonly name=BlacklistFlag>

						</TD>
					</TR>
				</table>
				<Input CLASS="readonly" readonly type=hidden name="AppntNo">
				<Input CLASS="readonly" readonly type=hidden name="AddressNo">
			</DIV>
			<!-- 保单查询结果部分（列表） -->
			<DIV id=DivLCContInfo STYLE="display: ''">
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
								OnClick="showPage(this,divLCPol1);">
						</td>
						<td class=titleImg>
							投保单查询结果
						</td>
					</tr>
				</table>
			</Div>
			<Div id="divLCPol1" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanPolGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<div id="divAppInfo" style="display: ''" align=center>
			</div>
			<hr>
			<Div id="divMain" style="display: 'none'"></Div>
			<!--附加险-->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
							OnClick="return ;showPage(this,divLCPol2);">
					</td>
					<td class=titleImg>
						被保人信息
					</td>
				</tr>
			</table>
			<div id="divInsuredInfo" style="display: ''" align=center>
			</div>
			<Div id="divLCPol2" style="display: 'none'">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanPolAddGrid"> </span>
						</td>
					</tr>
				</table>
			</Div>
			<hr>
			<div id=divUWButton1 style="display: ''" align=center></div>
			<div id="displayInfoDiv" style="display: ''"></div>
			<table class=common border=0 width=100%>
				<tr>
					<td>
						<INPUT VALUE="自核报告" class=cssButton TYPE=button
							onclick="showNewUWSub();">
						<INPUT VALUE="扫描件查询" class=cssButton TYPE=button
							onclick="ScanQuery();">
						<input value="记 事 本" class=cssButton type=button
							onclick="showNotePad();" width="200">
						<input value="告知整理" name="ImpartTrim" class=cssButton type=button
							onclick="ImpartToICD();">
						<input value="问题件查询" class=cssButton type=button
							onclick="QuestBack();">
						<input value="体检查询" class=cssButton type=button
							onclick="showHealthQ();">
						<INPUT VALUE="契调查询" class=cssButton TYPE=button
							onclick="RReportQuery();">
						<INPUT VALUE="保单明细" class=cssButton TYPE=button
							onclick="showChangePlan();">
						<input value="MagNum结论报告" name="MagNumUW" class=cssButton type=button 
						    onclick="MagNumUWSub();">	
					</td>
				</tr>
			</table>

			<!--HR-->
			<Div id="divMainI" style="display: ''">
				<!--产品结论-->
				<Div id="divLCPol3" style="display: ''">
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanPolVerdictGrid"> </span>
							</td>
						</tr>
					</table>
				</Div>
				<HR>

				<input type="hidden" name="PrtNoHide" value="">
				<input type="hidden" name="PolNoHide" value="">
				<input type="hidden" name="MainPolNoHide" value="">
				<input type="hidden" name="NoHide" value="">
				<input type="hidden" name="TypeHide" value="">
				<input type="hidden" class=Common name=UWGrade value="">
				<input type="hidden" class=Common name=AppGrade value="">
				<input type="hidden" class=Common name=PolNo value="">
				<input type="hidden" class=Common name="ContNo" value="">
				<input type="hidden" class=Common name="YesOrNo" value="">
				<input type="hidden" class=Common name="MissionID" value="">
				<input type="hidden" class=Common name="ActivityID" value="">
				<input type="hidden" class=Common name="SubMissionID" value="">
				<input type="hidden" class=Common name="SubConfirmMissionID"
					value="">
				<input type="hidden" class=Common name=SubNoticeMissionID value="">
				<input type="hidden" class=Common name=UWPopedomCode value="">
				<input type="hidden" class=Common name="LoadFlag" value="">
				<input type="hidden" class=Common name="fmtransact" value="">
				<INPUT type="hidden" class=Common name="PSendUpFlag" value="">
				<INPUT type="hidden" class=Common name="SendType" value="">
				<INPUT type="hidden" class=Common name="SendFlag" value="">


				<Div id="divPolInfo" style="display: ''">
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanProposalGrid"> </span>
							</td>
						</tr>
					</table>
				</Div>
				<div id="divUWInfo" style="display: ''">
					<table class=common align=center>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanUWInfoGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
				<div id="divUWResult" style="display: ''">
					<!-- 核保结论 -->
					<table class=common border=0 width=100%>
						<tr>
							<td class=titleImg align=center>
								核保结论：
							</td>
						</tr>
					</table>

					<table class=common align=center>
						<tr class=common>
							<TD class=title>
								核保结论
							</td>
							<td class=input>
								<Input class=codeNo name=uwState verify="核保结论|code:uwstate"
									ondblclick="return showCodeList('uwstate',[this,uwStateName],[0,1]);"
									onkeyup="return showCodeListKey('uwstate',[this,uwStateName],[0,1]);"><input class=codename name=uwStateName readonly=true>
							</TD>
							<TD class=title>
							</TD>
							<td class=input>
								是否自动下发核保通知书
								<input class=box type=checkbox name=NeedSendNoticeChk checked
									OnClick="if(this.checked) fm.NeedSendNotice.value='1'; else fm.NeedSendNotice.value='0';">
								<input type="hidden" class=Common name=NeedSendNotice value="1">
							</td>
						</TR>
					</table>
					<table class=common>

						<tr>
							<TD height="24" class=title>
								核保意见
							</TD>
							<td></td>
						</tr>
						<tr>
							<TD class=input>
								<textarea name="UWIdea" cols="100%" rows="5" witdh=100%
									class="common" verify="核保意见|len<=255"></textarea>
							</TD>
							<td></td>
						</tr>
					</table>
				</div>
				<div id="divUWSave" style="display: ''">
				</div>
				<div id="divUWAgree" style="display: 'none'">
				</div>
				<div id="divChangeResult" style="display: 'none'">
				</div>
			</Div>
			<table class=common align=center>

			</table>
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
