<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWManuHealthChk.jsp
//程序功能：承保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.workflowengine.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%
  //输出参数
  CErrors tError = null;
  CErrors cError = null;
  String FlagStr = "";
  String Content = "";
  
	boolean flag = true;

  GlobalInput tGlobalInput = new GlobalInput();
  tGlobalInput=(GlobalInput)session.getValue("GI");	  
  if(tGlobalInput == null) {
	out.println("session has expired");
	return;
  }

  
  	// 投保单列表
	//LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
	//LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
	
	String tContNo = request.getParameter("ContNo");
	String tInsureNo = request.getParameter("InsureNo");
	String tHospital = request.getParameter("Hospital");
	String tPrtNo =  request.getParameter("PrtNo");
	String tIfEmpty = request.getParameter("IfEmpty");
	String tEDate = request.getParameter("EDate");
	String tNote = request.getParameter("Note");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tTestGrpCode = request.getParameter("GrpTestCode");
	String tSerialNo[] = request.getParameterValues("HealthGridNo");
	String thealthcode[] = request.getParameterValues("HealthGrid1");
	String thealthname[] = request.getParameterValues("HealthGrid2");
	String mIfEmpty[] = request.getParameterValues("HealthGrid3");
	//String mIfEmpty[] = request.getParameterValues("HealthGrid3");
	
	//for(int a=0;a<mIfEmpty.length;a++)
	//{
	//	if(mIfEmpty[a].equals("Y")||mIfEmpty[a].equals("是"))
	//	{
	//		tIfEmpty="Y";
	//		break;
	//	}
	//else if(mIfEmpty[a].equals("N")||mIfEmpty[a].equals("否"))
	//	{
	//		tIfEmpty="N";
	//	}
	//else
	//	{
	//		tIfEmpty="";
	//	}
	//}
	//
	//System.out.println("ContNo:"+tContNo);
	//System.out.println("Hospital:"+tHospital);
	//System.out.println("note:"+tNote);
	//System.out.println("ifempty:"+tIfEmpty);
	//System.out.println("insureno:"+tInsureNo);
	//System.out.println("EDATE:"+tEDate);
	//
	//boolean flag = true;
	//int ChkCount = 0;
	//if(tSerialNo != null)
	//{		
	//	ChkCount = tSerialNo.length;
	//}
	//System.out.println("count:"+ChkCount);
	//if (ChkCount == 0  || tIfEmpty.equals(""))
	//{
	//	Content = "体检资料信息录入不完整!";
	//	FlagStr = "Fail";
	//	flag = false;
	//	System.out.println("111");
	//}
	//else
	//{
	//	System.out.println("222");
	//    //体检资料一
	//	    tLCPENoticeSchema.setContNo(tContNo);	    		
	//    	tLCPENoticeSchema.setPEAddress(tHospital);
	//    	tLCPENoticeSchema.setPEDate(tEDate);
	//    	tLCPENoticeSchema.setPEBeforeCond(tIfEmpty);
	//    	tLCPENoticeSchema.setRemark(tNote);
	//    	tLCPENoticeSchema.setCustomerNo(tInsureNo);
  //
	//    	System.out.println("chkcount="+ChkCount);
	//    	if (ChkCount > 0)
	//    	{
	//    		for (int i = 0; i < ChkCount; i++)
	//		{
	//			if (!thealthcode[i].equals(""))
	//			{
	//	  			LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();
	//	  			tLCPENoticeItemSchema.setContNo(tContNo);
	//				  tLCPENoticeItemSchema.setPEItemCode( thealthcode[i]);
	//				  tLCPENoticeItemSchema.setPEItemName( thealthname[i]);
	//				  tLCPENoticeItemSchema.setFreePE("N");	
	//				  tLCPENoticeItemSchema.setTestGrpCode(tTestGrpCode);   	    
	//		    	tLCPENoticeItemSet.add( tLCPENoticeItemSchema );
	//		      System.out.println("i:"+i);
	//    		  System.out.println("healthcode:"+thealthcode[i]);	    	
	//		   		flag = true;
	//			}
	//		}
	//		    
	//	}
	//	else
	//	{
	//		Content = "传输数据失败!";
	//		flag = false;
	//	}
	//}
	
	System.out.println("flag:"+flag);
  	if (flag == true)
  	{
  	    //获取体检件信息。
  	    String tStrSql = "select * from LCPENotice lcpen "
  	        + " where lcpen.contno = '" + tContNo + "' "
  	        + " and not exists (select 1 from lwmission where missionprop1 = '" + tPrtNo + "' "
  	        + " and missionprop4 = lcpen.prtseq "
  	        + " and missionid = '" + tMissionID + "' "
  	        + " and activityid = '0000001106') "
  	        + " and not exists (select 1 from lbmission where missionprop1 = '" + tPrtNo + "' "
  	        + " and missionprop3 = lcpen.prtseq "
  	        + " and missionid = '" + tMissionID + "' "
  	        + " and activityid = '0000001106') ";
  	    System.out.println("tStrSql: " + tStrSql);
  	    LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
  	    LCPENoticeSet tLCPENoticeSet = tLCPENoticeDB.executeQuery(tStrSql);
  	    
  	    if(tLCPENoticeSet != null && tLCPENoticeSet.size() > 0)
  	    {
  	        for(int i = 1; i <= tLCPENoticeSet.size(); i++)
  	        {
      	        LCPENoticeSchema tLCPENoticeSchema = null;
      	        LCPENoticeItemSet tLCPENoticeItemSet = null;
      	        
      	        tLCPENoticeSchema = tLCPENoticeSet.get(i);
      	        
      	        String tStrSql1 = "select * from LCPENoticeItem "
      	           + " where contno = '" + tLCPENoticeSchema.getContNo() + "' "
      	           + " and proposalcontno = '" + tLCPENoticeSchema.getProposalContNo() + "' "
      	           + " and Prtseq = '" + tLCPENoticeSchema.getPrtSeq() + "' ";
      	        LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
      	        tLCPENoticeItemSet = tLCPENoticeItemDB.executeQuery(tStrSql1);
      	        System.out.println("tStrSql1:" + tStrSql1);
      	        
      	        String tSql = "select SubMissionID from LWMission "
                     + " where MissionID='" + tMissionID + "' "
                     + " and ActivityID='0000001101' ";
                tSubMissionID = new ExeSQL().getOneValue(tSql);
      	        
                // 准备传输数据 VData
                VData tVData = new VData();
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("ContNo", tContNo);
                tTransferData.setNameAndValue("PrtNo", tPrtNo);
                tTransferData.setNameAndValue("CustomerNo", tLCPENoticeSchema.getCustomerNo());
                tTransferData.setNameAndValue("MissionID", tMissionID);
                tTransferData.setNameAndValue("SubMissionID", tSubMissionID );
                tTransferData.setNameAndValue("LCPENoticeSchema", tLCPENoticeSchema);
                tTransferData.setNameAndValue("LCPENoticeItemSet", tLCPENoticeItemSet);
                System.out.println("ContNo:" + tContNo);
                System.out.println("CustomerNo:" + tLCPENoticeSchema.getCustomerNo());
                System.out.println("PrtNo:" + tPrtNo);
                System.out.println("MissionID:" + tMissionID);
                System.out.println("SubMissionID:" + tSubMissionID);
                
                tVData.add(tGlobalInput);
                tVData.add(tTransferData);
                Content = "";
                
                // 数据传输
                TbWorkFlowUI tTbWorkFlowUI   = new TbWorkFlowUI();
                if (tTbWorkFlowUI.submitData(tVData,"0000001101") == false)
                {
                	tError = tTbWorkFlowUI.mErrors;
                    FlagStr = "Fail";
                    System.out.println(tLCPENoticeSchema.getPrtSeq() + "体检件下发失败。");
                    break;
                }
                System.out.println(tLCPENoticeSchema.getPrtSeq() + "体检件下发成功。");
                
                //创建下发单证工作流
                ExeSQL tExeSQL = new ExeSQL();
                String strSql = "select missionprop3 from lwmission where "
    				+ "MissionID='" + tMissionID+"' and "
    				+ "subMissionID='" + tSubMissionID+"'"	
    				+ " and activityid='0000001106'";
                String PrintNo = tExeSQL.getOneValue(strSql);
                
                tTransferData.setNameAndValue("PrtSeq",PrintNo);
                tTransferData.setNameAndValue("Code","03") ;
                
                VData cVData = new VData();
                cVData.add( tTransferData);
                cVData.add( tGlobalInput );
                
                TbWorkFlowUI cTbWorkFlowUI   = new TbWorkFlowUI();
                if (!cTbWorkFlowUI.submitData(cVData,"0000001106"))//生成回销的工作流节点
                {
    		        cError = cTbWorkFlowUI.mErrors;
                    FlagStr = "Fail";
                    System.out.println(tLCPENoticeSchema.getPrtSeq() + "体检件下发单证失败。");
                    break;
                }
                System.out.println(tLCPENoticeSchema.getPrtSeq() + "体检件下发单证成功。");
            }
  	    }
  	    // ------------------------------------
  	    
		
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    if ((!tError.needDealError())&&(!cError.needDealError()))
		    {                      
		    	//Content = " 承保体检资料录入成功! ";
		    	Content = " 体检件下发成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	//if(tError.needDealError()){
		    	//Content =Content+ " 承保体检资料录入失败，原因是:" + tError.getFirstError();
		    	//}
		    	if(cError.needDealError()){
		    	Content = Content+" 承保体检资料录入下发单证失败，原因是:" + cError.getFirstError();
		    	}
		    	FlagStr = "Fail";
		     }
		}
	    else
        {
            Content = " 体检件下发成功! ";
		    FlagStr = "Succ";
        }
		System.out.println("UWManuHealthChk.jsp is finished...");
	} 
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
