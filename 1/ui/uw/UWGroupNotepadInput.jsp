<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：团单记事本
//程序功能：记录保单纪录各个环节的备注信息
//创建日期：2006-9-21 17:57
//创建人  ：Wulg
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

 <%
  //个人下个人
	String GrpContNo = request.getParameter("GrpContNo");
	String PrtNo = request.getParameter("PrtNo");
	String ProposalContNo = "00000000000000000000";
	String ContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>"; //记录主键    
	var PrtNo = "<%=request.getParameter("PrtNo")%>";  //印刷号
</script>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="UWGroupNotepad.js"></SCRIPT>
<title>团险记事本</title>
<%@include file="UWGroupNotepadInit.jsp"%>
</head>
<body  onload="initForm(); initElementtype();" >
	<form action="./UWGroupNotepadeSave.jsp" method=post name=fm target="fraSubmit">
		
  <table width="100%">
    <tr> 
      <td class=common> <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWGroupNotepadGrid);"> 
      </td>
      <td width="100%" class=titleImg > 记事本查询 </td>
    </tr>
  </table>
		<table class= common width=100%>
			<tr width=100%>
				<td class= title >
					操作员
				</td>
				<td class= input >
					<input type="text" name="operator" class="common">
				</td>
				<td class= title >
					录入日期
				</td>
				<TD  class= input>
          <Input class="coolDatePicker" dateFormat="short" name=MakeDate verify="录入日期|date">
        </TD>
				<td>
					<input class=CssButton type=button value="查询" onclick="queryClick()">
				</td>
			</tr>
		</table>
	  
  <table width="100%">
    <tr width=100%>
        	<td class=common>
			      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWGroupNotepadGrid);">
    		  </td>
    		  <td class= titleImg width="100%">
    			   记事本内容：
    		  </td>
    	</tr>
    </table>
    <Div id= "divUWGroupNotepadGrid" style= "display: ''">
  	<table>
      <tr class= common>
        <td text-align: left colSpan=1 >
  		    <span id="spanUWGroupNotepadGrid">
  				</span> 
  		  </td>
  		</tr>
    </table> 
      <Div id= "divPage" align=center style= "display: 'none' ">
        <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
        <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>
    </div>
		
  <table width="100%" class= common>
    <tr align="left" class= common>
				<td width="100%" height="15%"  class= title>
					记事本内容
				</td>
			</tr>
			<tr class= common>
				<td height="85%"  class= title>
					<textarea name="Content" cols="135" rows="5" class="common"></textarea>
				</td>
			</tr>
		</table>
	  <table >
	  	<tr class= common>
	  		<td>
	  		<input type="hidden" name= "GrpContNo" value= "">
	  		<input type="hidden" name= "PrtNo" value= "">
	  		<input class=CssButton type=button value="新增" onclick="submitForm()">
	  		<input class=CssButton type=button value="清空记事本内容" onclick="fm.Content.value=''">
	  		<input class=CssButton type=button value="返回" onclick="parent.close();">
	  	</td>																
	  	</tr>
	  </table>
    <input  type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
	  <input type=hidden name="LoadFlag">
	</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
