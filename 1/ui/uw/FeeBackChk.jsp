<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FeeBackChk.jsp
//程序功能：充帐处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%

  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
  LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
 
  	if(tG == null) {
		out.println("session has expired");
		return;
	}        
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	boolean flag = false;
	LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();

	String tPayNo = request.getParameter("TempFeeNo");
	
	if (tPayNo.equals(""))
	{
		Content = " 请录入冲帐收据号! ";
	}
	else
	{
		System.out.println("TempFeeNo:"+tPayNo);
	  	
	
		tLJAPayPersonSchema.setPayNo( tPayNo );
	        	
		flag = true;
	}
	
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLJAPayPersonSchema );
		tVData.add( tG );
		
		// 数据传输
		FeeBackUI tFeeBackUI   = new FeeBackUI();
		if (tFeeBackUI.submitData(tVData,"INSERT") == false)
		{
			int n = tFeeBackUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tFeeBackUI.mErrors.getError(i).errorMessage);
			Content = " 冲帐确认失败，原因是: " + tFeeBackUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tFeeBackUI.mErrors;
		    //tErrors = tFeeBackUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 冲帐确认成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 冲帐确认失败，原因是：";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}
		    	FlagStr = "Fail";
		    }
		}
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>
</html>
