<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWManuDateChk.jsp
//程序功能：人工核保延期
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCPolSet tLCPolSet = new LCPolSet();
	LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();

	String tProposalNo = request.getParameter("ProposalNoHide");
	String tUWFlag = request.getParameter("Flag");
	String tvalidate = request.getParameter("validate");
	String tUWIdea = request.getParameter("UWIdea");
	System.out.println("polno:"+tProposalNo);
	
	boolean flag = false;
	
	if (!tProposalNo.equals("") && !tUWFlag.equals("") && !tvalidate.equals(""))
	{
 		LCPolSchema tLCPolSchema = new LCPolSchema();
 		LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
	
	    tLCPolSchema.setPolNo( tProposalNo);
	    tLCPolSchema.setUWFlag(tUWFlag);
	    tLCPolSchema.setRemark(tUWIdea);
	    tLCUWMasterSchema.setPostponeDay(tvalidate);
	    tLCUWMasterSchema.setUWIdea(tUWIdea);
	    
	    tLCPolSet.add( tLCPolSchema );
	    tLCUWMasterSet.add(tLCUWMasterSchema);
	    flag = true;
	}
	else
	{
		Content="数据传输失败!";
		FlagStr = "Fail";
	}	

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCPolSet );
		tVData.add( tLCUWMasterSet);
		tVData.add( tG );
		
		// 数据传输
		UWManuNormChkUI tUWManuNormChkUI = new UWManuNormChkUI();
		if (tUWManuNormChkUI.submitData(tVData,"INSERT") == false)
		{
			int n = tUWManuNormChkUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			Content = " 自动核保失败，原因是: " + tUWManuNormChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tUWManuNormChkUI.mErrors;
		    //tErrors = tUWManuNormChkUI.mErrors; 
		    if (!tError.needDealError())
		    {                          
		    	Content = " 人工核保成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 人工核保失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}

		    	FlagStr = "Fail";
		    }
		}
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
