<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：RReportQuery.jsp
//程序功能：生存调查报告查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var customers = "";                   //生调人选
</script>
<head >
<title> </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="RReportQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="RReportQueryInit.jsp"%>
  
</head>
<body  onload="initForm('<%=tContNo%>','<%=tLoadFlag%>');" >
  <form method=post name=fm target="fraSubmit">
  	 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 契调待回销队列
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCRReport" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanLCRReportGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>

    <div id="divHidden" style="display :'none'">
	 <table class= common align=center>
    	<tr class= common>
			<td  class= title> 姓名 </td>
			<td  class= input>
				<Input class=codeNo name=CustomerName ondblclick="showCodeListEx('CustomerName',[this,CustomerNameName,CustomerNo],[0,1,2],'', '', '', true);" onkeyup="showCodeListKeyEx('CustomerName',[this,CustomerNameName,CustomerNo],[0,1,2],'', '', '', true);"><input class=codename name=CustomerNameName readonly=true>  </td>   	
			<td  class= title> 客户号 </td>
			<td  class= input><Input readonly class=common name=CustomerNo>  </td>   	
		</tr>
    </table>
  </div>
  
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 契约调查项目明细报告
    		</td>
    	</tr>
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanQuestGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>
<Div  id= "divAddDelButton" style= "display: 'none'" align=left>
    <input type =hidden name="addRReportButton" class=cssButton value="保  存" onclick="addRecord();"></TD>
    <input type =button name="modRReportButton" class=cssButton value="修  改" onclick="modifyRecord();"></TD>
    <input type =button name="delRReportButton" class=cssButton value="删  除" onclick="delRecord();"></TD>
</DIV>
      
  <!--table width="121%" height="37%">
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 报告内容 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= title><textarea name="Content" cols="135" rows="10" class="common" readonly></textarea></TD>
    </TR>
  </table>
  <table width="121%" height="37%">
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 回复内容 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= title><textarea name="ReplyResult" cols="135" rows="10" class="common" readonly></textarea></TD>
    </TR>
  </table-->
<!--
    <table>
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"></td>
    		<td class= titleImg>	 生调项目</td>                            
    	</tr>	
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanRReportGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
-->
<BR>
    <table>
     <table class=common>
         <TR  class= common> 
           <TD  class= common> 契调结果 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Contente" cols="120" rows="3" class="common" >
             </textarea>
           </TD>
         </TR>
      </table>
<!--
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"></td>
    		<td class= titleImg>   生调结果</td>                            
    	</tr>	
-->
    </table>
<!--
    <Div  id= "divUWDis" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanRReportResultGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
      </div>
-->
       <table class=common>
         <TR  class= common> 
           <TD  class= common> 契调结论 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="ReplyContente" cols="120" rows="3" class="common" >
             </textarea>
           </TD>
         </TR>
      </table>
  <p> 
    <!--读取信息-->
    <input type= "hidden" name= "Flag" value="">
    <input type= "hidden" name= "ContNo" value= "">
    <input type= "hidden" name= "Type" value="">
    <input type= "hidden" name= "PrtSeq" value="">
    <input type= "hidden" name= "customer" value="">   
    <input type=hidden id="fmAction" name="fmAction">
    <INPUT  type= "hidden" class= Common name=RReportItemCode>
    <INPUT  type= "hidden" class= Common name=RReportItemName>
    <INPUT  type= "hidden" class= Common name=RRItemContent>
    <INPUT  type= "hidden" class= Common name=RRItemResult>
    <INPUT  type= "hidden" class= Common name=LoadFlag>
  </p>
<Div  id= "divRReportButton" style= "display: 'none'" align=left>  
  	<input value="契调信息结论保存" class=cssButton type=button onclick="saveRReport();" > 
</Div>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>