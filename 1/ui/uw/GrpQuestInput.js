//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var mDebug="0";
var mOperate="";
var showInfo;
var Flag=0;
var wGrpContNo="";
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
    var i = 0;
    var lineNum = 1;  //提示信息显示的行数
    var errType = "";
    var issueRuleCode = "";
    //var operatePos = "";
    var printFlag = "";
    
    GrpQuestGrid.delBlankLine();
    for(i=0;i<GrpQuestGrid.mulLineCount;i++)
    {
        //错误编码和规则编码不能同时为空，如果其中一个为空则用相应的值赋给对方
        errType = GrpQuestGrid.getRowColData(i,3);
        issueRuleCode = GrpQuestGrid.getRowColData(i,5);
        if( ( errType == "" || errType == null ) && ( issueRuleCode == "" || issueRuleCode == null ) )  
        {
            lineNum=i+1;
            alert("第"+lineNum+"行错误编码和规则编码不能都为空，确认后请填写全部或其中一个！");
            return false;
        }
        else if( issueRuleCode == "" || issueRuleCode == null )
        {
            GrpQuestGrid.setRowColData(i,5,errType);
        }
        else if( errType == "" || errType == null )
        {
            GrpQuestGrid.setRowColData( i, 3, issueRuleCode.substr(0,1) );
        }

        /*
        //操作位置不能为空
        operatePos = GrpQuestGrid.getRowColData(i,6);
        if (operatePos == "" || operatePos == null) 
        {
            lineNum=i+1;
            alert("第"+lineNum+"行操作位置不能为空，确认后请填写！");
            returnFlag = true;
        }
        */

        //打印标记不能为空
        printFlag = GrpQuestGrid.getRowColData(i,9);
        if (printFlag == "" || printFlag == null) 
        {
            lineNum=i+1;
            alert("第"+lineNum+"行打印标记不能为空，确认后请填写！");
            return false;
        }
         if (printFlag == 'Y' && GrpQuestGrid.getRowColData(i,7) != 0) 
        {
            lineNum=i+1;
            alert("第"+lineNum+"行打印标记不能为Y，因为下发对象不是投保人！");
            return false;
        }       
    }

    if( verifyInput2() == false ) return false;
    fm.fmtransact.value="INSERT||QUEST";
    //alert(Flag);

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    ChangeDecodeStr();
    fm.action="./GrpQuestSave.jsp";
    fm.submit(); //提交
	}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  UnChangeDecodeStr();
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initForm();
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LDDisease.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作  
}
      
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
      parent.fraMain.rows = "0,0,0,82,*";
  }
}

//Click事件，当点击增加图片时触发该函数
/*
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}  
 */        
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  var cardflag=0;
  var i=0;
  for(i=0;i<GrpQuestGrid.mulLineCount;i++)
  {
  	if(GrpQuestGrid.getSelNo(i))
  	{
  		cardflag=GrpQuestGrid.getSelNo();
  		break;
  	}
  }
 	if(cardflag)
 	{
 		fm.GrpContNo.value=GrpQuestGrid.getRowColData(cardflag - 1, 10);
 		fm.SerialNo.value=GrpQuestGrid.getRowColData(cardflag - 1, 11);
 		fm.IssueCode.value=GrpQuestGrid.getRowColData(cardflag - 1, 1);
 		fm.IssueName.value=GrpQuestGrid.getRowColData(cardflag - 1, 2);
 		if(GrpQuestGrid.getRowColData(cardflag - 1, 5)=='A'||
 		GrpQuestGrid.getRowColData(cardflag - 1, 5)=='B'||
 		GrpQuestGrid.getRowColData(cardflag - 1, 5)=='C')
 		fm.IssueRuleCode.value="";
 		else
 		fm.IssueRuleCode.value=GrpQuestGrid.getRowColData(cardflag - 1, 5);	
 		fm.IssueRuleCode1.value=GrpQuestGrid.getRowColData(cardflag - 1, 3);
 		fm.BackObjType.value=GrpQuestGrid.getRowColData(cardflag - 1, 6);
 		fm.BackObj.value=GrpQuestGrid.getRowColData(cardflag - 1, 7);
 		fm.IssueCont.value=GrpQuestGrid.getRowColData(cardflag - 1, 8);
 		fm.NeedPrint.value=GrpQuestGrid.getRowColData(cardflag - 1, 9);
 		fm.state.value=GrpQuestGrid.getRowColData(cardflag - 1, 12);
 		fm.prtseq.value=GrpQuestGrid.getRowColData(cardflag - 1, 13);
 		    var i = 0;
    var lineNum = 1;  //提示信息显示的行数
    var errType = "";
    var issueRuleCode = "";
    //var operatePos = "";
    var printFlag = "";
    
    GrpQuestGrid.delBlankLine();
    for(i=0;i<GrpQuestGrid.mulLineCount;i++)
    {
        //错误编码和规则编码不能同时为空，如果其中一个为空则用相应的值赋给对方
        errType = GrpQuestGrid.getRowColData(i,3);
        issueRuleCode = GrpQuestGrid.getRowColData(i,5);
        if( ( errType == "" || errType == null ) && ( issueRuleCode == "" || issueRuleCode == null ) )  
        {
            lineNum=i+1;
            alert("第"+lineNum+"行错误编码和规则编码不能都为空，确认后请填写全部或其中一个！");
            return false;
        }
        else if( issueRuleCode == "" || issueRuleCode == null )
        {
            GrpQuestGrid.setRowColData(i,5,errType);
        }
        else if( errType == "" || errType == null )
        {
            GrpQuestGrid.setRowColData( i, 3, issueRuleCode.substr(0,1) );
        }

        /*
        //操作位置不能为空
        operatePos = GrpQuestGrid.getRowColData(i,6);
        if (operatePos == "" || operatePos == null) 
        {
            lineNum=i+1;
            alert("第"+lineNum+"行操作位置不能为空，确认后请填写！");
            returnFlag = true;
        }
        */

        //打印标记不能为空
        printFlag = GrpQuestGrid.getRowColData(i,9);
        if (printFlag == "" || printFlag == null) 
        {
            lineNum=i+1;
            alert("第"+lineNum+"行打印标记不能为空，确认后请填写！");
            return false;
        }
         if (printFlag == 'Y' && GrpQuestGrid.getRowColData(i,7) != 0) 
        {
            lineNum=i+1;
            alert("第"+lineNum+"行打印标记不能为Y，因为下发对象不是投保人！");
            return false;
        }       
    }

    if( verifyInput2() == false ) return false;
 		var flag1=GrpQuestGrid.getRowColData(cardflag - 1, 12);
 		if(flag1==1||flag1==2)
 		{
 	 		if (confirm("您确实想修改该记录吗?"))
  		{
 	 			var i = 0;
 		 		if( verifyInput2() == false ) return false;
 			 			var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		 				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 	 					showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 	 
  //showSubmitFrame(mDebug);
 	 					fm.fmtransact.value = "UPDATE||MAIN";
 					 ChangeDecodeStr();
 					 fm.action="./GrpQuestSave.jsp";
 	 				fm.submit(); //提交
 	 		}else
 	 		{
    //mOperate="";
 	 			  alert("您取消了修改操作！");
 	 		}
 	}else
 	{
 			alert("已经打印保单!不能进行修改操作!");
 	}
 }else
		alert("请先选择一条需要修改的记录!");
 } 
          
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDiseaseQuery.html");
}           

function Reply()
{
	  var cardflag=0;
  var i=0;
  for(i=0;i<GrpQuestBackGrid.mulLineCount;i++)
  {
  	if(GrpQuestBackGrid.getSelNo(i))
  	{
  		cardflag=GrpQuestBackGrid.getSelNo();
  		break;
  	}
  }
 	if(cardflag)
 	{
 		fm.GrpContNo.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 10);
 		fm.SerialNo.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 11);
 		fm.IssueCode.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 1);
 		fm.IssueName.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 2);
 		if(GrpQuestBackGrid.getRowColData(cardflag - 1, 5)=='A'||
 		GrpQuestBackGrid.getRowColData(cardflag - 1, 5)=='B'||
 		GrpQuestBackGrid.getRowColData(cardflag - 1, 5)=='C')
 		fm.IssueRuleCode.value="";
 		else
 		fm.IssueRuleCode.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 5);	
 		fm.IssueRuleCode1.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 3);
 		fm.BackObjType.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 6);
 		fm.BackObj.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 7);
 		fm.IssueCont.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 8);
 		fm.NeedPrint.value=GrpQuestBackGrid.getRowColData(cardflag - 1, 9);
  if (confirm("请确认是否已修改投保件信息?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "REPLY||MAIN";
  fm.action="./GrpQuestSave.jsp";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了回销操作！");
  }
	} else{
		alert("请先选择一条需要回销的记录!");
	} 
}    
     
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//查询显示问题件
function initQuest()
{
//	alert();
  var GrpContNo = fm.GrpContNo.value;
  wGrpContNo=GrpContNo;
  if(GrpContNo==null||GrpContNo=="")
  {
    alert("打开页面有错误!合同号为空\n请检查是否以导入合同！");
    return;
  }
 // alert();
  var strSql = "select a.ErrField,a.ErrFieldName,substr(a.QuestionObj,1,1),c.codename,a.QuestionObj,"
             + "a.BackObj,a.BackObjType,a.IssueCont,a.needprint,'"+GrpContNo+"',a.SerialNo,a.state,a.prtseq,a.Operator "
             +"from LCGrpIssuePol a ,ldcode c "
             +"where a.ProposalGrpContNo='"+GrpContNo+"'  and c.codetype='questionobj' and c.code=substr(a.QuestionObj,1,1) and a.ReplyMan is null and a.state<>'5' order by a.SerialNo";
  
             //"select a.ErrField,b.IssueName,a.QuestionObj,'',a.OperatePos,a.BackObjType,a.BackObj,a.IssueCont,a.ErrContent,'"+GrpContNo+"',a.SerialNo "
             // +" from LCGrpIssuePol a,ldissue b where a.ProposalGrpContNo='"+GrpContNo+"' and a.IssueType=b.issuecode";

  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1); 

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("无团体问题件！");
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpQuestGrid;          
  //保存SQL语句
  turnPage.strQuerySql     = strSql;  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
}
function initQuestBack()
{
	//alert();
  var GrpContNo = fm.GrpContNo.value;
  if(GrpContNo==null||GrpContNo=="")
  {
    alert("打开页面有错误!合同号为空\n请检查是否以导入合同！");
    return;
  }
  
  var strSql = "select a.ErrField,a.ErrFieldName,substr(a.QuestionObj,1,1),c.codename,a.QuestionObj,"
             + "a.BackObj,a.BackObjType,a.IssueCont,case a.state when '5' then 'Y' else 'N' end,'"+GrpContNo+"',a.SerialNo,a.prtseq,case a.state when '5' then ReplyMan else '' end,a.Operator  "
             +"from LCGrpIssuePol a ,ldcode c "
             +"where a.ProposalGrpContNo='"+GrpContNo+"'  and c.codetype='questionobj' and c.code=substr(a.QuestionObj,1,1) and a.state in('2','3','4','5') order by a.SerialNo";
   
             //"select a.ErrField,b.IssueName,a.QuestionObj,'',a.OperatePos,a.BackObjType,a.BackObj,a.IssueCont,a.ErrContent,'"+GrpContNo+"',a.SerialNo "
             // +" from LCGrpIssuePol a,ldissue b where a.ProposalGrpContNo='"+GrpContNo+"' and a.IssueType=b.issuecode";

  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1); 
	if (!turnPage.strQueryResult) {
    //alert("无团体问题件！");
    return false;
  }
  //判断是否查询成功

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpQuestBackGrid;          
  //保存SQL语句
  turnPage.strQuerySql     = strSql;  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //
  //alert(turnPage.arrDataCacheSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
}
function SendIssueNotice()
{
  var tGrpContNo = fm.GrpContNo.value ;
  if(tGrpContNo == "")
  {
    alert("页面错误，未查询到团体合同信息！");
    return ;
  }
  var strSql = "select * from LCGrpIssuePol where GrpContNo='"+tGrpContNo+"' and needprint='Y'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    alert("未查询到需要打印的团体问题件信息！");
    return ;
  }
/*  var strSql = "select 1 from loprtmanager where otherno='"
               +tGrpContNo+"' and OtherNoType = '01' and code='54' and StandbyFlag1 is null";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    alert("已发团体问题件通知书。\n补发功能待作！");
    return ;
  }
*/
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //alert();
  fm.action = "./GrpSendIssueSave.jsp?GrpContNo="+tGrpContNo;
  //alert(fmQuery.action);
  fm.submit();
}
function EasyScanQuery() 
{
	var cardflag1=0;
	  var i=0;
  for(i=0;i<GrpQuestBackGrid.mulLineCount;i++)
  {
  	if(GrpQuestBackGrid.getSelNo(i))
  	{
  		cardflag1=GrpQuestBackGrid.getSelNo();
  		break;
  	}
  }
  if(cardflag1)
  {
  	var cPrtseq =GrpQuestBackGrid.getRowColData(cardflag1 - 1, 12);
  	var cGrpcontno = GrpQuestBackGrid.getRowColData(cardflag1 - 1, 10);
		if(cPrtseq==" "||cPrtseq=="")
		{
			alert("该问题件没有进行打印!");
			return;
		}else
			{  
		    window.open("./QCManageInputMainShow.jsp?EASYWAY=1&Grpcontno="+cGrpcontno+"&DocCode="+cPrtseq+"&BussTpye=TB&SubTpye=TB23");        
			}
	}else
		{
			alert("请先选择一条需要查看的记录!");
			return;
		}
}
function Back()
{
	var strSql = "select passflag,UWIdea from lcgcuwmaster where GrpContNo='"+wGrpContNo+"'";
	var arr=easyExecSql(strSql);
	if(arr)
	{
	fm.GUWState.value=arr[0][0];
	fm.GUWIdea.value=arr[0][1];
	}else
		{
			fm.GUWState.value="";
			fm.GUWIdea.value="";
		}
	fm.MissionID.value = MissionID;
  fm.SubMissionID.value = SubMissionID;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //alert();
  fm.action = "./UWRReportBackSave.jsp?MissionID="+MissionID+"&SubMissionID="+SubMissionID;
  //alert(fmQuery.action);
  fm.submit();
}
function Addline()
{
	    for(i=0;i<GrpQuestGrid.mulLineCount;i++)
    {
    	  printFlag = GrpQuestGrid.getRowColData(i,9);
        if (printFlag == "" || printFlag == null) 
        {
					GrpQuestGrid.setRowColData(i,9,'N');
        }    	
   }
}