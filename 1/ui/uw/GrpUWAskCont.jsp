<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html> 
<%
//程序名称：GrpUWAskCont.jsp
//程序功能：既往查询
//创建日期：2005-4-27
//创建人  ： LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GrpUWAskCont.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpUWAskContInit.jsp"%>
  <title> 既往询价信息 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCont);">
    		</td>
    		<td class= titleImg>
    			团体信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrpCont" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title8>
            团体客户号
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=GrpNo >
          </TD>
          <TD  class= title8>
            投保单位名称
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=GrpName>
          </TD>
          <TD  class= title8 style= "display: 'none'">
            团体保障号:
          </TD>
          <TD  class= input8 style= "display: 'none'">
            <Input class="readonly" readonly name=GrpPolNo >
          </TD>
        </TR>
    </table>
    </DIV>
    <!-- 既往保全部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpAsk);">
    		</td>
    		<td class= titleImg>
    			 既往询价信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLPGrpAsk" style= "display: ''" align = center>
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanAskGrid" >
					</span> 
				</td>
			</tr>
		</table>

      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();">
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();">
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();">
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();">
    </div>
       </DIV>
    <!-- 既往保全部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpAsk);">
    		</td>
    		<td class= titleImg>
    			 既往询价明细信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLPGrpAsk1" style= "display: ''" align = center>
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanAskGrid1" >
					</span> 
				</td>
			</tr>
		</table>

      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();">
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();">
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();">
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();">
    </div>
       <INPUT VALUE="询价投保书查询" Class="cssButton" TYPE=button onclick="showGrpCont();">
    <HR>
      <INPUT VALUE = "既往投保信息" Class="cssButton"  TYPE=button onclick= "showEnsureCont();">
      <INPUT VALUE = "既往询价信息" Class="cssButton" TYPE=button onclick= "showAskApp();">
      <INPUT VALUE = "既往保全信息" Class="cssButton" TYPE=button onclick= "showRejiggerApp();">
      <INPUT VALUE = "既往理赔信息" Class="cssButton" TYPE=button onclick= "showInsuApp();">
    <HR>
    <div id = "divUWAgree" style = "display: ''">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="GoBack();">
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>