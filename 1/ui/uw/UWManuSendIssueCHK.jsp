<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWManuRReportChk.jsp
//程序功能：新契约人工核保发送核保通知书报告录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  CErrors cError = null;
 String FlagStr = "Fail";
 String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
	System.out.println("\n\n");
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
	TransferData tTransferData = new TransferData();
	String tContNo = request.getParameter("ContNo");
	String tPrtNo = request.getParameter("PrtNoHide");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubNoticeMissionID");	
	System.out.println("Contno:"+tContNo);
	System.out.println("tPrtNo:"+tPrtNo);
	System.out.println("tMissionID:"+tMissionID);
	System.out.println("tSubMissionID:"+tSubMissionID);
	boolean flag = true;
	if (!tContNo.equals("")&&!tPrtNo.equals("")&& !tMissionID.equals(""))
	{
		//准备公共传输信息
		tTransferData.setNameAndValue("ContNo",tContNo);
		tTransferData.setNameAndValue("PrtNo",tPrtNo) ;
		tTransferData.setNameAndValue("MissionID",tMissionID);	
        
        // 0000001022子工作流节点号问题，序号问题。
        String tSql = "select SubMissionID from LWMission "
            + " where MissionID='" + tMissionID + "' "
            + " and ActivityID='0000001022' ";
        tSubMissionID = new ExeSQL().getOneValue(tSql);
		tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
        System.out.println("SubMissionID:" + tSubMissionID);
        // ------------------------------------------
	
	}
	else
	{
		flag = false;
		Content = "数据不完整!";
	}	
	System.out.println("flag:"+flag);
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData);
		tVData.add( tG );
		
		// 数据传输
		TbWorkFlowUI tTbWorkFlowUI   = new TbWorkFlowUI();
		if (!tTbWorkFlowUI.submitData(tVData,"0000001022"))//执行保全核保生调工作流节点0000000004
		{
			int n = tTbWorkFlowUI.mErrors.getErrorCount();
			Content = " 新契约人工核发送核保通知书失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//创建下发单证工作流
		    ExeSQL tExeSQL = new ExeSQL();
		    String strSql = "select missionprop3 from lwmission where "
		    							+"MissionID='"+tMissionID+"' and "
		    							+"subMissionID='"+tSubMissionID+"'"	
		    							+" and activityid='0000001023'";
		    String PrintNo = tExeSQL.getOneValue(strSql);
		    
        tTransferData.setNameAndValue("PrtSeq",PrintNo);
        tTransferData.setNameAndValue("Code","85") ;
		
		
				VData cVData = new VData();
				cVData.add( tTransferData);
				cVData.add( tG );
				TbWorkFlowUI cTbWorkFlowUI   = new TbWorkFlowUI();
		     if (!cTbWorkFlowUI.submitData(cVData,"0000001023"))//生成回销的工作流节点
		     {
		     	int n = cTbWorkFlowUI.mErrors.getErrorCount();
		     	Content = " 新契约人工核发送核保通知书失败，原因是: " + cTbWorkFlowUI.mErrors.getError(0).errorMessage;
		     	FlagStr = "Fail";
		     }
		
		
		
		
		//如果在Catch中发现异常，则不从错误类中提取错误信息
				if (FlagStr == "Fail")
		{
		    tError = tTbWorkFlowUI.mErrors;
		    cError = cTbWorkFlowUI.mErrors;
		    if ((!tError.needDealError())&&(!cError.needDealError()))
		    {                      
		    	Content = " 新契约人工核保发送问题件通知书成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	if(tError.needDealError()){
		    	Content =Content+ " 新契约人工核保发送问题件通知书失败，原因是:" + tError.getFirstError();
		    	}
		    	if(cError.needDealError()){
		    	Content = Content+" 新契约人工核保发送问题件通知书失败，原因是:" + cError.getFirstError();
		    	}
		    	FlagStr = "Fail";
		     }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");
</script>
</html>
