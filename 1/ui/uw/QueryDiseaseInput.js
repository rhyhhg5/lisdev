/*********************************************************************
 *  程序名称：QueryDiseaseInput.js
 *  程序功能：人工核保免得信息维护页面
 *  创建日期：2006-10-30 
 *  创建人  ：zhousp
 *  返回值：  无
 *  更新记录：  更新人    更新日期     更新原因/内容
 *********************************************************************
 */

var arrResult1 = new Array();
var arrResult2 = new Array();
var arrResult3 = new Array();
var arrResult4 = new Array();
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var temp = new Array();
var mOperate="";
var oldDataSet ; 
//alert(PrtNo);
window.onfocus=myonfocus;
/*********************************************************************
 *  查询免责信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function easyQueryClick(){
 		if( verifyInput() == false ) 
 		return false;
 		mSQL ="select a.code,a.codename  ";
	mSQL += " FROM  ldcode a";
	mSQL += " WHERE  1=1 and a.codetype='speccode'"+getWherePart('a.code','SpecNo')+""+getWherePart('a.codename','DiseaseName')+"" ;


	turnPage1.queryModal(mSQL, QueryDiseaseGrid);
 	}
/*********************************************************************
 *  新增免责信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function AddClick()
{
    QueryDiseaseGrid.clearData();
    QueryDiseaseGrid.addOne();
    QueryDiseaseGrid.checkBoxSelM(1);
    showSaveButton(true);
    return true;
}

function showSaveButton(flag)
{
    var tIsShowSaveBtnFlag = false;
    if(flag)
    {
        tIsShowSaveBtnFlag = true;
    }
    fm.btnQuery.disabled = tIsShowSaveBtnFlag;
    fm.btnAdd.disabled = tIsShowSaveBtnFlag;
    fm.btnEdit.disabled = tIsShowSaveBtnFlag;
    fm.btnDel.disabled = tIsShowSaveBtnFlag;
    
    fm.btnSave.disabled = !tIsShowSaveBtnFlag;
    fm.btnCancelSave.disabled = !tIsShowSaveBtnFlag;
}

/**
 * 保存新增的免责信息
 */
function addNewClick()
{
    UpdateClick();
}

/**
 * 取消新增动作
 */
function cancelAddNewClick()
{
    initForm();
}

/*********************************************************************
 *  保存免责信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function UpdateClick(){
    var tSel = QueryDiseaseGrid.getSelNo();
    
    if( tSel == 0 || tSel == null )
    {
	   alert("请先选择一条记录，再点击返回按钮。");
       return false;
    }
    
  for (i=0;i<QueryDiseaseGrid.mulLineCount;i++){
  	var mSpecNo = QueryDiseaseGrid.getRowColData(i,1);
   	var mDiseaseName=QueryDiseaseGrid.getRowColData(i,2);	
	if (mSpecNo == ''){
  alert("请输入免责信息代码");
  return;
  }
	if (mDiseaseName == ''){
	alert("请输入免责信息");
	return;
	}
 }
 if (confirm("您确实想修改该记录吗?"))
 {
 	

  fm.fmtransact.value = "UPDATE||MAIN" ;
   	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 // alert("11111111111111");
  fm.submit(); //提交
  return;
}
else {
	alert("您取消了修改操作！")
	}
	} 
/*********************************************************************
 *  删除免责信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function DeleteClick(){
    var tSel = QueryDiseaseGrid.getSelNo();
    
    if( tSel == 0 || tSel == null )
    {
       alert("请先选择一条记录，再点击返回按钮。");
       return false;
    }
    
	 if (confirm("您确实想删除该记录吗?"))
 {
  fm.fmtransact.value = "DELETE||MAIN" ;
   	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 // alert("11111111111111");
  fm.submit(); //提交
  return;
}
else {
	alert("您取消了删除操作!")
	}
	} 	
	
//提交，保存按钮对应操作
function submitForm()
{
	mOperate = "INSERT||MAIN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//    showDiv(operateButton,"true"); 
 //   showDiv(inputButton,"false"); 
    //执行下一步操作
    initForm();
  }
}