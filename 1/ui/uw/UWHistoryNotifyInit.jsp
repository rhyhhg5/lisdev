<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：UWHistoryNotifyInit.jsp
%>
<!--用户校验类-->
<%@page import="java.util.*"%>
<%@page import="java.lang.Math.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String tContNo = "";
	String tPrtNo = "";
	String tLoadFlag = "";
	Date today = new Date();
	today = PubFun.calDate(today, 15, "D", null);
	String tday = UWPubFun.getFixedDate(today);
	tContNo = request.getParameter("ContNo");
	tPrtNo = request.getParameter("PrtNo");
	tLoadFlag = request.getParameter("LoadFlag");
%>

<script language="JavaScript">
	// 输入框的初始化（单记录部分）
	function initInpBox() {
		try {
			fm.all('ContNo').value = '';
			fm.all('InsureNo').value = '';
		} catch (ex) {
			alert("在UWManuHealthInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}

	function initForm(tContNo, tPrtNo, tLoadFlag) {
		try {
			initHide(tContNo, tPrtNo, tLoadFlag);
			initInsureNo(tPrtNo);
			initImpartDetailGrid();
			initDisDesbGrid();
			easyQueryClickSingle();
			fillDisDesbGrid();
		} catch (re) {
			alert("UWManuHealthInit.jsp-->InitForm函数中发生异常,初始化界面错误!");
		}
	}

	// 告知明细信息列表的初始化
	function initImpartDetailGrid() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "被保人"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[1][1] = "60px"; //列宽
			iArray[1][2] = 10; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[2] = new Array();
			iArray[2][0] = "告知版别"; //列名
			iArray[2][1] = "60px"; //列宽
			iArray[2][2] = 60; //列最大值
			iArray[2][3] = 3; //是否允许输入,1表示允许，0表示不允许
			iArray[2][4] = "ImpartVer";
			iArray[2][9] = "告知版别|len<=5";
			iArray[2][18] = 300;

			iArray[3] = new Array();
			iArray[3][0] = "告知编码"; //列名
			iArray[3][1] = "60px"; //列宽
			iArray[3][2] = 60; //列最大值
			iArray[3][3] = 3; //是否允许输入,1表示允许，0表示不允许
			iArray[3][4] = "ImpartCode";
			iArray[3][5] = "2|3";
			iArray[3][6] = "0|1";
			//iArray[2][7]="getImpartCode";
			iArray[3][9] = "告知编码|len<=4";
			iArray[3][15] = "ImpartVer";
			iArray[3][17] = "1";
			iArray[3][18] = 700;

			iArray[4] = new Array();
			iArray[4][0] = "告知内容"; //列名
			iArray[4][1] = "200px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "疾病内容"; //列名
			iArray[5][1] = "200px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[6] = new Array();
			iArray[6][0] = "开始时间"; //列名
			iArray[6][1] = "100px"; //列宽
			iArray[6][2] = 150; //列最大值
			iArray[6][3] = 1; //是否允许输入,1表示允许，0表示不允许
			iArray[6][9] = "开始时间|date";

			iArray[7] = new Array();
			iArray[7][0] = "结束时间"; //列名
			iArray[7][1] = "90px"; //列宽
			iArray[7][2] = 90; //列最大值
			iArray[7][3] = 1; //是否允许输入,1表示允许，0表示不允许
			iArray[7][9] = "结束时间|date";

			iArray[8] = new Array();
			iArray[8][0] = "证明机构或医生"; //列名
			iArray[8][1] = "90px"; //列宽
			iArray[8][2] = 90; //列最大值
			iArray[8][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[9] = new Array();
			iArray[9][0] = "目前情况或结果"; //列名
			iArray[9][1] = "90px"; //列宽
			iArray[9][2] = 90; //列最大值
			iArray[9][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[10] = new Array();
			iArray[10][0] = "能否证明"; //列名
			iArray[10][1] = "90px"; //列宽
			iArray[10][2] = 90; //列最大值
			iArray[10][3] = 2; //是否允许输入,1表示允许，0表示不允许
			iArray[10][4] = "yesno";

			ImpartDetailGrid = new MulLineEnter("fm", "ImpartDetailGrid");
			//这些属性必须在loadMulLine前
			ImpartDetailGrid.mulLineCount = 0;
			ImpartDetailGrid.displayTitle = 1;
			ImpartDetailGrid.hiddenPlus = 1;
			ImpartDetailGrid.hiddenSubtraction = 1;
			ImpartDetailGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}

	// 责任信息列表的初始化
	function initDisDesbGrid() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "客户号码"; //列名
			iArray[1][1] = "60px"; //列宽
			iArray[1][2] = 100; //列最大值
			iArray[1][3] = 2; //是否允许输入,1表示允许，0表示不允许
			iArray[1][4] = "insuredno";
			iArray[1][5] = "1|2";
			iArray[1][6] = "0|1";
			iArray[1][15] = "ContNo";
			iArray[1][16] = fm.all('ContNO').value;
			iArray[1][19] = "1";

			iArray[2] = new Array();
			iArray[2][0] = "客户姓名"; //列名
			iArray[2][1] = "60px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "疾病症状"; //列名
			iArray[3][1] = "260px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array();
			iArray[4][0] = "疾病结论"; //列名
			iArray[4][1] = "260px"; //列宽
			iArray[4][2] = 60; //列最大值
			iArray[4][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "ICD编码"; //列名
			iArray[5][1] = "100px"; //列宽
			iArray[5][2] = 60; //列最大值
			iArray[5][3] = 2; //是否允许输入,1表示允许，0表示不允许

			iArray[6] = new Array();
			iArray[6][0] = "内部子序列号"; //列名
			iArray[6][1] = "260px"; //列宽
			iArray[6][2] = 100; //列最大值
			iArray[6][3] = 3; //是否允许输入,1表示允许，0表示不允许

			iArray[7] = new Array();
			iArray[7][0] = "标准名称"; //列名
			iArray[7][1] = "100px"; //列宽
			iArray[7][2] = 100; //列最大值
			iArray[7][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[8] = new Array();
			iArray[8][0] = "目前情况"; //列名
			iArray[8][1] = "100px"; //列宽
			iArray[8][2] = 100; //列最大值
			iArray[8][3] = 1; //是否允许输入,1表示允许，0表示不允许

			DisDesbGrid = new MulLineEnter("fm", "DisDesbGrid");
			//这些属性必须在loadMulLine前                            
			DisDesbGrid.mulLineCount = 0;
			DisDesbGrid.displayTitle = 1;
			DisDesbGrid.canChk = 0;
			DisDesbGrid.hiddenSubtraction = 0;
			DisDesbGrid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}

	function initHide(tContNo, tPrtNo, tLoadFlag) {
		fm.all('ContNO').value = tContNo;
		fm.all('PrtNo').value = tPrtNo;
		if (tLoadFlag == '1') {
			divUWDis1.style.display = "none";
		}
	}
</script>


