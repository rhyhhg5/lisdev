<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
///GlobalInput中存放的是全局变量（管理机构等变量）
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI"); //session中取到存放全局变量的GlobalInput对象赋值给tGI（GlobalInput对象）
tGI.ManageCom = "86";

%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; 	//记录管理机构
    var comcode = "<%=tGI.ComCode%>";		//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LPWBContInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LPWBContInit.jsp" %>  
<title>个人保单</title>
</head>
  
<body onload="initForm();">
		<form action="LPWBContSave.jsp" method="post" name="fm" target="fraSubmit">
			<table class="common">
				<TR class=common>
					<TD class=title>保单号</TD>
					<TD class="input"><input class="common" name="PolicyNo" verify="保单号|NUM" /></TD> 
					<TD class=title>管理机构</TD>
					<TD class="input"><Input class="codeNo" name="manageCom" verify="管理机构|NOTNULL&code:comcode"  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></TD>
					<TD class=title>是否配置</TD>
					<TD class="input"><input class="codeNo" name="isDeploy" value="0" CodeData="0|^0|未配置^1|已配置"  ondblclick="return showCodeListEx('sex',[this,isDeployName],[0,1]);" onkeyup="showCodeListKeyEx('sex',[this,isDeployName],[0,1]);"><Input class=common name= isDeployName value="未配置" ></TD>
				</TR>
			</table>
			<br>
	    	<INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQueryClick();">
	    	
	    <Div  id= "divLPWBContGrid" align=center style= "display: ''">
	        <Table  class= common>
	            <TR  class= common>
	             <TD text-align: left colSpan=1>
	                 <span id="spanLPWBContGrid" ></span> 
	       	    </TD>
	           </TR>
	         </Table>					
	           <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">  
	           <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	           <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	           <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
	      </Div>
	      <br/>
	      <table class="common">
				<TR class=common>
					<TD class=title>保单号</TD>
					<TD class="input"><input class="common" name="PolicyNo1"  /></TD> 
					<TD class=title>管理机构</TD>
					<TD class="input"><Input class="codeNo" name="ManageCom1" verify="管理机构|NOTNULL&code:comcode"  ondblclick="return showCodeList('comcode',[this,ManageComName1],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName1],[0,1]);"><input class=codename name=ManageComName1 readonly=true ></TD>
				</TR>
			</table>
			<br/>
			<div align="center">
				<input type="hidden" name="methodName" value="" />
		        <INPUT VALUE="添  加" class= cssButton TYPE=button onclick="insertCode();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <input value="删  除" class="cssButton" type="button" onclick="deleteCode();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  </div>
		</form>
		<!-- （双击）显示下拉列表 -->
		<span id="spanCode"  style="display:none; position:absolute; slategray"></span>
</body>
</html>
