//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;
var mDebug="0";
var arrStrReturn = new Array();
var arrGrid;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	try {
		fm.sql_where.value = eval("top.opener.fm.sql_where.value");
	} catch (ex) {
		fm.sql_where.value = "";
	}

  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    parent.fraSubmit.getGridResult();
    
    arrGrid = null;
    if( arrStrReturn[0] == '0|0^' ) {
    	SysCertifyGrid.clearData();
    } else {
			arrGrid = decodeEasyQueryResult(arrStrReturn[0]);
    	useSimulationEasyQueryClick(arrStrReturn[0]);
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在SysCertTakeBack.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function easyQueryClick()
{
	// 初始化表格
	initSysCertifyGrid();
	// 书写SQL语句
	var strSQL = "";	
    strSQL = "SELECT LWMission.MissionProp4,LWMission.MissionProp3,LWMission.MissionProp5,LWMission.MissionProp6,"
       + " case when LWMission.MissionProp7 is not null and LWMission.MissionProp7 != '' and substr(LWMission.MissionProp7,1,1) = 'D' then (select groupagentcode from laagent where agentcode = substr(LWMission.MissionProp7,2)) else LWMission.MissionProp7 end,"
       + " LWMission.MissionProp8, LWMission.MissionProp9,LWMission.MissionProp10,LWMission.MissionProp11,LWMission.MissionProp12,LWMission.MissionProp13,LWMission.MissionProp1 ,LWMission.MissionProp2 ,LWMission.MissionID ,LWMission.SubMissionID FROM LWMission WHERE  LWMission.ActivityID in ('0000001111','0000001112','0000001019','0000001113','0000001025')  " 
	   + "and LWMission.ProcessID = '0000000003'"
	   + getWherePart('LWMission.MissionProp4', 'CertifyCode') 
	   + getWherePart('LWMission.MissionProp3', 'CertifyNo')
	   + getWherePart('LWMission.MissionProp6','SendOutCom')
	   + getWherePart('LWMission.MissionProp7','ReceiveCom')
	   + getWherePart('LWMission.MissionProp8','Handler')
	   + getWherePart('LWMission.MissionProp9','HandleDate');
		  
	turnPage.strQueryResult  = easyQueryVer3(strSQL);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要回收的通知书！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  arrGrid = turnPage.arrDataCacheSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = SysCertifyGrid ; 
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}


function returnParent()
{
  try
  {
  	top.opener.afterQuery(getQueryResult());
  }
  catch(ex)
  {
    alert("没有发现父窗口的afterQuery接口。"+ex);
  }
  top.close();
}


function getQueryResult()
{
  var arrResult=null;
  tRow = SysCertifyGrid.getSelNo();
  if (tRow==0 || tRow==null )
    return arrResult;
  arrResult=new Array();
  //设置需要返回的数组
  arrResult[0]=arrGrid[tRow-1];
  return arrResult;
}

