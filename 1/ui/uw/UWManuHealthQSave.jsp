<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuHealthQChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
 
 	String tContNo = request.getParameter("ContNo");
 	String tProposalContNo =request.getParameter("ContNo");
 	String tCustomerNo=request.getParameter("InsureNo");
 	String tPrtSeq=request.getParameter("PrtSeq");
 	String tNote=request.getParameter("Note");
 	System.out.println("tContNo="+tContNo);
 	System.out.println("Note="+tNote);
 	System.out.println("tPrtSeq="+tPrtSeq);
 	
 	LCPENoticeResultSchema tLCPENoticeResultSchema ;
 	LCPENoticeResultSet tLCPENoticeResultSet = new LCPENoticeResultSet();
 	
	int lineCount = 0;
	String arrCount[] = request.getParameterValues("DisDesbGridNo");

	if(arrCount!=null)
	{
		String tDisDesb[]=request.getParameterValues("DisDesbGrid1");
		String tDisResult[]=request.getParameterValues("DisDesbGrid2");
		String tICDCode[]=request.getParameterValues("DisDesbGrid3");
		//String tDiseaseCode[]=request.getParameterValues("DisDesbGrid4");

		
		lineCount = arrCount.length;
		System.out.println("lineCount="+lineCount);
		for(int i = 0;i<lineCount;i++)
		{
			tLCPENoticeResultSchema = new LCPENoticeResultSchema();
			tLCPENoticeResultSchema.setContNo(tContNo);
			tLCPENoticeResultSchema.setProposalContNo(tProposalContNo);
			tLCPENoticeResultSchema.setPrtSeq(tPrtSeq);
			tLCPENoticeResultSchema.setCustomerNo(tCustomerNo);
			tLCPENoticeResultSchema.setDisDesb(tDisDesb[i]);
			System.out.println("tDisDesb="+tDisDesb[i]);
			System.out.println("tDisResult="+tDisResult[i]);
			System.out.println("tICDCode="+tICDCode[i]);
			tLCPENoticeResultSchema.setDisResult(tDisResult[i]);
			tLCPENoticeResultSchema.setICDCode(tICDCode[i]);       
			//tLCPENoticeResultSchema.setDiseaseCode(tDiseaseCode[i]);
			tLCPENoticeResultSchema.setRemark(tNote);
			tLCPENoticeResultSet.add(tLCPENoticeResultSchema);
		}
	}
	else
	{
	}
	
	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";

	tVData.add(tG);
	tVData.add(tLCPENoticeResultSet);
	
	DisDesbUI tDisDesbUI = new DisDesbUI();
	
	try{
		System.out.println("this will save the data!!!");
		tDisDesbUI.submitData(tVData,"");
	}
	catch(Exception ex){
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	
	if (!FlagStr.equals("Fail")){
		tError = tDisDesbUI.mErrors;
		if (!tError.needDealError()){
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
