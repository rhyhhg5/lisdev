<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：UWManuHealth.jsp
//程序功能：承保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
	
	<script>
		var PEFlag = "<%=request.getParameter("PEFlag")%>";
		var ContNo = "<%=request.getParameter("ContNo2")%>";
		var PrtSeq = "<%=request.getParameter("PrtSeq")%>";
				var peaddflag = "<%=request.getParameter("peaddflag")%>";
		var ProposalContNo = "<%=request.getParameter("ProposalContNo")%>";
			var currentDate = "<%=PubFun.getCurrentDate()%>";
	</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <!--<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>-->
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./UWManuHealth.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UWManuHealthInit.jsp"%>
  <title> 新契约体检资料录入 </title>
  
</head>
<body  onload="initForm('<%=tContNo%>','<%=tMissionID%>','<%=tSubMissionID%>','<%=tPrtNo%>');" >
  <form method=post name=fm target="fraSubmit" action= "./UWManuHealthChk.jsp">
    <!-- 非列表 -->
      <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>体检基本信息</td>
	</tr>
    </table>
    <table  class= common align=center>
    	<TR  class= common>
          <!--<TD  class= title>  合同号码  </TD>-->
           <Input type= "hidden" class="readonly" name=ContNo > 
           <INPUT  type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
           <INPUT  type= "hidden" class= Common name= SubMissionID value= "">
           <INPUT  type= "hidden" class= Common name= PrtNo value= "">
          <!--
          <TD  class= title COLSPAN="1">  打印状态 </TD>
          <TD  class= input COLSPAN="1">  <Input class="readonly" name=PrintFlag > </TD>
          -->
           <INPUT  type= "hidden" class="readonly" name=PrintFlag >
           <TD  class= title>  体检人  </TD>
          <TD  class= input> <Input class=codeno name=InsureNo  ondblClick="showCodeListEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" onFocus= "easyQueryClickSingle();"><input class=codename name=InsureNoName readonly=true > <!-- onFocus= "easyQueryClickSingle();easyQueryClick();"--> </TD>
	        <TD class=title  id=flag456 style='display:none'>
			      增项标识
			    </TD>
			    <TD class=input id=flag457 style='display:none'>
			      <Input   name="Pestateflag" class=common style='display:none' readonly >
			    </TD>

         
          <TD  class= title8 COLSPAN="1"> 体检日期   </TD>
          <TD  class= common >  <Input class=readonly  name=EDate  readonly>  </TD>
        </TR>
        <TR  class= common>
          <TD  class= title COLSPAN="1" >    体检医院  </TD>
           <TD  class= input> 
          <Input class= 'codeNo' name=Hospital onFocus= "checkInsureNo();" ondblclick="return showCodeList('lhhospitnamehtuw',[this,HospitName],[0,1],null,fm.ContNo.value,'managecom',1,350);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitnameht',[this,HospitName],[0,1],null,fm.ContNo.value,'HospitName',1,350)	; "><Input  class=codename  name=HospitName >
          <!--<Input class=code name=Hospital ondblclick="OpenHospital();">-->  </TD>
          <TD  class= title COLSPAN="1">    体检状态  </TD>
          <td class=common>
          <Input class=codeno name=PEState  readonly=true verify="体检状态|code:pecode" ondblclick="return showCodeList('pecode',[this,PEStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('pecode',[this,PEStateName],[0,1],null,null,null,1);" ><input class=codename name=PEStateName  elementtype=nacessary > 
        </td>
          <!--TD  class= title>  是否空腹 </TD>
          <TD  class= input-->   
          	<Input class=code type=hidden name=IfEmpty ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">  
        </TR>
        </table>

 

<Div  id= "divinfo" style= "display: 'none'">

          <table  class= common align=center> 
          	 
      <TR  class= common>
        <TD CLASS=title>
            年龄
        </TD>
        <TD CLASS=input >
        	<Input NAME=Birthday type="hidden" readonly=true CLASS=common  >
            <Input NAME=InsuredAppAge VALUE="" readonly=true CLASS=common  >
        </TD>
        <TD  class= title>
            性别
        </TD>
        <TD  class= input>
            <Input class=codeno name=Sex readonly=true ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName readonly=true elementtype=nacessary>
        </TD> 
        <TD  class= title>
           证件类型
        </TD>
        <TD  class= input>
           <Input class=codeno name=IDType  readonly=true value="0" ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=IDTypeName readonly=true elementtype=nacessary > 
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
           证件号码
        </TD>
        <TD  class= input>
           <Input class= common name="IDNo"  readonly=true onblur="checkidtype();getBirthdaySexByIDNo(this.value);getallinfo();"   >
        </TD>
        <TD  class= title>
	         办公电话
	      </TD>
	      <TD  class= input>
	         <Input class= common name="GrpPhone" readonly=true >
	      </TD> 
	      <TD class=title>
		      住宅电话
		    </TD>
		    <TD class=input>
		      <Input name="HomePhone" class=common readonly=true>
		    </TD> 
		 </TR>   
		 <TR  class= common>                          
	      <TD  class= title>
	         移动电话
	      </TD>
	      <TD  class= input>
	         <Input class= common name="Mobile"  readonly=true>
	      </TD>
	      <TD class=title>
				  家庭地址
				</TD>
				<TD class=input colspan=3>
				  <Input name="HomeAddress" class=common3 readonly=true>
				</TD>
	 	</TR>
          	 
          	</table>

</Div>	
			<br>
			<table>
    			<tr>
        			<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustom);"></td>
    				<td class= titleImg>体检人信息</td>                            
    			</tr>	
    		</table>
			<Div id="divCustom" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanCustomerGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<br>
			
    <table>
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg>	 未通过自核的体检原因</td>                            
    	</tr>	
    </table>
    <div id="meal" style="display: ''">
    <table class=common >
    	
    	<tr class=common>
    		
    	<TD  class= title>套餐</TD>
       <TD  class= input><Input class=code name=TestGrpCode  readonly=true  onFocus= "checkInsureNo();" ondblclick="return showCodeList('testgrpname',[this,GrpTestCode],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('testgrpname',[this,GrpTestCode],[0,1]);">  </TD>
        <INPUT  type= "hidden" class= Common name= GrpTestCode value= ""> 
      <td></td><td></td>
      </tr>
     
    </table>
   </div>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanUWErrGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    	
      </div>
    <table>
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg>	 体检项目录入</td>
    		                             
    	</tr>	
    </table>
   
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanHealthGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    	<Div id= "divPage123" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
  </Div>
      </div>
      <TD  class= title>价格</TD>
       <TD  class= input><Input class=readonly name=TestFee readonly>  </TD>
       
      
		    
      <td></td><td></td>
      <br><br>
       <table class=common>
         <TR  class= common> 
           <TD  class= common>体检原因 <Input class=code name=Reason  readonly=true ondblclick="return showCodeList('testreason',[this,Note],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('testreason',[this,Note],[0,1]);"></TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Note" cols="120" rows="3" class="common" verify="体检原因|len<=100">
             </textarea>
           </TD>
         </TR>
      </table>
      <div id="divAddPeItem" style="display: ''">
      	<INPUT type= "button" name= "sure" value="确  认" class=CssButton onclick="submitForm()">	
      	<INPUT type= "button" name= "Operate" value="删  除" class=CssButton onclick="deleteClick()">	      
      	<INPUT type= "button" name= "SendButton" value="发体检通知书" class=CssButton onclick="sendHealth()">	          
      	<INPUT type= "button" name= "ReturnParentButton" value="返  回" class=CssButton onclick="returnParent()">	          
    	</div>		
    	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanHistroyGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    	
    	<div id="divBooking" style="display: 'none'">
    		<TD  class= title8>预约日期</TD>
       <TD  class= input8><Input class="coolDatePicker" dateFormat="short" name=BookingDate verify="预约日期|date&notnull">  </TD>
       <TD  class= title>时间</TD>
       <TD  class= input><Input class=common name=BookingTime onclick="dFirstRecoTime()" verify="时间|notnull">  </TD>
      	<INPUT type= "button" name= "ConfomBooking" value="确  认" class=CssButton onclick="addBookingTime()">
      	<INPUT type= "button" name= "sure" value="体检项目变更" class=cssButton  onclick="addPEItem();">
       <input type=hidden name=hideOperate value=''>       
        <!--<Input class=common name=sql >-->
    	</div>
    <!--读取信息-->
  </form>
  <span id="spanCode"  style="display: ''; position:absolute; slategray"></span>
</body>
</html>