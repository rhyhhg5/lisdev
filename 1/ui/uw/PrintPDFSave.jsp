<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.LOPRTManagerSchema"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.io.*"%>
<%
  System.out.println("--------------------PDFPrintStart------------------");
  CError cError = new CError();
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  String PrtSeq = request.getParameter("PrtSeq");
  System.out.println("PrtSeq"+PrtSeq);
  String tMissionID = request.getParameter("MissionID");
  String tSubMissionID = request.getParameter("SubMissionID");
  String tContNo = request.getParameter("ContNo");
  String tPrtNo = request.getParameter("PrtNo");
  String tOperate = request.getParameter("fmtransact");
  String StrURL = "";
  String RePrintFlag = request.getParameter("RePrintFlag");
  String tCode = request.getParameter("Code");
  String aCode = tCode.substring(1);
  System.out.println("PDF单证aCode : " + aCode);
  String tOutXmlPath = application.getRealPath(""); //xml文件输出路径
  String tfilename = tCode + "-" + tPrtNo;
  String tprtflag = request.getParameter("PrtFlag");
  String sOutXmlPath = application.getRealPath("printdata/data/briefpdf/") + "/"; //pdf文件输出路径
  String tPrintServerInterface = (new ExeSQL()).getOneValue("select sysvarvalue from LDSYSVAR where sysvar='PrintServer'");
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  //tG.ClientIP = request.getRemoteAddr();
  tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
  tG.ServerIP = tG.GetServerIP();
  System.out.println("IP's address :" + tG.ClientIP);
  System.out.println("IP's address :" + tG.ServerIP);
  //这里可以得到服务器的IP,但正式机采用了多个网卡映射了多个内部IP,所以可能存在问题,暂时不采用,以待验证. huxl
  
  VData tVData = new VData();
  VData mResult = new VData();
  CErrors mErrors = new CErrors();
  TransferData tTransferData = new TransferData();
  LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
  tLOPRTManagerSchema.setPrtSeq(PrtSeq);
  tLOPRTManagerSchema.setCode(tCode);
  
  tTransferData.setNameAndValue("RePrintFlag", RePrintFlag);
  tVData.addElement(tLOPRTManagerSchema);
  tVData.addElement(tG);
  tVData.addElement(tOutXmlPath);
  tVData.addElement(tPrtNo);
  tVData.addElement(tTransferData);
  
  PrintPDFManagerBL tPrintPDFManagerBL = new PrintPDFManagerBL();
  XmlExport txmlExport = new XmlExport();
  if (!tPrintPDFManagerBL.submitData(tVData, tOperate)) {
    operFlag = false;
    Content = tPrintPDFManagerBL.mErrors.getFirstError().toString();
  }
  else {
    mResult = tPrintPDFManagerBL.getResult();
    txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
    if (txmlExport == null) {
      operFlag = false;
      Content = "没有得到要显示的数据文件";
    }
  }
  
  System.out.println("打开pdf页面 : " + operFlag);
  if (operFlag == true) {
    System.out.println("come in the Open");
    String url = tPrintServerInterface + "&filename=" + tfilename + "&action=preview";
    System.out.println(url);
    response.sendRedirect(url);
    FlagStr = "Succ";
    System.out.println("Open Over");
  }
  else {
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%
}
%>
