//程序名称：GroupUW.js
//程序功能：集体人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容


/*********************************************************************
 *  //该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var mSwitch = parent.VD.gVSwitch;
 var Resource;

/*********************************************************************
 *  提交对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);
  fmQuery.submit(); //提交
  alert("submit");
}



/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

  }
  else
  {
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
}



function afterSubmit1( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

  }
  else
  {
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showFeeRate();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}



/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}


/*********************************************************************
 *  查询团体单下个人单既往投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  var cInsureNo = fmQuery.InsuredNo.value;
  if(cProposalNo==""||cProposalNo==null|| cInsureNo==""||cInsureNo==null)
  {
    showInfo.close();
    alert("请选择个人保单,后查看其信息!");
    return ;
  }
  //showModalDialog("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单下个人单以往核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==""||cProposalNo==null)
  {
    showInfo.close();
    alert("请选择个人保单,后查看其信息!");
    return ;
  }
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单下个人单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==""||cProposalNo==null)
  {
    showInfo.close();
    alert("请先选择个人保单,后查看其信息!");
    return ;
  }
  window.open("./UWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGNewUWSub()
{
  var cGrpContNo=fmQuery.GrpContNo.value;
  if(cGrpContNo==""||cGrpContNo==null)
  {
    alert("请先选择一个团体投保单!");
    return ;
  }
  window.open("./UWGErrMain.jsp?GrpContNo="+cGrpContNo,"window1");
}



/*********************************************************************
 *  查询团体下的附属于各团单的个人保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==null||cProposalNo=="")
  {
    showInfo.close();
    alert("请先选择个人保单,后查看其信息!");

  }
  else
  {
    mSwitch.deleteVar( "PolNo" );
    mSwitch.addVar( "PolNo", "", cProposalNo );
    window.open("../app/ProposalMain.jsp?LoadFlag=4");
    showInfo.close();
  }
}


/*********************************************************************
 *  查询团体下个人保单体检资料
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if (cProposalNo != "")
  {
    window.open("./UWManuHealthMain.jsp?ProposalNo1="+cProposalNo,"window1");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择个人保单,后查看其信息!");
  }
}


/*********************************************************************
 *  对团体单下的个人加费承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  tUWIdea = fmQuery.all('UWIdea').value;
  if (cProposalNo != "")
  {
    window.open("./UWGSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag=2&UWIdea="+tUWIdea,"window1");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择一个个人投保单!");
  }
}


/*********************************************************************
 *  对团体单特约承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.GrpProposalContNo.value;
  var tUWIdea = fmQuery.GUWIdea.value;
  if (cProposalNo != "")
  {
    window.open("./UWGrpSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag=1&UWIdea="+tUWIdea,"window1");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择一个团体保单!");
  }
}


/*********************************************************************
 *  对团体主险投保单的个人体检资料查询(团体单体检件只容许录入到主险个单上)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealthQ()
{
  var cProposalNo=fmQuery.ProposalNo.value;
  var cGrpProposalContNo=fmQuery.GrpProposalContNo.value;
  var cGrpMainProposalNo=fmQuery.GrpMainProposalNo.value;

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

  if (cProposalNo != "" && cGrpProposalContNo == cGrpMainProposalNo)
  {
    window.open("./UWManuHealthQMain.jsp?ProposalNo1="+cProposalNo,"window1");

  }
  else
  {

    alert("请先选择一个团体主险投保单下的个人投保单!");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //initPolGrid();
  fmQuery.submit(); //提交
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  showModalDialog("./ProposalDuty.jsp",window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=13cm");

}

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  showModalDialog("./ProposalFee.jsp",window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");

}


/*********************************************************************
 *  显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


/*********************************************************************
 *  查询团体单下主附团体投保单单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function querygrp()
{
  // 初始化表格
  initPolBox();
  initGrpGrid();

  // 书写SQL语句
  var str = "";
  var PrtNo = fmQuery.all('PrtNoHide').value;
  var mOperate = fmQuery.all('Operator').value;

  var strsql = "";
  //	strsql = "select LCGrpPol.GrpProposalContNo,LCGrpPol.prtNo,LCGrpPol.GrpName,LCGrpPol.RiskCode,LCGrpPol.RiskVersion,LCGrpPol.ManageCom from LCGrpPol,LCGUWMaster where 1=1 "
  //			 	 + " and LCGrpPol.AppFlag='0'"
  //				 + " and LCGrpPol.ApproveFlag in('2','9') "
  //				 + " and LCGrpPol.UWFlag in ('2','3','5','6','8','7')"
  //				 + " and LCGrpPol.contno = '00000000000000000000'"				          //自动核保待人工核保
  //				 + " and LCGrpPol.PrtNo = '"+PrtNo+"'"
  //				 + " and LCGrpPol.GrpPolNo = LCGUWMaster.GrpPolNo"
  //				 + " and (LCGUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') or LCGUWMaster.appgrade is null)"
  //				 + " order by LCGrpPol.makedate ,LCGrpPol.maketime" ;
  strsql = "select a.ContPlanCode,a.contplanname,a.peoples2,"
           +" '',"//男女比例
           +" ((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2' and b.ContPlanCode=a.ContPlanCode and b.ContPlanCode not in ('00','11'))/(select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.ContPlanCode=a.ContPlanCode and b.ContPlanCode not in ('00','11')))*100,"
           +" (select varchar(round(sum((to_date(current date)-to_date(b.Birthday))/365)/(select count(1) from lcinsured b where a.grpcontno=b.grpcontno),2)) from lcinsured b where b.grpcontno=a.grpcontno and b.ContPlanCode=a.ContPlanCode) ,"
           +" d.RiskCode,e.RiskName ,'',"
           +" (select sum(standPrem) from lcpol where lcpol.grpcontno=a.grpcontno and lcpol.riskcode=d.riskcode and lcpol.insuredno in (select insuredno from lcinsured where lcinsured.grpcontno=d.grpcontno and lcinsured.contplancode=d.contplancode)) ,"
           +" (select sum(Prem) from lcpol where lcpol.grpcontno=a.grpcontno and lcpol.riskcode=d.riskcode and lcpol.insuredno in (select insuredno from lcinsured where lcinsured.grpcontno=d.grpcontno and lcinsured.contplancode=d.contplancode)) , "
           //+" varchar(((select sum(Prem) from lcpol where lcpol.grpcontno=a.grpcontno and lcpol.riskcode=d.riskcode and lcpol.insuredno in (select insuredno from lcinsured where lcinsured.grpcontno=d.grpcontno and lcinsured.contplancode=d.contplancode)))/(select count(1) from lcinsured where lcinsured.contplancode=d.contplancode and lcinsured.grpcontno=d.grpcontno)), "
           +" '' "
           +" from LCContPlan a,LCContPlanRisk d,LMRisk e where 1=1 and e.RiskCode=d.RiskCode and a.grpcontno=d.grpcontno and a.ContPlanCode=d.ContPlanCode and a.GrpContNo='"+GrpContNo+"' and a.contplancode not in ('00','11')";

  //execEasyQuery( strSQL );
  //查询SQL，返回结果字符串
  //turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    //alert("没有符合条件集体单！");
    //return "";
  }
  else
  {
    ///查询成功则拆分字符串，返回二维数组
    //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //
    ////设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    //turnPage.pageDisplayGrid = GrpGrid;
    //
    ////保存SQL语句
    //turnPage.strQuerySql = strsql;
    //
    ////设置查询起始位置
    //turnPage.pageIndex = 0;
    //
    ////在查询结果数组中取出符合页面显示大小设置的数组
    //var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //
    ////调用MULTILINE对象显示查询结果
    //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }



  var arrSelected = new Array();
  var strSQL = " select GrpContNo,PrtNo,ManageCom,SaleChnl,AgentCom,getUniteCode(AgentCode),AgentGroup,AppntNo,VIPValue, "  
							+" BlacklistFlag,PolApplyDate,CValiDate from lcgrpcont,LDGrp where proposalgrpcontno='"+GrpContNo+"' " 
							+" and lcgrpcont.AppntNo = LDGrp.CustomerNo "                                                  
							+" union "                                                                                     
							+" select GrpContNo,PrtNo,ManageCom,SaleChnl,AgentCom,getUniteCode(AgentCode),AgentGroup,AppntNo,VIPValue, " 
							+" BlacklistFlag,PolApplyDate,CValiDate from lbgrpcont,LDGrp where proposalgrpcontno='"+GrpContNo+"' " 
							+" and lbgrpcont.AppntNo = LDGrp.CustomerNo "                                                  
							+" union "                                                                                     
							+" select GrpContNo,PrtNo,ManageCom,SaleChnl,AgentCom,getUniteCode(AgentCode),AgentGroup,AppntNo,VIPValue, " 
							+" BlacklistFlag,PolApplyDate,CValiDate from lobgrpcont,LDGrp where proposalgrpcontno='"+GrpContNo+"' " 
							+" and lobgrpcont.AppntNo = LDGrp.CustomerNo  "           
 									;
  //alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  if (turnPage.strQueryResult)
  {
    fmQuery.all('GrpContNo').value = arrSelected[0][0];
    fmQuery.all('PrtNo').value = arrSelected[0][1];
    fmQuery.all('ManageCom').value = arrSelected[0][2];

    fmQuery.all('ManageCom_ch').value = easyExecSql("select codename from ldcode where codetype='station' and code='"+arrSelected[0][2]+"'");
    fmQuery.all('SaleChnl').value = arrSelected[0][3];
    fmQuery.all('SaleChnl_ch').value = easyExecSql("select codename from ldcode where codetype='salechnl' and code='"+arrSelected[0][3]+"'");

    fmQuery.all('AgentCom').value = arrSelected[0][4];
    if(arrSelected[0][4]=="")
    {
      fmQuery.all('AgentCom_ch').value="";
    }
    else
    {
      fmQuery.all('AgentCom_ch').value = easyExecSql("select name from lacom where AgentCom='"+arrSelected[0][4]+"'");
    }

    fmQuery.all('AgentCode').value = arrSelected[0][5];
    fmQuery.all('AgentCode_ch').value = easyExecSql("select name from laagent where groupagentcode='"+arrSelected[0][5]+"'");
    fmQuery.all('AgentGroup').value = arrSelected[0][6];
    fmQuery.all('AgentGroup_ch').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrSelected[0][6]+"'");
    fmQuery.all('AppntNo').value = arrSelected[0][7];
    fmQuery.all('VIPValue').value = arrSelected[0][8];
    fmQuery.all('BlacklistFlag').value = arrSelected[0][9];
    fmQuery.all('PolApplyDate').value = arrSelected[0][10];
    fmQuery.all('CValiDate').value = arrSelected[0][11];
  }
  	var tloadFlag=fmQuery.LoadFlag.value;
  	if(tloadFlag!=1){
  initSendTrace();
	}else if(tloadFlag==1){
	initQuerySendTrace();
	}
  showGrpAppntInfo();
  initQueryUWidea();
  initQuerySendReason();
  return true;
}


/*********************************************************************
 *  查询团体单下自动核保未通过的个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPol()
{
  var GrpProposalContNo = fmQuery.all('GrpProposalContNo').value;
  var mOperate = fmQuery.all('Operator').value;
  var strsql = "";

  // 初始化表格
  showDiv(divPerson,"false");
  initPolBox();
  //initPolGrid();
  // 书写SQL语句

  strsql = "select LCPol.ProposalNo,LCPol.AppntName,LCPol.RiskCode,LCPol.RiskVersion,LCPol.InsuredName,LCPol.ManageCom from LCPol where 1=1 "
           + "and LCPol.GrpPolNo = '"+GrpProposalContNo+"'"
           + " and LCPol.AppFlag='0'"
           + " and LCPol.UWFlag in ('3','5','6','8','7')"         //自动核保待人工核保
           + " and LCPol.contno = '00000000000000000000'"
           + " order by LCPol.makedate ";

  //alert(strsql);
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("此单下个人单核保均已全部通过！");
    return "";
  }

  showDiv(divPerson,"true");
  //个人体检资料录入只针对团体主险投保单下的个人单
  if(GrpProposalContNo == fmQuery.all('GrpMainProposalNo').value)
  {
    showDiv(divBoth,"true");
    showDiv(divAddFee,"false");
  }
  else
  {
    showDiv(divBoth,"false");
    showDiv(divAddFee,"true");
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strsql;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


/*********************************************************************
 *  查询集体下未过个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getGrpPolGridCho()
{
  fmQuery.PolTypeHide.value = '2';
  fmQuery.submit();
}



/*********************************************************************
 *  选择要人工核保保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolGridCho()
{
  fmQuery.PolTypeHide.value = '1';
  fmQuery.submit();
}


/*********************************************************************
 *  个人单核保确认
 *  UWState:(0 未核保 1拒保 2延期 3条件承保 4通融 5自动 6待上级 7问题件 8核保通知书 9正常 a撤单 b保险计划变更)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function manuchk()
{
  var cProposalNo=fmQuery.ProposalNo.value;
  var flag = fmQuery.all('UWState').value;
  var tUWIdea = fmQuery.all('UWIdea').value;

  if (flag == "0"||flag == "1"||flag == "4"||flag == "9")
  {
    //showModalDialog("./UWManuNormMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
    fmQuery.action = "./UWManuNormChk.jsp";
    fmQuery.submit();
  }
  if (flag == "")
  {
    alert("选择核保结论！")
  }

  initPolBox();
  //initPolGrid();
}


/*********************************************************************
 *  团体整单核保确认
 *  (0 未核保 1拒保 2延期 3条件承保 4通融 5自动 6待上级 7问题件 8核保通知书 9正常 a撤单 b保险计划变更)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function gmanuchk(flag)
{	
	var conMark = flag;
	if(flag == 0 || flag == 1){
		var risksql="select 1 from lcgrppol where prtno='"+fmQuery.PrtNo.value+"' and riskcode in ('163001','163002')";
		var riskarr=easyExecSql(risksql);
		var totalFactorsql = "select totalfactor from lcgrpcontroad where prtno='"+fmQuery.PrtNo.value+"'";
		var totalFactor=easyExecSql(totalFactorsql);
		var comcodesql="select 1 from lduser where usercode='"+operator+"' and comcode='86'";
		var comcode=easyExecSql(comcodesql);
		//一带一路险种，并且浮动比例不为空
		if(riskarr){
			if(totalFactor){
				var f = parseFloat(totalFactor)/100;
				if(f<1&&!comcode){
					alert("保费浮动比例小于1，需上报审批！");
					gmanuchk(3);
					return;
				}
			}else{
				alert("获取数据失败！");
				return;
			}
		}
		
		//无名单核保规则
		var nonameSql = "select 1 from lccont where prtno='"+fmQuery.PrtNo.value+"' and insuredname='无名单' and poltype='1' ";
		var noname = easyExecSql(nonameSql);
		if(noname&&!comcode){
			alert("无名单团体保单必须由总公司核保用户进行确认，已自动上报上级！");
			gmanuchk(3);
			return;
		}
		
		//270102
		if(!check270102()){
	    	return false;
	    }
	}
	//alert();
	//alert(fmQuery.GrpContNo.value);
	var qurSql="select bussno from es_issuedoc where bussno like '"+fmQuery.PrtNo.value+"%' and status='1' and stateflag='1'";
 	var arrqurSql=easyExecSql(qurSql);
 	if(arrqurSql){
 		if (!confirm("该单还有扫描修改申请待审批，确认要继续吗？"))
	       {
	         	return;
	       }
 		}
	var strsql="select count(1) from LCGrpIssuepol where grpcontno='"+fmQuery.GrpContNo.value+"' and state<>'5'";
	var arr=easyExecSql(strsql);
	if(arr)
	{
		if(arr[0][0]>0){
		if( confirm("该保单有未回销的问题件!强制通过人工核保点击“确定”，否则点“取消”。"))
		{
		}
		else
		{
			return;
		}
		}
	}
	var strSql = "select count(1) from lccont where uwflag not in ('1','4','9','a') and grpcontno='"+fmQuery.GrpContNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		if(arr[0][0]>0){
			alert("团单下还有个单没有核保结论！");
			return;
		}
	}
	
	var sql1 = " select count(1) from lccont where grpcontno='" + fmQuery.GrpContNo.value + "' and uwflag='a' ";
	var	sql2 = " select count(1) from lccont where grpcontno='" + fmQuery.GrpContNo.value + "'";
	var arr1 = easyExecSql(sql1);
	var arr2 = easyExecSql(sql2);
	if(arr1&&arr2){
		if(arr1[0][0]==arr2[0][0]){
		alert("团单下所有分单为撤销状态，核保失败！");
		return;
		}
	}

    //再保校验。
	if(flag == 1||flag == 0)
	{
	  if(!checkReInsure())
	  {
	    return false;
	  }
    }

	var strSql = "select count(1) from LCPol where uwflag not in ('1','4','9','a') and grpcontno='"+fmQuery.GrpContNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		if(arr[0][0]>0){
			alert("团单下还有个单没有核保结论！");
			return;
		}
	 }	
    //var tSaleChnl = fmQuery.SaleChnl.value; 
    //if(tSaleChnl == "03")
    //{
    //    if(fmQuery.AgentcyRate.value == "")
	//    {
	//        alert("请录入中介手续费");
	//        return false;
	//    }
	//    if(fmQuery.AgentcyRate.value == 0)
	//    {
	//    	if(!confirm("中介手续费录入为0！是否继续？"))
	//		{
	//			return false;
	//		}
	//    }
    //}
	
  if(flag==1)
  {
    fmQuery.all('YesOrNo').value = "Y";
  }
  if(flag ==2)
  {
    fmQuery.all('YesOrNo').value = "N";
  }
  if(flag ==3)
  {
    fmQuery.all('GrpSendUpFlag').value = "3";
  }
  var chk = chkUWIdea();
  if(chk == false )
  {
    return false;
  }
  var cGrpContNo=fmQuery.GrpContNo.value;
  var flag = fmQuery.all('GUWState').value;
  var tUWIdea = fmQuery.all('GUWIdea').value;
//alert("flag:"+flag);
  if( cGrpContNo == "" || cGrpContNo == null )
  {
    alert("请先选择一个团体投保单!");
    cancelchk();
    return ;
  }

  if (flag == null || flag == "")
  {
    alert("请先选择核保结论!");
    cancelchk();
    return ;
  }

  //撤单,上报,拒保,发加费通知书均是针对团体单中的主险投保单进行
  if(flag == "1"||flag == "6"||flag == "a"||flag == "7"||flag == "8")
  {
    /*		if( cGrpContNo != fmQuery.GrpMainProposalNo.value)
    		{
    			alert("请先选择一个团体主险投保单!");
    	        cancelchk();
    		    return ;
    		    }
    */
  }
  //投被保人黑名单校验
  if(flag == "9"){
	  if(conMark == "0" || conMark == "1"){
		  if(!checkBlackName()){
			  return false;
		  }
	  }
  }
  //操作功能实现
  if (flag == "1"||flag == "4"||flag == "6"||flag == "9"||flag == "8"||flag == "a")
  {
    fmQuery.WorkFlowFlag.value = "0000002004";
    fmQuery.MissionID.value = MissionID;
    fmQuery.SubMissionID.value = SubMissionID;
    //showModalDialog("./UWGrpManuNormMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fmQuery.action = "./UWConfirm.jsp";
    fmQuery.submit(); //提交
  }

  if (flag == "2") //延期
  {
    window.open("./UWManuDateMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
  }

  if (flag == "3") //条件承保
  {
    window.open("./UWManuSpecMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
  }

  if (flag == "7")//将问题件发送到相应的处理岗位待(管理中心或问题修改或复核修改)处理.
  {
    showModalDialog("./UWGrpManuNormMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  }

  //if(flag != "8"&&flag != "7")
  //{
  //	initInpBox();
  //	initPolBox();
  //  initGrpGrid();
  // //querygrp();
  //}
}

/**
1、符合再保审核条件，但没有发送给再保模块，需要给出提示
2、若有未结案的再保审核任务，需要给出提示
*/
function checkReInsure()
{
  var sql = "select 1 from LCUWError where GrpContNo = '" + GrpContNo + "' and "
          + "uwrulecode in (select uwcode from lmuw where riskcode "
          + "in (select riskcode from lcgrppol where grpcontno = '"+GrpContNo+"') and passflag='R') "
          + "and exists (select 1 from LCGrpPol where GrpContNo = '"+GrpContNo+"' and GrpPolNo not in (select PolNo from LCReinsurTask)) ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    return confirm("保单还有险种符合再保审核条件，但没有发送给再保模块，继续？");
  }
  
  var sql2 = "select 1 from LCReinsurTask "
      + "where PolNo in(select GrpPolNo from LCPol where GrpContNo = '" + GrpContNo + "') "
      + " and (select GrpContNo from lcgrpcont where GrpContNo = '"+GrpContNo+"') ||'BJ' not in (select polno from LCReinsurTask) "
      + " and State != '02' ";
  var rs2 = easyExecSql(sql2);
  if(rs2)
  {
    return confirm("保单还有未办结的再保审核任务，继续？");
  }
  
  return true;
}



/*********************************************************************
 *  个单问题件录入(目前只是预留,功能已实现,只是未在用户界面上未显示录入按钮)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestInput()
{
  var cProposalNo = fmQuery.ProposalNo.value;

  if(cProposalNo == "")
  {
    alert("请先选择一条个人单记录！");
  }
  else
  {
    window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cProposalNo+"&ProposalNo="+cProposalNo+"&Flag="+cflag,"window1");
  }

}


/*********************************************************************
 *  团体问题件录入(团体下的问题件录入原则是:个人单问题件全部发到该团体单下主险团体单上,不发到相应的个单上(考虑到团体单自身业务特点而设计))
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestInput()
{
  var cGrpProposalContNo = fmQuery.GrpContNo.value;  //团体保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  //  if(cGrpProposalContNo != fmQuery.GrpMainProposalNo.value)
  //{
  //		alert("请先选择一个团体主险投保单!");
  //		return ;
  //  }
  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
 var Resource=fmQuery.Resource.value;
 if(Resource==1|Resource==2|Resource==3){
window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag=5","window2");
}else{
  window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag=1","window2");
}
	}


/*********************************************************************
 *  个单问题件查询(目前只是预留,功能已实现,只是未在用户界面上未显示查询按钮)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
  var cProposalNo = fmQuery.ProposalNo.value;//个单投保单号
  if(cProposalNo==""||cProposalNo==null)
  {
    alert("请先选择一个个人投保单!");
    return ;
  }
  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
}


/*********************************************************************
 *  团单问题查询(团体下的问题件发放原则是:个人单问题件全部发到该团体单下主险团体单上,不发到相应的个单上(考虑到团体单自身业务特点而设计))
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestQuery()
{
  var cGrpProposalContNo = fmQuery.GrpProposalContNo.value;  //团单投保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  if(cGrpProposalContNo!=fmQuery.GrpMainProposalNo.value)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./QuestQueryMain.jsp?ProposalNo1="+cGrpProposalContNo+"&Flag="+cflag,"window1");
}


/*********************************************************************
 *  对团体单发加费核保通知书
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
/*
function SendNotice()
{
 var cGrpProposalContNo = fmQuery.GrpProposalContNo.value;
 fmQuery.GUWState.value = "8";
 if (cGrpProposalContNo != "" && cGrpProposalContNo == fmQuery.GrpMainProposalNo.value)
 {
gmanuchk();
cancelchk();
 }
 else
 {
 	alert("请先选择团体单下的主险投保单!");
 	cancelchk();
 }
}
 
 
*/
/*********************************************************************
 *  对团体单发送问题件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SendQuest()
{
  var cGrpProposalContNo = fmQuery.GrpMainProposalNo.value;
  fmQuery.GUWState.value = "7";
  if (cGrpProposalContNo != "" && cGrpProposalContNo==fmQuery.GrpMainProposalNo.value)
  {
    gmanuchk();
    //cancelchk();
  }
  else
  {
    alert("请先选择团体单下的主险投保单!");
    cancelchk();
  }
}


/*********************************************************************
 *  点击取消按钮,初始化界面各隐藏变量
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelchk()
{
  fmQuery.all('GUWState').value = "";
  fmQuery.all('GUWIdea').value = "";
}


/*********************************************************************
 *  返回到自动核保查询主界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GoBack()
{
  location.href  = "./ManuUWAll.jsp?type=2";

}
function temp()
{
  alert("此功能尚缺！");
}
/*********************************************************************
 *  点击扫描件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ScanQuery2()
{
  var arrReturn = new Array();

  var prtNo=fmQuery.all("PrtNo").value;
  if(prtNo==""||prtNo==null)
  {
    alert("请先选择一个团体投保单!");
    return ;
  }
  var SQLDocID=" select DocID from es_doc_main where doccode='"+prtNo+"' " ;
  var cDocID=easyExecSql(SQLDocID);
//  alert(cDocID[0][0]);
  window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+prtNo+"&BussTpye=TB&SubTpye=TB02");
 }

function GrpContQuery(cGrpContNo)
{
	var tloadFlag=fmQuery.LoadFlag.value;
	var Resource=fmQuery.Resource.value;
	//alert(tloadFlag);
  window.open("./GroupContMain.jsp?GrpContNo="+ cGrpContNo+"&Resource="+Resource+"&LoadFlag="+tloadFlag,"GroupContQuery");
}
/*********************************************************************
 *  进入团体合同
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showGrpCont()
{
  var cGrpContNo=fmQuery.GrpContNo.value;
  var Resource=fmQuery.Resource.value;
  var sql=" select proposalgrpcontno from lcgrpcont where grpcontno='"+cGrpContNo+"' "
					+" union "
					+" select proposalgrpcontno from lbgrpcont where grpcontno='"+cGrpContNo+"'"  
					+" union "
					+" select proposalgrpcontno from lobgrpcont where grpcontno='"+cGrpContNo+"'" 
  ;
  var arr=easyExecSql(sql);
  cGrpContNo=arr[0][0];
  //alert(arr[0][0]);
   var strSql = "SELECT * from lcgrppol l where prtno='"+PrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
   	var arrResult = easyExecSql(strSql);
   	if (arrResult == null) {

  //var newWindow=window.open("../app/GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+cGrpContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  window.open("../app/GroupPolApproveInfo.jsp?ScanFlag=1&scantype=scan&prtNo="+PrtNo+"&scantype=scan&prtNo="+PrtNo+"&Resource="+Resource+"&LoadFlag=16&polNo="+cGrpContNo,"window1");
}else{
 window.open("../uliapp/GroupPolApproveInfo.jsp?ScanFlag=1&scantype=scan&prtNo="+PrtNo+"&scantype=scan&prtNo="+PrtNo+"&Resource="+Resource+"&LoadFlag=16&polNo="+cGrpContNo,"window1");
}
}

//团单既往询价信息
function showAskApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cGrpContNo=fmQuery.GrpContNo.value;
  cAppntNo = fmQuery.AppntNo.value;

  if (cGrpContNo != "")
  {

    window.open("../askapp/AskUWAppMain.jsp?GrpContNo1="+cGrpContNo+"&AppntNo1="+cAppntNo,"window2");
    showInfo.close();
  }

}

/*********************************************************************
 *  集体体检通知书
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkBody()
{
  cGrpContNo=fmQuery.GrpContNo.value;


  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  if (cGrpContNo != "" )
  {
    fmQuery.action = "./UWManuSendBodyCheckChk.jsp";

    fmQuery.submit();

  }
  else
  {
    showInfo.close();
    alert("请先选择保单!");
  }
}

/*********************************************************************
 *  契调通知书
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
/*
function RReport()
{
 cGrpContNo=fmQuery.GrpContNo.value;
 
  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  if (cGrpContNo != "" )
  {
  	fmQuery.action = "./UWManuSendRReportChk.jsp";
  	
    fmQuery.submit();
   
  }
  else
  {
 	   showInfo.close();
 	   alert("请先选择保单!");
     }
 	
}
*/





/*********************************************************************
 *  既往保障信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showHistoryCont()
{
  cGrpContNo=fmQuery.GrpContNo.value;
  window.open("../uw/GrpUWHistoryContMain.jsp?GrpContNo="+cGrpContNo,"window2");
}

/*********************************************************************
 *  既往信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHistoryContI()
{
  cGrpContNo=fmQuery.GrpContNo.value;
  cAppntNo  = fmQuery.AppntNo.value;
  window.open("../uw/GrpUWHistoryCont.jsp?pmGrpContNo="+cGrpContNo+"&pmAppntNo=" +cAppntNo+ "","window2");
}
/*********************************************************************
 *  承保计划变更
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showChangePlan()
{
  var cpolNo = fmQuery.GrpContNo.value;
  var prtNo=fmQuery.PrtNo.value;
  //var cType = "ChangePlan";
//alert(cpolNo);
//alert(prtNo);
var msql="select MissionID,SubMissionID from lwmission where activityid in ('0000002004') and missionprop1='"+cpolNo+"' ";
var arr=easyExecSql(msql);
//alert(arr[0][0]);
//alert(arr[0][1]);
cMissionID=arr[0][0];
cSubMissionID=arr[0][1];
 // window.open("../app/GroupPolApproveInfo.jsp?ScanFlag=1&scantype=scan&LoadFlag=23&polNo="+cpolNo, "","status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
//window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo+"&Type=1&BussNoType=12&BussType=TB&SubType=TB1002", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
   var strSql = "SELECT * from lcgrppol l where prtno='"+PrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
   	var arrResult = easyExecSql(strSql);
   	if (arrResult == null) {
 window.open("../app/GroupPolApproveInfo.jsp?ScanFlag=1&LoadFlag=23&polNo="+cpolNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&scantype=scan&prtNo="+prtNo,"");
}else{
window.open("../uliapp/GroupPolApproveInfo.jsp?ScanFlag=1&LoadFlag=23&polNo="+cpolNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&scantype=scan&prtNo="+prtNo,"");
}
}


/*********************************************************************
 *  保险计划变更结论录入显示
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showChangeResultView()
{
  fmQuery.ChangeIdea.value = "";
  //fm.PolNoHide.value = fm.ProposalNo.value;  //保单号码
  //divUWResult.style.display= "none";
  divChangeResult.style.display= "";
}


//显示保险计划变更结论
function showChangeResult()
{
  //fmQuery.uwstate.value = "b";
  //fmQuery.UWIdea.value = fm.ChangeIdea.value;
  var cContNo = fmQuery.GrpContNo.value;

  cChangeResult = fmQuery.ChangeIdea.value;

  if (cChangeResult == "")
  {
    alert("没有录入结论!");
  }
  else
  {
    var strSql = "select * from LCIssuepol where ContNo='" + cContNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
      if(!window.confirm( tInfo ))
      {
        HideChangeResult()
        return;
      }
    }

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fmQuery.action = "./GrpUWChangPlanChk.jsp";
    fmQuery.submit(); //提交
  }
  //divUWResult.style.display= "";
  //divChangeResult.style.display= "none";
  //fmQuery.uwstate.value = "";
  //fmQuery.UWIdea.value = "";
  //fmQuery.UWPopedomCode.value = "";
}


//隐藏保险计划变更结论
function HideChangeResult()
{
  //divUWResult.style.display= "";
  divChangeResult.style.display= "none";
  //fmQuery.uwstate.value = "";
  //fmQuery.UWIdea.value = "";
  //fmQuery.UWPopedomCode.value = "";
}


//发首期交费通知书
function SendFirstNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fmQuery.GrpContNo.value;

  cOtherNoType="01"; //其他号码类型
  cCode="57";        //单据类型

  if (cProposalNo != "")
  {
    showModalDialog("./GrpUWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择保单!");
  }
}



//发核保通知书
function SendNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fmQuery.GrpContNo.value;


  if (cProposalNo != "")
  {
    fmQuery.action = "./UWSendNoticeChk.jsp";
    fmQuery.submit();
  }
  else
  {
    showInfo.close();
    alert("请先选择保单!");
  }

}


//问题件录入
function QuestInput()
{
  cContNo = fmQuery.ContNo.value;  //保单号码

  var strSql = "select * from LCcUWMaster where ContNo='" + cContNo + "' and ChangePolFlag ='1'";
  //alert(strSql);
  var arrResult = easyExecSql(strSql);
  //alert(arrResult);
  if (arrResult != null)
  {
    var tInfo = "承保计划变更未回复,确认要录入新的问题件?";
    if(!window.confirm( tInfo ))
    {
      return;
    }
  }
  window.open("./QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+cflag,"window1");

}

/*********************************************************************
 *  初始化是否上报核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initSendTrace()
{

  var tSql = "";

  tSql = "select sendflag,uwflag,uwidea,downuwcode from lcuwsendtrace where 1=1 "
         + " and otherno = '"+GrpContNo+"'"
         + " and othernotype = '2'"
         + " and uwno in (select max(uwno) from lcuwsendtrace where otherno = '"+GrpContNo+"')"
         ;
  turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	var strSql = "select downuwcode from lcuwsendtrace where 1=1 "
				 + " and otherno = '"+GrpContNo+"'"
         + " and othernotype = '2'"
         + " and sendtype not in ('4','5') "
         + " and uwno in (select min(uwno) from lcuwsendtrace where otherno = '"+GrpContNo+"')"
         ;
	var arr = easyExecSql(strSql);
	if(!arr){
		arr=new Array();
		arr[0] = new Array();
		//alert(arr[0][0]);
		arr[0][0]= operator;

	}
			//alert(arr[0][0]);
			//alert(operator);
  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    fmQuery.all('SendFlag').value =arrSelected[0][0];
    fmQuery.all('SendUWFlag').value =arrSelected[0][1];
    fmQuery.all('SendUWIdea').value =arrSelected[0][2];

    DivLCSendTrance.style.display="none";
  }
  var strSql2= "select passflag,uwidea from lcgcuwmaster where 1=1 "
           + " and grpcontno = '"+GrpContNo+"'"
           ;
  var arr2= easyExecSql(strSql2);
  if(!arr2){
  	arr2=new Array();
		arr2[0] = new Array();
		arr2[0][0]= "";
  }
	//alert(arrSelected);
  if(fmQuery.all('SendFlag').value == '0' && (operator==arr[0][0]) || arr2[0][0]=="z")
  {
    divUWSave.style.display = "";
    divUWAgree.style.display = "none";
  }
    else if((fmQuery.all('SendFlag').value == '1'&& operator!=arr[0][0]) || (operator!=arr[0][0]))
  {
    tSql = "select passflag,uwidea from lcgcuwmaster where 1=1 "
           + " and grpcontno = '"+GrpContNo+"'"
           ;
    turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
    arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
    if(arrSelected[0][0]==6||fmQuery.all('SendUWFlag').value ==6)
    {
      divUWAgree.style.display = "none";
      divUWSave.style.display = "";
    }
    else
    {
      fmQuery.all('GUWState').value = arrSelected[0][0];
      fmQuery.all('GUWIdea').value = arrSelected[0][1];
      divUWAgree.style.display = "";
      divUWSave.style.display = "none";
      fmQuery.all('GUWState').disable=true;
      fmQuery.all('GUWState').ondblclick="";
      fmQuery.all('GUWState').onkeyup="";
      //fmQuery.all('GUWState').className="readonly";
      fmQuery.all('GUWState').readOnly=true;
      fmQuery.all('GUWIdea').value="";
    }
  }
  var strSql3 = "select sum(a) from (select 1 a from lcuwsendtrace where sendtype='2' and otherno='"+GrpContNo+"' union all select 1 a from lcuwsendtrace where sendflag='0' and otherno='"+GrpContNo+"') as x  ";
  //alert(easyExecSql(strSql3));
  var arr = easyExecSql(strSql3);
  //alert(arr);
  if(!arr){
  	arr=new Array();
		arr[0] = new Array();
  }
  if(arr[0][0]>=2){
  	arr[0][0]="";
  	var strsql = "select operator d from lcuwsendtrace where sendtype='2' and otherno='"+GrpContNo+"'";
  	var arr_2 = easyExecSql(strsql);
  	if(arr_2){
	  	for(var m = 0 ; m < arr_2.length ; m++ ){
	  		if(operator == arr_2[m][0]){
	  			arr[0][0] = 3;
	  		}
	  	}
	  }
  }
  fmQuery.all('GrpSendUpFlag').value = arr[0][0];
  
}
function getfee(parm1,parm2)
{
  var grppolno=fmQuery.all(parm1).all('GrpGrid7').value;
  var strSQL="select sum(StandPrem),sum(Prem) from lcpol where grppolno='"+grppolno+"'";
  turnPage.queryModal(strSQL,GrpPolFeeGrid);
  var a = GrpPolFeeGrid.getRowColData(0,1);
  var b = GrpPolFeeGrid.getRowColData(0,2);
  var c = GrpPolFeeGrid.getRowColData(0,3);
  var a = a/1
          //var a = Math.round(a);
          var a = ''+a;
  //var c = b/a;
  //var c = ''+c;
  if(a=="0")
  {
    c =0;
    //alert(c);
  }
  else
  {
    var c = b/a;
    var c = ''+c;
    c = c.substring(0,5);
  }
  //c = c.substring(0,5);
  GrpPolFeeGrid.setRowColData(0,1,a);
  GrpPolFeeGrid.setRowColData(0,3,c);
  var row = GrpGrid.getSelNo()-1
            //alert();
            fmQuery.GrpProposalContNo.value = GrpGrid.getRowColData(row,1);
  //alert(fmQuery.GrpProposalContNo.value);
}

//中介比例调证
function showAgentDiv()
{
  //var tdata = GrpGrid.getRowColData(0,2);
  var tdata = fmQuery.PrtNo.value;
  if(tdata!=null||tdata!="")
  {
    showPage(this,divAgent);
    fmQuery.PrtNoInput.value=tdata;
  }
  else
  {
    alert("没有可用的团体合同！");
    return;
  }
}
//中介比例保存
function AgentcySave()
{
    var tAgentRate = fmQuery.AgentcyRate.value;
    if(isNaN(tAgentRate) || tAgentRate > 1)
    {
        alert("中介手续费必须为数值且不能大于1。");
        return false;
    }
    
  //var tdata = GrpGrid.getRowColData(0,2);
  var tdata = fmQuery.PrtNo.value;
  fmQuery.PrtNoInput.value=tdata;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);
  fmQuery.action="./AgentcySave.jsp";
  fmQuery.submit();
}

function showMarkPrice()
{
  window.open("./MakePriceMain.jsp?GrpContNo="+fmQuery.GrpContNo.value+"&RiskFlag=UW");
}
//校验核保结论
function chkUWIdea()
{
  var tUWIdea = fmQuery.GUWState.value;
  if(tUWIdea=="6")
  {
    if(!confirm("此结论为待上级核保。\n保单将会被列入核保订正状态，等待上级核保。\n是否继续？"))
    {
      return false;
    }
  }
}
//发团体问题件通知书
function SendIssueNotice()
{
  var tGrpContNo = fmQuery.GrpContNo.value ;
  if(tGrpContNo == "")
  {
    alert("页面错误，未查询到团体合同信息！");
    return ;
  }
  var strSql = "select * from LCGrpIssuePol where GrpContNo='"+tGrpContNo+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    alert("页面错误，未查询到团体问题件信息！");
    return ;
  }
  else
  {
  }
  var strSql = "select 1 from loprtmanager where otherno='"
               +tGrpContNo+"' and OtherNoType = '01' and code='54' and StandbyFlag1 is null";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    alert("已发团体问题件通知书。\n补发功能待作！");
    return ;
  }

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //alert();
  fmQuery.action = "./GrpSendIssueSave.jsp?GrpContNo="+tGrpContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
  //alert(fmQuery.action);
  fmQuery.submit();
}

function showRReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");


  var cContNo = fmQuery.GrpContNo.value;


  var cPrtNo = fmQuery.PrtNo.value;
  var mOperate = fmQuery.all('Operator').value;
 var Resource=fmQuery.Resource.value;
 //alert(Resource);
 if(Resource==1|Resource==2|Resource==3){
  if (cContNo != "")
  {
//alert(12);
    window.open("./GrpUWRReportMain.jsp?ContNo1="+cContNo+"&Resource="+Resource+"&PrtNo="+cPrtNo,"window1");
    showInfo.close();
  	}
	}else{
  if (cContNo != "")
  {

    window.open("./GrpUWRReportMain.jsp?ContNo1="+cContNo+"&PrtNo="+cPrtNo,"window1");
    showInfo.close();
  	}	
	}
}

//发团体契调通知书
function SendRReport()
{
  var tGrpContNo = fmQuery.GrpContNo.value ;
  if(tGrpContNo == "")
  {
    alert("页面错误，未查询到团体合同信息！");
    return ;
  }
  var strSql = "select 1 from LCGrpRReportItem where GrpContNo='"+tGrpContNo+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    alert("页面错误，未查询到团体契调信息！");
    return ;
  }
  var strSql = "select 1 from loprtmanager where otherno='"
               +tGrpContNo+"' and OtherNoType = '01' and code='74'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    alert("已发团体契调通知书。\n补发功能待作！");
    return ;
  }

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fmQuery.action = "./GrpSendRReportSave.jsp?GrpContNo="+tGrpContNo;
  //alert(fmQuery.action);
  fmQuery.submit();
}


//记事本
function showNotePade()
{
    
    window.open("../uw/UWGroupNotepadMain.jsp?GrpContNo="+GrpContNo+"&PrtNo="+PrtNo, "window1");
}

//发送再保审核
function ReInsure() {
  cGrpContNo = fmQuery.GrpContNo.value;  //保单号码
  var cMissionID = fmQuery.MissionID.value;         
  var cSubMissionID = fmQuery.SubMissionID.value;   
  var cPrtNo = fmQuery.PrtNo.value;                 
  window.open("./GrpReInsureInputMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+cflag+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");

}


//显示团体投保资料
function showGrpAppntInfo()
{
	var tSql = "select 1 from lcgrpcont where ContPrintType = '3' and prtno='" + fmQuery.PrtNo.value + "'";
    var arrResult = easyExecSql(tSql);
    if(arrResult){
    	fmQuery.PerAutoUWInfo.style.display = "none";
    	fmQuery.SendReUW.style.display = "none";
    	fmQuery.SendCheckBody.style.display = "none";
    	fmQuery.ChangePlan.style.display = "none";
    }
	var tloadFlag=fmQuery.LoadFlag.value;
	var tResource=fmQuery.Resource.value;
	//alert(GrpContNo);
	if(tloadFlag==1&tResource==2){
  var strSql = " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" case when (select 1 from lcgrpcontroad where prtno=a.prtno)=1 then (select avage from lcgrpcontroad where prtno=a.prtno) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lcpol where grpcontno=a.grpcontno and poltypeflag='0') end, "
               +" (select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lcgrpcont a where proposalgrpcontno='"+GrpContNo+"' and  a.uwflag in ('1','a') "
               +" union "
               +" select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lobpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lobpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lobinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lobpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lobpol where grpcontno=a.grpcontno and poltypeflag='0'), "
               +" (select count(1) from lobinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lobgrpcont a where proposalgrpcontno='"+GrpContNo+"' "
               ;
               //alert(1);
              }else if(tloadFlag==1&tResource==1){
  var strSql =  " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lbpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lbpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lbinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lbpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" case when (select 1 from lcgrpcontroad where prtno=a.prtno)=1 then (select avage from lcgrpcontroad where prtno=a.prtno) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lbpol where grpcontno=a.grpcontno and poltypeflag='0') end, "
               +" (select count(1) from lbinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lbgrpcont a where proposalgrpcontno='"+GrpContNo+"' " 
               ;
                //alert(2);           
              }else if(tloadFlag==1&tResource==3){
  var strSql =  " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" case when (select 1 from lcgrpcontroad where prtno=a.prtno)=1 then (select avage from lcgrpcontroad where prtno=a.prtno) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lcpol where grpcontno=a.grpcontno and poltypeflag='0') end, "
               +" (select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lcgrpcont a where proposalgrpcontno='"+GrpContNo+"' " 
               ;
               //alert(3);               
              }else{
   var strSql =  " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" case when (select 1 from lcgrpcontroad where prtno=a.prtno)=1 then (select avage from lcgrpcontroad where prtno=a.prtno) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lcpol where grpcontno=a.grpcontno and poltypeflag='0') end, "
//               +" (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lcpol where grpcontno=a.grpcontno and poltypeflag='0'), "
               +" (select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lcgrpcont a where proposalgrpcontno='"+GrpContNo+"' " 
               ;
               //alert(4);             
              }
  turnPage2.queryModal(strSql,GrpAppntGrid);
  //填充被保险人退休比例，被保险人平均年龄
  //var strSql = "select count(1) from lcinsured where GrpContNo='"+GrpContNo+"'";
  //var arr = easyExecSql(strSql);
  //var insuredCount = 0;
  //if(arr){
  //	insuredCount = arr[0][0];
  //}
  //var strSql = "select count(1) from lcinsured where GrpContNo='"+GrpContNo+"' and InsuredStat='2'";
  //arr = easyExecSql(strSql);
  //var offWorkPepoles=0;
  //if(arr){
  //	offWorkPepoles = arr[0][0];
  //}
  //var rate = 0;
  //if(offWorkPepoles>0){
  //	rate =
  //}
  //alert(GrpAppntGrid.mulLineCount);
  if(GrpAppntGrid.mulLineCount==0)
  {
    var strSql="select a.GrpName,a.GrpNature,a.BusinessBigType,a.BusinessType,a.peoples2 "
               +" from lcgrpcont a where grpcontno='"+GrpContNo+"'";
    turnPage2.queryModal(strSql,GrpAppntGrid);
  }
  //L 保障计划
  //M 人员类别
  //A 参保人数
  //B 计划中应保人数
  //C1 男
  //C2 女
  //D1 在职
  //D2 退休
  //E 平均年龄
  //F 投保险种代码
  //G 投保险种名称
  //H 标准保费
  //I 录入保费
  //I/A 人均保费
  //K 折扣比例
  	if(tloadFlag==1&tResource==2){
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' when aa is not null then aa else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +", case when Z.RiskCode in ('163001','561301','163002','561302') then  (select avage from lcgrpcontroad where prtno in (select prtno from lcgrpcont where GrpContNo=Y.grpcontno )) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and GrpContNo=Z.ProposalGrpContNo)  end  E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) H"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='0') H2"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) I"
               +",(select distinct 1 from LCDuty m where m.standprem = 0 and PolNo in (select PolNo from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode)) k"
               +",(select TotalFactor from lcgrpcontroad where prtno in (select prtno from lcgrpcont where grpcontno=Y.grpcontno)) aa "
               +" from LCContPlanRisk Z,LCContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X "
               +" union "
               +" select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' when aa is not null then aa  else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +", case when Z.RiskCode in ('163001','561301','163002','561302') then  (select avage from lcgrpcontroad where prtno in (select prtno from lcgrpcont where GrpContNo=Y.grpcontno )) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and GrpContNo=Z.ProposalGrpContNo)  end  E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) H"
               +",(select sum(StandPrem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='0') H2"
               +",(select sum(StandPrem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) I"
               +",(select distinct 1 from LobDuty m where m.standprem = 0 and PolNo in (select PolNo from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode)) k"
               +",(select TotalFactor from lcgrpcontroad where prtno in (select prtno from lcgrpcont where grpcontno=Y.grpcontno)) aa "
               +" from LobContPlanRisk Z,LobContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur "
               ;
              }else if(tloadFlag==1&tResource==1){
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' when aa is not null then aa else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +", case when Z.RiskCode in ('163001','561301','163002','561302') then  (select avage from lcgrpcontroad where prtno in (select prtno from lcgrpcont where GrpContNo=Y.grpcontno )) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and GrpContNo=Z.ProposalGrpContNo)  end  E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) H"
               +",(select sum(StandPrem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='0') H2"
               +",(select sum(StandPrem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) I"
               +",(select distinct 1 from LbDuty m where m.standprem = 0 and PolNo in (select PolNo from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode)) k"
               +",(select TotalFactor from lcgrpcontroad where prtno in (select prtno from lcgrpcont where grpcontno=Y.grpcontno)) aa "
               +" from LbContPlanRisk Z,LbContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur"
               ;              
              }else if(tloadFlag==1&tResource==3){
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' when aa is not null then aa  else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +", case when Z.RiskCode in ('163001','561301','163002','561302') then  (select avage from lcgrpcontroad where prtno in (select prtno from lcgrpcont where GrpContNo=Y.grpcontno )) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and GrpContNo=Z.ProposalGrpContNo)  end  E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) H"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='0') H2"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) I"
               +",(select distinct 1 from LCDuty m where m.standprem = 0 and PolNo in (select PolNo from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode)) k"
               +",(select TotalFactor from lcgrpcontroad where prtno in (select prtno from lcgrpcont where grpcontno=Y.grpcontno)) aa "
               +" from LCContPlanRisk Z,LCContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur "
               ;              
              }else{
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' when aa is not null then aa  else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +", case when Z.RiskCode in ('163001','561301','163002','561302') then  (select avage from lcgrpcontroad where prtno in (select prtno from lcgrpcont where GrpContNo=Y.grpcontno )) else (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and GrpContNo=Z.ProposalGrpContNo)  end  E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) H"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='0') H2"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode) I"
               +",(select distinct 1 from LCDuty m where m.standprem = 0 and PolNo in (select PolNo from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and ContPlanCode=Z.ContPlanCode)) k"
               +",(select TotalFactor from lcgrpcontroad where prtno in (select prtno from lcgrpcont where grpcontno=Y.grpcontno)) aa "
               +" from LCContPlanRisk Z,LCContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur "
               ;                            
              }
  turnPage2.queryModal(strSql,GrpGrid,0,0,1);
  
}

function showFeeRate()
{
	
	
  var strSql = "select a.RiskCode,b.RiskName,case when a.RiskCode in ('163001','561301','163002','561302') then (select TotalFactor from lcgrpcontroad where prtno=a.prtno) else div(a.prem,(select sum(standprem) from lcpol c where a.RiskCode=c.RiskCode and c.GrpContNo in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"'))) end,a.ComFeeRate,a.BranchFeeRate from LCGrpPol a,LMRisk b"+
               " where a.RiskCode=b.RiskCode and a.GrpContNo in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"') order by a.riskcode";
  turnPage3.queryModal(strSql,FeeRateGrid); 
  //如果该险种有一责任的标准保费为0,则折扣比例显示为空,并且总公司,分公司费用率分别为'N/A'
  var grppolSql = "select grppolno,standbyflag3,riskcode from lcgrppol where grpcontno in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"') order by riskcode";
  var arr1 = easyExecSql(grppolSql); 
  if(arr1)
  {
  	  for(var i = 0;i<arr1.length; i++)
  	  {   	  	
  	  	  if(arr1[i][1] == null || arr1[i][1] == "")
  	  	  {  	  	  	
							 var dutySql = "select sum(standprem),sum(prem) from lcduty where polno in (select polno from lcpol where grppolno='"+arr1[i][0]+"') and trim(dutycode) in (select dutycode from lmriskduty where riskcode='"+arr1[i][2]+"')";
							 var arr = easyExecSql(dutySql);
							 if(arr) 
							 {						
								    for(var a=0;a<arr.length;a++) 
								    {   							    	
								    	  if(arr[a][0] == "0")
								     	  { 				     	  
								     	  	   FeeRateGrid.setRowColData(i,3,'N/A') ;								     	  	   
								     	  	   FeeRateGrid.setRowColData(i,5,'N/A') ;
								     	  }
								     	  else
								     	  	{
                            var b=arr[a][1]/arr[a][0];
								     	      var b = ''+b;		
								     	      if(arr1[i][2]=="163001"||arr1[i][2]=="561301"||arr1[i][2]=="163002"||arr1[i][2]=="561302"){
								     	    	  var tsqll="select TotalFactor from lcgrpcontroad where prtno in (select prtno from lcgrpcont where grpcontno='"+GrpContNo+"')";
								     	    	  var arrr = easyExecSql(tsqll);
								     	    	  if(arrr){
								     	    		  b=arrr[0][0];
								     	    	  }
								     	    	 FeeRateGrid.setRowColData(i,3,b);
								     	      }else{
									     	  	   FeeRateGrid.setRowColData(i,3,b.substring(0,4)); 								     	      
								     	      }
								     	      
								     	  		}
								    }
						   }   
				  }   
				  else
				  {		
				  	var tSql = " select a.riskcode,b.riskname,a.standbyflag3,a.ComFeeRate,a.BranchFeeRate from lcgrppol a,lmrisk b where a.grpcontno = '"+GrpContNo+"'"
				  	         + " and a.riskcode = b.riskcode";			  	        
				  	turnPage3.queryModal(tSql,FeeRateGrid);          
         }				  
			}
	  }
}
function FeeRateSave()
{
  fmQuery.GrpContNo.value=GrpContNo;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);
  fmQuery.action="./FeeRateSave.jsp";
  fmQuery.submit();
}
//初始化核保既望核保信息
function initQueryUWidea(){
	var strSql="SELECT A.OPERATOR,B.USERNAME,'',CHAR(A.MAKEDATE)||' '||CHAR(A.MAKETIME),(SELECT CODENAME FROM LDCODE WHERE CODE=A.SENDTYPE AND CODETYPE='sendtype'),CODENAME,UWIDEA FROM LCUWSENDTRACE A,LDUSER B,LDCODE WHERE CODETYPE='uwflag' AND CODE=A.uwflag AND A.uwflag NOT IN ('0','5','z') AND A.OPERATOR=B.USERCODE AND otherno='"+GrpContNo+"' and sendtype not in ('4','5') order by uwno";
	var arr = easyExecSql(strSql);
	if(arr){
		displayMultiline(arr,HisUWInfoGrid);
	}
}	

function checkStandPrem(){
	if(GrpGrid.mulLineCount > 0)
  {
    for (var i = 0 ;i < GrpGrid.mulLineCount; i++)
    {
      var tStandPrem = GrpGrid.getRowColData(i,15);
      //alert(tStandPrem);
      if(tStandPrem == "1")
      {
        GrpGrid.setRowColData(i,11,'N/A') ;
        GrpGrid.setRowColData(i,14,'N/A') ;
      }
    }
  }
}

function initQuerySendReason()
{
	var strSql="select operator,reason from lcuwsendreason where otherno = '"+GrpContNo+"' order by uwno";
	
	var arr = easyExecSql(strSql);
	if(arr){
		displayMultiline(arr,UpSendReasonGrid);
	}
}
function initQuerySendTrace()
{
  var strSql2= " select passflag,uwidea from lcgcuwmaster where 1=1 and ProposalGrpContNo = '"+GrpContNo+"' " 
							+" union "                                                                                   
							+" select passflag,uwidea from lbgcuwmaster where 1=1 and ProposalGrpContNo = '"+GrpContNo+"' " 
							+" union "                                                                                   
							+" select passflag,uwidea from lobgcuwmaster where 1=1 and ProposalGrpContNo = '"+GrpContNo+"' "
           ;
  var arr2= easyExecSql(strSql2);
  if(arr2){
  	//alert(arr2[0][0]);
  	//alert(arr2[0][1]);
			fmQuery.all('GUWState').value = arr2[0][0];
      fmQuery.all('GUWIdea').value = arr2[0][1];
	}
}

/**
 * 查询中介手续费录入比例
 */
function queryAgentChargeRate()
{
    var tPrtNo = fmQuery.PrtNo.value;
    
    var tStrSql = " select varchar(double(Rate)) "
        + " from LARateCharge "
        + " where 1 = 1 "
        + " and OtherNo = '" + tPrtNo + "' "
        + " and OtherNoType = '0' "
        + " order by MakeDate desc, MakeTime desc "
        + " fetch first rows only "
        ;
        
    var tRate = easyExecSql(tStrSql);
    if(tRate)
    {
        fmQuery.AgentcyRate.value = tRate;
        showAgentDiv();
    }
}

function showPayPlanInfo()
{
    var tProposalGrpContNo = GrpContNo;
    
    var tStrUrl = "../app/GrpPayPlanMain.jsp?&Flag=sys&ProposalGrpContNo=" + tProposalGrpContNo;
    var newWindow = window.open(tStrUrl);
}
//by gzh 20140507 结余返还
function initBalance(){
	var tPrtNo = fmQuery.PrtNo.value;
	if(tPrtNo == null || tPrtNo == "" || tPrtNo == "null"){
		alert("获取印刷号码失败！");
		return false;
	}
	showInfo = window.open("../app/BalanceMain.jsp?PrtNo="+tPrtNo+"&LookFlag=1");
}

function HJAppnt(){
	var tContPrintType = "";
	var tContPrintTypeSQL = "select ContPrintType,ManageCom from lcgrpcont where grpcontno = '"+GrpContNo+"' ";
	var tContPrintTypeArr = easyExecSql(tContPrintTypeSQL);
	if(!tContPrintTypeArr){
		alert("获取保单数据失败！");
		return false;
	}else{
		tContPrintType = tContPrintTypeArr[0][0];
	}
	if(tContPrintType!="" && tContPrintType!="5"){
		alert("非汇交件不能查看投保人信息！");
		return;
	}else{
		showInfo = window.open("../app/HJAppntMain.jsp?GrpContNo="+GrpContNo+"&flag=view");
	}
	
}

//校验黑名单
function checkBlackName(){
	//黑名单校验投保人
	var sql1 = "select name from lcgrpappnt where prtno='"+fmQuery.PrtNo.value+"' ";
	var arrResult = easyExecSql(sql1);
	if(arrResult){
		var sqlblack = "select 1 from lcblacklist where name='"+arrResult[0][0]+"'";
		var arrResult1 = easyExecSql(sqlblack);
		if(arrResult1){
			if(!confirm("该保单投保人："+arrResult[0][0]+"，存在于黑名单中，确认要核保通过吗？")){
				return false;
			}
		}
	}
	
	//黑名单校验被保人
	var sql2 = "select name,idtype,idno from lcinsured where prtno='"+fmQuery.PrtNo.value+"'  ";
	var arrRes = easyExecSql(sql2);
	var tInsuNames = "";
	if(arrRes){
		for(var i = 0; i < arrRes.length; i++){
			var sqlblack2  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrRes[i][0]+"' and idnotype='"+arrRes[i][1]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrRes[i][0]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrRes[i][0]+"' and type='0' ";
			var arrRes1 = easyExecSql(sqlblack2);
			if(arrRes1){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , "+arrRes[i][0];
				}else{
					tInsuNames = tInsuNames + arrRes[i][0];
				}
			}
		}
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人："+tInsuNames+",存在于黑名单中，确认要核保通过吗？")){
			return false;
		}
	}
	return true;
}

//270102 保单层保险期间与保障计划中保险期间相同
function check270102(){
	var tSqlRisk = "select 1 from LCGrpPol where PrtNo='" + fmQuery.PrtNo.value + "' and riskcode='270102' ";
	var arr = easyExecSql(tSqlRisk);
	if(arr){
		var strSQL = "select CValiDate,CInValiDate from lcgrpcont where prtno = '"+fmQuery.PrtNo.value+"' ";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null){
			alert("校验缴费频次与保险区间时，获取数据失败！");
			return false;
		}
		var CValiDates = arrResult[0][0].split("-");
		var CInValiDates = arrResult[0][1].split("-");
//		var YearDif = parseInt(CInValiDates[0],10)-parseInt(CValiDates[0],10);// 年差
//		var MonthDif = YearDif*12 + parseInt(CInValiDates[1],10)-parseInt(CValiDates[1],10);// 月差
		var interval = 0;

	    var sYears = CValiDates[0];
	    var sMonths = CValiDates[1];
	    if(sMonths.length == 1) sMonths = "0" + sMonths;
	    var sDays = CValiDates[2];
	    if(sDays.length == 1) sDays = "0" + sDays; 

	    var eYears = CInValiDates[0];
	    var eMonths = CInValiDates[1];
	    if(eMonths.length == 1) eMonths = "0" + eMonths;
	    var eDays = CInValiDates[2];
	    if(eDays.length == 1) eDays = "0" + eDays;

	    interval = eYears - sYears;
	    if (eMonths > sMonths){
	    	interval++;
		}else{
			if (eMonths == sMonths && eDays > sDays){
				interval++;
				if (eMonths == 1){ //如果同是2月，校验润年问题
					if ((sYears % 4) == 0 && (eYears % 4) != 0){ //如果起始年是润年，终止年不是润年
						if (eDays == 28){ //如果终止年不是润年，且2月的最后一天28日，那么减一
							interval--;
						}
					}
				}
			}
		}
	    
	    var strSQL2 = "select contplancode,calfactorvalue from lccontplandutyparam where contplancode <> '11' and calfactor ='StandByFlag1' and grpcontno='"+fmQuery.all( 'GrpContNo' ).value+"'";
	    var arrResult2 = easyExecSql(strSQL2);
	    if(!arrResult2){
	    	alert("获取保单数据失败！");
	    	return false;
	    }
	    for(var i=0;i<arrResult2.length;i++){
	    	var strSQL3 = "select calfactorvalue from lccontplandutyparam where calfactor='StandByFlag' and grpcontno='"+fmQuery.all( 'GrpContNo' ).value+"' and contplancode='"+arrResult2[i][0]+"'";
	    	var arrResult3 = easyExecSql(strSQL3);
	    	if(!arrResult3){
	    		alert("获取保单数据失败！");
	    		return false;
	    	}
	    	if(arrResult2[i][1] == interval && arrResult3[0][0] == "Y"){
	    	}else{
	    		alert("保险单录入保险期间与保障计划中保险期间不一致，请核实!");
	    		return false;
	    	}
	    }
	}
	
    return true;
    
}