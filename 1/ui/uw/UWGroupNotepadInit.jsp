<%
//程序名称：团体记事本
//程序功能：记录保单纪录各个环节的备注信息
//创建日期：2006-9-21 18:42
//创建人  ：Wulg
//更新人  ：  
%>
<!--用户校验类-->

<script language="JavaScript">
function initInpBox() { 
  try {  
  }
  catch(ex) {
    alert("在UWGroupNotepadInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!"+ex.message);
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initUWGroupNotepadGrid();  
    queryClick();   
  }
  catch(re) {
    alert("UWGroupNotepadInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}
//领取项信息列表的初始化
var UWGroupNotepadGrid;
function initUWGroupNotepadGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();                    
		iArray[0][0]="序号";        //列名    
		iArray[0][1]="30px";       //列名    
		iArray[0][3]=0;         		//列名 0 只读 1 可写 2 代码格式 3 隐藏       
		
		iArray[1]=new Array();                    
		iArray[1][0]="印刷号";      //列名    
		iArray[1][1]="40px";       //列名    
		iArray[1][3]=0;         		//列名             
		
		iArray[2]=new Array();                    
		iArray[2][0]="记事本内容";  //列名    
		iArray[2][1]="200px";       //列名    
		iArray[2][3]=0;         		//列名        
		
    iArray[3]=new Array();                    
		iArray[3][0]="录入员";      //列名    
		iArray[3][1]="40px";       //列名    
		iArray[3][3]=0;         		//列名 
		
		iArray[4]=new Array();                    
		iArray[4][0]="录入日期";    //列名    
		iArray[4][1]="40px";       //列名    
		iArray[4][3]=0;         		//列名
		
		iArray[5]=new Array();                    
		iArray[5][0]="录入时间";    //列名    
		iArray[5][1]="40px";       //列名    
		iArray[5][3]=0;         		//列名
		 
    UWGroupNotepadGrid = new MulLineEnter( "fm" , "UWGroupNotepadGrid" ); 
    //这些属性必须在loadMulLine前           
    
    UWGroupNotepadGrid.canSel = 1;
	  UWGroupNotepadGrid.hiddenPlus = 1;
  	UWGroupNotepadGrid.hiddenSubtraction = 1;
    UWGroupNotepadGrid.mulLineCount = 0;   
    UWGroupNotepadGrid.displayTitle = 1;
    UWGroupNotepadGrid.canChk = 0;
    UWGroupNotepadGrid.selBoxEventFuncName = "showOne";
    UWGroupNotepadGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDClassInfoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
