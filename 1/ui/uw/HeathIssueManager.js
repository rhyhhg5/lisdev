//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;
  var tPrtSeq;
  var tCodetType;
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null ){
     alert( "请先选择一条记录，再点击返回按钮。" );
  }else{
	
	tPrtSeq = PolGrid.getRowColData(tSel-1,3);
	tCodetType = PolGrid.getRowColData(tSel-1,1);
		
  }

    var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
	fmSave.PrtSeq.value = tPrtSeq;
	fmSave.CodeType.value = tCodetType;
	fmSave.submit();

}



function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  easyQueryClick();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
  // 初始化表格
    initPolGrid();
    if(!verifyInput2())
        return false;
        
    if(fm.CodeType.value==""){
        alert("请录入单证类型！");
        return false;
    }    
    if(fm.PrtNo.value == "")
    {
        if(fm.startMakeDate.value == ""&&fm.endMakeDate.value == ""){
             alert("印刷号和下发起止日期至少录入一个！");
             return false;
        }
        if(fm.startMakeDate.value == "" || fm.endMakeDate.value == ""){
             alert("请录入下发起止期！");
             return false;
        }else{
             if(!checkDate())
             {
                return false;
             }
        }
     
    }
    if(fm.PrtNo.value != "" ){
       fm.startMakeDate.value = "";
       fm.endMakeDate.value = "";
    }
        

    var tSeltype="";//是否回销
    var tStateFlag="";//是否已打
    var strSQL = ""; // 书写SQL语句
    
    if(fm.CodeType.value == "0"){
        if (fm.all('SelType').value=="0")
        {
            tSeltype="and a.PrtSeq not in (select doccode from es_doc_main where subtype='TB22' )";
        }
        else if(fm.all('SelType').value=="1")
        {
            tSeltype="and a.PrtSeq in (select doccode from es_doc_main where subtype='TB22' )";
        }
        if(fm.all('StateFlag').value=="0")
        {
            tStateFlag=" and a.stateflag='0' ";
        }
        else if(fm.all('StateFlag').value=="1" )
        {
            tStateFlag=" and a.stateflag<>'0' ";
        }
        
    
    strSQL = "SELECT a.Code code,'问题件' type,a.PrtSeq, b.PrtNo, b.PolApplyDate, b.CValiDate, b.AppntName,c.QuestionObj, "
        + "b.Agentcode, " 
//        + "(select Name from LAAgent aa where aa.AgentCode = b.AgentCode) AgentName, "
        + "a.MakeDate, "
        + "(select MakeDate from es_doc_main where DocCode = a.PrtSeq and SubType = 'TB22') TakeBackDate, "
        + "(case when a.StateFlag='0' then '未打' else '已打' end) StateFlag, "
        + "b.ManageCom "
        + "FROM LOPrtManager a, LCCont b, LCIssuePol c "
        + "WHERE a.OtherNo = b.ContNo and a.Code = '85' "
        + "and c.PrtSeq = a.PrtSeq and c.ContNo = b.ContNo "
        + "and c.BackObjType = '3' and c.NeedPrint = 'Y' "
        + "and b.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart('b.AgentGroup', 'agentGroup')
        + getWherePart('b.PrtNo', 'PrtNo')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('a.MakeDate', 'startMakeDate', '>=')
        + getWherePart('a.MakeDate', 'endMakeDate', '<=')
        + tSeltype
        + tStateFlag;
        if(fm.AgentCode.value != "" ){
        	strSQL+=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
        }
        strSQL+= " order by b.prtno,a.MakeDate"
        ;
    }else if(fm.CodeType.value == "1"){
        if (fm.all('SelType').value=="0")
        {
            tSeltype="and a.PrtSeq not in (select doccode from es_doc_main where subtype='TB15' )";
        }
        else if(fm.all('SelType').value=="1")
        {
            tSeltype="and a.PrtSeq in (select doccode from es_doc_main where subtype='TB15' )";
        }
        if(fm.all('StateFlag').value=="0")
        {
            tStateFlag=" and a.stateflag='0' ";
        }
        else if(fm.all('StateFlag').value=="1" )
        {
            tStateFlag=" and a.stateflag<>'0' ";
        }
        
        strSQL = "SELECT a.Code code,'体检件' type,a.PrtSeq, b.PrtNo, b.PolApplyDate, b.CValiDate, b.AppntName,c.name QuestionObj, "
        + "b.Agentcode, " 
//        + "(select Name from LAAgent aa where aa.AgentCode = b.AgentCode) AgentName, "
        + "a.MakeDate, "
        + "(select MakeDate from es_doc_main where DocCode = a.PrtSeq and SubType = 'TB15') TakeBackDate, "
        + "(case when a.StateFlag='0' then '未打' else '已打' end) StateFlag, "
        + "b.ManageCom "
        + "FROM LOPrtManager a, LCCont b,LCPENotice c "
        + "WHERE a.OtherNo = b.ContNo and a.Code = '03' "
        + "and c.PrtSeq = a.PrtSeq and c.ContNo = b.ContNo "
        + "and b.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart('b.AgentGroup', 'agentGroup')
        + getWherePart('b.PrtNo', 'PrtNo')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('a.MakeDate', 'startMakeDate', '>=')
        + getWherePart('a.MakeDate', 'endMakeDate', '<=')
        + tSeltype
        + tStateFlag;
        if(fm.AgentCode.value != "" ){
        	strSQL+=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
        }
        strSQL+= " order by b.prtno,a.MakeDate"
        ;
        
    }else {
        if (fm.all('SelType').value=="0")
        {
            tSeltype="and a.PrtSeq not in (select doccode from es_doc_main where subtype='TB16' )";
        }
        else if(fm.all('SelType').value=="1")
        {
            tSeltype="and a.PrtSeq in (select doccode from es_doc_main where subtype='TB16' )";
        }
        if(fm.all('StateFlag').value=="0")
        {
            tStateFlag=" and a.stateflag='0' ";
        }
        else if(fm.all('StateFlag').value=="1" )
        {
            tStateFlag=" and a.stateflag<>'0' ";
        }
        
        strSQL = "SELECT a.Code code,'契调件' type,a.PrtSeq, b.PrtNo, b.PolApplyDate, b.CValiDate, b.AppntName,c.name QuestionObj, "
        + "b.Agentcode, " 
//        + "(select Name from LAAgent aa where aa.AgentCode = b.AgentCode) AgentName, "
        + "a.MakeDate, "
        + "(select MakeDate from es_doc_main where DocCode = a.PrtSeq and SubType = 'TB16') TakeBackDate, "
        + "(case when a.StateFlag='0' then '未打' else '已打' end) StateFlag, "
        + "b.ManageCom "
        + "FROM LOPrtManager a, LCCont b, LCRReport c "
        + "WHERE a.OtherNo = b.ContNo and a.Code = '04' "
        + "and c.PrtSeq = a.PrtSeq and c.ContNo = b.ContNo "
        + "and b.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart('b.AgentGroup', 'agentGroup')
        + getWherePart('b.PrtNo', 'PrtNo')
        + getWherePart('b.Agentcode', 'AgentCode')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('a.MakeDate', 'startMakeDate', '>=')
        + getWherePart('a.MakeDate', 'endMakeDate', '<=')
        + tSeltype
        + tStateFlag;
        if(fm.AgentCode.value != "" ){
        	strSQL+=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
        }
        strSQL+= " order by b.prtno,a.MakeDate"
        ;
    
    }

       
    fm.querySql.value = strSQL;

    //2014-10-31  杨阳   
    //业务员代码显示表LAAgent中GroupAgentCode
    strSQL = "select distinct code,type, PrtSeq, PrtNo, PolApplyDate, CValiDate, AppntName,QuestionObj, " 
    		+ "( select getUniteCode(X.agentcode) from dual), "
	        + "(select Name from LAAgent  where X.AgentCode = AgentCode)," 
	        + "MakeDate, TakeBackDate, StateFlag,  "
	        + "ManageCom from (" + strSQL + ") as X" ;
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
        alert("没有要删除的单证！");
        return false;
    }
    
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
    
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    
    //保存SQL语句
    turnPage.strQuerySql     = strSQL;
    
    //设置查询起始位置
    turnPage.pageIndex       = 0;
    
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //tArr = chooseArray(arrDataSet,[0,1,3,4])
    //调用MULTILINE对象显示查询结果
    
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    //displayMultiline(tArr, turnPage.pageDisplayGrid);
}


function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}



//下载投保单信息清单	2007-10-30
function printList()
{
	if(fm.querySql.value != null && fm.querySql.value != "" && PolGrid.mulLineCount > 0)
	{
		fm.action = "FillIssuePrint.jsp";
		fm.target = "_blank";
		fm.submit();
	}
	else
	{
		alert("请先执行查询操作，再打印！");
		return ;
	}
}


function checkDate()
{
  var sql = "select 1 from Dual where date('" + fm.endMakeDate.value + "') > date('" + fm.startMakeDate.value + "') + 3 month ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    alert("统计日期：下发起止时间大于3个月！");
    return false;
  }
  
  return true;
}