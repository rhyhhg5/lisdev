<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ContUW.jsp
//程序功能：合同单人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="ContUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ContUWQuery.jsp" method=post name=fmGrpQuery target="fraSubmit">
    <!-- 合同查询条件 -->
    <TR  class= common>
   <!--   <TD  class= titles>
        合同投保单号码
      </TD>
      <TD  class= input>-->
        <Input class= common name=ContNo  type="hidden">
    </TR>
       <INPUT VALUE="查询集体投保单" TYPE=button onclick="queryGrp();"> 
    <!-- 集体投保单查询结果部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
    		</td>
    		<td class= titleImg>
    			 集体投保单查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCGrp1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
       <INPUT VALUE="查询个人投保单" TYPE=button onclick="queryPol();"> 
  </form>
  <form action="./ContUWQuery.jsp" method=post name=fmPolQuery target="fraSubmit">
    <!-- 集体投保单查询结果部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 个人投保单查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
  </form>
  <form action="./ContUWSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 核保 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>核保信息：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      <!--    <TD  class= title>
            投保单号码
          </TD>
          <TD  class= input>-->
            <Input class="readonly" readonly name=ProposalNo  type="hidden">
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskVersion >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ManageCom >
          </TD>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntNo >
          </TD>
          <TD  class= title>
            投保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            被保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredNo >
          </TD>
          <TD  class= title>
            被保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredName >
          </TD>
          <TD  class= title>
            被保人性别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredSex >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            份数
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Mult >
          </TD>
          <TD  class= title>
            保费
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Prem >
          </TD>
          <TD  class= title>
            保额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Amnt >
          </TD>
        </TR>
	</table>
	<table class= common border=0 width=100% >
          <INPUT VALUE="保单明细信息" TYPE=button onclick="showPolDetail();"> 
          <INPUT VALUE="既往投保信息" TYPE=button onclick="showApp();"> 
          <INPUT VALUE="以往核保记录" TYPE=button onclick="showUWSub();">
          <INPUT VALUE="体检资料" TYPE=button > 
          <INPUT VALUE="发体检通知" TYPE=button > 
    </table>
    <!-- 核保结论 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>核保结论：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            核保结论
          </TD>
          <TD  class= input>
            <Input class="code" name=UWState ondblclick="return showCodeList('UWState',[this]);">
          </TD>
          <TD  class= title>
            核保意见
          </TD>
          <TD  class= input>
            <Input class= common name=UWIdea >
          </TD>
	</table>
          <INPUT VALUE="确定" TYPE=button > 
          <INPUT VALUE="取消" TYPE=button > 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
