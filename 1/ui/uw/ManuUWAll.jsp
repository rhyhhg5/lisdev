<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<%
//程序名称：ManuUWAll.jsp.
//程序功能：个单人工核保
//创建日期：2005-1-28 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tFlag = "";
	tFlag = request.getParameter("type");
%>
<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	String currentDate = PubFun.getCurrentDate();
%>
<script>
	var operFlag = "<%=tFlag%>";		//区分团险和个险的标志
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ManuUWAll.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ManuUWAllInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form method=post name=fm target="fraSubmit" action="./ManuUWAllChk.jsp"> 
  <Div  id= "queryPage" style= "display: ''" style="float: ">
  	<table class=common>
  		<tr class=common>
  			<TD  class= title>
          印刷号
        </TD>
        <TD  class= input_Acc>
          <Input class=common8 name="PrtNo" >
        </TD>
        <TD  class= title>
          投保单位
        </TD>
        <TD  class= input>
          <Input class=common8 name="AppntName" verify="投保人|len<=20">
        </TD>
        <TD  class= title>
          生效日期
        </TD>
        <TD  class= input>
          <Input class=coolDatePicker name="CValiDate" verify="申请日期|date">
        </TD>
              </tr>
          		<tr class=common>
        <TD  class= title>
          管理机构
        </TD>      
        <TD  class= input>
          <Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
        </TD>
	      	<TD  class= title8>
            业务员代码
          </TD>
          <TD  class= input8>
      			<Input class=codeNo name=AgentCode verify="代理人编码|code:AgentCodet" ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
         </TD>
        <TD  class= title>
          中介机构
        </TD>
        <TD  class= input>
          <Input class=common8 name="AgentComName" verify="中介机构|len<=100">
        </TD>  		
  		
  		</tr>
  		<TD  class= title >
        </TD>
        <TD  class= input>
          <Input class=common8 type=hidden name="ApplyDate" verify="申请日期|date">
        </TD>


  		</tr>
        <tr>
            <td class= title>核保日期从</td>
            <td class= input><Input class=coolDatePicker3 name="startUWDate" readonly verify="核保日期|date"></td>
            <td class= title>至</td>
            <td class= input><Input class=coolDatePicker3 name="endUWDate" value="<%=currentDate%>" readonly verify="核保日期|date"></td>
  		</tr>
        <tr>
            <td class= title>总保费从</td>
            <td class= input><Input class=common name="minPrem" verify="总保费|int"></td>
            <td class= title>至</td>
            <td class= input><Input class=common name="maxPrem" verify="总保费|int"></td>
        </tr>
  	</table>
  </div>
  <Div  id= "divApplyButton" style= "display: ''" style="float: "> 	
      <table  class= common>
  		<TR>
  			<td class="titleImg">核保申请
  			</td>
  		</TR>
  		</table>
  		<table class=common>
  		<TR>
          <TD  class= "title">
           保单渠道
          </TD>
          <TD  class= "input">
           <Input class="codeNo" value="" readonly=true name=ContSaleChnlType verify="保单渠道|notnull&code:grpcontsalechnluw" ondblclick="return showCodeList('grpcontsalechnluw',[this,ContSaleChnlTypeName],[0,1],null,null,null,1);" ><input class=codename name=ContSaleChnlTypeName  readonly=true  elementtype="nacessary">
          </TD>
          <TD>  			
  		  </TD>
          <TD>
          </TD>
          <TD>
          </TD> 
          <TD>
          </TD> 				
  		</TR>
  		<TR>
          <TD  class= title>
            申请件数
          </TD>
          <TD  class= input>
            <Input class=common name="ApplyNo" verify="申请件数|int&len<=4">
          </TD>
          <TD>  			
          <INPUT class=cssButton id="riskbutton" VALUE="申  请" TYPE=button onClick="ApplyUW();">
          (每次申请最多为10件)
  				</TD>
          <TD  class= title>
            待申请件数
          </TD>
          <TD  class= input>
            <Input CLASS="readonly" readonly name="UWNo" >
          </TD>  				
  		</TR>
      </table>
  </DIV>
   <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 待处理信箱：
    		</td>
    		 <td class=common>    		
    		 	<INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
    		</td>
    	</tr>  	
    </table>
    </Div>
    <Div  id= "divLCPol11" style= "display: ''" align = left>
     <table>
       <tr class=common>
      	  <td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  </td>
  			</tr>  
    	</table>    
    </div>
    <Div  id= "divLCPol2" style= "display: ''" align = center>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">   
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">    
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    </div>
   <DIV id=DivLCContInfo1 STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 待回复信箱：
    		</td>
    	</tr>  	
    </table>
    </Div>
    <Div  id= "divLCPol11" style= "display: ''" align = left>
     <table>
       <tr class=common>
      	  <td text-align: left colSpan=1 >
  					<span id="spanRePolGrid" >
  					</span> 
  			  </td>
  			</tr>  
    	</table>    
    </div>
    <Div  id= "divLCPol22" style= "display: ''" align = center>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();">   
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();">    
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">    
    </div>
       <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 已完成信箱：
    		</td>
    		 <td class=common>    		
                <input type=hidden name="historySql">
    		 	<INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="HistoryeasyQueryClick();">
    		 	<INPUT class=cssButton id="riskbutton" VALUE="清单下载" TYPE=button onClick="downloadHistoryList();">
    		</td>
    	</tr>  	
    </table>
    </Div>
    <Div  id= "divLCPol11" style= "display: ''" align = left>
     <table>
       <tr class=common>
      	  <td text-align: left colSpan=1 >
  					<span id="spanHistoryPolGrid" >
  					</span> 
  			  </td>
  			</tr>  
    	</table>    
    </div>
    <Div  id= "divLCPol12" style= "display: ''" align = center>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();">   
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();">    
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();">    
    </div>
         <span id="spanCode"  style="display: none; position:absolute; slategray" onclick="return ;	if(spanCode) showCodeList('bank',[ManageCom],null,null,null,null,1); spanCode=false;" onkeyup="return showCodeListKey('bank',[this],null,null,null,null,1);"></span>
    	 <Input type=hidden name="ApplyType" >
</body>
</html>
