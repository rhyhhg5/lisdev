<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QuestionAdditionalRiskChk.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	    //输出参数
	    CErrors tError = null;
	    String FlagStr = "Fail";
	    String Content = "";

		GlobalInput tG = new GlobalInput();  
		tG=(GlobalInput)session.getValue("GI");
	
		String tProposalNo = request.getParameter("ProposalNo");
		String tContent = request.getParameter("Content");
	
	
	  	//补充附加险表			    				
		LCAddPolSchema tLCAddPolSchema = new LCAddPolSchema();		
		tLCAddPolSchema.setRemark(tContent);
		tLCAddPolSchema.setMainPolNo(tProposalNo);	
			
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCAddPolSchema);
		tVData.add( tG );
		
		// 数据传输
		AddAdditionalRiskUI tAddAdditionalRiskUI   = new AddAdditionalRiskUI();
		if (tAddAdditionalRiskUI.submitData(tVData,"INSERT") == false)
		{
			Content = " 保存失败，原因是: " + tAddAdditionalRiskUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
        else     
		{                          
		    Content = " 保存成功! ";
		    FlagStr = "Succ";
		 }

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
