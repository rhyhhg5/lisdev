<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWManuHealth.jsp
//程序功能：承保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
  	<script>
		var peaddflag = "<%=request.getParameter("peaddflag")%>";
		var ProposalContNo = "<%=request.getParameter("ProposalContNo")%>";
		var hospitcode = "<%=request.getParameter("hospitcode")%>";
		var PrtSeq = "<%=request.getParameter("PrtSeq")%>";
    var Pestateflag = "<%=request.getParameter("Pestateflag")%>";
		</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./PEAdd.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEAddInit.jsp"%>
  <title> 承保体检资料录入 </title>
  
</head>
<body  onload="initForm('<%=tContNo%>','<%=tMissionID%>','<%=tSubMissionID%>','<%=tPrtNo%>');" >
  <form method=post name=fm target="fraSubmit" action= "./UWManuHealthChk.jsp">
    <!-- 非列表 -->
    <table>
    	<TR  class= common>
          <!--<TD  class= title>  合同号码  </TD>-->
          <TD  class= input> <Input class="readonly" type= "hidden" name=ContNo > </TD>
           <INPUT  type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
           <INPUT  type= "hidden" class= Common name= SubMissionID value= "">
           <INPUT  type= "hidden" class= Common name= PrtNo value= "">
           <INPUT  type= "hidden" class= Common name= PrtSeq value= "">
          <TD  class= title>  体检人  </TD>
          <TD  class= input> <Input class=codeNo name=InsureNo <!--ondblClick="showCodeListEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" --><input class=codename name=InsureNoName readonly=true ></TD>
        </TR>
        
    </table>

      
    <table>
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg>	 体检项目</td>                            
    	</tr>	
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanHealthGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    	</div>
    	<!--<Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
  </Div>-->
    	 <Div  id= "divUWDis" style= "display: 'none'">
    <table>
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg>   疾病结果</td>                            
    	</tr>	
    </table>
    <Div  id= "divUWDis" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanDisDesbGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
      </div>
      
        </div>
     <Div  id= "other" style= "display: 'none'">
    	<table class=common>
         <TR  class= common> 
           <TD  class= common> 其他体检信息 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Note" cols="120" rows="3" class="common" >
             </textarea>
           </TD>
         </TR>
      </table>
      </div>
	<input value="项目变更保存" class=cssButton type=button onclick="saveDisDesb();" >
	<td><INPUT class=cssbutton VALUE="返  回" type=button onclick="returnParent();"> 	</td>


    <!--读取信息-->
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>