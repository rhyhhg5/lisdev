<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DelayUrgeNoticeInit.jsp
//程序功能：延发催办通知书
//创建日期：2003-07-16 11:10:36
//创建人  ：SXY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
                      


                         

function initForm()
{
  try
  {
     initDelayUrgeNoticeInputGrid();
     easyQueryClick();
  }
  catch(ex)
  {
    alert("DelayUrgeNoticeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initDelayUrgeNoticeInputGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷流水号";         		//列名
      iArray[1][1]="160px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="号码";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="号码类型";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10] = "NoType";
      iArray[3][11] = "0|^00|个单保单号^01|集体保单号^02|合同号^03|批单号^04|实付收据号^05|保单印刷号";
      iArray[3][12] = "3";
      iArray[3][19] = "0";
  
      iArray[4]=new Array();
      iArray[4][0]="通知书类型";         		//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][10] = "NoticeType";
      iArray[4][11] = "0|^03|体检通知书^04|面见通知书^05|核保通知书^07|首期交费通知书";
      iArray[4][12] = "4";
      iArray[4][19] = "0";

      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="120px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="通知书生成日期";         		//列名
      iArray[6][1]="120px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="预计催办日期";         		//列名
      iArray[7][1]="120px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      DelayUrgeNoticeInputGrid = new MulLineEnter( "fm" , "DelayUrgeNoticeInputGrid" ); 
      //这些属性必须在loadMulLine前
      DelayUrgeNoticeInputGrid.mulLineCount = 3;   
      DelayUrgeNoticeInputGrid.displayTitle = 1;
      DelayUrgeNoticeInputGrid.locked = 1;
      DelayUrgeNoticeInputGrid.canSel = 0;
      DelayUrgeNoticeInputGrid.canChk = 1;
      DelayUrgeNoticeInputGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      
      }
      catch(ex)
      {
        alert("DelayUrgeNoticeInit.jsp-->initDelayUrgeNoticeInputGrid函数中发生异常:初始化界面错误!");
        alert(ex);
      }
}



</script>