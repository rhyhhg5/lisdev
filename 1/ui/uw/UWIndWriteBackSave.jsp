<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolInput.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//输出参数
	CErrors tError = null;         
	String FlagStr="";
	String Content = "";
	VData tVData = new VData();
	
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	String tAction = request.getParameter("fmAction");

  System.out.println("动作是:"+tAction);
  
  LCPolSchema tLCPolSchema = new LCPolSchema();
  tLCPolSchema.setContNo(request.getParameter("ContNo"));
  tLCPolSchema.setPolNo(request.getParameter("PolNo"));
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("CustomerReply",request.getParameter("CustomerIdea"));
  tTransferData.setNameAndValue("UWIdea",request.getParameter("UWIdea"));
  tVData.add(tLCPolSchema);
  tVData.add(tTransferData);
  tVData.add(tG);
  UWIndCustomerReplayUI tUWIndCustomerReplayUI = new UWIndCustomerReplayUI();
  
  try{
  	tUWIndCustomerReplayUI.submitData(tVData,tAction);
  }catch(Exception ex){
  	ex.printStackTrace();
  	FlagStr = "Fail";
  	Content = "发生异常错误原因是："+ex.getMessage();
  }
  
  tError = tUWIndCustomerReplayUI.mErrors;
  if(tError.needDealError()){
  	FlagStr = "Fail";
  	Content = "保存失败，原因是："+tError.getFirstError();
  }else{
  	FlagStr = "Succ";
  	Content = "保存成功！";
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>