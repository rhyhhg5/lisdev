<html>
<%
//Name:GrpUWRReport.jsp
//Function：立案界面的初始化程序
//Date：2005-08-1 14:12
//Author ：cuiwei
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>     
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GrpUWRReport.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpUWRReportInit.jsp"%>
  <script>

	var GrpContNo = "<%=request.getParameter("ContNo2")%>";
  var Resource="<%=request.getParameter("Resource")%>";
</script>
</head>
<body  onload="initForm('<%=tContNo%>','<%=tPrtNo%>');">
  <form action="./GrpUWRReportChk.jsp" method=post name=fm target="fraSubmit">
   <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,AppAffixList);">
    	</td>
    	<td class= titleImg>
    	 团体契约调查清单
    	</td>
    	
    </tr>
      </table>
      	<Div  id= "divUWSpec" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanInvestigateGrid" >
					</span>
				</td>
			</tr>	
		</table>
		<tr>		
			<TD  class= title>
            截止时间
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker"  dateFormat="short" name=EndDate verify="截止时间|date&notnull" >
          </TD>
          <TD  class= title>  客户访问专员  </TD>
          <TD  class= input> 
          	
          	<Input class="codeNo" name=Commissioner  verify="客户访问专员|notnull" ondblclick="return showCodeList('commissioner',[this,ComName],[0,1]);" onkeyup="return showCodeListKey('commissioner',[this,ComName],[0,1]);"><input class=codename name=ComName readonly=true >
          	  </TD>
		</tr>
		 <span  id= "GrpQuest11" style= "display: ''">
		<input class=cssButton name=confirm type=button value=" 保 存 " onclick="AffixSave()">
		<input class=cssButton type=hidden type=button value=" 修 改 " onclick="AffixUpdate()">
	</span>
		<input class=cssButton  type=button value=" 返 回 " onclick="top.close()">
		<INPUT VALUE = "发团单契调信息通知书" Class="cssButton" TYPE=hidden onclick="SendRReport();">
		
	</div>
	<input type=hidden id="fmtransact" name="fmtransact">
	
	<INPUT type= "hidden" name= "ProposalNoHide" value= "">
     
      <INPUT type= "hidden" name= "MissionID" value= "">
      <INPUT type= "hidden" name= "SubMissionID" value= "">
      <INPUT type= "hidden" name= "SubNoticeMissionID" value= "">
      <INPUT  type= "hidden" name= "Resource" value= "">
      <INPUT type= "hidden" name= "Flag"  value = "">
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>


</body>
</html>