//程序名称：GrpUWInsuCont.js
//程序功能：团体既往理陪函数
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

/*********************************************************************
 *  该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var cGrpContNo;
var PrtNo;

/*********************************************************************
 *  查询承保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQuery()
{
	// 初始化表格
	initEnsureGrid();

    var tStrSql = ""
        + " select lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, lgc.Peoples2, lgc.Prem, "
        + " CodeName('uwflag', lgc.UWFlag), "
        + " CodeName('appflag', lgc.AppFlag), "
        + " lgc.PolApplyDate "
        + " from LCGrpCont lgc "
        + " where 1 = 1 "
        + " and lgc.AppntNo = '" + mAppntNo + "' "
        + " and lgc.AppFlag not in ('1') "
        + " order by lgc.PrtNo, lgc.GrpContNo "
        + " with ur "
        ;

    turnPage1.pageDivName = "divEnsureGridPage";
    turnPage1.queryModal(tStrSql, EnsureGrid);

    if (!turnPage1.strQueryResult)
    {
        alert("没有既往投保信息！");
        return false;
    }
    
    return true;
}

/*********************************************************************
 *  取的客户团体的名称
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryI()
{
	// 书写SQL语句
	var strSQL = "select GrpName";
	var arrRs;
	strSQL    += "  from LDGrp";
	strSQL    += " where CustomerNo='"+mAppntNo+"'";
	
	fm.all("GrpNo").value=mAppntNo;
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	
	//查询成功设置画面上的数据
	if(turnPage.strQueryResult)
	{
	  arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
	  fm.all("GrpName").value=arrRs[0][0];
	}
	
	strSQL  = "select ProposalGrpContNo";
	strSQL += "  from LCGrpCont";
	strSQL += " where GrpContNo = '" + mGrpContNo + "'";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	
	//查询成功设置画面上的数据
	if(turnPage.strQueryResult)
	{
	  arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
	  fm.all("GrpPolNo").value=arrRs[0][0];
	}
}

/**
 * 展示既往投保明细信息。
 */
function queryEnsureDetail()
{
    var tRow = EnsureGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = EnsureGrid.getRowData(tRow);
    
    var tGrpContNo = tRowDatas[0];
    
    var tStrSql = ""
        + " select tmp.ContPlanCode, tmp.ContPlanName, tmp.RiskCode, tmp.RiskName, tmp.PeoplesCount, "
        + " Div(tmp.GentlemanCount, (tmp.GentlemanCount + tmp.LadyCount)), "
        + " Div(tmp.LadyCount, (tmp.GentlemanCount + tmp.LadyCount)), "
        + " Div(tmp.OffWorkPeoplesCount, tmp.ContCount), "
        + " tmp.AvgInsuredAppAge, tmp.StandPrem, tmp.Prem, "
        + " Div(tmp.Prem, tmp.PeoplesCount), "
        + " (case when Div(tmp.Prem, tmp.StandPrem) = '0' then 'N/A' else Div(tmp.Prem, tmp.StandPrem) end) "
        + " from ( "
        + " select lccp.ContPlanCode, lccp.ContPlanName, lccpr.RiskCode, "
        + " (select lmr.RiskName from LMRisk lmr where lmr.RiskCode = lccpr.RiskCode) RiskName, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode) PeoplesCount, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.Sex = '0') GentlemanCount, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.Sex = '1') LadyCount, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.InsuredStat = '2') OffWorkPeoplesCount, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.RelationToMainInsured = '00') ContCount, "
        + " (select varchar(Round(nvl(avg(double(lcp.InsuredAppAge)), 0), 2)) InsuredAppAge from LCPol lcp where lcp.GrpContNo = lccp.GrpContNo and lcp.RiskCode = lccpr.RiskCode and lcp.ContPlanCode = lccp.ContPlanCode) AvgInsuredAppAge, "
        + " (select nvl(sum(lcp.StandPrem), 0) from LCPol lcp where lcp.GrpContNo = lccp.GrpContNo and lcp.RiskCode = lccpr.RiskCode and lcp.ContPlanCode = lccp.ContPlanCode) StandPrem, "
        + " (select nvl(sum(lcp.Prem), 0) from LCPol lcp where lcp.GrpContNo = lccp.GrpContNo and lcp.RiskCode = lccpr.RiskCode and lcp.ContPlanCode = lccp.ContPlanCode) Prem "
        + " From LCContPlan lccp "
        + " inner join LCContPlanRisk lccpr on lccp.GrpContNo = lccpr.GrpContNo and lccp.ContPlanCode = lccpr.ContPlanCode "
        + " where 1 = 1 "
        + " and lccp.GrpContNo = '" + tGrpContNo + "' "
        + " and lccp.ContPlanCode not in ('11') "
        + " ) as tmp "
        + " order by tmp.ContPlanCode, tmp.RiskCode "
        + " with ur "
        ;
    
    turnPage2.pageDivName = "divEnsureDetailGridPage";
    turnPage2.queryModal(tStrSql, EnsureDetailGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有既往投保明细信息！");
        return false;
    }
    
    return true;	
}


function grpRiskPlanInfo()
{
	if(cGrpContNo&&PrtNo)
	{
	var newWindow = window.open("../app/ContPlan.jsp?prtNo="+PrtNo+"&GrpContNo="+cGrpContNo+"&LoadFlag=16");
	}else{
		alert("请先选择一条投保单!");
	}	
}


function showAskApp()
{
  window.open("../uw/GrpUWAskCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showRejiggerApp()
{
  window.open("../uw/GrpUWRejiggerCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往理陪信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsuApp()
{
  window.open("../uw/GrpUWInsuCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保障信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGrpUWHistoryCont()
{
  window.open("../uw/GrpUWHistoryCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

function showEnsureCont()
{
  window.open("../uw/GrpUWEnsureCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}


/**
 * 显示团体保单信息页面。
 */
function showGrpCont()
{
    var tRow = EnsureGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = EnsureGrid.getRowData(tRow);
    
    var tGrpContNo = tRowDatas[0];
    var tPrtNo = tRowDatas[3];

    window.open("../app/GroupPolApproveInfo.jsp?prtNo=" + tPrtNo + "&LoadFlag=16&polNo=" + tGrpContNo, "window1");
    
    return true;
}

