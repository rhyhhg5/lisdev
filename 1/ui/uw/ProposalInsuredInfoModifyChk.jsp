<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalAppntInfoModifyChk.jsp
//程序功能：新契约被保险人信息修改
//创建日期：2002-06-19 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
   <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
 String FlagStr = "Fail";
 String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
	TransferData tTransferData = new TransferData();
	String tContNo             = request.getParameter("ContNo");
	String tInsuredNo          = request.getParameter("InsuredNo");
	String tInsuredName        = request.getParameter("InsuredName"); 
	String tInsuredIDType      = request.getParameter("InsuredIDType");
	String tInsuredIDNo        = request.getParameter("InsuredIDNo");
  String tAppntNo            = request.getParameter("AppntNo");
	
	boolean flag = true;
	if (!tContNo.equals(""))
	{
		//准备公共传输信息
		tTransferData.setNameAndValue("ContNo",tContNo);		
		tTransferData.setNameAndValue("AppntNo",tAppntNo);
		tTransferData.setNameAndValue("InsuredNo",tInsuredNo);
		tTransferData.setNameAndValue("CustomerNo",tInsuredNo);
		tTransferData.setNameAndValue("Name",tInsuredName);	
		tTransferData.setNameAndValue("IDType",tInsuredIDType);	
		tTransferData.setNameAndValue("IDNo",tInsuredIDNo) ;		
		tTransferData.setNameAndValue("stateFlag","0");
	
	}
	else
	{
		flag = false;
		Content = "数据不完整!";
	}	
	System.out.println("flag:"+flag);
try
{           
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData);
		tVData.add( tG );
		
		// 数据传输
		EasyEdorTbUI tEasyEdorTbUI   = new EasyEdorTbUI();	
		
		if (!tEasyEdorTbUI.submitData(tVData,BQ.EASYEDORTYPE_INSURED))
		{
		//	int n = tEasyEdorTbUI.getErrorCount();
			Content = " 新契约修改投保人失败，原因是: " + tEasyEdorTbUI.getErrors().getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tEasyEdorTbUI.getErrors();
		    if (!tError.needDealError())
		    {                          
		    	Content = " 新契约修改被保险人成功,请确认是否重打合同! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 新契约修改被保险人失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		     }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
