<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：RReportReplyChk.jsp
//程序功能：续保人工核保生存调查报告回复
//创建日期：2002-12-23 11:10:36
//创建人  ：heyq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
 	LCRReportSchema tLCRReportSchema = new LCRReportSchema();
	TransferData tTransferData = new TransferData();
	String tContNo = request.getParameter("ContNo");
	String tPrtNo = request.getParameter("PrtNo");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tReplyResult = request.getParameter("ReplyResult");
	String tPrtSeq = request.getParameter("PrtSeq");	
	
	System.out.println("Contno:"+tContNo);
	System.out.println("tReplyResult:"+tReplyResult);
	System.out.println("tPrtNo:"+tPrtNo);
	System.out.println("MissionID:"+tMissionID);
	System.out.println("subMissionID:"+tSubMissionID);
	boolean flag = true;
	if (!tContNo.equals("")&& !tSubMissionID.equals("")&& !tMissionID.equals("")&&!tPrtSeq.equals(""))
	{
		tLCRReportSchema.setContNo(tContNo);
		tLCRReportSchema.setPrtSeq(tPrtSeq);
		tLCRReportSchema.setReplyContente(tReplyResult);
		
		//准备公共传输信息
		tTransferData.setNameAndValue("ContNo",tContNo);
		tTransferData.setNameAndValue("PrtNo",tPrtNo) ;
		tTransferData.setNameAndValue("MissionID",tMissionID);	
		tTransferData.setNameAndValue("PrtSeq",tPrtSeq);	
		tTransferData.setNameAndValue("SubMissionID",tSubMissionID);	
		tTransferData.setNameAndValue("LCRReportSchema",tLCRReportSchema);	
	}
	else
	{
		flag = false;
		Content = "数据不完整!";
	}	
	System.out.println("flag:"+flag);
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData);
		tVData.add( tG );
		
		// 数据传输
		TbWorkFlowUI tTbWorkFlowUI   = new TbWorkFlowUI();
		if (!tTbWorkFlowUI.submitData(tVData,"0000001113"))//执行保全核保生调工作流节点0000000004
		{
			int n = tTbWorkFlowUI.mErrors.getErrorCount();
			Content = " 续保生调通知书回复失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tTbWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 续保人工核保生调回复成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 续保人工核保生调回复失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		     }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
