 <%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TempFeeSave.jsp
//程序功能：
//创建日期：2002-07-22 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="CalBonus.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%> 

<%
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
	 
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI");  
  String action=request.getParameter("action");
  String accountYear=request.getParameter("AccountYear");
  if(accountYear==null||accountYear.equals(""))
  {
  		FlagStr="Fail";
  		Content="会计年度必须输入！";
  }
  else
  {
  
		  CalBonus tCalBonus = new CalBonus();
		  tCalBonus.setGlobalInput(tGI);
		  if(action.equals("1"))
		  {
		  	if(tCalBonus.verifyBonusStatus(accountYear)==false)
		  	{
		  		FlagStr="Fail";
		  		Content="红利校验失败！原因："+tCalBonus.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		FlagStr="Succ";
		  		Content="校验完成";		  		
		  	}
		  }
		  if(action.equals("2"))
		  {
		  	if(tCalBonus.realCalBonus(accountYear)==false)
		  	{
		  		FlagStr="Fail";
		  		Content="红利计算失败！原因："+tCalBonus.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		FlagStr="Succ";
		  		Content="红利计算完成";
		  	}
		  }
		  if(action.equals("3"))
		  {
		  	AssignBonus tAssignBonus = new AssignBonus();
		  	tAssignBonus.setGlobalInput(tGI);
		  	if(tAssignBonus.runAssignBonus()==false)
		  	{
		  		FlagStr="Fail";
		  		Content="红利分配失败！原因："+tAssignBonus.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		FlagStr="Succ";
		  		Content="红利分配完成";
		  	}
		  }		
  }

%>                      
<html>
<script language="javascript">
       if("<%=FlagStr%>"=="Succ")//操作成功
       {
           parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");          
       }
       else//操作失败
       {
        	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
       }
</script>
</html>

