<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewManuUWAddFeeApply.jsp
//程序功能：保全人工核保投保单申请校验
//创建日期：2003-04-09 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.workflow.bq.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}  
	
  //校验处理     
    LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
	String tEdorNoHide = request.getParameter("EdorNoHide");
	String tPolNoHide = request.getParameter("PolNoHide");
	String tPrtNoHide = request.getParameter("PrtNoHide");
	String tEdorTypeHide = request.getParameter("EdorTypeHide");
	
	System.out.println("EdorNoHide:"+tEdorNoHide);
	
	boolean flag = false;
	
	if (!tEdorNoHide.equals("")&&!tPolNoHide.equals("")&&!tEdorTypeHide.equals(""))
	{	
 	    tLPEdorMainSchema.setPolNo( tPolNoHide);
 	    tLPEdorMainSchema.setEdorNo( tEdorNoHide);
	    tLPEdorMainSchema.setEdorType( tEdorTypeHide);	    
	    flag = true;
	}
	else
	{
	    FlagStr = "Fail";
	    Content = "号码传输失败，申请加费失败，!";
	}
	
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLPEdorMainSchema );		
		tVData.add( tG );
		
		// 数据传输
		PEdorUWManuAddFeeApplyChkUI tPEdorUWManuAddFeeApplyChkUI   = new PEdorUWManuAddFeeApplyChkUI();
		if (tPEdorUWManuAddFeeApplyChkUI.submitData(tVData,"INSERT") == false)
		{
			Content = " 保全人工核保申请失败，原因是: " + tPEdorUWManuAddFeeApplyChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    
		    tError = tPEdorUWManuAddFeeApplyChkUI.mErrors;
		    if (!tError.needDealError())
		    {                     
		    	Content = " 保全人工核保申请申请成功!";
		    	FlagStr = "Succ";
		    }
		    else                                                              
		    {
		    	FlagStr = "Fail";
		    }
		}
	}
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>       
<html>
<script language="javascript">
	parent.fraInterface.afterAddFeeApply("<%=FlagStr%>","<%=Content%>");	
</script>
</html>
