<%@include file="../common/jsp/UsrCheck.jsp"%>
                <%
                //程序名称：GrpUWRReportChk.jsp
                //程序功能：
                //创建日期：2005-08-2
                //创建人  ：cuiwei
                //更新记录：  更新人    更新日期     更新原因/内容
                %>
                <!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
System.out.println("<-Star SavePage->");
LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
LCGrpContSet tLCGrpContSet = new LCGrpContSet();
LCGrpRReportItemSet tLCGrpRReportItemSet= new LCGrpRReportItemSet();
LCGrpRReportSchema tLCGrpRReportSchema= new LCGrpRReportSchema();


//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
if(tG == null) {
  out.println("session has expired");
  return;
}
transact = request.getParameter("fmtransact");
System.out.println(transact);

String tGrpContNo = request.getParameter("GrpContNo");
String tcode[] = request.getParameterValues("InvestigateGrid1");
String tcodename[] = request.getParameterValues("InvestigateGrid2");
String tRReportReason[] = request.getParameterValues("InvestigateGrid3");
String tEndDate = request.getParameter("EndDate");
String tCommissioner = request.getParameter("Commissioner");
   String tChk[] = request.getParameterValues("InpInvestigateGridChk");
boolean flag = true;
int ChkCount = 0;
if(tChk != null) {
  ChkCount = tChk.length;
}
System.out.println("count:"+ChkCount);
if (ChkCount == 0 ) {
  Content = "契调资料信息录入不完整!";
  FlagStr = "Fail";
  flag = false;
  System.out.println("111");
} else {
  tLCGrpContSchema.setGrpContNo(tGrpContNo);
  tLCGrpContSchema.setProposalGrpContNo(tGrpContNo);
  tLCGrpContSet.add(tLCGrpContSchema);
  if (ChkCount > 0) 
  {

    for(int index=0;index<tChk.length;index++) 
    {
      System.out.println("-----------------------4");
      System.out.println(" tChk[index]:   "+tChk[index]);
          if(tChk[index].equals("1"))
          { System.out.println("-----------------------5");
          LCGrpRReportItemSchema tLCGrpRReportItemSchema = new LCGrpRReportItemSchema();
          tLCGrpRReportItemSchema.setRReportItemCode(tcode[index]);
          tLCGrpRReportItemSchema.setRReportItemName(tcodename[index]);
          tLCGrpRReportItemSchema.setRemark(tRReportReason[index]);
          tLCGrpRReportItemSchema.setEndDate(tEndDate);
           tLCGrpRReportSchema.setGrpContNo(tGrpContNo);
           tLCGrpRReportSchema.setCommissioner(tCommissioner);
          System.out.println("该行被选中");
           tLCGrpRReportItemSet.add(tLCGrpRReportItemSchema);
          }
          
          
          if(tChk[index].equals("0")){
           System.out.println("该行未被选中");}
        
       
      
    }
  } 
  else {
    Content = "传输数据失败!";
    flag = false;
  }

}
if (flag == true) {
  // 准备传输数据 VData
  VData tVData = new VData();

  System.out.println("-----------------------");
  tVData.add(tLCGrpContSet);
  tVData.add(tLCGrpRReportItemSet);

  System.out.println("-----------------------2");
  tVData.add(tLCGrpRReportSchema);                     
  tVData.add(tG);
  System.out.println("-----------------------3");


  LCGrpRReportItemUI tLCGrpRReportItemUI = new LCGrpRReportItemUI();
  if (tLCGrpRReportItemUI.submitData(tVData,"INSERT||MAIN") == false) {
    int n = tLCGrpRReportItemUI.mErrors.getErrorCount();
    for (int i = 0; i < n; i++)
      Content = " 自动核保失败，原因是: " + tLCGrpRReportItemUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail") {
    tError = tLCGrpRReportItemUI.mErrors;
    if (!tError.needDealError()) {
      Content = " 人工核保成功! ";
      FlagStr = "Succ";
    } else {
      Content = " 人工核保失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

}
%>
<html>
<script language="javascript">
                 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
