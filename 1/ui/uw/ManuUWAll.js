//程序名称：ManuUWAll.js
//程序功能：个人人工核保
//创建日期：2005-01-24 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnConfirmPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约人工核保的EasyQuery
 *  描述:查询显示对象是合同保单.显示条件:合同未进行人工核保，或状态为待核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 书写SQL语句	
	var strSQL ="";
	var tSql = "";
	var arrSelected = new Array();
	var strSQL1 ="";
	k++;
	initGrpPolGrid();
	initGrpRePolGrid();
	//个单--人工核保,已经单独成立页面
	if(operFlag == "1")
	{
		 strSQL = "select t.missionprop1,t.missionprop2,t.missionprop7,"
		 						+"case t.activitystatus "
		 						+"when '1' then '未人工核保'  "
		 						+"when '3' then '核保已回复' "
		 						+"when '2' then '核保未回复' end,"
		 						+"case t.activityid when '0000001100' "
		 						+"then '新契约' end,t.missionid,t.submissionid,t.activityid ,''"
		 						+"from lwmission t where 1=1 "
								+ " and t.activityid in ('0000001100')"
								+ " and t.defaultoperator ='" +operator+ "'"
								+ " and t.missionprop2 not in (select OtherNo from lcuwsendtrace where SendFlag='1') "
							//	+ " order by t.modifydate asc,t.modifytime asc "
								+ " union "
								+"select t.missionprop1,t.missionprop2,t.missionprop7,"
		 						+"case t.activitystatus "
		 						+"when '1' then '未人工核保'  "
		 						+"when '3' then '核保已回复' "
		 						+"when '2' then '核保未回复' end,"
		 						+"case t.activityid when '0000001100' "
		 						+"then '新契约' end,t.missionid,t.submissionid,t.activityid ,"
		 						+ "case a.sendflag "
		 						+" when '1' then '上报' "
								+" when '0' then '' end "
		 						+"from lwmission t,lcuwsendtrace a where 1=1 "
								+ " and t.activityid in ('0000001100')"
								+ " and t.defaultoperator ='" +operator+ "'"
								+"  and t.missionprop2=a.otherno"
							//	+ " order by t.modifydate asc,t.modifytime asc "
								;
		 tSql = "select count(1) from lwmission t where 1=1 "
		 				+" and t.activityid in ('0000001100') "
		 				+" and (t.defaultoperator is null or t.defaultoperator='') "
		 				;
	}
	//团单--人工核保	
	if(operFlag == "2")
	{
		 var AgentComName="";
		 //alert(fm.AgentComName.value);
		if(fm.AgentComName.value==null|fm.AgentComName.value==""|fm.AgentComName.value=="null"){
		AgentComName="";
		}else{
		var AgentComName1=fm.AgentComName.value;
		 AgentComName=" and z.AgentCom in (select AgentCom from lacom where name like '%"+AgentComName1+"%')";
		}
	var uwUser = "";
	if(operator!="group"){
		uwUser = "    and (t.defaultoperator ='" +operator+ "' or t.defaultoperator in (select usercode from lduwuser where agentusercode ='"+operator+"' and uwtype='02'))";
	}
		//待处理信箱		
		 strSQL =" select z.prtno,z.grpname,z.cvalidate,z.prem,case when t.missionprop12='0' then '上报' when t.missionprop12='1' "      
						+" then '核保回退' when t.missionprop12='2' then '抽检' when (select count(1) from lcuwsendtrace where sendtype in('3','4') and otherno=z.proposalgrpcontno)>0 then '问题件回退' when  (select count(1) from lbmission where "                    
						+" activityid = '0000002001' and missionprop2=z.prtno and missionprop20='N' and "                               
						+" t.missionprop12 is null and missionprop2 not in (select doccode from es_doc_main   where "                           
						+" (doccode = z.prtno || '101' or doccode = z.prtno || '102') and SubType='TB23' and busstype='TB')) = 1 "                                            
						+" then '问题件已修改'  when (select count(1) from es_doc_main   where (doccode = z.prtno || '101' or doccode = z.prtno || '102') "                     
						+" and SubType='TB23' and busstype='TB')=1 then '问题件回销'  else '新单' end ,z.managecom,z.grpcontno,t.MissionProp19,t.SubMissionID,t.activityid,t.MissionID " 
						+" from lcgrpcont z,lwmission t where  t.activityid='0000002004'  and "                                                 
						+" t.activitystatus in ('1','3')   and t.missionprop1=z.proposalgrpcontno "      
		 				+"  "+AgentComName+" "
		 				+uwUser
		 + getWherePart("t.missionprop2", "PrtNo")
		 + getWherePart("z.GrpName", "AppntName","like",null,"%")
		 + getWherePart("z.CValiDate", "CValiDate")
		 + getWherePart("z.ManageCom", "ManageCom","like",null,"%")
		 + getWherePart("getUniteCode(t.missionprop5)", "AgentCode")
		 + " order by t.MissionProp19 with ur "
								;	
	  strSQL1 = "select A,B,C,D,ChNote(E,F,G),H,L,J,K,N "   
							+"from"                                                                                                                                            
							+" ("                                                                                                                                               
							+"select z.prtno A,z.grpname B,z.cvalidate C,z.prem D,z.managecom H,z.grpcontno L,t.SubMissionID J,t.activityid K,t.MissionID N,"                                               
							+"(SELECT count(1) FROM LOPRTManager where Code = '74' and OtherNo=z.grpcontno  AND PrtType = '0') E,"           
							+"(SELECT count(1) FROM LOPRTManager where Code = '03' and standbyflag3=z.grpcontno  AND PrtType = '0') F,"           
							+"(SELECT count(1) FROM LOPRTManager where Code = '54' and OtherNo=z.grpcontno  AND PrtType = '0') G "            
							+" from lcgrpcont z,lwmission t where t.activityid='0000002004' and t.activitystatus in ('2') and t.missionprop1=z.grpcontno" 
									 				+uwUser         
						  + getWherePart("t.missionprop2", "PrtNo")
						  + getWherePart("z.GrpName", "AppntName","like")
						  + getWherePart("z.CValiDate", "CValiDate")
						  + getWherePart("z.ManageCom", "ManageCom","like")
						  + getWherePart("getUniteCode(t.missionprop5)", "AgentCode")
						  +")as X " 
							+"order by A"
							;	
		 
		 
		 
		 tSql = "select count(1) from lwmission t where 1=1 "
		 				+" and t.activityid in ('0000002004') "
		 				+" and t.defaultoperator is null "
		 				;	
		 				
	}
	//询价--人工核保
	if(operFlag == "3")
	{
		 strSQL = "select a.prtno,a.PolApplyDate,a.CValiDate,b.makedate,a.grpname,"
		 + "c.name,(select name from lacom where agentcom=a.agentcom),e.name,case b.activityid when '0000002004' then '新契约' end"
		 + ",'',a.grpcontno,"
		 + "case b.activitystatus "
		 + "when '1' then '未人工核保'  "
		 + "when '3' then '核保已回复' "
		 + "when '2' then '核保未回复' end,"
		 + "case b.activityid when '0000006004' then '新契约' end,"
		 + "b.missionid,b.submissionid,b.activityid from lcgrpcont a,lwmission b,laagent c,ldcom e where 1=1 "
		 + " and b.activityid in ('0000006004')"
		 + " and a.grpcontno=b.missionprop1"
		 + " and a.agentcode=c.agentcode "
		 + " and a.managecom = e.comcode "
		 + " and b.defaultoperator ='" + operator + "' "
		 + getWherePart("b.missionprop2", "PrtNo")                 
     +getWherePart("a.GrpName", "AppntName")                
     + getWherePart("a.PolApplyDate", "ApplyDate")             
     + getWherePart("a.CValiDate", "CValiDate")                
     + getWherePart("a.ManageCom", "ManageCom","like")         
     // getWherePart("b.activitystatus", "Receive")             
     //+ getWherePart("b.missionprop2", "InsuredName")    
     + getWherePart("getUniteCode(b.missionprop4)", "AgentCode")            
		 + " order by b.modifydate asc,b.modifytime asc"
			 					;	
	//  strSQL = "select t.missionprop2,t.missionprop1,t.missionprop7,case t.activitystatus when '1' then '未人工核保'  when '3' then '核保已回复' when '2' then '核保未回复' end,case t.activityid when '0000002004' then '新契约' end,t.missionid,t.submissionid,t.activityid from lwmission t where 1=1 "
	//	 					+ " and t.activityid in ('0000006004')"
	//	 					+ " and t.defaultoperator ='" + operator + "'"
	//	 					+ " order by t.modifydate asc,t.modifytime asc"
	//	 					;	
		 tSql = "select count(1) from lwmission t where 1=1 "
		 				+" and t.activityid in ('0000006004') "
		 				+" and t.defaultoperator is null "
		 				;	
		 				;		
	}	
	
	turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1); 
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  fm.UWNo.value = arrSelected[0][0];
	//查询SQL，返回结果字符串
	
	/*
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有需要核保的保单,请先申请!");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //查询待核保件数
*/
	//dealUpReport();
	turnPage.queryModal(strSQL,PolGrid,0,0,1);
	turnPage1.queryModal(strSQL1,RePolGrid);

  return true;
}

/*********************************************************************
 *  执行新契约人工核保的EasyQueryAddClick
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryAddClick()
{
	var tSel = PolGrid.getSelNo();
	var activityid = PolGrid.getRowColData(tSel - 1,10);
	var ContNo = PolGrid.getRowColData(tSel - 1,7);
	var PrtNo = PolGrid.getRowColData(tSel - 1,1);
	var MissionID = PolGrid.getRowColData(tSel - 1,11);
	var SubMissionID = PolGrid.getRowColData(tSel - 1,9);
	if(activityid == "0000001100"||activityid == "0000001160")
	{
		window.location="./UWManuInputMain.jsp?ContNo="+ContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PrtNo="+PrtNo;
	}
	if(activityid == "0000002004")
	{
		window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
	}
	if(activityid == "0000006004")
	{
		window.location="../askapp/AskGroupUW.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+activityid; 
	}
}

/*********************************************************************
 *  申请核保
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyUW()
{
	if(!verifyInput2())
	return false;
	var cApplyNo = fm.ApplyNo.value;
	
	if(cApplyNo >10)
	{
		alert("每次申请最大数为10件");
		return;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	//fm.ApplyNo.value ="5";     //每次申请为5条
	fm.action = "./ManuUWAllChk.jsp";
  fm.submit(); //提交
}

/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  

  if (FlagStr == "Fail" )
  {                 
    alert(content);
  }
  else
  { 
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");     
    //执行下一步操作
  }
  easyQueryClick();

}

function dealUpReport()
{
	var strSql = "select t.missionprop1,t.missionprop2,t.missionprop7,"
								+"case t.activitystatus "
		 						+"when '1' then '未人工核保'  "
		 						+"when '3' then '核保已回复' "
		 						+"when '2' then '核保未回复' end,"
		 						+"case t.activityid when '0000001100' "
		 						+"then '新契约' end,t.missionid,t.submissionid,t.activityid ,''"
		 						+"from lwmission t where 1=1 "
								+ " and t.activityid in ('0000001160')"
								+ " and t.defaultoperator ='" +operator+ "'"
								+ " order by t.modifydate asc,t.modifytime asc ";
turnPage2.queryModal(strSql, UpReportPolGrid);

}

function easyQueryAddClick1()
{

	var tSel = RePolGrid.getSelNo();
	var activityid = RePolGrid.getRowColData(tSel - 1,9);
	var ContNo = RePolGrid.getRowColData(tSel - 1,7);
	var PrtNo = RePolGrid.getRowColData(tSel - 1,1);
	var MissionID = RePolGrid.getRowColData(tSel - 1,10);
	var SubMissionID = RePolGrid.getRowColData(tSel - 1,8);

	if(activityid == "0000001160")
	{
		window.location="./UWManuInputMain.jsp?ContNo="+ContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PrtNo="+PrtNo;
	}
	if(activityid == "0000002004")
	{
		window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
	}
	if(activityid == "0000006004")
	{
		window.location="../askapp/AskGroupUW.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+activityid; 
	}
}
function HistoryeasyQueryClick()
{
    //历史记录
    initGrpHistoryPolGrid();
    
    //校验核保日期
	if(fm.startUWDate.value == null || fm.startUWDate.value == "")
	{
	    alert("请选择核保起始日期！");
	    return ;
	}
	var endDateSql = "select 1 from (select date('"
	    + fm.startUWDate.value 
	    + "') + 1 year enddate from dual) as a where a.enddate < '"
	    + fm.endUWDate.value + "'";
	if(easyExecSql(endDateSql))
	{
	    alert("查询时间段大于1年，请缩短查询时间段！");
	    return ;
	}
	
	//保费数据的查询脚本
	var premSql = "";
	if(fm.minPrem.value != null && fm.minPrem.value != "")
	{
	    var minPrem = fm.minPrem.value;
	    premSql += " and z.Prem > case PayIntv when 1 then " + minPrem + "/12 when 3 then " + minPrem + "/4 when 6 then " + minPrem + "/2 else " + minPrem + " end ";
	}
	if(fm.maxPrem.value != null && fm.maxPrem.value != "")
	{
	    var maxPrem = fm.maxPrem.value;
	    premSql += " and z.Prem < case PayIntv when 1 then " + maxPrem + "/12 when 3 then " + maxPrem + "/4 when 6 then " + maxPrem + "/2 else " + maxPrem + " end ";
	}
	
    var AgentComName="";
    if(fm.AgentComName.value==null|fm.AgentComName.value==""|fm.AgentComName.value=="null"){
        AgentComName="";
    }else{
        var AgentComName1=fm.AgentComName.value;
        AgentComName=" and z.AgentCom in (select AgentCom from lacom where name like '%"+AgentComName1+"%')";
    }
	var strSql3= " select A,B,C,D,E,H,L "                                                    
            +" from "                                                               
            +" ( "                                                                  
            +" select z.prtno A,z.grpname B,z.cvalidate C,z.prem D,z.managecom H,z.proposalgrpcontno L, "               
            +" '投保' E "                                                           
            +"  from lcgrpcont z where z.uwflag in ('9','4') and z.appflag<>'1' and (z.cardflag is null or z.cardflag<>'0') "+AgentComName+" "   
            + getWherePart("z.PrtNo", "PrtNo")
            + getWherePart("z.GrpName", "AppntName","like",null,"%")
            + getWherePart("z.CValiDate", "CValiDate")
            + getWherePart("z.ManageCom", "ManageCom","like",null,"%")
            + getWherePart("z.AgentCode", "AgentCode")
            
            //添加核保日期和总保费的查询  2008-4-24
            + getWherePart("z.UWDate", "startUWDate", ">=")
            + getWherePart("z.UWDate", "endUWDate", "<=")
            + premSql
            
            +" ) as X "                                                             
            +" union "                                                              
            +" select A,B,C,D,case when E>=0 then '承保' when E<0 then '终止' end,H,L" 
            +" from "                                                               
            +" ( "                                                                  
            +" select z.prtno A,z.grpname B,z.cvalidate C,z.prem D,z.managecom H,z.proposalgrpcontno L, "               
            +" (select z.cinvalidate-current date from dual) E "                    
            +"  from lcgrpcont z where z.uwflag in ('9','4') and z.appflag='1' and (z.cardflag is null or z.cardflag<>'0') "+AgentComName+" "    
            + getWherePart("z.PrtNo", "PrtNo")
            + getWherePart("z.GrpName", "AppntName","like",null,"%")
            + getWherePart("z.CValiDate", "CValiDate")
            + getWherePart("z.ManageCom", "ManageCom","like",null,"%")
            + getWherePart("z.AgentCode", "AgentCode")
            
            //添加核保日期和总保费的查询  2008-4-24
            + getWherePart("z.UWDate", "startUWDate", ">=")
            + getWherePart("z.UWDate", "endUWDate", "<=")
            + premSql
            
            +" ) as X "                                                             
            +" union "                                                              
            +" select A,B,C,D,E,H,L "                                                   
            +" from "                                                               
            +" ( "                                                                  
            +" select z.prtno A,z.grpname B,z.cvalidate C,z.prem D,z.managecom H,z.proposalgrpcontno L, "               
            +" '退保' E "                                                           
            +"  from lbgrpcont z where z.uwflag in ('9','4') and z.appflag='1' and (z.cardflag is null or z.cardflag<>'0') "+AgentComName+" "    
            + getWherePart("z.PrtNo", "PrtNo")
            + getWherePart("z.GrpName", "AppntName","like",null,"%")
            + getWherePart("z.CValiDate", "CValiDate")
            + getWherePart("z.ManageCom", "ManageCom","like",null,"%")
            + getWherePart("z.AgentCode", "AgentCode")
            
            //添加核保日期和总保费的查询  2008-4-24
            + getWherePart("z.UWDate", "startUWDate", ">=")
            + getWherePart("z.UWDate", "endUWDate", "<=")
            + premSql
            
            +" ) as X "                                                             
            +" union "                                                              
            +" select A,B,C,D,E,H,L "                                                   
            +" from "                                                               
            +" ( "                                                                  
            +" select z.prtno A,z.grpname B,z.cvalidate C,z.prem D,z.managecom H,z.proposalgrpcontno L, "               
            +" '撤保' E "                                                           
            +"  from lobgrpcont z where z.uwflag in ('9','4') and (z.cardflag is null or z.cardflag<>'0') "+AgentComName+" "                     
            + getWherePart("z.PrtNo", "PrtNo")
            + getWherePart("z.GrpName", "AppntName","like",null,"%")
            + getWherePart("z.CValiDate", "CValiDate")
            + getWherePart("z.ManageCom", "ManageCom","like",null,"%")
            + getWherePart("z.AgentCode", "AgentCode")
            
            //添加核保日期和总保费的查询  2008-4-24
            + getWherePart("z.UWDate", "startUWDate", ">=")
            + getWherePart("z.UWDate", "endUWDate", "<=")
            + premSql
            
            +" ) as X "                                                             
            +" union "                                                              
            +" select A,B,C,D,E,H,L "                                                   
            +" from "                                                               
            +" ( "                                                                  
            +" select z.prtno A,z.grpname B,z.cvalidate C,z.prem D,z.managecom H,z.proposalgrpcontno L, "               
            +" '撤保' E "                                                           
            +"  from lcgrpcont z where z.uwflag in ('1','a') and (z.cardflag is null or z.cardflag<>'0') "+AgentComName+" "                      
            + getWherePart("z.PrtNo", "PrtNo")
            + getWherePart("z.GrpName", "AppntName","like",null,"%")
            + getWherePart("z.CValiDate", "CValiDate")
            + getWherePart("z.ManageCom", "ManageCom","like",null,"%")
            + getWherePart("z.AgentCode", "AgentCode")
            
            //添加核保日期和总保费的查询  2008-4-24
            + getWherePart("z.UWDate", "startUWDate", ">=")
            + getWherePart("z.UWDate", "endUWDate", "<=")
            + premSql
            													
            +" ) as X order by C with ur "  
            ;
    fm.historySql.value = strSql3;
	turnPage3.queryModal(strSql3,HistoryPolGrid,0,0,1);
}
function easyQueryHistory()
{
		var tSel = HistoryPolGrid.getSelNo();
		var ContNo = HistoryPolGrid.getRowColData(tSel - 1,7);
		var PrtNo = HistoryPolGrid.getRowColData(tSel - 1,1);
		var Resource= HistoryPolGrid.getRowColData(tSel - 1,5);
		if(Resource=="退保"){
			Resource=1;
		}else if(Resource=="撤保"){
			Resource=2;
		}else{
			Resource=3;
		}
		//alert(Resource);
		//alert(ContNo);
		//alert(PrtNo);
		window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&Resource="+Resource+"&LoadFlag=1";
}

//下载已完成信箱的下载
function downloadHistoryList()
{
    if(HistoryPolGrid.mulLineCount == 0)
    {
      alert("没有需要下载的数据");
      return false;
    }
    fm.target='_blank';
    fm.action = "ManuUWHistoryDown.jsp";
    fm.submit();
}
