//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//保存客户回复信息
function submitForm()
{
	beforeSubmit();
  var i = 0;
  fm.fmtransact.value= "INSERT||MAIN" ;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./UWResultWriteBackSave.jsp";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDiseaseName.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  //首先判断是否选择某一险种
  var tSel = UWResultGrid.getSelNo()-1;
  if(tSel<0)
  {
  	alert("请选择一条险种信息！");
  	return;
  }
  fm.PolNo.value=UWResultGrid.getRowColData(tSel,9);
  fm.LoadFlag.value="UWNOTICE";
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  fm.fmtransact.value = "INSERT||MAIN" ;
  alert(fm.fmtransact.value);
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//查询核保通知书
function QueryUWResultNotice(){
	if(tLoadFlag=='1')
	{
		fm.savebutton.disabled = true;
	}
	var strSql = "select a.otherno,a.prtseq,b.insuredname,b.riskcode,"
	//查询保额档次
	+"case when b.mult>0 then b.mult else b.amnt end, "
	+" case when c.AddPremFlag<>'0' and c.AddPremFlag is not null then '有加费' end,"
	+" case when c.SpecFlag<>'0' and c.SpecFlag is not null then '有免责' end, "
	+" d.codename,b.PolNo "
	+" from loprtmanager a,lcpol b,lcuwmaster c,ldcode d where a.otherno='"
	+ContNo+"' and a.otherno=c.ProposalContNo and b.contno=c.contno and b.proposalno=c.proposalno "
	+" and d.codetype='uwflag' and c.passflag=d.code and a.code='05' order by b.insuredno,b.riskcode ";
	//turnPage.queryModal(strSql, UWResultGrid);
	var arr = easyExecSql(strSql);
	if(arr){
		displayMultiline(arr,UWResultGrid,turnPage);
	}
}
//判断客户结论是否同意,如果不同意弹出不同意操作.
function afterCodeSelect(codeName,obj){
}