<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContUWAutoChk.jsp
//程序功能：合同单自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCContSet tLCContSet = new LCContSet();

	String tProposalNo[] = request.getParameterValues("ContGrid1");
	String tChk[] = request.getParameterValues("InpContGridChk");
	
	boolean flag = false;
	int proposalCount = tProposalNo.length;
	//int ProposalCount = 10;
	System.out.println("count:"+proposalCount);	
	if (proposalCount > 0)
	{
		for (int i = 0; i < proposalCount; i++)
		{
			if (tProposalNo[i] != null && tChk[i].equals("1"))
			{
				System.out.println("ProposalNo:"+i+":"+tProposalNo[i]);
	  			LCContSchema tLCContSchema = new LCContSchema();
	
		    		tLCContSchema.setProposalNo( tProposalNo[i] );
	    
				tLCContSet.add( tLCContSchema );
		    		flag = true;
			}
		}
	}
	
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCContSet );
		tVData.add( tG );
		
		// 数据传输
		ContUWAutoChkUI tContUWAutoChkUI   = new ContUWAutoChkUI();
		if (tContUWAutoChkUI.submitData(tVData,"INSERT") == false)
		{
			int n = tContUWAutoChkUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tContUWAutoChkUI.mErrors.getError(i).errorMessage);
			Content = " 自动核保失败，原因是: " + tContUWAutoChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tContUWAutoChkUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 自动核保成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 自动核保失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
	}
	else
	{
		Content = "没有数据提交!";
	}
		  
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initContGrid();
</script>
</html>
