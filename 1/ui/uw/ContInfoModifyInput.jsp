<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ContInfoModifyInput.jsp
//程序功能：新契约信息修改
//创建日期：2002-09-24 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构	

</script>

<head >
<title>记事本查询 </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="ContInfoModify.js"></SCRIPT>
  <%@include file="ContInfoModifyInit.jsp"%>
  
</head>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./ContInfoModifySave.jsp">
   <table  class= common align=center>
      	<TR  class= common>   
          <Input class= common name=ContNo  type="hidden">
          <TD  class= title> 印刷号码 </TD>
          <TD  class= input><Input class= common name=PrtNo verify="印刷号码|len=11&int"> </TD>
          <TD  class= title>  投保人姓名  </TD>
          <TD  class= input> <Input class= common name=AppntName verify="投保人姓名|len<=120"> </TD>
          <TD  class= title>  被保人姓名</TD>
          <TD  class= input> <Input class= common name=InsuredName verify="被保人姓名|len<=120"> </TD>     
           </TR>
        <TR  class= common>
        	<TD  class= title> 申请日期 </TD>
        	<TD  class= input><Input class=coolDatePicker name=PolApplyDate>  </TD>  
          <TD  class= title> 生效日期 </TD>
        	<TD  class= input><Input class=coolDatePicker name=CValiDate>  </TD>  
          <TD  class= title> 管理机构 </TD>
          <TD  class= input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true > </TD>     
       </TR>
        <TR  class= common>
         <TD  class= title> 业务员代码 </TD>
         <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
         <td class="title8">渠道</td>
         <td class="input8"><input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" /></td>
         <td class="title8">网点</td>
         <td class="input8"><input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" /></td>
        </TR>               
    </table>
    
          <INPUT class=cssbutton VALUE="查询投保单" TYPE=button onclick="easyQueryClick();"> 
          <INPUT class=cssbutton VALUE="清单下载" TYPE=button onclick="downloadClick();"> 
          <INPUT  type= "hidden" class= Common name= querySql >
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div> 					
  	</div>
    
  <p>
      <INPUT VALUE="信息修改" class= CssButton TYPE=button onclick="showContInfo();"> 
  </p>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>