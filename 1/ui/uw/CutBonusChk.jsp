<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CutBonusChk.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
  
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
  LOBonusPolSet tLOBonusPolSet = new LOBonusPolSet();
	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCPolSet tLCPolSet = new LCPolSet();

	String tflag = request.getParameter("PolType");
	
	String tPolNo[] = request.getParameterValues("PolGrid1");
	String tYear[] = request.getParameterValues("PolGrid3");
	String tmoney[] = request.getParameterValues("PolGrid4");
	String tgettype[] = request.getParameterValues("PolGrid7");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	
	boolean flag = false;
	int feeCount = tPolNo.length;
    LOBonusPolSchema tLOBonusPolSchema = new LOBonusPolSchema();
	for (int i = 0; i < feeCount; i++)
	{
		if (!tPolNo[i].equals("") && tChk[i].equals("1"))
		{
		    System.out.println("PolNo:"+i+":"+tPolNo[i]);
	  	    
	  	    tLOBonusPolSchema = new LOBonusPolSchema();
	  	    tLOBonusPolSchema.setPolNo(tPolNo[i]);
	  	    tLOBonusPolSchema.setFiscalYear(tYear[i]);
		    
		    tLOBonusPolSet.add( tLOBonusPolSchema );
		    System.out.println("size:"+tLOBonusPolSet.size());
		    flag = true;
		}
	}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLOBonusPolSet );
		tVData.add( tG );
		
		// 数据传输
		GetBonusUI tGetBonusUI   = new GetBonusUI();
		if (tGetBonusUI.submitData(tVData,"INSERT") == false)
		{
			int n = tGetBonusUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tGetBonusUI.mErrors.getError(i).errorMessage);
			Content = " 红利处理失败，原因是: " + tGetBonusUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tGetBonusUI.mErrors;
		    //tErrors = tGetBonusUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 红利处理成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 红利处理部分数据失败：请察看红利错误日志信息!";
		    	/*
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
				}
				*/
		    	FlagStr = "Fail";
		    }
		}
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initPolGrid();
</script>
</html>
