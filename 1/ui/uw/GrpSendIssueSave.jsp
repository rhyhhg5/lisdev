<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpSendIssueSave.jsp
//程序功能：新契约人工核保发送核保通知书报告录入
//创建日期：2005-07-25 11:10:36
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 
<%
  //输出参数
  CErrors tError = null;
 String FlagStr = "Fail";
 String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
  
    LCGrpContSet tLCGrpContSet = new LCGrpContSet();
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	String tGrpContNo = request.getParameter("GrpContNo");
	System.out.println("tGrpContNo="+tGrpContNo);
	tLCGrpContSchema.setGrpContNo(tGrpContNo);
	tLCGrpContSet.add(tLCGrpContSchema);
	
	boolean flag = true;
	
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpContSet);
		tVData.add( tG );
		System.out.println("dfdfdfdfd");
		UWSendIssueNoticeUI tUWSendIssueNoticeUI   = new UWSendIssueNoticeUI();
		if (tUWSendIssueNoticeUI.submitData(tVData,"INSERT") == false)
		{
			int n = tUWSendIssueNoticeUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			Content = " 发问题件通知书失败，原因是: " + tUWSendIssueNoticeUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tUWSendIssueNoticeUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 发问题件通知书成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 发问题件通知书失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
