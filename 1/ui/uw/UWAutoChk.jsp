<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWAutoChk.jsp
//程序功能：个人自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
//	LCContSet tLCContSet = new LCContSet();
	LCContSchema tLCContSchema = new LCContSchema();

	String tContNo[] = request.getParameterValues("PolGrid1");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	String tMissionID = request.getParameter("PolGrid6");
	String tSubMissionID = request.getParameter("PolGrid7");
	System.out.println("Missionid:"+tMissionID);
	System.out.println("SubMissionID:"+tSubMissionID);
	boolean flag = false;
	int contCount = tContNo.length;	
  TransferData tTransferData = new TransferData();

	for (int ii = 0; ii < contCount; ii++)
	{
		System.out.println("ContNo:" + tContNo[ii]);
		if (!tContNo[ii].equals("") && tChk[ii].equals("1"))
		{
			System.out.println("ContNo:"+ii+":"+tContNo[ii]);
		    tLCContSchema.setContNo( tContNo[ii] );
            tTransferData.setNameAndValue("LCContSchema",tLCContSchema);
	    	tTransferData.setNameAndValue("MissionID",tMissionID);
			tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
//		    tLCContSet.add( tLCContSchema );
		    flag = true;
		}
		try
		{
		System.out.println("flag=="+flag);
		  	if (flag == true)
		  	{
				// 准备传输数据 VData
				VData tVData = new VData();
//				tVData.add( tLCContSet );
				//tVData.add( tLCContSchema );
				tVData.add( tTransferData );
				tVData.add( tG );
				// 数据传输
				TbWorkFlowUI tTbWorkFlowUI   = new TbWorkFlowUI();
				if (tTbWorkFlowUI.submitData(tVData,"0000001003") == false)
				{
					int n = tTbWorkFlowUI.mErrors.getErrorCount();
					System.out.println("n=="+n);
					for (int j = 0; j < n; j++)
					System.out.println("Error: "+tTbWorkFlowUI.mErrors.getError(j).errorMessage);
					Content = " 自动核保失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
					FlagStr = "Fail";
				}
				//如果在Catch中发现异常，则不从错误类中提取错误信息
				if (FlagStr == "Fail")
				{
				    tError = tTbWorkFlowUI.mErrors;
				    //tErrors = tTbWorkFlowUI.mErrors;
				    Content = " 自动核保成功! ";
				    if (!tError.needDealError())
				    {                          
				    	//Content = "自动核保成功!";
				    	int n = tError.getErrorCount();
		    			if (n > 0)
		    			{    			      
					      for(int j = 0;j < n;j++)
					      {
					        //tError = tErrors.getError(j);
					        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
					      }
					    }
		
				    	FlagStr = "Succ";
				    }
				    else                                                                           
				    {
				    	int n = tError.getErrorCount();
				    	//Content = "自动核保失败!";
		    			if (n > 0)
		    			{
					      for(int j = 0;j < n;j++)
					      {
					        //tError = tErrors.getError(j);
					        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
					      }
							}
				    	FlagStr = "Fail";
				    }
				}
			}
			else
			{
				Content = "请选择保单！";
			}  
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Content = Content.trim() +" 提示:异常退出.";
		}
	}
System.out.println("AUTO UW END!");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initPolGrid();
	<%
	//parent.fraInterface.fm.submit();
	%>
</script>
</html>
