<%@page import="com.sinosoft.utility.*"%>
<%
//session中取到GlobalInput对象，在对象中取到ManageCom值
GlobalInput globalInput = (GlobalInput) session.getValue("GI");
String strManageCom = globalInput.ManageCom;
 %>

<script language="JavaScript">
	// 输入框的初始化（单记录部分）
	function initInpBox()
	{
	    try
	    {        
	    	//初始化
	    	var manage='<%=strManageCom%>';
	    	fm.all("ManageCom").value = manage;
	    	if(fm.all("ManageCom").value==86)
	        {
	            fm.all("ManageCom").readOnly=false;
	        }
	        else
	        {
	            fm.all("ManageCom").readOnly=true;
	        }
	        
	        if(fm.all("ManageCom").value!=null)
	        {
	            var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all("ManageCom").value+"'");
	            //显示代码选择中文
	            if (arrResult != null) 
	            {
	                fm.all("ManageComName").value=arrResult[0][0];
	            }
	        }
	    }
	    catch(ex)
	    {
	        alert("在PolicyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!"); 
	    }
	}
	
	//页面body加载的时候加载这个函数（初始化）
	function initForm()
	{
	  try
	  {
	  	initInpBox();
		initLPWBContGrid(); 
	  }
	  catch(ex)
	  {
	    alert("在PolicyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	  }
	}

	var LPWBContGrid;          //定义为全局变量，提供给displayMultiline使用

	//保单信息初始化
	function initLPWBContGrid(){
		var iArray = new Array();
	    try
	    {
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="40px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[1]=new Array();
	      iArray[1][0]="保单号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[1][1]="60px";            		//列宽
	      iArray[1][2]=10;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[2]=new Array();
	      iArray[2][0]="管理机构";         			//列名
	      iArray[2][1]="90px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[3]=new Array();
	      iArray[3][0]="投保机构";         			//列名
	      iArray[3][1]="82px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[4]=new Array();
	      iArray[4][0]="承保日期";         			//列名
	      iArray[4][1]="70px";            		//列宽
	      iArray[4][2]=100;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[5]=new Array();
	      iArray[5][0]="保单状态";         			//列名
	      iArray[5][1]="70px";            		//列宽
	      iArray[5][2]=100;            			//列最大值
	      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[6]=new Array();
	      iArray[6][0]="是否配置";         			//列名
	      iArray[6][1]="70px";            		//列宽
	      iArray[6][2]=100;            			//列最大值
	      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      LPWBContGrid = new MulLineEnter( "fm" , "LPWBContGrid" ); 
	      //这些属性必须在loadMulLine前
	      LPWBContGrid.mulLineCount = 3;   
	      LPWBContGrid.displayTitle = 1;
	      LPWBContGrid.hiddenPlus = 1;
	      LPWBContGrid.hiddenSubtraction = 1;
	      LPWBContGrid.canSel = 1;
	      
	      LPWBContGrid.loadMulLine(iArray);  
	      //在MulLine中单击RadioBox响应开发人员外部编写的js（在.js文件中）
	      LPWBContGrid.selBoxEventFuncName = "onclkSelBox";
	    }
	    catch(ex)
	    {
	        alert(ex);
	    }
	}
</script>
