//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//处理个案催收
function ConfirmData()
{
	if(!CheckData()){
		return false;
	}
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面。";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit();
}

function CheckData()
{
    if(GrpContGrid.mulLineCount!=1){
    	alert("保单待核销的数据查询失败!");
		return false;
    }
    fm.all("GetNoticeNo").value=GrpContGrid.getRowColData(0,6);
	if(fm.all('GetNoticeNo').value==null||fm.all('GetNoticeNo').value==''){
		alert("保单没有待核销的数据!");
		return false;
	}
	return true;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpContGrid();
	getContInfo();
}

//查询团单信息
function getContInfo()
{
	var strSQL = " select a.grpcontno,sum(a.prem),sum(b.prem),a.paytodate, "
			+" (select min(paytodate) from lcgrppayplan where prtno=a.prtno and int(plancode)=(int(a.plancode)+1)),a.getnoticeno "
			+" from lcgrppayactu a,lcgrppayplan b where a.prtno=b.prtno "
			+" and a.contplancode=b.contplancode and a.paytodate=b.paytodate "
			+" and a.plancode=b.plancode and a.state='2' "			
			+" and a.getnoticeno in (select getnoticeno from ljspayb where dealstate='4' and othernotype='1' and otherno='"+fm.all('GrpContNo').value+"') "
			+" group by a.prtno,a.getnoticeno,a.grpcontno,a.paytodate,a.plancode with ur ";
    turnPage.queryModal(strSQL, GrpContGrid);
}