<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：expirBenefitChangePayModeSave.jsp
//程序功能：
//创建日期：2007-12-3 11:30下午
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
  //输出参数
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");

  LJSGetSchema tLJSGetSchema = new LJSGetSchema();
  LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
  tLJSGetSchema.setGetNoticeNo(request.getParameter("GetNoticeNo"));
  tLJSGetSchema.setPayMode(request.getParameter("PayModeNew"));
  tLJSGetSchema.setBankCode(request.getParameter("BankCode"));
  tLJSGetSchema.setAccName(request.getParameter("AccName"));
  tLJSGetSchema.setBankAccNo(request.getParameter("BankAccNo"));
  //modify by hyy
  //tLJSGetSchema.setOtherNo(request.getParameter("contNoForModify"));
  // 这个地方不能乱存数据，tLJSGetSchema.setOtherNo 是任务号 不能放一个保单号进来。
  tLJSGetDrawSchema.setContNo(request.getParameter("contNoForModify"));
  
  
  System.out.println(request.getParameter("contNoForModify")+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`");
  
  VData tVData = new VData();
  tVData.add(tLJSGetSchema);
  tVData.add(tLJSGetDrawSchema);
  tVData.add(tGI);
   
  ExpirBenefitChangePayModeUI tExpirBenefitChangePayModeUI = new ExpirBenefitChangePayModeUI();
  if (!tExpirBenefitChangePayModeUI.submitData(tVData,""))
  {
    FlagStr = "Fail";
    Content = "保存失败，原因是:" + tExpirBenefitChangePayModeUI.mErrors.getFirstError();
  }
  else
  {
    FlagStr = "Succ";
    Content = "操作成功";
  }
  
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

