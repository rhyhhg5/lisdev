<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

//返回按钮初始化
var str = "";
var loadFlag = "<%=request.getParameter("LoadFlag")%>";
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
	
   // fm.all('AppntNo').value = '';
    //fm.all('GrpContNo').value = '';    
  
  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initCustomerGrid();
	initGrpJisPayGrid();
	initOperGrid();
	//查询文本框数据
	textQeury();
	//查询本通知书收费情况
	JisPayQuery();
    //查询客户受理情况
	easyQueryClick();
	//选择操作类型
	operSel();
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initCustomerGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";            		//列宽
		  iArray[0][2]=10;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[1]=new Array();
		  iArray[1][0]="受理号";         		//列名
		  iArray[1][1]="80px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[2]=new Array();
		  iArray[2][0]="业务类型";         	//列名
		  iArray[2][1]="50px";            		//列宽
		  iArray[2][2]=200;            			//列最大值
		  iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
		  iArray[2][4]="TaskTypeNo";
		  iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
		  iArray[2][9]="业务类型|code:TaskTypeNo&NOTNULL";
		  iArray[2][18]=250;
		  iArray[2][19]= 0 ;

		  iArray[3]=new Array();
		  iArray[3][0]="经办人";         		//列名
		  iArray[3][1]="70px";            		//列宽
		  iArray[3][2]=200;            			//列最大值
		  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[4]=new Array();
		  iArray[4][0]="受理机构";         		  //列名
		  iArray[4][1]="80px";            		//列宽
		  iArray[4][2]=200;            			//列最大值
		  iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		  iArray[4][4]="AcceptCom";
		  iArray[4][5]="3";              	                //引用代码对应第几列，'|'为分割符
		  iArray[4][9]="受理机构|code:AcceptCom";
		  iArray[4][18]=250;
		  iArray[4][19]= 0 ;

		  iArray[5]=new Array();
		  iArray[5][0]="处理状态";         		  //列名
		  iArray[5][1]="60px";            		//列宽
		  iArray[5][2]=200;            			//列最大值
		  iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		  iArray[5][4]="TaskStatusNo";
		  iArray[5][5]="3";              	                //引用代码对应第几列，'|'为分割符
		  iArray[5][9]="处理状态|code:TaskStatusNo&NOTNULL";
		  iArray[5][18]=250;
		  iArray[5][19]= 0 ;
      

		  CustomerGrid = new MulLineEnter( "fm" , "CustomerGrid" ); 
		  //这些属性必须在loadMulLine前
		  CustomerGrid.mulLineCount = 0;   
		  CustomerGrid.displayTitle = 1;
		  CustomerGrid.locked = 1;
		  CustomerGrid.canSel = 1;
		  CustomerGrid.hiddenPlus = 1;
		  CustomerGrid.hiddenSubtraction = 1;
		  CustomerGrid.loadMulLine(iArray);  
		   
      }
      catch(ex)
      {
        alert(ex);
      }
}


// 催收记录的初始化
function initGrpJisPayGrid()
  {                               
    var iArray = new Array();
      try
      {
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";            		//列宽
		  iArray[0][2]=10;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[1]=new Array();
		  iArray[1][0]="保单号";         		//列名
		  iArray[1][1]="80px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[2]=new Array();
		  iArray[2][0]="催收记录号";         		//列名
		  iArray[2][1]="80px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[3]=new Array();
		  iArray[3][0]="应收记录号";         		//列名
		  iArray[3][1]="90px";            		//列宽
		  iArray[3][2]=200;            			//列最大值
		  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[4]=new Array();
		  iArray[4][0]="保费金额";         		//列名
		  iArray[4][1]="80px";            		//列宽
		  iArray[4][2]=200;            		//列最大值
		  iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

		  iArray[5]=new Array();
		  iArray[5][0]="应收时间";         		//列名
		  iArray[5][1]="70px";            		//列宽
		  iArray[5][2]=200;            	        //列最大值
		  iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

		  iArray[6]=new Array();
		  iArray[6][0]="收费方式";         		//列名
		  iArray[6][1]="60px";            		//列宽
		  iArray[6][2]=200;            	        //列最大值
		  iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

		  iArray[7]=new Array();
		  iArray[7][0]="缴费期限/转帐时间";         		//列名
		  iArray[7][1]="80px";            		//列宽
		  iArray[7][2]=200;            	        //列最大值
		  iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

		  iArray[8]=new Array();
		  iArray[8][0]="业务员";         		//列名
		  iArray[8][1]="60px";            		//列宽
		  iArray[8][2]=200;            	        //列最大值
		  iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

		  iArray[9]=new Array();
		  iArray[9][0]="所属部门";         		//列名
		  iArray[9][1]="50px";            		//列宽
		  iArray[9][2]=200;            	        //列最大值
		  iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

          iArray[10]=new Array();
		  iArray[10][0]="回访处理结果";         		//列名
		  iArray[10][1]="80px";            		//列宽
		  iArray[10][2]=200;            	        //列最大值
		  iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

		  GrpJisPayGrid = new MulLineEnter( "fm" , "GrpJisPayGrid" ); 
		  //这些属性必须在loadMulLine前
		  GrpJisPayGrid.mulLineCount =0;   
		  GrpJisPayGrid.displayTitle = 1;
		  GrpJisPayGrid.canSel = 1;
		  GrpJisPayGrid.hiddenPlus = 1;
		  GrpJisPayGrid.hiddenSubtraction = 1;
		  GrpJisPayGrid.loadMulLine(iArray);  
          GrpJisPayGrid.selBoxEventFuncName ="payQeury"; 
	  }
      catch(ex)
      {
        alert("GrpPhoneVisitInit.jsp-->initGrpJisPayGrid函数中发生异常:初始化界面错误!");
      }
}

// 选择重设交费事项或终止缴费
function initOperGrid()
  {                               
    var iArray = new Array();
      try
      {
          iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="50px";            		//列宽
		  iArray[0][2]=10;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[1]=new Array();
		  iArray[1][0]="操作";         		    //列名
		  iArray[1][1]="70px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

		  iArray[2]=new Array();
		  iArray[2][0]="操作";         		    //列名
		  iArray[2][1]="70px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

         

		  OperGrid = new MulLineEnter( "fm" , "OperGrid" ); 
		  //这些属性必须在loadMulLine前
		  OperGrid.mulLineCount =0;   
		  OperGrid.displayTitle = 1;
		  OperGrid.canSel = 1;
		  OperGrid.hiddenPlus = 1;
		  OperGrid.hiddenSubtraction = 1;
		  OperGrid.loadMulLine(iArray);  
		  OperGrid.selBoxEventFuncName="operSelSelBox";

	  }
	  catch(ex)
      {
        alert("GrpPhoneVisitInit.jsp-->initOperGrid函数中发生异常:初始化界面错误!");
      }
  }
</script>
