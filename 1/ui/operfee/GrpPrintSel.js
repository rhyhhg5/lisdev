
//打印通知书
function printLetter()
{
	//即时打印
	if(fm.printMode[0].checked == true)
	{
		window.open("../f1print/GrpDueFeePrint.jsp?GetNoticeNo="+ tGetNoticeNo);
	}
	//批量打印
	else if(fm.printMode[1].checked == true)
	{
	  alert("暂不支持");
	  return false;
	}
  else
  {
    alert("请选择打印方式");
    return false;
  }
	
	top.close();
}


function goBack()
{
	top.close();
	top.opener.top.focus();
}

function submitForm()
{
	fm.submit();
}

//提交数据后的操作
function afterSubmit(FlagStr, content)
{
	window.focus();
	
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		top.close();
		top.opener.top.focus();
	}
	else if(fm.printType.value != "printSoon")
	{
		top.close();
		top.opener.top.location.reload();
		
		var win = window.open("../f1print/GrpDueFeePrint.jsp?GetNoticeNo="+ tGetNoticeNo);
		win.focus();
	}
    else
    {
		top.close();
		top.opener.top.location.reload();
    }
}