//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close(FlagStr);  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	

//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    //执行下一步操作
  } 
   
}




//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{
    var tSel = GrpContGrid.getSelNo();	
    if( tSel == 0 || tSel == null )
    {
	alert( "请先选择一条记录，再点击'团单续期个案催收'按钮。" );
	return false;
    }
    var tRow = GrpContGrid.getSelNo() - 1; 
    //说明应该在这里把窗体相关值填充！杨红于2005-07-19注释
    fm.PrtNo.value=GrpContGrid.getRowColData(tRow, 2);
    fm.all('GrpContNo').value=GrpContGrid.getRowColData(tRow, 1);
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function GrpMulti()
{

  var showStr="本程序运行时间可能较长，请点击‘确定’按钮，关闭本窗口。本次作业运行情况，请到本窗口的‘当前线程查询’查询。作业已完成的，请到‘催收记录查询’菜单中查询。";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	

  fmMulti.submit();	

}

function GrpSingle()
{
    if(CheckGrpPolNo())
    {  
		 var showStr="本程序运行时间可能较长，请点击‘确认’按钮，关闭本窗口。本次作业运行情况，请到本窗口的‘当前线程查询’查询。作业已完成的，请到‘催收记录查询’菜单中查询。";
		 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
    fm.submit();
    }
//    else 
//    alert("请输入正确的集体保单号");		
}

function CheckDate()
{
  if( !(fm.all('CValidate').value=="") && !isDate(fm.all('CValidate').value))  {
  	alert("请输入正确的日期");
    return false;
  }
  
  return true;	
	
}

// 查询按钮
function easyQueryClick()
{
	
	// 初始化表格
	initGrpContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收	
	
	var strSQL = "select b.GrpContNo,b.PrtNo,b.GrpName,b.CValiDate,(SELECT min(paytodate) from lccont a where '1136951704000'='1136951704000' and  a.grpcontno=b.grpcontno)" 
	     +",b.cinvalidate -1 day,(select codename from ldcode where codetype='payintv' and code=char(b.Payintv)) from LCGrpCont b where  "
			 +" (b.AppFlag='1' or b.AppFlag='9' ) "				
       + getWherePart( 'b.GrpContNo','GrpContNo' )
			 + getWherePart( 'b.AppntNo', 'AppntNo')
       + " with ur"
	turnPage.queryModal(strSQL, GrpContGrid); 
	
	
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var cGrpContNo = GrpContGrid.getRowColData( tSel - 1, 1 );
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ cGrpContNo+"&ContType=1");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}

//对比增减人对比
function CompPerson(){

	if(!CheckDate()){
		return false;
	}
	var selNo = GrpContGrid.getSelNo();	
	if (selNo==0 || selNo==null){
		alert("请选择一条记录");
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面。";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
	fmComp.all("HidGrpContNo").value = GrpContGrid.getRowColData(selNo - 1,1);
	fmComp.all("HidBankCode").value = fm.all("BankCode").value;
	fmComp.all("HidBankAccNo").value =  fm.all('BankAccNo').value;
	fmComp.all("HidBankAccName").value =  fm.all('BankAccName').value;
	fmComp.all("HidCValidate").value =  fm.all('CValidate').value;	

	fmComp.submit();
	
}






