<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：IndiPayForPrtIns.jsp
//程序功能：
//创建日期：2005-12-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	String tPayNo = request.getParameter("PayNo");
	String tPayDate = request.getParameter("PayDate");		
	String tDueMoney = request.getParameter("DueMoney");
	String tDif = request.getParameter("Dif");
	System.out.println(tPayNo);
	System.out.println(tPayDate);
	System.out.println(tDueMoney);
	System.out.println(tDif);

	LJAPaySchema tLJAPaySchema = new LJAPaySchema();
	tLJAPaySchema.setPayNo(tPayNo);  

	TransferData tTransferData= new TransferData();	
	VData tVData = new VData();
		
	tTransferData.setNameAndValue("PayDate",tPayDate);
	tTransferData.setNameAndValue("DueMoney",tDueMoney);
	tTransferData.setNameAndValue("Dif",tDif);
	tVData.addElement(tG);
	tVData.addElement(tLJAPaySchema);	
	tVData.addElement(tTransferData);	
 
	IndiPayComptPrintBL tIndiPayComptPrintBL = new IndiPayComptPrintBL(); 
    if(!tIndiPayComptPrintBL.submitData(tVData,"INSERT"))
    {
		operFlag = false;
     	Content = tIndiPayComptPrintBL.mErrors.getErrContent();                
    }
%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		parent.fraInterface.printPDF();
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>