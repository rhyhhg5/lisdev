 <html> 
<%
//程序名称：IndiManuUWCont.jsp
//程序功能：个险续保保单列表
//创建日期：2006-08-14 
//创建人  ：Yangyalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
  
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="IndiManuUWCont.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>


  <%@include file="IndiManuUWContInit.jsp"%>
</head>
<body  onload="initForm();" >

<form name=fm action="IndiManuUWContSave.jsp" target=fraSubmit method=post>
  <div id = "test"></div>
  <br></br>
  <br></br>
  <table  class= common align=center>
    <tr>
      <TD  class= title>
      管理机构:
      </TD>
      <TD  class= input>
        <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
      </TD>
      <TD  class= title>
      所属营业部代码:
      </TD>
      <TD>
        <Input class="codeNo"  name=AgentGroup elementtype=nacessary ondblclick="return showCodeList('agentgroup',[this,AgentGroupName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('agentgroup',[this,AgentGroupName],[0,1],null,null,null,1);"><input class=codename name=AgentGroupName readonly=true >
      </TD> 
      <TD  class= title>
      业务员代码
      </TD>
      <TD  class= input>
        <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
      </TD>   
      <TD  class= input colspan="2"> </td>
	  </tr>  
  	<TR  class= common>
      <TD  class= title>
        保单号
      </TD>
      <TD  class= input>
        <Input class= common name=ContNo>
      </TD>
      <TD class= title>
        应收记录批次号
      </TD>
      <TD  class= input>
        <Input class=common name=serialNo>
      </TD> 
      <TD  class= title>
        送核起止日期
      </TD>  
      <TD  class= input>  
        <Input class="coolDatePicker" name=StartDate verify="开始日期|date">
      </TD>
      <TD  class= input>  
        <Input class="coolDatePicker" name=EndDate verify="结束日期|date"> 
      </TD>
    </TR>
  </table>
  <Div>
		<input value="查  询" class=cssButton type=button onclick="queryContInfo();">
	</Div>
  <table>
    <tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);">
  		</td>
  		<td class= titleImg>
  			 续保送核清单
  		</td>
  	</tr>
  </table>
	<Div  id= "divLCCont" style= "display: ''">
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanContGrid" >
				</span> 
		  	</td>
		  </tr>
    </table>
    <center>   
      <Div id= "divPage2" align=center style= "display: 'none' "> 	
        <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage(),showUWIdea();"> 
        <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage(),showUWIdea();"> 					
        <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage(),showUWIdea();"> 
        <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage(),showUWIdea();">	
      </Div>		
    </center> 					
	</div> 
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
