<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {                                   
	// 保单查询条件
   fm.Infomation.value = "保单缴费信息表中“约定缴费总金额”与 “实际已缴总金额”两列显示的金额均不包含保单首期缴费金额";
	showAllCodeName();  
  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    

function initForm()
{
  try
  {
	initInpBox();
	initGrpContGrid();
	initGrpPayGrid();
	initGrpPayPlanGrid();
	
  }
  catch(re)
  {
    alert("GrpDueFeePlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpContGrid()
  {                               
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="生效日期";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="管理机构";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[4]=new Array();
      iArray[4][0]="管理机构名称";         		//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="印刷号";         		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 
      //这些属性必须在loadMulLine前
      GrpContGrid.mulLineCount = 0;   
      GrpContGrid.displayTitle = 1;
      GrpContGrid.locked = 1;
      GrpContGrid.hiddenPlus = 1;
      GrpContGrid.canSel = 1;
      GrpContGrid.canChk = 0;
      GrpContGrid.hiddenSubtraction = 1;
      GrpContGrid.loadMulLine(iArray);  
      GrpContGrid.selBoxEventFuncName = "getPolInfo";
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initGrpPayGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保障计划";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
         
     
	  iArray[2]=new Array();
      iArray[2][0]="约定缴费总金额";		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=60;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="实际已缴总金额";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      

      iArray[4]=new Array();
      iArray[4][0]="最后一次抽档金额";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许


      iArray[5]=new Array();
      iArray[5][0]="约定金额与实际将缴金额差";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            		//列最大值
      iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="GetNoticeNo";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=200;            		//列最大值
      iArray[6][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
   
      
      GrpPayGrid = new MulLineEnter( "fm" , "GrpPayGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPayGrid.mulLineCount =0;   
      GrpPayGrid.displayTitle = 1;
      GrpPayGrid.hiddenPlus = 1;
      GrpPayGrid.hiddenSubtraction = 1;
      GrpPayGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initGrpPayPlanGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保障计划";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
         
     
	  iArray[2]=new Array();
      iArray[2][0]="约定缴费总金额";		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=60;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="实际已缴总金额";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="缴费期次";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
   
      
      GrpPayPlanGrid = new MulLineEnter( "fm" , "GrpPayPlanGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPayPlanGrid.mulLineCount =0;   
      GrpPayPlanGrid.displayTitle = 1;
      GrpPayPlanGrid.hiddenPlus = 1;
      GrpPayPlanGrid.hiddenSubtraction = 1;
      GrpPayPlanGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}



</script>