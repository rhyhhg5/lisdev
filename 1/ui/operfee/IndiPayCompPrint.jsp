<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	XmlExport txmlExport = null;   
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	String tPayNo = request.getParameter("PayNo");
	String tPayDate = request.getParameter("PayDate");		
  String tDueMoney = request.getParameter("DueMoney");
  String tDif = request.getParameter("Dif");
  System.out.println(tPayNo);
  System.out.println(tPayDate);
  System.out.println(tDueMoney);
  System.out.println(tDif);

  LJAPaySchema tLJAPaySchema = new LJAPaySchema();
  tLJAPaySchema.setPayNo(tPayNo);  
  
  LOPRTManagerSchema tLOPRTManagerSchema=new LOPRTManagerSchema();
  tLOPRTManagerSchema.setStandbyFlag1(tPayDate);
  tLOPRTManagerSchema.setStandbyFlag2(tPayNo);
  tLOPRTManagerSchema.setStandbyFlag3(tDueMoney);
  tLOPRTManagerSchema.setStandbyFlag4(tDif);
   
  TransferData tTransferData= new TransferData();	
	VData tVData = new VData();
		
	tTransferData.setNameAndValue("PayDate",tPayDate);
	tTransferData.setNameAndValue("DueMoney",tDueMoney);
	tTransferData.setNameAndValue("Dif",tDif);
  tVData.addElement(tG);
  tVData.addElement(tLJAPaySchema);	
  tVData.addElement(tTransferData);
  tVData.addElement(tLOPRTManagerSchema);	
 
	          
  IndiPayComptPrintBL tIndiPayComptPrintBL = new IndiPayComptPrintBL(); 
  if(!tIndiPayComptPrintBL.submitData(tVData,"PRINT"))
  {
     	operFlag = false;
     	Content = tIndiPayComptPrintBL.mErrors.getErrContent();                
  }
  else
  {    
		  VData mResult = tIndiPayComptPrintBL.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	
	Content = PubFun.changForHTML(Content);
	
ExeSQL tExeSQL = new ExeSQL();
//获取临时文件名
String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
String strFilePath = tExeSQL.getOneValue(strSql);
String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
//获取存放临时文件的路径
//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
//String strRealPath = tExeSQL.getOneValue(strSql);
String strRealPath = application.getRealPath("/").replace('\\','/');
String strVFPathName = strRealPath + "//" +strVFFileName;

CombineVts tcombineVts = null;	
if (operFlag==true)
{
	//合并VTS文件
	String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);

	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	tcombineVts.output(dataStream);

	//把dataStream存储到磁盘文件
	//System.out.println("存储文件到"+strVFPathName);
	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	System.out.println("==> Write VTS file to disk ");

	System.out.println("===strVFFileName : "+strVFFileName);
	//本来打算采用get方式来传递文件路径
	response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	
	}	else	
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>