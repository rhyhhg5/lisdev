var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  easyQueryClick();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var strSQL = "select distinct a.SerialNo,a.otherno,d.contno, "
             + "(select appntname from lccont where contno = d.contno), "
             + "(select appntidno from lccont where contno = d.contno), "
             + "(select name from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "(select idno from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "a.sumgetmoney,d.lastgettodate,getUniteCode(d.agentcode),d.insuredno, "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno) then codename('paymode',a.paymode) else '' end), "
//             + "nvl((select count(distinct c.getnoticeno) from ljsgetdraw b ,ljsget c "
//             + "where b.getnoticeno = c.getnoticeno and c.dealstate ='1' and b.contno = d.contno and b.insuredno =d.insuredno ),0), "
             + "(select name from lacom where agentcom = d.agentcom), "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno) then '待给付' else '待给付确认' end), " 
             + "d.appntno,a.paymode,d.handler,d.GetNoticeNo from ljsget a ,ljsgetdraw d "; 
             
  var whereSQL = " where a.getnoticeno = d.getnoticeno "
             + "and d.riskcode = '330501' "
             + "and a.othernotype='20' and a.dealstate = '0' and feefinatype != 'YEI' "
             + "and not exists (select 1 from ljaget where actugetno = a.getnoticeno) "
             + " and d.ManageCom like '"+ fm.ManageCom.value + "%' "
             + getWherePart( 'a.OtherNo ','EdorAcceptNo')
             + getWherePart( 'd.ContNo ','ContNo') 
             + " and d.lastgettodate between '" + tStartDate + "' and '" + tEndDate + "' ";
             
  if(fm.AgentCom.value != "")
  {
    whereSQL += "and d.AgentCom is not null and d.AgentCom like '" + fm.AgentCom.value + "%' ";
  }           
             
  strSQL +=  whereSQL;
  turnPage1.queryModal(strSQL, LjsGetGrid); 
  
  fm.sql.value = whereSQL;
}
function queryClick()
{
  //得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	//应收时间起止期三个月校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>30)
    {
	//	alert("满期日期止期不能大于当前日期后30天!");
	//	return false;
    }
    
  easyQueryClick();
  if(LjsGetGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function getLjsGetDrawdetail()
{
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length <1 )
	{
		return false;	
	}

	var taskNo=LjsGetGrid.getRowColData(tChked[0],2);
	
	var SQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO, " 
	        + "case DutyCode when '000000' then '初始忠诚奖' when '000001' then '外加忠诚奖' end "
	        + "FROM LJSGETDRAW WHERE GETNOTICENO =(SELECT GETNOTICENO FROM LJSGET WHERE OTHERNOTYPE ='20' AND OTHERNO ='"+taskNo+"')";
	turnPage.queryModal(SQL, LjsGetDrawGrid);
}

function dealData()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
  fm.action="ExpirBenefitCancelSubmit.jsp";
  fm.submit();
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

