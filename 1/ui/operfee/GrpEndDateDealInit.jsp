<%
//程序名称：GrpEndDateDealInit.jsp
//程序功能：团单满期处理页面初始化文件
//创建日期：20060901 
//创建人  ：Yangyalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String comCode = tGI.ComCode;
  
  String currentDate = PubFun.getCurrentDate();
  String startDate = PubFun.calDate(currentDate, -3, "M", null);
%>

<SCRIPT language=javascript>

//表单初始化
function initForm()
{
	try
	{
	  initInputBox();
		initGrpEndDateGrid();
		initElementtype();
	}
	catch(re)
	{
		alert("初始化界面错误：" + re.message);
	}
}

//初始化录入框
function initInputBox()
{
  fm.ManageCom.value = "<%=comCode%>";
  fm.UserManageCom.value = "<%=comCode%>";
  fm.StartDate.value = "<%=startDate%>";
  fm.EndDate.value = "<%=currentDate%>";
}

//初始化保单列表
function initGrpEndDateGrid()
{                               
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";         		//列名
    iArray[1][1]="0px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="grpContNo0";
    
    iArray[2]=new Array();
    iArray[2][0]="保单号标记";         		//列名
    iArray[2][1]="0px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    //iArray[2][4]="comcode";
    iArray[2][21]="grpContNoTag";

    iArray[3]=new Array();
    iArray[3][0]="管理机构";         		//列名
    iArray[3][1]="55px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    //iArray[1][4]="comcode";
    iArray[3][21]="manageCom";
    
    iArray[4]=new Array();
    iArray[4][0]="销售部门";         	//列名
    iArray[4][1]="120px";            		//列宽
    iArray[4][2]=500;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="branch";
    
    iArray[5]=new Array();
    iArray[5][0]="业务员";         		//列名
    iArray[5][1]="70px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=2; 
    iArray[5][4]="agentcode"; 
    iArray[5][21]="grpName";

    iArray[6]=new Array();
    iArray[6][0]="投保单位";         		//列名
    iArray[6][1]="100px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;
    iArray[6][21]="appntName";

    iArray[7]=new Array();
    iArray[7][0]="单位性质";    	//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][4]="businesstype"; 
    iArray[7][21]="businessType";

    iArray[8]=new Array();
    iArray[8][0]="保单号";         		//列名
    iArray[8][1]="70px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="grpContNo";

    iArray[9]=new Array();
    iArray[9][0]="期交保费";         		//列名
    iArray[9][1]="50px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21]="prem";
    
    iArray[10]=new Array();
    iArray[10][0]="投保日期";         		//列名
    iArray[10][1]="70px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21]="polApplyDate";

    iArray[11]=new Array();
    iArray[11][0]="责任到期日";         		//列名
    iArray[11][1]="70px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[11][21]="endDate";

    iArray[12]=new Array();
    iArray[12][0]="交至日期";         		//列名
    iArray[12][1]="70px";            		//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[12][21]="payToDate";

    iArray[13]=new Array();
    iArray[13][0]="交费合计";         		//列名
    iArray[13][1]="50px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=0; 
    iArray[13][21]="sumPrem";
    
    iArray[14]=new Array();
    iArray[14][0]="退费合计";         	//列名
    iArray[14][1]="50px";            		//列宽
    iArray[14][2]=200;            			//列最大值
    iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[14][21]="sumPremTF";
    
    iArray[15]=new Array();
    iArray[15][0]="理赔金合计";         	//列名
    iArray[15][1]="60px";            		//列宽
    iArray[15][2]=200;            			//列最大值
    iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[15][21]="claimMoney";
    
    iArray[16]=new Array();
    iArray[16][0]="被保人总数";         	//列名
    iArray[16][1]="60px";            		//列宽
    iArray[16][2]=200;            			//列最大值
    iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[16][21]="peoples2";

    GrpEndDateGrid = new MulLineEnter( "fm" , "GrpEndDateGrid" ); 
    //这些属性必须在loadMulLine前
    GrpEndDateGrid.mulLineCount = 0;   
    GrpEndDateGrid.displayTitle = 1;
    GrpEndDateGrid.locked = 1;
    GrpEndDateGrid.canSel = 0;
    GrpEndDateGrid.canChk = 1;
    GrpEndDateGrid.hiddenSubtraction = 1;
    GrpEndDateGrid.hiddenPlus = 1;
    GrpEndDateGrid.loadMulLine(iArray);
    
  }
  catch(ex)
  {
    alert("IndiDueFeeInputInit.jsp-->initGrpEndDateGrid函数中发生异常:初始化界面错误!");
    return false;
  }
}
</SCRIPT>