<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： IndiDueFeeInsForPrt.jsp
//程序功能：
//创建日期：2005-12-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
	System.out.println("start");
	CError cError = new CError( );
	boolean operFlag=true;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	String ContNo="";
	
	String GetNoticeNo=request.getParameter("GetNoticeNo");
	LJSPayBDB  tLJSPayBDB=new LJSPayBDB();
	tLJSPayBDB.setGetNoticeNo(GetNoticeNo);
	
	if(tLJSPayBDB.getInfo())
	{
		ContNo=tLJSPayBDB.getOtherNo();
	}else
	{
		Content="没有数据";
	}
	//批单类型编码
	//普通为093 、少儿险为XB001
	String code = "93";
	String sql = "select distinct 'XB001' from ljspaypersonb where getnoticeno = '"
						+GetNoticeNo+"' and contno = '"
						+ContNo+"' and riskcode = '320106'"
						;
	ExeSQL aExeSQL = new ExeSQL();
  String tReturn = aExeSQL.getOneValue(sql);
  if(!"".equals(tReturn))
  {
  	code = tReturn ;
  }
  //判断是否需要在打印管理表中插入数据
  String PrtSeqSql = "select distinct prtseq from LOPRTManager where code = '"+code+"' and standbyflag2='"+GetNoticeNo+"'";
  String PrtSeq = aExeSQL.getOneValue(PrtSeqSql);
  if("".equals(PrtSeq))
  {
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput)session.getValue("GI");
		LCContSchema tLCContSchema = new LCContSchema();
	
		tLCContSchema.setContNo(ContNo);
		System.out.println("tLCContSchema.getContNo():" + tLCContSchema.getContNo());
	
		VData mResult = new VData();
		CErrors mErrors = new CErrors();
	
		VData tVData = new VData();
		tVData.addElement(tLCContSchema);
		tVData.addElement(tG);
		tVData.addElement(tLJSPayBDB);
		if("93".equals(code))
		{
			com.sinosoft.lis.f1print.IndiDueFeePrintBL tIndiDueFeePrintBL = new com.sinosoft.lis.f1print.IndiDueFeePrintBL();
    	if(!tIndiDueFeePrintBL.submitData(tVData,"INSERT"))
   	 	{
				operFlag = false;
     		Content = tIndiDueFeePrintBL.mErrors.getErrContent();                
   		}
  	}else if("XB001".equals(code))
  	{
  		IndiDueFeeChildPrintBL tIndiDueFeeChildPrintBL = new IndiDueFeeChildPrintBL();
  		if(!tIndiDueFeeChildPrintBL.submitData(tVData,"INSERT"))
    	{
				operFlag = false;
     		Content = tIndiDueFeeChildPrintBL.mErrors.getErrContent();                
    	}
  	}
  	PrtSeqSql = "select distinct prtseq from LOPRTManager where code = '"+code+"' and standbyflag2='"+GetNoticeNo+"'";
  	PrtSeq = aExeSQL.getOneValue(PrtSeqSql);
  }
%>
<html>
<script language="javascript">
	if("<%=operFlag%>" == "true")
	{	
		parent.fraInterface.printPDFNotice("0"+"<%=code%>","<%=GetNoticeNo%>","<%=PrtSeq%>");
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>