<html> 
<%
//程序名称：BatchPayInput.jsp
//程序功能：财务批量付费
//创建日期：2008-07-23 
//创建人  ：djw
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>  
<Script>
var comCode = <%=tG.ComCode%>
</Script>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  
  <SCRIPT src="ContPreserveInfoInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContPreserveInfoInit.jsp"%>
</head>
<body  onload="initForm();initElementtype()" >
<Form method=post action="ContPreserveInfoPrint.jsp" name=fm target="fraSubmit">

	<Table>
    	<TR>
        	<TD class=common>
	           
    		</TD>
    		<TD class= titleImg>
    			 查询条件
    		</TD>
    	</TR>
    </Table> 
   
    <table class= common>
	    <tr class= common> 
	         <TD  class= title>所属机构</TD>
	         <TD  class= input><Input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,1,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,1,1);"><input class=codename name=ManageComName readonly=true >
	    </tr>
	    <tr  class= common>
        <td class= title>统计时间起期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      	<td class= title>统计时间止期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>
	    </tr>
  	</Table>  
      <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();">  
      <INPUT VALUE="打  印" class= cssbutton TYPE=button onclick="easyPrint()">
    <Table>
    	<TR>
        	<td class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContPreserve);">
    		</td>
    		<td class= titleImg>
    			 保单留存信息
    		</td>
    	</TR>
    </Table>  
  <input type=hidden name=ComCode>  	
 <Div  id= "divContPreserve" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <td text-align: left colSpan=1>
            <span id="spanContPreserveGrid" ></span> 
  	    </td>
      </TR>
    </Table>					
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>	
<%--       <INPUT VALUE="留  存" class= cssbutton TYPE=button onclick="doRemain()">--%>
  <input type="hidden" name="strsql">
  <input type="hidden" name=StatisticsStartDate>
  <input type="hidden" name="StatisticsEndDate">
</Form>    
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 