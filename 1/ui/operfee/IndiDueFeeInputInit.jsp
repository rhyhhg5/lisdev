<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWAutoInit.jsp
//程序功能：个人自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('ContNo').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AppntNo').value = '';
    fm.all('ManageCom').value = <%=tGI.ManageCom%>;
    var sql = "select Current Date - 2 months, Current Date + 60 days from dual "; 
    var rs = easyExecSql(sql);
    fm.all('StartDate').value = rs[0][0];
    fm.all('EndDate').value = rs[0][1];
    showAllCodeName();
  }
  catch(ex)
  {
    alert("IndiDueFeeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                    

function initForm()
{
  try
  {
    initInpBox(); 
    initJisPayGrid();   
    initContGrid();  
	  initPolGrid(); 
	  
  }
  catch(re)
  {
    alert("IndiDueFeeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 催收记录的初始化
function initJisPayGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";         		//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="生成时间";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="催收性质";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[4]=new Array();
      iArray[4][0]="操作人";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="操作时间";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="保单号";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="应收记录号";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

	    JisPayGrid = new MulLineEnter( "fm" , "JisPayGrid" ); 
      //这些属性必须在loadMulLine前
      JisPayGrid.mulLineCount =0;   
      JisPayGrid.displayTitle = 1;
      JisPayGrid.hiddenPlus = 1;
      JisPayGrid.hiddenSubtraction = 1;
      //JisPayGrid.locked = 1;
//      JisPayGrid.canSel = 1;
      JisPayGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("IndiDueFeeInputInit.jsp-->initJisPayGrid函数中发生异常:初始化界面错误!");
      }
}

// 保单信息列表的初始化
function initContGrid()
  {                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="印刷号";         		//列名
      iArray[2][1]="0px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="投保人";         		//列名
      iArray[3][1]="70px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="特别约定有无";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[5]=new Array();
      iArray[5][0]="生效时间";         		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="已交保费合计";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="余额";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="应交保费";         		//列名
      iArray[8][1]="50px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[9]=new Array();
      iArray[9][0]="应收时间";         		//列名
      iArray[9][1]="70px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[10]=new Array();
      iArray[10][0]="收费方式";         		//列名
      iArray[10][1]="50px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[11]=new Array();
      iArray[11][0]="管理机构";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0; 
      
	    iArray[12]=new Array();
      iArray[12][0]="代理人编码";         		//列名
      iArray[12][1]="70px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[13]=new Array();
      iArray[13][0]="应收记录号";         		//列名
      iArray[13][1]="80px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
      //这些属性必须在loadMulLine前
      ContGrid.mulLineCount = 0;   
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 1;
      ContGrid.canChk = 0;
	    ContGrid.hiddenSubtraction = 1;
	    ContGrid.hiddenPlus = 1;
      ContGrid.loadMulLine(iArray);  
      ContGrid. selBoxEventFuncName = "easyQueryAddClick";
      
    }
    catch(ex)
    {
      alert("IndiDueFeeInputInit.jsp-->initContGrid函数中发生异常:初始化界面错误!");
    }
}
// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="0px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[1]=new Array();
	      iArray[1][0]="险种序号";         		//列名
	      iArray[1][1]="60px";            		//列宽
	      iArray[1][2]=200;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[2]=new Array();
	      iArray[2][0]="被保人姓名";         		//列名
	      iArray[2][1]="100px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
	           
	      iArray[3]=new Array();
	      iArray[3][0]="客户号";         		//列名
	      iArray[3][1]="80px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[4]=new Array();
	      iArray[4][0]="险种名称";         		//列名
	      iArray[4][1]="160px";            		//列宽
	      iArray[4][2]=100;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	     
	      iArray[5]=new Array();
	      iArray[5][0]="险种编码";         		//列名
	      iArray[5][1]="60px";            		//列宽
	      iArray[5][2]=200;            			//列最大值
	      iArray[5][3]=0; 
		  
		    iArray[6]=new Array();
	      iArray[6][0]="缴费频次";         		    //列名
	      iArray[6][1]="60px";            		//列宽
	      iArray[6][2]=100;            			//列最大值
	      iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许 
		 
		    iArray[7]=new Array();
	      iArray[7][0]="交至日期";         		//列名
	      iArray[7][1]="70px";            		//列宽
	      iArray[7][2]=100;            			//列最大值
	      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[8]=new Array();
	      iArray[8][0]="保费";         		//列名
	      iArray[8][1]="50px";            		//列宽
	      iArray[8][2]=100;            			//列最大值
	      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许       
	                  			
	      iArray[9]=new Array();
	      iArray[9][0]="生效日期";         		//列名
	      iArray[9][1]="80px";            		//列宽
	      iArray[9][2]=100;            			//列最大值
	      iArray[9][3]=0;         			//是否允许输入,1表示允许，0表示不允许 
	
		    iArray[10]=new Array();
	      iArray[10][0]="终交时间";         		//列名
	      iArray[10][1]="80px";            		//列宽
	      iArray[10][2]=100;            			//列最大值
	      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	
	      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
	      //这些属性必须在loadMulLine前
	      PolGrid.mulLineCount = 0;   
	      PolGrid.displayTitle = 1;
	      PolGrid.locked = 1;
	      PolGrid.canSel = 0;
	      PolGrid.canChk = 0;
		    PolGrid.hiddenSubtraction = 1;
		    PolGrid.hiddenPlus = 1;
	      PolGrid.loadMulLine(iArray);  
	      
	    }
      catch(ex)
      {
        alert("IndiDueFeeInputInit.jsp-->initPolGrid函数中发生异常:初始化界面错误!");
      }
}

</script>