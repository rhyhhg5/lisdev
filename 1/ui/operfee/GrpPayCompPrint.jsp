<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	XmlExport txmlExport = null;   
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	String tPayNo = request.getParameter("PayNo");	
  
  System.out.println(tPayNo);

  LJAPaySchema tLJAPaySchema = new LJAPaySchema();
  tLJAPaySchema.setPayNo(tPayNo); 
    
  TransferData tTransferData= new TransferData();	
	VData tVData = new VData();
  tVData.addElement(tG);
  tVData.addElement(tLJAPaySchema);	
	          
  GrpPayComptPrintBL tGrpPayComptPrintBL = new GrpPayComptPrintBL(); 
  if(!tGrpPayComptPrintBL.submitData(tVData,"PRINT"))
  {
     	operFlag = false;
     	Content = tGrpPayComptPrintBL.mErrors.getErrContent();                
  }
  else
  {    
		  VData mResult = tGrpPayComptPrintBL.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	ExeSQL tExeSQL = new ExeSQL();
//获取临时文件名
String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
String strFilePath = tExeSQL.getOneValue(strSql);
String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
//获取存放临时文件的路径
//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
//String strRealPath = tExeSQL.getOneValue(strSql);
String strRealPath = application.getRealPath("/").replace('\\','/');
String strVFPathName = strRealPath + "//" +strVFFileName;

CombineVts tcombineVts = null;	
if (operFlag==true)
{
	//合并VTS文件
	String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);

	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	tcombineVts.output(dataStream);

	//把dataStream存储到磁盘文件
	//System.out.println("存储文件到"+strVFPathName);
	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	System.out.println("==> Write VTS file to disk ");

	System.out.println("===strVFFileName : "+strVFFileName);
	//本来打算采用get方式来传递文件路径
	response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	
	}	else	
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>