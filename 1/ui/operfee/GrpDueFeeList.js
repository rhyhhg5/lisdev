 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  if(beforeSubmit())
  {
   var i = 0;
   var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   GrpDueQueryGrid.clearData("GrpDueQueryGrid");
   getPolInfo();
   
   showInfo.close();
   }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    //var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
    //if(!verifyInput()) return false;
    return true;
}           

function returnParent()
{
    top.close();

}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord(strRecord)
{

  //保存查询结果字符串
  turnPage.strQueryResult  = strRecord;

//alert(strRecord);
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray = new Array(5,0,37,43,19,18,38,11);

  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpDueQueryGrid;             
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
	
}

//被选中后显示的集体险种应该是符合条件的。++
function getPolInfo()
{
	initGrpDueFeeListGrid();
    var strSQL = "select a.contplancode ,count(c.InsuredNo) ,(select riskname  from lmrisk where riskcode=b.riskcode) , b.riskcode , "
			+ "	b.PayIntv , sum(b.prem) , b.CValiDate ,b.PaytoDate, b.EndDate "  
			+ "  from lccontplan a,lcpol b,lcinsured c, LCCont d where 1=1"
			+ "  and b.insuredno=c.insuredno "
			+ "  and b.contno=c.contno "
			+ "  and a.contplancode=c.contplancode "
			+ "	 and a.grpcontno=c.grpcontno"
			+ "  and a.grpcontno=b.grpcontno"
			+ "  and b.grpcontno=c.grpcontno"
			+ "  and c.Grpcontno=d.Grpcontno "
			+ "  and c.insuredno=d.insuredno "
			+ "  and b.PayIntv>0   and d.appflag='1' "
			+ "  and b.PaytoDate>='"+fm.all('StartDate').value+"'  and b.PaytoDate<b.PayEndDate "
			+ "  and b.GrpPolNo not in (select GrpPolNo from LJSPayGrp)"
			+ "  and b.RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
			+ getWherePart( 'b.riskcode', 'RiskCode')
			+ getWherePart( 'b.PayIntv', 'Payintv')
			+ getWherePart( 'a.grpcontno', 'GrpPolNo')
			+ "  group by a.contplancode,b.riskcode,b.PayIntv,b.CValiDate,b.PaytoDate,b.EndDate" 
			;

			 turnPage.queryModal(strSQL, GrpDueQueryGrid); 
}