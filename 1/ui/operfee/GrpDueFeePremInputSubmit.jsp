<%
//程序名称：GrpDueFeePremInpuSubmit.jsp
//程序功能：
//创建日期：2006-7-12
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.lang.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>  
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>  
  
<%@page contentType="text/html;charset=GBK" %> 

<%
  String flag = "";
  String content = "";
  String grpContNo = request.getParameter("grpContNo");
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  
  String contPlanCode[]= request.getParameterValues("LCContPlan2");
  String prem[]= request.getParameterValues("LCContPlan5");
  String peoples2Input[]= request.getParameterValues("LCContPlan6");
  String tChk[] = request.getParameterValues("InpLCContPlanChk");
  
  LCContPlanSet tLCContPlanSet = new LCContPlanSet();
  TransferData tTransferData = new TransferData();  //记录每个保障计划录入的保费
  
  for (int i=0;i<tChk.length;i++)
  {
    LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
    tLCContPlanSchema.setGrpContNo(grpContNo);
    tLCContPlanSchema.setContPlanCode(contPlanCode[i]);
    tLCContPlanSchema.setPeoples2(peoples2Input[i]);
    tLCContPlanSet.add(tLCContPlanSchema);
    
    tTransferData.setNameAndValue(contPlanCode[i], prem[i]);
  }
  
  VData d = new VData();
  d.add(tLCContPlanSet);
  d.add(tTransferData);
  d.add(tGI);
  GrpDueFeePremInpuUI ui = new GrpDueFeePremInpuUI();
  if(!ui.submitData(d, ""))
  {
    flag = "Fail";
    content = ui.mErrors.getErrContent();
    content = PubFun.changForHTML(content);
  }
  else
  {
    flag = "Succ";
    content = "保存保费和交费人数成功";
  }
  
%>                                              
<html>
<body>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</body>
</html>

