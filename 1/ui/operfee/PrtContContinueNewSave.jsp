<%@page contentType="text/html;charset=GBK" %>
<%@page pageEncoding="GBK"%>
<%request.setCharacterEncoding("GBK");%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/Download.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%
//程序名称：PrtContContinueNewSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "继续率汇总_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String tManageCom = request.getParameter("ManageCom");
    String tContType = request.getParameter("ContType");
    String tOrphans = request.getParameter("Orphans");
    String tSaleChnl = request.getParameter("SaleChnl");
    String tContinueType = request.getParameter("ContinueType");    
    String tStartDate = request.getParameter("StartDate");
    String tEndDate = request.getParameter("EndDate");
    String tRiskType = "0";
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("StartDate", tStartDate);
    tTransferData.setNameAndValue("EndDate", tEndDate);
    tTransferData.setNameAndValue("ManageCom", tManageCom);
    tTransferData.setNameAndValue("ContType", tContType);
    tTransferData.setNameAndValue("Orphans", tOrphans);
    tTransferData.setNameAndValue("SaleChnl", tSaleChnl);
    tTransferData.setNameAndValue("ContinueType", tContinueType);
    tTransferData.setNameAndValue("RiskType", tRiskType);
    
    
    VData tData = new VData();
    tData.add(tG);
    tData.add(tTransferData);
    
    PrtContContinueNewBL tbl = new PrtContContinueNewBL();
    CreateExcelList tCreateExcelList=new CreateExcelList();
    tCreateExcelList=tbl.getsubmitData(tData,tRiskType);
	if(tCreateExcelList==null)
	{
   		errorFlag=true;
    	System.out.println("EXCEL生成失败！");
    }
    else
    {
    	errorFlag=false;
    }
    if(!errorFlag)
    {
        //写文件到磁盘
        try
        {
            tCreateExcelList.write(tOutXmlPath);
        }
        catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    }
    System.out.println(errorFlag);
       //返回客户端
    if(!errorFlag)
    {
    	downLoadFile(response,filePath,downLoadFileName);
    }
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
    }
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
