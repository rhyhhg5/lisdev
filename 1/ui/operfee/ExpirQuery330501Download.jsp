<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ExpirQuery33501Download.jsp
//程序功能：常B给付追踪
//创建日期：2010-07-27
//创建人  ：About Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%
  
    System.out.println("in ExpirQuery33501Download.jsp …………");
    
    String mManageCom = request.getParameter("ManageCom");
    String mQueryDate = request.getParameter("QueryDate");
    String mStartQueryDate = request.getParameter("StartQueryDate");
    
    System.out.println("ManageCom="+mManageCom+",QueryDate="+mQueryDate);
    
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "常B给付追踪_"+ CommonBL.decodeDate(PubFun.getCurrentDate())+ mManageCom +tG.Operator + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    StringBuffer sql = new StringBuffer(256);
    sql.append("select w.name 分公司,	 ")
       .append("db2inst1.varchar(db2inst1.nvl(x.x1,0)) 本日应付满期金额, ")
       .append("db2inst1.nvl(x.x2,0) 本日应付件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(x.x3,0)) 本月应付满期金额, ")
       .append("db2inst1.nvl(x.x4,0) 本月应付件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(x.x5,0)) 本年应付满期金额, ")
       .append("db2inst1.nvl(x.x6,0) 本年应付件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(y.y1,0)) 本日给付确认满期金额, ")
       .append("db2inst1.nvl(y.y2,0) 本日给付确认件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(y.y3,0)) 本月给付确认满期金额, ")
       .append("db2inst1.nvl(y.y4,0) 本月给付确认件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(y.y5,0)) 本年给付确认满期金额, ")
       .append("db2inst1.nvl(y.y6,0) 本年给付确认件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(z.z1,0)) 本日给付完成满期金额, ")
       .append("db2inst1.nvl(z.z2,0) 本日给付完成件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(z.z3,0)) 本月给付完成满期金额, ")
       .append("db2inst1.nvl(z.z4,0) 本月给付完成件数, ")
       .append("db2inst1.varchar(db2inst1.nvl(z.z5,0)) 本年给付完成满期金额, ")
       .append("db2inst1.nvl(z.z6,0) 本年给付完成件数            ")
       .append("from  ")
       .append("(select comcode,name from ldcom where length(trim(comcode)) = 4 and substr(trim(comcode),1,4) <> '8600' order by comcode) as w  ")
       .append("left join  ")
       .append("(select substr(b.managecom,1,4) comcode,  ")
       .append("sum(case when b.gettodate between '"+mStartQueryDate+"' and '"+mQueryDate+"' then ")
       .append("(case when b.managecom = '86950000' then ")
       .append("((case when a.agentcom like 'PY002%' and a.agentcom is not null  then d.Moneyd else d.Moneycn end)+a.amnt) ")
       .append("when b.managecom = '86210600' then (d.Moneycy+a.amnt) ")
       .append("when b.managecom = '86310000' then (d.Moneycy+a.amnt) ")
       .append("when b.managecom = '86211100' then (d.Moneye+a.amnt) ")
       .append("else (d.Moneycn+a.amnt) end)")
       .append(" else 0 end) x1, ")//本日应付满期金额
       .append("sum(case when b.gettodate between '"+mStartQueryDate+"' and '"+mQueryDate+"' then 1 else 0 end) x2, ") //本日应付件数
       .append("sum(case when b.gettodate between  ")
       .append("date('"+mQueryDate+"')-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then ")
       .append("(case when b.managecom = '86950000' then ")
       .append("((case when a.agentcom like 'PY002%' and a.agentcom is not null  then d.Moneyd else d.Moneycn end)+a.amnt) ")
       .append("when b.managecom = '86210600' then (d.Moneycy+a.amnt) ")
       .append("when b.managecom = '86310000' then (d.Moneycy+a.amnt) ")
       .append("when b.managecom = '86211100' then (d.Moneye+a.amnt) ")
       .append("else (d.Moneycn+a.amnt) end)")
       .append(" else 0 end) x3, ") //本月应付满期金额
       .append("sum(case when b.gettodate between  ")
       .append("date('"+mQueryDate+"')-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then 1 else 0 end) x4, ") //本月应付件数
       .append("sum(case when b.gettodate between  ")
       .append("date('"+mQueryDate+"')-(month('"+mQueryDate+"')-1) months-(day('"+mQueryDate+"')-1) days ")
       .append("and '"+mQueryDate+"' then ")
       .append("(case when b.managecom = '86950000' then ")
       .append("((case when a.agentcom like 'PY002%' and a.agentcom is not null  then d.Moneyd else d.Moneycn end)+a.amnt) ")
       .append("when b.managecom = '86210600' then (d.Moneycy+a.amnt) ")
       .append("when b.managecom = '86310000' then (d.Moneycy+a.amnt) ")
       .append("when b.managecom = '86211100' then (d.Moneye+a.amnt) ")
       .append("else (d.Moneycn+a.amnt) end)")
       .append(" else 0 end) x5, ") //本年应付满期金额
       .append("sum(case when b.gettodate between  ")
       .append("date('"+mQueryDate+"')-(month('"+mQueryDate+"')-1) months-(day('"+mQueryDate+"')-1) days ")
       .append("and '"+mQueryDate+"' then 1 else 0 end) x6 ") //本年应付件数
       .append("from lcpol a,lcget b , lmdutygetalive c ,tempbonus_330501 d ")
       .append("where a.conttype ='1' and a.appflag ='1'  and a.contno = d.contno ")
       .append("and a.stateflag in ('1','3')   ")
       .append("and a.polno = b.polno  ")
       .append("and b.getdutycode = c.getdutycode  ")
       .append("and c.discntflag !='9' ")
       .append("and c.getdutykind ='0' ")
       .append("and b.dutycode in (select dutycode from lmriskduty where riskcode ='330501')  ")
       .append("and b.gettodate between  ")
       .append("date('"+mStartQueryDate+"') - 1 year and '"+mQueryDate+"' ")
       .append("and b.managecom like '"+mManageCom+"%'  ")
       .append("group by substr(b.managecom,1,4) ) as x  ")
       .append("on w.comcode = x.comcode ")
       .append("left join ")
       .append("(select substr(a.managecom,1,4) comcode,  ")
       .append("sum(case when b.makedate between '"+mStartQueryDate+"' and '"+mQueryDate+"' then b.sumgetmoney else 0 end) y1, ") //本日给付确认满期金额
       .append("sum(case when b.makedate between '"+mStartQueryDate+"' and '"+mQueryDate+"' then 1 else 0 end) y2, ")  //本日给付确认件数
       .append("sum(case when b.makedate between  ")
       .append("date('"+mQueryDate+"')-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then b.sumgetmoney else 0 end) y3, ") //本月给付确认满期金额
       .append("sum(case when b.makedate between  ")
       .append("date('"+mQueryDate+"')-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then 1 else 0 end) y4, ") //本月给付确认件数
       .append("sum(case when b.makedate between  ")
       .append("date('"+mQueryDate+"')-(month('"+mQueryDate+"')-1) months-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then b.sumgetmoney else 0 end) y5, ") //本年给付确认满期金额
       .append("sum(case when b.makedate between  ")
       .append("date('"+mQueryDate+"')-(month('"+mQueryDate+"')-1) months-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then 1 else 0 end) y6 ") //本年给付确认件数
       .append("from ljsgetdraw a ,ljaget b  ")
       .append("where a.getnoticeno = b.actugetno  ")
       .append("and a.dutycode in (select dutycode from lmriskduty where riskcode ='330501') ")
       .append("and feefinatype != 'YEI' ")
       .append("and b.makedate between  ")
       .append("date('"+mStartQueryDate+"') - 1 year and '"+mQueryDate+"' ")
       .append("and a.ManageCom like '"+mManageCom+"%' ")
       .append("and exists (select 1 from ljaget where actugetno = a.getnoticeno) ")
       .append("group by substr(a.managecom,1,4)) as y  ")
       .append("on w.comcode = y.comcode ")
       .append("left join  ")
       .append("(select substr(a.managecom,1,4) comcode, ")
       .append("sum(case when b.confdate between '"+mStartQueryDate+"' and '"+mQueryDate+"' then b.sumgetmoney else 0 end) z1, ") //本日给付完成满期金额
       .append("sum(case when b.confdate between '"+mStartQueryDate+"' and '"+mQueryDate+"' then 1 else 0 end) z2, ") //本日给付完成件数
       .append("sum(case when b.confdate between  ")
       .append("date('"+mQueryDate+"')-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then b.sumgetmoney else 0 end) z3, ") //本月给付完成满期金额
       .append("sum(case when b.confdate between  ")
       .append("date('"+mQueryDate+"')-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then 1 else 0 end) z4, ") //本月给付完成件数
       .append("sum(case when b.confdate between  ")
       .append("date('"+mQueryDate+"')-(month('"+mQueryDate+"')-1) months-(day('"+mQueryDate+"')-1) days  ")
       .append("and '"+mQueryDate+"' then b.sumgetmoney else 0 end) z5, ") //本年给付完成满期金额
       .append("sum(case when b.confdate between  ")
       .append("date('"+mQueryDate+"')-(month('"+mQueryDate+"')-1) months-(day('"+mQueryDate+"')-1) days ")
       .append("and '"+mQueryDate+"' then 1 else 0 end) z6 ") //本年给付完成件数
       .append("from ljsgetdraw a ,ljaget b  ")
       .append("where a.getnoticeno = b.actugetno  ")
       .append("and a.dutycode in (select dutycode from lmriskduty where riskcode ='330501') ")
       .append("and feefinatype != 'YEI' ")
       .append("and b.confdate between  ")
       .append("date('"+mStartQueryDate+"') - 1 year and '"+mQueryDate+"' ")
       .append("and a.ManageCom like '"+mManageCom+"%' ")
       .append("and exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate is not null) ")
       .append("group by substr(a.managecom,1,4)) as z ")
       .append("on w.comcode = z.comcode where w.comcode like '"+mManageCom+"%' ")
       .append("with ur");
    
	System.out.println("打印查询:"+sql.toString());
	
    //设置表头
    String[][] tTitle = {{"分公司",
                          "本日应付满期金额","本日应付件数","本月应付满期金额","本月应付件数","本年应付满期金额","本年应付件数",
                          "本日给付确认满期金额","本日给付确认件数","本月给付确认满期金额","本月给付确认件数","本年给付确认满期金额","本年给付确认件数",
                          "本日给付完成满期金额","本日给付完成件数","本月给付完成满期金额","本月给付完成件数","本年给付完成满期金额","本年给付完成件数"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(sql.toString(),displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>