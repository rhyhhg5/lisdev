//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var globalExdata;
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var mSQL = "";

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  }
  resetForm();
}

//重置按钮对应操作,重新查询抽档的相关信息
function resetForm()
{
  try
  {
  	getPolInfo();
  }
  catch(re)
  {
  	alert("在GrpDueFeePlanInput.js-->resetForm函数中发生异常!");
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//清单下载
function downLoad()
{
	try 
	{
		if(!CheckData()){
			return false;
		}
		var strSQL = " select lj.makedate," +
//				     "(select name from ldcom where comcode = b.managecom)," + // 如果需要名字用这个就行
				     " b.managecom, " +
				     "(select name from  LABranchGroup where agentgroup=b.agentgroup)," +
				     " b.GrpContNo,lj.getnoticeno," +
					"(case lj.dealstate when '0' then '抽档未交费'" 
					+" when '4' then '待核销' when '1' then '缴费已完成' when '2' then '应收作废' " 
					+" when '3' then '应收作废'end ),b.phone," +
					"(select distinct postaladdress from lcgrpappnt where grpcontno=b.grpcontno)," +
					"b.CValiDate,(select COALESCE(sum(prem),0) from lcgrppayplan where prtno= b.prtno)," +
					"(select sum(prem) from LCGrpPayActu where prtno=b.prtno " +
					"and getnoticeno=lj.getnoticeno)," +
					"(case lj.dealstate " +
			        " when '4' then (select max(paytodate) from LCGrpPayActuDetail where grpcontno =lj.otherno and state='2' ) " + //LCGrpPayActu
			        " when '0' then (select max(paytodate) from LCGrpPayActuDetail where grpcontno =lj.otherno and state='2' ) " +
			        " when '1' then (select max(paytodate) from LCGrpPayActuDetail where grpcontno =lj.otherno and state='1' ) " +
			        " when '2' then (select max(paytodate) from LCGrpPayActuDetail where grpcontno =lj.otherno and state='3' ) " +
			        " end), " +
					" (select sum(accGetMoney) from lcappacc where customerno = b.appntno)," +
					" (select codename from ldcode where codetype='paymode' and code=b.paymode),db2inst1.getUniteCode(b.agentcode)," +
					"(select name from laagent where agentcode=b.agentcode),lj.paydate," +
					"(select max(makedate) from ljapay where getnoticeno=lj.getnoticeno and incomeno=lj.otherno)"
					+" from LCGrpCont b,ljspayb lj " +
         		" where b.grpcontno=lj.otherno and b.managecom like '"+fm.ManageCom.value+"%'  "
				+ getWherePart( 'b.GrpContNo','GrpContNo' )
//				 + getWherePart( 'b.groupagentcode','AgentCode' )
				 + getWherePart( 'b.PrtNo','PrtNo' )
				 + getWherePart( 'lj.makedate','StartDate','>=' )
				 + getWherePart( 'lj.makedate','EndDate','<=' );
		if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
			strSQL = strSQL+" and b.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
		}
	 	
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.OperateType.value="DOWNLOAD";
	 fm.querySql.value = strSQL;
//	fm.querySql.value = mSQL;
	 fm.action = "GrpDuePlanDownLoadSave.jsp";
	 fm.submit();
	} catch(ex) 
	{
		//showInfo.close( );
		alert(ex);
	}
}

function CheckData()
{
	if(!verifyInput()){
		return false;
	}
//	if((fm.all('GrpContNo').value==null||fm.all('GrpContNo').value=='')&&(fm.all('PrtNo').value==null||fm.all('PrtNo').value=='')){
//		alert("集体保单号或者印刷号不能同时为空!");
//		return false;
//	}
	var sql = "select grpcontno from lcgrpcont a "
			+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') "
			+ getWherePart( 'GrpContNo','GrpContNo' )
			+ getWherePart( 'PrtNo', 'PrtNo')
			+ " and exists (select 1 from lcgrppayplan where prtno=a.prtno) with ur ";
	var result = easyExecSql(sql);
	if(result==null){
		alert("该团单不是约定缴费的团单或该保单已失效,请核对!");
		return false;
	}
	

	return true;
}

// 查询按钮
function easyQueryClick()
{

//	alert("85"+fm.all("StartDate").value+"85");
	// 初始化表格
	initGrpContGrid(); // 团体保单信息 2012-08-14 + by -【OoO?】
	initGrpPolGrid();  // 团体险种信息 2012-08-14 + by -【OoO?】 
	initGrpPlanGrid(); // 约定缴费计划信息 2012-08-14 + by -【OoO?】
	var tGetFlag1="";
	var tGetFlag2="";
	if(!CheckData()){
		return false;
	}
//	getJisInfo(); // 将该功能调整为查询条件，因此去掉此部分的查询 2012-08-14 + by -【OoO?】

    if((fm.all("StartDate").value == null|| fm.all("StartDate").value == '')
       &&(fm.all("EndDate").value == null|| fm.all("EndDate").value == '')
       &&(fm.all("ExStartDate").value == null|| fm.all("ExStartDate").value == '')
       &&(fm.all("ExEndDate").value == null|| fm.all("ExEndDate").value == ''))
       {
       	alert("不允许两组时间都为空，您至少添加一条时间。");
       	return false;
       }
    if(fm.all("GetFlag").value=='Y'){
    	if((fm.all("ExStartDate").value == null|| fm.all("ExStartDate").value == '')
    		       &&(fm.all("ExEndDate").value == null|| fm.all("ExEndDate").value == ''))
    		    {
    				tGetFlag1=" and  exists (select 1 from ljspay where getnoticeno=lj.getnoticeno and otherno=lj.otherno) ";
    				getContInfo(tGetFlag1,tGetFlag2);
    		    }
    		    else {
    		    	tGetFlag1=" and  exists (select 1 from ljspay where getnoticeno=lj.getnoticeno and otherno=lj.grpcontno) ";
    		    	getContInfoEx(tGetFlag1,tGetFlag2);
    		    }
    }
    else if(fm.all("GetFlag").value=='N'){
    	
    	
        if((fm.all("ExStartDate").value == null|| fm.all("ExStartDate").value == '')
        		    &&(fm.all("ExEndDate").value == null|| fm.all("ExEndDate").value == ''))
        		 {
        			tGetFlag1=" and not exists (select 1 from ljspay where getnoticeno=lj.getnoticeno and otherno=lj.otherno) ";
        			getContInfo(tGetFlag1,tGetFlag2);
        		 }
        else {
        			tGetFlag1=" and not exists (select 1 from ljspay where getnoticeno=lj.getnoticeno and otherno=lj.grpcontno) ";
        			tGetFlag2=getNoData();
        			getContInfoEx(tGetFlag1,tGetFlag2);
        		  }
        }
    else{
    	if((fm.all("ExStartDate").value == null|| fm.all("ExStartDate").value == '')
 		       &&(fm.all("ExEndDate").value == null|| fm.all("ExEndDate").value == ''))
 		    {
 				
 				getContInfo(tGetFlag1,tGetFlag2);
 		    }
 		    else {
 		    	tGetFlag2=getNoData();
 		    	getContInfoEx(tGetFlag1,tGetFlag2);
 		    }
 }
    
//	getPolInfo(); // 将该功能调整为触发式查询，因此去掉此部分的查询 2012-08-14 + by -【OoO?】
//	getDuePlanInfo(); // 将该功能调整为触发式查询，因此去掉此部分的查询 2012-08-14 + by -【OoO?】
}

function getNoData(){
	var tManageCom = "";
	if(fm.ManageCom.value!=null&&fm.ManageCom.value!=""){
		tManageCom = " and b.ManageCom like '"+fm.ManageCom.value+"%'";
	}
	var strSQL = " UNION all select b.GrpContNo," +
	"b.PrtNo," +
	"b.GrpName," +
	"''," +
	"b.CValiDate," +
	"(select COALESCE(sum(sumactupaymoney),0) from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC')," +
	"(select (select COALESCE(sum(prem),0) from lcgrppayplan where prtno= b.prtno)-(select COALESCE(sum(sumactupaymoney),0)" +
	" from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC') from dual )," + // 已缴保费合计
	"(select COALESCE(sum(prem),0) from lcgrppayplan where prtno= b.prtno)," +  //余额
	"COALESCE(lj.prem,0)," +  // 约定缴费金额
	//"lj.paytodate ," + 
//	"  (select paytodate from lcpol where grpcontno=b.grpcontno fetch first 1 rows only)," +
	" lj.paytodate  ," + 
	"(select codename from ldcode where codetype='paymode' and code=b.paymode),b.managecom," +
	" db2inst1.getUniteCode(b.AgentCode),'',lj.operator,'','' "
     +" from LCGrpCont b left join lcgrppayplandetail  lj " +
     		" on b.prtno=lj.prtno where 1=1" +
     		" and not exists (select 1 from ljspayb where otherno=b.grpcontno) "+
     		" and not exists (select 1 from ljspay where otherno=b.grpcontno) "+
     		" and b.payintv=-1 " // 之前没有添加payintv=-1这种情况，添加此部分的额外查询 2012-08-14 +  by -【OoO?】
     		+	tManageCom
     + getWherePart( 'b.GrpContNo','GrpContNo' )
     + getWherePart( 'b.appnto','BIGCustomerNo' ) // 添加对客户模糊查询的支持2012-08-14 + by -【OoO?】
	 //+ getWherePart( 'b.ManageCom', 'ManageCom','like')
//	 + getWherePart( 'b.AgentCode','AgentCode' )
	 + getWherePart( 'b.PrtNo','PrtNo' )
	 + getWherePart( 'lj.paytodate','ExStartDate','>=' )
	 + getWherePart( 'lj.paytodate','ExEndDate','<=' );
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strSQL = strSQL+" and b.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	}
	return strSQL;
}

// 数据返回父窗口
function returnParent()
{
	
	if( fm.all("tGrpContNo").value == null || fm.all("tGrpContNo").value == '' )
		alert( "请先查询保单信息。" );
	else
	{
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ fm.all("tGrpContNo").value+"&ContType=1");
		}
		catch(ex)
		{
			alert( "查询出错!" );
		}
	}
}

//查询团单信息
function getContInfo(tGetFlag1,tGetFlag2)
{	
	var tManageCom = "";
	if(fm.ManageCom.value!=null&&fm.ManageCom.value!=""){
		tManageCom = " and b.ManageCom like '"+fm.ManageCom.value+"%'";
	}
	var strSQL = " select b.GrpContNo,b.PrtNo,b.GrpName,'',b.CValiDate," +
			"(select COALESCE(sum(sumactupaymoney),0) from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC')," +
			"(select (select COALESCE(sum(prem),0) from lcgrppayplan where prtno= b.prtno)-(select COALESCE(sum(sumactupaymoney),0) from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC') from dual )," +
			"(select COALESCE(sum(prem),0) from lcgrppayplan where prtno= b.prtno)," +
			"COALESCE(lj.sumduepaymoney,0)," +
			"(case lj.dealstate " +
			" when '4' then (select max(paytodate) from LCGrpPayActuDetail where grpcontno =lj.otherno and state='2' ) " + //LCGrpPayActu
			" when '0' then (select max(paytodate) from LCGrpPayActuDetail where grpcontno =lj.otherno and state='2' ) " +
			" when '1' then (select max(confdate) from ljapay where incomeno =lj.otherno and getnoticeno=lj.getnoticeno ) " +
			" when '2' then (select max(paytodate) from LCGrpPayActuDetail where grpcontno =lj.otherno and state='3' ) " +
			" end), " +
			"(select codename from ldcode where codetype='paymode' and code=b.paymode),b.managecom," +
			" db2inst1.getUniteCode(b.AgentCode),lj.makedate,lj.operator,lj.getnoticeno,(case lj.dealstate when '0' then '抽档未交费'" 
				+" when '4' then '待核销' when '1' then '缴费已完成' when '2' then '应收作废' " 
				+" when '3' then '应收作废'end ) "
	         +" from LCGrpCont b left join ljspayb lj " +
	         		" on b.grpcontno=lj.otherno where 1=1" +
	         		tGetFlag1+
	         		" and b.payintv=-1 " // 之前没有添加payintv=-1这种情况，添加此部分的额外查询 2012-08-14 +  by -【OoO?】
	         		+	tManageCom
	         + getWherePart( 'b.GrpContNo','GrpContNo' )
	         + getWherePart( 'b.appnto','BIGCustomerNo' ) // 添加对客户模糊查询的支持2012-08-14 + by -【OoO?】
			 //+ getWherePart( 'b.ManageCom', 'ManageCom','like')
//			 + getWherePart( 'b.AgentCode','AgentCode' )
			 + getWherePart( 'b.PrtNo','PrtNo' )
			 + getWherePart( 'lj.makedate','StartDate','>=' )
			 + getWherePart( 'lj.makedate','EndDate','<=' )
             + getWherePart( 'lj.dealstate','BIGDealState') // 添加对抽档出的保单的状态的校验 2012-08-14 + by -【OoO?】
             + tGetFlag2;
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strSQL = strSQL+" and b.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	}
//			 +" with ur ";
    turnPage.queryModal(strSQL, GrpContGrid);
 if(GrpContGrid.mulLineCount==0){
		
		alert("未查到保单信息");
		return;
	}
 else
	 fm.sqlPDF.value = strSQL;
}

//查询团单信息 Ex版 // 为了支持查询计划好的续期应收范围内的保单数据， 2012-08-14 +  by -【OoO?】
function getContInfoEx(tGetFlag1,tGetFlag2)
{	
	//1. 先查有没有已经抽档了还没有缴费或者作废的数据， 2012-08-14 +  by -【OoO?】
//	var checkSQL ="select grpcontno from LCGrpPayActu where grpcontno='"+fm.all("GrpContNo").value+"' and state='2' ";
//	var checkSQLresult = easyExecSql(checkSQL);
	//2.如果有，则以这个时间跟界面上输入的时间区间进行比较， 2012-08-14 +  by -【OoO?】
	//3.如果没有，则用现存的最大的那个已缴费的为准
	//4.如果
	var tDealState = "";
	var tManageCom = "";
	if(fm.all("BIGDealState").value == '0'){
		
		fm.all("ChangeState").value = "2";
		tDealState = " and not exists (select 1 from ljtempfee where otherno =lj.grpcontno and getnoticeno=lj.getnoticeno) ";
	}
	else if(fm.all("BIGDealState").value == '1'){
		
		fm.all("ChangeState").value = "1";
	}
	else if(fm.all("BIGDealState").value == '2'){
		
		fm.all("ChangeState").value = "3";
	}
	else{
		fm.all("ChangeState").value = "2";
		tDealState = " and  exists (select 1 from ljtempfee where otherno =lj.grpcontno and getnoticeno=lj.getnoticeno) ";
	}
	if(fm.ManageCom.value!=null&&fm.ManageCom.value!=""){
		tManageCom = " and b.ManageCom like '"+fm.ManageCom.value+"%'";
	}
	
	var strSQL = " select b.GrpContNo," +
			"b.PrtNo," +
			"b.GrpName," +
			"''," +
			"b.CValiDate," +
			"(select COALESCE(sum(sumactupaymoney),0) from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC')," +
			"(select (select COALESCE(sum(prem),0) from lcgrppayplan where prtno= b.prtno)-(select COALESCE(sum(sumactupaymoney),0)" +
			" from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC') from dual )," + // 已缴保费合计
			"(select COALESCE(sum(prem),0) from lcgrppayplan where prtno= b.prtno)," +  //余额
			"COALESCE(lj.prem,0)," +  // 约定缴费金额
			//"lj.paytodate ," + 
//			"  (select paytodate from lcpol where grpcontno=b.grpcontno fetch first 1 rows only)," +
			"(case lj.state when '1' then (select max(confdate) from ljapay where incomeno =lj.grpcontno and getnoticeno=lj.getnoticeno )" +
			" else lj.paytodate end) ," + 
			"(select codename from ldcode where codetype='paymode' and code=b.paymode),b.managecom," +
			" db2inst1.getUniteCode(b.AgentCode),char(lj.paytodate),lj.operator,lj.getnoticeno,(case lj.state when '1' then '已缴费'" 
				+" when '2' then '未缴费或待核销' when '3' then '作废' end ) "
	         +" from LCGrpCont b left join LCGrpPayActu lj " +
	         		" on b.prtno=lj.prtno where 1=1" +
	         		tGetFlag1+
	         		" and b.payintv=-1 " // 之前没有添加payintv=-1这种情况，添加此部分的额外查询 2012-08-14 +  by -【OoO?】
	         		+	tManageCom
	         + getWherePart( 'b.GrpContNo','GrpContNo' )
	         + getWherePart( 'b.appnto','BIGCustomerNo' ) // 添加对客户模糊查询的支持2012-08-14 + by -【OoO?】
			 //+ getWherePart( 'b.ManageCom', 'ManageCom','like')
//	         + getWherePart( 'b.AgentCode','AgentCode' )
			 + getWherePart( 'b.PrtNo','PrtNo' )
			 + getWherePart( 'lj.paytodate','ExStartDate','>=' )
			 + getWherePart( 'lj.paytodate','ExEndDate','<=' )
			 + getWherePart( 'lj.state','ChangeState')
			 + tDealState
			 + tGetFlag2;
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strSQL = strSQL+" and b.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	}
//			 +" with ur ";

    turnPage.queryModal(strSQL, GrpContGrid);
 if(GrpContGrid.mulLineCount==0){
		
		alert("未查到保单信息");
		return;
	}
 else {
	 fm.sqlPDF.value = strSQL;
 	 
 	 //mSQL = strSQL;
 	 
	 
 }
	 
}

//查询收费信息
//function getJisInfo()
//{
//	var strSQL = "select a.managecom,COALESCE((startpaydate)," 
//				+"(select  paytodate from lcpol where grpcontno=a.grpcontno fetch first 1 rows only))," 
//				+"COALESCE((paydate)," 
//				+"(a.cinvalidate + 30 days))," 
//				+" (case (select count(1) from ljspayb where otherno=a.grpcontno) when 0 then '无抽档记录' " 
//				+" else '有抽档记录' end)," 
//				+" b.makedate," 
//				+"b.paydate," 
//				+"(case b.dealstate  when '0' then '抽档未交费'" 
//				+" when '4' then '待核销' when '1' then '缴费已完成' when '2' then '应收作废' " 
//				+" when '3' then '应收作废'end )," 
//				+" grpcontno,a.appntno " 
//				+" from lcgrpcont a left join ljspayb b on a.grpcontno=b.otherno where 1=1 " 
//				+ getWherePart( 'a.GrpContNo','GrpContNo' )
//				+ getWherePart( 'a.ManageCom', 'ManageCom','like')
//				+ getWherePart( 'a.AgentCode','AgentCode' )
//				+ getWherePart( 'a.PrtNo','PrtNo' )
//				+ getWherePart( 'b.makedate','StartDate','>=' )
//				+ getWherePart( 'b.makedate','EndDate','<=' )
//				+" with ur ";
//    turnPage3.queryModal(strSQL, GrpDueFeeGrid);   
//    if(GrpDueFeeGrid.mulLineCount==0){
//		
//		alert("未查到该保单续期数据");
//		return;
//	}
//}

//查询险种信息
function getPolInfo()
{
    globalExdata = GrpContGrid.getRowColDataByName (GrpContGrid.getSelNo()-1,"GrpContNoEx");
    fm.all("MGrpcontno").value=globalExdata;
  
		var strSQL = "select distinct contplancode,(case (select count(1) from lccont where  " +
					"grpcontno=b.GrpContNo and poltype='1') " +
					"when 0 then (select sum(peoples) " +
					"from lccont where grpcontno=b.GrpContNo )" +
					" else (select sum(peoples) from lccont " +
					"where  grpcontno=b.GrpContNo and poltype='1') end)," +
					"(select riskname from lmriskapp where riskcode=a.riskcode)," +
					"b.cvalidate," +
					"(select paytodate from lcpol where grpcontno=b.grpcontno fetch first 1 rows only)," +
					"b.cinvalidate "
		            + " from lcgrppayplandetail a,lcgrpcont b "
		            + " where a.prtno=b.prtno  "
		            + getWherePart( 'b.GrpContNo','MGrpcontno')
					 + getWherePart( 'b.ManageCom', 'ManageCom','like')
//					 + getWherePart( 'b.AgentCode','AgentCode' )
					 + getWherePart( 'b.PrtNo','PrtNo' )
//					+" with ur ";
						if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
							strSQL = strSQL+" and b.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
						}
		turnPage2.queryModal(strSQL, GrpPolGrid);
		if(GrpPolGrid.mulLineCount==0){
			
			alert("未查到保单险种信息");
			return;
		}	
    getDuePlanInfo();
}

//查询约定缴费信息
function getDuePlanInfo()
{
		var strAgent = "" ;
	    if(fm.AgentCode.value != null && fm.AgentCode.value != ""){
		  strAgent = "and b.AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
	     }	
		var strSQL = "select a.contplancode,sum(a.prem),a.paytodate "
		            + " from lcgrppayplan a,lcgrpcont b "
		            + " where a.prtno=b.prtno  "
		            + getWherePart( 'b.GrpContNo','MGrpcontno' )
					+ getWherePart( 'b.ManageCom', 'ManageCom','like')
					//+ getWherePart( 'b.AgentCode','AgentCode' )
					+ strAgent
					+ getWherePart( 'b.PrtNo','PrtNo' )
					+" group by a.contplancode,a.paytodate order by a.paytodate  with ur ";
		turnPage4.queryModal(strSQL, GrpPlanGrid);
		if(GrpPlanGrid.mulLineCount==0){
			
			alert("未约定缴费计划信息");
			return;
		}
		

}

//打印PDF前往打印管理表插入一条数据
function printPDF()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return False;
	}

	var count = 0;
	
		var selno = GrpContGrid.getSelNo();
		if (selno !=0){
	    var str = GrpContGrid.getRowColData(selno-1,1);
		}


//     if(tChked.length != 1)
//	{
//		alert("只能选择一条信息进行查看");
//		return false;	
//	}
	
		fm.action="./GrpDuePlanQueryPDF.jsp";
		fm.submit();
}

function checkPrint()
{
	if(fm.all("GetNoticeNo").value==null||fm.all("GetNoticeNo").value==""){
    alert("没有查询到可打印的催收记录，请先催收！");
    return false;
  }
  return true;
}

//PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}