<%
//程序名称：expirBenefitBatchInput.jsp
//程序功能：满期给付抽档
//创建日期：2007-11-20 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="expirBenefitBatchInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="expirBenefitBatchInputInit.jsp"%>
</head>
<%
	
        GlobalInput tGI = new GlobalInput();
	    tGI = (GlobalInput)session.getValue("GI");
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);               	               	
      
%>
<SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
var operator = '<%=tGI.Operator%>';
/*
var sql = "select char(date('" + CurrentTime + "') + 15 days) from dual ";
var rs = easyExecSql(sql);
if(rs)
{
  CurrentTime = rs[0][0];
}
*/
var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT> 
<body onload="initForm();">

<form name=fm action="./expirBenefitBatchQuery.jsp" target=fraSubmit method=post>

    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付保单查询：
      </td>
     </tr>
    </table>
    <Div  id= "divAlivePayMulti" style= "display: ''">
      <Table  class= common>
      	<tr>
      		<td class= title>满期日起期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      		<td class= title>满期日止期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>
      		<td class= title>操作机构</td>
      		<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename  name=ManageComName readonly></td>
      		</tr>
      	<tr>
      		<td class= title>保单号</td>
      		<td class= input><Input class= common name=ContNo></td>
      		<td class= title>客户号</td>
      		<td class= input><Input class=common name=AppntNo></td>
      		<td class= title>代理人部门</td>
      		<td class= input><Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName></td>
      	</tr>
      	<tr>
      	  <td class= title>代理人</td>
      	  <td class= input><Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this],[0]);" onkeyup="return showCodeListKey('AgentCode',[this],[0]);"></td>
      	  <td class= title>选择类型:</td>
		  <td class= input><input class="codeNo" name="queryType" value="1" CodeData="0|^1|请选择类型^2|普通单^3|少儿单^4|常无忧B" readOnly ondblclick="return showCodeListEx('queryType',[this,queryTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListEx('queryType',[this,queryTypeName],[0,1],null,null,null,1);" readonly><Input class="codeName" name="queryTypeName" readonly></td>
		  <td  class= title>险种</td>
          <td  class= input><Input class= "codeno"  name=RiskCode  ondblclick="return showCodeList('cangetrisk',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('cangetrisk',[this,RiskName],[0,1],null,null,null,1);" ><Input class=codename readonly name=RiskName >
		</tr>
      	<tr  class= common>
      	  <td class="title">销售渠道</td>
		  <td class="input"><input NAME="SaleChnl" VALUE MAXLENGTH="2"  CLASS=codeNo ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" ><input class=codename name=SaleChnlName readonly=true elementtype=nacessary></td> 
      	
        </tr>
      </Table>    
  </Div>     					                                                                            

<!-- 显示或隐藏信息 --> 
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 给付任务清单：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLjsGetGrid" >
  					</span>
  			  	</td>
  			</tr>
  			<tr><td>  					
  				<center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    			</center>
    		</td>
    		</tr>
  			<tr><td class= titleImg>给付明细：</td></tr>			
  			<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLjsGetDrawGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>     
  	</div>
  	  	<table>
  		<tr>
  			<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOptionButton);">
    		</td>
  			<td class= titleImg>操作按钮：</td>
  			</tr>
  		</table>  
  	<div id= "divOptionButton" style= "display: ''">
  	<!-- 显示或隐藏信息 -->

    <INPUT VALUE="查询可给付保单" class = cssbutton TYPE=button onclick="easyQueryClick();">   
    <INPUT VALUE="满期给付批处理" TYPE=button class = cssbutton id="payMulti" onclick="commonPayMulti()">
    <!--<INPUT VALUE="现金给付批处理" TYPE=button class = cssbutton onclick="cashPayMulti()">-->
	  <INPUT VALUE="打印给付通知书" class = cssbutton TYPE=button onclick="printOneNoticeNew();">
	  <INPUT VALUE="批量打印给付通知" class = cssbutton TYPE=button onclick="printAllNotice();"> 
	</div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td> <INPUT VALUE="保单明细查询" class = cssbutton TYPE=button onclick="returnParent();"> </td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndiContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">			
    </center>  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    		险种信息
    		</td>
    	</tr>
    </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanPolGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
    </center>  
  </Div>	 
  <br>
  <div id="ErrorsInfo"></div> 
  <INPUT type= "hidden" name= "QuerySql" value= "">
  <INPUT type= "hidden" name= "payMode" value= "Q">  	 
  <INPUT type= "hidden" name= "taskNo" value= "">
  <INPUT type= "hidden" name= "Today" id = "Today" value= "">
  <input type=hidden id="TempQueryType" name="TempQueryType">	
  <div id="test"></div>                                                                             
</form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>     
      		<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>   	
</body>
</html>
