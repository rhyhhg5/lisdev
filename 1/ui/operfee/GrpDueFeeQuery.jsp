<%
//程序名称：GrpDueFeeQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.*"%>  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>     
<%@page contentType="text/html;charset=GBK" %>
<% 
  //保存保单号  
 // String PrtNo = request.getParameter("PrtNo"); 
  String GrpContNo=request.getParameter("ProposalGrpContNo");//说明团单表的主键是GrpContNo,用它传值到后台处理
  String StartDate=request.getParameter("StartDate");
  String EndDate=request.getParameter("EndDate");//传入后台逻辑处理层做校验
  String ManageCom=request.getParameter("ManageCom");//传入后台逻辑处理层做校验
  String LoadFlag = request.getParameter("loadFlag");
  
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    //集体保单表
    //LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    tLCGrpContSchema.setGrpContNo(GrpContNo);
    //tLCGrpContSchema.setPrtNo(PrtNo);//Yangh于2005-07-19将以前的注销
    TransferData tempTransferData=new TransferData();
    tempTransferData.setNameAndValue("StartDate",StartDate);
    tempTransferData.setNameAndValue("EndDate",EndDate);
	  tempTransferData.setNameAndValue("ManageCom",ManageCom);
	  tempTransferData.setNameAndValue("LoadFlag",LoadFlag);
	  
    VData tVData = new VData(); 
    tVData.add(tLCGrpContSchema);
    tVData.add(tGI);
    tVData.add(tempTransferData);
    //杨红于2005-07-19添加说明：将前台输入的开始日期，终止日期传入后台做相关处理！
    GrpDueFeeUI tGrpDueFeeUI = new GrpDueFeeUI(); 
    tGrpDueFeeUI.submitData(tVData,"INSERT");
    if (!tGrpDueFeeUI.mErrors.needDealError())
     {                          
       Content = " 处理成功";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content =" 失败，原因是:" + tGrpDueFeeUI.mErrors.getFirstError();
       FlagStr = "Fail";
     }       
  }//页面有效区
  
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>

