<html> 
<%
//程序名称：NormPayCollInput.jsp
//程序功能：集体选名单交费
//创建日期：2002-10-08 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
      
  <SCRIPT src="OmnipFinUrgeVerify.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="OmnipFinUrgeVerifyInit.jsp"%>
  <%
      String CurrDate= PubFun.getCurrentDate();   
      String cCurrentYear=StrTool.getVisaYear(CurrDate);
      String cCurrentMonth=StrTool.getVisaMonth(CurrDate);
      String cCurrentDate=StrTool.getVisaDay(CurrDate);             	               	
      
      
      GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<SCRIPT>
var tCurrentYear=<%=cCurrentYear%>;  
var tCurrentMonth=<%=cCurrentMonth%>;  
var tCurrentDate=<%=cCurrentDate%>;
var CurrentTime=tCurrentYear+"-"+tCurrentMonth+"-"+tCurrentDate;

var manageCom = "<%=tGI.ManageCom%>";

</SCRIPT>  
</head>
<body  onload="initForm();" >					                                                                                  
<form action="./NormPayCollSave.jsp" method=post name=fm target="fraSubmit">

    <Div  id= "divNormPayColl" style= "display: ''">
      <table  class= common >             
        <TR class= common> 
		  <TD class=title>
		   批次核销：
		  </TD>
		   <TD class=title>
	           应收日期止期
	      </TD>
		  <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
		  <TD  class= common>
           <INPUT class= cssbutton VALUE="查询"   TYPE=button onclick="queryMultRecord();">        
          </TD>          
        </TR> 
		<TR class= common> 
		  <TD class=title>
		   单张核销：
		  </TD>
		   <TD class=title>
	           保单号
	      </TD>
		  <TD  class= input>
            <input class=common name=GrpContNo>
          </TD>
		  <TD  class= common>
           <INPUT class= cssbutton VALUE="查询"   TYPE=button onclick="querySingleRecord();">        
          </TD>          
        </TR>               
      </table>
    </Div>  
    <Input type=hidden name=NormType> 
    
   <table class= common>
	 <TR  class= common>       
	</TR>   
   </table>      
            
  <Div  id= "divNormGrid1" style= "display: ''">
    <table>
	  <tr>
    	  <td class= titleImg>
    	    查询清单
          </td>
    	</tr>
    </table>
    <table  class= common>
      <tr>
    	  	<td text-align: left colSpan=1>
	         <span id="spanNormPayCollGrid" ></span> 
		   </td>
	</tr>
	<tr>
	</tr>
	<tr class= common>
	  <td class= common>
      <INPUT VALUE="首页"   class= cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class= cssbutton TYPE=button onclick="turnPage.previousPage();"> 			  <INPUT VALUE="下一页" class= cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页"   class= cssbutton TYPE=button onclick="turnPage.lastPage();">	
       </td>
       </tr>
     </table>
      <Tr class=common>  
       <INPUT class= cssbutton VALUE="核销确认" name="verifyButton" id="verifyButton" TYPE=button onclick="verifyChooseRecord();">
	   <INPUT class= cssbutton VALUE="打印对帐单" TYPE=hidden onclick="printPayComp();">
	   <INPUT class= cssbutton VALUE="打印对帐单" TYPE=hidden onclick="printPayCompPDFNew();">
	   <INPUT class= cssbutton VALUE="打印清单" TYPE=button onclick="printList();">
	    
	  </Tr>
  </Div>
  </form>
  <Form name=fmSaveAll action="./OmnipFinSaveAll.jsp" method=post target="fraSubmit">   
		<Input type=hidden name=sql> 
  </Form>  
  <Form name=fmSubmitAll action="./OmnipFinFeeSubmitAll.jsp" method=post target="fraSubmit">   
         <Input type=hidden name=SubmitContNo> 
         <Input type=hidden name=SubmitNotice> 
         <Input type=hidden name=GetNoticeNo>
        
  </Form>
   <Form name=fmPrintAll action="./GrpNormPayListPrint.jsp" method=post target="fraSubmit">   
         <Input type=hidden name=PrintGrpContNo> 
		 <Input type=hidden name=PrintPayDate> 
		 <Input type=hidden name=PrintVerifyType>
		 
  </Form>
  <span id="spanNormPayGrp"  style="display:''; position:absolute; slategray"></span>  
</body>
</html>
