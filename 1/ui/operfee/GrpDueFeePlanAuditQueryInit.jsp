<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {                                   
	// 保单查询条件
   fm.Infomation.value = "保单缴费信息表中“约定缴费总金额”与 “实际已缴总金额”两列显示的金额均不包含保单首期缴费金额";
   fm.all('ManageCom').value = managecom;
   showAllCodeName();
  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    

function initForm()
{
  try
  {
	initInpBox();
	initAuditGrid();
	
	
  }
  catch(re)
  {
    alert("GrpDueFeePlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initAuditGrid()
  {                               
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="生效日";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="管理机构代码";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[4]=new Array();
      iArray[4][0]="管理机构名称";         		//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="约定缴费总金额";         //列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="实际已缴总金额";         //列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="本次抽档收费金额";         //列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="审批状态";         //列名
      iArray[8][1]="70px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="审批日期";         //列名
      iArray[9][1]="70px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      AuditGrid = new MulLineEnter( "fm" , "AuditGrid" ); 
      //这些属性必须在loadMulLine前
      AuditGrid.mulLineCount = 0;   
      AuditGrid.displayTitle = 1;
      AuditGrid.locked = 1;
      AuditGrid.hiddenPlus = 1;
      AuditGrid.hiddenSubtraction = 1;
      AuditGrid.loadMulLine(iArray);  
     
      }
      catch(ex)
      {
        alert(ex);
      }
}






</script>