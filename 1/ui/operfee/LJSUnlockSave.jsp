<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%
//程序名称：LJSUnlockSave.jsp
//程序功能：续期收付费信息
//创建日期：2009-4-10
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String tYWType = "";
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  tYWType = request.getParameter("ywtype");//业务类型，BQPAY保全收费，BQGET保全付费，XQPAY续期收费，XQGET续期付费(满期给付)
  System.out.println("续期收付费信息修改");
  System.out.println("业务类型："+tYWType);
  LJSUnlockUI tLJSUnlockUI = new LJSUnlockUI();
  try
  {
    VData tVData = new VData();
  	TransferData tTransferData = new TransferData();
  	tVData.add(tG);
  	tTransferData.setNameAndValue("YWType",tYWType);
    if("XQPAY".equals(tYWType))
    {
      tTransferData.setNameAndValue("GetNoticeNo",request.getParameter("GetNoticeNoR3"));
      tTransferData.setNameAndValue("PayMode",request.getParameter("PayMode3"));
      tTransferData.setNameAndValue("Drawer",request.getParameter("Drawer3"));
      tTransferData.setNameAndValue("DrawerID",request.getParameter("DrawerID3"));
      tTransferData.setNameAndValue("BankCode",request.getParameter("BankCode3"));
      tTransferData.setNameAndValue("BankAccNo",request.getParameter("BankAccNo3"));
      tTransferData.setNameAndValue("AccName",request.getParameter("AccName3"));
    }
    else if("XQGET".equals(tYWType))
    {
      tTransferData.setNameAndValue("GetNoticeNo",request.getParameter("ActuGetNoR4"));
      tTransferData.setNameAndValue("PayMode",request.getParameter("PayMode4"));
      tTransferData.setNameAndValue("Drawer",request.getParameter("Drawer4"));
      tTransferData.setNameAndValue("DrawerID",request.getParameter("DrawerID4"));
      tTransferData.setNameAndValue("BankCode",request.getParameter("BankCode4"));
      tTransferData.setNameAndValue("BankAccNo",request.getParameter("BankAccNo4"));
      tTransferData.setNameAndValue("AccName",request.getParameter("AccName4"));
    }
  	tVData.add(tTransferData);
    if(!tLJSUnlockUI.submitData(tVData,tYWType))
    {
      Content = "操作失败，原因是:" + tLJSUnlockUI.mErrors.getFirstError();
      System.out.println(Content);
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("续期收付费信息修改完成");
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  { 
    tError = tLJSUnlockUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "数据修改成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=tYWType%>");
</script>
</html>
