<html> 
<%
//程序名称：NormPayGrpChooseInput.jsp
//程序功能：集体选名单交费

//创建日期：2002-10-08    
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  

  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="./NormPayGrpChooseInput.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="NormPayGrpChooseInit.jsp"%>
</head>
<body  onload="initForm();" >					                                                                                  
<form action="./NormPayGrpChooseSave.jsp" method=post name=fm target="fraSubmit">
    <!--<%@include file="../common/jsp/OperateButton.jsp"%>-->
    <!--<%@include file="../common/jsp/InputButton.jsp"%>-->
    <Div  id= "divNormPayGrpChoose" style= "display: ''">
      <table  class= common >                   
        <TR class= common>                      
          <TD  class= title>
            总应交金额
          </TD>                   
          <TD  class= input>
            <Input class= common name=SumDuePayMoney readonly tabindex=-1 >
          </TD>           
          <TD  class= title>
            交至日期
          </TD>
          <TD  class= input>
            <Input class= common name=PaytoDate readonly tabindex=-1 >
          </TD>                                                                
        </TR>
        <TR class= common>                            
          <TD class=title>
	     集体合同号码
	  </TD>
	  <TD class= input>
	      <input class=common name=GrpContNo verify="集体保单号码|NOTNULL">
	  </TD>   
          <TD  class= title>
            交费日期
          </TD>
         <TD  class= input>
            <input class="common" name="PayDate" verify="交费日期|DATE&NOTNULL">                       
          </TD>                                                                             
        </TR>   
        <TR class= common>                            
          <TD class=title>
	     管理费比例
	  </TD>
	  <TD class= input>
	      <input class=common name=ManageFeeRate verify="管理费比例|NUM">
	  </TD>                                                                               
      <TD class=title>
	     投保单位
	  </TD>
	  <TD class= input>
	      <input class=common name=GrpName >
	  </TD> 
        </TR> 
                                        
      </table>
    </Div>  
   <table class= common>
         <TR  class= common>       
          <TD  class= common>
           <INPUT class= cssbutton VALUE="查询"  TYPE=button onclick="queryRecord();">        
          </TD> 	  	              
        </TR>   
   </table>      
      <!--<INPUT VALUE="返回" TYPE=button onclick="returnParent();">--> 					
            
  <Div  id= "divNormGrid1" style= "display: ''">
    <table>
    	  <td class= titleImg>
    	    集体中的个人信息
          </td>
    	</tr>
    </table>
     <table  class= common>
        <tr>
    	  	<td text-align: left colSpan=1>
	         <span id="spanNormPayGrpChooseGrid" ></span> 
		</td>
	</tr>
	<tr>
	</tr>
	<tr class= common>
	  <td class= common>
           <INPUT VALUE="首页"   class= cssbutton TYPE=button onclick="turnPage.firstPage();"> 
           <INPUT VALUE="上一页" class= cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
           <INPUT VALUE="下一页" class= cssbutton TYPE=button onclick="turnPage.nextPage();"> 
           <INPUT VALUE="尾页"   class= cssbutton TYPE=button onclick="turnPage.lastPage();">	
        </td>
       </tr>
       <Tr class=common>  
        <TD class=common>
         <INPUT class= cssbutton VALUE="保存页面上选中个人数据" TYPE=button onclick="submitCurData();">                   
         <INPUT class= cssbutton VALUE="保存团体下所有个人数据" TYPE=button onclick="submitCurDataAll();">                   
        </TD>
       </Tr>              
      </table>
      <Tr class=common>  
         <INPUT class= cssbutton VALUE="核销续期合同" TYPE=button onclick="verifyChooseRecord();">   
 	  </Tr>
         <!--隐藏的号码，用于保存集体保单号用于提交-->      
         <Input type=hidden name=SaveGrpContNo> 
  </Div>
  </form>
  <Form name=fmSaveAll action="./NormPayCollSaveAll.jsp" method=post target="fraSubmit">   
         <Input type=hidden name=SaveAllSubmitGrpContNo>      
  </Form> 
  <Form name=fmSubmitAll action="./NormPayGrpChooseSubmitAll.jsp" method=post target="fraSubmit">   
         <Input type=hidden name=SubmitGrpContNo> 
         <Input type=hidden name=SubmitPayDate> 
         <Input type=hidden name=SubmitManageFeeRate> 
  </Form>
  <span id="spanNormPayGrp"  style="display:''; position:absolute; slategray"></span>  
</body>
</html>
