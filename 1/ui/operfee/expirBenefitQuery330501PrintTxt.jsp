<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "常无忧B满期任务清单_"+tG.Operator+"_"+ min + sec + ".txt";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String tSQL = request.getParameter("QuerySql");
    
    String sql = "select "
                    + "(select Name from ldcom where comcode=a.ManageCom ) 管理机构, "
                    + " contno,(select name from lcinsured where contno = a.contno and insuredno = a.insuredno), " 
                    + " (select codename('idtype',IDType) from lcinsured where contno = a.contno and insuredno = a.insuredno), "
                    + " (select IDNo from lcinsured where contno = a.contno and insuredno = a.insuredno), "
                    + " getnoticeno, "
                     + " min(a.lastgettodate),"
                     + " nvl((select count(distinct c.getnoticeno) from ljsgetdraw b ,ljsget c "
                     + " where b.getnoticeno = c.getnoticeno and c.dealstate ='1' and b.contno = a.contno and b.insuredno =a.insuredno ),0),"
                     + " riskcode 给付险种 ,"
                     + "(select riskname from lmrisk where riskcode=a.riskcode ) 给付险种名称,"
                     + "(select prem from lccont where contno = a.contno) 保费, "
                     + "(select amnt from lcpol where polno = a.polno) 保额, "
                     + "sum(case when dutycode = '000000' then getmoney else 0 end) 初始忠诚奖, "
                     + "sum(case when dutycode = '000001' then getmoney else 0 end) 外加忠诚奖, "
                     + " sum(getmoney)  给付金额,"
                     + " (case when exists (select 1 from LPEdorEspecialData where EdorNo = a.GetNoticeNo and EdorType = 'MJ' "
                     + "and DetailType = 'DrawerCode' and EdorValue = '2') then '是' else '否' end) 是否转入投保人帐户, " 
                     + " (case when not exists (select 1 from ljaget where actugetno = a.getnoticeno) then '待给付确认' else "
                     + "(select case when b.dealstate='0' and c.confdate is null then '待给付' when  c.confdate is not null then '给付完成' "
                     + "when dealstate ='2' then '已撤销' end from ljsget b ,ljaget c " 
                     + "where b.getnoticeno = a.getnoticeno and c.actugetno = a.getnoticeno fetch first 1 row only) end  ), "
                     + " (case when exists (select 1 from loprtmanager where code in ('bq001','MX001','MX002','MX004') and standbyflag2 = a.getnoticeno "
                     + " and printtimes >= 1) then '已打印' else '未打印' end),(select codename('paymode',paymode) from ljsget where getnoticeno = a.getnoticeno ), "
                     + " (select confdate from ljaget where actugetno = a.getnoticeno),(select bankcode from ljsget where getnoticeno = a.getnoticeno), "
                     + " (select bankACCNO from ljsget where getnoticeno = a.getnoticeno),(select ACCNAME from ljsget where getnoticeno = a.getnoticeno),  "
                     + "makedate 抽档日期,"
                     + "(select ( case when c.Phone is null then c.homephone else c.phone end) from lcaddress c where c.CustomerNo=a.AppntNo  and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno)) 投保人联系电话,"
                     + "(select c.Mobile from lcaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno)) 投保人移动电话,"
                     + "(select c.PostalAddress from lcaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno)) 投保人通讯地址,"
                     + "(select c.ZipCode from lcaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno)) 投保人邮编,"
                     + "(select appntname from lcappnt where appntno=a.appntno fetch first 1 row only ) 投保人,"
                     + "(select agentcom from lccont where contno = a.contno) 银行网点代码,"
                     + "(select (select name from lacom where agentcom = lccont.agentcom) from lccont where contno = a.contno ) 银行网点名称,"
                     + "(select payenddate from lcpol where polno=a.polno) 满期日期,"
                     + "AGENTGROUP, (select min(name) from LABranchGroup where agentgroup = a.AGENTGROUP),"
                     + "getUniteCode(AGENTCODE), (select b.Name from laagent b where b.AgentCode=a.AgentCode) 代理人姓名, "
                     + "(select b.Mobile from laagent b where b.AgentCode=a.AgentCode) 代理人手机 "
                     + " from ljsgetdraw a where 1=1 "
                     + " and exists(select 1 from ljsget where a.getnoticeno = getnoticeno and othernotype ='20') and "
                     + tSQL
                     + " group by contno,polno,insuredno,getnoticeno,agentgroup,agentcode, "
                     + "riskcode,"
                     + "makedate,"
                     + "bankcode,"
                     + "polno,"
                     + "appntno,"
                     + "ManageCom "
                     + "with ur"
                     ;
    System.out.println("打印给付清单后台查询："+sql);
    
	//查询数据
    String conent="管理机构               |保单号               |被保人               |被保人证件类型               |被保人证件号码               |给付记录号               |应给付日期               |已给付次数               |给付险种               |给付险种名称               |保费               |保额               |初始忠诚奖               |外加忠诚奖               |给付金额               |是否转入投保人帐户               |任务状态               |通知书打印标志               |给付方式               |实际给付日期               |给付银行               |给付帐号               |帐号所有人               |抽档日期               |投保人联系电话               |投保人移动电话               |投保人通讯地址               |投保人邮编               |投保人               |银行网点代码               |银行网点名称               |满期日期               |营业部代码               |营业部名称               |业务员代码               |业务员姓名               |业务员手机            | \r\n";
    SSRS mssrs1 =new ExeSQL().execSQL(sql); 
    String [][]tData = mssrs1.getAllData();
    if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
    {
       for (int i=0;i<tData.length;i++)
       {
           for (int j=0;j<tData[0].length;j++)
           {
              conent+=tData[i][j]+"             | ";
           }
           conent+="\r\n";  
       }
	    
    }
    BufferedWriter out1 = null;
	try{
		   out1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tOutXmlPath, true)));
		   out1.write(conent);
		} 
    catch (Exception e) 
	{
		   e.printStackTrace();
	} 
	finally 
	{
		 try 
		 {
			 out1.close();
		 }	
		 catch (IOException e) 
		{
			e.printStackTrace();
		} 
    }    

    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>