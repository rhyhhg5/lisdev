var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";

//简单查询
function easyQuery()
{
	var tManageCom = fm.all('ManageCom').value;			//管理机构代码
	var tManageComName = fm.all('ManageComName').value;	//管理机构名称
	var tPolNo = fm.all('PolNo').value;					//保单号
	var tGetNoticeNo = fm.all('GetNoticeNo').value;		//应收记录号
	var tSaleChnl = fm.all('SaleChnl').value;			//保单类型
	var tDealState = fm.all('DealState').value;			//目前状态
	var tStartDate = fm.all('StartDate').value;			//应收时间起期
	var tEndDate = fm.all('EndDate').value;				//应收时间止期

	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("请输入应收时间起期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间起期",tStartDate))
		{
			fm.all('StartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("请输入应收时间止期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间止期",tEndDate)){
			fm.all('EndDate').focus();
			return false;
		}
	}
	//应收时间起止期三个月校验
	var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>92 )
    {
	  alert("应收时间起止日期不能超过3个月！")
	  return false;
    }
	//初始化Grid
	initRableRecGrid();
    //已抽档
    var tSql1 = "select "
	         +  "(select Name from ldcom where comcode=a.ManageCom),"
	         +  "a.ManageCom,"
	         +  "(select Name from labranchgroup where AgentGroup=(select AgentGroup from lccont where ContNo=a.ContNo union select AgentGroup from lbcont where ContNo=a.ContNo)),"
	         +  "a.ContNo,"
	         +  "a.RiskCode,"
	         +  "a.GetNoticeNo,"
	         +  "(select d.AppntName from lccont d where d.ContNo=a.ContNo union select d.AppntName from lbcont d where d.ContNo=a.ContNo),"
	         +  "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Mobile from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Phone from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.PostalAddress from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
			 +  "(select d.CValiDate from lcpol d where d.PolNo=a.PolNo union select d.CValiDate from lbpol d where d.PolNo=a.PolNo),"//生效日
	         +  "sum(a.sumactupaymoney),"
	         +  "a.lastpaytodate,"
	         +  "(select codename('paymode',d.PayMode) from lccont d where d.ContNo=a.ContNo "
	         +  "union select codename('paymode',d.PayMode) from lbcont d where d.ContNo=a.ContNo),"
	         +  "(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)," //业务员
	         +  "(select b.Mobile from laagent b where b.AgentCode=(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)),"
	         +  "(select b.Phone from laagent b where b.AgentCode=(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)),"
	         +  "(select b.Name from laagent b where b.AgentCode=(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)),"
	         +  "(select AgentCom from lccont where ContNo=a.ContNo union select AgentCom from lbcont where ContNo=a.ContNo),"
	         +  "(select Name from lacom where AgentCom=(select AgentCom from lccont where ContNo=a.ContNo union select AgentCom from lbcont where ContNo=a.ContNo)),"
	         +  "char((select paydate from ljspayb where getnoticeno=a.getnoticeno)),"
	         +  "char((select max(enteraccdate) from ljtempfee where tempfeeno=a.getnoticeno)),"
	         +  "char((select max(confdate) from ljapay where getnoticeno=a.getnoticeno)),"
	         +  "(select (select codealias from ldcode1 where codetype='salechnl' and code = d.SaleChnl) from lccont d where d.ContNo=a.ContNo "
	         +  "union select (select codealias from ldcode1 where codetype='salechnl' and code = d.SaleChnl) from lbcont d where d.ContNo=a.ContNo),"
	         +  "(select (case when	d.SaleChnl='04' then '银保' when d.SaleChnl='13' then '银保' when d.SaleChnl='14' then '互动' when d.SaleChnl='15' then '互动' else '个险' end) from lccont d where d.ContNo=a.ContNo "
	         +  "union select (case when	d.SaleChnl='04' then '银保' when d.SaleChnl='13' then '银保' when d.SaleChnl='14' then '互动' when d.SaleChnl='15' then '互动' else '个险' end) from lbcont d where d.ContNo=a.ContNo),"
	         +  "(select codename('dealstate',DealState) from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only), "
	         +  "(select sum(sumduepaymoney) from ljspaypersonb where polno = a.polno and lastpaytodate = a.lastpaytodate group by getnoticeno order by getnoticeno fetch first 1 row only ), " //险种应缴保费
	         +  "nvl((select sum(sumactupaymoney) from ljapayperson where polno = a.polno and lastpaytodate = a.lastpaytodate and getnoticeno = a.getnoticeno group by getnoticeno order by getnoticeno fetch first 1 row only ),0), " //险种实收金额
	         +  "(((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=a.PolNo union select d.CValiDate from lbpol d where d.PolNo=a.PolNo)))*12 "
             +  "+ (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=a.PolNo union select d.CValiDate from lbpol d where d.PolNo=a.PolNo))))/payintv +1 ), " //保单缴次
             +  "(select year(payenddate)-year(cvalidate) from lcpol d where d.polno=a.polno union select year(payenddate)-year(cvalidate) from lbpol d where d.polno=a.polno), " //20110212 新增 险种缴费年限
	         +  "(case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else (select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo) end ), " //原代理人代码
	         +  "(select name from laagent where agentcode = (case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else (select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo) end )), " //原代理人 
	         +  "(select codename('stateflag',stateflag) from lcpol where polno = a.polno union select codename('stateflag',stateflag) from lbpol where polno = a.polno), " //险种状态
	         //保单服务状态”字段：“在职单”、“孤儿单”
	         +  "(case when exists (select 1 from LAAscription where ContNo = a.ContNo and AscripState = '3') then '孤儿单' "  
	         +  "when exists (select 1 from laagent where agentcode = (select AgentCode from lccont where ContNo=a.ContNo "
	         +  "union select AgentCode from lbcont where ContNo=a.ContNo) and agentstate>='06') then '孤儿单' "
	         +  "when (select employdate from laagent where agentcode = (select AgentCode from lccont where ContNo=a.ContNo "
	         +  "union select AgentCode from lbcont where ContNo=a.ContNo))>(select SignDate from lccont where ContNo=a.ContNo "
	         +  "union select SignDate from lbcont where ContNo=a.ContNo) then '孤儿单'  "
	         +  "else '在职单' end ) "
	         
	         +  "from ljspaypersonb a "
	         +  "where "
	         +  "a.riskcode in (select riskcode from lmriskapp where riskperiod = 'L') and "
	         +  "a.sumactupaymoney > 0 and "
	         +  "not exists (select 1 from ljspaypersonb where lastpaytodate = a.lastpaytodate "
	         +  "and polno = a.polno and getnoticeno > a.getnoticeno) and "
	         +  "a.lastpaytodate between '" + tStartDate + "' and '" + tEndDate + "' and "
	         +  "a.managecom like '" + tManageCom + "%' " 
	         +  "and a.riskcode not in (select riskcode from lmriskapp where RiskType4='4') " 
	         +  "and a.riskcode not in (select code from ldcode1 where codetype = 'mainsubriskrela' and code1 in (select riskcode from lmriskapp where risktype4 = '4')) ";
	         
    //未抽档
	var tSql2 = "select "
	         +  "(select Name from ldcom where comcode=a.ManageCom),"
	         +  "a.ManageCom,"
			 +  "(select Name from labranchgroup where AgentGroup=a.AgentGroup),"
			 +  "a.ContNo,"
			 +  "a.RiskCode,"
			 +  "'',"
			 +  "a.AppntName,"
	         +  "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Mobile from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Phone from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.PostalAddress from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
			 +  "a.CValiDate,"
			 +  "a.Prem,"
			 +  "a.PaytoDate,"
			 +  "(select codename('paymode',a.PayMode) from dual),"
			 +  "a.AgentCode,"
			 +  "(select b.Mobile from laagent b where b.AgentCode=a.AgentCode),"
	         +  "(select b.Phone from laagent b where b.AgentCode=a.AgentCode),"
	         +  "(select b.Name from laagent b where b.AgentCode=a.AgentCode),"
			 +  "a.AgentCom,"
			 +  "(select Name from lacom where AgentCom=a.AgentCom),"
			 +  "'',"
			 +  "'',"
			 +  "'',"
			 +  "(select codealias from ldcode1 where codetype='salechnl' and code = a.SaleChnl),"
			 +  "(case when	a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' when a.SaleChnl='14' then '互动' when a.SaleChnl='15' then '互动' else '个险' end),"
			 +  "'未抽档', "
			 +  "a.Prem, " //险种应缴保费
	         +  "0, " //险种实收金额
	         +  "(((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 ), " //保单缴次
	         +  "year(payenddate)-year(cvalidate), " //20110212 新增 险种缴费年限
	         +  "(case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else a.agentcode end ), " //原代理人代码
	         +  "(select name from laagent where agentcode = (case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else a.agentcode end )), " //原代理人 
	         +  "codename('stateflag',stateflag) , " //险种状态
	         //保单服务状态”字段：“在职单”、“孤儿单” 
	         +  "(case when exists (select 1 from LAAscription where ContNo = a.ContNo and AscripState = '3') then '孤儿单' "  
	         +  "when exists (select 1 from laagent where agentcode = a.AgentCode and agentstate>='06') then '孤儿单' "
	         +  "when (select employdate from laagent where agentcode = a.AgentCode)>(select SignDate from lccont where ContNo=a.ContNo ) then '孤儿单'  "
	         +  "else '在职单' end ) "
	         
			 +  "from lcpol a "
			 +  "where "
			 +  "not exists (select 1 from ljspaypersonb where polno=a.polno and lastpaytodate=a.paytodate) and "
			 +  "a.conttype='1' and "
			 +  "a.stateflag='1' and "
	         +  "a.riskcode in (select riskcode from lmriskapp where riskperiod = 'L') and "
	         +  "a.prem <> 0 and "
			 +  "a.paytodate between '" + tStartDate + "' and '" + tEndDate + "' and "
			 +  "a.paytodate<a.payenddate and "
	         +  "a.managecom like '" + tManageCom + "%' "
	         +  "and a.riskcode not in (select riskcode from lmriskapp where RiskType4='4') " 
	         +  "and a.riskcode not in (select code from ldcode1 where codetype = 'mainsubriskrela' and code1 in (select riskcode from lmriskapp where risktype4 = '4')) ";
	         
    if(tSaleChnl == "1")
    {
    	tSql1 += "and not exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13','14','15')) ";
    	tSql1 += "and not exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13','14','15')) ";
    	tSql2 += "and a.salechnl not in ('04','13','14','15') ";
    }
    if(tSaleChnl == "2")
    {
    	tSql1 += "and (exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) or exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')))";
    	tSql2 += "and a.salechnl in ('04','13') ";
    }
    if(tSaleChnl == "3")
    {
    	tSql1 += "and (exists(select 1 from lccont where contno = a.contno and salechnl in ('14','15')) or exists(select 1 from lbcont where contno = a.contno and salechnl in ('14','15')))";
    	tSql2 += "and a.salechnl in ('14','15') ";
    }
    if(tPolNo != "" && tPolNo !=null)
    {
    	tSql1 += "and a.contno='" + tPolNo + "' ";
    	tSql2 += "and a.contno='" + tPolNo + "' ";
    }
    if(tGetNoticeNo != "" && tGetNoticeNo != null)
    {
    	tSql1 += "and a.getnoticeno = '" + tGetNoticeNo + "' ";
    	tSql2 += "and 1 = 2 ";
    }
    
    var groupSql = " group by a.ManageCom, a.ContNo, a.RiskCode, a.PolNo, a.GetNoticeNo, a.lastpaytodate, a.payintv  ";

    switch(tDealState)
    {
    	case "0":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='0' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "1":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='1' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "2":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='2' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "3":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='3' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "4":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='4' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "5":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='5' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "6":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='6' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "7":
			mSql = tSql2;
    		break;
    	default:
    	    tSql1 += groupSql;
    		mSql = tSql1 + " union all " +tSql2;
    		break;
    }
	turnPage1.queryModal(mSql, RableRecGrid);  
	if( RableRecGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	showCodeName(); 
}
function easyPrint()
{	
	if( RableRecGrid.mulLineCount == 0)
	{
		alert("没有需要打印的信息！");
		return false;
	}
	//传递页面信息
	fm.all('strsql').value=mSql;
	fm.submit();
}
//日期格式校验
function checkDateFormat(tName,strValue) {
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}