 <html> 
<%
//程序名称：GrpDueFeeInput.jsp
//程序功能：集体保费催收，实现数据从保费项表到应收集体表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpDueFeeBatchInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpDueFeeBatchInit.jsp"%>
</head>
<%
	
        GlobalInput tGI = new GlobalInput();
	    tGI = (GlobalInput)session.getValue("GI");
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);
        String loadFlag = request.getParameter("loadFlag");                	               	
      //  String MaxManageCom=PubFun.RCh(tGI.ManageCom,"9",8);
      //  String MinManageCom=PubFun.RCh(tGI.ManageCom,"0",8);  	
%>
<SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;
var Operator='<%=tGI.Operator%>'; 
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
var loadFlag = "<%=loadFlag%>";

var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT> 
<body onload="initForm();">

<form name=fm action=./GrpDueFeeMultiQuery.jsp target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->  
    <INPUT VALUE="查询可催收的团体保单" class = cssbutton TYPE=button onclick="easyQueryClick();">   
    <INPUT VALUE="生成催收记录" TYPE=button class = cssbutton name="multDue"  onclick="GrpMulti()">   
	<INPUT VALUE="打印PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManage();"> 
	<INPUT VALUE="批打PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManageBat();">
	<INPUT VALUE="当前线程查询" class = cssbutton TYPE=button onclick="queryProject();"> 
  <%@include file="ComplexProject.jsp"%>
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpmulti);">
      </td>
      <td class= titleImg>
        集体批处理催收
      </td>
     </tr>
    </table>
    <Div  id= "divGrpmulti" style= "display: ''">
      <Table  class= common>
        <tr>
      <TD  class= title> 管理机构</TD><TD  class= input>
      <Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
      <TD  class= title>
          应收日期范围：
          </TD>
		  <TD  class= title>
          开始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
          结束日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>           
		    </tr>   
      </Table>    
    </Div>     					                                                                            

<!-- 显示或隐藏信息 --> 
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 催收记录：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpJisPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
   </center>  	
  	</div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 团体保单信息
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td> <INPUT VALUE="团体保单明细" class = cssbutton TYPE=button onclick="returnParent();"> </td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
    </center>  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    			 团体险种信息
    		</td>
    	</tr>
    </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanGrpPolGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
    </center>  
  </Div>	  	
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpec);">
    		</td>
    		<td class= titleImg> 特别缴费约定</td>  			
    	</tr>
    </table>
    <Div  id= "divSpec" style= "display: "> 
      	<table  class= two>
       		<tr class=common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </DIV>   		
  <Input type=hidden name=GetNoticeNo>
  <Input type=hidden name=sqlPDF>
  <Input type=hidden name=loadFlag> 
</form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    <iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>        	
</body>
</html>
