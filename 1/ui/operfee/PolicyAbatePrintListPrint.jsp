<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PolicyAbatePrintListPrint.jsp
//程序功能：
//创建日期：2009-6-1 15:51:57
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    

    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "保单失效清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String subsql = request.getParameter("sql");
    
  //String strSQL =" select '1',a.makedate,b.manageCom,b.agentGroup,b.contNo,b.appntName "
	//			 +" ,(select mobile from LCAddress where  customerNo = b.appntNo and addressNo = d.addressNo) "
	//			 +" ,(select postalAddress from LCAddress where customerNo = b.appntNo and addressNo = d.addressNo) "
	//			 +" ,(select getnoticeno from ljspayb where otherno = b.contno and getnoticeno=standbyFlag3) ,b.prem "
	//			 +" ,varchar(b.payToDate) "
	//			 +" ,(select payDate from ljspayb where otherno = b.contno and getnoticeno=standbyFlag3) "
	//			 +" ,(select codeName from LDCode where codeType = 'paymode' and code = b.payMode), a.prtSeq,a.standbyFlag1 "
	//			 +" ,(select codeName from LDCode where codeType = 'pausecode' and code = a.code) "
	//			 +" , b.agentCode,(select mobile from LAAGent where agentCode = b.agentCode) "
	//			 +" ,(select name from LAAGent where agentCode = b.agentCode),a.makedate "
	//			 +" from LOPRTManager a, LCCont b, LCAppnt d "
	//			 +" where a.otherNo = b.ContNo "
	//			 +" and b.contNo = d.contNo and a.otherno = d.contno "
	//			 +" and a.MakeDate >='"+request.getParameter("StartDate")+"'"
	//			 +" and a.MakeDate <='"+request.getParameter("EndDate")+"'"
	//			 +" AND a.Code = '42' "
	//			 +" group by b.agentGroup, b.agentCode,b.manageCom,b.contNo,b.appntno,b.appntName,d.addressNo,b.payToDate, a.prtSeq,a.standbyFlag1,a.makedate,standbyFlag3,paymode,code,b.prem "
	//			 ;
    //设置表头
    String[][] tTitle = {{"失效时间", "管理机构", "营销部门", "保单号",
                         "投保人", "投保人电话", "投保人联系地址","应收记录号", "待收保费",
                         "待收时间 ","失缴费截止日期", "缴费方式",
                         "失效/暂停通知书号","保单失效/暂停日","类型","代理人编码"
                         ,"代理人电话","代理人姓名"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(subsql,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>