 <%
//程序名称：TempFeeQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 查询条件
    fm.all('GrpPolNo').value = '';
    fm.all('RiskCode').value = '';
    fm.all('StartDate').value = '2005-01-01';
    fm.all('Payintv').value = '';  
  }
  catch(ex)
  {
    alert("在GrpDueFeeListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在GrpDueFeeListInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();  
    initGrpDueFeeListGrid();
  }
  catch(re)
  {
    alert("在GrpDueFeeListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

 var GrpDueQueryGrid ;
 
// 保单信息列表的初始化
function initGrpDueFeeListGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保障计划";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[1][18]=300;
      
      
      iArray[2]=new Array();
      iArray[2][0]="参保人数";   		//列名
      iArray[2][1]="90px";            		//列宽
      iArray[2][2]=100;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，

	  iArray[3]=new Array();
      iArray[3][0]="险种名称";		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="险种代码";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

	  iArray[5]=new Array();
      iArray[5][0]="缴费频次";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="期交金额";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="生效日期";         		//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="交至日期";         		//列名
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="满期日期";         		//列名
      iArray[9][1]="90px";            		//列宽
      iArray[9][2]=200;            	        //列最大值
      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      
      GrpDueQueryGrid = new MulLineEnter( "fm" , "GrpDueQueryGrid" ); 
      //这些属性必须在loadMulLine前
      GrpDueQueryGrid.mulLineCount = 12;   
      GrpDueQueryGrid.displayTitle = 1;
      GrpDueQueryGrid.hiddenPlus = 1;
      GrpDueQueryGrid.hiddenSubtraction = 1;
//      GrpDueQueryGrid.locked = 1;
//      GrpDueQueryGrid.canSel = 1;
      GrpDueQueryGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>