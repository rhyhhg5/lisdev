//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var showInfo;
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var queryCondition = "";
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    resetForm();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {      
	  //afterContQuery();
	  //queryCont();
	  afterJisPayQuery();	  
  }
  catch(re)
  {
  	alert("在IndiDueFeeInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

 
//提交前的校验、计算  
function beforeSubmit()
{
  var arrReturn = new Array();
	var tSel = ContGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击催收按钮。" );
		return false;
	}	
	else
	{
		var tRow = ContGrid.getSelNo() - 1;	        
    var tContNo=ContGrid.getRowColData(tRow,1);  
    
    //注释原因，这样在催收结束再次查询需催收保单信息时查询条件可能与原来不相符
    //fm.all('ContNo').value = tContNo;  //在这里再次给页面input置值，以防用户选择一条记录后，修改了input里面的值
    
    fm.all('ContNoSelected').value = tContNo;
  }  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//校验时间
function CheckDate()
{
	if(fm.all('StartDate').value==null||fm.all('StartDate').value==""
	   ||fm.all('EndDate').value==null||fm.all('EndDate').value=="")
	{
	  alert("必须录入起止日期");
	  return false;	
	}
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	}
//生成催收记录
function PersonSingle()
{	
    if(beforeSubmit())
    { 
    	if(!CheckDate()) return false;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
      fm.submit();
    }	
}

function easyQueryAddClick()
{
	var arrReturn = new Array();
	var tSel = ContGrid.getSelNo();	
	var queryCondition5
	if(fm.queryType.value=='2')
	{
		queryCondition5 = " and a.riskcode not in('320106','120706') " ;
	}
	if(fm.queryType.value=='3')
	{
		queryCondition5 = " and a.riskcode in('320106','120706') " ;
	}
	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}	
	else
	{
		var tRow = ContGrid.getSelNo() - 1;	        
    var tContNo=ContGrid.getRowColData(tRow,1);      
		var tGetNotice = ContGrid.getRowColData(tRow,13);
		if (tGetNotice =='' || tGetNotice ==null)
		{
			var strSQL = "select a.RiskSeqNo,a.InsuredName,a.appntNo, (select riskname  from lmrisk where riskcode=a.riskcode) ,a.RiskCode, (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), "
			+ "	a.PaytoDate,a.prem, a.CValiDate , date(a.PayEndDate) - 1 day "  
			+ "  from lcpol a where 1=1"
			+ "  and a.contno='" + tContNo + "' "
			+ "  and a.appflag='1' and (a.StateFlag is null or a.StateFlag = '1')"
			+ "  and a.PaytoDate>='"+contCurrentTime+"' and a.PaytoDate<='"+contSubTime+"' "
			+ "  and a.managecom like '"+managecom+"%%'"
			+ "  and a.PolNo not in (select PolNo from LJSPayPerson)"
			+ "  and a.RiskCode  in (select RiskCode from LMRisk where (CPayFlag='Y' or RnewFlag != 'N') and lmrisk.riskcode=a.riskcode)"
			+ queryCondition5			
			;
		}else
	  {
           var strSQL = "select a.RiskSeqNo,a.InsuredName,a.appntNo, (select riskname  from lmrisk where riskcode=a.riskcode) ,a.RiskCode, (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), "
			+ "	 a.PaytoDate,a.prem, a.CValiDate , date(a.PayEndDate) - 1 day "  
			+ "  from lcpol a where 1=1"
			+ "  and a.contno='" + tContNo + "' "
			+ "  and a.PayIntv>0   and a.appflag='1' and (a.StateFlag is null or a.StateFlag = '1')"
			+ "  and a.PaytoDate>='"+contCurrentTime+"' and a.PaytoDate<='"+contSubTime+"' and a.PaytoDate<a.EndDate "
			+ "  and a.managecom like '"+managecom+"%%'"
			+ "  and a.PolNo in (select PolNo from LJSPayPerson)"
			+ "  and a.RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y' and lmriskpay.riskcode=a.riskcode)"			
			+ queryCondition5
			;
			
		}
		strSQL = strSQL + " order by a.RiskSeqNo ";
    turnPage2.queryModal(strSQL, PolGrid); 
   } 	
}

// 查询保单信息
function easyQueryClick(){
	// 初始化表格
	initContGrid();	
	initJisPayGrid();   
	initPolGrid(); 
	
	//杨红于2005-07-16添加开始，默认催收的是介于当天及之后30天范围的保单
	//新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	
	if(fm.all("StartDate").value!="")
	{		
		 CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		 SubTime=fm.all("EndDate").value;
	}	
	
	if( (fm.ContNo.value == "" || fm.ContNo.value == null) && (fm.AppntNo.value == "" || fm.AppntNo.value == null))
	{
		alert("客户号或保单号不能为空！");
		return false ;
	}
	//add by fuxin 2009-5-21 对抽档增加60天的限制。
	var getDate ="select current date from dual";
	var result = easyExecSql(getDate);
	if(result != "null" && result != "" && result != null)
	{
		var t1 = new Date(result[0][0].replace(/-/g,"\/")).getTime();
		var tEndDate = new Date(fm.EndDate.value.replace(/-/g,"\/")).getTime();
	  var leaveDate = (t1 - tEndDate)/(24*60*60*1000) ;
//	  if(Math.abs(leaveDate)>= 61 )
//	  {	
//		  alert("查询条件中[终止日期]不能超过系统当前时间60天！");
//		  return false ;
//	  }
	}
	
			//得到所选保单类型
		if(fm.queryType.value == "")
		{
		  alert("请录入保单类型。");
		  return false;
		}
		else
		{
			if(fm.queryType.value=='1')
			{
				alert("请录入保单类型。");
				return false ;
			}
			if(fm.queryType.value=='2')
			{
				queryCondition = " lcpol.riskcode   in (select riskcode from lmrisk where riskcode not in ('320106','120706')) and " ;
			}
			if(fm.queryType.value=='3')
			{
				queryCondition = " lcpol.riskcode  in ('320106','120706') and " ;
			}
	}
	queryCont();
	initPolGrid();
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	
	//若不注释的话，查询后会起止时间设置为当天 和 当天 + 30天
	//CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	//SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime
}

//查询需要催收的保单  qulq 2007-1-11  添加保单状态控制
function queryCont()
{
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  var strSQL = "select ContNo,PrtNo,AppntName,'',CValiDate,SumPrem,nvl((select AccGetMoney from LCAppAcc where CustomerNo=lccont.appntno),0),prem,paytodate,(select codename from ldcode where codetype='paymode' and code=PayMode),ShowManageName(ManageCom),getUniteCode(AgentCode)"                                                                                                                                                                                                                                                                               
     +" from LCCont where AppFlag='1' and (StateFlag is null or StateFlag = '1') "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
//     +" and ContNo not in (select otherno from ljspay where othernotype='2')"  
     +" and (cardflag in ('0','9','8','c','d') or (cardflag in ('1','2','3','5','6') and salechnl in ('01','04','08','10','13')) or cardflag is null "
     +"       or (cardflag != '0' and payintv = 1) or (cardflag != '0' and exists(select 1 from lcpol where contno = lccont.contno and riskcode ='330307'))" +
     		" or (cardflag ='2'  and exists (select 1 from lcpol where contno = lccont.contno and riskcode in ('5201','1206','121101','510901','520401','122001','520601','232101','334301','290201'))"+
     		//简易平台续期续保配置到套餐 
     		"      			or exists (select 1 from lcpol where contno=lccont.contno and  riskcode in (select riskcode from ldriskwrap where riskwrapcode in(select wrapcode from ldwrapxq where state='1'))))"+
//			简易平台百万安行险种续期，20130624，wdl
			" or (cardflag='6' and exists (select 1 from lcpol where contno=lccont.contno and riskcode in ('333501','532401','532501'))) ) "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
     +" and ContNo in (select ContNo from LCPol where "    
     + queryCondition    
     //续期                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
     +" ( paytodate<payenddate "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
     +"   and payintv>0 "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
     +"   and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode) "                                                                                                                                                                                                                                                                                                                                                                                                                                                           
     + "  or  "      
     //续保                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
     +"   exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode)"                                                                                                                                                                                                                                                                                                                                                                                                                                                                
     //2008-8-13少儿险停售后仍可续保
     //2010-2-26所有险种停售后不可卖新单但仍可续保 cbs00035250
     +"   and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2') " //and (endDate > LCPol.endDate or endDate is null or riskcode in ('320106','120706')))"
    // +"   and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2' and (endDate > LCPol.endDate or endDate is null ))"
     +"   and not exists(select contNo from LCRnewStateLog where polno = lcpol.polno and state in('4','5')) "
     +" )"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
     + " and contno=lccont.contno"
     //2015-5-28 保证续保的产品可能被理赔二核置为了续保终止状态，先放开，在BL里再处理
     +" and (polstate is null or (polstate is not null and polstate not like '02%%' and polstate not like '03%%' or (polstate='03070001' and exists (select code from ldcode where codetype='bzXB' and code=LCPol.riskcode)) ))"                                                                                                                                                                                                                                                                                                                                                                                                                                                  
     +" and PolNo not in (select PolNo from LJSPayPerson where contNo = LCCont.contNo)"                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
     +" and (StopFlag='0' or StopFlag is null)"	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
  	 +" and grppolno = '00000000000000000000' and stateflag = '1' "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
  	 +" and managecom like '"+managecom+"%%'"
  	 +" and paytodate>='" + CurrentTime + "' and paytodate<='" + SubTime +"')"	
  	 //20090825 zhanggm 过滤万能险
  	 +" and not exists (select 1 from lcpol c where c.contno = lccont.contno and exists (select 1 from lmriskapp where riskcode =c.riskcode and riskcode not in ('332301','334801','340501','340601') and risktype4= '4'))"
  	 +" and not exists  (select 1 from lcpol where contno=lccont.contno and  riskcode  in (select code from ldcode where codetype='ybkriskcode') and (prtno like 'YWX%' or prtno like 'YBK%')) "
  	 + getWherePart( 'ContNo','ContNo' )                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
//		 + getWherePart( 'AgentCode','AgentCode' )   
  	 + strAgent
  	 + getWherePart( 'AppntNo','AppntNo' ) 
  	 + "order by contNo "
  	 +" with ur";

	  turnPage.queryModal(strSQL, ContGrid,0,1);
	  
	  if(ContGrid.mulLineCount == 0)
	  {
	    alert("没有符合条件的可催收保单信息");
	  }
}

//催收完成后查询保单
function afterContQuery()
{
		// 初始化表格
	initContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	   
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	 var strSQL = "select b.otherno,a.PrtNo,a.AppntName,'',a.CValiDate,a.SumPrem,(select AccGetMoney from LCAppAcc where CustomerNo=a.appntno union select 0 from dual),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),getUniteCode(a.AgentCode),b.GetNoticeNo "
	 + " from LCCont  a, ljspay b, ljspayperson c where  "
	 + " a.AppFlag='1' and (a.StateFlag is null or a.StateFlag = '1') and b.OtherNo=a.ContNo and  b.othernotype='2' and c.GetNoticeNo=b.GetNoticeNo and c.ContNo=b.otherno and  b.MakeDate='" + tempCurrentTime
	 + "'  and a.ContNo in(select ContNo from LCPol "
	 + " where (PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime+"' and PaytoDate<PayEndDate )"
	 + " and managecom like '"+managecom+"%%'"
	 + " and PayIntv>0 and AppFlag='1' and (StateFlag is null or StateFlag = '1')"
	 + " and PolNo in (select PolNo from LJSPayPerson)"
	 + " and RiskCode  in (select RiskCode from LMRisk where CPayFlag='Y'))"
	 + getWherePart( 'a.ContNo','ContNo' )
	 + getWherePart( 'a.ManageCom', 'ManageCom','like')
//	 + getWherePart( 'a.AgentCode','AgentCode' )
	 + strAgent
	 + getWherePart( 'a.AppntNo','AppntNo' )
	 + " group by b.otherno,a.PrtNo,a.AppntName,a.CValiDate,a.SumPrem,a.appntno,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode"
	 //+ " order by b.GetNoticeNo"
	 + " union "
	 + " select b.otherno,a.PrtNo,a.AppntName,'',a.CValiDate,a.SumPrem,(select AccGetMoney from LCAppAcc where CustomerNo=a.appntno),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),getUniteCode(a.AgentCode),b.GetNoticeNo "
	 + " from LCCont  a, ljspay b, ljspayperson c where  "
	 + " ( a.ContNo =(select newcontno from LCRnewStateLog where contno=b.otherNo))   and  b.othernotype='2' and c.GetNoticeNo=b.GetNoticeNo  and  b.MakeDate='" + tempCurrentTime
	 + "' and exists(select RiskCode from LMRisk where ( RNewFlAg='Y') and riskcode=c.RiskCode )    "
	 + " AND a.ContNo=c.ContNo"
	 + " and a.ContNo in(select ContNo from LCPol "
	 + " where (PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime+"'  )"
	 + " and managecom like '"+managecom+"%%'"
	 
	 + getWherePart( 'b.OtherNo','ContNo' )
	 + getWherePart( 'a.ManageCom', 'ManageCom','like')
//	 + getWherePart( 'a.AgentCode','AgentCode' )
	 + strAgent
	 + getWherePart( 'a.AppntNo','AppntNo' )
	 + " group by b.otherno,a.PrtNo,a.AppntName,a.CValiDate,a.SumPrem,a.appntno,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode"
	 //+ " order by b.GetNoticeNo"
	 
	 ; 
	 // var strSQL = "select a.ContNo,a.PrtNo,a.AppntName,'',a.CValiDate,a.SumPrem,(select AccGetMoney from LCAppAcc where CustomerNo=a.appntno),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),a.AgentCode,b.GetNoticeNo "
	 //+ " from LCCont  a, ljspay b, ljspayperson c where  "
	 //+ " a.AppFlag='1' and b.OtherNo=a.ContNo and  b.othernotype='2' and c.GetNoticeNo=b.GetNoticeNo and c.ContNo=b.otherno and  b.MakeDate='" + tempCurrentTime
	 //+ "'  and a.ContNo in(select ContNo from LCPol "
	 //+ " where (PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime+"' and PaytoDate<PayEndDate )"
	 //+ " and managecom like '"+managecom+"%%'"
	 //+ " and PayIntv>0 and AppFlag='1'"
	 //+ " and PolNo in (select PolNo from LJSPayPerson)"
	 //+ " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y'))"
	 //+ getWherePart( 'a.ContNo','ContNo' )
	 //+ getWherePart( 'a.ManageCom', 'ManageCom','like')
	 //+ getWherePart( 'a.AgentCode','AgentCode' )
	 //+ getWherePart( 'a.AppntNo','AppntNo' )
	 //+ " group by a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.SumPrem,a.appntno,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode"
	 //+ " order by b.GetNoticeNo"
	 ;
	turnPage.queryModal(strSQL, ContGrid); 
	initPolGrid();  	
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	
}


//催收完成后查询催收纪录
function afterJisPayQuery()
{
	// 初始化表格
	initJisPayGrid(); 
	initContGrid();  
	initPolGrid(); 
	
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}		  

  //var strSQL ="	select distinct b.SerialNo,b.MakeDate,'',b.Operator,b.MakeDate, otherNo,getNoticeNo from  ljspay b where b.makedate='"+tempCurrentTime+"' and b.othernotype='2' and  b.MakeTime in (select max(MakeTime) from ljspay where makedate='"+tempCurrentTime+"' and othernotype='2')";
	var strSQL = "	select distinct b.SerialNo,b.MakeDate,'', "
	             + "    b.Operator,b.MakeDate, otherNo, getNoticeNo "
	             + "from  ljspay b "
	             + "where otherNo = '" + fm.ContNoSelected.value + "' "
	             + "    and b.othernotype = '2' ";
	
	turnPage4.queryModal(strSQL, JisPayGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
}

//催收完成后打印通知书
function printNotice()
{
	//该为从JisPayGrid中得到应收号
	if(JisPayGrid.mulLineCount == 0)
	{
	  alert("没有催收记录");
	  return false;
	}
	var tGetNoticeNo = JisPayGrid.getRowColData(0, 7);
	if(tGetNoticeNo == "")
	{
	  alert("没有得到应收记录号");
	  return false;
	}
	window.open("../operfee/IndiPrintSel.jsp?GetNoticeNo="+ tGetNoticeNo);
}
//打印PDF前往打印管理表插入一条数据
function printInsManage()
{
	//该为从JisPayGrid中得到应收号
	if(JisPayGrid.mulLineCount == 0)
	{
	  alert("没有催收记录");
	  return false;
	}
	var tGetNoticeNo=JisPayGrid.getRowColData(0, 7);
	/*
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '93' and standbyflag2='"+tGetNoticeNo+"'");

	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
	*/
		fm.all('GetNoticeNo').value = tGetNoticeNo;
		fm.action="./IndiDueFeeInsForPrt_v2.jsp";
		fm.submit();
	/*	
	}
	else
	{
		//window.open("../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
		fm.action = "../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
		fm.submit();
	}
	*/
}
function printPDF()
{
	if(JisPayGrid.mulLineCount == 0)
	{
	  alert("没有催收记录");
	  return false;
	}
	var tGetNoticeNo=JisPayGrid.getRowColData(0, 7);
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '93' and standbyflag2='"+tGetNoticeNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		alert("提取数据失败-->PrtSeq为空");
		return false;
	}
	fm.action="../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
	fm.submit();
}
function printPDFNotice(aCode,aGetNoticeNo,aPrtSeq)
{
		if(aCode=="093")
		{
			fm.action="../uw/PrintPDFSave.jsp?Code="+aCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+aGetNoticeNo+"&PrtSeq="+aPrtSeq;
			fm.submit();
		}else
		{
			fm.action="../uw/PrintPDFSave.jsp?Code="+aCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+aGetNoticeNo+"&PrtSeq="+aPrtSeq;
			fm.submit();	
			//window.open("../operfee/PrintChildNotice.jsp?GetNoticeNo="+aGetNoticeNo);
		}
}
//打印、下载应收清单
function printCont()
{
    if(!chenkDate()){
        return true ;
    }
  if(ContGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据!");
    return false;
  } 
  var url = "IndiDueFeePrt.jsp?CurrentTime=" + CurrentTime 
          + "&SubTime=" + SubTime + "&ContNo=" + fm.ContNo.value
          + "&ManageCom=" + fm.ManageCom.value + "&AgentCode=" + fm.AgentCode.value 
          + "&AppntNo=" + fm.AppntNo.value ;
  window.open(url);
} 
function newprintInsManage()
{
	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
//
//	var count = 0;
//	var tChked = new Array();
//	for(var i = 0; i < ContGrid.mulLineCount; i++)
//	{
//		if(ContGrid.getChkNo(i) == true)
//		{
//			tChked[count++] = i;
//		}
//	}
//	if(tChked.length != 1)
//	{
//		alert("只能选择一条信息进行查看");
//		return false;	
//	}
	var tSel = ContGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击打印按钮。" );
		return false;
	}	
	else
	{
		var tRow = ContGrid.getSelNo() - 1;	        
    var tContNo=ContGrid.getRowColData(tRow,1);  
	var tGetNoticeNo=ContGrid.getRowColData(tRow,13);
	fm.action = "../uw/PDFPrintSave.jsp?Code=93&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
    fm.submit();
		}
}
////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
