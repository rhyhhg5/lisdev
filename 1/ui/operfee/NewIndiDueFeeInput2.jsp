 <html> 
<%
//程序名称：IndiDueFeeInput.jsp
//程序功能：个人保费催收，实现数据从保费项表到应收个人表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="NewIndiDueFeeInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body >
<form name=fm action=./NewIndiDueFeeQuery.jsp target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->

    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divPersonSingle);">
      </td>
      <td class= titleImg>
       个人新单催收
      </td>
    </tr>
    </table>
    
    <Div  id= "divPersonSingle" style= "display: ''">
      <Table  class= common>
        <TR  class= common>
          <TD  class= title>
            投保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>
          <TD class= input>
          <INPUT VALUE="催收" TYPE=button onclick="PersonSingle()">           
          <TD>        
        </TR>
      </Table>    
    </Div>    
</form>

<form name=fmMulti action=./NewIndiDueFeeMultiQuery.jsp target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->      	
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divPersonmulti);">
      </td>
      <td class= titleImg>
        个人新单批处理催收
      </td>
     </tr>
    </table>
    <Div  id= "divPersonmulti" style= "display: ''">
      <Table  class= common>
        <TR  class= common>
          <TD  class= title>
            催收日期范围
          </TD>
        <TD>
          <INPUT VALUE="催收" TYPE=button onclick="PersonMulti()">           
        </TD>     
        </TR>  
        <TR class= common>
         <TD  class= title>
            起始日期 
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="StartDate" >           
          </TD>
        </TR> 
        <TR class= common>
         <TD  class= title>
          终止日期 
          </TD>        
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="EndDate" >                                 
          </TD>
        </TR>  
      </Table>    
    </Div> 
    					                                                                            
</form>
</body>
</html>
