<html> 
<%
//程序名称：ULIDueFeeInput.jsp
//程序功能：万能续期个案催收
//创建日期：12:34:56 07/08/09 
//创建人  ：zhanggm 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/CurrentTime.jsp"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  String SubDate=tD.getString(AfterDate);  
  String tSubYear=StrTool.getVisaYear(SubDate);
  String tSubMonth=StrTool.getVisaMonth(SubDate);
  String tSubDate=StrTool.getVisaDay(SubDate);
%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
  var managecom = "<%=tGI.ManageCom%>"; //记录登陆机构
  var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
  var CurrentYear=<%=tCurrentYear%>;  
  var CurrentMonth=<%=tCurrentMonth%>;  
  var CurrentDate=<%=tCurrentDate%>;
  var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
  var SubYear=<%=tSubYear%>;  
  var SubMonth=<%=tSubMonth%>;  
  var SubDate=<%=tSubDate%>;
  var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
  var contCurrentTime; //杨红于2005-07-16添加该全局变量，目的是保存 查询保单信息 的起始时间
  var contSubTime;    //杨红于2005-07-16添加该全局变量，目的是保存 查询保单信息 的终止时间
</script>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="ULIDueFeeInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ULIDueFeeInputInit.jsp"%>
</head>
<body  onload="initForm();" >
<form name=fm action="./IndiDueFeeQuery.jsp" target=fraSubmit method=post>
  <INPUT VALUE="查询可催收保单" class = cssbutton TYPE=button onclick="query();"> 
  <INPUT VALUE="生成催收记录" TYPE=button class = cssbutton onclick="PersonSingle();">   
  <INPUT VALUE="打印通知书" class = cssbutton style="width:100px" TYPE=button onclick="printInsManage();">
  <table class= common border=0 width=100%>
    <tr>
	  <td class= titleImg align= center>请输入查询条件：</td>
    </tr>
  </table>
  <table  class= common align=center>
	<tr>
      <td  class= title>开始日期</td>
      <td  class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      <td  class= title>终止日期</td>
      <td  class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>          
	</tr>  
    <tr  class= common>
      <td  class= title>客户号</td>
      <td  class= input><Input class=common name=AppntNo></td>      	
      <td  class= title>保单号</td>
      <td  class= input><Input class= common name=ContNo></td>
    </tr>
    <tr  class= common>
      <td  class= title>管理机构</td>
      <td  class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></td>        
      <td  class= title>代理人编码</td>
      <td  class= input><Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this],[0]);" onkeyup="return showCodeListKey('AgentCode',[this],[0]);"></td>        
    </tr>
  </table>
  <table>
    <tr>
      <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);"></td>
      <td class= titleImg>催收记录</td>
    </tr>
  </table>
  <div  id= "divJisPay" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1><span id="spanJisPayGrid" ></span></td>
  	  </tr>
    </table>
  </div>
  <table>
    <tr>
      <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);"></td>
    	<td class= titleImg>保单信息</td>
    </tr>
  </table>
  <div  id= "divLCCont" style= "display: ''">
    <table  class= common>
      <tr  class= common>
    	<td text-align: left colSpan=1><span id="spanContGrid" ></span></td>
	  </tr>
    </table>
  </div>
  <div id="divPage1" align=center style="display: 'none' ">
	  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
	  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
	  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
	  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </div>				
  <table>
    <tr>
      <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);"></td>
      <td class= titleImg>保单险种信息</td>
    </tr>
  </table>
  <div  id= "divLCPol1" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1><span id="spanPolGrid" ></span></td>
  	  </tr>
    </table>
  </div>
  <div id="divPage2" align=center style="display: 'none' ">
	<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">	
  </div>  	
  	<p>    
    <INPUT type= "hidden" name= "Operator" value= "">     
    <INPUT type= "hidden" name= "ContNoSelected" value= ""> 
    <Input type= "hidden" name="GetNoticeNo"> 
  	</p>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
