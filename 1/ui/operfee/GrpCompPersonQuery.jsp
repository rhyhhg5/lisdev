<%
//程序名称：GrpDueFeeQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.*"%>  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>     
<%@page contentType="text/html;charset=GBK" %>
<% 
  //保存保单号  
  String GrpContNo=request.getParameter("HidGrpContNo");//说明团单表的主键是GrpContNo,用它传值到后台处理
  String BankCode=request.getParameter("HidBankCode");//理赔金转帐银行
  String BankAccNo=request.getParameter("HidBankAccNo");//帐号
  String BankAccName=request.getParameter("HidBankAccName");//户名
  String CValidate=request.getParameter("HidCValidate");//生效日期
  System.out.println(GrpContNo);   
  System.out.println(BankCode);  
  System.out.println(BankAccNo);  
  System.out.println(BankAccName);  
  System.out.println(CValidate);   

// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    //集体保单表   
    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    tLCGrpContSchema.setGrpContNo(GrpContNo);   
    TransferData tempTransferData=new TransferData();
    tempTransferData.setNameAndValue("BankCode",BankCode); 
    tempTransferData.setNameAndValue("BankAccNo",BankAccNo);   
    tempTransferData.setNameAndValue("BankAccName",BankAccName);   
    tempTransferData.setNameAndValue("CValidate",CValidate);     
    VData tVData = new VData(); 
    tVData.add(tLCGrpContSchema);
    tVData.add(tGI);
    tVData.add(tempTransferData);
   
    GrpCompPersonBL tGrpCompPersonBL = new GrpCompPersonBL(); 
    tGrpCompPersonBL.submitData(tVData,"INSERT");
    if (!tGrpCompPersonBL.mErrors.needDealError())
     {                          
       Content = " 处理成功";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content =" 失败，原因是:" + tGrpCompPersonBL.mErrors.getFirstError();
       FlagStr = "Fail";
     }       
  }//页面有效区
  
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>

