//满期处理
function endDateDeal()
{
  if(!checkChildAge())
  {
    return false;
  }
	fm.PayModeNew.verify="";
	if(!verifyInput2())
  {
      return false;
  }
  if(fm.dealType.value == "")
  {
    alert("请录入满期处理操作。");
    return false;
  }
  else
  {
  	
    var temp = fm.action;
    fm.action = "PChildInsurMJSubmit.jsp?GetNoticeNo=" + fm.GetNoticeNo.value 
      + "&DealType=" + fm.dealType.value;
    showSavaInfoWindow();
    fm.submit();
    fm.action = temp;

  }
}

//打印续期终止结算单
function printEndDateList(prtType)
{
	if('XBDeal'==prtType) 
		{
	  	window.open("../f1print/PrtChildXBNotice.jsp?GetNoticeNo=" + fm.GetNoticeNo.value);
    }
    else
    {
	    window.open("../f1print/PrtChildMJNotice.jsp?GetNoticeNo=" + fm.GetNoticeNo.value);
    }
}

function buttonControl(targetObject,flag)
{
	document.getElementById(targetObject).style.display=flag;
}

//满期处理后的提示
function AfterEndTimeDeal(flag, content)
{
  showInfo.close();
  window.focus();
  
  if (flag == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		if(fm.dealType.value == '1')
    {
    	buttonControl("XBDealButton",'');
    	buttonControl("MJDealButton",'none');
    }else
    {
    	buttonControl("MJDealButton",'');
    	buttonControl("XBDealButton",'none');
    }
  }
}

//保存数据提示框
function showSavaInfoWindow()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
}

//200081231 zhanggm 少儿险被保人>=19岁，不允许续保
//090612 add by xp 添加客户投保后享有两年的保证续保权,此种情况直接略过
function checkChildAge()
{
  var tInsuredBirthday ;
  var sql = "select max(year(PayToDate - InsuredBirthday)) from LCPol where ContNo = '"
          + fm.all('ProposalGrpContNo').value + "' and riskcode = '320106' with ur";
  var rs = easyExecSql(sql);
  var XBcount;
  var passSQL = "select count(1) from ljspayb where otherno='"+ fm.all('ProposalGrpContNo').value + "' and dealstate='1' "
  					+ " and exists (select 1 from ljspaypersonb where getnoticeno=ljspayb.getnoticeno and riskcode='320106' and dealstate='1')" ;
  var passRS = easyExecSql(passSQL);
  if(rs&&passRS)
  {
    tInsuredBirthday = parseFloat(rs[0][0]);
    XBcount=parseFloat(passRS[0][0]);
    if(XBcount>=2)
    {
      alert("该少儿险保单已经续保两次，不能再续保！");
      return false;
    }
    
    if(fm.dealType.value == "1" && tInsuredBirthday>=19 && XBcount<2)
    {
          if(confirm("该少儿险被保人已经年满19周岁,但续保次数不到两次,是否继续续保?"))
           {
           return true;
           }
          else
           {
           alert("已经放弃续保,请继续其他操作。");
           return false;
           }
     }  
     if(fm.dealType.value == "1" && tInsuredBirthday>=19)
     {
     alert("被保人已经年满19周岁，不能续保！");
     return false;
     }        
  }
  return true;
}