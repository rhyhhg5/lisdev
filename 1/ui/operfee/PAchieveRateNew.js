var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";

function easyPrint()
{	
	var tManageCom = fm.all('ManageCom').value;			//管理机构代码
	var tSaleChnlType = fm.all('SaleChnlType').value;		//保单类型
	var tRiskType = fm.all('RiskType').value;			//险种类型
	var tSaleChnl = fm.all('SaleChnl').value;			//销售渠道
	var tOrphans = fm.all('Orphans').value;			//保单服务状态
	var tPayCount = fm.all('PayCount').value;			//缴次
	var tStartDate = fm.all('StartDate').value;			//应收时间起期
	var tEndDate = fm.all('EndDate').value;				//应收时间止期
	var tActuStartDate = fm.all('ActuStartDate').value;			//实缴时间起期
	var tActuEndDate = fm.all('ActuEndDate').value;				//实缴时间止期

	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	if(tSaleChnlType == "" || tSaleChnlType == null)
	{
		alert("请选择保单类型！");
		return false;
	}
	if(tRiskType == "" || tRiskType == null)
	{
		alert("请选择险种类型！");
		return false;
	}
	if(tSaleChnl == "" || tSaleChnl == null)
	{
		alert("请选择销售渠道！");
		return false;
	}
	if(tOrphans == "" || tOrphans == null)
	{
		alert("请选择保单服务状态！");
		return false;
	}
	if(tPayCount == "" || tPayCount == null)
	{
		alert("请选择缴次！");
		return false;
	}
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("请输入应收时间起期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间起期",tStartDate))
		{
			fm.all('StartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("请输入应收时间止期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间止期",tEndDate)){
			fm.all('EndDate').focus();
			return false;
		}
	}
	
    //应收时间起止期三个月校验&非工作时间起止日期一年内校验
	var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    if(t2-t1<0)
    {
      alert("生效开始日期不能晚于生效结束日期 "+tEndDate)
	  return false;
    }
    
   
	var interval = dateInterval(tStartDate, tEndDate);
	if (interval > 12) {
		alert("对不起，您不能打印一年以上的报表");
		return false;
	}
   
    
    
	// 传递页面信息
	fm.submit();
}
//日期格式校验
function checkDateFormat(tName,strValue) 
{
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}