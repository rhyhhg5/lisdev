<%
//ULIBenefitQueryInput.jsp
//程序功能：老年关爱给付抽档查询
//创建日期：2009-9-8 
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="ULIBenefitQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ULIBenefitQueryInputInit.jsp"%>
</head>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  String SubDate=tD.getString(AfterDate);  
  String tSubYear=StrTool.getVisaYear(SubDate);
  String tSubMonth=StrTool.getVisaMonth(SubDate);
  String tSubDate=StrTool.getVisaDay(SubDate);               	          
%>
<SCRIPT>
  var CurrentYear=<%=tCurrentYear%>;  
  var CurrentMonth=<%=tCurrentMonth%>;  
  var CurrentDate=<%=tCurrentDate%>;
  var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
  var SubYear=<%=tSubYear%>;  
  var SubMonth=<%=tSubMonth%>;  
  var SubDate=<%=tSubDate%>;
  var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
  var managecom = <%=tGI.ManageCom%>; 
  var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
  var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
</SCRIPT> 
<body onload="initForm();">
  <form name=fm action='' target=fraSubmit method=post>
    <table>
      <tr>
        <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);"></td>
      <td class= titleImg>给付任务查询：</td>
     </tr>
    </table>
    <div  id= "divAlivePayMulti" style= "display: ''">
      <table  class= common>
      	<tr>
          <td class= title>给付日起期</td>
      	  <td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      	  <td class= title>给付日止期</td>
      	  <td class= input><Input class="common" dateFormat="short" name=EndDate readonly></td>
      	  <td class= title>抽档日期</td>
      	  <td class= input><Input class="coolDatePicker" dateFormat="short" name=BatchDate ></td>
        </tr>
        <tr>
      	  <td class= title>操作人</td>
      	  <td class= input><Input class= common  name=Operator ></td>
      	  <td class= title>操作机构</td>
      	  <td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD></td>
      	  <td class= title>给付方式</td>
      	  <td class= input><Input class= "codeno"  name=PayMode CodeData="0|^1|现金^2|现金支票^3|转帐支票^4|银行转帐" verify="给付方式|notnull" ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PayModeName></td>
        </tr>
      	<tr>
      		<td class= title>保单号</td>
      		<td class= input><Input class= common name=ContNo></td>
      		<td class= title>客户号</td>
      		<td class= input><Input class=common name=AppntNo></td>
      		<td class= title>代理人部门</td>
      		<td class= input><Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName></td>
        </tr>
        <tr>
          <td class= title>代理人</td>
          <td class= input><Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this],[0]);" onkeyup="return showCodeListKey('AgentCode',[this],[0]);"></td>
          <td class= title>给付任务状态</td>
          <td class= input><Input class= "codeno"  name=PayTaskState CodeData="0|^0|待给付^1|给付成功" verify="给付状态|notnull" ondblclick="return showCodeListEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PayTaskStateName></td>
          <td class= title>打印状态</td>
          <td class= input><Input class= "codeno"  name=PrintState  CodeData="0|^0|未打印^1|已打印" verify="打印状态|notnull" ondblclick="return showCodeListEx('PrintState',[this,PrintStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PrintState',[this,PrintStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PrintStateName></td>
        </tr>
      </table>    
    </div>     					                                                                            
    <input value="查询满期任务明细" class = cssbutton TYPE=button onclick="easyQueryClick();">
<!-- 显示或隐藏信息 --> 
    <table>
      <tr>
        <td class=common>
          <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
        </td>
        <td class= titleImg>
            满期任务清单：
        </td>
      </tr>
    </table>
  <div  id= "divJisPay" style= "display: ''">
    <table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanLjsGetGrid" ></span></td>
      </tr>
      <tr>
        <td>  					
          <div id="divPage1" align=center style="display: 'none' ">
	        <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
	        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
	        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
	        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
          </div>				
        </td>
      </tr>
      <tr>
        <td class= titleImg>给付明细：</td>
      </tr>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanLjsGetDrawGrid" ></span></td>
      </tr>
    </table>     
  </div>
  <table>
    <tr>
      <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOptionButton);"></td>
      <td class= titleImg>操作按钮：</td>
    </tr>
  </table>  
  <div id= "divOptionButton" style= "display: ''">
	<input VALUE="打印给付清单" TYPE=button class = cssbutton onclick="printGetList()">
    <input VALUE="打印给付通知书" class = cssbutton TYPE=button onclick="printNotice();">
	<input VALUE="变更给付方式" TYPE=button class = cssbutton onclick="changePayMode()">
	<br>
	<br>
	<div align=left id="divInfo" style="display: 'none' ">
      <font color="#FF0000">提示：可以通过复选框选择多条给付任务打印通知书，如果不选择，默认打印给付列表中当前页全部任务。</font>
    </div>
  </div>
  <br>
  <div id="PayModeChangeID" style="display: 'none'">
    <table  class= common>
      <tr>
        <td class=titleImg colspan="10">给付信息变更：</td>
      </tr>
      <tr>
        <td class= title>原给付方式</td>
        <td class= input><Input type="hidden" name=PayModeOld><Input name=PayModeOldName style="width: 130" class=readonly readonly></td>
        <td class= title>变更为</td>
        <td class= input><Input class= "codeno" name=PayModeNew CodeData="0|^1|现金^2|现金支票^3|转帐支票^4|银行转帐" verify="给付方式|notnull" ondblclick="return showCodeListEx('PayMode2',[this,PayModeNewName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode2',[this,PayModeNewName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename name=PayModeNewName readonly style="width: 80"></td>
      </tr>
      <tr>
        <td class= titleImg colspan="2">账户信息由:</td>
      </tr>
      <tr>
        <td class= title>开户银行</td>
        <td class= input><Input class=readonly name=BankCodeOld style="width: 50" readonly ><input class=readonly name=BankNameOld readonly style="width: 80"></td>
        <td class= title>账户名</td>
        <td class= input><Input class=readonly name=AccNameOld style="width: 130" readonly></td>
        <td class= title>账号</td>
        <td class= input><Input class=readonly name=BankAccNoOld style="width: 130" readonly></td>
      </tr>
      <tr>
        <td class=titleImg colspan="2">变更为:</td>
      </tr>
      <tr>
        <td class= title>开户银行</td>
        <td class= input><Input class=codeNo name=BankCode style="width: 50" ondblclick="return showCodeList('Bank',[this,BankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankName],[0,1],null,null,null,1);"><input class=codename name=BankName readonly style="width: 80"></td>
        <td class= title>账户名</td>
        <td class= input><Input class=common1 name=AccName></td>
        <td class= title>账号</td>
        <td class= input><Input class=common1 name=BankAccNo></td>
        <td class= title>账号确认</td>
        <td class= input><Input class=common1 name=BankAccNo2></td>
      </tr>
      <tr>
        <td class= title><input VALUE="保全确认" TYPE=button class = cssbutton onclick="changePayModeSubmit()"></td>
      </tr>
    </table>  
  </div>
  <table>
    <tr>
      <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);"></td>
      <td class= titleImg>给付保单</td>
      <td class= titleImg><td>
      <td class= titleImg><td>
      <td class= titleImg><td>
      <td> <INPUT VALUE="保单明细查询" class = cssbutton TYPE=button onclick="getContInfo();"> </td>
    </tr>
  </table>
  <div id= "divLCCont" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      <td text-align: left colSpan=1><span id="spanIndiContGrid" ></span></td>
      </tr>
    </table>
    <div id="divPage2" align=center style="display: 'none' ">
      <center>    	
        <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">			
      </center>  
    </div>	
  </div>
  <table>
    <tr>
      <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);"></td>
      <td class= titleImg>给付险种</td>
    </tr>
  </table> 	
  <div  id= "divLCPol" style= "display: ''">
    <table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanPolGrid" ></span></td>
      </tr>
    </table>
    <div id="divPage3" align=center style="display: 'none' ">	
      <center>      				
        <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
        <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
        <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
        <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
      </center>
    </div>
  </div>
   
  <input type= "hidden" name= "QuerySql" value= "">  	
  <input type= "hidden" name= "taskNo" value="">
  <input type= "hidden" name= "GetNoticeNo" value="">  
  <input type= "hidden" name= "contNoForModify" value="">  	  	 
  <div id="test"></div>
  <div id="ErrorsInfo"></div>                                                                        
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>      
  <iframe name="printfrm" src="" width=0 height=0></iframe>
  <form method=post id=printform target="printfrm" action="">
    <input type=hidden name=filename value=""/>
  </form>  	
</body>
</html>
