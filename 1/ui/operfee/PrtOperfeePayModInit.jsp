<%
//程序名称：PrtContSuccessInit.jsp
//程序功能：
//创建日期：2007-1-15 16:35
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>

<%
  String title = "个单续期续保收费方式统计";
  String startTitleName = "实收起期";
  String endTitleName = "实收止期";
  
  String loadFlag = request.getParameter("LoadFlag");
  if("Deal".equals(loadFlag))
  {
    title = "续期续保处理情况统计";
    startTitleName = "操作起期";
    endTitleName = "操作止期";
  }
%>

<script language="JavaScript">
var operator = "<%=tGI.Operator%>";  //操作员编号
var mSelect;
var loadFlag = "<%=loadFlag%>";

//表单初始化
function initForm()
{
  initElement();
	initElementtype();
}
</script>