  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var strSql="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{	
	initQueryDataGrid();
	//判断关键定是否为空
	var strAgentCom=trim(document.fm.all("ManageCom").value);
	var riskCode=trim(document.fm.all("riskCode1").value);//个团标记
	var strSQL ="";
	//初始化防止出错
	if(strAgentCom==null||strAgentCom=='')
	{
		window.alert("请选择查询机构");
	}
	if(riskCode==null||riskCode=='')
	{
		window.alert("请选择查询险种");
	}else if(riskCode=='0'){
		 strSQL = " and riskcode in ('333701','332601','333001') "; 
	}else{
		strSQL = "  and riskcode = '"+riskCode+"' ";
	}

			  //查询strSql语句如下
			  	
				  strSql=" select (select name from ldcom where comcode=lcc.managecom), "+
					" lcc.contno, "+
					" (select riskname from lmriskapp where riskcode =lcp.riskcode), "+
					" lcc.appntname,lcc.insuredname,lcc.cvalidate,lcc.prem, "+
					" (select Phone from lcaddress a,lcappnt b where a.customerno =b.appntno "+
					" and a.addressno=b.addressno and contno=lcc.contno), "+
					" (select PostalAddress from lcaddress a,lcappnt b where a.customerno =b.appntno "+
					" and a.addressno=b.addressno and contno=lcc.contno), "+
					" (select name from lacom where agentcom = lcc.agentcom) "+
					" from lccont lcc inner join lcpol lcp on lcc.prtno = lcp.prtno "+
					" where lcc.conttype = '1'"+
					" and lcc.salechnl in ('04','13') "+
					" and lcc.appflag='1' and lcc.stateflag='1' "+
					" and (lcc.cvalidate + 1 years) <= (current date + 30 days) "+
					" and (lcc.cvalidate + 2 years) >=current date "+
					" and not exists (select 1 from lccontremain where contno=lcc.contno) "+
					" and lcc.managecom like '"+strAgentCom+"%' "
					+strSQL+" with ur  ";
			


	 //查询SQL，返回结果字符串
	 turnPage.strQueryResult=easyQueryVer3(strSql, 1, 0, 1); 
	 
	 if(!turnPage.strQueryResult)
	 {
	 	window.alert("没有满足条件的记录!");
	 	return false;
	 }
	 else
	 {
	 	
	 	turnPage.queryModal(strSql,DataGrid);
	 }
	 
}

function easyPrint()
{	
	if( DataGrid.mulLineCount == 0)
	{
		window.alert("没有需要打印的信息");
		return false;
	}
	fm.all('strsql').value=strSql;
	fm.submit();
}
