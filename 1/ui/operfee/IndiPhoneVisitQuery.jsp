<A HREF=""></A> <html> 
<%
//程序名称：GrpDueFeeInput.jsp
//程序功能：集体保费催收，实现数据从保费项表到应收集体表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	
        GlobalInput tGI = new GlobalInput();
        //PubFun PubFun=new PubFun();
	    tGI = (GlobalInput)session.getValue("GI");
	    System.out.println("1"+tGI.Operator);

        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);                	               	
        String tCustomer = request.getParameter("CustomerNo");	
        String DetailWorkNo = request.getParameter("DetailWorkNo");	
%>
<head >
  <SCRIPT>
		var CurrentYear=<%=tCurrentYear%>;  
		var CurrentMonth=<%=tCurrentMonth%>;  
		var CurrentDate=<%=tCurrentDate%>;
		var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
		var SubYear=<%=tSubYear%>;  
		var SubMonth=<%=tSubMonth%>;  
		var SubDate=<%=tSubDate%>;
		var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
		var managecom = <%=tGI.ManageCom%>;
		var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
		var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
		var operator = "<%=tGI.Operator%>";  //操作员编号
		var Customer = "<%=tCustomer%>";
		var DetailWorkNo = "<%=DetailWorkNo%>";
  </SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="IndiPhoneVisitQuery.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="IndiPhoneVisitInit.jsp"%>
</head>
<body onload="initForm();">
<form name=fm action="./IndiPhoneVisit.jsp" target=fraSubmit method=post>
  <div id="test"></div>
  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	 <INPUT VALUE="地址变更" class = cssbutton id="changeAddress" TYPE=button onclick="addressChange();"> 
	 <INPUT VALUE="补发收费通知书" class = cssbutton id="rePrintFeeNotice" TYPE=button onclick="printNotice();"> 
	 <INPUT VALUE="业务员通知" class = cssbutton id="noticeAgent" TYPE=button onclick="agentNotice();"> 
    <table  class= common align=center>
	     <tr class=common>
          <TD  class= title>
          客户号
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=AppntNo  readonly>
          </TD>
		  <TD  class= title>
          投保人
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=appntName  readonly>
          </TD>  		   
          <TD  class= title>
          催收通知书号
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=GetNoticeNo readonly >
          </TD>     
		</tr>
		     <tr class=common>
          <TD  class= title>
          住宅电话
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=HomePhone  readonly >
          </TD>
		  <TD  class= title>
          单位电话
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=Phone  readonly>
          </TD>  		   
          <TD  class= title>
          移动电话
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=Mobile  readonly>
          </TD>     
		</tr>
  </table>          
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 客户在办业务
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCustomerGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
   <center>    
    <Div id= "divPage" align="center" style= "display: 'none' ">	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">		
    </div>	
  </center>  	
  </div>
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			应缴记录：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpJisPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>     
	<center>    
	  <Div id= "divPage2" align="center" style= "display: 'none' ">	  				
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
		</div>
	</center>  
	</Div>	
	<TABLE CLASS="common">
			 <TR  class= common>
				 <TD  class= input colspan="8">
				   <input type="radio" name="operator" value="1">重设缴费事项<input type="radio" name="operator" value="0">终止缴费
				 </TD>	
			 </TR>	
			<TR  class= common>
				 <TD  class= title>收费方式</TD>
				 <TD  class= input>
				   <Input class="codeNo" name="PayMode" CodeData="0|^1|现金^4|银行转帐" verify="交付费方式|&code:PayMode"  ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1]);" onkeyup="return showCodeListEx('PayMode',[this,PayModeName],[0,1]);"><Input class="codeName" name="PayModeName"  elementtype="nacessary" readonly>
				 </TD>					
				<TD  class= title>转帐银行</TD>
				<TD  class= input>
					<Input class="common1" name=BankCode readonly >
				</TD>				
				<TD  class= title>帐号</TD>
				<TD  class= input>
					<Input class="common1" name=BankAccNo readonly >
				</TD>
				<TD  class= title>转帐日期/缴费期限</TD>
				<TD  class= input>
					<Input class="coolDatePicker" dateFormat="short" name=PayDate  >
				</TD> 
			 </TR>	
	</Table>
	 <br>
     <INPUT VALUE="保  存" class = cssbutton id="save" TYPE=button onclick="saveRecord();"> 
	 <INPUT VALUE="结  案" class=cssbutton id="endCase" TYPE=button onclick="overCase();">
	       <INPUT TYPE=hidden name=HidPayMode>
</form>
  <Form name=fmSubmitAll action="./IndiPhoneVisitCancle.jsp" method=post target="fraSubmit">   
		<INPUT TYPE=hidden name=SubmitNoticeNo> 
		<INPUT TYPE=hidden name=DetailWorkNo value="<%=DetailWorkNo%>">
  </Form>  
  <Form name=fmSaveAll action="./IndiPhoneVisit.jsp" method=post target="fraSubmit">  
	 <INPUT TYPE=hidden name=HidePayDate> 
	 <INPUT TYPE=hidden name=HidePayMode>
	 <INPUT TYPE=hidden name=HidOldBankCode>
	 <INPUT TYPE=hidden name=HidOldBackAccNo>
	 <INPUT TYPE=hidden name=HideNoticeNo>
	 <INPUT TYPE=hidden name=HidoperatorType>
	 <INPUT TYPE=hidden name=DetailWorkNo value="<%=DetailWorkNo%>">
  </Form>
  <Form name=fmOverAll action="./IndiPhoneVisitOver.jsp" method=post target="fraSubmit">   
		<INPUT TYPE=hidden name=HideCustomerNo> 
		<INPUT TYPE=hidden name=Sql>
	  <INPUT TYPE=hidden name=DetailWorkNo value="<%=DetailWorkNo%>">
  </Form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
