<%
//程序名称：LJSPayPersonSave.jsp
//程序功能：
//创建日期：2002-07-12 08:49:52
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>  
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>  
  
<%@page contentType="text/html;charset=GBK" %> 
<%
 
  //接收信息，并作校验处理。
  LJSPaySchema       tLJSPaySchema         = new LJSPaySchema();
  LJSPayPersonSet    tLJSPayPersonSet      = new LJSPayPersonSet();
  LCPremSet          tLCPremSet            = new LCPremSet();

  LCPremSchema       tLCPremSchema ; 
  LJSPayPersonSchema tLJSPayPersonSchema ;
   
  DuePayFeeUI tDuePayFeeUI   = new DuePayFeeUI();
//暂时没有用LJSPayPersonBL替换LJSPayPersonSchema
//  LJSPayPersonBL tLJSPayPersonSchema = new LJSPayPersonBL();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  String transact = "";
  //接受记录数目，用于循环存储
  String tableNum="";
  
  int index=0;
  int count=0;             
  String FlagStr = "";
  String Content = "";

  tableNum=request.getParameter("tableNum");   
  count=Integer.parseInt(tableNum);
  
  String PolNo=request.getParameter("PolNo");
System.out.println("PolNo="+PolNo);
  String Name=request.getParameter("Name");
  String Operateor=request.getParameter("Operateor");  
  String AppntNo=request.getParameter("AppntNo");
  String SumMoney=request.getParameter("SumDuePayMoney");
  String SumDuePayMoney=request.getParameter("SumDuePayMoney");
  

//还要添加数据 LJSPay 应收总表 一行数据即可
//通知书号码 （自动生成，暂时手工添加）对应暂收表的赞交费收据号码
//其它号码是个人保单号码 其它号码类型即为个人保单号
//根据对象不同，其它号码内容不同
//投保人客户号码 总应收金额 操作员 直接添加
    tLJSPaySchema.setGetNoticeNo(PolNo);//通知书号码，待修改
    tLJSPaySchema.setOtherNo(PolNo);     //待改进 
    tLJSPaySchema.setOtherNoType("2");  //待改进
    tLJSPaySchema.setOperator(Operateor);    
    tLJSPaySchema.setAppntNo(AppntNo);
    tLJSPaySchema.setSumDuePayMoney(SumDuePayMoney);

//下面填充应收个人交费表和保费项表数据    
while(index<count)
{
    index=index+1;  

    tLCPremSchema = new LCPremSchema(); 
    tLJSPayPersonSchema  = new LJSPayPersonSchema();
 

//共有数据成员，对应于应收个人交费表
    tLJSPayPersonSchema.setPolNo(PolNo);
    tLJSPayPersonSchema.setOperator(Operateor);    
    tLJSPayPersonSchema.setAppntNo(AppntNo);
    tLJSPayPersonSchema.setSumDuePayMoney(SumDuePayMoney);
    
//私有数据成员，对应于应收个人交费表  
    tLJSPayPersonSchema.setDutyCode(request.getParameter("DutyCode"+index));
    tLJSPayPersonSchema.setPayPlanCode(request.getParameter("PayPlanCode"+index));
    tLJSPayPersonSchema.setSumActuPayMoney(request.getParameter("SumActuPayMoney"+index));
    tLJSPayPersonSchema.setPayDate(request.getParameter("PayDate"+index));
    tLJSPayPersonSchema.setPayCount(request.getParameter("PayCount"+index));

    tLJSPayPersonSet.add(tLJSPayPersonSchema);
System.out.println("89=");    
//  流水号自动输入, 银行编码在UI中输入 
//  通知书号码由函数自动生成后和其他页面上填写的数据一起提交

//还要添加数据 LCPrem 保费项表 多行数据
//添加字段：保单号码 责任编码 交费计划编码 
//已交费次数（要自行加 1 ） 其它暂略
    tLCPremSchema.setPolNo(PolNo);    
    tLCPremSchema.setDutyCode(request.getParameter("DutyCode"+index));
    tLCPremSchema.setPayPlanCode(request.getParameter("PayPlanCode"+index));
    tLCPremSchema.setPayTimes(request.getParameter("PayCount"+index)+1);
    tLCPremSet.add(tLCPremSchema);
System.out.println("103=");     
}
//while 循环结束，将所有数据提交

     transact=request.getParameter("Transact");
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tLJSPaySchema);
   tVData.addElement(tLJSPayPersonSet);
   tVData.addElement(tLCPremSet);

   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tDuePayFeeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tDuePayFeeUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = transact+" 成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = transact+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理
%>                                              
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

