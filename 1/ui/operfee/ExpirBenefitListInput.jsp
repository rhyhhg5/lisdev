<html> 
<%
//程序名称：BatchPayInput.jsp
//程序功能：财务批量付费
//创建日期：2008-07-23 
//创建人  ：djw
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  
  <SCRIPT src="ExpirBenefitListInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ExpirBenefitListInit.jsp"%>
</head>
<body  onload="initForm();initElementtype()" >
<Form method=post action="ExpirBenefitListPrint.jsp" name=fm target="fraSubmit">

	<Table>
    	<TR>
        	<TD class=common>
	           
    		</TD>
    		<TD class= titleImg>
    			 查询条件
    		</TD>
    	</TR>
    </Table> 
   
    <table class= common>
	    <tr class= common> 
	         <TD  class= title>管理机构</TD>
	         <TD  class= input><Input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
	         <TD  class= title>给付状态</TD>
	         <TD  class= input><Input class="codeNo" name="EdorState" CodeData="0|^0|未给付^1|给付^2|全部" ondblclick="return showCodeListEx('EdorState',[this,EdorStateName],[0,1]);" onkeyup="return showCodeListEx('EdorState',[this,EdorStateName],[0,1]);"><Input class="codeName" name="EdorStateName" readonly></TD>
	         <TD  class= title>个团标志</TD>
		     <TD  class= input><Input class="codeNo" name="ContType" CodeData="0|^1|个险^2|团险^3|全部" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);" onkeyup="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);"><Input class="codeName" name="ContTypeName"  readonly></TD>
	    </tr>
	    <TR  class= common>
	       <TD  class= title width="25%">应给付开始日期</TD>
	      	<TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=AppStartDate verify="受理时间起期|NOTNULL" ></TD>
			<TD  class= title width="25%">应给付结束日期</TD>
	      	<TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=AppEndDate verify="受理时间止期|NOTNULL" ></TD>
	    </TR>
  	</Table>  
      <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();">  
      <INPUT VALUE="打  印" class= cssbutton TYPE=button onclick="easyPrint()">
    <Table>
    	<TR>
        	<td class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</td>
    		<td class= titleImg>
    			 应付总表信息
    		</td>
    	</TR>
    </Table>  
  <input type=hidden name=ComCode>  	
 <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <td text-align: left colSpan=1>
            <span id="spanDataGrid" ></span> 
  	    </td>
      </TR>
    </Table>					
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>	
  <div id='Savebutton' style='display:' > 
      <input type=hidden name="arrpSql">  
      <!--input type=button value="清单下载" class=cssButton onclick="download();"-->
  </div>
  <input type="hidden" name="strsql">
</Form>    
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 