<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String tYWType = "";
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  tYWType = request.getParameter("ywtype1");//业务类型，PAYMUL收费，GETMUL付费
  System.out.println("续期收付费批量解锁");
  System.out.println("业务类型："+tYWType);
  ArrayList arrayList = new ArrayList();
  if("PAYMUL".equals(tYWType))
  {
    String tGetNoticeNo[] = request.getParameterValues("LJSPayXQGrid3");	 
    String tChk[] = request.getParameterValues("InpLJSPayXQGridChk"); //参数格式=” Inp+MulLine对象名+Chk”
    int tCount = tGetNoticeNo.length;
	for(int j=0; j<tCount; j++)
	{
	  if(tChk[j].equals("1"))  
	  { 
	    arrayList.add(tGetNoticeNo[j]);
		System.out.println("---------GetNoticeNo: "+tGetNoticeNo[j]);
	  }		
	}
  }
  else if("GETMUL".equals(tYWType))
  {
    String tGetNoticeNo[] = request.getParameterValues("LJAGetXQGrid3");	 
    String tChk[] = request.getParameterValues("InpLJAGetXQGridChk"); //参数格式=” Inp+MulLine对象名+Chk”
    int tCount = tGetNoticeNo.length;
	for(int j=0; j<tCount; j++)
	{
	  if(tChk[j].equals("1"))  
	  { 
	    arrayList.add(tGetNoticeNo[j]);
		System.out.println("---------ActuGetNo: "+tGetNoticeNo[j]);
	  }		
	}
  }
	
  LJSXQMulUnlockBL tLJSXQMulUnlockBL = new LJSXQMulUnlockBL();
  try
  {
    VData tVData = new VData();
  	TransferData tTransferData = new TransferData();
  	tVData.add(tG); 
  	tTransferData.setNameAndValue("arrayList", arrayList);
  	tTransferData.setNameAndValue("ManageCom",request.getParameter("ManageComMul"));
  	tTransferData.setNameAndValue("ContNo",request.getParameter("ContNoMul"));
  	tTransferData.setNameAndValue("GetNoticeNo",request.getParameter("GetNoticeNoMul"));
  	tVData.add(tTransferData);
    if(!tLJSXQMulUnlockBL.submitData(tVData,tYWType))
    {
      Content = "操作失败，原因是:" + tLJSXQMulUnlockBL.mErrors.getFirstError();
      System.out.println(Content);
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("续期收付费信息修改完成");
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  { 
    tError = tLJSXQMulUnlockBL.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "数据修改成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit1("<%=FlagStr%>","<%=Content%>","<%=tYWType%>");
</script>
</html>
