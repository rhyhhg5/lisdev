<A HREF=""></A> <html> 
<%
//程序名称：IndiLJSCancelInput.jsp
//程序功能：集体保费催收，实现数据从保费项表到应收集体表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
  	String tManageCom=tG.ManageCom;
  	int len=tManageCom.length();
%>
<%
   String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<head >
 
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="XQConYearInput.js"></SCRIPT>  

</head>
<body>
<form name=fm action=./XQConYearReport.jsp target=f1print method=post>

  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>个险续期统计：</td>
		</tr>
	</table>
    <table  class= common align=center>
	     <tr class=common>
          <TD  class= title>
          机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName> 
          </TD>
		  </tr>
		  <tr>
          <TD  class= title>
          操作时间
          </TD>
          <TD  class= input>
			 <Input name=OperationEndDate class="coolDatePicker" dateFormat="short" verify="开始日期|NOTNULL">
			 至
             <Input name=OperationStartDate class="coolDatePicker" dateFormat="short" verify="结束日期|NOTNULL"> 
          </TD>  
		</tr>
  </table>          
	<br>
     <INPUT VALUE="续期年度分析打印" class = cssbutton TYPE=button onclick="submitForm();"> 
     <Input type=hidden name=SubmitNoticeNo> 
     <Input type=hidden name=SubmitCancelReason> 
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
