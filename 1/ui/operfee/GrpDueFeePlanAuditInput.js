//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  }
  //resetForm();
}

//重置按钮对应操作,重新查询抽档的相关信息
//function resetForm()
//{
//  try
//  {
//  	getPolInfo();
//  }
//  catch(re)
//  {
//  	alert("在GrpDueFeePlanInput.js-->resetForm函数中发生异常!");
//  }
//}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//处理个案催收
function saveAuditResult()
{
	if(GrpContGrid.mulLineCount==0){
		alert("请先查询需要审核的保单");
		return false;
	}
	 if(GrpContGrid.getSelNo() < 1)
	  {
	    alert("请选择需要审核的保单");
	    return false;
	  }
	 if(GrpPayGrid.mulLineCount==0)
	 {
		alert("未查询到保单的缴费信息，无法进行审核操作");
		return false;
	 }
	 if(!verifyInput()){
			return false;
		}

		
		
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面。";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit();
}






function CheckData()
{
	if(!verifyInput()){
		return false;
	}
	if((fm.all('GrpContNo').value==null||fm.all('GrpContNo').value=='')&&(fm.all('PrtNo').value==null||fm.all('PrtNo').value=='')){
		alert("集体保单号或者印刷号不能同时为空!");
		return false;
	}
	var sql = "select grpcontno from lcgrpcont a "
			+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') "
			+ getWherePart( 'GrpContNo','GrpContNo' )
			+ getWherePart( 'PrtNo', 'PrtNo')
			+ " and exists (select 1 from lcgrppayplan where prtno=a.prtno) with ur ";
	var result = easyExecSql(sql);
	if(result==null){
		alert("该团单不是约定缴费的团单或该保单已失效,请核对!");
		return false;
	}
	
	var sql1 = "select grpcontno from lcgrpcont a "
		+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') "
		+ getWherePart( 'GrpContNo','GrpContNo' )
		+ getWherePart( 'PrtNo', 'PrtNo')
		+ " and exists (select 1 from lcgrppayplan where prtno=a.prtno) and state = '03050002' with ur ";
	var result1 = easyExecSql(sql1);
	if(result1!=null){
		alert("该团单已做过团单终止保全项目，不能做续期抽档!");
		return false;
	}
	return true;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpContGrid();
	initGrpPayGrid();
	initGrpPayPlanGrid();
	getContInfo();
	
	
	
}

// 数据返回父窗口
function returnParent()
{
	
	if( fm.all("tGrpContNo").value == null || fm.all("tGrpContNo").value == '' )
		alert( "请先查询保单信息。" );
	else
	{
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ fm.all("tGrpContNo").value+"&ContType=1");
		}
		catch(ex)
		{
			alert( "查询出错!" );
		}
	}
}

//查询团单信息
function getContInfo()
{
	var strSQL = " select b.GrpContNo,b.cvalidate,b.ManageCom,ShowManageName(b.ManageCom),b.prtno "
	         +" from LCGrpCont b where  "
			 +" 1=1 and exists (select 1 from ljspay where otherno=b.grpcontno and cansendbank='Y') and payintv=-1 "
			+ getWherePart( 'GrpContNo','GrpContNo' )
			+ getWherePart( 'ManageCom', 'ManageCom','like')
			+ getWherePart( 'PrtNo','PrtNo' );
    turnPage.queryModal(strSQL, GrpContGrid);
    if(GrpContGrid.mulLineCount==0){
		
		alert("未查询到需要审核的保单信息");
		return;
	}
    fm.all("tPrtNo").value=GrpContGrid.getRowColData( 0, 5 );
    fm.all("tGrpContNo").value=GrpContGrid.getRowColData( 0, 1 );
   
}



//查询收费信息
function getPolInfo()
{
		var strSQL = "select contplancode, " 
					+ " sum(prem),(select nvl(sum(prem),0) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and state = '1'),"
					+ " (select nvl(prem,0) from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2')," 
					+ " sum(prem)-(select nvl(sum(prem),0) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and state = '1')-" 
					+"(select nvl(prem,0) from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2')," 
					+ " (select GetNoticeNo from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2') "
		            + " from lcgrppayplan a "
		            + " where prtno= '" + fm.all("tPrtNo").value + "'  "
		            + " and int(plancode)<>1 "
		            + " group by contPlanCode,a.prtno order by contPlanCode";
		turnPage1.queryModal(strSQL, GrpPayGrid);
		if(GrpPayGrid.mulLineCount==0){
			
			alert("未查询到需要审核保单的缴费信息");
			return;
		}
		fm.all("GetNoticeNo").value=GrpPayGrid.getRowColData( 0, 6 );
		getPlanInfo();

		
}


//查询缴费计划信息
function getPlanInfo()
{
		var strSQL = "select contplancode, " 
					+ " sum(prem),sum(prem),plancode "
					//+ " (select prem from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2')," 
					//+ " sum(prem)-(select (sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and state = '1')-" 
					//+"(select prem from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2')," 
					//+ " (select GetNoticeNo from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2') "
		            + " from lcgrppayplan a "
		            + " where prtno= '" + fm.all("tPrtNo").value + "'  "
		            + " and int(plancode)=1 "
		            + " group by contPlanCode,a.prtno,a.plancode " 
		            + " union all select contplancode, " 
					+ " sum(prem),(select nvl(sum(prem),0) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and state = '1' and plancode=a.plancode),plancode "
					//+ " (select prem from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2')," 
					//+ " sum(prem)-(select (sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and state = '1')-" 
					//+"(select prem from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2')," 
					//+ " (select GetNoticeNo from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2') "
		            + " from lcgrppayplan a "
		            + " where prtno= '" + fm.all("tPrtNo").value + "'  "
		            + " and int(plancode)<>1 "
		            + " group by contPlanCode,a.prtno,a.plancode order by contPlanCode,plancode";
		turnPage2.queryModal(strSQL, GrpPayPlanGrid);
		if(GrpPayPlanGrid.mulLineCount==0){
			
			alert("未查询到需要审核保单的缴费信息");
			return;
		}
		

		
}

