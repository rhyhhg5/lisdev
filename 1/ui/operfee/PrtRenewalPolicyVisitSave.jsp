<%@page contentType="text/html;charset=GBK" %>
<%@page pageEncoding="GBK"%>
<%request.setCharacterEncoding("GBK");%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/Download.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "续期及失效保单回访报表_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    //分割页面信息
//    String[] tArrString = request.getParameter("arrset").split(",");
//    System.out.println("Debug:tArrString-"+tArrString.length);
	//初始化数组下标
//    int tRow=-1;
	//取得页面信息总行数
//    int tArrRowLen = tArrString.length / 26;
	//取得页面信息总单元数
//    int tArrColLen = tArrString.length - 1;
//    String[][] tArrSet = new String[tArrRowLen][26];
//    System.out.println("Debug:tArrLen-"+tArrRowLen);
//    for(int tCol = 0;tCol < tArrColLen;tCol++)
//    {
//        System.out.println("Debug:tCol-"+tCol);
//        if(tCol%26 == 0)  tRow++;
//		tArrSet[tRow][tCol%26] = tArrString[tCol];
//    }

	String tSQL = request.getParameter("strsql");
	System.out.println("打印查询:"+tSQL);
    //设置表头
    String[][] tTitle = {{"管理机构","管理机构编码","抽档日期","营销部门","保单号","应收记录号","保单状态","投保人","投保人 移动电话","投保人 联系电话","邮编","投保人联系地址","被保险人姓名","被保险人联系电话","被保险人联系地址","保单生效日","保单缴次","应交险种","应交保费","应收时间","实收保费","收费方式","开户行","开户行 帐号","缴费截至日期","实交日期","催收状态","作废原因","代理人编码","代理人手机","代理人固话","代理人姓名","签单代理人","回访成功人员","回访成功日期","延误缴费的原因","是否存在销售人员截留挪用保费","意见或建议"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>