<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： ULIBenefitPrint.jsp
//程序功能：
//创建日期：2009-09-27
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
  System.out.println("ULIBenefitPrint.jsp start");
  String FlagStr = "";
  String Content = "";
  String tGetNoticeNo[] = request.getParameterValues("LjsGetGrid16");
  ArrayList arrayList = new ArrayList();
  String tChk[] = request.getParameterValues("InpLjsGetGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
    
  int tGridCount = tGetNoticeNo.length;
  boolean allFlag = true;
  for(int i = 0; i < tGridCount; i++)
  {
  	if(tChk[i].equals("1"))
  	{
  	  arrayList.add(tGetNoticeNo[i]);
  	  System.out.println("---------GetNoticeNo: "+tGetNoticeNo[i]);
  	  allFlag = false;
  	}		
  }
  if(allFlag)
  {
    for(int j = 0; j < tGridCount; j++)
    {
  	  arrayList.add(tGetNoticeNo[j]);
  	  System.out.println("---------GetNoticeNo: "+tGetNoticeNo[j]);
    }
  }
  	
  try
  {
    GlobalInput tG = new GlobalInput();      
    tG = (GlobalInput)session.getValue("GI");
    //tG.ClientIP = request.getRemoteAddr(); //操作员的IP地址
    tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("arrayList", arrayList);
    VData tVData = new VData();
    tVData.add(tG);
    tVData.add(tTransferData);
  
    ULIBenefitBatchPrintBL tULIBenefitBatchPrintBL = new ULIBenefitBatchPrintBL();
    if(!tULIBenefitBatchPrintBL.submitData(tVData,"PRINT"))
    {
      Content = "操作失败，原因是:" + tULIBenefitBatchPrintBL.mErrors.getFirstError();
      FlagStr = "Fail";
    }
    else                                                                           
    {
      Content = "操作成功";
      FlagStr = "Success";
    }
    System.out.println(Content);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
%>
<html>
<script language="javascript">	
  parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");
</script>
</html>