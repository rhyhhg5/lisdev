//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	var tCusttorNo = fmOverAll.all('HideCustomerNo').value ;
  showInfo.close();
  
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	
    //刷新页面
	  initForm();
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



function CheckDate()
{
  if(!isDate(fmMulti.all('StartDate').value)||!isDate(fmMulti.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fmMulti.all('StartDate').value,fmMulti.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

//查询文本框内容
function textQeury(contNo){

	var arrturn = new Array();
	var strSQL = "select b.appntName, c.HomePhone, c.CompanyPhone, c.Mobile "
		+" from LCAppnt b,LCAddress c "
		+ "where c.CustomerNo=b.AppntNo "
		+ "   and c.AddressNo=b.AddressNo "
		+ "   and  b.contNo='" + contNo + "' ";
	arrturn =easyExecSql(strSQL);			
	if (arrturn == null) {
		return false;
	} else {
		try { fm.all('AppntNo').value = Customer; } catch(ex) { };
		try { fm.all("AppntName").value=arrturn[0][0]; }catch(ex){};
		try { fm.all("HomePhone").value=arrturn[0][1]; }catch(ex){};
		try { fm.all('Phone').value = arrturn[0][2]; } catch(ex) { };
		try { fm.all("Mobile").value=arrturn[0][3]; }catch(ex){};
	}
}

//查询文本框内容
function payQeury(){
	 
  var  selNo=  GrpJisPayGrid.getSelNo();
	if (selNo==null || selNo==0 )
  {
		 alert("请选择一条应收记录");
		 return false;
	}else
	{
	  getLJSInfo(selNo - 1)
	}
}

//得到row行的应收信息
function getLJSInfo(row)
{
  var getNoticeNo = GrpJisPayGrid.getRowColDataByName(row, "getNoticeNo");
		var contNo = GrpJisPayGrid.getRowColDataByName(row, "contNo");
		var arrturn = new Array();
		var strSQL = "select b.PayMode,c.BankCode,c.BankAccNo,c.paydate, b.contNo "
		+ "from LCCont b,ljspayB c "
		+ "where b.contNo=c.otherno and b.contNo='" + contNo + "'"
		+ "   and c.GetNoticeNo = '" + getNoticeNo + "' "
		;
		arrturn =easyExecSql(strSQL);			
		if (arrturn == null) {
			return false;
		} else {
		  if(arrturn[0][1] != "" && arrturn[0][1] != "null" 
		      && arrturn[0][2] != "" && arrturn[0][2] != "null")
		  {
		    try { fm.all('PayMode').value = "4"; } catch(ex) { };
		    try { fm.all('PayModeName').value = "银行转账"} catch(ex) { };
		  }
		  else
		  {
		    try { fm.all('PayMode').value = "1"; } catch(ex) { };
		    try { fm.all('PayModeName').value = "现金"} catch(ex) { };
		  }
		  try { fm.all('GetNoticeNo').value = getNoticeNo; } catch(ex) { };
			try { fm.all('BankCode').value = arrturn[0][1]; } catch(ex) { };
			try { fm.all("BankAccNo").value=arrturn[0][2]; }catch(ex){};
			try { fm.all('PayDate').value = arrturn[0][3]; } catch(ex) { };
			try { fm.all('HidPayMode').value = arrturn[0][0]; } catch(ex) { };
			try { fmSubmitAll.all('DetailWorkNo').value = GrpJisPayGrid.getRowColDataByName(row, "workNo");; } catch(ex) { };
			try { fmSaveAll.all('DetailWorkNo').value = GrpJisPayGrid.getRowColDataByName(row, "workNo");; } catch(ex) { };
			try { fmOverAll.all('DetailWorkNo').value = GrpJisPayGrid.getRowColDataByName(row, "workNo");; } catch(ex) { };
			
		  textQeury(arrturn[0][4])
		}
}

//客户正在办理业务
function easyQueryClick()
{
	
	// 初始化表格
	initCustomerGrid();
	
	var strSQL = "select w.WorkNo, w.TypeNo, w.operator, w.AcceptCom, w.StatusNo " +
		 " from LGWork w, LGWorkTrace t  " +
		 " where  w.WorkNo = t.WorkNo  	 and    w.NodeNo = t.NodeNo  " +
		 " and  t.WorkBoxNo = (select WorkBoxNo from LGWorkBox where  OwnerTypeNo = '2' and OwnerNo = '" + operator + "') " + 
		 " and w.CustomerNo='" + Customer + "' " +
	     " and w.TypeNo='070016' " +
	     
			 
		 //其他类型的工单
		 //+" union " +
		 //" select w.WorkNo, w.TypeNo, t.SendPersonNo, w.AcceptCom, w.StatusNo "+
		 //" from LGWork w, LGWorkTrace t " +
		 //" where  w.WorkNo = t.WorkNo and   w.NodeNo = t.NodeNo "+
		 //" and (typeNo not like '03'   or customerNo is null)" +		//保全受理前扫描产生的工单
		 //" and  t.WorkBoxNo = (select WorkBoxNo from LGWorkBox   where OwnerNo = '" + operator + "'  and  OwnerTypeNo = '2') " 
		 //+ " and w.CustomerNo='" + Customer + "'" +
		 //  " and w.TypeNo='050014'" 
		  "order by w.workNo ";
	turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, CustomerGrid); 
	showCodeName();
	
}


//催收完成后查询催收纪录
function JisPayQuery()
{
	// 初始化表格
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
  var strSQL ;
	if(loadFlag != "PERSONALBOXVIEW")
	{
		strSQL = "select b.contNo, a.serialNo, a.getNoticeNo, a.sumDuePayMoney, "
		  + "   (select min(lastPayToDate) from LJSPayPersonB where getNoticeNo = a.getNoticeNo), "
		  + "   (select codename from ldcode where codetype='paymode' and code=b.PayMode), "
		  + "   a.payDate, getAgentName(b.AgentCode), ShowManageName(a.ManageCom), "
		  + "   case c.FinishType when '0' then '终止缴费' when '1' then '重设交费事项' else '' end, d.workNo "
		  + "from LJSPayB a, LCCont b, LGPhoneHasten c, LGWorkTrace d "
		  + "where a.otherNo = b.contNo "
		  + "   and a.getNoticeNo = c.getNoticeNo "
		  + "   and b.appntNo = c.customerNo "
		  + "   and c.workNo =d.workNo "
		  + "   and d.workBoxNo = (select workBoxNo from LGWorkBox where ownerNo = '" + operator + "') "
		  + "   and b.appntNo = '" + Customer + "' "
		  + "   and exists (select 1 from LGWork where statusNo not in('7','8') and workNo =d.workNo) "
		  + "order by c.WorkNo desc ";
	}else
	{
			strSQL = "select b.contNo, a.serialNo, a.getNoticeNo, a.sumDuePayMoney, "
		  + "   (select min(lastPayToDate) from LJSPayPersonB where getNoticeNo = a.getNoticeNo), "
		  + "   (select codename from ldcode where codetype='paymode' and code=b.PayMode), "
		  + "   a.payDate, getAgentName(b.AgentCode), ShowManageName(a.ManageCom), "
		  + "   case c.FinishType when '0' then '终止缴费' when '1' then '重设交费事项' else '' end, c.workNo "
		  + "from LJSPayB a, LCCont b, LGPhoneHasten c "
		  + "where a.otherNo = b.contNo "
		  + "   and a.getNoticeNo = c.getNoticeNo "
		  + "   and b.appntNo = c.customerNo "
		  + "   and b.ManageCom like '"+managecom+"%' "
		  + "   and b.appntNo = '" + Customer + "' "
		  + "   and exists (select 1 from LGWork where statusNo not in('7','8') and workNo =c.workNo) "
		  + "order by c.WorkNo desc ";
	}
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, GrpJisPayGrid); 
	
	if(GrpJisPayGrid.mulLineCount <= 0)
	{
	  alert("没有查询到应收记录信息")
	  return false;
	}
	
	//默认得到第一行的应收记录的客户信息
	textQeury(GrpJisPayGrid.getRowColDataByName(0, "contNo"));
	
	fmOverAll.Sql.value = strSQL;
	
	showCodeName();
}


//催收完成后查询催收纪录
function afterJisPayQuery()
{
	// 初始化表格
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
     var  selNo=  CustomerGrid.getSelNo();
	 if (selNo==null)
	 {
		 alert("请选择一条记录");
	 }else
	{		
		var strSQL ="select a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,min(c.LastPayToDate),b.PayMode,a.PayDate,b.AgentCode,ShowManageName(a.ManageCom) "
		+ " ,case when a.CancelReason='0' then '终止缴费'  when a.CancelReason='2' then '重设交费事项' end"
		+" from ljspayb a,lcgrpcont b,LJSPayPersonb c where a.otherno=b.grpcontno and a.otherNoType='1'"
		+ " and b.AppntNo='" + Customer + "'"
		+ " and a.DealState='0' and a.CancelReason is  not null"
		+ " group by a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,a.PayDate,a.ManageCom,b.PayMode,b.AgentCode,a.CancelReason "
		;

	  
	}  
	 
	
	turnPage2.queryModal(strSQL, GrpJisPayGrid); 
}


//选择操作
function operSelSelBox(){
	var  selNo=  GrpJisPayGrid.getSelNo();
	if (selNo==null || selNo==0 )
	 {
		 alert("请选择一条应收记录");
		 return false;
	 }
}


//地址变更
function addressChange()
{
	window.open("../task/TaskEasyEdorMain.jsp?CustomerNo=" + Customer);
}
//补发收费通知书
function printNotice()
{
	var tRow = GrpJisPayGrid.getSelNo() ;	
	if (tRow == 0 || tRow == null)
	{
		alert("请选择一条应收记录");
		return false;
	}

	 var tGetNoticeNo=GrpJisPayGrid.getRowColData(tRow - 1,3);
	 if (tGetNoticeNo =='')
	 {
		 alert("发生错误，原应是应收记录号为空");
		 return false;
	 }
	 window.open("../operfee/IndiPrintSel.jsp?GetNoticeNo="+ tGetNoticeNo);

}


//业务员通知（函件功能）
function agentNotice()
{
	selNo =   GrpJisPayGrid.getSelNo();
	if (selNo==null || selNo==0)
	{
		alert("请选择一条应收记录！");
		return false;
	}else
	{
		var tWorkNo = GrpJisPayGrid.getRowColDataByName(selNo - 1,"workNo");
	  window.open("../task/TaskLetterMain.jsp?workNo=" + tWorkNo);
	}
}

//结案按钮
function overCase()
{
	fmOverAll.all('HideCustomerNo').value=Customer;
	var showStr="正在，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fmOverAll.submit(); //提交
    
}

//保存按钮
function saveRecord()
{
	var  selLJSNo = GrpJisPayGrid.getSelNo();
	if (selLJSNo==null || selLJSNo==0 )
	{
	  alert("请选择一条应收记录！");
	  return false;
	}
	//校验是否选择回访结果
	var operatorType = "";
	if(fm.all('operator')[0].checked)
	{
	    operatorType = fm.all('operator')[0].value;
	}
  else if(fm.all('operator')[1].checked)
	{
	    operatorType = fm.all('operator')[1].value;
	}
	if(operatorType == "")
	{
	  alert("请选择回访结论！");
	  return false;
	}
	if(!isInteger(fm.PayMode.value))
	{
	  alert("收费方式应为数字");
	  return false;
	}
	
	if (operatorType == "1")
	{
    if (fm.all('HidPayMode').value != fm.all('PayMode').value && fm.all('PayMode').value==4)
    {
     alert("不能将收费方式改为银行转帐！");
     return false;
    }
    if(!isDate(fm.PayDate.value))
    {
      alert("转帐日期/缴费期限必须为日期，如2002-10-03"); 
      return true;
    }
    var sql = "select 1 from dual "
            + "where date(current date) > date('" + fm.PayDate.value + "') ";
    var rs = easyExecSql(sql);
    if(rs && rs[0][0] != "" && rs[0][0] != "null")
    {
      alert("转帐日期/缴费期限不能早于今天。");
      return false;
    }
    
    var tGrpContNo = GrpJisPayGrid.getRowColData(selLJSNo - 1,1);
    fmSaveAll.all('HidoperatorType').value=operatorType;
    fmSaveAll.all('HidePayMode').value=fm.all('PayMode').value;
    fmSaveAll.all('HidePayDate').value=fm.all('PayDate').value;
    fmSaveAll.all('HidOldBankCode').value=fm.all('BankCode').value;
    fmSaveAll.all('HidOldBackAccNo').value=fm.all('BankAccNo').value;
    fmSaveAll.all('HideNoticeNo').value=GrpJisPayGrid.getRowColDataByName(selLJSNo - 1,"getNoticeNo");
    var showStr="正在，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fmSaveAll.submit(); //提交
	}else
	{
		var tNoticeNo = GrpJisPayGrid.getRowColDataByName(selLJSNo - 1,"getNoticeNo");
		fmSubmitAll.all('SubmitNoticeNo').value=tNoticeNo;
		var showStr="正在，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fmSubmitAll.submit(); //提交
	}		
	
}
function controlButton()
{
  if(loadFlag == "PERSONALBOXVIEW")
  {
    hiddenButton();
  }
}

function hiddenButton()
{
  fm.all("changeAddress").style.display = "none";
  fm.all("save").style.display = "none";
  fm.all("endCase").style.display = "none";
}
