 <%
//程序名称：NormPayCollSave.jsp
//程序功能：
//创建日期：2002-10-10 08:49:52
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>  
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>  
  
<%@page contentType="text/html;charset=GBK" %> 
<%
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
   String Operator  = tGI.Operator ;  //保存登陆管理员账号
   String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  //表单中的隐藏字段
  String GrpContNo=request.getParameter("GrpContNo"); //集体保单号码   
  String PayDate=request.getParameter("PayDate"); //交费日期

//应收个人交费表中的多项数据                        
    String tTempClassNum[] = request.getParameterValues("NormPayCollGridNo");//序号
    String tChk[] = request.getParameterValues("InpNormPayCollGridChk");    //选中标记   
    String tPolNo[] = request.getParameterValues("NormPayCollGrid1"); //个人保单号
   // String tSumDuePayMoney[] = request.getParameterValues("NormPayCollGrid2");//应交金额    杨红于2005-07-21注销，在逻辑处理中未被处理
   // String tSumActuPayMoney[] = request.getParameterValues("NormPayCollGrid3");//实交金额   杨红于2005-07-21注销，在逻辑处理中未被处理 
	
    int TempCount = tTempClassNum.length; //记录数      
   // int selectTotal=0;//保存选中记录的总数！
   LJSPayPersonSet tLJSPayPersonSet    = new LJSPayPersonSet();
   for(int i=0;i<TempCount;i++)
   {
	   LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
	   tLJSPayPersonSchema.setPolNo(tPolNo[i]);
	   if(tChk[i].equals("1"))
	   {
       // selectTotal++；
	    
        tLJSPayPersonSchema.setInputFlag("1"); //录入标记
       // tLJSPayPersonSchema.setInputFlag("1");     
	   }
	   else
	   {
         tLJSPayPersonSchema.setInputFlag("0");
	   }
       tLJSPayPersonSet.add(tLJSPayPersonSchema);
   }
   /* //杨红于2005-07-21将下面逻辑注销掉，inputflag=0的记录没必要处理！
   for(int i=0;i<TempCount;i++)
   {
     tLJSPayPersonSchema = new LJSPayPersonSchema();
     tLJSPayPersonSchema.setPolNo(tPolNo[i]);
     tLJSPayPersonSchema.setGrpContNo(GrpContNo);   
     tLJSPayPersonSchema.setOperator(Operator);
     if(tChk[i].equals("1")) //如果被选中，那么录入标记设为1  
     { 
     	tLJSPayPersonSchema.setInputFlag("1"); //录入标记 
     }
     else
     {
     	tLJSPayPersonSchema.setInputFlag("0"); 
     }
     tLJSPayPersonSet.add(tLJSPayPersonSchema);
   }*/
 
  CollTimePayPersonUI tCollTimePayPersonUI = new CollTimePayPersonUI();
  try
  {
   VData tVData = new VData();
   tVData.add(tLJSPayPersonSet);
   tVData.add(tGI);
   tCollTimePayPersonUI.submitData(tVData,"INSERT");

   tError = tCollTimePayPersonUI.mErrors;
   if (tError.needDealError())        
    {
      Content = " 失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail"; 
    }
   else{
      Content = " 数据保存完毕!";
      FlagStr = "Succ";   	 
      } 
  }
  catch(Exception ex)
  {
    Content = "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
   
}//页面有效区
%>                                              
<html>
<body>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</body>
</html>

