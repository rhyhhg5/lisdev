<%
//程序名称：IndiManuUWContInit.jsp
//程序功能：个险续保保单列表
//创建日期：2006-08-14 
//创建人  ：Yangyalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String comCode = tGI.ComCode;
%>

<SCRIPT language=javascript>

//表单初始化
function initForm()
{
	try
	{
	  initInputBox();
		initContGrid();
	}
	catch(re)
	{
		alert("TaskPersonalBoxInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//初始化录入框
function initInputBox()
{
  fm.ManageCom.value = "<%=comCode%>";
}

//初始化保单列表
function initContGrid()
{                               
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="管理机构";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="comcode";
    iArray[1][21]="manageCom";
    
    iArray[2]=new Array();
    iArray[2][0]="业务员姓名";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2; 
    iArray[2][4]="agentcode"; 
    iArray[2][21]="agentName";

    iArray[3]=new Array();
    iArray[3][0]="业务员编码";         		//列名
    iArray[3][1]="0px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;
    iArray[3][21]="agentCode";

    iArray[4]=new Array();
    iArray[4][0]="业务员联系电话";    	//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="phoneNo";

    iArray[5]=new Array();
    iArray[5][0]="保单号";         		//列名
    iArray[5][1]="70px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="contNo";

    iArray[6]=new Array();
    iArray[6][0]="客户姓名";         		//列名
    iArray[6][1]="70px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="appntName";

    iArray[7]=new Array();
    iArray[7][0]="应缴日期";         		//列名
    iArray[7][1]="50px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="payToDate";
    
    iArray[8]=new Array();
    iArray[8][0]="期缴保费";         		//列名
    iArray[8][1]="50px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="prem";

    iArray[9]=new Array();
    iArray[9][0]="送核时间";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21]="uwToDate";

    iArray[10]=new Array();
    iArray[10][0]="送核原因";         		//列名
    iArray[10][1]="50px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21]="uwReason";

    iArray[11]=new Array();
    iArray[11][0]="核保完成时间";         		//列名
    iArray[11][1]="60px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0; 
    iArray[11][21]="uwFinishDate";
    
    iArray[12]=new Array();
    iArray[12][0]="核保结论";         	//列名
    iArray[12][1]="70px";            		//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[12][21]="uwIdea";
    
    iArray[13]=new Array();
    iArray[13][0]="受理号";         	//列名
    iArray[13][1]="70px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[13][21]="edorNo";
    
    iArray[14]=new Array();
    iArray[14][0]="营业部";         	//列名
    iArray[14][1]="70px";            		//列宽
    iArray[14][2]=200;            			//列最大值
    iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[14][21]="agentGroup";

    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
    //这些属性必须在loadMulLine前
    ContGrid.mulLineCount = 0;   
    ContGrid.displayTitle = 1;
    ContGrid.locked = 1;
    ContGrid.canSel = 0;
    ContGrid.canChk = 0;
    ContGrid.hiddenSubtraction = 1;
    ContGrid.hiddenPlus = 1;
    ContGrid.loadMulLine(iArray);  
    
  }
  catch(ex)
  {
    alert("IndiDueFeeInputInit.jsp-->initContGrid函数中发生异常:初始化界面错误!");
    return false;
  }
}
</SCRIPT>