  //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function fmSubmit()
{
    if(checkPremValue())
    {   
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fm.submit();    
    }	
}

function fmQuery()
{
  if(!verifyInput()) return false;	
  easyQueryClick();	
}

function checkPremValue()
{
   if(PremGrid.mulLineCount==0) 
   {
   	alert("请先查询!");
   	return false;
   }
   if(!verifyInput()) return false;
   
   return true;	
}
function checkValue()
{
    if(!verifyInput()) return false;
    
    return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initForm();    
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}     

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

        

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/**
 * 后台数据库查询
 */
function easyQueryClick() {
  // 拼SQL语句，从页面采集信息
 //杨红于2005-06-30注释掉旧的sql查询语句，注释开始 
 // var strSql = "select DutyCode,PayPlanCode,PayPlanType,PayTimes,StandPrem from LCPrem where ";
 // strSql=strSql+" PolNo in (select OtherNo from LJTempFee where OtherNo='"+trim(fm.all('PolNo').value)+"' and OtherNoType='0' and ConfFlag!='1')";
 // strSql=strSql+" and UrgePayFlag='N'";	
 // strSql=strSql+" and DutyCode not in(select DutyCode from LCDuty where PolNo in (select OtherNo from LJTempFee where OtherNo='"+trim(fm.all('PolNo').value)+"' and OtherNoType='0') and FreeRate=1) ";
 //注释结束
 //yangh于2005-06-30添加开始，修改查询Sql语句，新需求是根据合同号交费（区别于之前的根据险种交费）
 var strSql= "select PolNo,DutyCode,PayPlanCode,PayPlanType,PayTimes,Prem,Prem from LCPrem where";         
 strSql+=" ContNo in (select OtherNo from LJTempFee where OtherNo='"+trim(fm.all('ContNo').value)+"' and ConfFlag!='1')";	 
 strSql+=" and UrgePayFlag='N' and PayIntv='-1'"; 
 strSql+=" and DutyCode not in(select DutyCode from LCDuty where PolNo=LCPrem.PolNo and FreeRate=1)" ;
 //添加结束 
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  //alert(strSql);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    PremGrid.clearData();  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PremGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  
}