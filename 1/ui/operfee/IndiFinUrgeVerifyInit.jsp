<%
//程序名称：NormPayGrpCollInit.jsp
//程序功能：
//创建日期：2002-10-08 08:49:52
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String CurrentDate = PubFun.getCurrentDate();
  String CurrentTime = PubFun.getCurrentTime();
%> 
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initInpBox()
{ 
  try
  { 
    fm.all('GrpContNo').value = '';     
    fm.all('EndDate').value = CurrentTime;  
  }
  catch(ex)
  {
    alert("在NormPayCollInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在NormPayCollInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initNormPayCollGrid();     
  }
  catch(re)
  {
    alert("NormPayCollInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


//初始化表格
var NormPayCollGrid ;
function initNormPayCollGrid()
{
      var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";    	                //列名
      iArray[1][1]="80px";            		        //列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="投保人";    	                //列名
      iArray[2][1]="80px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="应收记录号 ";         		//列名
      iArray[3][1]="80px";            		        //列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="getNoticeNo";              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="应收金额";          		//列名
      iArray[4][1]="60px";            		        //列宽
      iArray[4][2]=50;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[5]=new Array();
      iArray[5][0]="应收日期";          		//列名
      iArray[5][1]="70px";            		        //列宽
      iArray[5][2]=50;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[6]=new Array();
      iArray[6][0]="保费帐户余额";          		//列名
      iArray[6][1]="80px";            		        //列宽
      iArray[6][2]=50;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[7]=new Array();
      iArray[7][0]="收费日期";          		//列名
      iArray[7][1]="70px";            		        //列宽
      iArray[7][2]=50;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[8]=new Array();
      iArray[8][0]="收费方式";          		//列名
      iArray[8][1]="60px";            		        //列宽
      iArray[8][2]=50;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="下一应收日期";          		//列名
      iArray[9][1]="85px";            		        //列宽
      iArray[9][2]=50;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[10]=new Array();
      iArray[10][0]="业务员代码";          		//列名
      iArray[10][1]="75px";            		        //列宽
      iArray[10][2]=50;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[11]=new Array();
      iArray[11][0]="操作人";          		//列名
      iArray[11][1]="70px";            		        //列宽
      iArray[11][2]=50;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[12]=new Array();
      iArray[12][0]="收据号";          		//列名
      iArray[12][1]="0px";            		        //列宽
      iArray[12][2]=50;            			//列最大值
      iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      NormPayCollGrid = new MulLineEnter( "fm" , "NormPayCollGrid" ); 
      //这些属性必须在loadMulLine前
      NormPayCollGrid.mulLineCount = 0;     
      NormPayCollGrid.displayTitle = 1;
      NormPayCollGrid.canSel = 1;
      NormPayCollGrid.hiddenPlus=1;   //隐藏"+"按钮
      NormPayCollGrid.hiddenSubtraction=1;//隐藏"-"按钮
      NormPayCollGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
  }

</script>