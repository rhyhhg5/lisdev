<%
//程序名称：ULIBenefitBatchQuery.jsp
//程序功能：万能老年关爱给付抽档Save页面
//创建日期：2009-9-3
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>  
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>  
<%  
  String StartDate = request.getParameter("StartDate");
  String EndDate = request.getParameter("EndDate");
  String ManageCom = request.getParameter("ManageCom");
  String QuerySql = request.getParameter("QuerySql");
  
  TransferData tTransferData=new TransferData();	
  tTransferData.setNameAndValue("StartDate",StartDate.trim());
  tTransferData.setNameAndValue("EndDate",EndDate.trim());
  tTransferData.setNameAndValue("ManageCom",ManageCom);
  tTransferData.setNameAndValue("QuerySql",QuerySql);
  tTransferData.setNameAndValue("PayMode",request.getParameter("payMode"));
  System.out.println("StartDate: " + StartDate);
  System.out.println("EndDate: " + EndDate);
  System.out.println("ManageCom:" +ManageCom);
	  	
  // 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  String batchNo = "";
 
  VData tVData = new VData();
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  tVData.add(tGI);
  tVData.add(tTransferData);
    
    ULIBenefitBatchUI tULIBenefitBatchUI = new ULIBenefitBatchUI();
    tULIBenefitBatchUI.submitData(tVData,"INSERT");
    batchNo = (String)tULIBenefitBatchUI.getResult().get(0);
    if (tULIBenefitBatchUI.mErrors.needDealError())
    {
      for(int n=0;n<tULIBenefitBatchUI.mErrors.getErrorCount();n++)
      {
        if(n!=0)
        {
          Content=Content+"||";
        }
        Content=Content+tULIBenefitBatchUI.mErrors.getError(n).errorMessage;
      }
      System.out.println(Content);
      FlagStr="Fail";
    }
    else
    {
      Content="满期给付抽档操作成功！";
      FlagStr="Succ";
    }
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=batchNo%>");
</script>
<body>
</body>
</html>

