<%
//程序名称：IndiDueFeeQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.*"%>  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>  
<%  
  //保存起始日期  
  String startDate = request.getParameter("StartDate"); 
  String endDate = request.getParameter("EndDate");  
	String bankcode = request.getParameter("bankcode"); 
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    LCPolSchema  tLCPolSchema = new LCPolSchema();  // 个人保单表
    tLCPolSchema.setGetStartDate(startDate);//将判断条件设置在起领日期字段中
    tLCPolSchema.setPayEndDate(endDate); ////将判断条件设置在终交日期字段中
    tLCPolSchema.setBankCode(bankcode); ////将判断条件设置
	
    VData tVData = new VData();
    tVData.add(tLCPolSchema);
    tVData.add(tGI);
    NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
    tNewIndiDueFeeMultiUI.submitData(tVData,"INSERT");
    if (tNewIndiDueFeeMultiUI.mErrors.needDealError())
    {
      Content="处理完成：";
      
      for(int n=0;n<tNewIndiDueFeeMultiUI.mErrors.getErrorCount();n++)
      {       
       Content=Content+tNewIndiDueFeeMultiUI.mErrors.getError(n).errorMessage;
       Content=Content+"|";
       System.out.println(Content);
      }
      FlagStr="Fail";
    }
    else
    {
      Content="处理成功完成！";
      FlagStr="Succ";
    }
   }//页面有效区
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>

