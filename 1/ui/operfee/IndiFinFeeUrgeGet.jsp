<html> 
<%
//程序名称：IndiFinFeeUrgeGet.jsp
//程序功能：个人即时交费和批量交费（即个人的续期催收交费核销）
//创建日期：2002-10-3 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="IndiFinFeeUrgeGet.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="IndiFinFeeUrgeGetInit.jsp"%>
</head>
<body >
<Form name=fm action="./IndiFinVerifyUrgeGet.jsp" method=post  target=fraSubmit>
    <table class= common>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divIndiFinFeeUrge);">
      </td>
      <td class= titleImg> 个人即时核销</td>
     </tr>
    </table>
    <Div  id= "divIndiFinFeeUrge" style= "display: ''">
     <TABLE class= common>
       <TR  class= common>
          <TD  class= title>
            暂交费收据号码
          </TD>
          <TD  class= input>
            <Input class= common name=TempFeeNo >
            <Input class= common type=Button value="核销" onclick="fmSubmit();" >
          </TD>
        </TR>
      </TABLE>
    </Div>     
</Form>
<Form name=fm2 action="./IndiFinVerifyUrgeGetByPolNo.jsp" method=post  target=fraSubmit>
    <table class= common>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divIndiFinFeeUrge);">
      </td>
      <td class= titleImg> 个人即时核销</td>
     </tr>
    </table>
    <Div  id= "divIndiFinFeeUrge" style= "display: ''">
     <TABLE class= common>
       <TR  class= common>
          <TD  class= title>
            保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
            <Input class= common type=Button value="核销" onclick="fmSubmit2();" >
          </TD>
        </TR>
      </TABLE>
    </Div>     
</Form>
<Form name=fmMult action=./MultFinVerifyUrgeGet.jsp target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->      	
    <table class= common>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divMultFinFeeUrge);">
      </td>
      <td class= titleImg>个人批理核销(需录入暂交费)</td>
     </tr>
    </table>
    <Div  id= "divMultFinFeeUrge" style= "display: ''">
      <Table  class= common>
        <TR  class= common>
          <TD  class= title>
            暂交费交费日期范围(当天及之前)
          </TD>
        <TD class= input>
          <INPUT class= common VALUE="核销" TYPE=button onclick= "fmMultsubmit();" >           
        </TD>     
        </TR>  
<!--        <TR class= common>
         <TD  class= title>
            起始日期 
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="StartDate" >           
          </TD>
        </TR> 
        <TR class= common>
         <TD  class= title>
          终止日期 
          </TD>        
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="EndDate" >                                 
          </TD>
        </TR>  -->
      </Table>    
    </Div>     		  			                                                                            
</Form>

<Form name=fmMultAuto action=./MultFinVerifyUrgeGetAuto.jsp target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->      	
    <table class= common>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divMultFinFeeUrge);">
      </td>
      <td class= titleImg>个人批理核销(余额抵交续期保费,无需录入暂交费)</td>
     </tr>
    </table>
    <Div  id= "divMultFinFeeUrge" style= "display: ''">
      <Table  class= common>
        <TR  class= common>
          <TD  class= title>
            应收表缴费日期范围(当天至2个月后)
          </TD>
        <TD class= input>
          <INPUT class= common VALUE="核销" TYPE=button onclick= "fmMultsubmitAuto();" >           
        </TD>          
        </TR> 
        <!-- 
        <TR class= common>
         <TD  class= title>
            起始日期 
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="StartDate" >           
          </TD>
        </TR> 
        <TR class= common>
         <TD  class= title>
          终止日期 
          </TD>        
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="EndDate" >                                 
          </TD>
        </TR> 
        --> 
      </Table>    
    </Div>     					                                                                            
</Form>


  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
