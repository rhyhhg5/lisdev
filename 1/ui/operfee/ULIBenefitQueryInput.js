//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var styleLength = 50;

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    //此处content可能较长，使用showModalDialog可能有问题，现将错误显示到页面
    fm.all("ErrorsInfo").innerHTML = content;
    
    content = "操作完毕，发生的错误信息见页面底部";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  initIndiContGrid();
  initPolGrid();
  initLjsGetGrid();
  initLjsGetDrawGrid();
  var tempCurrentTime=CurrentTime; //临时变量
  var tempSubTime=SubTime;  //临时变量
  //得到所选机构
  if(isNull(fm.ManageCom.value))
  {
    alert("请录入机构。");
    return fasle;
  }
  if(isNull(fm.all("StartDate").value))
  {
    alert("满期日期不能为空");
    return false;
  }
  if(isNull(fm.all("EndDate").value))
  {
    alert("满期日期不能为空");
    return false;
  }
  
  managecom = fm.ManageCom.value;
  CurrentTime=fm.all("StartDate").value;
  SubTime=fm.all("EndDate").value;
  
  var strSQL = "select serialno,(select otherno from ljsget where getnoticeno =a.getnoticeno), "
             + "a.makedate, min(operator),(select name from ldcom where comcode = a.managecom), "
             + "contno,(select name from lcinsured where contno = a.contno and insuredno = a.insuredno), "
             + "sum(getmoney),min(getdate),(select name from laagent where agentcode = a.agentcode), "
             + "agentgroup,(select codename('paymode',paymode) from ljsget where getnoticeno =a.getnoticeno),'', "
             + "(select case when b.dealstate='0' and c.confdate is null then '待给付' when ((b.dealstate='0' " 
             + "and c.confdate is not null) or b.dealstate='1') then '给付完成' when dealstate ='2' then '已撤销' end "
             + "from ljsget b,ljaget c where b.getnoticeno = a.getnoticeno and c.actugetno = a.getnoticeno), "
             + "(select case when printtimes is null or printtimes=0 then '未打印' else '已打印' end "
             + "from loprtmanager where code='OM003' and standbyflag2 = a.getnoticeno ), a.GetNoticeNo "
             + "from ljsgetdraw a where 1=1 "
             + "and feefinatype != 'YEI' "
             + "and exists(select 1 from ljaget where a.getnoticeno = actugetno and othernotype ='20') and ";
  
  var subSql = " a.curgettodate between '"+CurrentTime+"' and '"+SubTime+"' "
             + "and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') ";
  if(fm.all("BatchDate").value !="")
    subSql += " and a.makedate = '"+fm.all("BatchDate").value+"' ";
  if(fm.all("Operator").value !="")
    subSql += " and a.Operator = '"+fm.all("Operator").value+"' ";
  if(fm.all("ManageCom").value !="")
    subSql += " and a.ManageCom like '"+fm.all("ManageCom").value+"%' ";
  if(fm.all("PayMode").value !="")
    subSql += " and exists (select getnoticeno from ljsget where getnoticeno = a.getnoticeno and PayMode ='"+fm.all("PayMode").value+"') ";
  if(fm.all("ContNo").value !="")
    subSql += " and a.contno ='"+fm.all("ContNo").value+"' ";
  if(fm.all("AppntNo").value !="")
    subSql += " and a.AppntNo ='"+fm.all("AppntNo").value+"' ";
  if(fm.all("AgentGroup").value !="")
    subSql += " and a.AgentGroup ='"+fm.all("AgentGroup").value+"' ";
  if(fm.all("AgentCode").value !="")
    subSql += " and a.AgentCode ='"+fm.all("AgentCode").value+"' ";
  if(fm.all("PayTaskState").value !="")
    subSql += " and exists (select getnoticeno from ljsget where getnoticeno = a.getnoticeno and dealstate ='"+fm.all("PayTaskState").value+"') ";
  if(fm.all("PrintState").value =="0")
    subSql += " and exists (select standbyflag2 from loprtmanager where code = 'OM003' and standbyflag2 = a.getnoticeno and printtimes = 0 )";
  if(fm.all("PrintState").value =="1")
    subSql += " and exists (select standbyflag2 from loprtmanager where code = 'OM003' and standbyflag2 = a.getnoticeno and printtimes >= 1 ) ";
  
  strSQL += subSql;
  strSQL +=" group by getnoticeno,contno,insuredno,serialno,agentcode,agentgroup,managecom,polno,a.makedate with ur";	
  
  fm.QuerySql.value = subSql;
  turnPage1.pageDivName = "divPage1";
 // turnPage1.pageLineNum = 1;
  turnPage1.queryModal(strSQL, LjsGetGrid);
  contCurrentTime=CurrentTime;
  contSubTime=SubTime;
  fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
  fm.all("EndDate").value=contSubTime;
  CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
  SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
  if(LjsGetGrid.mulLineCount == 0)
  {
    document.all("divInfo").style.display = "none";
    alert("没有符合条件的满期任务");
  }
  else
  {
    document.all("divInfo").style.display = "";
  }
}

function getpoldetail()
{
  var arrReturn = new Array();
  var tSel = IndiContGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择一条记录，再点击返回按钮。" );
  }	
  else
  {
    getPolInfo();
  }  	                                          
}

function getPolInfo()
{
  var tRow = IndiContGrid.getSelNo() - 1;	        
  var tContNo=IndiContGrid.getRowColData(tRow,1);  
  var strSQL = "select a.RiskSeqNo,a.InsuredName, (select riskname  from lmrisk where riskcode=a.riskcode) ," 
             + "a.RiskCode,codeName('payintv', char(a.PayIntv)), "
             + "a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate "  
             + "from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
             + "and a.polno = b.polno and b.getdutycode = c.getdutycode "
             + "and a.contno='" + tContNo + "' and a.appflag='1' "
             + "group by a.polNo,a.RiskSeqNo,a.InsuredName,a.RiskCode, a.prem, a.CValiDate, a.PaytoDate, a.PayEndDate,a.enddate,a.PayIntv "
             + "order by a.RiskSeqNo ";		 
  turnPage3.pageDivName = "divPage3";
  turnPage3.queryModal(strSQL, PolGrid); 
}


//点击后查询保单
function afterContQuery(contno)
{
		// 初始化表格
  initIndiContGrid();
  var strSQL = "select a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.cinvalidate,'', "
             + "ShowManageName(a.ManageCom),a.agentgroup,a.AgentCode "
             + "from LCCont a "
             + "where a.AppFlag='1' and a.contno = '"+contno+"' ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSQL, IndiContGrid); 
  initPolGrid(); 
}

function getContInfo()
{
  var tSel1 = IndiContGrid.getSelNo();
  if( tSel1 == 0 || tSel1 == null )
  {		
    alert( "请先选择一条记录，再点击保单明细按钮。" );
  }
  else
  {
    var cContNo = IndiContGrid.getRowColData( tSel1 - 1, 1);		  
    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType=1");	
  }
}
function getLjsGetDrawdetail()
{
  var count = 0;
  var tChked = new Array();
  for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
  {
    if(LjsGetGrid.getChkNo(i) == true)
    {
      tChked[count++] = i;
    }
  }
  if(tChked.length <1 )
  {
    return false;	
  }
  
  var taskNo=LjsGetGrid.getRowColData(tChked[0],2);
  
  var SQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO FROM LJSGETDRAW WHERE GETNOTICENO =(SELECT GETNOTICENO FROM LJSGET WHERE OTHERNOTYPE ='20' AND OTHERNO ='"+taskNo+"')";
  turnPage.queryModal(SQL, LjsGetDrawGrid);
  for(var i = 0; i < LjsGetDrawGrid.mulLineCount; i++)
  {
    var sql = "select name from lcbnf where polno ='"+LjsGetDrawGrid.getRowColData(i,7)+"'";
    var rs = easyExecSql(sql);
    var bnf = "";
    if(rs!=null)
    {
      for(var j =0;j<=rs.length-2;j++)
      {
        bnf+=rs[j]+'、';
      }
      if(j==rs.length-1)
      {
        bnf+=rs[j];
      }
    }
    LjsGetDrawGrid.setRowColData(i,3,bnf);
  }
  afterContQuery(LjsGetDrawGrid.getRowColData(0,1));
}

//打印给付清单
function printGetList()
{
  if(LjsGetGrid.mulLineCount == 0)
  {
      alert("满期任务清单没有数据，请先查询");
      return false;
  }
  fm.target="_blank";
  fm.action="./PrintGetList.jsp";
  fm.submit();
}
//给付任务撤销
function getTaskCansel()
{
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行撤销");
		return false;	
	}	
	var taskNo=LjsGetGrid.getRowColData(tChked[0],2);
	fm.action = "GetTaskCansel.jsp?taskNo="+taskNo;
	fm.submit();
}
//给付方式变更
function changePayMode()
{
  if(fm.all("PayModeChangeID").style.display == "")
  {
    fm.all("PayModeChangeID").style.display = "none";
    return false;
  }
  
  var chkNum = 0;
  var tGetNoticeNo = "";
  var tContNoForModify = "";
  for (i = 0; i < LjsGetGrid.mulLineCount; i++)
  {
    if (LjsGetGrid.getChkNo(i)) 
    {
      chkNum = chkNum + 1;
      if (chkNum == 1)
      {
        tContNoForModify = LjsGetGrid.getRowColDataByName(i, "contNoForModify");
        tGetNoticeNo = LjsGetGrid.getRowColDataByName(i, "GetNoticeNo");
      }
    }
  }
  
  if(chkNum == 0)
  {
    alert("请选择需要变更的记录");
    return false;
  }
  if(chkNum != 1)
  {
    alert("只能选择一条记录进行变更");
    return false;
  }
  
  fm.GetNoticeNo.value = tGetNoticeNo;
  fm.contNoForModify.value = tContNoForModify ;
  fm.all("PayModeChangeID").style.display = "";
  var rs = getPayModeInfo(tGetNoticeNo);
  if(rs)
  {
    fm.PayModeOld.value = rs[0][0];
    fm.PayModeOldName.value = rs[0][1];
    fm.BankCodeOld.value = rs[0][2];
    fm.BankNameOld.value = rs[0][3];
    fm.AccNameOld.value = rs[0][4];
    fm.BankAccNoOld.value = rs[0][5];
  }
}

//变更给付方式确认
function changePayModeSubmit()
{
  if(fm.BankAccNo.value != fm.BankAccNo2.value)
  {
    alert("您两次录入的账号不同，请核对");
    return false;
  }
  
  if(fm.PayModeOld.value == fm.PayModeNew.value
    && fm.BankCodeOld.value == fm.BankCode.value
    && fm.AccNameOld.value == fm.AccName.value
    && fm.BankAccNoOld.value == fm.BankAccNo.value)
  {
    alert("你录入的新给付方式与原给付方式一致，不需要变更");
    return false;
  }
  
  if(fm.PayModeNew.value == "")
  {
    alert("请选择给付方式");
    return false;
  }
  else if(fm.PayModeNew.value == "3" || fm.PayModeNew.value == "4")
  {
    if(fm.BankCode.value == "" || fm.AccName.value == "" || fm.BankAccNo.value == "")
    {
      alert("银行账户信息必须录入完整");
      return false;
    }
  }
  else if(fm.PayModeNew.value == "1" || fm.PayModeNew.value == "2")
  {
    if(fm.BankCode.value != "" || fm.AccName.value != "" || fm.BankAccNo.value != "")
    {
      alert("您所选给付方式不需要录入帐户信息，请清空");
      return false;
    }
  }
  var rs = getPayModeInfo(fm.GetNoticeNo.value);
  if(rs[0][0] == fm.PayModeNew.value && rs[0][2] == fm.BankCode.value
    && rs[0][4] == fm.AccName.value && rs[0][5] == fm.BankAccNo.value)
  {
    alert("您录入的信息已进行了变更");
    return false;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var fmAction = fm.action;
  fm.action = "expirBenefitChangePayModeSave.jsp";
  fm.submit();
  fm.action = fmAction;
  return true;
}

//查询给付方式信息
function getPayModeInfo(cGetNoticeNo)
{
  var sql = "select PayMode, CodeName('paymode', a.PayMode), "
          + "   BankCode, (select BankName from LDBank where BankCode = a.BankCode), "
          + "   AccName, BankAccNo "
          + "from LJSGet a "
          + "where GetNoticeNo = '" + cGetNoticeNo + "' ";
  var rs = easyExecSql(sql);
  return rs;
}

function afterCodeSelect(cCodeName, Field)
{
  if(cCodeName=="PayMode2")
  {
    if(fm.PayModeNew.value != "4")
    {
      fm.BankCode.value = "";
      fm.BankName.value = "";
      fm.AccName.value = "";
      fm.BankAccNo.value = "";
      fm.BankAccNo2.value = "";
    }
  }
}

function printNotice()
{
  if (LjsGetGrid.mulLineCount == 0)
  {
    alert("打印列表没有数据");
    return false;
  }
  fm.action="./ULIBenefitPrint.jsp";
  fm.submit();
}
	
function afterSubmit2(FlagStr,Content)
{
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
   	 //window.parent.close(); 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

function isNull(s)
{
  var t = trim(s);
  if(t==""||t==null)
  {
    return true;
  }
  else
  {
    return false;
  }
}