//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
   var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
	
  }
  initForm();
}

//撤销
function GrpCancel()
{
  var tSel = GrpPolGrid.getSelNo();
  if( tSel == 0 || tSel == null )
		alert( "请先选择要撤销的保单!" );
  else
  {
	  fm.all('GrpContNo').value = GrpPolGrid.getRowColData( tSel - 1, 1 );
	  fm.all('PrtNo').value = GrpPolGrid.getRowColData( tSel - 1, 2 );
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
      fm.submit();
  }
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	var strSQL="select grpcontno,prtno,grpname,cvalidate from lcgrpcont"
	          +" where appflag='1' and grpcontno in (select otherno from ljspay where othernotype='1')"
	          + getWherePart( 'GrpContNo')
				    + getWherePart( 'PrtNo' )
				    + " and ManageCom like '" + manageCom + "%%'";
  //	var strSQL = "select g.GrpContNo,g.PrtNo,g.GrpName,g.CValiDate from lcgrpcont g,ljspay j "
	//   + "where j.otherno=g.grpcontno "
	//   + "and j.othernotype ='1'";
	/*   // + "and not exists(select otherno from ljtempfee where TempFeeType='2' and OtherNoType='1' and OtherNo=j.OtherNo)";
	if(trim(fm.all('GrpContNo').value).length == 0)
	{
	}
    else if(trim(fm.all('GrpContNo').value).length >0 && fm.all('GrpContNo').value.length !=20)
	{
		alert("集体保单号错误!请输入正确的集体保单号!");
		return;
	}
    else
    {
    	strSQL = strSQL + " and g.grppolno = '" + fm.all('GrpContNo').value + "'";
    }*/

 turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查到数据！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = GrpPolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
	
}

//团体保单明细
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择要查询的保单!" );
	else
	{
		var cGrpPrtNo = GrpPolGrid.getRowColData( tSel - 1, 2 );
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?PrtNo="+ cGrpPrtNo);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}