//IndiManuUWCont.jsp

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

/* 查询送核保单信息 */
function queryContInfo()
{
  if(fm.ManageCom.value == "")
  {
    alert("请录入管理机构");
    return false;
  }
  var condition = "";
  if(fm.StartDate.value != "")
  {
    condition = condition + "   and b.makeDate >= '" + fm.StartDate.value + "' ";
  }
  if(fm.EndDate.value != "")
  {
    condition = condition + "   and b.makeDate <= '" + fm.EndDate.value + "' ";
  }
  if(fm.AgentCode.value != "")
  {
   // condition = condition + "   and a.AgentCode = '" + fm.AgentCode.value + "' ";
    condition = condition + "   and a.AgentCode = getAgentcode('" + fm.AgentCode.value + "') ";                                               
  }
  if(fm.serialNo.value != "")
  {
    condition = condition + "   and exists (select 1 from LJSPayPersonB x where x.contNo = a.contNo and x.serialNo = '" + fm.serialNo.value + "') ";
  }
  if(fm.ContNo.value != "")
  {
    condition = condition + "   and a.contNo like '%%" + fm.ContNo.value + "%%' ";
  }
  
	//书写SQL语句
	var strSQL = " select distinct a.manageCom, c.name, getUniteCode(c.agentCode), c.phone, a.contNo, a.appntName, "
	            + "   a.payToDate, a.prem, b.makeDate, "
	            + "   (select remark from LGWork where workNo = b.edorNo), "
	            + "   (select max(modifyDate) from LPUWMaster where edorNo = b.edorNo), "
	            +"    '', b.edorNo, c.agentGroup "
	            + "from LCCont a, LPEdorItem b, LAAgent c "
	            + "where a.contNo = b.contNo "
	            + "   and a.agentCode = c.agentCode "
	            + "   and b.edorType = 'XB' "
	            + "   and a.manageCom like '" + fm.ManageCom.value + "%%' "
	            + condition
	            + getWherePart( 'c.AgentGroup','AgentGroup' );
	            + getWherePart( 'a.AgentCode','AgentCode' );
	            + getWherePart( 'a.ContNo','ContNo' )
	            + " order by a.manageCom, c.agentGroup, c.agentCode ";
	
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, ContGrid);
	
	if(ContGrid.mulLineCount == 0)
	{
	  alert("没有查询到数据。");
	  return false;
	}
	showUWIdea();
	showCodeName();
	
	return true;
}
//因为查询核保结论复杂，所以在查询到核保数据后再生成核保结论
function showUWIdea()
{
	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{
	  var edorNo = ContGrid.getRowColDataByName(i, "edorNo");
	  var sql = "select b.riskCode, a.sugUWIdea "
	            + "from LPUWMaster a, LPPol b "
	            + "where a.polNo = b.polNo "
	            + "   and a.edorNo = b.edorNo "
	            + "   and b.edorNo = '" +  + edorNo + "' ";
	  var rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    var idea = "";
	    for(var t = 0; t < rs.length; t++)
	    {
	      idea = idea + rs[t][0] + ": " + rs[t][1];
	    }
	    ContGrid.setRowColDataByName(i, "uwIdea", idea);
	  }
	}
}