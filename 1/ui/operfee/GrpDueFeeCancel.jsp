 <html> 
<%
//程序名称：GrpDueFeeCancel.jsp
//程序功能：集体保费催收的撤销
//创建日期：2004-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpDueFeeCancel.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="GrpDueFeeCancelInit.jsp"%>
</head>
<body onload="initForm();">
<form name=fm action=./GrpDueFeeQueryCancel.jsp target=fraSubmit method=post>
<table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 集体合同号码 </TD>
          <TD  class= input> <Input class= common name=GrpContNo >  </TD>
         
          <TD  class= title>  印刷号码 </TD>
          <TD  class= input>  <Input class= common name=PrtNo ></TD>
          
        </TR>       
    </table>
          <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick();">     
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 团体保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class = cssbutton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class = cssbutton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	
  
	<p>
 　　<INPUT VALUE="团单续期个案撤销" TYPE=button class = cssbutton onclick="GrpCancel();">   
    <INPUT VALUE="团体保单明细" class = cssbutton TYPE=button onclick="returnParent();"> 
	</p>
			
    
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
