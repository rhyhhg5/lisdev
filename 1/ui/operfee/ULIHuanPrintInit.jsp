<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>	

<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>

<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
  var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
 
//初始化表单
function initForm()
{ 
  try 
  {
    var sql = "select Current Date - 3 month, Current Date from dual "; 
    var rs = easyExecSql(sql);
    fm.all('StartDate').value = rs[0][0];
    fm.all('EndDate').value = rs[0][1];
    fm.all('ManageCom').value = <%=tGI.ManageCom%>;
    showAllCodeName();
    initLCPolGrid(); 
  }
  catch(re) 
  {
    alert("在ULIUnlockHuanInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//Mulline的初始化
function initLCPolGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=3;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="保单号";      //列名
    iArray[1][1]="130px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="ContNo";
   
    iArray[2]=new Array();
    iArray[2][0]="投保人";      //列名
    iArray[2][1]="50px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="投保日期";      //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[4]=new Array();
    iArray[4][0]="交至日期";      //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[5]=new Array();
    iArray[5][0]="保单状态";      //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="期缴保费";      //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="业务员";      //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="缓交状态";      //列名 
    iArray[8][1]="10px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=3;             //是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="Huan";       //1 -- 缓交状态
    
    iArray[9]=new Array();
    iArray[9][0]="打印状态";      //列名 
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="投保人号";      //列名 
    iArray[10][1]="10px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=3;             //是否允许输入,1表示允许，0表示不允许
    iArray[10][21]="AppntNo";
    
    LCPolGrid = new MulLineEnter("fm", "LCPolGrid"); 
	  //设置Grid属性
    LCPolGrid.mulLineCount = 0;
    LCPolGrid.displayTitle = 1;
    LCPolGrid.locked = 1;
    LCPolGrid.canSel = 1;	
    LCPolGrid.canChk = 0;
    LCPolGrid.hiddenSubtraction = 1;
    LCPolGrid.hiddenPlus = 1;
    LCPolGrid.loadMulLine(iArray);
    LCPolGrid.selBoxEventFuncName ="selectOne";
  }
  catch(ex)
  {
  	alert("在ULIUnlockHuanInit.jsp-->initLCPolGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
