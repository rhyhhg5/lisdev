<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： IndiDueFeeAllBatPrt.jsp
//程序功能： 实现个险续期批量催收时PDF全部打印
//创建日期： 2007-06-15
//创建人  ： Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
	boolean operFlag=true;
	int tCount = 0;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	String ContNo="";
	String tPrintServerPath = "";	
	String strsql = request.getParameter("strsql");
	System.out.println("-------------------"+strsql);
	String tOutXmlFile  = application.getRealPath("")+"\\";
	LJSPayBSet tLJSPayBSet = new LJSPayBSet();

	String arrPtrList[][];
	ExeSQL tPrtList= new ExeSQL();
  SSRS tSSRS = tPrtList.execSQL(strsql);
  arrPtrList = tSSRS.getAllData();

	int ContGridCount = tSSRS.getMaxRow();
	for(int j = 0; j < ContGridCount; j++)
	{
			LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
			tLJSPayBSchema.setGetNoticeNo(arrPtrList[j][11]);
			tLJSPayBSet.add(tLJSPayBSchema);
			System.out.println("---------GetNoticeNo: "+arrPtrList[j][11]);
	}
	CErrors mErrors = new CErrors();
	GlobalInput tG = new GlobalInput();      
	tG = (GlobalInput)session.getValue("GI");
	
	LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
	tLDSysVarSechma.setSysVarValue(tOutXmlFile);
	
	VData tVData = new VData();
	tVData.addElement(tLDSysVarSechma);
	tVData.addElement(tG);
	tVData.addElement(tLJSPayBSet);

	IndiDueFeeBatchPrtBL tIndiDueFeeBatchPrtBL = new IndiDueFeeBatchPrtBL();
	
	if(tIndiDueFeeBatchPrtBL.submitData(tVData,"PRINT"))
	{
		tCount = tIndiDueFeeBatchPrtBL.getCount();
		String tFileName[] = new String[ContGridCount];
		VData tResult = new VData();
		tResult = tIndiDueFeeBatchPrtBL.getResult();
		tFileName=(String[])tResult.getObject(0);
	
		String mFileNames = "";
		for (int i = 0;i<=(tCount-1);i++)
		{
	  		System.out.println("~~~~~~~~~~~~~~~~~~~"+i);
	  		System.out.println(tFileName[i]);
	  		mFileNames = mFileNames + tFileName[i]+":";
		}
	  System.out.println("===================="+mFileNames+"=======================");
		System.out.println("=========tFileName.length==========="+tFileName.length);
	
		String strRealPath = application.getRealPath("/").replace('\\','/');
		String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
		ExeSQL mExeSQL = new ExeSQL();
		tPrintServerPath =  mExeSQL.getOneValue(sql);
	%>
<html> 	
	<script language="javascript">
		var printform = parent.fraInterface.document.getElementById("printform");
		printform.elements["filename"].value = "<%=mFileNames%>";
		printform.action = "<%=tPrintServerPath%>";
		printform.submit();
	</script>
</html>
<%         			
	}
%>
<html>
<script language="javascript">	
if("<%=operFlag%>" == "true")
{	
	alert("打印成功");
	}
	else{
		alert("<%=Content%>");
}
</script>
</html>