
//程序名称：GrpEndDateDeal.jsp
//程序功能：团单满期处理页面时间响应文件
//创建日期：20060901 
//创建人  ：Yangyalin
//更新记录：  更新人    更新日期     更新原因/内容


var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var msql ="" ;


//查询满期保单
function queryGrpCont()
{
  if(!checkData())
  {
    return false;
  }
  
  var grpContNoPart = "";  //保单号查询条件
  if(trim(fm.GrpContNo.value) != "")
  {
    grpContNoPart = "   and grpContNo like '%" + fm.GrpContNo.value + "%' ";
  }
  var agentCodePart = "";  //保单号查询条件
  if(trim(fm.AgentCode.value) != "")
  {
    agentCodePart = "   and getUniteCode(a.AgentCode) like '%%" + fm.AgentCode.value + "%%' ";
  }
  var startDate = "";
  var endDate = "";
  
  if(trim(fm.StartDate.value) != ""&&trim(fm.EndDate.value) != "")
  {
  	startDate = "   and CInvalidate between '"
		          + fm.StartDate.value + "' and '"
		          + fm.EndDate.value + "' ";
  }
  else
  	{
		  if(trim(fm.StartDate.value) != "")
		  {
		    startDate = "   and CInvalidate >= '" + fm.StartDate.value + "' ";
		  }
		  
		  if(trim(fm.EndDate.value) != "")
		  {
		    endDate = "   and CInvalidate <= '" + fm.EndDate.value + "' ";
		  }
		}
  
  //组合险种查询条件
  var riskCodePart = "";
  var riskCode1 = fm.RiskCode1.value;
  var riskCode2 = fm.RiskCode2.value;
  if(riskCode1 != "" && riskCode2 != "")
  {
    //或
    if(fm.Condition.value == "1")
    {
      riskCodePart = " and exists "
                    + "   (select 1 from LCGrpPol "
                    + "   where riskCode in ('" + riskCode1 + "', '" + riskCode2 + "') "
                    + "       and grpContNo = a.grpContNo) ";
    }
    else
    {
      riskCodePart = "  and exists "
                    + "     (select 1 from LCGrpPol "
                    + "     where riskCode in ('" + riskCode1 + "') "
                    + "         and grpContNo = a.grpContNo) "
                    + " and exists "
                     + "     (select 1 from LCGrpPol "
                    + "     where riskCode in('" + riskCode2 + "') "
                    + "         and grpContNo = a.grpContNo) ";
    }
  }
  else if(riskCode1 != "")
  {
    riskCodePart = "  and exists "
                    + "     (select 1 from LCGrpPol "
                    + "     where riskCode in ('" + riskCode1 + "') "
                    + "         and grpContNo = a.grpContNo) ";
  }
  else if(riskCode2 != "")
  {
    riskCodePart = "  and exists "
                    + "     (select 1 from LCGrpPol "
                    + "     where riskCode in ('" + riskCode2 + "') "
                    + "         and grpContNo = a.grpContNo) ";
  }
  
  
  //"grpContNo"作为数据标志和数据内容的分隔符，在打印程序中去它前面的保单号来进行必要的校验
  var sql = "  select grpContNo, 'grpContNo', manageCom, "
            + " (select min(Name) from LaBranchGroup where AgentGroup =(select min(AgentGroup) from LCGrpCont where GrpContNo = a.GrpContNo)), "
            + " (select Name from LAAgent where agentCode = a.agentCode), "
            + "   grpName, codeName('businesstype',a.businessType), "
            + "   grpContNo, varchar(prem), polApplyDate, "
            + "   CInvalidate,"
            + "   (select max(payToDate) from LCGrpPol where grpContNo = a.grpcontNo), "
            //+ "   varchar((select sum(sumActuPayMoney) from LJAPayGrp where grpContNo = a.grpContNo)),"
            + "   varchar(abs((select nvl((select sum(sumActuPayMoney) from LJAPayGrp where grpContNo = a.grpContNo), 0) + nvl((select sum(getMoney) from LJAGetEndorse where grpContNo = a.grpContNo and getMoney > 0 and feeOperationType != 'NI'), 0) from dual))), "  //实收保费+保全缴费（其中增人保费已存入实收保费表）
            + "   varchar(abs(nvl((select sum(getMoney) from LJAGetEndorse where grpContNo = a.grpContNo and getMoney < 0), 0))), "
            //+ "   varchar(abs(nvl((select sum(pay) from LJAGetClaim where confDate is not null and contNo in(select contno from LCCont where grpContNo = a.grpContNo)) , 0))), "
            +" varchar(abs(nvl((select sum(pay) from LJAGetClaim lja where lja.grpcontno = a.grpcontno and OtherNoType = '5' ) , 0))), "
            + "   peoples2 "           
            + "from LCGrpCont a "
            + "where appflag = '1' "
            + "   and a.manageCom like '" + fm.UserManageCom.value + "%%' "
            + "	  and a.manageCom like '" + fm.ManageCom.value + "%%' "
            + "		and not exists (select 1 from LJSPay where otherNo = a.grpContNo ) "
            + grpContNoPart
            + startDate
            + endDate
            + agentCodePart
            + riskCodePart
            ;
  //document.all("test").innerHTML = sql;            
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql, GrpEndDateGrid);
  msql = sql;
  
  if(GrpEndDateGrid.mulLineCount == 0)
  {
    alert("没有查询到符合条件的保单数据。");
    return false;
  }
  fm.Sql.value = sql;
  
  showCodeName();
}

//校验录入数据的合法性
function checkData()
{
  if(!verifyInput2())
  {
    return false;
  }
  if(!isDate(fm.StartDate.value) || !isDate(fm.EndDate.value))
  {
    alert("开始时间和结束时间都必须为日期格式\"yyyy-mm-dd\"");
    return false;
  }
  
  //  
  var uerManageCom = fm.UserManageCom.value;
  var manageCom = fm.ManageCom.value;
  if(manageCom.indexOf(uerManageCom) < 0)
  {
  	alert("您只能查询本机构及以下机构的保单");
  	return false;
 	}
  return true;
}

//打印清单
function printList()
{
  if(GrpEndDateGrid.mulLineCount == 0)
  {
    alert("请先查询单数据");
    return false;
  }
  
	fm.action = "GrpEndDateDealListPrint.jsp";
	fm.target = "_blank";
	fm.submit();
}



  
//保单明细
function contDetail()
{
  var grpContNo = "";  //选中的保单号
  
  if(GrpEndDateGrid.mulLineCount == 0)
  {
    alert("请先查询");
    return false;
  }
  
  var count = 0;   //选中的保单数
  for (i = 0; i < GrpEndDateGrid.mulLineCount; i++)
	{
		if (GrpEndDateGrid.getChkNo(i)) 
		{
			count = count + 1;
			if(count > 1)
			{
			  alert("单张打印只能选择一个保单记录。");
			  return false;
			}
			grpContNo = GrpEndDateGrid.getRowColDataByName(i, "grpContNo");
		}
	}
	
	if(count == 0)
	{
	  alert("请选择保单");
	  return false;
	}
	
	window.open("../sys/GrpPolDetailQueryMain.jsp?ContType=1&ContNo=" + grpContNo, "ContDetail");
}

//选择打印
function printMult()
{
	var grpContNo;
  if(GrpEndDateGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  var count = 0;   //选中的保单数
  var tChked = new Array();
  for (i = 0; i < GrpEndDateGrid.mulLineCount; i++)
	{
		if (GrpEndDateGrid.getChkNo(i)==true) 
		{
			tChked[count++] = i;
		  grpContNo = GrpEndDateGrid.getRowColDataByName(i, "grpContNo");
		}
	}
	if(tChked.length != 1)
	{
	  alert("请选择一条记录");
	  return false;
	}
	
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '98' and otherno='"+grpContNo+"'");
	
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.all('GrpContNo').value = grpContNo ;
		fm.action="./GrpEndDateDealNoticePrint.jsp";
		fm.submit();
	}
	/*
	else
	{
		printPDF();		
	}
  */
		
	//单打
	/*
	if(count == 1)
	{
		//fm.action="../uw/PrintPDFSave.jsp?Code=098&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
	  //fm.action = "GrpEndDateDealNoticePrint.jsp?grpContNo=" + grpContNo;
	  fm.target = "_blank";
	  fm.submit();
	}
  else
  {
    alert("尚未实现，请选择一个保单进行打印。");
  }
*/
}


//全部打印
function printAll()
{
  if (GrpEndDateGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpEndDateGrid.mulLineCount; i++)
	{
		if(GrpEndDateGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.Sql1.value = msql;
		fm.action="./GrpexpiryAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		//alert("开始调用 GrpexpiryBatPrt.jsp ");
		fm.action="./GrpexpiryBatPrt.jsp";
		fm.submit();
	}
}


function printPDF()
{
	var count = 0;
	var grpContNo;
	var tChked = new Array();
  for (i = 0; i < GrpEndDateGrid.mulLineCount; i++)
	{
		if (GrpEndDateGrid.getChkNo(i)==true) 
		{
			tChked[count++] = i;
		  grpContNo = GrpEndDateGrid.getRowColDataByName(i, "grpContNo");
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
//	var grpContNo = GrpEndDateGrid.getRowColData( tChked[0], 1 );

	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '98' and otherno='"+grpContNo+"'");

	fm.action="../uw/PrintPDFSave.jsp?Code=098&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
	fm.submit();
//	window.open("../uw/PrintPDFSave.jsp?Code=098&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq);
//	window.open("../uw/PrintPDFSave.jsp?Code=090&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq );	

}

function printMultNew()
{
	var grpContNo;
  if(GrpEndDateGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  var count = 0;   //选中的保单数
  var tChked = new Array();
  for (i = 0; i < GrpEndDateGrid.mulLineCount; i++)
	{
		if (GrpEndDateGrid.getChkNo(i)==true) 
		{
			tChked[count++] = i;
		  grpContNo = GrpEndDateGrid.getRowColDataByName(i, "grpContNo");
		}
	}
	if(tChked.length != 1)
	{
	  alert("请选择一条记录");
	  return false;
	}
	var showStr="正在准备打印数据，请稍后...";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=98&OtherNo="+grpContNo;
	fm.submit();
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

function afterSubmit(FlagStr, content)
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}