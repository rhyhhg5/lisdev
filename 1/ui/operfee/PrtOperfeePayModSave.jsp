<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PrtContSuccessSave.jsp
//程序功能：
//创建日期：2007-1-15 20:37
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true;
  String flagStr = "";
  String content = "";

  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  TransferData t = new TransferData();
  t.setNameAndValue(FeeConst.STARTDATE, request.getParameter("StartDate"));
  t.setNameAndValue(FeeConst.ENDDATE, request.getParameter("EndDate"));
  t.setNameAndValue(FeeConst.MANAGECOM, request.getParameter("ManageCom"));
  
  VData d = new VData();
  d.add(tG);
  d.add(t);
         
  String loadFlag = request.getParameter("LoadFlag");
  XmlExport txmlExport = null;

  if("Deal".equals(loadFlag))
  {
    PrtOperfeeDealBL bl = new PrtOperfeeDealBL();
    txmlExport = bl.getXmlExport(d, "");
    if(txmlExport == null)
    {
      operFlag = false;
      content = bl.mErrors.getFirstError().toString();                 
    }
  }
  else
  {
    PrtOperfeePayModBL bl = new PrtOperfeePayModBL();
    txmlExport = bl.getXmlExport(d, "");
    if(txmlExport == null)
    {
      operFlag = false;
      content = bl.mErrors.getFirstError().toString();                 
    }
  }
  
  
	if (operFlag==true)
	{
	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();  
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);  
    session.putValue("PrintVts", dataStream);
	  session.putValue("PrintStream", txmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	flagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=content%>");
	top.close();
</script>
</html>
<%
  	}
%>