<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
	
   // fm.all('SerialNo').value = '';
    fm.all('GrpContNo').value = '';
    fm.all('PrintState').value  ='0'  
  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initGrpContGrid();
    initGrpJisPayGrid();	
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpContGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";            		//列宽
		  iArray[0][2]=10;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[1]=new Array();
		  iArray[1][0]="收据号";         		//列名
		  iArray[1][1]="80px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[2]=new Array();
		  iArray[2][0]="投保人姓名";         		//列名
		  iArray[2][1]="150px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		 	
		 	iArray[3]=new Array();
		  iArray[3][0]="投保人性别";         		//列名
		  iArray[3][1]="0px";            		//列宽
		  iArray[3][2]=100;            			//列最大值
		  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[4]=new Array();
		  iArray[4][0]="投保人电话";         		//列名
		  iArray[4][1]="0px";            		//列宽
		  iArray[4][2]=100;            			//列最大值
		  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[5]=new Array();
		  iArray[5][0]="保单号";         		//列名
		  iArray[5][1]="70px";            		//列宽
		  iArray[5][2]=200;            			//列最大值
		  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[6]=new Array();
		  iArray[6][0]="应收金额";         		//列名
		  iArray[6][1]="70px";            		//列宽
		  iArray[6][2]=200;            			//列最大值
		  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[7]=new Array();
		  iArray[7][0]="应缴时间";         		//列名
		  iArray[7][1]="80px";            		//列宽
		  iArray[7][2]=200;            			//列最大值
		  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[8]=new Array();
		  iArray[8][0]="实收金额";         		//列名
		  iArray[8][1]="70px";            		//列宽
		  iArray[8][2]=200;            			//列最大值
		  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许


		  iArray[9]=new Array();
		  iArray[9][0]="核销时间";         		//列名
		  iArray[9][1]="80px";            		//列宽
		  iArray[9][2]=200;            			//列最大值
		  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许


		  iArray[10]=new Array();
		  iArray[10][0]="核销人";         		//列名
		  iArray[10][1]="70px";            		//列宽
		  iArray[10][2]=200;            			//列最大值
		  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
		  iArray[11]=new Array();
		  iArray[11][0]="是否打印";         		//列名
		  iArray[11][1]="70px";            		//列宽
		  iArray[11][2]=200;            			//列最大值
		  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[12]=new Array();
		  iArray[12][0]="余额";         		//列名
		  iArray[12][1]="70px";            		//列宽
		  iArray[12][2]=200;            			//列最大值
		  iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[13]=new Array();
		  iArray[13][0]="缴费日期";         		//列名
		  iArray[13][1]="70px";            		//列宽
		  iArray[13][2]=200;            			//列最大值
		  iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[14]=new Array();
		  iArray[14][0]="代理人姓名";         		//列名
		  iArray[14][1]="70px";            		//列宽
		  iArray[14][2]=200;            			//列最大值
		  iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[15]=new Array();
		  iArray[15][0]="代理人性别";         		//列名
		  iArray[15][1]="70px";            		//列宽
		  iArray[15][2]=200;            			//列最大值
		  iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[16]=new Array();
		  iArray[16][0]="代理人手机";         		//列名
		  iArray[16][1]="70px";            		//列宽
		  iArray[16][2]=200;            			//列最大值
		  iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许

// added by suyanlin 2007/11/21
		  iArray[17]=new Array();
		  iArray[17][0]="代理人固话";         		//列名
		  iArray[17][1]="70px";            		//列宽
		  iArray[17][2]=200;            			//列最大值
		  iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许
// 
		  iArray[18]=new Array();
		  iArray[18][0]="销售机构";         		//列名
		  iArray[18][1]="70px";            		//列宽
		  iArray[18][2]=200;            			//列最大值
		  iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[19]=new Array();
		  iArray[19][0]="销售机构";         		//列名
		  iArray[19][1]="70px";            		//列宽
		  iArray[19][2]=200;            			//列最大值
		  iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许

		  
		  GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 
		  //这些属性必须在loadMulLine前
		  GrpContGrid.mulLineCount = 0;   
		  GrpContGrid.displayTitle = 1;
		  GrpContGrid.locked = 1;
		  GrpContGrid.canSel = 0;
		  GrpContGrid.canChk = 1;
		  GrpContGrid.hiddenPlus = 1;
		  GrpContGrid.hiddenSubtraction = 1;
		  GrpContGrid.loadMulLine(iArray);  
		  //GrpContGrid.selBoxEventFuncName ="afterJisPayQuery";      
      }
      catch(ex)
      {
        alert(ex);
      }
}
// 催收记录的初始化
function initGrpJisPayGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="核销批次号";         		//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="核销时间";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="操作人";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[4]=new Array();
      iArray[4][0]="当前状态";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

	    GrpJisPayGrid = new MulLineEnter( "fm" , "GrpJisPayGrid" ); 
      //这些属性必须在loadMulLine前
      GrpJisPayGrid.mulLineCount =0;   
      GrpJisPayGrid.displayTitle = 1;
      GrpJisPayGrid.hiddenPlus = 1;
      GrpJisPayGrid.hiddenSubtraction = 1;
      GrpJisPayGrid.locked = 1;
      GrpJisPayGrid.canChk = 1;
      GrpJisPayGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("GroupPolQueryInit.jsp-->initGrpJisPayGrid函数中发生异常:初始化界面错误!");
      }
}

</script>