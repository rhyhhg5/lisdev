<html> 
<%
//程序名称：FinVerify.jsp
//程序功能：应收个人交费表的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//            Yangh      2005-07-14  
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
 
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
      
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="IndiNormalPayVerify.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="IndiNormalPayVerifyInit.jsp"%>
</head>
<body onload="initForm();">
<Form action="./IndiNormalPayVerifySave.jsp" method=post name=fm target=fraSubmit>
   <table class=common>
       <TR  class= common>
          <TD  class= title>
            个人保单号
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo verify="个人保单号|NOTNULL&len=16">
          </TD>                                                  
          <TD  class= input>
            <Input class= cssButton type=Button value="查询" onclick="fmQuery();">
          </TD>
       </TR>                                    
  </table>
<tr>
</tr>
  <!--保费项明细 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanPremGrid" > </span> 
	</td>
       </tr>
    </Table>

          <TD  class= input>
            <Input class= cssButton type=Button value="核销" onclick="fmSubmit();">
          </TD>    
</Form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
