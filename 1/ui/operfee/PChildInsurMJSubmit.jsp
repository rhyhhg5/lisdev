<%
//程序名称：PChildInsurMJSubmit.jsp
//程序功能：
//创建日期：2008-2-26
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  String flag = "";
  String content = "";
  MMap tMMapLock = null;
  String getNoticeNo = request.getParameter("GetNoticeNo");
  String dealType = request.getParameter("DealType");
  GlobalInput tGI=(GlobalInput)session.getValue("GI");
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("getNoticeNo", getNoticeNo);
  tTransferData.setNameAndValue("dealType", dealType);
  tTransferData.setNameAndValue("customerBackDate", request.getParameter("CustomerBackDate"));
  //为满期给付准备
  tTransferData.setNameAndValue("payMode", request.getParameter("PayMode"));
  tTransferData.setNameAndValue("payDate", request.getParameter("PayDate"));
  tTransferData.setNameAndValue("bank", request.getParameter("Bank"));
  tTransferData.setNameAndValue("bankAccno", request.getParameter("BankAccno"));
  
  VData tVData = new VData();
  tVData.add(tGI);
  tVData.add(tTransferData);
  
  /******************************加锁，防止重复点击及并发*************************/
  LockTableActionBL tLockTableActionBL = new LockTableActionBL();
  /**满期给付锁定标志为："MJ"*/
  String tLockNoType = "MJ";
  /** 锁定有效时间*/
  String tAIS = "3600";
  TransferData tTransferDataLock = new TransferData();
  VData tVDataLock = new VData();
  
  tTransferDataLock.setNameAndValue("LockNoKey", getNoticeNo);
  tTransferDataLock.setNameAndValue("LockNoType", tLockNoType);
  tTransferDataLock.setNameAndValue("AvailabilityIntervalSecond", tAIS);
  tVDataLock.add(tGI);
  tVDataLock.add(tTransferDataLock);
  System.out.println("给LockNoKey"+getNoticeNo+"加锁");

  try{
    tMMapLock=tLockTableActionBL.getSubmitMap(tVDataLock, null);
	     if (tMMapLock == null)
	  {
	      
	       	flag = "Fail";
		    content = "少儿险处理失败"+tLockTableActionBL.mErrors.getFirstError();
	    
	  }
  
	  PubSubmit p = new PubSubmit();
	  VData tVDataSubmit = new VData();
	  tVDataSubmit.add(tMMapLock);
	    if(!p.submitData(tVDataSubmit, ""))
	  {
	      flag = "Fail";
		  content = "少儿险处理失败，请不要重复点击！";
	  
	  }
  }catch(Exception  e){
  		  flag = "Fail";
		  content = "少儿险处理失败，请不要重复点击！";
	   
  }

  //***************************加锁完毕*********************************************/
  
if(!flag.equals("Fail")){
	  EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(getNoticeNo,"XB");
	  tEdorItemSpecialData.add("BackDate",request.getParameter("CustomerBackDate"));
	  tEdorItemSpecialData.add("dealType",dealType);
	  if(!tEdorItemSpecialData.insert())
	  {
	  		flag = "Fail";
		    content = "少儿险处理失败";
		
	  }
	    
		if(dealType.equals("1"))
		{//这个是续保的
		  PChildInsurMJDealBL tPChildInsurMJDealBL = new PChildInsurMJDealBL();
		  if(!tPChildInsurMJDealBL.submitData(tVData, ""))
		  {
		    flag = "Fail";
		    content = "少儿险续保失败:"+tPChildInsurMJDealBL.mErrors.getFirstError();
		  }
		  else
		  {
		    flag = "Succ";
		    content = "少儿险续保成功!";
		  }
		}else
		{//这个是满期的
			PChildInsurPayDealBL tPChildInsurPayDealBL = new PChildInsurPayDealBL();
		  if(!tPChildInsurPayDealBL.submitData(tVData, ""))
		  {
		    flag = "Fail";
		    content = "少儿险满期处理失败";
		  }
		  else
		  {
		    flag = "Succ";
		    content = "少儿险满期处理成功";
		  }
			
		}
}
	content = PubFun.changForHTML(content);
	System.out.println("给getNoticeNo为"+getNoticeNo+"的已经处理！");
%>
<html>
<script language="javascript">
	parent.fraInterface.AfterEndTimeDeal("<%=flag%>", "<%=content%>");
</script>
</html>