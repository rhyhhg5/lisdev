<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：BatchPayQueryList.jsp
	//程序功能：清单下载
	//创建日期：2009-06-18
	//创建人  ：yanjing
	//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");

	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));

	//取得前台传入数据
	String managecom = request.getParameter("ManageCom");
	String flag = request.getParameter("Flag");
	String salechnl = request.getParameter("SaleChnl");
	String startDate = request.getParameter("StartDate");
	String endDate = request.getParameter("EndDate");
	String AgentGroup = request.getParameter("AgentGroup");
	String AgentCode = request.getParameter("AgentCode");
	String riskcode = request.getParameter("RiskCode");

	String querySql = "";
	String Sql1 = "";
	String Sql2 = "";
	String Sql3 = "";
	String Sql4 = "";
    
	if (!"".equals(AgentGroup)) {
		Sql1 = " and cp.agentgroup = '" + AgentGroup + "'";
	}

	if (!"".equals(AgentCode)) {
		//Sql2 = "and cp.agentcode = '" + AgentCode + "'";
		Sql2 = "and cp.agentcode = getAgentCode('" + AgentCode + "')";
	}
	
	if (!"".equals(riskcode)) {
		Sql3 = "and cp.riskcode = '" + riskcode + "'";
	}
	
    if("04".equals(salechnl)){
    	Sql4= " and cp.salechnl='04'";    
    }else if ("13".equals(salechnl)){
        Sql4= " and cp.salechnl='13'";    
    }else{
         Sql4= " and cp.salechnl in ('04','13')";   
    }
    
    
	// 客户申请查询SQL
	String strSQL1 = " select cp.managecom, (select Name from ldcom where comcode = cp.ManageCom), cp.contno,  cp.appntname, (select codename from ldcode where codetype='paymode' and  code= cp.paymode ),"
			+ " (select bankaccno from lccont where contno = cp.contno),"
			+ " (select cad.Phone from lcaddress cad, lcappnt lca  where cad.customerno = lca.appntno and cad.addressno = lca.addressno and lca.contno = cp.contno"
			+ "  union select cad.Phone from lcaddress cad, lbappnt lba where cad.customerno = lba.appntno and cad.addressno = lba.addressno and lba.contno = cp.contno),"
			+ " (select cad.PostalAddress from lcaddress cad, lcappnt lca  where cad.customerno = lca.appntno and cad.addressno = lca.addressno and lca.contno = cp.contno"
			+ "  union select cad.PostalAddress from lcaddress cad, lbappnt lba where cad.customerno = lba.appntno and cad.addressno = lba.addressno and lba.contno = cp.contno),"
			+ "  cp.signdate,  cp.riskcode, (select riskname from lmriskapp where riskcode = cp.riskcode), cp.agentcom, getUniteCode(cp.agentcode), (select branchattr from labranchgroup  where agentgroup=cp.agentgroup), "
			+ "  (select mobile from laagent where agentcode = cp.agentcode), cp.prem, (select char(sum(-getmoney)) from ljagetendorse where contno = cp.contno  and feeoperationtype = 'ZB'),"
			+ "  ljs. modifydate, '客户申请'  from lcpol cp, ljspayb ljs where cp.contno = ljs.otherno and  ljs.dealstate='2'  and ljs.cancelreason='1' and cp.standbyflag1 = '1' "
			+ "  and exists (select 1 from lmriskapp where  riskcode=cp.riskcode and kindcode='U' and risktype4='4' and riskprop='I' ) "
			+ "  and not exists (select 1  from dual where (select operator  from ljspayb ljp  where cp.contno = ljp.otherno and ljp.getnoticeno=ljs.getnoticeno and ljs.dealstate = '2' "
			+ "  and  ljp.cancelreason='1' order by ljs.modifydate desc, ljs.modifytime desc fetch first 1 rows only) != 'Server') and ljs. modifydate>='"+ startDate + "' and  ljs.modifydate<='" + endDate + "'"
			+ "  and cp.managecom like '"+ managecom + "%'  "
			+ " "+ Sql1 + "  " + Sql2 + " "+ Sql3 + " "+ Sql4 + "";

	// 系统自动SQL
	String strSQL2 = " select cp.managecom, (select Name from ldcom where comcode = cp.ManageCom), cp.contno,  cp.appntname, (select codename from ldcode where codetype='paymode' and  code= cp.paymode ),"
			+ " (select bankaccno from lccont where contno = cp.contno),"
			+ " (select cad.Phone from lcaddress cad, lcappnt lca  where cad.customerno = lca.appntno and cad.addressno = lca.addressno and lca.contno = cp.contno"
			+ "  union select cad.Phone from lcaddress cad, lbappnt lba where cad.customerno = lba.appntno and cad.addressno = lba.addressno and lba.contno = cp.contno),"
			+ " (select cad.PostalAddress from lcaddress cad, lcappnt lca  where cad.customerno = lca.appntno and cad.addressno = lca.addressno and lca.contno = cp.contno"
			+ "  union select cad.PostalAddress from lcaddress cad, lbappnt lba where cad.customerno = lba.appntno and cad.addressno = lba.addressno and lba.contno = cp.contno),"
			+ "  cp.signdate,  cp.riskcode, (select riskname from lmriskapp where riskcode = cp.riskcode), cp.agentcom, getUniteCode(cp.agentcode), (select branchattr from labranchgroup  where agentgroup=cp.agentgroup), "
			+ "  (select mobile from laagent where agentcode = cp.agentcode), cp.prem, (select char(sum(-getmoney)) from ljagetendorse where contno = cp.contno  and feeoperationtype = 'ZB'),"
			+ "  cp.modifydate, '系统自动'  from lcpol cp  where  cp.standbyflag1 = '1' "
			+ "  and exists (select 1 from lmriskapp where  riskcode=cp.riskcode and kindcode='U' and risktype4='4' and riskprop='I' ) "
			+ "  and not exists (select 1 from ljspayb where otherno = cp.contno and dealstate = '2' and cancelreason='1')"
			+ "  and cp. modifydate>='"+ startDate+ "' and  cp.modifydate<='"+ endDate+ "'"+ "  and cp.managecom like '"+ managecom+ "%'  "
			+ " "+ Sql1+ "  "+ Sql2+ " "+ Sql3 + " "+ Sql4 + " "
			+ "  union all"
			+ " select cp.managecom, (select Name from ldcom where comcode = cp.ManageCom), cp.contno,  cp.appntname, (select codename from ldcode where codetype='paymode' and  code= cp.paymode ),"
			+ " (select bankaccno from lccont where contno = cp.contno),"
			+ " (select cad.Phone from lcaddress cad, lcappnt lca  where cad.customerno = lca.appntno and cad.addressno = lca.addressno and lca.contno = cp.contno"
			+ "  union select cad.Phone from lcaddress cad, lbappnt lba where cad.customerno = lba.appntno and cad.addressno = lba.addressno and lba.contno = cp.contno),"
			+ " (select cad.PostalAddress from lcaddress cad, lcappnt lca  where cad.customerno = lca.appntno and cad.addressno = lca.addressno and lca.contno = cp.contno"
			+ "  union select cad.PostalAddress from lcaddress cad, lbappnt lba where cad.customerno = lba.appntno and cad.addressno = lba.addressno and lba.contno = cp.contno),"
			+ "  cp.signdate,  cp.riskcode, (select riskname from lmriskapp where riskcode = cp.riskcode), cp.agentcom, getUniteCode(cp.agentcode), (select branchattr from labranchgroup  where agentgroup=cp.agentgroup), "
			+ "  (select mobile from laagent where agentcode = cp.agentcode), cp.prem, (select char(sum(-getmoney)) from ljagetendorse where contno = cp.contno  and feeoperationtype = 'ZB'),"
			+ "  ljs. modifydate, '系统自动'  from lcpol cp, ljspayb ljs where cp.contno = ljs.otherno and  ljs.dealstate='2' and ljs.cancelreason='1' and cp.standbyflag1 = '1'  "
			+ "  and exists (select 1 from lmriskapp where  riskcode=cp.riskcode and kindcode='U' and risktype4='4' and riskprop='I' ) "
			+ "  and not exists (select 1  from dual where (select operator  from ljspayb ljp  where cp.contno = ljp.otherno and ljp.getnoticeno=ljs.getnoticeno and ljs.dealstate = '2' "
			+ "  and  ljp.cancelreason='1'  order by ljs.modifydate desc, ljs.modifytime desc fetch first 1 rows only) = 'Server') and ljs. modifydate>='"+ startDate
			+ "' and  ljs.modifydate<='"+ endDate+ "'"
			+ "  and cp.managecom like '"+ managecom+ "%'  "
			+ " "+ Sql1+ "  " + Sql2 + " "+ Sql3 + " "+ Sql4 + " ";

	System.out.println("客户申请" + strSQL1);
	System.out.println("系统自动为" + strSQL2);
	System.out.println(" 全部为" + strSQL1 + " union all " + strSQL2);
	/*
	 0 全部
	 1 客户申请
	 2 系统自动	  
	 */

	if ("1".equals(flag)) {
		querySql = strSQL1;
	} else if ("2".equals(flag)) {
		querySql = strSQL2;
	} else {
		querySql = strSQL1 + " union all " + strSQL2;
	}

	//生成文件
	CreateExcelListEx createexcellist = new CreateExcelListEx("");//指定文件名
	createexcellist.createExcelFile();
	String[] sheetName = { "list" };
	createexcellist.addSheet(sheetName);

	String downLoadFileName = "";
	String filePath = "";
	String tOutXmlPath = "";

	//文件名
	downLoadFileName = "万能险缓缴_" + tG.Operator + "_" + min + sec
			+ ".xls";
	filePath = application.getRealPath("temp");
	tOutXmlPath = filePath + File.separator + downLoadFileName;
	System.out.println("OutXmlPath:" + tOutXmlPath);

	//隐藏字段提供查询sql

	System.out.println(querySql);

	//设置表头
	String[] tTitle = { "管理机构代码", "机构名称", "保单号", "投保人姓名", "交费方式",
			"银行帐号", "联系电话", "联系地址", "承保日期", "险种代码", "险种名称", "销售部门",
			"业务员", "业务员所属团队", "业务员联系方式", "基本保费", "追加保费", "缓缴日期", "缓缴方式" };

	//数据的显示属性(指定对应列是否显示在清单中)
	int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			14, 15, 16, 17, 18, 19, };
	if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
		errorFlag = true;
	}

	if (!errorFlag) {
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}

	out.clear();
	out = pageContext.pushBody();
%>

