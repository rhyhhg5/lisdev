//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var showInfo;
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function beforeSubmit()
{

    if(fm.all('PolNo').value == '')
    {
    	alert("号码不能为空!");
    	return false;
    	}
 /*       
    if(fm.all('PolNo').value.length != 20)
    {
    	alert("投保单号码位数不对!");
    	return false;
   	}    
 */   
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function PersonMulti()
{
  if(checkValue())
  {
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");     	
  fmMulti.submit();	
  }
}

function PersonSingle()
{
    if(beforeSubmit())
    {  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
    fm.submit();
    }	
}

function checkValue()
{	
  if(!isDate(fmMulti.all('StartDate').value)||!isDate(fmMulti.all('EndDate').value))  
    {
      alert("日期输入有误!");
      return false;
    } 
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fmMulti.all('StartDate').value,fmMulti.all('EndDate').value);
  if(flag==1)
    {
       alert("日期输入有误!");
       return false;
    }
  return true;
}

function easyQueryAddClick()
{
	var tSelNo = PolGrid.getSelNo()-1;
	fm.PolNo.value = PolGrid.getRowColData(tSelNo,1);	
	//alert(fm.PolNo.value);
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	
	// 书写SQL语句
	k++;
	var strSQL = "";
	strSQL = "select PolNo,PrtNo,RiskCode,RiskVersion,AppntName,InsuredName from LCPol where "+k+" = "+k
				// + " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+ComCode+"','Pd')=1 "
				 + " and AppFlag='0'"          //承保
				 //+ " and UWFlag in ('4','9')"  //未核保
				 //+ " and LCPol.ApproveFlag = '9' "
				 + " and grppolno = '00000000000000000000'"
				 + " and contno = '00000000000000000000'"
				 + getWherePart( 'PolNo','PolNo2' )
				 + getWherePart( 'ManageCom', 'ManageCom')
//				 + getWherePart( 'AgentCode' )
				 + strAgent
				 + getWherePart( 'AgentGroup' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + " and ManageCom like '" + ComCode + "%%'"
				 + " order by ProposalNo ";
				 //+ getWherePart( 'RiskVersion' );
	  //查询SQL，返回结果字符串
 turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有待发首期催收的个人单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}
