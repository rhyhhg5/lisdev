<%
//程序名称：IndiDueFeeBatchInputInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('StartDate').value = '2008-01-01';
    fm.all('EndDate').value = CurrentTime;
	  fm.all('ManageCom').value = managecom;
	  //fm.all('queryType').value ='选择保单';
	   showAllCodeName();
   
  }
  catch(ex)
  {
    alert("IndiDueFeeBatchInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                    

function initForm()
{
  try
  {
    initInpBox();
    initIndiContGrid();
   // initLCInsuredGrid();
	  initLjsGetGrid();
	 // initLjsGetDrawGrid();
  }
  catch(re)
  {
    alert("IndiDueFeeBatchInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initIndiContGrid()
  {
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="任务批次号";         		//列名
      iArray[1][1]="70px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="给付任务号";         		//列名
      iArray[2][1]="70px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许	   

	    iArray[3]=new Array();
      iArray[3][0]="保单号";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[4]=new Array();
      iArray[4][0]="公司名称";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]=" 保单给付金额合计";         		//列名
      iArray[5][1]="110px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[6]=new Array();
      iArray[6][0]="应给付日期";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=2; 
      iArray[6][4]="comcode";              	        //是否引用代码:null||""为不引用
      iArray[6][5]="6";              	                //引用代码对应第几列，'|'为分割符
      iArray[6][9]="";
      iArray[6][18]=250;
      iArray[6][19]= 0 ;

	    iArray[7]=new Array();
      iArray[7][0]="保单业务员";         		//列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
     	iArray[8]=new Array();
      iArray[8][0]="给付方式";         		//列名
      iArray[8][1]="50px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[9]=new Array();
      iArray[9][0]="给付状态";         		//列名
      iArray[9][1]="50px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="打印状态";         		//列名
      iArray[10][1]="50px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
			iArray[11]=new Array();
      iArray[11][0]="应付号";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      IndiContGrid = new MulLineEnter( "fm" , "IndiContGrid" ); 
      //这些属性必须在loadMulLine前
      IndiContGrid.mulLineCount = 0;   
      IndiContGrid.displayTitle = 1;
      IndiContGrid.locked = 1;
      IndiContGrid.canSel = 1;
      IndiContGrid.hiddenPlus = 1;
      IndiContGrid.hiddenSubtraction = 1;
      IndiContGrid.loadMulLine(iArray);  
      //IndiContGrid.selBoxEventFuncName ="getpoldetail";      
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 催收记录的初始化
function initLjsGetGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      	
      iArray[0]=new Array();	
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许	
      	
      iArray[1]=new Array();
      iArray[1][0]="保单号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=10;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="GrpContNo" ;

      iArray[2]=new Array();
      iArray[2][0]="公司名称";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="被保人数";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保单给付金额合计";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[5]=new Array();
      iArray[5][0]="应给付日期";         		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=200;            		//列最大值
      iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="保单业务员";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="给付类型";         		//列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="回销状态";         		//列名
      iArray[8][1]="50px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      
    	iArray[9]=new Array();
    	iArray[9][0]="抽档标记";         		//列名
    	iArray[9][1]="80px";            		//列宽
    	iArray[9][2]=200;            	        //列最大值
    	iArray[9][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
    	
    	iArray[10]=new Array();
    	iArray[10][0]="保全挂起人数";         		//列名
    	iArray[10][1]="100px";            		//列宽
    	iArray[10][2]=100;            	        //列最大值
    	iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    	
    	iArray[11]=new Array();
    	iArray[11][0]="理赔挂起人数";         		//列名
    	iArray[11][1]="100px";            		//列宽
    	iArray[11][2]=100;            	        //列最大值
    	iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

	    LjsGetGrid = new MulLineEnter( "fm" , "LjsGetGrid" ); 
      //这些属性必须在loadMulLine前
      LjsGetGrid.mulLineCount =0;   
      LjsGetGrid.displayTitle = 1;
      LjsGetGrid.hiddenPlus = 1;
      LjsGetGrid.hiddenSubtraction = 1;
      LjsGetGrid.locked = 1;
      //LjsGetGrid.canSel = 1;
      LjsGetGrid.canChk = 1;
      LjsGetGrid.loadMulLine(iArray);  
      LjsGetGrid.chkBoxEventFuncName ="getCheckdetail";

	  }
      catch(ex)
      {
        alert("IndiDueFeeBatchInputInit.jsp-->initLjsGetGrid函数中发生异常:初始化界面错误!");
      }
}


</script>