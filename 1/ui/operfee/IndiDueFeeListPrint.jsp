<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：IndiDueFeePolListPrint.jsp
//程序功能：
//创建日期：2008-02-13
//创建人  ：Zhanggm modify by xp 改造成EXCEL下载方式
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
 

<%

    boolean errorFlag = false;
	boolean ipErrorFlag = false;//校验用户IP标识，在1小时内同一IP不可重复操作
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "应收清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    
      String sql = "set current query optimization=5";
        
      ExeSQL tExeSQL = new ExeSQL();
      tExeSQL.execUpdateSQL(sql);
    String subsql = request.getParameter("strsql");
    String fenSQL = request.getParameter("fenSQL");
    
    
    String ipAddress = request.getHeader("x-forwarded-for");//获取用户IP  
    if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress))
    	ipAddress = request.getHeader("Proxy-Client-IP");               
    if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress))   
        ipAddress = request.getHeader("WL-Proxy-Client-IP");      
    if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress))   
        ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");       
    if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress))   
        ipAddress = request.getHeader("HTTP_CLIENT_IP");         
    if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress))   
        ipAddress = request.getRemoteAddr();  
            //String host = request.getRemoteHost();  
    String ip = ipAddress;//获取得到实际IP  
    System.out.println("访问者IP="+ip);  
    
    String tLockNoType = "IP";
    MMap tMMap = null;
    if(fenSQL==null)//万能应收清单下载
    {
      fenSQL="";
      tLockNoType="PI";//万能应收清单置不同的类型
    }
    /**锁定有效时间（秒）*/
    String tAIS = "900";
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("LockNoKey", ip);
    tTransferData.setNameAndValue("LockNoType", tLockNoType);
    tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

    VData tVData = new VData();
    tVData.add(tG);
    tVData.add(tTransferData);

    LockTableActionBL tLockTableActionBL = new LockTableActionBL();
    //tMMap = tLockTableActionBL.submitData(tVData, null);
    if (!tLockTableActionBL.submitData(tVData, null))
    {
    	errorFlag = true;
       	ipErrorFlag = true;
    } 
    else{
    	String tSQL = "";
        tSQL  = "select '1', b.makedate,ShowManageName(a.ManageCom) mana,";
        tSQL += " (select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen,";
        tSQL += " a.ContNo,b.GetNoticeNo,";
        tSQL += " (select codename  from ldcode where codetype ='stateflag' and code=a.stateflag),";//保单状态
        tSQL += " a.AppntName,";
       // tSQL += " (select codename  from ldcode where codetype='sex' and code=a.appntsex),";//性别
        tSQL += " e.mobile, (case when e.phone is null then e.homephone else e.phone end)," ;
        tSQL += " e.zipcode," ;//邮编
        tSQL += " e.Postaladdress,a.CValiDate, ";
        tSQL += " (select count(1)+2 from ljspayb where otherno=b.otherno and dealstate='1' and getnoticeno<b.getnoticeno), ";//保单缴次
        tSQL += " GETRISKBYGETNOTICE(b.getnoticeno), ";
        tSQL += " nvl((select nvl(AccGetMoney,0) from LCAppAcc where CustomerNo=a.appntno),0),";
        tSQL += " b.SumDuePayMoney,(select min(LastPayToDate) from ljspaypersonb where getnoticeno = b.getnoticeno),";
        tSQL += " nvl((select sum(paymoney) from ljtempfee where tempfeeno=b.getnoticeno),0),";//实收保费
        tSQL += " (select codename from ldcode where codetype='paymode' and code=a.PayMode),";
        tSQL += " (select bankname from ldbank where bankcode=a.bankcode)," ;//开户行
        tSQL += " a.BankAccNo," ;//开户行帐号
        tSQL += " getUniteCode(a.AgentCode),d.Mobile,d.Phone, d.Name, ";
        tSQL += " (case when  (select  agentold from LAAscription  where contno = b.otherno and ascripstate = '3' order by makedate fetch first 1 rows only ) is null then d.name ";
        tSQL += " else (select (select name from laagent n where n.agentcode = t.agentold) from LAAscription t where contno = b.otherno";
        tSQL += " and ascripstate = '3' order by makedate fetch first 1 rows only) end ),"; //签单代理人
        tSQL += " b.PayDate, ";
        tSQL += " (select max(confmakedate) From ljtempfee where tempfeeno=b.getnoticeno ),  ";
        tSQL += " (case when (b.dealstate ='0' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno ";
        tSQL += " and riskcode in ('320106','120706')) is not null ) then '等待客户反馈' ";
        tSQL += " when (b.dealstate ='1' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno and riskcode in ('320106','120706')) is not null ) then '续保成功' ";
        tSQL += " when (b.dealstate ='3' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno  and riskcode in ('320106','120706')	) is not null ) then '满期给付' ";
        tSQL += " else (select codename  from ldcode where codetype='dealstate' and code=b.dealstate)  end),";
        tSQL += "(case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end) ";
        
        tSQL += " from LCCont a,ljspayB b,ljspaypersonB c,laagent d ,lcaddress e ";
        tSQL += " where 1=1 and a.AgentCode = d.AgentCode and a.appntno =e.customerno ";
        tSQL += " and not exists (select 1 from ljspayb s , ljspaypersonB t where s.otherno=b.otherno and s.getnoticeno=t.getnoticeno "+fenSQL+" and s.getnoticeno>b.getnoticeno)";
        tSQL += " and e.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ";
        tSQL += subsql;
        tSQL += "%' group by b.makedate,a.contno,a.prtno,a.CValiDate,a.managecom,a.AgentCode,b.GetNoticeNo,a.PayMode, a.stateflag,b.otherno,";
        tSQL += " b.dealstate,a.appntno,a.agentgroup,a.AppntName,b.SumDuePayMoney,b.paydate,e.Mobile,e.CompanyPhone,e.HomePhone,e.Postaladdress,d.Name,d.Mobile,d.Phone,a.appntsex,e.mobile,e.homephone,d.sex,b.Cancelreason,e.phone,a.AccName,a.BankAccNo,e.zipcode,a.bankcode " ;
        tSQL += " order by mana,agen,a.AgentCode,b.GetNoticeNo desc with ur";
		System.out.println("打印查询:"+tSQL);
	
    	//设置表头
    	String[][] tTitle = {{"序", "抽档日期", "管理机构", "营销部门", "保单号",
                         "应收记录号", "保单状态", "投保人","移动电话 ","联系电话", "邮编","投保人联系地址",
                         "原始保单生效日","保单缴次","应交险种","可抵余额","应交保费","应收时间","实收保费","收费方式","开户行","开户行帐号",
                         "代理人编码","代理人手机","代理人固话","代理人姓名","签单代理人","缴费截至日期","实交日期","催收状态","作废原因"}};
    	//表头的显示属性
    	int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    
    	//数据的显示属性
    	int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    	//生成文件
	    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
	    createexcellist.createExcelFile();
	    String[] sheetName ={"list"};
	    createexcellist.addSheet(sheetName);
	    int row = createexcellist.setData(tTitle,displayTitle);
	    if(row ==-1) errorFlag = true;
	//        createexcellist.setRowColOffset(row+1,0);//设置偏移
	    if(createexcellist.setData(tSQL,displayData)==-1)
	        errorFlag = true;
	    if(!errorFlag)
	        //写文件到磁盘
	        try{
	            createexcellist.write(tOutXmlPath);
	        }catch(Exception e)
	        {
	            errorFlag = true;
	            System.out.println(e);
	        }
	  //返回客户端
	    if(!errorFlag)
	    downLoadFile(response,filePath,downLoadFileName);
	    out.clear();
	    out = pageContext.pushBody();
    }
	
    
    if(errorFlag)
    {
    	if(ipErrorFlag){
    		
%>
    		<html>
<script language="javascript">	
	alert("打印应收清单正在进行，十五分钟内请勿多次重复操作");
	top.close();
</script>
</html>
<%
    	}
    	else{
%>
<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
    	}
    	}
%>