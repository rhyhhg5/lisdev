<%
//程序名称：GEdorTypeLPSave.jsp
//程序功能：
//创建日期：2008-11-13
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%
	String flag;
	String content;
	
	VData tVData = new VData();
	TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("doType",request.getParameter("doType"));
	tVData.addElement(tTransferData);
	
	GlobalInput gi = (GlobalInput)session.getValue("GI");
	String aEdorNo = request.getParameter("EdorNo");
	System.out.println("EdorNo:"+aEdorNo);
	String aGrpContNo = request.getParameter("GrpContNo");
	System.out.println("GrpContNo:"+aGrpContNo);
    GEdorTypeLPSaveBL tGEdorTypeLPSaveBL = new GEdorTypeLPSaveBL();
	if (!tGEdorTypeLPSaveBL.submitData(aEdorNo, aGrpContNo,gi,tVData))
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tGEdorTypeLPSaveBL.getErrors().getFirstError();
		System.out.println(tGEdorTypeLPSaveBL.getErrors().getErrContent());
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
	}
%>   
<html>
<script language="javascript">
	parent.fraInterface.afterSave("<%=flag%>", "<%=content%>");
</script>
</html>