var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";


function easyPrint()
{	
	mSql="";
	var tManageCom = fm.all('ManageCom').value;			//管理机构代码
	var tManageComName = fm.all('ManageComName').value;	//管理机构名称
	var tPolNo = fm.all('PolNo').value;					//保单号
	var tGetNoticeNo = fm.all('GetNoticeNo').value;		//应收记录号
	var tSaleChnl = fm.all('SaleChnl').value;			//保单类型
	var tDealState = fm.all('DealState').value;			//目前状态
	var tStartDate = fm.all('StartDate').value;			//应收时间起期
	var tEndDate = fm.all('EndDate').value;				//应收时间止期

	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("请输入应收时间起期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间起期",tStartDate))
		{
			fm.all('StartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("请输入应收时间止期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间止期",tEndDate)){
			fm.all('EndDate').focus();
			return false;
		}
	}
	//应收时间起止期三个月校验
	var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>92 )
    {
	  alert("应收时间起止日期不能超过3个月！");
	  return false;
    }

    var tSql1 = "select (select Name from ldcom where comcode=a.ManageCom),a.ManageCom,"+
    	" (select Name from labranchgroup where AgentGroup=a.AgentGroup),a.contno,a.riskcode ,"+
    	" (select max(getnoticeno) from ljapayperson  where polno=a.polno and curpaytodate=a.paytodate "+
    	" fetch first 1 row only) , a.AppntName,"+
    	" (select e.Mobile from lcaddress e, lcappnt d "+
    	" where e.customerno = d.appntno and e.addressno = d.addressno and d.contno=a.contno union select e.Mobile "+
    	"  from lcaddress e, lbappnt d where e.customerno = d.appntno and e.addressno = d.addressno and d.contno=a.contno) ,"+
    	" (select e.Phone from lcaddress e, lcappnt d where e.customerno = d.appntno and e.addressno = d.addressno "+
    	"  and d.contno=a.contno union select e.Phone from lcaddress e, lbappnt d where e.customerno = d.appntno "+
    	"  and e.addressno = d.addressno and d.contno=a.contno), "+
    	" (select e.PostalAddress from lcaddress e, lcappnt d "+
    	"  where e.customerno = d.appntno and e.addressno = d.addressno and d.contno=a.contno  union select "+
    	"  e.PostalAddress from lcaddress e, lbappnt d where e.customerno = d.appntno and e.addressno = d.addressno "+
    	"  and d.contno=a.contno),a.CValiDate,a.paytodate, codename('paymode',a.PayMode),"+
    	" getUniteCode(a.agentcode),"+
    	" (select b.Mobile from laagent b where b.AgentCode=a.AgentCode),"+
    	" (select b.Phone from laagent b where b.AgentCode=a.AgentCode),"+
    	" (select b.Name from laagent b where b.AgentCode=a.AgentCode),"+
    	"  a.AgentCom ,(select Name from lacom where AgentCom=a.AgentCom),"+
    	" b.paydate,(select max(enteraccdate) from ljtempfee where tempfeeno= b.getnoticeno),  "+
    	" (select max(confdate) from ljapay where getnoticeno=b.getnoticeno),  "+
    	"  (select codealias from ldcode1 where codetype='salechnl' and code = a.SaleChnl), "+
    	"  (case when     a.SaleChnl='04' then '银保' when a.SaleChnl='13'  then '银保' else '个险' end), "+
    	"  codename('dealstate',b.DealState), a.prem,"+
    	" (select sum(sumactupaymoney) from ljapayperson where polno = a.polno "+
    	"   and curpaytodate = a.paytodate   and getnoticeno =  b.getnoticeno   "+ 
    	"   group by getnoticeno order by getnoticeno fetch first 1 row only ), "+
    	" (case when a.payintv=0 then 0 else "+
    	"   ((year(a.paytodate)-year(a.CValiDate))*12 +    (month(a.paytodate)-month(a.CValiDate)))/a.payintv +1 end),   "+
    	"   ( year(a.payenddate)-year(a.cvalidate)), "+
    	" (case when exists (select 1 from laascription where contno = a.contno ) "+
    	"   then   (select agentold from laascription where contno = a.contno order by modifydate,   modifytime fetch first 1 row only) "+  
    	"   else   getUniteCode(a.agentcode) end ),"+
    	" (select name from laagent where agentcode = (   case when exists (select 1 from laascription where contno = a.contno )    "+
    	"   then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only)   "+ 
    	"   else a.AgentCode end )   ),codename('stateflag',a.stateflag) , "+
    	" (case when exists (select 1 from LAAscription where ContNo =a.contno and ascripState = '3') "+
    	"   then '孤儿单'  when exists (select 1 from laagent where agentcode = a.agentcode and agentstate>='06') then '孤儿单'    when  "+ 
    	"   (select employdate from laagent where agentcode = a.agentcode)>a.SignDate then '孤儿单'  else '在职单' end   )"+
    	" from lcpol a,ljspayb b,ljapayperson c"+
    	" where a.polno=c.polno and a.paytodate = c.curpaytodate"+
    	" and b.getnoticeno = c.getnoticeno"+
    	" and a.riskcode in (select riskcode from lmriskapp where RiskPeriod !='L')"+
    	" and  c.lastpaytodate between '" + tStartDate + "' and '" + tEndDate + "'"+
    	" and a.managecom like  '" + tManageCom + "%'"+
    	" and a.conttype='1'"+
    	" and c.paytype='ZC'" ;
    
    	var tSql2 = " select (select Name from ldcom where comcode=a.ManageCom),a.ManageCom,"+
    	" (select Name from labranchgroup where AgentGroup=a.AgentGroup),a.contno,a.riskcode ,"+
    	" (select max(getnoticeno) from ljspayperson  where polno=a.polno and lastpaytodate=a.paytodate "+
    	"  fetch first 1 row only) , a.AppntName,"+
    	" (select e.Mobile from lcaddress e, lcappnt d "+
    	"  where e.customerno = d.appntno and e.addressno = d.addressno and d.contno=a.contno union select e.Mobile "+
    	"  from lcaddress e, lbappnt d where e.customerno = d.appntno and e.addressno = d.addressno and d.contno=a.contno) ,"+
    	" (select e.Phone from lcaddress e, lcappnt d where e.customerno = d.appntno and e.addressno = d.addressno "+
    	"  and d.contno=a.contno union select e.Phone from lcaddress e, lbappnt d where e.customerno = d.appntno "+
    	"  and e.addressno = d.addressno and d.contno=a.contno), "+
    	" (select e.PostalAddress from lcaddress e, lcappnt d "+
    	"  where e.customerno = d.appntno and e.addressno = d.addressno and d.contno=a.contno  union select "+
    	"  e.PostalAddress from lcaddress e, lbappnt d where e.customerno = d.appntno and e.addressno = d.addressno "+
    	"  and d.contno=a.contno),a.CValiDate,a.paytodate, codename('paymode',a.PayMode),"+
    	" getUniteCode(a.agentcode),"+
    	" (select b.Mobile from laagent b where b.AgentCode=a.AgentCode),"+
    	" (select b.Phone from laagent b where b.AgentCode=a.AgentCode),"+
    	" (select b.Name from laagent b where b.AgentCode=a.AgentCode),"+
    	"  a.AgentCom ,(select Name from lacom where AgentCom=a.AgentCom),"+
    	" b.paydate,(select max(enteraccdate) from ljtempfee where tempfeeno= b.getnoticeno),  "+
    	" (select max(confdate) from ljapay where getnoticeno=b.getnoticeno),  "+
    	"  (select codealias from ldcode1 where codetype='salechnl' and code = a.SaleChnl), "+
    	"  (case when     a.SaleChnl='04' then '银保' when a.SaleChnl='13'  then '银保' else '个险' end), "+
    	"  codename('dealstate',b.DealState), a.prem,"+
    	" nvl((select sum(sumactupaymoney) from ljapayperson where polno = a.polno "+
    	"   and lastpaytodate = a.paytodate   and getnoticeno =  b.getnoticeno    "+
    	"   group by getnoticeno order by getnoticeno fetch first 1 row only ),0), "+
    	" (case when a.payintv=0 then 0 else "+
    	"   ((year(a.paytodate)-year(a.CValiDate))*12 +    (month(a.paytodate)-month(a.CValiDate)))/a.payintv +1 end),  "+
    	"   ( year(a.payenddate)-year(a.cvalidate)), "+
    	" (case when exists (select 1 from laascription where contno = a.contno ) "+
    	"   then   (select agentold from laascription where contno = a.contno order by modifydate,   modifytime fetch first 1 row only)  "+ 
    	"   else   getUniteCode(a.agentcode) end ),"+
    	" (select name from laagent where agentcode = (   case when exists (select 1 from laascription where contno = a.contno ) "+   
    	"   then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) "+   
    	"   else a.AgentCode end )   ),codename('stateflag',a.stateflag) , "+
    	" (case when exists (select 1 from LAAscription where ContNo =a.contno and ascripState = '3') "+
    	"   then '孤儿单'  when exists (select 1 from laagent where agentcode = a.agentcode and agentstate>='06') then '孤儿单'    when  "+ 
    	"   (select employdate from laagent where agentcode = a.agentcode)>a.SignDate then '孤儿单'  else '在职单' end   )"+
    	
    	" from lcpol a,ljspayb b,ljspaypersonb c"+
    	" where a.polno=c.polno and a.paytodate = c.lastpaytodate"+
    	" and b.getnoticeno = c.getnoticeno"+
    	" and a.riskcode in (select riskcode from lmriskapp where RiskPeriod !='L')"+
    	" and  c.lastpaytodate between '" + tStartDate + "' and '" + tEndDate + "'"+
    	" and a.managecom like '" + tManageCom + "%'"+
    	" and a.conttype='1'"+
    	" and c.paytype='ZC'";
	         

	         
//	         +  "a.lastpaytodate between '" + tStartDate + "' and '" + tEndDate + "' and "
//	         +  "a.managecom like '" + tManageCom + "%' " 
//	         +  "and a.riskcode not in (select riskcode from lmriskapp where RiskType4='4') " 
//	         +  "and a.riskcode not in (select code from ldcode1 where codetype = 'mainsubriskrela' and code1 in (select riskcode from lmriskapp where risktype4 = '4')) ";
	         
    
if(tDealState=='1'||tDealState=='6'){
	tSql1 = tSql1 + "  and b.dealstate ='"+tDealState+"'";
    if(tSaleChnl == "1")
    {
    	tSql1 += "and not exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) ";
    	tSql1 += "and not exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')) ";
    }
    if(tSaleChnl == "2")
    {
    	tSql1 += "and (exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) or exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')))";
    }
    if(tPolNo != "" && tPolNo !=null)
    {
    	tSql1 += "and a.contno='" + tPolNo + "' ";
    }
    if(tGetNoticeNo != "" && tGetNoticeNo != null)
    {
    	tSql1 += "and b.getnoticeno = '" + tGetNoticeNo + "' ";
    }
    mSql = tSql1;
    }else if(tDealState=='0'||tDealState=='2'||tDealState=='3'||tDealState=='4'||tDealState=='5'){
    	tSql2 = tSql2 + "  and b.dealstate ='"+tDealState+"'";
        if(tSaleChnl == "1")
        {
        	tSql2 += "and not exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) ";
        	tSql2 += "and not exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')) ";
        }
        if(tSaleChnl == "2")
        {
        	tSql2 += "and (exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) or exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')))";
        }
        if(tPolNo != "" && tPolNo !=null)
        {
        	tSql2 += "and a.contno='" + tPolNo + "' ";
        }
        if(tGetNoticeNo != "" && tGetNoticeNo != null)
        {
        	tSql2 += "and b.getnoticeno = '" + tGetNoticeNo + "' ";
        }
        mSql = tSql2;
    }else{
    	tSql1 =  tSql1 + " and b.dealstate in ('1','6')";
    	    if(tSaleChnl == "1")
    	    {
    	    	tSql1 += "and not exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) ";
    	    	tSql1 += "and not exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')) ";
    	    }
    	    if(tSaleChnl == "2")
    	    {
    	    	tSql1 += "and (exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) or exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')))";
    	    }
    	    if(tPolNo != "" && tPolNo !=null)
    	    {
    	    	tSql1 += "and a.contno='" + tPolNo + "' ";
    	    }
    	    if(tGetNoticeNo != "" && tGetNoticeNo != null)
    	    {
    	    	tSql1 += "and b.getnoticeno = '" + tGetNoticeNo + "' ";
    	    }
    	    tSql2 = tSql2 + " and b.dealstate in ('0','2','3','4','5')";
            if(tSaleChnl == "1")
            {
            	tSql2 += "and not exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) ";
            	tSql2 += "and not exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')) ";
            }
            if(tSaleChnl == "2")
            {
            	tSql2 += "and (exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) or exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')))";
            }
            if(tPolNo != "" && tPolNo !=null)
            {
            	tSql2 += "and a.contno='" + tPolNo + "' ";
            }
            if(tGetNoticeNo != "" && tGetNoticeNo != null)
            {
            	tSql2 += "and b.getnoticeno = '" + tGetNoticeNo + "' ";
            }
            mSql = mSql + tSql1 + " union " + tSql2 ; 
    }
	
	fm.all('strsql').value=mSql;
	fm.submit();
}
//日期格式校验
function checkDateFormat(tName,strValue) {
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}