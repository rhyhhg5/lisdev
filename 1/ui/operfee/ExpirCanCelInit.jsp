<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
	  showAllCodeName();
  }
  catch(ex)
  {
    alert("ExpirBenefitConfirmInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
	initLjsGetGrid();
  }
  catch(re)
  {
    alert("ExpirBenefitConfirmInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 催收记录的初始化
function initLjsGetGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="给付任务号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="GetNoticeNo";
      
      iArray[2]=new Array();
      iArray[2][0]="任务批次号";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="TaskNo"; 
      
      iArray[3]=new Array();
      iArray[3][0]="保单号";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="ContNo";

      iArray[4]=new Array();
      iArray[4][0]="投保人姓名";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="投保人证件号";         		//列名
      iArray[5][1]="120px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="被保人姓名";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="被保人证件号";         		//列名
      iArray[7][1]="120px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="保单给付金额合计";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="应给付日期";         		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=200;            	        //列最大值
      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[9][21]= "GetToDate";
      
      iArray[10]=new Array();
      iArray[10][0]="被保人客户号";         		//列名
      iArray[10][1]="80px";            		//列宽
      iArray[10][2]=200;            	        //列最大值
      iArray[10][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[10][21]="InsuredNo";

      iArray[11]=new Array();
      iArray[11][0]="创建日期";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=200;            	        //列最大值
      iArray[11][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[11][21]="MakeDate";

      iArray[12]=new Array();
      iArray[12][0]="创建时间";         		//列名
      iArray[12][1]="80px";            		//列宽
      iArray[12][2]=200;            	        //列最大值
      iArray[12][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[12][21]="InsuredNo";


      
	  LjsGetGrid = new MulLineEnter( "fm" , "LjsGetGrid" ); 
      //这些属性必须在loadMulLine前
      LjsGetGrid.mulLineCount =0;   
      LjsGetGrid.displayTitle = 1;
      LjsGetGrid.hiddenPlus = 1;
      LjsGetGrid.hiddenSubtraction = 1;
      //LjsGetGrid.locked = 1;
      LjsGetGrid.canChk = 0;
      LjsGetGrid.canSel = 1;
      LjsGetGrid.loadMulLine(iArray);  
      LjsGetGrid.selBoxEventFuncName ="selectOne";

	  }
      catch(ex)
      {
        alert("ExpirBenefitConfirmInit.jsp-->initLjsGetGrid函数中发生异常:初始化界面错误!");
      }
}

</script>