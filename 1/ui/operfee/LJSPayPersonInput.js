//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  alert(FlagStr);
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  //应交个人费用表中不能为空的字段检验,包括2部分
  //页面显示控件中要输入的字段	
  //隐藏的字段(MakeDate,MakeTime,ModifyDate,ModifyTime),在PersonFeeUI中输入
    var index=0;
    recordCount=fm.all('tableNum').value;
    alert("现在的记录数:"+recordCount);
    if(fmQuery.all('PolNo').value == '')
    {
    	alert("保单号码不能为空!");
    	return false;
    	}
        
//    if(fm.all('PolNo').value.length != 20)
//    {
//    	alert("保单号码位数不对!");
//    	return false;
//    	}    

while(index<recordCount)
{
	index=index+1;
     //个人信息表中可以为空的字段检验! 
    if(!isDate(fm.all('PayDate'+index).value))
    {
    	alert("交费日期输入有误!");
    	return false;
    	}     
   if(!isNumeric(fm.all('SumActuPayMoney'+index).value))
   {
   	alert("总实交金额输入有误!");
    	return false;
   }
 }      
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true");
  //表单中的隐藏字段"活动名称"赋为insert 
  fm.all('Transact').value ="insert";
   
  
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  //请考虑修改客户号码的情况
//  alert("update");
  //表单中的隐藏字段"活动名称"赋为update
  fm.all('Transact').value ="update";
  submitForm();  
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
   refresh();
   fmQuery.submit();
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
//  alert("delete");
  //尚未考虑全部为空的情况
  if(fm.all('PolNo').value == '')
  {
   alert("保单号码不能为空!");
   return false;
  }
  else
  {
  //表单中的隐藏字段"活动名称"赋为insert	
  fm.all('Transact').value ="delete";
  fm.submit();
  }
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function refresh()
{
var innerHTML="";                               
document.all('spanLCPremCode').innerHTML=innerHTML;
initForm();	
	
}

function save()
{       	
  fm.all('Transact').value ="insert";
  submitForm(); 
}
