//Yang Yalin
//2007-1-15 19:50
//PrtContSuccess.js


var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();


//--------------------------------事件响应区-------------------

function prtList()
{
  if(!checkData())
  {
    return false;
  }
  
  fm.action = "PrtContSuccessSave.jsp";
	fm.target = "_blank";
	fm.submit();
}



//--------------------------------校验区--------------------
function checkData()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  if(!isDate(fm.StartDate.value) || !isDate(fm.EndDate.value))
  {
    alert("起止日期都必须为日期格式yyyy-mm-dd");
    return false;
  }
  
  return true;
}





//---------------------------------初始化函数区-------------
function initElement()
{
  fm.ManageCom.value = manageCom;
  fm.LoadFlag.value = loadFlag;
}