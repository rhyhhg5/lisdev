var showInfo;
var mDebug="0";
var showInfo;
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
    afterJisPayQuery()
    easyQueryClick();
    initPolGrid(); 
  }
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  var tSel = ContGrid.getSelNo();	
  if( tSel == 0 || tSel==null)
  {
    alert( "请先选择一条记录，再点击催收按钮。" );
    return false;
  }	
  else
  {
    var tRow = ContGrid.getSelNo() - 1;	        
    var tContNo=ContGrid.getRowColData(tRow,1);  
    //校验保单是否存在失效的附加重疾险种
    var tStrSql = "select 1 from lcpol a where a.contno='"+tContNo+"'"
    + "  and exists (select 1 from ldcode where codetype='extralrisk' and code=a.riskcode)  "
    + " and a.stateflag='2' and (polstate is null or polstate!='03050002') fetch first 1 rows only  ";//除终止缴费外的
    var arrResult = easyExecSql(tStrSql);
    if(arrResult!=null&&arrResult[0][0]!="" && arrResult[0][0]!="null"){
    	alert("该单附加重疾已经失效,请复效后再进行续期抽档!");
    	return false;
    }
    fm.all('ContNoSelected').value = tContNo;
  }  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//校验时间
function CheckDate()
{
  if(fm.all('StartDate').value==null || fm.all('StartDate').value=="" || fm.all('EndDate').value==null || fm.all('EndDate').value=="")
  {
    alert("必须录入起止日期");
    return false;	
  }
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))
  {
    alert("录入日期格式错误");
    return false;
  }
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
  {
    alert("开始日期不能大于终止日期");
    return false;
  }
  return true;	
}

//生成催收记录
function PersonSingle()
{
  if(!beforeSubmit())
  {
    return false;
  } 
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "IndiDueFeeQuery.jsp";
  fm.submit();
}

function selectCont()
{
  var arrReturn = new Array();
  var tRow = ContGrid.getSelNo() - 1;	        
  var tContNo=ContGrid.getRowColData(tRow,1);      
  var tGetNotice = ContGrid.getRowColData(tRow,13);
  var strSQL = "select RiskSeqNo,InsuredName,appntNo, (select riskname  from lmrisk where riskcode = a.riskcode), "
             + "RiskCode, codename('payintv',char(a.PayIntv)), "
             + "PaytoDate,prem, CValiDate , date(PayEndDate) - 1 day "  
             + "from lcpol a where contno='" + tContNo + "' "
             + "and appflag='1' and (StateFlag is null or StateFlag = '1') "
             + "order by RiskSeqNo ";
  turnPage.pageDivName = "divPage2";
  turnPage2.queryModal(strSQL, PolGrid);
}

function checkQuery()
{
  if(isNull(fm.ContNo.value) && isNull(fm.AppntNo.value))
  {
  	alert("客户号和保单号不能同时为空！");
  	return false ;
  }
  return true;
}
//查询可催收保单
function query()
{
  if(!checkQuery())
  {
    return false;
  }
  if(!CheckDate()) 
  {
    return false;
  }
  easyQueryClick();
  if(ContGrid.mulLineCount == 0)
  {
    alert("没有符合条件的可催收保单信息");
  }
}

function easyQueryClick()
{
  tStartDate = fm.all('StartDate').value;
  tEndDate = fm.all('EndDate').value;
  tManageCom = fm.all('ManageCom').value;
  var str11 = ""
  if(fm.AgentCode.value!=null&& fm.AgentCode.value!=""){
  	str11 = "and AgentCode = getAgentCode('"+ fm.AgentCode.value +"')";
  }
  var strSQL = "select ContNo,PrtNo,AppntName,'',CValiDate,SumPrem,nvl((select AccGetMoney from LCAppAcc where CustomerNo=x.appntno),0), "
             + "prem,paytodate,(select codename from ldcode where codetype='paymode' and code=PayMode),ShowManageName(ManageCom),getUniteCode(AgentCode) " 
             + "from LCCont x "
             + "where exists (select 1 from lcpol a,lmriskapp b where a.riskcode = b.RiskCode and b.riskcode not in ('332301','334801','340501','340601') and b.risktype4='4' and a.Contno = x.ContNO) "
	         + "and not exists(select * from LCContState where StateType='GracePeriod' and state='0' and  LCContState.contNo = x.ContNo) "
	         + "and AppFlag='1' and ContType='1' and (StateFlag is null or StateFlag = '1') "
//	         + "and (cardflag='0' or cardflag is null or cardflag ='6') "
	         + "and (select count(1) from ljspay where othernotype='2' and OtherNo=x.ContNo)=0 "
	         + "and exists "
	         + "(select 'X' from LCPol "
			 + "where contno=x.contno "			
			 + "and PaytoDate>='"+tStartDate+"' and PaytoDate<='"+tEndDate + "' "
			 + "and (polstate is null or (polstate is not null and polstate not like '02%%' and polstate not like '03%%')) "
			 + "and (StandbyFlag1 is null or StandbyFlag1 = '0' or StandbyFlag1 = '2') "//自动缓交允许个案催收
			 + "and managecom like '"+tManageCom+"%%' "
			 + "and grppolno = '00000000000000000000' " 
			 + "and ((PaytoDate<payEndDate "
			 + "and PayIntv>0 "
			 + "and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode)) "
			 + " or (PaytoDate>=payEndDate "//添加万能产品续保
			 + "and exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode) "
			 + "and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2')) "
			 + " ) "
			 + "and not exists(select contNo from LCRnewStateLog where polno = lcpol.polno and state in('4','5')) "
			 + "and AppFlag='1' and (StopFlag='0' or StopFlag is null) "
             + "and (select count(1) from LJSPayPerson a where a.contno=lcpol.contno and a.polno=lcpol.PolNo)=0 "
             + ") "
  	         + "and managecom like '"+tManageCom+"%%' "			                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  	         + "and paytodate>='" + tStartDate + "' and paytodate<='" + tEndDate +"' "			                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  	         + getWherePart( 'ContNo','ContNo' )                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
	         //+ getWherePart( 'AgentCode','AgentCode' )                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
  	         + str11
  	         + getWherePart( 'AppntNo','AppntNo' ) 
  	         + " order by contNo ";
  turnPage.pageDivName = "divPage1";
  turnPage.queryModal(strSQL, ContGrid,0,1);
}


//催收完成后查询催收纪录
function afterJisPayQuery()
{
	var strSQL = "	select distinct b.SerialNo,b.MakeDate,'', "
	             + "    b.Operator,b.MakeDate, otherNo, getNoticeNo "
	             + "from  ljspay b "
	             + "where otherNo = '" + fm.ContNoSelected.value + "' "
	             + "    and b.othernotype = '2' ";
	turnPage4.queryModal(strSQL, JisPayGrid); 
}

//打印PDF前往打印管理表插入一条数据
function printInsManage()
{
	//该为从JisPayGrid中得到应收号
	if(JisPayGrid.mulLineCount == 0)
	{
	  alert("没有催收记录");
	  return false;
	}
	var tGetNoticeNo=JisPayGrid.getRowColData(0, 7);
	fm.action = "../uw/PDFPrintSave.jsp?Code=93&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
	fm.submit();
}

//PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
  if(FlagStr == "Fail")
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else if(FlagStr =="PrintError")
  {
   	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

function isNull(s)
{
  var t = trim(s);
  if(t==''||t==null)
  {
    return true;
  }
  else
  {
    return false;
  }
}
