//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, batchNo)
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    content = "满期给付抽档操作完成，" + content;
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  afterJisPayQuery(batchNo);
  initIndiContGrid();
  initPolGrid();
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	 parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function commonPayMulti()
{
  if(IndiContGrid.mulLineCount == 0)
  {
    alert("请先查询的可给付保单信息");
    return false;
  }
  if(!checkValidateDate()) 
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
  fm.payMode.value="Q";
  fm.action="./ULIBenefitBatchQuery.jsp"
  fm.submit();	
  fm.payMulti.disabled=true;
}

function cashPayMulti()
{
  if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
  fm.action="./ULIBenefitBatchQuery.jsp";
  fm.payMode.value="1";
  fm.submit();	
}

function easyQueryClick()
{
  // 初始化表格
  initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
  var tempCurrentTime=CurrentTime; //临时变量
  var tempSubTime=SubTime;  //临时变量
  //得到所选机构
  if(isNull(fm.ManageCom.value))
  {
    alert("请录入机构。");
    return fasle;
  }
  if(isNull(fm.all("StartDate").value))
  {
    alert("满期日期不能为空");
    return false;
  }
  if(isNull(fm.all("EndDate").value))
  {
    alert("满期日期不能为空");
    return false;
  }
  
  managecom = fm.ManageCom.value;
  CurrentTime=fm.all("StartDate").value;
  SubTime=fm.all("EndDate").value;
		 
  var strSQL = "select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'', "
             + "(select name from ldcom where comcode = a.managecom),agentgroup,agentcode "
             + "from lcpol a,lcget b , lmdutygetalive c "
             + "where a.conttype ='1' and a.appflag ='1' and a.grpcontno = '00000000000000000000'"
        //     + "and a.stateflag in ('1','3') "
             + "and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='4') "
             + "and a.polno = b.polno and b.getdutycode = c.getdutycode "
             + "and (select count(1) from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno)=0 "
             + "and a.managecom like '"+managecom+"%' "
             //判断抽档日期是否在周岁生日对应的保单周年日
             + "and (case when a.insuredbirthday+70 years <= a.cvalidate + (year(a.insuredbirthday+70 years) - year(a.cvalidate)) years  "
             + "then a.cvalidate + (year(a.insuredbirthday+70 years) - year(a.cvalidate)) years  "
             + "else a.cvalidate + (year(a.insuredbirthday+71 years) - year(a.cvalidate)) years end) "
             + "between '"+CurrentTime+"' and '"+SubTime+"' ";

  var subSql = "";
  if(fm.all("ContNo").value !="")
  		subSql += "and a.contno ='"+fm.all("ContNo").value+"' ";
  if(fm.all("AppntNo").value !="")
  		subSql += "and a.AppntNo ='"+fm.all("AppntNo").value+"' ";
  if(fm.all("AgentGroup").value !="")
  		subSql += "and a.AgentGroup ='"+fm.all("AgentGroup").value+"' ";
  if(fm.all("AgentCode").value !="")
  		subSql += "and a.AgentCode ='"+fm.all("AgentCode").value+"' ";
  strSQL += subSql;
  
  fm.QuerySql.value = strSQL; 
  turnPage2.pageDivName = "divPage2"; 
  turnPage2.queryModal(strSQL, IndiContGrid);
  initPolGrid();  
  contCurrentTime=CurrentTime; 
  contSubTime=SubTime; 
  fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
  fm.all("EndDate").value=contSubTime;
  CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
  SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
  if(IndiContGrid.mulLineCount == 0)
  {
      alert("没有符合条件的可给付保单信息");
  }
}

/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getpoldetail()
{
  var arrReturn = new Array();
  var tSel = IndiContGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择一条记录，再点击返回按钮。" );
  }	
  else
  {
    getPolInfo();
  }  	                                          
}
function getPolInfo()
{
  var tRow = IndiContGrid.getSelNo() - 1;	        
  var tContNo=IndiContGrid.getRowColData(tRow,1);  
  var strSQL = "select a.RiskSeqNo,a.InsuredName, (select riskname  from lmrisk where riskcode=a.riskcode) ," 
             + "a.RiskCode,codeName('payintv', char(a.PayIntv)), "
             + "a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate "  
             + "from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
             + "and a.polno = b.polno and b.getdutycode = c.getdutycode "
             + "and a.contno='" + tContNo + "' and a.appflag='1' "
             + "group by a.polNo,a.RiskSeqNo,a.InsuredName,a.RiskCode, a.prem, a.CValiDate, a.PaytoDate, a.PayEndDate,a.enddate,a.PayIntv "
             + "order by a.RiskSeqNo ";	
  turnPage3.pageDivName = "divPage3";
  turnPage3.queryModal(strSQL, PolGrid); 
}

/*********************************************************************
 *  校验日期范围
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkValidateDate()
{
  //校验录入的起始日期和终止日期必须在今天之前，否则不予提交
  var startDate=fm.StartDate.value;
  var endDate=fm.EndDate.value;
  if(startDate==''||endDate=='')
  {
  	alert("请录入查询日期范围！");
  	return false;
  }
  if(compareDate(startDate,endDate)==1)
  {
  	alert("起始日不能晚于截止日!");
  	return false;
  }
  return true;
}

//催收完成后查询保单
function afterContQuery()
{
  	// 初始化表格
  initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
  var tempCurrentTime=CurrentTime; //临时变量
  var tempSubTime=SubTime;  //临时变量
  if(fm.all("StartDate").value!="")
  {
  	CurrentTime=fm.all("StartDate").value;
  }
  if(fm.all("EndDate").value!="")
  {
  	SubTime=fm.all("EndDate").value;
  }
  var strSQL = "select a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.Prem,(select AccGetMoney from LCAppAcc where CustomerNo=a.appntno),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),a.AgentCode,b.GetNoticeNo "
             + "from LCCont  a, ljspay b, ljspayperson c "
             + "where a.AppFlag='1' and b.OtherNo=a.ContNo and  b.othernotype='2' "
             + "  and c.GetNoticeNo=b.GetNoticeNo and c.ContNo=b.otherno "
             + "  and  b.MakeDate=current date "
             + "  and exists"
             + "      (select 'X' from LCPol "
             + "      where contno=a.contno "
             + "        and PaytoDate>='" + CurrentTime + "' "
             + "        and PaytoDate<='"+SubTime+"' "
             + "        and PolNo in (select PolNo from LJSPayPerson) "
             + "      )"
             + getWherePart( 'a.ManageCom', 'ManageCom','like')
             + " group by a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.Prem,a.appntno,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode"
             + " order by b.GetNoticeNo with ur " ;
  turnPage2.queryModal(strSQL, IndiContGrid); 
  initPolGrid(); 
  contCurrentTime=CurrentTime;
  contSubTime=SubTime;
  fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
  fm.all("EndDate").value=contSubTime;
  CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
  SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
  
  if(IndiContGrid.mulLineCount == 0)
  {
    alert("没有符合条件的可催收保单信息");
  }
}


//催收完成后查询催收纪录
function afterJisPayQuery(batchNo)
{
  // 初始化表格
  initLjsGetGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
  var tempCurrentTime=CurrentTime; //临时变量
  var tempSubTime=SubTime;  //临时变量
  if(fm.all("StartDate").value!="")
  {
  	CurrentTime=fm.all("StartDate").value;
  }
  if(fm.all("EndDate").value!="")
  {
  	SubTime=fm.all("EndDate").value;
  }		  
  var strSQL = "select distinct a.SerialNo,a.otherno,'',min(d.contno), "
             + "(select name from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "max(a.sumgetmoney),min(d.curgettodate),"
             + "(select name from laagent where agentcode = min(d.agentcode)), min(d.insuredno), "
             + "codename('paymode',min(a.paymode)), "
             + "nvl((select count(distinct c.getnoticeno) from ljsgetdraw b ,ljsget c "
             + " where b.getnoticeno = c.getnoticeno and c.dealstate ='1' and b.contno = d.contno "
             + "and b.insuredno =d.insuredno ),0),'待给付','','','',d.GetNoticeNo "
             + "from ljsget a ,ljsgetdraw d "
             + "where a.getnoticeno = d.getnoticeno and a.makedate=current date "
             + "and a.othernotype='20' and  a.operator ='"+operator+"'"
             + "and a.SerialNo ='"+batchNo+"' "
             + "group by a.SerialNo,a.otherno,a.getnoticeno,d.contno,d.insuredno,d.GetNoticeNo " ;
  turnPage1.pageDivName = "divPage1"; 
  turnPage1.queryModal(strSQL, LjsGetGrid); 
  contCurrentTime=CurrentTime;
  contSubTime=SubTime;
  fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
  fm.all("EndDate").value=contSubTime;
  CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
  SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
  if(LjsGetGrid.mulLineCount == 0)
  {
    document.all("divInfo").style.display = "none";
  }
  else
  {
    document.all("divInfo").style.display = "";
  }
}

function getContInfo()
{
  var tSel = IndiContGrid.getSelNo();
  if( tSel == 0 || tSel == null )
  {		
    alert( "请先选择一条记录，再点击保单明细按钮。" );
  }
  else
  {
    var cContNo = IndiContGrid.getRowColData( tSel - 1, 1);		  
    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType=1");	
					
  }
}
function getLjsGetDrawdetail()
{
  var count = 0;
  var tChked = new Array();
  for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
  {
    if(LjsGetGrid.getChkNo(i) == true)
    {
    	tChked[count++] = i;
    }
  }
  if(tChked.length < 1)
  {
    return false;	
  }
  var taskNo=LjsGetGrid.getRowColData(tChked[0],2);
  
  var SQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO FROM LJSGETDRAW WHERE GETNOTICENO =(SELECT GETNOTICENO FROM LJSGET WHERE OTHERNOTYPE ='20' AND OTHERNO ='"+taskNo+"')";
  turnPage.queryModal(SQL, LjsGetDrawGrid); 
  for(var i = 0; i < LjsGetDrawGrid.mulLineCount; i++)
  {
    var sql = "select name from lcbnf where polno ='"+LjsGetDrawGrid.getRowColData(i,7)+"'";
    var rs = easyExecSql(sql);
    var bnf = "";
    if(rs!=null)
    {
      for(var j =0;j<=rs.length-2;j++)
      {
      	bnf+=rs[j]+'、';
      }
      if(j==rs.length-1)
      {
        bnf+=rs[j];
      }
    }
    LjsGetDrawGrid.setRowColData(i,3,bnf);
  }
}

function printNotice()
{
  if (LjsGetGrid.mulLineCount == 0)
  {
    alert("打印列表没有数据");
    return false;
  }
  fm.action="./ULIBenefitPrint.jsp";
  fm.submit();
}

function afterSubmit2(FlagStr,Content)
{
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
   	 //window.parent.close(); 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

function isNull(s)
{
  var t = trim(s);
  if(t==""||t==null)
  {
    return true;
  }
  else
  {
    return false;
  }
}
