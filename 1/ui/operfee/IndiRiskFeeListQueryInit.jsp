<%
//程序名称：IndiDueFeeListQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>                            
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

    fm.all('SingleEndDate').value = '<%=PubFun.getCurrentDate()%>'; 
    var sql1 = "select Current Date - 1 month from dual ";
	fm.all('SingleStartDate').value = easyExecSql(sql1);   
    fm.all('AppNo').value = '';
	showAllCodeName();
    
}


function initForm()
{
    initInpBox();	  
    initElementtype();  
    initContGrid();
    	
}

// 保单信息列表的初始化
function initContGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="管理机构";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[2]=new Array();
      iArray[2][0]="管理机构代码";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="营销部门";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="保单号";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
                                                                               
      iArray[5]=new Array();                                                       
      iArray[5][0]="险种代码";         		//列名                                   
      iArray[5][1]="80px";            		//列宽                                   
      iArray[5][2]=200;            			//列最大值                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许      

	   
	  iArray[6]=new Array();
      iArray[6][0]="应收记录号";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[7]=new Array();
      iArray[7][0]="投保人";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="移动电话";         		//列名
      iArray[8][1]="120px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[9]=new Array();
      iArray[9][0]="联系电话";         		//列名
      iArray[9][1]="120px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[10]=new Array();
      iArray[10][0]="投保人联系地址";         		//列名
      iArray[10][1]="160px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[11]=new Array();
      iArray[11][0]="签单日";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0; 
      
	  iArray[12]=new Array();
      iArray[12][0]="应交保费";         		//列名
      iArray[12][1]="80px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[13]=new Array();
      iArray[13][0]="基本保费";         		//列名
      iArray[13][1]="80px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
				
	  iArray[14]=new Array();
      iArray[14][0]="额外保费";         		//列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[15]=new Array();
      iArray[15][0]="应收时间";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=200;            			//列最大值
      iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
	  iArray[16]=new Array();
      iArray[16][0]="收费方式";         		//列名
      iArray[16][1]="80px";            		//列宽
      iArray[16][2]=200;            			//列最大值
      iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[17]=new Array();
      iArray[17][0]="代理人编码";         		//列名
      iArray[17][1]="80px";            		//列宽
      iArray[17][2]=200;            			//列最大值
      iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[18]=new Array();
      iArray[18][0]="代理人手机";         		//列名
      iArray[18][1]="80px";            		//列宽
      iArray[18][2]=200;            			//列最大值
      iArray[18][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[19]=new Array();
      iArray[19][0]="代理人在职状态";         		//列名
      iArray[19][1]="80px";            		//列宽
      iArray[19][2]=200;            			//列最大值
      iArray[19][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[20]=new Array();
      iArray[20][0]="代理人姓名";         		//列名
      iArray[20][1]="80px";            		//列宽
      iArray[20][2]=200;            			//列最大值
      iArray[20][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[21]=new Array();
      iArray[21][0]="代理机构编码";         		//列名
      iArray[21][1]="80px";            		//列宽
      iArray[21][2]=200;            			//列最大值
      iArray[21][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[22]=new Array();
      iArray[22][0]="代理机构名称";         		//列名
      iArray[22][1]="120px";            		//列宽
      iArray[22][2]=200;            			//列最大值
      iArray[22][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[23]=new Array();
      iArray[23][0]="缴费截止日期";         		//列名
      iArray[23][1]="80px";            		//列宽
      iArray[23][2]=200;            			//列最大值
      iArray[23][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[24]=new Array();
      iArray[24][0]="收费确认日期";         		//列名
      iArray[24][1]="80px";            		//列宽
      iArray[24][2]=200;            			//列最大值
      iArray[24][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[25]=new Array();
      iArray[25][0]="销售渠道";         		//列名
      iArray[25][1]="80px";            		//列宽
      iArray[25][2]=200;            			//列最大值
      iArray[25][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[26]=new Array();
      iArray[26][0]="保单类型";         		//列名
      iArray[26][1]="80px";            		//列宽
      iArray[26][2]=200;            			//列最大值
      iArray[26][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[27]=new Array();
      iArray[27][0]="状态";         		//列名
      iArray[27][1]="80px";            		//列宽
      iArray[27][2]=200;            			//列最大值
      iArray[27][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[28]=new Array();
      iArray[28][0]="保单年度";         		//列名
      iArray[28][1]="80px";            		//列宽
      iArray[28][2]=200;            			//列最大值
      iArray[28][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      

      ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
      ContGrid.mulLineCount = 0;   
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 1;
      ContGrid.canChk = 1;
      ContGrid.hiddenPlus = 1;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.loadMulLine(iArray);       
      }
      catch(ex)
      {
        alert(ex);
      }
}




</script>