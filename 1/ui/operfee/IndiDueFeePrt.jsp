<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：IndiDueFeePrt.jsp
//程序功能：打印、下载应收清单
//创建日期：2008-01-07
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	XmlExport txmlExport = null;   
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	String tCurrentTime = request.getParameter("CurrentTime");
	String tSubTime = request.getParameter("SubTime");
	String tContNo = request.getParameter("ContNo");
	String tManageCom = request.getParameter("ManageCom");
	String tAgentCode = request.getParameter("AgentCode");
	String tAppntNo = request.getParameter("AppntNo");

    TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("CurrentTime",tCurrentTime);
	tTransferData.setNameAndValue("SubTime",tSubTime);
	tTransferData.setNameAndValue("ContNo",tContNo);
	tTransferData.setNameAndValue("ManageCom",tManageCom);
	tTransferData.setNameAndValue("AgentCode",tAgentCode);
	tTransferData.setNameAndValue("AppntNo",tAppntNo);

	VData tVData = new VData();
    tVData.addElement(tG);
	tVData.addElement(tTransferData);
          
    IndiDueFeePrtUI tIndiDueFeePrtUI = new IndiDueFeePrtUI(); 
    if(!tIndiDueFeePrtUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tIndiDueFeePrtUI.mErrors.getErrContent();                
    }
    else
    {    
		VData mResult = tIndiDueFeePrtUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	System.out.println(operFlag);
	if (operFlag==true)
	{
	    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
        ByteArrayOutputStream dataStream = new ByteArrayOutputStream();  
        CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
        tcombineVts.output(dataStream);  
        session.putValue("PrintVts", dataStream);
		session.putValue("PrintStream", txmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>