<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String tYWType = "";
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");

  System.out.println("常无忧B批量撤销…………");
  
  ArrayList arrayList = new ArrayList();
    String tGetNoticeNo[] = request.getParameterValues("LjsGetGrid18");	 
    String tChk[] = request.getParameterValues("InpLjsGetGridChk"); //参数格式=” Inp+MulLine对象名+Chk”
    int tCount = tGetNoticeNo.length;
	for(int j=0; j<tCount; j++)
	{
	  if(tChk[j].equals("1"))  
	  { 
	    arrayList.add(tGetNoticeNo[j]);
		System.out.println("---------GetNoticeNo: "+tGetNoticeNo[j]);
	  }		
	}
	
  ExpirBenefitCancelBL tExpirBenefitCancelBL = new ExpirBenefitCancelBL();
  try
  {
    VData tVData = new VData();
  	TransferData tTransferData = new TransferData();
  	tVData.add(tG); 
  	tTransferData.setNameAndValue("arrayList", arrayList);
  	tTransferData.setNameAndValue("sql",request.getParameter("sql"));
  	tVData.add(tTransferData);
    if(!tExpirBenefitCancelBL.submitData(tVData,""))
    {
      Content = "操作失败，原因是:" + tExpirBenefitCancelBL.mErrors.getFirstError();
      System.out.println(Content);
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("常无忧B批量撤销完成");
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  { 
    tError = tExpirBenefitCancelBL.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "数据修改成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "数据修改成功！有给付任务无法撤销，原因是:";
      
      for(int n=0;n<tError.getErrorCount();n++)
      {       
	       Content=Content+tError.getError(n).errorMessage;
	       Content=Content+"、";
      }
      System.out.println(Content);
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
