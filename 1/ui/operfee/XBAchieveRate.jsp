<%
//程序名称：PAchieveRate.jsp
//程序功能：个险续期达成率统计
//创建日期：2010-10-13 
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="XBAchieveRate.js"></SCRIPT>
  <%@include file="XBAchieveRateInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form method=post name=fm action="./XBAchieveRateSave.jsp" >
		<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary readonly>
				</td>
				<td class= title>保单类型</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnlType verify="保单类型|NOTNULL" CodeData="0|^1|个险^2|银保" ondblclick="return showCodeListEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" value = "1"><Input class=codename name=SaleChnlTypeName elementtype=nacessary readonly  >
				</td>
		<!-- <td class= title>险种类型</td>
				<td class=input>
					<Input class= "codeno" name=RiskType verify="险种类型|NOTNULL" CodeData="0|^1|传统险^2|万能险" ondblclick="return showCodeListEx('RiskType',[this,RiskTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('RiskType',[this,RiskTypeName],[0,1]);" value = "1"><Input class=codename name=RiskTypeName elementtype=nacessary readonly >
				</td>   -->
			</tr>
	    	<tr>
	    	    <td class= title>销售渠道</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnl verify="销售渠道|NOTNULL" CodeData="0|^00|全部^01|个险直销^03|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);" value = "00"><Input class=codename name=SaleChnlName elementtype=nacessary readonly  >
				</td>
				<td class= title>保单服务状态</td>
				<td class=input>
					<Input class= "codeno" name=Orphans verify="保单服务状态|NOTNULL" CodeData="0|^0|全部^1|在职单^2|孤儿单" ondblclick="return showCodeListEx('Orphans',[this,OrphansName],[0,1]);" onkeyup="return showCodeListKeyEx('Orphans',[this,OrphansName],[0,1]);" value = "0"><Input class=codename name=OrphansName elementtype=nacessary readonly  >
				</td>
				<td class= title> 缴费次数</td>
				<td class=input>
					<Input class= "codeno" name=PayCount verify="缴次|NOTNULL" CodeData="0|^0|全部^2|两次^3|三次^4|四次及以上" ondblclick="return showCodeListEx('PayCount',[this,PayCountName],[0,1]);" onkeyup="return showCodeListKeyEx('PayCount',[this,PayCountName],[0,1]);" value = "0"><Input class=codename name=PayCountName elementtype=nacessary readonly  >
				</td>
	    	</tr>
	    	<tr>
				<td class= title>应缴开始日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=DueStartDate elementtype=nacessary verify="应缴开始日期|NOTNULL">
				</td>
				<td class= title>应缴截止日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=DueEndDate elementtype=nacessary verify="应缴开始日期|NOTNULL">
				</td>
			</tr>
			<tr>
				<td class= title>核销开始日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=ActuStartDate>
				</td>
				<td class= title>核销截止日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=ActuEndDate>
				</td>
				<td class= title>统计范围</td>
				<td class=input>
					<Input class= "codeno" name=CountArea verify="统计范围|NOTNULL" CodeData="0|^0|省级分公司^1|地市级机构" ondblclick="return showCodeListEx('CountArea',[this,CountAreaName],[0,1]);" onkeyup="return showCodeListKeyEx('CountArea',[this,CountAreaName],[0,1]);" value = "0"><Input class=codename name=CountAreaName elementtype=nacessary readonly  >
				</td>
			</tr>		
		</Table>
		<Table class= common>
		   <tr>
			<td>
				<input value="下载报表" class = cssButton TYPE=button onclick="easyPrint();">
				*以上统计不含少儿险（320106、120706）
			</td>
			</tr>
			</br>
			
		</Table>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
