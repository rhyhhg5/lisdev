  <%
//程序名称：IndiNormalPayVerifyInit.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
 fm.all('ContNo').value='';
   }
  catch(ex)
  {
    alert("程序名称：IndiNormalPayVerifyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

var PremGrid;
function initPremGrid()
  {    
                           
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      //杨红于2005-06-30添加个人险种号的显示信息
      //添加开始
      iArray[1]=new Array();
      iArray[1][0]="个人险种保单号码";          		//列名
      iArray[1][1]="80px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0; 
      //添加结束
      
      iArray[2]=new Array();
      iArray[2][0]="责任编码";          		//列名
      iArray[2][1]="80px";      	      		//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[3]=new Array();
      iArray[3][0]="交费计划编码";         			//列名
      iArray[3][1]="80px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="交费计划类型";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="已交费次数";      	   		//列名
      iArray[5][1]="60px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="应交金额";      	   		//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      iArray[7]=new Array();
      iArray[7][0]="实交金额";      	   		//列名
      iArray[7][1]="60px";            			//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许
     
      PremGrid = new MulLineEnter( "fm" , "PremGrid" ); 
      //这些属性必须在loadMulLine前
      PremGrid.mulLineCount = 0;   
      PremGrid.displayTitle = 1;
      PremGrid.hiddenPlus=1; 
      PremGrid.hiddenSubtraction=1;     
      PremGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initForm()
{
  try
  {
    initInpBox();
    initPremGrid();   
  }
  catch(re)
  {
    alert("FinVerifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
