<%
//程序名称：GrpDueFeePlanQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.*"%>  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>     
<%@page contentType="text/html;charset=GBK" %>
<% 
  //保存保单号  18201304141
  String PrtNo = request.getParameter("tPrtNo"); 
  String GrpContNo=request.getParameter("tGrpContNo");//说明团单表的主键是GrpContNo,用它传值到后台处理
  String ProposalGrpContNo=request.getParameter("ProposalGrpContNo");
  String PayDate=request.getParameter("PayDate");//审批标记

  String PlanCode[]= request.getParameterValues("GrpPolGrid6");
  String PayToDate[]= request.getParameterValues("GrpPolGrid5");
  String contPlanCode[]= request.getParameterValues("GrpPolGrid1");
  String prem[]= request.getParameterValues("GrpPolGrid4");
  
  String tPlanCode=PlanCode[0];
  
  LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
  
  for (int i=0;i<contPlanCode.length;i++)
  {
	  LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
	  tLCGrpPayPlanSchema.setPrtNo(PrtNo);
	  tLCGrpPayPlanSchema.setProposalGrpContNo(ProposalGrpContNo);
	  tLCGrpPayPlanSchema.setContPlanCode(contPlanCode[i]);
	  tLCGrpPayPlanSchema.setPrem(prem[i]);
	 
	  tLCGrpPayPlanSchema.setPaytoDate(PayToDate[i]);
	  tLCGrpPayPlanSchema.setPlanCode(PlanCode[i]);
	  tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
  }
// 输出参数      
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    TransferData tempTransferData=new TransferData();
    tempTransferData.setNameAndValue("PrtNo",PrtNo);
    tempTransferData.setNameAndValue("GrpContNo",GrpContNo);
    tempTransferData.setNameAndValue("ProposalGrpContNo",ProposalGrpContNo);
    tempTransferData.setNameAndValue("PlanCode",tPlanCode);
    tempTransferData.setNameAndValue("PayDate",PayDate);
    VData tVData = new VData(); 
    tVData.add(tGI);
    tVData.add(tempTransferData);
    tVData.add(tLCGrpPayPlanSet);
    GrpDueFeeSplitUI tGrpDueFeeSplitUI = new GrpDueFeeSplitUI(); 
    tGrpDueFeeSplitUI.submitData(tVData,"");
    if (!tGrpDueFeeSplitUI.mErrors.needDealError())
     {
       Content = " 处理成功";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content =" 失败，原因是:" + tGrpDueFeeSplitUI.mErrors.getFirstError();
       FlagStr = "Fail";
     }
  }//页面有效区
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>


