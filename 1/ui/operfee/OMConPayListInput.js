//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var mSql="";
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	
  }
}




//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


// 通过保单查询
function easyQueryClick()
{
	if(fm.all("GrpContNo").value==null || fm.all("GrpContNo").value==""){
		alert("请输入保单号！");
		return false;
	}
	// 初始化表格
	initGrpContGrid();
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
  /*
   var strSQL = "select distinct b.PayNo,a.AppntName,a.ContNo,"
    +"(select varchar(sum(SumDuePayMoney)) from LJAPayPerson where PayNo=b.PayNo),"
    +"(select  min(LastPayToDate) from LJAPayPerson where PayNo=b.PayNo),varchar(b.SumActuPayMoney),b.MakeDate,b.Operator"
    + ",case when (select stateFlag from LOPRTManager where OtherNoType='00' and OtherNo=a.contno and Code='92' and StandbyFlag2=b.PayNo )= '1' then '已打印' else '未打印' end "
		+",(select abs(sum(sumDuePayMoney)) from LJSPayPersonB where getNoticeNo = b.getNoticeNo and payType='YEL'),b.ConfDate"
		+" from LCCont a,LJAPay b"
    + " where 1=1"		
    + getWherePart( 'a.ContNo ','GrpContNo' ) 				  
    + " AND a.ContNo=b.IncomeNo and b.IncomeType='2'"			  
    //+ " and a.AppFlag='1' "
    + " and exists  (select GetNoticeNo from LJSPayB WHERE OtherNoType='2' and DealState='1' and getnoticeno = b.getnoticeno)"   
    + " order by a.ContNo ,b.PayNo desc" 
	;
	*/

	var strSQL = "select distinct b.PayNo,a.AppntName,codename('sex',a.appntsex)"
		+",(select max(Mobile) from lcaddress where a.appntno=customerno) "
		+"	,a.ContNo,"
    +"(select varchar(sum(SumDuePayMoney)) from LJAPayPerson where PayNo=b.PayNo),"
    +"(select  min(LastPayToDate) from LJAPayPerson where PayNo=b.PayNo),varchar(b.SumActuPayMoney),b.MakeDate,b.Operator"
    + ",case when (select stateFlag from LOPRTManager where OtherNoType='00' and OtherNo=a.contno and Code='92' and StandbyFlag2=b.PayNo )= '1' then '已打印' else '未打印' end "
		+",(select abs(sum(sumDuePayMoney)) from LJSPayPersonB where getNoticeNo = b.getNoticeNo and payType='YEL'),b.ConfDate"
	//	+", c.Name, c.Sex, c.Mobile "
    // 	+", c.Name, (select Codename from LDCode where Codetype = 'sex' and Code = c.Sex), c.Mobile " 
    	+", c.Name, (select Codename from LDCode where Codetype = 'sex' and Code = c.Sex), c.Mobile , c.Phone "
    +" ,c.agentgroup " 	
		+" from LCCont a, LJAPay b, LAAgent c "
    + " where 1=1 and a.Agentcode = c.Agentcode "		
    + getWherePart( 'a.ContNo ','GrpContNo' ) 				  
    + " AND a.ContNo=b.IncomeNo and b.IncomeType='2'"			  
    //+ " and a.AppFlag='1' "
    + " and exists  (select GetNoticeNo from LJSPayB WHERE OtherNoType='2' and DealState='1' and getnoticeno = b.getnoticeno)"   
    + " and exists (select 1 from lcpol lc,lmriskapp lm where lc.riskcode = lm.RiskCode and lm.risktype4 ='4' and lc.Contno = a.ContNO) "
    + " order by c.agentgroup,a.ContNo ,b.PayNo desc" 
	;
	
  mSql = strSQL;
	      
	turnPage2.queryModal(strSQL, GrpContGrid); 
	if(GrpContGrid.mulLineCount == 0)
  {
   		alert("没有满足条件的信息！");	
   		return false ;
  }
	
}
// 查询结果列表
function multQueryClick()
{
	
	// 初始化表格
	initGrpJisPayGrid();
	initGrpContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收

		var strSQL = "select distinct a.SerialNo,a.MakeDate,a.Operator,"
	   +"(select case when DealState='3' then '完成' else '进行中' end from LCUrgeVerifyLog where SerialNo=a.SerialNo)"
	   +" from LJAPay a"
     + " where a.IncomeType='2'"
	   + getWherePart( 'a.GrpContNo ','GrpContNo' ) 
	   + getWherePart( 'a.MakeDate ','StartDate' ,'>=') 
	   + getWherePart( 'a.MakeDate ','EndDate','<=' ) 
	   + getWherePart( 'a.ManageCom ','ManageCom','like' ) 
	   + " and exists(select 'X' from LJAPayPerson where PayNo=a.PayNo )"	
	   + " and exists(select 'X' from LJSPayB WHERE OtherNoType='2' and DealState='1' and GetNoticeNo=a.GetNoticeNo)"	
	   + " and exists (select 1 from lcpol lc,lmriskapp lm where lc.riskcode = lm.RiskCode and lm.risktype4 ='4' and lc.Contno = a.incomeno) "
	   + " group by  a.SerialNo,a.MakeDate,a.Operator  "
	   + " order by a.SerialNo desc fetch first 3000 rows only " 
		;
	      
	turnPage1.queryModal(strSQL, GrpJisPayGrid); 
	if(GrpJisPayGrid.mulLineCount == 0)
  {
   		alert("没有满足条件的信息！");	
   		return false ;
  }
	
}
//校验是否选择了要查询的批次
function grpJisPayChecked()
{
  var rowCount = 0;
  for (i = 0; i < GrpJisPayGrid.mulLineCount; i++)
	{
		if (GrpJisPayGrid.getChkNo(i)) 
		{
		  	rowCount = rowCount + 1;
		}
	}
	if(rowCount == 0)
	{
      alert("请选择需要查询明细的批次！");
	  return false;
	}
	
  return true;
}
//通过批次列表查询明细
function quaryList()
{
	
	if(GrpJisPayGrid.mulLineCount == 0)
    {
   		alert("请先查询批次！");	
   		return false ;
    }
    if(!grpJisPayChecked())
    {
        return false;
    }
	// 初始化表格
	initGrpContGrid();

  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var rowNum=GrpJisPayGrid.mulLineCount;
	if (rowNum==0) return false;
	var tCount = 0;
	var tSerialNo = "";
   for(var i=0;i<rowNum;i++)
   {                                                                   
       if(GrpJisPayGrid.getChkNo(i))
       {
				   tCount = tCount+1;
		
				   if (tCount == 1)
				   {
					   tSerialNo = "'" + GrpJisPayGrid.getRowColData( i , 1 )+ "'";
		
				   }else
				   {
		           tSerialNo = tSerialNo + ",'" + GrpJisPayGrid.getRowColData( i , 1 )+ "'";
				   }
       }
   }
   var strState = fm.all('PrintState').value //催收状态 2：全部,0: 未打印；1：已打印
   var SaleChnl=fm.all('SaleChnl').value;
   var wherePar="";
   if (strState=='0')
   {
		
		wherePar = wherePar
	             + " and ((select stateFlag from LOPRTManager where OtherNoType='00' and OtherNo=a.contno and Code='92'  and StandbyFlag2=b.PayNo)= '0' " 
                 + " or (select stateFlag from LOPRTManager where OtherNoType='00' and OtherNo=a.contno and Code='92'  and StandbyFlag2=b.PayNo) is null) "
                 ;	 
		 
   }else if(strState=='1')
   { 	
		wherePar = wherePar 
                 + " and (select stateFlag from LOPRTManager where OtherNoType='00' and OtherNo=a.contno and Code='92'  and StandbyFlag2=b.PayNo)= '1' " 
		         ;			  
   }
   if(SaleChnl!=null&&SaleChnl!=""&&SaleChnl!="null")
   {
       if(SaleChnl=="1")//个险
       {
           wherePar+=" and a.salechnl not in ('04','13') "; 
       }
       if(SaleChnl=="2")//银保
       {
           wherePar+=" and a.salechnl  in ('04','13') "; 
       }
    }
	var strSQL = " select distinct b.PayNo,a.AppntName,codename('sex',a.appntsex)"
			   + " ,(select max(Mobile) from lcaddress where a.appntno=customerno) "
			   + " ,a.ContNo,"
			   + " (select sum(SumDuePayMoney) from LJAPayPerson where PayNo=b.PayNo),"
			   + " (select  min(LastPayToDate) from LJAPayPerson where PayNo=b.PayNo),b.SumActuPayMoney,b.MakeDate,b.Operator"
	 	       + " ,case when (select stateFlag from LOPRTManager where OtherNoType='00' and OtherNo=a.contno and Code='92' and StandbyFlag2=b.PayNo )= '1' then '已打印' else '未打印' end "
	           + " ,(select abs(sum(sumDuePayMoney)) from LJSPayPersonB where getNoticeNo = b.getNoticeNo and payType='YEL'),b.ConfDate"
		       + " , c.Name, (select Codename from LDCode where Codetype = 'sex' and Code = c.Sex), c.Mobile ,c.Phone"
		       + " ,c.agentgroup "
		       + " from LCCont a, LJAPay b, LAAgent c "
               + " where b.SerialNo in (" + tSerialNo + ")"  
			   + " AND a.ContNo=b.IncomeNo and b.IncomeType='2' and a.Agentcode = c.Agentcode"	
			   + wherePar		  
			   + " and a.AppFlag='1' "
	           + " and exists(select 'X' from LJSPayB WHERE OtherNoType='2' and DealState='1' and GetNoticeNo=b.GetNoticeNo)"	  	  
               + " order by c.agentgroup,a.ContNo ,b.PayNo desc" 
		       ;
	mSql = strSQL;
	turnPage2.queryModal(strSQL, GrpContGrid); 
	if(GrpContGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的核销数据");
	}
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var cGrpContNo = GrpContGrid.getRowColData( tSel - 1, 1 );
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ cGrpContNo+"&ContType=1");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}
//打印清单
function printList()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}	
	window.open("../operfee/GrpConPayListPrint.jsp?SQL="+ mSql);
	
}

//打印对帐单
function printPayComp()
{

	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var selNo = GrpContGrid.getSelNo();
	if(selNo == null || selNo == 0)
	{
		alert("请选择一条记录");
		return false;
	}
	var cPayNo = GrpContGrid.getRowColData( selNo - 1, 1 ); 
	var cDueMoney = GrpContGrid.getRowColData( selNo - 1, 6 );//应交金额
	var cDif = GrpContGrid.getRowColData( selNo - 1, 10 );//余额
	var cPayDate = GrpContGrid.getRowColData( selNo - 1, 13 );

	window.open("../operfee/IndiPayCompPrint.jsp?PayNo="+ cPayNo+ "&PayDate=" + cPayDate+ "&DueMoney="+ cDueMoney+ "&Dif="+cDif);
}


function printPayCompPDF()
{//先往打印表存数
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
//	var selNo = GrpContGrid.getSelNo();
//	if(selNo == null || selNo == 0)
//	{
//		alert("请选择一条记录");
//		return false;
//	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
	
	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	var cDueMoney = GrpContGrid.getRowColData( tChked[0], 6 );//应交金额
	var cDif = GrpContGrid.getRowColData( tChked[0], 12 );//余额
	var cPayDate = GrpContGrid.getRowColData( tChked[0], 13 );

	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '92' and standbyflag2='"+cPayNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.all('PayNo').value = GrpContGrid.getRowColData( tChked[0], 1 );
		fm.all('DueMoney').value = cDueMoney;
		fm.all('Dif').value = cDif;
		fm.all('PayDate').value = cPayDate;
		fm.action="./IndiPayForPrtIns.jsp";
		fm.submit();
	}
	else
	{
		printPDF();
	}
}

function printPDF()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}

	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '92' and standbyflag2='"+cPayNo+"'");
	fm.action = "../uw/PrintPDFSave.jsp?Code=092&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
	fm.submit();
}
function printPDF2()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}

	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '97' and standbyflag2='"+cPayNo+"'");
	fm.action = "../uw/PrintPDFSave.jsp?Code=097&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
	fm.submit();
}
function printBatchPayCompPDF()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	
	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if(tChked.length == 0)
	{
		fm.all('pdfSql').value = mSql;
		fm.action="./IndiPayAllBatPrtInsNew.jsp";
		fm.submit();
	}
	else
	{
		fm.action="./IndiPayForBatPrtInsNew.jsp";
		fm.submit();
	}
}

function printAllHospital()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '97' and standbyflag2='"+cPayNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.action="./IndiHospitalPrint.jsp?PayNo="+cPayNo;
		fm.submit();
	}
	else
	{
		printPDF2();
	}
}

function printAllHospitalNew()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	//var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '97' and standbyflag2='"+cPayNo+"'");
	//if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	//{
	//	fm.action="./IndiHospitalPrint.jsp?PayNo="+cPayNo;
	//	fm.submit();
	//}
	//else
	//{
	//	printPDF2();
	//}
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=97&StandbyFlag2="+cPayNo;
	fm.submit();
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

function printPayCompPDFNew()
{//先往打印表存数
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
//	var selNo = GrpContGrid.getSelNo();
//	if(selNo == null || selNo == 0)
//	{
//		alert("请选择一条记录");
//		return false;
//	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
	
	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
  var cDueMoney = GrpContGrid.getRowColData( tChked[0], 6 );//应交金额
  var cDif = GrpContGrid.getRowColData( tChked[0], 12 );//余额
	var cPayDate = GrpContGrid.getRowColData( tChked[0], 13 );

//	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '92' and standbyflag2='"+cPayNo+"'");
//	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
//	{
//		fm.all('PayNo').value = GrpContGrid.getRowColData( tChked[0], 1 );
//		fm.all('DueMoney').value = cDueMoney;
//		fm.all('Dif').value = cDif;
//		fm.all('PayDate').value = cPayDate;
//		fm.action="./IndiPayForPrtIns.jsp";
//		fm.submit();
//	}
//	else
//	{
//		printPDF();
//  }
  var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=92&StandbyFlag2="+cPayNo+"&StandbyFlag1="+cPayDate+"&StandbyFlag3="+cDueMoney+"&StandbyFlag4="+cDif;
	fm.submit();
}