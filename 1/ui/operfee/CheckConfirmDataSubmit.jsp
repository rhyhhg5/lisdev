<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
  <%@page import="com.sinosoft.lis.db.LPEdorEspecialDataDB"%>
  <%@page import="com.sinosoft.lis.schema.LPEdorEspecialDataSchema"%>  
  <%@page import="com.sinosoft.task.CommonBL"%>
  <%@page import="com.sinosoft.lis.bq.BQ"%>
  <%@page import="com.sinosoft.lis.pubfun.MMap"%>
  <%@page import="com.sinosoft.lis.pubfun.PubSubmit"%>
  <%@page import="com.sinosoft.utility.SysConst"%>
  <%@page import="com.sinosoft.utility.VData"%>
<%  
  MMap mMMap = new MMap();
  
  String getnoticeno=request.getParameter("GetNoticeNo");//付费号
  String contno=request.getParameter("TempContNo");
  String tWorkNo = CommonBL.createWorkNo();
  
  String drawercode=request.getParameter("DrawerCode");//给付对象 0-投保人 1-被保人 2-转入投保人账户
  LPEdorEspecialDataSchema tDrawerCodeSchema = new LPEdorEspecialDataSchema();
  tDrawerCodeSchema.setEdorAcceptNo(getnoticeno);
  tDrawerCodeSchema.setEdorNo(getnoticeno);
  tDrawerCodeSchema.setEdorType("MJ");
  tDrawerCodeSchema.setDetailType("DrawerCode");
  tDrawerCodeSchema.setPolNo(BQ.FILLDATA);
  tDrawerCodeSchema.setEdorValue(drawercode);
        
  mMMap.put(tDrawerCodeSchema, SysConst.DELETE_AND_INSERT);
  
  LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
  tLPEdorEspecialDataDB.setEdorAcceptNo(tWorkNo);
  tLPEdorEspecialDataDB.setEdorNo(tWorkNo);
  tLPEdorEspecialDataDB.setEdorType("MJ");
  tLPEdorEspecialDataDB.setEdorValue(getnoticeno);
  tLPEdorEspecialDataDB.setDetailType("CWYB");
  tLPEdorEspecialDataDB.setState("1");
  tLPEdorEspecialDataDB.setPolNo(contno);
  mMMap.put(tLPEdorEspecialDataDB.getSchema(), SysConst.INSERT);
  
  String FlagStr = "";
  String Content = "";
   
  VData data = new VData();
  data.add(mMMap);
  PubSubmit tPubSubmit = new PubSubmit();
  if (!tPubSubmit.submitData(data, "")) 
  {
    Content="给付信息审核失败：";
    FlagStr="Fail";
  }
  else
  {
    Content="给付信息审核完成！";
    FlagStr="Succ";
  }
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>

