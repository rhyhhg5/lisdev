<%
//expirBenefitQueryInput.jsp
//程序功能：满期给付抽档
//创建日期：2007-11-20 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="expirBenefitQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="expirBenefitQueryInputInit.jsp"%>
</head>
<%
	
        GlobalInput tGI = new GlobalInput();
	    tGI = (GlobalInput)session.getValue("GI");
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);               	               	
      
%>
<SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
/*
var sql = "select char(date('" + CurrentTime + "') + 15 days) from dual ";
var rs = easyExecSql(sql);
if(rs)
{
  CurrentTime = rs[0][0];
}
*/
var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT> 
<body onload="initForm();">

<form name=fm action='' target=fraSubmit method=post>

    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付任务查询：
      </td>
     </tr>
    </table>
    <Div  id= "divAlivePayMulti" style= "display: ''">
      <Table  class= common>
      	<tr>
      		<td class= title>满期日起期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      		<td class= title>满期日止期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>
      		<td class= title>抽档日期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=BatchDate ></td>
      		</tr>
      		<tr>
      			<td class= title>操作人</td>
      			<td class= input><Input class= common  name=Operator ></td>
      			<td class= title>操作机构</td>
      			<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename  name=ManageComName readonly></td>
      			<td class= title>给付方式</td>
      			<td class= input><Input class= "codeno"  name=PayMode CodeData="0|^1|现金^2|现金支票^3|转帐支票^4|银行转帐" verify="给付方式|notnull" ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" style="width: 50" readonly><Input class=codename  name=PayModeName readonly></td>
      			</tr>
      	<tr>
      		<td class= title>保单号</td>
      		<td class= input><Input class= common name=ContNo></td>
      		<td class= title>客户号</td>
      		<td class= input><Input class=common name=AppntNo></td>
      		<td class= title>代理人部门</td>
      		<td class= input><Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName></td>
      </tr>
      <tr>
      	<td class= title>代理人</td>
      	<td class= input><Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this],[0]);" onkeyup="return showCodeListKey('AgentCode',[this],[0]);"></td>
      	<td class= title>给付任务状态</td>
      	<td class= input><Input class= "codeno"  name=PayTaskState CodeData="0|^0|待给付^1|给付完成^2|待给付确认" verify="给付状态|notnull" ondblclick="return showCodeListEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PayTaskStateName></td>
      	<td class= title>打印状态</td>
      	<td class= input><Input class= "codeno"  name=PrintState  CodeData="0|^0|未打印^1|已打印" verify="打印状态|notnull" ondblclick="return showCodeListEx('PrintState',[this,PrintStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PrintState',[this,PrintStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PrintStateName></td>
      	</tr>
      	<tr>
      	    <TD class= title>
				选择类型:
			</TD>
			<TD class= input>
				<input class="codeNo" name="queryType" value="1" CodeData="0|^1|请选择类型^2|普通单^3|少儿单^4|常无忧B" readOnly
    	          ondblclick="return showCodeListEx('queryType',[this,queryTypeName],[0,1]);" 
    	          onkeyup="return showCodeListEx('queryType',[this,queryTypeName],[0,1]);"><Input class="codeName" name="queryTypeName" readonly>
			</TD>
			<td class= title>给付确认日期起期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=GetStartDate ></td>
      		<td class= title>给付确认日期止期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=GetEndDate ></td>
      	</tr>
      	<tr>
			<td class= title>给付完成日期起期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=ConfStartDate ></td>
      		<td class= title>给付完成日期止期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=ConfEndDate ></td>
      		<td  class= title>是否审核通过</td>
	        <td  class= input><Input class= "codeno"  name=CheckState  value = 0 CodeData="0|^0|全部^1|是^2|否" ondblclick="return showCodeListEx('CheckState',[this,CheckStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListEx('CheckState',[this,CheckStateName],[0,1],null,null,null,1);" ><Input class=codename readonly name=CheckStateName >
      	</tr>
      </Table>    
    </Div>     					                                                                            
    <INPUT VALUE="查询满期任务明细" class = cssbutton TYPE=button onclick="easyQueryClick();">
<!-- 显示或隐藏信息 --> 
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 满期任务清单：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLjsGetGrid" >
  					</span>
  			  	</td>
  			</tr>
  			<tr><td>  					
  				<center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">			
    			</center>
    		</td>
    		</tr>
  			<tr><td class= titleImg>给付明细：</td></tr>
  			<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLjsGetDrawGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>     
  	</div>
  	  	<table>
  		<tr>
  			<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOptionButton);">
    		</td>
  			<td class= titleImg>操作按钮：</td>
  			</tr>
  		</table>  
  	<div id= "divOptionButton" style= "display: ''">
  	<!-- 显示或隐藏信息 -->
		<INPUT VALUE="打印给付清单" TYPE=button class = cssbutton onclick="printGetList()">
    <INPUT VALUE="批量打印给付通知" class = cssbutton TYPE=button onclick="printAllNoticeNew();">
	  <INPUT VALUE="打印给付通知书" class = cssbutton TYPE=button onclick="printOneNoticeNew();">
	  <!-- 
	  <INPUT VALUE="满期任务撤销" TYPE=button class = cssbutton onclick="getTaskCansel()">
	   -->
    <INPUT VALUE="变更给付方式" TYPE=button class = cssbutton onclick="changePayMode()">
    <input class = cssbutton value="打印常无忧B批单"  type=button onclick="print330501Data();">
	  </div>
	  
	  <div id="PayModeChangeID" style="display: 'none'">
	    <Table  class= common>
	      <tr>
  			  <td class=titleImg colspan="10">给付信息变更：</td>
  			</tr>
      	<tr>
      		<td class= title>原给付方式</td>
      		<td class= input><Input type="hidden" name=PayModeOld><Input name=PayModeOldName style="width: 130" class=readonly readonly></td>
      		<td class= title>变更为</td>
      		<td class= input><Input class= "codeno" name=PayModeNew CodeData="0|^1|现金^2|现金支票^3|转帐支票^4|银行转帐" verify="给付方式|notnull" ondblclick="return showCodeListEx('PayMode2',[this,PayModeNewName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode2',[this,PayModeNewName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename name=PayModeNewName readonly style="width: 80"></td>
      	</tr>
      	<tr>
    			<td class= titleImg colspan="2">账户信息由:</td>
      	</tr>
      	<tr>
    			<td class= title>开户银行</td>
    			<td class= input><Input class=readonly name=BankCodeOld style="width: 50" readonly ><input class=readonly name=BankNameOld readonly style="width: 80"></td>
    			<td class= title>账户名</td>
    			<td class= input><Input class=readonly name=AccNameOld style="width: 130" readonly></td>
    			<td class= title>账号</td>
    			<td class= input><Input class=readonly name=BankAccNoOld style="width: 130" readonly></td>
    		</tr>
      	<tr>
    			<td class=titleImg colspan="2">变更为:</td>
    		</tr>
      	<tr>
    			<td class= title>开户银行</td>
    			<td class= input><Input class=codeNo name=BankCode style="width: 50" ondblclick="return showCodeList('Bank',[this,BankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankName],[0,1],null,null,null,1);"><input class=codename name=BankName readonly style="width: 80"></td>
    			<td class= title>账户名</td>
    			<td class= input><Input class=common1 name=AccName></td>
    			<td class= title>账号</td>
    			<td class= input><Input class=common1 name=BankAccNo></td>
    			<td class= title>账号确认</td>
    			<td class= input><Input class=common1 name=BankAccNo2></td>
      	</tr>
      	<tr>
    			<td class= title>
    			  <INPUT VALUE="保全确认" TYPE=button class = cssbutton onclick="changePayModeSubmit()">
          </td>
      	</tr>
      </Table>  
	  </div>
	  
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td> <INPUT VALUE="保单明细查询" class = cssbutton TYPE=button onclick="returnParent();"> </td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndiContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage4.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage4.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage4.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage4.lastPage();">			
    </center>  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    		险种信息
    		</td>
    	</tr>
    </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanPolGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
    </center>  
  </Div>	 
  <INPUT type= "hidden" name= "QuerySql" value= "">  	
  <INPUT type= "hidden" name= "taskNo" value="">
  <INPUT type= "hidden" name= "GetNoticeNo" value="">  
  <INPUT type= "hidden" name= "contNoForModify" value="">  
  <input type=hidden id="TempQueryType" name="TempQueryType">	    	 
  <div id="test"></div>     
  <div id="ErrorsInfo"></div>                                                                        
</form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>      
      <iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>  	
</body>
</html>
