var turnPage = new turnPageClass();
var turnPageHospital = new turnPageClass();
var turnPageSpecHospital = new turnPageClass();
var isQuery = false;

function printAllHospital()
{
	//检验是否有内容
	if(isQuery)
	{
		if(HospitalGrid.mulLineCount != 0)
		{
			if(SpecHospitalGrid.mulLineCount != 0)
			{
				//在这里调用打印程序
				//fm.submit();
				var cManageCom = fm.all('ManageCom').value; 
				//var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '97' and standbyflag2='"+cManageCom+"'");
				//if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
				//{
				//	fm.action = "IndiHospitalQuerySave.jsp";
				//	fm.submit();
				//}
				//else
				//{
				//	printPDF2();
				//}
				var showStr="正在准备打印数据，请稍后...";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.target = "fraSubmit";
				fm.action="../uw/PDFPrintSave.jsp?Code=97&StandbyFlag2="+cManageCom;
				fm.submit();
			}
		}else
		{
			alert("没有需要打印的记录！");
		}
	}
	else
	{
		alert("请查询后再打印！");
	}
}

function printPDF2()
{
	var cManageCom = fm.all('ManageCom').value; 
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '97' and standbyflag2='"+cManageCom+"'");
	fm.action = "../uw/PrintPDFSave.jsp?Code=097&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
	fm.submit();
}

function queryHospital()
{
		//查询指定医院
	var sqlHospital = "select b.HospitName,b.Address from LDHospital b "
										+ " where associateclass in ('1','2') and b.managecom like '"
										+fm.all('ManageCom').value+"%'"
										;
	turnPageHospital.queryModal(sqlHospital, HospitalGrid);
	//查询推荐医院
	var sqlSpecHospital = "select b.HospitName,b.Address from LDHospital b "
										+ " where associateclass in ('1') and b.managecom like '"
										+fm.all('ManageCom').value+"%'"
										;
	turnPageSpecHospital.queryModal(sqlSpecHospital, SpecHospitalGrid);
	isQuery = true;
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.focus();
		window.location.reload();
	}
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}