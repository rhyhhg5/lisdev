<%
//程序名称：GrpExpirBenefitBatchInput.jsp
//程序功能：满期给付抽档
//创建日期：2008-11-5 14:34:14  
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>

	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpexpirBenefitBatchInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpexpirBenefitBatchInputInit.jsp"%>
</head>
<%
	
        GlobalInput tGI = new GlobalInput();
	      tGI = (GlobalInput)session.getValue("GI");
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);               	               	
      
%>
<SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
var operator = '<%=tGI.Operator%>';
/*
var sql = "select char(date('" + CurrentTime + "') + 15 days) from dual ";
var rs = easyExecSql(sql);
if(rs)
{
  CurrentTime = rs[0][0];
}
*/
var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT> 
<body onload="initForm();">

<form name=fm action="./GrpExpirBenefitBatchSave.jsp" target=fraSubmit method=post>
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付保单查询：
      </td>
     </tr>
    </table>
    <Div  id= "divAlivePayMulti" style= "display: ''">
      <Table  class= common>
      	<tr>
      		<td class= title>满期日起期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      		<td class= title>满期日止期</td>
      		<td class= input><Input class="common" dateFormat="short" name=EndDate readonly></td>
      		<td class= title>操作机构</td>
      		<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD></td>
      		</tr>
      	<tr>
      		<td class= title>保单号</td>
      		<td class= input><Input class= common name=GrpContNo></td>
      		<td class= title>客户号</td>
      		<td class= input><Input class=common name=AppntNo></td>
      		      			<TD class= title>
				给付方式:
			</TD>
			<TD class= input>
					<Input class= "codeno"  name=PayTaskState CodeData="0|^2|团体统一给付^3|个人给付" verify="给付状态|notnull" ondblclick="return showCodeListEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PayTaskStateName>
			</TD>
      	<!--	<td class= title>代理人部门</td>
      		<td class= input><Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName></td>-->
      </tr>
      <tr>
      <!--	<td class= title>代理人</td>
      	<td class= input><Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this],[0]);" onkeyup="return showCodeListKey('AgentCode',[this],[0]);"></td> -->

		</TR>
      	</tr>
      	   	<TR>
      </Table>    
    </Div>     					                                                                            
    <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick();">   
    <INPUT VALUE="批量抽档" TYPE=button class = cssbutton id="payMulti" onclick="doBatchBenefit();">
	  <INPUT VALUE="单份抽档" class = cssbutton TYPE=button onclick="doBenefit();">
<!-- 显示或隐藏信息 --> 
		<br><br>

 
		<br>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    		团体保单信息：
    		</td>
    	</tr>
    </table> 	
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLjsGetGrid" >
  					</span>
  			  	</td>
  			</tr>
  			<tr><td>  					
  				<center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
     				<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    			</center>
    		</td>
    		</tr>
    	</table>     
  	</div>
  	
  		     <TD class= title>
				给付方式:
			</TD>
			<TABLE>
			<TR>
				<!--
			<TD class= input>
				<!--
				<input class="codeNo" name="queryType" value="1" CodeData="0|^1|请选择类型^2|团体统一给付^3|个人给付" readOnly
    	          ondblclick="return showCodeListEx('queryType',[this,queryTypeName],[0,1]);" 
    	          onkeyup="return showCodeListEx('queryType',[this,queryTypeName],[0,1]);"><Input class="codeName" name="queryTypeName" readonly>
    	  
    	   <select name='queryType' onchange="a()">
    	   	<option value="1">请选择类型</option>
    	   	<option value="2">团体统一给付</option>
    	   	<option value="3">个人给付</option>
    	  </select>
    	</TD> 
    	-->
    	<TD>
    	<div   id=x   style="display:none">
			<select name="PayMode" style="width: 128; height: 23"  >
				<option value = "0">请选择缴费方式</option>
				<%
				//20080708 zhanggm 整个系统缴费方式统一，改成从ldcode表中取。
				//再增加缴费方式就在payMode数组中增加类型。
				String [] payMode = {"1","2","3","4","9"};
				String tSql = "";
				String payModeName = "";
				for(int i=0;i<payMode.length;i++)
				{
				  tSql = "select codename('paymode','" + payMode[i] + "') from ldcode";
				  payModeName = new ExeSQL().getOneValue(tSql);
				%>
				<option value = <%=payMode[i]%>><%=payModeName%></option>
				<%
				}
				%>
			</select>
    	</div>
		<div   id=y   style="display:none">
						<select name="GPayMode" style="width: 128; height: 23"  >
				<option value = "0">请选择缴费方式</option>
				<%
				String [] GpayMode = {"2","3","11"};
				String GSql = "";
				String GpayModeName = "";
				for(int i=0;i<GpayMode.length;i++)
				{
				  GSql = "select codename('paymode','" + GpayMode[i] + "') from ldcode";
				  GpayModeName = new ExeSQL().getOneValue(GSql);
				%>
				<option value = <%=GpayMode[i]%>><%=GpayModeName%></option>
				<%
				}
				%>
			</select>
		</div>
	</TD>
		</TR>
	</TABLE>
  	  	<table>
  		<tr>
  			<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 抽档信息：
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
		<!--	<td> <INPUT VALUE="保单明细查询" class = cssbutton TYPE=button onclick="returnParent();"> </td> -->
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndiContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">			
    </center>  	
  	</div>
  	 		<!--
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
   
    		<td class= titleImg>
    		转账信息：
    		</td>
    	</tr>
    </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanLCInsuredGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
    </center>  
  </Div>	 
   -->
 <br>                  
 <INPUT type= "hidden" name= "QuerySql" value="">          
 <INPUT type= "hidden" name= "QueryType" value="">                                         
</form>
	
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>     
      		<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>   	
</body>
</html>
