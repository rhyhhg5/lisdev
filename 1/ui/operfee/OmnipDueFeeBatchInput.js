//               该文件中包含客户端需要处理的函数和事件
//OmnipDueFeeBatchInput.js
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var SqlPDF = ""; 
var queryCondition = "";
var queryCondition2 = "" ;

var queryCondition2 = "" ;

function PersonSingle()
{
    if(beforeSubmit())
    {  
    	if (!checkValidateDate()) return false;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
      fm.submit();
    }	
}

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    //此处content可能较长，使用showModalDialog可能有问题，现将错误显示到页面
    fm.all("ErrorsInfo").innerHTML = content;
    
    content = "续期续保抽档操作完成，发生的错误信息见页面底部";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }

   resetForm();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  afterContQuery();
	  afterJisPayQuery();
	  
  }
  catch(re)
  {
  	alert("在GrpDueFeeBatchInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{

   
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function GrpMulti()
{
  if(IndiContGrid.mulLineCount == 0)
  {
    alert("请先查询");
    return false;
  }
  if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	

  fm.submit();	

}


//验证日期输入是否正确 
function CheckDate()
{
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1){
    return false;
  }
  return true;		
}

// 查询按钮
/*********************************************************************
 *  查询合同号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initIndiContGrid();
	
    //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量	
	if(fm.all("StartDate").value!="")
	{		
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{		
		SubTime=fm.all("EndDate").value;
	}
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	//对抽档给出三十天的验证 	
	//var tStartDate = new Date(fm.StartDate.value.replace(/-/g,"\/")).getTime();
	//var tEndDate = new Date(fm.EndDate.value.replace(/-/g,"\/")).getTime();
	//var leaveDate = (tEndDate - tStartDate)/(24*60*60*1000) ;
	
	var getDate ="select current date from dual";
	var result = easyExecSql(getDate);
	
	if(result != "null" && result != "" && result != null)
	{
		var t1 = new Date(result[0][0].replace(/-/g,"\/")).getTime();
		var tEndDate = new Date(fm.EndDate.value.replace(/-/g,"\/")).getTime();
	  var leaveDate = (t1 - tEndDate)/(24*60*60*1000) ;
	  if(Math.abs(leaveDate)> 60 )
	  {	
		  alert("查询时间不能超过系统当前时间60天！");
		  return false ;
	  }
	}
	
	managecom = fm.ManageCom.value;	
	
	//销售渠道
	var SaleChnl=fm.all('SaleChnl').value;
	var wherePar="";
	if(SaleChnl!=null&&SaleChnl!=""&&SaleChnl!="null")
    {
       if(SaleChnl=="1")//个险
       {
           wherePar+=" and salechnl not in ('04','13') "; 
       }
       if(SaleChnl=="2")//银保
       {
           wherePar+=" and salechnl  in ('04','13') "; 
       }
    }
	 
	var strSQL=""; 
  	strSQL = "select ContNo,PrtNo,AppntName,CValiDate,SumPrem,"
	     + "  nvl((select AccGetMoney from LCAppAcc  where CustomerNo=lccont.appntno),0),prem,min(paytodate),"
	     + "  (select codename from ldcode where codetype='paymode' and code=PayMode),"
	     + "  ShowManageName(ManageCom),getUniteCode(AgentCode),'' "
	     + "from LCCont "
	     +" where exists (select 1 from lcpol a,lmriskapp b where a.riskcode = b.RiskCode and (a.StandbyFlag1 is null or a.StandbyFlag1 = '0') "
	     + "and b.risktype4='4' and b.riskcode not in ('332301','334801','340501','340601') and a.Contno = lccont.ContNO and a.grpcontno = '00000000000000000000') "
	     +" and (select count(1) from LCContState where StateType='GracePeriod' and state='0' and  LCContState.contNo = LCCont.ContNo) = 0 "
	     + "and AppFlag='1'  and ContType='1' and (StateFlag is null or StateFlag = '1') "
	     + wherePar
	     + "    and (select count(1) from ljspay where othernotype='2' and OtherNo= LCCont.ContNo) = 0  "
	     + "    and exists "
	     + "       (select 'X' from LCPol "
		 + "       where contno=lccont.contno and grpcontno = '00000000000000000000' "			
		 + "          and PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime + "' "
		 + "          and (polstate is null or (polstate is not null and polstate not like '02%%' and polstate not like '03%%'))"
		 + "          and managecom like '"+managecom+"%%'"
		 //续期
		 + "          and ((PaytoDate<payEndDate "
		 + "          	and PayIntv>0 "
		 + "          	and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode)) "
		 + " 		 	or (PaytoDate>=payEndDate "//添加万能产品续保
		 + "		  	and exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode) "
		 + "		  	and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2')) "
		 + "          )   "
		 + "          and AppFlag='1' and (StopFlag='0' or StopFlag is null)"
       	 + "          and (select count(1) from LJSPayPerson a where a.contno=lcpol.contno and a.polno=lcpol.PolNo)=0"
         + "      ) "
         //20150729过滤处于期缴保费缓交状态的保单
         + " and not exists (select 1 from lccontstate where contno=lccont.contno and state='1' and statetype='ULIDEFER')"
         + " and managecom like '"+managecom+"%'"
         + "          and PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime + "' "
		 + "group by contNo,PrtNo,AppntName,CValiDate,SumPrem,appntno,prem,PayMode,ManageCom,AgentCode "
		 + "order by contNo "
		 + "with ur "
			 ;
  	
  
    fm.QuerySql.value = strSQL;
	turnPage.queryModal(strSQL, IndiContGrid); 
	initPolGrid();  
	fm.all("StartDate").value=CurrentTime;  
	fm.all("EndDate").value=SubTime;
	contCurrentTime = CurrentTime;
    contSubTime = SubTime;
	CurrentTime=tempCurrentTime;  
	SubTime=tempSubTime;          
		if(IndiContGrid.mulLineCount == 0)
	  {
	    alert("没有符合条件的可催收保单信息");
	  }
	
}

/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getpoldetail()
{
    var arrReturn = new Array();
	  var tSel = IndiContGrid.getSelNo();	
	  if( tSel == 0 || tSel == null )
	  {
		   alert( "请先选择一条记录，再点击返回按钮。" );
	  }	
	  else
	  {
		   getPolInfo();
    }  	                                          
                         	               	
}
/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolInfo()
{
	
	  var tRow = IndiContGrid.getSelNo() - 1;	        
		var tContNo=IndiContGrid.getRowColData(tRow,1);  
		var tGetNotice = IndiContGrid.getRowColData(tRow,12);
		var inLJSPayPerson = "";   //险种是否在LJSPayPerson中有记录
		if (tGetNotice =='' || tGetNotice ==null)
		{
			inLJSPayPerson =" not ";
		}
		
		var strSQL = "select a.RiskSeqNo,a.InsuredName, (select riskname  from lmrisk where riskcode=a.riskcode) ,a.RiskCode, (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), "
			+ "	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate "  
			+ "from lcpol a where 1=1"
			+ "  and a.contno='" + tContNo + "' "
			+ "  and a.appflag='1' "
			+ "  and a.PaytoDate>='"+contCurrentTime+"' and a.PaytoDate<='"+contSubTime+"' "
			//续期
			+ "  and (  a.PayIntv>0 and a.PaytoDate<a.EndDate "  //非趸缴、未满期
			+ "       or "
			//续保
			+ "         exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=a.riskcode) "  //可续保
			+ "         and exists (select riskCode from LMRiskApp where riskCode = a.riskCode and riskType5 = '2' and (endDate > a.endDate or endDate is null or 'Y' = (select rnewFlag from LMRisk where riskCode = a.riskCode))) "  //一年期、未停办
			+ "      ) "
			+ "  and a.managecom like '"+managecom+"%%'"
			+ "  and a.PolNo " + inLJSPayPerson + " in (select PolNo from LJSPayPerson) "
			+ "order by polNo "
			+ "with ur"			
			;
		
        turnPage2.queryModal(strSQL, PolGrid); 
}


/*********************************************************************
 *  重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为resetForm()
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
  try
  {    
	  afterContQuery();
	  afterJisPayQuery();
	  
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 



/*********************************************************************
 *  校验日期范围
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkValidateDate()
{
	//校验录入的起始日期和终止日期必须在今天之前，否则不予提交
	var startDate=fm.StartDate.value;
	var endDate=fm.EndDate.value;
	if(startDate==''||endDate=='')
	{
		alert("请录入查询日期范围！");
		return false;
	}
	if(compareDate(startDate,endDate)==1)
	{
		alert("起始日不能晚于截止日!");
		return false;
	}
	return true;
}

//催收完成后查询保单
function afterContQuery()
{
		// 初始化表格
	initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}
	 var strSQL = "select a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.Prem,nvl((select AccGetMoney from LCAppAcc where CustomerNo=a.appntno),0),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),getUniteCode(a.AgentCode),b.GetNoticeNo "
	 + "from LCCont  a, ljspay b, ljspayperson c "
	 + "where a.AppFlag='1' and b.OtherNo=a.ContNo and  b.othernotype='2' "
	  + "and  exists (select 1 from lcpol d,lmriskapp e where d.riskcode = e.RiskCode and e.risktype4 ='4' and d.Contno =a.ContNO)"
	 + "  and c.GetNoticeNo=b.GetNoticeNo and c.ContNo=b.otherno "
	 
	 + "  and  b.MakeDate=current date "
	 + "  and exists"
	 + "      (select 'X' from LCPol "
	 + "      where contno=a.contno "
	 + "        and PaytoDate>='" + CurrentTime + "' "
	 + "        and PaytoDate<='"+SubTime+"' "
	 + queryCondition2
	 + "        and PolNo in (select PolNo from LJSPayPerson) "
	 + "      )"
	 + getWherePart( 'a.ManageCom', 'ManageCom','like')
	 + " group by a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.Prem,a.appntno,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode"
	 + " order by b.GetNoticeNo with ur "
	 ;
	turnPage.queryModal(strSQL, IndiContGrid); 
	initPolGrid(); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	SqlPDF = strSQL;              //用于PDF批打
}


//催收完成后查询催收纪录
function afterJisPayQuery()
{
	// 初始化表格
	initJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}		  

  var strSQL ="	select distinct b.SerialNo,b.MakeDate,'',b.Operator,b.MakeDate from  ljspay b where b.makedate=current date and b.othernotype='2' and  b.MakeTime in (select max(MakeTime) from ljspay where makedate=current date and othernotype='2')";
	 
	turnPage3.queryModal(strSQL, JisPayGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
}


//催收完成后打印通知书
function printNotice()
{
	var tRow = IndiContGrid.getSelNo() ;	
	if (tRow == 0 || tRow == null)
	{
		alert("请选择一个保单");
		return false;
	}

	 var tGetNoticeNo=IndiContGrid.getRowColData(tRow - 1,12);
	 if (tGetNoticeNo =='')
	 {
		 alert("发生错误，原应是应收记录号为空");
		 return false;
	 }
	 	 window.open("../operfee/IndiPrintSel.jsp?GetNoticeNo="+ tGetNoticeNo);

}
/*********************************************************************
 *  投保信息列表中选择一条，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnParent()
{
	//从客户投保信息列表中选择一条，查看投保信息

		 var tSel1 = IndiContGrid.getSelNo();
		 if( tSel1 == 0 || tSel1 == null )
	   {		
		   alert( "请先选择一条记录，或输入保单号，再点击保单明细按钮。" );
	   }else
	   {
	   	  var cContNo = IndiContGrid.getRowColData( tSel1 - 1, 1);		  
				window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType=1");	
					
	   }
}
function printInsManage()
{
	if (IndiContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < IndiContGrid.mulLineCount; i++)
	{
		if(IndiContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	var tGetNoticeNo=IndiContGrid.getRowColData(tChked[0],12);
	if(tGetNoticeNo == "" || tGetNoticeNo == "null" || tGetNoticeNo == null)
	{
	    alert("请先生成催收记录");
		return false;
	}
	/*
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '93' and standbyflag2='"+tGetNoticeNo+"'");

	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
	*/
		fm.all('GetNoticeNo').value = tGetNoticeNo;
		fm.action="./IndiDueFeeInsForPrt_v2.jsp";
		fm.submit();
	/*
	}
	else
	{
		//window.open("../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
		fm.action = "../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
		fm.submit();
	}
	*/
}
function printPDF()
{
	if (IndiContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < IndiContGrid.mulLineCount; i++)
	{
		if(IndiContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	var tGetNoticeNo=IndiContGrid.getRowColData(tChked[0],12);
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '93' and standbyflag2='"+tGetNoticeNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		alert("提取数据失败-->PrtSeq为空");
		return false;
	}
	fm.action="../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
	fm.submit();
}

function printPDFNotice(aCode,aGetNoticeNo,aPrtSeq)
{
		if(aCode=="093")
		{
			fm.action="../uw/PrintPDFSave.jsp?Code="+aCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+aGetNoticeNo+"&PrtSeq="+aPrtSeq;
			fm.submit();
		}else
		{
			fm.action="../uw/PrintPDFSave.jsp?Code="+aCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+aGetNoticeNo+"&PrtSeq="+aPrtSeq;
			fm.submit();
		}
}
function printInsManageBat()
{
	if (IndiContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < IndiContGrid.mulLineCount; i++)
	{
		if(IndiContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if(tChked.length == 0)
	{
		fm.action="./IndiDueFeeAllBatPrt.jsp?strsql="+SqlPDF;
		fm.submit();
	}
	else
	{
		fm.action="./IndiDueFeeForBatPrt.jsp";
		fm.submit();
	}
}
//新PDF单打
function newprintInsManage()
{
	if (IndiContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < IndiContGrid.mulLineCount; i++)
	{
		if(IndiContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}
	var tGetNoticeNo=IndiContGrid.getRowColData(tChked[0],12);
	if(tGetNoticeNo == "" || tGetNoticeNo == "null" || tGetNoticeNo == null)
	{
	    alert("请先生成催收记录");
		return false;
	}
	if(fm.queryType.value=='2'){
	    fm.action = "../uw/PDFPrintSave.jsp?Code=93&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
	}
	if(fm.queryType.value=='3'){
	    fm.action = "../uw/PDFPrintSave.jsp?Code=XB001&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
	}
    fm.submit();
}
//新PDF批打
function newprintInsManageBat()
{
	if (IndiContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < IndiContGrid.mulLineCount; i++)
	{
		if(IndiContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	
	var tGetNoticeNo=IndiContGrid.getRowColData(0,12);
	if(tGetNoticeNo == "" || tGetNoticeNo == "null" || tGetNoticeNo == null)
	{
	    alert("请先生成催收记录");
		return false;
	}
	
	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if(tChked.length == 0)
	{
	    fm.strsql.value = SqlPDF;
		fm.action="./IndiDueFeeInsAllBatPrt.jsp";
		//fm.action="./IndiDueFeeAllBatPrt.jsp?strsql="+SqlPDF;
		fm.submit();
	}
	else
	{
		fm.action="./IndiDueFeeInsForBatPrt.jsp";
		fm.submit();
	}
}

////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}