<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    

<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>

<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="ExRenewaldelayInput.js"></SCRIPT>   

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

<%@include file="ExRenewaldelayInit.jsp"%>
 
   
</head>
<body  onload="initForm();initElementtype();" >
<form action="#" method=post name=fm
    target="fraSubmit">

<table>
    <tr>
        <td class="titleImg">银保万能险缓缴业务查询：</td>
    </tr>
</table>
<table class=common>
	  <tr>
	      <td class= title>管理机构</td>
	      <td class=input>
		  <Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary >
		   </td>
		   <td class= title>缓缴方式</td>
		   <td class=input>
		   <Input class= "codeno" name=Flag verify="缓缴方式|NOTNULL" CodeData="0|^0|全部^1|客户申请^2|系统自动" ondblclick="return showCodeListEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" ><Input class=codename name=SaleChnlTypeName elementtype=nacessary  >
			</td>
			<TD  class= title>
            险种
          </TD>
          <TD  class= input>
            <Input class= codeNo name=RiskCode ondblclick="return showCodeList('umriskcode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('umriskcode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true >
          </TD>			
	   </tr>
	   
	  <TR class= common>  
		
	  <td class= title>销售渠道</td>
	 <td class=input>
	 <Input class= "codeno" name=SaleChnl verify="销售渠道|NOTNULL" CodeData="0|^00|全部^04|银行代理^13|银代直销" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);" ><Input class=codename name=SaleChnlName elementtype=nacessary  >
	 </td>
      <TD  class= title>代理人部门</TD>
      <TD  class= input>
      <Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName>
      </TD> 
      <TD  class= title>代理人</TD>
      <TD  class= input>
      <Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCode" ondblclick="return showCodeList('agentcodetbq',[this,AgentCodeName,null,null],[0,1,3,4],null,fm.AgentGroup.value,'AgentGroup',1);" onkeyup="return showCodeListKey('agentcodetbq',[this,AgentCodeName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName  >
      </TD>
    </TR>
	<tr>
	   <td class= title>缓缴开始日期</td>
		<td class=input>
		<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="应收时间起期|NOTNULL">
	   </td>
	   <td class= title>缓缴截止日期</td>
		<td class=input>
		<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="应收时间止期|NOTNULL">
		</td>
	</tr>
</table>
<br>
	<table>
      <tr class=common>
           <td><input class=cssButton type=button value="下  载"onclick="downloadList();">&nbsp;&nbsp;</td>
      </tr>
	</table>
<input type="hidden" name="fmtransact" value=""></form>
 <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
