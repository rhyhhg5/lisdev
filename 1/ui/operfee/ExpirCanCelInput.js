var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  easyQueryClick();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{

  var riskSql =  " and riskcode not in ('320106','120706','330501','530301') ";

  var subSql = " and not exists (select 1 from ljaget where actugetno = a.getnoticeno) ";

  var strSQL = " select distinct a.getnoticeno, "
             + " a.otherno,d.contno,"
             + " (select appntname from lccont where contno = d.contno), "
             + " (select appntidno from lccont where contno = d.contno), "
             + " (select name from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + " (select idno from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + " a.sumgetmoney,a.getdate,d.insuredno,a.makedate,a.maketime  "
             + " from ljsget a left join ljsgetdraw d on a.getnoticeno = d.getnoticeno"
             + " where a.othernotype='20' and (feefinatype != 'YEI' or d.riskcode = '330501') "
             + subSql
             + riskSql 
             + getWherePart( 'a.getnoticeno','EdorAcceptNo')
             + getWherePart( 'd.ContNo','ContNo') 
             + "order by a.makedate,a.maketime  ";
             
  turnPage1.queryModal(strSQL, LjsGetGrid); 
}

function queryClick()
{
	var edoracceptno=fm.all("EdorAcceptNo").value;
	var contno=fm.all("ContNo").value;
	if((edoracceptno==null||(edoracceptno.trim())=="")&&(contno==null||(contno.trim())=="")){
		alert("请录入保单号或者给付任务号进行查询");
		return false;
	}
  easyQueryClick();
  if(LjsGetGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function selectOne()
{
    var tSelNo = LjsGetGrid.getSelNo();
    fm.GetNoticeNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetNoticeNo");
    fm.TempContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    fm.InsuredNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuredNo");
    
    fm.ContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    
}


//校验查询条件是否正确
function checkDealData()
{
  if(LjsGetGrid.mulLineCount == 0)
  {
    alert("没有符合条件的给付信息");
    return false;
  }
  if(LjsGetGrid.getSelNo() < 1)
  {
    alert("请选择需要确认的给付任务");
    return false;
  }
  
  var tGetNoticeNo = fm.GetNoticeNo.value;
  
  //该保单已经给付确认，不能重复操作
  var sql = "select 1 from ljaget where actugetno = '" + tGetNoticeNo + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    alert("该保单已经给付确认，不能撤销！");
    return false;
  }
  
//校验是否为最近一次的满期抽挡
  var lastSQL = "select 1 from ljsget where getnoticeno='"+tGetNoticeNo+"' and exists (  " +
  		" select 1 from ljsgetdraw a,lcget b where getnoticeno=ljsget.getnoticeno and " +
  		" a.contno=b.contno and a.insuredno=b.insuredno and a.getdutykind=b.getdutykind " +
  		" and a.CurGetToDate!=b.gettodate) with ur ";

  var resultSet = easyExecSql(lastSQL);
  if(resultSet!=null&&resultSet!="")
  {
	alert("该笔满期抽挡不是最新的满期抽挡，请先撤销最近的一次满期抽挡");
	return false;
  }
  	return true;
  
}

//处理选中的数据
function dealData()
{
  if(!checkDealData())
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.action = "./ExpirCanCelSubmit.jsp";
  fm.submit();	
}

function afterCodeSelect(cCodeName, Field)
{
  try	
  {
    if(cCodeName == "PayMode")	
    {
      showPayMode();
    }
    if(cCodeName == "DrawerCode")	
    {
      showDrawerInfo();
    }
    if(cCodeName == "queryType")
    {
      switch(fm.all('queryType').value)
      {
        case "1":
        fm.all('RiskCode1').value = "";
        fm.all('RiskName1').value = "";
        break;        
        case "2":
        fm.all('RiskCode1').value = "";
        fm.all('RiskName1').value = "";
        break;
        case "3":
        fm.all('RiskCode1').value = "320106";
        showAllCodeName();
        break;
        case "4":
        fm.all('RiskCode1').value = "330501";
        showAllCodeName();
        break;
      }
    }
  }
  catch(ex){}
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

