<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： IndiDueFeeInsForBatPrt.jsp
//程序功能： 团险续期批量催缴批打pdf部分
//创建日期： 2007-06-15
//创建人  ： Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
	boolean operFlag=true;
	String GrpContNo = "";
	String Content = "";
	String tPrintServerPath = "";
	String tOutXmlFile  = application.getRealPath("")+"\\";
	String tGetNoticeNo[] = request.getParameterValues("GrpContGrid13");	 
  String tChk[] = request.getParameterValues("InpGrpContGridChk");		//参数格式=” Inp+MulLine对象名+Chk”  
  System.out.println("tOutXmlFileis :"+tOutXmlFile);
    
  LJSPayBSet tLJSPayBSet = new LJSPayBSet();
	LCGrpContSet tLCGrpContSet = new LCGrpContSet();
    
	int tCount = tGetNoticeNo.length;
	for(int j = 0; j < tCount; j++)
	{
		if(tChk[j].equals("1"))  
		{	
			LJSPayBDB  tLJSPayBDB=new LJSPayBDB();
			tLJSPayBDB.setGetNoticeNo(tGetNoticeNo[j]);
			tLJSPayBSet.add(tLJSPayBDB.getSchema());
		}		
	}	

	CErrors mErrors = new CErrors();
	GlobalInput tG = new GlobalInput();      
	tG = (GlobalInput)session.getValue("GI");
	//tG.ClientIP = request.getRemoteAddr();
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
	tG.ServerIP =tG.GetServerIP();
	
	LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
	tLDSysVarSechma.setSysVarValue(tOutXmlFile);
	
	VData tVData = new VData();
	tVData.addElement(tG);
	tVData.addElement(tLDSysVarSechma);
	tVData.addElement(tLJSPayBSet);

	GrpDueFeeBatchPrtBL tGrpDueFeeBatchPrtBL = new GrpDueFeeBatchPrtBL();
	if(tGrpDueFeeBatchPrtBL.submitData(tVData,"batch"))
	{
//		tCount = tGrpDueFeeBatchPrtBL.getCount();
//	    String tFileName[] = new String[tCount];
//	    VData tResult = new VData();
//	    tResult = tGrpDueFeeBatchPrtBL.getResult();
//	    tFileName=(String[])tResult.getObject(0);
//	
//	    String mFileNames = "";
//	    for (int i = 0;i<=(tCount-1);i++)
//	    {
//	    	System.out.println("~~~~~~~~~~~~~~~~~~~"+i);
//	    	System.out.println(tFileName[i]);
//	    	mFileNames = mFileNames + tFileName[i]+":";
//	    }
//	    System.out.println("===================="+mFileNames+"=======================");
//		System.out.println("=========tFileName.length==========="+tFileName.length);
//	
//	    String strRealPath = application.getRealPath("/").replace('\\','/');
//		String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
//	    ExeSQL mExeSQL = new ExeSQL();
//	    tPrintServerPath =  mExeSQL.getOneValue(sql);
	%>
<%-- 		<html> 	
			<script language="javascript">
	    		var printform = parent.fraInterface.document.getElementById("printform");
	    		printform.elements["filename"].value = "<%=mFileNames%>";
	    		printform.action = "<%=tPrintServerPath%>";
	    		printform.submit();
	    	</script>
	    </html>--%>
	<%         			
	}
%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		alert("打印成功");
		//parent.fraInterface.afterInsForBatPrt();
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>