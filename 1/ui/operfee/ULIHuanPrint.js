var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}
function afterSubmit2(FlagStr, content) 
{
  showInfo.close();
  if (FlagStr == "Fail" ) 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  query();
}

//打印按钮对应操作
function print()
{
  if(LCPolGrid.mulLineCount == 0)
  {
    alert("没有需要打印的保单!");
    return false;
  }
  if(!checkPrint())
  {
    return false;
  }
  var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.target = "fraSubmit";
  fm.action="../uw/PDFPrintSave.jsp?Code=OM002&StandbyFlag1="+fm.ContNo1.value+"&StandbyFlag2="+fm.AppntNo1.value;
  fm.submit();
}

function easyQueryClick() 
{
  var printPtate = trim(fm.PrintState.value);
  var tManageCom = trim(fm.ManageCom.value);
  var tStartDate = trim(fm.StartDate.value);
  var tEndDate = trim(fm.EndDate.value);
  
  
  var wherePrintState = "";
  if(printPtate!="" && printPtate!=null)
  {
    wherePrintState = "exists (select 1 from LOPRTManager where code = 'OM002' and othernotype = 'OM' "
                    + "and otherno = a.contno) ";
    if(printPtate=="0")//未打印
    {
      wherePrintState = "and not " + wherePrintState;
    }
    else if(printPtate=="1")//已打印
    {
      wherePrintState = "and " + wherePrintState;
    }
    else
    {
      wherePrintState = "";
    }
  }
  else
  {
    wherePrintState = "";
  }
  var strSql = "select contno ,appntname,PolApplyDate,paytodate, "
             //+ "(case when standbyflag1 = '1' then '保费缓交' else codename('stateflag',stateflag) end) contstate, "
             + "codename('stateflag',stateflag), "
             + "prem, (select name from laagent where agentcode = a.agentcode), "
             + "case when standbyflag1 = '1' then '1' else '0' end, "
             + "case when (select count(1) from LOPRTManager where code = 'OM002' and othernotype = 'OM' "
             + "and otherno = a.contno)=0 then '未打印' else '已打印' end, "
             + "appntno "
             + "from lcpol a where mainpolno = polno and grpcontno = '00000000000000000000' "
             + "and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') "
             + "and stateflag != '0' and standbyflag1 = '1' " 
             + wherePrintState 
             + getWherePart('ContNo', 'ContNo','=')
             + getWherePart('AppntNo', 'AppntNo','=')
             + "and managecom like '" + tManageCom + "%' "
             + "and paytodate between '" + tStartDate + "' and '" + tEndDate + "' "
             + "order by managecom ,contno ";

  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSql, LCPolGrid);
  return true;
}

//Click事件，当点击“查询”图片时触发该函数
function query()
{ 
/*
  var tContNo = trim(fm.ContNo.value);
  var tAppntNo = trim(fm.AppntNo.value);
  if(tContNo == "" && tAppntNo == "")
  {
    alert("客户号和保单号不能全为空！");
    return false;
  }
  */
  if(!easyQueryClick())
  {
    return false;
  }
  if(LCPolGrid.mulLineCount == 0)
  {
    alert("没有查询到数据！");
    return false;
  }
  LCPolGrid.radioBoxSel(1);
  for(i = 0; i < LCPolGrid.mulLineCount; i++)
  {
    if(LCPolGrid.getRowColDataByName(i,"Huan")=="1")
    {
       LCPolGrid.radioBoxSel(i+1);
       break;
	}
  }
  selectOne();
}

function selectOne()
{
  fm.ContNo1.value = LCPolGrid.getRowColDataByName(LCPolGrid.getSelNo()-1,"ContNo");
  fm.AppntNo1.value = LCPolGrid.getRowColDataByName(LCPolGrid.getSelNo()-1,"AppntNo");
}

function checkPrint()
{
  if(LCPolGrid.getSelNo() == "0")
  {
    alert("请选择要打印的保单！");
    return false;
  }
  var tContNo = LCPolGrid.getRowColDataByName(LCPolGrid.getSelNo()-1,"ContNo");
  var sql = "select StateFlag,StandByFlag1 from lcpol where mainpolno = polno and contno = '"+tContNo+"' ";
  var result = easyExecSql(sql);
  var tStateFlag = "";
  var tStandByFlag1 = "";
  var errorInfo = "";
  if(result)
  {
    tStateFlag = result[0][0];
    tStandByFlag1 = result[0][1];
  }
  if("1"!=tStandByFlag1)
  {
    errorInfo = "该保单现在不是缓交状态，不能打印缓交通知书!";
    alert(errorInfo);
    return false;
  }
  if("1"!=tStateFlag)
  {
    errorInfo = "该保单现在是" + ("2"==tStateFlag?"失效":("3"==tStateFlag?"终止":("0"==tStateFlag?"未承保":"错误"))) + "状态，不能打印缓交通知书!";
    alert(errorInfo);
    return false;
  }
  return true;
}

//回车查询
function queryByKeyDown()
{
  if(event.keyCode == "13")
  {
	query();
  }
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}