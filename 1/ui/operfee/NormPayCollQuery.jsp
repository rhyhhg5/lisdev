 
  <%
//程序名称：NormPayCollQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="NormPayCollInput.js">       </SCRIPT>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=gb2312" %>

<!--Step 1--查询是否已经催收 -->
<%
 
  //保费项表  
  LCPremSet mLCPremSet       ;
  LCPremSchema tLCPremSchema ;  
  //个人保单表
  LCPolSet     mLCPolSet     ; 
  LCPolSchema  tLCPolSchema  ;
  //应收个人交费表
  LJSPayPersonSet mLJSPayPersonSet ;
  LJSPayPersonSchema tLJSPayPersonSchema;
  //应收总表
  LJSPaySchema tLJSPaySchema;
  //应收集体交费表
  LJSPayGrpSchema tLJSPayGrpSchema;
  LJSPayGrpSet mLJSPayGrpSet;
  //集体保单表
  LCGrpPolSet mLCGrpPolSet;
  LCGrpPolSchema tLCGrpPolSchema;
  //险种缴费定义表
  LMRiskPaySet mLMRiskPaySet;
  LMRiskPaySchema tLMRiskPaySchema;  
  
  //保存保单号  
  String GrpPolNo = request.getParameter("GrpPolNo"); 
 
  //返回的符合查询条件的纪录的数目
  int recordCount=0;   
  //纪录集体保单中的个人客户数目
  int clientCount=0;   
  
  //输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";

  VData tVData = new VData();
  
//1-查询应收集体交费表中是否存在该数据
   tLJSPayGrpSchema = new LJSPayGrpSchema();
   tLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
   tVData.add(tLJSPayGrpSchema);    
   DuePayCollectivityQueryUI tDuePayCollectivityQueryUI = new DuePayCollectivityQueryUI();
   if(!tDuePayCollectivityQueryUI.submitData(tVData,"QUERY"))
   {
System.out.println("查询应收集体交费表时发现该数据存在！");
    Content = " 查询应收集体交费表失败，原因是: " + tDuePayCollectivityQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";    
   }
   else
  {
//注意--第一个大扩号 
//2-查询集体保单表中是否存在该数据
    // 准备传输数据 VData   
   tVData.clear(); 
   tLCGrpPolSchema = new LCGrpPolSchema();
   tLCGrpPolSchema.setGrpPolNo(GrpPolNo);
   tVData.add(tLCGrpPolSchema); 
      
   GrpPolQueryUI tGrpPolQueryUI   = new GrpPolQueryUI();
   
   if(!tGrpPolQueryUI.submitData(tVData,"QUERY"))
    {
System.out.println("查询集体保单表时没有该数据存在！");
    Content = " 查询集体保单表失败，原因是: " + tGrpPolQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
    }  
   else
    {//注意--第二个大扩号  
    
     String UrgePayFlag="";//催付标记  
     tVData.clear();
     mLCGrpPolSet = new LCGrpPolSet();
     tLCGrpPolSchema = new LCGrpPolSchema();
     tLMRiskPaySchema  = new LMRiskPaySchema();
     tVData = tGrpPolQueryUI.getResult();   
     mLCGrpPolSet.set((LCGrpPolSet)tVData.getObjectByObjectName("LCGrpPolSet",0));
     tLCGrpPolSchema=(LCGrpPolSchema)mLCGrpPolSet.get(1);
     tLMRiskPaySchema.setRiskCode(tLCGrpPolSchema.getRiskCode());//险种编码
     tLMRiskPaySchema.setRiskVer(tLCGrpPolSchema.getRiskVersion());//险种版本
     tVData.clear();
     tVData.add(tLMRiskPaySchema);
//查询险种缴费定义表，得到催付标记字段                 
     LMRiskPayQueryUI tLMRiskPayQueryUI=new LMRiskPayQueryUI();
   if(!tLMRiskPayQueryUI.submitData(tVData,"QUERY"))
    {
System.out.println("查询险种缴费定义表时没有该数据存在！");
    Content = " 查询险种缴费定义表失败，原因是: " + tLMRiskPayQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
    }  
   else
    {//注意--第2-1个大扩号 
     mLMRiskPaySet = new LMRiskPaySet();
     tLMRiskPaySchema  = new LMRiskPaySchema();      
     tVData = tLMRiskPayQueryUI.getResult();   
     mLMRiskPaySet.set((LMRiskPaySet)tVData.getObjectByObjectName("LMRiskPaySet",0));
     tLMRiskPaySchema=(LMRiskPaySchema)mLMRiskPaySet.get(1);
     UrgePayFlag=tLMRiskPaySchema.getUrgePayFlag();//催付标记
     if(UrgePayFlag.equals("N"))
     {
System.out.println("查询险种缴费定义表时催付标记为 N ！");
    Content = " 查询险种缴费定义表成功，催付标记为 N ，请去财务处直接交费";
    FlagStr = "Fail";
    }         
   else
    { 
//注意--第三个大扩号     
    tVData.clear();       
//3-查询并从保费项表和个人保单表中得到相关数据
    tLCPolSchema = new LCPolSchema();
    tLCPolSchema.setGrpPolNo(GrpPolNo);
    tVData.add(tLCPolSchema);
    DuePayFeeQueryUI tDuePayFeeQueryUI   = new DuePayFeeQueryUI();
   // 查询纪录 
   if (!tDuePayFeeQueryUI.submitData(tVData,"QUERY"))
    {
System.out.println("查询保费项表和个人保单表时出错！没有相关数据存在");
    Content = " 查询保费项表和个人保单表失败，原因是: " + tDuePayFeeQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
    }   
   else
    {
//注意--第四个大扩号    

	tVData.clear();
	mLCPremSet = new LCPremSet();
	mLCPolSet  = new LCPolSet();
	tVData = tDuePayFeeQueryUI.getResult();
       //从保费项表得到多个纪录（组合主键:保单号码，责任编码，交费计划编码）
	mLCPremSet.set((LCPremSet)tVData.getObjectByObjectName("LCPremSet",0));
       //从个人保单表得到多条纪录数据:(客户姓名和操作员) 唯一主键:保单号,用于后面的循环
        mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));

      clientCount= mLCPolSet.size();//集体中的个人数 
      recordCount= mLCPremSet.size();//得到符合查询条件的纪录的数目，后面循环时用到

System.out.println("clientCount="+clientCount);              
System.out.println("recordCount="+recordCount);              
  

//4--从个人保单表中得到数据
   int indexNum = 0;
   float SumDuePayMoney = 0;
   int payCount = 0;
     
   //新的保费项表的存储容器
   LCPremSet tUpdateLCPremSet = new LCPremSet();
   tUpdateLCPremSet = new LCPremSet();
   //应收个人费用表数据存储容器
   mLJSPayPersonSet = new LJSPayPersonSet();  
      
    String AppntName = ""; //投保人名称
    String AgentCode = ""; //代理人编码
    String PolNo = "" ;    //保单号码
    String ContNo = "" ;   //总单/合同号码   
   
    //个人保单表是单一主键，唯一，而保费项表是组合主键，不唯一，对应多个纪录 
    //因此，从保费项表查出的N条纪录中的多条纪录对应个人保单表的一条纪录
      
    for(int i=0;i<clientCount;i++)
  {       
       tLCPolSchema = new LCPolSchema();
       tLCPolSchema=(LCPolSchema)mLCPolSet.get(i+1); 
            
         PolNo = tLCPolSchema.getPolNo();
         ContNo=tLCPolSchema.getContNo();
         if(tLCPolSchema.getAppntName()!= null)
            AppntName = tLCPolSchema.getAppntName().trim();
         else 
            AppntName = "null"; 
         if(tLCPolSchema.getAgentCode()!= null)
            AgentCode = tLCPolSchema.getAgentCode().trim();
         else 
            AgentCode = "null";
            
//5-事务操作
//插入数据到应收个人表中,插入数据到应收总表中,更新保费项表
   indexNum = 0;
   //新的保费项表的存储容器 
   
while(indexNum<recordCount) //循环查找符合条件的纪录
 {
 indexNum = indexNum+1;

//保费项表更新 
 tLCPremSchema = new LCPremSchema();
 tLCPremSchema=(LCPremSchema)mLCPremSet.get(indexNum);
 
//如果个人保单号相等，则从保费项表中取值到应收个人交费表

 if(PolNo.equals(tLCPremSchema.getPolNo()))
 {
System.out.println("PolNo="+PolNo); 
System.out.println("tLCPremSchema.getPolNo()="+tLCPremSchema.getPolNo()); 

//应收个人费用表插入数据 
 tLJSPayPersonSchema  = new LJSPayPersonSchema();
 
//这里只是得到部分字段的值
  tLJSPayPersonSchema.setPolNo(PolNo);
  tLJSPayPersonSchema.setContNo(ContNo);
  tLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
  tLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
  tLJSPayPersonSchema.setGrpPolNo(GrpPolNo);
  tLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes());
  tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem()); 
  tLJSPayPersonSchema.setAppntNo(tLCPremSchema.getAppntNo()); 
  tLJSPayPersonSchema.setOperator("hzm");
//银行编码待定，在BL层中设置

//更新数据
//集体交费，则每个人的交费次数是一致的，这个应该是放在循环外面的 
  payCount= tLCPremSchema.getPayTimes()+1;   
//已交费次数加1     
  tLCPremSchema.setPayTimes(tLCPremSchema.getPayTimes()+1);
//累计保费=实际保费+上次累计保费  
  tLCPremSchema.setSumPrem(tLCPremSchema.getSumPrem()+tLCPremSchema.getPrem());
//总应收金额 =各个纪录中的实际保费之和
  SumDuePayMoney = SumDuePayMoney+tLCPremSchema.getPrem();    
  
//加入新的保费项表的存储容器里
  tUpdateLCPremSet.add(tLCPremSchema);
//加入应收个人费用表数据存储容器里
  mLJSPayPersonSet.add(tLJSPayPersonSchema);  
 
 }//判断结束
 
 } //while 循环结束
 
 }//for循环结束
 
  //加入应收总表存储数据容器里
  tLJSPaySchema = new LJSPaySchema();
  tLJSPaySchema.setGetNoticeNo(GrpPolNo);
  tLJSPaySchema.setSumDuePayMoney(SumDuePayMoney);
  tLJSPaySchema.setOtherNo(GrpPolNo);//其它号码
  tLJSPaySchema.setOtherNoType("1");//其它号码类型
  tLJSPaySchema.setOperator("hzm");//操作员  

  //加入应收集体交费表容器
  tLJSPayGrpSchema = new LJSPayGrpSchema();
  tLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
  tLJSPayGrpSchema.setPayCount(payCount);
  tLJSPayGrpSchema.setContNo(ContNo);
  tLJSPayGrpSchema.setSumDuePayMoney(SumDuePayMoney);
  tLJSPayGrpSchema.setOperator("hzm");//操作员 
    

  DuePayFeeCollUI  tDuePayFeeCollUI = new DuePayFeeCollUI();
  try
  {  
//放入向后台提交的容器内，执行事务操作  
  tVData.clear();
  tVData.add(tUpdateLCPremSet);
  tVData.add(mLJSPayPersonSet);
  tVData.add(tLJSPaySchema);
  tVData.add(tLJSPayGrpSchema);  	
  tDuePayFeeCollUI.submitData(tVData,"INSERT");     
  } 
 catch(Exception ex)
  {
      Content = "INSERT 失败，原因是:" + ex.toString();
      FlagStr = "Fail";  
  }
      //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tDuePayFeeCollUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "INSERT 成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "INSERT 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  }//对应第四个大扩号

  }//对应第三个大扩号
 }// 对应第2-1个大扩号
 }//对应第二个大扩号
 
}//对应第一个大扩号 
       

%>                                     
                  


<!--Step 2--查询操作 -->

<%
     recordCount = 0;
     int indexNum = 0; 
     float SumDuePayMoney = 0;//总应收金额             
       
     //应收个人费用表数据存储容器   
   mLJSPayPersonSet = new LJSPayPersonSet();  
   tLJSPayPersonSchema  = new LJSPayPersonSchema();  
   tLJSPayPersonSchema.setGrpPolNo(GrpPolNo);  
   //查询应收个人费用表数据
   DuePayPersonFeeQueryUI tDuePayPersonFeeQueryUI = new DuePayPersonFeeQueryUI();
   tVData.clear();
   tVData.add(tLJSPayPersonSchema);
   if(!tDuePayPersonFeeQueryUI.submitData(tVData,"QUERY"))
    {
System.out.println("查询应收个人交费表失败！279");
    Content = " 查询失败，原因是: " + tDuePayPersonFeeQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
    }   
   else
   {
     recordCount = 0;
     indexNum = 0;
     tVData.clear();
     tVData = tDuePayPersonFeeQueryUI.getResult();
     mLJSPayPersonSet.set((LJSPayPersonSet)tVData.getObjectByObjectName("LJSPayPersonSet",0));    
      //得到符合查询条件的纪录的数目，后面循环时用到
     recordCount=mLJSPayPersonSet.size();
System.out.println("recordCount="+recordCount); 

    Content = " 查询成功 ";
    FlagStr = "Succ";    

   }        

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tDuePayPersonFeeQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 查询成功";
      FlagStr = "Succ= ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>                                        
<html>
<script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
<%  
    while(indexNum<recordCount)
    {
       indexNum=indexNum+1;
       tLJSPayPersonSchema  = new LJSPayPersonSchema();
       tLJSPayPersonSchema=(LJSPayPersonSchema)mLJSPayPersonSet.get(indexNum); 
       SumDuePayMoney=SumDuePayMoney+tLJSPayPersonSchema.getSumDuePayMoney();
%>
<script language="javascript">
     parent.fraInterface.NormPayCollGrid.addOne("NormPayCollGrid");
     parent.fraInterface.fm.NormPayCollGrid1[<%=indexNum-1%>].value="<%=tLJSPayPersonSchema.getPolNo() %>";     
     parent.fraInterface.fm.NormPayCollGrid2[<%=indexNum-1%>].value="<%=tLJSPayPersonSchema.getDutyCode()%>";
     parent.fraInterface.fm.NormPayCollGrid3[<%=indexNum-1%>].value="<%=tLJSPayPersonSchema.getPayPlanCode()%>"; 
     parent.fraInterface.fm.NormPayCollGrid4[<%=indexNum-1%>].value="<%=tLJSPayPersonSchema.getSumDuePayMoney()%>"; 
     parent.fraInterface.fm.NormPayCollGrid5[<%=indexNum-1%>].value="<%=tLJSPayPersonSchema.getSumActuPayMoney()%>"; 
     parent.fraInterface.fm.NormPayCollGrid6[<%=indexNum-1%>].value="<%=tLJSPayPersonSchema.getPayCount() %>";

</script>
<%
}
%>
<%
//查询应收集体交费表
       SumDuePayMoney = 0;
String AppntNo = "";
int    PayCount = 0;
float SumActuPayMoney = 0;
String ContNo = "";
String PayDate = "";

   tVData.clear();
   tLJSPayGrpSchema = new LJSPayGrpSchema();
   tLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
   tVData.add(tLJSPayGrpSchema);    
   DuePayFeeCollQueryUI tDuePayFeeCollQueryUI = new DuePayFeeCollQueryUI();
   // 查询纪录 
   if (!tDuePayFeeCollQueryUI.submitData(tVData,"QUERY"))
   {
System.out.println("查询应收集体交费表失败！");
    Content = " 查询失败，原因是: " + tDuePayFeeCollQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
   }
   else
   {
   	mLJSPayGrpSet  = new LJSPayGrpSet();
	tVData = tDuePayFeeCollQueryUI.getResult();
       //从应收集体交费表得到多个纪录
	mLJSPayGrpSet.set((LJSPayGrpSet)tVData.getObjectByObjectName("LJSPayGrpSet",0));
      
       tLJSPayGrpSchema = new LJSPayGrpSchema();
       tLJSPayGrpSchema=(LJSPayGrpSchema)mLJSPayGrpSet.get(1);                   
         AppntNo = tLJSPayGrpSchema.getAppntNo();	
         PayCount = tLJSPayGrpSchema.getPayCount();
         SumDuePayMoney = tLJSPayGrpSchema.getSumDuePayMoney();
         SumActuPayMoney = tLJSPayGrpSchema.getSumActuPayMoney();
         ContNo = tLJSPayGrpSchema.getContNo();
         PayDate = tLJSPayGrpSchema.getPayDate();         

   }  
%>
<script language="javascript">
    var number=<%=recordCount%>;
    parent.fraInterface.fm.all('AppntNo').value = "<%=AppntNo%>";
    parent.fraInterface.fm.all('PayCount').value = "<%=PayCount%>";
    parent.fraInterface.fm.all('SumDuePayMoney').value = "<%=SumDuePayMoney %>";
    parent.fraInterface.fm.all('SumActuPayMoney').value = "<%=SumActuPayMoney %>";       
    parent.fraInterface.fm.all('ContNo').value = "<%=ContNo %>";       
    parent.fraInterface.fm.all('PayDate').value = "<%=PayDate %>";       

//隐藏字段，存放集体保单号，提交保存时，作为表单参数传递    
    parent.fraInterface.fm.all('GrpPolNo').value =parent.fraInterface.fmQuery.all('GrpPolNo').value;  
      if(number==0)
   {
   parent.fraInterface.initForm();                         
   }
</script>
</body>
</html>