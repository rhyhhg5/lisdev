  <%
//程序名称：LJSPayPersonQureyResult.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="LJSPayPersonInput.js">       </SCRIPT>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //查询到的结果集
  LCPremSet mLCPremSet=new LCPremSet();
  LCPolSet  mLCPolSet = new LCPolSet() ; 
  //接收信息，并作校验处理。
  LCPremSchema tLCPremSchema   = new LCPremSchema();
  LCPolSchema  tLCPolSchema    = new LCPolSchema();

  DuePayFeeQueryUI tDuePayFeeQueryUI   = new DuePayFeeQueryUI();
  //返回的符合查询条件的纪录的数目
  int recordCount=0;  
  //输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  
  if(request.getParameter("PolNo").length()>0)
    tLCPremSchema.setPolNo(request.getParameter("PolNo"));

  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tLCPremSchema);
     
   // 查询纪录 
   if (!tDuePayFeeQueryUI.submitData(tVData,"QUERY"))
    {
    Content = " 查询失败，原因是: " + tDuePayFeeQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
    }   
   else
    {
	tVData.clear();
	tVData = tDuePayFeeQueryUI.getResult();
       //得到应收个人交费表数据
	mLCPremSet.set((LCPremSet)tVData.getObjectByObjectName("LCPremSet",0));
       //得到保单信息数据 客户姓名和操作员
        mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
      //得到符合查询条件的纪录的数目，后面循环时用到
        recordCount=mLCPremSet.size();
System.out.println("recordCount="+recordCount);              
     }    
   
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tDuePayFeeQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 查询成功";
      FlagStr = "Succ,recordCount= "+recordCount;
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>                                        
<html>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
<script language="javascript"> 
//查出因为责任编码和交费计划编码不同而出现的情况的次数
//根据次数，动态增加表格，显示这些数据
//在显示页面上已经默认并显示了一个表格
//因此，若情况有多种，则增加的表格序号从2开始
  var count=1;
  var num=<%=recordCount%>;   
  var innerHTML="";
 
    while(count<num)
  {
  count=count+1;  
  innerHTML = innerHTML +"<td><IMG  src= \"../common/images/butExpand.gif\" style= \"cursor:hand;\" OnClick= \"showPage(this,divLJSPayPerson"+count+");\" ></td>";
  innerHTML = innerHTML +"<Div  id= divLJSPayPerson"+count+" style= \"display: ''\">";
  innerHTML = innerHTML +"<table  class= common  >";
  innerHTML = innerHTML +"<TD  class= input><Input class= common name=DutyCode"+count+" > </TD>";     
  innerHTML = innerHTML +"<TD  class= input><Input class= common name=PayPlanCode"+count+" > </TD> ";
  innerHTML = innerHTML +"<TD  class= input><Input class= common name=SumActuPayMoney"+count+" > </TD>";
  innerHTML = innerHTML +"<TD  class= input><Input class= common name=PayDate"+count+" > </TD> ";
  innerHTML = innerHTML +"<TD  class= input><Input class= common name=PayCount"+count+" > </TD>";   
  innerHTML = innerHTML +"</table>";
  innerHTML = innerHTML +"</div>";
  innerHTML = innerHTML +"<TR></TR>"; 
  }
  try
  { 
    parent.fraInterface.fm.all('spanLCPremCode').innerHTML=innerHTML; 
  }
  catch(ex)
  {
    alert(ex);
  }
</script>

<%
int indexNum = 0;
tLCPremSchema = null;
tLCPolSchema = null;

//对应保费项表字段
String DutyCode="";
String PayPlanCode="";
int    PayTimes=0;
double  Mult=0;
double  StandPrem=0;
double  Prem=0;
double  SumPrem=0;
String AppntNo="";

//从个人投保单中得到投保人姓名，代理人编码
String AppntName="";
String AgentCode="";

//对应应收个人交费表字段
double  SumDuePayMoney=0;
double  SumActuPayMoney=0;
String PayDate="";
String LastPayToDate="";


//从个人保单表中得到数据
int LCPcount=0;
LCPcount=mLCPolSet.size();
if(LCPcount>0)
{ 
 tLCPolSchema=(LCPolSchema)mLCPolSet.get(1); 
 if(tLCPolSchema.getAppntName()!= null)
  AppntName = tLCPolSchema.getAppntName().trim();
 else 
  AppntName = "random";
  
 if(tLCPolSchema.getAgentCode()!= null)
  AgentCode = tLCPolSchema.getAgentCode().trim();
 else 
  AgentCode = "null";
}
System.out.println("line: 157 & recordCount="+recordCount);  
//循环，将查到的数据赋给表格中的值，表格随查询结果动态增长，序号从1开始
while(indexNum<recordCount)
 {
 indexNum = indexNum+1;
 tLCPremSchema=(LCPremSchema)mLCPremSet.get(indexNum);
 //这里只是得到部分字段的值

//对查询到的值为空时需要处理，并且要去掉空格，否则将会出错
 if(tLCPremSchema.getDutyCode()!= null)
  DutyCode = tLCPremSchema.getDutyCode().trim();
 else 
  DutyCode = "null";
  
 if(tLCPremSchema.getPayPlanCode()!= null)
  PayPlanCode = tLCPremSchema.getPayPlanCode().trim();
 else 
  PayPlanCode  = "null";   
  
  PayTimes = tLCPremSchema.getPayTimes();

  //注意double的空值如何比较!!! 
  Mult = tLCPremSchema.getMult();
  StandPrem=tLCPremSchema.getStandPrem();
  Prem = tLCPremSchema.getPrem();     
  SumPrem = tLCPremSchema.getSumPrem();

 if(tLCPremSchema.getAppntNo()!= null)
  AppntNo = tLCPremSchema.getAppntNo().trim();
 else 
  AppntNo= "random";
 
//对应保费项表字段,注意，这只是对应一个责任编码  
Prem=Mult*StandPrem; //实际保费=份数*每期保费

//SumPrem=SumPrem+Prem; //累计保费=上次累计保费+本次保费 可以设一个隐含字段
//对应应收个人交费表字段
SumDuePayMoney=SumDuePayMoney+Prem;      //是多个责任编码下的Prem字段的总和
//SumActuPayMoney   总实交保费待输入
//PayDate           交费日期，应该自动输入
//LastPayToDate=PaytoDate; //原交至日期=交至日期
  
%>
<script language="javascript">
    parent.fraInterface.fm.all('DutyCode'+<%=indexNum%>).value = "<%=DutyCode%>";
    parent.fraInterface.fm.all('PayPlanCode'+<%=indexNum%>).value = "<%=PayPlanCode%>";    
    parent.fraInterface.fm.all('SumActuPayMoney'+<%=indexNum%>).value = "<%=SumActuPayMoney%>"; 
    parent.fraInterface.fm.all('PayDate'+<%=indexNum%>).value = "<%=PayDate %>";
    parent.fraInterface.fm.all('PayCount'+<%=indexNum%>).value = "<%=PayTimes %>";
</script>
<%
}
%>
<script language="javascript">
    var number=<%=recordCount%>;
    parent.fraInterface.fm.all('AppntNo').value = "<%=AppntNo%>";
    parent.fraInterface.fm.all('Name').value = "<%=AppntName%>";
    parent.fraInterface.fm.all('SumDuePayMoney').value = "<%=SumDuePayMoney %>";
    parent.fraInterface.fm.all('Operateor').value = "<%=AgentCode %>";       
//隐藏字段，存放查询到的纪录数，用于后面保存时的循环处理    
    parent.fraInterface.fm.all('tableNum').value ="<%=recordCount %>";       
    parent.fraInterface.fm.all('PolNo').value =parent.fraInterface.fmQuery.all('PolNo').value;  
      if(number==0)
   {
   parent.fraInterface.initForm();                         
   }
    window.close();
</script>
</body>
</html>

