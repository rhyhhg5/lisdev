 <html> 
<%
//程序名称：IndiDueFeeInput.jsp
//程序功能：个人保费催收，实现数据从保费项表到应收个人表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
</script>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="IndiDueFeeInputCancel.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="IndiDueFeeInputInitCancel.jsp"%>
</head>
<body  onload="initForm();" >

<form name=fm action=./IndiDueFeeQueryCancel.jsp target=fraSubmit method=post>
<table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo2 >
          </TD>
          <TD  class= title>
            应收记录号
          </TD>
          <TD  class= input>
            <Input class=common name=getNoticeNo>
          </TD>
        </TR>        
    </table>
    <tr>
    <TD>
     <INPUT VALUE="查询" class= cssbutton TYPE=button onclick="easyQueryClick();"> 
     
    <TD> 
     </tr>
    <table>
    <tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 应收记录
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
<center>
      <INPUT VALUE="首页" class= cssbutton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= cssbutton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= cssbutton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= cssbutton TYPE=button onclick="getLastPage();"> 
</center>	  
  	</div>
  	<p>
    <INPUT VALUE="个单续期撤销" TYPE=button class= cssbutton onclick="PersonSingle()">   
    <INPUT type= "hidden" name= "Operator" value= ""> 
    <INPUT type= "hidden" name= "ContNoSelected" value= ""> 
    <INPUT type= "hidden" name= "PrtNoSelected" value= "">
    <INPUT type= "hidden" name= "GetNoticeNoSelected" value= "">
  	</p>

</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
