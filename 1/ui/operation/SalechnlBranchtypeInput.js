
var turnPage = new turnPageClass();

//查询
function easyQueryClick() {

	// 书写SQL语句
	var strSQL = "select code,code1,codename from ldcode1 where 1=1 and codetype = 'salechnl' ";
	
	var SalesChannels = fm.SalesChannels.value;
	var AcquisitionTypes = fm.AcquisitionTypes.value;
	var ChannelsType = fm.ChannelsType.value;
	
	if (fm.SalesChannels.value != "") {
		strSQL = strSQL + " and code=" + "'" + SalesChannels + "'";
	}
	if (fm.AcquisitionTypes.value != "") {
		strSQL = strSQL + " and code1=" + "'" + AcquisitionTypes + "'";
	}
	if (fm.ChannelsType.value != "") {
		strSQL = strSQL + " and codename=" + "'" + ChannelsType + "'";
	}
	//执行sql得到结果集
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		SaleChannelId.clearData();
		alert("未查询到满足条件的数据！");
		return false;
	}
	turnPage.pageDivName = "divturnPage";
	turnPage.queryModal(strSQL, SaleChannelId);
}


//删除
function easyDeleteClick() {
	fm.operatortype.value = "DELETE"; //标记标签赋值
	var i = 0;
	for (i = 0; i < SaleChannelId.mulLineCount; i++) {
		if (SaleChannelId.getSelNo(i)) {
			//取出表格的值付给隐藏域标签
			var code  = SaleChannelId.getRowColData(SaleChannelId.getSelNo(i) - 1, 1);
			var code1 = SaleChannelId.getRowColData(SaleChannelId.getSelNo(i) - 1, 2);
			var codeName = SaleChannelId.getRowColData(SaleChannelId.getSelNo(i) - 1, 3);
			break;
		}
	}
	fm.all("code").value = code;
	fm.all("code1").value = code1;
	fm.all("codeName").value = codeName;
	
	//校验
	if(code ==null || code == "" && code1 == null || code1 == "" && codeName == null || codeName == ""){
		alert("您没有选中左侧单选按钮，不能执行删除操作!")
		return false;
	}else{
		fm.all("Content").value ="删除数据成功!";
	}
	
	//执行数据操作
	if(confirm("您确实想删除记录吗?")){
		fm.submit();
	}
}

//更新
function easyUpdateClick(){
	fm.operatortype.value = "UPDATE";//标记标签赋值
	var i = 0;
	for (i = 0; i < SaleChannelId.mulLineCount; i++) {
		if (SaleChannelId.getSelNo(i)) {
			//取出表字段
			var code = SaleChannelId.getRowColData(
					SaleChannelId.getSelNo(i) - 1, 1);
			var code1 = SaleChannelId.getRowColData(
					SaleChannelId.getSelNo(i) - 1, 2);
			var codeName = SaleChannelId.getRowColData(
					SaleChannelId.getSelNo(i) - 1, 3);
			break;
		}
	}
	//校验1
	if(code ==null || code == "" && code1 == null || code1 == "" && codeName == null || codeName == ""){
		alert("您没有选中左侧单选按钮，不能执行更新除操作!")
		return false;
	}
	
	//校验2
	//將原始选中的行表格数据赋值隐藏域中
	fm.all("code").value = code;
	fm.all("code1").value = code1;
	fm.all("codeName").value = codeName;
	
	//增加数据必须保证数据不能增加数据库中三条通的的数据
	//a)取值作为查询条件
	var codeByGet = fm.all("SalesChannels").value;
	var code1ByGet = fm.all("AcquisitionTypes").value;
	var codeNameByGet = fm.all("ChannelsType").value;
	
	//b)查询sql并赋值给隐藏域中
	var resultSql = easyExecSql("SELECT distinct code FROM ldcode1 WHERE Codetype='salechnl" +
								"' AND code='"+codeByGet+
								"' AND code1='"+code1ByGet+
								"' AND codename='"+codeNameByGet+
								"' with ur");
	
	if(!resultSql == "" || !resultSql == null){
		alert("您还没有更新值的内容或者您更新的内容在数据库中已经存在!");
		return false;
	}else{
		fm.all("Content").value ="更新数据成功!";
	}
	
	//提交数据
	if(confirm("您确实想更新记录吗?")){
		fm.submit();
	}
	
}

//单击单选按钮触发的事件
function returnTableField(){
	var i = 0;
	for (i = 0; i < SaleChannelId.mulLineCount; i++) {
		if (SaleChannelId.getSelNo(i)) {
			//取出表字段
			var code = SaleChannelId.getRowColData(
					SaleChannelId.getSelNo(i) - 1, 1);
			var code1 = SaleChannelId.getRowColData(
					SaleChannelId.getSelNo(i) - 1, 2);
			var codeName = SaleChannelId.getRowColData(
					SaleChannelId.getSelNo(i) - 1, 3);
			break;
		}
	}

	//首先判断条件值
	if(code!=null && code!="" && code1!=null && code1!="" && codeName!=null && codeName!=""){
		//赋值编码
		fm.all("SalesChannels").value = code;
		fm.all("AcquisitionTypes").value = code1;
		fm.all("ChannelsType").value = codeName;
	
		//查询数据库看是否存在数据
		var sqlCodeRe = easyExecSql("SELECT codename FROM ldcode WHERE codetype IN ('salechnl','lcsalechnl') AND code='"+code+"' with ur");
		var sqlCode1Re = easyExecSql("SELECT codename FROM ldcode WHERE codetype = 'branchtype' AND code='"+code1+"' with ur");
		var sqlCodeNameRe = easyExecSql("SELECT codename FROM ldcode WHERE codetype = 'branchtype2' AND code='"+codeName+"' with ur");
	
		//赋值编码之对应名字
		fm.all("SalesChannelsName").value = sqlCodeRe;
		fm.all("AcquisitionTypesName").value = sqlCode1Re;
		fm.all("ChannelsTypeName").value = sqlCodeNameRe;
	}
	
}

//增加
function easyInsertClick(){
	//操作标记
	fm.operatortype.value = "INSERT";
	
	//增加数据必须保证数据不能增加数据库中三条通的的数据
	//a)取值作为查询条件
	var codeByGet = fm.all("SalesChannels").value;
	var code1ByGet = fm.all("AcquisitionTypes").value;
	var codeNameByGet = fm.all("ChannelsType").value;

	//校验1
	if(codeByGet == "" || !codeByGet == null && code1ByGet == "" || !code1ByGet == null && codeNameByGet == "" || !codeNameByGet == null){
		alert("增加的内容不能为空!");
		return false;
	}
	
	//校验2
	//b)查询sql并赋值给隐藏域中
	var resultSql= easyExecSql("SELECT distinct code FROM ldcode1 WHERE Codetype='salechnl" +
								"' AND code='"+codeByGet+
								"' AND code1='"+code1ByGet+
								"' AND codename='"+codeNameByGet+
								"' with ur");
	if(!resultSql == "" || !resultSql == null){
		alert("数据库中已有相同的字段,不能增加此些数据!");
		return false;
	}else{
		fm.all("Content").value ="增加数据成功!";
	}
	
	//提交数据
	if(confirm("您确实想增加记录吗?")){
		fm.submit();
	}
}

//页面提示信息
function afterSubmit(FlagStr,Content){
	if(FlagStr == "Fail"){
		 var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + Content;
		 showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		 easyQueryClick();
	}else{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + Content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		easyQueryClick();
	}
}










