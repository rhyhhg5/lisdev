
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：BankMessUpdInput.jsp
	//程序功能：银行信息维护
	//创建日期：2014-05-05 14:57:36
	//创建人  ：zjd
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
	//个人下个人   
	String tContNo = "";
	String tFlag = "";
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	tFlag = request.getParameter("type");
%>
<script>    
    var contNo = "<%=tContNo%>";          //个人单的查询条件.
    var operator = "<%=tGI.Operator%>";   //记录操作员
    var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
    var type = "<%=tFlag%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="../common/jsp/UsrCheck.jsp"%> 
<SCRIPT src="./SalechnlBranchtypeInput.js"></SCRIPT>
<%@include file="SalechnlBranchtypeInit.jsp"%>
<title>销售渠道和展业类型</title>
</head>
<body onload="initForm();initElementtype();">
	<form action="./SalechnlBranchtypeInputSave.jsp " method=post name=fm
		target="fraSubmit">
		<!-- 保单信息部分 -->
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>查询条件</td>
			</tr>
		</table>
		<table class=common align=center>
			<TR class=common>

				<TD class=title>销售渠道</TD>
				<TD class=input><Input class="codeNo" name=SalesChannels
					verify="销售渠道|NOTNULL"
					ondblclick="return showCodeList('statuskind',[this,SalesChannelsName],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('statuskind',[this,SalesChannelsName],[0,1],null,null,null,1);"><input
					class=codename name=SalesChannelsName 
					elementtype=nacessary></TD>

				<TD class=title>展业类型</TD>
				<TD class=input><Input class="codeNo" name=AcquisitionTypes
					verify="展业类型|NOTNULL"
					ondblclick="return showCodeList('branchtype',[this,AcquisitionTypesName],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('branchtype',[this,AcquisitionTypesName],[0,1],null,null,null,1);"><input
					class=codename name=AcquisitionTypesName 
					elementtype=nacessary></TD>

				<TD class=title>渠道类型</TD>
				<TD class=input><Input class="codeNo" name=ChannelsType
					verify="渠道类型|NOTNULL"
					ondblclick="return showCodeList('branchtype2',[this,ChannelsTypeName],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('branchtype2',[this,ChannelsTypeName],[0,1],null,null,null,1);"><input
					class=codename name=ChannelsTypeName 
					elementtype=nacessary></TD>

			</TR>
		</table>

		<INPUT VALUE="查   询" class=cssButton TYPE=button onclick="easyQueryClick();"> 
		<INPUT VALUE="删   除" class=cssButton TYPE=button onclick="easyDeleteClick();"> 
		<INPUT VALUE="更   新" class=cssButton TYPE=button onclick="easyUpdateClick();">
		<INPUT VALUE="增   加" class=cssButton TYPE=button onclick="easyInsertClick();">

		<Div id="divLCGrp1" style="display: ''" align=center>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanSaleChannelId">
					</span></td>
				</tr>
			</table>
		</Div>
		
		<!-- 操作标记 -->
		<input type=hidden name=operatortype value="">
		<!-- 储存表格的字段 -->
		<input type=hidden name=code value="">
		<input type=hidden name=code1 value="">
		<input type=hidden name=codeName value="">
		<!-- 成功之后的提示信息 -->
		<input type=hidden name=Content value="">
		
		
			
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
