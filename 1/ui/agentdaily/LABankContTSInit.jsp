<%
//程序名称：LABankContInput.jsp
//程序功能：
//创建日期：2005-03-20 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>                                           
<script language="JavaScript">

function initInpBox()
{ 
  try
  {        
   
    fm.all('ProtocolNo').value ='';
    fm.all('SignDate').value='';
    fm.all('ManageCom').value ='';
    fm.all('AgentCom').value ='';
    fm.all('RepresentA').value ='';
    fm.all('RepresentB').value ='';
    fm.all('StartDate').value ='';
    fm.all('EndDate').value ='';
    fm.all('CalType').value ='24';
    fm.all('BranchType').value ='<%=BranchType%>';
    fm.all('BranchType2').value ='';
    
  }
  catch(ex)
  {
    alert("在LAContInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {

    initInpBox();
    initContGrid();
  }
  catch(re)
  {
    alert("LAContInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ContGrid;
function initContGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="险种编码";          		        //列名
    iArray[1][1]="100px";      	      		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[1][4]="bankriskcode";
    iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值CodeQueryBL
    
      
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称";          		        //列名
    iArray[2][1]="200px";      	      		//列宽
    iArray[2][2]=20;            			//列最大值
    iArray[2][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    
    iArray[3]=new Array();
    iArray[3][0]="手续费比例";          		        //列名
    iArray[3][1]="100px";      	      		//列宽
    iArray[3][2]=20;            			//列最大值
    iArray[3][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    
    iArray[4]=new Array();
    iArray[4][0]="委托范围";  //列名
    iArray[4][1]="0px";  //列宽
    iArray[4][2]=100;   //列最大值
    iArray[4][3]=1;   //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    iArray[5]=new Array();
    iArray[5][0]="最高保额";  //列名
    iArray[5][1]="0px";  //列宽
    iArray[5][2]=100;   //列最大值
    iArray[5][3]=1;   //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

	iArray[6]=new Array();
	iArray[6][0]="出单方式"; //列名
	iArray[6][1]="80px";        //列宽
	iArray[6][2]=100;            //列最大值
	iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许    
//    iArray[4][4]="F03Code";
    iArray[6][9]="出单方式|NotNull";
    iArray[6][10]="F03Code";
    iArray[6][11]="0|^0|不区分|^1|柜台和银保通出单|^2|自助和网银出单";
     
 
    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
    //这些属性必须在loadMulLine前

    ContGrid.mulLineCount = 1;   
    ContGrid.displayTitle = 1;
  //  ContGrid.selBoxEventFuncName = "showOne";

    ContGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ContGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
