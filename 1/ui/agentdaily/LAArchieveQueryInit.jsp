<%
//程序名称：LAArchieveQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:05:58
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('AgentCode').value = "";
    fm.all('ArchieveNo').value = "";
    fm.all('ArchieveDate').value = "";
    fm.all('PigeOnHoleDate').value = "";
    fm.all('ArchType').value = "";
    fm.all('ArchItem').value = "";
    fm.all('DestoryDate').value = "";
    fm.all('Operator').value = "";
  }
  catch(ex) {
    alert("在LAArchieveQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLAArchieveGrid();  
  }
  catch(re) {
    alert("LAArchieveQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LAArchieveGrid;
function initLAArchieveGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="营销员编码";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="档案编码";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="档案日期";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="归档日期";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=0;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="档案类型";         		//列名
    iArray[5][1]="30px";         		//列名
    iArray[5][3]=0;         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="档案项目";         		//列名
    iArray[6][1]="30px";         		//列名
    iArray[6][3]=0;         		//列名
    
    iArray[7]=new Array();
    iArray[7][0]="销毁日期";         		//列名
    iArray[7][1]="30px";         		//列名
    iArray[7][3]=0;         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="操作员代码";         		//列名
    iArray[8][1]="0px";         		//列名
    iArray[8][3]=0;         		//列名
    
    LAArchieveGrid = new MulLineEnter( "fm" , "LAArchieveGrid" ); 
    //这些属性必须在loadMulLine前

    LAArchieveGrid.mulLineCount = 0;   
    LAArchieveGrid.displayTitle = 1;
    LAArchieveGrid.hiddenPlus = 1;
    LAArchieveGrid.hiddenSubtraction = 1;
    LAArchieveGrid.canSel = 1;
    LAArchieveGrid.canChk = 0;
    LAArchieveGrid.selBoxEventFuncName = "showOne";

    LAArchieveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAArchieveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
