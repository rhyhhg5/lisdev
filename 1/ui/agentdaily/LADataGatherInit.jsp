<%
//程序名称：LADataGatherInit.jsp
//程序功能：
//创建日期：2005-5-30 9:31
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT Language="JavaScript">
  //画面初始化
  function initForm()
  {
    initNoLeadGrpGrid();
    initElementtype();
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
  }
  
  //MulLine 初始化
  function initNoLeadGrpGrid()
  {
    var iArray = new Array();
    //状态内容
    var tState = "";
    tState     = "0|^00|初始状态";
    tState    += "^10|拆分结束";
    tState    += "^11|计算结束";
    tState    += "^12|重计算结束";
    tState    += "^13|佣金前重新计算";
    tState    += "^14|佣金计算完毕";
    tState    += "^21|佣金复核通过";
    tState    += "^22|已打印工资表";
    tState    += "^23|完成工资发放";
    tState    += "^51|完成人事变动";
    tState    += "^99|正在处理过程中";
  
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="提数年月";		    	//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="管理机构";	    	//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="展业类型";       		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="渠道类型";       		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="状态";       		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      /*
      iArray[5][3] =2;           			//是否允许输入,1表示允许，0表示不允许
      iArray[5][10]="End";
      iArray[5][11]=tState;
      iArray[5][12]="12";
      iArray[5][19]="0";
      */

      iArray[6]=new Array();
      iArray[6][0]="计算起期";       		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="计算止期";       		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="处理时间";       		//列名
      iArray[8][1]="110px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="操作员编码";       		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      DataGatherGrid = new MulLineEnter( "fm" , "DataGatherGrid" ); 
      //这些属性必须在loadMulLine前
      DataGatherGrid.mulLineCount = 10;
      DataGatherGrid.displayTitle = 1;
      DataGatherGrid.locked = 1;
      DataGatherGrid.canSel = 0;                 // 设置隐藏单选钮
      DataGatherGrid.hiddenPlus = 1;
      DataGatherGrid.hiddenSubtraction = 1;
      DataGatherGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //DataGatherGrid.setRowColData(1,1,"asdf");
    }
      catch(ex)
    {
	    alert(ex);
    }
  }
</SCRIPT>