<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddManageSave.jsp
//程序功能：
//创建日期：2008-10-14 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LARewardPunishSchema mLARewardPunishSchema = new LARewardPunishSchema();
  LARewardPunishSet  mLARewardPunishSet = new LARewardPunishSet();
  LAAgentSchema  mLAAgentSchema=new  LAAgentSchema();
  LAAddSeManageUI mLAAddSeManageUI = new LAAddSeManageUI();
  //输出参数
  CErrors tError = null;
  CErrors tError2 = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String flag="0";            //验证往后台传入SET还是SHEMA
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin LAWage schema...");
  String tFlag= request.getParameter("Flag");
  //取得佣金信息
  int lineCount = 0;
  String tSel[] = request.getParameterValues("InpWageGridChk");
  String tWageNo[] = request.getParameterValues("WageGrid1");
  String tAgentcode[] = request.getParameterValues("WageGrid2");
  String tManagecom[] = request.getParameterValues("WageGrid4");
  String tBranchAttr[] = request.getParameterValues("WageGrid5");
  String tAClass[] = request.getParameterValues("WageGrid6");
  String tDoneFlag[] = request.getParameterValues("WageGrid7");
  String tRewardTitle[] = request.getParameterValues("WageGrid8");
  String tMoney[] = request.getParameterValues("WageGrid9");
  String tSendGrp[] = request.getParameterValues("WageGrid10");
  String tIdx[] = request.getParameterValues("WageGrid11");
  
  if (tSel!=null)
  {
     lineCount = tSel.length; //行数
     for(int i=0;i<lineCount;i++)
     {
       if(tSel[i].equals("1"))
       {
   	   mLARewardPunishSchema=new LARewardPunishSchema();
    	 mLARewardPunishSchema.setAgentCode(tAgentcode[i]);
       mLARewardPunishSchema.setIdx(tIdx[i]);
       mLARewardPunishSchema.setBranchType(request.getParameter("BranchType"));
       mLARewardPunishSchema.setBranchType2(request.getParameter("BranchType2"));

       mLARewardPunishSet.add(mLARewardPunishSchema);
       }
     }
  }
  System.out.println("update123 LAWage:---" + mLAAgentSchema.getName()+flag);
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLARewardPunishSet);
  tVData.add(tFlag);

  try
  {
    System.out.println("begin to submit UI");
    mLAAddSeManageUI.submitData(tVData);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAAddSeManageUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>
