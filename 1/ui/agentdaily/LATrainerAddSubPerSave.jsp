<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LATrainerAddBSubPerSave.jsp
//程序功能：
//创建日期：2018-06-7
//创建人  ：WangQingMin
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentdaily.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK"%>
<%
	//接收信息，并作校验处理。
	//输入参数

	LATrainerIndexSet tLaTrainerIndexSet = new LATrainerIndexSet();
	LATrainerIndexADMoneyUI tLATrainerIndexADMoneyUI = new LATrainerIndexADMoneyUI();

	//输出参数
	CErrors tError = null;
	String tOperate = request.getParameter("hideOperate");
	tOperate = tOperate.trim();
	String tRela = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String mWageNo = request.getParameter("WageNo");
	String mManageCom = request.getParameter("ManageCom");
	String mFlag = request.getParameter("Flag");
	String mFlag1 = request.getParameter("Flag1");
	//如果加款有录入
	if ("2".equals(mFlag)) {

		LATrainerIndexSchema tLaTrainerIndexSchema = new LATrainerIndexSchema();

		tLaTrainerIndexSchema.setTrainerCode(request.getParameter("TrainerCode"));//组训员工工号

		//###
		tLaTrainerIndexSchema.setTrainerName(request.getParameter("Name"));
		tLaTrainerIndexSchema.setIdx(request.getParameter("Idx"));//主键

		tLaTrainerIndexSchema.setPManageCom(request.getParameter("ManageCom"));//八位管理机构

		tLaTrainerIndexSchema.setADFlag("0");//加款标识
		//-----------------------------------------------加 款记录

		tLaTrainerIndexSchema.setA1(request.getParameter("Money"));//加款金额

		tLaTrainerIndexSchema.setADState(request.getParameter("DoneFlag"));//加款类型

		tLaTrainerIndexSchema.setADReason(request.getParameter("PunishRsn"));//加款原因

		//------------------------------
		tLaTrainerIndexSchema.setWageNo(request.getParameter("WageNo"));
		tLaTrainerIndexSchema.setOperator(tG.Operator);
		tLaTrainerIndexSchema.setWageCode("GX0003");
		tLaTrainerIndexSchema.setWageName("奖励加款");
		tLaTrainerIndexSet.add(tLaTrainerIndexSchema);

	}
	//如果扣款有录入
	if ("2".equals(mFlag1)) {
		LATrainerIndexSchema tLaTrainerIndexSchema = new LATrainerIndexSchema();
		tLaTrainerIndexSchema.setTrainerCode(request.getParameter("TrainerCode"));
		tLaTrainerIndexSchema.setIdx(request.getParameter("Idx"));
		tLaTrainerIndexSchema.setTrainerName(request
				.getParameter("Name"));

		tLaTrainerIndexSchema.setPManageCom(request
				.getParameter("ManageCom"));

		tLaTrainerIndexSchema.setADFlag("1");
		//-----------------------------------------------扣款记录
		tLaTrainerIndexSchema.setA1(request.getParameter("Money1"));
		tLaTrainerIndexSchema.setADState(request
				.getParameter("DoneFlag1"));
		tLaTrainerIndexSchema.setADReason(request
				.getParameter("PunishRsn1"));//扣 款原因
		//-----------------------------------------------   
		tLaTrainerIndexSchema.setWageNo(request.getParameter("WageNo"));
		tLaTrainerIndexSchema.setOperator(tG.Operator);
		tLaTrainerIndexSchema.setWageCode("GX0003");
		tLaTrainerIndexSchema.setWageName("奖励扣款");
		tLaTrainerIndexSet.add(tLaTrainerIndexSchema);

	}

	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr = "";
	tVData.add(tG);
	tVData.add(mWageNo);
	tVData.add(mManageCom);
	tVData.addElement(tLaTrainerIndexSet);

	try {
		tLATrainerIndexADMoneyUI.submitData(tVData, tOperate);
	} catch (Exception ex) {
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail")) {
		tError = tLATrainerIndexADMoneyUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 操作成功! ";
			FlagStr = "Succ";
		} else {
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

	//添加各种预处理
%>
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
