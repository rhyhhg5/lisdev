<%
//程序名称：LAQualificationInput.jsp
//程序功能：功能描述
//创建日期：2005-03-21 15:48:13
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LAQualificationQueryInput.js"></SCRIPT> 
  <%@include file="LAQualificationQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();initElementtype();" >
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 资格证基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLAQualification1" style= "display: ''">
     <Div  id= "test"></div>
<table  class= common align='center' >
  <TR  class= common>
     <TD  class= title>
       管理机构
     </TD>
     <TD  class= input>
     <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
     ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
     onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
    ><Input class=codename name=ManageComName readOnly  elementtype=nacessary> 
     </TD> 
    <TD  class= title>
       代理人状态
    </TD> 
    <td class=input><input name=AgentState class=codeno name="AgentState" verify="代理人状态"
         CodeData="0|^01|在职|^02|离职登记|^03|离职确认"
         ondblClick="showCodeListEx('AgentState',[this,AgentStateName],[0,1]);"
         onkeyup="showCodeListKeyEx('AgentState',[this,AgentStateName],[0,1]);"><Input class=codename name=AgentStateName readOnly >
   </td> 
     
  </TR>   
  <TR  class= common>
    <TD  class= title>
      营销员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GroupAgentCode onchange="return checkvalid();">
    </TD>
    <TD  class= title>
      资格证书号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=QualifNo>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      批准单位
    </TD>
    <TD  class= input>
       <Input class= 'common' name=GrantUnit>
    </TD>
   <TD  class= title>
      发放日期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=GrantDate> 
    </TD>
  </TR>
   <TR  class= common>
    <TD  class= title>
      有效起期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=ValidStart > 
    </TD>
    <TD  class= title>
      有效止期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=ValidEnd > 
    </TD>
   </TR>
   <TR  class= common>
    <TD  class= title>
      资格证书状态
    </TD>
    <TD class=input><input name=QualifState class=codeno name="QualifState"  
         CodeData="0|^0|有效|^1|失效"
         ondblClick="showCodeListEx('QualifStateList',[this,QualifStateName],[0,1]);"
         onkeyup="showCodeListKeyEx('QualifStateList',[this,QualifStateName],[0,1]);"
        ><Input name=QualifStateName class="codename">
  </TD>
     <TD  class= title>
      失效日期
    </TD>
    <TD  class= input>
        <Input class= "coolDatePicker" dateFormat="short" name=InvalidDate> 
    </TD>
    </TR>
    <TR  class= common> 
    	<TD  class= title>
      失效原因
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InvalidRsn >
    </TD>
    <TD  class= title>
      通过考试日期
    </TD>
    <TD  class= input>
        <Input class= "coolDatePicker" dateFormat="short" name=PasExamDate> 
    </TD>
    
    </TR>
    <TR  class= common> 
    	<TD  class= title>
      考试年度
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ExamYear >
    </TD>
    <TD  class= title>
      考试次数
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ExamTimes >
    </TD>
  </TR>
  <tr class =common>
   <TD  class= title>
      距离资格证有效止期天数小于
    </TD>
    <TD  class= input>
      <Input class= "common"  name=DistanceDay > 
    </TD>
  </tr>
  
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>	<input type=hidden class=Common name=querySql > 
    <input type=hidden class=Common name=querySqlTitle > 
    <input type=hidden class=Common name=Title >
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 	
          <INPUT VALUE="下 载"  class=cssbutton  TYPE=button  align=left  onclick="ListExecl();">				
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAQualification1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLAQualification1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAQualificationGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage2" align=center style= "display: '' ">
  
  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  

    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden name=AgentCode value="">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
