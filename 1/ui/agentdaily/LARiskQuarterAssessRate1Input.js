//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          // 使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();
var showInfo;
var mDebug="0";

// 提数操作
function submitForm()
{
    // 首先检验录入框
// if(!verifyInput()) return false;
  if(!checkValue()) return false;
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); // 提交

}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
    initForm();
  }

}

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
   
function easyQueryClick()
{	
  if(!verifyInput()) return false;   
  var  sql = "select idx,agentgrade"
           +",(select gradename from laagentgrade where gradecode = a.agentgrade), "
           + "managecom ,(select name from ldcom where comcode = a.managecom),"
           + "riskcode,(select riskname from lmrisk where riskcode = a.riskcode)," 
           + " insureyear,payintv,code4,'',code1,code2,rate,code5 "
           + "from LADiscount a where a.discounttype='12' and a.code5 = '1'"
       	   +getWherePart('a.branchtype','BranchType')
    	   +getWherePart('a.branchtype2','BranchType2')
    	   + getWherePart( 'a.riskcode','RiskCode')
    	   + getWherePart( 'a.PayIntv','PayintvCode')
    	   + getWherePart( 'a.ManageCom','ManageCom','like')
    	   + getWherePart('a.code4','payyears','','1')
    	   + getWherePart( 'a.code1','StartDay','','1')
    	   + getWherePart( 'a.code2','EndDay','','1')
    	   + getWherePart( 'a.rate','Rate','','1')	
    	   + " Order by a.managecom ,a.riskcode,a.code1,a.code2 ";  
  turnPage.queryModal(sql, RiskQuarterAssessRate1Grid);
  if(RiskQuarterAssessRate1Grid.mulLineCount == 0)
  {
    alert("没有符合条件的绩效提奖比例信息");
  }
  
}

function checkValue()
{
	var countNum=0;
	var selFlag=true;
	if(RiskQuarterAssessRate1Grid.mulLineCount==0)
	{
		alert('没有选择要操作的记录！');
		return false;
	}
	for(var i=0;i<RiskQuarterAssessRate1Grid.mulLineCount;i++)
	{　
		if(RiskQuarterAssessRate1Grid.getChkNo(i))
		{
			countNum++;
			if(RiskQuarterAssessRate1Grid.getRowColData(i,4).trim()==''){
				alert('险种编码不能为空');
				return false;
			}
			if(RiskQuarterAssessRate1Grid.getRowColData(i,5).trim()==''){
				alert('险种名称不能为空');
				return false;
			}			
			if(RiskQuarterAssessRate1Grid.getRowColData(i,7).trim()==''){
				alert('管理机构不能为空');
				return false;
			}
			if(RiskQuarterAssessRate1Grid.getRowColData(i,8).trim()==''){
				alert('保费类型不能为空');
				return false;
			}
			if(RiskQuarterAssessRate1Grid.getRowColData(i,9).trim()==''){
				alert('缴费方式不能为空');
				return false;
			}
			if(RiskQuarterAssessRate1Grid.getRowColData(i,10).trim()==''){
				alert('保险期间不能为空');
				return false;
			}
			if(RiskQuarterAssessRate1Grid.getRowColData(i,12).trim()==''){
				alert('最低缴费年期不能为空');
				return false;
			}
			if(RiskQuarterAssessRate1Grid.getRowColData(i,13).trim()==''){
				alert('缴费年期小于不能为空');
				return false;
			}
			if((RiskQuarterAssessRate1Grid.getRowColData(i,12)-RiskQuarterAssessRate1Grid.getRowColData(i,13))>0){
			    alert('最低缴费年期不能大于缴费年期小于');
			    return;
			}
			if(RiskQuarterAssessRate1Grid.getRowColData(i,14).trim()==''){
				alert('间接绩效提奖比例不能为空');
				return false;
			} else if(RiskQuarterAssessRate1Grid.getRowColData(i,14)<'0'||RiskQuarterAssessRate1Grid.getRowColData(i,14)>'1')
			  {
			    alert("佣金比率不在0到1之间!");
			    SetGrid.setFocus(i,1,SetGrid);
			    selFlag = false;
			    break;
			  }
			if((RiskQuarterAssessRate1Grid.getRowColData(i,1) == null)||(RiskQuarterAssessRate1Grid.getRowColData(i,1)==""))
            {
              if((fm.all("fmtransact").value =="UPDATE") )
              {
                alert("必须先查询出记录，再修改！");
                RiskQuarterAssessRate1Grid.checkBoxAllNot();
                RiskQuarterAssessRate1Grid.setFocus(i,2,RiskQuarterAssessRate1Grid);
                selFlag = false;
                break;
              }
            }
            else if ((RiskQuarterAssessRate1Grid.getRowColData(i,1) != null)||(RiskQuarterAssessRate1Grid.getRowColData(i,1)!=""))
            {	// 如果idx不为空不能插入
              if(fm.all("fmtransact").value =="INSERT")
              {
                alert("此纪录已存在，不可插入！");
                RiskQuarterAssessRate1Grid.checkBoxAllNot();
                RiskQuarterAssessRate1Grid.setFocus(i,2,RiskQuarterAssessRate1Grid);
                selFlag = false;
                break;
              }
            }
		}
	}
	if(!selFlag)
	{
   	  return selFlag;
   	}
	if(countNum==0)
	{
		alert("请选择进行操作的记录。");
		return false;
	}
	if(fm.all("fmtransact").value=="UPDATE")
	{
		for(var i=0;i<RiskQuarterAssessRate1Grid.mulLineCount;i++)
		{
			if(RiskQuarterAssessRate1Grid.getRowColData(0,1).trim()=='')
			{
				alert('列表中存在系统没有的记录，不能修改');
				return false;
			}
		}
	} 
	return true 
}
function save()
{
	fm.all("fmtransact").value="INSERT";
	submitForm();
}
function update()
{
	fm.all("fmtransact").value="UPDATE";
	submitForm();
}

// 删除功能 --
function delete1()
{ 
	fm.all("fmtransact").value="DELETE";
	if (confirm("您确实想删除该条记录?"))
  {
	submitForm();
 }else
  {
    alert("您取消了修改操作！");
  }
}
// 导入功能涉及函数
function diskImport()
{ 
// alert("111111111111111");
// alert("====="+fm2.all("diskimporttype").value);
   if(fm2.FileName.value==null||fm2.FileName.value==""){
   	 alert("请选择要导入的文件！");
   	 return false;
   }
   var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fm2.submit();
}
function moduleDownload(){
// var tSQL="";
// fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "LADiscountDownload.jsp";
    fm.submit();
    fm.action = oldAction;
}
function queryImportResult(){
	var tbatchno=fm2.FileImportNo.value;
	var tqueryType=fm2.queryType.value;
	if(tbatchno==null||tbatchno==""){
		alert("请输入查询文件批次号！");
		return false;
	}
	if(tqueryType==null||tqueryType==""){
		alert("请选择查询类型！");
		return false;
	}
	strSQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog"
	+ " where batchno='"+tbatchno+"' ";
	if(tqueryType==1){
		strSQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		strSQL+=" and errorstate='0'";
	}
	strSQL+=" order by contid";
  // 执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportResultGrid);
	}
function clearImport(){
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
	fm2.all('FileImportNo').value='';
	fm2.all('queryType').value='';
	fm2.all('queryName').value='';
	try
	{
		initImportResultGrid();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}

