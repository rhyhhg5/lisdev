<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LARewardPunishQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAMAddSubPerQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAMAddSubPerQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <title>奖惩查询</title>
</head>  
<body  onload="initForm();initElementtype();" >
  <form action="./LAAMAddSubPerQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLARewardPunish1);">
    
    <td class=titleImg>
      加扣款信息查询条件
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLARewardPunish1" style= "display: ''"> 
    <table  class= common>
     
      <tr  class= common>      
       <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
        </TD> 
   	<td  class= title> 
   	代理机构 
   	</td>
    <td  class= input> <input class=codeno name=AgentCom 	 
         ondblclick="return getAgentCom(this,AgentComName);"
         onkeyup="return getAgentCom(this,AgentComName);" 
         ><input class="codename" name = "AgentComName"   > 
    </td>
      </tr>
     
    
      <tr  class= common> 
       <td class=title>
	加款类型
	</td>
        <td class=input><input name=DoneFlag class=codeno name="DoneFlag"verify="加款类型|NOTNULL"
            CodeData="0|^0|加款|^1|扣款"
            ondblClick="showCodeListEx('DoneFlagList',[this,DoneFlagName],[0,1]);"
            onkeyup="showCodeListKeyEx('DoneFlagList',[this,DoneFlagName],[0,1]);"
        ><Input name=DoneFlagName class="codename" elementtype=nacessary>
        </td>

         <TD  class= title width="25%"> 
		     调整年月
		    </td>
         <td class= input>
         <Input class= 'common'   name=WageNo verify="调整年月|len=6" >
         <font color="red">(yyyymm)
         </td>    

    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRewardPunishGrid);">
    		</td>
    		<td class= titleImg>
    			 加扣款信息结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divRewardPunishGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="SpanRewardPunishGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
