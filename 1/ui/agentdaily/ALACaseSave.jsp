<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ALACaseSave.jsp
//程序功能：
//创建日期：2003-7-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  String tCaseNo="";
  LACaseInfoSchema tLACaseInfoSchema   = new LACaseInfoSchema();

  LACaseInfoUI tLACaseInfoUI   = new LACaseInfoUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
   String tState=request.getParameter("State");

    tLACaseInfoSchema.setAppealNo(request.getParameter("AppealNo"));
    tLACaseInfoSchema.setCaseNo(request.getParameter("CaseNo"));
    //存储真实的AgentGroup
    tLACaseInfoSchema.setCaseDate(request.getParameter("CaseDate"));
    tLACaseInfoSchema.setAgentPutCase(request.getParameter("AgentPutCase"));
    tLACaseInfoSchema.setCaseHappenDate(request.getParameter("CaseHappenDate"));
    tLACaseInfoSchema.setState(request.getParameter("State"));
    
    tLACaseInfoSchema.setBranchType(request.getParameter("BranchType"));
     tLACaseInfoSchema.setBranchType2(request.getParameter("BranchType2"));
    if(tState.equals("1"))
    {
    tLACaseInfoSchema.setEndCaseUnit(request.getParameter("EndCaseManUnit"));
    tLACaseInfoSchema.setAgentEndCase(request.getParameter("AgentEndCase"));
    tLACaseInfoSchema.setAgentEndTel(request.getParameter("AgentEndTel"));
    tLACaseInfoSchema.setDealMind(request.getParameter("DealMind"));
    }
    tLACaseInfoSchema.setDirectLoss(request.getParameter("DirectLoss"));
    tLACaseInfoSchema.setReplevyLoss(request.getParameter("ReplevyLoss"));
    
   // tLACaseInfoSchema.setDeductCent(request.getParameter("DeductCent"));
    tLACaseInfoSchema.setOperator(tG.Operator);

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLACaseInfoSchema);
	tVData.add(tG);
  try
  {
    tLACaseInfoUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLACaseInfoUI.mErrors;
    if (!tError.needDealError())
    {    
        tCaseNo= tLACaseInfoUI.getCaseNo();                    
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('CaseNo').value = "<%=tCaseNo%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

