<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWageGatherSave.jsp
//程序功能：
//创建日期：2005-6-3 10:04
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
	//输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String tOperate = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	//接受参数
	String tManageCom = request.getParameter("ManageCom");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	String tIndexNo = request.getParameter("YearMounth");
	
	//准备参数
	LAWageSchema tLAWageSchema = new LAWageSchema();
	tLAWageSchema.setManageCom(tManageCom);
	tLAWageSchema.setBranchType(tBranchType);
	tLAWageSchema.setBranchType2(tBranchType2);
	tLAWageSchema.setIndexCalNo(tIndexNo);
	
	// 准备传输数据 VData
	VData tVData = new VData();
    FlagStr="";
	tVData.addElement(tLAWageSchema);
	tVData.add(tG);
	
  LAGrpWageGatherUI  tLAGrpWageGatherUI = new LAGrpWageGatherUI();
	//进行后台处理
  try
  {
    tLAGrpWageGatherUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "回退失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  //返回处理结果
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAGrpWageGatherUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    
    	Content = " 回退失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    	System.out.println(Content);
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
