<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String ManageCom=tG.ManageCom;
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAAddPerInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
  <%@include file="LAAddPerInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<%
  //加扣款类别

  String tTitleAgent = "";
  if ("1".equals(BranchType))
  {
   
    tTitleAgent = "营销员";
  } 
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAddPerSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAddSub);">
    
    <td class=titleImg>
      营销员加扣款信息
    </td> 
    </td>
    </tr>
    </table>
    <Div  id= "divAddSub" style= "display: ''">  
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		      营销员代码 
		    </td>
        <td  class= input> 
		      <input class= common name=AgentCode MaxLength=10 OnChange="return checkvalid();" verify="业务员编码|NOTNULL" elementtype=nacessary> 
		    </td>
		    <td  class= title>
		      营销员姓名
		    </td>
        <td  class= input>
		      <input name=Name class='readonly' readonly >
		    </td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		      销售单位代码 
		    </td>
        <td  class= input> 
		      <input class="readonly" readonly name=AgentGroup > 
		    </td>
        <td  class= title> 
		      管理机构 
		    </td>
        <td  class= input> 
		      <input class="readonly" readonly name=ManageCom > 
		    </td>
      </tr>
      <tr  class= common> 
        <td class=title>
	        调整类型
	      </td>
	      <td class=input>
		      <input name=AClass class=codeno  verify="调整类型|NOTNULL"
           ondblclick="return showCodeList('approject',[this,AClassName],[0,1]);" 
           onkeyup="return showCodeListKey('approject',[this,AClassName],[0,1]);"
          ><Input class=codename name=AClassName readOnly elementtype=nacessary > 
        </td>	
	      <td class=title>
	        调整项目
	      </td>
	      <td  class= input>
          <input class=codeno name=DoneFlag verify="调整项目|NOTNULL"
          ondblclick="return showCodeList('reputype',[this,DoneFlagName],[0,1]);" 
          onkeyup="return showCodeListKey('reputype',[this,DoneFlagName],[0,1]);"
          ><input class=codename readonly  name=DoneFlagName elementtype=nacessary> 
        </td>    
       </tr>
       <tr  class= common> 
         <td  class= title> 
		      调整原因
		     </td>
         <td  class= input> 
		     <input name=PunishRsn class= common   > 
		     </td>
         <td  class= title> 
		      调整金额(元)
		     </td>
         <td  class= input> 
		       <input name=Money class= common verify="金额|NOTNULL&num&value>0" elementtype=nacessary> 
		       <font color="red">(>=0)
		     </td>
       </tr>
   
   
      <tr  class= common>
        <TD  class= title width="25%"> 
		     调整年月
		    </td>
         <td class= input>
         <Input class= 'common'   name=WageNo verify="调整年月|NOTNULL&len=6" elementtype=nacessary>
         <font color="red">(yyyymm)
         </td>
	      
        <td  class= title> 
		     备注
		    </td>
        <td  class= input> 
	       <input name=Noti class= common > 
	      </td>	
		 </tr>
     <tr class=common>
       <td  class= title>
		   操作员代码
		   </td>
       <td  class= input>
		    <input name=Operator class="readonly" readonly >
		   </td>
       <td  class= title> 
		   最近操作日
		   </td>
       <td  class= input> 
		    <input class="readonly" readonly name=ModifyDate > 
		   </td>
	   </tr>    
   </table>
  
     <!--p> <font color='red'>注：加款项目和扣款项目至少录入一项！加款金额和扣款金额都为正数！</font></p-->
   
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=Idx value=''>
    <input type=hidden name=Flag value=''>
    <input type=hidden name=Flag1 value=''>
    <input type=hidden name=AwardTitle value=''>
    <input type=hidden name=BranchType value=''>   
    <input type=hidden name=BranchType2 value=''>  
    <input type=hidden name=SendGrp value=''> 
    <input type=hidden name=LogManagecom value='<%=ManageCom%>'>
    <input type=hidden class=Common name=querySql > 
    <!--input type=hidden name=SendGrp1 value=''-->  
  </form>
   <form action="./DiskImportAddPerSave.jsp" method=post name=fm2 enctype="multipart/form-data" target="fraSubmit">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportFile);">
    		</td>
    		<td class= titleImg>加扣款信息磁盘导入</td>
    		
    	</tr>
  </table>
  <div id="divImportFile" style="display:''">   

  <table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">文件名</TD>
				<TD class=input>
					<Input type="file" class= "common" style="width:300px" name="FileName" />
				</TD><td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()"></td>
			</TR>
  </table>
				  <Input type=button class=cssButton name="goDiskImport" value="导  入" id="goDiskImport"  onclick="diskImport()">
				  <!--Input type=button class=cssButton value="重  置" onclick="clearImport()"-->


<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportQuery);">
    		</td>
    		<td class= titleImg>加扣款信息磁盘导入日志查询</td>
    		
    	</tr>
  </table>
  <div id="divImportQuery" style="display:''">
  	<table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">导入文件批次</TD>
				<TD class=input>
					<Input type="text" class= "common"  name="FileImportNo"/>
				</TD>
				
				<TD  class=title style="width:100px">
				查询类型
				</td>
				<td class=input>
				<Input class="codeno" name=queryType  CodeData="0|^0|全部|^1|导入错误行|^2|导入成功行" ondblclick="return showCodeListEx('querytypelist',[this,queryName],[0,1]);" onkeyup="return showCodeListKeyEx('querytypelist',[this,queryName],[0,1]);" onchange=""><input class=codename name=queryName  readonly=true >
				</TD>
				<td></td>
			</TR>
   </table>
  <Input type=button class=cssButton value="查  询" onclick="queryImportResult()"/>
  <div id="divImportResultGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanImportResultGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage2.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage2.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage2.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage2.lastPage();">
  </div>
  </div>  

		
 </div>
  
  
  <Input type=hidden name=branchtype2> 
  <Input type=hidden  name=BranchType >
  <Input type=hidden name=diskimporttype>
 </form> 
  
  
  
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
