
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/Calendar/Calendar.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LACrossQueryInput.js"></SCRIPT> 
  <%@include file="LACrossQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form action="./LACrossInputSave.jsp" method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLACrossGrid1" style= "display: ''">    
<table  class= common align='center' >
   
    <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="code" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7" elementtype=nacessary 
          ondblclick="return showCodeList('comcode',[this],null,null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this],null,null,8,'char(length(trim(comcode)))',1);"> 
          </TD> 
    </TR>
    <TR  class= common> 
    <TD  class= title>
      起期
    </TD>
    <TD  class= input>
   <Input class= "coolDatePicker" dateFormat="short" name=ValidStart verify="起始有效期|DATE"  elementtype=nacessary> 
	
    </TD>
     <TD  class= title>
      止期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=ValidEnd verify="截至有效日期|NOTNULL&DATE elementtype=nacessary"> 
    </TD>
  </TR>
</TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=button width="10%" align=left>
        <INPUT VALUE="查 询"   TYPE=button   class=cssbutton onclick="queryClick();"> 
         
       
          <INPUT VALUE="返 回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLACross1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACrossGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
   <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=DoneFlag value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=Operate value=''>
  </form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
