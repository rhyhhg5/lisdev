<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");

    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAGrpAscriptionInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAGrpAscriptionInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./LAGrpAscriptionSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../agent/AgentOp3.jsp"%>
    <table>
       <tr>
         <td class=common>
         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAuthorize1);">
    	 <td class=titleImg>
    	   保单归属管理
    	 </td>
    	 </td>
    	</tr>
    </table>
    <!--险种授权 -->
    <Div id= "divLAAuthorize1" style= "display: ''">
    <table class= common>
      <TR class= common>
        <TD class= title>
          团体合同号
        </TD>
        <TD class= input>
          <Input class=common name=GrpContNo verify="团体合同号|NOTNULL"  onChange="return queryCompact();" elementtype=nacessary >
        </TD>
        <TD class= title>
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=GrpPolNo >
        </TD>
      </TR>
      <TR class= common>
			  <TD class= title>
          所属管理机构
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=ManageCom>
        </TD>
        <TD class= title>
          销售机构
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=BranchAttr >
        </TD>
      </TR>
      <TR class= common>
			  <TD class= title>
          投保客户编号
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=AppntNo>
        </TD>
        <TD class= title>
          投保客户名称
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=AppntName>
        </TD>
      </TR>
      <TR class= common>
			  <TD class= title>
          保费
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=TransMoney>
        </TD>
        <TD class= title>
          险种个数
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=RiskNum>
        </TD>
      </TR>
      <TR class= common>
        <TD class= title>
          缴费日期
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=CurPayToDate>
        </TD>
			  <TD class= title>
          签单日期
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=SignDate>
        </TD>
      </TR>
      <TR class= common>
        <TD class= title>
          操作员
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=Operator>
        </TD>
        <td  class= title> 
		     归属日期
		    </td>
        <td  class= input> 
		      <Input class= "coolDatePicker" dateFormat="short" name=AscripDate verify="归属日期|DATE&notnull"
		        elementtype=nacessary> 
		    </td>
      </TR>
    </table>
     </Div>
     <p> <font color="#ff0000">注：&nbsp;&nbsp;1.发生在归属日期后的业绩将随保单一块归属给新的业务员，发生在归属日期前的业绩将不随保单一块归属给新的业务员，
     <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;另外，归属日期所在月该管理机构应没有算过薪资，请注意正确填写归属日期。  
     <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.因成本中心代码关系，保单归属只能在同一团队人员中调整 
      <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.查询完毕后，请选择归属后的业务员信息  
      <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：目前团险不支持人员跨团队保单归属操作</font></p>
      
    <table> 
       <tr>
         <td class=common>
         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPrincipalGrid);">
    	 <td class=titleImg>
    	   保单归属人
    	 </td>
    	 </td>
    	</tr>
    </table>
    <Div  id= "divPrincipalGrid" style= "display: ''">
    <!--录入的险种 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanPrincipalGrid" >
	 </span>
	</td>
       </tr>
    </Table>
    </DIV>

    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=<%=BranchType%>>
    <input type=hidden name=BranchType2 value=<%=BranchType2%>>
    <input type=hidden name=AgentGroup value=''>
    <input type=hidden name=OldAgentCode value=''>
    <input type=hidden name=conttype value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>