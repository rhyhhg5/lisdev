//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LARewardPunishQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
//  showDiv(operateButton,"false"); 
//  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
//  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
//  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//xqq 2014-12-2
function checkvalid()
{
  var strSQL = "";
  var strAgent = "";
	if(fm.GroupAgentCode.value!=null&&fm.GroupAgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.GroupAgentCode.value+"') ";
	}
  var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
  if (getWherePart('GroupAgentCode')!='')
  {
     strSQL = "select agentcode from LAAgent where 1=1 "
//     + strAgent+"and ((AgentState  <'06' ) or (AgentState is null))"
//	     + " and branchtype='"+tBranchType+"'"
//	      +" and branchtype2='"+tBranchType2+"'"
	      + getWherePart('GroupAgentCode','GroupAgentCode');
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('GroupAgentCode').value = '';
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('GroupAgentCode').value="";
    return;
  }
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
   fm.all('AgentCode').value  = tArr[0][0];
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


function getQueryResult()
{
  var arrSelected = null;
  tRow = QualityAssessGrid.getSelNo();
 
  if( tRow == 0 || tRow == null || arrDataSet == null )
    return arrSelected;
  arrSelected = new Array();
  //alert(tRow);
  var GroupAgentCode=QualityAssessGrid.getRowColData(tRow-1,1);
  var idx=QualityAssessGrid.getRowColData(tRow-1,4)
  var strSQL1 = "select getunitecode(a.AgentCode),c.Name,a.Branchattr,a.ManageCom,a.MarkType,a.Mark,a.MarkContent,a.MarkRsn,(select ItemContext from LAQualityItemDef where ItemCode=trim(a.MarkRsn) ),a.DoneDate,a.Operator,a.Idx,a.AgentGroup,a.Noti from LAQualityAssess a,LAAgent c where c.GroupAgentCode ='"+QualityAssessGrid.getRowColData(tRow-1,1)+"'" 
	      +" and a.AgentCode = c.AgentCode and a.Idx = "+QualityAssessGrid.getRowColData(tRow-1,11);
  //alert(strSQL1);
  var strQueryResult  = easyQueryVer3(strSQL1, 1, 1, 1);  
  arrSelected = decodeEasyQueryResult(strQueryResult);
  return arrSelected;
}


function returnParent()
{
	
  var arrReturn = new Array();
  
  var tSel = QualityAssessGrid.getSelNo();	
 
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
   
    try
    {	
      //alert(tSel);
      arrReturn = getQueryResult();
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
		
  }
}




function easyQueryClick()
{
	 
	
  // 初始化表格
  initQualityAssessGrid();
	//var tReturn = getManageComLimitlike("a.ManageCom");
  // 书写SQL语句
  var strSQL = "";
//  var strAgent = "";
//	if(fm.GroupAgentCode.value!=null&&fm.GroupAgentCode.value!=''){
//		strAgent = " and a.agentcode=getUniteCode('"+fm.GroupAgentCode.value+"') ";
//	}
	strSQL = "select getunitecode(a.AgentCode),b.name,a.branchattr,a.managecom,a.marktype,a.mark,a.MarkContent,a.markrsn,a.DoneDate,a.operator,a.Idx,a.ModifyDate"
	         +" from LAQualityAssess a,LAAgent b where a.agentcode=b.agentcode "
//	        +tReturn
	  + getWherePart('a.AgentCode','AgentCode')
//	   + strAgent
	   + getWherePart('b.Name','Name') 
	   + getWherePart('a.BranchAttr','AgentGroup','like')
	   + getWherePart('a.ManageCom','ManageCom')
	   + getWherePart('a.MarkType','MarkType');
	 if(document.fm.Mark.value != "")
	 {
	 	 strSQL += "and a.Mark = " + document.fm.Mark.value;
	 }
	   //+ getWherePart('a.Mark','Mark')
	   //+ getWherePart('a.MarkContent','MarkContent')
	 strSQL += getWherePart('a.MarkRsn','MarkRsn')
	   + getWherePart('a.DoneDate','DoneDate')
	   + getWherePart('a.Operator','Operator')
	   + getWherePart('a.Idx','Idx')
     + getWherePart('a.BranchType','BranchType');	
     + getWherePart('a.BranchType2','BranchType2');	   	   
//	   + str_new_AgentGroup;
//     +" order by a.agentcode";
     +"  order by a.DoneDate,a.AgentCode"
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,7,8,9]);
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QualityAssessGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES); 

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function ListExecl()
{
  
//定义查询的数据
var strSQL = "";
// var strAgent = "";
//	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
//		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
//	}
strSQL = "select getunitecode(a.AgentCode),b.name,a.branchattr,a.managecom,a.marktype,a.mark,a.MarkContent,a.markrsn,a.DoneDate,a.operator,a.Idx,a.ModifyDate"
	         +" from LAQualityAssess a,LAAgent b where a.agentcode=b.agentcode "
	         + getWherePart('a.AgentCode','AgentCode')
//	         + strAgent
	         + getWherePart('b.Name','Name') 
	         + getWherePart('a.BranchAttr','AgentGroup','like')
	         + getWherePart('a.ManageCom','ManageCom')
	         + getWherePart('a.MarkType','MarkType');
	 if(document.fm.Mark.value != "")
	 {
	 	 strSQL += "and a.Mark = " + document.fm.Mark.value;
	 }
	 strSQL += getWherePart('a.MarkRsn','MarkRsn')
	        + getWherePart('a.DoneDate','DoneDate')
	        + getWherePart('a.Operator','Operator')
	        + getWherePart('a.Idx','Idx')
            + getWherePart('a.BranchType','BranchType');	
            + getWherePart('a.BranchType2','BranchType2');	   	   
            +"  order by a.DoneDate,a.AgentCode"
     
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '业务员编码','营销员姓名','销售单位','管理机构','分数类型','分数','加扣分原因','执行日期','记录顺序号'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '品质管理 查询下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}
