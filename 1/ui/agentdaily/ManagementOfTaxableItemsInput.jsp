<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<!-- 
程序名称：
程序功能：应纳税项管理界面
创建时间：2016-3-28
创建人  ：张心静 -->

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>
<%@page contentType="text/html;charset=GBK" %>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ManagementOfTaxableItemsInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="ManagementOfTaxableItemsInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>
   
<body  onload="initForm();initElementtype();" >
 <form action="./ManagementOfTaxableItemsSave.jsp" method=post name=fm target="fraSubmit">
  <table class="common">
   <tr>      
     	<td class=button width="10%" align=right >
     		<input type=button value="保  存" class=cssButton onclick="return DoInsert();">
     		<input type=button value="修  改" class=cssButton onclick="return DoUpdate();">  
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">  
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    	</td>
    </tr>      
  </table>
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg>营销员应纳税项信息</td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >    
   <tr>	
    <TD  class= title>
            营销员代码
          </TD>
          <TD  class= input>
            <Input class= common  name=GroupAgentCode verify="营销员代码|NOTNULL" elementtype=nacessary onchange="return changeAgent();">
          </TD>
            <TD  class= title>
            营销员姓名
          </TD>
          <TD  class= input>
            <Input class= "readonly"  name=AgentName readonly>
          </TD>
   </tr>
   <tr class=common> 
          <td  class= title> 
          销售单位
          </td>
          <TD  class= input> 
          <Input class= "readonly" name=Branch readonly>
          </TD>
          <td  class= title>
   		管理机构
   		</td>
          <TD  class= input> 
          <Input class= "readonly" name=Managecom readonly>
          </TD>
   </tr>
   <tr>	
    <TD  class= title>
            总公司奖励应纳税项
          </TD>
          <TD  class= input>
            <Input class= "common"  name=CBPTaxes >
          </TD>
            <TD  class= title>
          省公司奖励应纳税项
          </TD>
          <TD  class= input>
            <Input class= "common"  name=PCBPTaxes >
          </TD>
   </tr>
   <tr class=common> 
          <td  class= title> 
          中支公司奖励应纳税项
          </td>
          <TD  class= input> 
          <Input class= "common" name=ITBPTaxes>
          </TD>
          <td  class= title>
   		执行薪资月
   		</td>
          <TD  class= input> 
          <Input class= "common" name=WageNo verify="执行薪资月|notnull&len=6" elementtype=nacessary>
          <span><font color="red">&nbsp;&nbsp;格式如：201606</font></span>
          </TD>  			
   </tr>
   <tr class=common> 
          <td  class= title> 
          操作员代码
          </td>
          <TD  class= input> 
          <Input class= "readonly" name=Operator readonly>
          </TD>
          <td  class= title>
   		最近操作日期
   		</td>
          <TD  class= input> 
          <Input class= "readonly" name=ModifyDate readonly>
          </TD>
   </tr>
  </table>
  <br>
     <span><font color="red">注：修改操作时，只能修改总公司奖励应纳税项、省公司奖励应纳税项、中支公司奖励应纳税项这三项内容！</font></span>
</div>
  <br><hr>
  <input type=hidden name=hideOperate value=''>
  <input type=hidden name=AgentCode value=''>
  <input type=hidden name=BranchType value=''>
  <input type=hidden name=BranchType2 value=''>
  <input type=hidden class=Common name=querySql > 
  </form>
<form action="" enctype="multipart/form-data" method=post name="fmImport" target="fraSubmit"> 
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportFile);">
    		</td>
    		<td class= titleImg>应纳税项磁盘导入</td>
    		
    	</tr>
  </table>
  <div id="divImportFile" style="display:''">   

  <table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">文件名</TD>
				<TD class=input>
					<Input type="file" class= "common" style="width:300px" name="FileName" id="FileName"/>
				</TD><td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()"></td>
			</TR>
  </table>
				 <input class="cssButton" type="button"  name="btnImport2" value="导  入" onclick="importList2();" />
				  <Input type=button class=cssButton value="重  置" onclick="clearImport()">

<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportQuery);">
    		</td>
    		<td class= titleImg>应纳税项磁盘导入日志查询</td>
    		
    	</tr>
  </table>
  <div id="divImportQuery" style="display:''">
  	<table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">导入文件批次</TD>
				<TD class=input>
					<Input type="text" class= "common"  name="FileImportNo"/>
				</TD>
				
				<TD  class=title style="width:100px">
				查询类型
				</td>
				<td class=input>
				<Input class="codeno" name=queryType  CodeData="0|^0|全部|^1|导入错误行|^2|导入成功行" ondblclick="return showCodeListEx('querytypelist',[this,queryName],[0,1]);" onkeyup="return showCodeListKeyEx('querytypelist',[this,queryName],[0,1]);" onchange=""><input class=codename name=queryName  readonly=true >
				</TD>
				<td></td>
			</TR>
   </table>
  <Input type=button class=cssButton value="查  询" onclick="queryImportResult()"/>
  <div id="divImportResultGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanImportResultGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage2.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage2.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage2.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage2.lastPage();">
  </div>
  </div>  
 </div>  
  <Input type=hidden name=branchtype2> 
  <Input type=hidden  name=BranchType >
  <Input type=hidden name=diskimporttype>
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




