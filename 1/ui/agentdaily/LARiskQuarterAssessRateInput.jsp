<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：RiskQuarterAssessRateInput.jsp
//程序功能：
//创建日期：2009-02-19
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LARiskQuarterAssessRateInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LARiskQuarterAssessRateInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="./LARiskQuarterAssessRateSave.jsp" method=post name=fm
	target="fraSubmit">
<table>
	<tr>
		<td class=titleImg>管理机构信息</td>
	</tr>
</table>

<table class=common>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name=ManageCom
			ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,tmanagecom,'1',1);"
			verify="管理机构|notnull&code:comcode"><Input class=codename
			name=ManageComName readOnly elementtype=nacessary></TD>
     <td class=title>
     险种
     </td>
     <td class= input>
     	<Input class=codeno name=RiskCode verify="险种|code:RiskCode"
       ondblclick="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,1,1);"
       onkeyup="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,1,1);"
       ><input class=codename name=RiskCodename readonly=true  ></td>
   </tr>
   <tr class=common> 
     <TD  class= title>
                      保险期间
     </TD>
       <TD  class= input>
         <Input class= "codeno"  name=payyears 
         ondblclick="return showCodeList('payyear',[this,payyearsName],[0,1]);"
         onkeyup="return showCodeList('payyear',[this,payyearsName],[0,1]);"
         ><input class="codename" name=payyearsName readOnly=true>
       </TD>   
        <td  class= title>
   		佣金比率
   		</td>
          <TD  class= input> 
          <Input class= "common" name=Rate>
          </TD>  
   </tr>
   <TR  class= common> 
      	    <TD  class= title>
           缴费方式
          </TD>
          <TD  class= input>
             <Input class="codeno" name=PayintvCode  CodeData="0|^0|趸缴|^12|年缴|^6|半年缴|^3|季缴|^1|月缴" 
             ondblclick="return showCodeListEx('payintvlist',[this,PayintvName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('payintvlist',[this,PayintvName],[0,1]);" onchange=""
             ><input class=codename name=PayintvName  readonly=true ></TD>       
		<Input class="readonly" type=hidden name=diskimporttype>
		 <TD  class= title>
                         最低缴费年期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=StartDay >
          </TD>
   </TR>
      <tr>	

            <TD  class= title>
            缴费年期小于
          </TD>
          <TD  class= input>
            <Input class= "common"  name=EndDay >
          </TD>
   </tr>
</table>
<input type=button class=cssButton value="查 询"
	onclick="easyQueryClick();"> <input type=button class=cssButton
	value="保 存" onclick="save();"> <input type=button
	class=cssButton value="修 改" onclick="update();"> <input
	type=button class=cssButton value="删 除" onclick="delete1();"> <input
	type=hidden id="fmtransact" name="fmtransact"> <input
	type=hidden id="fmAction" name="fmAction"> <input type=hidden
	name=BranchType value=<%=BranchType%>> <input type=hidden
	name=BranchType2 value=<%=BranchType2%>>
<p><font color='red'>注：提奖比率必须为0-1的数，如23%，记为0.23!</font></p>
<p><font color='red'>注：修改功能,只能修改间接绩效提奖比例(*)</font></p>
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;"
			OnClick="showPage(this,divSpanOperatorIndexMarkGrid);"></td>
		<td class=titleImg>主管职级间接绩效提奖比例录入</td>
	</tr>
</table>
<Div id="divRiskQuarterAssessRateGrid" align=center style="display: '' ">
<table class=common>
	<tr class=common>
		<td text-align:left colSpan=1><span
			id="spanRiskQuarterAssessRateGrid"> </span></td>
	</tr>
</table>
</div>
<Div id="divPage" align=center style="display: 'none' "><INPUT
	VALUE="首  页" class=cssbutton TYPE=button
	onclick="turnPage.firstPage();"> <INPUT VALUE="上一页"
	class=cssbutton TYPE=button onclick="turnPage.previousPage();">
<INPUT VALUE="下一页" class=cssbutton TYPE=button
	onclick="turnPage.nextPage();"> <INPUT VALUE="尾  页"
	class=cssbutton TYPE=button onclick="turnPage.lastPage();"></div>

</form>
<hr>
<form action="./LARiskImportQuarterSave.jsp" method=post name=fm2 enctype="multipart/form-data" target="fraSubmit">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportFile);">
    		</td>
    		<td class= titleImg>佣金率磁盘导入</td>
    		
    	</tr>
  </table>
  <div id="divImportFile" style="display:''">   

  <table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">文件名</TD>
				<TD class=input>
					<Input type="file" class= "common" style="width:300px" name="FileName" id="FileName"/>
				</TD><td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()"></td>
			</TR>
  </table>
				  <Input type=button class=cssButton name="goDiskImport" value="导  入" id="goDiskImport"  onclick="diskImport()">
				  <Input type=button class=cssButton value="重  置" onclick="clearImport()">

<!--
 <Input type=button class=cssButton name="goDiskImport" value="磁盘导入" id="goDiskImport"  onclick="diskImport()">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()">
-->
<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportQuery);">
    		</td>
    		<td class= titleImg>佣金率磁盘导入日志查询</td>
    		
    	</tr>
  </table>
  <div id="divImportQuery" style="display:''">
  	<table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">导入文件批次</TD>
				<TD class=input>
					<Input type="text" class= "common"  name="FileImportNo"/>
				</TD>
				
				<TD  class=title style="width:100px">
				查询类型
				</td>
				<td class=input>
				<Input class="codeno" name=queryType  CodeData="0|^0|全部|^1|导入错误行|^2|导入成功行" ondblclick="return showCodeListEx('querytypelist',[this,queryName],[0,1]);" onkeyup="return showCodeListKeyEx('querytypelist',[this,queryName],[0,1]);" onchange=""><input class=codename name=queryName  readonly=true >
				</TD>
				<td></td>
			</TR>
   </table>
  <Input type=button class=cssButton value="查  询" onclick="queryImportResult()"/>
  <div id="divImportResultGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanImportResultGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage2.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage2.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage2.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage2.lastPage();">
  </div>
  </div>  
 </div>
  <Input type=hidden name=branchtype2> 
  <Input type=hidden  name=BranchType >
  <Input type=hidden name=diskimporttype>
 </form> 
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
