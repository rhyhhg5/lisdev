<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LABranchGroupInput.jsp
	//程序功能：
	//创建日期：
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>

<%@page contentType="text/html;charset=GBK"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./ManagementOfTaxableItemsQueryInput.js"></SCRIPT >
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./ManagementOfTaxableItemsInputInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="./*" method=post name=fm target="fraSubmit">
<table>
	<tr class=common>
		<td class=titleImg>应纳税项信息查询条件</td>
	</tr>
</table>
<Div id="divLABranchGroup1" style="display: ''">
<table  class= common>
        <TR  class= common>
          <TD class=title>管理机构</TD>
		<TD class=input><Input class='codeno' name=ManageCom
			verify="管理机构|notnull"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"
			><Input class=codename name=ManageComName  elementtype=nacessary>
			</TD>
			<TD class=title>执行薪资月</TD>
			<TD class=input><Input class=common name=WageNo verify="执行薪资月|notnull&len=6"  elementtype=nacessary>
			 <span><font color="red">&nbsp;&nbsp;格式如：201606</font></span>
			</TD>
		<tr>	
    <TD  class= title>
            营销员代码
          </TD>
          <TD  class= input>
            <Input class= common  name=GroupAgentCode>
          </TD>
           <td  class= title> 
          销售单位
          </td>
          <TD  class= input> 
          <Input class= common name=Branchattr >
          </TD>
   </tr>
 </table>
<span id="operateButton">
	<table class="common" align=center>
		<tr>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查  询"  name=Query TYPE=button onclick="return rqueryClick();">
				<INPUT class=cssButton VALUE="返  回"  name=Query TYPE=button onclick="return returnClick();">
				<INPUT VALUE="下  载"  class=cssbutton  TYPE=button   onclick="ListExecl();">
			</td>
		</tr>
	</table>
</span>
<table>
	<tr class=common>
		<td class=titleImg>加扣款信息结果</td>
	</tr>
</table>
<Div  id= "divAGroupGrid" style= "display: 'true'">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBranchGroupGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</Div>
</Div>
<input type=hidden name=hideOperate value=''> 
<input type=hidden name=AgentGroup value=''> <!--后台操作的隐式机构编码，不随机构的调整而改变 --> 
<input type=hidden class=Common name=querySql > 
<input type=hidden class=Common name=querySqlTitle > 
<input type=hidden class=Common name=Title >
<span  id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
