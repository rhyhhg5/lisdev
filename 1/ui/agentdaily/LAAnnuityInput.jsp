<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAAnnuityInput.jsp
//程序功能：
//创建日期：2005-6-30 10:09
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<% 
  String agentcode=request.getParameter("AgentCode");
  if(agentcode==null || agentcode.equals(""))
  {
     agentcode="";
  }
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAAnnuityInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAAnnuityInit.jsp"%>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "销售员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务人员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAnnuitySave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="./AnnuityButton.jsp"%>

    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
          养老金基本信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLAAccounts1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
     <%=tTitleAgent%>编码
    </TD>
    <TD  class= input>
      <Input class= 'common' dateFormat="short" name=AgentCode  verify="营销员代码|NotNull"
        elementtype=nacessary onchange="return queryPersonnel();">
    </TD>
    <TD  class= title>
     <%=tTitleAgent%>姓名
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=AgentName>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
     销售单位
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=AgentGroup >
    </TD>
    <TD  class= title>
     管理机构
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=ManageCode>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      离司日期
    </TD>
    <TD  class= input>
      <Input name=OutWorkDate class="coolDatePicker" dateFormat="short" 
             verify="离司时间|NotNull&DATE" elementtype=nacessary>
    </TD>
    <TD  class= title>
      实付日期
    </TD>
    <TD  class= input> 
      <Input name=PayDate class="coolDatePicker" dateFormat="short" > 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      领取标志
    </TD>
    <TD  class= input>
      <Input class=codeno name=DrawMarker verify="领取标记|NOTNULL" 
         CodeData="0|^0|未领取|^1|已领取"  
         ondblClick="showCodeListEx('StateList',[this,DrawMarkerName],[0,1]);"
         onkeyup="showCodeListKeyEx('StateList',[this,DrawMarkerName],[0,1]);"
         ><Input class=codename name=DrawMarkerName readOnly elementtype=nacessary> 
    </TD>
    <TD class= title>
      养老金合计
    </TD>
    <TD class= input>
      <Input class= 'readonly' readonly name=SumAnnuity >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      应付养老金金额
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DealAnnuity >
    </TD>
    <TD  class= title>
      领取养老金金额
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrawAnnuity >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      当月养老金提取基数
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=BaseAnnuity>
    </TD>
    <TD  class= title>
      养老金提取比例
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=ScaleAnnuity>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      公司养老金累计
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=CoAnnuity>
    </TD>
    <TD  class= title>
      个人养老金累计
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=SingleAnnuity>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      操作人员
    </TD>
    <TD  class= input>
      <Input class='readonly' readonly name=Operator >
    </TD>
  </TR>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="SeriesNo" name="SeriesNo">
    <input type=hidden id="BranchType" name="BranchType" value=<%=BranchType%>>
    <input type=hidden id="BranchType2" name="BranchType2" value=<%=BranchType2%>>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
