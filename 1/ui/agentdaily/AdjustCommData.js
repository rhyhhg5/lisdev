 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (beforeSubmit() == false)
    return false;
    
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  showSubmitFrame(mDebug);
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作  
  }
  
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if (verifyInput() == false)
    return false;    
  
  var strSQL = "";
  var strQueryResult;
  //校验代理人  
  strSQL = "select AgentCode From LAAgent Where 1=1"
          +getWherePart('ManageCom')
          +getWherePart('AgentCode')
          +getWherePart('BranchType');
  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("不存在所录入代理人！");
    return false;
  }       
  //校验新旧机构 （移到BL层了）
//  strSQL = "select (select AgentGroup From LABranchGroup Where BranchType = '1' "
//                  +"And trim(BranchAttr) = '"+fm.OriginBranchAttr.value+"') oldBranchCode, "
//                 +"(select AgentGroup From LABranchGroup Where BranchType = '1' "
//                  +"And trim(BranchAttr) = '"+fm.NewBranchAttr.value+"') newBranchCode "
//          +"From LDsysVar Where sysvar = 'onerow'";	
//  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
//  //alert(strSQL);
//  if (!strQueryResult) {
//    alert("查询失败！");
//    return false;
//    }
////查询成功则拆分字符串，返回二维数组
//  var arr = decodeEasyQueryResult(strQueryResult);
//  fm.OriginBranchCode.value = arr[0][0];
//  fm.NewBranchCode.value = arr[0][1];
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function AdjustAgentclick()
{
	
	fm.Operation.value='AdjustAgentclick';
	
	submitForm();
}

function AdjustCommclick()
{      
	
	
	
	fm.Operation.value='AdjustCommclick';	
	if (fm.AgentCode1.value==null||fm.AgentCode1.value=='')
	{		
		alert('请录入需要调整业绩的代理人编码');
		return false;
	}
	if (fm.IndexCalNo.value==null||fm.IndexCalNo.value=='')
	{
		alert('请录入业绩调整的起始年月');
		return false;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


