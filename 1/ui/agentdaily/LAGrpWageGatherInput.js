//程序名称：LAGrpWageGatherInput.js
//程序功能：查询没有主管的单位的函数
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

/*********************************************************************
 *  点击查询按钮响应事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	easyQuery();
}

/*********************************************************************
 *  描述  ：  执行回退操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
	//判断画面输入内容是否合法
  if(verifyInput2() == false) return false;
	
	if(checkInputValue() == false) return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
  fm.submit();
}

/*********************************************************************
 *  验证输入是否合法
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkInputValue()
{
	var tInputValue
	tInputValue = trim(document.fm.YearMounth.value);
	
	if(!isInteger(tInputValue))
	{
		alert("[回退年月]录入必须全部是数字！");
		return false;
	}
	if(!isDate(tInputValue.substring(0,4)+"-"+tInputValue.substring(4,6)+"-10"))
	{
		alert("[回退年月]请录入一个合法年月！");
		return false;
	}
	 var  tBranchType=fm.all("BranchType").value;
	 var  tBranchType2=fm.all("BranchType2").value;
     var  tYearMounth=fm.all("YearMounth").value;
     var strSql = " select ManageCom from LAWageHistory " 
                  +" where wageno = '"+tYearMounth+"'" 
                  +" and state='13' "            
                  +" and ManageCom like '"+fm.all("ManageCom").value+"%' "
                  +getWherePart('BranchType')
                  +getWherePart('BranchType2');

      //alert(strSql);	     
	  //查询SQL，返回结果字符串
     var strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

      //判断是否查询成功
     if(tBranchType== '2' && tBranchType2== '01' ){
	     if(!strQueryResult){
	         alert("该月薪资计算状态不正确,不能回退.");
	         return false;
	     }	
	 }
	return true;
}

/*********************************************************************
 *  回退操作结束后的处理
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	 	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");	
	}
}