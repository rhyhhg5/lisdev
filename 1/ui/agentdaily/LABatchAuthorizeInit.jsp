<%
//程序名称：LABatchAuthorizeInit.jsp
//程序功能：
//创建日期：2005-10-24 14:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     String currdate = PubFun.getCurrentDate();
%>                            
<script language="JavaScript">
var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and riskprop = #I#)";
</script>

<script language="JavaScript">
/**************************************
 * 初始化人员列表
 **************************************/
function initAgentGrid()
{
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]= "销售员代码";          		        //列名
    iArray[1][1]="25px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[2]=new Array();
    iArray[2][0]=  "销售员姓名";          		        //列名
    iArray[2][1]="30px";      	      		//列宽
    iArray[2][2]=30;            			//列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    
    iArray[3]=new Array();
    iArray[3][0]="管理机构";          		        //列名
    iArray[3][1]="20px";      	      		//列宽
    iArray[3][2]=30;            			//列最大值
    iArray[3][3]=0;             //是否允许输入,    
        
    iArray[4]=new Array();
    iArray[4][0]="销售单位";          		        //列名
    iArray[4][1]="35px";      	      		//列宽
    iArray[4][2]=10;            			//列最大值
    iArray[4][3]=0;             //是否允许输入,
    
    iArray[5]=new Array();
    iArray[5][0]="销售单位名称";          		        //列名
    iArray[5][1]="50px";      	      		//列宽
    iArray[5][2]=30;            			//列最大值
    iArray[5][3]=0;             //是否允许输入,
    
    iArray[6]=new Array();
    iArray[6][0]="入司时间";          		        //列名
    iArray[6][1]="25px";      	      		//列宽
    iArray[6][2]=30;            			//列最大值
    iArray[6][3]=0;             //是否允许输入,
 
     iArray[7]=new Array();
    iArray[7][0]="职级";          		        //列名
    iArray[7][1]="15px";      	      		//列宽
    iArray[7][2]=30;            			//列最大值
    iArray[7][3]=0;             //是否允许输入,
 
    AgentGrid = new MulLineEnter( "fm" , "AgentGrid" );

    AgentGrid.mulLineCount = 0;   
    AgentGrid.displayTitle = 1;
    AgentGrid.hiddenPlus = 1;
    AgentGrid.hiddenSubtraction = 1;
    AgentGrid.canSel = 0;
    AgentGrid.canChk = 1;

    AgentGrid.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}

/**************************************
 * 初始化险种选择列表
 **************************************/
function initAuthorizeGrid()
{                               
  var iArray = new Array();      
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="40px"; 	           		//列宽
	  iArray[0][2]=1;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="险种编码";          		//列名
	  iArray[1][1]="80px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
	  iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	  iArray[1][6]="0|1";
	  iArray[1][9]="险种编码|code:RiskCode";  
	  iArray[1][15]= "1";              	        //校验输入是否正确
	  iArray[1][16]= StrSql;  
	
	  iArray[2]=new Array();
	  iArray[2][0]="险种名称";         			//列名
	  iArray[2][1]="160px";            			//列宽
	  iArray[2][2]=20;            			//列最大值
	  iArray[2][3]=0; 
	           	        //校验输入是否正确
	      			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="取消销售资格起期";      	   		//列名
	  iArray[3][1]="100px";            			//列宽
	  iArray[3][2]=10;            			//列最大值
	  iArray[3][3]=1; 
	  iArray[3][4]="Date";     //引用代码:
	  iArray[3][9]="取消销售资格起期|Date"; 
	         			//是否允许输入,1表示允许，0表示不允许
	  
	  
	  iArray[4]=new Array();
	  iArray[4][0]="取消销售资格止期";      	   		//列名
	  iArray[4][1]="100px";            			//列宽
	  iArray[4][2]=10;            			//列最大值
	  iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  iArray[3][4]="Date";     //引用代码:
	  iArray[4][9]="取消销售资格止期|Date"; 
	  AuthorizeGrid = new MulLineEnter( "fm" , "AuthorizeGrid" ); 
	  //这些属性必须在loadMulLine前
	  AuthorizeGrid.mulLineCount = 0;   
	  AuthorizeGrid.displayTitle = 1;
	  AuthorizeGrid.hiddenPlus = 0;
	  AuthorizeGrid.hiddenSubtraction = 0;
	  AuthorizeGrid.locked=0;      
	  AuthorizeGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}

/**************************************
 * 初始化文本框
 **************************************/
function initInpBox()
{
  try
  {
    document.fm.AgentCode.value = "";
    document.fm.AgentName.value = "";
    document.fm.ManageCom.value = "";
    document.fm.ManageComName.value = "";
    document.fm.BranchAttr.value = "";
    document.fm.AgentGrade.value = "";
    document.fm.EnterYears.value = "";
    document.fm.hideOperate.value = "";
    document.fm.checkType.value = "FALSE";
    document.fm.listAgentCode.value = "";
    document.fm.BranchType.value = '<%=BranchType%>';
    document.fm.BranchType2.value = '<%=BranchType2%>';
  }
  catch(ex)
  {
    alert("在LABatchAuthorizeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

/**************************************
 * 初始化选择列表
 **************************************/
function initSelect()
{
  var se = document.getElementById("AgentList");
  se.options.length=0;
}

/**************************************
 * 页面初始化
 **************************************/
function initForm()
{
  try
  {
    initInpBox();
    initSelect();
    initAgentGrid();
    initAuthorizeGrid();
  }
  catch(re)
  {
    alert("LABatchAuthorizeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>