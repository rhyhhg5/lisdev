<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：ContChangeComInput.jsp
//程序功能：
//创建日期：2008-02-19 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContChangeComInit.jsp"%>
    <SCRIPT src="./ContChangeComInput.js"></SCRIPT>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();">
  <form action="./ContChangeComSave.jsp" method=post name=fm target="fraSubmit">  
  <!--    <%@include file="../agent/AgentOp2.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>-->
   <div id="inputButton" style="display: """>
	<table class="common" align=center>
		<tr align=right>
			<td class=button>
				&nbsp;&nbsp;
			</td>		
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="重  置"  TYPE=button onclick="return initForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return addClick();">
			</td>	
		</tr>
	</table>
</div>
    <table>
    <tr>
    <td>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    </td>
    <td class=titleImg>
     保单代理机构调整
    </td>
    </tr>
    </table>
  <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common>
    <TR>
     <TD  class= title>
           印刷号
          </TD>          
          <TD  class= input>
            <Input class=common name=PrtNo onchange="return checkCount();" elementtype=nacessary >
          </TD> 
    </TR>
    	<TR  class= common> 
       
          <TD  class= title>
            原保单代理机构代码
          </TD>          
          <TD  class= input>
            <Input class='readonly' readonly name=AgentComOld   >
          </TD>  
          <td  class= title>
		        原保单代理机构姓名
		</td>
        <td  class= input>
		  <input name=AgentComOldName class='readonly' readonly >
		</td>   
		</TR>
    <TR  class= common> 
       
          <TD  class= title>
            原销售人员代码
          </TD>          
          <TD  class= input>
            <Input class='readonly' readonly name=AgentOld   >
          </TD>  
          <td  class= title>
		        原销售人员姓名
		</td>
        <td  class= input>
		  <input name=PreName class='readonly' readonly >
		</td>   
		</TR>
	
			
<!-- 		<TR class=common>
		 <TD class=title>
		  原管理机构
		</TD>
		<TD  class= input>
          <Input class="codeno" name=ManageComOld readOnly verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComNameOld readOnly >
        </TD>
   <!--     <TD class=title>原销售机构代码</TD>
        <TD class=input>
        	<input class='readonly' name=BranchAttrOld readOnly>
        </TD>
      
		</TR>  
		 -->   
		 	<TR  class= common> 
       
          <TD  class= title>
            新保单代理机构代码
          </TD>          
          <TD  class= input>
            <Input class=common  name=AgentComNew onchange="return checkAgentcom();" elementtype=nacessary >
          </TD>  
          <td  class= title>
		        新保单代理机构姓名
		</td>
        <td  class= input>
		  <input name=AgentComNewName class='readonly' readonly >
		</td>   
		</TR> 
		<TR  class= common> 
		  
		 <td  class= title>
		  新代理人代码
		</td>
        <td  class= input>
		  <input name=AgentNew class='readonly' onchange="return checkname();" elementtype=nacessary>
		</td>   
          <TD  class= title>
            新代理人姓名
          </TD>          
          <TD  class= input>
            <Input class='readonly' readonly name=NameNew >
          </TD>  
     
		</TR>  
	<!--	
		<TR class=common>
		 <TD class=title>
		  新管理机构
		</TD>
		<TD  class= input>
          <Input class="codeno" name=ManageCom readOnly verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly >
        </TD>
          <TD class=title>新销售机构代码</TD>
        <TD class=input>
        	<input class='readonly' name=BranchAttr>
        
		</TR>     
		-->	  
     <TR  class= common> 
        <td  class= title> 
		 归属日期
		</td>
        <td  class= input> 
		  <Input class= "coolDatePicker" dateFormat="short" name=AscriptionDate verify="归属日期|DATE"  elementtype=nacessary> 
		</td> 
      <td  class= title>
		    操作员
		  </td>
      <td  class= input>
		    <input name=Operator class='readonly' readonly >
		  </td>        
                                   
       </TR> 
    </table>  
  
   </div>
    <input type=hidden name=AgentGroupOld value=''>
    <input type=hidden name=AgentGroupNew value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=hideOperate value=''>
       <input type=hidden name=ContNo value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
