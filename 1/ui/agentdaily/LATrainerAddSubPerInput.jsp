<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LATrainerAddBSubPerInput.jsp
//程序功能:组训加扣款录入
//创建日期：2018-06-7
//创建人  ：WangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String ManageCom=tG.ManageCom;
  String Operator=tG.Operator;
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LATrainerAddSubPerInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LATrainerAddSubPerInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LATrainerAddSubPerSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAddSub);">
    
    <td class=titleImg>
              加款信息
    </td> 
    </tr>
    </table>
    <Div  id= "divAddSub" style= "display: ''">  
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		      组训人员工号
		    </td>
        <td  class= input> 
		      <input class= common name=TrainerCode MaxLength=10 OnChange="return checkvalid();" verify="组训人员工号|NOTNULL" elementtype=nacessary> 
		    </td>
		    <td  class= title>
		      姓名
		    </td>
        <td  class= input>
		      <input name=Name class='readonly' readonly >
		    </td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		      管理机构 
		    </td>
        <td  class= input> 
		      <input class="readonly" readonly name=ManageCom > 
		    </td>
		    <td  class= title>
		   操作员代码
		   </td>
       <td  class= input>
		    <input name=Operator  class="readonly" value='<%=Operator%>' readonly  >
		   </td>
      </tr>
      <tr>
      <TD  class= title width="25%"> 
		     调整年月
		    </td>
         <td class= input>
         <Input class= 'common'   name=WageNo verify="调整年月|NOTNULL&num&len=6" elementtype=nacessary>
         <font color="red">(yyyymm)
         </td>
        <td  class= title> 
		   最近操作日
		   </td>
       <td  class= input> 
		    <input class="readonly"  readonly name=ModifyDate > 
		   </td>
	   </tr>
      <tr  class= common> 
        <td class=title>
	        加款类型
	      </td>
	      <td class=input>
		   <input name=DoneFlag class=codeno  verify="加款类型"
            ondblclick="return showCodeList('activebankmoneyj',[this,DoneFlagName],[0,1],null,8,'',null,null);" 
        	onkeyup="return showCodeListKey('activebankmoneyj',[this,DoneFlagName],[0,1],null,8,'',null,null);"
        	><Input class=codename name=DoneFlagName readOnly  > 
        </td>	
         <td  class= title> 
		      加款原因
		     </td>
         <td  class= input> 
		     <input name=PunishRsn class= common   > 
		     </td>
	  </tr>
	  <tr  class= common>
         <td  class= title> 
		      加款金额(元)
		     </td>
         <td  class= input> 
		       <input name=Money class= common verify="金额|num&value>0" > 
		     </td>
       </tr>
       
     
    </table>
    </Div>
      
    <table>
    <tr>
      <td class=common>
		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
      </td>
    	<td class= titleImg>
    	  扣款信息
     	</td>
    </tr>
    </table>
    <Div  id= "divAGroupGrid" style= "display: ''">
    <table  class= common>
    <tr  class= common> 
  	  <td class=title>
	      扣款类型
	    </td>
	    <td class=input>
	    	<input name=DoneFlag1 class=codeno   verify="扣款类型"
         ondblclick="return showCodeList('activebankmoneyk',[this,DoneFlag1Name],[0,1],null,8,'',null,null);" 
        	onkeyup="return showCodeListKey('activebankmoneyk',[this,DoneFlag1Name],[0,1],null,8,'',null,null);"
         ><Input class=codename name=DoneFlag1Name readOnly > 
       </td>	
       <td  class= title> 
		    扣款原因
	   </td>
       <td  class= input> 
		     <input name=PunishRsn1 class= common   > 
	  </td>
	 </tr>
      <tr  class= common> 
       <td  class= title> 
		    扣款金额(元)
	   </td>
       <td  class= input> 
		     <input name=Money1 class= common verify="金额|num&value>0" > 
		   </td>
      </tr>  
        
   </table>
    <p> <font color='red'>注：加款项目和扣款项目至少录入一项！加款金额和扣款金额都为正数！类型和金额是必填项！</font></p>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=Idx value=''>
    <input type=hidden name=Flag value=''>
    <input type=hidden name=Flag1 value=''>
    <input type=hidden name=AwardTitle value=''>
    <input type=hidden name=BranchType value=''>   
    <input type=hidden name=BranchType2 value=''>  
    <input type=hidden name=LogManagecom value='<%=ManageCom%>'>
    <input type=hidden name=AgentCode value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
