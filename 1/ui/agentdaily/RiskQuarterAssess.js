//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!checkValue()) return false;
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  //easyQueryClick();
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
   
function easyQueryClick()
{	
  if(!verifyInput()) return false;   
  var  sql = " select a.idx,b.gradecode,b.gradename, "
           + " a.riskcode,'RRR', COALESCE(a.standprem,0)"
           + " from laagentgrade b  left join    LADiscount a "
           + " on  a.agentgrade=b.gradecode  "
           + " and a.discounttype='05' "
           + " and a.branchtype='3' and a.branchtype2='01'  "
           + getWherePart('a.ManageCom', 'ManageCom')
           +" where b.branchtype='3' and b.branchtype2='01' and b.gradecode<>'F00' "           
           +" order by b.gradecode ";  
           
// var  sql = " select d.idx,aa.j,aa.k,aa.l,aa.m,COALESCE(d.standprem,0)  "//此处由COALESCE替换了nvl，不知道为什么DB2的nvl会产生非常诡异的小数位   update by lh 2009-7-1
//           + " from ( select j,k,l,m  "
//           + " from ( "
//           + " select a.gradecode j,a.gradename k,b.code l,b.codename m "
//           + " from laagentgrade a,ldcode b "
//           + " where a.branchtype='3' and a.branchtype2='01' "
//           + " and a.gradecode<>'F00' "
//           + " and b.codetype='bankfinish' and code in ('331201') "
//           + " )as hh "
//           + " )as aa left join ladiscount d on j=d.agentgrade and l=d.riskcode  "
//           + " and  d.discounttype='05' and d.branchtype='3' and d.branchtype2='01' "
//           + " and d.ManageCom='"+fm.all('ManageCom').value+"'"
//           + " order by aa.j";  
                       
//  turnPage.queryModal(sql, RiskQuarterAssessGrid);
//  if(RiskQuarterAssessGrid.mulLineCount == 0)
//  {
//    alert("没有符合条件的险种季度考核信息");
//  }
 var arrResult = new Array();
  arrResult = easyExecSql(sql);
 //var i=parseFloat(arrResult[89][5]);
  //alert(arrResult[89][5]);
  if (!arrResult) 
    {
    alert("没有符合条件的数据！");
    return false;
    }
    displayMultiline(arrResult,RiskQuarterAssessGrid);
}

function checkValue()
{
	var countNum=0;
	var selFlag=true;
	if(RiskQuarterAssessGrid.mulLineCount==0)
	{
		alert('没有选择要操作的记录！');
		return false;
	}
	for(var i=0;i<RiskQuarterAssessGrid.mulLineCount;i++)
	{　
		if(RiskQuarterAssessGrid.getChkNo(i))
		{
			countNum++;
			
			  if(RiskQuarterAssessGrid.getRowColData(i,2).trim()=='')
			    {
				  alert('职级不能为空');
				  return false;
			    }
			  if(RiskQuarterAssessGrid.getRowColData(i,3).trim()=='')
			    {
				  alert('职级名称不能为空');
				  return false;
			    }
		
			if((RiskQuarterAssessGrid.getRowColData(i,1) == null)||(RiskQuarterAssessGrid.getRowColData(i,1)=="")
			||(RiskQuarterAssessGrid.getRowColData(i,1)==0)||(RiskQuarterAssessGrid.getRowColData(i,1)=="null"))
            {
              if((fm.all("fmtransact").value =="UPDATE") )
              {
                alert("如果修改前季度考核标准为0,一般表示该职级未录入相关季度考核标准信息.请先对该职级进行考核标准录入操作！");
                RiskQuarterAssessGrid.checkBoxAllNot();
                RiskQuarterAssessGrid.setFocus(i,2,RiskQuarterAssessGrid);
                selFlag = false;
                break;
              }
            }
            //为了做链接而改动
     else {	//如果idx不为空不能插入
              if(fm.all("fmtransact").value =="INSERT")
              {
                alert("此纪录已存在，不可插入！");
                RiskQuarterAssessGrid.checkBoxAllNot();
                RiskQuarterAssessGrid.setFocus(i,2,RiskQuarterAssessGrid);
                selFlag = false;
                break;
              }
            }
        
		}
	}
	if(!selFlag)
	{
   	  return selFlag;
   	}
	if(countNum==0)
	{
		alert("请选择进行操作的记录。");
		return false;
	}
	if(fm.all("fmtransact").value=="UPDATE")
	{
		for(var i=0;i<RiskQuarterAssessGrid.mulLineCount;i++)
		{
			if(RiskQuarterAssessGrid.getRowColData(0,1).trim()=='')
			{
				alert('列表中存在系统没有的记录，不能修改');
				return false;
			}
		}
	} 
//	for(var i=0;i<RiskQuarterAssessGrid.mulLineCount;i++)
//	{
//		if(RiskQuarterAssessGrid.getChkNo(i)){
//      var  sql=" select gradecode from laagentgrade where gradecode='"+RiskQuarterAssessGrid.getRowColData(0,2)+"' with ur";
//      var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
//      if(!strQueryResult)
//      {
//        alert("职级:"+RiskQuarterAssessGrid.getRowColData(0,2)+"系统中不存在");
//        return false;
//      }
//    }
//	}
	return true 
}
function save()
{
	fm.all("fmtransact").value="INSERT";
	submitForm();
}
function update()
{
	fm.all("fmtransact").value="UPDATE";
	submitForm();
}