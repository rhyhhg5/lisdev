//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
  if (mOperate=="")
  {
  	addClick();
  }
  //alert(mOperate);
   //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   	//alert("请重新输入信息！");
   	//return false;
  //}
 if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  //if (!afterSubmit())
  //  return false;
   //alert("222");
  fm.submit(); //提交
 //  alert("111222");
  
  //  alert("111");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
 //fm.all.value = '';
  //document.all.AgentCode.readOnly="true";
 fm.reset();
 
//alert("123");
  mOperate="";
  showInfo.close();
  //fm.all.value == '';
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LAHols.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	 
//if ((fm.all('MettingAim').value == '')||(fm.all('MettingAim').value == null))
//{
//	alert("请输入会议目的和诉求！");
//	fm.all('MettingAim').focus();
//	return false;
//}
//if ((fm.all('AgentGroup').value == '')||(fm.all('AgentGroup').value == null))
//{
//	alert("请输入主办机构！");
//	fm.all('AgentGroup').focus();
//	return false;
//}     
  if(!verifyInput())
  return  false;
  if(JHshStrLen(document.fm.AgentGroup.value) > 30)
  {
  	alert("主办机构最多只能输入40个字！");
  	fm.all('AgentGroup').focus();
  	return false;
  }

  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("SeriesNo").value==null)||(fm.all("SeriesNo").value==''))
    alert('请先查询出要修改的纪录！');
  else if ((fm.all("MettingAim").value==null)||(fm.all("MettingAim").value==''))
    alert('会议目的和诉求不能为空！');
  else if ((fm.all("AgentGroup").value==null)||(fm.all("AgentGroup").value==''))
    alert('主办机构不能为空！');
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LAMettingAssessQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("SeriesNo").value==null)||(fm.all("SeriesNo").value==''))
    alert('请先查询出要修改的纪录！');
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


/*
function agentConfirm()
{
  fm.all('AgentGroup').value='';
  fm.all('ManageCom').value='';	
  parent.fraInterface.fm.action = "AgentCodeQuery.jsp";	
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
     alert("请输入代理人编码！");
     fm.all('AgentCode').focus();
     return false;
  }
  fm.submit();
  parent.fraInterface.fm.action = "LAHolsSave.jsp";	
  //alert('change');
}*/

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
                fm.all('SeriesNo').value = arrResult[0][0];
                //fm.all('AgentCode').readonly=true;
                //document.getElementById("myid").readonly=true;
             
                fm.all('MettingAim').value = arrResult[0][1];
                fm.all('AgentGroup').value = arrResult[0][2];
                fm.all('MettingForm').value = arrResult[0][3];
                fm.all('PayMoney').value = arrResult[0][4];
                fm.all('Import').value = arrResult[0][5];
                fm.all('DateTime').value = arrResult[0][6];
                fm.all('Address').value = arrResult[0][7];
               
                fm.all('Leads').value = arrResult[0][8];
                fm.all('Peoples').value = arrResult[0][9];
                fm.all('Agenda').value = arrResult[0][10];
              
                fm.all('Criterion').value = arrResult[0][11];
                fm.all('Decide').value = arrResult[0][12];
                
                fm.all('Register').value = arrResult[0][13];
              
                fm.all('hideOperate').value = arrResult[0][14];   
                //<addcode>############################################################//
             
                
                //<addcode>############################################################//                                                                                                                                                                                                                                            	
 	 //return true;
 	}     

 
}

// -----------------------------------------------------------------------------
// 本函数用于测试字符串sString的长度;
// 注:对本函数来说,1个汉字代表2单位长度;
// -----------------------------------------------------------------------------
function JHshStrLen(sString)
{
  var sStr,iCount,i,strTemp ; 

  iCount = 0 ;
  sStr = sString.split("");
  for (i = 0 ; i < sStr.length ; i ++)
  {
    strTemp = escape(sStr[i]); 
    if (strTemp.indexOf("%u",0) == -1) // 表示是汉字
    { 
      iCount = iCount + 1 ;
    }else{
      iCount = iCount + 2 ;
    }
  }
  return iCount ;
}
