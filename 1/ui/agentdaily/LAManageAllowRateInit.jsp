<%
//程序名称：LAManageAllowRateInput.jsp
//程序功能：
//创建日期：20090219
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>     
<script language="JavaScript">
var StrSql=" 1 and branchtype=#3# and branchtype2=#01#  AND GRADECODE>=#G41# ";
var rSql=" 1 and comcode=#0# ";

</script>
                                      
<script language="JavaScript">

function initInpBox()
{ 
  try
  {         
      fm.all('ManageCom').value = "";
      fm.all('ManageComName').value = "";
  }
  catch(ex)
  {
    alert("LAManageAllowRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initManageAllowRateGrid();
  }
  catch(re)
  {
    alert("LAManageAllowRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ManageAllowRateGrid;
function initManageAllowRateGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
//    iArray[1]=new Array();                                                 
//    iArray[1][0]="职级";         		//列名                             
//    iArray[1][1]="40px";            		//列宽                             
//    iArray[1][2]=200;            			//列最大值                           
//    iArray[1][3]=2;                                                        
//    iArray[1][4]="assessgrade2";                                           
//    iArray[1][5]="1|2";              	                                     
//    iArray[1][6]="0|1";                                                    
//    iArray[1][9]="职级|code:assessgrade2";                             
//    iArray[1][15]= "1";                                                    
//    iArray[1][16]= StrSql;                                                 
//                                                                           
    iArray[1]=new Array();                                                 
    iArray[1][0]="职级类别";         		//列名                             
    iArray[1][1]="40px";            		//列宽                             
    iArray[1][2]=200;            			//列最大值                           
    iArray[1][3]=2;                                                        
    iArray[1][4]="gradelevel";                                           
    iArray[1][5]="1|2";              	                                     
    iArray[1][6]="0|1";                                                    
    iArray[1][9]="职级类别|code:gradelevel";                             
    iArray[1][15]= "1";                                                    
    iArray[1][16]= rSql;            


//    iArray[1]=new Array();
//    iArray[1][0]="职级类别"; //列名
//    iArray[1][1]="60px";        //列宽
//    iArray[1][2]=100;            //列最大值
//    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许    
//    iArray[1][9]="职级类别|NotNull";
//    iArray[1][10]="agentgrade";
//    iArray[1][11]="0|^G5|渠道经理级别|^G7|营业部经理级别";


    
    
    iArray[2]=new Array();                                                 
    iArray[2][0]="职级类别名称";         		//列名                         
    iArray[2][1]="120px";            		//列宽                             
    iArray[2][2]=200;            			//列最大值                           
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="管理津贴";          		        //列名
    iArray[3][1]="120px";      	      		//列宽         			//列最大值
    iArray[3][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[3][3]=1;   
    iArray[3][9]="管理津贴|NotNull";  
    
    iArray[4]=new Array();
    iArray[4][0]="Idx号";          		        //列名
    iArray[4][1]="0px";      	      		//列宽         			//列最大值
    iArray[4][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[4][3]=3; 
    
    ManageAllowRateGrid = new MulLineEnter( "fm" , "ManageAllowRateGrid" ); 
    //这些属性必须在loadMulLine前

    ManageAllowRateGrid.mulLineCount = 0;  
    ManageAllowRateGrid.canChk = 1; 
    ManageAllowRateGrid.displayTitle = 1;
    ManageAllowRateGrid.hiddenPlus = 0;
    ManageAllowRateGrid.hiddenSubtraction = 1;    
    

    ManageAllowRateGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
