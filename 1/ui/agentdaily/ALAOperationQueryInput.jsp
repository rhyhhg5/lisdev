<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String tLoadFlag = "";
	String tGrpContNo = "";
	
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		tGrpContNo = request.getParameter( "GrpContNo" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "2";//LoadFlag本身的 意义关键就在于个单部分
	}
	catch( Exception e1 )
	{
		tLoadFlag = "2";
	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
        System.out.println("LoadFlag:" + tLoadFlag);
         System.out.println("扫描类型:" + request.getParameter("scantype"));       
         
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%> 
<script>
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var ProposalGrpContNo = "<%=request.getParameter("GrpContNo")%>";
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ALAOperationQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ALAOperationQueryInit.jsp"%>
</head>

<body  onload="initForm();" >
  <form action="./ALAOperationQuerySave.jsp" method=post name=fm target="fraSubmit">

  <DIV id=DivLCContButton STYLE="display:''">
  <table id="table1">
  		<tr>
  			<td>
  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroupPol1);">
  			</td>
  			<td class="titleImg">内控作业栏
  			</td>
  		</tr>
  </table>
  </DIV>
    <Div  id= "divGroupPol1" style= "display: ''">
      <table  class= common>
        <TR  class= common>	 
          <TD  class= title8>
            团体投保单号码
          </TD>
          <TD  class= input8>
            <Input class="common" name=ProposalGrpContNo readonly >
          </TD>
          <TD  class= title8>
            印刷号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=PrtNo readonly >
          </TD>
          <TD  class= title8>
            管理机构
          </TD>
          <TD  class= input8>
            <Input class=codeNo name=ManageCom readonly ><input class=codename  name=ManageComName readonly >
          </TD>
        </TR>
        <TR  class= common8>
          <TD  class= title8>
            销售渠道
          </TD> 
          <TD  class= input8>
          	<Input class=codeNo name=SaleChnl readonly ><input class=codename name=SaleChnlName readonly >
          </TD>
    	   <TD  class= title8>
            经办人签名
          </TD>
          <TD  class= input8>
          <Input name=HandlerName class="common" readonly >
          </TD>        
          <TD  class= title8>
            投保单填写日期
          </TD>
          <TD  class= input8>
            <Input class="common"  name=HandlerDate readonly >
          </TD>
        </TR>
        <TR class= common8>
          <TD  class= title8>
            中介公司代码
          </TD>
          <TD  class= input8>
            <Input class="common" name=AgentCom readonly >
          </TD>
          <TD  class= title8>
            中介公司名称
          </TD>
          <TD  class= input8>
            <Input class="common" name=AgentComName readonly >
          </TD>
					<TD  class= title8>
            业务员填写日期 
          </TD>
          <TD  class= input8>
            <Input class="common" name=AgentDate readonly >
          </TD>
        </TR>
	      <TR class=common>
	      	<TD  class= title8>
            业务员代码
          </TD>
          <TD  class= input8>
      			<Input class="common" NAME=AgentCode readonly >
         </TD>
          <TD  class= title8>
            业务员名称
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentName CLASS=common readonly >
         </TD>
        	<TD  class= title8>
            接收日期 
          </TD>
          <TD  class= input>
            <Input class=common name=ReceiveDate readonly >
        	</td>
        </TR>
		    <TR CLASS="common">
      </table>
    </Div>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol2);">
    		</td>
    		<td class= titleImg>
    			 团体基本资料（团体客户号 <Input class= common8 name=GrpNo readonly > ）
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       <TR>
          <TD class= title8>
            投保团体名称
          </TD>
          <TD class= input8>
            <Input class= common8 name=GrpName readonly >
          </TD>
          <TD  class= title8>
            单位地址
          </TD>
          <TD class= input8>
            <Input class= common8 name=GrpAddress readonly >
          </TD>
          <TD class= title8>
            邮政编码
          </TD>
          <TD class= input8>
            <Input class= common8 name=GrpZipCode readonly >
          </TD>
        </TR>
		    <TR CLASS="common">
          <TD  class= title8>
            联系人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1 readonly >
          </TD>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone1 readonly >
          </TD>
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax1 readonly >
          </TD>
        </TR>
		    <TR CLASS="common">
          <TD  class= title8>
            电子邮箱
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail1 readonly >
          </TD>
          <!--TD  class= title8>
            行业编码
          </TD>
          <TD  class= input8>
            <Input class="code8" name=BusinessType elementtype=nacessary verify="行业类别|notnull&code:BusinessType&len<=20" ondblclick="return showCodeList('BusinessType',[this,BusinessTypeName,BusinessBigType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('BusinessType',[this,BusinessTypeName,BusinessBigType],[0,1,2],null,null,null,1,300);">
          </TD--> 
          <TD  class= title8>
            行业性质
          </TD>                   
          <TD  class= input8>   
            <Input class="common8" name=BusinessTypeName readonly >
          </TD>                                     
          <!--TD  class= title8>
            企业类型编码
          </TD>
          <TD  class= input8>
            <Input class=code8 name=GrpNature elementtype=nacessary verify="单位性质|notnull&code:grpNature&len<=10" ondblclick="showCodeList('GrpNature',[this,GrpNatureName],[0,1]);" onkeyup="showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);">
          </TD--> 
          <TD  class= title8>
            企业类型
          </TD>                  
          <TD  class= input8>            
            <Input class=common8 name=GrpNatureName readonly >          
          </TD>
        </TR>
		    <TR CLASS="common">
          <TD  class= title8>
            员工总人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Peoples readonly >
          </TD>
          <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOnWorkPeoples readonly >
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOffWorkPeoples readonly >
          </TD>
        </TR>
		    <TR CLASS="common">
          <TD  class= title8>
            其他人员人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOtherPeoples readonly >
          </TD>                                               
      </table>
    </Div>
    <table class= common border=0 width=100%>
    	<tr>
		    <td class= titleImg align= center>团体保单核保结论：</td>
	    </tr>
    </table>
    <table  class= common border=0 width=100%>
      <TR  class= common>
        <TD  height="29" class= title>
           团体保单核保结论
           <Input class=codeno name=GUWState readonly ><input class=codename name=GUWStateName readonly >
        </TD>
      </TR>
      <tr>
        <TD  class= title>
          团体保单核保意见
        </TD>
      </tr>
      <tr>
        <TD  class= input> <textarea name="GUWIdea" cols="100%" rows="5" witdh=100% class="common" readonly ></textarea></TD>
      </tr>
    </table>
    
    <!--table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divpeople);">
    		</td>
    		<td class= titleImg>
    			 参保人员资料
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divpeople" style= "display: ''">
      <table class=common>
         <TR  class= common>       
          <TD  class= title8>
            被保险人数（成员）
          </TD>
          <TD  class= input8>
          <Input name=Peoples3 class= common8 elementtype=nacessary verify="被保险人数（成员）|int&notnull">
          </TD>           
          <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
          <Input name=OnWorkPeoples class= common8 >
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
          <Input name=OffWorkPeoples class= common8 >
          </TD>           
          </TR>
         <TR  class= common>                   
          <TD  class= title8>
            其他人员人数
          </TD>
          <TD  class= input8>
          <Input name=OtherPeoples class= common8 >
          </TD>                                          
         <TD  class= title8>
            连带被保险人数（家属）
          </TD>
          <TD  class= input8>
          <Input name=RelaPeoples class= common8 >
          </TD>  
          <TD  class= title8>
            配偶人数
          </TD>
          <TD  class= input8>
          <Input name=RelaMatePeoples class= common8 >
          </TD>                  
          </TR>  
          <TR  class= common>             
          <TD  class= title8>
            子女人数
          </TD>
          <TD  class= input8>
          <Input name=RelaYoungPeoples class= common8 >
          </TD>                                                    
          <TD  class= title8>
            其他人员人数
          </TD>
          <TD  class= input8>
          <Input name=RelaOtherPeoples class= common8 >
          </TD> 
          </TR>     
      </table>
    </Div-->
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>