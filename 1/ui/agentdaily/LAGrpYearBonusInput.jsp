<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAGroupZTRateCommSetInput.jsp
//程序功能：职团险种提奖比例录入
//创建时间：2008-05-15
//创建人  ：Elsa

%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<script language="JavaScript">
var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and riskprop = #I#)";
var tsql=" 1 and char(length(trim(comcode))) in (#8#,#4#,#2#) ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LAGrpYearBonusInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <SCRIPT src="LAGrpYearBonusInput.js"></SCRIPT>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LAGrpYearBonusSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
    	
    	
    	 <TD  class= title>
            管理机构
   </TD>
   <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary > 
   </TD> 
    	
     <TD  class= title>
            销售机构
          </TD>
          <TD  class= input>
            <Input class= "common"  name=BranchAttr >
          </TD>
             
   </tr>
   <tr>			
   		<TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
            <Input class= "common"  name=GroupAgentCode onchange = "return checkAgentCode()">
          </TD>
            <TD  class= title>
            业务员姓名
          </TD>
          <TD  class= input>
            <Input class= "common"  name=AgentName >
          </TD>
   	
   </tr>
   
   
   <TR  class= common> 
    
    
         <td  class= title>薪资年月</td>
          <TD  class= input> <Input class= "common" name=WageNo  ></TD>      
   </TR>

  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();"> 
     		<input type=button value="删  除" class=cssButton onclick="return DoDelete();"> 
    	</td>
   
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>业务员效益奖信息</td><td><font color="red" size=2>（效益奖支持两位小数）</font></td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
   <!--table class=common>
    <tr class=common>
     <td class=input colspan=4 align=center><input type=button value="增     加" class=common onclick="addOneRow();">
     </td>
    </tr>
   </table-->      
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>
    <Input type=hidden id="BranchType" name=BranchType value=''>
    <input type=hidden id="sql_where" name="sql_where" >
    <input type=hidden id="fmAction" name="fmAction"> 
    <input type=hidden name=BranchType2 value=''>  
    <input type=hidden name=AgentCode value=''>

 </form> 
  
<hr></hr>  
    		<p>请先通过功能“年效益奖标准报表”打出人员整年的月度提奖数据，再按照模板的要求进行导入</p>
    		<p>注：年效益奖录入规则：</p>
    		<p><1>：年效益奖录入规则：同一年份不能对同一业务员多次进行年效益奖录入操作</p> 
    		<p><2>: 分公司只能统一进行年效益奖发放操作(分公司不能跨薪资月进行业务员年效益奖发放)</p>
    		<p>批量导入过程中，如不满足条件<2>,则系统默认处理回滚导入操作！</p>
   <form action="./DiskImportGrpYearBonusSave.jsp" method=post name=fm2 enctype="multipart/form-data" target="fraSubmit">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportFile);">
    		</td>
    		<td class= titleImg>磁盘导入</td>

    	</tr>
  </table>
  <div id="divImportFile" style="display:''">   

  <table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">文件名</TD>
				<TD class=input>
					<Input type="file" class= "common" style="width:300px" name="FileName" id="FileName"/>
				</TD><td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()"></td>
			</TR>
  </table>
	<Input type=button class=cssButton name="goDiskImport" value="导  入" id="goDiskImport"  onclick="diskImport()">
	<Input type=button class=cssButton value="重  置" onclick="clearImport()">

  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportQuery);">
    		  </td>
    		  <td class= titleImg>磁盘导入日志查询</td>
    	</tr>
  </table>
  <div id="divImportQuery" style="display:''">
  	<table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">导入文件批次</TD>
				<TD class=input>
					<Input type="text" class= "common"  name="FileImportNo"/>
				</TD>
				
				<TD  class=title style="width:100px">
				查询类型
				</td>
				<td class=input>
				<Input class="codeno" name=queryType  CodeData="0|^0|全部|^1|导入错误行|^2|导入成功行" ondblclick="return showCodeListEx('querytypelist',[this,queryName],[0,1]);" onkeyup="return showCodeListKeyEx('querytypelist',[this,queryName],[0,1]);" onchange=""><input class=codename name=queryName  readonly=true >
				</TD>
				<td></td>
			</TR>
   </table>
  <Input type=button class=cssButton value="查  询" onclick="queryImportResult()"/>
  <div id="divImportResultGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanImportResultGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage2.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage2.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage2.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage2.lastPage();">
  </div>
  </div>  
		  
  <Input type=hidden name=BranchType2> 
  <Input type=hidden  name=BranchType >
  <Input type=hidden name=diskimporttype>
 </div>  
 </form> 
  

 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




