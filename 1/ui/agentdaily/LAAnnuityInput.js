//该文件中包含客户端需要处理的函数和事件

//查询业务员信息
function queryPersonnel()
{
	var tSql = "";
	
	//判断有无业务员代码
	if (getWherePart('AgentCode')=='')
  {
    document.fm.AgentGroup.value = "";
    document.fm.ManageCode.value  = "";
    document.fm.AgentName.value  = "";
   	return false;
  }

  tSql  = "SELECT";
  tSql += "    A.Name,";
  tSql += "    A.ManageCom,";
  tSql += "    B.BranchAttr";
  tSql += "  FROM";
  tSql += "    LAAgent A,";
  tSql += "    LABranchGroup B";
  tSql += " WHERE ";
  tSql += "    A.AgentGroup = B.AgentGroup";
  tSql += getWherePart('AgentCode');

  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    //document.fm.AgentCode.value="";
    document.fm.AgentGroup.value = "";
    document.fm.ManageCode.value  = "";
    document.fm.AgentName.value  = "";
    return;
  }
  
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  //设置姓名
  document.fm.AgentName.value = tArr[0][0];
  //管理机构
  document.fm.ManageCode.value = tArr[0][1];
  //销售单位
  document.fm.AgentGroup.value = tArr[0][2];
}

//查询按钮出发事件
function queryClick()
{
	  //下面增加相应的代码
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LAAnnuityQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

//保存按钮触发事件
function submitForm()
{
	//验证画面录入是否合法
	if(!checkValid())
	{
		return false;
	}
	document.fm.fmtransact.value = "INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//提交
	//alert("验证通过，准备提交！");
	document.fm.submit();
}

//修改按钮触发事件
function updateClick()
{
	//验证画面录入是否合法
	if(!checkValid())
	{
		return false;
	}
	document.fm.fmtransact.value = "UPDATE||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//提交
	//alert("验证通过，准备提交！");
	document.fm.submit();
}

//作画面验证
function checkValid()
{
	//验证数字录入
	var tTxtValue="";
	//养老金合计验证
	//得到画面上的输入内容并去掉空格
	tTxtValue = trim(document.fm.SumAnnuity.value);
	//进行数字验证
	if(tTxtValue.length>0)
	{
		if(!isNumeric(tTxtValue))
		{
			alert("[养老金合计]必须输入数字！");
			return false;
		}
	}
	//应付养老金金额
	//得到画面上的输入内容并去掉空格
	tTxtValue = trim(document.fm.DealAnnuity.value);
	//进行数字验证
	if(tTxtValue.length>0)
	{
		if(!isNumeric(tTxtValue))
		{
			alert("[应付养老金金额]必须输入数字！");
			return false;
		}
		var arrValue = tTxtValue.split(".")
		if(arrValue[0].length > 10)
		{
			alert("您输入[应付养老金金额]的数字太大，整数部分不能超过10位！");
			return false;
		}
		if(arrValue.length > 1)
		{
			if(arrValue[1].length > 2)
			{
				alert("您输入[应付养老金金额]的数字小数部分太长，小数部分不能超过2位！");
				return false;
			}
		}
	}
	
	//领取养老金金额
	//得到画面上的输入内容并去掉空格
	tTxtValue = trim(document.fm.DrawAnnuity.value);
	//进行数字验证
	if(tTxtValue.length>0)
	{
		if(!isNumeric(tTxtValue))
		{
			alert("[领取养老金金额]必须输入数字！");
			return false;
		}
		var arrValue = tTxtValue.split(".")
		if(arrValue[0].length > 10)
		{
			alert("您输入[领取养老金金额]的数字太大，整数部分不能超过10位！");
			return false;
		}
		if(arrValue.length > 1)
		{
			if(arrValue[1].length > 2)
			{
				alert("您输入[领取养老金金额]的数字小数部分太长，小数部分不能超过2位！");
				return false;
			}
		}
	}
	//当月养老金提取基数
	//得到画面上的输入内容并去掉空格
	tTxtValue = trim(document.fm.BaseAnnuity.value);
	//进行数字验证
	if(tTxtValue.length>0)
	{
		if(!isNumeric(tTxtValue))
		{
			alert("[当月养老金提取基数]必须输入数字！");
			return false;
		}
	}
	//养老金提取比例
	//得到画面上的输入内容并去掉空格
	tTxtValue = trim(document.fm.ScaleAnnuity.value);
	//进行数字验证
	if(tTxtValue.length>0)
	{
		if(!isNumeric(tTxtValue))
		{
			alert("[养老金提取比例]必须输入数字！");
			return false;
		}
	}
	//公司养老金累计
	//得到画面上的输入内容并去掉空格
	tTxtValue = trim(document.fm.CoAnnuity.value);
	//进行数字验证
	if(tTxtValue.length>0)
	{
		if(!isNumeric(tTxtValue))
		{
			alert("[公司养老金累计]必须输入数字！");
			return false;
		}
	}
	//个人养老金累计
	//得到画面上的输入内容并去掉空格
	tTxtValue = trim(document.fm.SingleAnnuity.value);
	//进行数字验证
	if(tTxtValue.length>0)
	{
		if(!isNumeric(tTxtValue))
		{
			alert("[个人养老金累计]必须输入数字！");
			return false;
		}
	}
	
	return true;
}

//接受查询画面返回的值
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
    fm.all('AgentCode').value= arrResult[0][0];      //业务员编码
    fm.all('AgentName').value= arrResult[0][1];      //业务员姓名
    fm.all('AgentGroup').value= arrResult[0][2];     //展业机构编码
    fm.all('ManageCode').value= arrResult[0][4];     //管理机构编码
    fm.all('OutWorkDate').value= arrResult[0][5];    //离司时间
    fm.all('PayDate').value= arrResult[0][6];        //实付时间
    fm.all('BaseAnnuity').value= "";//arrResult[0][20];   //当月养老金提取基数
    fm.all('ScaleAnnuity').value= arrResult[0][11];  //养老金提取比例
    fm.all('SumAnnuity').value= arrResult[0][13];     //养老金合计
    fm.all('DealAnnuity').value = arrResult[0][7];   //应付养老金金额
    fm.all('DrawAnnuity').value = arrResult[0][8];   //领取养老金金额
    fm.all('SeriesNo').value= arrResult[0][12];      //领取ID
    fm.all('DrawMarker').value= arrResult[0][10];    //领取状态ID
    fm.all('DrawMarkerName').value= arrResult[0][9]; //领取状态ID
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
 //   showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}