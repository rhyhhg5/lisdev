//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
   
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LARewardPunishQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


function getQueryResult()
{
  var arrSelected = null;
  tRow = RewardPunishGrid.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet == null )
    return arrSelected;
  arrSelected = new Array();
  //alert(tRow);
  var AgentCode=RewardPunishGrid.getRowColData(tRow-1,1);
  var idx=RewardPunishGrid.getRowColData(tRow-1,4)
  //alert(idx);
  var strSQL = "select c.groupAgentCode, c.name,b.BranchAttr,a.ManageCom,a.AwardTitle,a.PunishRsn,a.AClass,a.SendGrp,"
  +" a.Money,a.Noti,a.wageno,a.Idx,a.Operator,a.DoneFlag,c.agentgroup,a.ModifyDate" +
  		",doneflag"
	  +",codename('healthmoneyadd',doneflag)" +
  		"  from LARewardPunish a,labranchgroup b,LAAgent c "
  +" where c.groupAgentCode = '"+trim(AgentCode)+"'"
  +"  and a.Idx = "+trim(idx)+" "
  +" and a.AgentCode = c.AgentCode  and b.agentgroup=c.agentgroup";
  //alert(strSQL);
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  arrSelected = decodeEasyQueryResult(strQueryResult);
  return arrSelected;
}


function returnParent()
{
  var arrReturn = new Array();
  var tSel = RewardPunishGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
    try
    {	
      arrReturn = getQueryResult();
    //  alert();
    //   alert(arrReturn);
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
		
  }
}




function easyQueryClick()
{    
	 if (!verifyInput())
	 return false ;
  // 初始化表格
  initRewardPunishGrid();
	var tReturn = getManageComLimitlike("a.ManageCom");
  // 书写SQL语句
  var strSQL = "";
	strSQL = "select c.groupAgentCode,b.BranchAttr,a.ManageCom,a.Idx,a.AwardTitle,a.PunishRsn  ,a.Money,a.WageNo"	                  
	         +" from LARewardPunish a,labranchgroup b,laagent c where 1=1 and doneflag<>'4' and a.agentcode=c.agentcode "
	         +" and c.agentgroup=b.agentgroup "
	   //+      a.DoneFlag <> '0'
	         +tReturn
	   + getWherePart('c.groupAgentCode','AgentCode')
	   + getWherePart('a.ManageCom','ManageCom','like')
	   + getWherePart('a.WageNo','WageNo') 
	   + getWherePart('a.BranchType','BranchType')
	   + getWherePart('b.branchattr','BranchAttr')
	   + getWherePart('a.BranchType2','BranchType2');
	    
	  	
	if((fm.all("DoneFlag").value != null && fm.all("DoneFlag").value != '')
	 		&& (fm.all("DoneFlag1").value != null && fm.all("DoneFlag1").value!= ''))
	 		{
	 		strSQL+=" and a.doneflag in ('"+fm.all("DoneFlag").value+"','"+fm.all("DoneFlag1").value +"')";
	 	  }         
	         
	         
	    else 	if((fm.all("DoneFlag1").value != null && fm.all("DoneFlag1").value != ''))
	 		{
	 			strSQL+=" and a.doneflag ='"+fm.all("DoneFlag1").value+"'";
	 			}		
	 	
      else if((fm.all("DoneFlag").value != null && fm.all("DoneFlag").value != ''))
	 		{
	 			strSQL+=" and a.doneflag ='"+fm.all("DoneFlag").value+"'";
	 			}		

	 
           strSQL+=" order by a.modifydate ";
	     
	   //alert(strSQL);	   
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,7,8,9]);
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = RewardPunishGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES); 

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


//提交，保存按钮对应操作
function ListExecl()
{

  if (!verifyInput())
	 return false ;

	var tReturn = getManageComLimitlike("a.ManageCom");
	var tflag=fm.all("DoneFlag").value;
	var tBranchAttr = fm.all("BranchAttr").value;
  // 书写SQL语句

  var strSQL = "";
	strSQL = "select c.groupAgentCode,b.BranchAttr,a.ManageCom,a.AwardTitle,a.PunishRsn,a.Money,a.WageNo"	                  
	         +" from LARewardPunish a,labranchgroup b,laagent c where 1=1 and doneflag<>'4' and a.agentcode=c.agentcode "
	         +" and c.agentgroup=b.agentgroup "
	         +" and a.managecom like '"+fm.all("ManageCom").value+"%' "
	   + getWherePart('c.groupAgentCode','AgentCode')
	   + getWherePart('b.BranchAttr','BranchAttr')
	   + getWherePart('a.WageNo','WageNo') 
	   + getWherePart('a.BranchType','BranchType')
	   + getWherePart('a.BranchType2','BranchType2');	    

	if((fm.all("DoneFlag").value != null && fm.all("DoneFlag").value != '')
	 		&& (fm.all("DoneFlag1").value != null && fm.all("DoneFlag1").value!= ''))
	 		{
	 		strSQL+=" and a.doneflag in ('"+fm.all("DoneFlag").value+"','"+fm.all("DoneFlag1").value +"')";
	 	  }         
	         
	         
	    else 	if((fm.all("DoneFlag1").value != null && fm.all("DoneFlag1").value != ''))
	 		{
	 			strSQL+=" and a.doneflag ='"+fm.all("DoneFlag1").value+"'";
	 			}		
	 	
      else if((fm.all("DoneFlag").value != null && fm.all("DoneFlag").value != ''))
	 		{
	 			strSQL+=" and a.doneflag ='"+fm.all("DoneFlag").value+"'";
	 			}		

	 
           strSQL+=" order by a.modifydate ";

  // alert(strSQL);  
  fm.querySql.value = strSQL;
  
  
  var strSQLTitle = "select '代理人代码','销售单位','管理机构','加款原因','扣款原因','金额','调整年月'  from dual where 1=1 ";
  fm.querySqlTitle.value = strSQLTitle;
  
  //  alert(strSQLTitle);
  fm.all("Title").value="select '加扣款管理查询下载' from dual where 1=1 ";  

    fm.action = "../agentquery/LAHealthPrintTemplateSave.jsp";
    fm.submit();



  }
