<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LARewardPunishQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
   var mSql="1 and char(length(trim(comcode))) in (#4#,#8#) and comcode<>#86000000# ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAddBSubReview.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAddBSubReviewInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <title>奖惩查询</title>
</head>  
<body  onload="initForm();initElementtype();" >
  <form action="./LAAddBSubReviewSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
    <tr class=common>
     <td class=common>
     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLARewardPunish1);">
     <td class=titleImg>
      加扣款信息查询条件
     </td> 
     </td>
    </tr>
   </table>
   <Div  id= "divLARewardPunish1" style= "display: ''"> 
   <table  class= common>
    <tr  class= common> 
     <TD  class= title>
      管理机构
     </TD>
     <TD  class= input>
      <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>3"
       ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,mSql,1,1);"
       onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,mSql,1,1);"
      ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
     </TD> 
      <td  class= title> 
		  销售单位
		 </td>
     <td  class= input> 
		  <input class=common name=AgentGroup > 
		 </td>
    </tr>
    <tr  class= common> 
     <td  class= title> 
		  营销员代码
		 </td>
     <td  class= input> 
		  <input class= common name=GroupAgentCode OnChange="return checkvalid();"> 
		 </td>
		  <td class=title>
	        加款类型
	      </td>
	      <td  class= input>
          <input class=codeno name=DoneFlag verify="加款类型"
          ondblClick="showCodeList('bankmoneyj',[this,DoneFlagName],[0,1]);"
          onkeyup="showCodeListKey('bankmoneyj',[this,DoneFlagName],[0,1]);"
          ><input class=codename readonly  name=DoneFlagName > 
        </td>    
		  
    
    </tr>      
    <tr  class= common>
       <td class=title>
	        扣款类型
	      </td>
	      <td  class= input>
          <input class=codeno name=DoneFlag1 verify="扣款类型"
          ondblClick="showCodeList('bankmoneyk',[this,DoneFlag1Name],[0,1]);"
          onkeyup="showCodeListKey('bankmoneyk',[this,DoneFlag1Name],[0,1]);"
          ><input class=codename readonly  name=DoneFlag1Name > 
        </td>   
          
     <!--TD  class= title width="25%"> 
		  执行日期
		 </td>
     <TD  class= input  width="25%"  >
      <Input class= "coolDatePicker" dateFormat="short" name=DoneDate MaxLength=10 verify="执行日期|Date"   > 
	   </td-->
	    <TD  class= title width="25%"> 
		     调整年月
		    </td>
         <td class= input>
         <Input class= 'common'   name=WageNo verify="调整年月|len=6" >
         <font color="red">(yyyymm)
         </td>
	    </tr>      
    <tr  class= common>
     <td class=title>
      审批状态
     </td>
     <!-- 修改-->
     <td class=input><input name=SendGrp class=codeno
         CodeData="0|^01|提交|^02|审核通过|^03|驳回"
         ondblClick="showCodeListEx('DoneFlagList',[this,StateName],[0,1]);"
         onkeyup="showCodeListKeyEx('DoneFlagList',[this,StateName],[0,1]);"
     ><Input name=StateName class="codename">
     </td>   
     <TD class=title>员工类型</TD>
     <TD class=input><Input name=AgentType class='codeno'
			ondblclick="return showCodeList('agenttypecode',[this,AgentTypeName],[0,1]);"
			onkeyup="return showCodeListKey('agenttypecode',[this,AgentTypeName],[0,1]);"><Input
			class=codename name=AgentTypeName readOnly>
	</TD>
      
    </tr>
            
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <input type=hidden name=SendGrp1 value=''>          
          <input type=hidden class=Common name=querySql > 
          <input type=hidden class=Common name=querySqlTitle > 
          <input type=hidden class=Common name=Title >
          <input type=hidden name=AgentCode value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="下  载" class=cssbutton TYPE=button onclick="ListExecl();"> 
<!--          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> -->
          <INPUT VALUE="选择通过" class=cssbutton TYPE=button onclick="selSubmit();"> 
          <INPUT VALUE="全部通过" class=cssbutton TYPE=button onclick="allSubmit();"> 
          <INPUT VALUE="选择驳回" class=cssbutton TYPE=button onclick="selBack();"> 
          <INPUT VALUE="全部驳回" class=cssbutton TYPE=button onclick="allBack();"> 
          <input type=hidden name=fmAction value=''>
    <table>
    <tr>
     <td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRewardPunishGrid);">
     </td>
     <td class= titleImg>
    	加扣款信息结果
     </td>
     </tr>
    </table>
  	<Div  id= "divRewardPunishGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="SpanRewardPunishGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 					
    </div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
