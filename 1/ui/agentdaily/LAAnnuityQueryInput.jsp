<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAnnuityQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAnnuityQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>养老金查询 </title>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<script language="JavaScript">
  var tTitleAgent = "<%=tTitleAgent%>";
  var manageCom = "<%=tG.ManageCom%>";
  var msql=" 1 and branchtype=#"+"<%=BranchType%>"+"# and branchtype2=#"+"<%=BranchType2%>"+"#";
</script>
<body  onload="initForm();" >
  <!--form action="./LAAnnuityQuerySubmit.jsp" method=post name=fm target="fraSubmit"-->
  <form method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
  <table>
  	<tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAnnuity);">
      </td>
      <td class= titleImg>
        养老金查询条件
      </td>
    </tr>
  </table>
  <Div  id= "divLAAnnuity" style= "display: ''">
  <table  class= common>
      <TR  class= common>
        <TD class= title> 
          <%=tTitleAgent%>编码 
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCode >
        </TD>
        <TD class= title>
          管理机构
        </TD>
        <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
            ><Input name=ManageComName class="codename"> 
        </TD>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input class=common name=BranchAttr > 
        </TD>
      </TR>
      <TR class= common>
        <TD class= title>
          入司时间范围
        </TD>
        <TD class= input>
         <Input name=EmployDateF class="coolDatePicker" dateFormat="short">
        </TD>
        <TD class= title>
          ----
        </TD>
        <TD class= input>
          <Input name=EmployDateT class="coolDatePicker" dateFormat="short">
        </TD>
        <TD class= title>
          领取状态
        </TD>
        <TD  class= input>
            <Input class=codeno name=DrawMarker verify="领取标记|NOTNULL&DATE "
              CodeData="0|^0|未领取|^1|已领取" elementtype=nacessary 
              ondblClick="showCodeListEx('StateList',[this,DrawMarkerName],[0,1]);"
              onkeyup="showCodeListKeyEx('StateList',[this,DrawMarkerName],[0,1]);"
            ><Input name=DrawMarkerName class="codename">
        </TD>
        <!--TD class= title>
          职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="codeno" verify="职级|notnull&code:AgentGrade" 
            ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
            onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" elementtype=nacessary
          ><Input name=AgentGradeName class="codename">
        </TD-->
      </TR>
      <TR>
        <TD class= title>
          离司时间范围
        </TD>
        <TD class= input>
          <Input name=OutWorkDateF class="coolDatePicker" dateFormat="short">
        </TD>
        <TD class= title>
          ----
        </TD>
        <TD class= input>
          <Input name=OutWorkDateT class="coolDatePicker" dateFormat="short">
        </TD>
      </TR>
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <!--input name=testtxt value=''-->
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class="cssbutton" TYPE=button onclick="returnParent();"> 	
    </Div>      
          				
    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAnnuityGrid);">
    		</td>
    		<td class= titleImg>
    			 养老金结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAnnuityGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAnnuityGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
  	</div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
