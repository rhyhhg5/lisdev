
<%
//程序名称：RiskQuarterAssessRateInit.jsp
//程序功能：
//创建日期：2009-02-19
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">
  var tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and  GRADECODE like #G%# ";
  var rSql=" 1  and code<>#1201#  ";
</SCRIPT>
<script language="JavaScript">

function initInpBox()
{
  try
  {
      fm2.diskimporttype.value='LADiscount'; 
      fm2.BranchType.value='3'
      fm2.branchtype2.value='01';
//    alert(fm2.BranchType.value);
//    alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
    alert("在LARiskQuarterAssessRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

var RiskQuarterAssessRate1Grid;
function initRiskQuarterAssessRate1Grid() {                            
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    
    iArray[1]=new Array();
    iArray[1][0]="主键";         		//列名
    iArray[1][1]="0px";         		//宽度
   // iArray[1][3]=3;         		//宽度
   


    iArray[2]=new Array();                                                 
    iArray[2][0]="职级类别";         		//列名                             
    iArray[2][1]="0px";            		//列宽                             
    iArray[2][2]=200;            			//列最大值                           
    iArray[2][3]=2;                                                        
    iArray[2][4]="assessgrade2";                                           
    iArray[2][5]="2|3";              	                                     
    iArray[2][6]="0|1";                                                    
    iArray[2][9]="职级类别|code:assessgrade2";                             
    iArray[2][15]= "1";                                                    
    iArray[2][16]= StrSql;    
 //   iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
 //   iArray[2][14]='G5';         
    
    iArray[3]=new Array();
    iArray[3][0]="职级类别名称";         		//列名
    iArray[3][1]="0px";         		//宽度
    iArray[3][2]=200;
    iArray[3][3]=0;         		//最大长度
  //  iArray[3][14]='G5';    
     
    iArray[4]=new Array();
    iArray[4][0]="管理机构"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[4][4]="ComCode";
    iArray[4][5]="4|5";
    iArray[4][6]="0|1";
    iArray[4][9]="管理机构|NotNull";    
    iArray[4][15]="1";
    iArray[4][16]=tmanagecom;
                                      
    iArray[5]=new Array();
    iArray[5][0]="管理机构名称"; //列名
    iArray[5][1]="120px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许      

    iArray[6]=new Array();
    iArray[6][0]="险种编码";         		//列名
    iArray[6][1]="60px";         		//宽度
    iArray[6][2]=20;
    iArray[6][3]=2;         		//最大长度
    iArray[6][4]="RiskCode";              	        //是否引用代码:null||""为不引用
    iArray[6][5]="6|7"; ;
    iArray[6][6]="0|1";
    iArray[6][9]="险种编码|NotNull"; 
//    iArray[6][15]= "1";
//    iArray[6][16]=rSql;    
    iArray[6][19]=1;               //强制刷新                                                   
    
    iArray[7]=new Array();
    iArray[7][0]="险种名称";         		//列名
    iArray[7][1]="80px";         		//宽度
    iArray[7][2]=200;
    iArray[7][3]=0;         		//最大长度   
    
    iArray[8]=new Array();
    iArray[8][0]="保费类型"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[8][9]="保费类型|NotNull";
    iArray[8][10]="transtate";
    iArray[8][11]="0|^00|正常保费|^01|基本保费|^02|额外保费|^03|追加保费|^04|帐户保费|^05|风险保费";
        
    iArray[9]=new Array();
    iArray[9][0]="缴费方式"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[9][9]="缴费方式|NotNull";
    iArray[9][10]="PayintvCode";
    iArray[9][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";  
    
    iArray[10]=new Array();
    iArray[10][0]="保险期间"; //列名
    iArray[10][1]="60px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=2;              //是否允许输入,1表示允许,0表示不允许 
    iArray[10][4]="payyear"; 
    iArray[10][5]="10|11";  
    iArray[10][6]="0|1";  
    
    
    iArray[11]=new Array();
    iArray[11][0]="保险期间"; //列名
    iArray[11][1]="0px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="最低缴费年期";         		//列名
    iArray[12][1]="80px";         		//宽度
    iArray[12][2]=100;
    iArray[12][3]=1;         		//最大长度

    
    iArray[13]=new Array();
    iArray[13][0]="缴费年期小于";         		//列名
    iArray[13][1]="80px";         		//宽度
    iArray[13][2]=100;
    iArray[13][3]=1;         		//最大长度
   
    iArray[14]=new Array();
    iArray[14][0]="间接绩效提奖比例（小数）";         		//列名
    iArray[14][1]="140px";         		//宽度
    iArray[14][2]=200;
    iArray[14][3]=1;         		//最大长度

    iArray[15]=new Array();
    iArray[15][0]="保单年度";         		//列名
    iArray[15][1]="0px";         		//宽度
    iArray[15][2]=200;
    iArray[15][3]=0;         		//最大长度
    iArray[15][14]='1';  //保单年度默认为1
    
    RiskQuarterAssessRate1Grid = new MulLineEnter( "fm" , "RiskQuarterAssessRate1Grid" ); 
    //这些属性必须在loadMulLine前
    RiskQuarterAssessRate1Grid.canChk = 1;
    //QuarterAssessStandardGrid.mulLineCount = 0;   
    RiskQuarterAssessRate1Grid.displayTitle = 1;
    //QuarterAssessStandardGrid.hiddenPlus = 0;
    //QuarterAssessStandardGrid.hiddenSubtraction = 0;
    //QuarterAssessStandardGrid.canSel = 0;
    RiskQuarterAssessRate1Grid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("在RiskQuarterAssessRate1Init.jsp-->initRiskQuarterAssessRate1Grid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	  iArray[1][0]="导入文件批次";          		//列名
	  iArray[1][1]="120px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LARiskQuarterAssessRate1Init.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
	initInpBox()  
    initRiskQuarterAssessRate1Grid();
    initImportResultGrid();
  }
  catch(re)
  {
    alert("LARiskQuarterAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


</script>
