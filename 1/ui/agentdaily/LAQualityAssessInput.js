//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	if ( mOperate=="")
	{
		addClick();
	}
	if (!beforeSubmit())
    return false;
	  //alert(mOperate);
    //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   	//alert("请重新输入信息！");
   	//return false;
  //}
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	 mOperate="";
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

var strSQL_AddSum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" and MarkType='1'";
  var strQueryResult_AddSum=easyQueryVer3(strSQL_AddSum, 1, 1, 1);
  if (strQueryResult_AddSum)
  {
  var tArr_AddSum=decodeEasyQueryResult(strQueryResult_AddSum);
  fm.all('AddSum').value=tArr_AddSum[0][0];
  }
else
	{
		fm.all('AddSum').value="0";
	}
  var strSQL_SubSum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" and MarkType='0'";
  var strQueryResult_SubSum=easyQueryVer3(strSQL_SubSum, 1, 1, 1);
  if (strQueryResult_SubSum)
  {
  var tArr_SubSum=decodeEasyQueryResult(strQueryResult_SubSum);
  fm.all('SubSum').value=tArr_SubSum[0][0];
  }
  else
	{
		fm.all('SubSum').value="0";
	}
//var strSQL_Sum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" ";
//var strQueryResult_Sum=easyQueryVer3(strSQL_Sum, 1, 1, 1);
//if (strQueryResult_Sum)
//{
//var tArr_Sum=decodeEasyQueryResult(strQueryResult_Sum);
//fm.all('Sum').value=tArr_Sum[0][0];
//}
//else
//{
//	fm.all('Sum').value="0";
//}
fm.all('Sum').value=fm.all('AddSum').value-fm.all('SubSum').value;
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LARewardPunish.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if (!verifyInput())
  { 
    return false;       
  }  
//if (fm.all('AgentCode').value == '')
//{
//  alert("请输入业务员编码！");
//  fm.all('AgentCode').focus();
//  return false;	  
//}
//if (fm.all('DoneDate').value == '')
//{
//  alert("请输入执行日期!");
//  fm.all('DoneDate').focus();
//  return false;	  
//}
 //var  StrSql_Mark="select mark from laqualityassess where 1=1 "
            //  +"and Agentcode='"+fm.all('Agentcode').value+"'"
            //  +"and Marktype='"+fm.all('marktype').value+"'";
              
  	//var markQueryResult=easyQueryVer3( StrSql_Mark,1,1,1);
  	//if (markQueryResult)
    //{
    	//var arr1 = decodeEasyQueryResult(markQueryResult);
	   // var tmark = arr1[0][0];
      var StrSql_check="select count(*) from laqualityitemdef where downlimit<="+fm.all('Mark').value+" and uplimit>="+fm.all('Mark').value+" and itemcode='"+fm.all('MarkRsn').value+"'";           
    //  alert(StrSql_check);
      var checkQueryResult=easyQueryVer3( StrSql_check,1,0,1);
     
      if (!checkQueryResult)
      {
      	alert ("查询失败！");
      	return false;
      }
      
     var arr2 = decodeEasyQueryResult(checkQueryResult);
     var tcheck = arr2[0][0];
      //alert (tcheck);
     if (tcheck==0 ||tcheck == null ||tcheck=="" )
     {
     	 alert("加扣分超出规定范围！");
     	 return false;
     	}
   // }
    //else
   // {
    //alert("该记录不存在！");
   // return false;
   // }          
  return true;	
}           


//显示frmSubmit框架，用来调试


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码  
  if ((fm.all("GroupAgentCode").value==null)||(fm.all("GroupAgentCode").value==''))
    alert('请先重新输入营销源代码！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要修改的纪录！');
  else if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
   var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LAQualityAssessQueryInputHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  
}   
function queryClicksure()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  fm.submit();
  
}              

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	
  //下面增加相应的删除代码  
  if ((fm.all("GroupAgentCode").value==null)||(fm.all("GroupAgentCode").value==''))
    alert('请先查询出要删除的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要删除的纪录！');
  else if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function checkvalid()
{
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   if (getWherePart('GroupAgentCode')=='')
   {
   	return false;
   }
  //alert ("1");
  if (getWherePart('GroupAgentCode')!='')
  {
     strSQL = "select a.GroupAgentCode,a.managecom,a.name,a.branchcode from LAAgent a where 1=1 "
	     + getWherePart('a.groupAgentCode','GroupAgentCode')+" and ((AgentState  <'06') or (AgentState is null))"
	     + " and branchtype='"+tBranchType+"'"
	     + " and branchtype2='"+tBranchType2+"'" ;
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  }
 // alert("2");
  else
  {
    fm.all('GroupAgentCode').value ="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return false;
  }
  //alert("3");
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('GroupAgentCode').value="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return;
  }
  //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//
  fm.all('AgentCode').value = tArr[0][0];
  fm.all('Name').value = tArr[0][2];
 
  //</rem>######//
 // alert("5");
  fm.all('ManageCom').value  = tArr[0][1];
  
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][3];
  
  fm.all('HiddenAgentGroup').value=tArr[0][3];
 // alert("6");
  var strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"
     var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  //var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  //以备显示时使用
  //alert("7");
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  
  var strSQL_AddSum="select value(sum(Mark),0),ModifyDate,Operator from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" and MarkType='1' group by ModifyDate,Operator";
  var strQueryResult_AddSum=easyQueryVer3(strSQL_AddSum, 1, 1, 1);
  if (strQueryResult_AddSum)
  {
  var tArr_AddSum=decodeEasyQueryResult(strQueryResult_AddSum);
  fm.all('AddSum').value=tArr_AddSum[0][0];
  fm.all('ModifyDate').value=tArr_AddSum[0][1];
  fm.all('Operator').value=tArr_AddSum[0][2];
  }
else
	{
		fm.all('AddSum').value="0";
	}
  var strSQL_SubSum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" and MarkType='0'";
  var strQueryResult_SubSum=easyQueryVer3(strSQL_SubSum, 1, 1, 1);
  if (strQueryResult_SubSum)
  {
  var tArr_SubSum=decodeEasyQueryResult(strQueryResult_SubSum);
  fm.all('SubSum').value=tArr_SubSum[0][0];
  }
  else
	{
		fm.all('SubSum').value="0";
	}
 //var strSQL_Sum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" ";
//var strQueryResult_Sum=easyQueryVer3(strSQL_Sum, 1, 1, 1);
//if (strQueryResult_Sum)
//{
//var tArr_Sum=decodeEasyQueryResult(strQueryResult_Sum);
//fm.all('Sum').value=tArr_Sum[0][0];
//}
//else
//{
//	fm.all('Sum').value="0";
//}
fm.all('Sum').value=fm.all('AddSum').value-fm.all('SubSum').value;
}

function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  //alert(arrQueryResult);
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.all('GroupAgentCode').value = arrResult[0][0]; 
    fm.all('Name').value = arrResult[0][1];  
    fm.all('AgentGroup').value = arrResult[0][2];                                              
    fm.all('ManageCom').value = arrResult[0][3];                                              
    fm.all('MarkType').value = arrResult[0][4];
    fm.all('Mark').value=arrResult[0][5];   
   // fm.all('MarkContent').value = arrResult[0][6];                                             
    fm.all('MarkRsn').value = arrResult[0][7]; 
    fm.all('MarkRsn1').value = arrResult[0][8];                                           
    fm.all('DoneDate').value = arrResult[0][9];
    fm.all('Operator').value = arrResult[0][10];
    fm.all('Idx').value = arrResult[0][11];
    fm.all('ModifyDate').value = arrResult[0][12];
    
    var agentSQL = "SELECT AGENTCODE from laagent where  groupagentcode='"+arrResult[0][0]+"' ";
    var agentCodeResult=easyExecSql(agentSQL);
    
    fm.all('AgentCode').value = agentCodeResult[0][0];
     
    //<addcode>############################################################//
    fm.all('HiddenAgentGroup').value = arrResult[0][12];
    //</addcode>############################################################//
    fm.all('Noti').value = arrResult[0][13];
     var strSQL_AddSum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" and MarkType='1'";
  var strQueryResult_AddSum=easyQueryVer3(strSQL_AddSum, 1, 1, 1);
  if (strQueryResult_AddSum)
  {
  var tArr_AddSum=decodeEasyQueryResult(strQueryResult_AddSum);
  fm.all('AddSum').value=tArr_AddSum[0][0];
  }
else
	{
		fm.all('AddSum').value="0";
	}
  var strSQL_SubSum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" and MarkType='0'";
  var strQueryResult_SubSum=easyQueryVer3(strSQL_SubSum, 1, 1, 1);
  if (strQueryResult_SubSum)
  {
  var tArr_SubSum=decodeEasyQueryResult(strQueryResult_SubSum);
  fm.all('SubSum').value=tArr_SubSum[0][0];
  }
  else
	{
		fm.all('SubSum').value="0";
	}
 //var strSQL_Sum="select value(sum(Mark),0) from LAQualityAssess where 1=1 "+getWherePart('AgentCode','AgentCode')+" ";
//var strQueryResult_Sum=easyQueryVer3(strSQL_Sum, 1, 1, 1);
//if (strQueryResult_Sum)
//{
//var tArr_Sum=decodeEasyQueryResult(strQueryResult_Sum);
//fm.all('Sum').value=tArr_Sum[0][0];
//}
//else
//{
//	fm.all('Sum').value="0";
//}
fm.all('Sum').value=fm.all('AddSum').value-fm.all('SubSum').value;
                                                                                                                                                                                                                                                 	
  }
     
}