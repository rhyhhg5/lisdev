<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期： 
//创建人   
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");  
 
%>
<SCRIPT>
	var StrSql = "  1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode) and exists (select 1 from ldriskwrap where riskcode=lmrisk.riskcode and riskwrapcode = #WR0003#)";
  var AgentSql = " 1 and branchtype=#"+ <%=BranchType%> +"# and branchtype2=#01#";

</SCRIPT>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LABankContInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LABankContInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LABankContSave.jsp" method=post name=fm target="fraSubmit">
	<table class="common" align=center>
		<tr align=right>
			<td class=button>
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return addClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
				<td class=button width="10%">
				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="QueryClick();">
			</td>
		</tr>
	</table>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 银邮协议信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLACont1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      协议编码
    </TD>
    <TD  class= input>
      <Input class='readonly' readonly name=ProtocolNo >
    </TD>
    <TD  class= title>
      签约时间
    </TD>
    <TD  class= input>
       <Input name=SignDate class="coolDatePicker" dateFormat="short" verify="签约时间|NotNull&Date" elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      签署管理机构
    </TD>
    <TD  class= input>
            <Input class="codeno" name=ManageCom verify="签署管理机构|code:comcode&NOTNULL&len>=4" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
          </TD>
    <TD  class= title>
      签署银邮机构
    </TD>
    <TD  class= input>
       <Input class= 'codeno' name=AgentCom verify="签署银邮机构|NOTNULL" 
       ondblclick="return getAgentCom(this,AgentName);"
       onkeyup="return getAgentCom(this,AgentName);" ><Input class=codename name=AgentName readOnly elementtype=nacessary>
    </TD>
  </TR>
   <TR  class= common>
    <TD  class= title>
      保险公司代表人
    </TD>
    <TD  class= input>
     <Input class= 'common' name=RepresentA >
    </TD>
    <TD  class= title>
     银邮机构代表人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RepresentB >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
     协议起期
    </TD>
    <TD  class= input>
      <Input name=StartDate class="coolDatePicker" dateFormat="short" verify="协议起期|NotNull&Date" elementtype=nacessary>
    </TD>
    <TD  class= title>
     协议止期
    </TD>
    <TD  class= input>
      <Input name=EndDate class="coolDatePicker" dateFormat="short" verify="协议止期|NotNull&Date" elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common> 
    <TD  class= title>
     协议类型
    </TD>
    <TD  class= input>
          <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('AgentComType',[this,BranchType2Name],[0,1]);"
             onkeyup="return showCodeList('AgentComType',[this,BranchType2Name],[0,1]);"
              verify="协议类型|code:AgentComType&NOTNULL"  
          ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
    </TD>
    
   </TR>
   <!--tr class=common>		
		<td class=title>合同类型</td>
		<td class=input><input name=LAContType class=code name="LAContType" verify="合同类型|NOTNULL"
         CodeData="0|^PI|专业代理公司|^PC|兼业代理人|^PN|经纪公司"
         ondblClick="showCodeListEx('DoneFlagList',[this],[0,1]);"
         onkeyup="showCodeListKeyEx('DoneFlagList',[this],[0,1]);"></td>
      </tr-->
</table>

  <!-- 手续费比例信息（列表） -->
  	<Div  id= "divCont" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden name="CalType" value='51'>
    <input type=hidden name="ACType" value=''>
    <input type=hidden name=BranchType value=''>
    <!--input type=hidden name=BranchType2 value=''-->
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
