<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddSubSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAMeAddReduceUI tLAMeAddReduceUI = new LAMeAddReduceUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
;

  //加款详细信息
  LARewardPunishSchema tLARewardPunishSchema   = new LARewardPunishSchema();
  tLARewardPunishSchema.setAgentCode(request.getParameter("AgentCode"));
  tLARewardPunishSchema.setBranchAttr(request.getParameter("BranchAttr"));
  tLARewardPunishSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLARewardPunishSchema.setBranchType(request.getParameter("BranchType"));
  tLARewardPunishSchema.setBranchType2(request.getParameter("BranchType2"));
  tLARewardPunishSchema.setManageCom(request.getParameter("ManageCom"));
  
  tLARewardPunishSchema.setDoneFlag("4"); // 标识为4
  tLARewardPunishSchema.setAClass(request.getParameter("Aclass"));//标识各种加扣款类型
  tLARewardPunishSchema.setMoney(request.getParameter("Money"));
  tLARewardPunishSchema.setWageNo(request.getParameter("WageNo"));
  tLARewardPunishSchema.setNoti(request.getParameter("Noti"));
  
  System.out.println("传进来的操作符为:"+tOperate);
  if(tOperate.equals("UPDATE||MAIN")||tOperate.equals("DELETE||MAIN"))
  {
	  tLARewardPunishSchema.setAgentCode(request.getParameter("hideAgentCode"));
	  tLARewardPunishSchema.setIdx(request.getParameter("hideIdx"));
	  System.out.println("无语！！！！"+tLARewardPunishSchema.getIdx());
	  tLARewardPunishSchema.setAClass(request.getParameter("hideAclass"));
  }
 
  
  // 准备传输数据 VData
  VData tVData = new VData();
    FlagStr="";
	tVData.add(tG);
	tVData.add(tOperate);
	tVData.addElement(tLARewardPunishSchema);
	
  try
  {
	  tLAMeAddReduceUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAMeAddReduceUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
