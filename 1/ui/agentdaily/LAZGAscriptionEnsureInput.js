 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
 
	if(!verifyInput())
 {
 	return false;
 }	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	fm.reset;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    if (fm.action == './LAZGAscriptionEnsureSave.jsp')     
      saveClick=true;
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  
	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证字段的值
function checkagentcode()
{
 var tReturn = parseManageComLimitlike();
 strSQL = "select * from LAAgent where  InsideFlag<>'0'  "
          +tReturn
	  + getWherePart('groupagentcode','AgentCode')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');   
  
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("没有此代理人！");
    fm.all('AgentCode').value="";
    fm.all('Name').value="";
    return ;
  } 
  
  var ttArr = new Array();
  ttArr = decodeEasyQueryResult(strQueryResult);
  fm.all('Name').value =ttArr[0][5]; 
}
function checkagentname()
{
 var tReturn = parseManageComLimitlike();
 strSQL = "select * from LAAgent where  InsideFlag<>'0'  "
          +tReturn
	  + getWherePart('Name')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');   
  
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("没有此代理人！");
    fm.all('AgentCode').value="";
    fm.all('Name').value="";
    return ;
  } 
  

}

function agentConfirm()
{
	
	if(!verifyInput())
 {
 	return false;
 }	
	
  // if((fm.all('AgentCode').value=="")||(fm.all('AgentCode').value==null)) 
 //  {
 //   alert("请输入原代理人编码！");	
 //   fm.all('AgentCode').focus();
 //   return ;
 //  }
   
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tAgentCode = fm.all('AgentCode').value;
  
  initAscriptionGrid(); 
  var tReturn = parseManageComLimitlike();
 //       if((fm.all('AgentCode').value!="")&&(fm.all('AgentCode').value!=null))
 //       {
 //       	//alert(1);
 //      
 //      //离职人员、特殊组人员、内勤人员
 //     strSQL = "select * from LAAgent where (AgentState >='03' or InsideFlag='0' or agentgroup in "
 //              +" (select b.agentgroup from labranchgroup b where   b.AscripState='2')) "
 //              +tReturn
	//   + getWherePart('AgentCode');
	//      
 //      
 //      var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
 //      
 //      //判断是否查询成功
 //      if (!strQueryResult) {
 //        alert("该代理人的保单不需要归属！");
 //        fm.all('AgentCode').value="";
 //        fm.all('Name').value="";
 //        return ;
 //      } 
 //      
 //      var ttArr = new Array();
 //      ttArr = decodeEasyQueryResult(strQueryResult);
 //      fm.all('Name').value =ttArr[0][5]; 
 //       }       
  
//  strSQL = "select * from LCPol where mainpolno=polno and agentCode = '"+fm.all('AgentCode').value+"'";			 

  strSQL=" select distinct a.AscripNo,c.managecom,a.GrpContNo,a.ContNo,getUniteCode(a.AgentOld),c.name,getUniteCode(a.AgentNew),"
        +" (select name from laagent dd where dd.agentcode=a.agentnew),case when a.maketype='01' then '手工归属'  when a.maketype='02' then '系统自动归属'  when a.maketype='02' then '手工归属' end  "
        +" from laascription a,laagent c "
        +" where c.agentcode=a.agentold and  a.AscripState='2' and a.validflag='N'"
        +" and MakeType='"+fm.all('MakeType').value+"'"//+" and a.contno=d.contno"
  //     +tReturn
	  + getWherePart('c.groupagentcode','AgentCode')
	  + getWherePart('c.ManageCom','ManageCom','like')
	  + getWherePart('c.name','Name')
	  + getWherePart('a.contno','ContNo')
	  + getWherePart('a.grpcontno','GrpContNo')
	  ;
	     
  
  turnPage.queryModal(strSQL, AscriptionGrid); 
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败");
    return ;
    }
  
//查询成功则拆分字符串，返回二维数组
//arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
//turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3,4]);
////设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
//turnPage.pageDisplayGrid = AscriptionGrid;              
////保存SQL语句
//turnPage.strQuerySql     = strSQL;   
////设置查询起始位置
//turnPage.pageIndex       = 0;    
////在查询结果数组中取出符合页面显示大小设置的数组
////arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
//var tArr = new Array();
//tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
////调用MULTILINE对象显示查询结果  
//displayMultiline(tArr, turnPage.pageDisplayGrid);

}   
function submitSave()
{  
	//alert("111");   
 /*   if((fm.all('NewAgentCode').value=="")||(fm.all('NewAgentCode').value==null)) 
   {
    alert("请输入现代理人编码！");	
    fm.all('NewAgentCode').focus();
    return ;
   }
 */ 
//var tempObj = fm.all('AscriptionGridNo'); //假设在表单fm中
//if (tempObj == null)
//{
//   alert('无数据保存！');
//   return ;
//}
   var lineCount = AscriptionGrid.mulLineCount;
   var str='';
   var tAgentCode = fm.all("AgentCode").value;
 //  var tNewAgentCode = fm.all('NewAgentCode').value;
 //  if (tNewAgentCode == tAgentCode)
 //    	{
 //    		alert("不能归属给原代理人！");
 //    		return ;
 //    	}
     	//查询数据库，判断所输代理人是否存在
/*     	var strSQL = "";
        strSQL = "select * from LAAgent where 1=1 "
	         +"and AgentCode ='"+tNewAgentCode+"' and AgentState < '03' ";
        var strResult = easyQueryVer3(strSQL, 1, 1, 1);
        //alert(strSQL);
        //判断是否查询成功
        if (!strResult) 
        {        	
          alert("代理人"+tNewAgentCode+"不存在！");
     //     fm.all('NewAgentCode').value="";
          return ;
        }   
 */       
      fm.action="./LAZGAscriptionEnsureSave.jsp";
      submitForm();
     return ;
}
function submitSaveb()
{
	fm.all("Flag").value="SELECT";
	//Boolean tSel="false";
        tSel=false;
//
	//	var tAscripNo[] = AscriptionGrid1;
	var lineCount = AscriptionGrid.mulLineCount;
	for( i=0;i<lineCount;i++)
	{
	 tSel = AscriptionGrid.getChkNo(i);
	 if(tSel==true)
	 i=lineCount;
	//	alert(AscriptionGrid.getChkNo(0));
        }
	if( tSel == false || tSel == null )
		//top.close();
		alert( "请先选择一条记录!" );
	else
	{
				
	submitSave();
	}
}
function submitSaveall()
{
//	var arrSelected = null;
	fm.all("Flag").value="ALL";

	submitSave();
}

function clearMulLine()
{  
   AscriptionGrid.clearData("AscriptionGrid");
   saveClick=false;
}