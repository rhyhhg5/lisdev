<%
//程序名称：LAAddressInput.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:07:04
//创建人  ：ctrHTML
//更新人  ：  
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LAAnnuityFlagQuery.js"></SCRIPT> 
  <%@include file="LAAnnuityFlagQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();initElementtype();" >
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg >
        		 养老金缴纳标记信息查询
       		 </td>   		 
    	</tr>
    </table>
<table  class= common align='center'>
   <TR  class= common>
   <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
          </TD> 
    <TD  class= title>
      养老金缴纳标志
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name= Flag  
      ondblclick="return showCodeList('yesno',[this,FlagName],[0,1]);"
      onkeyup="return showCodeListKey('yesbi',[this,FlagName],[0,1]);"
      ><Input class=codename name=FlagName>
    </TD>
  </TR>
    <TR  class= common>
    <TD  class= title>
      销售人员代码
    </TD>
    <TD  class= input>
      <Input class= 'common'  name=GroupAgentCode>
    </TD>
   
    <TD  class= title>
      销售人员姓名
    </TD>
    <TD  class= input>
     <Input class= 'common'    name=AgentName >
    </TD>
   
  </TR>
  <TR  class= common>
    <TD  class= title>
      销售单位代码
    </TD>
    <TD  class= input>
     <Input class= 'common'   name=BranchAttr  verify="销售单位代码|len=10|len=12|len=14">
    </TD>                      
    <TD  class= title>
      销售单位名称
    </TD>
    <TD  class= input>
     <Input class= 'common'   name=BranchName    >
    </TD>         
  </TR>
 
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=button width="10%" align=left>
        <INPUT VALUE="查 询" TYPE=button class=cssbutton onclick="easyQueryClick();"> 
        <INPUT VALUE="下 载" TYPE=button class=cssbutton onclick="ListExecl();">
        <INPUT VALUE="返 回" TYPE=button class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAddress1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLAAddress1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAAddressGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    

 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <INPUT VALUE="" TYPE=hidden name=AgentGroup>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="fmtransact" name="BranchType" value="">
  <input type=hidden id="fmtransact" name="BranchType2" value="">
  <input type=hidden class=Common name=querySql > 
  <input type=hidden class=Common name=querySqlTitle > 
  <input type=hidden class=Common name=Title >
  <input type=hidden calss=Common name=AgentCode>
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
