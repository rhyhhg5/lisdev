<%
//程序名称：LAAscripQueryInit.jsp
//程序功能：功能描述
//创建日期：　
//创建人  　
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ManageCom').value = "";
    fm.all('AgentCode').value = "";
    fm.all('AgentName').value = "";
    fm.all('ContNo').value = "";
    fm.all('MakeDate').value = "";
    fm.all('BranchAttr').value = "";
        }
  catch(ex) {
    alert("LAAscripQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLACrossGrid();  
  }
  catch(re) {
    alert("LAAscripQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LACrossGrid;
function initLACrossGrid() {                               
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="团单号";         		//列名
    iArray[1][1]="150px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="个单号";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="投保人名称";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;    
    
    iArray[4]=new Array();
    iArray[4][0]="原业务员姓名";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;    
    
    iArray[5]=new Array();
    iArray[5][0]="原业务员代码";         		//列名
    iArray[5][1]="120px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0; 
      
    iArray[6]=new Array();
    iArray[6][0]="原销售单位代码";         		//列名
    iArray[6][1]="100px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0; 
    
    iArray[7]=new Array();
    iArray[7][0]="现业务员姓名";         		//列名
    iArray[7][1]="100px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0; 
     
    iArray[8]=new Array();
    iArray[8][0]="现业务员代码";         		//列名
    iArray[8][1]="120px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;    
    
    iArray[9]=new Array();
    iArray[9][0]="现销售单位代码";         		//列名
    iArray[9][1]="100px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;    
    
    iArray[10]=new Array();
    iArray[10][0]="分配操作日期";         		//列名
    iArray[10][1]="80px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;
    
    iArray[11]=new Array();
    iArray[11][0]="分配状态";         		//列名
    iArray[11][1]="100px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;
    
    iArray[12]=new Array();
    iArray[12][0]="分配方式";         		//列名
    iArray[12][1]="100px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;
    
    iArray[13]=new Array();
    iArray[13][0]="客户地址";         		//列名
    iArray[13][1]="100px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;
    
    
    iArray[14]=new Array();
    iArray[14][0]="客户电话";         		//列名
    iArray[14][1]="100px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0;
    
    LACrossGrid = new MulLineEnter( "fm" , "LACrossGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACrossGrid.mulLineCount = 0;   
    LACrossGrid.displayTitle = 1;
    LACrossGrid.hiddenPlus = 1;
    LACrossGrid.hiddenSubtraction = 1;
    LACrossGrid.canSel = 0;
    LACrossGrid.canChk = 0;
    LACrossGrid.selBoxEventFuncName = "showOne";
  
    LACrossGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACrossGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
</script>
