var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

// 页面查询功能
function rqueryClick()
{	
   if (verifyInput()==false) return false; // 页面简单校验
   fm.all('hideOperate').value='Query';
   initBranchGroupGrid();
   var strSQL = "";
   var strSQL = "select b.groupagentcode,c.branchattr,a.managecom,a.CBPTaxes,a.PCBPTaxes,a.ITBPTaxes,a.wageno from lataxadjust a,laagent b,labranchgroup c where 1=1 and a.agentcode=b.agentcode and b.agentgroup=c.agentgroup "  
			+ getWherePart('a.ManageCom','ManageCom','like')
			+ getWherePart('b.GroupAgentCode','GroupAgentCode')
			+ getWherePart('c.Branchattr','Branchattr')
			+ getWherePart('a.WageNo','WageNo');	
	var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	if(!strQueryResult)
	{  
		alert("没有符合条件的信息！"); 
		turnPage.queryModal(strSQL, BranchGroupGrid);
		return false;
	}
	else turnPage.queryModal(strSQL, BranchGroupGrid);   	
}

//返回操作
function returnClick()
{
    var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{					
			try
			{		
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();		
	}
}
// 查询返回数据信息
function getQueryResult()
{
   	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	if( tRow == 0 || tRow == null) return arrSelected;
	arrSelected = new Array();
	// 书写SQL语句
	var strSQL = "";
	var strSQL = "select b.groupagentcode,b.name,c.branchattr,a.managecom,a.CBPTaxes,a.PCBPTaxes,a.ITBPTaxes,a.wageno,a.operator,a.modifydate,a.agentcode,a.BranchType,a.BranchType2 from lataxadjust a,laagent b,labranchgroup c " ; 
	strSQL +=" where  a.agentcode=b.agentcode and b.agentgroup=c.agentgroup and b.groupagentcode='"+BranchGroupGrid.getRowColData(tRow-1,1)+"' and a.WageNo='"+BranchGroupGrid.getRowColData(tRow-1,7)+"' and a.managecom='"+BranchGroupGrid.getRowColData(tRow-1,3)+"' ";     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
		
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
	  alert("查询失败！");
	  return false;
    }
//查询成功则拆分字符串，返回二维数组
    arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);	
	return arrSelected;	
}
function ListExecl()
{
	if(fm.all('hideOperate').value!='Query') 
	{	
		alert("请先点击查询！");
		return false;
	}
	if(verifyInput() == false ) return false; 
	var tsql="";
	//定义查询的数据
	var strSQL = "";
	var strSQL = "select b.groupagentcode,c.branchattr,a.managecom,a.CBPTaxes,a.PCBPTaxes,a.ITBPTaxes,a.wageno from lataxadjust a,laagent b,labranchgroup c where 1=1 and a.agentcode=b.agentcode and b.agentgroup=c.agentgroup "  
		+ getWherePart('a.ManageCom','ManageCom','like')
		+ getWherePart('b.GroupAgentCode','GroupAgentCode')
		+ getWherePart('c.Branchattr','Branchattr')
		+ getWherePart('a.WageNo','WageNo');	    
	fm.querySql.value = strSQL;
	
	//定义列名
	var strSQLTitle = "select '销售人员代码','销售单位 ','管理机构',' 总公司奖励应纳税项','省公司奖励应纳税项',' 中支公司奖励应纳税项','执行薪资月'  from dual where 1=1 ";
	fm.querySqlTitle.value = strSQLTitle;
	  
	//定义表名
	fm.all("Title").value="select '应纳税项 查询下载' from dual where 1=1  ";    
	fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
	fm.submit();

}
