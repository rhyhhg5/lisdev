<%
//程序名称：LAAscriptionInput.jsp
//程序功能：
//创建日期：2004-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
  fm.all('Name').value ='';
  fm.all('BranchType').value = '<%=BranchType%>';   
  fm.all('BranchType2').value = '<%=BranchType2%>';   
  }
  catch(ex)
  {
    alert("在LAAscriptionInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();    
    initAscriptionGrid();    
  }
  catch(re)
  {
    alert("在LAAscriptionInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
// 担保人信息的初始化
function initAscriptionGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="主险保单号码";          		        //列名
      iArray[1][1]="140px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="险种";         		        //列名
      iArray[2][1]="80px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保险起期";      	   		//列名
      iArray[3][1]="70px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保险止期";      	   		//列名
      iArray[4][1]="70px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="保费";      	   		//列名
      iArray[5][1]="70px";            			//列宽
      iArray[5][2]=10;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="保额";      	   		//列名
      iArray[6][1]="70px";            			//列宽
      iArray[6][2]=10;            			//列最大值
      iArray[6][3]=0;           			//是否允许输入,1表示允许，0表示不允许
      
      

      
      AscriptionGrid = new MulLineEnter( "fm" , "AscriptionGrid" ); 
      //这些属性必须在loadMulLine前
      AscriptionGrid.mulLineCount = 10;   
    AscriptionGrid.displayTitle = 1;
    AscriptionGrid.hiddenPlus = 1;
    AscriptionGrid.hiddenSubtraction = 1;
    AscriptionGrid.loadMulLine(iArray);
    AscriptionGrid.canChk =1; //复选框   
      }
      catch(ex)
      {
        alert(ex);
      }
}
}
</script>