<%
//程序名称：LABranchGroupQuery.js
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
//    fm.all('AgentCode').value = '';
//    fm.all('AgentGroup').value = '';
 //   fm.all('ManageCom').value = '';    
    fm.all('DoneFlag').value = '';    
    fm.all('DoneFlagName').value = ''; 
    fm.all('DoneFlag1').value = '';
    fm.all('DoneFlag1Name').value = '';
    fm.all('SendGrp').value = '';  
    fm.all('StateName').value = '';  
    fm.all('AgentType').value = '';  
    fm.all('AgentTypeName').value = '';   
//    fm.all('WageNo').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';

  }
  catch(ex)
  {
    alert("在LAAddBSubQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                                

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();   
    initRewardPunishGrid();
  }
  catch(re)
  {
    alert("在LAAddBSubQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化RewardPunishGrid
 ************************************************************
 */
function initRewardPunishGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理人代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="销售单位";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="管理机构";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="顺序号";         //列名
        iArray[4][1]="0px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="加扣款类型";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="加款原因";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="扣款原因";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="金额";         //列名
        iArray[8][1]="60px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="调整年月";         //列名
        iArray[9][1]="70px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
        
        iArray[10]=new Array();
        iArray[10][0]="执行日期";         //列名
        iArray[10][1]="0px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[11]=new Array();
        iArray[11][0]="审批状态";         //列名
        iArray[11][1]="60px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[12]=new Array();
        iArray[12][0]="员工类型";         //列名
        iArray[12][1]="60px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[13]=new Array();
        iArray[13][0]="idx";         //列名
        iArray[13][1]="0px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
 
        RewardPunishGrid = new MulLineEnter( "fm" , "RewardPunishGrid" ); 

        //这些属性必须在loadMulLine前
        RewardPunishGrid.mulLineCount = 0;   
        RewardPunishGrid.displayTitle = 1;
        RewardPunishGrid.hiddenPlus = 1;
        RewardPunishGrid.hiddenSubtraction = 1;
        RewardPunishGrid.locked=1;
        RewardPunishGrid.canSel=1;
        RewardPunishGrid.canChk=1;
        RewardPunishGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化RewardPunishGrid时出错："+ ex);
      }
    }


</script>