<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAQualificationSave.jsp
//程序功能：
//创建日期：2005-03-21 15:48:13
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LAQualificationSchema tLAQualificationSchema   = new LAQualificationSchema();
  OLAQualificationUI tOLAQualificationUI   = new OLAQualificationUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tQualifNoQuery=request.getParameter("QualifNoQuery");
  String tAgentCodeQuery= request.getParameter("AgentCodeQuery");
  String AgentCode = request.getParameter("AgentCode");
  if(transact.equals("UPDATE||MAIN"))
  {
	  tLAQualificationSchema.setAgentCode(request.getParameter("AgentCodeQuery"));  
  }
  else
  { 
	  tLAQualificationSchema.setAgentCode(request.getParameter("AgentCode"));
  }
    tLAQualificationSchema.setQualifNo(request.getParameter("QualifNo").trim());
    tLAQualificationSchema.setGrantUnit(request.getParameter("GrantUnit"));
    tLAQualificationSchema.setGrantDate(request.getParameter("GrantDate"));
    tLAQualificationSchema.setInvalidDate(request.getParameter("InvalidDate"));
    tLAQualificationSchema.setInvalidRsn(request.getParameter("InvalidRsn"));
    tLAQualificationSchema.setValidStart(request.getParameter("ValidStart"));
    tLAQualificationSchema.setValidEnd(request.getParameter("ValidEnd"));
    tLAQualificationSchema.setState(request.getParameter("QualifState"));
    tLAQualificationSchema.setPasExamDate(request.getParameter("PasExamDate"));
    tLAQualificationSchema.setExamYear(request.getParameter("ExamYear"));
    tLAQualificationSchema.setExamTimes(request.getParameter("ExamTimes"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tQualifNoQuery);
	tVData.add(tLAQualificationSchema);
  	tVData.add(tG);
  	

    tOLAQualificationUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLAQualificationUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
