<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAscriptionSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>


<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAscriptionSchema tLAAscriptionSchema= new LAAscriptionSchema();
  LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
  LABankAscripEnsureUI tLABankAscripEnsureUI  = new LABankAscripEnsureUI();
 
  //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Succ";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tFlag= request.getParameter("Flag");
  String tagentold= request.getParameter("AgentCode"); 
  String tmanagecom= request.getParameter("ManageCom"); 
  String tname= request.getParameter("name");
  String tbranchtype= request.getParameter("BranchType");
  String tbranchtype2= request.getParameter("BranchType2");
  String tMakeType = request.getParameter("MakeType");
  System.out.println(tbranchtype+" "+tbranchtype2);
  VData tVData = new VData();
  String tConttype="";//保单类型：1--个单  2--团单
    //取得授权信息
  if(tFlag.equals("SELECT"))
  {
  System.out.println("begin Ascription schema : "+tFlag);
  String tChk[] = request.getParameterValues("InpAscriptionGridChk");
  int lineCount = 0;
  int number = 0; 
  for(int index=0;index<tChk.length;index++)
  {
     if(tChk[index].equals("1"))           
     number++;
  }
  System.out.println("number"+number);    
  if(number==0)
	{
    	Content = " 失败，原因:没有选择要调整的员工！";
   		FlagStr = "Fail";		
	} 
  else
	{
	 String tAscripNo[] = request.getParameterValues("AscriptionGrid1");
   String tContNo[]=request.getParameterValues("AscriptionGrid3");
   String tAgentOld[]=request.getParameterValues("AscriptionGrid4");
   String tAgentNew[]=request.getParameterValues("AscriptionGrid7");
   String tAgentComNew[]=request.getParameterValues("AscriptionGrid9");
   String tAgentComOld[]=request.getParameterValues("AscriptionGrid6");
          	 
	 lineCount = tChk.length; //行数
	 System.out.println("length= "+String.valueOf(lineCount));
	
	 for(int i=0;i<lineCount;i++)
	 {
	   if(tChk[i].trim().equals("1"))
	   {
	    tLAAscriptionSchema = new LAAscriptionSchema();
	    System.out.println("Chk:"+tChk[i]);
	    tLAAscriptionSchema.setAscripNo(tAscripNo[i]);  
	    tLAAscriptionSchema.setAgentOld(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+tAgentOld[i]+"'"));
	    tConttype=new ExeSQL().getOneValue("select conttype from lccont where grpcontno='00000000000000000000' and contno='"+tContNo[i]+"' union select conttype from lccont where grpcontno<>'00000000000000000000' and grpcontno='"+tContNo[i]+"'");
        System.out.println("保单类型:["+tConttype+"]");
	      	if(tConttype.equals("1"))
	      	{
	      	 tLAAscriptionSchema.setContNo(tContNo[i]);  
	      	 System.out.println("保单:["+tContNo[i]+"]");
	      	}
	      	else if(tConttype.equals("2"))
	      	{
	      	 tLAAscriptionSchema.setGrpContNo(tContNo[i]);
	      	 System.out.println("保单:["+tContNo[i]+"]");
	      	}
        tLAAscriptionSchema.setAgentNew(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+tAgentNew[i]+"'"));
        tLAAscriptionSchema.setAgentComNew(tAgentComNew[i]);
        tLAAscriptionSchema.setAgentComOld(tAgentComOld[i]);
        tLAAscriptionSchema.setBranchType(tbranchtype);
        tLAAscriptionSchema.setBranchType2(tbranchtype2);
        tLAAscriptionSet.add(tLAAscriptionSchema);
	   }
	   System.out.println("i:"+tChk[i]);
	  } 
	  System.out.println("end 归属信息...");
	}
  }	  
  else if(tFlag.equals("ALL"))
  {
       String oldAgentcode = request.getParameter("AgentCode");
       String tManageCom = request.getParameter("ManageCom");
       System.out.println("管理机构:["+tManageCom+"]");
       String tcontNo = request.getParameter("ContNo");
       tLAAscriptionSchema.setAgentOld(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+oldAgentcode+"'"));
       tLAAscriptionSchema.setManageCom(tManageCom);
        System.out.println("管理机构apple:["+tLAAscriptionSchema.getManageCom()+"]");
         if(!tcontNo.equals("")&&tcontNo!=null)
       {
        tConttype=new ExeSQL().getOneValue("select conttype from lccont where grpcontno='00000000000000000000' and contno='"+tcontNo+"' union select conttype from lccont where grpcontno<>'00000000000000000000' and grpcontno='"+tcontNo+"'");
        System.out.println("保单类型:["+tConttype+"]");
	      	if(tConttype.equals("1"))
	      	{
	      	 tLAAscriptionSchema.setContNo(tcontNo);  
	      	 System.out.println("保单:["+tcontNo+"]");
	      	}
	      	else if(tConttype.equals("2"))
	      	{
	      	 tLAAscriptionSchema.setGrpContNo(tcontNo);
	      	 System.out.println("保单:["+tcontNo+"]");
	      	}
	      }
	      else
	      {
	       tLAAscriptionSchema.setContNo(tcontNo);  
	      }
       System.out.println("All Start"+oldAgentcode+"^^"+tManageCom+"***"+tcontNo);
       System.out.println("All Start");
 }
  if(!FlagStr.equals("Fail"))
  {
	// 准备传输数据 VData
    FlagStr="";
    //tLAAscriptionSchema = new LAAscriptionSchema();
    if(tFlag.equals("SELECT")){
	    tVData.addElement(tLAAscriptionSet);
	    tVData.add(tG);
	    tVData.add(tFlag);
	    tVData.add(tMakeType);
	    tVData.add(tConttype);
	    System.out.println("add over");
    }else if(tFlag.equals("ALL")){
    	tVData.add(tLAAscriptionSchema);
  	    tVData.add(tG);
  	    tVData.add(tFlag);
  	    tVData.add(tMakeType);
  	    //if(!tConttype.equals("")&& tConttype!=null){
  	        tVData.add(tConttype);
  	   // }
  	    System.out.println("all over");
    }
    try
    {
      tLABankAscripEnsureUI.submitData(tVData,tOperate);
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
   }   
   if (!FlagStr.equals("Fail"))
   {
     tError = tLABankAscripEnsureUI.mErrors;
     if (!tError.needDealError())
     {
     	Content = " 保存成功! ";
     	FlagStr = "Succ";
     }
     else
     {
     	Content = " 保存失败，原因是:" + tError.getFirstError();
     	FlagStr = "Fail";
     }
   }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

