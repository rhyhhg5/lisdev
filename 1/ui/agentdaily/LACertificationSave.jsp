<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LACertificationSave.jsp
//程序功能：
//创建日期：2005-03-20 18:07:04
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LACertificationSchema tLACertificationSchema   = new LACertificationSchema();
  OLACertificationUI tOLACertificationUI   = new OLACertificationUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  
  //根据集团统一工号查询业务员编码
	String agentcode = "";
	if(request.getParameter("GroupAgentCode")!=null&&!"".equals(request.getParameter("GroupAgentCode"))){
	agentcode = new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("GroupAgentCode")+"'");
	}
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    String tCertifNoQuery=request.getParameter("CertifNoQuery");
    tLACertificationSchema.setAgentCode(agentcode);
    tLACertificationSchema.setCertifNo(request.getParameter("CertifNo").trim());
    tLACertificationSchema.setAuthorUnit(request.getParameter("AuthorUnit"));
    tLACertificationSchema.setGrantDate(request.getParameter("GrantDate"));
    tLACertificationSchema.setInvalidDate(request.getParameter("InvalidDate"));
    tLACertificationSchema.setInvalidRsn(request.getParameter("InvalidRsn"));
    tLACertificationSchema.setValidStart(request.getParameter("StartValidDate"));
    tLACertificationSchema.setValidEnd(request.getParameter("EndValidDate"));
    tLACertificationSchema.setState(request.getParameter("CertiState"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	
  	tVData.add(tCertifNoQuery);
	tVData.add(tLACertificationSchema);
  	tVData.add(tG);
    tOLACertificationUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLACertificationUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
