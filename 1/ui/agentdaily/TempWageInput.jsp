<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：TempWageInput.jsp
//程序功能：佣金零时计算
//创建时间：2008-07-07
//创建人  ：songgh

%>
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="TempWageInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <SCRIPT src="TempWage.js"></SCRIPT>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./TempWageSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
    	
    	
    	 <TD  class= title>
            管理机构
         </TD>
   <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
   </TD> 
   </tr>
   <TR class=common> 	            
          <TD  class= title>
           起始年月
          </TD>
          <TD  class= input>
             <Input class=common name=wageno></TD>  
          </TD>    
                			
        <TD  class= title>
          终止年月
          </TD>
          <TD  class= input>
             <Input class=common name=wagenoend></TD>  
          </TD>  
   </tr>  
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">     		
    		<input type=button value="计  算" class=cssButton onclick="return DoInsert();">    		
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 

    	</td>
   
    </tr>      
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
   
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
  <Input type=hidden id="BranchType" name=BranchType value=''>

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




