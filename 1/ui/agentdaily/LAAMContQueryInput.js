/** 
 * 程序名称：LAContInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-20 18:05:58
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick()
{
	if (document.fm.SpareMonths.value.length>0){
	  if(!isNumeric(document.fm.SpareMonths.value))
	  {
		  alert('剩余有效月份内请填写数字!');
		  return false;
	  }
	}
	var tSysDate = document.fm.SysDate.value;
	//此处书写SQL语句
	var tReturn = parseManageComLimitlike();
	var strSql = "select a.ProtocolNo ,a.SignDate,a.ManageCom,a.AgentCom,a.RepresentA,a.RepresentB,a.StartDate,a.EndDate,a.noti "
	+" from LACont a  "
	+" where  a.AgentCom in (select b.AgentCom from LACom  b where b.ACType in (select code from ldcode where codetype='actype' and code<>'01') and branchtype='1' and branchtype2 in ('02','04') )" + tReturn
    + getWherePart("ProtocolNo", "ProtocolNo","like")
    + getWherePart("SignDate", "SignDate")
    + getWherePart("ManageCom", "ManageCom","like")
    + getWherePart("AgentCom", "AgentCom")
    + getWherePart("RepresentA", "RepresentA","like")
    + getWherePart("RepresentB", "RepresentB","like")
    + getWherePart("StartDate", "StartDate")
    + getWherePart("EndDate", "EndDate");
  if (document.fm.SpareMonths.value.length>0)
  {
    strSql += " and date('"+tSysDate+"')+"+document.fm.SpareMonths.value+" months >= a.enddate and date('"+tSysDate+"') < a.enddate";
  }
	turnPage.queryModal(strSql, LAContGrid);
 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
}
function showOne(parm1, parm2) {
  //判断该行是否确实被选中
  //alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
	
   var arrReturn = new Array();
	var tSel = LAContGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
				
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LAContGrid.getSelNo();
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LAContGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
