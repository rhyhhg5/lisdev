//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//修改人：解青青  时间：2014-11-23
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  1=1" 
             + getWherePart("groupagentcode","GroupAgentCode")
//   		"groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
    
}





/*****************************
  * 注: 提交前的校验、计算  
  ****************************/
function beforeSubmit()
{
  //添加操作	
}           
/*****************************
  * 注:显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示 
  ****************************/
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

/******************************************
 * 注: 查询得到返回值
 ******************************************/
function getQueryResult()
{
  var arrSelected = null;
  tRow = RewardPunishGrid.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet == null )
    return arrSelected;
  arrSelected = new Array();

  var GroupAgentCode=RewardPunishGrid.getRowColData(tRow-1,1);
  var aclass=RewardPunishGrid.getRowColData(tRow-1,9);
  var idx = RewardPunishGrid.getRowColData(tRow-1,10);
 var strSQL = "select getUniteCode(a.agentcode)," 
       +" a.name,"
       +" b.branchattr,"
       +"(select name from labranchgroup where branchtype = '2' and branchtype2 = '02' and agentgroup =b.agentgroup),"
       +" a.managecom,"
       +" (select agentgrade from latree where agentcode = a.agentcode),"
       +" b.aclass,"     
       +" b.money,"
       +" b.wageno,"
       +" b.noti,b.idx,b.agentgroup, "
       +" a.agentcode,a.agenttype"
       +"  from laagent a, LAREWARDPUNISH b  where a.agentcode = b.agentcode and b.doneflag = '4' and a.branchtype = '2' and a.branchtype2 = '02'"
  +" and  a.GroupAgentCode = '"+trim(GroupAgentCode)+"'"
  +" and b.aclass = '"+trim(aclass)+"' "
  +" and b.idx = "+trim(idx)+" ";

  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  arrSelected = decodeEasyQueryResult(strQueryResult);
  return arrSelected;
}

/******************************************
 * 注: 返回主界面
 ******************************************/
function returnParent()
{
  var arrReturn = new Array();
  var tSel = RewardPunishGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
    try
    {	
      arrReturn = getQueryResult();
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
		
  }
}

/******************************************
 * 注: 查询操作
 ******************************************/
function easyQueryClick()
{
     // 提前校验
	 if (!verifyInput()) return false ;
	 
     // 初始化表格
     initRewardPunishGrid();

  // 书写SQL语句
   var strSQL = "select getUniteCode(a.agentcode)," 
       +" a.name,"
       +" b.branchattr,"
       +" a.managecom,"
       +" (select codename from ldcode where codetype = 'grpmoney' and code = b.aclass),"       
       +" b.noti,"
       +" b.money,"
       +" b.wageno,b.aclass,b.idx"
       +"  from laagent a, LAREWARDPUNISH b  where a.agentcode = b.agentcode and b.doneflag = '4' and a.branchtype= '2' and a.branchtype2 = '02'"
       + getWherePart('b.ManageCom','ManageCom')
       + getWherePart('b.branchattr','BranchAttr')
       + getWherePart('b.AgentCode','AgentCode')
       + getWherePart('a.Name','Name')
       + getWherePart('b.Aclass','Aclass')
       + getWherePart('b.WageNo','WageNo')
       
      turnPage.queryModal(strSQL, RewardPunishGrid);  
       
	
}