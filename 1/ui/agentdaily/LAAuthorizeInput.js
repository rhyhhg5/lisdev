 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
 if(!verifyInput())
 {
 	return false;
}
if(!AuthorizeGrid.checkValue())
{
	return false;
}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  
	 
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证字段的值
function agentConfirm()
{
	var tReturn = parseManageComLimitlike();
   if((fm.all('GroupAgentCode').value=="")||(fm.all('GroupAgentCode').value==null)) 
   {
    alert("目的代理人编码不能为空！");	
    fm.all('AgentCode').focus();
    return false;
   } 
   var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+fm.all('GroupAgentCode').value+"' ";
   var strQueryResult = easyExecSql(tYAgentCodeSQL);
   if(strQueryResult == null){
   	alert("获取业务员工号失败！");
   	return false;
   }
   fm.all('AgentCode').value = strQueryResult[0][0];
   var strSQL = "";
   var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
   
   strSQL = "select * from LAAgent where 1=1 "
	  + getWherePart('AgentCode')+" and ((AgentState not like '03') or (AgentState is null))"
	  + tReturn
	//  + " and branchtype2='"+tBranchType2+"'"
	  + " and branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"'";
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  //判断是否查询成功
  if (!strQueryResult) {
    alert("该代理人不存在！");
    fm.all('AgentCode').value="";
    return false;
  }   
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult);
  	fm.all('AgentName').value = arrDataSet[0][5];
  }
  AuthorizeGrid.unLock();
  strSQL = "select distinct riskcode,(select lmrisk.riskname from lmrisk where lmrisk.riskcode=LAAuthorize.riskcode),authorstartdate,authorenddate "
           +" from LAAuthorize where 1=1 and LAAuthorize.AuthorObj = '"+fm.all('AgentCode').value+"'";			 
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) 
  {
    //alert("无纪录！");
    return false;
  }
  
//查询成功则拆分字符串，返回二维数组
  //arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,9,29,36,40,43]);
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AuthorizeGrid;              
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;   
  //设置查询起始位置
  turnPage.pageIndex       = 0;    
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果  
  displayMultiline(tArr, turnPage.pageDisplayGrid);
   /*  
   fm.action="./LAAuthorizeConfirm.jsp";
   submitForm();
   */
   //fm.submit();
   //fm.action="./LAAuthorizeSave.jsp";
 
}   
function submitSave()
{     
  AuthorizeGrid.delBlankLine ("AuthorizeGrid");
  var tempObj = fm.all('AuthorizeGridNo'); //假设在表单fm中
  if (tempObj == null)
  {
     alert('请录入授权记录！');
     return false;
  }
  var lineCount = 0;
  lineCount = AuthorizeGrid.mulLineCount; 
  if (lineCount == 0)
  {
     alert('请录入授权记录！');
     return false;
  }
  if (lineCount > 0)
  {
    //alert("lineCount:"+lineCount);    
    var tRC=''; 
    for(var i=0;i<=lineCount-1;i++)
    {
    	tRC=AuthorizeGrid.getRowColData(i,1);
    	if ((tRC==null)||(tRC==''))
       	{
       		alert('险种不能为空！');
       		return false;
       	}
       	
       	for(var j=i+1;j<lineCount;j++)
       	{
       	   //alert("compare:"+tRC+"?"+AuthorizeGrid.getRowColData(j,1));
       	   if (tRC==AuthorizeGrid.getRowColData(j,1))
       	   {
       	      alert('一种险种不能授权两次以上！');
       	      return false;	
       	   }
       	}  
       	var tRC1='';      	
    	tRC1=AuthorizeGrid.getRowColData(i,3);
    	if ((tRC1==null)||(tRC1==''))
       	{
       		alert('取消销售资格起期不能为空！');
       		return false;
       	}
       	var tRC2=''; 
       	tRC2=AuthorizeGrid.getRowColData(i,4);
    	if ((tRC2==null)||(tRC2==''))
       	{
       		alert('取消销售资格止期不能为空！');
       		return false;
       	}
       	if(tRC2<tRC1)
       	{
       		
       	alert('取消销售资格止期应该大于取消销售资格起期！');
       		return false;
       	}	
       	
    }	
  }  	 
    
   fm.action="./LAAuthorizeSave.jsp";
   submitForm();
   return true;
}

function clearMulLine()
{
   AuthorizeGrid.clearData("AuthorizeGrid");
   AuthorizeGrid.lock();   
   //alert("chag");
}
