 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if (mOperate=="")
  {
  	addClick();
  }
	//alert(11);
	if(!beforeSubmit())
 {
 	return false;
 	}
 	//alert(13);
 	fm.all("hideOperate").value=mOperate;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{       initLGAppealGrid();
	var tCaseNo=fm.all("CaseNo").value;
	
	fm.reset();
	fm.all("CaseNo").value=tCaseNo;
	
	// document.all.AgentOld.readOnly=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    if (fm.action == './LACaseInfoSave.jsp')     
      saveClick=true;
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

}
 
 
 	

	

 	
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
 // alert(123);

   if ((fm.all('CaseDate').value == '')||(fm.all('CaseDate').value == null))
  {
  	alert("请员输入立案日期 ！");
  	fm.all('CaseDate').focus();
  	return false;
  }
 //  alert(123);
  if ((fm.all('AgentPutCase').value == '')||(fm.all('AgentPutCase').value == null))
  {
  	alert("请输入立案人代码！");
  	fm.all('AgentPutCase').focus();
  	return false;
  }
 //  alert(123);
	if ((fm.all('CaseHappenDate').value == '')||(fm.all('CaseHappenDate').value == null))
  {
  	alert("请输入案发日期 ！");
  	fm.all('CaseHappenDate').focus();
  	return false;
  }
  if( fm.all('CaseHappenDate').value>fm.all('CaseDate').value)
  {
   alert("立案日期应大于案发日期!");	
   return false;
  }
  if(!verifyInput())
  {
  return false;
  	
  }	
   var arrReturn = new Array();
	var tSel = LGAppealGrid.getSelNo();
	
	
		
       if( tSel == 0 || tSel == null )
		//top.close();
       {
          alert( "请先查询出投诉信息并选择一条记录，再进行此操作。" );
	
            return false ;
        }
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				
				fm.all('AppealNo').value= arrReturn[0][10];
			}
			catch(ex)
			{
				alert( "查询投诉信息出错。" + ex );
				return false;
			}
		
		
	}
 var tState=fm.all("State").value;
  var tEndCaseManUnit=fm.all("EndCaseManUnit").value;
 var tAgentEndCase=fm.all("AgentEndCase").value; 
 var tAgentEndTel=fm.all("AgentEndTel").value;
 var tDealMind=fm.all("DealMind").value;   
 var tFlag=true;
 
 if(tState!='1')
 {
   if(tEndCaseManUnit==null ||tEndCaseManUnit=='')
   {
    tFlag=true;
   }
   else	
   {
     alert( "案件处理状态为未处理,不能录入结案人单位"  );
     return false;   		
   }
   if(tAgentEndCase==null ||tAgentEndCase=='')
   {
    tFlag=true;
   }
   else	
   {
     alert( "案件处理状态为未处理,不能录入结案人"  );
     return false;   		
   }   	
   if(tAgentEndTel==null ||tAgentEndTel=='')
   {
    tFlag=true;
   }
   else	
   {
     alert( "案件处理状态为未处理,不能录入结案人电话"  );
     return false;   		
   }   	   	
   if(tDealMind==null ||tDealMind=='')
   {
    tFlag=true;
   }
   else	
   {
     alert( "案件处理状态为未处理,不能录入处理意见"  );
     return false;   		
   }   	   	    	   
   	
  }

 	if(document.fm.DirectLoss.value != "" || document.fm.DirectLoss.value != null)
 	{
 		if(!isNumeric(document.fm.DirectLoss.value))
 		{
 			alert("直接损失录入内容不正确，请录入一个数字！");
 			return false;
 		}
 	}
 	
 	if(document.fm.ReplevyLoss.value != "" || document.fm.ReplevyLoss.value != null)
 	{
 		if(!isNumeric(document.fm.ReplevyLoss.value))
 		{
 			alert("直接损失录入内容不正确，请录入一个数字！");
 			return false;
 		}
 	}
  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if ((fm.all("CaseNo").value==null)||(fm.all("CaseNo").value==''))
	{
    alert('请先查询出要修改的纪录！');
    return false;
}
 
   if(!beforeSubmit())
 {
 	return false;
 	}
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	 //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./ALACaseQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
 
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function easyQueryClick()
{
  initLGAppealGrid();	    
	var strSql = "select AppealObjNo,AppealObjName,AppealObjPhone, AppealObjDep,AppealWayNo,AppealContent,LawMan,LawManTel,LawManAddress,State,AppealNo,DealNo from LGAppeal where 1=1 and AppealKind='11'"
    + getWherePart("AppealWayNo", "AppealWayNo")
    + getWherePart("AppealObjNo", "AppealObjNo")
    + getWherePart("AppealObjName", "AppealObjName")
    + getWherePart("AppealObjPhone", "AppealObjPhone")
    + getWherePart("AppealObjDep", "AppealObjDep")
    + getWherePart("LawMan", "LawMan")
    + getWherePart("LawManTel", "LawManTel")
    + getWherePart("LawManAddress", "LawManAddress")
    + getWherePart("State", "State1")
  ;
  //alert(strSql);
 //turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1); 		
	turnPage.queryModal(strSql, LGAppealGrid);
	 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }	
	
	
	
}

function getQueryResult()
{
	
	var arrSelected = null;
	tRow = LGAppealGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	
	arrSelected[0] = LGAppealGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}  
function checkAgentPutCase()
{
  var sql="select * from laagent where 1=1 and AgentState <='03'"
     + getWherePart("AgentCode", "AgentPutCase");
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("查询该业务人员失败！");
    fm.all('AgentPutCase').value="";
    return ;
  } 
}
function checkState()
{
	alert(1);
 fm.all('AgentEndCase').readOnly=true;
 fm.all('EndCaseManUnit').readOnly=true;
 fm.all('AgentEndTel').readOnly=true;
	
}
function checkAgentEndCase()
{
	var sql="select * from laagent where 1=1 and AgentState <='03'"
     + getWherePart("AgentCode", "AgentEndCase")
     + getWherePart("BranchType", "BranchType")
     +getWherePart("BranchType2", "BranchType2");
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("查询该业务人员失败！");
    fm.all('AgentEndCase').value="";
    return ;
  } 
}


function afterQuery(arrQueryResult)
{	
	
	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		
		arrResult = arrQueryResult;
		
                fm.all('AgentPutCase').value = arrResult[0][0];
                //fm.all('AgentCode').readonly=true;
                //document.getElementById("myid").readonly=true;
  //           var strSql_name = "select  name from laagent where 1=1"
  //                              + getWherePart("AgentCode", "AgentPutCase");
  //               var strQueryResult = easyQueryVer3(strSql_name, 1, 1, 1);  
  //              var tArr = new Array();
 //               tArr = decodeEasyQueryResult(strQueryResult);
                               
 //               fm.all('Name').value = tArr[0][0];
                fm.all('CaseDate').value = arrResult[0][1];
                fm.all('CaseHappenDate').value = arrResult[0][2];
                fm.all('State').value = arrResult[0][3];
                var strSql_name = "select  codename from ldcode where codetype='casestate' "
                              + getWherePart("code", "State");
                var strQueryResult = easyQueryVer3(strSql_name, 1, 1, 1);  
                var tArr = new Array();
                tArr = decodeEasyQueryResult(strQueryResult);
                               
                fm.all('StateName').value = tArr[0][0];
                fm.all('AgentEndCase').value = arrResult[0][4];
             
                fm.all('DirectLoss').value = arrResult[0][5];
                fm.all('ReplevyLoss').value = arrResult[0][6];
//                fm.all('DeDuctCent').value = arrResult[0][7];
                fm.all('CaseNo').value = arrResult[0][8];
                fm.all('AppealNo').value = arrResult[0][9];
                  fm.all('EndCaseManUnit').value = arrResult[0][10];
                  fm.all('DealMind').value = arrResult[0][12];
                    fm.all('AgentEndTel').value = arrResult[0][11];
              initLGAppealGrid();	  
                
                var tAppealNo=fm.all('AppealNo').value;
     //  	alert(1+tAgentNew);
       var strSql = "select AppealObjNo,AppealObjName,AppealObjPhone, AppealObjDep,AppealWayNo,AppealContent,LawMan,LawManTel,LawManAddress,State,AppealNo,DealNo from LGAppeal where 1=1 and AppealKind='11' and AppealNo='"+tAppealNo+"'";
   
       turnPage.queryModal(strSql, LGAppealGrid);
	 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }	
 	}
}

function changeAgent()
{
  
	 var strSQL="";		 
      var sql="select name from laagent where 1=1 and AgentState <='03'"
     + getWherePart("AgentCode", "AgentPutCase")
     + getWherePart("BranchType", "BranchType")
     +getWherePart("BranchType2", "BranchType2");
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("查询该业务人员失败！");
    fm.all('AgentPutCase').value="";
    return ;
  } 
  
    var arrResult = decodeEasyQueryResult(strQueryResult);
   fm.all('Name').value=arrResult[0][0];
}
   
