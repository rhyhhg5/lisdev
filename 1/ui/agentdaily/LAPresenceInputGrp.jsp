<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAPresenceInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAPresenceInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAPresenceInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LAPresenceSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPresence1);">
    <td class=titleImg>
      dsfasdf差勤信息
    </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPresence1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode onchange="return checkValid();">
          </TD>
          <TD  class= title>
            代理人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            销售机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AgentGroup>
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ManageCom>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            纪录顺序号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Idx >
          </TD>
          <TD  class= title>
            类别
          </TD>
          <TD  class= input>
            <Input class='code' name=AClass verify="类别|notnull" maxlength=1 ondblclick="return showCodeList('presenceaclass',[this]);" onkeyup="return showCodeListKey('presenceaclass',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            考勤执行次数
          </TD>
          <TD  class= input>
            <Input class= common name=Times >
          </TD>
          <!--TD  class= title>
            金额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=SumMoney >
          </TD-->
          <TD  class= title>
            批注
          </TD>
          <TD  class= input>
            <Input class= common name=Noti >
          </TD>
        </TR>
        <TR  class= common><TD  class= title>
            执行日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=DoneDate dateFormat='short' >
          </TD>
         
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Operator >
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=BranchType value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
