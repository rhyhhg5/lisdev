<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddSubSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数

  LARewardPunishSet tLARewardPunishSet   = new LARewardPunishSet();
  LAAddSubPerUI tLAAddSubPerUI           = new LAAddSubPerUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");
  String mWageNo=request.getParameter("WageNo");
  System.out.println("begin LAWage schema..."+mWageNo);
  String mDoneDate =mWageNo.substring(0,4)+"-"+mWageNo.substring(4)+"-01";
   System.out.println("begin LAWage schema..."+mDoneDate);
  String mManageCom  = request.getParameter("ManageCom");
  String mFlag  = request.getParameter("Flag");
  String mFlag1  = request.getParameter("Flag1");
  //如果加款有录入
  LARewardPunishSchema tLARewardPunishSchema   = new LARewardPunishSchema();
  tLARewardPunishSchema.setAgentCode(request.getParameter("AgentCode"));
  tLARewardPunishSchema.setIdx(request.getParameter("Idx"));
  tLARewardPunishSchema.setBranchAttr(request.getParameter("AgentGroup"));
  tLARewardPunishSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
  tLARewardPunishSchema.setBranchType(request.getParameter("BranchType"));
  tLARewardPunishSchema.setBranchType2(request.getParameter("BranchType2"));
  tLARewardPunishSchema.setManageCom(request.getParameter("ManageCom"));
  
  tLARewardPunishSchema.setAClass(request.getParameter("AClass"));
  tLARewardPunishSchema.setMoney(request.getParameter("Money"));
  tLARewardPunishSchema.setDoneFlag(request.getParameter("DoneFlag"));
  tLARewardPunishSchema.setAwardTitle(request.getParameter("PunishRsn"));//加款原因
  tLARewardPunishSchema.setWageNo(request.getParameter("WageNo"));
  tLARewardPunishSchema.setSendGrp(request.getParameter("SendGrp"));
  tLARewardPunishSchema.setNoti(request.getParameter("Noti"));
  tLARewardPunishSchema.setDoneDate(mDoneDate);
  tLARewardPunishSchema.setOperator(tG.Operator);
  tLARewardPunishSet.add(tLARewardPunishSchema);
 

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.add(mDoneDate);
	tVData.add(mManageCom);
	tVData.addElement(tLARewardPunishSet);
	
  try
  {
    tLAAddSubPerUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAddSubPerUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
