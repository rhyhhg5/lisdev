<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddManageInput.jsp
//程序功能：
//创建日期：2008-09-24 15:12:44
//创建人  ：CrtHtml程序创建 Alse
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String ManageCom=tG.ManageCom;
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
     <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAAddManageInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAddManageInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>加扣款审核管理 </title>
</head>
<%
  //加扣款类别
  
  String tDoneFlag1 = "";
  String tDoneFlag2 = "";
  String tTitleAgent = "";
  if ("1".equals(BranchType))
  {
    tDoneFlag1 = "0|^1|附加佣金加款|^2|同业衔接加款|^3|员工制加款|^5|其他加款";
    tDoneFlag2 = "0|^1|附加佣金扣款|^2|同业衔接扣款|^3|员工制扣款|^5|其他扣款";
    tTitleAgent = "营销员";
  } 
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAddManageSave.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAWage1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAWage1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
        <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','char(length(trim(comcode)))',1);" 
             onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,'8','char(length(trim(comcode)))',1);"
             ><Input class=codename name=ManageComName readOnly elementtype=nacessary>  
          </TD>
         
         <TD  class= title>
            销售单位代码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentGroup >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            营销员代码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode>
          </TD>
          
          <TD  class= title>
            营销员姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>

         </TR>
        
       
        <TR  class= common>
       
       <td class=title>
	        调整类型
	      </td>
	      <td class=input>
		      <input name=AClass class=codeno  verify="调整类型"
           ondblclick="return showCodeList('approject',[this,AClassName],[0,1]);" 
           onkeyup="return showCodeListKey('approject',[this,AClassName],[0,1]);"
          ><Input class=codename name=AClassName readOnly  > 
        </td>	
	      <td class=title>
	        调整项目
	      </td>
	      <td  class= input>
          <input class=codeno name=DoneFlag verify="调整项目"
          ondblclick="return showCodeList('reputype',[this,DoneFlagName],[0,1]);" 
          onkeyup="return showCodeListKey('reputype',[this,DoneFlagName],[0,1]);"
          ><input class=codename readonly  name=DoneFlagName > 
        </td>    
        </TR>
         <TR>
       <TD  class= title width="25%"> 
		     调整年月
		    </td>
         <td class= input>
         <Input class= 'common'   name=WageNo verify="调整年月|len=6" >
         <font color="red">(yyyymm)
         </td>
         <td class=title>
      流转状态
     </td>
     <td class=input><input name=SendOne class=codeno
         CodeData="0|^00|待审批|^11|审批同意|^12|审批不同意"
         ondblClick="showCodeListEx('SendOne',[this,SendOneName],[0,1]);"
         onkeyup="showCodeListKeyEx('SendOne',[this,SendOneName],[0,1]);"
     ><Input name=SendOneName class="codename">
     </td>  
          </TR>
      </table>
          <INPUT VALUE="查  询" class=cssbutton name=Query TYPE=button onclick="easyQueryClick();"> 				
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
    <Div  id= "divAGroupGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanWageGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页"  class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页"  class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页"  class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页"  class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 		
      <p>
        <INPUT VALUE="同意"  class=cssbutton name=Agree TYPE=button  onclick="submitAgree();"> 
        <INPUT VALUE="不同意"  class=cssbutton name=DisAgree TYPE=button onclick="submitDisAgree();"> 
        <input type=hidden name=BranchType value='<%=BranchType%>'>
        <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
        <input type=hidden name=Flag value=''>
         <input type=hidden name=LogManagecom value='<%=ManageCom%>'>
      </p>			
    </div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
