//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
     
  if (fm.all('hideOperate').value!="UPDATE||MAIN"&&(fm.all('hideOperate').value!="DELETE||MAIN"))
  {  
      fm.all('hideOperate').value="INSERT||MAIN";
  }
  
  if(fm.all('hideOperate').value=="INSERT||MAIN")
  {
     if(fm.all('hideAgentCode').value!=null&&fm.all('hideAgentCode').value!='')
      {
          alert("查询出来的信息,请使用修改功能");
          return false;
      }
      var tWageNo = fm.all('WageNo').value;
      if(trim(tWageNo)<'201206')
      {
          alert("加扣款功能页面录入最小执行年月为201206,烦请核实相关信息！");
          return false;
      }
      if(tWageNo.substr(4,6)>12||tWageNo.substr(4,6)<01){
          alert("执行年月不正确");
          return false;
      }
      var tGroupAgentCode = fm.all('GroupAgentCode').value;
	  if(tGroupAgentCode==null||tGroupAgentCode=='')
	  {
	     alert("请先录入业务员编码");
	     return false;
	  }
	  var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+tGroupAgentCode+"' ";
      var strQueryResult = easyExecSql(tYAgentCodeSQL);
      if(strQueryResult == null){
    	  alert("获取业务员工号失败！");
    	  return false;
      }
      fm.all('AgentCode').value = strQueryResult[0][0];
  }
  
  if (!verifyInput()) return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.submit(); //提交
  

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
   
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    initInpBox();
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LARewardPunishQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

/*********************************************************
  * 注:通过业务员编码查询 相关信息
  * 涉及内容包括:业务员姓名、销售单位、团队名称、管理机构、业务员职级
  ********************************************************/
  function checkvalid()
  {
    var tGroupAgentCode = fm.all('GroupAgentCode').value;
    if(tGroupAgentCode==null||tGroupAgentCode=='')
    {
       alert("请先录入业务员编码");
       return false;
    }
    var check_sql = "select a.name,b.branchattr,b.name,a.managecom,(select agentgrade from latree where agentcode =a.agentcode),b.agentgroup,a.agentcode,a.agenttype from laagent a,labranchgroup b where 1=1 and a.agentgroup= b.agentgroup and a.branchtype = '2' and a.branchtype2= '01'"
               + getWherePart('a.groupagentcode','GroupAgentCode');
               
    var strQueryResult = easyQueryVer3(check_sql, 1, 1, 1);
    if(!strQueryResult)
    {
       alert("人员编码录入有误！");
       return false;
    }else
    {
         var tArr = new Array();
         tArr = decodeEasyQueryResult(strQueryResult);
         fm.all('Name').value=tArr[0][0];
         fm.all('BranchAttr').value=tArr[0][1];
         fm.all('BranchName').value=tArr[0][2];
         fm.all('ManageCom').value=tArr[0][3];
         fm.all('AgentGrade').value=tArr[0][4];
         fm.all('AgentGroup').value=tArr[0][5];
         fm.all('AgentCode').value=tArr[0][6];
         fm.all('AgentType').value=tArr[0][7];
    }
  }
 
 /*********************************************************
  * 注: 执行修改操作
  *
  **********************************************************/
  function updateClick()
  {
    var hiddenAgentCode = fm.all('hideAgentCode').value;
    if(hiddenAgentCode==null||hiddenAgentCode=='')
    {
       alert("新录入的信息,不能使用修改功能");
       return false;
    }
    var tHiddenAclass = fm.all('hideAclass').value;
    var tAclass = fm.all('Aclass').value;
    if(tAclass!=tHiddenAclass)
    {
      alert("不能修改加扣款类型字段");
     fm.all('Aclass').value=tHiddenAclass;
	 var aclaSQL = "select codename from ldcode where codetype = 'grpmoney' and code = '"+tHiddenAclass+"'";
	 var strQueryResult = easyQueryVer3(aclaSQL, 1, 1, 1);
     var tArr = new Array();
     tArr = decodeEasyQueryResult(strQueryResult);
     fm.all('AclassName').value=tArr[0][0];
	 
//      switch(fm.all('Aclass').value)
//       {
//          case '01':
//              fm.all('AclassName').value="提奖加款"
//          break
//          case '02':
//              fm.all('AclassName').value="其他加款"
//          break
//          case '11':
//             fm.all('AclassName').value="提奖扣款"
//          break
//          case '12':
//             fm.all('AclassName').value="其他扣款"
//         break
//        default:
//      }
      return false;
    }
    fm.all('hideOperate').value='UPDATE||MAIN';
    submitForm();
  }
  
 /*********************************************************
  * 注: 执行删除功能
  *
  **********************************************************/
  function deleteClick()
  {
    var hiddenAgentCode = fm.all('hideAgentCode').value;
    if(hiddenAgentCode==null||hiddenAgentCode=='')
    {
       alert("新录入的信息,不能使用删除功能");
       return false;
    }
    fm.all('hideOperate').value='DELETE||MAIN';
    submitForm();
  }
 /*********************************************************
  * 注: 执行查询功能
  *
  **********************************************************/  
  function queryClick()
  {
    var tBranchType=fm.all('BranchType').value;
    var tBranchType2=fm.all('BranchType2').value;
    showInfo=window.open("./LAAddReduceQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  }
   /*********************************************************
  * 注: 接受返回数据
  *
  **********************************************************/ 
  function afterQuery(arrQueryResult){
	var arr = new Array();
	if( arrQueryResult != null )
	{
	   arr = arrQueryResult;
	   fm.all('GroupAgentCode').value= arr[0][0];
	   fm.all('hideAgentCode').value = arr[0][12];
	   fm.all('Name').value= arr[0][1];
	   fm.all('BranchAttr').value=arr[0][2] ;
	   fm.all('BranchName').value=arr[0][3] ;
	   fm.all('ManageCom').value=arr[0][4] ;
	   fm.all('AgentGrade').value=arr[0][5] ;
	   fm.all('Aclass').value=arr[0][6] ;
	   fm.all('hideAclass').value=arr[0][6] ;
	   var tClass = fm.all('Aclass').value;
	   var tclaSQL = "select codename from ldcode where codetype = 'grpmoney' and code = '"+tClass+"'";
	   var strQueryResult = easyQueryVer3(tclaSQL, 1, 1, 1);
	   if(!strQueryResult)
	   {
	      alert("加扣款类型出错");
	      return false;
	   }else
	   {
	        var tArr = new Array();
	        tArr = decodeEasyQueryResult(strQueryResult);
	        fm.all('AclassName').value=tArr[0][0];
	   }
//	   switch(tClass)
//       {
//          case '01':
//              fm.all('AclassName').value="提奖加款"
//          break
//          case '02':
//              fm.all('AclassName').value="其他加款"
//          break
//          case '11':
//             fm.all('AclassName').value="提奖扣款"
//          break
//          case '12':
//             fm.all('AclassName').value="其他扣款"
//         break
//        default:
//      }
    
	   
	   fm.all('Money').value= arr[0][7];
	   fm.all('WageNo').value=arr[0][8] ;
	   fm.all('Noti').value=arr[0][9] ;
	   fm.all('hideIdx').value= arr[0][10];
	   fm.all('AgentGroup').value = arr[0][11];
	   fm.all('AgentType').value = arr[0][13];
	   
	   // 返回置一些元素为只读
	   document.getElementById('GroupAgentCode').readOnly=true;
	   document.getElementById('WageNo').readOnly=true;
	 }	 
	}
  function getAgentType(Aclass,AclassName)
  {
	    var tGroupAgentCode = fm.all('GroupAgentCode').value;
	    if(tGroupAgentCode==null||tGroupAgentCode=='')
	    {
	       alert("请先录入业务员代码");
	       return false;
	    }
    var strsql ="1 and  (othersign =#"+fm.all("AgentType").value+"# or comcode = #其它#)";
    showCodeList('grpmoney',[Aclass,AclassName],[0,1],null,strsql,1,1);
  }  