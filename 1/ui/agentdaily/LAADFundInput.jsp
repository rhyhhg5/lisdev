<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-06-16 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAADFundInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAADFundInit.jsp"%>
  <!--%@include file="../agent/SetBranchType.jsp"%-->
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./LAADFundSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAADFund1);">
    
    <td class=titleImg>
      员工加扣款信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLAADFund1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  员工编码 
		</td>
        <td  class= input> 		  
		  <Input class="codeno" name=AgentCode readonly verify="代理人编码|code:AgentCode" 
		  ondblclick="return showCodeList('AgentCode',[this,AgentCodeName], [0,1],null,acodeSql,'1',null);" 
		  onkeyup="return showCodeListKey('AgentCode', [this,AgentCodeName], [0,1],null,acodeSql,'1',null);"><input class=codename name=AgentCodeName readonly=true >
		</td>
		<td  class= title>
		   员工姓名
		</td>
        <td  class= input>
		  <input name=Name class='readonly' readonly >
		</td>
      </tr>
      <tr  class= common> 
      <td  class= title> 
		  销售机构 
		</td>
        <td  class= input> 
		  <input class='readonly' readonly name=AgentGroup > 
		</td>
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class='readonly' readonly name=ManageCom >
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  加扣款称号
		</td>
        <td  class= input> 
        	  <Input class="codeno" name=AwardTitle ondblclick="return showCodeList('adfund',[this,AwardTitleName], [0,1],null,dcodeSql,'1',null);" onkeyup="return showCodeListKey('adfund',[this,AwardTitleName], [0,1],null,dcodeSql,'1',null);"><input class=codename name=AwardTitleName readonly=true >
        </td>
        <td  class= title> 
		  加扣款原因
		</td>
        <td  class= input> 
		  <input name=PunishRsn class= common  > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  加扣款发放单位类别
		</td>
        <td  class= input> 
		  <input name=AClass class='codeno' ondblclick="return showCodeList('rpaclass',[this,AClassName],[0,1]);" onkeyup="return showCodeListKey('rpaclass',[this,AClassName],[0,1]);" ><input class=codename name=AClassName readonly=true > 
		</td>
        <td  class= title> 
		  发放单位
		</td>
        <td  class= input> 
		  <input class= common name=SendGrp > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  金额
		</td>
        <td  class= input> 
		  <input name=Money class= common > 
		</td>
        <td  class= title> 
		  批注
		</td>
        <td  class= input> 
		  <input name=Noti class= common > 
		</td>
      </tr>
      <tr  class= common>
        <td  class= title> 
		  执行日期
		</td>
        <td  class= input> 
		  <input name=DoneDate class='coolDatePicker' dateFormat='short'>
		</td>
        <td  class= title> 
		  纪录顺序号 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=Idx > 
		</td>
      </tr>
      <tr class=common>
        <td  class= title>
		   操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class="readonly" readonly >
		</td>
      </tr>
    </table>
  </Div>
      <Div  id= "divHouse" style= "display: 'none'"> 
      	<tr class=common>
          <td  class= title>
		缺勤时间（单位：小时）
	  </td>		
          <td  class= input>	
		<input name=HouseNumber class=common OnChange="return calMoney();" >
	  </td>
      	</tr>
      </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=DoneFlag value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


<script>
  var acodeSql = "1 and (BranchType=#2# or BranchType=#3#) and (AgentState = #01# or AgentState = #02#)";
  var dcodeSql = "1 and code<>#01#";	
</script>



