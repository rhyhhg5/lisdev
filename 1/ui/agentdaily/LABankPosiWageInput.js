//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //initArchieveGrid();
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("LAContFYCRateInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
	
//if(!ArchieveGrid.checkValue("ArchieveGrid")) return false;
if(!chkMulLine()) return false;
return true;

}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  if(!beforeSubmit())
  {
    return false;
  }
	

   var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	  if(!beforeSubmit())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if(!beforeSubmit())
  {
    return false;
  }  
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

///////////////////////////////////////////////////////////////////////
//function getname()
//{
// var strsql="select name from laagent where agentstate<'03' "
//            + getWherePart('AgentCode','AgentCode')
//            + getWherePart('BranchType','BranchType')	
//	    + getWherePart('BranchType2','BranchType2');
// var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
// //alert(strsql);
//     if (!strQueryResult)
//  {
//    alert("没有此代理人！");
//
//    fm.all('AgentCode').value  = '';
//    fm.all('AgentCodeName').value  = '';
//    return;
//  }
//   //查询成功则拆分字符串，返回二维数组
//   
//  var tArr = new Array();
//  tArr = decodeEasyQueryResult(strQueryResult);	    
//  fm.all('AgentCodeName').value=tArr[0][0];
//}
///////////////////////////////////////////////////////////////////////
function easyQueryClick() {
  if (verifyInput() == false)
  return false;	
  initArchieveGrid();
  
	//此处书写SQL语句			     			    
  var strSql = "select a.gradecode,a.gradename ,b.basewage ,b.idx "
  			+"from laagentgrade a left join labasewage b on a.gradecode=b.agentgrade and b.type='01' and b.branchtype='3' and b.branchtype2='01' and b.managecom like '"
  			+fm.all('ManageCom').value+"%' "
  			+"where a.branchtype='3' and a.branchtype2='01' and a.gradecode<>'F00'"
	   	    //+" from  latree b   where  b.branchtype='3' and b.branchtype2='01'"
	   	    //+ getWherePart('ManageCom', 'ManageCom')
	   	    +" order by a.gradecode " ;
   // alert(strSql);
 
  var arrResult = new Array();
  arrResult = easyExecSql(strSql);
  
  if (!arrResult) 
    {
    alert("没有符合条件的数据！");
    return false;
    }
    displayMultiline(arrResult,ArchieveGrid);
   //查询成功则拆分字符串，返回二维数组
 
}
/////////////////////////////////////////////////////////
//function getQueryResult()
//{
//	var arrSelected = null;
//	tRow = ArchieveGrid.getSelNo();
//	//alert("111" + tRow);
//	//edit by guo xiang at 2004-9-13 17:54
//	//if( tRow == 0 || tRow == null || arrDataSet == null )
//	if( tRow == 0 || tRow == null )
//	    return arrSelected;
//	
//	arrSelected = new Array();
//	
//	//设置需要返回的数组
//	//edit by guo xiang at 2004-9-13 17:54
//	arrSelected[0] = new Array();
//	arrSelected[0] = ArchieveGrid.getRowData(tRow-1);
//	//arrSelected[0] = arrDataSet[tRow-1];
//	
//	return arrSelected;
//}             
///////////////////////////////////////////////////////


//提交，修改按钮对应操作
function DoSave()
{
	
	fm.fmtransact.value = "UPDATE";
	if (verifyInput() == false)
  return false;	
  if((fm.all('ManageCom').value=="")||(  fm.all('ManageCom').value==null))
 {
 	alert("管理机构不能为空！");
 	return false;
 }

	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

//提交，保存按钮对应操作
function DoInsert()
{
	//alert(fm.all('ManageCom').value);
	 if((fm.all('ManageCom').value=="")||(  fm.all('ManageCom').value==null))
 {
 	alert("管理机构不能为空！");
 	return false;
 }
  fm.fmtransact.value = "INSERT" ;
  if (verifyInput() == false)
  return false;	
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{

	var i;
	var selFlag = true;
	var iCount = 0;
	var manageCom=fm.all('ManageCom').value;
	var rowNum = ArchieveGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(ArchieveGrid.getChkNo(i))
		{
			iCount++;
			if((ArchieveGrid.getRowColData(i,1) == null)||(ArchieveGrid.getRowColData(i,1) == ""))
			{
				alert("第"+(i+1)+"行职级代码不能为空");
				ArchieveGrid.setFocus(i,1,ArchieveGrid);
				selFlag = false;
				break;
			}
			if((ArchieveGrid.getRowColData(i,3) == null)||(ArchieveGrid.getRowColData(i,3) == ""))
			{
				alert("第"+(i+1)+"行基本工资不能为空");
				ArchieveGrid.setFocus(i,1,ArchieveGrid);
				selFlag = false;
				break;
			}
	
	    //如果idx为空不能修改
			if((ArchieveGrid.getRowColData(i,4) == null)||(ArchieveGrid.getRowColData(i,4)=="") || (ArchieveGrid.getRowColData(i,4)==0))
			{

				if(fm.fmtransact.value == "UPDATE")
				{
					alert("有未保存的新增纪录！不可修改，请先保存记录。");
					ArchieveGrid.checkBoxAllNot();
					ArchieveGrid.setFocus(i,1,ArchieveGrid);
					selFlag = false;
					break;
				}
				else if (fm.fmtransact.value == "INSERT")
				{
				var strsql="select idx from labasewage  where agentgrade='"+(ArchieveGrid.getRowColData(i,1))+"' and managecom='"+manageCom+"' ";
				var arrResult = new Array();
        arrResult = easyExecSql(strsql);
  
        if (arrResult) 
        {
        alert("序号"+(i+1)+"纪录已存在，不可保存！");
				ArchieveGrid.checkBoxAllNot();
				ArchieveGrid.setFocus(i,1,ArchieveGrid);
				selFlag = false;
				break;
        }
       }
			}
			else 
				{	//如果idx不为空不能插入
					if(fm.fmtransact.value == "INSERT")
					{
						alert("序号"+(i+1)+"纪录已存在，不可保存！");
						ArchieveGrid.checkBoxAllNot();
						ArchieveGrid.setFocus(i,1,ArchieveGrid);
						selFlag = false;
						break;
					}
					else if(fm.fmtransact.value == "UPDATE")
					{
				var tsql="select agentgrade from labasewage  where idx="+(ArchieveGrid.getRowColData(i,4));
				var strQueryResult = easyQueryVer3(tsql, 1, 1, 1);
        var tArr = new Array();
        tArr = decodeEasyQueryResult(strQueryResult);	
        var grade=tArr[0][0];
   
       if(grade!==(ArchieveGrid.getRowColData(i,1)))
       {
       	alert("不能变更职级修改！");
						ArchieveGrid.checkBoxAllNot();
						ArchieveGrid.setFocus(i,1,ArchieveGrid);
						selFlag = false;
						break;
       	}
        
        
        	}
				}

			}
			else
				{//不是选中的行
					if((ArchieveGrid.getRowColData(i,4) == null)||(ArchieveGrid.getRowColData(i,4)==""))
					{
						alert("有未保存的新增纪录,请先保存记录!");
						ArchieveGrid.checkBoxAllNot();
						ArchieveGrid.setFocus(i,1,ArchieveGrid);
						selFlag = false;
						break;
					}
				}
			}
			if(!selFlag) return selFlag;
			if(iCount == 0)
			{
				alert("请选择要保存或修改的记录!");
				return false;
			}
			
			
			return true;
		}