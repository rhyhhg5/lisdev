<%
//程序名称：LAContQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:05:58
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ProtocolNo').value = "";
    fm.all('SignDate').value = "";
    fm.all('ManageCom').value = "";
    fm.all('AgentCom').value = "";
    fm.all('RepresentA').value = "";
    fm.all('RepresentB').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('SpareMonths').value = "";
  }
  catch(ex) {
    alert("在LAContQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLAContGrid();  
  }
  catch(re) {
    alert("LAContQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LAContGrid;
function initLAContGrid() {                               
  var iArray = new Array();
    
  try {
  
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="合同编号";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="签约时间";         		//列名
    iArray[2][1]="100px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="签约管理机构";         		//列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="签约中介机构";         		//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=0;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="保险公司代表人";         		//列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=0;         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="中介机构代表人";         		//列名
    iArray[6][1]="100px";         		//列名
    iArray[6][3]=0;         		//列名
    
    iArray[7]=new Array();
    iArray[7][0]="合同起期";         		//列名
    iArray[7][1]="100px";         		//列名
    iArray[7][3]=0;         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="合同止期";         		//列名
    iArray[8][1]="100px";         		//列名
    iArray[8][3]=0;         		//列名
    
     
    iArray[9]=new Array();
    iArray[9][0]="合同类型";         		//列名
    iArray[9][1]="0px";         		//列名
    iArray[9][3]=0; 



   LAContGrid = new MulLineEnter( "fm" , "LAContGrid" ); 
    //这些属性必须在loadMulLine前
 
    LAContGrid.mulLineCount = 0;   
    LAContGrid.displayTitle = 1;
    LAContGrid.hiddenPlus = 1;
    LAContGrid.hiddenSubtraction = 1;
    LAContGrid.canSel = 1;
    LAContGrid.canChk = 0;
    LAContGrid.selBoxEventFuncName = "showOne";
  
    LAContGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAContGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
