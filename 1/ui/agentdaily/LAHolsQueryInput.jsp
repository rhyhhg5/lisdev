<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAHolsQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAHolsQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAHolsQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>营销员考勤信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAHolsQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAHols1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAHols1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  营销员代码 
		</td>
        <td  class= input> 
		  <input class= common id="myid" name=GroupAgentCode onchange="return checkValid();"> 
		</td>
        <td  class= title> 
		  销售机构 
		</td>
        <td  class= input> 
		  <input class=common name=AgentGroup > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class=common name=ManageCom > 
		</td>
        <td  class= title> 
		  纪录顺序号 
		</td>
        <td  class= input> 
		  <input class=common name=Idx > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  请假类型
		</td>
        <td  class= input> 
		  <input name=AClass class='codeno' maxlength=2 
		   ondblclick="return showCodeList('holsaclass',[this,AClassName],[0,1]);" 
		   onkeyup="return showCodeListKey('holsaclass',[this,AClassName],[0,1]);" 
		  ><Input name=AClassName class="codename"> 
		</td>
        <td  class= title> 
		  假期天数
		</td>
        <td  class= input> 
		  <input name=VacDays class= common > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  请假日期
		</td>
        <td  class= input> 
		  <input name=LeaveDate class='coolDatePicker' dateFormat='short'> 
		</td>
        <td  class= title> 
		  应销假日期
		</td>
        <td  class= input> 
		  <input name=ShouldEndDate class='coolDatePicker' dateFormat='short' > 
		</td>
      </tr>
      <tr  class= common>
        <td  class= title>
		  请假单填写标志
		</td>
        <td  class= input>
		  <input name=FillFlag class='codeno' maxlength=1 
		   ondblclick="return showCodeList('yesno',[this,FillFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,FillFlagName],[0,1]);" 
		  ><Input name=FillFlagName class="codename">
		</td>
        <td  class= title>
		  诊断证明书标志
		</td>
        <td  class= input>
		  <input name=ConfIdenFlag class='codeno' maxlength=1 
		   ondblclick="return showCodeList('yesno',[this,ConfIdenFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,ConfIdenFlagName],[0,1]);" 
		  ><Input name=ConfIdenFlagName class="codename">
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title>
		  产假增加标志
		</td>
        <td  class= input>
		  <input name=AddVacFlag class='codeno' maxlength=1 
		   ondblclick="return showCodeList('yesno',[this,AddVacFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,AddVacFlagName],[0,1]);" 
		  ><Input name=AddVacFlagName class="codename">
		</td>
        <!--td  class= title> 
		  缺勤天数
		</td>
        <td  class= input> 
		  <input name=AbsDays class=common > 
		</td>
      </tr-->
      <!--tr  class= common>
        <td  class= title>
		  金额
		</td>
        <td  class= input>
		  <input name=SumMoney class=common >
		</td-->
        <td  class= title>
		  备注
		</td>
        <td  class= input>
		  <input name=Noti class= common id="Noti" >
		</td>
      </tr>
      <tr  class= common>
        <td  class= title> 
		核准人
		</td>
        <td  class= input> 
		  <input name=ApproveCode class= common > 
		</td>
        <td  class= title>
		  操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class=common >
		</td>
      </tr>
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <input type=hidden class=Common name=querySql > 
          <input type=hidden class=Common name=querySqlTitle > 
          <input type=hidden class=Common name=Title >
          <input type=hidden name=AgentCode value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();">
          <INPUT VALUE="下  载" class=cssbutton TYPE=button onclick="ListExecl();"> 
          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHolsGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divHolsGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanHolsGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 				
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>      
  </form>
</body>
</html>
