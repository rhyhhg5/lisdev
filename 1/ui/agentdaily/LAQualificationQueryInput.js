/** 
 * 程序名称：LAQualificationInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-21 15:48:13
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
//修改人：解青青 日期：2014-11-06
function checkvalid()
{
	var tSql = "";
  
  tSql  = "SELECT";
  tSql += "    A.AgentCode";
  tSql += "  FROM";
  tSql += "    LAAgent A";
  tSql += " WHERE 1=1";
  tSql += getWherePart('A.GroupAgentCode','GroupAgentCode');
  
  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
  
   if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('GroupAgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //判断是否查询成功
  
  //查询成功则拆分字符串，返回二维数组
  else{
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  //设置姓名
  document.fm.AgentCode.value = tArr[0][0];
  }
}

// 查询按钮
function easyQueryClick() {
 		
 		
  var tReturn = getManageComLimitlike("b.ManageCom");	
  if(!verifyInput())
  {
  	return false;
  } 
  var tsql="";
  var tState=fm.all("AgentState").value;
  if(tState=='01')
  {
  	 tsql = " and AgentState<='02'   ";
  	}
  else if (tState=='02')
  {
  	 tsql = " and AgentState<='05'  and AgentState>='03'  ";
  }
  else if (tState=='03')
  {
  	tsql = " and AgentState>='06'   ";
  }    
	var strSql = "select getUniteCode(a.AgentCode),a.QualifNo,a.GrantUnit,a.GrantDate,a.InvalidDate,a.InvalidRsn,a.ValidStart,a.ValidEnd,"
	  +" Case a.State WHEN '0' THEN '有效' else '无效' END,a.PasExamDate,a.ExamYear,a.ExamTimes,a.Operator,a.ModifyDate,(case a.state when '1' then '已失效' else (select char(days(a.validend) - days(current date)) from dual) end),a.AgentCode from LAQualification a,laagent b where a.agentcode=b.agentcode and b.branchtype = '1' and b.branchtype2= '01'   "
    + tReturn
    + getWherePart("a.AgentCode", "AgentCode")
    + getWherePart("a.QualifNo", "QualifNo")
    + getWherePart("a.GrantUnit", "GrantUnit")
    + getWherePart("a.GrantDate", "GrantDate")
    + getWherePart("a.InvalidDate", "InvalidDate")
    + getWherePart("a.InvalidRsn", "InvalidRsn")
    + getWherePart("a.State", "QualifState")
    + getWherePart("a.PasExamDate", "PasExamDate")
    + getWherePart("a.ExamYear", "ExamYear")
    + getWherePart("a.ValidStart", "ValidStart")
    + getWherePart("a.ValidEnd", "ValidEnd")
    + getWherePart("a.ExamTimes", "ExamTimes")
    + getWherePart("b.ManageCom", "ManageCom")
    + tsql
     if(fm.all("DistanceDay").value!=null&&fm.all("DistanceDay").value!="")
    {
    	strSql+=" and current date >= a.validend- "+fm.all("DistanceDay").value+" day  and a.validend>=current date and a.state = '0'";
    }
    +"order by agentcode";
  //alert(strSql);
turnPage.queryModal(strSql, LAQualificationGrid);
	
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  //alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LAQualificationGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LAQualificationGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LAQualificationGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
function ListExecl()
{
  
//定义查询的数据
var strSQL = "";
var tsql="";
var tState=fm.all("AgentState").value;
  if(tState=='01')
  {
  	 tsql = " and AgentState<='02'   ";
  	}
  else if (tState=='02')
  {
  	 tsql = " and AgentState<='05'  and AgentState>='03'  ";
  }
  else if (tState=='03')
  {
  	tsql = " and AgentState>='06'   ";
  }    
	var strSql = "select getUniteCode(a.AgentCode),b.name,a.QualifNo,a.GrantUnit,a.GrantDate,a.ValidStart,a.ValidEnd,"
	  +" Case a.State WHEN '0' THEN '有效' else '无效' END,a.InvalidDate,a.InvalidRsn,a.PasExamDate,a.ExamYear,a.ExamTimes,(case a.state when '1' then '已失效' else (select char(days(a.validend) - days(current date)) from dual) end) from LAQualification a,laagent b where a.agentcode=b.agentcode  and 1=1 "
    + getWherePart("a.AgentCode", "AgentCode")
    + getWherePart("a.QualifNo", "QualifNo")
    + getWherePart("a.GrantUnit", "GrantUnit")
    + getWherePart("a.GrantDate", "GrantDate")
    + getWherePart("a.InvalidDate", "InvalidDate")
    + getWherePart("a.InvalidRsn", "InvalidRsn")
    + getWherePart("a.State", "QualifState")
    + getWherePart("a.PasExamDate", "PasExamDate")
    + getWherePart("a.ExamYear", "ExamYear")
    + getWherePart("a.ValidStart", "ValidStart")
    + getWherePart("a.ValidEnd", "ValidEnd")
    + getWherePart("a.ExamTimes", "ExamTimes")
    + getWherePart("b.ManageCom", "ManageCom")
    +tsql
     if(fm.all("DistanceDay").value!=null&&fm.all("DistanceDay").value!="")
    {
    	strSql+=" and current date >= a.validend- "+fm.all("DistanceDay").value+" day  and a.validend>=current date and a.state = '0'";
    }
    +"order by b.agentcode";
fm.querySql.value = strSql;
  
//定义列名
var strSQLTitle = "select '代理人代码','代理人姓名','资格证书号','批准单位','发放日期','有效起期','有效止期','资格证书状态','失效日期','失效原因','通过考试日期','考试年度','考试次数','距离资格证有效止期天数'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '业务员资格证信息 查询下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}



