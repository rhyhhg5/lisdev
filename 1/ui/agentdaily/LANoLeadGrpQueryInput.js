//程序名称：LANoLeadGrpQueryInput.js
//程序功能：查询没有主管的单位的函数
//创建日期：2005-5-24 14:57
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

/*********************************************************************
 *  查询当前没有主管的销售单位
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

var turnPage = new turnPageClass();

/*********************************************************************
 *  查询没有主管的销售单位
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQuery()
{
  // 初始化表格
	initNoLeadGrpGrid();
	
	// 书写SQL语句
	var strSQL = "SELECT";
	strSQL    += "    BranchAttr,";
	strSQL    += "    name,";
	strSQL    += "    FoundDate,";
	strSQL    += "    ManageCom";
	strSQL    += " FROM";
	strSQL    += "    LABranchGroup";
	strSQL    += " WHERE";
	strSQL    += "    (EndFlag is null or EndFlag <> 'Y') AND";
	strSQL    += "    (State is null or State <> '1') AND";
	strSQL    += "    (BranchManager is null or BranchManager = '')";
	strSQL    += getWherePart('ManageCom', 'ManageCom', 'like');
	strSQL    += getWherePart('BranchAttr', 'BranchAttr', 'like');
	strSQL    += " ORDER BY";
	strSQL    += "    BranchAttr";
	
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有查到相关信息！");
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = NoLeadGrpGrid;

  //保存SQL语句
  turnPage.strQuerySql = strSQL;

  //设置查询起始位置
  turnPage.pageIndex = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

/*********************************************************************
 *  点击查询按钮处理
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
  //进行查询处理
  easyQuery();
}