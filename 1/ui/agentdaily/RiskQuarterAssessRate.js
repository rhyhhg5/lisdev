//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!checkValue()) return false;
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }

}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
   
function easyQueryClick()
{	
  if(!verifyInput()) return false;   
  var  sql = "select idx,agentgrade"
           +",(select codename from ldcode where code = a.agentgrade and codetype='gradelevel' ), "
           + "riskcode,(select riskname from lmrisk where riskcode = a.riskcode), insureyear,banktype,payintv,code4,'',code1,code2,code5,rate "
           + "from LADiscount a where discounttype='06' "
           + getWherePart('ManageCom', 'ManageCom');    
  turnPage.queryModal(sql, RiskQuarterAssessRateGrid);
  if(RiskQuarterAssessRateGrid.mulLineCount == 0)
  {
    alert("没有符合条件的绩效提奖比例信息");
  }
  
}

function checkValue()
{
	var countNum=0;
	var selFlag=true;
	if(RiskQuarterAssessRateGrid.mulLineCount==0)
	{
		alert('没有选择要操作的记录！');
		return false;
	}
	for(var i=0;i<RiskQuarterAssessRateGrid.mulLineCount;i++)
	{　
		if(RiskQuarterAssessRateGrid.getChkNo(i))
		{
			countNum++;
			if(RiskQuarterAssessRateGrid.getRowColData(i,2).trim()=='')
			{
				alert('职级不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,4).trim()==''){
				alert('险种编码不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,5).trim()==''){
				alert('险种名称不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,6).trim()==''){
				alert('保费类型不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,7).trim()==''){
				alert('渠道类型不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,8).trim()==''){
				alert('缴费方式不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,9).trim()==''){
				alert('保险期间不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,11).trim()==''){
				alert('最低缴费年期不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,12).trim()==''){
				alert('缴费年期小于不能为空');
				return false;
			}
			if(RiskQuarterAssessRateGrid.getRowColData(i,13).trim()==''){
				alert('保单年度不能为空');
				return false;
			}
			if((RiskQuarterAssessRateGrid.getRowColData(i,1) == null)||(RiskQuarterAssessRateGrid.getRowColData(i,1)==""))
            {
              if((fm.all("fmtransact").value =="UPDATE") )
              {
                alert("必须先查询出记录，再修改！");
                RiskQuarterAssessRateGrid.checkBoxAllNot();
                RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                selFlag = false;
                break;
              }
               if(fm.all("fmtransact").value =="INSERT")
              {

                var trisk=RiskQuarterAssessRateGrid.getRowColData(i,4).trim();
                var tyearstart=RiskQuarterAssessRateGrid.getRowColData(i,11).trim();
                 var myearstart=parseInt(tyearstart);
                var tyearend=RiskQuarterAssessRateGrid.getRowColData(i,12).trim();
                var myearend=parseInt(tyearend);
                var tyearend1 = parseInt(tyearend);
                //alert("***"+myearstart+"---"+myearstart >=0 || myearend <=100);
                if(trisk=='240501'||trisk=='340201')
                { 
                    if(myearend!=3 &&myearstart!=5)
                     {
                       alert("此纪录录入的缴费年期有错，只能为3或5,不可插入！");
                       RiskQuarterAssessRateGrid.checkBoxAllNot();
                       RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                       selFlag = false;
                       break;
                     }
                   if(myearstart==3 &&tyearend1>5)
                     {
                      alert("此纪录录入的缴费年期有错，为3~5,不可插入！");
                      RiskQuarterAssessRateGrid.checkBoxAllNot();
                      RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                      selFlag = false;
                      break;
                     }
                   if(myearstart==5 &&myearend!=100)
                    {
                     alert("此纪录录入的缴费年期有错,为5~100,不可插入！");
                     RiskQuarterAssessRateGrid.checkBoxAllNot();
                     RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                     selFlag = false;
                     break;
                    }
                }else if(trisk=='333501'||trisk=='532401'||trisk=='532501')
                {
                  if(myearstart!=5 &&myearstart!=10)
                    {
                     alert("此纪录录入的缴费年期起期有错，只能为5或10,不可插入！");
                     RiskQuarterAssessRateGrid.checkBoxAllNot();
                     RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                     selFlag = false;
                     break;
                    }
                  if(myearstart==5 &&tyearend1>10)
                    {
                     alert("此纪录录入的缴费年期有错，为5~10,不可插入！");
                     RiskQuarterAssessRateGrid.checkBoxAllNot();
                     RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                     selFlag = false;
                     break;
                    }
                  if(myearstart==10&&myearend!=100)
                   {
                    alert("此纪录录入的缴费年期有错,为10~100,不可插入！");
                    RiskQuarterAssessRateGrid.checkBoxAllNot();
                    RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                    selFlag = false;
                    break;
                  }
                }
                else
                {
                if(myearstart <0 && myearend >100){
                alert("此纪录的缴费年期有错，只能为0~100,不可插入！");
                RiskQuarterAssessRateGrid.checkBoxAllNot();
                RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                selFlag = false;
                break;
                }
                }
              }
            }
            else if ((RiskQuarterAssessRateGrid.getRowColData(i,1) != null)||(RiskQuarterAssessRateGrid.getRowColData(i,1)!=""))
            {	//如果idx不为空不能插入
              if(fm.all("fmtransact").value =="INSERT")
              {
                alert("此纪录已存在，不可插入！");
                RiskQuarterAssessRateGrid.checkBoxAllNot();
                RiskQuarterAssessRateGrid.setFocus(i,2,RiskQuarterAssessRateGrid);
                selFlag = false;
                break;
              }
            }
		}
	}
	if(!selFlag)
	{
   	  return selFlag;
   	}
	if(countNum==0)
	{
		alert("请选择进行操作的记录。");
		return false;
	}
	if(fm.all("fmtransact").value=="UPDATE")
	{
		for(var i=0;i<RiskQuarterAssessRateGrid.mulLineCount;i++)
		{
			if(RiskQuarterAssessRateGrid.getRowColData(0,1).trim()=='')
			{
				alert('列表中存在系统没有的记录，不能修改');
				return false;
			}
		}
	} 
	return true 
}
function save()
{
	fm.all("fmtransact").value="INSERT";
	submitForm();
}
function update()
{
	fm.all("fmtransact").value="UPDATE";
	submitForm();
}

//删除功能 --
function delete1()
{ 
	fm.all("fmtransact").value="DELETE";
	if (confirm("您确实想删除该条记录?"))
  {
	submitForm();
 }else
  {
    alert("您取消了修改操作！");
  }
}