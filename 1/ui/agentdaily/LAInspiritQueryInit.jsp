<%
//程序名称：LAInspiritQueryInit.jsp
//程序功能：
//创建日期：2005-6-30 10:10
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
  //添加页面控件的初始化。
%>                            
<script language="JavaScript">
var tBranchType = "<%=BranchType%>";
var tAgentTitleGrid = "营销员";
if("2"==tBranchType)
{
  tAgentTitleGrid = "业务员";
}

//初始化画面上的文本框
function initInpBox()
{ 
  try
  {
  	document.fm.PloyName.value = "";
  	document.fm.StartDateF.value = "";
  	document.fm.StartDateT.value = "";
  	document.fm.Organizer.value = "";
  	document.fm.EndDateF.value = "";
  	document.fm.EndDateT.value = "";
  	document.fm.AgentCode.value = "";
  	document.fm.AgentName.value = "";
  	document.fm.InspiritName.value = "";
  	document.fm.ManageCom.value = "<%=tG.ManageCom%>";
  	document.fm.BranchType.value = "<%=BranchType%>";
  	document.fm.BranchType2.value = "<%=BranchType2%>";
  }
  catch(ex)
  {
    alert("在LAInspiritQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

//初始化页面
function initForm()
{
  try
  {
    initInpBox();
    initInspiritGrid();
  }
  catch(re)
  {
    alert("LAInspiritQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//初始化列表
function initInspiritGrid()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="活动起期";         //列名
    iArray[1][1]="80px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="活动止期";         //列名
    iArray[2][1]="80px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="统计日期";         //列名
    iArray[3][1]="80px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="活动名称";         //列名
    iArray[4][1]="120px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="组织单位";         //列名
    iArray[5][1]="120px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="获奖人代码";         //列名
    iArray[6][1]="60px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[7]=new Array();
    iArray[7][0]="获奖人名";         //列名
    iArray[7][1]="60px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="获奖称号";         //列名
    iArray[8][1]="120px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="奖励内容";         //列名
    iArray[9][1]="0px";         //宽度
    iArray[9][2]=100;         //最大长度
    iArray[9][3]=3;         //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="信息序列号";         //列名
    iArray[10][1]="0px";         //宽度
    iArray[10][2]=100;         //最大长度
    iArray[10][3]=3;         //是否允许录入，0--不能，1--允许
    
    InspiritGrid = new MulLineEnter( "fm" , "InspiritGrid" ); 

    //这些属性必须在loadMulLine前
    InspiritGrid.mulLineCount = 0;   
    InspiritGrid.displayTitle = 1;
    InspiritGrid.hiddenPlus = 1;
    InspiritGrid.hiddenSubtraction = 1;
    InspiritGrid.locked=1;
    InspiritGrid.canSel=1;
    InspiritGrid.canChk=0;
    InspiritGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("初始化InspiritGrid时出错："+ ex);
  }
}
</script>
