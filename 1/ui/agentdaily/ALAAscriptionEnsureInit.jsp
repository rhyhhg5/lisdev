<%
//程序名称：LAAscriptionInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
  fm.all('Name').value ='';
  fm.all('AgentCode').value ='';
  fm.all('ManageCom').value ='';
   fm.all('Flag').value ='';
   fm.all('BranchType').value = '<%=BranchType%>'; 
    fm.all('BranchType2').value = '<%=BranchType2%>'; 
    fm.all('MakeType').value='<%=MakeType%>'
    fm.all('ContNo').value = '';
  }
  catch(ex)
  {
    alert("在LAAscriptionInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAAscriptionInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initAscriptionGrid();    
  }
  catch(re)
  {
    alert("在LAAscriptionInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  
//var   AscriptionGrid;
// 担保人信息的初始化
function initAscriptionGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();             
      iArray[1][0]="归属序列号";          		        //列名 主键,用于后台的确认
      iArray[1][1]="0px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		        //列名
      iArray[2][1]="100px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="团单号";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="个单号";         		//列名
      iArray[4][1]="100px";         		//宽度
      iArray[4][3]=100;         		//最大长度
      iArray[4][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[5]=new Array();
      iArray[5][0]="原代理人";         		//列名
      iArray[5][1]="100px";         		//宽度
      iArray[5][3]=100;         		//最大长度
      iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[6]=new Array();
      iArray[6][0]="原代理人姓名";         		//列名
      iArray[6][1]="100px";         		//宽度
      iArray[6][3]=100;         		//最大长度
      iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[7]=new Array();
      iArray[7][0]="原代理人机构";         		//列名
      iArray[7][1]="100px";         		//宽度
      iArray[7][3]=100;         		//最大长度
      iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[8]=new Array();
      iArray[8][0]="现代理人";         		//列名
      iArray[8][1]="100px";         		//宽度
      iArray[8][3]=100;         		//最大长度
      iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[9]=new Array();
      iArray[9][0]="现代理人姓名";         		//列名
      iArray[9][1]="100px";         		//宽度
      iArray[9][3]=100;         		//最大长度
      iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    
    
      iArray[10]=new Array();
      iArray[10][0]="现代理人机构";         		//列名
      iArray[10][1]="100px";         		//宽度
      iArray[10][3]=100;         		//最大长度
      iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
     
      iArray[11]=new Array();
      iArray[11][0]="归属类别";         		//列名
      iArray[11][1]="100px";         		//宽度
      iArray[11][3]=100;         		//最大长度
      iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[12]=new Array();
      iArray[12][0]="归属日期";         		//列名
      iArray[12][1]="100px";         		//宽度
      iArray[12][3]=100;         		//最大长度
      iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
      
      iArray[13]=new Array();
      iArray[13][0]="归属状态";      	   		//列名
      iArray[13][1]="100px";            			//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=3;           			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="操作类别";         		//列名
      iArray[14][1]="100px";         		//宽度
      iArray[14][3]=100;         		//最大长度
      iArray[14][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[15]=new Array();
      iArray[15][0]="投保人姓名";         		//列名
      iArray[15][1]="100px";         		//宽度
      iArray[15][3]=100;         		//最大长度
      iArray[15][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[16]=new Array();
      iArray[16][0]="性别";         		//列名
      iArray[16][1]="100px";         		//宽度
      iArray[16][3]=100;         		//最大长度
      iArray[16][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[17]=new Array();
      iArray[17][0]="年龄";         		//列名
      iArray[17][1]="100px";         		//宽度
      iArray[17][3]=100;         		//最大长度
      iArray[17][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[18]=new Array();
      iArray[18][0]="联系方式";         		//列名
      iArray[18][1]="100px";         		//宽度
      iArray[18][3]=100;         		//最大长度
      iArray[18][4]=0;         		//是否允许录入，0--不能，1--允许
    
      iArray[19]=new Array();
      iArray[19][0]="保费";         		//列名
      iArray[19][1]="100px";         		//宽度
      iArray[19][3]=100;         		//最大长度
      iArray[19][4]=0;         		//是否允许录入，0--不能，1--允许
     
      iArray[20]=new Array();
      iArray[20][0]="原销售单位代码";         		//列名
      iArray[20][1]="100px";         		//宽度
      iArray[20][3]=100;         		//最大长度
      iArray[20][4]=0;         		//是否允许录入，0--不能，1--允许    
      
      iArray[21]=new Array();
      iArray[21][0]="现销售机构代码";         		//列名
      iArray[21][1]="100px";         		//宽度
      iArray[21][3]=100;         		//最大长度
      iArray[21][4]=0;         		//是否允许录入，0--不能，1--允许    
      

      
      AscriptionGrid = new MulLineEnter( "fm" , "AscriptionGrid" ); 
      //这些属性必须在loadMulLine前
      AscriptionGrid.canChk =1
      AscriptionGrid.mulLineCount = 0;   
    AscriptionGrid.displayTitle = 1;
    AscriptionGrid.hiddenPlus = 1;
    AscriptionGrid.hiddenSubtraction = 1;
    AscriptionGrid.loadMulLine(iArray);
    AscriptionGrid.canChk =1; //复选框   
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>