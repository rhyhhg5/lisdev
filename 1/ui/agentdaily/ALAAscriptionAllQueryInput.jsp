<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序功能：F1报表生成
//创建日期：2007-07-20
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
 <script>
   var msql=" 1 and   char(length(trim(comcode)))<=#8# ";
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ALAAscriptionAllQueryInput.js"></SCRIPT>  
<%@include file="./ALAAscriptionAllQueryInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%> 
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body  onload="initForm();initElementtype();" >   
  <form action="" method=post name=fm target="f1print">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 保单分配实时查询
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,1,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,1,1);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>        
       
         </TR>   
            <TR  class= common> 
         <TD class= title>
							个单号
						</TD>
						<TD class= input>
							<Input  class=common name=ContNo verify="个单号" onchange=validContNo()>
						</TD>
        <TD class= title>
							团单号
						</TD>
						<TD class= input>
							<Input  class=common name=GrpContNo verify="团单号" onchange=validGrpContNo()>
						</TD>
         </TR>               
       <TR  class= common>
  	    <TD  class= title>
            原代理人编码
          </TD>          
          <TD  class= input>
            <Input class=common name=AgentCode verify="代理人员编码" onchange=validAgentCode()>
          </TD>  
          <td  class= title>
		   原代理人姓名
		</td>
        <td  class= input>
		  <input class=common name=AgentName verify="代理人姓名" onchange=validAgentName()>
		</td> 
   </TR>
   <TR  class= common>
  	    <TD  class= title>
            新代理人编码
          </TD>          
          <TD  class= input>
            <Input class=common name=AgentNew verify="代理人员编码" onchange=validAgentCode2()>
          </TD>  
          <td  class= title>
		   新代理人姓名
		</td>
        <td  class= input>
		  <input class=common name=AgentNewName verify="代理人姓名" onchange=validAgentName2()>
		</td> 
   </TR>
   <TR  class= common>
   		<td  class= title>
		   原销售单位代码
		</td>
        <td  class= input>
		  <input name=BranchAttr  class= common verify="原销售单位" onchange=validBranchattr()>
		</td> 
  	    <TD  class= title>
           分配操作日期
          </TD>          
          <TD  class= input>
            <Input class='coolDatePicker' dateFormat='short' name=MakeDate  verify="分配日期|Date">
          </TD>  
          
   </TR>
   <TR  class= common>
   		<td  class= title>
		   查询起期（归属日期）
		</td>
        <td  class= input>
		  <Input class='coolDatePicker' dateFormat='short' name=StartDate  verify="分配日期|Date">
		</td> 
  	    <TD  class= title>
           查询止期（归属日期）
          </TD>          
          <TD  class= input>
            <Input class='coolDatePicker' dateFormat='short' name=EndDate  verify="分配日期|Date">
          </TD>  
          
   </TR>
    </table>
    <INPUT VALUE="查　询" class="cssButton" TYPE="button" onclick="easyQueryClick()">
    <INPUT VALUE="下　载" class="cssButton" TYPE="button" onclick="doDownLoad()">    
    <Div id="divLACross1" style="display:''">
    <table class=common>
    	<tr class=common>
	  <td text-align:left colSpan=1>
  	   <span id="spanLACrossGrid">
  	   </span> 
	 </td>
	</tr>
    </table>
    </div>
    <Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 				
    </div>
    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup value=''> 
    <input type=hidden name=querySQL value=""> 
　　
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 