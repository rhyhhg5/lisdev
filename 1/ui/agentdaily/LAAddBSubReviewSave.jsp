<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LAAddBSubReviewUI tLAAddBSubReviewUI = new LAAddBSubReviewUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      String tSendGrp1 = request.getParameter("SendGrp1");    //状态标识00初始 01提交
      LARewardPunishSet tSetU = new LARewardPunishSet();	//用于更新
	  // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      tVData.add(tGI);
      //创建数据集
      if(tAction.equals("selSubmit")||tAction.equals("selBack")){
      	String tChk[] = request.getParameterValues("InpRewardPunishGridChk"); 
      	String tAgentCode[] = request.getParameterValues("RewardPunishGrid1");
      	String tIdx[] = request.getParameterValues("RewardPunishGrid14");
      	for(int i=0;i<tChk.length;i++){
      		if(tChk[i].equals("1")){
          		//创建一个新的Schema
      	  		LARewardPunishSchema tSch = new LARewardPunishSchema();
				tSch.setIdx(tIdx[i]);
				tSch.setAgentCode(tAgentCode[i]);
				tSch.setSendGrp(tSendGrp1);
      	  		tSetU.add(tSch);
       		}
      	}
      	tVData.add(tSetU);
      	tLAAddBSubReviewUI.submitData(tVData,tAction);
      }else{
      	String tManageCom=request.getParameter("ManageCom");
      	String tAgentGroup=request.getParameter("AgentGroup");
      	String tGroupAgentCode=request.getParameter("GroupAgentCode");
      	String tDoneFlag=request.getParameter("DoneFlag");
      	String tDoneFlag1=request.getParameter("DoneFlag1");
      	String tWageNo = request.getParameter("WageNo");
      	String tSendGrp = request.getParameter("SendGrp");
      	String tAgentType = request.getParameter("AgentType");
      	tVData.add(1,tManageCom);
      	tVData.add(2,tAgentGroup);
      	tVData.add(3,tGroupAgentCode);
      	tVData.add(4,tDoneFlag);
      	tVData.add(5,tDoneFlag1);
      	tVData.add(6,tWageNo);
      	tVData.add(7,tSendGrp);
      	tVData.add(8,tAgentType);
      	tVData.add(9,tSendGrp1);
      	tLAAddBSubReviewUI.submitData(tVData,tAction);
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAddBSubReviewUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
