<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddSubSave.jsp
//程序功能：
//创建日期：2016-04-07
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数

  LATaxAdjustSet tLATaxAdjustSet  = new LATaxAdjustSet();
  ManagementOfTaxableItemsUI tManagementOfTaxableItemsUI   = new ManagementOfTaxableItemsUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();              
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

    tG=(GlobalInput)session.getValue("GI");
    LATaxAdjustSchema tLATaxAdjustSchema  = new LATaxAdjustSchema();

    tLATaxAdjustSchema.setAgentCode(request.getParameter("AgentCode"));
    tLATaxAdjustSchema.setManageCom(request.getParameter("Managecom"));
    tLATaxAdjustSchema.setCBPTaxes(request.getParameter("CBPTaxes"));
    tLATaxAdjustSchema.setITBPTaxes(request.getParameter("ITBPTaxes"));
    tLATaxAdjustSchema.setPCBPTaxes(request.getParameter("PCBPTaxes"));
    tLATaxAdjustSchema.setBranchType(request.getParameter("BranchType"));
    tLATaxAdjustSchema.setBranchType2(request.getParameter("BranchType2"));
    tLATaxAdjustSchema.setWageNo(request.getParameter("WageNo"));
    tLATaxAdjustSchema.setOperator(tG.Operator);
    tLATaxAdjustSet.add(tLATaxAdjustSchema);

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(tLATaxAdjustSet);
	
  try
  {
    tManagementOfTaxableItemsUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tManagementOfTaxableItemsUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
