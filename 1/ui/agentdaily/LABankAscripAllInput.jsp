<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：
	//程序功能：
	//创建日期：2007-07-23 
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
	if (BranchType == null || BranchType.equals("")) {
		BranchType = "";
	}
	if (BranchType2 == null || BranchType2.equals("")) {
		BranchType2 = "";
	}
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LABankAscripAllInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LABankAscripAllInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="./LABankAscripAllSave.jsp" method=post name=fm
	target="fraSubmit">
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLAAscription);"></td>
		<td class=titleImg>查询条件</td>
	</tr>
</table>
<Div id="divLAAscription" style="display: ''">
<table class=common align='center'>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name=ManageCom
			verify="管理机构|code:comcode&NOTNULL&len>7"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"><Input
			class=codename name=ManageComName readOnly elementtype=nacessary>
		</TD>
		<TD class=title>销售机构编码</TD>
		<TD class=input><Input class=common name=BranchAttr></TD>
	</TR>
	<TR class=common>
		<TD class=title>业务员代码</TD>
		<TD class=input><Input class=common name=AgentCode
			verify="业务员代码|NOTNULL" elementtype=nacessary></TD>
		<td class=title>网点机构代码</td>
		<td class=input><input name=AgentCom class=common></td>
	</TR>
	<TR class=common>
		<TD class=title>保单号</TD>
		<TD class=input><Input class=common name=ContNo verify="保单号">
		</TD>
		<td class=title>险种编码</td>
		<td class=input><input name=RiskCode class=codeno verify="险种编码"
			ondblclick="return showCodeList('bankriskcode',[this,RiskCodeName],[0,1]);"
			onkeyup="return showCodeListKey('bankriskcode',[this,RiskCodeName],[0,1]);"><Input
			class=codename name=RiskCodeName readOnly></td>
	</TR>
	<TR>
		<TD class=title>人员状态</TD>
		<td class=input><Input class="codeno" name=AgentState
			verify="人员状态|NOTNULL" CodeData="0|^01|在职|^02|离职"
			ondblclick="return showCodeListEx('AgentState',[this,AgentStateName],[0,1]);"
			onkeyup="return showCodeListKeyEx('AgentState',[this,AgentStateName],[0,1]);"><Input
			class="codename" name=AgentStateName readonly=true
			elementtype=nacessary></td>

		<td class=title>缴费方式</td>
		<TD class=input><Input class="codeno" name=PayIntv
			CodeData="0|^12|期缴|^0|趸缴|"
			ondblclick="return showCodeListEx('PayIntv',[this,PayIntvName],[0,1]);"
			onkeyup="return showCodeListKeyEx('PayIntv',[this,PayIntvName],[0,1]);"
			onchange=""><input class=codename name=PayIntvName
			readonly=true></TD>

	</TR>

	<TR>
		<td class=title>保单状态</td>
		<TD class=input><Input class="codeno" name=StateFlag
			CodeData="0|^0|投保|^1|承保有效|^2|失效中止|^3|终止|^4|合同终止|"
			ondblclick="return showCodeListEx('StateFlag',[this,StateFlagName],[0,1]);"
			onkeyup="return showCodeListKeyEx('StateFlag',[this,StateFlagName],[0,1]);"
			onchange=""><input class=codename name=StateFlagName
			readonly=true></TD>
	</TR>

	<TR class=input>
		<TD class=common><INPUT class=cssButton VALUE="查  询" TYPE=button
			onclick="return easyQueryClick();"></TD>
	</TR>
</table>
</Div>
<hr>
<br>
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLAAscription3);">
		<td class=titleImg>归属后人员信息</td>
		</td>
	</tr>
</table>
<div id = 'divLAAscription3'>
<table class=common align='center'>
	<tr>
		<TD class=title>新业务员代码</TD>
		<TD class=input><Input class="codeno" name=NewAgentCode
			ondblclick="return getNewAgentCode(this,NewAgentName);"
			onkeyup="return getNewAgentCode(this,NewAgentName);"><Input
			class=codename name=NewAgentName elementtype=nacessary readOnly >
	</tr>
</table>
<INPUT class=cssButton VALUE="批量保存" TYPE=button
	onclick="return submitForm();"> 
</div>
<br>
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLAAscription2);">
		<td class=titleImg>查询结果</td>
		</td>
	</tr>
</table>
<Div id="divLAAscription2" style="display: ''">
<table class=common>
	<tr class=common>

		<td text-align: left colSpan=1><span id="spanAscriptionGrid">
		</span></td>
	</tr>
</table>
<Div id="divPage" align=center style="display: 'none' "><INPUT
	class=cssbutton VALUE="首  页" TYPE=button
	onclick="turnPage.firstPage();"> <INPUT class=cssbutton
	VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> <INPUT
	class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
<INPUT class=cssbutton VALUE="尾  页" TYPE=button
	onclick="turnPage.lastPage();"></Div>

</Div>
<br><br>
<p><font color="#ff0000">注：此功能只记录保单变更的轨迹，不对数据进行修改。如果想进行变更，请通过“归属确认”进行操作。</font></p>
<p><font color="#ff0000">归属的原则是：不变更网点，只变更人员。</font></p>
<p><font color="#ff0000">查询保单数据量较大，请耐心等待</font></p>
<input type=hidden
	id="fmtransact" name="fmtransact"> <input type=hidden
	id="fmAction" name="fmAction"> <input type=hidden
	name=AgentGroup value=''> <input type=hidden name=BranchType
	value=''> <input type=hidden name=BranchType2 value=''>
	<input type=hidden name=querySQL value=''> 
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
