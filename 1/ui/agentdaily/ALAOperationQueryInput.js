/****************************************************
 * 功能简介：根据传进来的集体投保单号码查询出保单相关信息
 * 传入参数：无
 * 返 回 值：无
 ****************************************************/
function queryGrpCont()
{
	var tSQL = "";
	var arrResult ;

	fm.all('ProposalGrpContNo').value = ProposalGrpContNo;
	fm.all('PrtNo').value = prtNo;
	
	tSQL  = "select ManageCom,SaleChnl,HandlerName,HandlerDate,AgentCom,AgentDate,getUniteCode(AgentCode),ReceiveDate,AppntNo";
	tSQL += "  from LCGrpCont where ProposalGrpContNo = '"+ProposalGrpContNo+"'";

	//arrResult = easyExecSql(tSQL);
	var strQueryResult = easyQueryVer3(tSQL,1,0,1,0,1);
	arrResult = decodeEasyQueryResult(strQueryResult,0,1);
	
	if(arrResult == null)
	{
		return;
	}
	
	fm.all('ManageCom').value = arrResult[0][0];
	fm.all('ManageComName').value = getQueryValue("select Name from ldcom where Sign='1' and ComCode = '"+arrResult[0][0]+"'");
	fm.all('SaleChnl').value = arrResult[0][1];
	fm.all('SaleChnlName').value = getNameFromLDCode("salechnl",arrResult[0][1]);
	fm.all('HandlerName').value = arrResult[0][2];
	fm.all('HandlerDate').value = arrResult[0][3];
	fm.all('AgentCom').value = arrResult[0][4];
	fm.all('AgentComName').value = getNameByCode("Name","LACom","AgentCom",arrResult[0][4]);
	fm.all('AgentDate').value = arrResult[0][5];
	fm.all('AgentCode').value = arrResult[0][6];
	fm.all('AgentName').value = getQueryValue("select name from LAagent where groupAgentcode = '"+arrResult[0][6]+"' fetch first 1 rows only");
	fm.all('ReceiveDate').value = arrResult[0][7];
	fm.all('GrpNo').value = arrResult[0][8];
}

/****************************************************
 * 功能简介：查询客户信息
 * 传入参数：无
 * 返 回 值：无
 ****************************************************/
function queryLDGrp()
{
	var tSQL = "";
	var tAppGrpCode = "";
	var arrResult ;
	
	tAppGrpCode = document.fm.GrpNo.value;
	
	tSQL  = "select a.GrpName,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Phone1,b.Fax1,b.E_Mail1,a.GrpNature,a.BusinessType,";
	tSQL += "       a.OnWorkPeoples+a.OffWorkPeoples+a.OtherPeoples,a.OnWorkPeoples,a.OffWorkPeoples,a.OtherPeoples";
	tSQL += "  from LDGrp a,LCGrpAddress b where a.CustomerNo = b.CustomerNo and a.CustomerNo = '"+tAppGrpCode+"'";
	
	//arrResult = easyExecSql(tSQL);
	var strQueryResult = easyQueryVer3(tSQL,1,0,1,0,1);
	arrResult = decodeEasyQueryResult(strQueryResult,0,1);
	
	if(arrResult == null)
	{
		return ;
	}
	
	fm.all('GrpName').value = arrResult[0][0];
	fm.all('GrpAddress').value = arrResult[0][1];
	fm.all('GrpZipCode').value = arrResult[0][2];
	fm.all('LinkMan1').value = arrResult[0][3];
	fm.all('Phone1').value = arrResult[0][4];
	fm.all('Fax1').value = arrResult[0][5];
	fm.all('E_Mail1').value = arrResult[0][6];
	fm.all('BusinessTypeName').value = getNameFromLDCode("businesstype",arrResult[0][7]);
	fm.all('GrpNatureName').value = getNameFromLDCode("grpnature",arrResult[0][8]);
	fm.all('Peoples').value = arrResult[0][9];
	fm.all('AppntOnWorkPeoples').value = arrResult[0][10];
	fm.all('AppntOffWorkPeoples').value = arrResult[0][11];
	fm.all('AppntOtherPeoples').value = arrResult[0][12];
}

/****************************************************
 * 功能简介：查询核保结论
 * 传入参数：无
 * 返 回 值：无
 ****************************************************/
function getLCGCUWMaster()
{
	var tSQL = "";
	var arrResult ;

	tSQL  = "select passFlag ,uwidea  from LCGCUWMaster where ProposalGrpContNo = '"+ProposalGrpContNo+"'";
	
	var strQueryResult = easyQueryVer3(tSQL,1,0,1,0,1);
	arrResult = decodeEasyQueryResult(strQueryResult,0,1);
	
	if(arrResult == null)
	{
		return ;
	}
	
	document.fm.GUWState.value = arrResult[0][0];
	document.fm.GUWStateName.value = getNameFromLDCode("uwstatequery",arrResult[0][0]);
	document.fm.GUWIdea.value = arrResult[0][1];
}

/****************************************************
 * 功能简介：查询传入SQL文查询的结果
 * 传入参数：pmSQL  传入SQL文
 * 返 回 值：返回查询结果
 ****************************************************/
function getQueryValue(pmSQL)
{
	var arrResult ;
	var tRtValue = "";
	
	if (pmSQL == null || pmSQL == "")
	{
		return "";
	}
	
	var strQueryResult = easyQueryVer3(pmSQL,1,0,1,0,1);
	arrSelected = decodeEasyQueryResult(strQueryResult,0,1);
	
	if(arrSelected==null)
		return("");
	else
		return(arrSelected[0][0]);
}