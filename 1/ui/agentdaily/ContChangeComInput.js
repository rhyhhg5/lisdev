 // 该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	

	if(!beforeSubmit())
   {
 	return false;
 	}
 	fm.all("hideOperate").value='INSERT||MAIN';
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

   initForm();
} 
 
// function checkPrename()
// {
//  var tBranchType=fm.all('BranchType').value;
//  var tBranchType2=fm.all('BranchType2').value;
// 	var sql="select (select b.branchattr from labranchgroup b where b.agentgroup=a.agentgroup),a.name,a.agentgroup,a.managecom from laagent a where 1=1 "
// 	 + getWherePart("a.AgentCode","AgentNew") 
// 	 + " and branchtype='"+tBranchType+"'"
//	 + " and branchtype2='"+tBranchType2+"'";
//     var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
//     if (!strQueryResult)
//  {
//    alert("没有此代理人！");
//    fm.all('AgentOld').value='';	
//    fm.all('BranchAttr').value = '';
//    fm.all('Prename').value  = '';
//    fm.all('AgentGroup').value  = '';
//    fm.all('ManageCom').value = '';
//    return;
//  }
//   //查询成功则拆分字符串，返回二维数组
//   
//  var tArr = new Array();
//  tArr = decodeEasyQueryResult(strQueryResult);
//  fm.all('BranchAttr').value  = tArr[0][0];
//  fm.all('PreName').value = tArr[0][1];
//  fm.all('AgentGroup').value = tArr[0][2];
//  fm.all('ManageCom').value=tArr[0][3];
//  showOneCodeNametoAfter('comcode','ManageCom','ManageComName');
// 
//  //存储原AgentGroup值，以备保存时使用  
//  //<addcode>############################################################//
// 
// 	}
 	
//***********************************
//查询新代理机构信息
//***********************************
function 	checkAgentcom()
{

	var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
	var  sql = "select a.agentcom,a.name,b.agentcode,(select name from laagent where agentcode=b.agentcode) ,b.agentgroup from lacom a,lacomtoagent b  where 1=1 "
	    + getWherePart('a.agentcom','AgentComNew') + " and a.agentcom=b.agentcom  and b.relatype='1' "
	    + " and branchtype='"+tBranchType+"'"
	    + " and branchtype2='"+tBranchType2+"'";
	    
	var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
     if (!strQueryResult)
  {
    alert("没有此代理机构！");
    fm.all('AgentComNew').value='';
    fm.all('AgentComNewName').value = '';
    return false;
  }  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);　
  fm.all('AgentComNewName').value  = tArr[0][1];
  fm.all('AgentNew').value = tArr[0][2];
  fm.all('NameNew').value = tArr[0][3];
  fm.all('AgentGroupNew').value = tArr[0][4];
  
 }
//**********************************
//查询保单信息
//**********************************
function	checkCount()
{

	var sql="select PrtNo,agentcode,(select name from laagent where agentcode=lccont.agentcode),agentcom,(select name from lacom where agentcom=lccont.agentcom),agentgroup,contno from lccont where 1=1  and salechnl='04' "
	       + getWherePart('PrtNo','PrtNo')
	       +" UNION select PrtNo,agentcode,(select name from laagent where agentcode=lBcont.agentcode),agentcom,(select name from lacom where agentcom=lBcont.agentcom),agentgroup,contno from lBcont where 1=1  and salechnl='04' "
	       + getWherePart('PrtNo','PrtNo');
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("此保单不存在！");
    fm.all('PrtNo').value='';
    return false;
   }
   //查询保单所属的业务员
   else
   {
     var ttArr = new Array();
       ttArr = decodeEasyQueryResult(strQueryResult);
       fm.all('AgentOld').value = ttArr[0][1];
       fm.all('PreName').value = ttArr[0][2];
       fm.all('AgentComOld').value = ttArr[0][3];
       fm.all('AgentComOldName').value = ttArr[0][4];
       fm.all('AgentGroupOld').value = ttArr[0][5];
       fm.all('ContNo').value = ttArr[0][6];
     }
   }
 	
//提交前的校验、计算  
function beforeSubmit()
{

  if ((fm.all('PrtNo').value == '')||(fm.all('PrtNo').value == null))
  {
  	alert("请输入印刷号！");
  	fm.all('PrtNo').focus();
  	return false;
  } 
  
  if ((fm.all('AgentComOld').value != '')&&(fm.all('AgentComOld').value != null))
  {
  	alert("此功能只能处理银行代理机构为空的保单！");
  	initForm();
  	return false;
  } 
   
  //新代理人不能与原代理人相同
  if((fm.all('AgentComOld').value==fm.all('AgentComNew').value))
  {
    alert("原代理机构与新代理机构相同！");
    initForm();
    return false;

  }

  //提示
  if(!confirm("确定将保单'"+fm.all('PrtNo').value+"'转到机构'"+fm.all('AgentComNew').value+"'名下?"))
  {
    alert("你取消了操作!");
    resetForm();
    return false;
  }
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
		

  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码 
//   if((fm.all('AgentComOld').value==fm.all('AgentComNew').value))
//  {
//  	  	alert("原代理机构不能与新代理机构相同!!");
//  	  	return false;
//  }
  mOperate="INSERT||MAIN";
  submitForm();
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




