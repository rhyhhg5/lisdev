//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	
	if (!beforeSubmit())
	 return false;
	fm.fmtransact.value = "INSERT||MAIN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
 //   showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LActiveAccountsInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  if(!verifyInput()) return false;


	

  if (!checkValid())
  {
  	return false;
  }
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  
    //当状态修改为有效，对有效账户进行校验
 
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(fm.all('Account').value=='')
	{
		alert("请查询数据再进行修改!");
	}else{
  //下面增加相应的代码
  if (confirm("您确实想修改该条记录?"))
  {
 	
 
  //showSubmitFrame(mDebug);
    fm.fmtransact.value = "UPDATE||MAIN";
    
    //fdisable();
	
//	if(fm.all('OpenDate').value!=tOpenDate){
// 		alert('开户日期不能修改');
// 		
// 	   tdisable();
// 	
// 		return false;	
// 	}
  if (!beforeSubmit())
  {
 // 	tdisable();
  	return false;
  }
  //当状态修改为有效，对有效账户进行校验
//  if(fm.all('State').value==0){
//  	if(!checkAccount()){
//  		tdisable();
//  		return false;
//  	}
//  }
  	
   var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
    
  
  fm.submit(); //提交
	
	fm.all('AccountOld').value= fm.all('Account').value;
	fm.all('AgentCodeOld').value= fm.all('AgentCode').value;
//	tdisable();
 	
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
	}
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{  
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2= fm.all('BranchType2').value;
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LActiveAccountsQuery.html?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  if (!beforeSubmit())
  {
 // 	tdisable();
  	return false;
  }
  
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
	   
	
	
//	fm.all('modifyButton').disabled=false;
		arrResult = arrQueryResult;
    fm.all('GroupAgentCode').value= arrResult[0][0];
    fm.all('Account').value= arrResult[0][1];
    fm.all('conAccount').value= arrResult[0][1];
    fm.all('AccountName').value= arrResult[0][2];
//    fm.all('AgentName').value= arrResult[0][2];
    if(arrResult[0][3]=='有效')
    {
    	fm.all('State').value='0';
    	fm.all('StateName').value='有效';
    }
    else
  	{
  		fm.all('State').value='1';
  		fm.all('StateName').value='无效';
  	}     
    fm.all('OpenDate').value= arrResult[0][4];
 //   tOpenDate= arrResult[0][4];
    fm.all('DestoryDate').value= arrResult[0][5];
    fm.all('Bank').value= arrResult[0][6];
    
    fm.all('Operator').value= arrResult[0][8];
    fm.all('ModifyDate').value= arrResult[0][9];
	fm.all('AccountOld').value= arrResult[0][1];
	fm.all('AgentCodeOld').value= arrResult[0][0];
	
	  
	
    getAgentName();
  //  tdisable();
    
	}
}               
function checkValid()
{
	var Account1=trim(fm.all('Account').value);
	var Account2=trim(fm.all('conAccount').value);

	if(Account1!=Account2)
	{
		alert("两次输入的帐号不一样！");
		fm.all('Account').value='';
		fm.all('conAccount').value='';
		return false;
	}
	return true;
}       

//查询业务员信息
function getAgentName()
{
	var tSql = "";
	
	//判断有无业务员代码
	if (getWherePart('GroupAgentCode')=='')
  {
    document.fm.AgentGroup.value = "";
    document.fm.ManageCode.value  = "";
    document.fm.AgentName.value  = "";
    document.fm.AgentCode.value = "";
    document.fm.GroupAgentCode.value = "";
    document.fm.ManageComName.value="";
    document.fm.AccountName.value="";
   	return false;
  }
  
  tSql  = "SELECT";
  tSql += "    A.Name,";
  tSql += "    A.ManageCom,";
  tSql += "    B.BranchAttr,";
  tSql += "    A.AgentCode ";
  tSql += "    FROM ";
  tSql += "    LAAgent A,";
  tSql += "    LABranchGroup B";
  tSql += " WHERE 1=1 AND ";
  tSql += "    A.AgentGroup = B.AgentGroup  ";
  tSql += getWherePart('a.groupagentcode','GroupAgentCode');
  tSql += getWherePart('a.branchtype','BranchType');
  tSql += getWherePart('a.branchtype2','BranchType2');
  
  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员或业务员所在的渠道不为互动渠道!");
    //document.fm.AgentCode.value="";
    document.fm.AgentGroup.value = "";
    document.fm.ManageCode.value  = "";
    document.fm.AgentName.value  = "";
    document.fm.AgentCode.value = "";
    document.fm.GroupAgentCode.value = "";
    document.fm.ManageComName.value="";
    document.fm.AccountName.value="";
    return;
  }
  
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  //设置姓名
  document.fm.AgentName.value = tArr[0][0];
  document.fm.AccountName.value = document.fm.AgentName.value;
  //管理机构
  document.fm.ManageCode.value = tArr[0][1];
  //销售单位
  document.fm.AgentGroup.value = tArr[0][2];
  document.fm.AgentCode.value = tArr[0][3];
  document.fm.AgentCodeOld.value = tArr[0][3];
  
  
 var  tNameSql =" select name from ldcom where comcode ='"+document.fm.ManageCode.value+"'";
  
  var strQueryResult1 = easyQueryVer3(tNameSql, 1, 0, 1);
  
  if (!strQueryResult1) {
	    //document.fm.AgentCode.value="";
	   
	    document.fm.ManageComName.value="";
	    return;
	  }
  var tNameArr = new Array();
  tNameArr = decodeEasyQueryResult(strQueryResult1);
  document.fm.ManageComName.value = tNameArr[0][0];
  
  
}
				


