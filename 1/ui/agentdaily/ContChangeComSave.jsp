//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LCContSchema mLCContSchema = new LCContSchema();
  LCContSchema mNewLCContSchema = new LCContSchema();
  ContChangeComUI mContChangeComUI  = new ContChangeComUI();

 //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||INSERT";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin ContChangeComUI schema...");
  //取得被调整信息
  mLCContSchema.setPrtNo(request.getParameter("PrtNo"));
  mLCContSchema.setContNo(request.getParameter("ContNo"));
  mLCContSchema.setAgentCode(request.getParameter("AgentOld"));
  mLCContSchema.setAgentGroup(request.getParameter("AgentGroupOld"));
  mLCContSchema.setAgentCom(request.getParameter("AgentComOld"));
  System.out.println("ContNo:"+request.getParameter("PrtNo"));
  System.out.println("ContNo:"+request.getParameter("ContNo"));
  System.out.println("AgentCode:"+request.getParameter("AgentOld"));
  System.out.println("原机构:"+request.getParameter("AgentComOld"));
  //取得目标单位信息
  System.out.println("begin to ContChangeCom set...");
  mNewLCContSchema.setAgentCom(request.getParameter("AgentComNew"));//调整后的机构代码
  mNewLCContSchema.setAgentCode(request.getParameter("AgentNew"));     //调整后的业务员代码
  mNewLCContSchema.setAgentGroup(request.getParameter("AgentGroupNew"));//调整后的业务员团队

  System.out.println("end 单位信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLCContSchema);
  System.out.println(mLCContSchema.getAgentCode());
  tVData.addElement(mNewLCContSchema);
  System.out.println(mNewLCContSchema.getAgentCode());
  System.out.println("add over");
  try
  {
   mContChangeComUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (!FlagStr.equals("Fail"))
  {
    tError = mContChangeComUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";

    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

