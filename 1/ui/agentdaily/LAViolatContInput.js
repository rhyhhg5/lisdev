 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if (mOperate=="")
  {
  	addClick();
  }
	//alert(11);
	if(!beforeSubmit())
 {
 	return false;
 	}
  fm.all("hideOperate").value=mOperate;
  document.all.PrtNo.disabled=false;
  document.all.ContNo.disabled=false;  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  // alert(fm.all("State").value);
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{      
  var tViolatNo=fm.all("ViolatNo").value;	
  fm.reset();
  fm.all("ViolatNo").value=tViolatNo;
  document.all.PrtNo.disabled=false;
  document.all.ContNo.disabled=false;
	// document.all.AgentOld.readOnly=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    if (fm.action == './LAViolatContSave.jsp')     
      saveClick=true;
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

} 	
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
 // alert(123);
  if(fm.all('ViolatEndDate').value!="" && fm.all('ViolatEndDate').value!=null)
  {
   if( fm.all('ViolatEndDate').value<fm.all('ViolatDate').value)
   {
    alert("结案日期应大于或等于立案日期!");	
    return false;
   }
  }
  if(!verifyInput())
  {
  return false;
  	
  }	  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{	
   if ((fm.all("ViolatNo").value==null)||(fm.all("ViolatNo").value==''))
 {
    alert('请先查询出要修改的纪录！');
    return false;
 }
 
   if(!beforeSubmit())
 {
 	return false;
 	}
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	 //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAViolatContQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
 
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function getcontno()
{
  var sql="select contno,appntno,appntname,insuredno,insuredname,managecom,agentcode,agentgroup from lccont where 1=1  "
     + getWherePart("PrtNo", "PrtNo");
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("没有此投保单号！");
    fm.all('PrtNo').value="";
    return ;
  }
  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('ContNo').value=arr[0][0]; 
  fm.all('AppntNo').value=arr[0][1];  		
  fm.all('AppntName').value=arr[0][2]; 
  fm.all('InsuredNo1').value=arr[0][3]; 	
  fm.all('InsuredName1').value=arr[0][4]; 
  fm.all('ManageCom').value=arr[0][5];
  fm.all('AgentCode').value=arr[0][6];
  fm.all('AgentGroup').value=arr[0][7];
  sql="select  name  from  laagent  where agentcode='"+fm.all("AgentCode").value+"' ";
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("没有此销售人员！");
    fm.all('PrtNo').value="";
    fm.all('ContNo').value=""; 
    fm.all('AppntNo').value="";  		
    fm.all('AppntName').value=""; 
    fm.all('InsuredNo1').value=""; 	
    fm.all('InsuredName1').value=""; 
    fm.all('ManageCom').value="";
    fm.all('AgentCode').value="";
    fm.all('AgentGroup').value="";   
    return ;
  }
  arr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentName').value=arr[0][0];
  sql="select insuredno,name from lcinsured where PrtNo='"+fm.all("PrtNo").value+"'  and "
    +" contno='"+fm.all("ContNO").value+"'  and  insuredno<>'"+fm.all("InsuredNo1").value+"' "; 
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);   
  if (strQueryResult) 
  { 
    arr = decodeEasyQueryResult(strQueryResult);
    fm.all('InsuredNo2').value=arr[0][0]; 	
    fm.all('InsuredName2').value=arr[0][1]; 
    if(arr.length>1)
    {
     fm.all('InsuredNo3').value=arr[1][0]; 	
     fm.all('InsuredName3').value=arr[1][1]; 
    }     
    
  }
  
}
function getprtno()
{
  var sql="select prtno,appntno,appntname,insuredno,insuredname,managecom,agentcode,agentgroup from lccont where 1=1 and salechnl='01' "
     + getWherePart("PrtNo", "PrtNo");
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("没有此投保单号！");
    fm.all('ContNo').value="";
    return ;
  }
  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('PrtNo').value=arr[0][0]; 
  fm.all('AppntNo').value=arr[0][1];  		
  fm.all('AppntName').value=arr[0][2]; 
  fm.all('InsuredNo1').value=arr[0][3]; 	
  fm.all('InsuredName1').value=arr[0][4]; 
  fm.all('ManageCom').value=arr[0][5];
  fm.all('AgentCode').value=arr[0][6];
  fm.all('AgentGroup').value=arr[0][7];
  sql="select  name  from  laagent  where agentcode='"+fm.all("AgentCode").value+"' ";
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("没有此销售人员！");
    fm.all('PrtNo').value="";
    fm.all('ContNo').value=""; 
    fm.all('AppntNo').value="";  		
    fm.all('AppntName').value=""; 
    fm.all('InsuredNo1').value=""; 	
    fm.all('InsuredName1').value=""; 
    fm.all('ManageCom').value="";
    fm.all('AgentCode').value="";
    fm.all('AgentGroup').value="";   
    
    return ;
  }
  arr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentName').value=arr[0][0];
  sql="select insuredno,name from lcinsured where PrtNo='"+fm.all("PrtNo").value+"'  and "
    +" contno='"+fm.all("ContNO").value+"'  and  insuredno<>'"+fm.all("InsuredNo1").value+"' "; 
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);   
  if (strQueryResult) 
  { 
    arr = decodeEasyQueryResult(strQueryResult);
    fm.all('InsuredNo2').value=arr[0][0]; 	
    fm.all('InsuredName2').value=arr[0][1]; 
    if(arr.length>1)
    {
     fm.all('InsuredNo3').value=arr[1][0]; 	
     fm.all('InsuredName3').value=arr[1][1];     
    }
    
  }
  
}
function easyQueryClick()
{
  initLGAppealGrid();	    
	var strSql = "select AppealObjNo,AppealObjName,AppealObjPhone, AppealObjDep,AppealWayNo,AppealContent,LawMan,LawManTel,LawManAddress,State,AppealNo,DealNo from LGAppeal where 1=1 and AppealKind='11'"
    + getWherePart("AppealWayNo", "AppealWayNo")
    + getWherePart("AppealObjNo", "AppealObjNo")
    + getWherePart("AppealObjName", "AppealObjName")
    + getWherePart("AppealObjPhone", "AppealObjPhone")
    + getWherePart("AppealObjDep", "AppealObjDep")
    + getWherePart("LawMan", "LawMan")
    + getWherePart("LawManTel", "LawManTel")
    + getWherePart("LawManAddress", "LawManAddress")
    + getWherePart("State", "State1")
  ;
  //alert(strSql);
 //turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1); 		
	turnPage.queryModal(strSql, LGAppealGrid);
	 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }	
	
	
	
}

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
        fm.all('ViolatNo').value= arrResult[0][0];   
       fm.all('ViolatDate').value= arrResult[0][1];     
       fm.all('PrtNo').value= arrResult[0][2];
       fm.all('ContNo').value= arrResult[0][3];
       fm.all('AgentCode').value= arrResult[0][4];
       fm.all('AgentName').value= arrResult[0][5];       
       fm.all('AppntNo').value= arrResult[0][6];
       fm.all('AppntName').value= arrResult[0][7];
       fm.all('InsuredNo1').value= arrResult[0][8];
       fm.all('InsuredNo2').value= arrResult[0][9];
       fm.all('InsuredNo3').value= arrResult[0][10];
       fm.all('InsuredName1').value= arrResult[0][11];
       fm.all('InsuredName2').value= arrResult[0][12];
       fm.all('InsuredName3').value= arrResult[0][13];
       fm.all('DirectLoss').value= arrResult[0][14];
       fm.all('ReplevyLoss').value= arrResult[0][15];
       fm.all('ViolatEndDate').value= arrResult[0][16];
       fm.all('Operator').value= arrResult[0][17];  
       fm.all('ModifyDate').value= arrResult[0][18];
       fm.all('DealMind').value= arrResult[0][19];
       var sql="select agentgroup,managecom,branchtype,branchtype2  from  laviolatcont  where  violatno='"+fm.all('ViolatNo').value+"'";
       var strQueryResult = easyQueryVer3(sql, 1, 1, 1);   
       
       if (strQueryResult) 
       { 
         var arr = decodeEasyQueryResult(strQueryResult);
         
         fm.all('AgentGroup').value=arr[0][0]; 	
       
         fm.all('ManageCom').value=arr[0][1]; 
         fm.all('BranchType').value=arr[0][2]; 
         
         fm.all('BranchType2').value=arr[0][3]; 
       }
      document.all.PrtNo.disabled="true";
      document.all.ContNo.disabled="true";
      
    }
    return true;
}
   
