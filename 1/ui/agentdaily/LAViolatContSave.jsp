<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAViolatContSave.jsp
//程序功能：
//创建日期：2003-7-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  System.out.println("11111111"+request.getParameter("State"));
  String tViolatNo="";
  LAViolatContSchema tLAViolatContSchema   = new LAViolatContSchema();

  LAViolatContUI tLAViolatContUI   = new LAViolatContUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tState=request.getParameter("State");
  String tViolatEndDate=request.getParameter("ViolatEndDate");
  tLAViolatContSchema.setViolatNo(request.getParameter("ViolatNo"));
  tLAViolatContSchema.setPrtNo(request.getParameter("PrtNo"));
  tLAViolatContSchema.setContNo(request.getParameter("ContNo"));
  //存储真实的AgentGroup
  tLAViolatContSchema.setViolatDate(request.getParameter("ViolatDate"));
  tLAViolatContSchema.setAgentCode(request.getParameter("AgentCode"));
  tLAViolatContSchema.setManageCom(request.getParameter("ManageCom"));
  tLAViolatContSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLAViolatContSchema.setViolatEndDate(request.getParameter("ViolatEndDate"));
  tLAViolatContSchema.setAppntNo(request.getParameter("AppntNo"));
  tLAViolatContSchema.setInsuredNo1(request.getParameter("InsuredNo1"));
  tLAViolatContSchema.setInsuredNo2(request.getParameter("InsuredNo2"));
  tLAViolatContSchema.setInsuredNo3(request.getParameter("InsuredNo3"));  
  tLAViolatContSchema.setBranchType(request.getParameter("BranchType"));
  tLAViolatContSchema.setBranchType2(request.getParameter("BranchType2"));
  tLAViolatContSchema.setDealMind(request.getParameter("DealMind"));
  System.out.println("11111111"+request.getParameter("InsuredNo1"));
  if(tViolatEndDate.equals("") || tViolatEndDate==null)
  {
   tLAViolatContSchema.setState("0");//默认值为未处理0

  }
  else
  {
    tLAViolatContSchema.setState("1");//有处理日期，案件状态值为已处理
  }
  tLAViolatContSchema.setDirectLoss(request.getParameter("DirectLoss"));
  tLAViolatContSchema.setReplevyLoss(request.getParameter("ReplevyLoss"));
  tLAViolatContSchema.setOperator(tG.Operator);

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLAViolatContSchema);
	tVData.add(tG);
  try
  {
    tLAViolatContUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAViolatContUI.mErrors;
    if (!tError.needDealError())
    {    
        tViolatNo= tLAViolatContUI.getViolatNo();                    
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('ViolatNo').value = "<%=tViolatNo%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

