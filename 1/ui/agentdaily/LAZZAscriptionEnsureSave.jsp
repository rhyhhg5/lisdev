<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAscriptionSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>


<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAscriptionSchema tLAAscriptionSchema;
  LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
  LAZZAscriptionEnsureUI tLAZZAscriptionEnsureUI  = new LAZZAscriptionEnsureUI();
 
  //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Succ";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tFlag= request.getParameter("Flag");
  String tagentold= request.getParameter("AgentCode"); 
  String tmanagecom= request.getParameter("ManageCom"); 
  String tname= request.getParameter("name");
  String tbranchtype= request.getParameter("BranchType");
  String tbranchtype2= request.getParameter("BranchType2");
  String tMakeType = request.getParameter("MakeType");
    //取得授权信息
  if(tFlag.equals("SELECT"))
  {
  System.out.println("begin Ascription schema : "+tFlag);
  String tChk[] = request.getParameterValues("InpAscriptionGridChk");
  int lineCount = 0;
  int number = 0; 
  for(int index=0;index<tChk.length;index++)
  {
     if(tChk[index].equals("1"))           
     number++;
  }
  System.out.println("number"+number);    
  if(number==0)
	{
    	Content = " 失败，原因:没有选择要调整的员工！";
   		FlagStr = "Fail";		
	} 
  else
	{
	 String tAscripNo[] = request.getParameterValues("AscriptionGrid1");
   String tContNo[]=request.getParameterValues("AscriptionGrid4");
   String tGrpContNo[]=request.getParameterValues("AscriptionGrid3");
   String tAgentOld[]=request.getParameterValues("AscriptionGrid5");
   String tAgentNew[]=request.getParameterValues("AscriptionGrid7");
          	 
	 lineCount = tChk.length; //行数
	 System.out.println("length= "+String.valueOf(lineCount));
	
	 for(int i=0;i<lineCount;i++)
	 {
	   if(tChk[i].trim().equals("1"))
	   {
	      tLAAscriptionSchema = new LAAscriptionSchema();
	      System.out.println("Chk:"+tChk[i]);
	      tLAAscriptionSchema.setAscripNo(tAscripNo[i]);
	      if(!"".equals(tAgentOld[i])){
	    	  String  strSQL = "select AgentCode from LAAgent where 1=1 and groupagentcode='"+tAgentOld[i]+"'";
	    	  String cagentold = new ExeSQL().getOneValue(strSQL);
	    	  tLAAscriptionSchema.setAgentOld(cagentold); 
	          System.out.println("&&"+tLAAscriptionSchema.getAgentOld());
	       }
	       if(!"".equals(tAgentNew[i])){
	    		  String  strSQL = "select AgentCode from LAAgent where 1=1 and groupagentcode='"+tAgentNew[i]+"'";
	    		  String cagentnew = new ExeSQL().getOneValue(strSQL);
	    		  tLAAscriptionSchema.setAgentNew(cagentnew);
	    	        System.out.println("****"+tLAAscriptionSchema.getAgentNew());
	    	   }
        tLAAscriptionSchema.setContNo(tContNo[i]);  
        tLAAscriptionSchema.setGrpContNo(tGrpContNo[i]);  
       
        tLAAscriptionSet.add(tLAAscriptionSchema);
	   }
	   System.out.println("i:"+tChk[i]);
	  } 
	  System.out.println("end 归属信息...");
	}
  }	  
  else if(tFlag.equals("ALL"))
  {
       tLAAscriptionSchema = new LAAscriptionSchema();
       if(!"".equals(tagentold)){
	    	  String  strSQL = "select AgentCode from LAAgent where 1=1 and groupagentcode='"+tagentold+"'";
	    	  String cagentold = new ExeSQL().getOneValue(strSQL);
	    	  tLAAscriptionSchema.setAgentOld(cagentold);  
	          System.out.println("&&"+tLAAscriptionSchema.getAgentOld());
	       }
       tLAAscriptionSchema.setNoti(tname);
       tLAAscriptionSchema.setBranchType(tbranchtype);
       tLAAscriptionSchema.setBranchType2(tbranchtype2);
       tLAAscriptionSchema.setAgentNew(tmanagecom);
       tLAAscriptionSet.add(tLAAscriptionSchema);
   }
  if(!FlagStr.equals("Fail"))
  {
	  // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";
    //tLAAscriptionSchema = new LAAscriptionSchema();
    tVData.addElement(tLAAscriptionSet);
    tVData.add(tG);
    tVData.add(tFlag);
    tVData.add(tMakeType);
    //  tVData.add(tLAAscriptionSchema);
    System.out.println("add over");
    try
    {
      tLAZZAscriptionEnsureUI.submitData(tVData,tOperate);
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
   }   
   if (!FlagStr.equals("Fail"))
   {
     tError = tLAZZAscriptionEnsureUI.mErrors;
     if (!tError.needDealError())
     {
     	Content = " 保存成功! ";
     	FlagStr = "Succ";
     }
     else
     {
     	Content = " 保存失败，原因是:" + tError.getFirstError();
     	FlagStr = "Fail";
     }
   }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

