<%
//程序名称：LABankAscripAllInit.jsp
//程序功能：
//创建日期：2005-05-17 18:07:04
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
 var  tSQL="#86950000# and branchtype=#3# and branchtype2=#01#  ";
</SCRIPT>  
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                     
    fm.all('ManageCom').value = '';  
    fm.all('ManageComName').value='';
    fm.all('BranchAttr').value='';
    fm.all('StateFlag').value='';
    fm.all('StateFlagName').value='';
    fm.all('NewAgentCode').value='';
    fm.all('NewAgentName').value='';
    fm.all('ContNo').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentState').value = '';
    fm.all('AgentStateName').value = '';
    fm.all('AgentCom').value = '';
    fm.all('RiskCode').value = '';
    fm.all('RiskCodeName').value = '';
    fm.all('PayIntv').value = '';
    fm.all('PayIntvName').value = '';
    fm.all('querySQL').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
   
  }
  catch(ex)
  {
    alert("在LABankAscripAllInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LABankAscripAllInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initAscriptionGrid();   
  }
  catch(re)
  {
    alert("LABankAscripAllInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  }
var AscriptionGrid;
function initAscriptionGrid()
  {                              
    var iArray = new Array();      
    try
    {
      
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
 
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";         		//列名
    iArray[1][1]="110px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="签单日期";         		//列名
    iArray[2][1]="80px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="起保日期";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="止保日期";         		//列名
    iArray[4][1]="80px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="交致日";         		//列名
    iArray[5][1]="80px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="保费";         		//列名
    iArray[6][1]="60px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="管理机构编码";         		//列名
    iArray[7][1]="0px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    
    
    iArray[8]=new Array();
    iArray[8][0]="销售机构编码";         		//列名
    iArray[8][1]="0px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
     
    iArray[9]=new Array();
    iArray[9][0]="代理人编号";         		//列名
    iArray[9][1]="80px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="姓名";         		//列名
    iArray[10][1]="50px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="网点机构编码";         		//列名
    iArray[11][1]="80px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许 
    
    iArray[12]=new Array();
    iArray[12][0]="branchtype";         		//列名
    iArray[12][1]="0px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许 
    iArray[12][14]="3"; 
    
    
      iArray[13]=new Array();
    iArray[13][0]="新代理人编码";         		//列名
    iArray[13][1]="0px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=2;     
 	iArray[13][4]="agentcodet3";
    iArray[13][5]="13|14";              	               
    iArray[13][6]="0|1";
    iArray[13][9]="新代理人编码|NOTNULL"; 
    iArray[13][15]="managecom";
//    iArray[13][16]=tSQL; 
    iArray[13][17]= "7"; 
    
    iArray[14]=new Array();
    iArray[14][0]="姓名";         		//列名
    iArray[14][1]="0px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[15]=new Array();
    iArray[15][0]="新网点机构编码";         		//列名
    iArray[15][1]="0px";         		//宽度
    iArray[15][2]=100;         		//最大长度
    iArray[15][3]=2;         		//是否允许录入，0--不能，1--允许
    iArray[15][4]="comtoagent1";   
    iArray[15][5]="15";              	               
    iArray[15][6]="0";
//    iArray[15][9]="新网点机构编码|NOTNULL"; 
    iArray[15][15]="agentcode";
    iArray[15][17]= "13"; 
    
    
    AscriptionGrid = new MulLineEnter( "fm" , "AscriptionGrid" ); 
      //这些属性必须在loadMulLine前
    AscriptionGrid.canChk =0
    AscriptionGrid.mulLineCount = 0;   
    AscriptionGrid.displayTitle = 1;
    AscriptionGrid.hiddenPlus = 1;
    AscriptionGrid.hiddenSubtraction = 1;
    AscriptionGrid.loadMulLine(iArray);
    AscriptionGrid.canChk =0; //复选框   



     }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
