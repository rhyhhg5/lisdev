<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAHolsInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAHolsSchema tLAHolsSchema   = new LAHolsSchema();

  ALAHolsUI tLAHols   = new ALAHolsUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  String tLeaveState=request.getParameter("LeaveState");
  if(tLeaveState.equals("N"))
  {
  tLeaveState="0";
  }
  else
  {
  tLeaveState="1";
  } 
  
  
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLAHolsSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAHolsSchema.setBranchAttr(request.getParameter("AgentGroup"));
    tLAHolsSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
    tLAHolsSchema.setManageCom(request.getParameter("ManageCom"));
    tLAHolsSchema.setBranchType(request.getParameter("BranchType"));
    tLAHolsSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAHolsSchema.setIdx(request.getParameter("Idx"));
    tLAHolsSchema.setAClass(request.getParameter("AClass"));
    tLAHolsSchema.setVacDays(request.getParameter("VacDays"));
    tLAHolsSchema.setLeaveDate(request.getParameter("LeaveDate"));
    tLAHolsSchema.setShouldEndDate(request.getParameter("ShouldEndDate"));
    tLAHolsSchema.setEndDate(request.getParameter("EndDate"));
    tLAHolsSchema.setApproveCode(request.getParameter("ApproveCode"));
    tLAHolsSchema.setFillFlag(request.getParameter("FillFlag"));
    tLAHolsSchema.setConfIdenFlag(request.getParameter("ConfIdenFlag"));
    tLAHolsSchema.setAddVacFlag(request.getParameter("AddVacFlag"));
    tLAHolsSchema.setNoti(request.getParameter("Noti"));
    tLAHolsSchema.setOperator(request.getParameter("Operator"));
    tLAHolsSchema.setLeaveState(tLeaveState);

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLAHolsSchema);
	tVData.add(tG);
  try
  {
    tLAHols.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAHols.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

