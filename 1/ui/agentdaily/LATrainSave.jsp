<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LATrainSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LATrainSchema tLATrainSchema   = new LATrainSchema();
  ALATrainUI tALATrainUI   = new ALATrainUI();

  //输出参数
  CErrors tError = null;
  String tOperate = request.getParameter("hideOperate");
  tOperate = tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = " ";
  
  GlobalInput tG = new GlobalInput();
    //tG.Operator = "Admin";
    //tG.ComCode  = "001";
  //session.putValue("GI",tG);
  tG=(GlobalInput)session.getValue("GI");
  
  //根据集团统一工号查询业务员编码
	String agentcode = "";
	if(request.getParameter("GroupAgentCode")!=null&&!"".equals(request.getParameter("GroupAgentCode"))){
	agentcode = new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("GroupAgentCode")+"'");
	}

  tLATrainSchema.setAgentCode(agentcode);
  tLATrainSchema.setManageCom(request.getParameter("ManageCom"));
  tLATrainSchema.setBranchAttr(request.getParameter("AgentGroup"));
  tLATrainSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
  tLATrainSchema.setBranchType(request.getParameter("BranchType"));
  tLATrainSchema.setIdx(request.getParameter("Idx"));
  tLATrainSchema.setAClass(request.getParameter("AClass"));
  tLATrainSchema.setTrainUnit(request.getParameter("TrainUnit"));
  tLATrainSchema.setTrainCode(request.getParameter("TrainCode"));
  tLATrainSchema.setTrainName(request.getParameter("TrainName"));
  tLATrainSchema.setCharger(request.getParameter("Charger"));
  tLATrainSchema.setResult(request.getParameter("Result"));
  tLATrainSchema.setResultLevel(request.getParameter("ResultLevel"));
  tLATrainSchema.setTrainPassFlag(request.getParameter("TrainPassFlag"));
  tLATrainSchema.setTrainStart(request.getParameter("TrainStart"));
  tLATrainSchema.setTrainEnd(request.getParameter("TrainEnd"));
  tLATrainSchema.setDoneDate(request.getParameter("DoneDate"));
  tLATrainSchema.setNoti(request.getParameter("Noti"));
  tLATrainSchema.setDoneFlag(request.getParameter("DoneFlag"));
  tLATrainSchema.setOperator(tG.Operator);


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLATrainSchema);
  tVData.add(tG);
  try
  {
    tALATrainUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tALATrainUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功! ";
      FlagStr = "Succ";
    }
    else                                                                           
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
  parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

