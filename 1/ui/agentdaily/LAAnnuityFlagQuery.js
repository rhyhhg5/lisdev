/** 
 * 程序名称：LAAddressInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-20 18:07:04
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

 function changeGroup()
{
	var strSQL = "";
 var tBranchAttr=fm.all('BranchAttr').value;
 //alert(12);
   if (getWherePart('BranchAttr')!='')
  {	
     strSQL = "select AgentGroup,branchmanager,branchmanagername from LAbranchgroup where 1=1  "
     + getWherePart('BranchType','BranchType')
       + getWherePart('BranchType2','BranchType2')
	     + getWherePart('BranchAttr','BranchAttr');
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此销售单位！");
    fm.all('BranchAttr').value="";
    fm.all('AgentGroup').value = "";
  //  fm.all('BranchManager').value = "";
  //   fm.all('BranchManagername').value = "";
    
    return;
  }
  }
 
else
	{
		fm.all('BranchAttr').value="";
    fm.all('AgentGroup').value = "";
 //    fm.all('BranchManager').value = "";
 //    fm.all('BranchManagername').value = "";
     return;
  }
  //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);

  fm.all('AgentGroup').value  =tArr[0][0];
  // fm.all('BranchManager').value  =tArr[0][1];
 //  fm.all('BranchManagername').value  =tArr[0][2];
  
//  alert(fm.all('AgentGroup').value);
 
	}

// 查询按钮
function easyQueryClick() {
	
		// 初始化表格
     if (!verifyInput())
     {
     	return  false;
      }
     initLAAddressGrid()
	  
     var tReturn =getManageComLimitlike("a.ManageCom");
	//此处书写SQL语句			     
     var strSql = "select a.managecom,getUniteCode(a.agentcode),b.name,c.branchattr,c.name,a.flag  from  LAAnnuityFlag A,laagent b, "
 	        + " labranchgroup c  where a.agentcode=b.agentcode and b.agentgroup=c.agentgroup "
 	 +tReturn
    +  getWherePart("a.ManageCom", "ManageCom")
    + getWherePart("b.groupAgentCode", "GroupAgentCode")
    + getWherePart("a.Flag", "Flag")
    + getWherePart("b.Name", "AgentName")    
    + getWherePart("c.BranchAttr", "BranchAttr",'like')
    + getWherePart("c.name", "BranchName")
    + getWherePart("a.BranchType","BranchType")
    + getWherePart("a.BranchType2","BranchType2");
	turnPage.queryModal(strSql, LAAddressGrid); 
     if (!turnPage.strQueryResult) {
    alert("没有满足条件的信息！");
    return false;
    }	
}
function showOne(parm1, parm2) {	

}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LAAddressGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LAAddressGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LAAddressGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

function ListExecl()
{
//定义查询的数据
var strSQL = "";
strSQL = "select a.managecom,getUniteCode(a.agentcode),b.name,c.branchattr,c.name,a.flag  from  LAAnnuityFlag A,laagent b, "
 	        + " labranchgroup c  where a.agentcode=b.agentcode and b.agentgroup=c.agentgroup "
            +  getWherePart("a.ManageCom", "ManageCom")
            + getWherePart("b.groupAgentCode", "GroupAgentCode")
            + getWherePart("a.Flag", "Flag")
            + getWherePart("b.Name", "AgentName")    
            + getWherePart("c.BranchAttr", "BranchAttr",'like')
            + getWherePart("c.name", "BranchName")
            + getWherePart("a.BranchType","BranchType")
            + getWherePart("a.BranchType2","BranchType2");
     
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '管理机构','销售员代码','销售员姓名','销售单位代码','销售单位名称','养老金缴纳标记'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '养老金缴纳标记录入 查询下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}

