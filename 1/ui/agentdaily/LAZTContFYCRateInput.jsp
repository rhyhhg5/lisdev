<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAZTContFYCRateInput.jsp
//程序功能：团险职团非标准提奖比例管理
//创建日期：2008-07-08 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<% 

  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>

<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAZTContFYCRateInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAZTContFYCRateInit.jsp"%>
    <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务人员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAZTContFYCRateSave.jsp" method=post name=fm target="fraSubmit">
	<table class="common" align=center>
		<tr align=right>
			<td class=button>
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return addClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>
		</tr>
	</table>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		非标准业务提奖比例录入
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLAArchieve1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>

    <TD  class= title>
      保单号
    </TD>
    <TD  class= input>
      <Input class= 'common'   name=ContNo verify="保单号|notnull" elementtype=nacessary>
    </TD>
    <TD  class= title>
      所属薪资月
    </TD>
    <TD  class= input>
      <Input class= 'common'   name=WageNo verify="薪资月|notnull" elementtype=nacessary><font color="red">(yyyymm)
    </TD>
  </TR>

</table>

<BR>
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssButton onclick="easyQueryClick();">				
      </TD>
    </TR>
  </table>
     <p> <font color="#ff0000">注：非标比例不能高于标准比例  </font></p>
     <p> <font color="#ff0000">注：保单明细险种对应保费为负的记录，不允许进行非标比例的调整  </font></p>
<BR>

  	<Div  id= "divArchieve" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanArchieveGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="fmAction" name="BranchType">
    <input type=hidden id="fmAction" name="BranchType2">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
