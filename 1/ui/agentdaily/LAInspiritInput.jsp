<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAInspiritInput.jsp
//程序功能：
//创建日期：2006-1-5 16:36
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String agentcode=request.getParameter("AgentCode");
  if(agentcode==null || agentcode.equals(""))
  {
     agentcode="";
  }
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAInspiritInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="./LAInspiritInit.jsp"%>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "销售员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务人员";
  }
%>
<body onload="initForm();initElementtype();" >
  <form action="./LAInspiritSave.jsp" method=post name=fm target="fraSubmit">
    <table class="common" align=center>
		  <tr align=right>
			  <td class=button>
				  &nbsp;&nbsp;
			  </td>
			  <td class=button width="10%">
				  <INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return saveClick();">
			  </td>
			  <td class=button width="10%">
				  <INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
				</td>
			  <td class=button width="10%">
				  <INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return queryClick();">
				</td>
		  </tr>
	  </table>
    <table>
    	<tr>
    		<td>
    		     <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
          业务激励信息
       	</td>
    	</tr>
    </table>
    <Div  id= "divLAAccounts1" style= "display: ''">
<table class= common align='center' >
  <TR class= common>
    <TD class= title>
      活动起期日
    </TD>
    <TD class= input>
      <Input name=StartDate class="coolDatePicker" dateFormat="short" verify="活动起期日|NotNull" elementtype=nacessary >
    </TD>
    <TD class= title>
      活动止期日
    </TD>
    <TD class= input>
      <Input name=EndDate class="coolDatePicker" dateFormat="short" verify="活动止期日|NotNull" elementtype=nacessary >
    </TD>
  </TR>
  <TR class= common>
    <TD class= title>
      统计时间
    </TD>
    <TD class= input>
      <Input name=StatDate class="coolDatePicker" dateFormat="short" verify="统计时间|NotNull" elementtype=nacessary >
    </TD>
    <TD class= title>
      组织单位
    </TD>
    <TD class= input>
      <Input class= 'common' name=Organizer>
    </TD>
  </TR>
  <TR class= common>
    <TD class= title>
       获奖人代码
    </TD>
    <TD class= input>
      <Input name=AgentCode class="code" verify="获奖人代码|NOTNULL&code:Agentcode" id="AgentCode"
             ondblclick="EdorType(this,AgentCodeName);" onkeyup="KeyUp(this,AgentCodeName);"
             elementtype=nacessary >
    </TD>
    <TD class= title>
      获奖人姓名
    </TD>
    <TD class= input> 
      <Input class= 'readonly' readonly name=AgentCodeName>
    </TD>
  </TR>
  <TR class= common>
    <TD class= title>
      活动名称
    </TD>
    <TD class= input>
      <Input class= 'common' name=PloyName verify="活动名称|NotNull" elementtype=nacessary >
    </TD>
    <TD class= title>
      获奖称号
    </TD>
    <TD class= input>
      <Input class= 'common' name=InspiritName>
    </TD>
  </TR>
  <TR class= common>
    <TD class= title>
      奖励内容
    </TD>
    <TD class= input colSpan=3 >
      <Input class='common3' name=InspiritContent>
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="SeqNo" name="SeqNo">
    <input type=hidden id="Operate" name="Operate">
    <input type=hidden id="ManageCom" name="ManageCom">
    <input type=hidden id="BranchType" name="BranchType">
    <input type=hidden id="BranchType2" name="BranchType2">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
