<%@include file="../common/jsp/UsrCheck.jsp"%> 
<% 
  //程序名称：LAContSave.jsp
  //程序功能：
  //创建日期：2005-03-20 18:05:58 
  //创建人  ：CrtHtml程序创建
  //更新记录：  更新人    更新日期     更新原因/内容 
%> 
    <!--用户校验类--> 
   <%@page contentType="text/html;charset=GBK" %> 
   <%@page import="com.sinosoft.utility.*"%>
   <%@page import="com.sinosoft.lis.schema.*"%> 
   <%@page import="com.sinosoft.lis.vschema.*"%> 
   <%@page import="com.sinosoft.lis.pubfun.*"%> 
   <%@page import="com.sinosoft.lis.agentdaily.*"%> 
<% 
  //接收信息，并作校验处理。
  LAContSchema tLAContSchema   = new LAContSchema(); 
  LAContSet  tLAContSet = new LAContSet(); 
  String mProtocalNo="";
  LAAMContUI tLAAMContUI   = new LAAMContUI();
  //输出参数 
  CErrors tError = null;
  String tRela  = "";             
  String FlagStr = "";
  String Content = "";
  String transact = "";  
  String tOperate="INSERT||MAIN"; 
  String mTest =""; 
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录       
  transact = request.getParameter("fmtransact");
  String LAContType=request.getParameter("LAContType");
  System.out.println("LAContType:"+LAContType);
  System.out.println("BranchType2:"+request.getParameter("BranchType2"));
	String tProtocolNo = request.getParameter("ProtocolNo");
	if(!transact.equals("INSERT||MAIN"))
	{
		tLAContSchema.setProtocolNo(tProtocolNo);
		tLAContSchema.setManageCom(request.getParameter("HManageCom"));
	}else{
		tLAContSchema.setManageCom(request.getParameter("ManageCom"));
	}

  tLAContSchema.setSignDate(request.getParameter("SignDate"));
  //tLAContSchema.setManageCom(request.getParameter("ManageCom"));
  tLAContSchema.setAgentCom(request.getParameter("AgentCom"));
  tLAContSchema.setRepresentA(request.getParameter("RepresentA"));
  tLAContSchema.setRepresentB(request.getParameter("RepresentB"));
  tLAContSchema.setStartDate(request.getParameter("StartDate"));
  tLAContSchema.setEndDate(request.getParameter("EndDate"));
  


  System.out.println("transact"+transact);  
  System.out.println("end 档案信息...");


  
 // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(tLAContSchema);
  tVData.addElement(LAContType);
  try
  {
    tLAAMContUI.submitData(tVData,transact);
    
  }
  catch(Exception ex)
  {
 	ex.printStackTrace();
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLAAMContUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	mProtocalNo=(String)(tLAAMContUI.getResult().get(0));
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProtocalNo%>");
</script>
</html>
