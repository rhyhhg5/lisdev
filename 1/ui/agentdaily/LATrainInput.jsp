<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LATrainInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LATrainInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<%
  String tTitleAgent="";

  if("1".equals(BranchType))
  {
    tTitleAgent = "销售人员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();initElementtype();"  >
  <form action="./LATrainSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLATrain1);">
    <td class=titleImg>
      <%=tTitleAgent%>培训信息
    </td>
    </td>
    </tr>
    </table>
    
  <Div  id= "divLATrain1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  <%=tTitleAgent%>代码 
		</td>
        <td  class= input> 
		  <input class= common name=GroupAgentCode elementtype=nacessary onchange="return checkValid();"> 
		</td>
          <TD  class= title>
            <%=tTitleAgent%>姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name>
          </TD>
      </tr>
      <tr  class= common>
        <td  class= title> 
		  销售单位
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentGroup verify="销售单位|notnull&code:AgentGroup"> 
		</td> 
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ManageCom verify="管理机构|notnull&code:ManageCom"> 
		</td>
      </tr>
      <tr  class= common> 
        
        <td  class= title> 
		  培训类别
		</td>
        <td  class= input> 
		  <input name=AClass class="codeno" 
		   ondblclick="return showCodeList('trainaclass',[this,AClassName],[0,1],null,fm.all('BranchType').value,'ComCode',1);" 
		   onkeyup="return showCodeList('trainaclass',[this,AClassName],[0,1],null,fm.all('BranchType').value,'ComCode',1);" 
		   ><Input class=codename name=AClassName readOnly> 
		</td>
    <td  class= title> 
		  培训主办单位
		</td>
    <td  class= input> 
		  <input name=TrainUnit class= common > 
		</td>
    </tr>
    <tr class=common>
		<td  class= title> 
		  培训代码
		</td>
		
        <td  class= input> 
		  <input class= code name=TrainCode 
		   ondblclick="return showCodeList('traincourse',[this,TrainName],[0,1],null,fm.all('AClass').value,'OtherSign',1);" 
		   showCodeList('traincourse',[this,Name],[0,1],null,fm.all('AClass').value,'OtherSign',1);" > 
		</td>
      <td  class= title> 
		  培训名称 
		</td>
        <td  class= input> 
		  <input class="common"  name=TrainName verify="培训名称|notnull&code:Name"> 
		</td>
		</tr>
  <tr class=common>
    <td  class= title>
		  培训起始日期
		</td>
        <td  class= input>
		  <input name=TrainStart class='coolDatePicker' dateFormat='short' >
		</td>
    <td  class= title>
		  培训终止日期
		</td>
        <td  class= input>
		  <input name=TrainEnd class='coolDatePicker' dateFormat='short' >
		</td>
  </tr>
		<tr class=common>
        <td  class= title> 
		  主讲老师 
		</td>
        <td  class= input> 
		  <input name=Charger class= common  > 
		</td>
      
     
        <td  class= title> 
		  成绩
		</td>
        <td  class= input> 
		  <input name=Result class= common > 
		</td>
		</tr>
		<tr class=common>
        <td  class= title> 
		  成绩级别
		</td>
        <td  class= input> 
		  <input name=ResultLevel class="codeno" 
		   ondblclick="return showCodeList('ResultLevel',[this,ResultLevelName],[0,1]);" 
		   onkeyup="return showCodeListKey('ResultLevel',[this,ResultLevelName],[0,1]);" 
		   ><Input class=codename name=ResultLevelName readOnly> 
		</td>
     
        <td  class= title>
		  培训通过标志
		</td>
        <td  class= input>
		  <input name=TrainPassFlag class= "codeno"  
		   ondblclick="return showCodeList('yesno',[this,TrainPassFlagName],[0,1]);";
		   onkeyup="return showCodeListKey('yesno',[this,TrainPassFlagName],[0,1]);"
		   ><Input class=codename name=TrainPassFlagName readOnly elementtype=nacessary>
		</td>
		 </tr>
      <tr  class= common>
        <td  class= title>
		  备注
		</td>
        <td  class= input>
		  <input name=Noti class= common >
		</td>
     
        <td  class= title>
		  执行日期
		</td>
        <td  class= input>
		  <input name=DoneDate class='coolDatePicker' dateFormat='short' >
		</td>
		</tr>
  <tr class=common>
    <td  class= title>
		  操作员代码
		</td>
    <td  class= input>
		  <input name=Operator class="readonly" readonly >
		</td>
	  <td  class= title> 
		  最近操作日
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ModifyDate > 
		</td>
	</tr>	 
      </table>
  </Div>
    <input name=DoneFlag type = hidden class= 'code' ondblclick="return showCodeList('DoneFlag',[this]);";onkeyup="return showCodeListKey('DoneFlag',[this]);">
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=Idx value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=AgentCode value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
