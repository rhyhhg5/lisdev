//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  if(!chkMulLine()) 
  {
    return false;
  }
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
    
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick() 
{
  if (!verifyInput())
  {
    return false;	
  }
  initManageAllowRateGrid();
  var sql = "select b.agentgrade,"
  //+"(select gradename from laagentgrade where gradecode=b.agentgrade),"
  +"(select codename from ldcode where code=b.agentgrade and codetype='gradelevel'),"
             + "b.yearrate ,b.idx "
  			 +"from labasewage b "
  			 +"where b.type='12' and b.branchtype='3' and b.branchtype2='01' "
  		     + getWherePart('ManageCom', 'ManageCom'); 
	   	     +" order by b.agentgrade " ;
  turnPage.queryModal(sql, ManageAllowRateGrid);
  if(ManageAllowRateGrid.mulLineCount == 0)
  {
    alert("没有符合条件的管理津贴信息");
  }
}


//提交，修改按钮对应操作
function DoSave()
{
  fm.fmtransact.value = "UPDATE";
  if (!verifyInput())
  {
    return false;	 
  }
  if((fm.all('ManageCom').value=="")||(  fm.all('ManageCom').value==null))
  {
 	alert("管理机构不能为空！");
 	return false;
  }
  if(!beforeSubmit()) 
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function DoInsert()
{
  fm.fmtransact.value = "INSERT" ;
  if (!verifyInput())
  {
    return false;	 
  }
  if((fm.all('ManageCom').value=="")||(  fm.all('ManageCom').value==null))
  {
 	alert("管理机构不能为空！");
 	return false;
  }
  if(!beforeSubmit()) 
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{
  var i;
  var selFlag = true;
  var iCount = 0;
  var manageCom=fm.all('ManageCom').value;
  var rowNum = ManageAllowRateGrid.mulLineCount;
  for(i=0;i<rowNum;i++)
  {
    if(ManageAllowRateGrid.getChkNo(i))
    {
      iCount++;
      if((ManageAllowRateGrid.getRowColData(i,1) == null)||(ManageAllowRateGrid.getRowColData(i,1) == ""))
      {
        alert("第"+(i+1)+"行职级代码不能为空");
        ManageAllowRateGrid.setFocus(i,1,ManageAllowRateGrid);
        selFlag = false;
        break;
      }
      if((ManageAllowRateGrid.getRowColData(i,3) == null)||(ManageAllowRateGrid.getRowColData(i,3) == ""))
      {
        alert("第"+(i+1)+"行管理津贴不能为空");
        ManageAllowRateGrid.setFocus(i,1,ManageAllowRateGrid);
        selFlag = false;
        break;
      }
      //如果idx为空不能修改
      if((ManageAllowRateGrid.getRowColData(i,4) == null)||(ManageAllowRateGrid.getRowColData(i,4)=="") || (ManageAllowRateGrid.getRowColData(i,4)==0))
      {
        if(fm.fmtransact.value == "UPDATE")
        {
          alert("有未保存的新增纪录！不可修改，请先保存记录。");
          ManageAllowRateGrid.checkBoxAllNot();
          ManageAllowRateGrid.setFocus(i,1,ManageAllowRateGrid);
          selFlag = false;
          break;
        }
        else if (fm.fmtransact.value == "INSERT")
        {
          var strsql="select idx from labasewage  where agentgrade='"+(ManageAllowRateGrid.getRowColData(i,1))+"' and managecom='"+manageCom+"' and type = '12'";
          var arrResult = new Array();
          arrResult = easyExecSql(strsql);
          if (arrResult) 
          {
            alert("序号"+(i+1)+"纪录已存在，不可保存！");
            ManageAllowRateGrid.checkBoxAllNot();
            ManageAllowRateGrid.setFocus(i,1,ManageAllowRateGrid);
            selFlag = false;
            break;
          }
        }
      }
      else 
      {	//如果idx不为空不能插入
        if(fm.fmtransact.value == "INSERT")
        {
          alert("序号"+(i+1)+"纪录已存在，不可保存！");
          ManageAllowRateGrid.checkBoxAllNot();
          ManageAllowRateGrid.setFocus(i,1,ManageAllowRateGrid);
          selFlag = false;
          break;
        }
        else if(fm.fmtransact.value == "UPDATE")
        {
          var tsql="select agentgrade from labasewage  where idx="+(ManageAllowRateGrid.getRowColData(i,4));
          var strQueryResult = easyQueryVer3(tsql, 1, 1, 1);
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);	
          var grade=tArr[0][0];
          if(grade!==(ManageAllowRateGrid.getRowColData(i,1)))
          {
            alert("不能变更职级修改！");
            ManageAllowRateGrid.checkBoxAllNot();
            ManageAllowRateGrid.setFocus(i,1,ManageAllowRateGrid);
            selFlag = false;
            break;
          }
        }
      }
    }
    else
    {//不是选中的行
      if((ManageAllowRateGrid.getRowColData(i,4) == null)||(ManageAllowRateGrid.getRowColData(i,4)==""))
      {
        alert("有未保存的新增纪录,请先保存记录!");
        ManageAllowRateGrid.checkBoxAllNot();
        ManageAllowRateGrid.setFocus(i,1,ManageAllowRateGrid);
        selFlag = false;
        break;
      }
    }
  }
  if(!selFlag) return selFlag;
  if(iCount == 0)
  {
    alert("请选择要保存或修改的记录!");
    return false;
  }
  return true;
}



