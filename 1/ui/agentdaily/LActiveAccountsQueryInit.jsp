<%
//程序名称：LAAccountsQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:03:35
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('AgentCode').value = "";
    fm.all('Account').value = "";
    fm.all('State').value = "";
    
    fm.all('OpenDate').value = "";
    fm.all('DestoryDate').value = "";
    fm.all('AccountName').value = "";

  }
  catch(ex) {
    alert("在LAAccountsQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLAAccountsGrid();  
  }
  catch(re) {
    alert("LAAccountsQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LAAccountsGrid;
function initLAAccountsGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="营销员编码";          		        //列名
    iArray[1][1]="60px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[2]=new Array();
    iArray[2][0]="帐号";          		        //列名
    iArray[2][1]="60px";      	      		//列宽
    iArray[2][2]=30;            			//列最大值
    iArray[2][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    
    iArray[3]=new Array();
    iArray[3][0]="帐号名称";          		        //列名
    iArray[3][1]="60px";      	      		//列宽
    iArray[3][2]=30;            			//列最大值
    iArray[3][3]=1;             //是否允许输入,    
        
    iArray[4]=new Array();
    iArray[4][0]="状态";          		        //列名
    iArray[4][1]="30px";      	      		//列宽
    iArray[4][2]=10;            			//列最大值
    iArray[4][3]=1;             //是否允许输入,

    
    iArray[5]=new Array();
    iArray[5][0]="开户日期";          		        //列名
    iArray[5][1]="60px";      	      		//列宽
    iArray[5][2]=30;            			//列最大值
    iArray[5][3]=1;             //是否允许输入,
    
    iArray[6]=new Array();
    iArray[6][0]="销户日期";          		        //列名
    iArray[6][1]="60px";      	      		//列宽
    iArray[6][2]=30;            			//列最大值
    iArray[6][3]=1;             //是否允许输入,
    
    iArray[7]=new Array();
    iArray[7][0]="开户银行";          		        //列名
    iArray[7][1]="60px";      	      		//列宽
    iArray[7][2]=30;            			//列最大值
    iArray[7][3]=1;             //是否允许输入,
 
         
    
    iArray[8]=new Array();
    iArray[8][0]="操作员代码";          		        //列名
    iArray[8][1]="0px";      	      		//列宽
    iArray[8][2]=30;            			//列最大值
    iArray[8][3]=3;             //是否允许输入,
    
    
    iArray[9]=new Array();
    iArray[9][0]="最近操作日";          		        //列名
    iArray[9][1]="0px";      	      		//列宽
    iArray[9][2]=30;            			//列最大值
    iArray[9][3]=3;             //是否允许输入,
    
    iArray[10]=new Array();
    iArray[10][0]="管理机构";          		        //列名
    iArray[10][1]="0px";      	      		//列宽
    iArray[10][2]=30;            			//列最大值
    iArray[10][3]=3;             //是否允许输入,
    
    iArray[11]=new Array();
    iArray[11][0]="开户银行";          		        //列名
    iArray[11][1]="0px";      	      		//列宽
    iArray[11][2]=30;            			//列最大值
    iArray[11][3]=3;             //是否允许输入,
 
    LAAccountsGrid = new MulLineEnter( "fm" , "LAAccountsGrid" ); 
    //这些属性必须在loadMulLine前

    LAAccountsGrid.mulLineCount = 0;   
    LAAccountsGrid.displayTitle = 1;
    LAAccountsGrid.hiddenPlus = 1;
    LAAccountsGrid.hiddenSubtraction = 1;
    LAAccountsGrid.canSel = 1;
    LAAccountsGrid.canChk = 0;
  //  LAAccountsGrid.selBoxEventFuncName = "showOne";

    LAAccountsGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAAccountsGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
