/** 
 * 程序名称：LHHealthServPlanQueryInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-03-20 17:20:48
 * 创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
 			 
	var LEN = manageCom.length;
  if(LEN == 8){LEN = 4; }   
	var strSql = "select  distinct a.ServPlanCode,a.ServPlanName,"
	          +"(select  CodeName from LDCode where codetype = 'servplanlevel' and ldcode.code=a.ServPlanLevel),"
	          +"(select showname from ldcom  where  ldcom.comcode=a.ComID),"
	          +"a.ServItemCode,a.ServItemName,a.ServItemSN"
	          +" from LHHealthServPlan a "
				    +" where a.managecom like '"+manageCom.substring(0,LEN)+"%%'"
				    + getWherePart("a.ServPlanCode", "ServPlanCode", "like")
				    + getWherePart("a.ServPlanName", "ServPlanName", "like")
				    + getWherePart("a.ServPlanLevel", "ServPlanLevelCode", "like")
				    + getWherePart("a.ComID", "ComIDCode", "like")
				    + getWherePart("a.ServItemCode", "ServItemCode", "like")
  					;
 // alert(strSql);
//  alert(easyExecSql(strSql));
	turnPage.queryModal(strSql, LHHealthServPlanGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");

}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHHealthServPlanGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHHealthServPlanGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LHHealthServPlanGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
