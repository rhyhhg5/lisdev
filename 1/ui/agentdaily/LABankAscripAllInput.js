//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
 fm.fmtransact.value = "INSERT||MAIN" ;
 var i = 0;
 if (!beforeSubmit())
 return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "INSERT||MAIN";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
    initForm();
 //   showDiv(operateButton,"true"); 
 //   showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAddress.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作

 
//提交前的校验、计算  
function beforeSubmit()
{
if(!verifyInput()) return false;
//if(!chkMulLine()) return false;

if(fm.all('NewAgentCode').value==null || fm.all('NewAgentCode').value==''){
	alert('请选择要归属的新业务员！');
	return false;
}

if(trim(fm.all('NewAgentCode').value)==trim(fm.all('AgentCode').value)){
	alert("业务员没有变更，请选择新的业务员!");
	return false;
}
	saveClick();
	return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}



//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数

function queryClick()
{
	//下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./ALAAscriptionQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);	
	}
	
function easyQueryClick() 
 {  //此处书写SQL语句	
   if(!verifyInput()) return false;
   	    var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSql="";
   	     strSql = "select a.contno,a.signdate,a.cvalidate,a.cinvalidate,a.paytodate,a.prem " 
               +",a.managecom,c.branchattr,getunitecode(a.agentcode),b.name,a.agentcom,c.branchtype"
                +",(select getunitecode(agentcode) from lacomtoagent where agentcom=a.agentcom and relatype='1') "
               +",(select name from laagent where agentcode=(select agentcode from lacomtoagent where agentcom=a.agentcom and relatype='1')) " 
               +",a.agentcom "
    		       +" from lccont a,laagent b,labranchgroup c  " 
			         +" where  a.agentcode=b.agentcode  " 
			         +" and b.agentgroup=c.agentgroup and a.uwflag<>'a' and a.grpcontno='00000000000000000000' "
			         //校验是否正在做理赔或者保全
//+ " and not exists (select '1' from llcase aa, LLClaimDetail bb where aa.caseno = bb.caseno  and bb.contno = a.contno  and aa.rgtstate not in ('11','12','14'))"
//+" and not exists (select '1' from LPEdorItem cc, lpedorapp dd where cc.ContNo =a.contno  and cc.edoracceptno= dd.edoracceptno and dd.EdorState !='0' )"
               +" and a.signdate is not null  and c.branchtype='3'  and c.branchtype2='01' "               
	             + " and a.ManageCom  like '"+fm.all('ManageCom').value+"%'"
	             //+ getWherePart('a.AgentCode','AgentCode')
	             + strAgent
         + getWherePart('a.ContNo','ContNo')
	             + getWherePart('a.AgentCom','AgentCom')
	             + getWherePart('c.BranchAttr','BranchAttr');
	      if(fm.all('PayIntv').value!=''&& fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="0")
	      {
	      var strSql=strSql+ " and a.payintv=0" ;
	      }
	      if(fm.all('PayIntv').value!='' && fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="12")
	      {
	      var strSql=strSql+ " and a.payintv=12" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="0")
	      {
	      var strSql=strSql+ " and a.stateflag='0'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="1")
	      {
	      var strSql=strSql+ " and a.stateflag='1'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="2")
	      {
	      var strSql=strSql+ " and a.stateflag='2'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="3")
	      {
	      var strSql=strSql+ " and a.contno in (select lcc.contno from lccont lcc where lcc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lcc.stateflag='3' union all select lbc.contno from lbcont lbc where lbc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lbc.stateflag='3')" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="4")
	      {
	      var strSql=strSql+ " and a.stateflag='4'" ;
	      }
	      if(fm.all('AgentState').value=='01'){
	      	var strSql=strSql+ " and b.agentstate<='02'" ;
	      	}
	      if(fm.all('AgentState').value=='02'){
	      	var strSql=strSql+ " and b.agentstate>='03'" ;
	      	}	
        if(fm.all('RiskCode').value!=""&&fm.all('RiskCode').value!=null){
        	var strSql=strSql+ " and a.contno in (select distinct contno from lcpol where riskcode='"
        	+fm.all('RiskCode').value+"' and agentcode=getagentcode('"+fm.all('AgentCode').value+"')) " ;
        	}
        else {
        	var strSql=strSql+ " and a.contno in (select distinct contno from lcpol where exists (select 1 from lmriskapp where riskcode=lcpol.riskcode) and agentcode=getagentcode('"+fm.all('AgentCode').value+"' )) " ;
        	}
           strSql += "union select a.grpcontno,a.signdate,a.cvalidate,a.cinvalidate,(select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo and (StateFlag is null or StateFlag != '3')),a.prem " 
               +",a.managecom,c.branchattr,getunitecode(a.agentcode),b.name,a.agentcom,c.branchtype"
                +",(select getunitecode(agentcode) from lacomtoagent where agentcom=a.agentcom and relatype='1') "
               +",(select name from laagent where agentcode=(select agentcode from lacomtoagent where agentcom=a.agentcom and relatype='1')) " 
               +",a.agentcom "
    		       +" from lcgrpcont a,laagent b,labranchgroup c  " 
			         +" where  a.agentcode=b.agentcode  " 
			         +" and b.agentgroup=c.agentgroup and a.uwflag<>'a' and a.grpcontno<>'00000000000000000000' "
			         //校验是否正在做理赔或者保全
//+ " and not exists (select '1' from llcase aa, LLClaimDetail bb where aa.caseno = bb.caseno  and bb.contno = a.contno  and aa.rgtstate not in ('11','12','14'))"
//+" and not exists (select '1' from LPEdorItem cc, lpedorapp dd where cc.ContNo =a.contno  and cc.edoracceptno= dd.edoracceptno and dd.EdorState !='0' )"
               +" and a.signdate is not null  and c.branchtype='3'  and c.branchtype2='01' "               
	             + " and a.ManageCom  like '"+fm.all('ManageCom').value+"%'"
	             //+ getWherePart('a.AgentCode','AgentCode')
	             + strAgent
         + getWherePart('a.grpContNo','ContNo')
	             + getWherePart('a.AgentCom','AgentCom')
	             + getWherePart('c.BranchAttr','BranchAttr');
	      if(fm.all('PayIntv').value!=''&& fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="0")
	      {
	      var strSql=strSql+ " and a.payintv=0" ;
	      }
	      if(fm.all('PayIntv').value!='' && fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="12")
	      {
	      var strSql=strSql+ " and a.payintv=12" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="0")
	      {
	      var strSql=strSql+ " and a.stateflag='0'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="1")
	      {
	      var strSql=strSql+ " and a.stateflag='1'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="2")
	      {
	      var strSql=strSql+ " and a.stateflag='2'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="3")
	      {
	      var strSql=strSql+ " and a.grpcontno in (select lcc.grpcontno from lcgrpcont lcc where lcc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lcc.stateflag='3' union all select lbc.grpcontno from lbgrpcont lbc where lbc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lbc.stateflag='3')" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="4")
	      {
	      var strSql=strSql+ " and a.stateflag='4'" ;
	      }
	      if(fm.all('AgentState').value=='01'){
	      	var strSql=strSql+ " and b.agentstate<='02'" ;
	      	}
	      if(fm.all('AgentState').value=='02'){
	      	var strSql=strSql+ " and b.agentstate>='03'" ;
	      	}	
        if(fm.all('RiskCode').value!=""&&fm.all('RiskCode').value!=null){
        	var strSql=strSql+ " and a.grpcontno in (select distinct grpcontno from lcgrppol where riskcode='"
        	+fm.all('RiskCode').value+"' and agentcode=getagentcode('"+fm.all('AgentCode').value+"')) " ;
        	}
        else {
        	var strSql=strSql+ " and a.grpcontno in (select distinct grpcontno from lcgrppol where exists (select 1 from lmriskapp where riskcode=lcgrppol.riskcode) and agentcode=getagentcode('"+fm.all('AgentCode').value+"' )) " ;
        	}
       
    turnPage.queryModal(strSql, AscriptionGrid);
    fm.all('querySQL').value = strSql;
    if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据");
    return false;
    }	
}

function saveClick() 
{
   //此处书写SQL语句	
   if(!verifyInput()) return false;
   	    var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSql ="";
   	     strSql = "select a.contno,a.signdate,a.cvalidate,a.cinvalidate,a.paytodate,a.prem " 
               +",a.managecom,c.branchattr,getunitecode(a.agentcode),b.name,a.agentcom,c.branchtype"
                +",(select getunitecode(agentcode) from lacomtoagent where agentcom=a.agentcom and relatype='1') "
               +",(select name from laagent where agentcode=(select agentcode from lacomtoagent where agentcom=a.agentcom and relatype='1')) " 
               +",a.agentcom "
    		       +" from lccont a,laagent b,labranchgroup c  " 
			         +" where  a.agentcode=b.agentcode  " 
			         +" and b.agentgroup=c.agentgroup and a.uwflag<>'a' and a.grpcontno='00000000000000000000' "
			         //校验是否正在做理赔或者保全
//+ " and not exists (select '1' from llcase aa, LLClaimDetail bb where aa.caseno = bb.caseno  and bb.contno = a.contno  and aa.rgtstate not in ('11','12','14'))"
//+" and not exists (select '1' from LPEdorItem cc, lpedorapp dd where cc.ContNo =a.contno  and cc.edoracceptno= dd.edoracceptno and dd.EdorState !='0' )"
               +" and a.signdate is not null  and c.branchtype='3'  and c.branchtype2='01' "               
	             + " and a.ManageCom  like '"+fm.all('ManageCom').value+"%'"
	             //+ getWherePart('a.AgentCode','AgentCode')
	             + strAgent
	             + getWherePart('a.ContNo','ContNo')
	             + getWherePart('a.AgentCom','AgentCom')
	             + getWherePart('c.BranchAttr','BranchAttr');
	      if(fm.all('PayIntv').value!=''&& fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="0")
	      {
	      var strSql=strSql+ " and a.payintv=0" ;
	      }
	      if(fm.all('PayIntv').value!='' && fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="12")
	      {
	      var strSql=strSql+ " and a.payintv=12" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="0")
	      {
	      var strSql=strSql+ " and a.stateflag='0'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="1")
	      {
	      var strSql=strSql+ " and a.stateflag='1'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="2")
	      {
	      var strSql=strSql+ " and a.stateflag='2'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="3")
	      {
	      var strSql=strSql+ " and a.contno in (select lcc.contno from lccont lcc where lcc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lcc.stateflag='3' union all select lbc.contno from lbcont lbc where lbc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lbc.stateflag='3')" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="4")
	      {
	      var strSql=strSql+ " and a.stateflag='4'" ;
	      }
	      if(fm.all('AgentState').value=='01'){
	      	var strSql=strSql+ " and b.agentstate<='02'" ;
	      	}
	      if(fm.all('AgentState').value=='02'){
	      	var strSql=strSql+ " and b.agentstate>='03'" ;
	      	}	
        if(fm.all('RiskCode').value!=""&&fm.all('RiskCode').value!=null){
        	var strSql=strSql+ " and a.contno in (select distinct contno from lcpol where riskcode='"
        	+fm.all('RiskCode').value+"' and agentcode=getagentcode('"+fm.all('AgentCode').value+"') ) " ;
        	}
        else {
        	var strSql=strSql+ " and a.contno in (select distinct contno from lcpol where exists (select 1 from lmriskapp where riskcode=lcpol.riskcode) and agentcode=getagentcode('"+fm.all('AgentCode').value+"') ) " ;
        	}
          strSql += "union select a.grpcontno,a.signdate,a.cvalidate,a.cinvalidate,(select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo and (StateFlag is null or StateFlag != '3')),a.prem " 
               +",a.managecom,c.branchattr,getunitecode(a.agentcode),b.name,a.agentcom,c.branchtype"
                +",(select getunitecode(agentcode) from lacomtoagent where agentcom=a.agentcom and relatype='1') "
               +",(select name from laagent where agentcode=(select agentcode from lacomtoagent where agentcom=a.agentcom and relatype='1')) " 
               +",a.agentcom "
    		       +" from lcgrpcont a,laagent b,labranchgroup c  " 
			         +" where  a.agentcode=b.agentcode  " 
			         +" and b.agentgroup=c.agentgroup and a.uwflag<>'a' and a.grpcontno<>'00000000000000000000' "
			         //校验是否正在做理赔或者保全
//+ " and not exists (select '1' from llcase aa, LLClaimDetail bb where aa.caseno = bb.caseno  and bb.contno = a.contno  and aa.rgtstate not in ('11','12','14'))"
//+" and not exists (select '1' from LPEdorItem cc, lpedorapp dd where cc.ContNo =a.contno  and cc.edoracceptno= dd.edoracceptno and dd.EdorState !='0' )"
               +" and a.signdate is not null  and c.branchtype='3'  and c.branchtype2='01' "               
	             + " and a.ManageCom  like '"+fm.all('ManageCom').value+"%'"
	             //+ getWherePart('a.AgentCode','AgentCode')
	             + strAgent
         + getWherePart('a.grpContNo','ContNo')
	             + getWherePart('a.AgentCom','AgentCom')
	             + getWherePart('c.BranchAttr','BranchAttr');
	      if(fm.all('PayIntv').value!=''&& fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="0")
	      {
	      var strSql=strSql+ " and a.payintv=0" ;
	      }
	      if(fm.all('PayIntv').value!='' && fm.all('PayIntv').value!=null && fm.all('PayIntv').value=="12")
	      {
	      var strSql=strSql+ " and a.payintv=12" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="0")
	      {
	      var strSql=strSql+ " and a.stateflag='0'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="1")
	      {
	      var strSql=strSql+ " and a.stateflag='1'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="2")
	      {
	      var strSql=strSql+ " and a.stateflag='2'" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="3")
	      {
	      var strSql=strSql+ " and a.grpcontno in (select lcc.grpcontno from lcgrpcont lcc where lcc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lcc.stateflag='3' union all select lbc.grpcontno from lbgrpcont lbc where lbc.agentcode=getagentcode('"+fm.all('AgentCode').value+"') and lbc.stateflag='3')" ;
	      }
	      if(fm.all('StateFlag').value!='' && fm.all('StateFlag').value!=null && fm.all('StateFlag').value=="4")
	      {
	      var strSql=strSql+ " and a.stateflag='4'" ;
	      }
	      if(fm.all('AgentState').value=='01'){
	      	var strSql=strSql+ " and b.agentstate<='02'" ;
	      	}
	      if(fm.all('AgentState').value=='02'){
	      	var strSql=strSql+ " and b.agentstate>='03'" ;
	      	}	
        if(fm.all('RiskCode').value!=""&&fm.all('RiskCode').value!=null){
        	var strSql=strSql+ " and a.grpcontno in (select distinct grpcontno from lcgrppol where riskcode='"
        	+fm.all('RiskCode').value+"' and agentcode=getagentcode('"+fm.all('AgentCode').value+"')) " ;
        	}
        else {
        	var strSql=strSql+ " and a.grpcontno in (select distinct grpcontno from lcgrppol where exists (select 1 from lmriskapp where riskcode=lcgrppol.riskcode) and agentcode=getagentcode('"+fm.all('AgentCode').value+"' )) " ;
        	}
        
    fm.all('querySQL').value = strSql;
    //alert(strSql);
}


//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = AscriptionGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(AscriptionGrid.getChkNo(i))
		{
			iCount++;
			
			if((AscriptionGrid.getRowColData(i,13) == null)||(AscriptionGrid.getRowColData(i,13)=="") || (AscriptionGrid.getRowColData(i,13)==0))
			{
				alert("第"+(i+1)+"行没有指定新的业务员");
				AscriptionGrid.setFocus(i,1,AscriptionGrid);
				selFlag = false;
				break;
      }
			if(AscriptionGrid.getRowColData(i,9) == AscriptionGrid.getRowColData(i,13))
			{
				alert("第"+(i+1)+"行没有变更业务员");
				AscriptionGrid.setFocus(i,1,AscriptionGrid);
				selFlag = false;
				break;
			}
			if(((AscriptionGrid.getRowColData(i,11)!=null)&&(AscriptionGrid.getRowColData(i,11)!="")) &&((AscriptionGrid.getRowColData(i,15) ==null||AscriptionGrid.getRowColData(i,15)=="")))
			{
			    alert("第"+(i+1)+"行保单为直销业务，不能指定代理机构");
				AscriptionGrid.setFocus(i,1,AscriptionGrid);
				selFlag = false;
				break;
			}
			if(((AscriptionGrid.getRowColData(i,11)==null)&&(AscriptionGrid.getRowColData(i,11)=="")) &&((AscriptionGrid.getRowColData(i,15) !=null||AscriptionGrid.getRowColData(i,15)!="")))
			{
			    alert("第"+(i+1)+"行保单为银行代理业务，必须指定代理机构");
				AscriptionGrid.setFocus(i,1,AscriptionGrid);
				selFlag = false;
				break;
			}
			var oldagentcode=AscriptionGrid.getRowColData(i,9)
			var newagentcode=AscriptionGrid.getRowColData(i,13)
			var sql="select getunitecode(agentcode) from laagent where agentcode=getagentcode('"+newagentcode+"') and agentstate<'03'";
			var oldmanagecom="select managecom from laagent where agentcode=getagentcode('"+oldagentcode+"')";
			var newmanagecom="select managecom from laagent where agentcode=getagentcode('"+newagentcode+"')";
			var arrResult = easyExecSql(sql);
			var oldarrResult = easyExecSql(oldmanagecom);
			var newarrResult = easyExecSql(newmanagecom);
			if(!arrResult)
			{
			     alert(newagentcode+"不存在或已离职，请重新指定新代理人")
			     return false;
			}
			if(oldarrResult[0][0]!=newarrResult[0][0])
			{
			     alert("新代理人与原代理人必须在同一管理机构");
			     return false;
			}
		}
		else
		{//不是选中的行
					if((AscriptionGrid.getRowColData(i,4) == null)||(AscriptionGrid.getRowColData(i,4)==""))
					{
						alert("有未保存的新增纪录,请先保存记录!");
						AscriptionGrid.checkBoxAllNot();
						AscriptionGrid.setFocus(i,1,AscriptionGrid);
						selFlag = false;
						break;
					}
		}
	}
	if(!selFlag) return selFlag;
	if(iCount == 0)
	{
			alert("请选择要保存的记录!");
			return false;
	}
	return true;
}


function getNewAgentCode(cObj,cName){
	var managecom = fm.all('ManageCom').value;
  if (managecom ==null || trim(managecom) == '')
  {
  	alert("请先选择管理机构！");
  	return false;
  } 
	var msql ="1 and managecom like #"+managecom+"%#";
	showCodeList('agentcodet3',[cObj,cName],[0,1],null,msql,1,1);
}



function afterCodeSelect(codeName,Field)
{
	if(codeName == "comcode")
	{
	   fm.all('NewAgentCode').value = '';
	   fm.all('NewAgentName').value = '';
	   fm.all('querySQL').value = '';
	}
}