<%
//程序名称：LAAddressQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:07:04
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
  }
  catch(ex) {
    alert("在LAAddressQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    
    initInpBox();
    initALACaseGrid();  
  }
  catch(re) {
    alert("LAAddressQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ALACaseGrid;
function initALACaseGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="立案编号";         		//列名
    iArray[1][1]="100px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="立案日期";         		//列名
    iArray[2][1]="70px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="投保单号";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="保险单号";         		//列名
    iArray[4][1]="80px";         		//宽度
    iArray[4][3]=200;         		//最大长度
    iArray[4][4]="0";         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array(); 
    iArray[5][0]="营销员代码";   //列名
    iArray[5][1]="70px"        //宽度
    iArray[5][3]=200;        //最大长度
    iArray[5][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();  
    iArray[6][0]="营销员姓名";         		//列名 
    iArray[6][1]="70px"        //宽度 
    iArray[6][3]=200;        //最大长度 
    iArray[6][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();  
    iArray[7][0]="投保人号";         		//列名 
    iArray[7][1]="0px"        //宽度 
    iArray[7][3]=200;        //最大长度 
    iArray[7][4]="0";  //是否允许录入，0--不能，1--允许
    
 //   iArray[8]=new Array();  
 //   iArray[8][0]="扣分值";         		//列名 AgentGroup
 //   iArray[8][1]="70px"        //宽度 
//    iArray[8][3]=200;        //最大长度 
//    iArray[8][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();  
    iArray[8][0]="投保人姓名";         		//列名 AgentGroup
    iArray[8][1]="70px"        //宽度 
    iArray[8][3]=200;        //最大长度 
    iArray[8][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();  
    iArray[9][0]="被保人号";         		//列名 AgentGroup
    iArray[9][1]="0px"        //宽度 
    iArray[9][3]=200;        //最大长度 
    iArray[9][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();  
    iArray[10][0]="被保人号";         		//列名 AgentGroup
    iArray[10][1]="0px"        //宽度 
    iArray[10][3]=200;        //最大长度 
    iArray[10][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();  
    iArray[11][0]="被保人号";         		//列名 AgentGroup
    iArray[11][1]="0px"        //宽度 
    iArray[11][3]=200;        //最大长度 
    iArray[11][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();  
    iArray[12][0]="被保人姓名";         		//列名 AgentGroup
    iArray[12][1]="70px"        //宽度 6
    iArray[12][3]=200;        //最大长度 
    iArray[12][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();  
    iArray[13][0]="被保人姓名";         		//列名 AgentGroup
    iArray[13][1]="70px"        //宽度 
    iArray[13][3]=200;        //最大长度 
    iArray[13][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();  
    iArray[14][0]="被保人姓名";         		//列名 AgentGroup
    iArray[14][1]="70px"        //宽度 
    iArray[14][3]=200;        //最大长度 
    iArray[14][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[15]=new Array();  
    iArray[15][0]="直接损失";         		//列名 AgentGroup
    iArray[15][1]="70px"        //宽度 
    iArray[15][3]=200;        //最大长度 
    iArray[15][4]="0";  //是否允许录入，0--不能，1--允许        
    
    iArray[16]=new Array();  
    iArray[16][0]="追回损失";         		//列名 AgentGroup
    iArray[16][1]="70px"        //宽度 
    iArray[16][3]=200;        //最大长度 
    iArray[16][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();  
    iArray[17][0]="结案日期";         		//列名 AgentGroup
    iArray[17][1]="70px"        //宽度 
    iArray[17][3]=200;        //最大长度 
    iArray[17][4]="0";  //是否允许录入，0--不能，1--允许 
             
    iArray[18]=new Array();  
    iArray[18][0]="操作员代码";         		//列名 AgentGroup
    iArray[18][1]="70px"        //宽度 
    iArray[18][3]=200;        //最大长度 
    iArray[18][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[19]=new Array();  
    iArray[19][0]="操作日期";         		//列名 AgentGroup
    iArray[19][1]="70px"        //宽度 
    iArray[19][3]=200;        //最大长度 
    iArray[19][4]="0";  //是否允许录入，0--不能，1--允许        
    
    iArray[20]=new Array();  
    iArray[20][0]="备注";         		//列名 AgentGroup
    iArray[20][1]="200px"        //宽度 
    iArray[20][3]=200;        //最大长度 
    iArray[20][4]="0";  //是否允许录入，0--不能，1--允许                                                                                                              
    ALACaseGrid = new MulLineEnter( "fm" , "ALACaseGrid" ); 
    //这些属性必须在loadMulLine前
 
    ALACaseGrid.mulLineCount = 0;   
    ALACaseGrid.displayTitle = 1;
    ALACaseGrid.hiddenPlus = 1;
    ALACaseGrid.hiddenSubtraction = 1;
    ALACaseGrid.canSel = 1;
    ALACaseGrid.canChk = 0;
    ALACaseGrid.selBoxEventFuncName = "showOne";
 
    ALACaseGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAAddressGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
