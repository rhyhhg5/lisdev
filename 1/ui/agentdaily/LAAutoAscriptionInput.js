//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
 var i = 0;
 if (!beforeSubmit())
 return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "INSERT||MAIN";
  fm.action="LAAutoAscriptionSave.jsp";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
 //   showDiv(operateButton,"true"); 
 //   showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
 

 

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAddress.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作

 
//提交前的校验、计算  
function beforeSubmit()
{
if(!verifyInput()) return false;
/*
if(fm.all('Address').value==""||fm.all('Address').value=null);
{
	alert("请输入销售单位销售地址！");
   fm.all('Address').focus();
    return false;	 
	}
if(fm.all('Address').value==""||fm.all('Address').value=null);
{
	//alert("请输入销售单位销售地址！");
  //  fm.all('Address').focus();
    return false;	 
	}

if(fm.all('ZipCode').value==""||fm.all('ZipCode').value=null);
{
	alert("请输入邮政编码！");
 //   fm.all('ZipCode').focus();
    return false;	 
	}
	*/
	return true;
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function checkagentcode()
{ 
// var tReturn = parseManageComLimitlike();
	var srtSQL ;
 strSQL = "select * from LAAgent where  InsideFlag<>'0'  "
         // +tReturn
	  + getWherePart('groupagentcode','GroupAgentCode')
	  //+getWherePart('AgentCode')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');   
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("没有此代理人！");
    fm.all('groupagentcode').value="";
    fm.all('Name').value="";
    return ;
  } 
  var ttArr = new Array();
  ttArr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentCode').value =ttArr[0][0]; 
}
function checkagentname()
{
// var tReturn = parseManageComLimitlike();
 strSQL = "select * from LAAgent where  InsideFlag<>'0'  "
       //   +tReturn
	  + getWherePart('Name')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');   
  
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("没有此代理人！");
    fm.all('GroupAgentCode').value="";
    fm.all('Name').value="";
    return ;
  } 
  

}



//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数

function queryClick()
{
	//下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./ALAAscriptionQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);	
	}
	
function easyQueryClick() 
{
   //此处书写SQL语句	
   initAscriptionGrid();
   if(!verifyInput()) return false;
   var strAgent = " " ;
   if(fm.GroupAgentCode.value != null && fm.GroupAgentCode.value != ""){
   	 strAgent = " and b.AgentCode = getAgentCode('"+fm.GroupAgentCode.value+"')";
   }
   var strSql ="select a.GrpContNo,a.ContNo, getUniteCode(a.AgentOld),(select name from laagent where agentcode=a.AgentOld)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentOld))," 
			   +" getUniteCode(a.AgentNew),(select name from laagent where agentcode=a.AgentNew)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentNew))," 
			   +"a.AClass, a.AscriptionDate,case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认' else '确认' end  "
	           + " ,d.appntname ,case when d.appntsex='0' then '男' else '女' end,(select year((current date)-(d.appntbirthday)) from dual)," 
               +"  (select e.mobile from lcaddress e where e.customerno = d.appntno and " 
               +"e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno)),d.prem,"
               +"(select agentgroup from laagent where agentcode=a.AgentOld) ,(select agentgroup from laagent where agentcode=a.AgentNew)"
//               +"  (select e.postaladdress from lcaddress e where e.customerno = d.appntno " 
//               +"and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno))" 
	           + " from LAAscription a ,laagent b,labranchgroup c ,lccont d where a.agentold=b.agentcode  and " 
	           +"  a.ascripstate in ('1','2') and b.agentgroup=c.agentgroup and a.maketype='02' and a.contno=d.contno"
	           +" and a.validflag='N'  and a.grpcontno is null "
	           + getWherePart('b.ManageCom','ManageCom')
	           //+ getWherePart('b.AgentCode','AgentCode')
	           + strAgent
	           + getWherePart('c.BranchAttr','BranchAttr')
	           + getWherePart('b.Name','Name')
	           + getWherePart('b.OutWorkDate','OutWorkDate')
	           +" union "
	           +"select a.GrpContNo,a.ContNo, getUniteCode(a.AgentOld),(select name from laagent where agentcode=a.AgentOld)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentOld))," 
			   +" getUniteCode(a.AgentNew),(select name from laagent where agentcode=a.AgentNew)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentNew))," 
			   +"a.AClass, a.AscriptionDate,case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认' else '确认' end  "
	           + " ,d.grpname ,'',0," 
               +"  '',d.prem,"
               +"(select agentgroup from laagent where agentcode=a.AgentOld) ,(select agentgroup from laagent where agentcode=a.AgentNew)"
//               +"  (select e.postaladdress from lcaddress e where e.customerno = d.appntno " 
//               +"and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno))" 
	           + " from LAAscription a ,laagent b,labranchgroup c ,lcgrpcont d where a.agentold=b.agentcode  and " 
	           +"  a.ascripstate in ('1','2') and b.agentgroup=c.agentgroup and a.maketype='02' and a.grpcontno=d.grpcontno"
	           +" and a.validflag='N'  and a.contno is null "
	           + getWherePart('b.ManageCom','ManageCom')
	           //+ getWherePart('b.AgentCode','AgentCode')
	           + strAgent
	           + getWherePart('c.BranchAttr','BranchAttr')
	           + getWherePart('b.Name','Name')
	           + getWherePart('b.OutWorkDate','OutWorkDate')
	             ;
	 /*
	 //修改前
    //turnPage.queryModal(strSql, AscriptionGrid);
    */
    //修改
     turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1); 
     if (!turnPage.strQueryResult) {
  	  AscriptionGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
      turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = AscriptionGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    //alert(strSql);
    if (!turnPage.strQueryResult) {
    alert("没有符合的数据！");
    return false;
    }	
}

//下载ｅｘｃｅｌ文件
function DoNewDownload()
{
 // 书写SQL语句
 	if(!verifyInput()) return false;
    var strAgent = " " ;
    if(fm.GroupAgentCode.value != null && fm.GroupAgentCode.value != ""){
   	  strAgent = " and b.AgentCode = getAgentCode('"+fm.GroupAgentCode.value+"')";
    }
	var strSql ="select a.GrpContNo, a.ContNo, getUniteCode(a.AgentOld),(select name from laagent where agentcode=a.AgentOld)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentOld))," 
			   +" getUniteCode(a.AgentNew),(select name from laagent where agentcode=a.AgentNew)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentNew))," 
			   +"a.AClass, a.AscriptionDate,case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认' else '确认' end  "
	           + " ,d.appntname ,case when d.appntsex='0' then '男' else '女' end,(select year((current date)-(d.appntbirthday)) from dual)," 
               +"  (select e.mobile from lcaddress e where e.customerno = d.appntno and " 
               +"e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno)),d.prem,"
               +"(select agentgroup from laagent where agentcode=a.AgentOld) ,(select agentgroup from laagent where agentcode=a.AgentNew)"
//               +"  (select e.postaladdress from lcaddress e where e.customerno = d.appntno " 
//               +"and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno))" 
	           + " from LAAscription a ,laagent b,labranchgroup c ,lccont d where a.agentold=b.agentcode  and " 
	           +"  a.ascripstate in ('1','2') and b.agentgroup=c.agentgroup and a.maketype='02' and a.contno=d.contno"
	           +" and a.validflag='N' and a.grpcontno is null "
	           + getWherePart('b.ManageCom','ManageCom')
	           //+ getWherePart('b.AgentCode','AgentCode')
	           + strAgent
	           + getWherePart('c.BranchAttr','BranchAttr')
	           + getWherePart('b.Name','Name')
	           + getWherePart('b.OutWorkDate','OutWorkDate')
	             +" union "
	           +"select a.GrpContNo,a.ContNo, getUniteCode(a.AgentOld),(select name from laagent where agentcode=a.AgentOld)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentOld))," 
			   +" getUniteCode(a.AgentNew),(select name from laagent where agentcode=a.AgentNew)," 
			   +"(select branchattr from labranchgroup where agentgroup=(select branchcode from laagent where agentcode=a.AgentNew))," 
			   +"a.AClass, a.AscriptionDate,case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认' else '确认' end  "
	           + " ,d.grpname ,'',0," 
               +"  '',d.prem,"
               +"(select agentgroup from laagent where agentcode=a.AgentOld) ,(select agentgroup from laagent where agentcode=a.AgentNew)"
//               +"  (select e.postaladdress from lcaddress e where e.customerno = d.appntno " 
//               +"and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno))" 
	           + " from LAAscription a ,laagent b,labranchgroup c ,lcgrpcont d where a.agentold=b.agentcode  and " 
	           +"  a.ascripstate in ('1','2') and b.agentgroup=c.agentgroup and a.maketype='02' and a.grpcontno=d.grpcontno"
	           +" and a.validflag='N'  and a.contno is null "
	           + getWherePart('b.ManageCom','ManageCom')
	           //+ getWherePart('b.AgentCode','AgentCode')
	           + strAgent
	           + getWherePart('c.BranchAttr','BranchAttr')
	           + getWherePart('b.Name','Name')
	           + getWherePart('b.OutWorkDate','OutWorkDate')
	             ;
	//alert(strSql);
    fm.querySql.value = strSql;
    fm.action = "LAAutoAscriptionDownload.jsp";
    fm.submit();
}