<%
//程序名称：LAQualificationQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-21 15:48:13
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('AgentCode').value = "";
    fm.all('QualifNo').value = "";
    //fm.all('Idx').value = "";
    fm.all('GrantUnit').value = "";
    fm.all('GrantDate').value = "";
    //fm.all('InvalidDate').value = "";
    fm.all('InvalidRsn').value = "";
    fm.all('ValidStart').value = "";
    fm.all('ValidEnd').value = "";

    //fm.all('reissueDate').value = "";
    //fm.all('reissueRsn').value = "";
    //fm.all('ValidPeriod').value = "";
    fm.all('QualifState').value = "";
    fm.all('PasExamDate').value = "";
    fm.all('ExamYear').value = "";
    fm.all('ExamTimes').value = "";
    //fm.all('Operator').value = "";
    //fm.all('MakeDate').value = "";
    //fm.all('MakeTime').value = "";
    //fm.all('ModifyDate').value = "";
    //fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LAQualificationQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLAQualificationGrid();  
  }
  catch(re) {
    alert("LAQualificationQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LAQualificationGrid;
function initLAQualificationGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="营销员代码";         		//列名
    iArray[1][1]="100px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="资格证书号";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="批准单位";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="发放日期";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="失效日期";         		//列名
    iArray[5][1]="100px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="失效原因";         		//列名
    iArray[6][1]="100px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="有效起期";         		//列名
    iArray[7][1]="100px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="有效止期";         		//列名
    iArray[8][1]="100px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="资格证状态";         		//列名
    iArray[9][1]="100px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="通过考试日期";         		//列名
    iArray[10][1]="100px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="考试年度";         		//列名
    iArray[11][1]="100px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="考试次数";         		//列名
    iArray[12][1]="100px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="操作员";         		//列名
    iArray[13][1]="0px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=3;         		//是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();
    iArray[14][0]="最近操作日";         		//列名
    iArray[14][1]="0px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=3;         		//是否允许录入，0--不能，1--允许
    
    iArray[15]=new Array();
    iArray[15][0]="距离资格证有效止期天数";         		//列名
    iArray[15][1]="100px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=3;         		//是否允许录入，0--不能，1--允许
    
    iArray[16]=new Array();
    iArray[16][0]="隐藏";         		//列名
    iArray[16][1]="100px";         		//宽度
    iArray[16][2]=100;         		
    iArray[16][3]=3;         		//是否允许录入，0--不能，1--允许
    
    LAQualificationGrid = new MulLineEnter( "fm" , "LAQualificationGrid" ); 
    //这些属性必须在loadMulLine前

    LAQualificationGrid.mulLineCount = 0;   
    LAQualificationGrid.displayTitle = 1;
    LAQualificationGrid.hiddenPlus = 1;
    LAQualificationGrid.hiddenSubtraction = 1;
    LAQualificationGrid.canSel = 1;
    LAQualificationGrid.canChk = 0;
    LAQualificationGrid.selBoxEventFuncName = "showOne";

    LAQualificationGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAQualificationGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
