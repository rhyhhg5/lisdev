<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");

%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">	
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAAddReduceInput.js"></SCRIPT>
  <%@include file="LAAddReduceInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAddReduceSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <table>
    <tr class=common>
    <td class=titleImg>
      加扣款信息
    </td> 
    </tr>
    </table>
  <Div  id= "divAddSub" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		      业务员代码 
		    </td>
        <td class= input> 
		      <input class= common name=GroupAgentCode  OnChange="return checkvalid();" verify="业务员编码|NOTNULL"  elementtype=nacessary > 
		    </td>
		    <td  class= title>
		      业务员姓名
		    </td>
        <td  class= input>
		  <input name=Name class='readonly' readonly >
		</td>
      </tr>
      <tr  class= common> 
      <td  class= title> 
		  销售单位 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=BranchAttr > 
		</td>
        <td  class= title> 
		  团队名称
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=BranchName > 
		</td>
      </tr>
      <tr  class= common> 
      <td  class= title> 
		  管理机构
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ManageCom > 
		</td>
        <td  class= title> 
		  业务员职级
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentGrade > 
		</td>
      </tr>
      <tr  class= common> 
	<td class=title>
	    类型
	</td>
	<td>
      <Input class="codeno" name=Aclass  verify="类型|NOTNULL" 
         ondblclick="return getAgentType(Aclass,AclassName);"
         onkeyup="return getAgentType(Aclass,AclassName);" ><Input class="codename" name= AclassName readonly=true elementtype=nacessary></td>
        <td  class= title> 
		  金额(元)
		</td>
        <td  class= input> 
		  <input name=Money class= common verify="金额|NOTNULL&num&>0"  elementtype=nacessary > 
		</td>
      </tr>   
      <tr  class= common>
        <TD  class= title width="25%"> 
		  执行年月
		</td>
        <TD  class= input  width="25%"  >
             <Input class= "common"  name=WageNo MaxLength=6 verify="执行日期|NOTNULL&len=6&num" elementtype=nacessary id =WageNo > 
             <font color="red">(yyyymm)
	</td>
        <td  class= title> 
		  原因
		</td>
        <td  class= input> 
	     <input name=Noti class= common > 
	</td>	
</tr>
    </table>
  </Div>
  <Input type=hidden name=BranchType value = <%=BranchType %>> 
  <Input type=hidden  name=BranchType2 value = <%=BranchType2 %>>
    <Input type=hidden  name=AgentType >
  <Input type=hidden  name=AgentGroup >
  <Input type=hidden  name=hideOperate >
  <Input type=hidden  name=hideAgentCode>
  <Input type=hidden  name=hideAclass>
  <Input type=hidden  name=hideIdx>
  <input type=hidden name=AgentCode >
  </form>
   <p><font color='red'>注：1.加扣款执行年月必须大于最大计算薪资月</font></p>
    <font color='red'>注：2.页面输入金额必须大于等于0</font></p>
    <p><font color='red'>注：3.最近12个月内的提奖/绩效扣款和最近8个月内的提奖/绩效加款之和不能大于0 </font></p>
<!--    <p><font color='red'>注：4.最近12个月内的其他扣款和其他加款之和不能大于0 </font></p>-->
    <p><font color='red'>注：4.加扣款执行年月必须大于等于201206</font></p>
    <p><font color='red'>注：5.提奖扣款金额，请在4个月内及时通过提奖加款完成发放操作，超过相关时间限制的扣款金额将不能再通过加款回补！</font></p>
    <p><font color='red'>注：6.提奖/绩效预扣补发加款不能大于去年同薪资月的医疗险种提奖预扣金额。！</font></p>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
