<%
//程序名称：LAContInput.jsp
//程序功能：
//创建日期：2005-03-20 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>                                           
<script language="JavaScript">

function initInpBox()
{ 
  try
  { 
	fm.all('ManageComHierarchy').value='';
	fm.all('StartDate').value='';
	fm.all('EndDate').value ='';

    
  }
  catch(ex)
  {
    alert("在LAContInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {

    initInpBox();
    initContGrid();
  }
  catch(re)
  {
    alert("LAContInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ContGrid;
function initContGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";          		        //列名
    iArray[1][1]="80px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";          		        //列名
    iArray[2][1]="70px";      	      		//列宽
    iArray[2][2]=20;            			//列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[3]=new Array();
    iArray[3][0]="保费收入";          		        //列名
    iArray[3][1]="70px";      	      		//列宽
    iArray[3][2]=20;            			//列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    iArray[4]=new Array();
    iArray[4][0]="应付手续费";  //列名
    iArray[4][1]="90px";  //列宽
    iArray[4][2]=100;   //列最大值
    iArray[4][3]=0;   //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    iArray[5]=new Array();
    iArray[5][0]="实付手续费";  //列名
    iArray[5][1]="70px";  //列宽
    iArray[5][2]=100;   //列最大值
    iArray[5][3]=0;   //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    
    
    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
    //这些属性必须在loadMulLine前

    ContGrid.mulLineCount = 0;
    ContGrid.displayTitle = 1;
    ContGrid.hiddenPlus = 1;
    ContGrid.hiddenSubtraction = 1;
    ContGrid.canSel=1;
    ContGrid.canChk=0;
    ContGrid.locked=1;
    ContGrid.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
