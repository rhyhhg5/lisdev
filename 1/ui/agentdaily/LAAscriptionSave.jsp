<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAscriptionSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.wagecal.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAscriptionSchema tLAAscriptionSchema;
  LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
  ALAAscriptionUI tALAAscriptionUI  = new ALAAscriptionUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin Ascription schema...");
    //取得授权信息
  int lineCount = 0;
  String tChk[] = request.getParameterValues("InpAscriptionGridChk");
  int number = 0; 
    for(int index=0;index<tChk.length;index++)
      {
          if(tChk[index].equals("1"))           
           number++;
      }
  System.out.println("number"+number);    
       if(number==0)
	{
       		Content = " 失败，原因:没有选择要调整的员工！";
       		FlagStr = "Fail";		
	} 
      else
	{
	  String tPolno[] = request.getParameterValues("AscriptionGrid1");
	  String tAgentCode = request.getParameter("AgentCode");
	  String tNewAgentCode = request.getParameter("NewAgentCode");	
	  String tBranchType = request.getParameter("BranchType"); 
	  lineCount = tChk.length; //行数
	  System.out.println("length= "+String.valueOf(lineCount));
		
	
	for(int i=0;i<lineCount;i++)
	  {
	    if(tChk[i].trim().equals("1"))
	    {
	      tLAAscriptionSchema = new LAAscriptionSchema();
              tLAAscriptionSchema.setAgentOld(tAgentCode);    
              tLAAscriptionSchema.setPolNo(tPolno[i]);    
              tLAAscriptionSchema.setAgentNew(tNewAgentCode);
              tLAAscriptionSet.add(tLAAscriptionSchema);
	    }
	    System.out.println("i:"+tChk[i]);
	    
	  } 
	  System.out.println("end 归属信息...");
	  
	  // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";
    tLAAscriptionSchema = new LAAscriptionSchema();
    tVData.addElement(tLAAscriptionSet);
    tVData.add(tG);
    tVData.add(tBranchType);
  System.out.println("add over");
  try
  {
    tALAAscriptionUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tALAAscriptionUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

	}
  
  
  
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

