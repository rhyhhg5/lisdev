
<%
	//程序名称：LACertificationDataUploadSave.jsp
	//程序功能：
	//创建日期：2006-10-24
	//创建人  ：张斌
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
	System.out.println("开始执行Save页面");
	GlobalInput globalInput = new GlobalInput();
	globalInput.setSchema((GlobalInput) session.getValue("GI"));

	CErrors tError = null;

	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String mDescType = ""; //将操作标志的英文转换成汉字的形式

	System.out.println("开始进行获取数据的操作！！！");

	VData tVData = new VData();
	//将团单的公共信息通过TransferData传到UI

	String fileName = "LACertification.xls";
	String tOutXmlPath = application.getRealPath("vtsfile_la") + "/"
			+ fileName;
	String FileName = tOutXmlPath.substring(tOutXmlPath
			.lastIndexOf("/") + 1);
	File file = new File(tOutXmlPath);

	response.reset();
	response.setContentType("application/octet-stream");
	response.setHeader("Content-Disposition", "attachment; filename="
			+ FileName + "");
	response.setContentLength((int) file.length());

	byte[] buffer = new byte[4096];
	BufferedOutputStream output = null;
	BufferedInputStream input = null;
	//写缓冲区
	try {
		output = new BufferedOutputStream(response.getOutputStream());
		input = new BufferedInputStream(new FileInputStream(file));

		int len = 0;
		while ((len = input.read(buffer)) > 0) {
			output.write(buffer, 0, len);
		}
		input.close();
		output.close();
	} catch (Exception e) {
		e.printStackTrace();
	} // maybe user cancelled download
	finally {
		if (input != null)
			input.close();
		if (output != null)
			output.close();
		//file.delete();
	}

	if (FlagStr == "") {
		if (!tError.needDealError()) {
			Content = mDescType + "操作成功";
			FlagStr = "Succ";
		} else {
			Content = mDescType + " 失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>

<html>
	<script language="javascript">
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>
	1_");
</script>
</html>