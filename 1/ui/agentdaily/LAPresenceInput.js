//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	
	if (mOperate=="")
	{
	  addClick();
	}
    //首先检验录入框
   if(!verifyInput()) return false;
   //alert(mOperate);
   //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   //alert("请重新输入信息！");
   //return false;
  //}
  
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  mOperate="";
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LAPresence.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	 
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
  	alert("请输入业务员编码！");
  	fm.all('AgentCode').focus();
  	return false;
  }
  if ((fm.all('Times').value != '')&&(fm.all('Times').value != null))
  {
  	if(fm.all('Times').value<=0)
  	{
  	alert("考勤执行次数应该为有效数字!");
  	//fm.all('Times').focus();
  	return false;
        }
  }    
  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
 // showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("GroupAgentCode").value==null)||(fm.all("GroupAgentCode").value==''))
  {
    alert('请先查询出要修改的纪录！');
  }
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要修改的纪录！');
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
  	//alert(fm.all('hiddenAgentGroup').value);
  	//alert(fm.all('HiddenAgentGroup').value);
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAPresenceQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("GroupAgentCode").value==null)||(fm.all("GroupAgentCode").value==''))
    alert('请先查询出要删除的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要删除的纪录！');
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//修改人：解青青 2014-11-06
function checkValid()
{
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  //alert('aaa');
  if (getWherePart('GroupAgentCode')!='')
  {
     
     strSQL = "select * from LAAgent where 1=1 "
	     + getWherePart('GroupAgentCode','GroupAgentCode')+" and ((AgentState  <'06') or (AgentState is null))"
	     + " and branchtype='"+tBranchType+"' and BranchType2='"+tBranchType2+"'";
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);

  }
  else
  {
    fm.all('GroupAgentCode').value = '';
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value = "";
    fm.all('AgentCode').value = '';
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('GroupAgentCode').value="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    fm.all('AgentCode').value = '';
    //fm.all('AgentCode').focus();
    return false;
  }
  //alert('aa');
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentCode').value  = tArr[0][0];
//  alert(fm.all('AgentCode').value);
  fm.all('ManageCom').value  = tArr[0][2];
  //<addcode>############################################################//
  fm.all('HiddenAgentGroup').value = tArr[0][1];
  //</addcode>############################################################//
  fm.all('Name').value = tArr[0][5];
  
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][1];
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"
  var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  //</addcode>############################################################//
}
/*
function agentConfirm()
{
  fm.all('AgentGroup').value='';
  fm.all('ManageCom').value='';	
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
     alert("请输入代理人编码！");
     fm.all('AgentCode').focus();
     return false;
  }
  fm.action = "AgentCodeQuery.jsp";	
  fm.submit();
  fm.action = "LAPresenceSave.jsp";	
  //alert('change');
}*/

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('GroupAgentCode').value = arrResult[0][0];
		checkValid();
		fm.all('Name').value = arrResult[0][19];
		fm.all('AgentGroup').value = arrResult[0][17];
		fm.all('ManageCom').value = arrResult[0][2];
		fm.all('Idx').value = arrResult[0][3];
		fm.all('AClass').value = arrResult[0][6];
		fm.all('Times').value = arrResult[0][5];
		fm.all('Noti').value = arrResult[0][9];
		fm.all('DoneDate').value = arrResult[0][7];
		fm.all('ModifyDate').value = arrResult[0][14];
		fm.all('Operator').value = arrResult[0][11];
		
	//<addcode>############################################################//
	    fm.all('HiddenAgentGroup').value = arrResult[0][1];
	    //alert(arrResult[0][12]);
	   
	//<addcode>############################################################//	                                                                                                                                                                                                                                                	
 	}
 	
}
//无用
function handler(key_event)
{
	//status = String.fromCharCode(key_event.keyCode);
	if (document.all)
	{
		if (key_event.keyCode==13)
		{
		    parent.fraInterface.fm.action = "AgentCodeQuery.jsp";	
		    if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
                    {
  	              alert("请输入代理人代码！");
  	              return false;
                }
                
		    fm.submit();
		    parent.fraInterface.fm.action = "LAPresenceSave.jsp";	
		    return true;
		}
	}
}
//统计月缺勤天数
function getmon()
{ var a=0;
  if (!beforeSubmit())return false;
  if ((fm.all('StatByMon').value == '')||(fm.all('StatByMon').value == null))
  {
  	alert("请输入统计月份！");
  	fm.all('StatByMon').focus();
  	return false;
  }
  var StatByMon=fm.all('StatByMon').value;
  var strSQL = "";
  strSQL = "select count(*) from LAPresence where 1=1 "
	     + getWherePart('AgentCode')
	     + " and to_char(DoneDate,'YYYYMM')='"+StatByMon+"'";
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  var tArr1=new Array();
  tArr1=decodeEasyQueryResult(strQueryResult);
  if(strQueryResult)
  {
    fm.all('mstatdays').value =tArr1[0][0];
    return true;
  }
  alert("无缺勤记录！");
}
//统计年缺勤天数
function getyea()
{ var b=0;
  if (!beforeSubmit())return false;
  if ((fm.all('StatByYear').value == '')||(fm.all('StatByYear').value == null))
  {
  	alert("请输入统计年份！");
  	fm.all('StatByYear').focus();
  	return false;
  }
  var StatByYear=fm.all('StatByYear').value;
  var strSQL = "";
  strSQL = "select count(*) from LAPresence where 1=1 "
	     + getWherePart('AgentCode')
	     + " and to_char(DoneDate,'YYYY')='"+StatByYear+"'";
  var strQueryResult= easyQueryVer3(strSQL, 1, 1, 1);
  var tArr2=new Array();
  tArr2=decodeEasyQueryResult(strQueryResult);
  if(strQueryResult)
  {
    fm.all('ystatdays').value =tArr2[0][0];
    return true;
  }
  alert("无缺勤记录！");
}