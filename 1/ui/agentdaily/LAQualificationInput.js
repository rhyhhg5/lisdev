//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
   if(fm.all('AgentCodeQuery').value!=null&&(fm.all('AgentCodeQuery').value!=''))
   {
      alert("查询出来的内容,请使用修改操作！");
      return false;
   }
    if(!checkdate())    return false;
	if(!beforeSubmit()) return false;
	fm.fmtransact.value = "INSERT||MAIN";
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
    initInpBox();//清空	
    document.all.AgentCode.readOnly=false;
	fm.reset();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAQualification.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value==null))
  {
  	alert("请输入营销员代码！");
  	fm.all('AgentCode').focus();
  	return false;
  }	  
  if ((fm.all('QualifNo').value == '')||(fm.all('QualifNo').value==null))
  {
  	alert("请输入资格证书号！");
  	fm.all('QualifNo').focus();
  	return false;
  }  
  if ((fm.all('GrantUnit').value == '')||(fm.all('GrantUnit').value==null))
  {
  	alert("请输入批准单位！");
  	fm.all('GrantUnit').focus();
  	return false;
  }
  if (!verifyInput()) return false;
  if ((fm.all('QualifState').value == '')||(fm.all('QualifState').value==null))
  {
  	alert("请输入资格证书状态！");
  	fm.all('QualifState').focus();
  	return false;
  }

  if (fm.all('QualifState').value=='1'&& fm.all('QualifState1').value=='0'&& trim(fm.all('InvalidRsn').value)=='')
  {
  	alert("请录入失效原因!")  ;
  	fm.all('InvalidRsn').focus();
  	return false;
	}
	if (fm.all('QualifState').value=='1'&& fm.all('QualifState1').value=='0'&& trim(fm.all('InvalidDate').value)=='')
  {
  	alert("请录入失效日期!")  ;
  	fm.all('InvalidDate').focus();
  	return false;
	}	
	if (trim(fm.all('InvalidDate').value)!='' &&  trim(fm.all('InvalidDate').value)<trim(fm.all('ValidStart').value))
  {
  	alert("失效日期不能小于有效起期!")  ;
  	fm.all('InvalidDate').focus();
  	return false;
	}		
	return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
      
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码

  if (confirm("您确实想修改该记录吗?"))
  {
  fm.fmtransact.value = "UPDATE||MAIN";
  fdisable();
 
  if (!beforeSubmit())
  {
  	return false;
  }
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}       
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LAQualificationQuery.html");
}           
    
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
	    tdisable();
		arrResult = arrQueryResult;
        fm.all('GroupAgentCode').value= arrResult[0][0];   
        fm.all('QualifNo').value= arrResult[0][1];   
        fm.all('AgentCodeQuery').value= arrResult[0][15]; 
        fm.all('QualifNoQuery').value= arrResult[0][1];    //为了修改这个字段（主键） 
        fm.all('GrantUnit').value= arrResult[0][2];
        fm.all('GrantDate').value= arrResult[0][3];
        fm.all('InvalidDate').value= arrResult[0][4];
        fm.all('InvalidRsn').value= arrResult[0][5];       
        fm.all('ValidStart').value= arrResult[0][6];
        fm.all('ValidEnd').value= arrResult[0][7];
        fm.all('QualifState1').value= arrResult[0][8];
        fm.all('PasExamDate').value= arrResult[0][9];
        fm.all('ExamYear').value= arrResult[0][10];
        fm.all('ExamTimes').value= arrResult[0][11];
        fm.all('Operator').value= arrResult[0][12];
        fm.all('ModifyDate').value= arrResult[0][13];
  		if(arrResult[0][8]=='有效')
    {
    	fm.all('QualifState').value='0';
    	fm.all('QualifStateName').value='有效';
    }
    else
  	{
  		fm.all('QualifState').value='1';
  		fm.all('QualifStateName').value='无效';
  	}     
       document.all.AgentCode.readOnly="true";
       checkvalid();
       tdisable();// 按钮置灰
	}
}    
         
function tdisable()
{
        fm.all('GroupAgentCode').disabled=true;
		fm.all('AgentName').disabled=true;
		fm.all('QualifNo').disabled=true;
	//	fm.all('GrantUnit').disabled=true;
	//	fm.all('GrantDate').disabled=true;
	//	fm.all('ValidStart').disabled=true;
	//	fm.all('InvalidDate').disabled=true;
	//	fm.all('InvalidRsn').disabled=true;
	//	fm.all('PasExamDate').disabled=true;
	//	fm.all('ExamYear').disabled=true;
	//	fm.all('ExamTimes').disabled=true;
		fm.all('Operator').disabled=true;
		fm.all('ModifyDate').disabled=true;

}
function fdisable()
{
       fm.all('GroupAgentCode').disabled=false;
		fm.all('AgentName').disabled=false;
		fm.all('QualifNo').disabled=false;
		fm.all('GrantUnit').disabled=false;
		fm.all('GrantDate').disabled=false;
		fm.all('ValidStart').disabled=false;
		fm.all('InvalidDate').disabled=false;
		fm.all('InvalidRsn').disabled=false;
		fm.all('PasExamDate').disabled=false;
		fm.all('ExamYear').disabled=false;
		fm.all('ExamTimes').disabled=false;
		fm.all('Operator').disabled=false;
		fm.all('ModifyDate').disabled=false;


}
   
function checkvalid()
{
	var tSql = "";
	
	//判断有无业务员代码
	if (getWherePart('GroupAgentCode')=='')
  {
    document.fm.AgentName.value  = "";
    document.fm.GroupAgentCode.value = "";
    document.fm.AgentCode.value = "";
   	return false;
  }
  
  tSql  = "SELECT";
  tSql += "    A.Name,";
  tSql += "    A.ManageCom,";
  tSql += "    B.BranchAttr,";
  tSql += "    A.AgentCode";
  tSql += "  FROM";
  tSql += "    LAAgent A,";
  tSql += "    LABranchGroup B";
  tSql += " WHERE 1=1 AND";
  tSql += "    A.AgentGroup = B.AgentGroup";
  tSql += getWherePart('A.GroupAgentCode','GroupAgentCode');
  
  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    document.fm.AgentName.value  = "";
    document.fm.GroupAgentCode.value = "";
    document.fm.AgentCode.value = "";
    return;
  }
  
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  //设置姓名
  document.fm.AgentName.value = tArr[0][0];
  document.fm.AgentCode.value = tArr[0][3];
}

function checkdate()
{
//  tSql =   "select '1' from LAQualification"
//  tSql +=   " WHERE 1=1  and";
//  tSql +=  "(('"+fm.all('ValidStart').value+"'<=Validend and '"+fm.all('ValidStart').value+"'>=ValidStart) or('"+fm.all('ValidEnd').value+"'<=Validend and '"+fm.all('ValidEnd').value+"'>=ValidStart))";
//  tSql +=  getWherePart('AgentCode')
//  tSql +=   " union select '1' from LAQualificationb"
//  tSql +=   " WHERE 1=1  and";
//  tSql +=  "(('"+fm.all('ValidStart').value+"'<=Validend and '"+fm.all('ValidStart').value+"'>=ValidStart) or('"+fm.all('ValidEnd').value+"'<=Validend and '"+fm.all('ValidEnd').value+"'>=ValidStart))";
//  tSql +=  getWherePart('AgentCode');
//  
//  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
//  if (!strQueryResult) 
//  {
//    return true;
//  }
//  else
//  {
//    alert('录入的有效区间与原有记录重复,请重新录入');
//  	return false;
//  }
  
  return true;
}

function diskImport()
{ 
   var strUrl = "../agentdaily/DiskImportAgentMain.jsp?diskimporttype="+fm.all("diskimporttype").value;
   showInfo=window.open(strUrl,"资格证书清单导入","width=400, height=150, top=150, left=250, t====="+fm.all("diskimporttype").value);
   var strUrl = "../agentdaily/DiskImportAgentMain.jsp?diskimporttype="+fm.all("diskimporttype").value;
   showInfo=window.open(strUrl,"资格证书清单导入","width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=yes");
}


function checkInput(str)
{	
	fm.all('QualifNo').value = str.replace(/(^[\s\u3000]*)|([\s\u3000]*$)/g, "");
}


