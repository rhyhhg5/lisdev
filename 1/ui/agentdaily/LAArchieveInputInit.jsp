<%
//程序名称：LAArchieveInput.jsp
//程序功能：
//创建日期：2005-03-20 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>                                           
<script language="JavaScript">

function initInpBox()
{ 
  try
  {
    fm.all('AgentCode').value = "<%=agentcode%>";
    fm.all('GroupAgentCode').value = "<%=agentcode%>";
    fm.all('AgentCodeName').value = "";
    if("<%=agentcode%>" != "" && "<%=agentcode%>" != "null" && "<%=agentcode%>" != null)
    {
      getname();
    }
    //fm.all('ArchieveNo').value = "<%=agentcode%>";
    fm.all('BranchType').value = "<%=BranchType%>";
    fm.all('BranchType2').value = "<%=BranchType2%>";
    fm.all('ArchieveDate').value = "";
  }
  catch(ex)
  {
    alert("在LAArchieveInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initArchieveGrid();
  }
  catch(re)
  {
    alert("LAArchieveInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ArchieveGrid;
function initArchieveGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="档案类型编码";          		        //列名
    iArray[1][1]="50px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[1][4]="archtype";
    iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值

    iArray[2]=new Array();
    iArray[2][0]="档案类型名称";          		        //列名
    iArray[2][1]="60px";      	      		//列宽
    iArray[2][2]=20;            			//列最大值
    iArray[2][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[3]=new Array();
    iArray[3][0]="档案项目编码";          		        //列名
    iArray[3][1]="100px";      	      		//列宽
    iArray[3][2]=20;            			//列最大值
    iArray[3][3]=2;             //是否允许输入,    
    iArray[3][4]="architem";
    iArray[3][5]="3|4";
    iArray[3][6]="0|1";
    iArray[3][15]="OtherSign";
    iArray[3][17]="1";  
   
    iArray[4]=new Array();
    iArray[4][0]="档案项目名称";          		        //列名
    iArray[4][1]="100px";      	      		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    iArray[5]=new Array();
    iArray[5][0]="流水号";  
    iArray[5][1]="0px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[5][2]=100;   
    iArray[5][3]=3;  
    
    iArray[6]=new Array();
    iArray[6][0]="档案编码";  
    iArray[6][1]="0px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[6][2]=100;   
    iArray[6][3]=3;  
     
    iArray[7]=new Array();
    iArray[7][0]="归档日期(YYYY-MM-DD)";  
    iArray[7][1]="100px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[7][2]=100;   
    iArray[7][3]=1; 
    
    iArray[8]=new Array();
    iArray[8][0]="建档日期";  
    iArray[8][1]="100px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[8][2]=100;   
    iArray[8][3]=0;  
    
    iArray[9]=new Array();
    iArray[9][0]="业务员编码";  
    iArray[9][1]="100px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[9][2]=100;   
    iArray[9][3]=0; 
     
    iArray[10]=new Array();
    iArray[10][0]="业务员姓名";  
    iArray[10][1]="100px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[10][2]=100;   
    iArray[10][3]=0; 
     
    iArray[11]=new Array();
    iArray[11][0]="档案编码";  
    iArray[11][1]="100px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[11][2]=100;   
    iArray[11][3]=0; 

    ArchieveGrid = new MulLineEnter( "fm" , "ArchieveGrid" ); 
    //这些属性必须在loadMulLine前

    ArchieveGrid.mulLineCount = 0;   
    ArchieveGrid.displayTitle = 1;

    ArchieveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ArchieveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
