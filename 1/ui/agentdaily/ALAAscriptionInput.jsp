<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ALAAscriptionInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="ALAAscriptionInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ALAAscriptionInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();">
  <form action="./ALAAscriptionSave.jsp" method=post name=fm target="fraSubmit">  
  <!--    <%@include file="../agent/AgentOp2.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>-->
   <div id="inputButton" style="display: """>
	<table class="common" align=center>
		<tr align=right>
			<td class=button>
				&nbsp;&nbsp;
			</td>		
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="重  置"  TYPE=button onclick="return initForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return addClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td> 
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="删  除"  TYPE=button onclick="return deleteClick();">
			</td>
		</tr>
	</table>
</div>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    <td class=titleImg>
      原销售人员信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common>
       <TR  class= common> 
          <TD  class= title>
            原销售人员代码
          </TD>          
          <TD  class= input>
            <Input class=common name=AgentOld onchange="return checkPrename();" verify="原代理人编码|notnull&len=10" type=hidden>
            <Input class=common name=GroupAgentOld onchange="return checkPrename();" verify="原代理人编码|notnull&len=10" elementtype=nacessary>
          </TD>  
          <td  class= title>
		   原销售人员姓名
		  </td>
          <td  class= input>
		  <input name=PreName class='readonly' readonly >
		  </td>   
	   </TR>
	   <TR class=common>		
        <TD class=title>
         原销售机构代码
        </TD>
        <TD class=input>
        <input class='readonly' readonly name=BranchAttr>
        </TD>
        <TD class=title>
         原代理人离职日期
        </TD>
        <TD class=input>
        <input class='readonly' readonly  name=OutWorkDate>
        </TD>
	   </TR>     
	   <TR>
		<TD class=title>
		  管理机构
		</TD>
		
         <TD class=input>
           <input class='readonly' readonly  name=ManageCom>
         </TD>
		</TR>
		<TR  class= common>  
		  <td  class= title>
		   新代理人代码
		  </td>
          <td  class= input>
		   <input name=AgentNew class=common onchange="return checkname();"  type=hidden>
		   <input name=GroupAgentNew class=common onchange="return checkname();" elementtype=nacessary>
		  </td>   
          <TD  class= title>
            新代理人姓名
          </TD>          
          <TD  class= input>
            <Input class='readonly' readonly name=NameNew >
          </TD>     
		</TR>		
        <TR  class= common> 
          <TD  class= title>
           个单号
          </TD>          
          <TD  class= input>
            <Input class=common name=ContNo onchange="return checkCount();">
          </TD> 
            <TD  class= title>
           团单号
          </TD>          
          <TD  class= input>
            <Input class=common name=GrpContNo onchange="return checkCount1();">
          </TD> 
		</TR> 
		<TR  class= common> 		  
         <td  class= title> 
		 归属日期
		 </td>
         <td  class= input> 
		  <Input class= "coolDatePicker" dateFormat="short" name=AscriptionDate verify="归属日期|DATE"  elementtype=nacessary  >   
		 </td> 
         <td  class= title>
		 操作员
		 </td>
         <td  class= input>
		   <input name=Operator class='readonly' readonly >
		 </td>      
     </TR> 
    </table>  
    <p> <font color="#ff0000">注：只变更离职人员名下的孤儿单，归属日期必须大于人员的离职日期及新人员最大薪资月下个月1号。分配后请及时进行归属确认。 </font></p>
   </div>
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			原销售人员未签单或未回执回销保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
      <INPUT VALUE="下  载" TYPE=button onclick="doDownload();" class="cssButton"> 				 				
  	</div>
   <p> <font color="#ff0000">注：未签单或未回执回销的保单不能做保单分配。</font></p> 
   <input type=hidden name=AgentGroup value=''>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
     <input type=hidden name=BranchType2 value=''>
     <input type=hidden name=AscripNo value=''>
     <input type=hidden name=State value=''>
     <input type=hidden name=AgentNew1 value=''>
     <input type=hidden name=ContNo1 value=''>
     <input type=hidden name=GrpContNo1 value=''>
     <input type=hidden class=Common name=querySql > 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


 <!--< p> <font color="#ff0000">注：只变更归属日期之后的保单业绩和提佣，请选择正确的归属日期 。</font></p > -->
   <!--< p> <font color="#ff0000">如归属日期在保单财务实收日期之前，则变更业绩给新业务员；如归属日期在财务实收日期之后，则不变更业绩给新业务员。 </font></p> -->  
  
