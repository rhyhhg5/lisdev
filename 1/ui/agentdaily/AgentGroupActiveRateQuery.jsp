<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AgentGroupActiveRateQuery.jsp
//程序功能：
//创建日期：2008-03-08 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<head >
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="AgentGroupActiveRateQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentGroupActiveRateInit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./AgentGroupActiveRateSave.jsp" method=post name=fm target="fraSubmit">  
    <table>
    	<tr class=common>
    		<td class=common>
    			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    		<td class=titleImg>查询条件</td>
    	</tr>
    </table>
 <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common> 
			<TR  class= common> 
          <td  class= title>
		        管理机构
					</td>
        	<td  class= input>
		  			<Input class="codeno" name=ManageCom 
             	ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|notnull&code:comcode"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </td> 
          <td  class= title>
		        所属年月
					</td>
        	<td  class= input>
		  			<Input class=common name=WageNo verify="所属年月|notnull&len=6&int" elementtype=nacessary >
            <font color="red">  'YYYYMM'</font>
          </td>     
			</TR> 
		   
		  <TR  class= common> 
          <td  class= title>
		        销售单位代码
					</td>
        	<td  class= input>
		  			<input name=AgentCom class="common" >
          </td> 
          <td  class= title>
		        销售单位名称
					</td>
        	<td  class= input>
		  			<input name=AgentComName class="common" >
          </td>     
			</TR> 
		
     <TR  class= common> 
          <td  class= title>
		        业务员代码
					</td>
        	<td  class= input>
		  			<input name=GroupAgentCode class="common" onchange="return checkAgentCode()">
          </td> 
          <td  class= title>
		        业务员姓名
					</td>
        	<td  class= input>
		  			<input name=AgentName class="common" >
          </td>    
			</TR>
	 </table>
	</div>	
	<input type =button class=cssButton value="打 印" onclick="submitForm();">
    <input type=hidden name=BranchType value=<%=BranchType%>>
    <input type=hidden name=BranchType2 value=<%=BranchType2%>>
    <input type=hidden name=AgentCode value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
