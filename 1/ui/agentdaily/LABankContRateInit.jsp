<%
//程序名称：LAArchieveInput.jsp
//程序功能：
//创建日期：2005-03-20 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>                                           
<script language="JavaScript">

function initInpBox()
{ 
  try
  {         
                         
 
      fm.all('GrpContNo').value = "";
//      fm.all('ManageCom').value = ""; 
//      fm.all('AgentCode').value = "";
//      fm.all('AgentCodeName').value = "";
//    fm.all('ArchieveDate').value = "";
   // fm.all('PigeOnHoleDate').value = "";
   // fm.all('ArchType').value = "";
    //fm.all('ArchItem').value = "";
    //fm.all('Operator').value = "";
  }
  catch(ex)
  {
    alert("LAContFYCRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {

    initInpBox();
    initArchieveGrid();
  }
  catch(re)
  {
    alert("LAContFYCRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ArchieveGrid;
function initArchieveGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="中介机构编码";          		        //列名
    iArray[1][1]="80px";      	      		//列宽         			//列最大值
    iArray[1][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[1][3]=0;  
    
    iArray[2]=new Array();
    iArray[2][0]="中介机构名称";          		        //列名
    iArray[2][1]="150px";      	      		//列宽         			//列最大值
    iArray[2][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[2][3]=0; 
    
    iArray[3]=new Array();
    iArray[3][0]="保单号";          		        //列名
    iArray[3][1]="70px";      	      		//列宽         			//列最大值
    iArray[3][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[3][3]=0;     
    
    iArray[4]=new Array();
    iArray[4][0]="险种号";          		        //列名
    iArray[4][1]="50px";      	      		//列宽         			//列最大值
    iArray[4][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[4][3]=0; 
    
    iArray[5]=new Array();
    iArray[5][0]="保费";  
    iArray[5][1]="50px";      	      		//列宽         			//列最大值
    iArray[5][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[5][3]=1; 
    
        iArray[6]=new Array();
    iArray[6][0]="手续费比例";  
    iArray[6][1]="60px";      	      		//列宽         			//列最大值
    iArray[6][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[6][3]=1; 
    iArray[6][9]="手续费比例|notnull"; 
  
   
    iArray[7]=new Array();
    iArray[7][0]="中介专员名称";  
    iArray[7][1]="70px";      	      		//列宽         			//列最大值
    iArray[7][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[7][3]=1; 
    
    iArray[8]=new Array();
    iArray[8][0]="投保人";  
    iArray[8][1]="60px";      	      		//列宽         			//列最大值
    iArray[8][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[8][3]=1; 
   
    iArray[9]=new Array();
    iArray[9][0]="签单日期";  
    iArray[9][1]="80px";      	      		//列宽         			//列最大值
    iArray[9][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[9][3]=1; 
    
    iArray[10]=new Array();
    iArray[10][0]="grppol";  
    iArray[10][1]="0px";      	      		//列宽         			//列最大值
    iArray[10][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[10][3]=1; 
    
    iArray[11]=new Array();
    iArray[11][0]="salchnl";  
    iArray[11][1]="0px";      	      		//列宽         			//列最大值
    iArray[11][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[11][3]=1; 
    
    iArray[12]=new Array();
    iArray[12][0]="salchnl";  
    iArray[12][1]="0px";      	      		//列宽         			//列最大值
    iArray[12][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[12][3]=1;     
    
    ArchieveGrid = new MulLineEnter( "fm" , "ArchieveGrid" ); 
    //这些属性必须在loadMulLine前

    ArchieveGrid.mulLineCount = 0;   
    ArchieveGrid.displayTitle = 1;
    ArchieveGrid.hiddenPlus = 1;
    ArchieveGrid.hiddenSubtraction = 1;    
    ArchieveGrid.canSel = 0;
    ArchieveGrid.canChk = 1;
    
    
   // ArchieveGrid.selBoxEventFuncName = "showOne";

    ArchieveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ArchieveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
