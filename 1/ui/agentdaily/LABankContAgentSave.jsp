//更新记录：  更新人    更新日期     更新原因/内容
%>
//<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAscriptionSchema tLAAscriptionSchema   = new LAAscriptionSchema();
  LABankContAgentUI tLABankContAgentUI   = new LABankContAgentUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tManageCom="";
  String tconttype = request.getParameter("conttype").trim();
  System.out.println("apple:"+tconttype); 
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  System.out.println("begin agent schema start..."); 
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   transact = request.getParameter("hideOperate");
   tLAAscriptionSchema.setBranchType(request.getParameter("BranchType"));
   tLAAscriptionSchema.setBranchType2(request.getParameter("BranchType2"));
   tLAAscriptionSchema.setAgentOld(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("AgentOld")+"'"));
   tLAAscriptionSchema.setAgentNew(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("AgentNew")+"'"));
   if(tconttype.equals("1"))
   {
     tLAAscriptionSchema.setContNo(request.getParameter("ContNo"));
   }
   else if(tconttype.equals("2"))
   {
      tLAAscriptionSchema.setGrpContNo(request.getParameter("ContNo"));
   }
   tLAAscriptionSchema.setValidFlag("N");
   tLAAscriptionSchema.setAscriptionDate(request.getParameter("AscriptionDate"));
   tLAAscriptionSchema.setAscripState("3");
   tLAAscriptionSchema.setAgentGroup(request.getParameter("AgentGroup"));
   tLAAscriptionSchema.setAscripNo(request.getParameter("AscripNo"));
   tLAAscriptionSchema.setBranchAttr(request.getParameter("BranchAttrOld1"));
   tLAAscriptionSchema.setManageCom(request.getParameter("ManageComOld")); 
   tLAAscriptionSchema.setAgentComOld(request.getParameter("AgentComOld"));
   if(request.getParameter("AgentComNew")==null || request.getParameter("AgentComNew").equals("")){
   	tLAAscriptionSchema.setAgentComNew(""); 
   }else{
   	tLAAscriptionSchema.setAgentComNew(request.getParameter("AgentComNew")); 
   }
   tLAAscriptionSchema.setMakeType("09");  //新单业务员变更   
          
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tG);
  	tVData.add(tconttype);
  	tVData.add(tLAAscriptionSchema); 
  	  System.out.println("begin agent schema..."); 
    tLABankContAgentUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLABankContAgentUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
