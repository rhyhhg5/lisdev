<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：DiskImportAgentSave.jsp
//程序功能：佣金率相关信息磁盘导入上传
//创建日期：2008-04-25
//创建人  ：苗祥征
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.agentconfig.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%
	String flag = "";
	String content = "";
	String path = "";      //文件上传路径
	String fileName = "";  //上传文件名
	String diskimporttype="";
	String branchtype = "";
	String branchtype2 = "";
	int count=0;
	
	//得到全局变量
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	
    //得到excel文件的保存路径
    String sql = "select SysvarValue "
                + "from ldsysvar "
                + "where sysvar='SaleXmlPath'";
    ExeSQL tExeSQL = new ExeSQL();
    String subPath = tExeSQL.getOneValue(sql);
 	path = application.getRealPath(subPath);
 	
	if(!path.endsWith("/")){
	      path += "/";
	}

	DiskFileUpload fu = new DiskFileUpload();
	// 设置允许用户上传文件大小,单位:字节
	fu.setSizeMax(10000000);
	// maximum size that will be stored in memory?
	// 设置最多只允许在内存中存储的数据,单位:字节
	fu.setSizeThreshold(4096);//4096
	// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
	fu.setRepositoryPath(path);
	//开始读取上传信息
	List fileItems=null;
	try
	{
	   fileItems = fu.parseRequest(request);
	}
	catch (Exception e){
		e.printStackTrace();
	}
	
	String ImportPath = "";

	// 依次处理每个上传的文件
	Iterator iter = fileItems.iterator();
	while (iter.hasNext())
	{
	  FileItem item = (FileItem) iter.next();
	  if(item.getFieldName().compareTo("diskimporttype")==0){
	  	diskimporttype=item.getString();
	  	System.out.println("........save:diskimporttype"+diskimporttype);
	  }
	  
	  if(item.getFieldName().compareTo("BranchType")==0){
		  branchtype=item.getString();
		  	System.out.println("........save:branchtype"+branchtype);
	  }
	  if(item.getFieldName().compareTo("branchtype2")==0){
		  branchtype2=item.getString();
		  	System.out.println("........save:diskimporttype"+diskimporttype);
	  }
	  
	  
	  //忽略其他不是文件域的所有表单信息
	  if (!item.isFormField())
	  {
	    String name = item.getName();
	    long size = item.getSize();
	
	    if((name==null||name.equals("")) && size==0)
	      continue;
	    ImportPath= path ;
	//    ImportPath= path + ImportPath;
		ImportPath = ImportPath.replace('\\','/');	
	    fileName = name.replace('\\','/');
	    fileName = fileName.substring(fileName.lastIndexOf("/") +1);
	    System.out.println("...............save jsp fileName:"+ fileName);
	
	    //保存上传的文件到指定的目录
	    try
	    {
	    //System.out.println("..............savejsphere:once");
	      item.write(new File(ImportPath + fileName));
	      count = 1;
	      System.out.println("count:"+count);
	    }
	    catch(Exception e){
	      System.out.println("upload file error ...");
	    }
	  }
	}
	
	ExeSQL tExe = new ExeSQL();
//   String tSql = "select int(max(idx)) from laratecommision order by 1 desc ";
//    String strIdx = "";
//    strIdx = tExe.getOneValue(tSql);
//    String tSqlCompare = "select int(max(idx)) from laratecommisioncompare order by 1 desc ";
//    String strIdxCompare = "";
//    strIdxCompare = tExe.getOneValue(tSqlCompare);
	TransferData t = new TransferData();
	t.setNameAndValue("diskimporttype", diskimporttype);
	t.setNameAndValue("branchtype", branchtype);
	t.setNameAndValue("branchtype2", branchtype2);
	String temp=diskimporttype;
	System.out.println("LARiskImportQuarterSave.jsp:diskimporttype"+temp);
//	System.out.println("LARiskImportQuarterSave.jsp:diskimporttype"+temp);
//	System.out.println("LARiskImportQuarterSave.jsp:diskimporttype"+temp);
	t.setNameAndValue("path", path);
	t.setNameAndValue("fileName", fileName);
//	t.setNameAndValue("idx",strIdx);
//	t.setNameAndValue("idxCompare",strIdxCompare);
	VData v = new VData();
	v.add(t);
	v.add(tGI);
    System.out.println("vdata added");
	//从磁盘导入代理人相关信息清单
	LARiskBankImportQuarterUI tLARiskBankImportQuarterUI = new LARiskBankImportQuarterUI();
	if (!tLARiskBankImportQuarterUI.submitData(v, "INSERT"))
	{
        flag = "Fail";
        content = tLARiskBankImportQuarterUI.mErrors.getErrContent();
        System.out.println(tLARiskBankImportQuarterUI.mErrors.getFirstError());
	}
	else
	{
        System.out.println("successful");
        flag = "Succ";
        if(tLARiskBankImportQuarterUI.getImportPersons() > 0||tLARiskBankImportQuarterUI.getUnImportRecords()>0)
        {
            //int totalnum=tDiskImportRateCommisionUI.getImportPersons();
            if(tLARiskBankImportQuarterUI.getUnImportRecords()>0){
            	content = "操作已完成！其中"+tLARiskBankImportQuarterUI.getImportPersons()
            	        +"条记录导入成功，"+tLARiskBankImportQuarterUI.getUnImportRecords()
            	        +"条记录导入失败。详细信息请按导入文件批次进行查询。对导入失败的数据可将数据改正并更改文件的批次后重新导入。";
            }else{
            	content="操作已成功！共有"+tLARiskBankImportQuarterUI.getImportPersons()
            	       +"条记录被成功录入。";
            }
        }
        
        //执行成功，但是有未导入信息
        if(tLARiskBankImportQuarterUI.mErrors.needDealError())
        {
            content += tLARiskBankImportQuarterUI.mErrors.getErrContent();
        }
	}
	System.out.println(content);
	content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

