 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
 
	if(!verifyInput())
 {
 	return false;
 }	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	fm.reset;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    if (fm.action == './LAAscriptionSave.jsp')     
      saveClick=true;
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  
	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证字段的值
function checkagentcode()
{
 var tReturn = parseManageComLimitlike();
   var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
 strSQL = "select * from LAAgent where  1=1  "
          +tReturn
	  //+ getWherePart('AgentCode')
          + strAgent
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');   
  
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("没有此代理人！");
    fm.all('AgentCode').value="";
    fm.all('Name').value="";
    return ;
  } 
  
  var ttArr = new Array();
  ttArr = decodeEasyQueryResult(strQueryResult);
  fm.all('Name').value =ttArr[0][5]; 
}
function checkagentname()
{
 var tReturn = parseManageComLimitlike();
 strSQL = "select * from LAAgent where  InsideFlag<>'0'  "
          +tReturn
	  + getWherePart('Name')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');   
  
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("没有此代理人！");
    fm.all('AgentCode').value="";
    fm.all('Name').value="";
    return ;
  } 
  

}

function agentConfirm()
{
	
	if(!verifyInput())
 {
 	return false;
 }	
  // if((fm.all('AgentCode').value=="")||(fm.all('AgentCode').value==null)) 
 //  {
 //   alert("请输入原代理人编码！");	
 //   fm.all('AgentCode').focus();
 //   return ;
 //  }
   
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tAgentCode = fm.all('AgentCode').value;
  
  initAscriptionGrid(); 
  var tReturn = parseManageComLimitlike();
    var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.AgentOld=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  strSQL=" select distinct a.AscripNo,c.managecom,a.ContNo,getunitecode(a.AgentOld),c.name," 
       +" a.agentcomold,"
       +" getunitecode(a.AgentNew)," 
  		 +" (select name from laagent dd where dd.agentcode=a.agentnew)," 
  		 +" a.agentcomnew," 
		   +" a.AscriptionDate,"
		   +" case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认'  end " 
  		 +" from laascription a,laagent c ,lccont d where c.agentcode=a.agentold " 
  		 +" and  a.AscripState='2' and a.validflag='N' "
  		 +" and a.branchtype='"+fm.all('BranchType').value+"' and a.branchtype2='"+fm.all('BranchType2').value+"'"
  		 +" and a.MakeType='"+fm.all('MakeType').value+"' and a.contno=d.contno and d.grpcontno='00000000000000000000' "
  //     +tReturn
	     //+ getWherePart('a.AgentOld','AgentCode')
  		 + strAgent
	     + getWherePart('c.ManageCom','ManageCom','like')
	     + getWherePart('c.name','Name')
	     + getWherePart('a.contno','ContNo')
	     ;
  strSQL+="union select distinct a.AscripNo,c.managecom,a.GrpContNo,getunitecode(a.AgentOld),c.name," 
       +" a.agentcomold,"
       +" getunitecode(a.AgentNew)," 
  		 +" (select name from laagent dd where dd.agentcode=a.agentnew)," 
  		 +" a.agentcomnew," 
		   +" a.AscriptionDate,"
		   +" case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认'  end " 
  		 +" from laascription a,laagent c ,lcgrpcont d where c.agentcode=a.agentold " 
  		 +" and  a.AscripState='2' and a.validflag='N' "
  		 +" and a.branchtype='"+fm.all('BranchType').value+"' and a.branchtype2='"+fm.all('BranchType2').value+"'"
  		 +" and a.MakeType='"+fm.all('MakeType').value+"' and a.grpcontno=d.grpcontno and d.grpcontno<>'00000000000000000000' "
  //     +tReturn
	     //+ getWherePart('a.AgentOld','AgentCode')
  		 + strAgent
	     + getWherePart('c.ManageCom','ManageCom','like')
	     + getWherePart('c.name','Name')
	     + getWherePart('a.grpcontno','ContNo')
	     ;   
  
  turnPage.queryModal(strSQL, AscriptionGrid); 
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据");
    return ;
    }
  
//查询成功则拆分字符串，返回二维数组
//arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
//turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3,4]);
////设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
//turnPage.pageDisplayGrid = AscriptionGrid;              
////保存SQL语句
//turnPage.strQuerySql     = strSQL;   
////设置查询起始位置
//turnPage.pageIndex       = 0;    
////在查询结果数组中取出符合页面显示大小设置的数组
////arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
//var tArr = new Array();
//tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
////调用MULTILINE对象显示查询结果  
//displayMultiline(tArr, turnPage.pageDisplayGrid);

}   
function submitSave()
{  
   var lineCount = AscriptionGrid.mulLineCount;
   var str='';
   var tAgentCode = fm.all("AgentCode").value;
//   fm.action="./LABankAscripEnsureSave.jsp";
   submitForm();
   return ;
}
function submitSaveb()
{
	fm.all("Flag").value="SELECT";
  tSel=false;
	var lineCount = AscriptionGrid.mulLineCount;
	for( i=0;i<lineCount;i++)
	{	
	 //alert( i );	
	 tSel = AscriptionGrid.getChkNo(i);
	 if(tSel==true)
	 {
	 var agentold=AscriptionGrid.getRowColData(i,4);
	 //alert("原代理人：" +　agentold );	
	 var agentnew=AscriptionGrid.getRowColData(i,7);
	 //alert("新代理人：" +　agentold );	
	 var strSQL =" select managecom from laagent where agentcode=getagentcode('"+agentold+"') and managecom in ("
		             +" select managecom from laagent where agentcode=getagentcode('"+agentnew+"') ) with ur ";
		    var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
		    if(!strQueryResult)
			{
			  alert( "不能跨管理机构!" );			  
			  return false;
			}
	 }
	 if(tSel==true)
	 i=lineCount;	 
    }
	if( tSel == false || tSel == null )
		alert( "请先选择一条记录!" );
	else
	{
	submitSave();
	}
}
function submitSaveall()
{
//	var arrSelected = null;
	fm.all("Flag").value="ALL";

	submitSave();
}

function clearMulLine()
{  
   AscriptionGrid.clearData("AscriptionGrid");
   saveClick=false;
}