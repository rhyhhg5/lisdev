<%@ page contentType="text/html;charset=GBK" %>
<!--*******************************************************
* 程序名称：Title.jsp
* 程序功能：上海人保业务处理系统标题页面
* 最近更新人：卢旭攀
* 最近更新日期：2001-09-23
*******************************************************-->


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<!-- 页面样式  -->
<link rel='stylesheet' type='text/css' href='../common/css/Project.css'>
<script language="JavaScript">
function showTitle(){
	parent.fraMain.rows = "0,0,0,74,*";
}
function showHideFrame(){
	try{
		if(parent.fraSet.cols==	"0%,*,0%"){
			parent.fraSet.cols = "120,*,0%";
			menuPowerImage.src = "../common/images/butHide.gif";
		}
		else if(parent.fraSet.cols=="120,*,0%"){
			parent.fraSet.cols = "0%,*,0%";
			menuPowerImage.src = "../common/images/butShow.gif";
			}
		}
		catch(re){}
	}
</script>
</head>
<body text="#000000" leftmargin="0" topmargin="2" marginwidth="0" marginhigh="2">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center bgcolor="#94FEFC">
		<tr> 
			<td background="../common/images/bg.gif" width=36 height=72 valign=bottom><img name=menuPowerImage src="../common/images/butHide.gif" onclick="showHideFrame();" width="36" height="16"></td>
			<td width=735 height=72><img src="../common/images/logo.gif" width=735 height=72></td>
			<td width=100%>&nbsp;</td>
		</tr>
	</table>
</body>
</html>