<%@page contentType="text/html;charset=GBK" %>
<!--
*******************************************************
* 程序名称：Menus.jsp
* 程序功能：用户登录信息输入页面
* 创建日期：2002-08-11
* 更新记录：  更新人    更新日期     更新原因/内容
*             朱向峰   2002-08-11    新建
              Dingzhong 2002-10-17   修改
*******************************************************
-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.logon.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.menumang.*"%>
<%@page import="com.sinosoft.lis.agentlogon.*"%>

<%@page import="java.util.*"%>
 

<html>
<head></head>
<meta http-equiv=Content-Type content="text/html; charset=GBK">
<link rel='stylesheet' type='text/css' href='../common/css/Project.css'>
<base target='fraInterface'>
<body topmargin="10" onload="Go();" oncontextmenu=self.event.returnValue=false>
	<script type='text/javascript'>function Go(){return}</script>
	<script type='text/javascript' src='menus.js'></script>
	<script type='text/javascript' src='menu8_com.js'></script>
<%
int i = 0;
Vector vector = null;           //装载代码查询结果的对象数组容器
int intPosition=0;
int intFieldNum=5;

System.out.println("start menu get ...");
/*****************************
 *    temp delete
 *****************************/


int[] menuCount = new int[1];
menuCount[0] = 0;

int node_count = 0;
String[][] node = null;	//建立一个二维数组

String sl = "";

VData tData=new VData();
   
LAAgentSchema tLAAgentSchema = new LAAgentSchema();

LAAgentMenuQueryUI tLAAgentMenuQueryUI = new LAAgentMenuQueryUI();

String AgentCode = request.getParameter("AgentCode");
tLAAgentSchema.setAgentCode(AgentCode);
System.out.println(AgentCode);

if ( AgentCode.compareTo("001") != 0) {
   tData.add(tLAAgentSchema);
}

//tData.add(tLAAgentSchema);
tLAAgentMenuQueryUI.submitData(tData,"query"); 

MenuShow menuShow = new MenuShow();  

if (tLAAgentMenuQueryUI.mErrors.needDealError()) {
    System.out.println(tLAAgentMenuQueryUI.mErrors.getFirstError()) ;
    
    //Minim adjust here for MenuShort
    out.println("<script>");
    out.println("var strMenuShort='" + "" + "';");
    out.println("var strMenuOperator='" + AgentCode + "';");
    out.println("</script>");
    
   //调用生成菜单函数
    String menustr = "";
    
    out.println("<script>");
     
    int totalMenu = 3;
    out.println("var NoOffFirstLineMenus=" + totalMenu + ";");

    System.out.println("var NoOffFirstLineMenus=" + totalMenu + ";");
                                                                     
    menuCount[0]++;
    menustr = "Menu1" + " =new Array('重新登录','../agentlogon/logout.jsp','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','重新登陆');";
    out.println(menustr);

    menustr = "Menu2"  + "=new Array('密码修改','../agentlogon/LAAgentChgPwdInput.jsp','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','密码修改');";
    out.println(menustr);

    menustr = "Menu3" + "=new Array('','','',0,MenuHeightBottom,120,'','#4db7b7','#4db7b7','#4db7b7','#4db7b7','black',-1,1,-1,'','');";
    out.println(menustr);
    
    out.println("parent.fraQuick.window.location = '../agentlogon/MenuShortInitialization.jsp';");
    out.println("</script>");
    
} else {
    tData.clear() ; 
    tData=tLAAgentMenuQueryUI.getResult();
    node_count = tLAAgentMenuQueryUI.getResultNum();
    
    node = new String[node_count][5];

    String tStr="";
    tStr = tLAAgentMenuQueryUI.getResultStr();
    //tStr=(String)tData.get(0);
    //tStr=StrTool.unicodeToGBK(tStr);
    sl=tStr;



    
   sl += SysConst.RECORDSPLITER;
    //根据排序正确的字符串给数组重新赋值
   for (i=0 ; i< node_count ; i++) {
       String str = StrTool.decodeStr(sl,SysConst.RECORDSPLITER,i+1);

       for (int j = 0; j < 5; j++) {
              str += "|";
//              System.out.println("...." + str + "....");
              node[i][0] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,1);
              node[i][1] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,2);
	      node[i][2] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,3);
	      node[i][3] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,4);
	      node[i][4] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,5);
	       
      }
   }
   
   
   String  leafNodeStr = menuShow.getAllLeafNodePath(node,node_count);
//   System.out.println("leafNodeStr:" + leafNodeStr); 
   
    //Minim adjust here for MenuShort
    out.println("<script>");
    out.println("var strMenuShort='" + tStr + "';");
    out.println("var strMenuOperator='" + AgentCode + "';");
    out.println("var strMenuLeaf = '" + leafNodeStr + "';");
    out.println("</script>");
   
    //调用生成菜单函数

    out.println("<script>");
     
    System.out.println("menuCount before getMenu");
    System.out.println(menuCount[0]);
    

    
    System.out.println("menushow");
    System.out.println("nodecount:" + node_count);
    System.out.println("menuCount" + menuCount);
    out.println(menuShow.getMenu(node,0,node_count,menuCount));

    System.out.println("menuCount after getMenu");
    System.out.println(menuCount[0]);
    int totalMenu = menuCount[0] + 3; 

    out.println("var NoOffFirstLineMenus=" + totalMenu + ";");
  
    System.out.println("complete getMenu");
    String menustr = "";

    menuCount[0]++;
    menustr = "Menu" + menuCount[0] + " =new Array('重新登录','../agentlogon/logout.jsp','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','重新登陆');";
    System.out.println("menustr");
    System.out.println(menustr);
    out.println(menustr);

    menuCount[0]++;
    menustr = "Menu" +  menuCount[0] + "=new Array('密码修改','../agentlogon/LAAgentChgPwdInput.jsp','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','密码修改');";
    out.println(menustr);

    menuCount[0]++;
    menustr = "Menu" +  menuCount[0] + "=new Array('','','',0,MenuHeightBottom,120,'','#4db7b7','#4db7b7','#4db7b7','#4db7b7','black',-1,1,-1,'','');";
    out.println(menustr);
   
    out.println("parent.fraQuick.window.location = '../agentlogon/MenuShortInitialization.jsp';");
    out.println("</script>");
}



// 
%>
</body>
</html>