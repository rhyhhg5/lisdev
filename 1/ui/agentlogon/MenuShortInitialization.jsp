<!--*******************************************************
* 程序名称：MenuShortInitialization.jsp
* 程序功能：快捷菜单初始化
* 创 建 人：胡  博	
* 最近更新日期：2002-09-23
*******************************************************-->

<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<%@include file="../common/easyQueryVer3/EasyQueryKernel.jsp"%>

<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
String strOperator = tGI.Operator;

//String strSql = "select NodeName,NodeCode,Operator from LDMenuShort where 1=1 and Operator = 'Admin'";
String strSql = "select NodeName, NodeCode, RunScript from LDMenuShort where Operator = '" + strOperator + "' order by NodeOrder";

String strQueryResult = easyQueryKernel(strSql, "1");     //查找快捷菜单表
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@ page contentType="text/html;charset=GBK" %>
<link rel='stylesheet' type='text/css' href='../common/css/Project.css'>
</head>
<style type="text/css">
<!--
	body {FONT-FAMILY: 宋体;font-size:9pt;FONT-WEIGHT: bold}
	td {FONT-FAMILY: 宋体;font-size:9pt;FONT-WEIGHT: bold}
	input {FONT-FAMILY: 宋体;font-size:9pt;FONT-WEIGHT: bold}
-->
</style>

<SCRIPT>
//var MAXMENUSHORTNUM = 5;                       //快捷菜单个数，在COMMON。JS中定义
var arrName = new Array(MAXMENUSHORTNUM);        //专门存储菜单项名称
var arrRunScript = new Array(MAXMENUSHORTNUM);   //专门存储菜单项连接
	
// 数据显示函数
function displayEasyResult(arrDisplayData) {
	var i, j;
	
	for (i=0; i<MAXMENUSHORTNUM; i++) {          //初始化快捷菜单数组
	  arrName[i] = "";
	  arrRunScript[i] = "";
	}

	if (typeof(arrDisplayData) == "undefined" || arrDisplayData == null) {
    return false;	
  }		
  
  try {                                          //拆分查询结果	
		for(i=0; i<arrDisplayData.length; i++) {
			arrName[i] = arrDisplayData[i][0];         //与SQL语句写法密切相关
			arrRunScript[i] = arrDisplayData[i][2];    //与SQL语句写法密切相关
		} 
	} catch(ex) {
	  return false;
	}
	 
}

// 取得数据库查找结果，执行数据分离和显示操作
function initMenuShort()
{
  var strQueryResult = '<%=strQueryResult%>';
  if (strQueryResult == null) {
    return false;
  }
  
  var arrEasyQuery = decodeEasyQueryResult(strQueryResult);
  displayEasyResult(arrEasyQuery);
}

function returnRunScript(index) {
  parent.fraInterface.window.location = arrRunScript[index]; 
}

initMenuShort();
</SCRIPT>

<body text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginhigh="2" >
	
<tr><span id="menuShortNode"></span>

<script>
var innerHTML = '<table width="100%" border="0" cellspacing="1" cellpadding="1" align=center bgcolor=black>';
for (i=0; i<MAXMENUSHORTNUM; i++) {
  innerHTML = innerHTML +  '<td width=15% bgcolor="#4db7b7" height=16 align=center>';
  innerHTML = innerHTML +  '<a href="#"  onclick="returnRunScript(' + i + ')">';
  innerHTML = innerHTML +  '<font color="#013835">';
  innerHTML = innerHTML +  arrName[i];
  innerHTML = innerHTML +  '</font></a></td>';
}
innerHTML = innerHTML +  '<td width=15% bgcolor="#4db7b7" height=16 align=center><font color="blue"><a href="./MenuShortInput.jsp" target="fraInterface">定制快捷菜单</a></font></td>';
innerHTML = innerHTML +  '</tr></table>';
document.all("menuShortNode").innerHTML = innerHTML;  
</script>
	
</body>
</html>

