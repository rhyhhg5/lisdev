<%@page contentType="text/html;charset=GBK" %>
<!--
*******************************************************
* 程序名称：Menus.jsp
* 程序功能：用户登录信息输入页面
* 创建日期：2002-08-11
* 更新记录：  更新人    更新日期     更新原因/内容
*             朱向峰   2002-08-11    新建
              Dingzhong 2002-10-17   修改
*******************************************************
-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.logon.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.menumang.*"%>
<%@page import="java.util.*"%>
 
<%!
//菜单生成主函数
public String getMenu(String[][] strNode, int intIndex , int NodeCount,int[] menuCount)
{       
        System.out.println("start getMenu...");
        System.out.println("NodeCount");
        System.out.println(NodeCount);
	String strMenuReturn = "";
	int z = 1;
	String strMenu = "";
	for (int j=intIndex ; j< NodeCount ; j++ )
	{
		
		if ( strNode[j][0].compareTo("0") == 0 )
		{
                        int[] childNum = new int[1];
                        childNum[0] = 0;
                        
                        String temp = "";
			strMenu = "Menu" + z;                        
                        String childStr = "";
                        String menuStr = "";
                        
                        //得到子节点菜单字符串
			if (strNode[j][3].compareTo("0") != 0){
			     
			        // 判断是否下面的点真是它的子节点
			        if (j == NodeCount)
			          break;
			          
			        if (strNode[j+1][0].compareTo(strNode[j][1]) == 0) {
				    strMenu = strMenu + "_";
				    System.out.println("<<<<" + j);
				    //调用生成子菜单函数
				    childStr = getMenuChild(strNode,j + 1,strNode[j][3],strMenu,NodeCount,childNum);
				    System.out.println("***" + childStr);
				    if (childNum[0] == 0) 
				        continue;
		                 }
				
			}
                        
                        if (strNode[j][3].compareTo("0") != 0 && childNum[0] ==0) {
                            System.out.println("lost:" + z);
                            continue;
			}
			
			if (strNode[j][4].compareTo("") == 0){
				menuStr = "Menu" + z + "=new Array('" + strNode[j][2] + "','',''," + childNum[0] + ",MenuHeight,120,'','','','','','',-1,1,-1,'','" + strNode[j][2] + "');";
			}
			else{
				menuStr = "Menu" + z + "=new Array('" + strNode[j][2] + "','" + strNode[j][4]+ "',''," + childNum[0] + ",MenuHeight,120,'','','','','','',-1,1,-1,'','" + strNode[j][2] + "');";
			}
			
			z++;
			menuCount[0]++;
			
			strMenuReturn += menuStr + childStr;
			
			System.out.println("<<<<<" + strMenuReturn);
			
		}
	}
	
	System.out.println(strMenuReturn);

	return strMenuReturn;
}
//子菜单递归生成函数
public String getMenuChild(
    String[][] strNode, //菜单节点二维数组
    int intIndex ,      //第一个子节点可能的位子
    String ChildCount,  //菜单节点拥有的子节点总数
    String strMenu,     //子节点菜单前缀
    int nodeCount,      //全局菜单节点总数
    int[] childNum)     //菜单节点拥有的实际子节点数
{
        childNum[0] = 0;
        
	String strMenuChildReturn = "";
	int k = intIndex;
	String strParent = strNode[intIndex][0];	//取得当前节点的父节点
	
	    
	Integer tInteger = new Integer(ChildCount);	//转化父节点的字节点总数
	int ForCount = tInteger.intValue();
	
	//循环字节点的个数，好处是快，嘿嘿
	System.out.println(ForCount);
	
	for (int y=1; y<= ForCount; y++)
	{
		if (intIndex >= nodeCount)
		    break;
		    
		    
		//判定是否当前节点为父节点，如果是则递归调用函数getMenuChild
		if (strNode[intIndex][3].compareTo("0") != 0 )
		{       
		        System.out.println(">>>>" + intIndex);
		        int[] myChildNum = new int[1];
			if (strNode[intIndex+1][0].compareTo(strNode[intIndex][1]) == 0) {
			    System.out.println(">>>>" + intIndex);
			    int tempid = childNum[0]+1;
			    String strMenus = strMenu + tempid + "_";	//递归层次，0层：menu1；1层:menu1_1；2层：menu1_1_1；类推
			    String temp = getMenuChild(strNode,intIndex + 1,strNode[intIndex][3],strMenus,nodeCount,myChildNum);
//			    System.out.println(">>>>" + temp);
			    
			    //myChildNum不为0表示本菜单节点是有效的
			    if (myChildNum[0] != 0) {
			        childNum[0]++;
			        strMenuChildReturn += strMenu + childNum[0]+ "=new Array('" + strNode[intIndex][2] + "','',''," + myChildNum[0] + ",MenuHeightChild,150,'','','','','','',-1,1,-1,'','" + strNode[intIndex][2] + "');";
			        strMenuChildReturn = strMenuChildReturn + temp; 
			        System.out.println(">>>>" + strMenuChildReturn);
			        
			    }
			    
			    if (intIndex == nodeCount)
			        break;
	                } 		
		}
		else
		{
//		        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
			childNum[0]++;
			strMenuChildReturn = strMenuChildReturn + strMenu + childNum[0];
			//判定是否有相应连接，并相应改变颜色
			if (strNode[intIndex][4].compareTo("") == 0)
			{
				strMenuChildReturn = strMenuChildReturn + "=new Array('" + strNode[intIndex][2] + "','',''," + strNode[intIndex][3] + ",MenuHeightChild,150,'','','','','','',-1,1,-1,'','" + strNode[intIndex][2] + "');";
			}
			else
			{
				strMenuChildReturn = strMenuChildReturn + "=new Array('" + strNode[intIndex][2] + "','" + strNode[intIndex][4] + "',''," + strNode[intIndex][3] + ",MenuHeightChild,150,'','','','','','',-1,1,-1,'','" + strNode[intIndex][2] + "');";
			}

		}
		//将指针指到父节点为strParent的下一个数据
//		System.out.println("Come on here!!!!!!!!!!!");
//		System.out.println("intIndex:" + intIndex);
//		System.out.println("nodeCount:" + nodeCount);
		k = nodeCount;
		if (intIndex + 1 < nodeCount){
//		        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
			for (int i=intIndex + 1; i < nodeCount; i++)
			{
				if (strNode[i][0].compareTo(strParent) == 0)
				{
					k = i;	//一旦找到，立即退出循环
					break;
				}
			}
//			System.out.println("OKOKOK" + intIndex);
		}
		intIndex = k;	//并将该指针数据赋给intIndex
		//这样的坏处是如果缺少相应的字节点个数，可能导致最后一个字节点重复出现
	}
	return strMenuChildReturn;
}
%>
<html>
<head></head>
<meta http-equiv=Content-Type content="text/html; charset=GBK">
<link rel='stylesheet' type='text/css' href='../common/css/Project.css'>
<base target='fraInterface'>
<body topmargin="10" onload="Go();" oncontextmenu=self.event.returnValue=false>
	<script type='text/javascript'>function Go(){return}</script>
	<script type='text/javascript' src='menus.js'></script>
	<script type='text/javascript' src='menu8_com.js'></script>
<%
int i = 0;
Vector vector = null;           //装载代码查询结果的对象数组容器
int intPosition=0;
int intFieldNum=5;

System.out.println("start menu get ...");
/*****************************
 *    temp delete
 *****************************/


int[] menuCount = new int[1];
menuCount[0] = 0;

int node_count = 0;
String[][] node = null;	//建立一个二维数组

String sl = "";

VData tData=new VData();
   
LDUserSchema tLDUserSchema = new LDUserSchema();
//LDMenuQueryBL tLDMenuQueryBL = new LDMenuQueryBL();

LDMenuQueryUI tLDMenuQueryUI = new LDMenuQueryUI();

String userCode = request.getParameter("userCode");
tLDUserSchema.setUserCode(userCode);
System.out.println(userCode);
if ( userCode.compareTo("001") != 0) {
   tData.add(tLDUserSchema);
}
  tLDMenuQueryUI.submitData(tData,"query");   
//tLDMenuQueryBL.submitData(tData,"query");
if (tLDMenuQueryUI.mErrors.needDealError())
{
    System.out.println(tLDMenuQueryUI.mErrors.getFirstError()) ;
    
   //调用生成菜单函数
    String menustr = "";
    out.println("<script>");
     
    int totalMenu = 3;
    out.println("var NoOffFirstLineMenus=" + totalMenu);
  
    menuCount[0]++;
    menustr = "Menu1" + " =new Array('重新登陆','','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','重新登陆');";
    out.println(menustr);

    menustr = "Menu2"  + "=new Array('用户密码修改','','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','用户密码修改');";
    out.println(menustr);

    menustr = "Menu3" + "=new Array('','','',0,MenuHeightBottom,120,'','#4db7b7','#4db7b7','#4db7b7','#4db7b7','black',-1,1,-1,'','');";
    out.println(menustr);
   
    out.println("parent.fraQuick.window.location = '../hb/LDMenuShortQuery.jsp?Operator=Admin';");
    out.println("</script>");
    
}
else
{
    tData.clear() ; 
    tData=tLDMenuQueryUI.getResult();
    node_count = tLDMenuQueryUI.getResultNum();
    
    node = new String[node_count][5];

    String tStr="";
    tStr = tLDMenuQueryUI.getResultStr();
    //tStr=(String)tData.get(0);
    //tStr=StrTool.unicodeToGBK(tStr);
    sl=tStr;

    //Minim adjust here for MenuShort
    out.println("<script>");
    out.println("var strMenuShort='" + tStr + "';");
    out.println("var strMenuOperator='Admin';");
    out.println("</script>");

    System.out.println(tStr) ;
    
   sl += SysConst.RECORDSPLITER;
    //根据排序正确的字符串给数组重新赋值
   for (i=0 ; i< node_count ; i++) {
       String str = StrTool.decodeStr(sl,SysConst.RECORDSPLITER,i+1);

       for (int j = 0; j < 5; j++) {
              str += "|";
              System.out.println("...." + str + "....");
              node[i][0] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,1);
              node[i][1] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,2);
	      node[i][2] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,3);
	      node[i][3] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,4);
	      node[i][4] = StrTool.decodeStr(str,SysConst.PACKAGESPILTER,5);
	      
	      System.out.println(node[i][0]);
	      System.out.println(node[i][1]);
	      System.out.println(node[i][2]);
	      System.out.println(node[i][3]);
	      System.out.println(node[i][4]); 	  
      }
   }
/********************原来的算法***********************************************
    for (i=0 ; i< node_count ; i++) {
        intPosition = StrTool.getPos( sl , StrTool.PACKAGESPILTER , intFieldNum );
        if (intPosition == sl.length()-1)  {
              String tempStr =  sl;
              node[i][0] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,1);
              node[i][1] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,2);
	      node[i][2] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,3);
	      node[i][3] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,4);
	      node[i][4] = StrTool.decodeStr(sl,SysConst.RECORDSPLITER,1);
               	  
	      System.out.println(node[i][0]);
//	      System.out.println(node[i][1]);
	      System.out.println(node[i][2]);
	      System.out.println(node[i][3]);
//	      System.out.println(node[i][4]); 	  
	      break;
         }  else  {
            
              String tempStr =  sl.substring(0, intPosition + 1);
              node[i][0] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,1);
	      node[i][1] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,2);
              node[i][2] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,3);
	      node[i][3] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,4);
	      node[i][4] = StrTool.decodeStr(sl,SysConst.RECORDSPLITER,1);
	      
	      if ( i < 10) {
	      System.out.println(node[i][0]);
	      System.out.println(node[i][1]);
	      System.out.println(node[i][2]);
	      System.out.println(node[i][3]);
	      System.out.println(node[i][4]);
              }
              sl = sl.substring(intPosition + 1);
         }
    }
 *****************************************************************************/   
    
    //调用生成菜单函数

    out.println("<script>");
     
    System.out.println("menuCount before getMenu");
    System.out.println(menuCount[0]);
    out.println(getMenu(node,0,node_count,menuCount));
    System.out.println("menuCount after getMenu");
    System.out.println(menuCount[0]);
    int totalMenu = menuCount[0] + 3;
    out.println("var NoOffFirstLineMenus=" + totalMenu);
  
    System.out.println("complete getMenu");
    String menustr = "";

    menuCount[0]++;
    menustr = "Menu" + menuCount[0] + " =new Array('重新登陆','','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','重新登陆');";
    System.out.println("menustr");
    System.out.println(menustr);
    out.println(menustr);

    menuCount[0]++;
    menustr = "Menu" +  menuCount[0] + "=new Array('用户密码修改','','',0,MenuHeight,120,'','','','','','',-1,1,-1,'','用户密码修改');";
    out.println(menustr);

    menuCount[0]++;
    menustr = "Menu" +  menuCount[0] + "=new Array('','','',0,MenuHeightBottom,120,'','#4db7b7','#4db7b7','#4db7b7','#4db7b7','black',-1,1,-1,'','');";
    out.println(menustr);
   
    out.println("parent.fraQuick.window.location = '../logon/MenuShortInitialization.jsp';");
    out.println("</script>");
}
%>
</body>
</html>