<%@page contentType="text/html;charset=GBK" %>
<!--
*******************************************************
* 程序名称：Menu.jsp
* 程序功能：用户登录信息输入页面
* 创建日期：2002-05-10
* 更新记录：  更新人    更新日期     更新原因/内容
*             朱向峰   2002-05-10    新建
*             欧阳晟   2002-05-20    修改生成菜单算法
*******************************************************
-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.logon.*"%>
<%@page import="java.util.*"%> 


<%! 
/**************************************************************
 *递归父节点不为0的数据节点
 *参数，数组，父节点值，数组个数
 **************************************************************
 */
public String node_Item(String[][] array_node,String fa_string,int intCount)
{
  String strValue = "";
		
  for (int m=0;m<intCount;m++)
  {
    if (array_node[m][0].compareTo(fa_string) == 0)
    {
	  strValue = strValue + array_node[m][0] + StrTool.PACKAGESPILTER + array_node[m][1] + StrTool.PACKAGESPILTER + array_node[m][2] + StrTool.PACKAGESPILTER + array_node[m][3] + StrTool.PACKAGESPILTER + array_node[m][4] + StrTool.PACKAGESPILTER;
	  strValue = strValue + node_Item(array_node,array_node[m][1],intCount);
	}
  }
  return strValue;
}
	 
/************************************************************** 
 *递归函数入口，父节点为0
 *参数：数组strNode，数组的个数intCount，确定循环
 **************************************************************
 */
public String getNodeItem(String[][] strNode, int intCount)
{
  String strReturn = "";	//初始化返回字串
  for (int j=0;j<intCount;j++)
  {
	if (strNode[j][0].compareTo("0") == 0)
	{
	  strReturn = strReturn + strNode[j][0] + StrTool.PACKAGESPILTER + strNode[j][1] +
	              StrTool.PACKAGESPILTER + strNode[j][2] + StrTool.PACKAGESPILTER + 
	              strNode[j][3] + StrTool.PACKAGESPILTER + strNode[j][4] + 
	              StrTool.PACKAGESPILTER;
	  strReturn = strReturn + node_Item(strNode,strNode[j][1],intCount);
	}           
  }
  return strReturn;
}
       
/**************************************************************   
 *页节点处理；
 *strHref:超连接文件名; strChildDisplay:菜单项显示名称; intvalue:node_valuel;
 **************************************************************
 */
public String getMenuItem(String strHref,String strChildDisplay, int intLevel)
{
  String tdItemLink = "<table class=menu><tr><td class=itemLink2>";
  String strChildReturn = "";
  strChildReturn = tdItemLink + "<A class=menu href='" + strHref + "' target='fraInterface'>" 
                   + strChildDisplay + "</A></td></tr></table>";
  //返回页节点字符串
  
  return strChildReturn;
}                  
	
/**************************************************************   
 *页节点处理；
 *strHref:超连接文件名; strChildDisplay:菜单项显示名称; intvalue:node_valuel;
 **************************************************************
 */	
public String getMenuParent(String[][] strNode, int intIndex , int NodeCount ,int levelCounter )
{
  String strHtmlReturn = "";	              
  String strParentValue = strNode[intIndex][0];//取当前节点的父节点值
  levelCounter = levelCounter + 1;		
  		    
  for (int j=intIndex ; j<( NodeCount ); j++ )
  {
    //循环全部数组，查询父节点为strParentValue的节点
    if ( strNode[j][0].compareTo( strParentValue ) == 0 )
	{   
	  if ( isParent( strNode, j ,NodeCount )== 0 )
	  {     
	    String clickNode = "";
		clickNode = "T" + strNode[j][1];
						            
		strHtmlReturn = strHtmlReturn + 
		  		        "<table class=menu><tr><td class=titleLink"+ levelCounter +
		  		        " onclick='showMenu(" + clickNode + ")'>" + strNode[j][2] + 
		  		        "</td></tr></table>";
		
		strHtmlReturn = strHtmlReturn + "<div id='" + clickNode + "' style='display:none'>";
					
		//递归下一个节点是否为父节点  
		strHtmlReturn = strHtmlReturn + getMenuParent(strNode,j + 1,NodeCount , levelCounter );  
		strHtmlReturn = strHtmlReturn + "</div>"; 
	  }
	  else
	  { 		
	    if (strNode[j][4].compareTo("") == 0)        //判定是否是父节点			
		{
		  String clickNode = "";      
		  clickNode = "T" + strNode[j][1];
		  strHtmlReturn = strHtmlReturn + 
						  "<table class=menu><tr><td class=titleLink" + levelCounter + 
						  " onclick='showMenu(" + clickNode + ")'>" + strNode[j][2]  + 
						  "</td></tr></table>";
		  strHtmlReturn = strHtmlReturn + 
						  "<div id='" + clickNode + "' style='display:none'>";
				
		  strHtmlReturn  = strHtmlReturn + "</div>"; 
		  
		}
		else                        
		{
		  strHtmlReturn = strHtmlReturn + getMenuItem(strNode[j][4],strNode[j][2],1);
		}
	  }
	}
  }				
  return strHtmlReturn;
}                                                         

/**************************************************************   
 *节点性质判断
 **************************************************************
 */	
public int isParent(String[][] strNode,int intIndex,int NodeCount)
{    
  int i = 0 ;
  if (intIndex  >= NodeCount-1)
  {
    return 1;
  }
		
  String strParent=strNode[intIndex+1][0];
  for ( i=0 ; i<=intIndex ;i++)
  {   
    if (strNode[i][0].compareTo(strParent)==0)
	{
	  return 1;
	}			
  }   
  return 0 ;
}	
%>

<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=GBK">
<link rel='stylesheet' type='text/css' href='../common/css/Project.css'>

<!-- JavaScript脚本-->
<script language=JavaScript>
//显示菜单事件
function showMenu(divID)
{
  if (divID.style.display == "")
	divID.style.display="none";
  else
	divID.style.display="";
} 

function loadForm()
{
  var i;
  for(i=0;i<document.getElementsByTagName("A").length;i++)
  {
	document.getElementsByTagName("A")[i].onclick = clickme;
  }
}

function clickme()
{
  parent.fraSet.cols = "134,*,0%";
}

function relogon()
{
  parent.fraSet.cols = "0%,*,0%";
  parent.fraInterface.window.location="LogonInput.jsp";
}
</script>

</head>
<body bgColor=#5c9c7c>


<%
int i = 0;                
Vector vector = null;           //装载代码查询结果的对象数组容器
int intPosition=0;   
int intFieldNum=5;


/*****************************   
 *    temp delete
 *****************************/
/*UsrGrade usrgrade = new UsrGrade();
vector = usrgrade.usrGrade(usr.getBm_usr());
                                                                        
int node_count = vector.size();	//将数值整形化*/
 
int node_count = 157;
if (node_count >0)
{
	/****************************
	 *   temp delete
	 ****************************/
	String[][] node = new String[node_count][5];	//建立一个二维数组
	//给数组赋值
	/*for (i=0 ; i< node_count ; i++)
	{
		node[i][0] = ChgData.chgData( ( (Menu_scriptScm)vector.elementAt(i) ).getVon_node() );
		node[i][1] = ChgData.chgData( ( (Menu_scriptScm)vector.elementAt(i) ).getCurr_node() );
		node[i][2] = ((Menu_scriptScm)vector.elementAt(i)).getM_script();
		node[i][3] = ((Menu_scriptScm)vector.elementAt(i)).getNode_value();
		node[i][4] = ((Menu_scriptScm)vector.elementAt(i)).getRun_command();
	}
	//调用数组排序
	String sl = getNodeItem(node,node_count);                         */

    String sl = "0|1|Web业务处理Demo|1||" +
                  "1|101|承保|1||" +
                    "101|1011|个人投保单录入|1|../app/ProposalInput.jsp|" +
                  "1|102|核保|2||" + 
                    "102|1021|个人保单核保|1|../uw/Underwrite.jsp|" + 
                    "102|1022|核保订正|2|../uw/UWRecover.jsp|" + 
                  "1|103|保全|3|blank.html|" + 
                    "103|1031|复效|1|blank.html|" + 
                  "1|104|财务收付费|4||" +
                    "104|1041|财务收费|1|blank.html|" + 
                    "104|1042|暂收费录入|2|blank.html|" + 
                    "104|1043|暂收费加费|3|blank.html|" + 
                    "104|1044|财务付费|4|blank.html|" + 
                  "1|105|业务收付费|5||" + 
                    "105|1051|正常交费|1|blank.html|" + 
                    "105|1052|续期交费|2|blank.html|" + 
                  "1|106|生存领取|6|blank.html|" + 
                    "106|1061|个人生存领取|1|blank.html|" + 
                  "1|107|案件处理|7||" + 
                    "107|1071|报案|1|blank.html|" + 
                    "107|1072|立案|2|blank.html|" + 
                    "107|1073|调查报告|3|blank.html|" + 
                    "107|1074|赔案|4|blank.html|" + 
                    "107|1075|拒赔|5|blank.html|" + 
                    "107|1076|案件查询|6|blank.html|" + 
                  "1|108|基础信息维护|8|blank.html|" + 
                    "108|1081|客户信息录入|1|../sys/LDPersonInput.jsp|";
    MenuUI tMenuUI=new MenuUI();
    VData tData=new VData();
    tMenuUI.submitData(tData,"QUERY||MAIN");
    if (tMenuUI.mErrors.needDealError())
    {
      System.out.println(tMenuUI.mErrors.getFirstError()) ;
      
    }
    else
    {
      tData.clear() ;
      tData=tMenuUI.getResult() ;
      String tStr="";
      tStr=(String)tData.get(0);
      tStr=StrTool.unicodeToGBK(tStr);
      sl=tStr;
      System.out.println(tStr) ;
    }

    
	//根据排序正确的字符串给数组重新赋值

	for (i=0 ; i< node_count ; i++)
	{
		intPosition = StrTool.getPos( sl , StrTool.PACKAGESPILTER , intFieldNum );
	    if (intPosition == sl.length()-1)
        { 
          String tempStr =  sl;
          node[i][0] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,1);
		  node[i][1] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,2);
		  node[i][2] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,3);
		  node[i][3] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,4);
		  node[i][4] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,5); 
		  break;                         
          
		}
        else
        {  
          String tempStr =  sl.substring(0, intPosition + 1);
          node[i][0] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,1);
		  node[i][1] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,2);
		  node[i][2] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,3);
		  node[i][3] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,4);
		  node[i][4] = StrTool.decodeStr(sl,SysConst.PACKAGESPILTER,5);
          sl = sl.substring(intPosition + 1);
        }      		
	}

	//调用生成菜单函数
	//out.println(getMenuParent(node,0,node_count,0));  
}         
%>
<script language="javascript">
	document.write("<script language=\"javascript\" src=\"menu.js\"><\/script>");
</script>
	<table class=menu cellspacing=0 cellpadding=0>
		<tr>
			<td class=itemLink text-align=center onclick="return relogon();">重新登录</td>
		<tr>
		<tr>
			<td class=itemLink>用户密码修改</td>
		<tr>
	</table>
</body>
<script language="javascript">
  showMenu(T1001);
  showMenu(T1002);
  showMenu(T1018);
  showMenu(T1031);
//  showMenu(T108);
	
</script>
</html>
