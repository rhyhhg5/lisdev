 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,NewBranchAttr )
{
  //fm.reset();	
  initForm();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //fm.all('NewBranchAttr').value=NewBranchAttr;
    
    // tianjia --查询调整后团队信息
    var after_sql = "select branchattr from labranchgroup where agentgroup = '"+fm.all('hideAdjustAgentGroup').value+"'";
    var arr_temp = easyExecSql(after_sql);
    fm.all('NewBranchAttr').value=arr_temp[0][0];

    //执行下一步操作
    //fm.all('AdjustBranchCode').value = '';
    //fm.all('AdjustAfterBranchCode').value = '';
    BranchChange();
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
	var tReturn = parseManageComLimitlike();
	var strSQL = "select AgentGroup,managecom,costcenter  from LABranchGroup where 1=1  and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null) "   
          + tReturn
          + getWherePart('BranchAttr','AdjustBranchCode')// 需调整机构外部编码
          + getWherePart('BranchType','BranchType')
          + getWherePart('BranchType2','BranchType2');
  var arr = easyExecSql(strSQL);
	fm.hideAdjustAgentGroup.value=arr[0][0] ;
	var tManageCom=arr[0][1] ;
	var tCostCenter=arr[0][2] ;
	strSQL = "select AgentGroup,managecom ,costcenter from LABranchGroup where 1=1 and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null) "
          + tReturn
          + getWherePart('BranchAttr','AdjustAfterBranchCode')// 调往机构外部编码
          + getWherePart('BranchType','BranchType')
          + getWherePart('BranchType2','BranchType2');
  arr = easyExecSql(strSQL);
  var tupManageCom=arr[0][1] ;
  var tupCostCenter=arr[0][2] ;
	fm.AdjustAfterAgentGroup.value=arr[0][0] ;
	if(tupManageCom != tManageCom )
	{
		alert("不能在不同的管理机构间调动团队！");	
    return false;
	}
	// 注:个险新需求 支持跨成本中心团队调整
	//if(tupCostCenter != tCostCenter )
	//{
	//	alert("不能在不同的成本中心调动团队！");	
    //    return false;
	//}
	return true ;
	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证字段的值
function BranchConfirm()
{
   fm.all('AdjustBranchCode').value=trim(fm.all('AdjustBranchCode').value);
   fm.all('AdjustAfterBranchCode').value=trim(fm.all('AdjustAfterBranchCode').value);
   if((trim(fm.all('AdjustBranchCode').value)=="")||(fm.all('AdjustBranchCode').value==null))
   {    
    alert("请先录入被调整机构代码！");	
    fm.all('AdjustBranchCode').focus();
    return false;
   }
    if((fm.all('AdjustAfterBranchCode').value==null)||(trim(fm.all('AdjustAfterBranchCode').value)=="")) 
   {    
    alert("请先录入调整目标机构代码！");	
    fm.all('AdjustBranchCode').focus();
    return false;
   }
  var tReturn = parseManageComLimitlike();
   
  var strSQL = "";
  //查询出非停业且展业级别在营业组的
  strSQL = "select AgentGroup,managecom,UpBranch,branchLevel,BranchManager,BranchManagerName,Name from LABranchGroup where 1=1 and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null) "
          + tReturn
          + getWherePart('BranchAttr','AdjustBranchCode')
          + getWherePart('BranchType','BranchType')
          + getWherePart('BranchType2','BranchType2');

  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  //判断是否查询成功
  if (!strQueryResult) {
    alert(" 被调整机构代码无效！");
    fm.all('AdjustBranchCode').value="";
    fm.all('hideAdjustAgentGroup').value="";
    fm.all('UpBranchAttr').value="";
    fm.all('hideLevel').value = "";
    fm.all('hideCom').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('AdjustBranchCode').focus();
    return false;
  }   
  var arr = decodeEasyQueryResult(strQueryResult);
  //branchlevel is not null
  if (trim(arr[0][3])=="")
  {
    alert('被调整单位的机构级别无效！');
    fm.all('AdjustBranchCode').focus();
    return false;
  }
  
  
  //被调整机构信息     
  fm.all('hideAdjustAgentGroup').value= arr[0][0]; //隐式机构内部编码
  fm.all('hideUpBranch').value = arr[0][2];     //被调整机构上级机构
  fm.all('hideLevel').value = arr[0][3];     //被调整机构级别
  fm.all('hideCom').value = arr[0][1];       //被调整机构的管理机构
  fm.all('AdjustBranchName').value =  arr[0][6];
  
  fm.all('BranchManagerName').value =arr[0][5];
  strSQL="select branchmanager,branchmanagername,name from labranchgroup where 1=1 and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null) "
          + tReturn
          + getWherePart('BranchAttr','AdjustAfterBranchCode')
          + getWherePart('BranchType','BranchType')
          + getWherePart('BranchType2','BranchType2');
  
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult) {
    alert("调整目标机构代码无效!");   
    fm.all('AdjustAfterBranchCode').focus();
    return false;
  }   
  arr = decodeEasyQueryResult(strQueryResult);
  fm.all('AdjustAfterBranchName').value=arr[0][2];
}

/************************************
   注: 点击保存按钮
 ************************************/
function submitSave()
{   
   
   if(beforeSubmit()==false) return false;// 校验是否跨成本中心
   if((trim(fm.all('AdjustBranchCode').value)=="")||(fm.all('AdjustBranchCode').value==null)||(fm.all('AdjustAfterBranchCode').value==null)||(trim(fm.all('AdjustAfterBranchCode').value)=="")) 
   {
      alert("请输入机构代码！");	
      return false;
   }  
   if (fm.all('hideAdjustAgentGroup').value == null || fm.all('hideAdjustAgentGroup').value == '')
   {
   	alert("录入完被调整机构代码请点击'保存'按钮！");
   	return false;
   }
   if((fm.all('AdjustAfterBranchName').value==null)||(trim(fm.all('AdjustAfterBranchName').value)=="")) 
   {
      alert("请输入目标机构名称！");	
      return false;
   }     
   if (!verifyInput()) return false;
     submitForm();
     
}

function BranchChange()
{
   fm.all('hideLevel').value = '';
   fm.all('hideCom').value = '';
   fm.all('hideUpBranch').value = '';
   fm.all('hideAdjustAgentGroup').value = '';
   //BranchConfirm1();
   //clearGrid();
   //saveClick=false;
}

function BranchChange1()
{
   fm.all('hideLevel').value = '';
   fm.all('hideCom').value = '';
   fm.all('hideUpBranch').value = '';
   fm.all('hideAdjustAgentGroup').value = '';
   BranchConfirm1();
   //clearGrid();
   //saveClick=false;
}

function BranchChange2()
{
   fm.all('AdjustAfterBranchName').value = '';
   fm.all('AdjustDate').value = '';
   fm.all('NewBranchAttr').value = '';
   //fm.all('hideAdjustAgentGroup').value = '';
   BranchConfirm2();
   //clearGrid();
   //saveClick=false;
}

function clearGrid()
{
   fm.all('AdjustBranchCode').value = '';
   fm.all('AdjustBranchName').value = '';
   fm.all('AdjustAfterBranchCode').value = '';
   fm.all('AdjustAfterBranchName').value = '';
   fm.all('NewBranchAttr').value='';
   //BranchGrid.clearData("BranchGrid");
   //saveClick=false;
}

function getBranchName(tBranchCode)
{
	var tBranchName='';
	var strSQL = "select Name from LABranchGroup where 1=1 and AgentGroup='"+tBranchCode
             +"'";
             //alert(strSQL);
  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  if (strQueryResult)
  {
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  tBranchName = arrDataSet[0][0]; 
	}
	//alert(tBranchName);
  return tBranchName;
}

/***************************************
  注:得到主管信息
  *************************************/
function getBranchManager(tBranchCode){
	var tBranchManager='';
	var tBranchType = fm.BranchType.value;
	var tBranchType2 = fm.BranchType2.value;
	var strSQL = "select getUniteCode(BranchManager),BranchManagerName from LABranchGroup where 1=1 and branchattr='"+tBranchCode
             +"' and  branchtype='"+tBranchType+"' and  branchtype2='"+tBranchType2+"' ";
  var arr = easyExecSql(strSQL);
 
  return arr;
}

/***************************************
  注:双击返回
  *************************************/
function afterCodeSelect( cCodeName, Field ) 
{
  if( cCodeName == "branchattr"){
  	var adminrr = getBranchManager(fm.AdjustBranchCode.value);
  	if(adminrr){
  		fm.BranchManager.value=adminrr[0][0];
  		fm.BranchManagerName.value=adminrr[0][1];
  	}
  }
}
/***********************************
   注: 得到机构信息
 ***********************************/
function getChangeComName(cObj,cName)
{
  if (fm.all('AddManageCom').value ==null || trim(fm.all('AddManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#1# and BranchType2=#01# and ManageCom like #" + fm.all('AddManageCom').value + "%# and EndFlag=#N# ";
	showCodeList('branchattr',[cObj,cName],[0,1],null,strsql,'1',1);
}


