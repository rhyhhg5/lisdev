<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LABranchGroupInput.jsp
	//程序功能：
	//创建日期：
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>

<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LABranchGroupBuildInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LABranchGroupBuildInit.jsp"%>
</head>
<body onload="initElementtype();">
<form action="./LABranchGroupBuildSave.jsp" method=post name=fm target="fraSubmit">
<%@include file="../common/jsp/OperateAgentButton.jsp"%>
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLABranchGroup1);">
		<td class=titleImg>团队建设录入</td>
	</tr>
</table>
<Div id="divLABranchGroup1" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title>营销服务部代码</TD>
		<TD class=input><Input class=common name=BranchAttr verify="营销服务部代码|notnull&len=10"  elementtype=nacessary onchange="return getBranch();">
		</TD>
		<TD class=title>销售单位名称</TD>
		<TD class=input><Input class=readonly name=Name>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class="readonly" readonly name=ManageCom>
		</TD>
		<TD class=title>级别</TD>
		<TD class=input><Input class="readonly" readonly name=BranchLevel>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>团队主管</TD>
		<TD class=input><Input class="readonly" readonly name=GroupAgentCode>
		</TD>
		<TD class=title>主管姓名</TD>
		<TD class=input><Input class="readonly" readonly name=BranchManagerName>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>申请标志</TD>
		<TD class=input><Input class='codeno' name=ApplyGBFlag
			verify="申请标志|notnull" CodeData="0|^Y|是"
			ondblclick="return showCodeListEx('ApplyGBFlag',[this,ApplyGBFlagName],[0,1]);"
			onkeyup="return showCodeListKeyEx('ApplyGBFlag',[this,ApplyGBFlagName],[0,1]);"
			readonly ><Input class=codename name=ApplyGBFlagName readOnly elementtype=nacessary>
			</TD>			
		<TD class=title>申报标志起期</TD>
		<TD class=input><Input class='coolDatePicker' name=ApplyGBStartDate
			verify="申报标志起期|notnull&DATE" format='short' elementtype=nacessary>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>团队建设类型</TD>
		<TD class=input><Input class='codeno' name=GbuildFlag
		    verify="团队建设类型|notnull"
		    CodeData="0|^A|A类|^B|B类"
			ondblclick="return showCodeListEx('GbuildFlag',[this,GbuildFlagName],[0,1]);"
			onkeyup="return showCodeListKeyEx('GbuildFlag',[this,GbuildFlagName],[0,1]);"
			readonly><Input class=codename name=GbuildFlagName elementtype=nacessary>
	   </TD>
		<TD class=title>团队建设起期</TD>
		<TD class=input><Input class='coolDatePicker' name=GbuildStartDate
			verify="团队建设起期|notnull&DATE" format='short'  onchange="return getEndDate();" elementtype=nacessary>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>团队建设止期</TD>
		<TD class=input><Input class="readonly" readonly name=GbuildEndDate>
		</TD>
		<TD class=title>操作员代码</TD>
		<TD class=input><Input class="readonly" readonly name=Operator value = <%=Operator %>>
		</TD>
	</TR>
</table>
</Div>
<input type=hidden name=hideOperate value=''> 
<input type=hidden name=BranchType value=<%=BranchType %>>
<input type=hidden name=BranchType2 value=<%=BranchType2 %>> 
<input type=hidden name=AgentGroup value=''> <!--后台操作的隐式机构编码，不随机构的调整而改变 --> 
<input type=hidden name=FoundDate value=''>
<input type=hidden name=ReturnFlag value=''>
<input type=hidden name=BranchManager value=''>
<span  id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
