<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupQueryInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType22222:"+BranchType2);
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and branchlevelcode<#03# ";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LABranchGroupBuildQuery.js"></SCRIPT >
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LABranchGroupBuildQueryInit.jsp"%>
<title>销售团队</title>
</head>
<body onload="initForm();initElementtype();">
  <form action="./*" method=post name=fm target="fraSubmit">
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            销售团队代码
          </TD>
          <TD  class= input>
            <Input class=common name=BranchAttr >
          </TD>
          <TD  class= title>
            销售团队名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
        </TR>
        <TR  class= common>
           <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
           <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD> 
     <TD  class= title>
            级别
          </TD>
	      <TD class=input><Input class='codeno' name=BranchLevel verify="级别|NOTNULL" CodeData="0|^03|营销服务部"
			ondblclick="return showCodeListEx('BranchLevel',[this,BranchLevelName],[0,1]);"
			onkeyup="return showCodeListKeyEx('BranchLevel',[this,BranchLevelName],[0,1]);"
			readonly ><Input class=codename name=BranchLevelName readOnly elementtype=nacessary>
			</TD>			
          </TR>
        <TR class=common>
          <TD  class= title>
            团队主管代码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchManager >
          </TD>
        <TD  class= title>
            团队主管名称
          </TD>
          <TD  class= input>
            <Input class= common name=BranchManagerName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            团队成立日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' name=FoundDate format='short'>
          </TD>
          <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate format='short'>
          </TD>
      </table>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 销售团队结果
    		</td>
    	</tr>
    </table>
    <Input type=hidden name=BranchType >
    <Input type=hidden name=BranchType2 >
    <input type=hidden name=AgentGroup value=''>  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
  	<Div  id= "divAGroupGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBranchGroupGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
