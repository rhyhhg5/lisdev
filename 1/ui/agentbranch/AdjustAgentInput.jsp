<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AdjustAgentInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<%
  String tTitleAgent="";

  if("1".equals(BranchType))
  {
    tTitleAgent = "销售人员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="AdjustAgentInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AdjustAgentInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./AdjustAgentSave.jsp" method=post name=fm target="fraSubmit">  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent);">
    <td class=titleImg>
      调入单位信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent" style= "display: ''">      
    <table  class= common>
       <TR  class= common> 
          <TD  class= title>
            销售单位代码
          </TD>          
          <TD  class= input>
            <Input class=common name=BranchCode >
          </TD>          
           <!--确定按钮 -->
          <TD  class= common>
            <Input type=button class= cssButton value="确认" onclick="BranchConfirm();">
          </TD>                               
       </TR> 
      </table>
      <table class=common>
       <TR  class= common>
          <TD  class= title>
            销售单位名称
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=BranchName >
          </TD>
        
        </TR>
       <TR  class= common>
          <TD  class= title>
            机构管理人员代码
          </TD>
          <TD  class= inqqput>
            <!--Input class=common name=BranchManager onchange="return changeManager();"-->
            <Input class='readonly'readonly name=BranchManager >
          </TD>
          <TD  class= title>
            机构管理人名称
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=ManagerName >
          </TD>
        </TR>
    </table>  
    
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent2);">
    <td class=titleImg>
      查询调动人员信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent2" style= "display: ''">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
           销售单位代码 
          </TD>
          <TD  class= input>
            <Input class=common name=AgentGroup >
          </TD>
          <TD  class= title>
           <%=tTitleAgent%>代码  
          </TD>
          <TD  class= input>
            <Input class=common name=AgentCode >
          </TD>
        </TR>
        <TR  class= common>         
           <TD class= title>
          <%=tTitleAgent%>职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="codeno" " 
           ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          ><Input name=AgentGradeName class="codename" readOnly elementtype=nacessary >  
        </TD>
        </TR>
    </table> 
    <table class=common>
      <TR  class= common>
          <TD  class= title>
           调动日期 
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=AdjustDate dateFormat='short' verify="调动日期|notnull&DATE">
          </TD>
          <TD class= common>
          <Input type=button class= cssButton name=queryb value="查询" onclick="queryAgent();">
          <input type=button class=cssButton value='重置' onclick="clearGrid();">
          <TD>
        </TR>
    </table>
    <table  class= common>
         <tr  class= common>                    
             <td text-align: left colSpan=1> 
		  <span id="spanAgentGrid">
		  </span> 
             </td>
         </tr>
    </table> 
    </div>   
    <br>
    <table  class= common>
	    <tr>
	      <td alain=right>
	      <INPUT VALUE="首页" TYPE=button class=cssButton onclick="turnPage.firstPage();"> 
	    
	      <INPUT VALUE="上一页" TYPE=button class=cssButton onclick="turnPage.previousPage();"> 
	    					
	      <INPUT VALUE="下一页" TYPE=button class=cssButton onclick="turnPage.nextPage();"> 
	     
	      <INPUT VALUE="尾页" TYPE=button class=cssButton onclick="turnPage.lastPage();"> 
	      </td>	
	    </tr>
	    <tr>
	    	<td>
	    	<input type=button class=cssButton name=saveb value='保存' onclick="return submitSave()">
	       	
	    	<input type=button class=cssButton value='重置' onclick="clearAll();">
	    </td>
	    </tr>
    </table>
   </div>
    <input type=hidden name=hideBranchManager value=''>
    <input type=hidden name=hideAgentCode value=''>  
    <input type=hidden name=BranchType2 value=''>
    <Input  type=hidden name=BranchType value='' >
    
    <input type=hidden name=hideBranchLevel value=''>
    <input type=hidden name=hideUpBranch value=''>
    <input type=hidden name=hideAgentGroup value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
