//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
          
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
                
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//提交，保存按钮对应操作
function DoInsert()
{

 if((fm.all('ManageCom').value=="")||(  fm.all('ManageCom').value==null))
 {
 	alert("管理机构不能为空！");
 	return false;
 }  
  if (verifyInput() == false)
  return false;	
  if(!chkMulLine()) return false;
  
  fm.fmtransact.value = "INSERT" ;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{

	var i;
	var selFlag = true;
	var iCount = 0;
	var manageCom=fm.all('ManageCom').value;
	var rowNum = ArchieveGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
			iCount++;
			if((ArchieveGrid.getRowColData(i,5) == null)||(ArchieveGrid.getRowColData(i,5) == ""))
			{
				alert("第"+(i+1)+"行2011营业部编码不能为空");
				ArchieveGrid.setFocus(i,1,ArchieveGrid);
				return false;
			}
			if((ArchieveGrid.getRowColData(i,6) == null)||(ArchieveGrid.getRowColData(i,6) == ""))
			{
				alert("第"+(i+1)+"行2011营业部编码名称不能为空");
				ArchieveGrid.setFocus(i,1,ArchieveGrid);
				return false;
			}
            if((ArchieveGrid.getRowColData(i,7) == null)||(ArchieveGrid.getRowColData(i,7) == ""))
			{
				alert("第"+(i+1)+"行新成本中心编码不能为空");
				ArchieveGrid.setFocus(i,1,ArchieveGrid);
				return false;
			}
			if(!checkSap((ArchieveGrid.getRowColData(i,7)),fm.ManageCom.value))
 	        {
 		        return false;
 	        }

	}
			if(iCount == 0)
			{
				alert("无需进行保存!");
				return false;
			}			
			
			return true;
	}
		
//对成本中心编码进行校验
function checkSap(CostCenter,tManageCom)
{
 var erroInfo="成本中心代码有误，请根据发文《人保健康计财[2008] 212号》申请成本中心代码！";
 var tt=CostCenter.substr(4,2);
 if(tt!="90"&&tt!="91"&&tt!="92")
 {
 	alert(erroInfo);
 	return false;
 } 

 var tSQL="select codealias from licodetrans  where codetype='ManageCom' and code='"+tManageCom+"'";
 var strQueryResult1 = easyQueryVer3(tSQL, 1, 1, 1); 	  
 var arr1 = decodeEasyQueryResult(strQueryResult1);
 if(!arr1)
 {
 	alert("SAP没有定义公司编码，请联系SAP定义公司编码！"); 
 	return false;
 }else{
 	var ss=arr1[0][0]
 	if(ss!=CostCenter.substr(0,4))
 	{
 	alert(erroInfo);
 	return false;
	 }
 }
 return 1;
}		
		
		
function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "comcode"){
  var strSQL = "select c.agentgroup,c.branchattr,c.Name,'迁移',double((trim(managecom)||'00'))+row_number() over()   "
  +",(select trim(name) from ldcom where comcode=c.managecom)||'营业'||rtrim(char(row_number() over()))||'部'  "
  +",costcenter "
  	+"from LABranchgroup c where  c.managecom= '"+fm.ManageCom.value + "' "
  	+"  and c.branchtype='3'  and c.branchtype2='01'    and c.branchlevel='41' order by branchattr  with ur";
  	
  var arrResult = new Array();
  arrResult = easyExecSql(strSQL);
  
  if (!arrResult) 
    {
    alert("没有符合条件的数据！");
    return false;
    }
    displayMultiline(arrResult,ArchieveGrid);
    }
}