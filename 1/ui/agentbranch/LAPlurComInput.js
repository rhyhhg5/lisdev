//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    } 
  }
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  if (mOperate=='INSERT||MAIN')
  { 	
    //校验代理机构代码
	var strSQL = "";
	strSQL = "select AgentCom from LACom where 1=1 "
	        + getWherePart('AgentCom');	 
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
	if (strQueryResult) 
	  {
	    alert("该代理机构编码已存在！");
	    fm.all("AgentCom").value = '';
	    return false;
	  }
  }   
//  showSubmitFrame(1);
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LAPlurComInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 

//提交前的校验、计算  
function beforeSubmit()
{
	if (!verifyInput()) 
    return false;
  //添加操作
  if(fm.all('AgentCom').value==null || fm.all('AgentCom').value=="")
  {
    alert("请输入代理机构编码！");
    fm.all('AgentCom').focus();
    return false;
  }
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,100,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if ((fm.all("AgentCom").value==null)||(fm.all("AgentCom").value==''))
  {
    alert("请先确定代理机构！");
    fm.all('AgentCom').focus();
  }
  else
  {
    if (!queryAgentCom())
      return;
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./LAPlurComQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCom").value==null)||(fm.all("AgentCom").value==''))
  {
    alert("请先确定代理机构！");
    fm.all('AgentCom').focus();
  }
  else
  {
    if (!queryAgentCom())
      return false;
    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";  
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function getManagerName(input)
{
	
	if(fm.all('AgentCode').value==""||fm.all('AgentCode').value==null)
	{		
		fm.all('AgentName').value=="";
		}
	else
		{
  	var sql = "select Name from LAAgent where 1=1"
          + getWherePart('AgentCode','AgentCode');
  	var strResult = easyQueryVer3(sql, 1, 1, 1);
  	if (strResult)
  	{
  		var arrDataSet = decodeEasyQueryResult(strResult);
  		var tArr_Record = new Array();
  		tArr_Record = chooseArray(arrDataSet,[0]);
  		input.value = tArr_Record[0][0];
 	 }
  	else
 	 {
  		input.value="";
  		alert("客户经理代码输入有误！");
  	}
	}
}


//验证代理人编码的合理性
function checkValid()
{ 
  if (getWherePart('AgentCode')=='')
    return false;
  var strSQL = "";
  strSQL = "select * from LAAgent where 1=1 "
	   + getWherePart('AgentCode');
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此专管员！");
    fm.all('AgentCode').value = "";
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = chooseArray(arrDataSet,[0,1,43,44,61]);
  if(tArr[0][2]!="30"){
    alert("该人非专管员！");
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    return;
  }
  if(tArr[0][4]=="91"){
    alert("此专管员已被解聘！");
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    return;
  }
  //<rem>######//
  //fm.all('AgentGroup').value = tArr[0][1];
  //</rem>######//
  fm.all('AgentState').value = tArr[0][4];
  fm.all('DevGrade').value   = tArr[0][3];

  //<addcode>############################################################//
  old_AgentGroup=tArr[0][1];
  fm.all('HiddenAgentGroup').value = tArr[0][1];
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"'"
     var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = chooseArray(arrDataSet_AgentGroup,[0,1,2]);
  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  //</addcode>############################################################//
}

function queryAgentCom()
{
  //var turnPage = new turnPageClass();	
  
	// 书写SQL语句
        var strSQL = "";
	strSQL = "select AgentCom from LACom where 1=1 "
	        + getWherePart('AgentCom');	 
	//turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
  if (!strQueryResult) {
    alert("不存在所要操作的代理机构！");
    fm.all("AgentCom").value = '';
    return false;
    }
    return true;
}

//用来显示返回的选项
function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  if( arrQueryResult != null )
  {
  	initInpBox();
    arrResult = arrQueryResult;
    fm.all('AgentCom').value = arrResult[0][0];
    fm.all('Name').value = arrResult[0][5];  
    fm.all('BusinessType').value = arrResult[0][17];
    fm.all('BusinessCode').value = arrResult[0][40]; 
    fm.all('BusiLicenseCode').value=arrResult[0][28];
    fm.all('AppAgentCom').value=arrResult[0][37];  
    fm.all('LicenseNo').value = arrResult[0][35];
    fm.all('GrpNature').value = arrResult[0][18];
    fm.all('LicenseStartDate').value = arrResult[0][41];
    fm.all('LicenseEndDate').value = arrResult[0][42];
    fm.all('LinkMan').value = arrResult[0][12];
    fm.all('Corporation').value=arrResult[0][14]; 
    fm.all('Address').value = arrResult[0][6];                                                                                   
    fm.all('Phone').value = arrResult[0][8];                                                                                         
    fm.all('RegionalismCode').value = arrResult[0][36];
    fm.all('EMail').value = arrResult[0][10];  
    fm.all('ChiefBusiness').value = arrResult[0][31];
    fm.all('Noti').value = arrResult[0][39];     
    fm.all('ManageCom').value = arrResult[0][1];                                              
    fm.all('UpAgentCom').value = arrResult[0][4];                                              
    fm.all('SellFlag').value = arrResult[0][20];
    fm.all('ACType').value = arrResult[0][19];
    getRelationInfo();
    if (fm.all('UpAgentCom').value!=null&&fm.all('UpAgentCom').value!='')
    {
   	 getComName(fm.all('UpComName'));
  	}                                                                                                                                                                                                                                               	
  } 
}

function WriteContract()
{
//	alert("WriteContract!");
	if(fm.all('ContNo').value !="")
	{
		showInfo=window.open("./LAPlurComWriteContract.html");
	}
	else
	{
		alert("请先保存再录入合同细则！");
	}
}

/*function CreatContractNo()
{
//	alert("CreatContractNo!");
	var str_sql="";
	str_sql="select max(Contno) from LACont";
	var strQueryResult = easyQueryVer3(str_sql, 1, 1, 1);
	var arrDataSet = decodeEasyQueryResult(strQueryResult);
	var tArr = new Array();
	tArr = chooseArray(arrDataSet,[0,1,2]);
	fm.all('ContNo').value=tArr[0][0];
	alert(tArr[0][0]);
}
*/
function afterCodeSelect( cCodeName, Field )
{
	try{
	if (cCodeName=="yesno")
	{
		if (Field.value=="Y")
		{ 
		   write.style.display=""; 
		}
		else
		{
		   write.style.display="none";
		}
	}
	}catch(ex){}
} 

function getComName(input)
{
  var sql = "select Name from LACom where 1=1"
          + getWherePart('AgentCom','UpAgentCom');
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	input.value = tArr_Record[0][0];
  }
  else
  {
  	input.value="";
  	alert("上级代理机构代码输入有误或没有此上级代理机构！");
  }
}

function getManagerName(input)
{
  var sql = "select Name from LAAgent where 1=1"
          + getWherePart('AgentCode');
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	input.value = tArr_Record[0][0];
  }
  else
  {
  	input.value="";
  	alert("代理人代码输入有误！");
  }
}

function getRelationInfo()
{
  var sql = "select a.branchattr from labranchgroup a,LAComToAgent b where 1=1 and b.AgentCom='"+fm.all('AgentCom').value+"' and b.RelaType='0' and a.agentgroup = b.agentgroup";
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	fm.all('AgentGroup').value = tArr_Record[0][0];
  }
  else
  {
  	fm.all('AgentGroup').value="";
  }
  var sql = "select AgentCode from LAComToAgent where 1=1 and AgentCom='"+fm.all('AgentCom').value+"' and RelaType='1'";
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	fm.all('AgentCode').value = tArr_Record[0][0];
  	getManagerName(fm.all('AgentName'));
  }
  else
  {
  	fm.all('AgentCode').value="";
  	fm.all('AgentName').value="";
  }
}


/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	//alert(cCodeName);
	try	{
		if( cCodeName == "AgentCode" )	
		{
			checkvalid();//loadFlag在页面出始化的时候声明
		}
	}
	catch( ex ) {
	}
}


function checkvalid()
{
  var strSQL = "";
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select * from LAAgent where 1=1 "
	     + getWherePart('AgentCode')+" and ((AgentState  not like '03') or (AgentState is null))";
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value  = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此代理人！");
    fm.all('AgentCode').value="";
    fm.all('AgentName').value  = "";
    return;
  }
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//
  fm.all('AgentName').value = tArr[0][5];
  //</rem>######//
}