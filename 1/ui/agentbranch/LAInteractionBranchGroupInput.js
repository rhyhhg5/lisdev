//该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (!((fm.all("EndDate").value==null)||(trim(fm.all("EndDate").value)==''))&&(trim(fm.all("EndFlag").value)=='N'))
    {
     alert("停业属性为N，不能置停业日期！"
    
    );
    return false;}

  //下面增加相应的代码

  if ((fm.all("BranchAttr").value==null)||(trim(fm.all("BranchAttr").value)==''))
    alert("请确定要修改的销售团队！");
  else
  {
    if (!queryBranchCode())
      return false;

    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }

}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
    var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    showInfo=window.open("./LAInteractionBranchGroupQuery.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

//提交，保存按钮对应操作
function submitForm()
{
	if (mOperate!="UPDATE||MAIN")
	{
	  mOperate="INSERT||MAIN";
	}	       
	
	//如果是新增操作，则需要将主管编码和主管姓名清空，否则会传入后台，如果是更新操作，则不需要如此处理
	if (mOperate=="INSERT||MAIN"){
		fm.all('BranchManager').value = "";
		fm.all('BranchManagerName').value = "";
	}
	
    // 控制数据
	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	
	// 简单数据校验
	if (!beforeSubmit())
		return false;
		
	// 校验停业属性
	if (!changeGroup())
		return false;		
    
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
  }
	mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LASocialBranchGroupInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
	if( verifyInput() == false ) return false;
	var cManageCom = fm.all('ManageCom').value;
	var cBranchAttr = fm.all('BranchAttr').value;

	// 团队编码前8位与机构编码一致
	if (cManageCom.substring(0,cManageCom.length) != cBranchAttr.substring(0,cManageCom.length))
	{
	  alert("销售团队代码与管理机构不匹配！");
	  fm.all('BranchAttr').focus();
      return false;
	}	 	 
	
 	//对成本中心编码进行校验
 	if(fm.ManageCom.value=="")
 	{
 		alert("所属管理机构不能为空！");
 		return false;
 	}
 	
 	//校验成本中心
 	if(!checkSap(fm.CostCenter.value,fm.ManageCom.value))
 	{
 		return false;
 	}
	return true;
}

//对成本中心编码进行校验
function checkSap(CostCenter,tManageCom)
{
  var erroInfo="成本中心代码有误，请根据发文《人保健康计财[2008] 212号》申请成本中心代码！";
  var tt=CostCenter.substr(4,1);
  if(tt!="9")
  {
 	alert(erroInfo);
 	return false;
  } 

  var tSQL="select codealias from licodetrans  where codetype='ManageCom' and code='"+tManageCom+"'";
  var strQueryResult1 = easyQueryVer3(tSQL, 1, 1, 1); 	  
  var arr1 = decodeEasyQueryResult(strQueryResult1);
  if(!arr1)
  {
 	alert("SAP没有定义公司编码，请联系SAP定义公司编码！"); 
 	return false;
  }else{
	var ss=arr1[0][0]
 	if(ss!=CostCenter.substr(0,4))
 	{
 	 alert(erroInfo);
 	 return false;
	 }
 }
 return 1;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,500,82,*";
  }
  else
  {
 	parent.fraMain.rows = "0,0,0,82,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function afterQuery(arrQueryResult)
{
 var arrResult = new Array();
 if( arrQueryResult != null )
 {
 	arrResult = arrQueryResult;
 	document.all.BranchAttr.readOnly="true";
 	fm.all('AgentGroup').value = arrResult[0][0];
 	fm.all('BranchName').value = arrResult[0][1];
 	fm.all('ManageCom').value = arrResult[0][2];
 	fm.all('ManageComName').value= arrResult[0][3] ;
 	fm.all('BranchAttr').value = arrResult[0][4];
 	fm.all('BranchType').value = arrResult[0][5];
 	fm.all('BranchLevel').value = arrResult[0][6];

	fm.all('BranchManager').value = arrResult[0][7];
	fm.all('BranchManagerName').value = arrResult[0][8];	 
	fm.all('BranchAddress').value = arrResult[0][9];
	fm.all('BranchZipcode').value = arrResult[0][10];
	
    fm.all('BranchPhone').value = arrResult[0][11];
	fm.all('FoundDate').value = arrResult[0][12];
	fm.all('EndFlag').value = arrResult[0][13];
	fm.all('EndFlagName').value = arrResult[0][14];	

	fm.all('EndDate').value = arrResult[0][15];
	fm.all('ComStyle').value = arrResult[0][16];
	fm.all('ComStyleName').value = arrResult[0][17];
	fm.all('Operator').value = arrResult[0][18];
	fm.all('CostCenter').value = arrResult[0][20];
	fm.all('State').value = arrResult[0][21];
	fm.all('StateName').value = arrResult[0][22];

 }
}

// 查询直销团队信息
function queryBranchCode()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select AgentGroup from LABranchGroup where 1=1   "
	  	 + getWherePart('BranchAttr')
	     + getWherePart('BranchType')
	     + getWherePart('BranchType2');
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult) 
  {
    alert("系统未找到销售团队代码为"+fm.all('BranchAttr').value+"的销售团队！");
    fm.all("BranchAttr").value = '';
    fm.all('AgentGroup').value = '';
    return false;
  }

  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentGroup').value = arr[0][0];
  return true;
}

// 团队停业属性校验
function changeGroup()
{
	if(fm.all('EndFlag').value == null || fm.all('EndFlag').value == "")
	{
	   alert("停业属性为空，请置位！");
	   return false;
	}
	if(fm.all('EndFlag').value == 'Y' && (fm.all('EndDate').value == null || fm.all('EndDate').value == ""))
    {
  	   alert("停业属性为Y，请置停业日期！");
       return false;

    }
    if(fm.all('EndFlag').value == 'Y')
    {
      
    	var tFoundDate = fm.all('FoundDate').value;
    	var tEndDate = fm.all('EndDate').value;
    	var arr1 = tFoundDate.split("-");
    	var arr2 = tEndDate.split("-");
    	
    	var tFoundDate1 = new Date(arr1[0],arr1[1],arr1[2]);	
    	var tEndDate1 = new Date(arr2[0],arr2[1],arr2[2]);
    	
    	
    	if(tEndDate1.getYear()<tFoundDate1.getYear()
		||(tEndDate1.getYear()==tFoundDate1.getYear() && tEndDate1.getMonth()<(tFoundDate1.getMonth()))
		||(tEndDate1.getYear()==tFoundDate1.getYear() && tEndDate1.getMonth()==(tFoundDate1.getMonth()) &&　tEndDate1.getDate()<tFoundDate1.getDate()))
		{
		  alert("团队停业时间不应早于团队建立日期！操作失败。");
		  return false;
	    }
	    return true;
    	
    	
    }
	if(fm.all('EndFlag').value == 'N' && fm.all('EndDate').value != "")
    {  
  	  alert("停业属性为N，不能置停业日期！");
  	  fm.all('EndDate').value = "";
      return false;
    }

  return true;
}
