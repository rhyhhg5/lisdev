<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LABranchGroupInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql='1 and branchtype=#'+<%=BranchType%>+'# and branchtype2=#'+'<%=BranchType2%>'+'# and branchlevelcode<#03# ';
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LABranchGroupQuChuInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LABranchGroupQuChuInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LABranchGroupQuChuSave.jsp" method=post name=fm target="fraSubmit" >
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      销售单位
      </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            销售单位代码
          </TD>
          <TD  class= input>
            <Input class=common name=BranchAttr id =BranchAttr  verify = "销售单位代码|notnull&len>9" elementtype=nacessary onchange="return getUpBranch();">
          </TD>
          <TD  class= title>
            销售单位名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name verify = "销售单位名称|notnull" elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
          readonly ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD> 
          <TD  class= title>
            级别
          </TD>
          <TD  class= input>
            <Input class= 'codeno' name=BranchLevel verify="展业机构级别|notnull&code:BranchLevel" 
                                                  ondblclick="return showCodeList('branchlevel',[this,BranchLevelName],[0,1],null,msql,1);" 
                                                  onkeyup="return showCodeListKey('branchlevel',[this,BranchLevelName],[0,1],null,msql,1);"
                                                  readOnly 
            ><Input class=codename name=BranchLevelName readOnly elementtype=nacessary>
          </TD>
          <!--TD  class= title>
            类型
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=BranchType >
          </TD-->
        </TR>
        <TR  class= common>
          
          <TD  class= title>
            管理人员代码
          </TD>
          <TD  class= input>
          	<!--张飞飞 2014-11-13 集团统一工号-->
          	<Input class="readonly"readonly name=GroupAgentCode>
            <!-- <Input class="readonly"readonly name=BranchManager > -->
          </TD>
          <TD  class= title>
            管理人员名称
          </TD>
          <TD  class= input>
            <Input class="readonly"readonly name=BranchManagerName >
          </TD>
        </TR>
        
        <TR  class= common>
          <!--TD  class= title>
            展业机构地址编码
          </TD>
          <TD  class= input>
            <Input class= 'code' name=BranchAddressCode ondblclick="return showCodeList('station',[this]);" 
                                                        onkeyup="return showCodeListKey('station',[this]);">
          </TD-->
          <TD  class= title>
            上级机构代码
          </TD>
          <TD  class= input>
            <Input name=UpBranchA class="readonly"readonly>
          </TD>
          <TD  class= title>
            上级机构名称
          </TD>
          <TD  class= input>
            <Input name=UpBranchAName class="readonly"readonly>
          </TD>
          <!--TD  class= input colspan=2>
          </TD-->
          
        </TR>
        <TR  class= common>
          <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddress >
          </TD>
         
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhone >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= common name=BranchFax >
          </TD>

          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcode verify="展业机构邮编|len<=6" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' name=FoundDate verify="成立标志日期|notnull&DATE" format='short' elementtype=nacessary>
          </TD>
          <TD  class= title>
            是否有独立的营销职场
          </TD>
          <TD  class= input>
            <Input class= 'codeno' name=FieldFlag verify="是否有独立的营销职场|code:yesno" 
                                                ondblclick="return showCodeList('yesno',[this,FieldFlagName],[0,1]);" 
                                                onkeyup="return showCodeListKey('yesno',[this,FieldFlagName],[0,1]);" 
            ><Input class=codename name=FieldFlagName readOnly >
        </TR>
        <TR  class= common>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='codeno' name=EndFlag verify="停业|notnull&code:yesno" 
                                             ondblclick="return showCodeList('yesno',[this,EndFlagName],[0,1]);" 
                                             onkeyup="return showCodeListKey('yesno',[this,EndFlagName],[0,1]);"
            ><Input class=codename name=EndFlagName readOnly elementtype=nacessary>
          </TD>
          <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate format='short' onfocusout="return changeGroup();">
          </TD>
        </TR>
        <TR  class= common>
          <!--TD  class= title>
            是否单证交接
          </TD>
           <TD  class= input>
            <Input class= 'code' name=CertifyFlag MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" 
                                                             onkeyup="return showCodeListKey('yesno',[this]);"-->
          <TD  class= title>
            是否区直辖处
          </TD>
          <TD  class= input id='titleI' style="display:''">
            <Input class='codeno' name=UpBranchAttr verify="与上级的隶属关系|notnull|code:upbranchattr" 
                                                  ondblclick="return showCodeList('upbranchattr',[this,UpBranchAttrName],[0,1]);" 
                                                  onkeyup="return showCodeListKey('upbranchattr',[this,UpBranchAttrName],[0,1]);"><Input class=codename name=UpBranchAttrName elementtype=nacessary>
          </TD>
          <TD  class= input id='titleG' style="display:'none'">
            <Input class=readOnly readOnly>
          </TD>
       	 <TD  class= title>
            托管状态
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=TrusteeShip >
          </TD>
       </TR>      
               <TR  class= common>
       	 <TD  class= title>
            团队申报标志
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ApplyGbFlag >
          </TD>
       	 <TD  class= title>
            申报标志起期
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ApplyGbStartDate >
          </TD>    
          </TR>
       <TR  class= common>
              	 <TD  class= title>
            团队建设类型
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GbuildFlag >
          </TD>
       	 <TD  class= title>
            团队建设起期
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GbuildStartDate >
          </TD>    
          </TR>
       <TR  class= common>
       	 <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Operator >
          </TD>
          <!--TD  class= title>
            标志
          </TD>
          <TD  class= input>
            <Input class=common name=State MaxLength=10 >
          </TD>
          <TD class= title>
            最近操作日
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ModifyDate >
          </TD-->
       </TR>
      </table>
    </Div>
    <input type=hidden name=ModifyDate value=''>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=AgentGroup value='' >  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
    <input type=hidden name=UpBranch value='' >  <!--上级机构代码，存储隐式机构代码 -->
    <input type=hidden name=CostCenter value='' >  <!--上级机构代码，存储隐式机构代码 -->
    <!--张飞飞 2014-11-13 集团统一工号-->
    <Input type=hidden name=BranchManager value=''> <!--集团统一工号的老工号 -->
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
