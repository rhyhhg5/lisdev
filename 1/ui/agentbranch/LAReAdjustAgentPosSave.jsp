<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AdjustAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
//输入参数
	LAAssessAccessorySchema mLAAssessAccessorySchema = new LAAssessAccessorySchema();
	LAReAssessInputUI mLAReAssessInputUI = new LAReAssessInputUI();

//输出参数
	CErrors tError = null;
	String tOperate="";
	String tRela  = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String tBranchAttr = request.getParameter("NewAgentGroup");
	String tNewAgentGrade = request.getParameter("NewAgentGrade");
	String toldAgentGrade = request.getParameter("OldAgentGrade");

	//add lyc 统一工号 2014-11-25
	ExeSQL cExe = new ExeSQL();
	String cSQL = "select agentcode from laagent where groupagentcode = '"+request.getParameter("AgentCode")+"'";
	SSRS cSSRS = cExe.execSQL(cSQL); 
	  System.out.println("lyc_fighting: "+cSSRS.MaxRow);
	mLAAssessAccessorySchema.setAgentCode(cSSRS.GetText(1,1));
	mLAAssessAccessorySchema.setAgentGrade(request.getParameter("OldAgentGrade"));//存的是原来的职级
	mLAAssessAccessorySchema.setAgentGrade1(request.getParameter("NewAgentGrade"));//存的是将要调整至的职级
	mLAAssessAccessorySchema.setAgentGroup(request.getParameter("AgentGroup"));
//  mLAAssessAccessorySchema.setOperator(request.getParameter("Operator"));
	System.out.println("AgentGroup:"+request.getParameter("AgentGroup"));
//	mLAAssessAccessorySchema.setAgentGroupNew(request.getParameter("NewAgentGroup"));
	mLAAssessAccessorySchema.setIndexCalNo(request.getParameter("IndexCalNo"));
	mLAAssessAccessorySchema.setBranchType(request.getParameter("BranchType"));
	mLAAssessAccessorySchema.setBranchType2(request.getParameter("BranchType2"));
	mLAAssessAccessorySchema.setMakeTime(request.getParameter("MakeTime"));
	mLAAssessAccessorySchema.setManageCom(request.getParameter("ManageCom"));
	mLAAssessAccessorySchema.setModifyFlag(request.getParameter("TransferType"));
	mLAAssessAccessorySchema.setAgentSeries(request.getParameter("OldSeries"));//存的是laaegentgrade 中gradeproperty2字段
	mLAAssessAccessorySchema.setAgentSeries1(request.getParameter("NewSeries"));//存的是laaegentgrade 中gradeproperty2字段
	mLAAssessAccessorySchema.setStandAssessFlag("0");//0  手工考核
	mLAAssessAccessorySchema.setAssessType("00"); //默认为职级考核维度
	if(tNewAgentGrade.substring(0,1).equals("L")){
		tBranchAttr="1";
	}
	//由于NewSeries 没有赋上值 导致处理代码复杂化
	if(toldAgentGrade.substring(0,1).equals("Q")&&tNewAgentGrade.substring(0,1).equals("Q")){
		tBranchAttr="1";
	}
	if(toldAgentGrade.substring(0,1).equals("R")&&tNewAgentGrade.substring(0,1).equals("R")){
		tBranchAttr="1";
	}
//    System.out.println("调整机构："+tBranchAttr);
    System.out.println("调整机构："+tBranchAttr);
// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	tVData.add(tG);
    tVData.add(tBranchAttr);
	tVData.addElement(mLAAssessAccessorySchema);
	try{
		mLAReAssessInputUI.submitData(tVData,"INSERT||MAIN");
	}
	catch(Exception ex){
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = mLAReAssessInputUI.mErrors;
		if (!tError.needDealError())
		{
	Content = " 保存成功! ";
	FlagStr = "Succ";
		}
		else
		{
	Content = " 保存失败，原因是:" + tError.getFirstError();
	FlagStr = "Fail";
		}
	}

//添加各种预处理
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

