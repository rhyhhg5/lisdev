<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAComInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./LAComInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>  
  <%@include file="./LAComInit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAComSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACom1);"></td>
        <td class=titleImg>银行机构基本信息</td> 
      </tr>
          </table>
  <Div  id= "divLACom1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 银行机构编码 </td>
        <td  class= input> <input class='readonly' readonly  name=AgentCom    > </td>
        <td  class= title> 银行机构名称 </td>
        <td  class= input> <input class=common  name=Name verify="银行机构名称|notnull&len<21" elementtype=nacessary> </td>
      </tr>
      
      <TR class=common>
       <td  class= title> 级别 </td>
         <td  class= input>
         <input name=BankType2 class=codeno verify="级别|code:BankType2&notnull"  
		             ondblClick="showCodeList('BankType2',[this,BankType2Name],[0,1]);fm.all('hiddenBankType2').value=fm.all('BankType2').value" 
		             onkeyup="showCodeListKey('BankType2',[this,BankType2Name],[0,1])" 
		             onpropertychange="initUpAgentCom()"
		     ><Input class=codename name=BankType2Name readOnly elementtype=nacessary>        				     
		     <input type=hidden name=hiddenBankType2 >
		     </td> 
	       <td  class= title> 销售资格 </td>
         <td  class= input> <input  name=SellFlag class= 'codeno' verify="销售资格|notnull&code:YesNo"  
		           CodeData = "0|^Y|有|^N|无" 
             ondblclick="return showCodeListEx('SellFlag',[this,SellFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('SellFlag',[this,SellFlagName],[0,1]);"
             ><Input class=codename name=SellFlagName readOnly elementtype=nacessary>     
	      </td>	   
      </TR>	 
        
      <tr  class= common>
        <td  class= title> 上级机构编码 </td>
        <td  class= input> <input class=code name=UpAgentCom verify="上级机构编码|notnull"
         ondblclick="return getUpAgentCom(this,UpAgentComName);"
             onkeyup="return getUpAgentCom(this,UpAgentComName);"  elementtype=nacessary > </td>
        <td  class= title> 上级机构名称 </td>
        <td  class= input> <input class='readonly' readonly  name=UpAgentComName > </td>    	
      </tr>
      
      <TR  class= common>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='codeno' name=EndFlag  CodeData = "0|^Y|是|^N|否" 
             ondblclick="return showCodeListEx('EndFlag',[this,EndFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('EndFlag',[this,EndFlagName],[0,1]);"
             ><Input class=codename name=EndFlagName readOnly>
          </TD>
	        <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate verify="停业日期|Date" format='short' >
          </TD>
        </TR>       
  
      <tr  class= common> 
        <td  class= title> 地址 </td>
        <td  class= input> <input  class= common name=Address verify="地址|len<27"> </td>
	      <td  class= title> 电话 </td>
        <td  class= input> <input  class=common name=Phone> </td>
      </tr>   
      <tr  class= common>
        <td  class= title>中介机构类型</td>
        <td  class= input><Input class="codeno" name=ACType verify="中介机构类型|notnull&code:actype301" 
             ondblClick="showCodeList('actype301',[this,ACTypeName],[0,1]);"
             onkeyup="showCodeListKey('actype301',[this,ACTypeName],[0,1]);"
            ><Input class=codename name=ACTypeName readOnly elementtype=nacessary>
         </td> 
        <td  class= title> 保险兼业代理资格证号 </td>
        <td  class= input> <input  class= common name=BusiLicenseCode  elementtype=nacessary> </td>
        <!-- verify="保险兼业代理资格证号|NOTNULL" elementtype=nacessary -->
      </tr>
      
      <tr  class= common>
      <td  class= title> 获得许可证日期 </td>
         <td  class= input> 
           <input class=coolDatePicker name=Licensestart elementtype=nacessary verify="业务许可证有效起期|NOTNULL&Date" format='short'>
         </td>
      <td  class= title> 许可证到期日期 </td>
      <td  class= input> 
       <input class=coolDatePicker name=Licenseend elementtype=nacessary verify="业务许可证有效止期|NOTNULL&Date" format='short'>
      </td>
      </tr>
      <tr>
      <td  class= title> 是否县域网点 </td>
      <td  class= input> <input  name=IsDotFlag class= 'codeno' verify="是否县域网点|notnull"  
		           CodeData = "0|^Y|是|^N|否" 
             ondblclick="return showCodeListEx('IsDotFlag',[this,IsDotFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('IsDotFlag',[this,IsDotFlagName],[0,1]);"
             ><Input class=codename name=IsDotFlagName readOnly >     
	      </td>	 
	  <td  class= title> 中介组织机构代码 </td>
         <td  class= input> 
           <input class=common name=AgentOrganCode>
         </td>
      </tr>
      <tr>
        <td  class= title> 联系人 </td>
        <td  class= input> <input  class= common name=LinkMan verify="联系人|len<6"> </td>
      </tr>
   </table>
   
    <Div  id= "Type1" style= "display: none"> 
      <table  class= common>
        <tr class = common >
        <td class=title> SAP供应商编号 </td>
	      <td class= input ><input class=common  name=BankCode  verify="SAP供应商编号|len=10" elementtype=nacessary>  </td>    
	      <td class=title></td>
	      <td class=input></td>    
      </tr>
    </table> 
    </Div>
   
   <table class= common>
     <tr class= common>
       <td class=title>
         备注
       </td>
     </tr>  
     <tr class=common>
     <td class=title>
       <textarea  cols="100" rows="3" class="common"  name=Noti>
       </textarea>
      </td>
     </tr>
   </table>
</Div>
<table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLASpecCom2);"></td>
        <td class=titleImg>账户信息</td> 
      </tr>
    </table>
  <Div  id= "divLASpecCom2" style= "display: ''"> 
   <table  class= common>
      <tr  class= common>
        <td  class= title> 
        帐号 
        </td>
        <td  class= input> 
        <input class=common name=BankAccNo  verify="机构帐户|num"> 
        </td>
	      <td  class= title> 
	      帐号确认 
	      </td>
        <td  class= input> 
        <input class=common name=BankAccNoInsure verify="机构帐户确认|num" > 
        </td> 
      </tr>
       <tr  class= common>
        <td  class= title> 
         帐户名 
        </td>
        <td  class= input> 
        <input class=common name=BankAccName  > 
        </td>
	      <td  class= title> 
	      开户行 
	      </td>
        <td  class= input> 
        <input class=common name=BankAccOpen  > 
        </td> 
      </tr>
      </table>
     </Div>  
<table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACom3);"></td>
        <td class=titleImg>对应协议信息</td> 
      </tr>
    </table>
  <Div  id= "divLACom3" style= "display: ''"> 
    <table  class= common>
    	<TR  class= common>
    	<TD  class= title>
      	签约时间
    	</TD>
    	<TD  class= input>
       	<Input name=SignDate class="coolDatePicker" dateFormat="short" verify="签约时间|NotNull&Date" elementtype=nacessary>
    	</TD>
    	<TD  class= title>
     		协议到期日
   	 	</TD>
    	<TD  class= input>
      		<Input name=ProEndDate class="coolDatePicker" dateFormat="short" verify="协议止期|NotNull&Date" elementtype=nacessary>
    	</TD>
 	 </TR>
 	 <TR  class= common>
          <TD  class= title>
            协议向下覆盖
          </TD>
          <TD  class= input>
            <Input class='codeno' name=CoverFlag value="N" CodeData = "0|^Y|是|^N|否" 
             ondblclick="return showCodeListEx('EndFlag',[this,CoverFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('EndFlag',[this,CoverFlagName],[0,1]);"
             ><Input class=codename name=CoverFlagName value="否" readOnly elementtype=nacessary>
          </TD>
      </TR>
    </table>
  </div>

    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACom2);"></td>
        <td class=titleImg>保险公司对应信息</td> 
      </tr>
    </table>
  <Div  id= "divLACom2" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 管理机构 </td>
        <td  class= input><Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
         </td> 
      </tr>
      <tr  class= common>
	      <td  class= title> 对应营业部 </td>
        <td  class= input> <input class='code' name=ChangeCom  
        	   ondblclick="return getChangeComName(this,ChangeComName);"
             onkeyup="return getChangeComName(this,ChangeComName);" elementtype=nacessary></td>
	      <td  class= title> 对应营业部名称 </td>
	      <td  class= input> <input class='common' readonly name=ChangeComName> </td>
      </tr>
      <tr class = common >
        <td  class= title> 负责员工代码 </td>
        <td  class= input> <input class='code' name=SiteManagerCode  
             ondblclick="return getSiteManager(this,SiteManager);"
             onkeyup="return getSiteManager(this,SiteManager);" elementtype=nacessary></td>
        <td  class= title> 负责员工姓名 </td>
        <td  class= input> <input class='common' readonly name=SiteManager > </td>
      </tr>
      <tr class = common >
        <td class=title> 操作员代码 </td>
	      <td class=input ><input class='readonly' readonly name=Operator> </td>    
   
      </tr>
   </table>
    <tr><td><p> <font color="#ff0000">注：在录入、电话、地址等信息时，请不要输入Tab、空格或是其他特殊字符，这些特殊字符会导致系统查询该机构时失败。</font></p></td></tr>
    <tr><td><p> <font color="#ff0000">注：在选择级别为"01-分行"时,可以不用输入许可证号起止期</font></p></td></tr>
    <tr><td><p> <font color="#ff0000">注：在选择山东、支行层级时，可以不录入供应商编码！除山东外其他机构、分行层级也可以不录入供应商编码</font></p></td></tr>
    <tr><td><p> <font color="#ff0000">注：网点层级为分行时可不必录入“保险兼业代理资格证号”</font></p></td></tr>
    <tr><td><p> <font color="#ff0000">注：分理处和网点层级需录入"是否县域网点"字段</font></p></td></tr>
    <tr><td><p> <font color="#ff0000">注：在选择江苏机构时，则中介组织机构代码、账号、账号确认、账号名、开户行不能为空</font></p></td></tr>
  </Div>
  
  <input type=hidden name=hideSiteManagerCode value=''>
  <input type=hidden name=hideOperate value=''>
  <input type=hidden name=HiddenAgentGroup value=''>
  <input type=hidden name=HiddenAgentGroup2 value=''>
  <input type=hidden name=HiddenAgentGroup3 value=''>
  <input type=hidden name=BranchType value=''>
  <input type=hidden name=BranchType2 value=''>
  <input type=hidden name=Bank value=''>
  <input type=hidden name=UpAgentCom1 value=''>
  <input type=hidden name=BankType3 value=''>
  <input type=hidden name=EndFlag1 value=''>
  <input type=hidden name=Mark value=''>	
  <input type=hidden name=ProtocolNo value=''>
  <input type=hidden name=HiddenManageCom value=''>
  <input type=hidden name=bankcode1 value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
