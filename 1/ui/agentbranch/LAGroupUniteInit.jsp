<%
//程序名称：LAGroupUniteInit.jsp
//程序功能：
//创建日期：2005-7-18 10:29
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{
  initInpBox1();
  initInpBox2();
  initInpBox3();
  
  try
  {
    fm.all('BranchType').value='<%=BranchType%>';
    fm.all('BranchType2').value='<%=BranchType2%>';
  }
  catch(ex)
  {
    alert("在LAGroupUniteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initInpBox1()
{
  try
  {
    fm.all('BranchAttr').value="";
    fm.all('BranchName').value="";
    fm.all('ManageComm').value="";
    fm.all('BranchLevel').value="";
    fm.all('AgentCode').value="";
    fm.all('Name').value="";
    fm.all('FoundDate').value="";
    fm.all('AgentCount').value="";
  }catch(e)
  {
    alert("在LAGroupUniteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initInpBox2()
{
  try
  {
    fm.all('BranchAttrU').value="";
    fm.all('BranchNameU').value="";
    fm.all('ManageCommU').value="";
    fm.all('BranchLevelU').value="";
    fm.all('AgentCodeU').value="";
    fm.all('NameU').value="";
    fm.all('FoundDateU').value="";
    fm.all('AgentCountU').value="";
  }catch(e)
  {
    alert("在LAGroupUniteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initInpBox3()
{
  try
  {
    fm.all('GroupManager').value="";
    fm.all('GroupManagerName').value="";
    fm.all('AgentGradeOld').value="";
    fm.all('AgentGradeNew').value="";
    fm.all('AgentGradeNewName').value="";
    fm.all('AdjustDate').value="";
  }catch(e)
  {
    alert("在LAGroupUniteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAGroupUniteInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("LAGroupUniteInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>