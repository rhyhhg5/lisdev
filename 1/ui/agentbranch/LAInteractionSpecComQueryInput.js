//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();

//提交按钮
function submitForm()
{
//alert("&&&&&&&&&&&&&&&&&&&");
   if(!check())
    return false ;
    
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	
  if (tBranchType=='5' && tBranchType2=='01')
  {//alert("@@@@@@@@@@@@@@@@@");
  	 DownLoad();//互动报表优化，用excel展示
  
  }
}
function check()
{
 if (!verifyInput())
 return false;
 return true; 
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LASpecComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function returnParent()
{
  var arrReturn = new Array();
  var tSel = ComGrid.getSelNo();
  if(fm.all('BranchType2').value==null || fm.all('BranchType2').value==""){
		alert("渠道类型不能为空！");
		return false;
	}		
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
    try
    {	
      arrReturn = getQueryResult();
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
		
  }
}

function getQueryResult()
{
  var arrSelected = new Array();
  tRow = ComGrid.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet == null )
    return arrSelected;

	var strSQL = "";
   strSQL = "select a.AgentCom,a.Name,a.ManageCom,a.ACType,a.Assets,a.ChiefBusiness,a.Profits,a.Corporation,"
   +"a.ProtocalNo,a.SellFlag,a.UpAgentCom,a.BankAccNo,a.LicenseNo,a.Licensestartdate,a.Licenseenddate,"
   +"a.branchtype2,(select d.codename from ldcode d where d.code=a.branchtype2 and d.codetype='branchtype2')BranchType2Name ,"
   +"a.BankCode ,a.EndFlag,a.EndDate,a.bankaccname,a.bankaccopen,"
   +"(select signdate from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only),  "
   +"(select enddate from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only),"
   +"(select ProtocolNo from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only),"
   +"(select startdate from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only),a.Address,a.AgentOrganCode,a.bankcode "
   +"from lacom a where 1=1 " 
   + "and a.agentcom='"+ComGrid.getRowColData(tRow-1,1)+"'"; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
        //判断是否查询成功
  if (!turnPage.strQueryResult) {
      alert("查询失败！");
      return false;
     }
  //查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  return arrSelected;

}

// 查询按钮
function easyQueryClick()
{
  // 初始化表格
  initComGrid();
  var tReturn = parseManageComLimitlike();
  // 书写SQL语句
  var strSQL = "";

  	strSQL  = "select AgentCom,ManageCom,Name,UpAgentCom,(select ProtocolNo from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only),LicenseStartDate,LicenseEndDate"//(select startdate from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only),(select enddate from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only)	       
	          +" from LACom a where actype in "
	          +"(select code from ldcode where codetype='actype' and code<>'01') "
	          + tReturn
	          + getWherePart('AgentCom','AgentCom','like')
	          + getWherePart('Name','Name','like')
	          + getWherePart('ManageCom','ManageCom','like')
	          + getWherePart('ACType')
	          + getWherePart('Assets','Asset','','1')
	          + getWherePart('ChiefBusiness')
	          + getWherePart('AgentOrganCode');
	  if (document.fm.Asset.value != "" && document.fm.Asset.value != null)
	  {
	  	strSQL += " and Assets = "+document.fm.Asset.value;
	  }
	  if(document.fm.Profit.value != "" && document.fm.Profit.value != null)
	  {
	  	strSQL += " and Profits = "+document.fm.Profit.value;
	  }
	  strSQL += getWherePart('Corporation')
	          + getWherePart('SellFlag')	         
	          + getWherePart('BankAccNo')
	          + getWherePart('BranchType')
	          + getWherePart('BranchType2');
	          
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3,4,5,6]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ComGrid;              
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;   
  //设置查询起始位置
  turnPage.pageIndex       = 0;   
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}
  //下载按钮
function DownLoad(){
	//alert("***********************");
  var strDownloadSQL = "";
  //var tReturn = parseManageComLimitlike();
  strDownloadSQL="select AgentCom,ManageCom,Name,ProtocalNo,LicenseStartDate,LicenseEndDate"//(select startdate from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only),(select enddate from lacont where agentcom=a.agentcom order by ProtocolNo desc fetch first 1 rows only)	       
	          +" from LACom a where actype in "
	          +"(select code from ldcode where codetype='actype' and code<>'01') "
	          //+ tReturn
	          + getWherePart('AgentCom','AgentCom','like')
	          + getWherePart('Name','Name','like')
	          + getWherePart('ManageCom','ManageCom','like')
	          + getWherePart('ACType')
	          + getWherePart('Assets','Asset','','1')
	          + getWherePart('ChiefBusiness');
	       if (document.fm.Asset.value != "" && document.fm.Asset.value != null)
	  {
	  	strDownloadSQL += " and Assets = "+document.fm.Asset.value;
	  }
	  if(document.fm.Profit.value != "" && document.fm.Profit.value != null)
	  {
	  	strDownloadSQL += " and Profits = "+document.fm.Profit.value;
	  }
	         strDownloadSQL += getWherePart('Corporation')
	                           + getWherePart('SellFlag')	        
	                           + getWherePart('BankAccNo')
	                           + getWherePart('BranchType')
	                           + getWherePart('BranchType2');
	                           + " order by AgentCom";
	     fm.querySql.value = strDownloadSQL;
//定义列名
var strSQLTitle = "select '中介机构编码','负责管理机构','中介机构名称','协议号','协议起始日','协议到期日' from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
 
//定义表名
 fm.all("Title").value="select '中介协议查询结果' from dual where 1=1 " ;  
fm.action = " ../agentbranch/LPrintTempSave.jsp";
fm.submit();
}