 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mFlag="0"; 
var mFlag1="0"; 
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
	if(showInfo!=null){
	  try{
	    showInfo.focus();
	  }
	  catch(ex){
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (beforeSubmit() == false)
    return false;
  if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    AgentGrid.clearData("AgentGrid");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
//if(fm.AgentSeries.value!='0'&&fm.AgentGrade.value!='B01'){
//	if(fm.IsSingle.value=='1'){
//		alert("该销售人员非业务序列人员，只能选择断裂原有关系！如要带走原有关系，请执行团队调整操作。");
//		return false;
//	}
//}
 alert(fm.all("AgentCode").value);
 var tsql="select a.agentgroup,b.agentstate from  latree a,laagent b   where  a.agentcode=b.agentcode and  b.groupagentcode='"+fm.all("AgentCode").value+"' ";
 var strQueryResult = easyQueryVer3(tsql, 1, 0, 1);
 if (!strQueryResult) {
 	alert("不存在要调动的业务员！");
 	return false ;
 	
  }
  
  var arr = decodeEasyQueryResult(strQueryResult);
  var tAgentGroup=arr[0][0];
  var tAgentState=arr[0][1];
  
   
  if(tAgentGroup!=fm.all("OldAgentGroup").value)
 {
 	alert("销售人员代码与销售单位代码不对应！");
 	return false ;
 	
 }
 if (tAgentState>='06')
 {
   alert("离职人员不能调动团队！");	
   return false ;
 }


}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//验证字段的值
function BranchConfirm()
{
	var tReturn = parseManageComLimitlike();
  if((trim(fm.all('BranchCode').value)=="")||(fm.all('BranchCode').value==null))
  {
    alert("请输入目标展业机构代码！");
    fm.all('BranchCode').focus();
    return false;
  }
  var strSQL = "";

  //查询出非停业且展业级别在营业组以上的
  strSQL = "select BranchAttr,Name,BranchType,BranchManager,BranchLevel,UpBranch,AgentGroup,Branchmanagername,BranchType2 from LABranchGroup where 1=1 "//xijh增加Branchmanagername
         + tReturn
	     +" and BranchAttr = '"+trim(fm.all('BranchCode').value)+"' and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null)"
	     +getWherePart('BranchType','BranchType')
	     +getWherePart('BranchType2','BranchType2');

  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!strQueryResult) {
  	//查询失败
    alert("该展业机构无效！");
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    //fm.all('BranchType').value = "";
    fm.all('BranchManager').value = "";
    fm.all('ManagerName').value ="";
    fm.all('hideAgentCode').value = "";
    fm.all('hideBranchLevel').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('hideAgentGroup').value = "";
    return false;
  }
  var arr = decodeEasyQueryResult(strQueryResult);
  //if (((trim(arr[0][2])=='3')&&(trim(arr[0][4])!='3'))||((trim(arr[0][2])=='1')&&(trim(arr[0][4])!='01')))//jiangcx add for BK
  if ((trim(arr[0][2])=='1')&&(trim(arr[0][8])=='01') && (trim(arr[0][4])!='01'))//xjh修改，保留原有银代部分，去掉个险部分，因为个险部分已经将调整范围扩大
  {
  	//判定展业机构类型
    alert('业务员只能在营业组间调动！');
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    //fm.all('BranchType').value = "";
    fm.all('BranchManager').value = "";
    fm.all('ManagerName').value ="";
    fm.all('AgentCode').value = "";
    fm.all('BranchLevel').value = "";
    fm.all('UpBranch').value = "";
    fm.all('AgentGroup').value = "";
    return false;

  }
  fm.all('BranchName').value = arr[0][1];
  fm.all('BranchType').value = arr[0][2];
  fm.all('BranchManager').value = arr[0][3];//xjh增加，显示机构管理人员代码 
	fm.all('ManagerName').value = arr[0][7];//xjh增加，显示机构管理人名称
  fm.all('BranchLevel').value = arr[0][4]; 
  fm.all('hideUpBranch').value = arr[0][5];
  fm.all('hideAgentGroup').value = arr[0][6];

  if(arr[0][2]=='2'){
  	//如果是法人则不作目标管理人员检测。
  	//fm.BranchManager.style.readonly='true';此句无效
  	return true;
  }

  if((arr[0][3]==null)||(trim(arr[0][3])==''))
  {
  	alert('请先确定目标机构的管理人员！');
  	fm.all('BranchManager').value = '';
  	fm.all('hideAgentCode').value = '';
  	fm.all('BranchManager').focus();
  	return false;
  }
  else
  {
  	strSQL = "select Name,AgentCode from LAAgent where AgentCode = '"+trim(arr[0][3])+"'";
  	strQueryResult = easyQueryVer3(strSQL,1,0,1);
  	if (!strQueryResult)
  	{
  	   fm.all('BranchManager').value = '';
  	   fm.all('hideAgentCode').value = '';
  	   //fm.BranchManager.disabled = false;
  	   alert('请先确定目标机构的管理人员！');
  	   fm.all('BranchManager').focus();
  	   return false;
  	}
  	else
  	{
  	   arr = decodeEasyQueryResult(strQueryResult);
           fm.all('ManagerName').value = arr[0][0];
  	   fm.all('hideAgentCode').value = arr[0][1];
  	   fm.all('BranchManager').value = arr[0][1];
  	   //fm.BranchManager.disabled = true;
  	}
  }
  return true;

}

function changeManager()
{
   if (getWherePart('BranchManager')=='')
     return false;
   var strSQL = "select AgentCode,Name from LAAgent where 1=1 and (AgentState is null or AgentState < '03')"
                + getWherePart('AgentCode','BranchManager')
                + getWherePart('AgentGroup','BranchCode','<>');
   var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('该代理人无效！');
   	fm.all('BranchManager').value = '';
   	fm.all('ManagerName').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   fm.all('ManagerName').value = arr[0][1];
}

function queryAgent()
{
   var tReturn = getManageComLimitlike("c.ManageCom");
   var  tBranchCode=fm.all('BranchCode').value;
   var  tManageCom=tBranchCode.substring(0,8);
  // alert(tManageCom);
   if((fm.all('BranchCode').value==null)||(trim(fm.all('BranchCode').value)==""))
   {
      alert("请先确定目标展业机构！");
      fm.all('BranchCode').focus();
      return false;
   }
  
   if(trim(fm.all('BranchCode').value) == trim(fm.all('AgentGroup').value))
   {
   	alert('调动人员所属展业机构不能和目标机构相同！');
   	fm.all('AgentGroup').focus();
   	return false;
   }
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  if(tBranchType==1 && tBranchType2=='01')

  strSQL = "select a.AgentCode,b.Name,c.BranchAttr,a.AgentGrade from LATree a,LAAgent b,LABranchGroup c where 1=1 and (AgentGrade <= 'B01') "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('a.AgentCode','AgentCode')
	     + getWherePart('a.AgentGrade','AgentGrade')	     
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup  "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and  c.managecom ='"+tManageCom+"' " 
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"' ";

	else if(tBranchType==3)

		strSQL = "select a.AgentCode,b.Name,c.BranchAttr,a.AgentGrade from LATree a,LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('a.AgentCode','AgentCode')
	     + getWherePart('a.AgentGrade','AgentGrade')
	     + getWherePart('a.AgentCode','BranchManager','<>')
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and b.agentkind>'03' and  c.managecom ='"+tManageCom+"' "
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"'";

	     else if(tBranchType==2 && tBranchType2=='01')

		strSQL = "select a.AgentCode,b.Name,c.BranchAttr,a.AgentGrade from LATree a,LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('a.AgentCode','AgentCode')	    
	     + getWherePart('a.AgentGrade','AgentGrade')
	     + getWherePart('a.AgentCode','BranchManager','<>')
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup and a.agentgrade<'E01'  "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and  c.managecom ='"+tManageCom+"' "
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"'";;

  // alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
  	alert('查询失败,不存在有效纪录！');
  	return false;
  }
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3]);
  turnPage.pageDisplayGrid = AgentGrid;
  turnPage.strQuerySql     = strSQL;
  turnPage.pageIndex       = 0;
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}



function afterCodeSelect( cCodeName, Field )
 {
 	
  if( cCodeName == "laagentname")
  {
	  
  //	var agent_Sql="select branchcode from laagent where agentcode='"++"'";
  	tsql = "select a.agentgrade,(select b.gradename from laagentgrade b where b.gradecode=a.agentgrade),"
  	+"c.recomagentcode,(select d.name from laagent d where d.agentcode=c.recomagentcode),"
  	+"c.recomagentgrade,(select b.gradename from laagentgrade b where b.gradecode=c.recomagentgrade), "
  	+"agentseries,a.branchcode,getUniteCode(c.recomagentcode) from latree a left join larecomrelation c on c.agentcode=a.agentcode and recomgens=1 "
  	+" where a.agentcode = (select agentcode from laagent where groupagentcode = '"+fm.AgentCode.value+"')";
  	//'"+fm.AgentCode.value+"'
  	var arr = easyExecSql(tsql);
  	//alert(arr);
  	//alert(arr[0][8]);
  	if(arr){
  		fm.AgentGrade.value=arr[0][0];
  		 
  		if(fm.AgentGrade.value>'B01')
  		{
  			alert('不能对主管进行调动！');
  			//initForm();
  			fm.AgentCode.value="";
  			return false;
  			
  		}
  		fm.AgentGradeName.value=arr[0][1];
  		fm.RecomAgentCode.value=arr[0][2];
  		fm.RecomAgentName.value=arr[0][3];
  		fm.RecomAgentGrade.value=arr[0][4];
  		fm.RecomAgentGradeName.value=arr[0][5];
  		fm.AgentSeries.value=arr[0][6];
  		//alert(fm.AgentSeries.value);
  		fm.all("OldAgentGroup").value=arr[0][7];
  		//zff 2014-11-14 集团统一工号
  		fm.groupAgentCode.value=arr[0][8];
  		
  	//	alert(arr[0][7]);
  	//	alert(fm.all("OldAgentGroup").value);
  		mFlag='1';
  	}
  	 
  	tsql = "select name,branchattr,branchmanager,managecom,"
  	+"(select b.name from laagent b where b.agentcode = a.branchmanager),getUniteCode(branchmanager) "
  	+" from labranchgroup a where agentgroup='"+fm.OldAgentGroup.value+"'";
  	var brr = easyExecSql(tsql);
  	if(brr){
  		fm.OldBranchName.value = brr[0][0];
  		fm.OldBranchCode.value = brr[0][1];
  		fm.OldBranchManager.value = brr[0][2];
  		fm.OldManageCom.value = brr[0][3];
  		fm.OldManagerName.value = brr[0][4];
  		//zff 2014-11-14 集团统一工号
  		fm.grpOldBranchManager.value = brr[0][5];
  	}
  	
  	
  }
  if(cCodeName == "branchattr"){
  	tsql="select name,agentgroup,branchmanager,managecom,branchlevel,"
  	+"(select b.name from laagent b where b.agentcode = a.branchmanager), getUniteCode(branchmanager)"
  	+" from labranchgroup a where branchtype='1' and branchtype2='01' and branchattr='"
  	+fm.BranchCode.value+"'   and (EndFlag <> 'Y' or EndFlag is null) ";
  	var crr = easyExecSql(tsql);
  	if(crr){
  		fm.BranchName.value = crr[0][0];
  		fm.AgentGroup.value = crr[0][1];
  		fm.BranchManager.value = crr[0][2];
  		fm.ManageCom.value = crr[0][3];
  		fm.BranchLevel.value = crr[0][4];
  		fm.ManagerName.value = crr[0][5];
  		//zff 2014-11-14 集团统一工号
  		fm.grpBranchManager.value = brr[0][6];
  		mFlag="1";

  	}
  else
  	{
  		alert("没有此团队!");
  		fm.BranchCode.value='';
  		return false ;
  	}
  }
}
