<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankBranchGroupSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
  ALABankBranchGroupZUUI tALABankBranchGroupZUUI = new ALABankBranchGroupZUUI();
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  System.out.println("hideOperate:----"+request.getParameter("hideOperate"));
  System.out.println("operater:--"+tOperate);
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  tLABranchGroupSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLABranchGroupSchema.setName(request.getParameter("BranchName"));
  if(request.getParameter("hmanagecom")!=null && !request.getParameter("hmanagecom").equals("")){
  	tLABranchGroupSchema.setManageCom(request.getParameter("hmanagecom"));
  }else{
  	tLABranchGroupSchema.setManageCom(request.getParameter("ManageCom"));
  }
  if(request.getParameter("hbranchlevel")!=null && !request.getParameter("hbranchlevel").equals("")){
  	tLABranchGroupSchema.setBranchLevel(request.getParameter("hbranchlevel"));//级别
  }else{
  	tLABranchGroupSchema.setBranchLevel(request.getParameter("BranchLevel"));//级别
  }
  //System.out.println("savejsp:managecom   "+request.getParameter("ManageCom"));
  //System.out.println("UpBranchAttr:"+request.getParameter("UpBranchAttr"));
  //if(request.getParameter("UpBranch")!=null && !request.getParameter("UpBranch").equals('')){
  	//tLABranchGroupSchema.setUpBranch(request.getParameter("UpBranch"));
  //}else if
  if(request.getParameter("hupbranchattr")!=null && !request.getParameter("hupbranchattr").equals("") && tLABranchGroupSchema.getBranchLevel().equals("41")){
  	tLABranchGroupSchema.setUpBranch(request.getParameter("hupbranchattr"));
  }else if(tLABranchGroupSchema.getBranchLevel().equals("42")){
  	tLABranchGroupSchema.setUpBranch("");
  }else {
  	tLABranchGroupSchema.setUpBranch(request.getParameter("UpBranch"));
  }
    
    
  System.out.println("UpBranch:"+tLABranchGroupSchema.getUpBranch().trim());
  tLABranchGroupSchema.setBranchAttr(request.getParameter("BranchAttr"));
  tLABranchGroupSchema.setBranchType(request.getParameter("BranchType"));
  tLABranchGroupSchema.setBranchType2(request.getParameter("BranchType2"));
  tLABranchGroupSchema.setInsideFlag(request.getParameter("InsideFlag"));
   
  tLABranchGroupSchema.setBranchManager(request.getParameter("BranchManager"));
  tLABranchGroupSchema.setBranchManagerName(request.getParameter("BranchManagerName"));
  tLABranchGroupSchema.setBranchAddressCode(request.getParameter("BranchAddressCode"));
  tLABranchGroupSchema.setBranchAddress(request.getParameter("BranchAddress"));
  tLABranchGroupSchema.setBranchPhone(request.getParameter("BranchPhone"));
  tLABranchGroupSchema.setBranchFax(request.getParameter("BranchFax"));
  tLABranchGroupSchema.setBranchZipcode(request.getParameter("BranchZipcode"));
  tLABranchGroupSchema.setFoundDate(request.getParameter("FoundDate"));
  tLABranchGroupSchema.setAStartDate(request.getParameter("FoundDate"));
  tLABranchGroupSchema.setEndDate(request.getParameter("EndDate"));
  tLABranchGroupSchema.setEndFlag(request.getParameter("EndFlag"));
  //tLABranchGroupSchema.setCertifyFlag(request.getParameter("CertifyFlag"));
  //String BranchJobType=(request.getParameter("FieldFlag").equals("Y"))?"1":"0";
  //tLABranchGroupSchema.setBranchJobType(BranchJobType);/*jiangcx 是否是外勤字段*/
  tLABranchGroupSchema.setState(request.getParameter("State"));
  //tLABranchGroupSchema.setCostCenter(request.getParameter("CostCenter"));
  tLABranchGroupSchema.setOperator(request.getParameter("Operator"));
  //tLABranchGroupSchema.setUpBranchAttr("");/*jiangcx 上级机构代码*/
  System.out.println("operater:--"+tOperate);
  System.out.println("BranchType:--"+request.getParameter("BranchType"));
  System.out.println("BranchType2:--"+request.getParameter("BranchType2"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLABranchGroupSchema);
	tVData.add(tG);
  try
  {
    tALABankBranchGroupZUUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tALABankBranchGroupZUUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">

parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>