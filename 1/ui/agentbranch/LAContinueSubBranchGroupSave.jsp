<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupQuChuSave.jsp
//程序功能：
//创建日期：2008-07-02 15:12:33
//创建人  ：苗祥征
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();

  LABranchGroupSubChuUI tLABranchGroupSubChuUI = new LABranchGroupSubChuUI();

  //输出参数
  CErrors tError = null;
  //设置编码格式
  request.setCharacterEncoding("GBK");
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLABranchGroupSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLABranchGroupSchema.setName(request.getParameter("Name"));
    System.out.println(request.getParameter("Name"));
    tLABranchGroupSchema.setManageCom(request.getParameter("ManageCom"));
    tLABranchGroupSchema.setBranchAttr(request.getParameter("BranchAttr"));
    tLABranchGroupSchema.setBranchType(request.getParameter("BranchType"));
    tLABranchGroupSchema.setBranchType2(request.getParameter("BranchType2"));
    System.out.println("BranchType:"+request.getParameter("BranchType"));
    tLABranchGroupSchema.setBranchLevel(request.getParameter("BranchLevel"));
    //xjh Modify 2005/03/15,当插入一条新纪录时，UpBranch暂时存放上级机构的外部编码
    if(tOperate.equals("INSERT||MAIN"))
    {
        tLABranchGroupSchema.setUpBranch(request.getParameter("UpBranchA"));
    }
    else
    {
    		tLABranchGroupSchema.setUpBranch(request.getParameter("UpBranchA"));
    		tLABranchGroupSchema.setBranchAttr(request.getParameter("HBranchAttr"));
    }
  //  tLABranchGroupSchema.setBranchManager(request.getParameter("BranchManager"));
 //   tLABranchGroupSchema.setBranchManagerName(request.getParameter("BranchManagerName"));
    tLABranchGroupSchema.setBranchAddressCode(request.getParameter("BranchAddressCode"));
    tLABranchGroupSchema.setBranchAddress(request.getParameter("BranchAddress"));
    tLABranchGroupSchema.setBranchPhone(request.getParameter("BranchPhone"));
    tLABranchGroupSchema.setBranchFax(request.getParameter("BranchFax"));
    tLABranchGroupSchema.setBranchZipcode(request.getParameter("BranchZipcode"));
    tLABranchGroupSchema.setFoundDate(request.getParameter("FoundDate"));
    tLABranchGroupSchema.setAStartDate(request.getParameter("FoundDate"));
    tLABranchGroupSchema.setEndDate(request.getParameter("EndDate"));
    tLABranchGroupSchema.setEndFlag(request.getParameter("EndFlag"));
//    tLABranchGroupSchema.setCertifyFlag(request.getParameter("CertifyFlag"));
    tLABranchGroupSchema.setFieldFlag(request.getParameter("FieldFlag"));
    tLABranchGroupSchema.setState(request.getParameter("State"));
    tLABranchGroupSchema.setOperator(request.getParameter("Operator"));
    tLABranchGroupSchema.setUpBranchAttr(request.getParameter("UpBranchAttr"));
    tLABranchGroupSchema.setTrusteeShip(request.getParameter("TrusteeShip"));
    tLABranchGroupSchema.setCostCenter(request.getParameter("CostCenter"));
 
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLABranchGroupSchema);
	tVData.add(tG);
  try
  {
    System.out.println("come in");
    tLABranchGroupSubChuUI.submitData(tVData,tOperate);
    System.out.println("come out");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABranchGroupSubChuUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {

    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    	System.out.println(Content);
    }
  }
  System.out.println(Content);
  //添加各种预处理
System.out.println("111111");
%>
<html>

<script language="javascript" type="">
	var FlagStr='<%=FlagStr%>';
	var Content1='<%=Content%>';
parent.fraInterface.afterSubmit(FlagStr,Content1);
</script>
</html>

