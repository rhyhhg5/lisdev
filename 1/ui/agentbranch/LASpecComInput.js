//               该文件中包含客户端需要处理的函数和事件

var acodeSql;
var mDebug="1";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    } 
  }
}

//提交，保存按钮对应操作 提交后数据传输到save.jsp页面
function submitForm()
{
  if (!beforeSubmit())
    return false;
  if (!changeGroup())
		return false;
  if(!checkAgentOrganCode())
	  return false;
  if(document.fm.hideOperate.value=="")
  {
  	mOperate="INSERT||MAIN";
  	if(document.fm.AgentCom.value != "" && document.fm.AgentCom.value != null)
		{
			alert("查询出来的机构，请用修改功能！");
			return false;
		}
  }

//  showSubmitFrame(1);

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,AgentCom )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	//fm.all('AgentCom').value=AgentCom;
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LASpecComInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 

//提交前的校验、计算  
function beforeSubmit()
{
	//document.fm.ACTypeCode.value = document.fm.ACType.value;
	
	//2012-05-07 gy添加专业代理机构和经纪机构在录入信息和修改信息时，其“业务许可证号”必须为15位校验
	 if( fm.all('ACType').value =='03'|| fm.all('ACType').value == '04')
	    {
	      if( fm.all('LicenseNo').value.length != "15" ) 
	      {
	        alert("专业代理机构和经纪机构在录入信息和修改信息时，其“业务许可证号”必须为15位！");
	        fm.all('LicenseNo').value="";
	        return false;
	      }
	    }
	
	if( fm.all('BranchType2').value == '04' )
	{
	    if( fm.all('ACType').value != '06' && fm.all('ACType').value != '07' ) 
	    {
	        alert("交叉销售渠道的中介机构类型只能是'财险'或'寿险'！");
	        return false;
	    }
	}
	if( fm.all('BranchType2').value=='02' )
	{
	    if( fm.all('ACType').value == '06' || fm.all('ACType').value == '07' ){
	        alert("中介渠道的中介机构类型不能是'财险'或'寿险'！");
	        return false;
	    }
	}
	if (!document.fm.ACType.value.length>0)
	{
		alert("请填写[中介机构类型]！");
		return false;
	}
   if(fm.all('ACType').value!='05')
    {
	 	var rowvalue=ComToAgentGrid.getRowColData(0,1);
  
		if(rowvalue=='')
		{
		 alert("请录入中介业务专员!");
		 return false;
		}
	}
	if (!document.fm.SellFlag.value.length>0)
	{
		alert("请填写[销售资格]！");
		return false;
	}
	if(!isDate(document.fm.ChiefBusiness.value))
	{
		alert("[开业时间]的输入内容不合法！");
		return false;
	}
	if(fm.all('Asset').value!=null && fm.all('Asset').value!="")
	{
		if(!isNumeric(document.fm.Asset.value))
		{
			alert("[注册资金]的输入内容不合法！");
			return false;
		}
	}
	if(fm.all('Profit').value!=null && fm.all('Profit').value!="")
	{
//		alert(12);
//		alert(fm.all('Profit').value);
		if(!isNumeric(document.fm.Profit.value))
		{
			alert("[去年手续费收入]的输入内容不合法！");
			return false;
		}
	}

	
	
	if (!verifyInput()) 
    return false;
  //添加操作
 if(fm.all('SignDate').value>fm.all('ProEndDate').value)
	{
	  alert("协议起始日应该小于协议到期时间");
	  fm.all('SignDate').value='';
	  fm.all('ProEndDate').value='';
	  fm.all('SignDate').focus();
	  return false;
	}
  if(fm.all('BankAccNo').value!=null && fm.all('BankAccNo').value!="")
  {
    if (fm.all('BankAccNo').value != fm.all('BankAccNoInsure').value)
    {
    	 alert("两次录入的[帐户号码]不一致!");
    	 fm.all('BankAccNoInsure').focus();
    	 return false;
    }
  }
  else
	{
		alert("请填写[机构帐户]！");
    fm.all('BankAccNo').focus();
    return false;
	}
	
	   if (fm.all('BankCode').value ==null || trim(fm.all('BankCode').value) == '')
        {
  	     alert("必须录入“SAP供应商编号”！");
  	     return false;
        } 
        else
        {
          var tBankCode=fm.all('BankCode').value;
          if(tBankCode.substring(0,1)!='2'||fm.all('BankCode').value.length!='10')	
        	{
  	       alert("SAP供应商编号录入不符合要求，请重新录入");
  	       return false;
          }
           
         // var strSQL = "";
         // strSQL = "select   bankcode  from  lacom where 1=1 and agentcom<>'"+fm.all('AgentCom').value+"' " 
         // + getWherePart('BankCode','BankCode');
         // var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
          //if (strQueryResult) {
          //  alert("该SAP供应商编码已经存在，请重新录入！");
          //  fm.all("BankCode").value = '';
          //  return false;
          //}   
        }

	
  fm.all('ACType').disabled=false;
  fm.all('ManageCom').disabled=false;	
  return true;
  
    
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,100,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


          

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if ((fm.all("AgentCom").value==null)||(fm.all("AgentCom").value==''))
  {
    alert("请先确定代理机构！");
    fm.all('AgentCom').focus();
  }

  else
  {
   if (!queryAgentCom())
      return;
   if(!checkAgentOrganCode())
		  return false;
    if (confirm("您确实想修改该记录吗?"))
    {
      document.fm.hideOperate.value = "UPDATE||MAIN";
      mOperate = "UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
  
  /**
  else
  {
  if (confirm("你确实像修改该记录么?"))
  {
    if(fm.all('SignDate').value==fm.all('SignDate1').value)
    {
    document.fm.hideOperate.value = "UPDATE||MAIN";
    mOperate = "UPDATE||MAIN";
      submitForm();
    }
    else
    {
     //var AgentComstr = fm.all.('AgentCom').value.substring(0,2);
     // var ManageComstr = fm.all('ManageCom').value.substring(2,4);
     //  var AgentCom1 = AgentComstr+ManageComstr+"0000";
     //var StartDate = fm.all('StartDate').value;
     
    return;
    }
  
  }
  }
  */


}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
   var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    showInfo=window.open("./LASpecComQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCom").value==null)||(fm.all("AgentCom").value==''))
  {
    alert("请先确定代理机构！");
    fm.all('AgentCom').focus();
  }
  else
  {
    if (!queryAgentCom())
      return false;
    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";  
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function getManagerName(input)
{
	
	if(fm.all('AgentCode').value==""||fm.all('AgentCode').value==null)
	{		
		fm.all('AgentName').value=="";
		}
	else
		{
  	var sql = "select Name from LAAgent where 1=1"
          + getWherePart('AgentCode','AgentCode');
  	var strResult = easyQueryVer3(sql, 1, 1, 1);
  	if (strResult)
  	{
  		var arrDataSet = decodeEasyQueryResult(strResult);
  		var tArr_Record = new Array();
  		tArr_Record = chooseArray(arrDataSet,[0]);
  		input.value = tArr_Record[0][0];
 	 }
  	else
 	 {
  		input.value="";
  		alert("客户经理代码输入有误！");
  	}
	}
}

//验证业务员编码的合理性
function checkValid()
{ 
  if (getWherePart('AgentCode')=='')
    return false;
  var strSQL = "";
  strSQL = "select * from LAAgent where 1=1 "
	   + getWherePart('AgentCode');
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此专管员！");
    fm.all('AgentCode').value = "";
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = chooseArray(arrDataSet,[0,1,43,44,61]);
  if(tArr[0][2]!="30"){
    alert("该人非专管员！");
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    return;
  }
  if(tArr[0][4]=="91"){
    alert("此专管员已被解聘！");
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    return;
  }
  //<rem>######//
  //fm.all('AgentGroup').value = tArr[0][1];
  //</rem>######//
  fm.all('AgentState').value = tArr[0][4];
  fm.all('DevGrade').value   = tArr[0][3];

  //<addcode>############################################################//
  old_AgentGroup=tArr[0][1];
  fm.all('HiddenAgentGroup').value = tArr[0][1];
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"
     var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = chooseArray(arrDataSet_AgentGroup,[0,1,2]);
  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  //</addcode>############################################################//
}

function queryAgentCom()
{
  //var turnPage = new turnPageClass();	
  
	// 书写SQL语句
        var strSQL = "";
	strSQL = "select AgentCom from LACom where 1=1 "
	        + getWherePart('AgentCom');	 
	//turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
  if (!strQueryResult) {
    alert("不存在所要操作的代理机构！");
    fm.all("AgentCom").value = '';
    return false;
    }
    return true;
}

//用来显示返回的选项,查询之后选中目标 点击返回,返回到父页 并且显示数据
function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();

  if( arrQueryResult != null )
  {
  	initForm();
    arrResult = arrQueryResult;
    fm.all('AgentCom').value = arrResult[0][0];  
    fm.all('Name').value = arrResult[0][1];                                              
    fm.all('ManageCom').value = arrResult[0][2]; 
    showOneCodeNametoAfter('comcode','ManageCom');                                             
    fm.all('ACType').value = arrResult[0][3];  
    showOneCodeNametoAfter('ACType','ACType');                                           
    fm.all('Asset').value = arrResult[0][4];                                                                                   
    fm.all('ChiefBusiness').value = arrResult[0][5];  
     
    fm.all('profit').value = arrResult[0][6];                                                                                     
    fm.all('Corporation').value = arrResult[0][7];
   //   alert(12);
   // fm.all('ProtocalNo').value="12";
     
    fm.all('SellFlag').value = arrResult[0][9];
    showOneCodeNametoAfter('yesno','SellFlag'); 
   // alert(13);
   // fm.all('UpAgentCom').value = arrResult[0][10];
     
    fm.all('BankAccNo').value=arrResult[0][11];
    fm.all('BankAccNoInsure').value=arrResult[0][11];
    fm.all('LicenseNo').value=arrResult[0][12];
    fm.all('Licensestart').value=arrResult[0][13];
    fm.all('Licenseend').value=arrResult[0][14];
    fm.all('BankAccName').value=arrResult[0][20];
    fm.all('BankAccOpen').value=arrResult[0][21];
    fm.all('BranchType2').value=arrResult[0][15];
    fm.all('BranchType2Name').value=arrResult[0][16];
//
    fm.all('BankCode').value=arrResult[0][17];
    fm.all('EndFlag').value=arrResult[0][18];
    showOneCodeNametoAfter('EndFlag','EndFlag');
    if(arrResult[0][19] != null && arrResult[0][19] != '')
    {
      fm.all('EndDate').value=arrResult[0][19];
    }
    //
    fm.all('ACType').disabled=true;
    //modify by zhuxt 20140924
    fm.all('ManageCom').disabled=true;
    //fanting  添加2个属性在字段上
    
    fm.all('SignDate').value = arrResult[0][22];
    fm.all('ProEndDate').value= arrResult[0][23];
    fm.all('ProtocolNo').value= arrResult[0][24];
    fm.all('SignDate1').value= arrResult[0][22];
    //alert("fm.all('SignDate1').value为"+fm.all('SignDate1').value);
    //
    fm.all('StartDate').value= arrResult[0][25];  
    fm.all('Address').value= arrResult[0][26];
    fm.all('AgentOrganCOde').value= arrResult[0][27];
    getQUERYSQL();
    getRelationInfo();
    
                                                                                                                                                                                                                                                  	
  } 
}

function WriteContract()
{
//	alert("WriteContract!");
	if(fm.all('ContNo').value !="")
	{
		showInfo=window.open("./LASpecComWriteContract.html");
	}
	else
	{
		alert("请先保存再录入合同细则！");
	}
}

/*function CreatContractNo()
{
//	alert("CreatContractNo!");
	var str_sql="";
	str_sql="select max(Contno) from LACont";
	var strQueryResult = easyQueryVer3(str_sql, 1, 1, 1);
	var arrDataSet = decodeEasyQueryResult(strQueryResult);
	var tArr = new Array();
	tArr = chooseArray(arrDataSet,[0,1,2]);
	fm.all('ContNo').value=tArr[0][0];
	alert(tArr[0][0]);
}
*/
function afterCodeSelect( cCodeName, Field )
{ 
	try{
	if (cCodeName=="yesno")
	{
		if (Field.value=="Y")
		{ 
		   write.style.display=""; 
		}
		else
		{
		   write.style.display="none";
		}
	}
	
	if (cCodeName=="comcode" || cCodeName=="BranchType2")
	{
		 
		var tBranchType = fm.all('BranchType').value;
        var tBranchType2 = fm.all('BranchType2').value; 
		acodeSql = " #"+tBranchType+"# and LABranchGroup.BranchType2=#"+tBranchType2+"# and LAAgent.ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
		initComToAgentGrid();
	}
	if( cCodeName == "AgentCode" )	
		{
			checkvalid();//loadFlag在页面出始化的时候声明
		}
	}catch(ex){}
} 

function getSql()
{
	return  acodeSql ;
}

function getComName(input)
{
  var sql = "select Name from LACom where 1=1"
          + getWherePart('AgentCom','UpAgentCom');
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	input.value = tArr_Record[0][0];
  }
  else
  {
  	input.value="";
  	alert("上级代理机构代码输入有误或没有此上级代理机构！");
  }
}

function getManagerName(input)
{
  var sql = "select Name from LAAgent where 1=1"
          + getWherePart('AgentCode');
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	input.value = tArr_Record[0][0];
  }
  else
  {
  	input.value="";
  	alert("业务员代码输入有误！");
  }
}

function getRelationInfo()
{
  var sql = "select a.Groupagentcode,a.name,b.branchattr,b.name,a.Groupagentcode from laagent a,labranchgroup b,LAComToAgent c where 1=1 and c.AgentCom='"+fm.all('AgentCom').value+"' and c.RelaType='1' and a.agentcode = c.agentcode and a.branchcode=b.agentgroup and (b.state<>'1' or b.state is null)";
  turnPage.strQueryResult  = easyQueryVer3(sql, 1, 1, 1);  
 //alert(strSQL);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("查询失败！");
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);


  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ComToAgentGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = sql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(arrDataSet, turnPage.pageIndex, '25');
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
 
}
function changeGroup(){
	if(fm.all('EndFlag').value == null || fm.all('EndFlag').value == "")
	{
		alert("停业属性为空，请置位！");
		return false;
	}
	if(fm.all('EndFlag').value == 'Y' && (fm.all('EndDate').value == null || fm.all('EndDate').value == ""))
  {
  	alert("停业属性为Y，请置停业日期！");
    return false;
  }
	if(fm.all('EndFlag').value == 'N' && fm.all('EndDate').value != "")
  {  
  	alert("停业属性为N，不能置停业日期！");
  	fm.all('EndDate').value = "";
    return false;
  }
  return true;
}
 
 
function checkvalid()
{
  var strSQL = "";
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select * from LAAgent where 1=1 "
	     + getWherePart('AgentCode')+" and ((AgentState  <'06') or (AgentState is null))";
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value  = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('AgentCode').value="";
    fm.all('AgentName').value  = "";
    return;
  }
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//
  fm.all('AgentName').value = tArr[0][5];
  //</rem>######//
}
function getQUERYSQL()
{
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value; 
  acodeSql = " #"+tBranchType+"# and LABranchGroup.BranchType2=#"+tBranchType2+"# and LAAgent.ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  initComToAgentGrid();
}
//增加对于江苏机构     中介组织机构代码  校验
function checkAgentOrganCode()
{
	var tmanagecom = fm.all('ManageCom').value.substring(0,4);
    if(tmanagecom=='8632')
    {
    	if(!verifyNotNull('中介组织机构代码',fm.all('AgentOrganCode').value )) 
        {
         alert("江苏公司中介机构录入时需录入中介组织机构代码");
         return false;
        }
    	var agentOrganCode = fm.all('AgentOrganCode').value.trim();
    	if(agentOrganCode.length!=9)
    	{
    		 alert("中介组织机构代码只能为9位");
             return false;
    	}
    }
    return true;
}