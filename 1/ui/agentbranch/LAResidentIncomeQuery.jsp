<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LAResidentIncomeQuery.jsp
	//程序功能：
	//创建日期：2014-3-18
	//创建人  ：zlw程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
<title>Sinosoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAResidentIncomeQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LAResidentIncomeQueryInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form method=post name=fm>
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divAgent1);"></td>
		<td class=titleImg>居民收入录入</td>
	</tr>
</table>
<Div id="divAgent1" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title>居民类型</TD><TD class=input><Input class='codeno' name=ResidentType
			CodeData="0|^1|城镇|^2|农村"
			ondblclick="return showCodeListEx('ResidentType',[this,ResidentTypeName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKey('ResidentType',[this,ResidentTypeName],[0,1],null,null,null,1);"><Input class=codename name=ResidentTypeName></TD>
        <TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name=ManageCom
			ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,4,'to_char(length(trim(comcode)))',1);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);"
			verify="管理机构|notnull&code:comcode"><Input class=codename
			name=ManageComName  elementtype=nacessary></TD>
	</TR>
	<TR class=common>
		<TD class=title>居民收入起始日期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=StartDate></TD>
		<TD class=title>居民收入终止日期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=EndDate></TD>
	</TR>
	<tr id='rpcdi' class=common style="display: '' ">
		<TD class=title>最近年度居民人均可支配收入</TD>
		<TD class=input><Input class='common' name=RecentPCDI
			verify="最近年度居民人均可支配收入|NUM"></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</tr>
	<tr id='rpcni' class=common style="display: 'none' ">
		<TD class=title>最近年度居民人均纯收入</TD>
		<TD class=input><Input class='common' name=RecentPCNI
			verify="最近年度居民人均纯收入|NUM"></TD>
	</tr>
</table>
<input type="hidden" class=input name=fmAction>
<input type="hidden" class=input name=querySql>
<input type="hidden" class=input name=querySqlTitle>
<input type="hidden" class=input name=Title>
</Div>
<table class=common border=0 width=100%>
	<TR class=common>
		<TD class=button width="10%" align=left><INPUT VALUE="查 询"
			TYPE=button class=cssbutton onclick="return  easyQueryClick();">
		<INPUT VALUE="下 载" TYPE=button class=cssbutton onclick="ListExecl();">
		<INPUT VALUE="返 回" TYPE=button class=cssbutton
			onclick="returnParent();"></TD>
	</TR>
</table>
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLACommisionGrid);"></td>
		<td class=titleImg>查询结果</td>
	</tr>
</table>
<div id="divLACommisionGrid" style="display:''">
<table class=common>
	<tr class=common>
		<td text-align:left colSpan=1><span id="spanLACommisionGrid"></span></td>
	</tr>
</table>
<INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
<INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();">
<INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
<INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();">
</div>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
