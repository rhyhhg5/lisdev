//该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	  parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
    //首先检验录入框
    if(!verifyInput()) return false;   
    initLACommisionGrid();
    var  sql= "select ManageCom,"+
    "(select name from ldcom where comcode = LAResidentIncome.managecom)"
    +",case Residenttype when '1' then '城镇' when '2' then '农村' end,"
    +"StartDate,EndDate,RecentPCDI,RecentPCNI,idx from LAResidentIncome"+" where  1=1";
			
	if(fm.all('ManageCom').value!=null && fm.all('ManageCom').value!=''){
		sql += " and managecom like '"+fm.all('ManageCom').value+"%'";
	}
	if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
		sql+=" and Startdate >='"+fm.all('StartDate').value+"'";
	}
	if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
		sql+=" and EndDate <='"+fm.all('EndDate').value+"'";
	}
	if(fm.all('Residenttype').value!=null && fm.all('Residenttype').value!=''){
		sql+=" and Residenttype = '"+fm.all('Residenttype').value+"'";
	}
	if(fm.all('RecentPCDI').value!=null && fm.all('RecentPCDI').value!=''){
		sql+=" and RecentPCDI = "+fm.all('RecentPCDI').value;
	}
	if(fm.all('RecentPCNI').value!=null && fm.all('RecentPCNI').value!=''){
		sql+=" and RecentPCNI = "+fm.all('RecentPCNI').value;
	}
    if (!easyQueryVer3(sql, 1, 0, 1)) {
    alert("查询失败！");
    return false;
    }
	turnPage.queryModal(sql, LACommisionGrid);   	
}


function ListExecl()
{	 
    //首先检验录入框
    if(!verifyInput()) return false;   
 
    var  sql= "select ManageCom,"+
    "(select name from ldcom where comcode = LAResidentIncome.managecom)"
    +",case Residenttype when '1' then '城镇' when '2' then '农村' end,"
    +"StartDate,EndDate,RecentPCDI,RecentPCNI from LAResidentIncome"+" where  1=1";
			
	if(fm.all('ManageCom').value!=null && fm.all('ManageCom').value!=''){
		sql += " and managecom like '"+fm.all('ManageCom').value+"%'";
	}
	if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
		sql+=" and Startdate >='"+fm.all('StartDate').value+"'";
	}
	if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
		sql+=" and EndDate <='"+fm.all('EndDate').value+"'";
	}
	if(fm.all('Residenttype').value!=null && fm.all('Residenttype').value!=''){
		sql+=" and Residenttype = '"+fm.all('Residenttype').value+"'";
	}
	if(fm.all('RecentPCDI').value!=null && fm.all('RecentPCDI').value!=''){
		sql+=" and RecentPCDI = "+fm.all('RecentPCDI').value+"";
	}
	if(fm.all('RecentPCNI').value!=null && fm.all('RecentPCNI').value!=''){
		sql+=" and RecentPCNI = "+fm.all('RecentPCNI').value+"";
	}
	
	fm.all('querySql').value = sql;
	var strSQLTitle = "select '管理机构','管理机构名称','居民类型','居民收入起始日期','居民收入终止日期','最近年度居民人均可支配收入','最近年度居民人均纯收入'  from dual where 1=1 ";
    fm.querySqlTitle.value = strSQLTitle;
	fm.all("Title").value="select '居民收入查询报表'  from dual where 1=1 ";  
    fm.action = "../agentquery/LAPrintTemplateSave.jsp";
    fm.submit();
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == 'ResidentType')
	{
	   fm.all('ManageCom').value = '';
	   fm.all('ManageComName').value = '';
	   if(fm.all('ResidentType').value=='1'){
		   fm.all('rpcdi').style.display='';
		   fm.all('rpcni').style.display='none';
		   fm.all('RecentPCDI').value = '';
		   fm.all('RecentPCNI').value = '';
	   }else if(fm.all('ResidentType').value=='2'){
	   	   fm.all('rpcdi').style.display='none';
	   	   fm.all('rpcni').style.display='';
	   	   fm.all('RecentPCDI').value = '';
		   fm.all('RecentPCNI').value = '';
	   }
	}
}

function beforeSubmit()
{
    var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
	if(startDate>endDate){
	   alert("终止日期要大于或等于起始日期！");
	   return false;
	}
   return true;
}

function returnParent()
{
  var arrReturn = new Array();
  var tSel = LACommisionGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点击返回按钮。" );
	}	
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery(arrReturn);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}
function getQueryResult()
{
	var arrSelected = null;
    arrSelected = new Array();
	var strSQL = "";
	tRow = LACommisionGrid.getSelNo();
	var idx = LACommisionGrid.getRowColData(tRow-1,8);
    strSQL +=" select * from LAResidentIncome  where 1=1 and idx ='"+idx+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
    //查询成功则拆分字符串，返回二维数组
  	arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}

