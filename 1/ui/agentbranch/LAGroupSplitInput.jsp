<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAGroupSplitInput.jsp
//程序功能：
//创建日期：2005-7-30 14:12
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAGroupSplitInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAGroupSplitInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAGroupSplitSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="./GroupSplitButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent);">
    <td class=titleImg>
      目标团队信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent" style= "display: ''">      
    <table class=common>
      <TR  class= common>
        <TD  class= title>
          销售团队代码
        </TD>
        <TD  class= input>
          <Input class='common' name=BranchAttr onChange="getGroupArm();" 
                 elementtype=nacessary verify="销售团队代码|NOTNULL&len=10" maxlength=10 >
        </TD>
        <TD  class= title>
          销售团队名称
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=BranchName >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队管理机构
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=ManageComm>
        </TD>
        <TD  class= title>
          级别
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=BranchLevel >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队管理人员代码
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=AgentCode>
        </TD>
        <TD  class= title>
          团队管理人名称
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=Name >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队成立日期
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=FoundDate >
        </TD>
        <TD  class= title>
          团队内人数
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=AgentCount>
        </TD>
      </TR>
    </table>
   </div>
   <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      新建销售单位
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
        
         <TD  class= title>
            销售团队代码
          </TD>
          <TD  class= input>
          	<Input class=common name=BranchAttrNew elementtype=nacessary verify="销售团队代码|NOTNULL&len=10" maxlength=10>
          </TD>
          <TD  class= title>
            销售团队名称
          </TD>
          <TD  class= input>
            <Input class= common name=BranchNameNew elementtype=nacessary verify="销售团队名称|NOTNULL">
          </TD>
	 
        </TR>
        <TR  class= common>
          <TD  class= title>
            所属管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageComNew verify="管理机构|code:comcode&NOTNULL&len>=4" 
            ondblclick="return showCodeList('comcode',[this,ManageComNewName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ManageComNewName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageComNewName readOnly elementtype=nacessary> 
          </TD> 
          <TD  class= title>
            级别
          </TD>
          <TD  class= input>
            <Input class= 'codeno' name=BranchLevelNew verify="展业机构级别|code:BranchLevel&NOTNULL" 
		                               ondblclick="return showCodeList('branchlevel',[this,BranchLevelNewName],[0,1],null,msql,1);" 
                                   onkeyup="return showCodeListKey('branchlevel',[this,BranchLevelNewName],[0,1],null,msql,1);" 
                                   ><Input class=codename name=BranchLevelNewName readOnly elementtype=nacessary> 
          </TD>          
            <Input type=hidden name=UpBranchAttrNew >
            <Input type=hidden name=FieldFlagNew value=''>
        </TR>
        <TR  class= common>
	      <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddressNew >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcodeNew verify="展业机构邮编|len<=6" >
          </TD>        
        </TR>
        <TR  class= common>
	      <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhoneNew >
          </TD>
          
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' verify="成立日期|NOTNULL" name=FoundDateNew format='short' elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='codeno' name=EndFlagNew  verify="停业|code:yesno&NOTNULL" 
             ondblclick="return showCodeList('yesno',[this,EndFlagNewName],[0,1]);" 
             onkeyup="return showCodeListKey('yesno',[this,EndFlagNewName],[0,1]);"
             ><Input class=codename name=EndFlagNewName readOnly elementtype=nacessary> 
          </TD>
        </tr>
      </table>
    </Div>
    <p> <font color="#ff0000"> 请注意维护营业部经理与分立团队经理的育成关系！  </font></p>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent2);">
    <td class=titleImg>
      选择调入新机构的人员
    </td>
    </td>
    </tr>
    </table>
    <Div  id= "divAdjustAgent2" style= "display: ''">
      <table  class= common>
        <tr  class= common>                    
           <td text-align: left colSpan=1> 
		         <span id="spanAgentGrid"> </span> 
           </td>
        </tr>
      </table> 
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
    </div>
    <br>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=GroupPart value=''>
    <input type=hidden name=AgentGroupU value=''>
    <input type=hidden name=AgentGroupA value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
