//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{	
	if (fm.all('ManageCom').value == null || fm.all('ManageCom').value == '')
  	{
  		alert("管理结构不能为空！");
  		fm.all('UpAgentCom').focus();
  		return false;
  	}
	var strSQL = "";
    strSQL = "select a.AgentCom,a.Name,"
             +"(Select c.Name From LAAgent c,lacomtoagent d Where c.AgentCode=d.AgentCode and a.AgentCom=d.AgentCom and d.RelaType='1'),"
             +"(select getunitecode(b.agentcode) from lacomtoagent b where a.AgentCom=b.AgentCom and b.RelaType='1'),"
             +"a.managecom,"
    	     +" case a.endflag when 'Y' then '是' when 'N' then '否' else '否' end ,a.makedate,a.enddate, "
             +" case a.sellflag when 'Y' then '有' when 'N' then '无' else '无' end ,(select codename from ldcode where codealias=a.BranchType and code=a.ACType and comcode=a.BranchType2),a.BusiLicenseCode  "
    	     +" from lacom a "
    	     +" where 1=1  and a.BranchType = '3' and a.BranchType2 = '01' " ; 
       if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!="")
       {
    	   strSQL += " and a.managecom like '"+fm.all('ManageCom').value+"%'";
       }
       if(fm.all('AgentCom').value!=null&&fm.all('AgentCom').value!="")
       {
    	   strSQL += " and a.AgentCom='"+fm.all('AgentCom').value+"'";
       }
       if(fm.all('Name').value!=null&&fm.all('Name').value!="")
       {
    	   strSQL+= " and a.name='"+fm.all('Name').value+"'";
       }
       if(fm.all('BankType').value!=null&&fm.all('BankType').value!="")
       {
    	   strSQL+= " and a.banktype='"+fm.all('BankType').value+"'";
       }
       if(fm.all('SellFlag').value!=null&&fm.all('SellFlag').value!="")
       {
    	   strSQL += " and a.sellflag ='"+fm.all('SellFlag').value+"'";
       }
       if(fm.all('ACType').value!=null&&fm.all('ACType').value!="")
       {
    	   strSQL += " and a.ACType ='"+fm.all('ACType').value+"'";
       }
       if(fm.all('BusiLicenseCode').value!=null&&fm.all('BusiLicenseCode').value!="")
       {
    	   strSQL += " and a.BusiLicenseCode ='"+fm.all('BusiLicenseCode').value+"'";
       }
       if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!="")
       {
           strSQL += "and a.agentcom in (select agentcom from lacomtoagent where agentcode=getagentcode('"+fm.all('AgentCode').value+"'))";
       }
       if(fm.all('AgentName').value!=null&&fm.all('AgentName').value!="")
       {
           strSQL += " and a.agentcom in (select agentcom from lacomtoagent where agentcode in (select agentcode from laagent where name='"+fm.all('AgentName').value+"' and branchtype='3' and branchtype2='01'))";
       }
       if(fm.all('EndFlag').value!=null&&fm.all('EndFlag').value=='Y')
       {
    	   strSQL += " and a.endflag ='Y'";
       }
       if(fm.all('EndFlag').value!=null&&fm.all('EndFlag').value=='N'){
    	   strSQL += " and (a.endflag ='N' or a.endflag is null)";
       }
       strSQL +=" order by a.agentcom";
     
       fm.querySql.value = strSQL;
  
       //定义列名
      var strSQLTitle = "select '银行机构编码','银行机构名称','负责员工姓名','负责员工代码','管理机构','是否停业','录入时间','停业时间','销售资格','中介机构类型','银保保险兼业代理资格证号'  from dual where 1=1 ";
      fm.querySqlTitle.value = strSQLTitle;
  
      //定义表名
      fm.all("Title").value="select '银行代理机构信息明细表 查询下载' from dual where 1=1  ";  
  
      fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
      fm.submit();

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}

function showCodeListManageCom(cObj,ManageComName)
{
    mEdorType =" 1 and  length(trim(comcode))=8";
	showCodeList('comcode',[cObj,ManageComName], [0,1], null, mEdorType, "1",1);               

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function returnParent()
{
	var tSel = -1;
	if (ComGrid.mulLineCount == 0)
	{
		alert("请先查询信息！");
		return false;
	}
  else if (ComGrid.mulLineCount == 1)
		tSel = 1;	
  else 
  	tSel = ComGrid.getSelNo();
  var arrReturn = new Array();
  if( tSel == -1 || tSel == 0)
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
    try
    {	
      //alert(tSel);
      arrReturn = getQueryResult();
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
  }
}

function getQueryResult()
{
  var arrSelected = new Array();
  var tRow = ComGrid.getSelNo();
	if (ComGrid.mulLineCount == 1)
		tRow = 1;	  
  if( tRow == 0 || tRow == null )
    return arrSelected;

	var	ResultSQL = "select AgentCom,Name,Address,Phone,"
    +"LinkMan,GrpNature,BankType,SellFlag,ManageCom,"
	+"AreaType,ACType,ChiefBusiness,BusiAddress,Noti,Operator,BusinessType,UpAgentCom,endflag,enddate,"
	+"BusiLicenseCode,LicenseStartDate,LicenseEndDate,"
	+"(select signdate from lacont where agentcom=lacom.agentcom order by protocolno desc fetch first 1 rows only),"
	+"(select enddate from lacont where agentcom=lacom.agentcom order by protocolno desc fetch first 1 rows only) ,bankcode,isdotflag  "
	+" ,BankAccNo,BankAccName,BankAccOpen,AgentOrganCode,"
	+"(select codename from ldcode where codealias=BranchType and code=ACType and comcode=BranchType2)"
	+"from lacom  where 1=1  "
	 + getWherePart('BranchType','BranchType')
     + getWherePart('BranchType2','BranchType2')
	       + " and agentcom='"+ComGrid.getRowColData(tRow-1,1)+"'";
	turnPage.strQueryResult  = easyQueryVer3(ResultSQL, 1, 0, 1);  
	//alert(ResultSQL);
        //判断是否查询成功
  if (!turnPage.strQueryResult) {
      alert("没有满足条件的记录！");
      return false;
     }
  //查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  return arrSelected;
}


// 查询按钮
function easyQueryClick()
{
  if (fm.all('ManageCom').value == null || fm.all('ManageCom').value == '')
  	{
  		alert("管理结构不能为空！");
  		fm.all('UpAgentCom').focus();
  		return false;
  	}
  // 初始化表格
  initComGrid();
  var tReturn = parseManageComLimitlike();
  // 书写SQL语句
  var strSQL = "select " +
  		"a.AgentCom" +
  		",a.Name" +
  		",(select getunitecode(b.agentcode) from lacomtoagent b where a.AgentCom=b.AgentCom and b.RelaType='1')" +
  		",(Select c.Name From LAAgent c,lacomtoagent d Where c.AgentCode=d.AgentCode and a.AgentCom=d.AgentCom and d.RelaType='1')" +
  		",a.Phone" +
  		",(select codename from ldcode where codealias=a.BranchType and code=a.ACType and comcode=a.BranchType2)"+
  		",a.BusiLicenseCode" +
  		",(select b.agentcode from lacomtoagent b where a.AgentCom=b.AgentCom and b.RelaType='1')"	       
	         +" from LACom a where  1=1 "
	         +tReturn
             + getWherePart('a.ACType','ACType')
	         + getWherePart('a.AgentCom','AgentCom')
	         + getWherePart('a.Name','Name')
	         + getWherePart('a.BankType','BankType')
	         + getWherePart('a.SellFlag','SellFlag')
	         + getWherePart('a.BusiLicenseCode','BusiLicenseCode')
	         +" and ManageCom like '"+fm.all('ManageCom').value+"%' "
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2');

	  if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!='')
    {
      strSQL=strSQL+" and a.agentcom in (select agentcom from lacomtoagent where agentcode=getagentcode('"+fm.all('AgentCode').value+"'))";
     } 
    if(fm.all('AgentName').value!=null&&fm.all('AgentName').value!='')
    {
      strSQL=strSQL+" and a.agentcom in (select agentcom from lacomtoagent where agentcode in (select agentcode from laagent where name='"+fm.all('AgentName').value+"' and branchtype='3' and branchtype2='01'))";
     }
     if(fm.all('EndFlag').value!=null && fm.all('EndFlag').value=='Y'){
     	strSQL=strSQL+" and a.endflag='Y'";
     }    
     if(fm.all('EndFlag').value!=null && fm.all('EndFlag').value=='N'){
     	strSQL=strSQL+" and (a.endflag='N' or a.endflag is null)";
     }     
	     strSQL=strSQL+" order by a.AgentCom";
	         	         
 		turnPage.queryModal(strSQL,ComGrid,0,1);
	  if (ComGrid.mulLineCount == 0)
	  {
	    alert("没有查询到相应的代理机构信息！");
	    return false;
	  } 
}

function getAgentCom(cObj,cName)
{
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输管理机构！");
  	return false;
  } 
  var strsql =" 1 and branchtype=#3# and branchtype2=#01# and managecom  like #" + fm.all('ManageCom').value + "%#   " ;
  
  showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);
  

}