<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAPlurComSave.jsp
//程序功能：
//创建日期：2003-09-17 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAPlurComUI tLAPlurComUI = new LAPlurComUI();
  
  LAComSchema tLAComSchema   = new LAComSchema();
  LAComToAgentSchema tLAComToAgentSchema0=new LAComToAgentSchema();
  LAComToAgentSchema tLAComToAgentSchema1=new LAComToAgentSchema();
  LAComToAgentSet tLAComToAgentSet=new LAComToAgentSet();

  //输出参数
  CErrors mErrors = new CErrors();
  String tOperate = request.getParameter("hideOperate");
  String ChannelType = request.getParameter("ChannelType");
  tOperate = tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
    //tG.Operator = "Admin";
    //tG.ComCode  = "001";
  //session.putValue("GI",tG);
  tG=(GlobalInput)session.getValue("GI");

  //代理机构
  tLAComSchema.setAgentCom(request.getParameter("AgentCom"));
  tLAComSchema.setName(request.getParameter("Name"));
  tLAComSchema.setBusinessType(request.getParameter("BusinessType"));
  tLAComSchema.setBusinessCode(request.getParameter("BusinessCode"));
  tLAComSchema.setBusiLicenseCode(request.getParameter("BusiLicenseCode"));
  tLAComSchema.setAppAgentCom(request.getParameter("AppAgentCom"));
  tLAComSchema.setLicenseNo(request.getParameter("LicenseNo"));
  tLAComSchema.setGrpNature(request.getParameter("GrpNature"));
  tLAComSchema.setLicenseStartDate(request.getParameter("LicenseStartDate"));
  tLAComSchema.setLicenseEndDate(request.getParameter("LicenseEndDate"));     
  tLAComSchema.setCorporation(request.getParameter("Corporation"));
  tLAComSchema.setLinkMan(request.getParameter("LinkMan"));
  tLAComSchema.setAddress(request.getParameter("Address"));
  tLAComSchema.setPhone(request.getParameter("Phone"));
  tLAComSchema.setRegionalismCode(request.getParameter("RegionalismCode"));
  tLAComSchema.setEMail(request.getParameter("EMail"));
  tLAComSchema.setChiefBusiness(request.getParameter("ChiefBusiness"));
  tLAComSchema.setNoti(request.getParameter("Noti"));  
  tLAComSchema.setUpAgentCom(request.getParameter("UpAgentCom"));
  tLAComSchema.setManageCom(request.getParameter("ManageCom"));
  tLAComSchema.setACType(request.getParameter("ACType"));
  tLAComSchema.setSellFlag(request.getParameter("SellFlag"));
  tLAComSchema.setOperator(tG.Operator);
  tLAComSchema.setAreaType(" ");
  tLAComSchema.setChannelType(request.getParameter("ChannelType"));
  
  //与机构关系
  tLAComToAgentSchema0.setAgentCom(request.getParameter("AgentCom"));
  tLAComToAgentSchema0.setRelaType("0");//与机构关联
  tLAComToAgentSchema0.setAgentGroup(request.getParameter("AgentGroup"));
  tLAComToAgentSchema0.setAgentCode("   ");
  tLAComToAgentSchema0.setOperator(tG.Operator);
  tLAComToAgentSet.add(tLAComToAgentSchema0);
  
  //与代理人关联
  tLAComToAgentSchema1.setAgentCom(request.getParameter("AgentCom"));
  tLAComToAgentSchema1.setRelaType("1");//与代理人关联
  tLAComToAgentSchema1.setAgentCode(request.getParameter("AgentCode"));
  tLAComToAgentSchema1.setAgentGroup("   ");
  tLAComToAgentSchema1.setOperator(tG.Operator);
  tLAComToAgentSet.add(tLAComToAgentSchema1);
  
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tLAComSchema);
  tVData.add(tLAComToAgentSet);
  tVData.add(tG);

  try
  {
    if(!tLAPlurComUI.submitData(tVData,tOperate))
    {
          // @@错误处理
      mErrors.copyAllErrors(tLAPlurComUI.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAPlurComSave";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      mErrors.addOneError(tError) ;
    }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    mErrors = tLAPlurComUI.mErrors;
    if (!mErrors.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Succ";
    }
    else                                                                           
    {
      Content = " 保存失败，原因是:" + mErrors.getFirstError();
      FlagStr = "Fail";
    }
  }
    //添加各种预处理

%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

