<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
  LASocialBranchGroupBL tLASocialBranchGroupBL = new LASocialBranchGroupBL();
  
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String cAgentCode = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //add by lyc 统一工号 2014-11-27
  if(!request.getParameter("BranchManager").equals("")){
  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("BranchManager")+"' fetch first row only";
   cAgentCode = new ExeSQL().getOneValue(cSql);
  }
  //隐藏域信息
  tLABranchGroupSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLABranchGroupSchema.setBranchType(request.getParameter("BranchType"));
  tLABranchGroupSchema.setBranchType2(request.getParameter("BranchType2"));
  
  // 表单信息
  tLABranchGroupSchema.setBranchAttr(request.getParameter("BranchAttr"));  
  tLABranchGroupSchema.setName(request.getParameter("BranchName"));
  tLABranchGroupSchema.setManageCom(request.getParameter("ManageCom"));  
  tLABranchGroupSchema.setBranchLevel(request.getParameter("BranchLevel"));
  tLABranchGroupSchema.setBranchManager(cAgentCode);
  tLABranchGroupSchema.setBranchManagerName(request.getParameter("BranchManagerName"));
  tLABranchGroupSchema.setBranchAddress(request.getParameter("BranchAddress"));
  tLABranchGroupSchema.setBranchZipcode(request.getParameter("BranchZipcode"));
  tLABranchGroupSchema.setBranchPhone(request.getParameter("BranchPhone"));
  tLABranchGroupSchema.setFoundDate(request.getParameter("FoundDate"));
  tLABranchGroupSchema.setAStartDate(request.getParameter("FoundDate"));
  tLABranchGroupSchema.setBranchFax(request.getParameter("BranchFax"));
  tLABranchGroupSchema.setEndFlag(request.getParameter("EndFlag"));
  tLABranchGroupSchema.setEndDate(request.getParameter("EndDate"));
  tLABranchGroupSchema.setComStyle(request.getParameter("ComStyle"));
  tLABranchGroupSchema.setState(request.getParameter("State"));
  tLABranchGroupSchema.setCostCenter(request.getParameter("CostCenter"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLABranchGroupSchema);
  tVData.add(tG);
  try
  {
	  tLASocialBranchGroupBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLASocialBranchGroupBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>