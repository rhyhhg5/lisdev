<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAGroupSplitSave.jsp
//程序功能：
//创建日期：2005-7-30 17:06
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  LAGroupSplitUI tLAGroupSplitUI = new LAGroupSplitUI();
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchemaAim = new LABranchGroupSchema();
  LABranchGroupSchema tLABranchGroupSchemaNew = new LABranchGroupSchema();
  LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
  LATreeSet tLATreeSet = new LATreeSet();
  //输出参数
  CErrors tError = null;

  String tOperate="";

  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String cAgentCode = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //目标团队代码
  tLABranchGroupSchemaAim.setBranchAttr(request.getParameter("BranchAttr"));
  //分离的新机构信息

  tLABranchGroupSchemaNew.setName(request.getParameter("BranchNameNew"));
  tLABranchGroupSchemaNew.setManageCom(request.getParameter("ManageComNew"));
  tLABranchGroupSchemaNew.setUpBranch("");
  tLABranchGroupSchemaNew.setBranchAttr(request.getParameter("BranchAttrNew"));
  tLABranchGroupSchemaNew.setBranchType(request.getParameter("BranchType"));
  tLABranchGroupSchemaNew.setBranchType2(request.getParameter("BranchType2"));
  tLABranchGroupSchemaNew.setBranchLevel(request.getParameter("BranchLevelNew"));
  tLABranchGroupSchemaNew.setBranchAddressCode("");
  tLABranchGroupSchemaNew.setBranchAddress(request.getParameter("BranchAddressNew"));
  tLABranchGroupSchemaNew.setBranchPhone(request.getParameter("BranchPhoneNew"));
  tLABranchGroupSchemaNew.setBranchFax("");
  tLABranchGroupSchemaNew.setBranchZipcode(request.getParameter("BranchZipcodeNew"));
  tLABranchGroupSchemaNew.setFoundDate(request.getParameter("FoundDateNew"));
  tLABranchGroupSchemaNew.setAStartDate(request.getParameter("FoundDateNew"));
  tLABranchGroupSchemaNew.setEndDate("");
  tLABranchGroupSchemaNew.setEndFlag("N");
  
  //机构信息
  tLABranchGroupSet.add(tLABranchGroupSchemaAim);
  tLABranchGroupSet.add(tLABranchGroupSchemaNew);
  
  //被调动的人员信息
  int lineCount = 0;
  String tChk[] = request.getParameterValues("InpAgentGridChk"); 
  String tAgentCode[] = request.getParameterValues("AgentGrid1");
  String tAgentGrade[] = request.getParameterValues("AgentGrid4");
  
  lineCount = tChk.length; //行数
  System.out.println("length= "+String.valueOf(lineCount));
  LATreeSchema tLATreeSchema;
  for(int i=0;i<lineCount;i++)
  {
	  //add by lyc 统一工号 2014-11-27
	  if(""!= tAgentCode[i]){
	  String cSql = "select agentcode from laagent where groupagentcode = '"+tAgentCode[i]+"' fetch first row only";
	   cAgentCode = new ExeSQL().getOneValue(cSql);
	  }
    if(tChk[i].trim().equals("1"))
    {
      tLATreeSchema = new LATreeSchema();
      tLATreeSchema.setAgentCode(cAgentCode);//代理人代码
      tLATreeSchema.setAgentGrade(tAgentGrade[i]);//代理人职级
      tLATreeSchema.setBranchType(request.getParameter("BranchType"));
      tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
      tLATreeSchema.setAstartDate(request.getParameter("FoundDateNew"));
      tLATreeSchema.setManageCom(request.getParameter("ManageComm"));
      tLATreeSet.add(tLATreeSchema);
      System.out.println("Agentcode:"+tAgentCode[i]);
    }
    System.out.println("i:"+tChk[i]);
  }
  System.out.println("end 个人信息..."+tLATreeSet.size());


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.add(tLABranchGroupSet);
	tVData.add(tLATreeSet);
	System.out.println("数据准备完毕，开始后台操作.");
  try
  {
    tLAGroupSplitUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAGroupSplitUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>