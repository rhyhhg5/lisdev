<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AdjustAgentNewInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
   
%>
<%
  String CurrentDate= PubFun.getCurrentDate(); 
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<%
  String tTitleAgent="";

  if("1".equals(BranchType))
  {
    tTitleAgent = "销售人员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="AdjustAgentNewInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AdjustAgentNewInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./AdjustAgentNewSave.jsp" method=post name=fm target="fraSubmit">  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent);">
    <td class=titleImg>
      调入单位信息
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent" style= "display: ''">      
    <table  class= common>
       <TR  class= common> 
         
          <TD  class= title>
            销售单位代码
          </TD>          
          <TD  class= input>
            <Input class=common name=BranchCode elementtype=nacessary>
          </TD>          
          <TD  class= common>
            <Input type=button class= cssButton value="确认" onclick="BranchConfirm();">
          </TD>  
                                       
       </TR> 
      </table>
      <table class=common>
       <TR  class= common>
          <TD  class= title>
            销售单位名称
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=BranchName >
          </TD>
        
        </TR>
       <TR  class= common>
          <TD  class= title>
            机构管理人员代码
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=BranchManager >
          </TD>
          <TD  class= title>
            机构管理人名称
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=ManagerName >
          </TD>
        </TR>
    </table>  
    </div>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent2);">
    <td class=titleImg>
      查询调动人员信息
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent2" style= "display: ''">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
           销售单位代码 
          </TD>
          <TD  class= input>
            <Input class=common name=AgentGroup verify="销售单位代码|NotNull" elementtype=nacessary>
          </TD>
          <TD  class= title>
           <%=tTitleAgent%>代码  
          </TD>
          <TD  class= input>
            <Input class=common name=AgentCode  verify="人员代码 |NotNull" elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>         
           <TD class= title>
          <%=tTitleAgent%>职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="codeno"  
           ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          ><Input name=AgentGradeName class="codename"  verify="人员职级|NotNull" readOnly elementtype=nacessary>  
        </TD>
        </TR>
   
      <TR  class= common>
          <TD  class= title>
           调动生效日期
          </TD>
          <TD  class= input>
            <Input class='common' name=AdjustDate dateFormat='short'  readonly = true>
          </TD>
         
          <TD class= common>
         
          <TD>
        </TR>
    </table>
     <Input type=button class= cssButton name=queryb value="查询" onclick="queryAgent();">
          <input type=button class=cssButton value='重置' onclick="clearGrid();">
     <p> <font color="#ff0000">注：调动生效日期可以为以下两种情况：</font></p>
     <p> <font color="#ff0000">1、人员已参与过薪资计算，则系统默认调动生效日期为已算薪资月下个月1号。同时需保证此人名下在调动生效日期至操作日无任何收费保单，如果存在这样的保单则无法进行调动。请通过保单归属功能将保单归属给原团队人员。</font></p>
     <p> <font color="#ff0000">2、如人员未参与过薪资计算，则系统默认调动生效日期为期入司时间。同时保证此人名下在调动生效日期至操作日无任何收费保单，如果存在这样的保单则无法进行调动。请通过保单归属功能将保单归属给原团队人员。</font></p>
        </div>
 <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 业务员结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
  	</div>
  	 <tr>
	    	<td>
	    	<input type=button class=cssButton name=saveb value='保存' id = saveb onclick="return submitSave()">
	       	
	    	<input type=button class=cssButton value='重置' onclick="clearAll();">
	    </td>
  	<p></p>
  	 <table>
    	<tr>
        	<td class=common>
		      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			 人员保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage1.lastPage();"> 						
  	</div>
	   
	    </tr>
    </table>
   
    <input type=hidden name=hideAgentCode value=''>  
    <input type=hidden name=BranchType2 value=''>
    <Input  type=hidden name=BranchType value='' >
    <input type="hidden" class=input name=CurrentDate value="<%=CurrentDate%>">
    <input type=hidden name=hideBranchLevel value=''>
    <input type=hidden name=hideUpBranch value=''>
    <input type=hidden name=hideAgentGroup value=''>
    <input type=hidden name=Flag value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
