//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var arrDataSet;
var select="1";//机构
var AgentGroup=""; 
var turnPage = new turnPageClass();
var mOperate="";
var sql="";
var sqlField="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//
//提交，保存按钮对应操作
function submitForm()
{
//  if (!beforeSubmit())
//    return false;    
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value = mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
//  showSubmitFrame(mDebug);  
  fm.submit(); //提交  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	if (fm.all('RelaType').value=="0")
		{
			fm.all('AgentCode').disabled = true ;	
		}
		if (fm.all('RelaType').value=="1")
		{
			fm.all('BranchAttr').disabled = true ;
		}     
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{  
  if (!verifyInput()) 
    return false;  
  return true;
}       
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
} 
   
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	fm.all('AgentCode').disabled = false ;	
	fm.all('BranchAttr').disabled = false ;	
  //下面增加相应的代码  
  if ((fm.all('Agentcom').value==null)||(fm.all('Agentcom').value==''))
    alert("请查询出要修改的记录！");
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}   
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码  
  showInfo=window.open("./LAChangeQuery.html");
}  
      

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	
	var arrResult = new Array();	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('AgentCom').value = arrResult[0][0]; 
		fm.all('StartDate').value = arrResult[0][2];  
		fm.all('StartDateB').value = arrResult[0][2];  
		fm.all('AgentCode').value = arrResult[0][3];  
		fm.all('AgentCodeB').value = arrResult[0][3]; 
		fm.all('BranchAttr').value = arrResult[0][4]; 
		fm.all('RelaType').value = arrResult[0][5];  
		fm.all('AgentGroup').value = arrResult[0][6];
		fm.all('AgentGroupB').value = arrResult[0][6];
		fm.all('MakeDate').value = arrResult[0][7];
		fm.all('MakeTime').value = arrResult[0][8];
		if (arrResult[0][5]=="0")
		{
			fm.all('AgentCode').disabled = true ;	
		}
		if (arrResult[0][5]=="1")
		{
			fm.all('BranchAttr').disabled = true ;
		}                          
	}       
}               

//检验AgentCode操作
function checkAgentCodeValid()
{ 
  var strSQL = "";
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select * from LAAgent where branchtype=3 "
	     + getWherePart('AgentCode');
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    fm.all('AgentName').value="";
    fm.all('ChannelName').value="";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) 
  {    
    alert("无此代理人！");
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    fm.all('AgentName').value="";
    fm.all('ChannelName').value="";
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = chooseArray(arrDataSet,[0,1,43,44,61,5,76]);
  //alert(tArr);
  if(tArr[0][2]!="01"){
    alert("代理人类型不符！");
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('AgentState').value = "";
    fm.all('DevGrade').value   = "";
    fm.all('AgentName').value="";
    fm.all('ChannelName').value="";
    return false;
  }
 if(tArr[0][4]!="01"&&tArr[0][4]!="02")
 {
   alert("代理人已被解聘！");
   fm.all('AgentCode').value  = "";
   fm.all('AgentGroup').value = "";
   fm.all('AgentState').value = "";
   fm.all('DevGrade').value   = "";
   fm.all('AgentName').value="";
   fm.all('ChannelName').value="";
   return false;
 }
  //<rem>######//
  //fm.all('AgentGroup').value = tArr[0][1];
  //<rem>######//
  fm.all('AgentState').value = tArr[0][4];
  fm.all('DevGrade').value   = tArr[0][3];
  fm.all('AgentName').value=tArr[0][5];
  fm.all('ChannelName').value=tArr[0][6];
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][1];
  fm.all('HiddenAgentGroup').value = tArr[0][1];
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"
  var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = chooseArray(arrDataSet_AgentGroup,[0,1,2]);
  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  //</addcode>############################################################//
}