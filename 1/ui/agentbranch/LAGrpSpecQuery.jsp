<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAGrpSpecQuery.jsp
//程序功能：
//创建日期：2004-03-25
//创建人  ：zy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAGrpSpecQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAGrpSpecQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>专业代理机构信息</title>
</head>
<body  onload="initForm();" >
  <form  method=post name=fm target="fraSubmit">
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLASpecCom1);"></IMG></td>
        <td class=titleImg> 查询条件 </td>
      </tr>
    </table>
    <Div  id= "divLASpecCom1" style= "display: ''">
    <table  class= common>
      <tr  class= common>
        <td  class= title> 代理机构编码 </td>
        <td  class= input> <input class=common name=AgentCom> </td>
        <td  class= title> 代理机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
      </tr>

      <tr  class= common>
        <td  class= title> 代理机构地址 </td>
        <td  class= input> <input  class= common name=Address> </td>
        <td  class= title> 代理单位负责人 </td>
        <td  class= input> <input class=common name=Corporation> </td>
      </tr>
      <tr class = common >
         <td  class= title> 代理机构电话 </td>
         <td  class= input> <input  class=common name=Phone> </td>
         <td  class= title> 负责管理机构 </td>
         <td  class= input> <input class='code' name=ManageCom ondblclick="return showCodeList('comcode',[this]);"> </td>
      </tr>
      <tr  class= common>
        <td  class= title> 上级代理机构 </td>
        <td  class= input> <input class=common name=UpAgentCom > </td>

      </tr>
    </table>
    </Div>
    <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
    <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divComGrid);"></td>
    	<td class= titleImg> 查询结果 </td>
      </tr>
    </table>
    <Div  id= "divComGrid" style= "display: ''">
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  	    <span id="spanComGrid" >
  	    </span>
  	  </td>
  	</tr>
      </table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
