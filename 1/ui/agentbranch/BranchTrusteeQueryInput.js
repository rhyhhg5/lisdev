/** 
 * 程序名称：
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-07-18 18:07:04
 * 创建人  ：
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var arrDataSet; 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   
    //执行下一步操作
  }
}



// 查询按钮
function easyQueryClick() {
	
		// 初始化表格
	  initBranchTrusteeGrid()
			    
 	var strSql = "select  a.managecom,d.branchattr,d.name,d.branchmanager,d.branchmanagername,b.idx,a.agentcode,c.name, "
 	+" a.startdate,a.enddate,a.VacDays,d.agentgroup,a.Operator,a.idxno "
 	+" from labranchtrustee a,lahols b,laagent c,labranchgroup d,latree f where a.agentcode=c.agentcode "
 	            +" and a.branchmanager=b.agentcode and a.idx=b.idx and a.agentgroup=d.agentgroup  and a.agentcode=f.agentcode "
    +  getWherePart("a.ManageCom", "ManageCom")
    + getWherePart("d.BranchAttr", "BranchAttr")
    + getWherePart("a.BranchManager", "BranchManager")
    + getWherePart("d.BranchManagerName", "BranchManagerName")
 //   + getWherePart("b.Idx", "Idx",null,1)
    + getWherePart("a.AgentCode","AgentCode")
      + getWherePart("d.Name", "AgentName")
    + getWherePart("f.AgentGrade","AgentGrade")
    + getWherePart("a.StartDate","StartDate");
      + getWherePart("a.EndDate", "EndDate")
    + getWherePart("a.VacDays","TimeDistance")
    + getWherePart("BranchType","BranchType")
    + getWherePart("BranchType2","BranchType2")
       ;
  //   alert(strSql);	 
	turnPage.queryModal(strSql, BranchTrusteeGrid); 
	 arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
}
function showOne(parm1, parm2) {	

}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = BranchTrusteeGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				// alert(1);
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	
	tRow = BranchTrusteeGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	//'"+HolsGrid.getRowColData(tRow-1,1)+"'
	if( tRow == 0 || tRow == null )
	return arrSelected;
	
	arrSelected = new Array();
	//edit by guo xiang at 2004-9-13 17:54
	 arrSelected[0] = arrDataSet[tRow-1];
     
        return arrSelected;
	
}
