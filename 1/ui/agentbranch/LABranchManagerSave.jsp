<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchManagerSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();

  ALABranchManagerUI tLABranchManager = new ALABranchManagerUI();
  ALAGrpBranchManagerUI tALAGrpBranchManagerUI=new ALAGrpBranchManagerUI();
  ALABankAgentBranchManagerUI tALABankAgentBranchManagerUI=new ALABankAgentBranchManagerUI();
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String tAgentGrade = request.getParameter("AgentGrade");
  String tPartTime = request.getParameter("PartTime");

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");
  String tBranchManager = "";
  //add by lyc 统一工号 2014-11-27
  if(""!= request.getParameter("BranchManager")){
  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("BranchManager")+"' fetch first row only";
  tBranchManager = new ExeSQL().getOneValue(cSql);
  }
  String tAdjustDate=request.getParameter("AdjustDate");
  String tBranchType2=request.getParameter("BranchType2");
  String tBranchType=request.getParameter("BranchType");
    tLABranchGroupSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLABranchGroupSchema.setName(request.getParameter("Name"));
    tLABranchGroupSchema.setManageCom(request.getParameter("ManageCom"));
    tLABranchGroupSchema.setUpBranch(request.getParameter("UpBranch"));
    tLABranchGroupSchema.setBranchAttr(request.getParameter("BranchAttr"));
    tLABranchGroupSchema.setBranchType(request.getParameter("BranchType"));
    tLABranchGroupSchema.setBranchType2(request.getParameter("BranchType2"));
    tLABranchGroupSchema.setBranchLevel(request.getParameter("BranchLevel"));
    tLABranchGroupSchema.setBranchManager(tBranchManager);//任命主管编码
    tLABranchGroupSchema.setBranchManagerName(request.getParameter("BranchManagerName"));//主管姓名
    tLABranchGroupSchema.setBranchAddressCode(request.getParameter("BranchAddressCode"));
    tLABranchGroupSchema.setBranchAddress(request.getParameter("BranchAddress"));
    tLABranchGroupSchema.setBranchPhone(request.getParameter("BranchPhone"));
    tLABranchGroupSchema.setBranchFax(request.getParameter("BranchFax"));
    tLABranchGroupSchema.setBranchZipcode(request.getParameter("BranchZipcode"));
    tLABranchGroupSchema.setFoundDate(request.getParameter("FoundDate"));
    tLABranchGroupSchema.setEndDate(request.getParameter("EndDate"));
    tLABranchGroupSchema.setEndFlag(request.getParameter("EndFlag"));
    tLABranchGroupSchema.setFieldFlag(request.getParameter("FieldFlag"));
    tLABranchGroupSchema.setState(request.getParameter("State"));
    tLABranchGroupSchema.setUpBranchAttr(request.getParameter("UpBranchAttr"));
    tLABranchGroupSchema.setModifyDate(request.getParameter("AdjustDate"));//存储任命日期

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.addElement(tLABranchGroupSchema);
	tVData.add(tPartTime);
  tVData.add(tAdjustDate);//任命日期
  tVData.add(tBranchManager);//任命主管
  try
  {
   	if( "1".equals(tBranchType) && "01".equals(tBranchType2))
   	{
     	tLABranchManager.submitData(tVData,tOperate);
   	}
   	else if ("3".equals(tBranchType) && "01".equals(tBranchType2))
   	{ //这里进行销售银代的主管任命处理
   		tALABankAgentBranchManagerUI.submitData(tVData,tOperate);
  	}
  	else
   	{
    	tALAGrpBranchManagerUI.submitData(tVData,tOperate);
   	}
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
   if( "1".equals(tBranchType) && "01".equals(tBranchType2))
   {
    tError = tLABranchManager.mErrors;
   }
   else if("3".equals(tBranchType) && "01".equals(tBranchType2))
   {
   	tError = tALABankAgentBranchManagerUI.mErrors;
   }
 	 else
   {
    tError = tALAGrpBranchManagerUI.mErrors;
   }
   System.out.println(tError.getErrorCount()+"||||");
   System.out.println(tError);
   
   if (!tError.needDealError())
   {
   	Content = " 保存成功! ";
   	FlagStr = "Succ";
   }
   else
   {
   	Content = " 保存失败，原因是:" + tError.getFirstError();
   	FlagStr = "Fail";
   }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

