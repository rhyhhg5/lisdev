<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AdjustAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
  LATreeSchema mManagerTreeSchema = new LATreeSchema();
  LATreeSet mLATreeSet = new LATreeSet();
  AdjustAgentUI mAdjustAgentUI = new AdjustAgentUI();
  AdjustGrpAgentUI mAdjustGrpAgentUI = new AdjustGrpAgentUI();

  //输出参数
  CErrors tError = null;
  String tOperate="";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin AdjustBranchGroup schema..222222222222222222222222222222222222222222222222222222.");
  String OriginManager = request.getParameter("hideAgentCode");
  String tBranchType=request.getParameter("BranchType");
  String tBranchType2=request.getParameter("BranchType2");
  if(request.getParameter("BranchType").equals("2")) tOperate="";//法人组没有管理人员
  //目标机构
  mLABranchSchema.setAgentGroup(request.getParameter("hideAgentGroup"));
  mLABranchSchema.setBranchAttr(request.getParameter("BranchCode"));
  mLABranchSchema.setBranchLevel(request.getParameter("hideBranchLevel"));
  mLABranchSchema.setBranchManager(request.getParameter("hideBranchManager"));
  mLABranchSchema.setUpBranch(request.getParameter("hideUpBranch"));
  mLABranchSchema.setBranchType(request.getParameter("BranchType"));
  mLABranchSchema.setBranchType2(request.getParameter("BranchType2"));
  System.out.println("Manager:"+mLABranchSchema.getBranchManager());

  
  //取得调动人员信息
  System.out.println("begin to AdjustAgent set...");  
  
  int lineCount = 0;
  String tChk[] = request.getParameterValues("InpAgentGridChk"); 
  String tAgentCode[] = request.getParameterValues("AgentGrid5");//使用内部工号
  String tAgentGrade[] = request.getParameterValues("AgentGrid4");
  
  lineCount = tChk.length; //行数
  System.out.println("length= "+String.valueOf(lineCount));
  LATreeSchema tLATreeSchema;
  for(int i=0;i<lineCount;i++)
  {
    if(tChk[i].trim().equals("1"))
    {
      tLATreeSchema = new LATreeSchema();
      tLATreeSchema.setAgentCode(tAgentCode[i]);//代理人代码
      tLATreeSchema.setAgentGrade(tAgentGrade[i]);//代理人职级
      tLATreeSchema.setBranchType(request.getParameter("BranchType"));
      tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
      tLATreeSchema.setAstartDate(request.getParameter("AdjustDate"));
      mLATreeSet.add(tLATreeSchema);
      System.out.println("Agentcode:"+tAgentCode[i]);
    }
    System.out.println("i:"+tChk[i]);
  }
  System.out.println("end 个人信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
        tVData.add(tG);
        tVData.addElement(mLABranchSchema);
        tVData.addElement(mLATreeSet);
  System.out.println("add over");
  try
  {
  
    if(tBranchType.equals("2") && tBranchType2.equals("01"))
    {
      mAdjustGrpAgentUI.submitData(tVData,"INSERT||MAIN");
    }
    else
    {
      mAdjustAgentUI.submitData(tVData,"INSERT||MAIN");
    }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    if(tBranchType.equals("2") && tBranchType2.equals("01"))
    {
    tError = mAdjustGrpAgentUI.mErrors;
    }
    else
    {
    tError = mAdjustAgentUI.mErrors;
    }
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

