<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LABranchGroupInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql='1 and branchtype=#'+<%=BranchType%>+'# and branchtype2=#'+'<%=BranchType2%>'+'# ';
   var strsql="1 and codealias= #"+'2'+"#";
</script>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="./LAMediBranchGroupInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="./LAMediBranchGroupInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAMediBranchGroupSave.jsp" method=post name=fm target="fraSubmit" >
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      中介渠道单位
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
        
         <TD class= title>
            中介团队代码
          </TD>
          <TD  class= input>
          	<Input class= 'readonly' readonly  name=BranchAttr  verify="中介团队代码|NOTNULL&len=10" maxlength=10>
          <!--
            <Input  class=common name=BranchAttr elementtype=nacessary verify="中介团队代码|NOTNULL&len=8" maxlength=8>
          -->
          </TD>
          <TD  class= title>
            中介团队名称
          </TD>
          <TD  class= input>
            <Input class= common name=BranchName elementtype=nacessary verify="中介团队名称|NOTNULL">
          </TD>
	 
        </TR>
        <TR  class= common>
          <TD  class= title>
            所属管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>=4" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            onchange="return getBranchCodeInit();"><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
          </TD> 
          <TD  class= title>中介团队类型</TD><TD  class= input>
          	<Input class="codeno"  CodeData="0|2^0|内勤^1|外勤" name=InsideFlag  verify="中介专员类型|notnull" ondblclick="return showCodeListEx('InsideFlag',[this,InsideFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('InsideFlag',[this,InsideFlagName],[0,1]);"><Input class=codename name=InsideFlagName readOnly elementtype=nacessary></TD>      
          <!--TD  class= title>
            工作属性
          </TD>
          <TD  class= input>
          </TD>
	        <TD  class= title>
            上一级团队代码
          </TD>
          <TD  class= input>
          </TD-->
            <Input type=hidden name=UpBranchAttr >
            <Input type=hidden name=FieldFlag value=''>
        </TR>
        <TR  class= common>
	  <TD  class= title>
            团队主管代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=BranchManager >
          </TD>
          <TD  class= title>
            团队主管姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=BranchManagerName >
          </TD>
        </TR>
        <TR  class= common>
	      <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddress >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcode verify="展业机构邮编|len<=6" >
          </TD>        
        </TR>
        <TR  class= common>
	      <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhone >
          </TD>
          
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' verify="成立日期|NOTNULL&Date" name=FoundDate format='short' elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='codeno' name=EndFlag verify="停业|code:yesno&NOTNULL" 
             ondblclick="return showCodeList('yesno',[this,EndFlagName],[0,1]);" 
             onkeyup="return showCodeListKey('yesno',[this,EndFlagName],[0,1]);"
             ><Input class=codename name=EndFlagName readOnly elementtype=nacessary>
          </TD>
	        <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate verify="停业日期|Date" format='short' onfocusout="return changeGroup();">
          </TD>
        </TR>       

        <tr class=common>
          <TD class = title>
             渠道类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);"
             onkeyup="return showCodeListKey('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);" 
              verify="渠道类型|notnull&code:BranchType2" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
          <TD  class= title>
            成本中心编码
          </TD>
          <TD  class= input>
            <Input class= common name=CostCenter verify="成本中心编码|notnull&len=10"  elementtype=nacessary>
          </TD>
          
        </tr>
        <tr class=common>
	     
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Operator >
          </TD>
        </tr>
      </table>
    </Div>
   
    <Input type=hidden name=Operator_name >
    <Input type=hidden name=hideOperate value=''>
    <Input type=hidden name=tCostCenter value=''>
    <Input type=hidden name=BranchType>
     <!-- <Input type=hidden name=BranchType2> -->
    <Input type=hidden name=AgentGroup value='' >  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
    <Input type=hidden name=UpBranch value='' >  <!--上级机构代码，存储隐式机构代码 -->
    <Input type=hidden class='common' name=ManageComCode value='<%=tG.ManageCom%>'>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

