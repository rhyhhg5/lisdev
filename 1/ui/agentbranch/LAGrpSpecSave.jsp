<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAGrpSpecSave.jsp
//程序功能：
//创建日期：2004-03-25
//创建人  ：zy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数
LAGrpSpecComUI tLAGrpSpecComUI = new LAGrpSpecComUI();
LAComSchema tLAComSchema   = new LAComSchema();

//输出参数
CErrors mErrors = new CErrors();
String tOperate = request.getParameter("hideOperate");
String ChannelType = request.getParameter("ChannelType");
tOperate = tOperate.trim();
String FlagStr = "Fail";
String Content = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//代理机构
tLAComSchema.setAgentCom(request.getParameter("AgentCom"));
tLAComSchema.setName(request.getParameter("Name"));
tLAComSchema.setBankType(request.getParameter("BankType"));
tLAComSchema.setAddress(request.getParameter("Address"));
tLAComSchema.setCorporation(request.getParameter("Corporation"));
tLAComSchema.setPhone(request.getParameter("Phone"));
tLAComSchema.setGrpNature(request.getParameter("GrpNature"));
tLAComSchema.setUpAgentCom(request.getParameter("UpAgentCom"));
tLAComSchema.setManageCom(request.getParameter("ManageCom"));
tLAComSchema.setACType(request.getParameter("ACType"));
tLAComSchema.setChannelType(request.getParameter("ChannelType"));
tLAComSchema.setAreaType(request.getParameter("AreaType"));
tLAComSchema.setBranchType("2");
tLAComSchema.setOperator(tG.Operator);


// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";
tVData.add(tLAComSchema);
tVData.add(tG);

try{
	if(!tLAGrpSpecComUI.submitData(tVData,tOperate)){
		// @@错误处理
		mErrors.copyAllErrors(tLAGrpSpecComUI.mErrors);
		CError tError = new CError();
		tError.moduleName = "LAGrpSpecComSave";
		tError.functionName = "submitData";
		tError.errorMessage = "数据提交失败!";
		mErrors.addOneError(tError) ;
	}
}
catch(Exception ex){
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (!FlagStr.equals("Fail")){
	mErrors = tLAGrpSpecComUI.mErrors;
	if (!mErrors.needDealError()){
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else{
		Content = " 保存失败，原因是:" + mErrors.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>