<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BankChangeComSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();
  LAComToAgentSchema mNewLAComToAgentSchema = new LAComToAgentSchema();
  BankChangeComUI mBankChangeComUI  = new BankChangeComUI();

  //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||INSERT";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin BankChangeComUI schema...");
  String mAdjustDate=request.getParameter("AdjustDate");
  System.out.println("mAdjustDate:"+mAdjustDate);
  //取得被调整单位信息
  mLAComToAgentSchema.setAgentCom(request.getParameter("AgentOldCom"));
  mLAComToAgentSchema.setAgentCode(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("OldAgentCode")+"'"));
  mLAComToAgentSchema.setAgentGroup(request.getParameter("OldAgentGroup"));
  mLAComToAgentSchema.setEndDate(request.getParameter("EndDate"));
  mLAComToAgentSchema.setMakeDate(request.getParameter("MakeDate"));
  mLAComToAgentSchema.setMakeTime(request.getParameter("MakeTime"));
  mLAComToAgentSchema.setModifyDate(request.getParameter("ModifyDate"));
  mLAComToAgentSchema.setModifyTime(request.getParameter("ModifyTime"));
  mLAComToAgentSchema.setOperator(request.getParameter("OldOperator"));
  mLAComToAgentSchema.setRelaType(request.getParameter("RelaType"));
  mLAComToAgentSchema.setStartDate(request.getParameter("StartDate"));

  System.out.println("AgentOldCom:"+request.getParameter("AgentOldCom"));
  System.out.println("OldAgentCode:"+request.getParameter("OldAgentCode"));
  System.out.println("原机构停业日期:"+mLAComToAgentSchema.getEndDate());
  //取得目标单位信息
  System.out.println("begin to changebankagentcom set...");
  mNewLAComToAgentSchema.setAgentCom(request.getParameter("AgentOldCom"));
  mNewLAComToAgentSchema.setAgentCode(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("NewAgentCode")+"'"));     //调整后的上级机构内部编码
  mNewLAComToAgentSchema.setAgentGroup(request.getParameter("NewAgentGroup"));//调整后的上级机构级别
  mNewLAComToAgentSchema.setStartDate(request.getParameter("AdjustDate")); //调整后的机构名称
  System.out.println("end 单位信息...");
//  TransferData tdata = new TransferData();
//  tdata.setNameAndValue("IsSingle",request.getParameter("IsSingle"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLAComToAgentSchema);
  System.out.println(mNewLAComToAgentSchema.getAgentCode());
  tVData.addElement(mNewLAComToAgentSchema);
  System.out.println(mNewLAComToAgentSchema.getAgentCode());
  //tVData.addElement(mAdjustDate);
   //tVData.addElement(tdata);
  System.out.println("add over");
  try
  {
   mBankChangeComUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	String newAgentCode="";
  if (!FlagStr.equals("Fail"))
  {
    tError = mBankChangeComUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	newAgentCode=(String)mBankChangeComUI.getResult().getObject(0);
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=newAgentCode%>");
</script>
</html>

