<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupQueryInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql="1 and branchtype=#1# and branchtype2=#02#";
      var strsql="1 and codealias= #"+'2'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LAAMBranchGroupQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LAAMBranchGroupQueryInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
<title>中介团队</title>
</head>
<body onload="initForm();initElementtype();">
  <form action="./LABranchGroupQuerySubmit.jsp" method=post name=fm target="fraSubmit">
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
              <TR  class= common>
          <TD  class= title>   
          管理机构  
          </TD>
          <TD  class= input>  
          <Input class="codeNo"  name=ManageCom  verify="管理机构|code:comcode&NOTNULL" 
          ondblclick="return showCodeList('comcode1',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode1',[this,ManageComName],[0,1],null,null,null,1);"
          ><input class=codename name=ManageComName readonly=true ></TD>   
            <TD class = title>
             渠道类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);"
             onkeyup="return showCodeListKey('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);" 
              verify="渠道类型|notnull&code:BranchType2" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
          </TR>
        <TR  class= common>
          <TD  class= title>
            中介团队代码
          </TD>
          <TD  class= input>
            <Input class=common name=BranchAttr >
          </TD>
          <TD  class= title>
            中介团队名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
        </TR>

         <!--TD  class= input>
            <Input class= 'code' name=BranchLevel verify="展业机构级别|code:BranchLevel" ondblclick="return showCodeList('branchlevel',[this],null,null,msql,1);" 
                                   onkeyup="return showCodeListKey('branchlevel',[this],null,null,msql,1);" >
          </TD>
          </TR>
        <TR  class= common>
          <TD  class= title>
            地址编码
          </TD>
          <TD  class= input>
            <Input class= 'code' name=BranchAddressCode ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
          <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddress >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhone >
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= common name=BranchFax >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcode maxlength=6 >
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            团队主管代码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchManager >
          </TD>
        <TD  class= title>
            团队主管名称
          </TD>
          <TD  class= input>
            <Input class= common name=BranchManagerName >
          </TD>
        </TR-->
        <TR  class= common>
          <TD  class= title>
            成立标志日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' name=FoundDate format='short'>
          </TD>
          <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate format='short'>
          </TD>
         </TR>
     
      </table>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 中介团队结果
    		</td>
    	</tr>
    </table>
    <Input type=hidden name=BranchType >
    <Input type=hidden name=BranchLevel > 
    <input type=hidden name=AgentGroup value=''>  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
    <input type=hidden name=UpBranch value=''>  <!--上级机构代码，存储隐式机构代码 -->
  	<Div  id= "divAGroupGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBranchGroupGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
