 <%
//程序名称：AdjustAgentInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">                                

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox(); 
    initAgentGrid();  
    initContGrid();  
  }
  catch(re)
  {
    alert("AdjustAgentInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initInpBox()
{ 

  try
  {                                   
    fm.all('BranchCode').value = '';
    fm.all('BranchName').value = '';
    fm.all('BranchManager').value = '';
    fm.all('ManagerName').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('hideAgentCode').value = '';
    fm.all('hideAgentGroup').value = '';
    fm.all('AdjustDate').value = '';
    fm.all('BranchType').value='<%=BranchType%>';
    fm.all('BranchType2').value='<%=BranchType2%>';
  }
  catch(ex)
  {
    alert("在AdjustAgentInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 险种授权的初始化
function initAgentGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="<%=tTitleAgent%>代码" ;          		//列名
      iArray[1][1]="100px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      //iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      //iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      //iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值


      iArray[2]=new Array();
      iArray[2][0]="<%=tTitleAgent%>名称";         			//列名
      iArray[2][1]="80px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="展业机构代码";      	   		//列名
      iArray[3][1]="150px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
    
      
      iArray[4]=new Array();
      iArray[4][0]="<%=tTitleAgent%>职级";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="内部编码";      	   		//列名
      iArray[5][1]="0px";            			//列宽
      iArray[5][2]=10;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 
      //这些属性必须在loadMulLine前
      AgentGrid.mulLineCount = 0;   
      AgentGrid.displayTitle = 1;
      AgentGrid.locked=1;   
      AgentGrid.canSel=1;
      AgentGrid.canChk=0;  
      AgentGrid.hiddenPlus = 1;
      AgentGrid.hiddenSubtraction = 1;
      AgentGrid.selBoxEventFuncName = "setContValue";
      AgentGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
/************************************************************
 *initContGrid            
 *输入：          没有
 *输出：          没有
 *功能：          初始化ContGrid
 ************************************************************
 */
function initContGrid()
  {                               
    var iArray = new Array();
      
      try
      {
           
	      iArray[0]=new Array();
		    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		    iArray[0][1]="30px";            		//列宽
		    iArray[0][2]=10;            			//列最大值
		    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[1]=new Array();
		    iArray[1][0]="保单号";         		//列名
		    iArray[1][1]="80px";            		//列宽
		    iArray[1][2]=100;            			//列最大值
		    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		    
		    iArray[2]=new Array();
		    iArray[2][0]="客户号";         		//列名
		    iArray[2][1]="90px";            		//列宽
		    iArray[2][2]=100;            			//列最大值
		    iArray[2][3]=0; 
		  
		    iArray[3]=new Array();
		    iArray[3][0]="客户姓名";         		//列名
		    iArray[3][1]="100px";            		//列宽
		    iArray[3][2]=100;            			//列最大值
		    iArray[3][3]=0; 
		    
		    iArray[4]=new Array();
		    iArray[4][0]="起保日期";         		//列名
		    iArray[4][1]="80px";            		//列宽                                              
		    iArray[4][2]=100;            			//列最大值                                              
		    iArray[4][3]=0;                   
		    
		    iArray[5]=new Array();
		    iArray[5][0]="止保日期";         		//列名
		    iArray[5][1]="80px";            		//列宽
		    iArray[5][2]=100;            			//列最大值
		    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[6]=new Array();
		    iArray[6][0]="总保费";         		//列名
		    iArray[6][1]="80px";            		//列宽
		    iArray[6][2]=100;            			//列最大值
		    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		    
		    
		    iArray[7]=new Array();
		    iArray[7][0]="签单日期";         		//列名
		    iArray[7][1]="80px";            		//列宽
		    iArray[7][2]=100;            			//列最大值
		    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		    
		    iArray[8]=new Array();
		    iArray[8][0]="保单状态";         		//列名
		    iArray[8][1]="70px";            		//列宽
		    iArray[8][2]=200;            			//列最大值
		    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[9]=new Array();
		    iArray[9][0]="保单类型";         		//列名
		    iArray[9][1]="60px";            		//列宽
		    iArray[9][2]=200;            			//列最大值
		    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许		  
		   
		  
        
        ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
      
        //这些属性必须在loadMulLine前
        ContGrid.mulLineCount = 0;   
        ContGrid.displayTitle = 1;
        ContGrid.canSel=0;
        ContGrid.canChk=0;
        ContGrid.locked=1;
        ContGrid.hiddenPlus = 1;
      	ContGrid.hiddenSubtraction = 1;
      
        ContGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert("初始化ContGrid时出错："+ ex);
      }
    } 
      


</script>
