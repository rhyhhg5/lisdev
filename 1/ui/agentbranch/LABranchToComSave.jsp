<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChangeBranchSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
  LABranchGroupSchema mAimLABranchSchema = new LABranchGroupSchema();
  LABranchToComUI mLABranchToComUI  = new LABranchToComUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin AdjustBranchGroup schema...");
  String mAdjustDate=request.getParameter("AdjustDate");
  System.out.println("mAdjustDate:"+mAdjustDate);
  //取得被调整单位信息
  mLABranchSchema.setAgentGroup(request.getParameter("hideAdjustAgentGroup"));//机构内部编码
  mLABranchSchema.setBranchLevel(request.getParameter("hideLevel"));
  mLABranchSchema.setBranchAttr(request.getParameter("AdjustBranchCode")); 
  mLABranchSchema.setManageCom(request.getParameter("ManageCom"));  
  System.out.println("BranchType:"+request.getParameter("BranchType"));
  System.out.println("BranchType2:"+request.getParameter("BranchType2"));
  mLABranchSchema.setBranchType(request.getParameter("BranchType"));
  mLABranchSchema.setBranchType2(request.getParameter("BranchType2"));
  
 
  String tNewManageCom = request.getParameter("NewManageCom");
  System.out.println("end 单位信息...");
	TransferData tdata = new TransferData();
	 
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  System.out.println(mLABranchSchema.getBranchAttr());
  tVData.add(tG);
  tVData.addElement(mLABranchSchema);
  
  tVData.addElement(tNewManageCom);
  
  tVData.addElement(mAdjustDate);
  
  System.out.println("add over");
  try
  {
   mLABranchToComUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	String newBranchAttr="";
  if (!FlagStr.equals("Fail"))
  {
    tError = mLABranchToComUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	newBranchAttr=(String)mLABranchToComUI.getResult().getObject(0);
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=newBranchAttr%>");
</script>
</html>

