<%
//程序名称：LAResidentIncomeQueryInit.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
  }
  catch(ex)
  {
    alert("LAResidentIncomeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initLACommisionGrid();
  }
  catch(re)
  {
    alert("LAResidentIncomeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LACommisionGrid;
function initLACommisionGrid() {  
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         		//列名
    iArray[1][1]="20px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         		//列名
    iArray[2][1]="50px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="居民类型";         		//列名
    iArray[3][1]="20px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    iArray[4]=new Array();
    iArray[4][0]="居民收入起始日期";         		//列名
    iArray[4][1]="60px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="居民收入终止日期";         		//列名
    iArray[5][1]="60px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[6]=new Array();
    iArray[6][0]="最近年度居民人均可支配收入";         		//列名
    iArray[6][1]="60px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="最近年度居民人均纯收入";         		//列名
    iArray[7][1]="60px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="idx";         		//列名
    iArray[8][1]="0px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
           
   LACommisionGrid = new MulLineEnter( "fm" , "LACommisionGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACommisionGrid.mulLineCount = 0;   
    LACommisionGrid.displayTitle = 1;
    LACommisionGrid.hiddenPlus = 1;
    LACommisionGrid.hiddenSubtraction = 1;
    LACommisionGrid.canSel = 1;
    LACommisionGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}
</script>