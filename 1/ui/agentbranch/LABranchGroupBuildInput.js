
// 页面触发获取营业部基础信息
function getBranch()
{
	
	var tBranchAttr = fm.all('BranchAttr').value;
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2 = fm.all('BranchType2').value;
	var sql = "select name,managecom,branchlevel,branchmanager,branchmanagername,agentgroup,FoundDate,ApplyGBFlag,GbuildFlag";
	sql+=" ,(select a.groupagentcode from laagent a where a.agentcode = branchmanager)"
	sql+=" from labranchgroup where 1=1 and branchlevel = '03' and EndFlag = 'N'";
	sql+=" and branchtype = '"+tBranchType+"' ";
	sql+=" and branchtype2 = '"+tBranchType2+"' ";
	sql+=" and BranchAttr = '"+tBranchAttr+"' ";
	
	var strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
	if(strQueryResult)
	{
		var arr = decodeEasyQueryResult(strQueryResult);
		fm.all('Name').value=arr[0][0];
		fm.all('ManageCom').value=arr[0][1];
		fm.all('BranchLevel').value=arr[0][2];
		fm.all('BranchManager').value=arr[0][3];
		fm.all('BranchManagerName').value=arr[0][4];
		fm.all('AgentGroup').value=arr[0][5];
		fm.all('FoundDate').value=arr[0][6];
		var tApplyGBFlag=arr[0][7];
		var tGbuildFlag =arr[0][8];
		fm.all('GroupAgentCode').value = arr[0][9];
		if(tApplyGBFlag!=null&&tApplyGBFlag!="")
		{
			alert("该营销服务部已标记申报标志,请先通过查询操作查询结果然后返回进行修改操作！");
			initInpBox();
			return false;
		}
		if(tGbuildFlag!=null&&tGbuildFlag!="")
		{
		    alert("该营销服务部已标记团队建设标记，烦请通知IT部门进行相关问题解决！");
		    initInpBox();
			return false;	
		}
	}else{
	  fm.all('BranchAttr').value='';
	  alert("页面录入营业部编码已停业或编码有误,烦请重新录入！");
	  return false;
	}
}

// 保存操作
function submitForm()
{
	
  if(fm.all('ReturnFlag').value=='Y')
  {
     alert("查询返回的信息如想进行调整，烦请通过修改按钮进行操作！");
     return false;
  }
  //前台录入信息校验 
  if( verifyInput() == false ) return false;
  if(!beforeSubmit()) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
}
// 修改操作
function updateClick()
{
  
  if( verifyInput() == false ) return false;
  if(!beforeSubmit()) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
    var tBranchType = "1";
    var tBranchType2 = "01";
  showInfo=window.open("./LABranchGroupBuildQueryHtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2);
}    

// 查询页面，查询返回操作
function afterQuery(arrQueryResult)
{
	var arr = new Array();
	if( arrQueryResult != null )
	{
		arr = arrQueryResult;
		fm.all('BranchAttr').value=arr[0][0];
		fm.all('Name').value=arr[0][1];
		fm.all('ManageCom').value=arr[0][2];
		fm.all('BranchLevel').value=arr[0][3];
		fm.all('BranchManager').value=arr[0][4];
		fm.all('BranchManagerName').value=arr[0][5];
		fm.all('ApplyGBFlag').value=arr[0][6];
		var tApplyGBFlag=arr[0][6];
		if(tApplyGBFlag!=null&&tApplyGBFlag!="")
		{
			if(tApplyGBFlag=='Y')
			{
				fm.all('ApplyGBFlagName').value ='是';
			}else{
				fm.all('ApplyGBFlagName').value ='否';
			}
		}
		fm.all('ApplyGBStartDate').value=arr[0][7];
		fm.all('GbuildFlag').value=arr[0][8];
		var tGbuildFlag=arr[0][8];
		if(tGbuildFlag!=null&&tGbuildFlag!="")
		{
			if(tGbuildFlag=='A')
			{
				fm.all('GbuildFlagName').value ='A类';
			}else{
				fm.all('GbuildFlagName').value ='B类';
			}
		}
		fm.all('GbuildStartDate').value=arr[0][9];
		fm.all('GbuildEndDate').value=arr[0][10];
		fm.all('BranchType').value=arr[0][11];
		fm.all('BranchType2').value=arr[0][12];
		fm.all('AgentGroup').value=arr[0][13];
		fm.all('FoundDate').value=arr[0][14];
		fm.all('ReturnFlag').value='Y';
		fm.all('groupagentcode').value = arr[0][15];
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}
// 简单逻辑校验
function beforeSubmit()
{
	var tGbuildFlag = fm.all('GbuildFlag').value;
	var tGbuildStartDate =fm.all('GbuildStartDate').value;
	if(tGbuildFlag!=null&&tGbuildFlag!='')
	{
		if(tGbuildStartDate==null||tGbuildStartDate=='')
		{
		  alert("页面如果录入团队建设标记，则必须录入相应的团队建设起期！");
		  return false;
		}
	}
	if(tGbuildStartDate!=null&&tGbuildStartDate!='')
	{
		if(tGbuildFlag==null||tGbuildFlag=='')
		{
		  alert("页面如果录入团队建设起期，则必须录入相应的团队建设标记！");
		  return false;
		}
	}
	return true;
}