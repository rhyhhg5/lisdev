<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChangeBranchSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
  LABranchGroupSchema mAimLABranchSchema = new LABranchGroupSchema();
  ChangeBranchNewUI mChangeBranchNewUI  = new ChangeBranchNewUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin AdjustBranchGroup schema...");
  String mAdjustDate=request.getParameter("AdjustDate");
  System.out.println("mAdjustDate:"+mAdjustDate);
  //取得被调整单位信息
  mLABranchSchema.setAgentGroup(request.getParameter("hideAdjustAgentGroup"));
  mLABranchSchema.setBranchLevel(request.getParameter("hideLevel"));
  mLABranchSchema.setUpBranch(request.getParameter("hideUpBranch"));
  mLABranchSchema.setBranchAttr(request.getParameter("AdjustBranchCode"));//增加了修改此属性的
  mLABranchSchema.setEndDate(request.getParameter("AdjustDate"));
  System.out.println("BranchType:"+request.getParameter("BranchType"));
  System.out.println("BranchType2:"+request.getParameter("BranchType2"));
  mLABranchSchema.setBranchType(request.getParameter("BranchType"));
  mLABranchSchema.setBranchType2(request.getParameter("BranchType2"));
  
  System.out.println("原机构停业日期:"+mLABranchSchema.getEndDate());
  //取得目标单位信息
  System.out.println("begin to changeBranchGroup set..."); 
  
  mAimLABranchSchema.setAgentGroup(request.getParameter("AdjustAfterAgentGroup"));//调整后的上级机构内部编码
  mAimLABranchSchema.setBranchLevel(request.getParameter("AdjustAfterLevel"));//调整后的上级机构级别
  mAimLABranchSchema.setName(request.getParameter("AdjustAfterBranchName")); //调整后的机构名称
  mAimLABranchSchema.setBranchAttr(request.getParameter("AdjustAfterBranchCode"));
  mAimLABranchSchema.setBranchType(request.getParameter("BranchType"));
  mAimLABranchSchema.setBranchType2(request.getParameter("BranchType2"));
  System.out.println("end 单位信息...");
  
	TransferData tdata = new TransferData();
	tdata.setNameAndValue("IsSingle",request.getParameter("IsSingle"));//调动类型 0 断裂原有关系 1 带走原有关系

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLABranchSchema);
  System.out.println(mLABranchSchema.getBranchAttr());
  tVData.addElement(mAimLABranchSchema);
  System.out.println(mAimLABranchSchema.getBranchAttr());
  tVData.addElement(mAdjustDate);
  tVData.addElement(tdata);
  System.out.println("add over");
  try
  {
   mChangeBranchNewUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	String newBranchAttr="";
  if (!FlagStr.equals("Fail"))
  {
    tError = mChangeBranchNewUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	newBranchAttr=(String)mChangeBranchNewUI.getResult().getObject(0);
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=newBranchAttr%>");
</script>
</html>

