<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAECComQueryInput.jsp
//程序功能：
//创建日期：2017-11-27
//创建人  ：王清民
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType+"  BranchType2"+BranchType2);
  
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
	<script> 
   var msql='1 and code <> #01# ';
   var strsql="1 and codealias= #"+'2'+"#";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./LAECComQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAECComQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAECComQuerySubmit.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLASpecCom1);"></IMG></td>
        <td class=titleImg> 查询条件 </td>
      </tr>
    </table> 
  
  <Div  id= "divLASpecCom1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 中介机构编码 </td>
        <td  class= input> <input class=common name=AgentCom> </td>
        <td  class= title> 中介机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
      </tr>
      
      <tr  class= common> 
        <TD  class= title>
            所属管理机构
          </TD>
<TD  class= input>  <Input class="codeNo"  name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>             </TD> 
        <td  class= title> 中介机构类型 </td>  
        <td  class= code> <input class="codeno" name="ACType" verify="中介机构类型|NOTNULL&code:ACType"
        	   ondblclick="return showCodeList('ACType',[this,ACTypeName],[0,1],null,msql,1);"
        	   onkeyup="return showCodeListKey('ACType',[this,ACTypeName],[0,1],null,msql,1);"
        	   ><Input class=codename name=ACTypeName readOnly >
        </td>
      </tr>
      
      <tr class = common >
       <td  class= title> 注册资金 </td>
        <td  class= input> <input  class= common name=Asset> 单位:万元 </td>
         <td  class= title> 开业时间 </td>
         <td  class= input> <input class=common name=ChiefBusiness> </td>
        
      </tr>
      
      <tr  class= common> 
       <td  class= title> 去年手续费收入 </td>
         <td  class= input> <input class= common name=Profit> 单位:万元</td>
        <td  class= title> 法人代表 </td>
        <td  class= input> <input  class= common name=Corporation> </td>
	      
      </tr>  
      
      <tr class=common>
      
       	<td  class= title> 销售资格 </td>
        <td  class= input> <input  name=SellFlag class= 'codeno'   verify="销售资格|NOTNULL&code:YesNo"
		           ondblclick="return showCodeList('YesNo',[this,SellFlagName],[0,1]);" 
		           onkeyup="return showCodeListKey('YesNo',[this,SellFlagName],[0,1]);"
		           ><Input class=codename name=SellFlagName readOnly > 
		    </td>
        <td  class= title> 机构帐户 </td>
        <td  class= input> <input class=common name=BankAccNo > </td>
      </tr> 
      <tr>
      <TD  class= title>
            合作终止状态
          </TD>
          <TD  class= input>
             <Input class='codeno' name=CalFlag verify="合作终止状态|code:yesno&NOTNULL" CodeData = "0|^N|有效|^Y|无效"
             ondblclick="return showCodeListEx('CalFlag',[this,CalFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('CalFlag',[this,CalFlagName],[0,1]);"
             ><Input class=codename name=CalFlagName readOnly >
          </TD>

         <TD class = title style="display:none">
             渠道类型
          </TD>
          <TD  class= input style="display:none">
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);"
             onkeyup="return showCodeListKey('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);" 
              verify="渠道类型|notnull&code:BranchType2" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
        </tr > 
    </table>
    </Div>
     <Input type=hidden name=BranchType >
   
    <INPUT class=cssButton VALUE="查 询" TYPE=button onclick="easyQueryClick();"> 
    <INPUT class=cssButton VALUE="返 回" TYPE=button onclick="returnParent();"> 					
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divComGrid);"></td>
    	<td class= titleImg> 查询结果 </td>
      </tr>
    </table>
    <Div  id= "divComGrid" style= "display: ''">
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  	    <span id="spanComGrid" >
  	    </span> 
  	  </td>
  	</tr>
      </table>
      <INPUT class=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 					
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
