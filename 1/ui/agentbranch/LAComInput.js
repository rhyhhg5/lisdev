//               该文件中包含客户端需要处理的函数和事件

var mDebug="1";
var mOperate="";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var showInfo;
var old_AgentGroup="";
var new_AgentGroup="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}
function getBank(cObj,cName)
{
	showCodeList('bankno',[cObj,cName],[0,1]);
}
function initUpAgentCom(){

	if(fm.all('hiddenBankType2').value==fm.all('BankType2').value){
		
	}else{
		fm.all('UpAgentCom').value="";
		fm.all('UpAgentComName').value="";
		fm.all('hiddenBankType2').value=fm.all('BankType2').value;
	}
	if( fm.all('BankType2').value == "01" ){
       fm.all('Licensestart').elementtype = "" ;
       fm.all('Licensestart').verify = "" ;
       fm.all('Licenseend').elementtype = "" ;
       fm.all('Licenseend').verify = "" ;
    }
}
function getUpAgentCom(cObj,cName)
{
   if (fm.all('BankType2').value == null || trim(fm.all('BankType2').value) == '')
     {
  	   alert("请先输银行机构的级别！");
  	   return false;
     } 
  if(fm.all('BankType2').value=='01')
    {
      showCodeList('bankno',[cObj,cName],[0,1],null,null,null,1);	
    }
  else
    {
      var strsql =" 1 and  AgentCom Like #" + fm.all('Bank').value + "%# and  actype=#01#  and banktype<#" + fm.all('BankType2').value + "# " ;
      showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);
    }

}
function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入网点管理机构！");
  	return false;
  } 
  var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) "
              + " and agentcode in (select agentcode from latree where branchtype=#3# and branchtype2=#01# and agentseries<>#2# ) ";
  if (fm.all('ChangeCom').value !=null && trim(fm.all('ChangeCom').value) != '')
    strsql += "and AgentGroup=#"+fm.all('HiddenAgentGroup').value+"# ";
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getSiteManager1(cObj,cName)
{
  if (fm.all('ManageCom').value ==null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入网点管理机构！");
  	return false;
  } 
  var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) "
              +" and agentcode in (select agentcode from latree where branchtype=#3# and branchtype2=#01# and agentseries<>#2#) ";
  if (fm.all('ChangeCom').value !=null && trim(fm.all('ChangeCom').value) != '')
 	strsql += "and AgentGroup=#"+fm.all('HiddenAgentGroup').value+"# ";
	showCodeListKey('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getChangeComName(cObj,cName)
{
  if (fm.all('ManageCom').value ==null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and EndFlag<>#Y# and branchlevel=#43#";
	showCodeList('branchattr',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getChangeComName1(cObj,cName)
{
  if (fm.all('ManageCom').value ==null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
   var strsql =" #1# and BranchType=#3# and ManageCom like #" + fm.all('ManageCom').value + "%# and EndFlag<>#Y# and branchlevel=#43#";
	
	
   showCodeListKey('branchattr',[cObj,cName],[0,1],null,strsql,'1',1);
}

//提交，保存按钮对应操作
function submitForm()
{	
	if (mOperate!="UPDATE||MAIN")
	{
   	  mOperate="INSERT||MAIN";
   	  fm.all('UpAgentCom1').value =  fm.all('upAgentCom').value;
   	  fm.all('BankType3').value = fm.all('BankType2').value;
    }
  if (!beforeSubmit())
   return false;
  //增加对于 江苏机构的校验
  if(!checkJiangSu())
  	return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
 
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  if(!getCodebyGroupAgentCode(fm.all('SiteManagerCode').value))
  {
      return false;
  }
  fm.submit(); //提交
}
function showCodeListManageCom(cObj,ManageComName)
{
    mEdorType = " #1# and length(trim(comcode))=#8#";
	showCodeList('comcode',[cObj,ManageComName], [0,1], null, mEdorType, "1");               

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  fm.reset();
	initInpBox();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  	mOperate = "";
	  if (fm.hideOperate.value=="UPDATE||MAIN")
	  {
		  fm.all('SiteManagerCode').disabled= true;    //负责网点员工代码
		  fm.all('ChangeCom').disabled= true;    
		  fm.all('ManageCom').disabled = true;
		  fm.all('AgentCom').readOnly = true;
	  }
	}
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initInpBox()
function resetForm()
{
  try
  {
    initInpBox();
  }
  catch(re)
  {
    alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
}
 

//提交前的校验、计算  
function beforeSubmit()
{
  	var tBankType2=fm.all('BankType2').value;
	var tACType=fm.all('ACType').value;
	if(tBankType2=='01'||tACType=='11')
	{
	   fm.all('BusiLicenseCode').verify='';
	} else {
		fm.all('BusiLicenseCode').verify='保险兼业代理资格证号|notnull';
	}
	// 如果机构级别是"网点"、"代理处" 则校验必须录入"是否县域网点"字段
    if(tBankType2=='03'||tBankType2=='04')
	{
	   fm.all('IsDotFlag').verify='是否县域网点|notnull';
	} else {
		fm.all('IsDotFlag').verify='';
	}
	
	if (!verifyInput2())
    return false;  
  //添加操作

  	if (fm.all('UpAgentCom').value == null || fm.all('UpAgentCom').value == '')
  	{
  		alert("上级机构编码不能为空！");
  		fm.all('UpAgentCom').focus();
  		return false;
  	}
 	if (fm.all('ChangeCom').value == null || fm.all('ChangeCom').value == '')
  	{
  		alert("请输入对应销售团队");
  		fm.all('ChangeCom').focus();
  		return false;
  	}
  
    if (fm.all('SiteManagerCode').value == null || fm.all('SiteManagerCode').value == '')
  	{
  		alert("请输入负责网点员工代码");
  		fm.all('SiteManagerCode').focus();
  		return false;
  	}
  if(mOperate == "INSERT||MAIN")
	{
	
		
	var tSql="";
  	var upagentcom=fm.all('UpAgentCom').value;
  	var tSql = "select managecom from lacom where agentcom = '"+upagentcom+"'";
  	var arrResult = easyExecSql(tSql);
  	if(arrResult!=fm.all('ManageCom').value && trim(upagentcom).length!='5')
  	 {
  	  alert("管理机构必须与上级机构所属管理机构相同");
  	  return false;
  	 }	
		
	var tSql="";
  	tSql="select endflag from lacom where agentcom='"+fm.all('UpAgentCom').value+"' with ur"
  	//alert(tSql);
  	var arrResult = easyExecSql(tSql);
  	if(arrResult=='Y')
  	{
  	    alert("上级机构"+fm.all('UpAgentCom').value+"已停业，不能新增下级机构！")
  	    resetForm();
  	    return false;
  	}
  	
    var strSQL = "";
    strSQL = "select   agentcom  from  lacom where 1=1 and actype='01' " 
    + getWherePart('Name','Name');
    var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (strQueryResult) {
    alert("该银行机构名称已经存在，请录入其他的名称！");
    fm.all("Name").value = '';
    //resetForm();
    return false;
    }	
    
    var strSQL1 = "";
    strSQL1 = "select   agentcom  from  lacom where 1=1  and agentcom='"+fm.all('AgentCom').value+"'"; 
    var strQueryResult1  = easyQueryVer3(strSQL1, 1, 0, 1);
    if (strQueryResult1) {
    alert("该银行机构编码已经存在，不能保存！");
    fm.all("AgentCom").value = '';
    resetForm();
    return false;
    }			
}
   if(fm.all('EndFlag').value=='N' || fm.all('EndFlag').value==null || fm.all('EndFlag').value=='')
   {
    if(fm.all('EndDate').value!=null&&fm.all('EndDate').value!='')
    {
     alert("未停业机构不能录入停业日期!");
     return false;   
     }
   }
   if(fm.all('EndFlag').value=='Y')
  	 {
  	  if(fm.all('EndDate').value==null||fm.all('EndDate').value=='')
  	  {
  	   alert("请录入停业日期!");
  	   return false;
  	   }
  	   //校验机构下是否还有未停业机构
       else
       {
        var MSQL = "select '1' FROM LACom WHERE  agentcom like '"+fm.all('AgentCom').value+"%'" +
    				" and agentcom<>'"+fm.all('AgentCom').value+"' and endflag<>'Y' ";
    	var strQueryResult  = easyQueryVer3(MSQL, 1, 0, 1);
    	if(strQueryResult)			
    	{
    	   if(confirm('该代理机构下还有未停业的机构,是否要将该机构下的所以代理机构置停业?'))
    	   {  
    	    return true; 	        	    
    	   }
    	   else
    	   {
    	     alert("您取消了操作!");
    	     return false;
    	   }
    	  }
        }
      } 

  //校验山东的sap供应商编码
   if (fm.all('ManageCom').value !=null && trim(fm.all('ManageCom').value) != ''&&(trim(fm.all('ManageCom').value).substring(0,4)=='8637' ))
   {
  	  if (trim(fm.all('BankType2').value) =='01' )
  	  {
  	  	if (fm.all('BankCode').value ==null || trim(fm.all('BankCode').value) == '')
        {
  	     alert("必须录入“SAP供应商编号”！");
  	     return false;
        } 
        else
        {
          var tBankCode=fm.all('BankCode').value;
          if(tBankCode.substring(0,1)!='2'||fm.all('BankCode').value.length!='10')	
          {
  	        alert("SAP供应商编号录入不符合要求，请重新录入");
  	        return false;
          }          
          var strSQL = "";
          strSQL = "select   bankcode  from  lacom where 1=1  and banktype='01' " 
          + getWherePart('BankCode','BankCode')
          + getWherePart('AgentCom','AgentCom','<>');
          var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
          if (strQueryResult) {
            alert("该SAP供应商编码已经存在，请重新录入！");
            fm.all("BankCode").value = '';
            return false;
          }  
        }
  	   }
  	   else
  	   {
  	        if (fm.all('BankCode').value !=null && trim(fm.all('BankCode').value) != '')
  	        {
  	        alert("不需要录入“SAP供应商编号”！");
  	        fm.all("BankCode").value = '';
  	        return false;
  	        }
  	   }  	    	    
    } 
      
      //除山东外，其他管理机构均在支行层级录入供应商编码
   else if (fm.all('ManageCom').value !=null && trim(fm.all('ManageCom').value) != ''&&trim(fm.all('ManageCom').value).substring(0,4)!='8637')
   {
  	    if (trim(fm.all('BankType2').value) =='02'|| trim(fm.all('BankType2').value) =='01')
  	    {
  	    	if (fm.all('BankCode').value ==null || trim(fm.all('BankCode').value) == '')
            {
  	         alert("必须录入“SAP供应商编号”！");
  	         return false;
            } 
            else
            {
              var tBankCode=fm.all('BankCode').value;
              if(tBankCode.substring(0,1)!='2'||fm.all('BankCode').value.length!='10')	
        	  {
  	            alert("SAP供应商编号录入不符合要求，请重新录入");
  	            return false;
              }          
//              var strSQL = "";
//              strSQL = "select   bankcode  from  lacom where 1=1  and banktype='01' " 
//              + getWherePart('BankCode','BankCode');
//              var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
//              if (strQueryResult) {
//                 alert("该SAP供应商编码已经存在，请重新录入！");
//                 fm.all("BankCode").value = '';
//                 return false;
//              }  
            }
  	    }
  	     else
  	   {
  	        if (fm.all('BankCode').value !=null && trim(fm.all('BankCode').value) != '')
  	        {
  	        alert("不需要录入“SAP供应商编号”！");
  	        fm.all("BankCode").value = '';
  	        return false;
  	        }
  	   }  	   	    	    
   } 
   fm.all('ManageCom').disabled=false;
  return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,100,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  
   //if(fm.all('EndFlag1').value=='Y')
   //{
   // alert("停业机构不能再进行修改!");
    //return false;
   //}
  //下面增加相应的代码
  if ((fm.all("AgentCom").value==null)||(fm.all("AgentCom").value==''))
  {
    alert("请先确定银行机构编码！");
    fm.all('AgentCom').focus();
  }  
  else
  {
  	//查询判断是否这个银行机构存在
    if (!queryAgentCom())
      return false;
    if(fm.all('EndFlag').value=='Y' && fm.all('EndFlag1').value=='Y'){
    	alert("操作失败，不能对停业中的代理机构在没有将其置为开业的情况下对其进行修改。");
    	return false;
    }
    if(fm.all('EndFlag').value=='N' && fm.all('EndFlag1').value=='Y'){
    	var tSQL="select 1 from ldcom where comcode='"
    	         +fm.all('ManageCom').value+"' and sign='1' and length(trim(comcode))=8 ";
    	var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
    	if(!strQueryResult){
    		alert('校验管理机构信息时失败，该管理机构不存在或还没有开业。');
    		return false;
    	}
    	tSQL="select 1 from labranchgroup where managecom='"
    	     +fm.all('ManageCom').value+"' and branchattr='"
    	     +fm.all('ChangeCom').value+"' and endflag<>'Y' and enddate is null "
    	     +" and branchtype='3' and branchtype2='01'";
    	strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
    	if(!strQueryResult){
    		alert('校验对应营业部信息时失败，该团队不存在或停业或不在该管理机构下。');
    		return false;
    	}     
    	tSQL="select 1 from laagent where managecom = '"
    	         +fm.all('ManageCom').value+"' and agentgroup in "
    	         +"(select agentgroup from labranchgroup where branchattr='"
    	         +fm.all('ChangeCom').value+"' and branchtype='"
    	         +fm.all('BranchType').value+"' and branchtype2='"
    	         +fm.all('BranchType2').value+"') and groupagentcode='"
    	         +fm.all('SiteManagerCode').value+"' and agentstate<'06'";
    	strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
    	if(!strQueryResult){
    		alert('校验负责该代理机构的代理人信息时失败，该代理人不存在或已离职或不在该团队。');
    		return false;
    	}
    	
    }
    //增加对于 江苏机构的校验
    if(!checkJiangSu())
    	return false;
    if (confirm("您确实想修改该记录吗?"))
    {     	
      mOperate="UPDATE||MAIN";
      if(fm.all('ManageCom').value!=fm.all('HiddenManageCom').value)
      {
        alert("管理机构不能修改!");
		return false; 
      }
      fm.all('ManageCom').disabled = false;
      fm.all('SiteManagerCode').disabled = false;
      fm.all('ChangeCom').disabled = false;
      submitForm();
      //fm.all('ManageCom').disabled = true; 
      fm.all('SiteManagerCode').disabled = true; 
      fm.all('ChangeCom').disabled = true; 
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
 // mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAComQuery.html?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCom").value==null)||(fm.all("AgentCom").value==''))
  {
    alert("请先确定银行机构编码！");
    fm.all('AgentCom').focus();
  }
  else
  {
    if (!queryAgentCom())
      return false;
    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";  
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//验证机构编码的合理性
function checkValid()
{ 
  if (getWherePart('AgentCom')=='')
    return false;
  if (trim(fm.all('AgentCom').value).length == 3)
  {
  	fm.all('BankType2').value='00';
  }
  else if (trim(fm.all('AgentCom').value).length == 6)
  {
  	fm.all('BankType2').value='01';
  }
  else if (trim(fm.all('AgentCom').value).length == 9)
  {
  	fm.all('BankType2').value='02';
  }
  else if (trim(fm.all('AgentCom').value).length == 12)
  {
  	//fm.all('BankType2').value='03';
  }
  else if (trim(fm.all('AgentCom').value).length == 15)
  {
  	//fm.all('BankType2').value='04';
  }
  else
  {
      alert("银行机构编码位数错误！");
      fm.all('ManageCom').value="";
		  fm.all('ManageComName').value = '';
          fm.all('AgentCom').value="";
		  fm.all('ChangeCom').value = '';
		  fm.all('SiteManagerCode').value = '';
		  fm.all('SiteManager').value = '';
		  fm.all('HiddenAgentGroup').value = '';
		  fm.all('HiddenAgentGroup2').value = '';
	  	return false;
  }
  showOneCodeNametoAfter('BankType2','BankType2','BankType2Name');
  
  var strSQL = "select * from LACom where 1=1 and branchtype='3'"
	   + getWherePart('AgentCom');
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  //判断是否查询成功
  if (strQueryResult)
  {
      alert("此银行机构编码已经存在！");
      fm.all('ManageCom').value="";
		  fm.all('ManageComName').value = '';
      fm.all('AgentCom').value="";
		  fm.all('ChangeCom').value = '';
		  fm.all('SiteManagerCode').value = '';
		  fm.all('SiteManager').value = '';
		  fm.all('HiddenAgentGroup').value = '';
		  fm.all('HiddenAgentGroup2').value = '';
		  fm.all('BankType2').value = '';
		  fm.all('BankType2Name').value = '';		  
      return false;
  }
  var tCode = fm.all('AgentCom').value.substring(3,5);
  fm.all('ManageCom').value = '86'+tCode+'0000';
  var strSQL = "select * from LDCom where 1=1 "
	   + getWherePart('ComCode','ManageCom');
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  //判断是否查询成功
  if (!strQueryResult)
  {
      alert("不存在对应的管理机构！");
      fm.all('ManageCom').value="";
		  fm.all('ManageComName').value = '';
      fm.all('AgentCom').value="";
		  fm.all('ChangeCom').value = '';
		  fm.all('SiteManagerCode').value = '';
		  fm.all('SiteManager').value = '';
		  fm.all('HiddenAgentGroup').value = '';
		  fm.all('HiddenAgentGroup2').value = '';
      return false;
  }
  showOneCodeNametoAfter('comcode','ManageCom');
}

//验证上级机构编码的合理性
function checkValidUp()
{ 
  if (getWherePart('UpAgentCom')=='')
    return false;
  var strSQL = "select Name from LACom where 1=1 and branchtype='3'"
	   + getWherePart('AgentCom','UpAgentCom');
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
   if (strQueryResult)
   {
   	   var arr = decodeEasyQueryResult(strQueryResult);
       fm.all('UpAgentComName').value=arr[0][0];
   }
   else
   {
   	alert("此代理机构不存在！");
   	fm.all('UpAgentCom').focus();
   	fm.all('UpAgentComName').value= '';
   	fm.all('UpAgentCom').value= '';
  	return false;
   }
}

function queryAgentCom()
{
  //查询判断是否这个银行机构存在
  
	// 书写SQL语句
        var strSQL = "";
	strSQL = "select AgentCom from LACom where 1=1 "
	        + getWherePart('AgentCom');	 
	//turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
  if (!strQueryResult) {
    alert("不存在所要操作的代理机构！");
    fm.all("AgentCom").value = '';
    return false;
    }
    return true;
}

//用来显示返回的选项
function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  if( arrQueryResult != null )
  {
   initInpBox();
   arrResult = arrQueryResult;
   fm.all('AgentCom').value = arrResult[0][0];  
   fm.all('Name').value = arrResult[0][1];                                              
   fm.all('Address').value = arrResult[0][2];                                              
   fm.all('Phone').value = arrResult[0][3];                                             
   fm.all('LinkMan').value = arrResult[0][4];                                                                                                                   
   fm.all('BankType2').value = arrResult[0][6];
   fm.all('SellFlag').value = arrResult[0][7];
   fm.all('ManageCom').value = arrResult[0][8];  
   fm.all('ManageCom').disabled=true;
   fm.all('HiddenManageCom').value = arrResult[0][8];
   fm.all('ACType').value = arrResult[0][10];
   fm.all('ACTypeName').value = arrResult[0][30];
   fm.all('Noti').value=arrResult[0][13];  
   fm.all('Operator').value=arrResult[0][14];
   fm.all('UpAgentCom').value = arrResult[0][16];
   fm.all('EndFlag').value = arrResult[0][17];
   fm.all('EndFlag1').value = arrResult[0][17];
   fm.all('EndDate').value = arrResult[0][18];
   fm.all('BusiLicenseCode').value = arrResult[0][19];
   fm.all('Licensestart').value = arrResult[0][20];
   fm.all('Licenseend').value = arrResult[0][21];
   fm.all('SignDate').value = arrResult[0][22];
   fm.all('ProEndDate').value = arrResult[0][23];
   fm.all('BankCode').value=arrResult[0][24];
   fm.all('bankcode1').value=arrResult[0][24];
   //alert("ttt:"+fm.all('bankcode1').value);
   fm.all('IsDotFlag').value = arrResult[0][25];
   if(arrResult[0][25]=='Y')
   {
   	fm.all('IsDotFlagName').value='是';
   }else if(arrResult[0][25]=='N')
   {
   	fm.all('IsDotFlagName').value='否';
   }else
   {
   	fm.all('IsDotFlagName').value='';
   }
   if((arrResult[0][6]=='01'&&trim(arrResult[0][8]).substring(0,4)=='8637')){
  		fm.all("Type1").style.display = '';
  	}else if(arrResult[0][6]=='02'&&trim(arrResult[0][8]).substring(0,4)!='8637'){
  		fm.all("Type1").style.display = '';
  	}
  	else if(arrResult[0][6]=='01'&&trim(arrResult[0][8]).substring(0,4)!='8637'){
  		fm.all("Type1").style.display = '';
  	}else
  	{
  	    fm.all("Type1").style.display = 'none';
  	}
   fm.all('Bank').value=fm.all('AgentCom').value.substring(0,5);
   fm.all('UpAgentCom1').value = arrResult[0][16];
   fm.all('BankType3').value = arrResult[0][6];
   getRelationInfo();
   showCodeName();
   if (fm.all('UpAgentCom').value!=null&&fm.all('UpAgentCom').value!='')
    {
   	 getComName();
  	}      
  	if(arrResult[0][7]=='Y'){
  		fm.all('SellFlagName').value='有';
  	}else{
  		fm.all('SellFlagName').value='无';
  	}      
  	if(arrResult[0][17]=='Y'){
  		fm.all('EndFlagName').value='是';
  	}else{
  		fm.all('EndFlagName').value='否';
  	}
   //if(fm.all('EndFlag').value=='Y')      
   //{
   // fm.all('EndFlag').disabled = true;
    //fm.all('EndDate').disabled = true;
   //}
   var  tChange=fm.all("ChangeCom").value;
   var  tManage=fm.all("SiteManagerCode").value;
   if((tChange!=null)&&(tChange!= '')&&(tManage!=null)&&(tManage!= ''))
   {
   	fm.all('SiteManagerCode').disabled = true;
   	fm.all('ChangeCom').disabled = true;
   	}
   	else
   	{
   			fm.all('SiteManagerCode').disabled = false;
      	fm.all('ChangeCom').disabled = false;
      	fm.all("Mark").value = 'M';
   			}
   	//alert(fm.all("Mark").value);

   showOneCodeNametoAfter('comcode','ManageCom','ManageComName');
   
   showOneCodeNametoAfter('banktype2','BankType2','BankType2Name');
   //showOneCodeNametoAfter('EndFlag','EndFlag','EndFlagName'); 
   var strsql ="select name,agentgroup from labranchgroup where 1=1 and BranchType='3'  and BranchType2='01' and ManageCom like '" + fm.all('ManageCom').value + "%' and branchattr = '" + fm.all('ChangeCom').value + "' ";
	 var strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);  
	  if (!strQueryResult) {
        alert("不存在对应的营业部 ！");
        fm.all("ChangeComName").value = '';
        return false;
    }
   var tArr = new Array();
   tArr = decodeEasyQueryResult(strQueryResult);

   fm.all("ChangeComName").value =tArr[0][0];
   fm.all("HiddenAgentGroup3").value=tArr[0][1];
   fm.all('UpAgentCom').disabled = true;
   fm.all('BankType2').disabled = true;
   fm.all('Bank').disabled=true;
   fm.all('AgentCom').readOnly= true;
   //fm.all('ManageCom').disabled = true;
   if(arrResult[0][17]=='Y'){
   	fm.all('SiteManagerCode').disabled = false;
   	fm.all('ChangeCom').disabled = false;
   	//fm.all('ManageCom').disabled = false;
   	fm.all("Mark").value = 'M';
   } 
   fm.all('BankAccNo').value = arrResult[0][26];
   fm.all('BankAccNoInsure').value = arrResult[0][26];
   fm.all('BankAccName').value = arrResult[0][27];
   fm.all('BankAccOpen').value = arrResult[0][28];
   fm.all('AgentOrganCode').value = arrResult[0][29];
   
  }
}

function getLacomtoagent()
{
}

function WriteContract()
{
//	alert("WriteContract!");
	if(fm.all('ContNo').value !="")
	{
		showInfo=window.open("./LAComWriteContract.html");
	}
	else
	{
		alert("请先保存再录入合同细则！");
	}
}	

function getComName()
{
 var sql = "";
 if(fm.all('BankType2').value!='01')
 {
	  sql = "select Name from LACom where 1=1"
          + getWherePart('AgentCom','UpAgentCom');
  }
 else
 {
	 sql =" select  CodeName  from ldcode where 1 = 1 and codetype = 'bankno' "
          + getWherePart('code','UpAgentCom'); 		
 }

	 //      input.value="";
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arr = decodeEasyQueryResult(strResult); 
  	fm.all('UpAgentComName').value = arr[0][0];
  }
  else
  {      	
  	alert("上级机构编码输入有误或没有此上级机构！");
  }
  return true;
}

function getManagerName(input)
{
	if(fm.all('SiteManagerCode').value==""||fm.all('SiteManagerCode').value==null)
	{		
		fm.all('SiteManager').value=="";
		return false;
	}
	else
	{
  	var sql = "select Name,AgentGroup from LAAgent where 1=1"
          + getWherePart('GroupAgentCode','SiteManagerCode');
  	var strResult = easyQueryVer3(sql, 1, 1, 1);
  	if (strResult)
  	{
  		var arrDataSet = decodeEasyQueryResult(strResult);
   		input.value = arrDataSet[0][0];
   		fm.all('HiddenAgentGroup2').value=arrDataSet[0][1];
   		fm.all('HiddenAgentGroup').value = arrDataSet[0][1];
  		return true;
 	 }
  	else
 	 {
  		input.value="";
  		alert("负责网点员工代码输入有误！");
  		return false;
  	}
	}
}

function getRelationInfo()
{
  var sql = "select a.branchattr from labranchgroup a,LAComToAgent b where 1=1 and b.AgentCom='"+fm.all('AgentCom').value+"' and b.RelaType='0' and a.agentgroup = b.agentgroup and (a.state<>'1' or a.state is null)";
  var strResult = easyQueryVer3(sql, 1, 1, 1);
 // alert(sql);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	fm.all('ChangeCom').value = tArr_Record[0][0];
  }
  else
  {
  	fm.all('ChangeCom').value="";
  }
  var sql = "select getunitecode(AgentCode) from LAComToAgent where 1=1 and AgentCom='"+fm.all('AgentCom').value+"' and RelaType='1'";
  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strResult);
  	var tArr_Record = new Array();
  	tArr_Record = chooseArray(arrDataSet,[0]);
  	fm.all('SiteManagerCode').value = tArr_Record[0][0];
  	getManagerName(fm.all('SiteManager'));
  }
  else
  {
  	fm.all('SiteManagerCode').value="";
  	fm.all('SiteManager').value="";
  }
}

function afterCodeSelect( cCodeName, Field )
{
	
 var tvalue = Field.value;
	//alert(cCodeName);
  if (tvalue == null || tvalue == '')
     return false;
	try	
	{
		if (cCodeName=="yesno")
		{
			if (Field.value=="Y")
			{ 
			   write.style.display=""; 
			}
			else
			{
			   write.style.display="none";
			}
		}
		else if(cCodeName == "BankType2") {
		if(Field.value=='01'||Field.value=='02'){
  		fm.all("Type1").style.display = "";
  		}
	  else {
    	fm.all("Type1").style.display = "none";
	    }
    }
		else if ( cCodeName == "comcode" )
		{
			fm.all('ChangeCom').value = '';
			fm.all('ChangeComName').value = '';
			fm.all('SiteManager').value = '';
			fm.all('SiteManagerCode').value = '';
		}
		else if( cCodeName == "branchattr" )	
		{
			fm.all('SiteManager').value = '';
			fm.all('SiteManagerCode').value = '';
		  var strSQL = "select AgentGroup from LABranchGroup where branchtype='3' and branchtype2='01'"
			           + " and branchattr='"+tvalue+"'";
		  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
		  //alert(strSQL)
		  if (!strQueryResult)
		  {
		   	alert("此销售团队不存在！");
		   	fm.all('ChangeCom').focus();
		   	return false;
		  }
		  var arr = decodeEasyQueryResult(strQueryResult);
		  fm.all('HiddenAgentGroup').value = arr[0][0];
		  fm.all('HiddenAgentGroup2').value = arr[0][0];
		}
		else if( cCodeName == "AgentCode" )	
		{
				 if (fm.all('ChangeCom').value != null && fm.all('ChangeCom').value != '')
				   return true;
		     var strSQL = "select a.AgentGroup,b.BranchAttr from LAAgent a,LABranchGroup b where a.branchtype='3' and a.branchtype2='01' "
			              + " and a.AgentCode='"+tvalue+"' and a.AgentGroup=b.AgentGroup";
		     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
		     if (!strQueryResult)
		     {
		   	  	alert("此销售团队不存在！");
		  	  	fm.all('ChangeCom').focus();
		  	  	return false;
		     }
		     var arr = decodeEasyQueryResult(strQueryResult);
		     fm.all('HiddenAgentGroup').value = arr[0][0];
		     fm.all('HiddenAgentGroup2').value = arr[0][0];
		     fm.all('ChangeCom').value = arr[0][1];
		     showOneCodeNametoAfter('branchattr','ChangeCom','ChangeComName');
		}
		
	}
	catch( ex ) 
	{
		alert('业务员设置出错!');
	}
}
function getCodebyGroupAgentCode(pGroupAgentCode)
{
    var strSQL = "select AgentCode from LAAgent where groupagentcode='"+pGroupAgentCode+"'";
    var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
    if (!strQueryResult)
    {
      alert("未查询到该负责员工信息！");
      return false;
    }
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('hideSiteManagerCode').value = arr[0][0];
    return true;
}
//增加对于江苏机构的校验
function  checkJiangSu()
{
	//增加对于江苏机构的校验
	   if (fm.all('ManageCom').value !=null && trim(fm.all('ManageCom').value) != ''&&(trim(fm.all('ManageCom').value).substring(0,4)=='8632' ))
	   {
	    	if(!verifyNotNull('中介组织机构代码',fm.all('AgentOrganCode').value )) 
	        {
	         alert("江苏公司银行机构录入时需录入中介组织机构代码");
	         return false;
	        }
	    	var agentOrganCode = fm.all('AgentOrganCode').value.trim();
	    	if(agentOrganCode.length!=9)
	    	{
	    		 alert("中介组织机构代码只能为9位");
	             return false;
	    	}
	    	if(fm.all('BankAccNo').value==null||fm.all('BankAccNo').value=="")
	    	{
	    		alert("江苏公司银行机构时录入时需要录入帐号");
	    		fm.all('BankAccNo').focus();
	    		return false;
	    	}
	    	if(fm.all('BankAccNoInsure').value==null||fm.all('BankAccNoInsure').value=="")
	    	{
	    		alert("江苏公司银行机构时录入时需要录入帐号确认");
	    		fm.all('BankAccNoInsure').focus();
	    		return false;
	    	}
	    	if(fm.all('BankAccNo').value!=fm.all('BankAccNoInsure').value)
	    	{
	    	    alert("两次录入的[帐户号码]不一致!");
	    	    fm.all('BankAccNoInsure').focus();
	    	    return false;
	    	}
	    	if(fm.all('BankAccName').value==null||fm.all('BankAccName').value=="")
	    	{
	    		alert("江苏公司银行机构时录入时需要录入帐号名");
	    		fm.all('BankAccNo').focus();
	    		return false;
	    	}
	    	if(fm.all('BankAccOpen').value==null||fm.all('BankAccOpen').value=="")
	    	{
	    		alert("江苏公司银行机构时录入时需要录入开户行");
	    		fm.all('BankAccOpen').focus();
	    		return false;
	    	}
	   }
	   return true;
}