<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAGroupUniteInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#  and GradeCode like #D%#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAGroupUniteInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAGroupUniteInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAGroupUniteSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="./GroupUniteButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent);">
    <td class=titleImg>
      目标团队信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent" style= "display: ''">      
    <table class=common>
      <TR  class= common>
        <TD  class= title>
          销售团队代码
        </TD>
        <TD  class= input>
          <Input class='common' name=BranchAttr onChange="getGroupArm();" 
                 elementtype=nacessary verify="销售团队代码|NOTNULL&len=10" maxlength=10 >
        </TD>
        <TD  class= title>
          销售团队名称
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=BranchName >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队管理机构
        </TD>
        <TD  class= input>
          <!--Input class=common name=BranchManager onchange="return changeManager();"-->
          <Input class='readonly'readonly name=ManageComm>
        </TD>
        <TD  class= title>
          级别
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=BranchLevel >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队管理人员代码
        </TD>
        <TD  class= input>
          <!--Input class=common name=BranchManager onchange="return changeManager();"-->
          <Input class='readonly'readonly name=AgentCode>
        </TD>
        <TD  class= title>
          团队管理人名称
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=Name >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队成立日期
        </TD>
        <TD  class= input>
          <!--Input class=common name=BranchManager onchange="return changeManager();"-->
          <Input class='readonly'readonly name=FoundDate >
        </TD>
        <TD  class= title>
          团队内人数
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=AgentCount>
        </TD>
      </TR>
    </table>
   </div>
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent2);">
          <td class=titleImg>
            被合并团队信息
          </td>
        </td>
      </tr>
    </table>
  <Div  id= "divAdjustAgent2" style= "display: ''">
    <table  class= common>
    </table> 
    <table class=common>
      <TR  class= common>
        <TD  class= title>
          销售团队代码
        </TD>
        <TD  class= input>
          <Input class='common' name=BranchAttrU onChange="getGroupU()"
                 elementtype=nacessary verify="销售团队代码|NOTNULL&len=10" maxlength=10 >
        </TD>
        <TD  class= title>
          销售团队名称
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=BranchNameU>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队管理机构
        </TD>
        <TD  class= input>
          <!--Input class=common name=BranchManager onchange="return changeManager();"-->
          <Input class='readonly'readonly name=ManageCommU>
        </TD>
        <TD  class= title>
          级别
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=BranchLevelU>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队管理人员代码
        </TD>
        <TD  class= input>
          <!--Input class=common name=BranchManager onchange="return changeManager();"-->
          <Input class='readonly'readonly name=AgentCodeU >
        </TD>
        <TD  class= title>
          团队管理人名称
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=NameU>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          团队成立日期
        </TD>
        <TD  class= input>
          <!--Input class=common name=BranchManager onchange="return changeManager();"-->
          <Input class='readonly'readonly name=FoundDateU >
        </TD>
        <TD  class= title>
          团队内人数
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=AgentCountU >
        </TD>
      </TR>
    </table>
    </div>
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupManager);">
          <td class=titleImg>
            被合并团队主管调整
            <font color="red">(调整到新机构的职级不能再是主管职级)</font>
          </td>
        </td>
      </tr>
    </table>
    <Div id= "divGroupManager" style= "display: ''">
    <Table class= common>
      <TR class= common>
        <TD class= title>
          团队主管代码
        </TD>
        <TD class= input>
          <Input class='readonly'readonly name=GroupManager>
        </TD>
        <TD class= title>
          团队主管名称
        </TD>
        <TD class= input>
          <Input class='readonly'readonly name=GroupManagerName>
        </TD>
      </TR>
      <TR class= common>
        <TD class= title>
          原职级
        </TD>
        <TD class= input>
          <Input class='readonly'readonly name=AgentGradeOld>
        </TD>
        <TD class= title>
          调整经理职级
        </TD>
        <TD class= input>
          <Input class='codeNo' name=AgentGradeNew verify="客户经理职级|notnull&code:AgentGrade" 
						     ondblclick="return showCodeList('AgentGrade',[this,AgentGradeNewName],[0,1],null,msql,1);" 
                 onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeNewName],[0,1],null,msql,1);"
                 ><Input class=codename name=AgentGradeNewName readonly >
        </TD>
      </TR>
      <TR class= common>
        <!--TD class= title>
          考核年月
        </TD>
        <TD class= input>
          <Input class='common' name=IndexCalNo verify="考核年月|notnull&len<=6&int">
        </TD-->
        <TD  class= title>
           调动日期 
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=AdjustDate dateFormat='short'
                   verify="调动日期|notnull&DATE" elementtype=nacessary>
          </TD>
        <!--TD class= title>
          待处理标记
        </TD>
        <TD class= input>
          <Input class=codeNo name=ActMarker
                 ondblclick="return showCodeList('yesno',[this,ActMarkerName],[0,1]);" 
                 onkeyup="return showCodeListKey('yesno',[this,ActMarkerName],[0,1]);"
                 ><Input class=codename name=ActMarkerName readonly elementtype=nacessary>
        </TD-->
      </TR>
      <!--TR  class= common>
        <TD  class= title>
          调整目标团队代码
        </TD>
        <TD  class= input>
          <Input class='common' name=BranchAttrC onChange="getGroupChange();" maxlength=10 >
        </TD>
        <TD  class= title>
          调整目标团队名称
        </TD>
        <TD  class= input>
          <Input class='readonly'readonly name=BranchNameC >
        </TD>
      </TR-->
    </Table>
    </Div>
    <br>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=GroupPart value=''>
    <input type=hidden name=AgentGroupU value=''>
    <input type=hidden name=AgentGroupA value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
