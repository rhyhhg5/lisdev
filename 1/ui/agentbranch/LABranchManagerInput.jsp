<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LABranchManagerInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LABranchManagerInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LABranchManagerInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LABranchManagerSave.jsp" method=post name=fm target="fraSubmit" >
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchManager1);">
    </IMG>
      <td class=titleImg>
      无主管机构基本信息
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchManager1" style= "display: ''">
      <table  class= common>
      <tr class = common>
      	<TD  class= common>
            <Input type=button class="cssButton" value="查询" onclick="BranchQuery();">
          </TD>
          </tr>

        <TR  class= common>
          <TD  class= title>
            销售团队代码
          </TD>
          <TD  class= input>
            <Input class='common' name=BranchAttr >
          </TD>
          <TD  class= title>
            销售团队名称
          </TD>
          <TD  class= input>
            <Input class= 'readonly' readonly  name=Name >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=ManageCom >
          </TD>
          <TD  class= title>
            类型
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BranchType >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            级别
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BranchLevel >
          </TD>
          <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= 'readonly' readonly name=BranchAddress >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= 'readonly' readonly name=BranchPhone >
          </TD>
       <!--   <TD  class= title>
            传真 
          </TD>
          <TD  class= input>
            <Input class= 'readonly' readonly name=BranchFax >
          </TD>  -->  
      		<TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= 'readonly' readonly name=BranchZipcode  >
          </TD>  
        </TR>
        <TR  class= common>
          <TD  class= title>
            成立标志日期
          </TD>
          <TD  class= input>
            <Input class= 'readonly' readonly name=FoundDate verify="成立标志日期|notnull&DATE" format='short'>
          </TD>
          <TD  class= title>
            停业
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=EndFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=EndDate >
          </TD>
  		  	<TD class= title>
           <!-- 传真 --> 
          </TD>
          <TD  class= input>
            <Input type=hidden class= 'readonly' readonly name=BranchFax >
          </TD>
        </TR>
       </table>
    </Div>
        <Div id="hh" style= "display: 'none'">
        <TR  class= common>
          <TD  class= title>
            是否有独立的营销职场
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=FieldFlag>
          </TD>
          <TD  class= title>
            与上级的隶属关系
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=UpBranchAttr  >
          </TD>
          </TR>
				</Div>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchManager);">
    </IMG>
      <td class=titleImg>
      管理人员信息
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchManager" style= "display: ''">
      <table  class= common>
    <TR  class= common>
        <TD  class= title>
            管理人员代码
          </TD>
          <TD  class= input>
            <Input class=common name=BranchManager onchange="QueryBranchManagerName();" elementtype=nacessary>
          </TD>
          <TD  class= title>
            管理人员名称
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=BranchManagerName >
          </TD>
        </TR>
        <TD  class= title>
            任命日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=AdjustDate dateFormat='short' verify="任命日期|notnull&date">
          </TD>
        </TR>
        <tr class = common>
        	<TD  class= common>
            <Input type=button class="cssButton" value="任命" onclick="return submitForm();">
          </TD>
          </tr>
        </table>
        </div>
        <p> <font color="#ff0000">注：如果是分立团队的主管任命，请注意维护育成关系！  </font></p>
    <input type=hidden name=hideOperate value=''>
    <Input type=hidden  name=Operator >
    <Input type=hidden  name=BranchType2 value='' >
    <input type=hidden name=AgentGrade >
    <input type=hidden name=AgentGroup value='' >  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
    <input type=hidden name=UpBranch value='' >  <!--上级机构代码，存储隐式机构代码 -->
    <input type=hidden name=PartTime value = '1'>  <!--默认改变组信息-->
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
