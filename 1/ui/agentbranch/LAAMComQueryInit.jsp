<%
//程序名称：LASpecComQueryInit.jsp
//程序功能：
//创建日期：2003-09-16 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                           
    fm.all('AgentCom').value = '';
    fm.all('Name').value = '';
    fm.all('ManageCom').value = '';
    fm.all('ACType').value = '';
    fm.all('Asset').value = '';
    fm.all('Corporation').value = '';
    fm.all('Profit').value = '';
    fm.all('ChiefBusiness').value = '';
  //  fm.all('ProtocalNo').value = '';
    fm.all('SellFlag').value = '';
  //  fm.all('UpAgentCom').value = '';
  fm.all('BankAccNo').value = '';
  fm.all('BranchType').value='<%=BranchType%>';
  fm.all('BranchType2').value='02';
   fm.all('BranchType2Name').value='中介';
  }
  catch(ex)
  {
    alert("在LASpecComInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LASpecComInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initComGrid(); 
  }
  catch(re)
  {
    alert("LASpecComInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化ComGrid
 ************************************************************
 */
function initComGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="专业代理机构编码";         //列名
    iArray[1][1]="180px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="负责管理机构";         //列名
    iArray[2][1]="180px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="专业代理机构名称";         //列名
    iArray[3][1]="180px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="上级代理机构";         //列名
    iArray[4][1]="0px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=3;         //是否允许录入，0--不能，1--允许
        
/*    iArray[5]=new Array();
    iArray[5][0]="代理人编码";         //列名
    iArray[5][1]="180px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="销售机构";         //列名
    iArray[6][1]="180px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
*/  
    ComGrid = new MulLineEnter( "fm" , "ComGrid" ); 

    //这些属性必须在loadMulLine前
    ComGrid.mulLineCount = 0;
    ComGrid.hiddenPlus = 1;
    ComGrid.hiddenSubtraction = 1;
    ComGrid.displayTitle = 1;
    ComGrid.canSel=1;
    ComGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
        alert("初始化ComGrid时出错："+ ex);
  }
}

</script>