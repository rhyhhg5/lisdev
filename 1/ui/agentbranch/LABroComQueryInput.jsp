<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABroComQueryInput.jsp
//程序功能：
//创建日期：2003-09-16 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LABroComQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LABroComQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>经纪公司机构信息</title>
</head>
<body  onload="initForm();" >
  <form action="./LABroComQuerySubmit.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABroCom1);"></IMG></td>
        <td class=titleImg> 查询条件 </td>
      </tr>
    </table>
    <Div  id= "divLABroCom1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 经纪公司机构编码 </td>
        <td  class= input> <input class=common name=AgentCom> </td>
        <td  class= title> 经纪公司机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
      </tr>
      
      <tr  class= common> 
        <td  class= title> 负责管理机构 </td>
        <td  class= input> <input class='code' name=ManageCom ondblclick="return showCodeList('station',[this]);"> </td>
        <td  class= title> 上级代理机构 </td>
        <td  class= input> <input class=common name=UpAgentCom > </td>
      </tr>
      
      <tr  class= common>
        <td  class= title> 经纪公司业务负责人 </td>
        <td  class= input> <input  class= common name=LinkMan> </td>
        <td  class= title> 经纪公司单位负责人 </td>
        <td  class= input> <input  class= common name=Corporation> </td>
      </tr>
      
      <tr  class= common> 
        <td  class= title> 销售资格 </td>
        <td  class= input> <input  name=SellFlag class= 'code'
		           ondblclick="return showCodeList('YesNo',[this]);" 
		           onkeyup="return showCodeListKey('YesNo',[this]);"> </td>
        <td  class= title> 代理机构类别 </td>
        <td  class= input> <input class = readonly readonly name=ACType > </td>
      </tr>   
    </table>
    </Div>
    <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
    <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divComGrid);"></td>
    	<td class= titleImg> 查询结果 </td>
      </tr>
    </table>
    <Div  id= "divComGrid" style= "display: ''">
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  	    <span id="spanComGrid" >
  	    </span> 
  	  </td>
  	</tr>
      </table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 					
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
