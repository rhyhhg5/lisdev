//               该文件中包含客户端需要处理的函数和事件
var mDebug="1";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
	if(showInfo!=null){
		try{
			showInfo.focus();
		}
		catch(ex){
			showInfo=null;
		}
	}
}

//提交，保存按钮对应操作
function submitForm(){
	//数据校验
	if (!beforeSubmit())
		return false;
	if (verifyInput() == false)
		return false;
	//如果mOperate为空，则表示新增操作，重置mOperate值
	//alert(mOperate);
	if (mOperate==""){
		mOperate="INSERT||MAIN";
		//校验代理机构代码
		var strSQL = "";
		strSQL = "select AgentCom from LACom where 1=1 "
			+ getWherePart('AgentCom');
		var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		
		if (strQueryResult){
			alert("该代理机构编码已存在！");
			fm.all("AgentCom").value = '';
			return false;
		}
	}
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value==""){
		alert("操作控制数据丢失！");
	}
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
	showInfo.close();
	if (FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showDiv(operateButton,"true");
		showDiv(inputButton,"false");
	}
	//初始化mOperate
	mOperate="";
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm(){
	try{
		initForm();
	}
	catch(re){
		alert("在LAGrpSpec.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//取消按钮对应操作
function cancelForm(){
	showDiv(operateButton,"true");
	showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
	if(fm.all('AgentCom').value==null || fm.all('AgentCom').value==""){
		alert("请输入代理机构编码！");
		fm.all('AgentCom').focus();
		return false;
	}
	if(fm.all('BankType').value==null || fm.all('BankType').value==""){
		alert("请选择代理机构级别！");
		fm.all('BankType').focus();
		return false;
	}
	else{
		//如果机构级别为一级，则可以不录入上级机构
		if(fm.all('BankType').value!="01"&&fm.all('UpAgentCom').value==""){
			alert("请输入该代理机构的上级机构！");
			fm.all('UpAgentCom').focus();
			return false;
		}
		else{
			//机构编码和级别的校验
			//alert(trim(fm.all('AgentCom').value).length);
			if(fm.all('BankType').value=="01"){
				if(trim(fm.all('AgentCom').value).length!=4){
					alert("代理机构编码与机构级别不匹配，请查询！");
					fm.all('AgentCom').focus();
					return false;
				}
			}
			if(fm.all('BankType').value=="02"){
				//机构编码和级别的校验
				if(trim(fm.all('AgentCom').value).length!=6){
					alert("代理机构编码与机构级别不匹配，请查询！");
					fm.all('AgentCom').focus();
					return false;
				}
				//二级机构的上级机构编码4位
				if(trim(fm.all('UpAgentCom').value).length!=4){
					alert("上级机构录入错误，请查询！");
					fm.all('UpAgentCom').focus();
					return false;
				}
				if(fm.all('AgentCom').value.substring(0,4)!=fm.all('UpAgentCom').value){
					alert("该代理机构编码与上级机构编码不匹配！");
					fm.all('AgentCom').focus();
					return false;
				}
			}
			if(fm.all('BankType').value=="03"){
				//机构编码和级别的校验
				if(trim(fm.all('AgentCom').value).length!=8){
					alert("代理机构编码与机构级别不匹配，请查询！");
					fm.all('AgentCom').focus();
					return false;
				}
				//三级机构的上级机构编码6位
				if(trim(fm.all('UpAgentCom').value).length!=6){
					alert("上级机构录入错误，请查询！");
					fm.all('UpAgentCom').focus();
					return false;
				}
				if(fm.all('AgentCom').value.substring(0,6)!=fm.all('UpAgentCom').value){
					alert("该代理机构编码与上级机构编码不匹配！");
					fm.all('AgentCom').focus();
					return false;
				}
			}
		}
	}
	if(fm.all('AreaType').value==null || fm.all('AreaType').value==""){
		alert("请输入地区类型！");
		fm.all('AreaType').focus();
		return false;
	}
	if(fm.all('ChannelType').value==null || fm.all('ChannelType').value==""){
		alert("请输入渠道类型 ！");
		fm.all('ChannelType').focus();
		return false;
	}
	return true;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,100,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

//Click事件，当点击增加图片时触发该函数
function addClick(){
	//下面增加相应的代码
	mOperate="INSERT||MAIN";
	showDiv(operateButton,"false");
	showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick(){
	if ((fm.all("AgentCom").value==null)||(fm.all("AgentCom").value=='')){
		alert("请先确定代理机构！");
		fm.all('AgentCom').focus();
	}
	else{
		if (!queryAgentCom())
			return;
		if (confirm("您确实想修改该记录吗?")){
			mOperate="UPDATE||MAIN";
			submitForm();
		}
		else{
			mOperate="";
			alert("您取消了修改操作！");
		}
	}
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick(){
//	mOperate="QUERY||MAIN";
	showInfo=window.open("./LAGrpSpecQuery.html");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function queryAgentCom(){
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select AgentCom from LACom where 1=1 "
		+ getWherePart('AgentCom');
	//turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (!strQueryResult) {
		alert("不存在所要操作的代理机构！");
		fm.all("AgentCom").value = '';
		return false;
	}
	return true;
}

//用来显示返回的选项
function afterQuery(arrQueryResult){
  var arrResult = new Array();
  if( arrQueryResult != null ){
  	initInpBox();
    arrResult = arrQueryResult;
    //没有全部赋值
    fm.all('AgentCom').value = arrResult[0][0];
    fm.all('ManageCom').value = arrResult[0][1];
    fm.all('AreaType').value = arrResult[0][2];
    fm.all('ChannelType').value = arrResult[0][3];
    fm.all('UpAgentCom').value = arrResult[0][4];
    fm.all('Name').value = arrResult[0][5];
    fm.all('Address').value = arrResult[0][6];
    fm.all('Phone').value = arrResult[0][8];
    fm.all('Corporation').value=arrResult[0][14];
    fm.all('GrpNature').value = arrResult[0][18];
    fm.all('ACType').value = arrResult[0][19];
    fm.all('BankType').value = arrResult[0][26];
    getBankTypeName(fm.all('BankTypeName'));
    if (fm.all('UpAgentCom').value!=null&&fm.all('UpAgentCom').value!=''){
   	 getComName(fm.all('UpComName'));
  	}
  }
}

function afterCodeSelect( cCodeName, Field ){
	try{
		if (cCodeName=="yesno"){
			if (Field.value=="Y"){
				write.style.display="";
			}
			else{
				write.style.display="none";
			}
		}
	}
	catch(ex){}
}

function getComName(input){
	var sql = "select Name from LACom where 1=1"
		+ getWherePart('AgentCom','UpAgentCom');
	var strResult = easyQueryVer3(sql, 1, 0, 1);
	if (strResult){
		var arrDataSet = decodeEasyQueryResult(strResult);
		var tArr_Record = new Array();
		tArr_Record = chooseArray(arrDataSet,[0]);
		input.value = tArr_Record[0][0];
	}
	else{
		input.value="";
		fm.all('UpAgentCom').value="";
		fm.all('UpAgentCom').focus();
		alert("上级代理机构代码输入有误或没有此上级代理机构！");
	}
}

function getBankTypeName(input){
	var sql = "select CodeName from LDCode where CodeType='banktype' and Code = '"+fm.all('BankType').value+"'";
	var strResult = easyQueryVer3(sql, 1, 0, 1);
	if (strResult){
		var arrDataSet = decodeEasyQueryResult(strResult);
		var tArr_Record = new Array();
		tArr_Record = chooseArray(arrDataSet,[0]);
		input.value = tArr_Record[0][0];
	}
	else{
		input.value="";
	}
}
