<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAAMBranchGroupInput.jsp
//程序功能：个险中介团队录入
//创建日期：2011-1-1
//创建人  ：龙程彬
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");

  System.out.println("BranchType:"+BranchType);

%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var strsql="1 and codealias= #"+'2'+"#";
</script>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="./LAAMBranchGroupInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="./LAAMBranchGroupInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAMBranchGroupSave.jsp" method=post name=fm target="fraSubmit" >
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      个险中介渠道单位
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
       <TR  class= common>
          <TD  class= title>
            所属管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>=4" 
            ondblclick="return showCodeList('comcode1',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode1',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            onchange="return getBranchCodeInit();"
            ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
          </TD> 
            <TD class = title>
             渠道类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);"
             onkeyup="return showCodeListKey('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);" 
              verify="渠道类型|notnull&code:BranchType2" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>
        
         <TD class= title>
            中介团队代码
          </TD>
          <TD  class= input>
          	<Input class= 'readonly' readonly  name=BranchAttr  verify="中介团队代码|NOTNULL&len=10" maxlength=10>
          </TD>
          <TD  class= title>
            中介团队名称
          </TD>
          <TD  class= input>
            <Input class= common name=BranchName elementtype=nacessary verify="中介团队名称|NOTNULL">
          </TD>	 
        </TR>
        <TR  class= common>
	      <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddress >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcode verify="展业机构邮编|len<=6" >
          </TD>        
        </TR>
        <TR  class= common>
	      <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhone >
          </TD>
          
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' verify="成立日期|NOTNULL&Date" name=FoundDate format='short' elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='codeno' name=EndFlag verify="停业|code:yesno&NOTNULL" 
             ondblclick="return showCodeList('yesno',[this,EndFlagName],[0,1]);" 
             onkeyup="return showCodeListKey('yesno',[this,EndFlagName],[0,1]);"
             ><Input class=codename name=EndFlagName readOnly elementtype=nacessary>
          </TD>
	        <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate verify="停业日期|Date" format='short' onfocusout="return changeGroup();">
          </TD>
        </TR>       

        <tr class=common>
          <TD  class= title>
            成本中心编码
          </TD>
          <TD  class= input>
            <Input class= common name=CostCenter verify="成本中心编码|notnull&len=10"  elementtype=nacessary>
          </TD>
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Operator >
          </TD>
        </tr>
      </table>
    </Div>

    <Input type=hidden name=InsideFlag >
    <Input type=hidden name=InsideFlagName >
    <Input type=hidden name=BranchManager >
    <Input type=hidden name=BranchManagerName >
    <Input type=hidden name=UpBranchAttr >
    <Input type=hidden name=FieldFlag value=''>
    <Input type=hidden name=Operator_name >
    <Input type=hidden name=hideOperate value=''>
    <Input type=hidden name=tCostCenter value=''>
    <Input type=hidden name=BranchType>
    <Input type=hidden name=AgentGroup value='' >  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
    <Input type=hidden name=UpBranch value='' >  <!--上级机构代码，存储隐式机构代码 -->
    <Input type=hidden class='common' name=ManageComCode value='<%=tG.ManageCom%>'>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

