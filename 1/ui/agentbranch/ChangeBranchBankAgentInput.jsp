<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>
<%
//程序名称：ChangeBranchBankAgentInput.jsp                          
//程序功能：
//创建日期：2008-03-05 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String tOperator = tG.Operator;
	String BranchType=request.getParameter("BranchType");
	String BranchType2=request.getParameter("BranchType2");
	String CurrentDate= PubFun.getCurrentDate();   
%>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">
 function initDate(){
 	fm.MakeTime.value="<%=CurrentDate%>";
 	fm.Operator.value="<%=tOperator%>";
	fm.all('BranchType').value ='<%=BranchType%>';
	fm.all('BranchType2').value='<%=BranchType2%>'
 }
 var msql=" 1 and BranchLevel = #41# and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#01# and  (EndFlag <> #Y# or EndFlag is null) and (state<>#1# or state is null) ";
 var msql2=" 1 and BranchLevel = #42# and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#01#";	
</script>
<head >
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="ChangeBranchBankAgentInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="ChangeBranchBankAgentInit.jsp"%>
	<%@include file="../agent/SetBranchType.jsp"%>
	<%@include file="../common/jsp/ManageComLimit.jsp"%>
	<title></title>
</head>
<body  onload="initDate();initForm();initElementtype();" >
	<form action="./ChangeBranchBankAgentSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divChangeBranch);">
				</td>
				<td class=titleImg>被调整单位信息</td>
			</tr>
		</table>
		<Div  id= "divChangeBranch" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD  class= title>营业组代码</TD>
						<TD  class= input>
							<Input class=code name=AdjustBranchCode readonly ondblclick="return showCodeList('branchattr',[this,AdjustBranchName],[0,1],null,msql,1,1);" onkeyup="return showCodeListKey('branchattr',[this,AdjustBranchName],[0,1],null,msql,1,1);" verify="营业组代码|notnull&len=12"  >
						</TD>
						<TD  class= title>营业组名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=AdjustBranchName>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>营业组管理人员</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BranchManager>
						</TD>
						<TD  class= title>营业组管理人员姓名</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BranchManagerName>
						</TD>
					</TR>
				</table>
			</div>
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBankAgentGrid);">
						<td class=titleImg>
							营业组人员信息
						</td>
					</td>
				</tr>
			</table>
			<Div  id= "divBankAgentGrid" style= "display: ''">
				<table class=common>   
    			<tr class=common>
    		 		<td text-align:left colSpan=1>
      				<span id="spanBankAgentGrid" ></span>
     				</td>
    			</tr>    
   			</table>
   			<INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      		<INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      		<INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      		<INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
   			      
			</div>
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAimChangeBranch);">
						<td class=titleImg>
							目标单位信息
						</td>
					</td>
				</tr>
			</table>
			<Div  id= "divAimChangeBranch" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD class= title>目标机构代码</TD>
						<TD  class= input>
							<Input class=code name=AdjustAfterBranchCode  readonly ondblclick="return showCodeList('branchattr',[this,AdjustAfterBranchName,AdjustAfterAgentGroup],[0,1,2],null,msql2,1);" onkeyup="return showCodeListKey('branchattr',[this,AdjustAfterBranchName,AdjustAfterAgentGroup],[0,1,2],null,msql2,1);" verify="目标机构代码|notnull">
						</TD>
						<TD class= title>目标机构名称</TD>
						<TD  class= input>
							<Input class=readonly readonly  name=AdjustAfterBranchName verify="目标机构名称|notnull">
						</TD>
					</tr>
					<TR  class= common>
						<TD  class= title>调整日期</TD>
						<TD  class= input >
							<Input class='coolDatePicker' dateformat= 'short' name=AdjustDate verify="调整日期|notnull&Date" elementtype=nacessary>
						</TD>
						<TD  class= title>调动后机构代码</TD>
						<TD  class= input>
							<Input class=readonly  name=NewBranchAttr readonly>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>操作日期</TD>
						<TD  class= input>
							<Input class=readonly readonly name=MakeTime >
						</TD>
						<TD  class= title>操作员代码</TD>
						<TD  class= input>
							<Input class="readonly" readonly name=Operator><Input type=hidden name=optname>
						</TD>
					</TR>
				</table>
				
				<br>
				<!--input type=button class=common name=savea value='确认' onclick="return BranchConfirm()"-->
				<input type=button class=cssButton name=saveb value='保存' onclick="return submitSave()">
				<input type=button class=cssButton value='重置' onclick="clearGrid();">
			</div>
			<input type=hidden name=UpBranchAttr value=''>
			<input type=hidden name=hideAdjustAgentGroup value=''>
			<input type=hidden name=hideAdjustAgentName value=''><!--这里存储调动后的新营业组名称-->
			<input type=hidden name=hideAimAgentGroup value=''>
			<input type=hidden name=hideLevel value=''>
			<input type=hidden name=hideCom value=''>
			<input type=hidden name=hideUpBranch value=''>
			<input type=hidden name=AdjustAfterAgentGroup value=''>
			<input type=hidden name=AdjustAfterLevel value=''>
			<input type=hidden name=AdjustAfterAttr value=''>
			<input type=hidden name=BranchType value=''>
			<input type=hidden name=BranchType2 value=''>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
