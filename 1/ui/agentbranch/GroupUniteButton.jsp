<%
//*******************************************************
//* 程序名称：GroupUniteButton.jsp
//* 程序功能：页面中一般的增加，修改，删除，查询 按钮
//* 创建日期：2005-7-15 11:20
//* 更新记录：  更新人    更新日期     更新原因/内容
//*
//******************************************************
%>
<span id="operateButton">
	<table class="common" align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="执行合并"  TYPE=button onclick="return submitForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查询目标单位"  TYPE=button onclick="return queryAimClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查询被合并单位"  TYPE=button onclick="return queryUniteClick();">
			</td>
		</tr>
	</table>
</span>