<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
	//程序名称：LAResidentIncomeSave.jsp
	//程序功能：
	//创建日期：2014-3-17
	//创建人  ：zlw
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	LAResidentIncomeUI tLAResidentIncomeUI = new LAResidentIncomeUI();
	LAResidentIncomeSchema tLAResidentIncomeSchema = new LAResidentIncomeSchema();
	//===
	//输出参数
	CErrors mErrors = new CErrors();
	String tOperate = request.getParameter("hideOperate");
	tOperate = tOperate.trim();
	String tRela = "";
	String FlagStr = "Fail";
	String Content = "";
	GlobalInput tG = new GlobalInput();

	tG = (GlobalInput) session.getValue("GI");

	//代理机构,将数据存储在tLAResidentIncomeSchema中.
	tLAResidentIncomeSchema.setIdx(request.getParameter("idx"));
	tLAResidentIncomeSchema.setResidentType(request.getParameter("ResidentType"));
	tLAResidentIncomeSchema.setManageCom(request.getParameter("ManageCom"));
	tLAResidentIncomeSchema.setStartDate(request.getParameter("StartDate"));
	tLAResidentIncomeSchema.setEndDate(request.getParameter("EndDate"));
	tLAResidentIncomeSchema.setRecentPCDI(request.getParameter("RecentPCDI"));
	tLAResidentIncomeSchema.setRecentPCNI(request.getParameter("RecentPCNI"));
	tLAResidentIncomeSchema.setOperator(tG.Operator);

	VData tVData = new VData();
	FlagStr = "";
	tVData.add(tLAResidentIncomeSchema);
	tVData.add(tG);
	tVData.add(tOperate);

	try {
		System.out.println("处理数据+++++++");
		if (!tLAResidentIncomeUI.submitData(tVData, tOperate)) {
			// @@错误处理
			mErrors.copyAllErrors(tLAResidentIncomeUI.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAResidentIncomeSave";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			mErrors.addOneError(tError);
		}
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	if (!FlagStr.equals("Fail")) {
		mErrors = tLAResidentIncomeUI.mErrors;
		if (!mErrors.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Succ";
		} else {
			Content = " 保存失败，原因是:" + mErrors.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


