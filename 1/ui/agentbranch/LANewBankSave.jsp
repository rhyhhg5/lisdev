<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LANewBankSave.jsp
//程序功能：
//创建日期：2012-10-11
//创建人  ：gy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  System.out.println("~~~~~~~~~~~~~~~~~");
  LANewBankUI tLANewBankUI   = new LANewBankUI();
//输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("Code",request.getParameter("Code"));
  tTransferData.setNameAndValue("CodeName",request.getParameter("CodeName"));
  tTransferData.setNameAndValue("hideOperate",request.getParameter("hideOperate"));
	
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tTransferData);
 // System.out.println("与机构关系放入完毕111111111111111111111111111111111111222222!"+tLAComSchema.getUpAgentCom());
  try
  {
	  tLANewBankUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLANewBankUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
        System.out.println("no reord");
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

