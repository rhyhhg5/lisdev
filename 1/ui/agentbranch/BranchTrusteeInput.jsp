<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-03-20 18:07:04
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
 <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
 <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="BranchTrusteeInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BranchTrusteeInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BranchTrusteeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg >
        		团对托管信息
       		 </td>   		 
    	</tr>
    </table>
 <Div  id= "divBranchTrustee" style= "display: ''"> 
<table  class= common align='center'>
  <TR  class= common>
   <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
          ><Input class="codename" name=ManageComName  elementtype=nacessary > 
          </TD> 
  </TR>
  <TR>        
    <TD  class= title>
      被托管团队代码
    </TD>
    <TD  class= input>
      <Input class= 'common'   name=BranchAttr verify="托管团队代码|NOTNULL" OnChange="return checkBranchGroup()" elementtype=nacessary>
    </TD>
    <TD  class= title>
      托管团队名称
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly   name=BranchName  >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      主管代码
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly  name=BranchManager >
    </TD>
   
    <TD  class= title>
      主管姓名
    </TD>
    <TD  class= input>
     <Input class= 'readonly' readonly name=BranchManagerName  >
    </TD>                      
   
  </TR>
  <!--TR  class= common>
    <TD  class= title>
      请假记录顺序号
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=Idx >
    </TD>
    </TR>
  <TR--> 
    <TD  class= title>
      托管人代码
    </TD>
    <TD  class= input>
     <Input class= 'common'  name=AgentCode elementtype=nacessary  verify="托管人代码|NOTNULL" OnChange="return checkAgentCode()">
    </TD>
    <TD  class= title>
      托管人姓名
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly  name=AgentName >
    </TD>
     </TR>
 <TR  class= common>
   <TD  class= title>
      托管人职级
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=AgentGrade >
    </TD>
   
  </TR>
  <TR  class= common>
     
    <TD  class= title>
      托管起期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=StartDate  verify="托管起始时间|NOTNULL" elementtype=nacessary  >  
    </TD>
    <TD  class= title>
      托管止期
    </TD>
    <TD  class= input>
     <Input class= "coolDatePicker" dateFormat="short" name=EndDate  >  
    </TD>
  </TR>
  <TR  class= common>
   
    <TD  class= title>
      托管天数
    </TD>
    <TD  class= input>
     <Input class= 'readonly' readonly   name=TimeDistance >
    </TD>
    <TD  class= title>
      托管次数
    </TD>
    <TD  class= input>
     <Input class= 'readonly' readonly   name=IdxNo >
    </TD>
    
   </TR>
   <TR>
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
     <Input class= 'readonly' readonly   name=Operator >
    </TD>
    
    <TD  class= title>
      操作时间
    </TD>
    <TD  class= input>
     <Input class= 'readonly' readonly   name=MakeTime >
    </TD>
   </TR>
   
  </table>
  
   <!--table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGAppeal);">
            <td class= titleImg>
                请假主管信息查询
            </td>
            </td>
    	</tr>
     </table>
     
     
 <Div id= "divLGAppea2" style= "display: ''">
  <table class=common align='center'>
  
   <TR  class= common  >
     <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom1 verify="管理机构|code:comcode&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName1],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName1],[0,1],null,8,'char(length(trim(comcode)))',1);"
          ><Input class="codename" name=ManageComName1  > 
          </TD> 
   </TR>
   <TR  class= common>
    <TD  class= title>
      托管团队代码
    </TD>
    <TD  class= input>
      <Input class= 'common'  name=BranchAttr1 >
    </TD>
   
    <TD  class= title>
      托管团队名称
    </TD>
    <TD  class= input>
      <Input class= 'common'  name=BranchName1 >
    </TD>
   
   </TR>
   <TR  class= common>
    <TD  class= title>
      主管代码
    </TD>
    <TD  class= input>
     <Input class= 'common'  name=BranchManager1 >
    </TD>
   
    <TD  class= title>
      主管名称
    </TD>
    <TD  class= input>
     <Input class= 'common'  name=BranchManagerName1 >
    </TD>
    
   </TR>
    <TR  class= common>
    <TD  class= title>
      查询起期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=StartDate1  >  
    </TD>
   
    <TD  class= title>
      查询止期
    </TD>
    <TD  class= input>
     <Input class= "coolDatePicker" dateFormat="short" name=EndDate1 >
    </TD>
    
   </TR>
   
  <table>		
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查     询"   TYPE=button   class=common onclick="easyQueryClick();">				
      </TD>
    </TR>
  </table>
   </Div>
   <BR>
  <!-- 档案类型信息（列表）>
  	 <Div  id= "divLAAgent2" style= "display: ''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanTrusteeGrid">
  				</span> 
		    </td>
			</tr>
		</table>
    </div>
    	 <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"  class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"  class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"  class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"  class="cssButton"--> 
   


   
    <input type=hidden name=Idx value=''> 
    <input type=hidden name=AgentGroup value=''> 
    <input type=hidden name=UpIdxNo value=''> 
    <input type=hidden name=UpAgentCode value=''> 
    <input type=hidden name=BranchType value=''> 
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=hideOperate value=''>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>

</body>
</html>
