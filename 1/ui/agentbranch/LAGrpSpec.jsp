<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAGrpSpec.jsp
//程序功能：
//创建日期：2004-03-23
//创建人  ：zy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
%>
<script>
var manageCom = <%=tG.ManageCom%>;
</script>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LAGrpSpec.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAGrpSpecInit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAGrpSpecSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLASpecCom1);"></td>
        <td class=titleImg>代理机构信息</td>
      </tr>
    </table>
  <Div  id= "divLASpecCom1" style= "display: ''">
    <table  class= common>
      <tr  class= common>
        <td  class= title> 代理机构编码 </td>
        <td  class= input> <input class=common name=AgentCom elementtype=nacessary> </td>
        <td  class= title> 代理机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
      </tr>

      <tr  class= common>
        <td  class= title> 代理机构级别 </td>
        <td  class= input> <input class=code name=BankType ondblclick="return showCodeList('BankType',[this,BankTypeName],[0,1]);" onkeyup="return showCodeListKey('BankType',[this,BankTypeName],[0,1]);" elementtype=nacessary> </td>
        <td  class= title> 代理机构级别名称 </td>
        <td  class= input> <input class=readonly name=BankTypeName></td>
      </tr>
      <tr  class= common>
        <td  class= title> 上级代理机构 </td>
        <td  class= input> <input class=common name=UpAgentCom onchange="getComName(UpComName)"> </td>
	      <td  class= title> 上级机构名称 </td>
        <td  class= input> <input class='readonly' readonly name=UpComName > </td>
      </tr>
      <tr  class= common>
        <td  class= title> 地区类型 </td>
        <td  class= input> <input  class= code name=AreaType
                              ondblclick="return showCodeList('AreaType',[this]);"
                              onkeyup="return showCodeListKey('AreaType',[this]);" elementtype=nacessary> </td>
        <td  class= title> 渠道类型 </td>
        <td  class= input> <input class=code name=ChannelType
                              ondblclick="return showCodeList('ChannelType',[this]);"
                              onkeyup="return showCodeListKey('ChannelType',[this]);" elementtype=nacessary> </td>
      </tr>

      <tr  class= common>
        <td  class= title> 机构注册地址 </td>
        <td  class= input> <input  class= common name=Address> </td>
        <td  class= title> 代理单位负责人 </td>
        <td  class= input> <input class=common name=Corporation> </td>
      </tr>
      <tr class = common >
         <td  class= title> 代理机构电话 </td>
         <td  class= input> <input  class=common name=Phone> </td>
         <td  class= title> 单位性质 </td>
         <td  class= input> <input   name=GrpNature class='code'
		           ondblclick="return showCodeList('GrpNature',[this]);"
		           onkeyup="return showCodeListKey('GrpNature',[this]);"></td>
      </tr>
      <tr  class= common>
	    	<td  class= title> 负责管理机构 </td>
        <td  class= input> <input class='code' name=ManageCom verify="负责管理机构|code:comcode&NOTNULL"  ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);" elementtype=nacessary> </td>
        <td  class= title> 代理机构类别 </td>
        <td  class= input> <input class='code'  name=ACType
                ondblclick="return showCodeList('ACType',[this]);"
		onkeyup="return showCodeListKey('ACType',[this]);"> </td>
      </tr>
    </table>
  </Div>
     <input type=hidden name=hideOperate value=''>
     <input type=hidden name=HiddenAgentGroup value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
