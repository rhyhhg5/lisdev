//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm(){

  /*中英的系统不需要检测上级机构。
  if (!queryBranchAttr())
    return false;
  */

//  showSubmitFrame(1);

	if (mOperate!="UPDATE||MAIN"){
		//alert('here1');
		mOperate="INSERT||MAIN";
	}
		
	//如果是新增操作，则需要将主管编码和主管姓名清空，否则会传入后台，如果是更新操作，则不需要如此处理
	if (mOperate=="INSERT||MAIN"){
		fm.all('BranchManager').value = "";
		fm.all('BranchManagerName').value = "";
		
		if ((fm.all("ManageCom").value==null)||(trim(fm.all("ManageCom").value)==''))
    {
    	alert("请确定所属管理机构！");
    return false;
    }
		fm.all('BranchAttr').value = getBranchCodeInit();
	
	}

	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value==""){
		alert("操作控制数据丢失！");
	}
	if (!beforeSubmit())
		return false;
	if (!changeGroup())
		return false;
    fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
	mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroup.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
	if( verifyInput() == false ) return false;
	var cManageCom = fm.all('ManageCom').value;
	var cBranchAttr = fm.all('BranchAttr').value;
	//xjh Modify for PICCH,2005/2/17,修改使其一致
	if (cManageCom.substring(0,cManageCom.length) != cBranchAttr.substring(0,cManageCom.length)){
		alert("销售团队代码录入错误，请查询！");
		fm.all('BranchAttr').focus();
		return false;
	}
	if(cManageCom.length < 8)
	{
		var a = "0000000000000000";
		if(cBranchAttr.substring(cManageCom.length,8) != a.substring(cManageCom.length,8))
		{
		alert("销售团队代码录入错误，请查询！");
		fm.all('BranchAttr').focus();
		return false;
		}
	}
	//对成本中心编码进行校验
 	if(fm.ManageCom.value=="")
 	{
 		alert("所属管理机构不能为空！");
 		return false;
 	}
 	if(!checkSap(fm.CostCenter.value,fm.ManageCom.value))
 	{
 		return false;
 	}
	return true;

}


//对成本中心编码进行校验
function checkSap(CostCenter,tManageCom)
{
 var erroInfo="成本中心代码有误，请根据发文《人保健康计财[2008] 212号》申请成本中心代码！";
 var tt=CostCenter.substr(4,1);
 if(tt!="9")
 {
 	alert(erroInfo);
 	return false;
 } 



 var tSQL="select codealias from licodetrans  where codetype='ManageCom' and code='"+tManageCom+"'";
 var strQueryResult1 = easyQueryVer3(tSQL, 1, 1, 1); 	  
 var arr1 = decodeEasyQueryResult(strQueryResult1);
 if(!arr1)
 {
 	alert("SAP没有定义公司编码，请联系SAP定义公司编码！"); 
 	return false;
 }else{
 	var ss=arr1[0][0]
 	if(ss!=CostCenter.substr(0,4))
 	{
 	alert(erroInfo);
 	return false;
	 }
 }
 return 1;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,500,82,*";
  }
  else
  {
 	parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
  fm.all('BranchManager').value ='';
  fm.all('BranchManagerName').value ='';
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (!((fm.all("EndDate").value==null)||(trim(fm.all("EndDate").value)==''))&&(trim(fm.all("EndFlag").value)=='N'))
    {alert("停业属性为N，不能置停业日期！");
    return false;}

  //下面增加相应的代码

  if ((fm.all("BranchAttr").value==null)||(trim(fm.all("BranchAttr").value)==''))
    alert("请确定要修改的销售团队！");
  else
  {
    if (!queryBranchCode())
      return false;

    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }

}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";
//alert("BranchType2:"+fm.all('BranchType2').value);
//alert("./LAMediBranchGroupQueryHtml.jsp?BranchType="+fm.all('BranchType').value+"&BranchType2="+fm.all('BranchType2').value+"");
  showInfo=window.open("./LAMediBranchGroupQueryHtml.jsp?BranchType="+fm.all('BranchType').value+"&BranchType2="+fm.all('BranchType2').value+"");
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function afterQuery(arrQueryResult)
{
	// 初始化页面
	initForm();
	
  var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('AgentGroup').value = arrResult[0][0];
		//alert(arrResult[0][0]);
		fm.all('BranchName').value = arrResult[0][1];
		fm.all('ManageCom').value = arrResult[0][2];
		fm.all('UpBranch').value = arrResult[0][3];
        fm.all('BranchAttr').value = arrResult[0][4];
		fm.all('BranchType').value = arrResult[0][5];
	//	fm.all('InsideFlag').value = arrResult[0][6];
		fm.all('BranchManager').value = arrResult[0][7];
		fm.all('BranchAddress').value = arrResult[0][8];
		fm.all('BranchPhone').value = arrResult[0][9];
		fm.all('BranchZipcode').value = arrResult[0][10];
		fm.all('FoundDate').value = arrResult[0][11];
		fm.all('EndDate').value = arrResult[0][12];
		fm.all('EndFlag').value = arrResult[0][13];
		fm.all('FieldFlag').value = (arrResult[0][14]=="1"?"Y":"N");
		fm.all('BranchManager').value = arrResult[0][19];
		fm.all('BranchManagerName').value = arrResult[0][20];
		fm.all('InsideFlag').value = arrResult[0][21];
		fm.all('BranchType2').value=arrResult[0][22];
		fm.all('BranchType2Name').value=arrResult[0][23];
		fm.all('ManageComName').value=arrResult[0][24];
		fm.all('CostCenter').value=arrResult[0][25];
		fm.all('tCostCenter').value=arrResult[0][25];
		//fm.all('BranchName').value='aaa';
		if(arrResult[0][13]=="Y"){
			fm.all('EndFlagName').value="是";
		}else if(arrResult[0][13]=="N"){
			fm.all('EndFlagName').value="否";
		}
	}
	if (arrResult[0][6]!="1")
	{
		var strSQL = "";
  		strSQL = "select BranchAttr from LABranchGroup where agentgroup='"+arrResult[0][3]+"'  and (state<>'1' or state is null) ";
	       	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	       	if (strQueryResult)
	       	{
	       		var arr = decodeEasyQueryResult(strQueryResult);
	       		fm.all('UpBranchAttr').value=arr[0][0];
	       	}
	}
	else
  {
    fm.all('UpBranchAttr').value="";
  }
	fm.all('Operator').value = arrResult[0][15];
	
	if (arrResult[0][21]=="1")
	{
		fm.all('InsideFlagName').value = "外勤";
	}
		if (arrResult[0][21]=="0")
	{
		fm.all('InsideFlagName').value = "内勤";
	}
	
	//fm.all('BranchManagerName').value = arrResult[0][16];
}

function queryBranchCode()
{
  // 书写SQL语句
  var strSQL = "";
  //alert(document.fm.BranchType.value);
  strSQL = "select AgentGroup,UpBranch from LABranchGroup where 1=1 and (EndFlag is null or EndFlag <> 'Y') and (state<>'1' or state is null) "
	  + getWherePart('BranchAttr') + " and BranchType='"+document.fm.BranchType.value+"' and branchtype2='"+document.fm.BranchType2.value+"' ";

  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  if (!strQueryResult) {
    alert("不存在所要操作的销售单位！");
    fm.all("BranchAttr").value = '';
    fm.all('AgentGroup').value = '';
    fm.all('UpBranch').value = '';
    return false;
  }
  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentGroup').value = arr[0][0];
  fm.all('UpBranch').value = arr[0][1];
  return true;
}

function queryBranchAttr()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select AgentGroup from LABranchGroup where 1=1 and (EndFlag is null or EndFlag <> 'Y') and (state<>'1' or state is null) "
	  + getWherePart('BranchAttr','UpBranchAttr') + " and BranchType='"+document.fm.BranchType.value+"' and BranchType2='"+document.fm.BranchType2.value+"'";

  var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  if (!strQueryResult) {
    alert("不存在上级销售团队！");
    fm.all("BranchAttr").value = '';
    fm.all('AgentGroup').value = '';
    fm.all('UpBranch').value = '';
    return false;
  }

  if (((fm.all("EndDate").value==null)||(trim(fm.all("EndDate").value)==''))&&(trim(fm.all("EndFlag").value)=='Y'))
    {  alert("停业属性为Y，请置停业日期！");
    return false;  }


  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('UpBranch').value = arr[0][0];
  return true;
}

function changeGroup()
{
	if(fm.all('EndFlag').value == null || fm.all('EndFlag').value == "")
	{
		//alert("停业属性为空，请置位！");
		return false;
	}
	if(fm.all('EndFlag').value == 'Y' && (fm.all('EndDate').value == null || fm.all('EndDate').value == ""))
  {
  	alert("停业属性为Y，请置停业日期！");
    return false;
  }
	if(fm.all('EndFlag').value == 'N' && fm.all('EndDate').value != "")
  {  
  	alert("停业属性为N，不能置停业日期！");
  	fm.all('EndDate').value = "";
    return false;
  }
  return true;
}

/*******************************************
 * 自动给用户推荐一个机构外部编码
 *******************************************/
function getBranchCodeInit()
{
	var tSQL = "";
//	var ManageComCode = document.fm.ManageComCode.value;
  var ManageComCode=fm.all('ManageCom').value;
	
	tSQL = "select max(branchattr) from labranchgroup where branchtype='" 
	     + document.fm.BranchType.value + "' and branchtype2='" +document.fm.BranchType2.value
	     + "' and managecom like '"+ManageComCode+"%25'";
	
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);

	if (!strQueryResult)
	{
		return ManageComCode + "01";
	}
  
	var arr = decodeEasyQueryResult(strQueryResult);
	
	var tBranchAttrNew = arr[0][0];
	if(tBranchAttrNew == "" || tBranchAttrNew == null)
	{
		return ManageComCode + "01";
	}
	
	var tGroupNo = parseInt(tBranchAttrNew.substring(tBranchAttrNew.length-1)) + 1;
	
	var tBranchAttr = tBranchAttrNew.substring(0,tBranchAttrNew.length-2) + ((100 + tGroupNo)+"").substring(1,3);
	
	return tBranchAttr;
}