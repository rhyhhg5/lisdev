<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {

        initBatchDetailGrid();
        fm.all('ManageCom').value = <%=strManageCom%>;
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}



function initBatchDetailGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="申请机构";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="申请日期";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="申请人";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="批次状态";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;

        BatchDetailGrid = new MulLineEnter("fm", "BatchDetailGrid"); 

        BatchDetailGrid.mulLineCount = 0;   
        BatchDetailGrid.displayTitle = 1;
        BatchDetailGrid.canSel = 1;
        BatchDetailGrid.hiddenSubtraction = 1;
        BatchDetailGrid.hiddenPlus = 1;
        BatchDetailGrid.canChk = 0;
        BatchDetailGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化BatchDetailGrid时出错：" + ex);
    }
}
</script>