//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var succFlag = false;
window.onfocus=myonfocus;
//parent.fraMain.rows = "0,0,0,0,*";
var ConfirmFlag = false;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

function queryInsured(){
    var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
    showInfo = window.open( "../sys/LDPersonQueryNew2.jsp?Flag=insured","",sFeatures);  
}

//保存险种信息
function savePolInfo(){

   if(!CheckBnfDate()){
   return false;
   }
   if(!checkWrap()){
   return;
   }
   
   
   
   
   //校验被保人信息
   var tSql="select count(1) from lcinsured where prtno='"+prtNo+"'";
   var arr = easyExecSql(tSql);

   if(arr){
    if(arr==0 || arr[0][0] == 0){
    alert("请先保存被保人信息!");
    return;
    }
   }else{
    alert("请先保存被保人信息!");
    return;
   }
   
  //被保人信息校验
   if(!CheckInSuredsInfo()){
   return ;
   }
    var i = 0;
    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmAction.value = "INSERT||MAIN";
    fm.action = "./FamilySingleContSave.jsp";
    fm.submit();

}

//校验被保人信息(年龄)
function CheckInSuredsInfo(){
    
    var tbooleanflag=true;
    var tcontent="";
    var tSql = " select Birthday,InsuredNo,Name from lcinsured where contno='"+ContNo+"' ";
    
    var arr=easyExecSql(tSql);
    if(arr!="" && arr !=null ){
     //家庭成员年龄校验
     if(arr.length <= 2){//家庭成员小于3个人
        for(var i=0;i<arr.length ;i++){
           var currdate=getCurrentDate("-");
           var tbirthday=arr[i][0];
           var tdatediff=dateDiff(tbirthday,currdate,"D")
           var tage=calAge(tbirthday);
           if(tdatediff < 28 || tage > 55){
            tcontent+=""+arr[i][2]+"的年龄不符合投保要求,俩人家庭投保年龄为出生满28天—55周岁!\n";
            tbooleanflag=false;
           }
           
        }
     }else if(arr.length > 2  ){ //家庭成员3人及3人以上
        for(var i=0;i<arr.length ;i++){
           var currdate=getCurrentDate("-");
           var tbirthday=arr[i][0];
           var tdatediff=dateDiff(tbirthday,currdate,"D")
           var tage=calAge(tbirthday);
           if(tdatediff < 28 || tage > 65){
            tcontent+=""+arr[i][2]+"的年龄不符合投保要求,3及3人以上的家庭投保年龄为出生满28天—65周岁!\n";
            tbooleanflag=false;
           }
           
        }
     }
     //校验套餐份额
     for(var i=0;i<arr.length;i++){
        var StrSql="  select distinct lcr.calfactorvalue,lcr.prtno from lcriskdutywrap lcr where insuredno='"+arr[i][1]+"' and lcr.riskwrapcode='WR0296' and lcr.calfactor='Copys' and lcr.prtno <> '"+prtNo+"' and exists (select 1 from lccont  where prtno=lcr.prtno and uwflag<>'a' and stateflag in ('0','1') ) ";
        var StrSqlMax=" select Maxcopys from lDwrap where riskwrapcode = 'WR0296' and wraptype='10'";
        var arrprtno=easyExecSql(StrSql);
        var arrMax=easyExecSql(StrSqlMax);
        if(arrprtno!="" && arrprtno!=null && arrMax!="" && arrMax!=null){
        var tCopys=0;
        for(var j=0;j<arrprtno.length;j++){
        tCopys=parseInt(tCopys)+ parseInt(arrprtno[j][0]);
        }
        tCopys=tCopys+parseInt(RiskWrapGrid.getRowColData(i,12));
        if(tCopys > arrMax[0][0]){
        tcontent+="被保人"+arr[i][2]+"购买此套餐已达上限,最多"+arrMax[0][0]+"份,不能继续购买!";
        tbooleanflag = false;
        }
        }
        
     }
     
    }
    
    
    if(!tbooleanflag){
    alert(tcontent);
    return false;
    }else{
    return true;
    }
    
}

//检查是否选择了险种

function checkWrap(){
   var tcheno=RiskWrapGrid.getChkNo;
   if(tcheno=="" || tcheno == null){
   alert("请选择套餐!");
   return false;
   }
   
   var tcontline=RiskWrapGrid.mulLineCount;
   for(var i=0;i<tcontline;i++){
   var tcopys=RiskWrapGrid.getRowColData(i,12);
   if(tcopys!=""){
   if(isNaN(tcopys)){
   alert("套餐份数必须是数字!");
   return false;
   }
   
   if(tcopys >2 || tcopys<1){
   alert("录入的套餐份额错误,套餐份额必须是大于0小于等于2!");
   return false;
   }
   
   }else{
   RiskWrapGrid.setRowColData(i,12,"1");
   }
   
   
   }
   return true;
}

function afterQueryInsured(arrResult){
    if(arrResult){
        var CustomerNo = arrResult[0][0];
        var strSql = "select a.name,a.sex,a.birthday,a.englishname,a.idtype,a.IDNo,a.CustomerNo,(select codename from ldcode where codetype='idtype' and code=a.idtype) idtypename,(select codename from ldcode where codetype='sex' and code=a.sex) sexname,nativeplace,nativecity from ldperson a where a.CustomerNo='"+CustomerNo+"' ";
        var arr = easyExecSql(strSql);
        if(arr){

                try{fm.all('insured_Name').value=arr[0][0];}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Sex').value=arr[0][1];}catch(ex){alert(ex.message)};      
                try{fm.all('insured_SexName').value=arr[0][8];}catch(ex){alert(ex.message)};        
                try{fm.all('insured_Birthday').value=arr[0][2];}catch(ex){alert(ex.message)};   
                try{fm.all('insured_IDType').value=arr[0][4];}catch(ex){alert(ex.message)};          
                try{fm.all('insured_IDTypeName').value=arr[0][7];}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_IDNo').value=arr[0][5];}catch(ex){alert(ex.message)};     
                try{fm.all('insured_InsuredNo').value=arr[0][6];}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_NativePlace').value=arr[0][9];}catch(ex){alert(ex.message)};
                try{fm.all('insured_NativeCity').value=arr[0][10];}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_NativePlaceName').value="";}catch(ex){alert(ex.message)};  
                var strSql = "select a.PostalAddress,a.postalprovince , a.postalcity , a.postalcounty ,a.postalstreet ,a.postalcommunity,a.HomePhone,a.Mobile,a.CompanyPhone,a.EMail,a.AddressNo,a.zipcode,a.Phone,GrpName from LCaddress a where a.CustomerNo='"+CustomerNo+"'  order by AddressNo desc";
                var arr = easyExecSql(strSql);
            if(arr){
            	var rs1 = easyExecSql("select codename from ldcode1 where codetype='province1' and code='"+arr[0][1]+"' and code1 ='0'");
            	var rs2 = easyExecSql("select codename from ldcode1 where codetype='city1' and code='"+arr[0][2]+"' and code1 ='"+arr[0][1]+"'");
            	var rs3 = easyExecSql("select codename from ldcode1 where codetype='county1' and code='"+arr[0][3]+"' and code1 ='"+arr[0][2]+"'");
            	if(rs1 != null){
            		try{fm.all('Province').value=arr[0][1];}catch(ex){alert(ex.message)};   
            		try{fm.all('City').value=arr[0][2];}catch(ex){alert(ex.message)};   
            		try{fm.all('County').value=arr[0][3];}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalProvince').value=rs1;}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCity').value=rs2;}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCounty').value=rs3;}catch(ex){alert(ex.message)};   
            	}else{
            		var rs4 = easyExecSql("select code from ldcode1 where codetype='province1' and codename ='"+arr[0][1]+"'");
            		var rs5 = easyExecSql("select code from ldcode1 where codetype='city1' and codename ='"+arr[0][2]+"'");
            		var rs6 = easyExecSql("select code from ldcode1 where codetype='county1' and codename ='"+arr[0][3]+"'");
            		try{fm.all('Province').value=rs4;}catch(ex){alert(ex.message)};   
            		try{fm.all('City').value=rs5;}catch(ex){alert(ex.message)};   
            		try{fm.all('County').value=rs6;}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalProvince').value=arr[0][1];}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCity').value=arr[0][2];}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCounty').value=arr[0][3];}catch(ex){alert(ex.message)};   
            	}
            	try{fm.all('insured_PostalStreet').value=arr[0][4];}catch(ex){alert(ex.message)};   
            	try{fm.all('insured_PostalCommunity').value=arr[0][5];}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_PostalAddress').value=arr[0][0];}catch(ex){alert(ex.message)};   
                try{fm.all('insured_HomePhone').value=arr[0][6];}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Mobile').value=arr[0][7];}catch(ex){alert(ex.message)};          
                try{fm.all('insured_CompanyPhone').value=arr[0][8];}catch(ex){alert(ex.message)};    
                try{fm.all('insured_EMail').value=arr[0][9];}catch(ex){alert(ex.message)};          
                try{fm.all('insured_ZipCode').value=arr[0][11];}catch(ex){alert(ex.message)};      
                try{fm.all('insured_Phone').value=arr[0][12];}catch(ex){alert(ex.message)};
                try{fm.all('insured_WorkName').value=arr[0][13];}catch(ex){alert(ex.message)};
            }else{
                try{fm.all('insured_PostalAddress').value="";}catch(ex){alert(ex.message)};   
                try{fm.all('insured_HomePhone').value="";}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Mobile').value="";}catch(ex){alert(ex.message)};          
                try{fm.all('insured_CompanyPhone').value="";}catch(ex){alert(ex.message)};    
                try{fm.all('insured_EMail').value="";}catch(ex){alert(ex.message)};          
                try{fm.all('insured_ZipCode').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_Phone').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_WorkName').value="";}catch(ex){alert(ex.message)};
            }
        }
    }
    showAllCodeName();
	controlNativeCity("");
    
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  fm.insuredNo.value="";
  if(fm.fmAction.value == "DELETE||MAIN" && FlagStr == "Success"){
  	content="删除成功！";
	}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    ConfirmFlag = false;
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    ConfirmFlag = true;
    clearInsured();
    queryInsuredInit();
    queryRiskWrap();
    initWrapCheck();
  }
 
}

//校验被保人客户信息

function checkInsuredInfo(){
    if(fm.insured_ZipCode.value!="" && fm.insured_ZipCode.value!=null){
         var zipcode=fm.insured_ZipCode.value;
         if(zipcode.length > 6){
           alert("邮政编码过长最大长度是6位!");
           return false;
         }
    }
    
    if(fm.insured_OccupationType.value != "" && fm.insured_OccupationType.value !=null){
         var tOccupationType = fm.insured_OccupationType.value;
         var tsql="  select Maxoccutype from lDwrap where riskwrapcode = 'WR0296' and wraptype='10'";
         var arr=easyExecSql(tsql);
         if(arr !="" && arr.length >0){
           if(tOccupationType > arr[0][0] ){
            alert("职业类型不符合要求,最大类型值为"+arr[0][0]+"");
            return false;
           }
         }
    }
    
    return true;
}

//校验客户与客户号匹配

function checkInsuredandNo(){

   if(fm.insured_InsuredNo.value!="" && fm.insured_InsuredNo.value!=null){
    var tSql=" select customerno from ldperson where customerno='"+fm.insured_InsuredNo.value+"' and name ='"+fm.insured_Name.value+"' and sex='"+fm.insured_Sex.value+"' and idtype='"+fm.insured_IDType.value+"' and idno='"+fm.insured_IDNo.value+"' and birthday='"+fm.insured_Birthday.value+"'";
    var arr=easyExecSql(tSql);
    if(arr =="" || arr==null){
     alert("客户信息与客户号不匹配,请检查录入信息!");
     return false;
    }
   }
  return true;
}

//增加被保人

function addInsured(){
	
	//增加被保人身份证号信息校验 20170828
   var tFlag = checkIdNo(fm.insured_IDType.value,fm.insured_IDNo.value,fm.insured_Birthday.value,fm.insured_Sex.value);
   if(tFlag != ""){
	   alert(tFlag);
		return false;
   }

   fm.insured_PostalAddress.value=fm.insured_PostalProvince.value+fm.insured_PostalCity.value+fm.insured_PostalCounty.value+fm.insured_PostalStreet.value+fm.insured_PostalCommunity.value;
  var tSqlnum=" select count(1) from lcinsured where contno='"+ContNo+"'";
  
  var arrnum=easyExecSql(tSqlnum);
  if(arrnum!=null && arrnum!=""){
    if(arrnum[0][0] ==6 ){
     alert("不能新增被保人,被保人已满额最多6人!");
     return;
    }
  }
  
  if(fm.insured_RelationToAppnt.value=="" || fm.insured_RelationToAppnt.value==null){
    alert("与投保人关系不能为空!");
     return ;
   }
   
   if(fm.insured_RelationToMainInsured.value=="" || fm.insured_RelationToMainInsured.value==null){
    alert("与主被保险人关系不能为空!");
    return ;
   }
   
    //校验投保人与被保人的关系
   if(!checkappntrelation()){
   return;
   }
   
   if(!checkAllInsu('')){
      return false;
   }
  
  if(fm.insured_InsuredNo.value!="" && fm.insured_InsuredNo.value!=null){
     var tSql=" select InsuredNo from LCInsured where ContNo='"+ContNo+"' and InsuredNo ='"+fm.insured_InsuredNo.value+"'";
      var arr = easyExecSql(tSql);
  if(arr!=null && arr[0][0] !="" ){
     alert("被保人客户号为:"+fm.insured_InsuredNo.value+"的客户已存在本保单下!");
     return ;
  }
  }
  
  if(fm.insured_RelationToAppnt.value!="" && fm.insured_RelationToAppnt.value!="00"){
  
   if(fm.insured_RelationToAppnt.value!="00"){
   if(fm.insured_Name.value=="" || fm.insured_Name.value==null){
    alert("被保人姓名不能为空!");
     return ;
   }else{
      var name = trim(fm.insured_Name.value);
      if(name.length<2){
         alert("被保人姓名需大于一个字符");
         return false;
      }
   }
   
   if(fm.insured_IDType.value=="" || fm.insured_IDType.value==null){
    alert("被保人证件类型不能为空!");
     return ;
   }
   
   
   if(fm.insured_IDNo.value=="" || fm.insured_IDNo.value==null){
    alert("被保人证件号码不能为空!");
     return ;
   }
   
   if(fm.insured_Sex.value=="" || fm.insured_Sex.value==null){
    alert("被保人性别不能为空!");
     return ;
   }
   
   
   if(fm.insured_Birthday.value=="" || fm.insured_Birthday.value==null){
    alert("被保人出生日期不能为空!");
     return ;
   }
   
//   if(fm.insured_PostalAddress.value=="" || fm.insured_PostalAddress.value==null){
//    alert("被保人联系地址不能为空!");
//     return ;
//   }
   
  //校验被保人客户信息
    if(!checkinsuredid()){
     return ;
     }
     
     if(!checkInsuredInfo()){
     return;
     }
   
   }
  }
  
  if(fm.insured_RelationToAppnt.value=="00"){
   var tSql=" select i.InsuredNo from lcinsured i where i.contno ='"+ContNo+"' and insuredno= (select c.AppntNo from lccont c where c.contno='"+ContNo+"')";
    var arr = easyExecSql(tSql);
  if(arr!=null && arr[0][0] !="" ){
     alert("被保人客户号为:"+arr[0][0]+"的客户已存在本保单下!");
     return ;
    }
  }
  
  if(!checkInsuredandNo()){
  return false;
  }
  
    var i = 0;
    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmAction.value = "INSERT||MAIN";
    fm.action = "./FamilyInsuredSingleContSave.jsp";
    fm.submit(); //提交

}


//校验投保人与被保人关系

function checkappntrelation(){

     if(fm.insured_RelationToAppnt.value!=""){
        var trelation=fm.insured_RelationToAppnt.value;
        if(trelation!="00" && trelation!="01" && trelation!="02" && trelation !="03" && trelation!="04" && trelation!="05" && trelation!="06" && trelation !="07"&& trelation!="08" && trelation!="09" && trelation !="11"){
         alert("被保人与投保人关系只能是本人、父母、子女、配偶的关系!");
         return false;
        }
     }
     return true;
}

//校验被保人身份证和生日性别
function checkinsuredid(){
if(fm.insured_IDType.value == "0"){
  
    try{
    
    if(!checkIdCard(fm.insured_IDNo.value)){
            return false;
        }
        var birthday = fm.insured_Birthday.value;
        var sex = fm.insured_Sex.value;
        var id=fm.insured_IDNo.value;
        if(birthday!=getBirthdatByIdNo(id)){
          alert("被保人" + fm.insured_Name.value + "身份证号与出生日期不符，请核查！");
          return false;
        }
        
        if(sex != getSexByIDNo(id)){
          alert("被保人" + fm.insured_Name.value + "身份证号与性别不符，请核查！");
          return false;
        }
        
    }catch(e){
     alert(e.message);
    }  
    }
    return true;
  }
    
//更新被保人
function updateInsured()
{
	//增加被保人身份证号信息校验 20170828
	   var tFlag = checkIdNo(fm.insured_IDType.value,fm.insured_IDNo.value,fm.insured_Birthday.value,fm.insured_Sex.value);
	   if(tFlag != ""){
		   alert(tFlag);
			return false;
	   }
	  fm.insured_PostalAddress.value=fm.insured_PostalProvince.value+fm.insured_PostalCity.value+fm.insured_PostalCounty.value+fm.insured_PostalStreet.value+fm.insured_PostalCommunity.value;

      var tno=InsuredGrid.getSelNo();
      
      if(tno==0){
      alert("请先选择一条被保人信息!");
      return;
      }
      
     
     if(fm.insured_RelationToAppnt.value=="" || fm.insured_RelationToAppnt.value==null){
     alert("与投保人关系不能为空!");
     return ;
     }
   
     if(fm.insured_RelationToMainInsured.value=="" || fm.insured_RelationToMainInsured.value==null){
      alert("与主被保险人关系不能为空!");
      return ;
     }
   
     //校验投保人与被保人的关系
     if(!checkappntrelation()){
      return;
      }
      
      if(!checkAllInsu('1')){
         return false;
      }
   
   
    if(fm.insured_Name.value=="" || fm.insured_Name.value==null){
    alert("被保人姓名不能为空!");
     return ;
     }else{
      var name = trim(fm.insured_Name.value);
      if(name.length<2){
         alert("被保人姓名需大于一个字符");
         return false;
      }
   }
   
    if(fm.insured_IDType.value=="" || fm.insured_IDType.value==null){
     alert("被保人证件类型不能为空!");
     return ;
     }
   
    if(fm.insured_IDNo.value=="" || fm.insured_IDNo.value==null){
    alert("被保人证件号码不能为空!");
     return ;
     }
   
    if(fm.insured_Sex.value=="" || fm.insured_Sex.value==null){
    alert("被保人性别不能为空!");
     return ;
     }
   
    if(fm.insured_Birthday.value=="" || fm.insured_Birthday.value==null){
    alert("被保人出生日期不能为空!");
     return ;
     }
   
//    if(fm.insured_PostalAddress.value=="" || fm.insured_PostalAddress.value==null){
//    alert("被保人联系地址不能为空!");
//     return ;
//     }
   

     if(!checkinsuredid()){
     return;
     }
    
    if(!checkInsuredandNo()){
     return false;
     }
     
     if(!checkInsuredInfo()){
     return;
     }
     
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmAction.value = "UPDATE||MAIN";
    fm.action = "./FamilyInsuredSingleContSave.jsp";
    
    fm.submit(); //提交
  
}

//删除被保人
function deleteInsured()
{
     
      var tno=InsuredGrid.getSelNo();
      
      if(tno==0){
      alert("请先选择一条被保人信息!");
      return;
      }
      
      var tInsuredno=InsuredGrid.getRowColData(tno-1,1);
      if(tInsuredno=="" || tInsuredno==null){
      alert("请选择有效的行!");
      return;
      }
      
      if(fm.insured_InsuredNo.value=="" || fm.insured_InsuredNo.value==null){
     alert("被保人客户号不能为空!");
     return ;
     }
      
      if(!checkInsuredandNo()){
      return false;
      }
    
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmAction.value = "DELETE||MAIN";
    fm.action = "./FamilyInsuredSingleContSave.jsp";
    
    fm.submit(); //提交
  
}

//查询被保人信息

function clickSelBox(){

  document.getElementById("InsuredInfo").style.display="";
  var tno=InsuredGrid.getSelNo();

  var tInsuredno=InsuredGrid.getRowColData(tno-1,1);
  fm.insuredNo.value=tInsuredno;
  if(tInsuredno=="" || tInsuredno==null){
  alert("请选择有效的行!");
  return;
  }
  
  var tSql="select Contno from lccont where prtno='"+prtNo+"'";
  var arr = easyExecSql(tSql);
  if(arr){
  QueryInsured(tInsuredno,arr[0][0]);
  }else{
  alert("查询保单信息出错!");
  return;
  }
  
    
}

//反显被保人信息

function QueryInsured(Insuredno,contno){
    if(Insuredno){
        var CustomerNo = Insuredno;
        var strSql = "select a.name,a.sex,a.birthday,a.englishname,a.idtype,a.IDNo,a.InsuredNo,(select codename from ldcode where codetype='idtype' and code=a.idtype) idtypename,(select codename from ldcode where codetype='sex' and code=a.sex) sexname,a.IDStartDate,a.IDEndDate,a.Salary,a.OccupationType,a.OccupationCode,(select b.OccupationName from LDOccupation b where OccupationCode=a.OccupationCode  and OccupationType=a.OccupationType),a.RelationToAppnt,(select codename from ldcode where codetype='relation' and code=a.RelationToAppnt),a.RelationToMainInsured,(select codename from ldcode where codetype='relation' and code=a.RelationToMainInsured),Position,nativeplace,nativecity from lcinsured a where a.InsuredNo='"+CustomerNo+"' and a.ContNo='"+contno+"' ";
        var arr = easyExecSql(strSql);
        if(arr){

                try{fm.all('insured_Name').value=arr[0][0];}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Sex').value=arr[0][1];}catch(ex){alert(ex.message)};      
                try{fm.all('insured_SexName').value=arr[0][8];}catch(ex){alert(ex.message)};        
                try{fm.all('insured_Birthday').value=arr[0][2];}catch(ex){alert(ex.message)};   
                try{fm.all('insured_IDType').value=arr[0][4];}catch(ex){alert(ex.message)};          
                try{fm.all('insured_IDTypeName').value=arr[0][7];}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_IDNo').value=arr[0][5];}catch(ex){alert(ex.message)};     
                try{fm.all('insured_InsuredNo').value=arr[0][6];}catch(ex){alert(ex.message)};  
                
                try{fm.all('IDStartDate').value=arr[0][9];}catch(ex){alert(ex.message)};  
                try{fm.all('IDEndDate').value=arr[0][10];}catch(ex){alert(ex.message)};  
                try{fm.all('insured_Salary').value=arr[0][11];}catch(ex){alert(ex.message)};
                
                try{fm.all('insured_OccupationType').value=arr[0][12];}catch(ex){alert(ex.message)};
                try{fm.all('insured_OccupationCode').value=arr[0][13];}catch(ex){alert(ex.message)};
                try{fm.all('insured_OccupationCodeName').value=arr[0][14];}catch(ex){alert(ex.message)}; 
                
                try{fm.all('insured_RelationToAppnt').value=arr[0][15];}catch(ex){alert(ex.message)};
                try{fm.all('insured_RelationToAppntName').value=arr[0][16];}catch(ex){alert(ex.message)};
                try{fm.all('insured_RelationToMainInsured').value=arr[0][17];}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_RelationToMainInsuredName').value=arr[0][18];}catch(ex){alert(ex.message)};
                try{fm.all('insured_Position').value=arr[0][19];}catch(ex){alert(ex.message)};
                try{fm.all('insured_NativePlace').value=arr[0][20];}catch(ex){alert(ex.message)};
                try{fm.all('insured_NativeCity').value=arr[0][21];}catch(ex){alert(ex.message)};
                try{fm.all('insured_NativePlaceName').value="";}catch(ex){alert(ex.message)};  
                var strSql = "select a.PostalAddress,a.postalprovince , a.postalcity , a.postalcounty ,a.postalstreet ,a.postalcommunity,a.HomePhone,a.Mobile,a.CompanyPhone,a.EMail,a.AddressNo,a.zipcode,a.Phone,GrpName from LCaddress a where a.CustomerNo='"+CustomerNo+"' and a.AddressNo=(select i.AddressNo from lcinsured i where i.InsuredNo='"+CustomerNo+"' and i.ContNo='"+ContNo+"')  order by AddressNo desc";
                var arr = easyExecSql(strSql);
            if(arr){
            	var rs1 = easyExecSql("select codename from ldcode1 where codetype='province1' and code='"+arr[0][1]+"' and code1 ='0'");
            	var rs2 = easyExecSql("select codename from ldcode1 where codetype='city1' and code='"+arr[0][2]+"' and code1 ='"+arr[0][1]+"'");
            	var rs3 = easyExecSql("select codename from ldcode1 where codetype='county1' and code='"+arr[0][3]+"' and code1 ='"+arr[0][2]+"'");
            	if(rs1 != null){
            		try{fm.all('Province').value=arr[0][1];}catch(ex){alert(ex.message)};   
            		try{fm.all('City').value=arr[0][2];}catch(ex){alert(ex.message)};   
            		try{fm.all('County').value=arr[0][3];}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalProvince').value=rs1;}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCity').value=rs2;}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCounty').value=rs3;}catch(ex){alert(ex.message)};   
            	}else{
            		var rs4 = easyExecSql("select code from ldcode1 where codetype='province1' and codename ='"+arr[0][1]+"'");
            		var rs5 = easyExecSql("select code from ldcode1 where codetype='city1' and codename ='"+arr[0][2]+"'");
            		var rs6 = easyExecSql("select code from ldcode1 where codetype='county1' and codename ='"+arr[0][3]+"'");
            		try{fm.all('Province').value=rs4;}catch(ex){alert(ex.message)};   
            		try{fm.all('City').value=rs5;}catch(ex){alert(ex.message)};   
            		try{fm.all('County').value=rs6;}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalProvince').value=arr[0][1];}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCity').value=arr[0][2];}catch(ex){alert(ex.message)};   
            		try{fm.all('insured_PostalCounty').value=arr[0][3];}catch(ex){alert(ex.message)};   
            	}
            	try{fm.all('insured_PostalStreet').value=arr[0][4];}catch(ex){alert(ex.message)};   
            	try{fm.all('insured_PostalCommunity').value=arr[0][5];}catch(ex){alert(ex.message)};   
                try{fm.all('insured_PostalAddress').value=arr[0][0];}catch(ex){alert(ex.message)};   
                try{fm.all('insured_HomePhone').value=arr[0][6];}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Mobile').value=arr[0][7];}catch(ex){alert(ex.message)};          
                try{fm.all('insured_CompanyPhone').value=arr[0][8];}catch(ex){alert(ex.message)};    
                try{fm.all('insured_EMail').value=arr[0][9];}catch(ex){alert(ex.message)};          
                try{fm.all('insured_ZipCode').value=arr[0][11];}catch(ex){alert(ex.message)};      
                try{fm.all('insured_Phone').value=arr[0][12];}catch(ex){alert(ex.message)};
                try{fm.all('insured_WorkName').value=arr[0][13];}catch(ex){alert(ex.message)};
            }else{
                try{fm.all('insured_PostalAddress').value="";}catch(ex){alert(ex.message)};   
                try{fm.all('insured_HomePhone').value="";}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Mobile').value="";}catch(ex){alert(ex.message)};          
                try{fm.all('insured_CompanyPhone').value="";}catch(ex){alert(ex.message)};    
                try{fm.all('insured_EMail').value="";}catch(ex){alert(ex.message)};          
                try{fm.all('insured_ZipCode').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_Phone').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_WorkName').value="";}catch(ex){alert(ex.message)};
            }
        }
    }
    showAllCodeName();
	controlNativeCity("");
}


//清空被保人信息

function clearInsured(){
   
                try{fm.all('insured_Name').value="";}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Sex').value="";}catch(ex){alert(ex.message)};      
                try{fm.all('insured_SexName').value="";}catch(ex){alert(ex.message)};        
                try{fm.all('insured_Birthday').value="";}catch(ex){alert(ex.message)};   
                try{fm.all('insured_IDType').value="";}catch(ex){alert(ex.message)};          
                try{fm.all('insured_IDTypeName').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_IDNo').value="";}catch(ex){alert(ex.message)};     
                try{fm.all('insured_InsuredNo').value="";}catch(ex){alert(ex.message)};  
                
                try{fm.all('IDStartDate').value="";}catch(ex){alert(ex.message)};  
                try{fm.all('IDEndDate').value="";}catch(ex){alert(ex.message)};  
                try{fm.all('insured_Salary').value="";}catch(ex){alert(ex.message)};
                
                try{fm.all('insured_OccupationType').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_OccupationCode').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_OccupationCodeName').value="";}catch(ex){alert(ex.message)}; 
                
                try{fm.all('insured_RelationToAppnt').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_RelationToAppntName').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_RelationToMainInsured').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_RelationToMainInsuredName').value="";}catch(ex){alert(ex.message)};
               
                try{fm.all('insured_NativePlace').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_NativePlaceName').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_NativeCity').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_NativeCityName').value="";}catch(ex){alert(ex.message)}; 
               
                try{fm.all('insured_PostalAddress').value="";}catch(ex){alert(ex.message)};   
                try{fm.all('insured_PostalProvince').value="";}catch(ex){alert(ex.message)};   
        		try{fm.all('insured_PostalCity').value="";}catch(ex){alert(ex.message)};   
        		try{fm.all('insured_PostalCounty').value="";}catch(ex){alert(ex.message)};   
        		try{fm.all('insured_PostalStreet').value="";}catch(ex){alert(ex.message)};   
        		try{fm.all('insured_PostalCommunity').value="";}catch(ex){alert(ex.message)}; 
                try{fm.all('insured_HomePhone').value="";}catch(ex){alert(ex.message)};       
                try{fm.all('insured_Mobile').value="";}catch(ex){alert(ex.message)};          
                try{fm.all('insured_CompanyPhone').value="";}catch(ex){alert(ex.message)};    
                try{fm.all('insured_EMail').value="";}catch(ex){alert(ex.message)};          
                try{fm.all('insured_ZipCode').value="";}catch(ex){alert(ex.message)};      
                try{fm.all('insured_Phone').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_WorkName').value="";}catch(ex){alert(ex.message)};
                try{fm.all('insured_Position').value="";}catch(ex){alert(ex.message)};
                fm.all("NativePlace1").style.display = "none";
	            fm.all("NativeCity1").style.display = "none";
          
            }


function goback(){
 top.close();
}


//双击下拉后操作
function afterCodeSelect(tCodeName,tObj,InputObj){
    if(!InputObj){
        InputObj=tObj;
    }
     //自动填写受益人信息
    if (tCodeName == "customertype") {
    
        if (tObj.value == "A") {
        var tSql=" select AppntName,AppntSex,IDType,IDNo from LCAppnt where contno='"+fm.ContNo.value+"' ";
        var arr=easyExecSql(tSql);
        if(arr){
            var index = BnfGrid.mulLineCount;
            BnfGrid.setRowColData(index-1, 2, arr[0][0]);
            BnfGrid.setRowColData(index-1, 3, arr[0][1]);
            BnfGrid.setRowColData(index-1, 4, arr[0][2]);
            BnfGrid.setRowColData(index-1, 5, arr[0][3]);
            BnfGrid.setRowColData(index-1, 10, "A");
        }
            
        }
        else if (tObj.value == "I")
        {
            alert("因多被保人请手动填写信息,暂不支持速填!");
        }

    }
    
    
    //InsuredInfo
     if (tCodeName == "Relation") {
    
        if(fm.insured_RelationToAppnt.value=="00"){
        document.getElementById("InsuredInfo").style.display="none";
        }else{
        document.getElementById("InsuredInfo").style.display="";
        } 

    }
    if(tCodeName=="NativePlace" && InputObj.name=="insured_NativePlace"){
        if (tObj.value == "OS"){
           fm.all("NativePlace1").style.display = "";
	       fm.all("NativeCity1").style.display = "";
	       fm.all("NativeCityTitle1").innerHTML="国家";
        }else if (tObj.value == "HK"){
           fm.all("NativePlace1").style.display = "";
	       fm.all("NativeCity1").style.display = "";
	       fm.all("NativeCityTitle1").innerHTML="地区";
        }else{
           fm.all("NativePlace1").style.display = "none";
	       fm.all("NativeCity1").style.display = "none";
        }
        fm.all("insured_NativeCity").value = "";
        fm.all("insured_NativeCityName").value = "";
    } 
     
}
    
    /**
    *受益人信息校验
    */
    function CheckBnfDate(){
    var passVerify = true;
        BnfGrid.delBlankLine();
        if(BnfGrid.mulLineCount>0){
            var i;
        
        for (i=0; i<BnfGrid.mulLineCount; i++) {
        var sumLiveBnf = new Array();
        var sumDeadBnf = new Array();
          for(var j=0;j<BnfGrid.mulLineCount;j++){
             if(BnfGrid.getRowColData(i,6) == BnfGrid.getRowColData(j,6)){
             
             if (BnfGrid.getRowColData(j, 9)==null||BnfGrid.getRowColData(j, 9)=='') {
              BnfGrid.setRowColData(j, 9,"1");
               }
             if (BnfGrid.getRowColData(j, 8)==null||BnfGrid.getRowColData(j, 8)=='') {
              BnfGrid.setRowColData(j, 8,"1");
               } 
             if (BnfGrid.getRowColData(j, 1) == "0") {
                if (typeof(sumLiveBnf[parseInt(BnfGrid.getRowColData(j, 9))]) == "undefined")
                  sumLiveBnf[parseInt(BnfGrid.getRowColData(j, 9))] = 0;
                sumLiveBnf[parseInt(BnfGrid.getRowColData(j, 9))] = sumLiveBnf[parseInt(BnfGrid.getRowColData(j, 9))] + parseFloat(BnfGrid.getRowColData(j, 8));
            } else if (BnfGrid.getRowColData(j, 1) == "1") {
                if (typeof(sumDeadBnf[parseInt(BnfGrid.getRowColData(j, 9))]) == "undefined")
                  sumDeadBnf[parseInt(BnfGrid.getRowColData(j, 9))] = 0;
                sumDeadBnf[parseInt(BnfGrid.getRowColData(j, 9))] = sumDeadBnf[parseInt(BnfGrid.getRowColData(j, 9))] + parseFloat(BnfGrid.getRowColData(j, 8));
            }
             }
          }
          
           for (var j=0; j<sumLiveBnf.length; j++) {
            if (typeof(sumLiveBnf[j])!="undefined" && sumLiveBnf[j]>1) {
                alert("生存受益人受益顺序 " + j + " 的受益比例和为：" + sumLiveBnf[j] + " 。大于100%，不能提交！");
                passVerify = false;
            } else if (typeof(sumLiveBnf[j])!="undefined" && sumLiveBnf[j]<1) {
                alert("注意：生存受益人受益顺序 " + j + " 的受益比例和为：" + sumLiveBnf[j] + " 。小于100%");
                passVerify = false;
            }
        }
    
        for (var j=0; j<sumDeadBnf.length; j++) {
              if (typeof(sumDeadBnf[j])!="undefined" && sumDeadBnf[j]>1) {
                alert("死亡受益人受益顺序 " + j + " 的受益比例和为：" + sumDeadBnf[j] + " 。大于100%，不能提交！");
                passVerify = false;
              } else if (typeof(sumDeadBnf[j])!="undefined" && sumDeadBnf[j]<1) {
                alert("注意：死亡受益人受益顺序 " + j + " 的受益比例和为：" + sumDeadBnf[j] + " 。小于100%");
                passVerify = false;
              }
        }
        for (var j=0; j<BnfGrid.mulLineCount; j++){
            if(BnfGrid.getRowColData(j,1)=="1" && BnfGrid.getRowColData(j,7)=="00"){
                alert("身故受益人不能选择被保险人本人,请检查!");
              passVerify = false;
            }
          }
          
          if(BnfGrid.checkValue2(BnfGrid.name,BnfGrid)== false)
            passVerify = false;
          
          //校验受益人关联的被保人号
          
          var tInsuredno=BnfGrid.getRowColData(i,6);
          var tInsuredname=BnfGrid.getRowColData(i,2);
          var tSql=" select SequenceNo from LCInsured where ContNo='"+ContNo+"' and SequenceNo='"+tInsuredno+"' ";
          var arr=easyExecSql(tSql);
          if(arr =="" || arr==null){
           alert("受益人"+tInsuredname+"关联的被保人已修改请重新关联被保人!");
           passVerify = false;
           }
          }
    
        }
        
        
    
    

    return passVerify;
}

var scanFlag="1";
    
function inputConfirm(wFlag){
    
	 if(!checkCrsBussSaleChnl()){
	        return false;
	    }
	    
//     if(!checkSaleChnlRisk())
//    {
//        return false;
//    }

    if(!checkAppnt()){
       return false;
    }
    if(!checkInsured()){
        
        return false;
    }
    //校验是否已经保存险种信息和被保人发生变化
    if(!checkInsuredandprem()){
    return false;
    }
    
    if(!checkAppInsured()){
      return false;
    }
 
   
    if (wFlag ==1 ) //录入完毕
    {
    	//黑名单校验
    	if(!checkBlackName()){
    		return false;
    	}
        var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '16140526361'";
        turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
        fm.WorkFlowFlag.value = "0000013001";
        fm.ContNo.value=ContNo;
        fm.MissionID.value = MissionID;
        fm.SubMissionID.value = SubMissionID;			//录入完毕
    }
  
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./FamilyInputConfirm.jsp";
    fm.submit(); //提交
  //showInfo.close();
  
  }
function afterSubmit1( FlagStr, content )
{

	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		approvefalg = "";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "成功！";
		cMissionID = fm.MissionID.value;
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

	}
	mAction = ""; 
}


//校验保单是否已经算费,和是否算费的被保人和现有被保人匹配
function checkInsuredandprem(){
    var tSql=" select distinct insuredno from lcpol where contno='"+ContNo+"'";
    
    var tSqlI=" select insuredno from lcinsured where contno='"+ContNo+"'";
    
    var arr=easyExecSql(tSql);
    var arri=easyExecSql(tSqlI);
    
    
    var tb=false;
    if(arr!=null && arr.length > 0 && arri!=null && arri.length > 0){
    
    if(arr.length != arri.length){
         alert("被保人发生变化请先保存险种信息!");
          return false;
    }
       var arrlength=0;
       for(var i=0;i<arr.length ;i++){
         for(var j=0;j<arri.length;j++){
           if(arr[i][0] == arri[j][0]){
             arrlength = arrlength + 1;
           }
         }
       }
       if(arrlength!=arr.length){
          alert("被保人发生变化请先保存险种信息!");
          return false;
      }
       
       
    }else{
      alert("请先保存险种信息!");
      return false;
    }
    return true;
}

function checkAppInsured(){

   var tSql1 = " select insuredno from lcinsured where contno='"+ContNo+"' and RelationToAppnt='00'";
   var tSql2 = " select appntno,prem from lccont where contno='"+ContNo+"' ";
   var arr1=easyExecSql(tSql1);
   var arr2=easyExecSql(tSql2);
   if(arr1!=null){
       if(arr1[0][0]!=arr2[0][0]){
           alert("投保人发生变化，请对被保人为投保人本人的被保人进行修改！");
           return false;
       }
   }
   if(arr2[0][1]==0){
        alert("保单信息发生变化，请重新算费后再进行录入完毕操作！");
        return false;
   }
   
   return true;

}

//校验简易家庭保单录单 集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   add by 赵庆涛 2016-06-28
function checkCrsBussSaleChnl()
{
	var tPrtNo = prtNo;
    var Crs_SaleChnl = null;
    
    var arr = easyExecSql("select Crs_SaleChnl from lccont where prtno = '" + tPrtNo + "' ");
    if(arr)
    {
    	Crs_SaleChnl = arr[0][0];
    }
	if(Crs_SaleChnl != null && Crs_SaleChnl != "")
	{
		var strSQL = "select 1 from lccont lgc where 1 = 1"  
			+ " and lgc.Crs_SaleChnl is not null " 
			+ " and lgc.Crs_BussType = '01' " 
			+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			+ " and lgc.PrtNo = '" + tPrtNo + "' ";
		var arrResult = easyExecSql(strSQL);
        if (arrResult != null)
        {
        	alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        	return false;
        }
	}
	return true;
}

function checkSaleChnlRisk()
{   
    var tPrtNo = prtNo;
    var saleChannel = null;
    
    var arr = easyExecSql("select salechnl from lccont where prtno = '" + tPrtNo + "' ");
    if(arr)
    {
        saleChannel = arr[0][0];
    }
    
    if(saleChannel != null && saleChannel == "13")
    {
        var tStrSql = " select 1 "
            + " from LCPol lcp "
            + " left join LDCode1 ldc1 on lcp.SaleChnl = ldc1.Code and lcp.RiskCode = ldc1.code1 and ldc1.codetype = 'checksalechnlrisk' "
            + " where 1 = 1 "
            + " and ldc1.CodeType is null "
            + " and lcp.PrtNo = '" + tPrtNo + "' "
            ;
        var result = easyExecSql(tStrSql);
        
        if(result)
        {
            alert("该单为银代直销保单，但存在非银代直销产品，请核实。");
            return false;
        }
    }
    
    return true;
}

function checkInsured(){
    var tSql = "select lci.name,lci.idno,lci.idtype,lci.birthday,lci.sex,lca.phone,lca.mobile,lca.homephone,lca.email from lcinsured lci,lcaddress lca where lci.prtno='" + prtNo + "'"
	         +" and lci.addressno=lca.addressno and lca.customerno=lci.insuredno ";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		for(var index = 0; index < appntResult.length; index++){
			// 被保人姓名
			var name = appntResult[index][0];
			// 证件号
			var id = appntResult[index][1];
			// 证件类型
			var idtype = appntResult[index][2];
			// 出生日期
			var birthday = appntResult[index][3];
			// 性别
			var sex = appntResult[index][4];
			// 联系电话
			var phone = appntResult[index][5];
			// 移动电话
			var mobile = appntResult[index][6];
			// 固定电话
			var homephone = appntResult[index][7];
			// 邮箱
			var email = appntResult[index][8];
			
			if(!isNull(email)){
				   var checkemail =CheckEmail(email);
				   if(checkemail !=""){
					   alert("被保人"+name+checkemail);
					   return false;
				   }
			   }
			 if(!isNull(homephone)){
				   var checkhomephone =CheckFixPhone(homephone);
				   if(checkhomephone !=""){
					   alert("被保人"+name+checkhomephone);
					   return false;
				   }
			   }
			 if(mobile != null && mobile !="") {
				    if(!isInteger(mobile) || mobile.length != 11){
					    alert("被保人"+name+"移动电话需为11位数字，请核查！");
			   	      	return false;
				    } 
				}
			if(!checkMobile(2,mobile,name)){
				  return false;
				}
			
			if(idtype == "0"){

				if(birthday != getBirthdatByIdNo(id)){
					alert("被保人" + name + "身份证号与出生日期不符，请核查！");
					return false;
				}
			
				if(sex != getSexByIDNo(id)){
					alert("被保人" + name + "身份证号与性别不符，请核查！");
					return false;
				}
			}
			
			if(!isNull(phone)){
				var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			if(!isNull(mobile)){
				var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "移动电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				
			}
			if(!isNull(homephone)){
				var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "固定电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "固定电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
		}
	}

	return true;
}

function checkidtype() {
  if(fm.insured_IDType.value=="") {
    alert("请先选择证件类型！");
    fm.insured_IDNo.value="";
  }
}

function getBirthdaySexByIDNo(iIdNo) {
  if(fm.all('insured_IDType').value=="0") {
    fm.all('insured_Birthday').value=getBirthdatByIdNo(iIdNo);
    fm.all('insured_Sex').value=getSexByIDNo(iIdNo);
    fm.all('insured_SexName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'sex' and Code='"+fm.all('insured_Sex').value+"'");
  }
  if(fm.all('insured_IDType').value=="5") {
	    fm.all('insured_Birthday').value=getBirthdatByIdNo(iIdNo);
	    fm.all('insured_Sex').value=getSexByIDNo(iIdNo);
	    fm.all('insured_SexName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'sex' and Code='"+fm.all('insured_Sex').value+"'");
	  }
}

function checkAppnt(){

	var tSql = "select cc.prtno,cad.Customerno,cc.Managecom,cad.Mobile,cad.Phone,cad.homephone,cad.companyphone,ca.idno,ca.idtype,ca.appntbirthday,ca.appntsex,cc.appntname," 
		+" cad.email,cad.homecode,cad.homenumber " 
		+" from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + prtNo+ "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		// 印刷号
		var prtno = appntResult[0][0];
		// 投保人客户号
		var customerno = appntResult[0][1];
		// 管理机构
		var managecom = appntResult[0][2];
		// 移动电话
		var mobile = appntResult[0][3];
		// 联系电话
		var phone = appntResult[0][4];
		// 家庭电话
		var homephone = appntResult[0][5];
		// 单位电话
		var companyphone = appntResult[0][6];
		// 证件号
		var id = appntResult[0][7];
		// 证件类型
		var idtype = appntResult[0][8];
		// 出生日期
		var birthday = appntResult[0][9];
		// 性别
		var sex = appntResult[0][10];
		// 姓名
		var name = appntResult[0][11];
		// 邮箱
		var email = appntResult[0][12];
		// 固定电话区号
		var homecode = appntResult[0][13];
		// 固定电话号码
		var homenumber = appntResult[0][14];
		
		if(!isNull(email)){
			   var checkemail =CheckEmail(email);
			   if(checkemail !=""){
				   alert(checkemail);
				   return false;
			   }
		   }
		if(!chenkHomePhone(1,homecode,homenumber)){
			  return false;
			}
		 if(!isNull(homephone)){
			   var checkhomephone =CheckFixPhone(homephone);
			   if(checkhomephone !=""){
				   alert("投保人"+checkhomephone);
				   return false;
			   }
		   }
		// 共有校验
		if(!isNull(mobile)) {
			if(!isInteger(mobile) || mobile.length != 11){
				alert("投保人移动电话需为11位数字，请核查！");
		   		return false;
			}
		}
		if(!checkMobile(1,mobile,name)){
			  return false;
			}
		if(idtype == "0"){
		
			if(birthday != getBirthdatByIdNo(id)){
				alert("投保人身份证号与出生日期不符，请核查！");
				return false;
			}
			
			if(sex != getSexByIDNo(id)){
				alert("投保人身份证号与性别不符，请核查！");
				return false;
			}
		}
		
		// 机构特殊校验  --需要在数据库中配置 codetype = 字段名 + check

		// 暂时统一对投保人进行校验，若需要单独校验，可以调整为按字段进行校验
		// 若配置为空则会对全部机构进行校验
		if(true){
			
			if(isNull(id)){
				alert("投保人证件号码不能为空，请核查！");
				return false;
			}
			
			if(isNull(mobile) && isNull(phone) && isNull(homephone)){
				alert("投保人固定电话、移动电话、联系电话不能同时为空，请核查！");
		   		return false;
			}
			
		}
		if(!isNull(mobile)){
				var mobilSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and addressno=lcaddress.addressno) ";
				var result = easyExecSql(mobilSql);
				if(result){
					var count = result[0][0];
					if(count >= 2){
						alert("该投保人移动电话已在三个以上不同投保人的保单中出现！");
						return false;
					}
				}
		}
		if(!isNull(phone)){
			var phoneSql = "select count(distinct customerno) from lcaddress where phone='" + phone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and addressno=lcaddress.addressno) ";
			var result = easyExecSql(phoneSql);
			if(result){
				var count = result[0][0];
				if(count >= 2){
				    alert("该投保人联系电话已在三个以上不同投保人的保单中出现！" );
					return false;
				}
			}
		}
		if(!isNull(homephone)){
			var phoneSql = "select count(distinct customerno) from lcaddress where homephone='" + homephone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and addressno=lcaddress.addressno) ";
			var result = easyExecSql(phoneSql);
			if(result){
				var count = result[0][0];
				if(count >= 2){
				    alert("该投保人固定电话已在三个以上不同投保人的保单中出现！");
					return false;
				}
			}
		}
		if(true){
	    		//校验是否是业务员本人，如果不是进行手机号校验
				// 业务员手机号码校验
			if(!isNull(phone)){
				var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			if(!isNull(mobile)){
				var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人移动电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
			}
			if(!isNull(homephone)){
				var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人固定电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人固定电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			}
	}
	return true;
}

function checkAppInsr(relation,name,birthday){
    if(relation=="03"){
       var appday = easyExecSql("select AppntBirthday + 18 year from lcappnt where prtno='"+fm.PrtNo.value+"'");
       var fdate = new Date(Date.parse(appday[0][0].replace(/-/g,"/")));
       var cdate = new Date(Date.parse(birthday.replace(/-/g,"/")));
      
       if(Date.parse(cdate)-Date.parse(fdate) < 0){
           if(!confirm("被保人"+name+"与投保人年龄差距小于18岁，是否继续？")){
               return false;
           }
       }
    }
    return true;
}

function chenkIdNo(type,idtype,idno,name){
        var str = "";
        if(type=="1"){
          str="投保人";
        }else{
          str="被保人"+name;
        }
        if(idtype == "" || idtype == null){
             alert("证件类型不能为空！");
             return false;
        }else{
             var arr = easyExecSql("select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'");
             if(arr!=null){
                var othersign = arr[0][2];
                var first = arr[0][0];
                var two = arr[0][1];
                if("double"==othersign){
                   if(idno.length!=first&&idno.length!=two){
                       alert("录入的"+str+"证件号码有误，请检查！");
                       return false;
                   }
                }
                if("min"==othersign){
                   if(idno.length<first){
                      alert("录入的"+str+"证件号码有误，请检查！");
                      return false;
                   }
                }
                if("between"==othersign){
                   if(idno.length<first||idno.length>two){
                      alert("录入的"+str+"证件号码有误，请检查！");
                      return false;
                   }
                }
                
             }else{
                alert("选择的"+str+"证件类型有误，请检查！");
                return false;
             }
        }
        return true;
}

function checkMobile(type,mobile,name){
   var str = "";
   if(type=="1"){
      str="投保人";
   }else{
      str="被保人"+name;
   }
   if(mobile != "" && mobile != null){
      if(mobile.length!=11){
          alert(str+"移动电话不符合规则，请核实！");
          return false;
      }else{
         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
            alert(str+"移动电话不符合规则，请核实！");
            return false;
         }
      }
   }else if(type=="1"){
      if(!confirm("投保人移动电话为空，是否继续？")){
         return false;
	  }
   }
   return true;
}

function checkAllInsu(type){
   var mobile = fm.insured_Mobile.value;
   var idtype = fm.insured_IDType.value;
   var idno = fm.insured_IDNo.value;
   var realtion = fm.insured_RelationToAppnt.value;
   var birthday = fm.insured_Birthday.value;
   var naticeplace = fm.insured_NativePlace.value;
   var naticecity = fm.insured_NativeCity.value;
   var homephone = fm.insured_HomePhone.value;
   var email = fm.insured_EMail.value;
   if(!isNull(email)){
	   var checkemail =CheckEmail(email);
	   if(checkemail !=""){
		   alert("被保人"+checkemail);
		   return false;
	   }
   }
   if(!isNull(homephone)){
	   var checkhomephone =CheckFixPhone(homephone);
	   if(checkhomephone !=""){
		   alert("被保人"+checkhomephone);
		   return false;
	   }
   }
   if(mobile != null && mobile !="") {
	    if(!isInteger(mobile) || mobile.length != 11){
		    alert("被保人移动电话需为11位数字，请核查！");
  	      	return false;
	    } 
	}
   if(type=="1"){
     if(naticeplace=="OS"||naticeplace=="HK"){
        if(naticecity==""||naticecity==null){
           alert("被保人所属国家/地区未录入！");
           return false;
        }
     }
   }
   if(realtion!="00"){
     if(naticeplace=="OS"||naticeplace=="HK"){
        if(naticecity==""||naticecity==null){
           alert("被保人所属国家/地区未录入！");
           return false;
        }
     }
     if(!checkAddress()){
 		return false;
 	}
     
     if(!chenkIdNo(2,idtype,idno,'')){
	  return false;
	}
	
	if(!checkAppInsr(realtion,'',birthday)){
	  return false;
	} 
	
	if(!checkMobile(2,mobile,'')){
	  return false;
	}
	
   }
    
   return true;
}

function checkAddress(){
	   var insured_PostalProvince = fm.insured_PostalProvince.value;
	   var insured_PostalCity = fm.insured_PostalCity.value;
	   var insured_PostalCounty = fm.insured_PostalCounty.value;
	   var insured_PostalStreet = fm.insured_PostalStreet.value;
	   var insured_PostalCommunity = fm.insured_PostalCommunity.value;   
	   
	   if(insured_PostalProvince == null || insured_PostalProvince == ""){
			alert("被保人省（自治区直辖市）不能为空！")
			document.getElementById('Province').focus();
			return false;
		}
		if(insured_PostalCity == null || insured_PostalCity == ""){
			alert("被保人市不能为空！")
			document.getElementById('City').focus();
			return false;
		}
		if(insured_PostalCounty == null || insured_PostalCounty == ""){
			alert("被保人县（区）不能为空！")
			document.getElementById('County').focus();
			return false;
		}
		if(insured_PostalStreet == null || insured_PostalStreet == ""){
			alert("被保人乡镇（街道）不能为空！")
			document.getElementById('insured_PostalStreet').focus();
			return false;
		}
		if(insured_PostalCommunity == null || insured_PostalCommunity == ""){
			alert("被保人村（社区）不能为空！")
			document.getElementById('insured_PostalCommunity ').focus();
			return false;
		}
	   //校验联系地址级联关系
	   var checkCityError = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
			   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"' )");
	   if(checkCityError == null || checkCityError == ""){
		   alert("被保人省、市地址级联关系不正确，请检查！");
		   return false;
	   }
	   var checkCityError2 = easyExecSql("select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"')");
	//   var checkCityError2 = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
//		   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"'))");
	   if(checkCityError2 == null || checkCityError2 == ""){
		   alert("被保人市、县地址级联关系不正确，请检查！");
		   return false;
	   }
	 //当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式
	   if(fm.insured_PostalCity.value == '空' && fm.insured_PostalCounty.value == '空'){
		   fm.insured_PostalAddress.value=insured_PostalProvince+insured_PostalStreet+insured_PostalCommunity;
	   }else if(fm.insured_PostalCity.value != '空' && fm.insured_PostalCounty.value == '空') {
		   fm.insured_PostalAddress.value=insured_PostalProvince+insured_PostalCity+insured_PostalStreet+insured_PostalCommunity;
	   }else{
		   fm.insured_PostalAddress.value=insured_PostalProvince+insured_PostalCity+insured_PostalCounty+insured_PostalStreet+insured_PostalCommunity;
	   }
	   return true;
	}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}

function controlNativeCity(displayFlag)
{
  if(fm.insured_NativePlace.value=="OS"){
    fm.all("NativePlace1").style.display = displayFlag;
	fm.all("NativeCity1").style.display = displayFlag;
	fm.all("NativeCityTitle1").innerHTML="国家";
  }else if(fm.insured_NativePlace.value=="HK"){
    fm.all("NativePlace1").style.display = displayFlag;
	fm.all("NativeCity1").style.display = displayFlag;
	fm.all("NativeCityTitle1").innerHTML="地区";
  }else{
    fm.all("NativePlace1").style.display = "none";
	fm.all("NativeCity1").style.display = "none";
  }
} 

//校验黑名单
function checkBlackName(){
	//黑名单校验受益人
	var sql3 =  "select distinct name, idtype, idno from lcbnf where contno=(select contno from lccont where prtno='"+fm.PrtNo.value+"')  ";
	var arrR = easyExecSql(sql3);
	var tBnfNames = "";
	if(arrR){
		for(var i = 0; i < arrR.length; i++){
			var sqlblack3  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrR[i][0]+"' and idnotype='"+arrR[i][1]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrR[i][0]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrR[i][0]+"' and type='0' ";
			var arrR1 = easyExecSql(sqlblack3);
			if(arrR1){
				if(tBnfNames!=""){
					tBnfNames = tBnfNames+ " , "+arrR[i][0];
				}else{
					tBnfNames = tBnfNames + arrR[i][0];
				}
			}
		}
	}
	if(tBnfNames != ""){
		if (!confirm("该保单受益人："+tBnfNames+",存在于黑名单中，确认要保存吗？")){
			return false;
		}
	}
	return true;
}

function chenkHomePhone(type,homecode,homenumber){
    
    var str = "";
    if(type=="1"){
    	str ="投保人";
    }else{
    	str ="被保人";
    }
    
    if(homecode == "" || homecode == null){
         if(homenumber != "" && homenumber != null){
            alert(str+"固定电话区号未进行录入！");
            return false;
         }
    }else{
        if(homecode.length!=3 && homecode.length!=4||!isInteger(homecode)){
            alert(str+"固定电话区号录入有误，请检查！");
            return false;
        }
        if(homenumber == "" || homenumber == null){
            alert(str+"固定电话电话号码未进行录入！");
            return false;
        }
        if(homenumber.length!=7&&homenumber.length!=8&&homenumber.length!=10||!isInteger(homenumber)){
            alert(str+"固定电话电话号码录入有误，请检查！");
            return false;
        }else{
           if(homenumber.length==10){
               if(homenumber.substring(0, 3)!=400&&homenumber.substring(0, 3)!=800){
                  alert(str+"固定电话电话号码录入有误，请检查！");
                  return false;
               }
           }
        }
    }
    return true;
}