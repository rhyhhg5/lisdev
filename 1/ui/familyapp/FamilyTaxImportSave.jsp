<%@page import="com.sinosoft.lis.familyapp.FamilyTaxBL"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FamilySignQueryInputSave.jsp
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%

CErrors tError = null;
String FlagStr = "";
String Content = "";
String tBatchNo = "";

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
TransferData tTransferData = new TransferData();
String strManageCom = tG.ComCode;

int length = 8;
if (strManageCom.length() < length)
{
    strManageCom = PubFun.RCh(strManageCom, "0", length);
}

try
{
    tBatchNo = PubFun1.CreateMaxNo("XFYJBATCHNO", strManageCom, null);
    System.out.println("BatchNo:" + tBatchNo);
}
catch(Exception e)
{
    tBatchNo = null;
    e.printStackTrace();
}
if(tBatchNo==null){
	FlagStr = "Fail";
	Content = "创建批次号失败";
}else{
	VData mdata = new VData();
	mdata.add(tG);
	tTransferData.setNameAndValue("BatchNo", tBatchNo);
	mdata.add(tTransferData);
	FamilyTaxBL tFamilyTaxBL=new FamilyTaxBL();
	if(!tFamilyTaxBL.submitData(mdata, "")){
		Content="申请失败，原因是"+tFamilyTaxBL.mErrors.getLastError().toString();
		FlagStr = "Fail";
		
	}else{
		Content = " 申请成功! ";
	      FlagStr = "Succ";
	
	}


}



%>
<html>
<script language="javascript">
                 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=tBatchNo%>");
</script>
</html>
