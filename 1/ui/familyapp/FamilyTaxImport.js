var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;


//为客户申请一个批次号
function Apply(){
	
		
		fm.ApplyButton.disabled = true;//申请当中，按钮设置为不可用
		
		var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		
		fm.action = "./FamilyTaxImportSave.jsp";
		fm.submit();
		
		fm.action = "";
	
}
//提交申请之后的处理函数
function afterSubmit(FlagStr,Content,tBatchNo){
	showInfo.close();//关掉弹窗
	fm.ApplyButton.disabled = false;//返回申请的结果后，按钮设置为可用
	if( FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		fm.BatchNo.value=tBatchNo;
		QueryBatchClick();
	}
}
//查询已申请的批次信息
function QueryBatchClick(){
	
	var tSQL = "select batchno,mngcom,modifydate,operator,case state when '0' then '待导入' when '1' then '待确认' when '2'  then '已确认'  when '3' then '导入存在问题' end "
			 + " from XFYJAPPNT "
			 + "where 1=1 and prtno='000000'"
			 + getWherePart("BatchNo","BatchNo")
			 + getWherePart("ManageCom","ManageCom","like")					 
			 + getWherePart("modifyDate","ApplyDate")
			 + " order by batchno";
	turnPage1.queryModal(tSQL,BatchDetailGrid);
	if(BatchDetailGrid.mulLineCount<=0){
		alert("没有查询到相关数据");
	}
}
//批次导入
function batchImport(){
	var tRow = BatchDetailGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0){
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = BatchDetailGrid.getRowData(tRow);
    var tBatch = tRowDatas[0];
//    var tBatchState = tRowDatas[5];
//    //已确认的批次只能查看导入的被保人清单
//    if(tBatchState == "已确认"){
//    	alert("批次："+tBatch+"为已确认状态，仅能查看导入的被保人清单,点击确定后将弹出批次被保人详情查看页面！");
//    	window.open("./TaxInsuredInfoMain.jsp?BatchNo="+tBatch);
//    }else{
    window.location = "./FamilyTaxListImportInput.jsp?BatchNo="+tBatch;
 //   }
}
//批次被保人详情
function importInsuredDetail(){
	var tRow = BatchDetailGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0){
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = BatchDetailGrid.getRowData(tRow);
    var tBatch = tRowDatas[0];
	window.open("./TaxInsuredInfoMain.jsp?BatchNo="+tBatch);
}
