<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String printChannelType = request.getParameter("printChannelType");
%>
<script>
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var printChannelType = "<%=printChannelType%>";//外包打印和分公司打印的标记 0：外包打印；1分公司打印
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="FamilyContPrintInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FamilyContPrintInputInit.jsp"%>
  <title>打印家庭保单 </title>
</head>
<body onload="initForm();" >
	<form action="./FamilyContPrintSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 个人保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr><td class= titleImg align= center>请输入保单查询条件：</td></tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>销售渠道</TD>
				<TD class= input><Input class="codeNo"  name=SaleChnl  ondblclick="return showCodeList(tSaleChnlCode,[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey(tSaleChnlCode,[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true > </TD>			
				<TD class= title>管理机构</TD>
				<TD class= input><Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > </TD>
				</TD>
				<TD class= title>业务员代码</TD>
				<TD class= input><Input class="codeNo"  name=AgentCode  verify="业务员代码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1],null,null,null,1);"><input class=codename name=AgentCodeName readonly=true > </TD>						
			</TR>
			<TR class= common>
				<TD class= title>保险合同号</TD>
				<TD class= input> <Input class= common name=ContNo verify="保险合同号|int"> </TD>
				<TD class= title>印刷号码</TD>
				<TD class= input> <Input class= common name=PrtNo verify="印刷号码|int"> </TD>
			    <td class="title">是否打印</td>
                <td class="input"><input class="codeno" CodeData="0|8^0|已打印^1|未打印" name="SFState"  ondblclick="return showCodeListEx('SFState',[this,SFStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('SFState',[this,SFStateName],[0,1],null,null,null,1);" /><input class=codename name=SFStateName></td>
			</TR>
			<tr class=common>
				<td class=title>承保起期</td>
				<td class=input><Input class= coolDatePicker name=startUWDate></td>
				<td class=title>承保止期</td>
				<td class=input><Input class= coolDatePicker name=endUWDate></td>
			</tr>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<INPUT VALUE="打印保单" class="cssButton" TYPE=button onclick="printPol();" name='printButton' >
		<INPUT VALUE="客户卡信息下载(txt)" class="cssButton" TYPE=button onclick="downloadCustomerCard('txt');" name='downloadCustomerCardButton' style="display: none">
		<INPUT VALUE="客户卡信息下载(excel)" class="cssButton" TYPE=button onclick="downloadCustomerCard('xls');" name='downloadCustomerCardButtonExcel' style="display: none">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''" align=center>
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanContGrid" ></span>
					</td>
				</tr>
			</table>
			<INPUT VALUE="首 页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾 页" class="cssButton" TYPE=button onclick="getLastPage();">
		</div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
