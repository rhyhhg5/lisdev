
<%--
  保存简易家庭保单信息
--%>
<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.LCContDB"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.familyapp.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
	String FlagStr = ""; //操作结果
	String Content = ""; //控制台信息
	String tAction = ""; //操作类型：delete update insert
	String tOperate = ""; //操作代码
	String mLoadFlag = "";
	CErrors tError;
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	tAction = request.getParameter("fmAction");
	String tContType = request.getParameter("ContType");
	System.out.println(tContType);
	String tMissionProp5 = request.getParameter("MissionProp5");
	LCContSchema tLCContSchema = new LCContSchema();
	LCContDB tLCContDB = new LCContDB();
	try {
		tLCContDB.setContNo(request.getParameter("ContNo"));
        if (tLCContDB.getInfo()){
        	tLCContSchema=tLCContDB.getSchema();
        }
		tLCContSchema.setContNo(request.getParameter("ContNo"));
		tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
		tLCContSchema.setContType(request.getParameter("ContType"));
		tLCContSchema.setCardFlag(request.getParameter("CardFlag"));
		tLCContSchema.setManageCom(request.getParameter("ManageCom"));
		tLCContSchema.setAgentCom(request.getParameter("AgentCom"));
		tLCContSchema.setAgentCode(request.getParameter("AgentCode"));
		System.out.println("=============agentgroup"+request.getParameter("AgentGroup")+"---------");
		tLCContSchema.setAgentGroup(request.getParameter("AgentGroup"));
		tLCContSchema.setAgentType(request.getParameter("AgentType"));
		tLCContSchema.setSaleChnl(request.getParameter("SaleChnl"));
		tLCContSchema.setAppntNo(request.getParameter("AppntNo"));//投保人客户号
		tLCContSchema.setAppntName(request.getParameter("AppntName"));//投保人姓名
		tLCContSchema.setAppntSex(request.getParameter("AppntSex"));//投保人性别
		tLCContSchema.setAppntBirthday(request
		.getParameter("AppntBirthday"));
		tLCContSchema.setCValiDate(request.getParameter("PolApplyDate"));
		tLCContSchema.setPayIntv(request.getParameter("PayIntv"));
		tLCContSchema.setPayMode(request.getParameter("PayMode"));
		tLCContSchema.setBankCode(request.getParameter("BankCode"));
		tLCContSchema.setBankAccNo(request.getParameter("BankAccNo"));
		System.out.println("投保人的银行帐号"
		+ request.getParameter("BankAccNo"));
		tLCContSchema.setAccName(request.getParameter("AccName"));
		tLCContSchema.setCurrency(request.getParameter("Currency"));
		tLCContSchema.setRemark(request.getParameter("Remark"));
		tLCContSchema.setPeoples(request.getParameter("Peoples"));
		tLCContSchema.setInputDate(request.getParameter("InputDate"));
		tLCContSchema.setInputTime(request.getParameter("InputTime"));
		tLCContSchema.setPolApplyDate(request
		.getParameter("PolApplyDate"));
		tLCContSchema.setReceiveDate(request
		.getParameter("ReceiveDate"));
		tLCContSchema.setTempFeeNo(request.getParameter("TempFeeNo"));
		tLCContSchema.setGrpAgentCom(request
		.getParameter("GrpAgentCom"));
		tLCContSchema.setGrpAgentIDNo(request
		.getParameter("GrpAgentIDNo"));
		tLCContSchema.setDueFeeMsgFlag(request
		.getParameter("DueFeeMsgFlag"));
		tLCContSchema.setGrpAgentCode(request
		.getParameter("GrpAgentCode"));
		tLCContSchema.setGrpAgentName(request
		.getParameter("GrpAgentName"));
		tLCContSchema.setAgentSaleCode(request
		.getParameter("AgentSaleCode"));

		// 集团交叉业务
		tLCContSchema.setCrs_SaleChnl(request
		.getParameter("Crs_SaleChnl"));
		tLCContSchema.setCrs_BussType(request
		.getParameter("Crs_BussType"));

		// 缴费模式
		tLCContSchema.setPayMethod(request.getParameter("PayMethod"));
	} catch (Exception ex) {
		ex.printStackTrace();
		System.out.println("出错了！");
	}
	//处理投保人
	LCAppntSchema tLCAppntSchema = new LCAppntSchema();
	try {
		tLCAppntSchema.setAppntNo(request.getParameter("appnt_AppntNo"));
		tLCAppntSchema.setPosition(request.getParameter("appnt_Position"));
		tLCAppntSchema.setSalary(request.getParameter("appnt_Salary"));
		tLCAppntSchema.setOccupationType(request.getParameter("appnt_OccupationType"));
		tLCAppntSchema.setOccupationCode(request.getParameter("appnt_OccupationCode"));
		tLCAppntSchema.setAppntName(request.getParameter("appnt_AppntName"));
		tLCAppntSchema.setAppntSex(request.getParameter("appnt_AppntSex"));
		tLCAppntSchema.setAppntBirthday(request.getParameter("appnt_AppntBirthday"));
		tLCAppntSchema.setIDType(request.getParameter("appnt_IDType"));
		tLCAppntSchema.setIDStartDate(request.getParameter("appnt_IDStartDate"));
		tLCAppntSchema.setIDEndDate(request.getParameter("appnt_IDEndDate"));
		tLCAppntSchema.setIDNo(request.getParameter("appnt_IDNo"));
		tLCAppntSchema.setNativeCity(request.getParameter("appnt_NativeCity"));
		tLCAppntSchema.setNativePlace(request.getParameter("appnt_NativePlace"));
		

	} catch (Exception ex) {
		ex.printStackTrace();
		System.out.println("出错了~~~~");
	}
	System.out.println("处理完成投保人信息！");
	System.out.println("开始处理地址信息~");
	LCAddressSchema mAppntAddressSchema = new LCAddressSchema();

	try {
		mAppntAddressSchema.setCustomerNo(request.getParameter("appnt_AppntNo"));
		mAppntAddressSchema.setGrpName(request.getParameter("WorkName"));
		mAppntAddressSchema.setPostalAddress(request.getParameter("appnt_PostalAddress"));
		mAppntAddressSchema.setZipCode(request.getParameter("appnt_ZipCode"));
		mAppntAddressSchema.setPhone(request.getParameter("appnt_Phone"));
		mAppntAddressSchema.setHomePhone(request.getParameter("appnt_HomePhone"));
		mAppntAddressSchema.setMobile(request.getParameter("appnt_Mobile"));
		mAppntAddressSchema.setCompanyPhone(request.getParameter("appnt_CompanyPhone"));
		mAppntAddressSchema.setEMail(request.getParameter("appnt_EMail"));
        mAppntAddressSchema.setHomeCode(request.getParameter("appnt_HomeCode"));
        mAppntAddressSchema.setHomeNumber(request.getParameter("appnt_HomeNumber"));
        mAppntAddressSchema.setPostalCity(request.getParameter("City"));
        mAppntAddressSchema.setPostalCommunity(request.getParameter("appnt_PostalCommunity"));
        mAppntAddressSchema.setPostalCounty(request.getParameter("County"));
        mAppntAddressSchema.setPostalProvince(request.getParameter("Province"));
        mAppntAddressSchema.setPostalStreet(request.getParameter("appnt_PostalStreet"));
		
	} catch (Exception ex) {
		ex.printStackTrace();
		System.out.println("出错！");
	}

	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
	//#1855 客户类型
	LCContSubSchema tLCContSubSchema = new LCContSubSchema();
	tLCContSubSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCContSubSchema.setCountyType(request
			.getParameter("appnt_CountyType"));
	tLCContSubSchema.setFamilySalary(request
			.getParameter("FamilySalary"));

	//处理LDPerson信息
	String tWorkName = request.getParameter("WorkName");
	System.out.println("工作单位是..." + tWorkName);
	//完成全部的封装，开始递交．
	FamilyContUI tBriefFamilyContUI = new FamilyContUI();
	try {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("AppntAddress",
		mAppntAddressSchema);
		tTransferData.setNameAndValue("WorkName", tWorkName);
		tTransferData.setNameAndValue("MissionProp5", tMissionProp5);
		tTransferData.setNameAndValue("ContType", tContType);
		tVData.add(tLCContSchema);
		tVData.add(tLCAppntSchema);
		tVData.add(tLCExtendSchema);
		tVData.add(tLCContSubSchema);

		tVData.add(tG);
		tVData.add(tTransferData);
		System.out.println("开始递交数据！" + tAction);
		tBriefFamilyContUI.submitData(tVData, tAction);
	} catch (Exception ex) {
		ex.printStackTrace();
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	if (FlagStr == "") {
		tError = tBriefFamilyContUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Success";
		} else {
			Content = "保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
	VData tResult = tBriefFamilyContUI.getResult();
	LCContSchema resultLCContSchema = new LCContSchema();
	String ContNo = "";
	if (FlagStr.equals("Success")) {
		resultLCContSchema = (LCContSchema) tResult
		.getObjectByObjectName("LCContSchema", 0);
		ContNo = resultLCContSchema.getContNo();
	}
%>
<html>
	<script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=ContNo%>");
</script>
</html>
