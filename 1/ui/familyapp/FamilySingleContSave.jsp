
<%--
  保存简易保单信息 2005-11-11 16:31 Yangming
--%>
<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.familyapp.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
            String FlagStr = ""; //操作结果
            String Content = ""; //控制台信息
            String tAction = ""; //操作类型：delete update insert
            String tOperate = ""; //操作代码
            String mLoadFlag = "";
            CErrors tError;
            GlobalInput tG = new GlobalInput();
            tG = (GlobalInput) session.getValue("GI");
            tAction = request.getParameter("fmAction");

            //合同信息查询
            LCContSchema tLCContSchema = new LCContSchema();
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setPrtNo(request.getParameter("insured_PrtNo"));
            tLCContSchema = tLCContDB.query().get(1);

            //投保人信息

            LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(tLCContSchema.getContNo());
            tLCAppntSchema = tLCAppntDB.query().get(1);

            //处理被保人
            LCInsuredSet tLCInsuredSet = new LCInsuredSet();
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(tLCContSchema.getContNo());
            tLCInsuredSet = tLCInsuredDB.query();

            //处理多受益人
            // 受益人信息BnfGrid
            LCBnfSet tLCBnfSet = new LCBnfSet();
            String tBnfNum[] = request.getParameterValues("BnfGridNo");
            String tBnfType[] = request.getParameterValues("BnfGrid1");
            String tName[] = request.getParameterValues("BnfGrid2");
            String tSex[] = request.getParameterValues("BnfGrid3");
            String tIDType[] = request.getParameterValues("BnfGrid4");
            String tIDNo[] = request.getParameterValues("BnfGrid5");
            //被保人序号
            String tBnfNo[] = request.getParameterValues("BnfGrid6");
            String tBnfRelationToInsured[] = request.getParameterValues("BnfGrid7");
            String tBnfLot[] = request.getParameterValues("BnfGrid8");
            String tBnfGrade[] = request.getParameterValues("BnfGrid9");
            String tAddress[] = request.getParameterValues("BnfGrid10");
            int BnfCount = 0;
            if (tBnfNum != null)
                BnfCount = tBnfNum.length;
            for (int i = 0; i < BnfCount; i++)
            {
                if (tName[i] == null || tName[i].equals(""))
                    break;
                LCBnfSchema xLCBnfSchema = new LCBnfSchema();
                
                //通过被保人序号查找被保人号
                if(tLCInsuredSet!=null && tLCInsuredSet.size()>0){
                   for(int j=1;j<= tLCInsuredSet.size();j++){
                     if(tLCInsuredSet.get(j).getSequenceNo().equals(tBnfNo[i])){
                     xLCBnfSchema.setCustomerNo(tLCInsuredSet.get(j).getInsuredNo());
                     }
                   }
                }
                xLCBnfSchema.setBnfType(tBnfType[i]);
                xLCBnfSchema.setName(tName[i]);
                xLCBnfSchema.setSex(tSex[i]);
                xLCBnfSchema.setIDType(tIDType[i]);
                xLCBnfSchema.setIDNo(tIDNo[i]);
                xLCBnfSchema.setRelationToInsured(tBnfRelationToInsured[i]);
                xLCBnfSchema.setBnfLot(tBnfLot[i]);
                xLCBnfSchema.setBnfGrade(tBnfGrade[i]);
                System.out.println("受益人信息性别..." + xLCBnfSchema.getSex());
                System.out.println("受益人信息身份证..." + xLCBnfSchema.getIDType());
                tLCBnfSet.add(xLCBnfSchema);
            }
            System.out.println("jdieoahfeioipwpjfio" + tLCBnfSet.size());
            System.out.println("end set schema 受益人信息...");

            //处理套餐信息
            LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
            String tWrapChk[] = request.getParameterValues("InpRiskWrapGridChk");
            //int tWrapCol = Integer.parseInt(StrTool.cTrim(request.getParameter("RiskWrapGridCol")));
            String tRiskWrapCode[] = request.getParameterValues("RiskWrapGrid1");
            String tRiskWrapAmntname[] = request.getParameterValues("RiskWrapGrid3");
            String tRiskWrapAmnt[] = request.getParameterValues("RiskWrapGrid4");
            String tRiskWrapPremname[] = request.getParameterValues("RiskWrapGrid5");
            String tRiskWrapPrem[] = request.getParameterValues("RiskWrapGrid6");
            String tRiskWrapInsuYearname[] = request.getParameterValues("RiskWrapGrid7");
            String tRiskWrapInsuYear[] = request.getParameterValues("RiskWrapGrid8");
            String tRiskWrapInsuYearFlagname[] = request.getParameterValues("RiskWrapGrid9");
            String tRiskWrapInsuYearFlag[] = request.getParameterValues("RiskWrapGrid10");
            String tRiskWrapCopysname[] = request.getParameterValues("RiskWrapGrid11");
            String tRiskWrapCopys[] = request.getParameterValues("RiskWrapGrid12");
            
            String tWrapCode = request.getParameter("RiskWrapGrid1");
            
            if (tWrapChk != null)
            {
                for (int index = 0; index < tWrapChk.length; index++)
                {
                    if (tWrapChk[index].equals("1"))
                    {
                        for(int i=3;i<13 ;i=i+2){
                         LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
                         tLCRiskDutyWrapSchema.setRiskWrapCode(tRiskWrapCode[index]);
                         if(i==3){
                         tLCRiskDutyWrapSchema.setCalFactor("Amnt");
                         tLCRiskDutyWrapSchema.setCalFactorValue(tRiskWrapAmnt[index]);
                         }else if(i==5){
                          tLCRiskDutyWrapSchema.setCalFactor("Prem");
                         tLCRiskDutyWrapSchema.setCalFactorValue(tRiskWrapPrem[index]);
                         }else if(i==7){
                          tLCRiskDutyWrapSchema.setCalFactor("InsuYear");
                         tLCRiskDutyWrapSchema.setCalFactorValue(tRiskWrapInsuYear[index]);
                         }else if(i==9){
                          tLCRiskDutyWrapSchema.setCalFactor("InsuYearFlag");
                         tLCRiskDutyWrapSchema.setCalFactorValue(tRiskWrapInsuYearFlag[index]);
                         }else if(i==11){
                          tLCRiskDutyWrapSchema.setCalFactor("Copys");
                         tLCRiskDutyWrapSchema.setCalFactorValue(tRiskWrapCopys[index]);
                         }
                         
                         System.out.println("套餐编码:" + tLCRiskDutyWrapSchema.getRiskWrapCode());
                         mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
                        }
                    }
                    System.out.println("mmmmmmmmmmmmm..." + mLCRiskDutyWrapSet.size());
                }
            }

            //处理LDPerson信息
            String tWorkName = request.getParameter("WorkName");
            System.out.println("工作单位是..." + tWorkName);
            //完成全部的封装，开始递交．
            FamilySingleContUI tFamilySingleContUI = new FamilySingleContUI();
            try
            {
                VData tVData = new VData();
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("WorkName", tWorkName);
                tTransferData.setNameAndValue("WrapCode", tWrapCode);
                tVData.add(tLCContSchema);
                tVData.add(tLCInsuredSet);
                tVData.add(tLCAppntSchema);

                tVData.add(tG);
                tVData.add(tTransferData);
                tVData.add(tLCBnfSet);
                tVData.add(mLCRiskDutyWrapSet);
                System.out.println("开始递交数据！" + tAction);
                tFamilySingleContUI.submitData(tVData, tAction);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                Content = "保存失败，原因是:" + ex.toString();
                FlagStr = "Fail";
            }
            if (FlagStr == "")
            {
                tError = tFamilySingleContUI.mErrors;
                if (!tError.needDealError())
                {
                    Content = " 保存成功! ";
                    FlagStr = "Success";
                }
                else
                {
                    Content = "保存失败，原因是:" + tError.getFirstError();
                    FlagStr = "Fail";
                }
            }
%>
<html>
    <script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
