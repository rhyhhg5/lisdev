
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  // showSubmitFrame(mDebug);
  initContGrid();

  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	for( i = 0; i < ContGrid.mulLineCount; i++ )
	{
		if( ContGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all("printButton").disabled=true;
	
	fm.submit();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;
	arrSelected = new Array();
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

function afterSubmit( FlagStr, content )
{
  window.focus();
  try
  {
	  showInfo.close();
	}
	catch(ex)
	{}
	
	fm.all("printButton").disabled=false;
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
	 
}

function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initContGrid();
    var strSQL = "";
    if(fm.PrtNo.value == "" && fm.ContNo.value == "" && fm.startUWDate.value == "")
    {
        alert("印刷号、保单号和签单起始日期至少录入一个！");
        return false;
    }
//    if(fm.PrtNo.value != "" || fm.ContNo.value != "")
//    {
//        fm.startUWDate.value = "";
//        fm.endUWDate.value = "";
//   }
//   var printstate =fm.SFState.value;
//    if(printstate==""||printstate==null){
//    alert("请选择是否打印！");
//    return false;
//    }
var tPrintState = fm.SFState.value;
   var consql="";
    switch(tPrintState)
    {
        case "1":
            consql += " and PrintCount = 0 ";
            break;
        case "0":
            consql += " and PrintCount >= 1 ";
            break;
    }

    var strSql = " select contno,prtno,prem,appntname,CValiDate,printcount,"
               + " case when PrintCount > 0 then '已打印' else '未打印' end, CodeName('lcsalechnl', SaleChnl),b.groupagentcode "
               + " from lccont a,laagent b where a.appflag='1' and a.signdate is not null and a.cardflag='2' and a.familytype='F' "
               + consql
               + getWherePart('a.SaleChnl' ,'SaleChnl' )
               + getWherePart('a.ContNo','ContNo')
               + getWherePart('a.PrtNo', 'PrtNo')
               + getWherePart('b.groupAgentCode', 'AgentCode')
               + getWherePart('a.ManageCom', 'ManageCom', 'like')
               + getWherePart('a.UWDate','startUWDate','>=')
               + getWherePart('a.UWDate','endUWDate','<=')
               + " and a.agentcode=b.agentcode and a.agentgroup = b.agentgroup "
               + " order by a.ManageCom,a.AgentGroup,b.groupAgentCode "
               + "with ur ";
//if(printstate=="1"){
//	strSQL = "select ContNo,PrtNo,Prem,AppntName,CValiDate,PrintCount," 
//	  +" case when PrintCount > 0 then '已打印' else '未打印' end, CodeName('lcsalechnl', SaleChnl),agentcode" 
//      +" from lccont a where appflag='1' and printcount< 1 and signdate is not null and cardflag='2' and familytype='F'"
//      + " and ManageCom like '" + fm.ManageCom.value + "%'"
//      + getWherePart('a.ContNo','ContNo')
//	  + getWherePart('a.PrtNo','PrtNo')
//	  + getWherePart('a.AgentCode','AgentCode')
//	  + getWherePart('a.SaleChnl','SaleChnl')
	  //+ getWherePart('a.ManageCom', 'ManageCom', 'like')
//      + getWherePart('a.UWDate','startUWDate','>=')
 //     + getWherePart('a.UWDate','endUWDate','<=')
//	  + " order by ManageCom,AgentGroup,AgentCode "
//	  + "with ur ";
//}else{
//    	strSQL = "select ContNo,PrtNo,Prem,AppntName,CValiDate,PrintCount," 
//	  +" case when PrintCount > 0 then '已打印' else '未打印' end, CodeName('lcsalechnl', SaleChnl),agentcode" 
//      +" from lccont where appflag='1' and printcount>= 1 and signdate is not null and cardflag='2' and familytype='F'"
//      + getWherePart('ContNo')
//	  + getWherePart('PrtNo')
//	  + getWherePart('AgentCode')
//	  + getWherePart('SaleChnl')
//	  + getWherePart('ManageCom', 'ManageCom', 'like')
//      + getWherePart('UWDate','startUWDate','>=')
//     + getWherePart('UWDate','endUWDate','<=')
//	  + " order by ManageCom,AgentGroup,AgentCode "
//	  + "with ur ";

//}
	turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);

	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}

	turnPage.queryModal(strSql, ContGrid);
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}

function downloadCustomerCard(suffix)
{
  var count = 0;
  for(var i = 0; i < ContGrid.mulLineCount; i++)
  {
    if(ContGrid.getChkNo(i))
    {
      count++;
    }
  }
  if(count == 0)
  {
    alert("请选择保单");
    return false;
  }
  
  var fmAction = fm.action;
  fm.action = "../intlapp/DownloadCustCardSave.jsp?Suffix=" + suffix;
  fm.submit();
}