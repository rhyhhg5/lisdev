<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">
	
    <script src="FamilyTaxListImportInput.js"></script>
    <%@include file="FamilyTaxListImportInit.jsp" %>
</head>

<body onload="initForm();">
    <form action="" method="post" name="fmImport" target="fraSubmit" enctype="multipart/form-data">
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divBatchApply);"/>
                </td>
                <td class="titleImg">批次清单导入</td>
            </tr>
        </table>
        <div id="divBatchApply" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">批次号</td>
                    <td class="input">
                        <input class="readonly" name="BatchNo" readonly="readonly" />
                    </td>
                    <td style="display:'none'">
                        <input class="cssButton" type="button" id="btnBatchNoApply" name="btnBatchNoApply" value="申  请" onclick="applyNewBatchNo();" />
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <div id="divFileImport" name="divFileImport" style="display:''">
            <span><font color="red">导入文件名必须与批次号一致。</font></span>
            <table class="common">
                <tr class="common">
                    <td class="title">清单文件</td>
                    <td class="common">
                        <input class="cssfile" name="FileName" type="file" />
                    </td>
                </tr>
            </table>
            <input class="cssButton" type="button" id="btnImport" name="btnImport" value="清单导入" onclick="importTaxList();" />
        </div>
    </form>
  	<hr />
	<form action="" method="post" name="fm" target="fraSubmit">        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportErrCondCode);" />
                </td>
                <td class="titleImg">批次导入日志查询</td>
            </tr>
        </table>
        
        <div id="divImportErrCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">导入批次号</td>
                    <td class="input">
                        <input class="common" name="ImportErrBatchNo"  readonly="readonly"  />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>

            <table>
                <tr>
                    <td class="common">
                       
                        <input class="cssButton" type="button" value="查  询" onclick="queryImportErrLog();" />
                    	<input class=cssButton value="下  载"  id='download' name='download' type=button onclick="queryDown(); return false;" >
                    </td>
                </tr>
            </table>
            
            <!-- Mulline Code -->
            <table>
                <tr>
                    <td class="common">
                        <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportErrLogGrid);" />
                    </td>
                    <td class="titleImg">导入批次日志</td>
                </tr>
            </table>
            <div align="center" id="divImportErrLogGrid" style="display: ''">
                <table class="common">
                    <tr class="common">
                        <td>
                            <span id="spanImportErrLogGrid"></span> 
                        </td>
                    </tr>
                </table>
                
                <div id="divImportErrLogGridPage" style="display: ''" align="center">
                    <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                    <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                    
                    <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                    <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                   
                </div>
            </div>
            <!-- Mulline Code End -->
        </div>
        <hr />
        <table style="display:'none'">
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCondCode);" />
                </td>
                <td class="titleImg">导入批次查询</td>
            </tr>
        </table>
        <div id="divCondCode" style="display:'none'">
            <table class="common">
                <tr class="common">
                    <td class="title">导入批次号</td>
                    <td class="input">
                        <input class="readonly" name="ImportBatchNo" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>

        <table style="display:'none'">
            <tr>
                <td class="common">
                    
                    <input class="cssButton" type="button" value="查  询" onclick="queryBatchList();" />
                	<input class=cssButton value="下  载"  type=button onclick="queryDown();" name="DownloadButton">
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportBatchGrid);" />
                </td>
                <td class="titleImg">导入清单列表</td>
            </tr>
        </table>
        <div id="divBatchInfo" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">批次号</td>
                    <td class="input">
                        <input class="common" name="BatchNo" readonly="readonly" />
                    </td>
                    <td class="title">导入人数</td>
                    <td class="input">
                        <input class="common" name="SumImportBatchCount" readonly="readonly" />
                    </td>
                    <td class="title">导入成功人数</td>
                    <td class="input">
                        <input class="common" name="SuccessCount" readonly="readonly" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divBatchListInfo" style="display:'none'">
            <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportBatchGrid);" />
                </td>
                <td class="titleImg">客户查询条件</td>
            </tr>
            </table>
            <table class="common">
                <tr class="common">
                    <td class="title">客户姓名</td>
                    <td class="input">
                        <input class="common" name="Name" />
                    </td>
                    <td class="title">证件类型</td>
                    <td class="input">
                        <Input class=codeNo name="IDType"  ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);"><input class=codename name=IDTypeName readonly=true elementtype=nacessary> 
                    </td>
                    <td class="title">证件号码</td>
                    <td class="input">
                        <input class="common" name="IDNo"  />
                    </td>
                </tr>
            </table>
            <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryInsured();" /> 	
                </td>
            </tr>
            
           </table>
        </div>
        <div align="center" id="divImportBatchGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanImportBatchGrid"></span> 
                    </td>
                </tr>
            </table>
            <div id="divImportBatchGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" />
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" />
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
            </div>
        </div>        
        
        <div style="display: ''" align="left">
            <input type="button" class="cssButton"  id="btnImportDelete" name="btnImportDelete" value="错误保单删除" onclick="delImport();" />            
            <input type="button" class="cssButton"  id="btnImportDelete" name="btnImportDelete" value="批次整单删除" onclick="delImport1();" />    
            <input type="button" class="cssButton"  id="btnImportConfirm" name="btnImportConfirm" value="批次导入确认" onclick="confirmImport();" />
        </div>
        
        <hr />
        
        <br />
        
        <input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="返  回" onclick="goBack();" />
        <input type="hidden" id="fmOperatorFlag" name="fmOperatorFlag" />
        <Input type=hidden name=Sql>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>

