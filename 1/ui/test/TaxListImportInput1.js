/**
 * Created by admin on 2016/10/10.
 */
/**
 * 导入清单。
 */
function importTaxList(){

    //if(!beforeImportTaxList()){
    //    return false;
    //}

    fmImport.btnImport.disabled = true;

    var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fmImport.action = "./ImportFileTest.jsp";
    fmImport.submit();

    fmImport.action = "";

}