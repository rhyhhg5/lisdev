<%
//程序名称：AllPBqQueryInit.jsp
//程序功能：
//创建日期：2002-12-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>  
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>                               

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('ContNo').value = '';
  }
  catch(ex)
  {
    alert("在AllPBqQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AllPBqQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initTMQGrid();
  }
  catch(re)
  {
    alert("AllPBqQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initTMQGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="135px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种号";         		//列名
      iArray[2][1]="135px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21] = "PolNo";
      
      iArray[3]=new Array();
      iArray[3][0]="状态";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=65;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21] = "StateType";

      iArray[4]=new Array();
      iArray[4][0]="失效时间";         		//列名
      iArray[4][1]="90px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][21] = "StartDate";

      TMQGrid = new MulLineEnter( "fm" , "TMQGrid" ); 
      
      //这些属性必须在loadMulLine前
      TMQGrid.mulLineCount = 0;   
      TMQGrid.displayTitle = 1;
      TMQGrid.unlocked = 1;
      //TMQGrid.canSel = 1;
      TMQGrid.canChk =1;	
     // TMQGrid.selBoxEventFuncName = "reportDetailClick";
      //TMQGrid.chkBoxEventFuncName = "reportDetailClick";
      TMQGrid.hiddenPlus=0;           //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      TMQGrid.hiddenSubtraction=0;    //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
      TMQGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
      //TMQGrid.setRowColData(1,1,"asdf");
      //var index = TMQGrid.getSelNo ();
  	  //alert("--");
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>