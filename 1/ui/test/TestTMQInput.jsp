<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TestTMQInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="TestTMQInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();">
  <form action="./TestTMQSubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入个人保全查询条件：
			</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo >
          </TD>
        </TR>
    </table>
          <table class= common align="center">
          <INPUT class = cssbutton VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT class = cssbutton VALUE="失效中止" TYPE=button onclick="CancelFail();">
          <INPUT class = cssbutton VALUE="失效终止" TYPE=button onclick="EndFail();">
          </table>
          	
    <Div  id= "divTMQDetail" style= "display: ''">
    <table>
    	<tr>
        	<td class=common align="center" >
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTMQMain);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divTMQMain" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanTMQGrid" >
  					</span> 
  			  	</td>
  			</tr> 		
    	</table>
    </div> 
    </Div>
    <div>
      <INPUT class = cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class = cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class = cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class = cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	 
	</div>
  	<input type=hidden name=Transact >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
