<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMRiskRnewInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskRnewInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMRiskRnewsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMRiskRnew1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRiskRnew1);">
      </td>
      <td class= titleImg>
        LMRiskRnew1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMRiskRnew1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
          <TD  class= title>
            续保类型
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=RnewType >生成新单
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            宽限期
          </TD>
          <TD  class= input>
            <Input class=common name=GracePeriod >
          </TD>
          <TD  class= title>
            宽限期单位
          </TD>
          <TD  class= input>
            <Input class= code name=GracePeriodUnit >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            宽限日期计算方式
          </TD>
          <TD  class= input>
            <Input class= code name=GraceDateCalMode >
          </TD>
          <TD  class= title>
            续保最大年龄
          </TD>
          <TD  class= input>
            <Input class= common name=MaxAge >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            续保限制
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=RnewLmt >理赔后能续保
          </TD>
          <TD  class= title>
            原保单终止日算法
          </TD>
          <TD  class= input>
            <Input class= code name=EndDateCalMode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            续保确认日算法
          </TD>
          <TD  class= input>
            <Input class= code name=ComfirmDateCalMode >
          </TD>
          <TD  class= title>
            保证续保标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=AssuRnewFlag >能保证续保
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保证续保要求的续保次数
          </TD>
          <TD  class= input>
            <Input class= common name=RnewTimes >
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
