<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMDutyGetInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMDutyGetInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMDutyGetsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMDutyGet1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMDutyGet1);">
      </td>
      <td class= titleImg>
        LMDutyGet1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMDutyGet1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            给付代码
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyCode >
          </TD>
          <TD  class= title>
            给付名称
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            给付类型
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyKind >
          </TD>
          <TD  class= title>
            给付间隔
          </TD>
          <TD  class= input>
            <Input class= common name=GetIntv >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            默认值
          </TD>
          <TD  class= input>
            <Input class= common name=DefaultVal >
          </TD>
          <TD  class= title>
            算法
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            反算算法
          </TD>
          <TD  class= input>
            <Input class= common name=CnterCalCode >
          </TD>
          <TD  class= title>
            其他算法
          </TD>
          <TD  class= input>
            <Input class= common name=OthCalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领期间
          </TD>
          <TD  class= input>
            <Input class= common name=GetYear >
          </TD>
          <TD  class= title>
            起领期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=GetYearFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领日期计算参照
          </TD>
          <TD  class= input>
            <Input class= code name=StartDateCalRef >
          </TD>
          <TD  class= title>
            起领日期计算方式
          </TD>
          <TD  class= input>
            <Input class= common name=StartDateCalMode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领期间上限
          </TD>
          <TD  class= input>
            <Input class= common name=MinGetStartPeriod >
          </TD>
          <TD  class= title>
            止领期间
          </TD>
          <TD  class= input>
            <Input class= common name=GetEndPeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            止领期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=GetEndUnit >
          </TD>
          <TD  class= title>
            止领日期计算参照
          </TD>
          <TD  class= input>
            <Input class= code name=EndDateCalRef >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            止领日期计算方式
          </TD>
          <TD  class= input>
            <Input class= code name=EndDateCalMode value="" ondblclick="return showCodeList('EndDateCalMode',[this]);" 
            onkeyup="return showCodeListKey('EndDateCalMode',[this]);">
          </TD>
          <TD  class= title>
            止领期间下限
          </TD>
          <TD  class= input>
            <Input class= common name=MaxGetEndPeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            递增标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=AddFlag >是
          </TD>
          <TD  class= title>
            性别关联标记
          </TD>
          <TD  class= input>
            <Input class= code name=SexRelaFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位投保关联标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=UnitAppRelaFlag >有关
          </TD>
          <TD  class= title>
            算入保额标记
          </TD>
          <TD  class= input>
            <Input class= code name=AddAmntFlag value="" ondblclick="return showCodeList('AddAmntFlag',[this]);" 
            onkeyup="return showCodeListKey('AddAmntFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            贴现领取标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=DiscntFlag >允许
          </TD>
          <TD  class= title>
            利率标记
          </TD>
          <TD  class= input>
            <Input class= code name=InterestFlag value="" ondblclick="return showCodeList('InterestFlag',[this]);" 
            onkeyup="return showCodeListKey('InterestFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            多被保人分享标记
          </TD>
          <TD  class= input>
            <Input class= code name=ShareFlag value="" ondblclick="return showCodeList('ShareFlag',[this]);" 
            onkeyup="return showCodeListKey('ShareFlag',[this]);">
          </TD>
          <TD  class= title>
            录入标记
          </TD>
          <TD  class= input>
            <Input class= code name=InpFlag value="" ondblclick="return showCodeList('InpFlag',[this]);" 
            onkeyup="return showCodeListKey('InpFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            受益人标记
          </TD>
          <TD  class= input>
            <Input class= code name=BnfFlag value="" ondblclick="return showCodeList('BnfFlag',[this]);" 
            onkeyup="return showCodeListKey('BnfFlag',[this]);">
          </TD>
          <TD  class= title>
            催付标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=UrgeGetFlag >发催付
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            被保人死亡后有效标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=DeadValiFlag >有效
          </TD>
          <TD  class= title>
            给付初始化标记
          </TD>
          <TD  class= input>
            <Input class= common name=GetInitFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起付限
          </TD>
          <TD  class= input>
            <Input class= common name=GetLimit >
          </TD>
          <TD  class= title>
            赔付限额
          </TD>
          <TD  class= input>
            <Input class= common name=MaxGet >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            赔付比例
          </TD>
          <TD  class= input>
            <Input class= common name=GetRate >
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
