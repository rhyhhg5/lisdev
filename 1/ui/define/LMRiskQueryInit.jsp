<%
//程序名称：LMRiskQuery.js
//程序功能：
//创建日期：2003-10-28 15:15:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
 
  }
  catch(ex)
  {
    alert("在LMRiskQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在LMRiskQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
   
  try
  {
     initInpBox();
     initLMRiskGrid();
    
  }
  catch(re)
  {
    alert("LMRiskQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化LDRiskComOperateGrid
 ************************************************************
 */
var LMRiskGrid;          //定义为全局变量，提供给displayMultiline使用
function initLMRiskGrid()
  {  
                         
    var iArray = new Array();     
      try
      {
        
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=0; 
        
        iArray[1]=new Array();
        iArray[1][0]="险种编码";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=0; 
        
        iArray[2]=new Array();
        iArray[2][0]="险种版本";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0; 
        
        iArray[3]=new Array();
        iArray[3][0]="险种名称";
        iArray[3][1]="120px";
        iArray[3][2]=100;
        iArray[3][3]=0; 
        
        iArray[4]=new Array();
        iArray[4][0]="选择责任标记";
        iArray[4][1]="70px";
        iArray[4][3]=0; 
        
        iArray[5]=new Array();
        iArray[5][0]="续期收费标记";
        iArray[5][1]="70px";
        iArray[5][3]=0; 
        
        iArray[6]=new Array();
        iArray[6][0]="生存给付标记";
        iArray[6][1]="70px";
        iArray[6][3]=0; 
        
        iArray[7]=new Array();
        iArray[7][0]="保全标记";
        iArray[7][1]="50px";
        iArray[7][3]=0; 
        
        iArray[8]=new Array();
        iArray[8][0]="续保标记";
        iArray[8][1]="50px";
        iArray[8][3]=0; 
        
        iArray[9]=new Array();
        iArray[9][0]="核保标记";
        iArray[9][1]="50px";
        iArray[9][3]=0; 
        
        iArray[10]=new Array();
        iArray[10][0]="分保标记";
        iArray[10][1]="50px";
        iArray[10][3]=0; 
        
        iArray[11]=new Array();
        iArray[11][0]="保险帐户标记";
        iArray[11][1]="70px";
        iArray[11][3]=0; 
        
        LMRiskGrid = new MulLineEnter( "fm" , "LMRiskGrid" ); 
        //这些属性必须在loadMulLine前
        LMRiskGrid.mulLineCount = 12;   
        LMRiskGrid.displayTitle = 1;
        LMRiskGrid.canSel=1;
        LMRiskGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化LMRiskGrid时出错："+ ex);
      }
    }
</script>
