<%
//程序名称：LMDutyGetInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMDutyGetSchema tLMDutyGetSchema   = new LMDutyGetSchema();

  UILMDutyGet tLMDutyGet   = new UILMDutyGet();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "insert";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

    tLMDutyGetSchema.setGetDutyCode(request.getParameter("GetDutyCode"));
    tLMDutyGetSchema.setGetDutyName(request.getParameter("GetDutyName"));
    tLMDutyGetSchema.setGetDutyKind(request.getParameter("GetDutyKind"));
    tLMDutyGetSchema.setGetIntv(request.getParameter("GetIntv"));
    tLMDutyGetSchema.setDefaultVal(request.getParameter("DefaultVal"));
    tLMDutyGetSchema.setCalCode(request.getParameter("CalCode"));
    tLMDutyGetSchema.setCnterCalCode(request.getParameter("CnterCalCode"));
    tLMDutyGetSchema.setOthCalCode(request.getParameter("OthCalCode"));
    tLMDutyGetSchema.setGetYear(request.getParameter("GetYear"));
    tLMDutyGetSchema.setGetYearFlag(request.getParameter("GetYearFlag"));
    tLMDutyGetSchema.setStartDateCalRef(request.getParameter("StartDateCalRef"));
    tLMDutyGetSchema.setStartDateCalMode(request.getParameter("StartDateCalMode"));
    tLMDutyGetSchema.setMinGetStartPeriod(request.getParameter("MinGetStartPeriod"));
    tLMDutyGetSchema.setGetEndPeriod(request.getParameter("GetEndPeriod"));
    tLMDutyGetSchema.setGetEndUnit(request.getParameter("GetEndUnit"));
    tLMDutyGetSchema.setEndDateCalRef(request.getParameter("EndDateCalRef"));
    tLMDutyGetSchema.setEndDateCalMode(request.getParameter("EndDateCalMode"));
    tLMDutyGetSchema.setMaxGetEndPeriod(request.getParameter("MaxGetEndPeriod"));
    tLMDutyGetSchema.setAddFlag(request.getParameter("AddFlag"));
    tLMDutyGetSchema.setSexRelaFlag(request.getParameter("SexRelaFlag"));
    tLMDutyGetSchema.setUnitAppRelaFlag(request.getParameter("UnitAppRelaFlag"));
    tLMDutyGetSchema.setAddAmntFlag(request.getParameter("AddAmntFlag"));
    tLMDutyGetSchema.setDiscntFlag(request.getParameter("DiscntFlag"));
    tLMDutyGetSchema.setInterestFlag(request.getParameter("InterestFlag"));
    tLMDutyGetSchema.setShareFlag(request.getParameter("ShareFlag"));
    tLMDutyGetSchema.setInpFlag(request.getParameter("InpFlag"));
    tLMDutyGetSchema.setBnfFlag(request.getParameter("BnfFlag"));
    tLMDutyGetSchema.setUrgeGetFlag(request.getParameter("UrgeGetFlag"));
    tLMDutyGetSchema.setDeadValiFlag(request.getParameter("DeadValiFlag"));
    tLMDutyGetSchema.setGetInitFlag(request.getParameter("GetInitFlag"));
    tLMDutyGetSchema.setGetLimit(request.getParameter("GetLimit"));
    tLMDutyGetSchema.setMaxGet(request.getParameter("MaxGet"));
    tLMDutyGetSchema.setGetRate(request.getParameter("GetRate"));


  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLMDutyGetSchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMDutyGet.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = t%functionname%.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

