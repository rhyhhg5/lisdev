<%
//程序名称：LMDutyQuery.js
//程序功能：
//创建日期：2003-10-28 15:15:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
 
  }
  catch(ex)
  {
    alert("在LMDutyQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在LMDutyQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
   
  try
  {
     
     initLMDutyGrid();
    
  }
  catch(re)
  {
    alert("LMDutyQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化LMDutyGrid
 ************************************************************
 */
var LMDutyGrid;          //定义为全局变量，提供给displayMultiline使用
function initLMDutyGrid()
  {  
                         
    var iArray = new Array();     
      try
      {
        
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=0; 
        
        iArray[1]=new Array();
        iArray[1][0]="责任代码";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=2; 
        iArray[1][4]="DutyCode";              	        //是否引用代码:null||""为不引用
        iArray[1][5]="4";              	                //引用代码对应第几列，'|'为分割符
        iArray[1][9]="责任代码|code:DutyCode&NOTNULL";
        iArray[1][18]=250;
        iArray[1][19]= 0 ;
        
        iArray[2]=new Array();
        iArray[2][0]=" 责任名称";
        iArray[2][1]="120px";
        iArray[2][2]=100;
        iArray[2][3]=2; 
        iArray[2][4]="DutyName";              	        //是否引用代码:null||""为不引用
        iArray[2][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[2][9]=" 责任名称|code:DutyName&NOTNULL";
        iArray[2][18]=250;
        iArray[2][19]= 0 ;
        
        iArray[3]=new Array();
        iArray[3][0]="缴费终止期间";
        iArray[3][1]="120px";
        iArray[3][2]=100;
        iArray[3][3]=2; 
        iArray[3][4]="PayEndYear";              	        //是否引用代码:null||""为不引用
        iArray[3][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[3][9]="缴费终止期间code:PayEndYear&NOTNULL";
        iArray[3][18]=250;
        iArray[3][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]=" 缴费终止期间单位";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]=" 缴费终止期间单位|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]="缴费终止日期计算参照";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]=" 缴费终止日期计算参照 |code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]="缴费终止日期计算方式";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]=" 缴费终止日期计算方式|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]=" 起领期间";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]=" 起领期间|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]=" 起领期间单位";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]="起领期间单位|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]=" 保险期间";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]=" 保险期间|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]=" 保险期间单位";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]="保险期间单位|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]=" 意外责任期间";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]=" 意外责任期间|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]=" 意外责任期间单";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="PayEndYearFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]=" 意外责任期间|code:PayEndYearFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        
        iArray[5]=new Array();
        iArray[5][0]="缴费终止日期计算参照";
        iArray[5][1]="200px";
        iArray[5][2]=100;
        iArray[5][3]=0; 
        
  
        LMDutyGrid = new MulLineEnter( "fm" , "LMDutyGrid" ); 
        //这些属性必须在loadMulLine前
        LMDutyGrid.mulLineCount = 10;   
        LMDutyGrid.displayTitle = 1;
        LMDutyGrid.canSel=1;
        LMDutyGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化LMDutyGrid时出错："+ ex);
      }
    }
</script>
