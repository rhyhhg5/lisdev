<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人  gyj  更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMRiskAccPayInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskAccPayInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMRiskAccPaysave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMRiskAccPay1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRiskAccPay1);">
      </td>
      <td class= titleImg>
        LMRiskAccPay1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMRiskAccPay1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
          <TD  class= title>
            保险帐户号码
          </TD>
          <TD  class= input>
            <Input class= common name=InsuAccNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险帐户名称
          </TD>
          <TD  class= input>
            <Input class= common name=InsuAccName >
          </TD>
          <TD  class= title>
            缴费编码
          </TD>
          <TD  class= input>
            <Input class= common name=PayPlanCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费名称
          </TD>
          <TD  class= input>
            <Input class= common name=PayPlanName >
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
