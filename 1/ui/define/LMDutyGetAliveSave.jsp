<%
//程序名称：LMDutyGetAliveInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMDutyGetAliveSchema tLMDutyGetAliveSchema   = new LMDutyGetAliveSchema();

  UILMDutyGetAlive tLMDutyGetAlive   = new UILMDutyGetAlive();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "insert";

  String tRela  = "";
  String FlagStr = "";
  String Content = "";

    tLMDutyGetAliveSchema.setGetDutyCode(request.getParameter("GetDutyCode"));
    tLMDutyGetAliveSchema.setGetDutyName(request.getParameter("GetDutyName"));
    tLMDutyGetAliveSchema.setGetDutyKind(request.getParameter("GetDutyKind"));
    tLMDutyGetAliveSchema.setType(request.getParameter("Type"));
    tLMDutyGetAliveSchema.setGetIntv(request.getParameter("GetIntv"));
    tLMDutyGetAliveSchema.setDefaultVal(request.getParameter("DefaultVal"));
    tLMDutyGetAliveSchema.setCalCode(request.getParameter("CalCode"));
    tLMDutyGetAliveSchema.setCnterCalCode(request.getParameter("CnterCalCode"));
    tLMDutyGetAliveSchema.setOthCalCode(request.getParameter("OthCalCode"));
    tLMDutyGetAliveSchema.setGetStartPeriod(request.getParameter("GetStartPeriod"));
    tLMDutyGetAliveSchema.setGetStartUnit(request.getParameter("GetStartUnit"));
    tLMDutyGetAliveSchema.setStartDateCalRef(request.getParameter("StartDateCalRef"));
    tLMDutyGetAliveSchema.setStartDateCalMode(request.getParameter("StartDateCalMode"));
    tLMDutyGetAliveSchema.setMinGetStartPeriod(request.getParameter("MinGetStartPeriod"));
    tLMDutyGetAliveSchema.setGetEndPeriod(request.getParameter("GetEndPeriod"));
    tLMDutyGetAliveSchema.setGetEndUnit(request.getParameter("GetEndUnit"));
    tLMDutyGetAliveSchema.setEndDateCalRef(request.getParameter("EndDateCalRef"));
    tLMDutyGetAliveSchema.setEndDateCalMode(request.getParameter("EndDateCalMode"));
    tLMDutyGetAliveSchema.setMaxGetEndPeriod(request.getParameter("MaxGetEndPeriod"));
    tLMDutyGetAliveSchema.setAddFlag(request.getParameter("AddFlag"));
    tLMDutyGetAliveSchema.setAddIntv(request.getParameter("AddIntv"));
    tLMDutyGetAliveSchema.setAddStartPeriod(request.getParameter("AddStartPeriod"));
    tLMDutyGetAliveSchema.setAddStartUnit(request.getParameter("AddStartUnit"));
    tLMDutyGetAliveSchema.setAddEndPeriod(request.getParameter("AddEndPeriod"));
    tLMDutyGetAliveSchema.setAddEndUnit(request.getParameter("AddEndUnit"));
    tLMDutyGetAliveSchema.setAddType(request.getParameter("AddType"));
    tLMDutyGetAliveSchema.setAddValue(request.getParameter("AddValue"));
    tLMDutyGetAliveSchema.setMaxGetCount(request.getParameter("MaxGetCount"));
    tLMDutyGetAliveSchema.setAfterGet(request.getParameter("AfterGet"));
    tLMDutyGetAliveSchema.setGetActionType(request.getParameter("GetActionType"));
    tLMDutyGetAliveSchema.setUrgeGetFlag(request.getParameter("UrgeGetFlag"));
    tLMDutyGetAliveSchema.setDiscntFlag(request.getParameter("DiscntFlag"));
    tLMDutyGetAliveSchema.setGetCond(request.getParameter("GetCond"));


  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLMDutyGetAliveSchema);

   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMDutyGetAlive.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = t%functionname%.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

