//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ; 
  if (fm.RiskCode.value==""||fm.RiskVer.value==""||fm.RiskName.value==""){
      alert("所填写信息不全,请确认后再提交保存!");
      return false;
  }
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  
  if (fm.ChoDutyFlag.checked == true){
      fm.ChoDutyFlag.value = "Y";	
  }else{
      fm.ChoDutyFlag.value = "N";
  }
  if (fm.CPayFlag.checked == true){
      fm.CPayFlag.value = "Y";	
  }else{
      fm.CPayFlag.value = "N";
  }
  if (fm.GetFlag.checked == true){
      fm.GetFlag.value = "Y";	
  }else{
      fm.GetFlag.value = "N";
  }
  if (fm.EdorFlag.checked == true){
      fm.EdorFlag.value = "Y";	
  }else{
      fm.EdorFlag.value = "N";
  }
  
  if (fm.RnewFlag.checked == true){
      fm.RnewFlag.value = "Y";	
  }else{
      fm.RnewFlag.value = "N";
  }
  if (fm.UWFlag.checked == true){
      fm.UWFlag.value = "Y";	
  }else{
      fm.UWFlag.value = "N";
  }
  if (fm.RinsFlag.checked == true){
      fm.RinsFlag.value = "Y";	
  }else{
      fm.RinsFlag.value = "N";
  }
  if (fm.InsuAccFlag.checked == true){
      fm.InsuAccFlag.value = "Y";	
  }else{
      fm.InsuAccFlag.value = "N";
  }

  fm.action = "./LMRiskSave.jsp?RiskCode="+fm.RiskCode.value+"&RiskVer="+fm.RiskVer.value+"&RiskName="+fm.RiskName.value+"&ChoDutyFlag="+fm.ChoDutyFlag.value+"&CPayFlag="+fm.CPayFlag.value+"&GetFlag="+fm.GetFlag.value+"&EdorFlag="+fm.EdorFlag.value+"&RnewFlag="+fm.RnewFlag.value+"&UWFlag="+fm.UWFlag.value+"&RinsFlag="+fm.RinsFlag.value+"&InsuAccFlag="+fm.InsuAccFlag.value+"&fmtransact=INSERT||MAIN";
  fm.submit();
   //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  alert(FlagStr);
  if (FlagStr.equals("Fail"))
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LMRisk.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{  
  if (fm.RiskCode.value==""||fm.RiskVer.value==""||fm.RiskName.value==""){
      alert("所填写信息不全,请确认后再提交保存!");
      return false;
  }  
  if (fm.ChoDutyFlag.checked == true){
      fm.ChoDutyFlag.value = "Y";	
  }else{
      fm.ChoDutyFlag.value = "N";
  }
  if (fm.CPayFlag.checked == true){
      fm.CPayFlag.value = "Y";	
  }else{
      fm.CPayFlag.value = "N";
  }
  if (fm.GetFlag.checked == true){
      fm.GetFlag.value = "Y";	
  }else{
      fm.GetFlag.value = "N";
  }
  if (fm.EdorFlag.checked == true){
      fm.EdorFlag.value = "Y";	
  }else{
      fm.EdorFlag.value = "N";
  }
  
  if (fm.RnewFlag.checked == true){
      fm.RnewFlag.value = "Y";	
  }else{
      fm.RnewFlag.value = "N";
  }
  if (fm.UWFlag.checked == true){
      fm.UWFlag.value = "Y";	
  }else{
      fm.UWFlag.value = "N";
  }
  if (fm.RinsFlag.checked == true){
      fm.RinsFlag.value = "Y";	
  }else{
      fm.RinsFlag.value = "N";
  }
  if (fm.InsuAccFlag.checked == true){
      fm.InsuAccFlag.value = "Y";	
  }else{
      fm.InsuAccFlag.value = "N";
  }
  if (confirm("您确实想修改该记录吗?")){
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;  
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
      fm.action = "./LMRiskSave.jsp?RiskCode="+fm.RiskCode.value+"&RiskVer="+fm.RiskVer.value+"&RiskName="+fm.RiskName.value+"&ChoDutyFlag="+fm.ChoDutyFlag.value+"&CPayFlag="+fm.CPayFlag.value+"&GetFlag="+fm.GetFlag.value+"&EdorFlag="+fm.EdorFlag.value+"&RnewFlag="+fm.RnewFlag.value+"&UWFlag="+fm.UWFlag.value+"&RinsFlag="+fm.RinsFlag.value+"&InsuAccFlag="+fm.InsuAccFlag.value+"&fmtransact=UPDATE||MAIN";      
      fm.submit();
  }else{
      alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick(){
  showInfo=window.open("./LMRiskQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick(){  
  if (confirm("您确实想删除该记录吗?")){
      var i = 0;
      var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
      fm.fmtransact.value = "DELETE||MAIN";
      fm.submit();
      initForm();
  }else{
      alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function afterQuery(arrQueryResult)
{
    var arrResult = new Array();
	
    if( arrQueryResult != null ){
	arrResult = arrQueryResult;
        fm.all('RiskCode').value= arrResult[0][0];
        fm.all('RiskVar').value= arrResult[0][1];
        fm.all('RiskName').value= arrResult[0][2];
	if(arrResult[0][3].equals("Y")){
		fm.all('ChoDutyFlag').checked = true;
	}
    }
}               
