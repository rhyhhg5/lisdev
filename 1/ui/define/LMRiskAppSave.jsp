<%
//程序名称：LMRiskAppInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMRiskAppSchema tLMRiskAppSchema   = new LMRiskAppSchema();

  UILMRiskApp tLMRiskApp   = new UILMRiskApp();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "insert";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

    tLMRiskAppSchema.setRiskCode(request.getParameter("RiskCode"));
    tLMRiskAppSchema.setRiskVer(request.getParameter("RiskVer"));
    tLMRiskAppSchema.setRiskName(request.getParameter("RiskName"));
    tLMRiskAppSchema.setKindCode(request.getParameter("KindCode"));
    tLMRiskAppSchema.setRiskType(request.getParameter("RiskType"));
    tLMRiskAppSchema.setRiskType1(request.getParameter("RiskType1"));
    tLMRiskAppSchema.setRiskType2(request.getParameter("RiskType2"));
    tLMRiskAppSchema.setRiskProp(request.getParameter("RiskProp"));
    tLMRiskAppSchema.setRiskPeriod(request.getParameter("RiskPeriod"));
    tLMRiskAppSchema.setRiskTypeDetail(request.getParameter("RiskTypeDetail"));
    tLMRiskAppSchema.setRiskFlag(request.getParameter("RiskFlag"));
    tLMRiskAppSchema.setPolType(request.getParameter("PolType"));
    tLMRiskAppSchema.setInvestFlag(request.getParameter("InvestFlag"));
    tLMRiskAppSchema.setBonusFlag(request.getParameter("BonusFlag"));
    tLMRiskAppSchema.setBonusMode(request.getParameter("BonusMode"));
    tLMRiskAppSchema.setListFlag(request.getParameter("ListFlag"));
    tLMRiskAppSchema.setSubRiskFlag(request.getParameter("SubRiskFlag"));
    tLMRiskAppSchema.setCalDigital(request.getParameter("CalDigital"));
    tLMRiskAppSchema.setCalChoMode(request.getParameter("CalChoMode"));
    tLMRiskAppSchema.setRiskAmntMult(request.getParameter("RiskAmntMult"));
    tLMRiskAppSchema.setInsuPeriodFlag(request.getParameter("InsuPeriodFlag"));
    tLMRiskAppSchema.setMaxEndPeriod(request.getParameter("MaxEndPeriod"));
    tLMRiskAppSchema.setAgeLmt(request.getParameter("AgeLmt"));
    tLMRiskAppSchema.setSignDateCalMode(request.getParameter("SignDateCalMode"));
    tLMRiskAppSchema.setProtocolFlag(request.getParameter("ProtocolFlag"));
    tLMRiskAppSchema.setGetChgFlag(request.getParameter("GetChgFlag"));
    tLMRiskAppSchema.setProtocolPayFlag(request.getParameter("ProtocolPayFlag"));
    tLMRiskAppSchema.setEnsuPlanFlag(request.getParameter("EnsuPlanFlag"));
    tLMRiskAppSchema.setEnsuPlanAdjFlag(request.getParameter("EnsuPlanAdjFlag"));
    tLMRiskAppSchema.setStartDate(request.getParameter("StartDate"));
    tLMRiskAppSchema.setEndDate(request.getParameter("EndDate"));
    tLMRiskAppSchema.setMinAppntAge(request.getParameter("MinAppntAge"));
    tLMRiskAppSchema.setMaxAppntAge(request.getParameter("MaxAppntAge"));
    tLMRiskAppSchema.setMaxInsuredAge(request.getParameter("MaxInsuredAge"));
    tLMRiskAppSchema.setMinInsuredAge(request.getParameter("MinInsuredAge"));
    tLMRiskAppSchema.setAppInterest(request.getParameter("AppInterest"));
    tLMRiskAppSchema.setAppPremRate(request.getParameter("AppPremRate"));
    tLMRiskAppSchema.setInsuredFlag(request.getParameter("InsuredFlag"));
    tLMRiskAppSchema.setShareFlag(request.getParameter("ShareFlag"));
    tLMRiskAppSchema.setBnfFlag(request.getParameter("BnfFlag"));
    tLMRiskAppSchema.setTempPayFlag(request.getParameter("TempPayFlag"));
    tLMRiskAppSchema.setInpPayPlan(request.getParameter("InpPayPlan"));
    tLMRiskAppSchema.setImpartFlag(request.getParameter("ImpartFlag"));
    tLMRiskAppSchema.setInsuExpeFlag(request.getParameter("InsuExpeFlag"));
    tLMRiskAppSchema.setLoanFalg(request.getParameter("LoanFalg"));
    tLMRiskAppSchema.setMortagageFlag(request.getParameter("MortagageFlag"));
    tLMRiskAppSchema.setIDifReturnFlag(request.getParameter("IDifReturnFlag"));
    tLMRiskAppSchema.setCutAmntStopPay(request.getParameter("CutAmntStopPay"));
    tLMRiskAppSchema.setRinsRate(request.getParameter("RinsRate"));
    tLMRiskAppSchema.setSaleFlag(request.getParameter("SaleFlag"));
    tLMRiskAppSchema.setFileAppFlag(request.getParameter("FileAppFlag"));
    tLMRiskAppSchema.setMngCom(request.getParameter("MngCom"));


  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLMRiskAppSchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMRiskApp.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = t%functionname%.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

