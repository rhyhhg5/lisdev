<%
//程序名称：LMDutyInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMDutySchema tLMDutySchema   = new LMDutySchema();

  UILMDuty tLMDuty   = new UILMDuty();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "insert";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

    tLMDutySchema.setDutyCode(request.getParameter("DutyCode"));
    tLMDutySchema.setDutyName(request.getParameter("DutyName"));
    tLMDutySchema.setPayEndYear(request.getParameter("PayEndYear"));
    tLMDutySchema.setPayEndYearFlag(request.getParameter("PayEndYearFlag"));
    tLMDutySchema.setPayEndDateCalRef(request.getParameter("PayEndDateCalRef"));
    tLMDutySchema.setPayEndDateCalMode(request.getParameter("PayEndDateCalMode"));
    tLMDutySchema.setGetYear(request.getParameter("GetYear"));
    tLMDutySchema.setGetYearFlag(request.getParameter("GetYearFlag"));
    tLMDutySchema.setInsuYear(request.getParameter("InsuYear"));
    tLMDutySchema.setInsuYearFlag(request.getParameter("InsuYearFlag"));
    tLMDutySchema.setAcciYear(request.getParameter("AcciYear"));
    tLMDutySchema.setAcciYearFlag(request.getParameter("AcciYearFlag"));
    tLMDutySchema.setCalMode(request.getParameter("CalMode"));


  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLMDutySchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMDuty.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = t%functionname%.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

