<%
//程序名称：LMRiskPayInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.productdef.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMRiskPaySchema tLMRiskPaySchema   = new LMRiskPaySchema();

  OLMRiskPayUI tLMRiskPay   = new OLMRiskPayUI();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  transact = request.getParameter("fmtransact");
    tLMRiskPaySchema.setRiskCode(request.getParameter("RiskCode"));
    tLMRiskPaySchema.setRiskVer(request.getParameter("RiskVer"));
    tLMRiskPaySchema.setRiskName(request.getParameter("RiskName"));
    tLMRiskPaySchema.setUrgePayFlag(request.getParameter("UrgePayFlag"));
    tLMRiskPaySchema.setChargeType(request.getParameter("ChargeType"));
    tLMRiskPaySchema.setCutPayIntv(request.getParameter("CutPayIntv"));
    tLMRiskPaySchema.setPayAvoidType(request.getParameter("PayAvoidType"));
    tLMRiskPaySchema.setChargeAndPrem(request.getParameter("ChargeAndPrem"));
    tLMRiskPaySchema.setBalaDateType(request.getParameter("BalaDateType"));
    tLMRiskPaySchema.setPayAvoidFlag(request.getParameter("PayAvoidFlag"));
    tLMRiskPaySchema.setPayAndRevEffe(request.getParameter("PayAndRevEffe"));
    tLMRiskPaySchema.setGracePeriod(request.getParameter("GracePeriod"));
    tLMRiskPaySchema.setGracePeriodUnit(request.getParameter("GracePeriodUnit"));
    tLMRiskPaySchema.setGraceDateCalMode(request.getParameter("GraceDateCalMode"));
    

  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLMRiskPaySchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMRiskPay.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    //tError = t%functionname%.mErrors;
    tError = tLMRiskPay.mErrors;
    if (!tError.needDealError())
    {   
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

