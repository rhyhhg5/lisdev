<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMDutyGetClmInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMDutyGetClmInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMDutyGetClmsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMDutyGetClm1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMDutyGetClm1);">
      </td>
      <td class= titleImg>
        LMDutyGetClm1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMDutyGetClm1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            给付代码
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyCode >
          </TD>
          <TD  class= title>
            给付名称
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyName >
          </TD>
        </TR>
       </table>
     </Div>
     <!--（列表） -->
     <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
            </td>
            <td class= titleImg>
                     责任给付陪付明细
            </td>
    	</tr>
     </table>
     <Div  id= "divLCInsured2" style= "display: ''">
     <table  class= common>
            <tr  class= common>
                    <td text-align: left colSpan=1>
                                  <span id="spanLMDutyGetClmGrid" >
                                  </span>
                            </td>
                    </tr>
            </table>
     </div>
     <script language="javascript">
	function hideMe()
	{
 		 divLMDutyGetDetail.style.display ='none';
	}
    </script>
    <Div  id= "divLMDutyGetDetail" style="display: none; position:absolute; slategray">
      <table  class= common BORDER=1 CELLPADDING=2 CELLSPACING=0>
        <TR  class= common>
          <TD  class= title>
            序号/给付责任类型
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyKind >
          </TD>
          <TD  class= title>
            给付类型
          </TD>
          <TD  class= input>
            <Input class= common name=Type >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            默认值
          </TD>
          <TD  class= input>
            <Input class= common name=DefaultVal >
          </TD>
          <TD  class= title>
            算法
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            反算算法
          </TD>
          <TD  class= input>
            <Input class= common name=CnterCalCode >
          </TD>
          <TD  class= title>
            其他算法
          </TD>
          <TD  class= input>
            <Input class= common name=OthCalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            录入标记
          </TD>
          <TD  class= input>
            <Input class= common name=InpFlag >
          </TD>
          <TD  class= title>
            统计类别
          </TD>
          <TD  class= input>
            <Input class= common name=StatType >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起付限
          </TD>
          <TD  class= input>
            <Input class= common name=MinGet >
          </TD>
          <TD  class= title>
            给付后动作
          </TD>
          <TD  class= input>
            <Input class= common name=AfterGet >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            赔付限额
          </TD>
          <TD  class= input>
            <Input class= common name=MaxGet >
          </TD>
          <TD  class= title>
            赔付比例
          </TD>
          <TD  class= input>
            <Input class= common name=ClaimRate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            赔付天数限额
          </TD>
          <TD  class= input>
            <Input class= common name=ClmDayLmt >
          </TD>
          <TD  class= title>
            累计赔付天数限额
          </TD>
          <TD  class= input>
            <Input class= common name=SumClmDayLmt >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            免赔额
          </TD>
          <TD  class= input>
            <Input class= common name=Deductible >
          </TD>
          <TD  class= title>
            免赔天数
          </TD>
          <TD  class= input>
            <Input class= common name=DeDuctDay >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            观察期
          </TD>
          <TD  class= input>
            <Input class= common name=ObsPeriod >
          </TD>
          <TD  class= title>
            被保人死亡后有效标记
          </TD>
          <TD  class= input>
            <Input class= common name=DeadValiFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            死亡给付与现值关系
          </TD>
          <TD  class= input>
            <Input class= common name=DeadToPValueFlag >
          </TD>
        </TR>
       <tr ><td align=right><input type="button" value="确认修改" onclick="hideMe();">
       </td></tr>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
