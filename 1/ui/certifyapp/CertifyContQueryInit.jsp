<%@page import="com.sinosoft.utility.*"%>
<%
    GlobalInput globalInput = new GlobalInput();
    globalInput = (GlobalInput)session.getValue("GI");
%>

<script language="JavaScript">

var comCode = "<%=globalInput.ManageCom%>";

// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        fm.all('manageCom').value = "<%=globalInput.ManageCom%>";
        if(fm.all('manageCom').value != null)
        {
            var arrResult = easyExecSql("select Name from LDCom where ComCode = '" 
                + fm.all('manageCom').value + "'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('manageComName').value=arrResult[0][0];
            }
        }
        //签单时间段默认为三个月
        var arrResult = easyExecSql("select Current Date - 3 MONTH + 1 DAY, Current Date from dual");
        if (arrResult != null) {
            fm.all('startSignDate').value=arrResult[0][0];
            fm.all('endSignDate').value=arrResult[0][1];
        }
    }
    catch(ex)
    {
        alert("CertifyContQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initCertifyContQueryGrid();
    }
    catch(re)
    {
        alert("CertifyContQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var CertifyContQueryGrid;

// 保单信息列表的初始化
function initCertifyContQueryGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="机构编码";
        iArray[1][1]="60px";
        iArray[1][2]=60;
        iArray[1][3]=0;
        iArray[1][21]="ManageCom";

        iArray[2]=new Array();
        iArray[2][0]="机构代码";
        iArray[2][1]="80px";
        iArray[2][2]=80;
        iArray[2][3]=0;
        iArray[2][21]="ManageName";

        iArray[3]=new Array();
        iArray[3][0]="单证类型";
        iArray[3][1]="75px";
        iArray[3][2]=80;
        iArray[3][3]=0;
        iArray[3][21]="CertifyCode";

        iArray[4]=new Array();
        iArray[4][0]="单证名称";
        iArray[4][1]="65px";
        iArray[4][2]=65;
        iArray[4][3]=0;
        iArray[4][21]="CertifyName";

        iArray[5]=new Array();
        iArray[5][0]="承保件数";
        iArray[5][1]="75px";
        iArray[5][2]=75;
        iArray[5][3]=0;
        iArray[5][21]="Count";

        iArray[6]=new Array();
        iArray[6][0]="承保人数";
        iArray[6][1]="80px";
        iArray[6][2]=80;
        iArray[6][3]=0;
        iArray[6][21]="SumInsured";

        iArray[7]=new Array();
        iArray[7][0]="承保保费";
        iArray[7][1]="60px";
        iArray[7][2]=60;
        iArray[7][3]=0;
        iArray[7][21]="SumPrem";

        CertifyContQueryGrid = new MulLineEnter("fm", "CertifyContQueryGrid");
        //这些属性必须在loadMulLine前
        CertifyContQueryGrid.mulLineCount = 0;
        CertifyContQueryGrid.displayTitle = 1;
        CertifyContQueryGrid.hiddenPlus = 1;
        CertifyContQueryGrid.hiddenSubtraction = 1;
        CertifyContQueryGrid.canSel = 0;
        CertifyContQueryGrid.canChk = 0;
        CertifyContQueryGrid.loadMulLine(iArray);
        CertifyContQueryGrid.selBoxEventFuncName = "CertifyContQueryDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>