<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="CertifyListImportInput.js"></script>
    <%@include file="CertifyListImportInit.jsp" %>
</head>

<body onload="initForm();">
    <form action="" method="post" name="fmImport" target="fraSubmit" enctype="multipart/form-data">

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">卡折清单导入</td>
            </tr>
        </table>
        
        <div id="divBatchApply" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">批次号</td>
                    <td class="input">
                        <input class="readonly" name="BatchNo" readonly="readonly" />
                    </td>
                    <td style="display:'none'">
                        <input class="cssButton" type="button" id="btnBatchNoApply" name="btnBatchNoApply" value="申  请" onclick="applyNewBatchNo();" />
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <hr />
        
        <div id="divFileImport" name="divFileImport" style="display:''">
            <span><font color="red">导入文件名必须与批次号一致。</font></span>
            <table class="common">
                <tr class="common">
                    <td class="title">清单文件</td>
                    <td class="common">
                        <input class="cssfile" name="FileName" type="file" />
                    </td>
                </tr>
            </table>
            <input class="cssButton" type="button" id="btnImport" name="btnImport" value="清单导入" onclick="importCertifyList();" />
        </div>
            
        <hr />
    </form>

    <br />
  
	<form action="" method="post" name="fm" target="fraSubmit">        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportErrCondCode);" />
                </td>
                <td class="titleImg">批次导入错误日志查询</td>
            </tr>
        </table>
        
        <div id="divImportErrCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">导入批次号</td>
                    <td class="input">
                        <input class="common" name="ImportErrBatchNo" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>

            <table>
                <tr>
                    <td class="common">
                        <input class="cssButton" type="button" value="查  询" onclick="queryImportErrLog();" />
                    </td>
                </tr>
            </table>
            
            <!-- Mulline Code -->
            <table>
                <tr>
                    <td class="common">
                        <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportErrLogGrid);" />
                    </td>
                    <td class="titleImg">导入批次错误日志</td>
                </tr>
            </table>
            <div align="center" id="divImportErrLogGrid" style="display: ''">
                <table class="common">
                    <tr class="common">
                        <td>
                            <span id="spanImportErrLogGrid"></span> 
                        </td>
                    </tr>
                </table>
                
                <div id="divImportErrLogGridPage" style="display: ''" align="center">
                    <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                    <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                    
                    <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                    <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                   
                </div>
            </div>
            
            
            <!-- Mulline Code End -->
        </div>
        
        <hr />
        
        <br />
        
        <table style="display:'none'">
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCondCode);" />
                </td>
                <td class="titleImg">卡折导入批次查询</td>
            </tr>
        </table>
        <div id="divCondCode" style="display:'none'">
            <table class="common">
                <tr class="common">
                    <td class="title">导入批次号</td>
                    <td class="input">
                        <input class="readonly" name="ImportBatchNo" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>

        <table style="display:'none'">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryBatchList();" />
                </td>
            </tr>
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportBatchGrid);" />
                </td>
                <td class="titleImg">查询结果列表</td>
            </tr>
        </table>
        <div id="divBatchInfo" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">批次号</td>
                    <td class="input">
                        <input class="common" name="ConfirmBatchNo" readonly="readonly" />
                    </td>
                    <td class="title">导入总件数</td>
                    <td class="input">
                        <input class="common" name="SumImportBatchCount" readonly="readonly" />
                    </td>
                    <td class="title">导入总保费</td>
                    <td class="input">
                        <input class="common" name="SumImportBatchPrem" readonly="readonly" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title">包含总退保件数</td>
                    <td class="input">
                        <input class="common" name="SumImportWTCount" readonly="readonly" />
                    </td>
                    <td class="title">包含总退保保费</td>
                    <td class="input">
                        <input class="common" name="SumImportWTPrem" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div align="center" id="divImportBatchGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanImportBatchGrid"></span> 
                    </td>
                </tr>
            </table>
            <div id="divImportBatchGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" />
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" />
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
            </div>
        </div>        
        
        <div style="display: ''" align="left">
            <input type="button" class="cssButton"  id="btnImportDelete" name="btnImportDelete" value="批次导入删除" onclick="delImport();" disabled="disabled" />
            <input type="button" class="cssButton"  id="btnBatchListChk" name="btnBatchListChk" value="清单信息检查" onclick="viewChkWnd();" />
            <input type="button" class="cssButton"  id="btnImportConfirm" name="btnImportConfirm" value="批次导入确认" onclick="confirmImport();" disabled="disabled" />
        </div>
        
        <hr />
        
        <br />
        
        <input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="返  回" onclick="goBack();" />
        
        <input type="hidden" id="fmOperatorFlag" name="fmOperatorFlag" />
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
