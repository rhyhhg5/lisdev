<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：CertifyImportBatchAllDelSave.jsp
	//程序功能：卡折删除批次号下的所有信息save页面
	//更新记录：更新人    更新日期     更新原因/内容
//2、一页显示30条记录。
//3、增加“批次整体删除”按钮，点此按钮后，弹出提示框，提示“该操作将会将批次号为XXXXXXXXXX下的所有卡折，是否继续？”，选择“是”，删除整个批次下的卡折。选择“否”，则取消操作。
	
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tChk[] = request.getParameterValues("InpCertifyGridChk");
	String gbatchNo[] = request.getParameterValues("CertifyGrid1");
	CertifyImportBatchDelUI tCertifyImportBatchDelUI = new CertifyImportBatchDelUI();
	//定义一个错误全局变量
	String tAllError = "";
/*
判断复选框选中的值，将某一批次下的所有cardno查询出来，并作处理；
*/
	try {
		for (int i = 0; i < tChk.length; i++) {
			if (tChk[i].equals("1") && gbatchNo[i] != null) {
			System.out.println("处理数量："+i );
				String tsql = "select CardNo from LICertify where BatchNo = '"
						+ gbatchNo[i] + "' with ur";			
				SSRS tSSRS = new ExeSQL().execSQL(tsql);
				int a = tSSRS.MaxRow;
				if(a!=0){
				        for (int j = 1; j <= a; j++) {
                     System.out.println("批次下的卡信息："+a+"，当前处理数量："+j );
                     TransferData mTransferData = new TransferData();		
					mTransferData.setNameAndValue("cardNo", tSSRS.GetText(j, 1));
					VData vData = new VData();
					vData.add(tG);
					vData.add(mTransferData);
					tCertifyImportBatchDelUI.submitData(vData, "DELETE");
				if (FlagStr == "") {
				tError = tCertifyImportBatchDelUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 删除成功! ";
					FlagStr = "Success";
				} else {
					tAllError += tError.getFirstError();
					Content = " 删除失败，原因是:" + tAllError;
					FlagStr = "Fail";
				}
			}					
			       }				
				}
			}		

		}
	} catch (Exception ex) {
		Content = "删除失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
%>
<html>
	<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
</html>
