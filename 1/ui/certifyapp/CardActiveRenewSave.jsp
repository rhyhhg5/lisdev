<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-01-19
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%> 
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>


<%
String FlagStr = "Fail";
String Content = "";

String tCardNo = request.getParameter("CardNo");
GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("CardNo", tCardNo);
    
    tVData.add(tTransferData);
    tVData.add(tGI);
    
    CertActSyncUI tCertActSyncUI = new CertActSyncUI();
    if(!tCertActSyncUI.submitData(tVData, "ActSync"))
    {
        System.out.println(tCertActSyncUI.mErrors.getFirstError());
        Content = " 卡激活数据处理失败，原因是: " + tCertActSyncUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 卡激活数据处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 卡激活数据处理失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterImportSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
