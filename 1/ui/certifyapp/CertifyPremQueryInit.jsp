<%@page import="com.sinosoft.utility.*"%>
<%
    GlobalInput globalInput = new GlobalInput();
    globalInput = (GlobalInput)session.getValue("GI");
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        fm.all('manageCom').value = "<%=globalInput.ManageCom%>";
        if(fm.all('manageCom').value != null)
        {
            var arrResult = easyExecSql("select Name from LDCom where ComCode = '" 
                + fm.all('manageCom').value + "'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('manageComName').value=arrResult[0][0];
            }
        }
        //初始化生效日期
        var arr = easyExecSql("select Current Date - 3 month, Current Date from dual");
        if(arr){
            fm.all('startSignDate').value = arr[0][0];
            fm.all('endSignDate').value = arr[0][1];
        }
    }
    catch(ex)
    {
        alert("CertifyPremQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initCertifyPremQueryGrid();
    }
    catch(re)
    {
        alert("CertifyPremQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var CertifyPremQueryGrid;

// 保单信息列表的初始化
function initCertifyPremQueryGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";		//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;		//列最大值
        iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="管理机构";
        iArray[1][1]="50px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="manageCom";

        iArray[2]=new Array();
        iArray[2][0]="结算单号";
        iArray[2][1]="70px";
        iArray[2][2]=70;
        iArray[2][3]=0;
        iArray[2][21]="PrtNo";

        iArray[3]=new Array();
        iArray[3][0]="单证类型";
        iArray[3][1]="60px";
        iArray[3][2]=60;
        iArray[3][3]=0;
        iArray[3][21]="certifyCode";

        iArray[4]=new Array();
        iArray[4][0]="单证名称";
        iArray[4][1]="70px";
        iArray[4][2]=70;
        iArray[4][3]=0;
        iArray[4][21]="certifyName";

        iArray[5]=new Array();
        iArray[5][0]="业务员";
        iArray[5][1]="75px";
        iArray[5][2]=75;
        iArray[5][3]=0;
        iArray[5][21]="agentCode";

        iArray[6]=new Array();
        iArray[6][0]="中介机构";
        iArray[6][1]="80px";
        iArray[6][2]=80;
        iArray[6][3]=0;
        iArray[6][21]="agentCom";

        iArray[7]=new Array();
        iArray[7][0]="导入日期";
        iArray[7][1]="70px";
        iArray[7][2]=70;
        iArray[7][3]=0;
        iArray[7][21]="importDate";

        iArray[8]=new Array();
        iArray[8][0]="签单日期";
        iArray[8][1]="70px";
        iArray[8][2]=70;
        iArray[8][3]=0;
        iArray[8][21]="signDate";

        iArray[9]=new Array();
        iArray[9][0]="生效日期";
        iArray[9][1]="70px";
        iArray[9][2]=70;
        iArray[9][3]=0;
        iArray[9][21]="CValiDate";

        iArray[10]=new Array();
        iArray[10][0]="保费";
        iArray[10][1]="40px";
        iArray[10][2]=40;
        iArray[10][3]=0;
        iArray[10][21]="sumPrem";

        CertifyPremQueryGrid = new MulLineEnter("fm", "CertifyPremQueryGrid");
        //这些属性必须在loadMulLine前
        CertifyPremQueryGrid.mulLineCount = 0;
        CertifyPremQueryGrid.displayTitle = 1;
        CertifyPremQueryGrid.hiddenPlus = 1;
        CertifyPremQueryGrid.hiddenSubtraction = 1;
        CertifyPremQueryGrid.canSel = 0;
        CertifyPremQueryGrid.canChk = 0;
        CertifyPremQueryGrid.loadMulLine(iArray);
        CertifyPremQueryGrid.selBoxEventFuncName = "CertifyPremQueryDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>