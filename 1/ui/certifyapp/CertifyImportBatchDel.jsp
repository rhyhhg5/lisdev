<%@ page contentType="text/html;charset=GBK"%>
<%@ include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：
	//程序功能：
	//创建日期：2008-8-14
	//创建人  ：GUZG	
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
	<head>
		<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
		<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
		<script src="../common/javascript/Common.js"></script>
		<script src="../common/cvar/CCodeOperate.js"></script>
		<script src="../common/javascript/MulLine.js"></script>
		<script src="../common/Calendar/Calendar.js"></script>
		<script src="../common/javascript/VerifyInput.js"></script>

		<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

		<script src="CertifyImportBatchDel.js"></script>
		<%@include file="CertifyImportBatchDelInit.jsp"%>
	</head>

	<body onload="initForm();">
		<form action="CertifyImportBatchDelSave.jsp" method="post" name="fm" target="fraSubmit">
			<div id="divCondCode" style="display:''">
				<table class="common">
					<tr class="common">
						<td class="title">
							批次号
						</td>
						<td class="input">
							<input class="common" name="BatchNo" />
						</td>
						<td class="title">
							管理机构
						</td>
						<td class="input">
							<input class="codeNo" name="ManageCom"
								verify="管理机构|code:comcode&notnull"
								ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" />
						</td>
						<td class="title">
							单证号
						</td>
						<td class="input">
							<input class="common" name="CardNo" />
						</td>
						<td class="title">
							录入日期
						</td>
						<td class="input">
							<input class="coolDatePicker" dateFormat="short" name="Date" verify="生成日期|date" />
						</td>
					</tr>
				</table>
			</div>

			<table>
				<tr>
					<td class="common">
						<input class="cssButton" type="button" value="查  询" onclick="queryCertify();" />
					</td>
				</tr>
			</table>

			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divResultGrid);" />
					</td>
					<td class="titleImg">
						查询结果列表
					</td>
				</tr>
			</table>
			<div id="divMissionGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyGrid"></span>
						</td>
					</tr>
				</table>
			</div>

			<div id="divCertifyPage" style="display: ''" align="center">
				<input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />    
			</div>

			<table>
				<tr>
					<td class="common">
						<input class="cssButton" type="button" id="btnDelMission" value="删  除" onclick="delImportBatch();" />
					</td>
					<td class="common" >
					   <input class="cssButton" type="button"  id="btnDelMisson2" value="批次整体删除"  onclick="delAllImportBatch();"/>
					</td>
				</tr>
			</table>
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
