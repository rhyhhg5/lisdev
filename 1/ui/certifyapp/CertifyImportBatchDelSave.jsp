<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：CertifyImportBatchDelSave.jsp
	//程序功能：卡折删除save页面
	//创建日期：2009-6-8
	//创建人  ：guzg
//更新记录:  于明明   更新日期  2015-1-14  更新原因/内容 1、单选框改为复选框。
//2、一页显示30条记录。
//3、增加“批次整体删除”按钮，点此按钮后，弹出提示框，提示“该操作将会将批次号为XXXXXXXXXX下的所有卡折，是否继续？”，选择“是”，删除整个批次下的卡折。选择“否”，则取消操作。

%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
    String tChk[] = request.getParameterValues("InpCertifyGridChk");
    String gbatchNo[]=request.getParameterValues("CertifyGrid1");
    String gcardNo[]=request.getParameterValues("CertifyGrid3");
    CertifyImportBatchDelUI  tCertifyImportBatchDelUI = new CertifyImportBatchDelUI();
     //定义一个错误全局变量
     String tAllError="";
        
        //判断复选框选中的zhi
     try{ 
           for (int i=0; i<tChk.length;i++){
                  if(tChk[i].equals("1") && gcardNo[i]!=null){
                  TransferData mTransferData = new TransferData();
                    mTransferData.setNameAndValue("batchNo",gbatchNo[i]);
                   mTransferData.setNameAndValue("cardNo",gcardNo[i]);
                    VData vData = new VData();
                     vData.add(tG);
                     vData.add(mTransferData);
                     tCertifyImportBatchDelUI.submitData(vData,"DELETE");
                   }    
                 }      
             }catch(Exception ex){
        Content = "删除失败，原因是:" + ex.toString();
	 	FlagStr = "Fail";
       }                     
           if (FlagStr == "") {
		     tError = tCertifyImportBatchDelUI.mErrors;   
				if (!tError.needDealError()) {
					Content = " 删除成功! ";
					FlagStr = "Success";
				} else {
				    tAllError+=tError.getFirstError();
					Content = " 删除失败，原因是:" +tError.getFirstError() ;
                     FlagStr = "Fail";
				          }
		        }

	//获得单证主表信息
	/*String batchNo = request.getParameter("BatchNo");
	String cardNo = request.getParameter("CardNo");
	System.out.println("BatchNo : " + batchNo + "  BussNo : " + cardNo);

	CertifyImportBatchDelUI tCertifyImportBatchDelUI = new CertifyImportBatchDelUI();
	try {
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("cardNo", cardNo);
		mTransferData.setNameAndValue("batchNo", batchNo);

		VData vData = new VData();
		vData.add(tG);
		vData.add(mTransferData);

		tCertifyImportBatchDelUI.submitData(vData, "DELETE");
	} catch (Exception ex) {
		Content = "删除失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	System.out.println("after submit!");

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "") {
		tError = tCertifyImportBatchDelUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 删除成功! ";
			FlagStr = "Success";
		} else {
			Content = " 删除失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}*/
%>
<html>
	<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
</html>
