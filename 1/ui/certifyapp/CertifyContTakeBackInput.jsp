<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
    var branch = "";//更加销售渠道查询业务员时用到
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="CertifyContTakeBackInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyContTakeBackInit.jsp"%>
<title>单证核销</title>
</head>
<body onload="initForm();initElementtype();" >
	<form action="./CertifyContTakeBackSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr>
				<td class= titleImg align= center>请输入单证查询条件：</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>管理机构</TD>
				<TD  class= input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true > </TD>
				<TD class= title>单证编码</TD>
				<td class= input><input NAME=CertifyTakeBackCode CLASS=codeNo ondblclick="return showCodeList('certifytakebackcode',[this,CertifyTakeBackName],[0,1],null,'1 and code not in (#06#,#03#)','1',1);" onkeyup="return showCodeListKey('certifytakebackcode',[this,CertifyTakeBackName],[0,1],null,'1 and code not in (#06#,#03#)','1',1);" verify="单证编码|code:certifytakebackcode&notnull"><input class=codename name=CertifyTakeBackName readonly=true elementtype=nacessary>  </td>
				<td></td>
				<td></td>
			</TR>
			<tr class="common">
		        <td class="title">起始号</td>
		        <td class="input"><input class="common" name="StartNo" elementtype=nacessary></td>
		        <td class="title">终止号</td>
		        <td class="input"><input class="common" name="EndNo" elementtype=nacessary></td>
		        <td></td>
				<td></td>
				<td></td>
        	</tr>
		</table>
		<INPUT VALUE="查询单证" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<INPUT  type="hidden" class=Common name=querySql >
	</form>
	<form action="./CertifyContTakeBackSave.jsp" method=post name=fmSave target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>已发放单证信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<TR class= common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>

		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmOperator" name="fmOperator">
		<table>
			<tr>
				<td>
					<INPUT VALUE="保  存" name=savebtn class="cssButton" TYPE=button onclick="savepol();">
				</td>
				<td><INPUT VALUE="修  改" name=updatebtn class="cssButton" TYPE=button onclick="updatepol();"></td>
			</tr>
		</table>
	</form>
<span id="spanCode"style="display: none; position:absolute; slategray"></span>
</body>
</html>
