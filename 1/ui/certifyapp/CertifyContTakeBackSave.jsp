<%
//程序名称：CertifyTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certifybusiness.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;
  String szFailSet = "";
  String mCertifyCode = "";

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  //mCertifyCode= request.getParameter("CertifyTakeBackCode");
  //System.out.println(request.getParameter("CertifyTakeBackCode"));

  //校验处理
  //内容待填充
	try {
	  // 单证信息部分
	  String szNo[]= request.getParameterValues("PolGridNo");
	  String tRadio[] = request.getParameterValues("InpPolGridChk");	//Radio选项
	  String mCertifyCodes[]= request.getParameterValues("PolGrid1");
	  String mCardNo[]= request.getParameterValues("PolGrid2");
	  String mGrpContNo[]= request.getParameterValues("PolGrid4");
	  String mStateFlag[]= request.getParameterValues("PolGrid5");
	  
	  String tOperator = request.getParameter("fmtransact");
	  
	  if( szNo == null ) {
	  	throw new Exception("没有输入要回收的起始单证号和终止单证号");
	  }
	  
	  LCCertifyTakeBackSet tLCCertifyTakeBackSet = new LCCertifyTakeBackSet();
	  for(int nIndex = 0; nIndex < szNo.length; nIndex ++ ) {
		if("1".equals(tRadio[nIndex])){
			System.out.println("第"+nIndex+"个单证号码"+mCardNo[nIndex]+",对应保单号码"+mGrpContNo[nIndex]);
		    LCCertifyTakeBackSchema tLCCertifyTakeBackSchema = new LCCertifyTakeBackSchema( );
		    tLCCertifyTakeBackSchema.setCardNo(mCardNo[nIndex]);
		    tLCCertifyTakeBackSchema.setContNo(mGrpContNo[nIndex]);
		    tLCCertifyTakeBackSchema.setState(mStateFlag[nIndex]);
		    tLCCertifyTakeBackSet.add(tLCCertifyTakeBackSchema);
		    mCertifyCode = mCertifyCodes[0];
		}
	  }
	  	TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("CertifyCode", mCertifyCode);
		
	  // 准备传输数据 VData
	  VData vData = new VData();
	
	  vData.addElement(globalInput);
	  vData.addElement(tLCCertifyTakeBackSet);
	  vData.add(mTransferData);
	  
	  // 数据传输
	  CertContTakeBackUI tCertContTakeBackUI = new CertContTakeBackUI();

	  if (!tCertContTakeBackUI.submitData(vData, tOperator)) {
	    Content = " 保存失败，原因是: " + tCertContTakeBackUI.mErrors.getFirstError();
	    FlagStr = "Fail";
	  } else {
	  	Content = " 保存成功 ";
	  	FlagStr = "Succ";
	  }
	} catch(Exception ex) {
		ex.printStackTrace( );
   		Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   		FlagStr = "Fail";
	}
%>
<html>
  <script language="javascript">
  	<%= szFailSet %>
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
<body>
</body>
</html>

