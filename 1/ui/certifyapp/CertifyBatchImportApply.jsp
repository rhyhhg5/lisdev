<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
System.out.println("CertifyBatchImportApply.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

String tBatchNo = null;

GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;

int length = 8;
if (strManageCom.length() < length)
{
    strManageCom = PubFun.RCh(strManageCom, "0", length);
}

try
{
    tBatchNo = PubFun1.CreateMaxNo("LICBATCHNO", strManageCom, null);
    System.out.println("tBatchNo:" + tBatchNo);
}
catch(Exception e)
{
    tBatchNo = null;
    e.printStackTrace();
}


if (tBatchNo == null || tBatchNo.equals(""))
{
    Content = " 批次号申请失败! ";
    FlagStr = "Fail";
}
else
{
    Content = " 批次号申请成功！";
    FlagStr = "Succ";
}

System.out.println("CertifyBatchImportApply.jsp End ...");
%>

<html>
<script language="javascript">
    parent.fraInterface.afterApplyNewBatchNoSubmit("<%=FlagStr%>", "<%=Content%>", "<%=tBatchNo%>");
</script>
</html>
