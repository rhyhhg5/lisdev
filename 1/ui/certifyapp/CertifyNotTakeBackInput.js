
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

//下载
function downLoad() {
	if(fm.querySql.value != null && fm.querySql.value != "" && CertifyGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "CertifyNotTakeBackDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
function queryClick() {
	initCertifyGrid();
	var tManageCom = fm.all('ManageCom').value;
	if(tManageCom == null || tManageCom == "")
	{
		alert("管理机构必须填写！");
		return false;
	}
	
	var tStartDate = fm.all('StartDate').value;
	var tEndDate = fm.all('EndDate').value;
	if(tStartDate == null || tStartDate == "" || tEndDate == null || tEndDate == "")
	{
		alert("起止日期必须填写！");
		return false;
	}
	
	if(dateDiff(tStartDate,tEndDate,"M") > 3)
	{
		alert("起止日期间隔必须为3个月！");
		return false;
	}
	var strSQL = "select "
				+" lmd.managecom,"
				+" lmd.certifycode,"
				+" lmd.certifyname,"
				+" lzc.handledate,"
				+" lzc.startno,"
				+" lzc.endno,"
				+" substr(receivecom,2),"
				+" case lzc.state "
				+" when '8' then substr(receivecom,2)"
				+" when '9' then (select comcode from lduser where usercode = substr(receivecom,2))"
				+" when '10' then (select managecom from laagent where agentcode = substr(receivecom,2)) "
				+" when '11' then (select managecom from lacom where agentcom = substr(receivecom,2)) "
				+" when '12' then (select managecom from laagent where agentcode = substr(receivecom,2) union all select managecom from lacom where agentcom = substr(receivecom,2)) "
				+" when '13' then (select managecom from laagent where agentcode = substr(receivecom,2) union all select managecom from lacom where agentcom = substr(receivecom,2)) "
				+" when '14' then (select managecom from laagent where agentcode = substr(receivecom,2) union all select managecom from lacom where agentcom = substr(receivecom,2)) "
				+" end,"
				+" (select codename from ldcode where codetype = 'lzcardstate' and code = lzc.state )"
				+" from lmcertifydes lmd"
				+" inner join lzcard lzc on lzc.certifycode = lmd.certifycode"
				+" where lmd.certifyclass = 'D'"
				+" and lzc.state in ('8','9','10','11','12','13','14') "
				+ getWherePart('lmd.managecom', 'ManageCom','like')
		        + getWherePart('lzc.handledate', 'StartDate','>=')
		        + getWherePart('lzc.handledate', 'EndDate','<=')
		        + getWherePart('lmd.CertifyCode', 'CertifyCode')
		        + getWherePart('lmd.CertifyName', 'CertifyName')
		        + "union all "
		        +" select "
				+" lmd.managecom,"
				+" lmd.certifycode,"
				+" lmd.certifyname,"
				+" lzc.handledate,"
				+" lzc.startno,"
				+" lzc.endno,"
				+" substr(receivecom,2),"
				+" case lzc.stateflag "
				+" when '7' then (select comcode from lduser where usercode = substr(receivecom,2))"
				+" when '8' then (select managecom from laagent where agentcode = substr(receivecom,2) union all select managecom from lacom where agentcom = substr(receivecom,2)) "
				+" end,"
				+" (select codename from ldcode where codetype = 'lzcardstateflag' and code = lzc.stateflag )"
				+" from lmcertifydes lmd"
				+" inner join lzcard lzc on lzc.certifycode = lmd.certifycode"
				+" where lmd.certifyclass = 'P'"
				+" and lzc.stateflag in ('7','8') "
				+ getWherePart('lmd.managecom', 'ManageCom','like')
		        + getWherePart('lzc.handledate', 'StartDate','>=')
		        + getWherePart('lzc.handledate', 'EndDate','<=')
		        + getWherePart('lmd.CertifyCode', 'CertifyCode')
		        + getWherePart('lmd.CertifyName', 'CertifyName')
		        ;
	
	turnPage1.queryModal(strSQL, CertifyGrid);
	showCodeName();	//显示编码代表的汉字
	fm.querySql.value = strSQL;
}

