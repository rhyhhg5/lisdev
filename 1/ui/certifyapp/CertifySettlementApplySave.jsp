<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-17
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
System.out.println("CertifySettlementApplySave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
GlobalInput tG = (GlobalInput)session.getValue("GI");

String tActivityID = request.getParameter("ActivityID");
String mPrtNo = null;
String chk = "";

try
{
    mPrtNo = PubFun1.CreateMaxNo("LICPRTNO", null);
    System.out.println("PrtNo: " + mPrtNo);
    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("PrtNo", mPrtNo);
    tTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
    tTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
    tTransferData.setNameAndValue("Operator", tG.Operator);
    
    tVData.add(tTransferData);
    tVData.add(tG);
    
    GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
    if(!tGrpTbWorkFlowUI.submitData(tVData, tActivityID))
    {
        Content = " 结算单申请失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 结算单申请成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 结算单申请失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("CertifySettlementApplySave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.fm.PrtNo.value = "<%=mPrtNo%>";
    parent.fraInterface.afterApplyCertifySettlementSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
