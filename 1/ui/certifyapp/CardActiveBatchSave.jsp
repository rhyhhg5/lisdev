<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-01-19
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>


<%
System.out.println("CardActiveBatchSave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

String tActiveDate = request.getParameter("ActiveDate");
String tAutoDealFlag = request.getParameter("AutoDealFlag");

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{
    if(tAutoDealFlag != null && tAutoDealFlag.equals("1"))
    {
        if(tActiveDate == null)
        {
            Calendar tCurDate = Calendar.getInstance();
            tCurDate.add(Calendar.DATE, -1);
            
            tActiveDate = new FDate().getString(tCurDate.getTime());
        }
    }
    else
    {
        if(tGI == null)
        {
            return;
        }
    }
    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ActiveDate", tActiveDate);
    
    tVData.add(tTransferData);
    tVData.add(tGI);
    
    CertActBatchImportBL tCertActBatchImportBL = new CertActBatchImportBL();
    if(!tCertActBatchImportBL.submitData(tVData, "ActSyncBatch"))
    {
        Content = " 卡激活数据处理失败，原因是: " + tCertActBatchImportBL.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 卡激活数据处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 卡激活数据处理失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("CardActiveBatchSave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterImportSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
