<%@page import="com.sinosoft.utility.*"%>
<%
    GlobalInput globalInput = new GlobalInput();
    globalInput = (GlobalInput)session.getValue("GI");
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        fm.all('manageCom').value = "<%=globalInput.ManageCom%>";
        if(fm.all('manageCom').value != null)
        {
            var arrResult = easyExecSql("select Name from LDCom where ComCode = '" 
                + fm.all('manageCom').value + "'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('manageComName').value=arrResult[0][0];
            }
        }
    }
    catch(ex)
    {
        alert("CertifyQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initCertifyQueryGrid();
    }
    catch(re)
    {
        alert("CertifyQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var CertifyQueryGrid;

// 保单信息列表的初始化
function initCertifyQueryGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="导入批次号";
        iArray[1][1]="60px";
        iArray[1][2]=60;
        iArray[1][3]=0;
        iArray[1][21]="BatchNo";

        iArray[2]=new Array();
        iArray[2][0]="管理机构";
        iArray[2][1]="60px";
        iArray[2][2]=60;
        iArray[2][3]=0;
        iArray[2][21]="manageCom";

        iArray[3]=new Array();
        iArray[3][0]="结算单号";
        iArray[3][1]="80px";
        iArray[3][2]=80;
        iArray[3][3]=0;
        iArray[3][21]="payNo";

        iArray[4]=new Array();
        iArray[4][0]="单证流水号";
        iArray[4][1]="75px";
        iArray[4][2]=80;
        iArray[4][3]=0;
        iArray[4][21]="cardNo";

        iArray[5]=new Array();
        iArray[5][0]="单证状态";
        iArray[5][1]="65px";
        iArray[5][2]=65;
        iArray[5][3]=0;
        iArray[5][21]="state";

        iArray[6]=new Array();
        iArray[6][0]="业务员";
        iArray[6][1]="75px";
        iArray[6][2]=75;
        iArray[6][3]=0;
        iArray[6][21]="agentCode";

        iArray[7]=new Array();
        iArray[7][0]="中介机构";
        iArray[7][1]="80px";
        iArray[7][2]=80;
        iArray[7][3]=0;
        iArray[7][21]="agentCom";

        iArray[8]=new Array();
        iArray[8][0]="被保险人";
        iArray[8][1]="60px";
        iArray[8][2]=60;
        iArray[8][3]=0;
        iArray[8][21]="insuredName";
        
        iArray[9]=new Array();
        iArray[9][0]="职业名称";
        iArray[9][1]="60px";
        iArray[9][2]=60;
        iArray[9][3]=0;
        iArray[9][21]="occupationName";
        
        iArray[10]=new Array();
        iArray[10][0]="职业类别";
        iArray[10][1]="60px";
        iArray[10][2]=60;
        iArray[10][3]=0;
        iArray[10][21]="occupationtype";
        
        iArray[11]=new Array();
        iArray[11][0]="职业代码";
        iArray[11][1]="60px";
        iArray[11][2]=60;
        iArray[11][3]=0;
        iArray[11][21]="occupationcode";

        iArray[12]=new Array();
        iArray[12][0]="保费";
        iArray[12][1]="40px";
        iArray[12][2]=40;
        iArray[12][3]=0;
        iArray[12][21]="prem";

        iArray[13]=new Array();
        iArray[13][0]="单证类型";
        iArray[13][1]="60px";
        iArray[13][2]=60;
        iArray[13][3]=0;
        iArray[13][21]="certifyCode";

        iArray[14]=new Array();
        iArray[14][0]="单证名称";
        iArray[14][1]="70px";
        iArray[14][2]=70;
        iArray[14][3]=0;
        iArray[14][21]="certifyName";

        iArray[15]=new Array();
        iArray[15][0]="导入日期";
        iArray[15][1]="70px";
        iArray[15][2]=70;
        iArray[15][3]=0;
        iArray[15][21]="inmpotDate";

        iArray[16]=new Array();
        iArray[16][0]="结算日期";
        iArray[16][1]="70px";
        iArray[16][2]=70;
        iArray[16][3]=0;
        iArray[16][21]="clearDate";

        iArray[17]=new Array();
        iArray[17][0]="签单日期";
        iArray[17][1]="70px";
        iArray[17][2]=70;
        iArray[17][3]=0;
        iArray[17][21]="signDate";
        
        iArray[18]=new Array();
        iArray[18][0]="生效日期";
        iArray[18][1]="70px";
        iArray[18][2]=70;
        iArray[18][3]=0;
        iArray[18][21]="cvalidate";
        
        iArray[19]=new Array();
        iArray[19][0]="保单号";
        iArray[19][1]="70px";
        iArray[19][2]=70;
        iArray[19][3]=0;
        iArray[19][21]="grpcontno";

        CertifyQueryGrid = new MulLineEnter("fm", "CertifyQueryGrid");
        //这些属性必须在loadMulLine前
        CertifyQueryGrid.mulLineCount = 0;
        CertifyQueryGrid.displayTitle = 1;
        CertifyQueryGrid.hiddenPlus = 1;
        CertifyQueryGrid.hiddenSubtraction = 1;
        CertifyQueryGrid.canSel = 0;
        CertifyQueryGrid.canChk = 0;
        CertifyQueryGrid.loadMulLine(iArray);
        CertifyQueryGrid.selBoxEventFuncName = "CertifyQueryDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>