<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('IDNo').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
    
    fm.all('ManageCom').value = <%=strManageCom%>;

  }
  catch(ex)
  {
    alert("在InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initAgentGrid();
    
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initAgentGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="业务员代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="业务员姓名";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="证件号";         //列名
        iArray[3][1]="120px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="销售团队代码";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="销售团队名称";         //列名
        iArray[6][1]="120px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="人员状态";         //列名
        iArray[7][1]="50px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

  
        AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 

        //这些属性必须在loadMulLine前
        AgentGrid.mulLineCount = 0;   
        AgentGrid.displayTitle = 1;
      AgentGrid.hiddenPlus = 1;
      AgentGrid.hiddenSubtraction = 1;
        AgentGrid.canSel=0;
        AgentGrid.canChk=0;
        AgentGrid.locked=1;
        AgentGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentGrid时出错："+ ex);
      }
    }


</script>