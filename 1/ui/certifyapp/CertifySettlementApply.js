//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifySettlementList()
{
    if(!verifyInput2())
    {
        return false;
    }

    var tStrSql = ""
        + " select lwm.MissionProp1, lwm.MissionProp2, lwm.MissionProp3, lwm.MissionProp4, "
        + " lwm.MissionId, lwm.SubMissionId, lwm.ProcessId, lwm.ActivityId, lwm.ActivityStatus "
        + " from LWMission lwm "
        + " where 1 = 1 "
        + " and lwm.ProcessId = '0000000011' "
        + " and lwm.ActivityId = '0000011001' "
        + getWherePart("lwm.MissionProp1", "PrtNo")
        + getWherePart("lwm.MissionProp2", "ApplyDate")
        + getWherePart("lwm.MissionProp3", "ManageCom",'like')
        + " with ur "
        ;
    
    turnPage1.pageDivName = "divCertifySettleListGridPage";
    turnPage1.queryModal(tStrSql, CertifySettleListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有待处理的结算待信息！");
        return false;
    }
    
    return true;
}

/**
 * 进入结算单录入。
 */
function inputCertifySettlement()
{
    var tRow = CertifySettleListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = CertifySettleListGrid.getRowData(tRow);
    
    var tPrtNo = tRowDatas[0];
    var tMissionId = tRowDatas[4];
    var tSubMissionId = tRowDatas[5];
    var tProcessId = tRowDatas[6];
    var tActivityId = tRowDatas[7];
    var tActivityStatus = tRowDatas[8];
    var tManageCom = tRowDatas[2];

    var strSql = "select * from ldsystrace where PolNo='" + tPrtNo + "' and PolState=1009 ";
	var arrResult = easyExecSql(strSql);
	if (arrResult!=null && arrResult[0][1]!=operator) {
	    alert("该印刷号的投保单已经被操作员(" + arrResult[0][1] + ")在(" + arrResult[0][5] + ")位置锁定！您不能操作，请选其它的印刷号！");
	    return;
	}
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + tPrtNo + "&CreatePos=卡折结算&PolState=1009&Action=INSERT";
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
    
    
    var tStrUrl = "./CertifyContSettlement.jsp"
        + "?PrtNo=" + tPrtNo
        + "&MissionId=" + tMissionId
        + "&SubMissionId=" + tSubMissionId
        + "&ProcessId=" + tProcessId
        + "&ActivityId=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        + "&ManageCom=" + tManageCom
        ;

    window.location = tStrUrl;
}

/**
 * 申请单证结算单。
 */
function applyCertifySettlement()
{
    fm.btnApplyCertifySettlement.disabled = true;
    
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./CertifySettlementApplySave.jsp";
    fm.ActivityID.value = "0000011999";
    fm.submit();

    fm.action = "";
}

/**
 * 申请单证结算单提交后动作。
 */
function afterApplyCertifySettlementSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("申请批次失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        queryCertifySettlementList();
    }
    
    //initBatchNoInput(cBatchNo);
    fm.btnApplyCertifySettlement.disabled = false;
}

