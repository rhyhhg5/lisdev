<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-05-04
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="WSCertify.js"></script>
    <%@include file="WSCertifyInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">查询条件</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title8">生效日期起</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="StartCValidate" verify="生效日期起|date" />
                    </td>
                    <td class="title8">生效日期止</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="EndCValidate" verify="生效日期止|date" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title8">保险卡号</td>
                    <td class="input8">
                        <input class="common" name="CardNo" verify="保险卡号|len>=11&len<=12" maxlength="12" />
                    </td>
                    <td class="title8">结算单号</td>
                    <td class="input8">
                        <input class="common" name="PrtNo" verify="结算单号|len=11" maxlength="11" />
                    </td>
                    <td class="title8">&nbsp;</td>
                    <td class="input8">&nbsp;</td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryCertifyList();" /> 	
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCertifyListGrid)" />
                </td>
                <td class="titleImg">卡折清单列表</td>
            </tr>
        </table>
        <div id="divCertifyListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifyListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertifyListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>        
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnWSCertify" value="保险卡实名化" onclick="dealWSCertify();" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnWSCertify" value=" 查看被保人 " onclick="queryCertInsuList();" />   
                </td>
            </tr>
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">被保人列表</td>
            </tr>
        </table>
        <div id="divCertInsuListGrid" style="display: 'none'">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertInsuListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertInsuListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                     
            </div>
        </div>
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
