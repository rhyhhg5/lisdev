//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifyList()
{
    if(!verifyInput2())
    {
        return false;
    }
    
    cleanCertInsuMul();

    var tStrSql = ""
        + " select lict.CardNo, lict.PrtNo, lict.CValidate, lict.ActiveDate, "
        + " licti.Name, licti.IdNo, lict.InsuPeoples, "
        + " CodeName('certifycontstate', lict.State) State, "
        + " CodeName('ccwsstate', lict.WSState) WSState "
        + " from LICertify lict "
        + " left join LICertifyInsured licti on licti.CardNo = lict.CardNo and licti.SequenceNo = '1' "
        + " where 1 = 1 "
        + " and lict.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        + getWherePart("lict.CardNo", "CardNo")
        + getWherePart("lict.PrtNo", "PrtNo")
        ;
    
    turnPage1.pageDivName = "divCertifyListGridPage";
    turnPage1.queryModal(tStrSql, CertifyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有待处理的结算待信息！");
        return false;
    }
    
    return true;
}

/**
 * 卡折实名化
 */
function dealWSCertify()
{
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    //卡折 4-遗失，5-销毁，6-作废 校验
    var tRowDatas = CertifyListGrid.getRowData(tRow);
    var tCardNo = tRowDatas[0];
    
    var tStrSql ="select a.State "
			   +" from lzcard a, lzcardnumber b "
			   +" where a.subcode = b.cardtype "
			   +" and a.startno= b.cardserno " 
			   +" and b.cardno = '" + tCardNo + "' " 
			   ;
    var CardState = easyExecSql(tStrSql);
    if(CardState == '4')
    {
        alert("此卡折状态为遗失，不可进行实名化");
        return false;
    }
    if(CardState == '5')
    {
        alert("此卡折状态为销毁，不可进行实名化");
        return false;
    }
    if(CardState == '6')
    {
        alert("此卡折状态为作废，不可进行实名化");
        return false;
    }
    

    if (confirm("实名化成功后，数据将不能回退，您确实想实名化该记录吗?"))
    {
        showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
        fm.btnWSCertify.disabled = true;
        fm.action = "WSCertifySave.jsp";
        fm.submit();
        fm.action = "";
    }

}

/**
 * 查询被保人信息
 */
function queryCertInsuList()
{
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = CertifyListGrid.getRowData(tRow);
    var tCardNo = tRowDatas[0];
    
    var tStrSql = ""
        + " select licti.CardNo, "
        + " licti.Name, CodeName('sex', licti.Sex), licti.Birthday, CodeName('idtype', licti.IdType), licti.IdNo "
        + " from LICertifyInsured licti "
        + " where 1 = 1 "
        + " and licti.CardNo = '" + tCardNo + "'"
        ;
    
    turnPage2.pageDivName = "divCertInsuListGridPage";
    turnPage2.queryModal(tStrSql, CertInsuListGrid);
    
    if (!turnPage2.strQueryResult)
    {
        alert("该卡未找到被保人相关信息！");
        return false;
    }
    
    fm.all('divCertInsuListGrid').style.display = "";
}

function clkCertInfo()
{
    cleanCertInsuMul();
}

/**
 * 清除被保人信息查询列表
 */
function cleanCertInsuMul()
{
    CertInsuListGrid.clearData();
    fm.all('divCertInsuListGrid').style.display = "none";
}

/**
 * 实名化提交后动作。
 */
function afterWSSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("实名化失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        //fm.BatchNo.value = cBatchNo;
        //queryCertifyImportBatchList();
    }

    fm.btnWSCertify.disabled = false;
}

