<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.cbsws.xml.ctrl.blogic.*"%>

<%
//程序名称：IHMSPayTrade_SJZJY.jsp
//程序功能：
//创建日期：
//创建人  ：
//修改人  ：
System.out.println("IHMPayTrade_SJZJY.jsp start ...");

String FlagStr = "0";
String Content = "";

String strResult = null;


//接受业务类型
// type = 01 - 生成应收
// type = 02 - 查询是否收费成功
// type = 03 - 应收撤销
String type = request.getParameter("type");
System.out.println("type="+type + " -------- 01 - 生成应收;02 - 查询是否收费成功;03 - 应收撤销");

//设置request的接受字符集
String tGrpContNo = request.getParameter("GrpContNo");
System.out.println("tGrpContNo="+tGrpContNo);
String tManageCom = request.getParameter("ManageCom");
String tAppntNo = request.getParameter("AppntNo");
String tMoney = request.getParameter("Money");
String tAgentCode = request.getParameter("AgentCode");
String tOperator = request.getParameter("Operator");

try
{
	VData tVData = new VData();
   	TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
    tTransferData.setNameAndValue("ManageCom", tManageCom);
    tTransferData.setNameAndValue("AppntNo", tAppntNo); 
    tTransferData.setNameAndValue("Money", tMoney);
    tTransferData.setNameAndValue("AgentCode", tAgentCode);
    tTransferData.setNameAndValue("Operator", tOperator);
    tVData.add(tTransferData);
    
    HMSPayTradeGrpBL tBL = new HMSPayTradeGrpBL();
    if(!tBL.submitData(tVData, type))
    {
        FlagStr = "0";
    }
    else
    {
        FlagStr = "1";
    }
    Content = tBL.mErrors.getFirstError();
}
catch(Exception ex)
{
	System.out.println("处理异常");
	Content = "处理异常";
    FlagStr = "0";
}
strResult = FlagStr + "|" + Content;
System.out.println("strResult=" + strResult);
System.out.println("IHMSPayTrade_SJZJY.jsp end ...");

%>
<%=strResult%>

