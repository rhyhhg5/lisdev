<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-16
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<script>
	var ManageCom = "<%=request.getParameter("ManageCom")%>";
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="CertifyContSettlement.js"></script>
    <%@include file="CertifyContSettlementInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
    
        <table>
            <tr>
                <td class="titleImg">结算单基本信息：</td>
            </tr>
        </table>
        
        <div id="divCSLBaseInfo" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">结算单号</td>
                    <td class="input">
                        <input class="readonly" name="PrtNo" verify="结算单号|notnull" elementtype="nacessary" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <br />
    
        <table>
            <tr>
                <td class="titleImg">请输入查询条件：</td>
            </tr>
        </table>
        
        <div id="divCSLCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">管理机构</td>
                    <td class="input">
                        <input class="codeNo" name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">生效日期起</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="StartCValidate" verify="生效日期|date" />
                    </td>
                    <td class="title">生效日期止</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="EndCValidate" verify="生效日期|date" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title">单证类别</td>
                    <td class="input">
                        <input class="codeNo" name="CertifyTypeCode" verify="单证类别|notnull" ondblclick="return showCodeList('certifycoded',[this,CertifyTypeCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('certifycoded',[this,CertifyTypeCodeName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="CertifyTypeCodeName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">单证起始号</td>
                    <td class="input">
                        <input class="common" name="StartCardNo" />
                    </td>
                    <td class="title">单证终止号</td>
                    <td class="input">
                        <input class="common" name="EndCardNo" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title">售出渠道</td>
                    <td class="input">
                        <!--<input class="codeNo" name="SaleChnl" verify="售出渠道|notnull" ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1],null,'1 and code not in (#07#,#16#)','1',1);" onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="SaleChnlName" readonly="readonly" elementtype="nacessary" />-->
                        <Input class=codeNo name=SaleChnl verify="销售渠道|notnull" ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
                    </td>
                    <td class="title">批次号</td>
                    <td class="input">
                        <input class="common" name="BatchNo"  />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td CLASS="title" id="AgentComTitleID" style="display: none">中介机构</td>
						<td CLASS="input" id="AgentComInputID" COLSPAN="1" style="display: none">
						  <input NAME="AgentCom" VALUE CLASS="code" ondblclick="return showCodeList('agentcombank',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('agentcombank',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" verify="代理机构|code:AgentCom"><Input type='hidden' name=AgentComName readonly >
			    	</td>
                    
                    <td CLASS="title" id="AgentComTitleID1" style="display: none">中介机构</td>
						<td CLASS="input" id="AgentComInputID1" COLSPAN="1" style="display: none">
						  <input NAME="AgentCom1" VALUE CLASS="code" ondblclick="return showCodeList('agentcominput',[this,AgentComName1,AgentCom,AgentComName],[0,1,0,1],null, fm.all('AgentVar').value,'branchtype',null);" onkeyup="return showCodeListKey('agentcominput',[this,AgentComName1,AgentCom,AgentComName],[0,1,0,1],null,fm.all('AgentVar').value,'branchtype',null);" ><Input type='hidden' name=AgentComName1 readonly >
			    	  <input name="AgentVar" class="common" type="hidden" readonly>
			    	</td>
                    
                    <td class="title">业务员代码</td>
                    <td class="input">
                        <input class="code" name="AgentCode"   ondblclick="return queryAgent();" type="hidden" >
                        <input class="code" name="GroupAgentCode" verify="业务员代码|notnull" elementtype="nacessary" ondblclick="return queryAgent();" >
                    </td>
                    <td class="title">业务员姓名</td>
                    <td class="input">
                        <input class="common" name="AgentName" readonly="readonly" />
                    </td>
                </tr>
                <tr>
            		<td colspan="6"><font color="black">如果是交叉销售,请选择</font><INPUT TYPE="checkbox" NAME="MixComFlag" onclick="isMixCom();"></td>
        		</tr>
        		<%@include file="../sys/MixedSalesAgent.jsp"%>
        		<!--
        		<tr class="common8" id="GrpAgentComID" style="display: none">
		            <td class="title8">交叉销售渠道</td>
		            <td class="input8">
		                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype="nacessary"/>
		            </td>
		            <td class="title8">交叉销售业务类型</td>
		            <td class="input8">
		                <input class="codeNo" name="Crs_BussType" id="Crs_BussType" verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" elementtype="nacessary"/>
		            </td>
		            <td class="title8">&nbsp;</td>
		            <td class="input8">&nbsp;</td>
        		</tr>	
                <tr class=common id="GrpAgentTitleID" style="display: 'none'">
		    	    <td CLASS="title" >对方机构代码</td>
					<td CLASS="input" COLSPAN="1" >
		    	      <Input class="code" name="GrpAgentCom" elementtype=nacessary  ondblclick="return queryGrpAgentCom();">
		    	    </td>
		    	    <td  class= title>对方机构名称</td>
			        <td  class= input>
			          <Input class="common" name="GrpAgentComName" TABINDEX="-1" readonly >
			        </td>  
					<td CLASS="title">对方业务员代码</td>
					 
					<td CLASS="input" COLSPAN="1">
					  	<input NAME="GrpAgentCode" CLASS="common" elementtype="nacessary">
		    	  	</td>
		    	  	 
		    	  	<td CLASS="input" COLSPAN="1">
			        <input NAME="GrpAgentCode" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return queryGrpAgent();" >
    		        </td>
	        	</tr>
	        	<tr class=common id="GrpAgentTitleIDNo" style="display: 'none'">
	        	   <td  class="title" >对方业务员姓名</td>
	          		<td  class="input" COLSPAN="1">
	            		<Input  name=GrpAgentName CLASS="common">
	          		</td>
			        <td CLASS="title">对方业务员身份证</td>
			         
			     	<td CLASS="input" COLSPAN="1">
							<input NAME="GrpAgentIDNo" CLASS="common" >
			        </td>
			        
			        <td CLASS="input" COLSPAN="1">
			        <input NAME="GrpAgentIDNo" VALUE MAXLENGTH="18" CLASS="code" elementtype=nacessary ondblclick="return queryGrpAgent();" >
    	            </td>
            	</tr>
            	 -->
            </table>
        </div>
        <table class="common">
        	 <tr>
            <td colspan="6"><font color="black">综合开拓标示</font><INPUT TYPE="checkbox" NAME="ExtendFlag" onclick="isExtend();"></td>
        </tr>
        <tr class="common8" id="ExtendID" style="display: none">
            <td class="title8">协助销售渠道</td>
            <td class="input8">
                <input class="codeNo" name="AssistSaleChnl" id="AssistSaleChnl" verify="协助销售渠道|code:AssistSaleChnl" ondblclick="return showCodeList('AssistSaleChnl',[this,AssistSalechnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AssistSaleChnl',[this,AssistSalechnlName],[0,1],null,null,null,1);" /><input class="codename" name="AssistSalechnlName" readonly="readonly" elementtype=nacessary/>
            </td>
            <td CLASS="title">协助销售人员代码</td>
    		<td CLASS="input" COLSPAN="1">
			<input NAME="AssistAgentCode" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return queryAssistAgent();" >
    		</td>
            <td  class="title" >协助销售人员姓名</td>
	        <td  class="input" COLSPAN="1">
	            <Input  name=AssistAgentName CLASS="common" elementtype=nacessary readonly>
	        </td>
        </tr>
        </table>
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryAvailableCertifyContList();" /> 	
                </td>
            </tr>
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divAvailableCertifyListGrid);" />
                </td>
                <td class="titleImg">未结算保单信息</td>
            </tr>
        </table>
        <div id="divAvailableCertifyListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanAvailableCertifyListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divAvailableCertifyListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                    
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                   
            </div>
        </div>
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnAddCertifyCont" name="btnAddCertifyCont" value="加入结算单" onclick="addCertifyCont();" disabled="disabled" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnAddAllCertifyCont" name="btnAddAllCertifyCont" value="全部加入结算单" onclick="addAllCertifyCont();" disabled="disabled" />   
                </td>
            </tr>
        </table>
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divSettlementCertifyContListGrid);" />
                </td>
                <td class="titleImg">当前结算单下单证清单。</td>
            </tr>
        </table>
        <div id="divSettlementCertifyContListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanSettlementCertifyContListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divSettlementCertifyContListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                    
                <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                   
            </div>
        </div>
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnDelCertifyCont" name="btnDelCertifyCont" value="从结算单中取消" onclick="delCertifyCont();" disabled="disabled" />   
                </td>
            </tr>
        </table>
        
        <br />
        
        <hr />
        
        <div id="divCSLInfo" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">结算单单证总数量</td>
                    <td class="input">
                        <input class="common" name="SumCertifyContCount" readonly="readonly" />
                    </td>
                    <td class="title">结算单总保费</td>
                    <td class="input">
                        <input class="common" name="SumCertifyContPrem" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td class="title">内含承保单证总数量</td>
                    <td class="input">
                        <input class="common" name="SumCBCertifyContCount" readonly="readonly" />
                    </td>
                    <td class="title">内含承保总保费</td>
                    <td class="input">
                        <input class="common" name="SumCBCertifyContPrem" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td class="title">内含退保单证总数量</td>
                    <td class="input">
                        <input class="common" name="SumWTCertifyContCount" readonly="readonly" />
                    </td>
                    <td class="title">内含退保总保费</td>
                    <td class="input">
                        <input class="common" name="SumWTCertifyContPrem" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <hr />
        
        <div id="divCSLPayModeInfo" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">付费方式</td>
                    <td class="input">
                        <input class="codeNo" name="PayMode" CodeData="0|^1|现金^3|转账支票^11|银行汇款" verify="缴费方式|notnull" value="1" ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="PayModeName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">缴费频次</td>
                    <td class="input">
                        <Input class="codeNo" name="PayIntv" CodeData="0|^0|趸缴" verify="缴费频次|notnull" value="0" ondblclick="return showCodeListEx('PayIntv',[this,PayIntvName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('PayIntv',[this,PayIntvName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="PayIntvName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <hr />
        
        <div id="divCSLCValidateInfo" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">团单生效日期</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="CValidate" verify="" elementtype="nacessary" />
                    </td>
                    <td class="title">团单终止日期</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="CInValidate" verify="" elementtype="nacessary" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <hr />
        
        <div id="divCSLAppntGrpInfo" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">结算单位</td>
                    <td class="input">
                        <input class="common" type="text" name="AppntName" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
       <tr>
            <td colspan="6"><font color="black"> 如果是共保保单,请选择</font><INPUT TYPE="checkbox" NAME="CoInsuranceFlag" onclick="isCoInsurance();"></td>
       </tr>
        
        <div id="DivLCContButton" style="display:'none'">
	        <table>
	            <tr>
	                <td>
	                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
	                </td>
	                <td class="titleImg">共保信息要素<font color="red">（共保负担比例之和不能大于1）</font></td>
	            </tr>
	        </table>
    	</div>
    
	    <div id="divCoInsuranceParam" style="display: 'none'">
	        <table class="common">
	            <tr class="common">
	                <td>
	                    <span id="spanCoInsuranceParamGrid"></span> 
	                </td>
	            </tr>
	        </table>
	    </div>

        <hr />
        
        <div id="DivISTODS" style="display:'none'">
	        <table class="common">
	            <tr class="common">
	                <td>
	                   <font color="black"> <INPUT TYPE="checkbox" NAME="ISTODS">转为直销业务</font>
	                </td>
	                <td></td>
	                <td>
            			业务员代码
          			</td>
          			<td>
      					<Input NAME=ZXAgentCode VALUE="" MAXLENGTH=0 CLASS=code8  ondblclick="return queryZXAgent();"onkeyup="return queryAgent2();" type = "hidden">
      					<Input NAME=ZXGroupAgentCode VALUE="" MAXLENGTH=0 CLASS=code8  ondblclick="return queryZXAgent();" readonly>
         			</td>
          			<td>
            			业务员名称
          			</td>
          			<td>
      					<Input NAME=ZXAgentName VALUE=""  readonly CLASS=common >
         			</td>
	            </tr>
	        </table>
	         <hr />
    	</div>
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnCreateGrpCont" name="btnCreateGrpCont" value="清单结算" onclick="createGrpCont();" disabled="disabled" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value=" 返  回 " onclick="goBack();" />   
                </td>
            </tr>
        </table>
        
        <input type="hidden" id="fmOperatorFlag" name="fmOperatorFlag" />
        
        <input type="hidden" id="querySql" name="querySql" />
        
        <input type="hidden" id="GrpContAgentCom" name="GrpContAgentCom" />
        <input type="hidden" id="GrpContAgentCode" name="GrpContAgentCode" />
        <input type="hidden" id="GrpContSaleChnl" name="GrpContSaleChnl" />
        <input type="hidden" id="GrpContManageCom" name="GrpContManageCom" />
        <input type="hidden" id="CertifyCode" name="CertifyCode" />
        <input type="hidden" id="Crs_SaleChnl_KZ" name="Crs_SaleChnl_KZ" />
        <input type="hidden" id="Crs_BussType_KZ" name="Crs_BussType_KZ" />
        <input type="hidden" id="GrpAgentCom_KZ" name="GrpAgentCom_KZ" />
        <input type="hidden" id="GrpAgentCode_KZ" name="GrpAgentCode_KZ" />
        <input type="hidden" id="GrpAgentName_KZ" name="GrpAgentName_KZ" />
        <input type="hidden" id="GrpAgentIDNo_KZ" name="GrpAgentIDNo_KZ" />
        
        <input type="hidden" id="MissionId" name="MissionId" />
        <input type="hidden" id="Flag" name="Flag" value = "0"/>
        <input type="hidden" id="SubMissionId" name="SubMissionId" />
        <input type="hidden" id="ProcessId" name="ProcessId" />
        <input type="hidden" id="ActivityId" name="ActivityId" />
        <input type="hidden" id="ActivityStatus" name="ActivityStatus" />
        <input type="hidden" id="ZXFlag" name="ZXFlag" value = "0"/>
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
