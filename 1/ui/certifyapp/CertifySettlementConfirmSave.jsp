<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-17
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	String Priview = "PREVIEW";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	TransferData tTransferData = new TransferData();
  
  	//接收信息
  	// 投保单列表
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	String tGrpContNo[] = request.getParameterValues("CertifySettleListGrid1");
	String tRadio[] = request.getParameterValues("InpCertifySettleListGridSel");
	String tMissionID[] = request.getParameterValues("CertifySettleListGrid7");   
	String tSubMissionID[] = request.getParameterValues("CertifySettleListGrid8");
	String workType = request.getParameter("workType");
	
	boolean flag = false;
	int grouppolCount = tGrpContNo.length;
	for (int i = 0; i < grouppolCount; i++)
	{
		if( tGrpContNo[i] != null && tRadio[i].equals( "1" ))
		{
            System.out.println("GrpContNo:"+i+":"+tGrpContNo[i]);
		    tLCGrpContSchema.setGrpContNo( tGrpContNo[i] );
		    if("PREVIEW".equals(workType))
		    {
		      tLCGrpContSchema.setAppFlag("9");
		    }
		    tTransferData.setNameAndValue("GrpContNo",tGrpContNo[i] );
		    tTransferData.setNameAndValue("MissionID",tMissionID[i] );
	     	tTransferData.setNameAndValue("SubMissionID",tSubMissionID[i] );
		    flag = true;
			break;
		}
	}

  	if (flag == true)
  	{
        String tActivityId = "0000011002";
        
        
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpContSchema );
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
		//LCGrpContSignBL tLCGrpContSignBL   = new LCGrpContSignBL();
		
		//boolean bl = tLCGrpContSignBL.submitData( tVData, "INSERT" );
		//int n = tLCGrpContSignBL.mErrors.getErrorCount();
        GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
		boolean bl= tTbWorkFlowUI.submitData( tVData, tActivityId);
		int n = tTbWorkFlowUI.mErrors.getErrorCount();
		if( n == 0 ) 
	    { 
		    if ( bl )
		    {
	    		Content = " 结算成功";
		   		FlagStr = "Succ";
	    	}
	    	else
	    	{
	    	   Content = " 结算失败";
	    	   FlagStr = "Fail";
	    	}
	    }
	    else
	    {
	         
	    	String strErr = "";
			for (int i = 0; i < n; i++)
			{
				strErr += (i+1) + ": " + tTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
				System.out.println(tTbWorkFlowUI.mErrors.getError(i).errorMessage );
			}
			 if ( bl )
			 {
	    		Content = " 部分结算成功,但是有如下信息: " +strErr;
		   		FlagStr = "Succ";
	    	}
	    	else
	    	{
		 		  Content = " 结算失败，原因是: " + strErr;
				  FlagStr = "Fail";
			}
		} // end of if
	} // end of if
	
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
