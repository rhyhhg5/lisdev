var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

function initForm()
{
  try
  {                                   
	  fm.all('ManageCom').value = managecom;
	  // 保单查询条件
	  var sql = "select current date - 3 month day,current date - 1 day from dual ";
      var rs = easyExecSql(sql);
      fm.all('StartMakeDate').value = rs[0][0];
      fm.all('EndMakeDate').value = rs[0][1];

	  showAllCodeName();
  }
  catch(ex)
  {
      alert("CardActDetail.js-->initForm函数中发生异常:初始化界面错误!");
  }      
}

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    if(!check())
	{
		return false;
	}
    
    var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;
    
    if((tStartMakeDate != null || tStartMakeDate != "") && (tEndMakeDate != null || tStartMakeDate != ""))
    {
		if(dateDiff(tStartMakeDate, tEndMakeDate, "M") > 3)
		{
			alert("统计期最多为三个月！");
			return false;
		}
	}
	
    if(fm.all('CardOperateType').value =="5")
    {
        var tStrSQL = ""
		+ "select "
		+ "case when lc.salechnl in ('03','04','10','15') then lc.agentcom else getUniteCode(lc.AgentCode) end,lc.ManageCom,lc.PrtNo,CValiDate,null,Prem,InsuredName, "
		+ "codename('sex',InsuredSex), InsuredBirthday,codename('idtype',InsuredIDtype),InsuredIDNo,PostalAddress,ZipCode,Phone, "
		+ "Mobile,lc.Operator,null,'已结算' "
        + "from lccont lc,lcinsured lci, lcaddress la "
        + "where 1 = 1 "
        + "and lc.ContNo = lci.ContNo and la.CustomerNo = lci.contNo and la.AddressNo = lci.AddressNo "
        + "and lc.cardflag='b' "
        + "and exists(select 1 from LICertSalesPayInfo where relainfolist = lc.prtno and wrapcode in( select code1 from ldcode1  where codetype='wrapcode' and code = 'gwzx' )) "
        + getWherePart("lc.UWDate", "StartMakeDate", ">=")
        + getWherePart("lc.UWDate", "EndMakeDate", "<=")
        + getWherePart("lc.PrtNo", "StartNo", ">=")
        + getWherePart("lc.PrtNo", "EndNo", "<=")
        + getWherePart("lc.ManageCom", "ManageCom", "like")
        + getWherePart("getUniteCode(lc.AgentCode)", "AgentCode")
        + getWherePart("lc.AgentCom", "AgentCom")
        + getWherePart("lc.Cvalidate", "StartCvalidate",">=")
        + getWherePart("lc.Cvalidate", "EndCvalidate","<=")
        + " order by lc.ManageCom,  lc.PrtNo"
        ;
	}
	else if(fm.all('CardOperateType').value =="6")
	{
	    var tStrSQL = ""
		+ "select "
		+ "case when lc.salechnl in ('03','04','10','15') then lc.agentcom else getUniteCode(lc.AgentCode) end,lc.ManageCom,lc.PrtNo,CValiDate,null,Prem,InsuredName, "
		+ "codename('sex',InsuredSex), InsuredBirthday,codename('idtype',InsuredIDtype),InsuredIDNo,PostalAddress,ZipCode,Phone, "
		+ "Mobile,lc.Operator,null,'已结算' "
        + "from lccont lc,lcinsured lci, lcaddress la "
        + "where 1 = 1 "
        + "and lc.ContNo = lci.ContNo and la.CustomerNo = lci.contNo and la.AddressNo = lci.AddressNo "
        + "and lc.cardflag='b' "
        + "and exists(select 1 from LICertSalesPayInfo where relainfolist = lc.prtno and wrapcode in( select code1 from ldcode1  where codetype='wrapcode' and code = 'hzwx' )) "
        + getWherePart("lc.UWDate", "StartMakeDate", ">=")
        + getWherePart("lc.UWDate", "EndMakeDate", "<=")
        + getWherePart("lc.PrtNo", "StartNo", ">=")
        + getWherePart("lc.PrtNo", "EndNo", "<=")
        + getWherePart("lc.ManageCom", "ManageCom", "like")
        + getWherePart("getUniteCode(lc.AgentCode)", "AgentCode")
        + getWherePart("lc.AgentCom", "AgentCom")
        + getWherePart("lc.Cvalidate", "StartCvalidate",">=")
        + getWherePart("lc.Cvalidate", "EndCvalidate","<=")
        + " order by lc.ManageCom,  lc.PrtNo"
        ;
        + " union "
        + "select "
        + "ReceiveSale, ManageCom, CardNo, CValidate, ActiveDate, Prem,InsuredName, codename('sex',InsuredSex), InsuredBirthday, codename('idtype',InsuredIDtype), "
        + "InsuredIDNo, PostalAddress, ZipCode, Phone, Mobile, "
        + "ActiveOperator,(case (select distinct cardstatus from licardactiveinfolist where cardno = a.cardno) when '03' then '已撤单' when '02' then  '已作废' else '有效' end) "
        + "from LIActiveCardDetail a "
        + "where 1 = 1 and "
        + "exists (select * from wfcontlist where batchno like 'SF0002%' and cardno=a.cardno) "
        ;
	}
	else if(fm.all('CardOperateType').value =="7")
	{
	    var tStrSQL = ""
		+ "select "
		+ "case when lc.salechnl in ('03','04','10','15') then lc.agentcom else getUniteCode(lc.AgentCode) end,lc.ManageCom,lc.PrtNo,CValiDate,null,Prem,InsuredName, "
		+ "codename('sex',InsuredSex), InsuredBirthday,codename('idtype',InsuredIDtype),InsuredIDNo,PostalAddress,ZipCode,Phone, "
		+ "Mobile,lc.Operator,null,'已结算' "
        + "from lccont lc,lcinsured lci, lcaddress la "
        + "where 1 = 1 "
        + "and lc.ContNo = lci.ContNo and la.CustomerNo = lci.contNo and la.AddressNo = lci.AddressNo "
        + "and lc.cardflag='b' "
        + "and exists(select 1 from LICertSalesPayInfo where relainfolist = lc.prtno and wrapcode in( select code1 from ldcode1  where codetype='wrapcode' and code = 'zjwx' )) "
        + getWherePart("lc.UWDate", "StartMakeDate", ">=")
        + getWherePart("lc.UWDate", "EndMakeDate", "<=")
        + getWherePart("lc.PrtNo", "StartNo", ">=")
        + getWherePart("lc.PrtNo", "EndNo", "<=")
        + getWherePart("lc.ManageCom", "ManageCom", "like")
        + getWherePart("getUniteCode(lc.AgentCode)", "AgentCode")
        + getWherePart("lc.AgentCom", "AgentCom")
        + getWherePart("lc.Cvalidate", "StartCvalidate",">=")
        + getWherePart("lc.Cvalidate", "EndCvalidate","<=")
        + " order by lc.ManageCom, lc.ExecuteCom, lc.PrtNo "
        ;
        + " union "
        + "select "
        + "ReceiveSale, ManageCom, CardNo, CValidate, ActiveDate, Prem,InsuredName, codename('sex',InsuredSex), InsuredBirthday, "
        + "codename('idtype',InsuredIDtype),InsuredIDNo, PostalAddress, ZipCode, Phone, Mobile, ActiveOperator "
        + "(case (select distinct cardstatus from licardactiveinfolist where cardno = a.cardno) when '03' then '已撤单' when '02' then  '已作废' else '有效' end)"
        + "from LIActiveCardDetail a where 1 = 1 "
        + "and exists (select * from wfcontlist where batchno like '01mingya%'  and cardno=a.cardno) "
        + "or exists (select * from wfcontlist where batchno like '01baoer%'   and cardno=a.cardno) "
        + "or exists (select * from licardactiveinfolist where licardactiveinfolist.operator='ctrip' and licardactiveinfolist.cardstatus='01' and cardno=a.cardno) "
        + "or exists (select * from wfcontlist where batchno like 'AI0008%'  and cardno=a.cardno) "
        + "or exists (select * from wfcontlist where batchno like  'AI0009%'  and cardno=a.cardno) "
        ;
	
	}
	else{
        var tStrSQL = ""
        + " select "
        + " case when getUniteCode(ReceiveSale)<>'' then getUniteCode(ReceiveSale) else ReceiveSale end , ManageCom, CardNo, CValidate, ActiveDate, Prem,"
        + " InsuredName, codename('sex',InsuredSex), InsuredBirthday, codename('idtype',InsuredIDtype),InsuredIDNo, "
        + " PostalAddress, ZipCode, Phone, Mobile, ActiveOperator,(case (select distinct cardstatus from licardactiveinfolist "
        + "where cardno = a.cardno) when '03' then '已撤单' when '02' then  '已作废' else '有效' end), " 
        + "(case (select 1 from licertify where cardno=a.cardno) when '1' then '已结算' else '未结算' end) "
        + " from LIActiveCardDetail a "
        + " where 1 = 1 "
        + getWherePart("a.OperateType", "CardOperateType")
        + getWherePart("a.MakeDate", "StartMakeDate", ">=")
        + getWherePart("a.MakeDate", "EndMakeDate", "<=")
        + getWherePart("a.CardNo", "StartNo", ">=")
        + getWherePart("a.CardNo", "EndNo", "<=")
        + getWherePart("a.CertifyCode", "CertifyTypeCode")
        + getWherePart("a.ManageCom", "ManageCom", "like")
        + getWherePart("getUniteCode(a.ReceiveSale)", "AgentCode")
        + getWherePart("a.ReceiveSale", "AgentCom")
        + getWherePart("a.Cvalidate", "StartCvalidate",">=")
        + getWherePart("a.Cvalidate", "EndCvalidate","<=")
        + " order by ManageCom, ReCeiveSale, CardNo "
        ;
    }
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "CardActDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}


function querySaleInfo()
{
    var tStrSql = null;
    
    var tAgentCode = fm.AgentCode.value;
    var tAgentCom = fm.AgentCom.value;
    
    if(tAgentCode != null && tAgentCode != "")
    {
        if(tAgentCom != null && tAgentCom != "")
        {
            alert("[业务员代码]与[中介机构代码]条件不能同时填写！");
            fm.AgentCode.value = "";
            fm.AgentCom.value = "";
            return false;
        }
        tStrSql = " select Name from LAAgent where groupAgentCode = '" + tAgentCode + "'";
        var arrResult = easyExecSql(tStrSql);
        if(arrResult)
        {
            var tTmpAgentName = arrResult[0][0];
            alert("[" + tAgentCode + "]对应的业务员为：[" + tTmpAgentName + "]");
        }
        else
        {
            alert("[" + tAgentCode + "]未找到对应的业务员信息。");
        }
    }
    else if(tAgentCom != null && tAgentCom != "")
    {
        tStrSql = " select Name from LACom where AgentCom = '" + tAgentCom + "'";
        var arrResult = easyExecSql(tStrSql);
        if(arrResult)
        {
            var tTmpAgentComName = arrResult[0][0];
            alert("[" + tAgentCom + "]对应的中介机构为：[" + tTmpAgentComName + "]");
        }
        else
        {
            alert("[" + tAgentCom + "]未找到对应的中介结构信息。");
        }
    }
    
    return true;
}

function check()
{
	var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;
    var tStartNo = fm.StartNo.value; 
    var tEndNo = fm.EndNo.value;
    var tCardOperateType = fm.CardOperateType.value;
    var StartCvalidate = fm.StartCvalidate.value;
    var EndCvalidate = fm.EndCvalidate.value;
    
    if((tStartMakeDate == null || tStartMakeDate == "") && 
    	(tEndMakeDate == null || tEndMakeDate == "") && 
    	(tStartNo == null || tStartNo == "") && 
    	(tEndNo == null || tEndNo == "") && 
    	(tCardOperateType == null || tCardOperateType == "")&&
    	(StartCvalidate == null || StartCvalidate == "")&&
    	(EndCvalidate == null || EndCvalidate == ""))
    {
    	alert("查询起止日期、销售类型、激活卡起止号和生效日期起止期必须填写一项才能进行下载！");
    	return false;
    }
    
    if(tStartMakeDate != null && tStartMakeDate != "" && (tEndMakeDate == null || tEndMakeDate == ""))
    {
    	alert("请填写投保日期止！");
    	return false;
    }
    
    if((tStartMakeDate == null || tStartMakeDate == "") && tEndMakeDate != null && tEndMakeDate != "")
    {
    	alert("请填写投保日期起！");
    	return false;
    }
    
    if((tStartNo == null || tStartNo == "") && tEndNo != null && tEndNo != "")
    {
    	alert("请填写激活卡起始号！");
    	return false;
    }
    
    if(tStartNo != null && tStartNo != "" && (tEndNo == null || tEndNo == ""))
    {
    	alert("请填写激活卡终止号！");
    	return false;
    }
     if((StartCvalidate == null || StartCvalidate == "") && EndCvalidate != null && EndCvalidate != "")
    {
    	alert("请填写激活卡生效日期起期！");
    	return false;
    }
    
    if(StartCvalidate != null && StartCvalidate != "" && (EndCvalidate == null || EndCvalidate == ""))
    {
    	alert("请填写激活卡生效日期止期！");
    	return false;
    }
    
    return true;
}

