<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-17
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<%
    GlobalInput tGI = new GlobalInput(); 
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";
	var strManageCom = "<%=tGI.ComCode%>";
</script>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="CertifySettlementApply.js"></script>
    <%@include file="CertifySettlementApplyInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">请输入查询条件：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">单证结算号</td>
                    <td class="input8">
                        <input class="common" name="PrtNo" />
                    </td>
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title8">申请日期</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="ApplyDate" verify="申请日期|date" />
                    </td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryCertifySettlementList();" /> 	
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnApplyCertifySettlement" name="btnApplyCertifySettlement" value="申  请" onclick="applyCertifySettlement();" />    
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divGrpCustomerGrid);" />
                </td>
                <td class="titleImg">待处理结算单</td>
            </tr>
        </table>
        <div id="divCertifySettleListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifySettleListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertifySettleListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>        
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnDelMission" value="卡折结算" onclick="inputCertifySettlement();" />   
                </td>
            </tr>
        </table>
        
        <input type="hidden" class="common" name="ActivityID" value="" />
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
