function initForm() {
	try {
		fm.all('ManageCom').value = managecom;
		// 保单查询条件
		var sql = "select current date - 3 month day,current date - 1 day from dual ";
		var rs = easyExecSql(sql);
		fm.all('StartMakeDate').value = rs[0][0];
		fm.all('EndMakeDate').value = rs[0][1];

		showAllCodeName();
	} catch (ex) {
		alert("CardActDetail.js-->initForm函数中发生异常:初始化界面错误!");
	}
}
// 提交，保存按钮对应操作
function submitForm() {
	if (verifyInput() == false) {
		return false;
	}

	if (!check()) {
		return false;
	}

	tStartMakeDate = fm.StartMakeDate.value;
	var tEndMakeDate = fm.EndMakeDate.value;

	if ((tStartMakeDate != null || tStartMakeDate != "")
			&& (tEndMakeDate != null || tStartMakeDate != "")) {
		if (dateDiff(tStartMakeDate, tEndMakeDate, "M") > 3) {
			alert("统计期最多为三个月！");
			return false;
		}
	}

	if (fm.all('ProjectName').value == "zm") {

		var tStrSQL = ""
			+ "  SELECT  DISTINCT lcc.managecom,ld. NAME, 'zm', ldc.CODEALIAS, lcr.RISKWRAPCODE, ldr.RISKWRAPNAME, ldr.MainRISKCODE,  "
			+ "  lmr.RISKNAME, 'health' , laa.AGENTCODE,  laa.NAME,lcc.contno, "
			+ "  lcc.CValiDate, "
			+ "  NULL,ldcode.CODENAME,lcc.PREM,lcc.InsuredName, "
			+ "  codename ('sex', lcc.InsuredSex), "
			+ "  lcc.InsuredBirthday ,"
			+ "  codename ('idtype', lcc.InsuredIDtype) , "
			+ "  lcc.InsuredIDNo ,  lca.PostalAddress ,lca.ZipCode  ,lca.Phone , lca.Mobile "
			+ "  FROM lccont lcc,ldcom ld,ldcode1 ldc,LDRISKWRAP ldr,LcRISKDUTYWRAP lcr,lcinsured lci,lcaddress lca, WXContRelaInfo wxc, laagent  laa,lmriskapp lmr,ldcode ldcode "
			+ "  where 1 = 1 " + " and ld.COMCODE = lcc.MANAGECOM "
			+ " and ldc.CODETYPE = 'salechnl' "
			+ "  and ldc.code = lcc.salechnl "
			+ "  and lmr.RISKCODE =  ldr.MainRISKCODE"
			+ "  and lcr.contno = lcc.contno "
			+ "  and lcc.ContNo = lci.ContNo "
			+ "  and lcc.insuredno =  lci.insuredno "
			+ "  and lca.CustomerNo = lci.InsuredNo "
			+ "  and lca.AddressNo = lci.AddressNo  "
			+ "  and ldr.RISKWRAPCODE=lcr.RISKWRAPCODE "
			+ " and  laa.AGENTCODE=lcc.AGENTCODE "
			+ " and lcc.cardflag = 'b' " + " and wxc.prtno = lcc.prtno "
			+ " and lcc.appflag = '1' " + " and wxc.batchno like 'ZMB%' "
			+ " and ldcode.codetype = 'payintv'  "
			+ "  and ldcode.code = lcc.payintv   "
			+ getWherePart("lcc.Makedate", "StartMakeDate", ">=")
			+ getWherePart("lcc.Makedate", "EndMakeDate", "<=")
			+ getWherePart("lcc.contno", "StartNo", ">=")
			+ getWherePart("lcc.contno", "EndNo", "<=")
			+ getWherePart("lcc.ManageCom", "ManageCom", "like")
			+ getWherePart("getUniteCode(lcc.AgentCode)", "AgentCode")
			+ getWherePart("lcc.AgentCom", "AgentCom")
			+ getWherePart("lcc.Cvalidate", "StartCvalidate", ">=")
			+ getWherePart("lcc.Cvalidate", "EndCvalidate", "<=")
			+ "  order by lcc.ManageCom, lcc.contno";

	} else if (fm.all('ProjectName').value == "HZW") {
		var tStrSQL = ""
				+ "  SELECT  DISTINCT lcc.managecom,ld. NAME, 'HZW', ldc.CODEALIAS, lcr.RISKWRAPCODE, ldr.RISKWRAPNAME,ldr.MainRISKCODE,  "
				+ "  lmr.RISKNAME, 'health' , laa.AGENTCODE,  laa.NAME,lcc.contno, "
				+ "  lcc.CValiDate, "
				+ "  NULL,ldcode.CODENAME,lcc.PREM,lcc.InsuredName, "
				+ "  codename ('sex', lcc.InsuredSex), "
				+ "  lcc.InsuredBirthday ,"
				+ "  codename ('idtype', lcc.InsuredIDtype) , "
				+ "  lcc.InsuredIDNo ,  lca.PostalAddress ,lca.ZipCode  ,lca.Phone , lca.Mobile "
				+ "  FROM lccont lcc,ldcom ld,ldcode1 ldc,LDRISKWRAP ldr,LcRISKDUTYWRAP lcr,lcinsured lci,lcaddress lca, WXContRelaInfo wxc, laagent  laa,lmriskapp lmr,ldcode ldcode "
				+ "  where 1 = 1 " + " and ld.COMCODE = lcc.MANAGECOM "
				+ " and ldc.CODETYPE = 'salechnl' "
				+ "  and ldc.code = lcc.salechnl "
				+ "  and lmr.RISKCODE =  ldr.MainRISKCODE "
				+ "  and lcr.contno = lcc.contno "
				+ "  and lcc.ContNo = lci.ContNo "
				+ "  and lcc.insuredno =  lci.insuredno "
				+ "  and lca.CustomerNo = lci.InsuredNo "
				+ "  and lca.AddressNo = lci.AddressNo  "
				+ "  and ldr.RISKWRAPCODE=lcr.RISKWRAPCODE "
				+ " and  laa.AGENTCODE=lcc.AGENTCODE "
				+ " and lcc.cardflag = 'b' " + " and wxc.prtno = lcc.prtno "
				+ " and lcc.appflag = '1' " + " and wxc.batchno like 'HZW%' "
				+ " and ldcode.codetype = 'payintv'  "
				+ "  and ldcode.code = lcc.payintv   "
				+ getWherePart("lcc.Makedate", "StartMakeDate", ">=")
				+ getWherePart("lcc.Makedate", "EndMakeDate", "<=")
				+ getWherePart("lcc.contno", "StartNo", ">=")
				+ getWherePart("lcc.contno", "EndNo", "<=")
				+ getWherePart("lcc.ManageCom", "ManageCom", "like")
				+ getWherePart("getUniteCode(lcc.AgentCode)", "AgentCode")
				+ getWherePart("lcc.AgentCom", "AgentCom")
				+ getWherePart("lcc.Cvalidate", "StartCvalidate", ">=")
				+ getWherePart("lcc.Cvalidate", "EndCvalidate", "<=")
				+ "  order by lcc.ManageCom, lcc.contno";
	} else if (fm.all('ProjectName').value == "QBD") {
		var tStrSQL = ""
			+ "  SELECT  DISTINCT lcc.managecom,ld. NAME, 'QBD', ldc.CODEALIAS, lcr.RISKWRAPCODE, ldr.RISKWRAPNAME,lcpol.RISKCODE, "
			+ "  lmr.RISKNAME, 'health' , laa.AGENTCODE,  laa.NAME,lcc.contno, "
			+ "  lcc.CValiDate, "
			+ "  NULL,ldcode.CODENAME,lcpol.PREM,lcc.InsuredName, "
			+ "  codename ('sex', lcc.InsuredSex), "
			+ "  lcc.InsuredBirthday ,"
			+ "  codename ('idtype', lcc.InsuredIDtype) , "
			+ "  lcc.InsuredIDNo ,  lca.PostalAddress ,lca.ZipCode  ,lca.Phone , lca.Mobile "
			+ "  FROM lccont lcc,ldcom ld,ldcode1 ldc,LDRISKWRAP ldr,LcRISKDUTYWRAP lcr,lcinsured lci,lcaddress lca, WXContRelaInfo wxc, laagent  laa,lmriskapp lmr,ldcode ldcode, lcpol  lcpol  "
			+ "  where 1 = 1 " + " and ld.COMCODE = lcc.MANAGECOM "
			+ " and ldc.CODETYPE = 'salechnl' "
			+ "  and ldc.code = lcc.salechnl "
			+ "  and lmr.RISKCODE =  lcpol.RISKCODE"
			+ "  and lcr.contno = lcc.contno "
			+ "  and lcc.ContNo = lci.ContNo "
			+ "  and lcc.insuredno =  lci.insuredno "
			+ "  and lca.CustomerNo = lci.InsuredNo "
			+ "  and lca.AddressNo = lci.AddressNo  "
			+ "  and ldr.RISKWRAPCODE=lcr.RISKWRAPCODE "
			+ " and  laa.AGENTCODE=lcc.AGENTCODE "
			+ " and lcc.cardflag = 'b' " + " and wxc.prtno = lcc.prtno "
			+ " and lcc.appflag = '1' " + " and wxc.batchno like 'QBD%' "
			+ " and ldcode.codetype = 'payintv'  "
			+ "  and ldcode.code = lcc.payintv   "
			+ "  and lcpol.contno= lcc.contno  "
			+ getWherePart("lcc.Makedate", "StartMakeDate", ">=")
			+ getWherePart("lcc.Makedate", "EndMakeDate", "<=")
			+ getWherePart("lcc.contno", "StartNo", ">=")
			+ getWherePart("lcc.contno", "EndNo", "<=")
			+ getWherePart("lcc.ManageCom", "ManageCom", "like")
			+ getWherePart("getUniteCode(lcc.AgentCode)", "AgentCode")
			+ getWherePart("lcc.AgentCom", "AgentCom")
			+ getWherePart("lcc.Cvalidate", "StartCvalidate", ">=")
			+ getWherePart("lcc.Cvalidate", "EndCvalidate", "<=")
			+ "  order by lcc.ManageCom, lcc.contno,lcpol.RISKCODE";
	} else if (fm.all('ProjectName').value == "tb"){
		var tStrSQL = ""
			+ "  SELECT  DISTINCT lcc.managecom,ld. NAME, 'tb', ldc.CODEALIAS, lcr.RISKWRAPCODE, ldr.RISKWRAPNAME,lcpol.RISKCODE, "
			+ "  lmr.RISKNAME, 'health' , laa.AGENTCODE,  laa.NAME,lcc.contno, "
			+ "  lcc.CValiDate, "
			+ "  NULL,ldcode.CODENAME,lcpol.PREM,lcc.InsuredName, "
			+ "  codename ('sex', lcc.InsuredSex), "
			+ "  lcc.InsuredBirthday ,"
			+ "  codename ('idtype', lcc.InsuredIDtype) , "
			+ "  lcc.InsuredIDNo ,  lca.PostalAddress ,lca.ZipCode  ,lca.Phone , lca.Mobile "
			+ "  FROM lccont lcc,ldcom ld,ldcode1 ldc,LDRISKWRAP ldr,LcRISKDUTYWRAP lcr,lcinsured lci,lcaddress lca, WXContRelaInfo wxc, laagent  laa,lmriskapp lmr,ldcode ldcode, lcpol  lcpol  "
			+ "  where 1 = 1 " + " and ld.COMCODE = lcc.MANAGECOM "
			+ " and ldc.CODETYPE = 'salechnl' "
			+ "  and ldc.code = lcc.salechnl " 
			+ "  and lmr.RISKCODE =  lcpol.RISKCODE"
			+ "  and lcr.contno = lcc.contno "
			+ "  and lcc.ContNo = lci.ContNo "
			+ "  and lcc.insuredno =  lci.insuredno "
			+ "  and lca.CustomerNo = lci.InsuredNo "
			+ "  and lca.AddressNo = lci.AddressNo  "
			+ "  and ldr.RISKWRAPCODE=lcr.RISKWRAPCODE "
			+ " and  laa.AGENTCODE=lcc.AGENTCODE "
			+ " and lcc.cardflag = 'b' " + " and wxc.prtno = lcc.prtno "
			+ " and lcc.appflag = '1' " + " and wxc.batchno like 'tb%' "
			+ " and ldcode.codetype = 'payintv'  "
			+ "  and ldcode.code = lcc.payintv   "
			+ "  and lcpol.contno= lcc.contno  "
			+ getWherePart("lcc.Makedate", "StartMakeDate", ">=")
			+ getWherePart("lcc.Makedate", "EndMakeDate", "<=")
			+ getWherePart("lcc.contno", "StartNo", ">=")
			+ getWherePart("lcc.contno", "EndNo", "<=")
			+ getWherePart("lcc.ManageCom", "ManageCom", "like")
			+ getWherePart("getUniteCode(lcc.AgentCode)", "AgentCode")
			+ getWherePart("lcc.AgentCom", "AgentCom")
			+ getWherePart("lcc.Cvalidate", "StartCvalidate", ">=")
			+ getWherePart("lcc.Cvalidate", "EndCvalidate", "<=")
			+ "  order by lcc.ManageCom, lcc.contno,lcpol.RISKCODE";
	}else if (fm.all('ProjectName').value == "QNE"){
		var tStrSQL = ""
			+ "  SELECT  DISTINCT lcc.managecom,ld. NAME, 'QNE', ldc.CODEALIAS, lcr.RISKWRAPCODE, ldr.RISKWRAPNAME,lcpol.RISKCODE, "
			+ "  lmr.RISKNAME, 'health' , laa.AGENTCODE,  laa.NAME,lcc.contno, "
			+ "  lcc.CValiDate, "
			+ "  NULL,ldcode.CODENAME,lcpol.PREM,lcc.InsuredName, "
			+ "  codename ('sex', lcc.InsuredSex), "
			+ "  lcc.InsuredBirthday ,"
			+ "  codename ('idtype', lcc.InsuredIDtype) , "
			+ "  lcc.InsuredIDNo ,  lca.PostalAddress ,lca.ZipCode  ,lca.Phone , lca.Mobile "
			+ "  FROM lccont lcc,ldcom ld,ldcode1 ldc,LDRISKWRAP ldr,LcRISKDUTYWRAP lcr,lcinsured lci,lcaddress lca, WXContRelaInfo wxc, laagent  laa,lmriskapp lmr,ldcode ldcode, lcpol  lcpol  "
			+ "  where 1 = 1 " + " and ld.COMCODE = lcc.MANAGECOM "
			+ " and ldc.CODETYPE = 'salechnl' "
			+ "  and ldc.code = lcc.salechnl "
			+ "  and lmr.RISKCODE =  lcpol.RISKCODE"
			+ "  and lcr.contno = lcc.contno "
			+ "  and lcc.ContNo = lci.ContNo "
			+ "  and lcc.insuredno =  lci.insuredno "
			+ "  and lca.CustomerNo = lci.InsuredNo "
			+ "  and lca.AddressNo = lci.AddressNo  "
			+ "  and ldr.RISKWRAPCODE=lcr.RISKWRAPCODE "
			+ " and  laa.AGENTCODE=lcc.AGENTCODE "
			+ " and lcc.cardflag = 'b' " + " and wxc.prtno = lcc.prtno "
			+ " and lcc.appflag = '1' " + " and lcr.RISKWRAPCODE = 'ZZJ045' "
			+ " and ldcode.codetype = 'payintv'  "
			+ "  and ldcode.code = lcc.payintv   "
			+ "  and lcpol.contno= lcc.contno  "
			+ getWherePart("lcc.Makedate", "StartMakeDate", ">=")
			+ getWherePart("lcc.Makedate", "EndMakeDate", "<=")
			+ getWherePart("lcc.contno", "StartNo", ">=")
			+ getWherePart("lcc.contno", "EndNo", "<=")
			+ getWherePart("lcc.ManageCom", "ManageCom", "like")
			+ getWherePart("getUniteCode(lcc.AgentCode)", "AgentCode")
			+ getWherePart("lcc.AgentCom", "AgentCom")
			+ getWherePart("lcc.Cvalidate", "StartCvalidate", ">=")
			+ getWherePart("lcc.Cvalidate", "EndCvalidate", "<=")
			+ "  order by lcc.ManageCom, lcc.contno,lcpol.RISKCODE";
	}else if(fm.all('ProjectName').value == "SZM"){
		var tStrSQL = "select lmc.managecom ,ld. NAME, 'ML',NULL ,ldw.riskwrapcode ,ldw.wrapname ,lmr.riskcode ,lmr.riskname, 'health' ," 
				+ "case when wf.agentcode=null then null else wf.agentcode end,case when wf.agentcode=null then null else (select name from laagent where agentcode=wf.agentcode) end ,wf.cardno,wf.cvalidate,NULL ," 
				+ "ldc.CODENAME , case ldr.riskcode when  520601 then 9*wf.Copys when  122001 then 33*wf.Copys  end, " 
				+ "wfi.name,codename ('sex', wfi.Sex),wfi.birthday,codename ('idtype',wfi.IDtype), wfi.idno,wfi.postaladdress,wfi.zipcode,wfi.phont,wfi.mobile  " 
				+ "from  wfcontlist wf ,licardactiveinfolist lic ,LMCertifyDes lmc ,ldcom ld , lmriskapp lmr ,ldwrap ldw ,ldriskwrap ldr ,ldcode ldc  ,WFINSULIST wfi   " 
				+ "where 1=1  and ld.COMCODE = lmc.managecom " 
				+ "and ldc.codetype = 'payintv' " 
				+ "and ldc.code = wf.payintv " 
				+ "and ldw.riskwrapcode = wf.riskcode   " 
				+ "and ldr.riskwrapcode = wf.riskcode  " 
				+ "and wf.cardno = lic.cardno   " 
				+ "and wf.BRANCHCODE = 'SZM'  " 
				+ "and wfi.cardno=lic.cardno " 
				+ "and wfi.insuno = lic.sequenceno " 
				+ "and lmc.certifycode = wf.certifycode " 
				+  "and lmr.riskcode=ldr.riskcode  " 
				+ "and lic.cardstatus in ('01') " 
				+ getWherePart("lic.makedate", "StartMakeDate", ">=")
				+ getWherePart("lic.makedate", "EndMakeDate", "<=")
				+ getWherePart("wf.cardno", "StartNo", ">=")
				+ getWherePart("wf.cardno", "EndNo", "<=")
				+ getWherePart("lmc.managecom", "ManageCom", "like")
				+ getWherePart("getUniteCode(wf.AgentCode)", "AgentCode")
				+ getWherePart("wf.AgentCom", "AgentCom")
				+ getWherePart("wf.Cvalidate", "StartCvalidate", ">=")
				+ getWherePart("wf.Cvalidate", "EndCvalidate", "<=")
				+ " order by wf.cardno";
	}else if(fm.all('ProjectName').value == "LMM"){
		var tStrSQL = "select lmc.managecom ,ld. NAME, 'ML',NULL ,ldw.riskwrapcode ,ldw.wrapname ,lmr.riskcode ,lmr.riskname, 'health' ," 
			+ "case when wf.agentcode=null then null else wf.agentcode end,case when wf.agentcode=null then null else (select name from laagent where agentcode=wf.agentcode) end ,wf.cardno,wf.cvalidate,NULL ," 
			+ "ldc.CODENAME , case ldr.riskcode when  520601 then 9*wf.Copys when  122001 then 33*wf.Copys  end, " 
			+ "wfi.name,codename ('sex', wfi.Sex),wfi.birthday,codename ('idtype',wfi.IDtype), wfi.idno,wfi.postaladdress,wfi.zipcode,wfi.phont,wfi.mobile  " 
			+ "from  wfcontlist wf ,licardactiveinfolist lic ,LMCertifyDes lmc ,ldcom ld , lmriskapp lmr ,ldwrap ldw ,ldriskwrap ldr ,ldcode ldc  ,WFINSULIST wfi   " 
			+ "where 1=1  and ld.COMCODE = lmc.managecom " 
			+ "and ldc.codetype = 'payintv' " 
			+ "and ldc.code = wf.payintv " 
			+ "and ldw.riskwrapcode = wf.riskcode   " 
			+ "and ldr.riskwrapcode = wf.riskcode  " 
			+ "and wf.cardno = lic.cardno   " 
			+ "and wf.BRANCHCODE = 'LMM'  " 
			+ "and wfi.cardno=lic.cardno " 
			+ "and wfi.insuno = lic.sequenceno " 
			+ "and lmc.certifycode = wf.certifycode " 
			+  "and lmr.riskcode=ldr.riskcode  " 
			+ "and lic.cardstatus in ('01') " 
			+ getWherePart("lic.makedate", "StartMakeDate", ">=")
			+ getWherePart("lic.makedate", "EndMakeDate", "<=")
			+ getWherePart("wf.cardno", "StartNo", ">=")
			+ getWherePart("wf.cardno", "EndNo", "<=")
			+ getWherePart("lmc.managecom", "ManageCom", "like")
			+ getWherePart("getUniteCode(wf.AgentCode)", "AgentCode")
			+ getWherePart("wf.AgentCom", "AgentCom")
			+ getWherePart("wf.Cvalidate", "StartCvalidate", ">=")
			+ getWherePart("wf.Cvalidate", "EndCvalidate", "<=")
			+ " order by wf.cardno";
}
	fm.querySql.value = tStrSQL;

	var oldAction = fm.action;

	fm.action = "CardActDetailSaveds.jsp";
	fm.submit();
	fm.action = oldAction;

}

function querySaleInfo() {
	var tStrSql = null;

	var tAgentCode = fm.AgentCode.value;
	var tAgentCom = fm.AgentCom.value;

	if (tAgentCode != null && tAgentCode != "") {
		if (tAgentCom != null && tAgentCom != "") {
			alert("[业务员代码]与[中介机构代码]条件不能同时填写！");
			fm.AgentCode.value = "";
			fm.AgentCom.value = "";
			return false;
		}
		tStrSql = " select Name from LAAgent where groupAgentCode = '"
				+ tAgentCode + "'";
		var arrResult = easyExecSql(tStrSql);
		if (arrResult) {
			var tTmpAgentName = arrResult[0][0];
			alert("[" + tAgentCode + "]对应的业务员为：[" + tTmpAgentName + "]");
		} else {
			alert("[" + tAgentCode + "]未找到对应的业务员信息。");
		}
	} else if (tAgentCom != null && tAgentCom != "") {
		tStrSql = " select Name from LACom where AgentCom = '" + tAgentCom
				+ "'";
		var arrResult = easyExecSql(tStrSql);
		if (arrResult) {
			var tTmpAgentComName = arrResult[0][0];
			alert("[" + tAgentCom + "]对应的中介机构为：[" + tTmpAgentComName + "]");
		} else {
			alert("[" + tAgentCom + "]未找到对应的中介结构信息。");
		}
	}

	return true;
}

function check() {
	var tStartMakeDate = fm.StartMakeDate.value;
	var tEndMakeDate = fm.EndMakeDate.value;
	var tStartNo = fm.StartNo.value;
	var tEndNo = fm.EndNo.value;
	var tCardOperateType = fm.CardOperateType.value;
	var ProjectName = fm.ProjectName.value;
	var StartCvalidate = fm.StartCvalidate.value;
	var EndCvalidate = fm.EndCvalidate.value;

	if ((tStartMakeDate == null || tStartMakeDate == "")
			&& (tEndMakeDate == null || tEndMakeDate == "")
			&& (tStartNo == null || tStartNo == "")
			&& (tEndNo == null || tEndNo == "")
			&& (tCardOperateType == null || tCardOperateType == "")
			&& (StartCvalidate == null || StartCvalidate == "")
			&& (EndCvalidate == null || EndCvalidate == "")) {
		alert("查询起止日期、销售类型、激活卡起止号和生效日期起止期必须填写一项才能进行下载！");
		return false;
	}

	if (tStartMakeDate != null && tStartMakeDate != ""
			&& (tEndMakeDate == null || tEndMakeDate == "")) {
		alert("请填写查询日期止！");
		return false;
	}

	if ((tStartMakeDate == null || tStartMakeDate == "")
			&& tEndMakeDate != null && tEndMakeDate != "") {
		alert("请填写查询日期起！");
		return false;
	}

	if ((tStartNo == null || tStartNo == "") && tEndNo != null && tEndNo != "") {
		alert("请填写激活卡起始号！");
		return false;
	}

	if (tStartNo != null && tStartNo != "" && (tEndNo == null || tEndNo == "")) {
		alert("请填写激活卡终止号！");
		return false;
	}
	if ((StartCvalidate == null || StartCvalidate == "")
			&& EndCvalidate != null && EndCvalidate != "") {
		alert("请填写激活卡生效日期起期！");
		return false;
	}

	if (StartCvalidate != null && StartCvalidate != ""
			&& (EndCvalidate == null || EndCvalidate == "")) {
		alert("请填写激活卡生效日期止期！");
		return false;
	}

	return true;
}
