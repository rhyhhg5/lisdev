<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.utility.*" %>

<%
System.out.println("CertifyChkErrSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

CErrors tError = null;

try
{
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    String tBatchNo = request.getParameter("BatchNo");    
    
    TransferData tTransferData = new TransferData();
    
    tTransferData.setNameAndValue("BatchNo", tBatchNo);
    
    VData tVData = new VData();
    tVData.add(tG);
    tVData.add(tTransferData);

    CertifyImpListCheckUI tCertifyImpListCheckUI = new CertifyImpListCheckUI();
    if (!tCertifyImpListCheckUI.submitData(tVData, "Check"))
    {
        Content = " 检查清单失败! 原因是: " + tCertifyImpListCheckUI.mErrors.getLastError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 检查成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 检查失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("CertifyChkErrSave.jsp End ...");
%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
