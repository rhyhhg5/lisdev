//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifyList()
{
    if(!verifyInput2())
    {
        return false;
    }
    //查询前的校验
    if(!CheckDate())
    {
    	return false;
    }
  
    var tStrSql = ""
        + " select lict.CardNo, lict.PrtNo, lict.ManageCom, licail.CValidate, licail.ActiveDate, "
        + " lict.CertifyCode, "
        + " ("
        + " select "
        + " ldw.WrapName "
        + " from LMCardRisk lmcr "
        + " inner join LDWrap ldw on ldw.RiskWrapCode = lmcr.RiskCode and lmcr.RiskType = 'W' "
        + " where 1 = 1 "
        + " and lmcr.CertifyCode = lict.CertifyCode "
        + " ) RiskWrapName, "
        + " ( "
        + " select "
        + " max(case when ldrdw.calfactor = 'InsuYear' then ldrdw.calfactorvalue end) || max(case when ldrdw.calfactor = 'InsuYearFlag' then ldrdw.calfactorvalue end) "
        + " from LDRiskDutyWrap ldrdw "
        + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W'"
        + " where 1 = 1 "
        + " and lmcr.CertifyCode = lict.CertifyCode "
        + " and ldrdw.CalFactor in ('InsuYear', 'InsuYearFlag') "
        + " fetch first rows only "
        + " ) InsuYear, "
        + " varchar(lict.Prem) Prem, "
        + " varchar(lict.Amnt) Amnt, "
        + " licail.Name, "
        + " CodeName('sex', licail.Sex) Sex, licail.Birthday, "
        + " CodeName('idtype', licail.IdType) IdType, varchar(licail.IdNo) IdNo, "
        + " licail.PostalAddress, licail.ZipCode, licail.Phone, licail.Mobile, licail.Email, "  // add  by zhangyang
        + " CodeName('certifycontstate', lict.State) State, "
        + " CodeName('ccwsstate', lict.WSState) WSState "
        + " from LICertify lict "
        + " left join LICardActiveInfoList licail on licail.CardNo = lict.CardNo "
        + " where 1 = 1 "
        + " and lict.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart("lict.CardNo", "CardNo")
        + getWherePart("licail.Name", "InsuName")
        + getWherePart("licail.IdNo", "InsuIdNo")
        + getWherePart("licail.CValidate", "CValiStartDate", ">=")
        + getWherePart("licail.CValidate", "CValiEndDate", "<=")
        + " union "
        + " select "
        + " licail.CardNo, lict.PrtNo, lict.ManageCom, licail.CValidate, licail.ActiveDate, "
        + " lzc.CertifyCode, "
        + " ("
        + " select "
        + " ldw.WrapName "
        + " from LMCardRisk lmcr "
        + " inner join LDWrap ldw on ldw.RiskWrapCode = lmcr.RiskCode and lmcr.RiskType = 'W' "
        + " where 1 = 1 "
        + " and lmcr.CertifyCode = lzc.CertifyCode "
        + " ) RiskWrapName, "
        + " ( "
        + " select "
        + " max(case when ldrdw.calfactor = 'InsuYear' then ldrdw.calfactorvalue end) || max(case when ldrdw.calfactor = 'InsuYearFlag' then ldrdw.calfactorvalue end) "
        + " from LDRiskDutyWrap ldrdw "
        + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W'"
        + " where 1 = 1 "
        + " and lmcr.CertifyCode = lzc.CertifyCode "
        + " and ldrdw.CalFactor in ('InsuYear', 'InsuYearFlag') "
        + " fetch first rows only "
        + " ) InsuYear, "
        + " varchar(licail.Prem) Prem, "
        + " varchar(licail.Amnt) Amnt, "
        + " licail.Name, "
        + " CodeName('sex', licail.Sex) Sex, licail.Birthday, "
        + " CodeName('idtype', licail.IdType) IdType, varchar(licail.IdNo) IdNo, "
        + " licail.PostalAddress, licail.ZipCode, licail.Phone, licail.Mobile, licail.Email, "  // add  by zhangyang
        + " CodeName('certifycontstate', lict.State) State, "
        + " CodeName('ccwsstate', lict.WSState) WSState "
        + " from LICardActiveInfoList licail "
        + " left join LICertify lict on licail.CardNo = lict.CardNo "
        + " inner join LZCardNumber lzcn on lzcn.CardNo = licail.CardNo "
        + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
        + " where 1 = 1 "
        + " and not exists (select 1 from LICertify lic where lic.CardNo = licail.CardNo) "
        + getWherePart("licail.CardNo", "CardNo")
        + getWherePart("licail.Name", "InsuName")
        + getWherePart("licail.IdNo", "InsuIdNo")
        + getWherePart("licail.CValidate", "CValiStartDate", ">=")
        + getWherePart("licail.CValidate", "CValiEndDate", "<=")
        + " with ur "
        ;
    
    turnPage1.pageDivName = "divCertifyListGridPage";
    turnPage1.queryModal(tStrSql, CertifyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有待处理的结算待信息！");
        return false;
    }
    
    return true;
}

//查询前对日期进行的校验
function CheckDate()
{
	var CValiStartDate = fm.all('CValiStartDate').value;
	var CValiEndDate = fm.all('CValiEndDate').value;
	
	var CardNo = fm.all('CardNo').value;
	
	var InsuName = fm.all('InsuName').value;
	var InsuIdNo = fm.all('InsuIdNo').value;
	
	if((CardNo == null || CardNo == "") && (InsuName == null || InsuName == "") && (InsuIdNo == null || InsuIdNo == "")){
		if((CValiStartDate == null || CValiStartDate == "")
			&& (CValiEndDate == null || CValiEndDate == ""))
		{
			alert("请选择生效起止日期才能进行查询！");
			return false;
		}
		if((CValiStartDate == null || CValiStartDate == "")
				&& (CValiEndDate != null && CValiEndDate != ""))
		{
			alert("请选择生效日期起期！");
			return false;
		}
			
		if((CValiStartDate != null && CValiStartDate != "")
				&& (CValiEndDate == null || CValiEndDate == ""))
		{
			alert("请选择生效日期止期！");
			return false;
		}
		if(CValiStartDate != null && CValiStartDate != ""
				&& CValiEndDate != null && CValiEndDate != "")
		{
			if(dateDiff(CValiStartDate,CValiEndDate,"M")>3)
			{
				alert("生效日期统计期最多为三个月！");
				return false;
			}
		}
	}
	
	return true;
}

/**
 * 卡折实名化
 */
function dealWSCertify()
{
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }

    if (confirm("实名化成功后，数据将不能回退，您确实想实名化该记录吗?"))
    {
        showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
        fm.btnWSCertify.disabled = true;
        fm.action = "WSCertifySave.jsp";
        fm.submit();
        fm.action = "";
    }

}

/**
 * 实名化提交后动作。
 */
function afterWSSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("实名化失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        //fm.BatchNo.value = cBatchNo;
        //queryCertifyImportBatchList();
    }

    fm.btnWSCertify.disabled = false;
}

