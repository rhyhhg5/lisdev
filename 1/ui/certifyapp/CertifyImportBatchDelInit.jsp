<%
//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录:  于明明   更新日期  2015-1-14  更新原因/内容 1、单选//框改为复选框。
//2、一页显示30条记录。
//3、增加“批次整体删除”按钮，点此按钮后，弹出提示框，提示“该操作将会将批次号为XXXXXXXXXX下的所有卡折，是否继续？”，选择“是”，删除整个批次下的卡折。选择“否”，则取消操作。

%>

<%
 GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initCertifyGrid();
        
        fm.all('ManageCom').value = <%=strManageCom%>;
        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initCertifyGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="管理机构";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="单证号";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="印刷号";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="录入日期";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;

        CertifyGrid = new MulLineEnter("fm", "CertifyGrid"); 

        CertifyGrid.mulLineCount = 0;   
        CertifyGrid.displayTitle = 1;
        //CertifyGrid.canSel = 1; //单选框；
        CertifyGrid.hiddenSubtraction = 1;
        CertifyGrid.hiddenPlus = 1;
        CertifyGrid.canChk = 1; 
        CertifyGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化CertifyGrid时出错：" + ex);
    }
}
</script>

