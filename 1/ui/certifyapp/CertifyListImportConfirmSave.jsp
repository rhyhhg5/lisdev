<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
System.out.println("CertifyListImportConfirmSave.jsp Begin ...");

GlobalInput tG = (GlobalInput)session.getValue("GI");

String FlagStr = "Fail";
String Content = "";

String tBatchNo = request.getParameter("ConfirmBatchNo");
String tOperateType = request.getParameter("fmOperatorFlag");

if("ImportConfirm".equals(tOperateType))
{
    tOperateType = "ImportConfirm";
}
else if("ImportDelete".equals(tOperateType))
{
    tOperateType = "ImportDelete";
}
else
{
    tOperateType = null;
}

try
{
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("BatchNo", tBatchNo);
    
    tVData.add(tTransferData);
    tVData.add(tG);

    AddCertifyListUI tAddCertifyListUI = new AddCertifyListUI();
    
    if (!tAddCertifyListUI.submitData(tVData, tOperateType))
    {
        Content = " 数据处理失败! 原因是: " + tAddCertifyListUI.mErrors.getLastError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 数据处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 异常失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("CertifyListImportConfirmSave.jsp End ...");
%>
                  
<html>
<script language="javascript">
parent.fraInterface.afterConfirmImport("<%=FlagStr%>", "<%=Content%>");
</script>
</html>

