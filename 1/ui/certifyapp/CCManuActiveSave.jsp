<%
//程序名称：
//程序功能：
//创建日期：2008-12-08
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="java.io.File" %>

<%
System.out.println("CCManuActiveSave.jsp Begin ...");

GlobalInput tG = (GlobalInput)session.getValue("GI");

String FlagStr = "Fail";
String Content = "";


try
{
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    
    tVData.add(tTransferData);
    tVData.add(tG);

    CardActiveBatchImportBL tCardActiveBatchImportBL = new CardActiveBatchImportBL();
    
    if (!tCardActiveBatchImportBL.submitData(tVData, "ActiveImport"))
    {
        Content = " 导入清单失败! 原因是: " + tCardActiveBatchImportBL.mErrors.getLastError();
        FlagStr = "Fail";
    }
    else if(tCardActiveBatchImportBL.mErrors.needDealError())
    {
        Content = tCardActiveBatchImportBL.mErrors.getErrContent();
        FlagStr = "Succ";
    }
    else
    {
        Content = " 导入清单成功！";
        FlagStr = "Succ";
    }
  
	Content = PubFun.changForHTML(Content);
}
catch (Exception e)
{
    Content = " 获取批次信息失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}
  
//Content = PubFun.changForHTML(content);

System.out.println("CCManuActiveSave.jsp end ...");
%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

