var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

function initForm()
{
  try
  {                                   
	  fm.all('ManageCom').value = managecom;
	  // 保单查询条件
	  var sql = "select current date - 3 month day,current date - 1 day  from dual ";
      var rs = easyExecSql(sql);
      fm.all('StartMakeDate').value = rs[0][0];
      fm.all('EndMakeDate').value = rs[0][1];
      fm.all('CardPremType').value = "0";

	  showAllCodeName();
  }
  catch(ex)
  {
      alert("CardActSumDetail.js-->initForm函数中发生异常:初始化界面错误!");
  }      
}

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;
    
    if((tStartMakeDate != null || tStartMakeDate != "") && (tEndMakeDate != null || tStartMakeDate != ""))
    {
		if(dateDiff(tStartMakeDate, tEndMakeDate, "M") > 3)
		{
			alert("统计期最多为三个月！");
			return false;
		}
	}
	
	var tPremType = fm.CardPremType.value;
	
	//承保保费统计
	if(tPremType == "0")
	{
		var tStrSQL = ""
        + " select "
        + " ManageCom,CertifyCode,CertifyName, sum(double(Prem)) Prem "
        + " from LIActiveCardDetail "
        + " where 1 = 1 "
        + getWherePart("OperateType", "CardOperateType")
        + getWherePart("ActiveDate", "StartMakeDate", ">=")
        + getWherePart("ActiveDate", "EndMakeDate", "<=")
        + getWherePart("ManageCom", "ManageCom", "like")
        + " group by ManageCom,CertifyCode,CertifyName with ur";
	}
	
	//结算保费统计
	if(tPremType == "1")
	{
		var tStrSQL = ""
        + " select "
        + " ManageCom,CertifyCode,CertifyName, sum(double(Prem)) Prem "
        + " from LIActiveCardDetail a "
        + " where 1 = 1 "
        + " and exists (select 1 from LICertify where State = '04' and CardNo = a.CardNo) "
        + getWherePart("OperateType", "CardOperateType")
        + getWherePart("ActiveDate", "StartMakeDate", ">=")
        + getWherePart("ActiveDate", "EndMakeDate", "<=")
        + getWherePart("ManageCom", "ManageCom", "like")
        + " group by ManageCom,CertifyCode,CertifyName with ur";
	}
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "CardActSumDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}