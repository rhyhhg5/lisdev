<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "";
    String Content = "";
    String Priview = "PREVIEW";

    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    TransferData tTransferData = new TransferData();
  
    //接收信息
    //投保单列表
    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    String tGrpContNo = request.getParameter("GrpContNo");
    String tMissionID = request.getParameter("MissionID");   
    String tSubMissionID = request.getParameter("SubMissionID");
    
    System.out.println("GrpContNo : " + tGrpContNo);
    System.out.println("MissionID : " + tMissionID);
    System.out.println("SubMissionID : " + tSubMissionID);
    tLCGrpContSchema.setGrpContNo(tGrpContNo);
    tTransferData.setNameAndValue("GrpContNo",tGrpContNo);
    tTransferData.setNameAndValue("MissionID",tMissionID);
    tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
    String tActivityId = "0000011002";
    
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.add(tLCGrpContSchema);
    tVData.add(tTransferData);
    tVData.add(tG);
    
    GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
    boolean bl = tTbWorkFlowUI.submitData(tVData, tActivityId);
    int n = tTbWorkFlowUI.mErrors.getErrorCount();
    if(n == 0)
    {
        if (bl)
        {
            Content = " 结算成功";
            FlagStr = "Succ";
        }
        else
        {
           Content = " 结算失败";
           FlagStr = "Fail";
        }
    }
    else
    {
        String strErr = "";
        for (int i = 0; i < n; i++)
        {
            strErr += (i+1) + ": " + tTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
            System.out.println(tTbWorkFlowUI.mErrors.getError(i).errorMessage);
        }
        if (bl)
        {
            Content = " 部分结算成功,但是有如下信息: " +strErr;
            FlagStr = "Succ";
        }
        else
        {
            Content = " 结算失败，原因是: " + strErr;
            FlagStr = "Fail";
        }
    }
%>                      
<html>
<script language="javascript">
    parent.fraInterface.afterSignCont("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
