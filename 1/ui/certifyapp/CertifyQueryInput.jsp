<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="CertifyQueryInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="CertifyQueryInit.jsp"%>
    <title>卡折信息查询</title>
</head>
<body onload="initForm();" >
    <form action="./CertifyQuerySave.jsp" method=post name=fm target="fraSubmit">
        <!-- 个人保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>请输入卡折查询条件：</td></tr>
        </table>
        <table class= common align=center>
            <TR class= common>
                <TD class= title>管理机构</TD>
                <TD class= input><input class="codeNo" name=manageCom ondblclick="return showCodeList('ComCode',[this,manageComName],[0,1],null,null,null,1);" ><input class=codename name=manageComName readonly ></TD>
                <TD class= title>单证类型</TD>
                <TD class= input><input class="codeNo" name=certifyCode ondblclick="return showCodeList('CertifyCodeD',[this,certifyName],[0,1]);" ><input class=codename name=certifyName readonly ></TD>
                <TD class= title>单证状态</TD>
                <TD class= input><input class="codeNo" name=state ondblclick="return showCodeList('CertifyContState',[this,stateName],[0,1]);" ><input class=codename name=stateName readonly ></TD>
            </TR>
            <TR class= common>
                <TD class= title>结算单号</TD>
                <TD class= input><input class="common" name=payNo ></TD>
                <TD class= title>结算状态</TD>
                <TD class= input><input class="codeNo" name="SettleState" CodeData="0|^1|未结算^2|已结算" ondblclick="return showCodeListEx('SettleState',[this,SettleStateName],[0,1]);" /><input class=codename name="SettleStateName" readonly="readonly" /></TD>
                <TD class= title>导入批次号</TD>
                <TD class= input><input class="common" name="BatchNo" ></TD>
            </TR>
            <TR class= common>
                <TD class= title>业务员</TD>
                <TD class= input><input class="common" name=agentCode ></TD>
                <TD class= title>中介机构</TD>
                <TD class= input><input class="common" name=agentCom ></TD>
                <TD class= title>&nbsp;</TD>
                <TD class= input>&nbsp;</TD>
            </TR>
            <TR class= common>
                <TD class= title>单证流水号</TD>
                <TD class= input><input class="common" name=cardNo ></TD>
                <TD class= title>被保险人姓名</TD>
                <TD class= input><input class="common" name=insuredName ></TD>
                <TD class= title>&nbsp;</TD>
                <TD class= input>&nbsp;</TD>
            </TR>
            <TR class= common>
                <TD class= title>导入日期起期</TD>
                <TD class= input><input class="coolDatePicker" name="importStartDate" /></TD>
                <TD class= title>导入日期止期</TD>
                <TD class= input><input class="coolDatePicker" name="importEndDate" /></TD>
                <TD class= title>&nbsp;</TD>
                <TD class= input>&nbsp;</TD>
            </TR>
            <TR class= common>
                <TD class= title>结算日期起期</TD>
                <TD class= input><input class="coolDatePicker" name="clearStartDate" ></TD>
                <TD class= title>结算日期止期</TD>
                <TD class= input><input class="coolDatePicker" name="clearEndDate" ></TD>
                <TD class= title>&nbsp;</TD>
                <TD class= input>&nbsp;</TD>
            </TR>
            <TR class= common>
                <TD class= title>签单日期起期</TD>
                <TD class= input><input class="coolDatePicker" name="signStartDate" ></TD>
                <TD class= title>签单日期止期</TD>
                <TD class= input><input class="coolDatePicker" name="signEndDate" ></TD>
                <TD class= title>&nbsp;</TD>
                <TD class= input>&nbsp;</TD>
            </TR>
        </table>
        <input type="hidden" class="common" name="querySql" >
        <input type="button" class="cssButton" value="查询卡折信息" onclick="easyQueryClick();">
        <input type="button" class="cssButton" value="下载清单" onclick="downloadList();">
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyQuery);">
                </td>
                <td class= titleImg>卡折信息列表</td>
            </tr>
        </table>
        <div id= "divCertifyQuery" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanCertifyQueryGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
                <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
