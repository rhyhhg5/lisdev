<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：EsDocPagesSave.jsp
//程序功能：
//创建日期：2005-07-16 11:38
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.easyscan.*"%>
  <%@page import="java.sql.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//接收信息，并作校验处理。
  EsDocPagesUI tEsDocPagesUI   = new EsDocPagesUI();

  //输入参数
 ES_DOC_PAGESSet tES_DOC_PAGESSet   = new ES_DOC_PAGESSet();

  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("--EsDocPagesSave.jsp--transact:"+transact);
  
  String tGridNo[] = request.getParameterValues("EsDocPagesGridNo");  //得到序号列的所有值
  String tChk[] = request.getParameterValues("InpEsDocPagesGridChk");    //参数格式="MulLine对象名+Chk"
  String tGrid1[] = request.getParameterValues("EsDocPagesGrid1");  //得到第1列的所有值
  String tGrid2[] = request.getParameterValues("EsDocPagesGrid2");  //得到第2列的所有值
  String tGrid3[] = request.getParameterValues("EsDocPagesGrid3");  //得到第3列的所有值
  String tGrid4[] = request.getParameterValues("EsDocPagesGrid4");  //得到第4列的所有值
  String tGrid5[] = request.getParameterValues("EsDocPagesGrid5");  //得到第5列的所有值
  String tGrid6[] = request.getParameterValues("EsDocPagesGrid6");  //得到第6列的所有值
  String tGrid7[] = request.getParameterValues("EsDocPagesGrid7");  //得到第7列的所有值
  String tGrid8[] = request.getParameterValues("EsDocPagesGrid8");  //得到第8列的所有值
  String tGrid9[] = request.getParameterValues("EsDocPagesGrid9");  //得到第9列的所有值
  String tGrid10[] = request.getParameterValues("EsDocPagesGrid10");  //得到第9列的所有值
  String tGrid11[] = request.getParameterValues("EsDocPagesGrid11");  //得到第9列的所有值
  String tGrid12[] = request.getParameterValues("EsDocPagesGrid12");  //得到第9列的所有值
  String tGrid13[] = request.getParameterValues("EsDocPagesGrid13");  //得到第9列的所有值
  String tGrid14[] = request.getParameterValues("EsDocPagesGrid14");  //得到第9列的所有值
  String tGrid15[] = request.getParameterValues("EsDocPagesGrid15");  //得到第9列的所有值
  String tGrid16[] = request.getParameterValues("EsDocPagesGrid16");  //得到第9列的所有值
  
  int count = tChk.length; //得到接收到的记录数
  
  System.out.println("--EsDocPagesSave.jsp--20:" + count + ":" + tGridNo.length);
  
  
  for(int index=0; index< count; index++)
  {
  int index2=index+1;
  	System.out.println("--EsDocPagesSave.jsp--30");
    //if(tChk[index].equals("1"))
    //{
    System.out.println("--EsDocPagesSave.jsp--31");
    //选中的行
    ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
    
    tES_DOC_PAGESSchema.setPageCode(tGrid1[index]);
    tES_DOC_PAGESSchema.setManageCom(tGrid2[index]);
    tES_DOC_PAGESSchema.setOperator(tGrid3[index]);
    tES_DOC_PAGESSchema.setMakeDate(tGrid4[index]);
    tES_DOC_PAGESSchema.setMakeTime(tGrid5[index]);
    tES_DOC_PAGESSchema.setModifyDate(tGrid6[index]);
    tES_DOC_PAGESSchema.setModifyTime(tGrid7[index]);
    tES_DOC_PAGESSchema.setPageID(tGrid8[index]);
    tES_DOC_PAGESSchema.setDocID(tGrid9[index]);
    tES_DOC_PAGESSchema.setHostName(tGrid10[index]);
    tES_DOC_PAGESSchema.setPageName(tGrid11[index]);
    tES_DOC_PAGESSchema.setPageSuffix(tGrid12[index]);
    tES_DOC_PAGESSchema.setPicPathFTP(tGrid13[index]);
    tES_DOC_PAGESSchema.setPageFlag(tGrid14[index]);
    tES_DOC_PAGESSchema.setPicPath(tGrid15[index]);
    tES_DOC_PAGESSchema.setPageType(tGrid16[index]);
    System.out.println(tGrid1[index]);
    System.out.println("--EsDocManageSave.jsp--增加第:"+index2+"条记录");
    
    tES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
    //}
  }

  System.out.println("--EsDocPagesSave.jsp--40");
  
  try
  {
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.add(tES_DOC_PAGESSet);
  	tVData.add(tG);

    System.out.println("--EsDocPagesSave.jsp--before submitData");
    tEsDocPagesUI.submitData(tVData,transact);
    System.out.println("--EsDocPagesSave.jsp--after  submitData");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tEsDocPagesUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Success";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>