//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;
var  arrResult;

//提交，保存按钮对应操作
function submitForm()
{


    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    // showSubmitFrame(mDebug);
    disableButton(true);

    fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    //无论打印结果如何，都重新激活打印按钮
    showInfo.close();
    if (FlagStr == "Fail" )
    {
        //如果打印失败，展现错误信息
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    }
    else
    {	
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    }
}




//提交前的校验、计算
function beforeSubmit()
{
    //添加操作
}

//用来查询符合条件的结果
function query()
{
    // 初始化表格
    initDelApplyGrid();
    if(!verifyInput2())
    return false;
    // 书写SQL语句
    var SQLextra="";
    var SQLQuery="";
    var strsql = "";

    strsql="select distinct main.docid,iss.bussno, iss.SubType,main.NumPages ,iss.MakeDate,main.ScanOperator,"
        + "main.ManageCom, (select codename from ldcode where codetype = 'eschecktype' and code = '1') "
        + "from Es_IssueDoc iss ,es_doc_main main "
        + "where main.doccode=trim(iss.bussno) and iss.Status = '1' and iss.StateFlag = '1' "
        + getWherePart('main.ManageCom', 'ManageCom', 'like')
        + getWherePart('main.doccode', 'DocCode')
        + getWherePart('iss.BussNo', 'PrtNo','like')
        + getWherePart('iss.SubType', 'SubType')
        + getWherePart('iss.MakeDate', 'MakeDate')
        + getWherePart('main.scanoperator', 'Operator')
        + " union "
        + "select distinct main.docid,iss.bussno, iss.SubType,main.NumPages ,iss.MakeDate,main.ScanOperator,"
        + "main.ManageCom,(select codename from ldcode where codetype = 'eschecktype' and code = '2') "
        + "from Es_IssueDoc iss ,es_doc_main main "
        + "where main.doccode=trim(iss.bussno) and iss.Status = '0' and iss.StateFlag = '2'"
        + getWherePart('main.ManageCom', 'ManageCom', 'like')
        + getWherePart('main.doccode', 'DocCode')
        + getWherePart('iss.BussNo', 'PrtNo','like')
        + getWherePart('iss.SubType', 'SubType')
        + getWherePart('iss.MakeDate', 'MakeDate')
        + getWherePart('main.scanoperator', 'Operator');


    //查询SQL，返回结果字符串

    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult) {
        alert("没有符合条件的撤消数据 ！");
        return "";
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = DelApplyGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strsql;

    //设置查询起始位置
    turnPage.pageIndex       = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

    return true;
}

//判断是否选中mulline中的一行，并把mulline 相应的列的值赋给相应的页面的字段
function deleteCont()
{

    var tSel = DelApplyGrid.getSelNo();
    if( tSel == 0 || tSel == null )
    {
        alert( "请先选择一条记录。" );
        return false;
    }

    fm.Code.value = DelApplyGrid.getRowColData(tSel-1,2);
    fm.SubTpye.value = DelApplyGrid.getRowColData(tSel-1,3);


    if (!confirm("确认要撤销数据吗？"))
    {
        return;
    }


    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //fm.action = "./EsTakeBackSave.jsp";
    fm.all("Operate").value="ApplyDel";


    //	fm.fmtransact.value = "UPDATE||MAIN";
    fm.submit();
    initForm()
}

//判断mulline 是否被选中
function ApplyinitButton()
{


    var i = 0;
    var checkFlag = 0;
    var tDocCode;
    for (i=0; i<DelApplyGrid.mulLineCount; i++) {
        if (DelApplyGrid.getSelNo(i)) {
            checkFlag = DelApplyGrid.getSelNo();
            tDocCode=DelApplyGrid.getRowColData(checkFlag-1,2);
            true;
        }
    }
}

