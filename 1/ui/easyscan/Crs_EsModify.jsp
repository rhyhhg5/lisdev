<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
// 可对签单后保单进行申请的标志
String tCanDoSignContFlag = request.getParameter("CanDoSignCont");
System.out.println("tCanDoSignCont: " + request.getParameter("CanDoSignCont"));
%>
<script>
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var comcode = "<%=tGI.ComCode%>";//记录登陆机构
var Operator="<%=tGI.Operator%>";//记录操作员编码
var Flag="<%=request.getParameter("PageFlag")%>";//记录是质检员还是扫描员
var tCanDoOfSignContFlag = "<%=tCanDoSignContFlag%>";// 可对签单后保单进行申请的标志
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="EsModify.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="EsModifyInit.jsp"%>
<title>集团交叉销售质检</title>
</head>
<body onload="initForm();" >
	<form action="./EsModifySave.jsp" method=post name=fm target="fraSubmit">
		<table class= common border=0>
			 <tr>
			 	<td class= titleImg align= center>请输入集团交叉销售查询条件：</td>
			 </tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>管理机构</TD>
				<TD class= input>
						<Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > </TD>
			  <TD class= title>扫描日期</TD>
				<TD class= input>
					<Input class="coolDatePicker" name=MakeDate verify="扫描时间|date"></TD>
				<TD class= title>扫描员</TD>
				<TD class= input> 
						<Input class= common name=Operator> </TD>
			</TR>
			<TR class= common>
				<TD class= title>单证类型</TD>
				<TD class= input>
						<Input class="codeNo" name=SubType ondblclick="return showCodeList('cqapplytype',[this,SubTpyeName],[0,1]);" onkeyup="return showCodeListKey('cqapplytype',[this,SubTpyeName]),[0,1];" readonly><input class=codename name=SubTpyeName readonly=true ></TD>
				<TD class= title>单证号码</TD>
				<TD class= input> 
						<Input class= common name=DocCode > </TD>
		<!--<TD class= title>扫描审批状态</TD>-->
				<TD class= title>印刷号</TD>
				<TD class= input> 
						<Input class= common name=PrtNo >
					  <!--Input class="codeNo"  name=CheckType value='1' type ='hidden' ondblclick="return showCodeList('eschecktype',[this,CheckTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('eschecktype',[this,CheckTypeName],[0,1],null,null,null,1);" readonly><input class=codename name=CheckTypeName readonly=true value='待审批' type ='hidden'--></TD>
			</TR>
			<TR class= common id="CheckTypeTRID">
		    <TD class= title>扫描审批状态</TD>
				<TD class= input> 
				  <Input class="codeNo"  name=CheckType value='1' ondblclick="return showCodeList('eschecktype',[this,CheckTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('eschecktype',[this,CheckTypeName],[0,1],null,null,null,1);" readonly><input class=codename name=CheckTypeName readonly=true value='待审批' >
				</TD>
			</TR>
		</table>
		<INPUT VALUE="查    询" class="cssButton" TYPE=button onclick="Crs_easyQueryClick();">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>查询结果</td>
			</tr>
		</table>
		<Div id= "divQCer" style= "display: 'none'">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanApplyGrid" ></span>
					</td>
				</tr>
			</table>
			<br>
			<Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage(); getESCheckTypeName();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); getESCheckTypeName();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); getESCheckTypeName();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); getESCheckTypeName();"> 
    	</Div>
		</Div>
		<Div id= "divScaner" style= "display: 'none'">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanReApplyGrid" ></span>
					</td>
				</tr>
			</table>
			<br>
			<INPUT VALUE="首 页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾 页" class="cssButton" TYPE=button onclick="getLastPage();">
		</Div>
		<TABLE class= common>
				 <TR>
				 		<TD class=titleImg>申请原因</TD>
				 </TR>
			   <TR class= common>
			   		<TD class= common>
                <textarea name="ApplyReason" verify="申请原因|len<800" verifyorder="1" cols="110" rows="3" class="common">
                </textarea>		
            </TD>
          </TR>
				 <TR>
				 		<TD class=titleImg>申请回复</TD>
				 </TR>
			   <TR class= common>
			   		<TD class= common> 
                <textarea name="ApplyResponse" verify="申请回复|len<800" verifyorder="1" cols="110" rows="3" class="common">
                </textarea>		
            </TD>
          </TR>
    </TABLE>
		<Div id= "divApplyButton" style= "display: 'none'">
			<table class= common>
				<tr class= common>
					<td class= common align="right">
						<INPUT VALUE="扫描修改申请" name=Apply class="cssButton" TYPE=button onclick="submitForm(1);">
						<INPUT TYPE=hidden value="" name=Operate>
						<INPUT TYPE=hidden value="" name=mDocID>
						<INPUT TYPE=hidden value="" name=mDocCode>
						<INPUT TYPE=hidden value="" name=mBussTpye>
						<INPUT TYPE=hidden value="" name=mSubTpye>
					</td>
				</tr>
			</table>
		</Div>
		<Div id= "divReApplyButton" style= "display: 'none'">
			<table class= common>
				<tr class= common>
					<td class= common align="right">
						<INPUT VALUE="审批通过" name=ApplyPass class="cssButton" TYPE=button onclick="submitForm(2);">
			      <INPUT VALUE="审批不通过" name=ApplyNoPass  class="cssButton" TYPE=button onclick="submitForm(3);">
					</td>
				</tr>
			</table>
		</Div>
		<!--隐藏对象 用于传输数据-->
		<Div id= "divHiddenButton" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td class= common >
					</td>
				</tr>
			</table>
		</Div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
