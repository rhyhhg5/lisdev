var showinfo ;
var showStr = "正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面22222";
var FlagSt,content;
/**
 * 1、直接添加补扫影像件
 */
function run() {
	if(fm.Prtno.value != "" && fm.Prtno.value.substring(0,2) != "PD") {
		alert("您所输入的印刷号不属于PAD单范畴！");
		return;
	}
	
	if(fm.Prtno.value == "" || fm.Prtno.value == null || fm.Prtno.value == "null") {
		alert("请输入印刷号！");
	    return;
	}
	/*var sql = "select * from es_doc_main where doccode like '"+fm.Prtno.value+"%' and subtype in ('TB27','TB28')";
	var arrSRResult = easyExecSql(sql);
	  if (arrSRResult ==null || arrSRResult.size <2) {
		  alert("印刷号"+fm.Prtno.value+"为空，或者印刷号不属于补扫范围！");
		  return false;
	  }*/
	
	fm.flag.value = "1";
	//跳转页面，提交数据，进入后台处理
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

/**
 * 2、替换原有扫描件
 */
function run2() {
	if(fm.Prtno2.value != "" && fm.Prtno2.value.substring(0,2) != "PD") {
		alert("您所输入的印刷号不属于PAD单范畴！");
		return;
	}
	
	if(fm.Prtno2.value == "" || fm.Prtno2.value == null || fm.Prtno2.value == "null") {
		alert("请输入印刷号！");
	    return;
	}
	/*var sql = "select docid from es_doc_main where doccode like '"+fm.Prtno2.value+"%' and subtype in ('TB27','TB28')";
	var arrSRResult = easyExecSql(sql);
	if(arrSRResult.size == 1){
	alert("印刷号:"+fm.Prtno2.value+" 只有一种类型，不能进行补扫！！ ");
	}
	  if (arrSRResult ==null || arrSRResult.size != 2) {
		  alert("印刷号"+fm.Prtno2.value+"为空，或者印刷号不属于补扫范围！");
		  return false;
	  }*/
	  if(fm.Page1.value == ""||fm.Page1.value == null || fm.Page1.value == "null"){
			alert("请输入要替换的首页！");
		    return;
		}
	if(fm.Page2.value == ""||fm.Page2.value == null || fm.Page2.value == "null"){
			alert("请输入要替换的尾页！");
		    return;
		}
	var sql1 = "select numpages from es_doc_main where doccode like '"+fm.Prtno2.value+"%' and subtype = 'TB28'";
	var num = easyExecSql(sql1);
	if(num == null){
		alert("印刷号不在删除范围内或者印刷号错误！");
		return false;
	}
	var num1 = fm.Page1.value - 0;
	var num2 = fm.Page2.value - 0;
	if(num2>num[0][0] || (num2-num1)>num[0][0] || num2<num1){
		alert("尾页错误或者范围错误！");
	    return;
	}
	/*var sql2 = "select numpages from es_doc_main where doccode like '"+fm.Prtno2.value+"%' and subtype = 'TB28'";
	var page = new ExeSQL().getOneValue(sql2);
	if(fm.Page1.value>fm.page2.value || page<fm.Page2.value){
		alert("页码输入有误，请重新输入");
		return false;
	}*/
	fm.flag.value = "2";
	//跳转页面，提交数据，进入后台处理
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

/**
 * 3、删除指定页码影像件
 */
function run3() {
	if(fm.Prtno3.value != "" && fm.Prtno3.value.substring(0,2) != "PD") {
		alert("您所输入的印刷号不属于PAD单范畴！");
		return;
	}
	
	if(fm.Prtno3.value == "" || fm.Prtno3.value == null || fm.Prtno3.value == "null") {
		alert("请输入印刷号！");
	    return;
	}
	if(fm.Page3.value == ""||fm.Page3.value == null || fm.Page3.value == "null"){
		alert("请输入要删除的首页！");
	    return;
	}
	if(fm.Page4.value == ""||fm.Page4.value == null || fm.Page4.value == "null"){
		alert("请输入要删除的尾页！");
	    return;
	}
	var sql = "select * from es_doc_main where doccode like '"+fm.Prtno3.value+"%' and subtype in ('TB27','TB28')";
	var arrSRResult = easyExecSql(sql);
	  if (arrSRResult ==null) {
		  alert("印刷号"+fm.Prtno3.value+"为空!");
		  return false;
	  }
	  var sql1 = "select numpages from es_doc_main where doccode like '"+fm.Prtno3.value+"%' and subtype = 'TB28'";
		var num = easyExecSql(sql1);
		if(num == null){
			alert("印刷号不在删除范围内或者印刷号错误！");
		    return;
		}
		var num3 = fm.Page3.value - 0;
		var num4 = fm.Page4.value - 0;
		if(num4>num[0][0] || num4<num3){
			alert("尾页错误或者范围错误！");
		    return;
		}
	 /* var sql2 = "select numpages from es_doc_main where doccode like '"+fm.Prtno2.value+"%' and subtype = 'TB28'";
		var page = new ExeSQL().getOneValue(sql2);
		if(fm.Page3.value>fm.page2.value || page<fm.Page4.value){
			alert("页码输入有误，请重新输入");
			return false;
		}*/
	fm.flag.value = "3";
	//跳转页面，提交数据，进入后台处理
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
} 