<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
       <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
       <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
       <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
       <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
       <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
       <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
       <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
       <script src="PAD_Maintenance.js"></script>
	</head>
	<body>
		<form name='fm' action="PAD_MaintenanceSave.jsp" method="post" name="fm" target="fraSubmit">
			<!-- 页面头部 -->
			<div>
				<h3>PAD补扫影像件调整：</h3>
				<span style="color: red">情况：</span><br>
				<span style="color: red">1、直接添加补扫影像件</span><br>
				<span style="color: red">2、替换原有扫描件</span><br>
				<span style="color: red">3、删除指定页码影像件</span><br><br>
				<span style="color: red">页码也必须填，单页需要首尾页相同</span><br>
				<span>------------------------------------------</span><br>
			</div>
			<br>
			<!-- 页面头部结束 -->
			<!-- 第一种情况：直接添加补扫影像件 -->
			<div>
				<table>
					<tr>
						<td><img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPart1);"></td>
						<td class="titleImg">直接添加补扫影像件</td>
					</tr>
				</table>
			</div>
			<div id="divPart1">
				<table class=common>
					<tr class = common>
						<td class = title>印刷号&nbsp;<input name="Prtno" class = common></td>
					</tr>
					<tr>
						<td><input value="调  整" class=cssButton type=button onclick="return run();"></td>
					</tr>
				</table>
			</div><br>
			<!-- 第一种情况结束 -->
			<!-- 第二种情况：替换原有扫描件 -->
			<div>
				<table>
					<tr>
						<td><img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPart2);"></td>
						<td class="titleImg">替换原有扫描件</td>
					</tr>
				</table>
			</div>
			<div id="divPart2">
				<table class=common>
					<tr class = common>
						<td class = title>印刷号&nbsp;<input name="Prtno2" class = common></td>
					</tr>
					<tr class = common>
						<td class = title>替换从&nbsp;<input style="width:50px;" name="Page1" class = common>&nbsp;页，到&nbsp;<input style="width:50px;" name="Page2" class = common>&nbsp;页</td>
					</tr>
					<tr>
						<td><input value="调  整" class=cssButton type=button onclick="return run2();"></td>
					</tr>
				</table>
			</div><br>
			<!-- 第二种情况结束 -->
			<!-- 第三种情况： 删除指定页码影像件 -->
			<div>
				<table>
					<tr>
						<td><img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPart3);"></td>
						<td class="titleImg">删除指定页码影像件</td>
					</tr>
				</table>
			</div>
			<div id="divPart3">
				<table class=common>
					<tr class = common>
						<td class = title>印刷号&nbsp;<input name="Prtno3" class = common></td>
					</tr>
					<tr class = common>
						<td class = title>删除从&nbsp;<input style="width:50px;" name="Page3" class = common>&nbsp;页，到&nbsp;<input style="width:50px;" name="Page4" class = common>&nbsp;页</td>
					</tr>
					<tr>
						<td><input value="调  整" class=cssButton type=button onclick="return run3();"></td>
					</tr>
				</table>
			</div><br>
			<!-- 第三种情况结束 -->
			<!-- 设置flag标签，区分是哪种业务形式 -->
			<input name="flag" type="hidden">
			<!-- flag标签结束 -->
		</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>