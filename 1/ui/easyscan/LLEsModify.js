//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var PolNo;
var manageCom;

//扫描审核权限判断

function rol ()
{
	  var bfalg = null	
           var i = 0;
           var checkFlag = 0;

           for (i=0; i<ApplyGrid.mulLineCount; i++) {
                         if (ApplyGrid.getSelNo(i)) {
                                checkFlag = ApplyGrid.getSelNo();
                             break;
                            }
            }
           var tDocID=ApplyGrid.getRowColData(checkFlag-1,1);
        //团单不用判断
       var sbt ="select subtype from ES_DOC_MAIN where docid ="+tDocID+" ";
       var st = easyExecSql(sbt);
       var type =st[0][0]   ;
    	// alert(type);
       if(null!=type&&'LP01'==type){
                     var sql = " select count(*) from llcase ll , es_doc_main edm where edm.DocId="+tDocID+" and (ll.caseno = edm.DocCode or ll.rgtno=edm.DocCode) and ll.handler='"+fm.Operate.value+"' ";
                      var rs = easyExecSql(sql);
                      var  v =rs[0][0];
                     //  alert(v);
                        if(v==0){
                                alert("您不是此案件的处理人，无权审核此扫描件！");
                                    return false   ;
                         }else{
                         	return true;
                         }

       
        
     } else if (null!=type&&'LP03'==type) {
        var sql = " select count(*) from llprepaidclaim ll , es_doc_main edm where edm.DocId="+tDocID+" and ll.prepaidno = edm.DocCode and ll.handler='"+fm.Operate.value+"' ";
        var rs = easyExecSql(sql);
        var  v =rs[0][0];
        if(v==0) {
           alert("您不是此次预付的处理人，无权审核此扫描件！");
           return false;
        }else{
           return true;
        }
     } else {
        return true ;
     }
 }


//提交，保存按钮对应操作
function submitForm(stateflag)
{
	//alert("QQQQQQQQQQQ   " +stateflag);	
	//stateflag=1  扫描修改申请
	//stateflag=2  审核通过
	//stateflag=3  审核不通过
	if(stateflag=="1"){ 
	   //alert("扫描修改申请");
			fm.all("Operate").value="Apply";
			if(fm.all("ApplyReason").value==""){
				alert("请在[申请原因]中录入扫描修改的原因!");
				return false;
				}
		}
	else if(stateflag=="2"){
	    //  alert("========================");
	         //此处进行权限判断
            
            	if(!rol()){
	           		return false ;
            	}
            	fm.all("Operate").value="ApplyPass";
				//alert("222222222222222222");
			
		}
	else if(stateflag=="3"){
			//此处进行权限判断
           		if(!rol()){
           		  return false ;
           		}
        	    fm.all("Operate").value="ApplyNoPass";
				if(fm.all("ApplyResponse").value==""){
					alert("请在[申请回复]中录入审批不通过的原因!");
					return false;
					}
			//alert("3333333333333333");	
			
		}
	else{
		alert("错误的状态");
		return false;
		}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  // showSubmitFrame(mDebug);
  disableButton(true);
  //alert("fm.submit(); //提交");
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < ContGrid.mulLineCount; i++ )
	{
		if( ContGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.all("printButton").disabled=true;
	fm.submit();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//无论打印结果如何，都重新激活打印按钮
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
function easyQueryClick()
{
	resetButton();
	// 初始化表格
	var SQLextra="";
	var SQLQuery="";
	var check="";
	check=fm.all("CheckType").value;
	if(fm.DocCode.value == null||fm.DocCode.value == "")
	{
		alert("单证号码为必录项!");
		return false;
	}
	if(check==""){
			check="9";
		}
		
	SQLQuery="'"+check+"'";
	if(check=="9"){
			//SQLQuery="(case when a.doccode in (select bussno from es_issuedoc) then (case when a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='1') then '1' else (case when a.doccode in (select bussno from es_issuedoc where status='0' and stateflag='2') then '2' else (case when a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='3') then '3' else '' end) end) end) else '0' end) ";
            SQLQuery = "''";
		}
	if(check=="0"){
			//SQLextra=" and ((a.doccode not in (select bussno from es_issuedoc)) or (a.doccode in (select bussno from es_issuedoc where status='1' and result is not null)))";
            SQLextra = " and (ei.BussNo is null or (ei.Status = '1' and ei.Result is not null)) "
                + " and (not exists (select 1 from es_issuedoc tei where tei.BussNo = edm.DocCode and (tei.Status = '0' or (tei.Status = '1' and tei.Result is null)))) ";
		}
	else if(check=="1" && fm.DocCode.value.substr(0, 1)=="Y"){
	//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='1')";
          SQLextra = " and ei.Status = '1' and ei.StateFlag = '1' and exists (select 1 from llprepaidclaim where prepaidno=edm.doccode and handler='"+fm.upUserCode.value+"') ";
	}
	else if(check=="1" ){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='1')";
            SQLextra = " and ei.Status = '1' and ei.StateFlag = '1' and exists (select 1 from llcase where (caseno=edm.doccode or rgtno=edm.doccode or caseno=substr(edm.doccode,1,length(edm.doccode)-1) ) and handler='"+fm.upUserCode.value+"' and mngcom like '"+ fm.ManageCom.value +"%')" ;
		}	
	else if(check=="2"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='0' and stateflag='2')";			
            SQLextra = " and ei.Status = '0' and ei.StateFlag = '2' ";
		}
	else if(check=="3"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='3')";			
            SQLextra = " and ei.Status = '1' and ei.StateFlag = '3' ";
		}	
	initApplyGrid();
	var strsql = "";
	//if(fm.PageFlag.value=="1"){
	// if(fm.SubType.value=="LP01"){
	// 	strsql=" right join llcase ll on ll.caseno = edm.DocCode and ll.handler='"+fm.Operate.value+"' ";
	//　}
　　//　}  此处意图不清，考虑？？
	
	
	
	
	/*strSQL =  "select distinct a.docid,a.doccode,b.subtypename,a.numpages,a.makedate,a.scanoperator,a.managecom , "
				 + SQLQuery
	       + " checktype from es_doc_main a,ES_DOC_DEF b,es_doc_relation c "
	       + "where a.busstype='TB' and a.subtype = b.subtype and a.docID = c.DocID "
	       //+ "and a.doccode in (select missionprop2 t from lwmission where activityid in('0000001001','0000002001','0000002004', '0000001150', '0000002006') "
	       //+" union select missionprop1 t from lwmission where activityid in ('0000001100','0000001099', '0000002099')) "
	       + getWherePart('a.ManageCom', 'ManageCom', 'like')
	       + getWherePart('a.doccode', 'DocCode')
	       + getWherePart('c.BussNo', 'PrtNo','like')
	       + getWherePart('a.SubType', 'SubType')
	       + getWherePart('a.MakeDate', 'MakeDate')
	       + getWherePart('a.scanoperator', 'Operator')
	       + SQLextra
	       + "";*/
    strSQL = " select distinct DocID, DocCode, SubTypeName, NumPages, MakeDate, ScanOperator, ManageCom, "
        + SQLQuery +",1,1,promptoperator,checkoperator,'','',typename "
        + " from ( "
        + " select edm.docid, edm.doccode, edd.subtypename, edm.numpages, edm.makedate, "
        + " edm.scanoperator, edm.managecom, ei.Status Status, ei.StateFlag StateFlag, ei.Result Result,ei.promptoperator,ei.checkoperator,"
        + " (case when ei.Status = '1' and ei.StateFlag = '1' then '待审批' when ei.Status = '0' and ei.StateFlag = '2' then '审批通过' "
        + " when ei.Status = '1' and ei.StateFlag = '3' then '审批不通过' else '可申请' end) typename "
        + " from es_doc_main edm "
        + " inner join es_doc_def edd on edd.SubType = edm.SubType "
        + " inner join es_doc_relation edr on edr.DocID = edm.DocID and edm.subtype=edr.subtype"
        + " left join es_issuedoc ei on ei.BussNo = edm.DocCode and edm.subtype=ei.subtype"
        +strsql
        + " where 1 = 1 "
        //+ getWherePart('edm.ManageCom', 'ManageCom', 'like')
        + getWherePart('edm.doccode', 'DocCode')
        + getWherePart('edr.BussNo', 'PrtNo','like')
        + getWherePart('edm.SubType', 'SubType')
        + getWherePart('edm.MakeDate', 'MakeDate')
        + getWherePart('edm.scanoperator', 'Operator')
        + " and (edm.State is null or edm.State != '03'  ) and edm.busstype ='LP'"  //已发送前置机待外包录入的扫描件不能修改
       // + " and exists (select 1 from LLInqApply where SurveyNo=edm.doccode and LocFlag='1' and inqdept like '"+ fm.ManageCom.value +"%'"
       // + " or edm.ManageCom like '"+ fm.ManageCom.value +"%')"
        + SQLextra
        + " ) as temp ";
   	turnPage2.pageDivName = "divPage2";
   	turnPage2.queryModal(strSQL, ApplyGrid);
	
	getESCheckTypeName();
}

function getESCheckTypeName()
{
    if (fm.all("CheckType").value=='9') {
      return;
    }
	for(var i = 0; i < ApplyGrid.mulLineCount; i++)
	{
	  var sql = "select CodeName('eschecktype', '" + ApplyGrid.getRowColDataByName(i, "ESCcheckType") + "') from dual ";
	  var rs = easyExecSql(sql);
	  if(rs)
	  {
	    ApplyGrid.setRowColDataByName(i, "ESCheckTypeName", rs[0][0]);
	  }
	}
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}
function ApplyinitButton()
{
 
 resetButton();
 var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<ApplyGrid.mulLineCount; i++) {
    if (ApplyGrid.getSelNo(i)) { 
       checkFlag = ApplyGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	
  	var tDocID=ApplyGrid.getRowColData(checkFlag-1,1);
  	//alert(tDocID);
  	var tDocCode=ApplyGrid.getRowColData(checkFlag-1,2);
  
  	var tSubTpyeName=ApplyGrid.getRowColData(checkFlag-1,3);
  	
  	var arrRet = easyExecSql("select SubType, BussType from es_doc_def where SubTypeName = '"+tSubTpyeName+"'");
  	var tSubTpye = "";
    var tBussTpye = "";
    if(arrRet)
    {
        tSubTpye = arrRet[0][0];
        tBussTpye = arrRet[0][1];
    }
  	var tFlag=ApplyGrid.getRowColData(checkFlag-1,8);
  	//赋其他值
    fm.all("mDocID").value=tDocID;
    fm.all("mDocCode").value=tDocCode;
    fm.all("mBussTpye").value=tBussTpye;
    fm.all("mSubTpye").value=tSubTpye;
		//判断stateflag
		if(tFlag=="0"){//可申请
				//alert(Flag);
				//initButton
				fm.all("Apply").disabled=false;
        fm.all("ApplyPass").disabled=true;
        fm.all("ApplyNoPass").disabled=true;
        //赋值
        fm.all("ApplyReason").value="";
        fm.all("ApplyResponse").value="";
        
			}
		else if(tFlag=="1"){//待审批
				fm.all("Apply").disabled=true;
        fm.all("ApplyPass").disabled=false;
        fm.all("ApplyNoPass").disabled=false;
        
        var strSQL="select issuedesc from ES_Issuedoc where bussno='"+tDocCode+"' and busstype='" + tBussTpye + "' and SubType='"+tSubTpye+"'";
				arrResult = easyExecSql(strSQL);
				if(arrResult != null)
				{
					fm.all("ApplyReason").value=arrResult[0][0];
        
        fm.all("ApplyResponse").value="";
			
				}
        //fm.all('ApplyReason').readOnly=true;        
			}
		else if(tFlag=="2"){//审批通过
				fm.all("Apply").disabled=true;
        fm.all("ApplyPass").disabled=true;
        fm.all("ApplyNoPass").disabled=true;
        arrResult=easyExecSql("select issuedesc,checkcontent from ES_Issuedoc where bussno='"+tDocCode+"' and busstype='" + tBussTpye + "' and SubType='"+tSubTpye+"'",1,0);
        fm.all("ApplyReason").value=arrResult[0][0];
        fm.all("ApplyResponse").value=arrResult[0][1];
			}
		else if(tFlag=="3"){//审批不通过
				fm.all("Apply").disabled=false;
        fm.all("ApplyPass").disabled=false;
        fm.all("ApplyNoPass").disabled=true;
        arrResult=easyExecSql("select issuedesc,checkcontent from ES_Issuedoc where bussno='"+tDocCode+"' and busstype='" + tBussTpye + "' and SubType='"+tSubTpye+"'",1,0);
        fm.all("ApplyReason").value=arrResult[0][0];
        fm.all("ApplyResponse").value=arrResult[0][1];				
			}
  	//fm.all("QDocID").value=tQDocID;
  	//fm.all("QDocCode").value=tQDocCode;
  	
  }
  else {
    alert("请先选择一条保单信息！"); 
  }

}
function ReApplyinitButton()
{
 resetButton();
 var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<ReApplyGrid.mulLineCount; i++) {
    if (ReApplyGrid.getSelNo(i)) { 
      checkFlag = ReApplyGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	var tDocID=ReApplyGrid.getRowColData(checkFlag-1,1);
  	var tDocCode=ReApplyGrid.getRowColData(checkFlag-1,2);
  	var tBussTpye=ReApplyGrid.getRowColData(checkFlag-1,3);
  	var tSubTpye=ReApplyGrid.getRowColData(checkFlag-1,4);
  
  	fm.all("QDocID").value=tDocID
  	fm.all("QDocCode").value=tDocCode
  	fm.all("QBussTpye").value=tBussTpye
  	fm.all("QSubTpye").value=tSubTpye
  	fm.all("UnpassReason").value=tUnpassReason;
  	if(tScanOpeState==3){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=false;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  	if(tScanOpeState==2){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=true;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=true;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=false;
  		}
  	if(tScanOpeState==1){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=true;
         fm.all("Scanerdelete").disabled=true;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  	if(tScanOpeState==""||tScanOpeState==null||tScanOpeState=="null"){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=false;
         fm.all("ScanermodifyOK").disabled=true;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}
function resetButton()
{
	fm.all("Apply").disabled=true;
  fm.all("ApplyPass").disabled=true;
  fm.all("ApplyNoPass").disabled=true;
  //赋值
  fm.all("ApplyReason").value="";
  fm.all("ApplyResponse").value="";

}
function temp()
{
resetButton();

resetButton();
 var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<ApplyGrid.mulLineCount; i++) {
    if (ApplyGrid.getSelNo(i)) { 
      checkFlag = ApplyGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1);
    var	ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
    var MissionID =GrpGrid.getRowColData(checkFlag - 1, 4);
    var SubMissionID =GrpGrid.getRowColData(checkFlag - 1, 5);
		var PolApplyDate = GrpGrid.getRowColData(checkFlag - 1, 3);
    var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;

    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);

    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    if (strReturn == "1") 
    	if(type=="1")
    	{
    		window.open("./ContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures);        
 			}
 			if(type=="2")
 			{
 				window.open("./GrpContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures); 
 			}
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

function showPic()
{
	if(fm.all("QDocID").value==""||fm.all("QDocCode").value==""||fm.all("QBussTpye").value==""||fm.all("QSubTpye").value=="")
	{
		alert("请选择一条记录！");
		return;
		}
		var cDocID=fm.all("QDocID").value;
  	var cDocCode = fm.all("QDocCode").value;
    var	cBussTpye = fm.all("QBussTpye").value;
    var cSubTpye =fm.all("QSubTpye").value;
    
    window.open("./QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        

}
function QCsubmit(SubFlag)
{
	//SubFlag=1  质检通过
	//SubFlag=2  质检建议修改
	//SubFlag=3  质检建议删除
  //SubFlag=4  扫描员修改中
	//SubFlag=5  扫描员删除
	//SubFlag=6  扫描员修改完毕
	//
	if(fm.all("QDocID").value==""||fm.all("QDocCode").value==""||fm.all("QBussTpye").value==""||fm.all("QSubTpye").value=="")
	{
		alert("请选择一条记录！");
		return;
		}
	var tFlag="1";
 if(SubFlag==1){
 	fm.all("QOperate").value="QCPASS";
 	}
else if(SubFlag==2){
  fm.all("QOperate").value="QCMODIFY";
	}
else if(SubFlag==3){
	if(fm.all("UnpassReason").value==""){
		alert("请输入未通过原因！");
		return;
		}
  fm.all("QOperate").value="QCDELETE";
  }
else if(SubFlag==4){
  fm.all("QOperate").value="SCANMODIFY";
	}
else if(SubFlag==5){
  fm.all("QOperate").value="SCANDELETE";
	}
else if(SubFlag==6){
  fm.all("QOperate").value="SCANMODIFYOK";
	}
else if(SubFlag==7){
  fm.all("QOperate").value="SCANDELETEOK";
	}
else{
	fm.all("QOperate").value="";
	tFlag="0";
	}
if(tFlag==1){
	submitForm();
	}
else{
	alert("不允许该操作");
	}
}

// 禁用界面上的按钮，防止重复录入。
// true：禁用，false：启用
function disableButton(tFlag)
{
    fm.all("Apply").disabled = tFlag;
    fm.all("ApplyPass").disabled = tFlag;
    fm.all("ApplyNoPass").disabled = tFlag;
}