<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html> 
<%
//程序名称：EsDocPages.jsp
//程序功能：
//创建日期：2005-07-15 11:02:00
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="EsDocPagesSort.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EsDocPagesSortInit.jsp"%>
</head>
<body  onload="initForm();easyqueryClick();">
  <form action="./EsDocPagesSave.jsp" method=post name=fm target="fraSubmit">
  	<table>
			<tr>
				<td class=titleImg>
					扫描件基本信息
				</td>
			</tr>
			<tr>
				<td class=common>
					单证类型
				</td>
				<td>
					<Input class= common name=tSubType readonly value=<%=request.getParameter("SubType")%>>
				</td>
				<td class=common>
					单证号码
				</td>
				<td>
					<Input class= common name=doccode readonly value=<%=request.getParameter("prtNo")%>>
					<Input  type = hidden name=docid value=<%=request.getParameter("docid")%>>
				</td>				
			</tr>
	  </table>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEsDocPages1);">
    		</td>
    		<td class= titleImg>
        		 扫描页信息
       	</td>   		 
    	</tr>
    </table>

    <Div  id= "divEsDocPages1" style= "display: ''" align=center>
    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanEsDocPagesGrid">
	  				</span> 
			    </td>
				</tr>
		</table>
    </Div>
 <table>
 	<tr>
 		<td>
 			&nbsp;<INPUT VALUE="修改" TYPE=button class=cssButton onclick="saveUpdate()"> 
 	  </td>
 	</tr>
</table>

		  <!-- 信息（列表） -->
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type= hidden name ="ContraItemNo">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
