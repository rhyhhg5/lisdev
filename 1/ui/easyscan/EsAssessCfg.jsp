<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var comcode = "<%=tGI.ComCode%>";//记录登陆机构
var Operator="<%=tGI.Operator%>";//记录操作员编码
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="EsAssessCfg.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="EsAssessCfgInit.jsp"%>
<title>质检</title>
</head>
<body onload="initForm();" >
	<form action="./EsAssessCfgSave.jsp" method=post name=fm target="fraSubmit">
		<table class= common border=0>
			 <tr>
			 	<td class= titleImg align= center>请输入查询条件：</td>
			 </tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>管理机构</TD>
				<TD class= input>
						<Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > </TD>
			  <TD class= title>用户编码</TD>
				<TD class= input>
						<Input class="common" name=UserCode></TD>
				<TD class= title>用户姓名</TD>
				<TD class= input> 
						<Input class= common name=UserName > </TD>
			</TR>
		</table>
		<INPUT VALUE="查    询" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>质检信息</td>
			</tr>
		</table>
		<Div id= "divUser" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanUserGrid" ></span>
					</td>
				</tr>
			</table>
			<br>
			<INPUT VALUE="首 页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾 页" class="cssButton" TYPE=button onclick="getLastPage();">
		</Div>
		<Div id= "divEsUser" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanEsUserGrid" ></span>
					</td>
				</tr>
			</table>
			<br>
		</Div>
		<Div id= "divUserButton" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td class= common>
						<INPUT VALUE="保存" name=QCershowpic class="cssButton" TYPE=button onclick="submitForm();">
					</td>
				</tr>
			</table>
		</Div>
		<!--隐藏对象 用于传输数据-->
		<Div id= "divHiddenButton" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td class= common >
						<INPUT  name=mUserCode  TYPE=hidden value="">
					</td>
				</tr>
			</table>
		</Div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
