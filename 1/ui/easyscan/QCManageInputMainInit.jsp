<%
//程序名称：QCManagerInputMainInit.jsp
//程序功能：
//创建日期：2005-12-30 11:10:36
//创建人  ：Dongjb程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
String strOperator=globalInput.Operator;
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
		fm.reset();
	}
	catch(ex)
	{
		alert("QCManageInputMainInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
	try
	{
		initInpBox();
		initScanerGrid();
		initQCerGrid();
    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
    if(Flag==1)
    {
    	divQCer.style.display="";
    	divQCerButton.style.display="";
     }
     if(Flag==2)
     {
     	divScaner.style.display="";
     	divScanerButton.style.display="";
     	fm.all('UnpassReason').readOnly=true;
     	}

	}
	catch(re)
	{
		alert("QCManageInputMainInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//定义为全局变量，提供给displayMultiline使用
var QCerGrid;
// 保单信息列表的初始化
function initQCerGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="docid";
		iArray[1][1]="100px";
		iArray[1][2]=20;
		iArray[1][3]=3;

		iArray[2]=new Array();
		iArray[2][0]="单证号码";
		iArray[2][1]="80px";           
		iArray[2][2]=60;            	
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="单证大类";
		iArray[3][1]="0px";           
		iArray[3][2]=60;            	
		iArray[3][3]=3;

		iArray[4]=new Array();
		iArray[4][0]="类型代码";
		iArray[4][1]="0px";
		iArray[4][2]=20;
		iArray[4][3]=3;
		
		iArray[5]=new Array();
		iArray[5][0]="单证类型";
		iArray[5][1]="70px";
		iArray[5][2]=60;
		iArray[5][3]=0;
		

		iArray[6]=new Array();
		iArray[6][0]="扫描员";
		iArray[6][1]="60px";
		iArray[6][2]=40;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="扫描管理机构";
		iArray[7][1]="80px";
		iArray[7][2]=80;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="最后修改日期";
		iArray[8][1]="80px";
		iArray[8][2]=60;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="审核状态代码";
		iArray[9][1]="0px";
		iArray[9][2]=40;
		iArray[9][3]=3;
		
		iArray[10]=new Array();
		iArray[10][0]="审核状态";
		iArray[10][1]="70px";
		iArray[10][2]=40;
		iArray[10][3]=0;
		
		iArray[11]=new Array();
		iArray[11][0]="处理建议";
		iArray[11][1]="0px";
		iArray[11][2]=40;
		iArray[11][3]=3;
		
		iArray[12]=new Array();
		iArray[12][0]="未通过原因";
		iArray[12][1]="180px";
		iArray[12][2]=200;
		iArray[12][3]=0;
		
		iArray[13]=new Array();
		iArray[13][0]="处理状态代码";  
		iArray[13][1]="40px";
		iArray[13][2]=40;
		iArray[13][3]=3;
		
		iArray[14]=new Array();
		iArray[14][0]="处理状态";  
		iArray[14][1]="70px";
		iArray[14][2]=40;
		iArray[14][3]=0;
    
		QCerGrid = new MulLineEnter( "fm" , "QCerGrid" );
		//这些属性必须在loadMulLine前
		QCerGrid.mulLineCount = 0;
		QCerGrid.displayTitle = 1;
		QCerGrid.hiddenPlus = 1;
		QCerGrid.hiddenSubtraction = 1;
		QCerGrid.canSel = 1;
		QCerGrid.canChk = 0;
		QCerGrid.selBoxEventFuncName ="QCerinitButton";
		QCerGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}
var ScanerGrid;
// 保单信息列表的初始化
function initScanerGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="docid";
		iArray[1][1]="100px";
		iArray[1][2]=20;
		iArray[1][3]=3;

		iArray[2]=new Array();
		iArray[2][0]="单证号码";
		iArray[2][1]="80px";           
		iArray[2][2]=60;            	
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="单证大类";
		iArray[3][1]="0px";           
		iArray[3][2]=60;            	
		iArray[3][3]=3;

		iArray[4]=new Array();
		iArray[4][0]="类型代码";
		iArray[4][1]="0px";
		iArray[4][2]=20;
		iArray[4][3]=3;
		
		iArray[5]=new Array();
		iArray[5][0]="单证类型";
		iArray[5][1]="70px";
		iArray[5][2]=60;
		iArray[5][3]=0;
		

		iArray[6]=new Array();
		iArray[6][0]="扫描员";
		iArray[6][1]="60px";
		iArray[6][2]=40;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="扫描管理机构";
		iArray[7][1]="80px";
		iArray[7][2]=80;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="最后修改日期";
		iArray[8][1]="80px";
		iArray[8][2]=60;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="审核状态代码";
		iArray[9][1]="0px";
		iArray[9][2]=40;
		iArray[9][3]=3;
		
		iArray[10]=new Array();
		iArray[10][0]="审核状态";
		iArray[10][1]="70px";
		iArray[10][2]=40;
		iArray[10][3]=0;
		
		iArray[11]=new Array();
		iArray[11][0]="处理建议";
		iArray[11][1]="0px";
		iArray[11][2]=40;
		iArray[11][3]=3;
		
		iArray[12]=new Array();
		iArray[12][0]="未通过原因";
		iArray[12][1]="180px";
		iArray[12][2]=200;
		iArray[12][3]=0;
		
		iArray[13]=new Array();
		iArray[13][0]="处理状态代码";  
		iArray[13][1]="40px";
		iArray[13][2]=40;
		iArray[13][3]=3;
		
		iArray[14]=new Array();
		iArray[14][0]="处理状态";  
		iArray[14][1]="70px";
		iArray[14][2]=40;
		iArray[14][3]=0;

		ScanerGrid = new MulLineEnter( "fm" , "ScanerGrid" );
		//这些属性必须在loadMulLine前
		ScanerGrid.mulLineCount = 0;
		ScanerGrid.displayTitle = 1;
		ScanerGrid.hiddenPlus = 1;
		ScanerGrid.hiddenSubtraction = 1;
		ScanerGrid.canSel = 1;
		ScanerGrid.canChk = 0;
		ScanerGrid.selBoxEventFuncName ="ScanerinitButton";
		ScanerGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>