<%
//程序名称：QCManagerInputMainInit.jsp
//程序功能：
//创建日期：2005-12-30 11:10:36
//创建人  ：Dongjb程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
String strOperator=globalInput.Operator;
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
		fm.reset();
	}
	catch(ex)
	{
		alert("QCManageInputMainInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
	try
	{
		initInpBox();
		initUserGrid();
		initEsUserGrid();
    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}

	}
	catch(re)
	{
		alert("QCManageInputMainInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//定义为全局变量，提供给displayMultiline使用
var UserGrid;
// 保单信息列表的初始化
function initUserGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="管理机构";
		iArray[1][1]="50px";
		iArray[1][2]=20;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="用户编码";
		iArray[2][1]="50px";           
		iArray[2][2]=60;            	
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="用户姓名";
		iArray[3][1]="0px";           
		iArray[3][2]=60;            	
		iArray[3][3]=0;
    
		UserGrid = new MulLineEnter( "fm" , "UserGrid" );
		//这些属性必须在loadMulLine前
		UserGrid.mulLineCount = 0;
		UserGrid.displayTitle = 1;
		UserGrid.hiddenPlus = 1;
		UserGrid.hiddenSubtraction = 1;
		UserGrid.canSel = 1;
		UserGrid.canChk = 0;
		UserGrid.selBoxEventFuncName ="easyQueryClick2";
		UserGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}
var EsUserGrid;
// 保单信息列表的初始化
function initEsUserGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="UserCode";
		iArray[1][1]="0px";
		iArray[1][2]=20;
		iArray[1][3]=0;
    
		iArray[2]=new Array();
		iArray[2][0]="单证类型编码";
		iArray[2][1]="20px";           
		iArray[2][2]=80;            	
		iArray[2][3]=2;
		iArray[2][4]="esapplytype";
		
		iArray[3]=new Array();
		iArray[3][0]="单证类型名称";
		iArray[3][1]="0px";           
		iArray[3][2]=20;            	
		iArray[3][3]=0;
		
		
		EsUserGrid = new MulLineEnter( "fm" , "EsUserGrid" );
		//这些属性必须在loadMulLine前
		EsUserGrid.mulLineCount = 0;
		EsUserGrid.displayTitle = 1;
		EsUserGrid.canSel = 0;
		EsUserGrid.canChk = 0;
		EsUserGrid.hiddenPlus=0;
		EsUserGrid.hiddenSubtraction=0;
		EsUserGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>