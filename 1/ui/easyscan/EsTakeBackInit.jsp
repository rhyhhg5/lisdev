<%
//程序名称：QCManagerInputMainInit.jsp
//程序功能：
//创建日期：2005-12-30 11:10:36
//创建人  ：Dongjb程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
String strOperator=globalInput.Operator;
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
		fm.reset();
	}
	catch(ex)
	{
		alert("EsModifyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
	try
	{
		initInpBox();
		initDelApplyGrid();
		fm.all('ManageCom').value = <%=strManageCom%>;
	}
	catch(re)
	{
		alert("EsModifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//定义为全局变量，提供给displayMultiline使用
var DelApplyGrid;
function initDelApplyGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="docid";
		iArray[1][1]="10px";
		iArray[1][2]=20;
		iArray[1][3]=3;

		iArray[2]=new Array();
		iArray[2][0]="单证号码";
		iArray[2][1]="80px";           
		iArray[2][2]=60;            	
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="单证类型";
		iArray[3][1]="80px";           
		iArray[3][2]=60;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="页数";
		iArray[4][1]="50px";
		iArray[4][2]=20;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="扫描日期";
		iArray[5][1]="70px";
		iArray[5][2]=60;
		iArray[5][3]=0;
		

		iArray[6]=new Array();
		iArray[6][0]="扫描操作员";
		iArray[6][1]="60px";
		iArray[6][2]=40;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="管理机构";
		iArray[7][1]="80px";
		iArray[7][2]=80;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="审批状态";
		iArray[8][1]="80px";
		iArray[8][2]=60;
		iArray[8][3]=3;
		iArray[8][4]="eschecktype";
		iArray[8][21]="ESCcheckType";
		
		iArray[9]=new Array();
		iArray[9][0]="审核状态代码";
		iArray[9][1]="0px";
		iArray[9][2]=40;
		iArray[9][3]=3;
		
		iArray[10]=new Array();
		iArray[10][0]="审核状态";
		iArray[10][1]="70px";
		iArray[10][2]=40;
		iArray[10][3]=3;
		
		iArray[11]=new Array();
		iArray[11][0]="处理建议";
		iArray[11][1]="0px";
		iArray[11][2]=40;
		iArray[11][3]=3;
		
		iArray[12]=new Array();
		iArray[12][0]="未通过原因";
		iArray[12][1]="180px";
		iArray[12][2]=200;
		iArray[12][3]=3;
		
		iArray[13]=new Array();
		iArray[13][0]="处理状态代码";  
		iArray[13][1]="40px";
		iArray[13][2]=40;
		iArray[13][3]=3;
		
		iArray[14]=new Array();
		iArray[14][0]="处理状态";  
		iArray[14][1]="70px";
		iArray[14][2]=40;
		iArray[14][3]=3;
		
		iArray[15]=new Array();
		iArray[15][0]="审批状态";  
		iArray[15][1]="70px";
		iArray[15][2]=40;
		iArray[15][3]=0;
		iArray[15][21]="ESCheckTypeName";
		

    
		DelApplyGrid = new MulLineEnter( "fm" , "DelApplyGrid" );
		//这些属性必须在loadMulLine前
		DelApplyGrid.mulLineCount = 0;
		DelApplyGrid.displayTitle = 1;
		DelApplyGrid.hiddenPlus = 1;
		DelApplyGrid.hiddenSubtraction = 1;
		DelApplyGrid.canSel = 1;
		DelApplyGrid.canChk = 0;
		DelApplyGrid.selBoxEventFuncName ="ApplyinitButton";
	//	DelApplyGrid.chkBoxEventFuncName = "displayCheckBoxValue1";
		DelApplyGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>