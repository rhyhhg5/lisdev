<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：EsPicDownloadShowMain.jsp
//程序功能：扫描单证下载查看框架
//创建日期：2005-08-25
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

<%

//	String tprtNo = request.getParameter("prtNo");
//	String tBussType = request.getParameter("BussType");
//	String tSubType = request.getParameter("SubType");
//  String tBussNoType = request.getParameter("BussNoType");

//	String BussNoType = "";
//	String BussType = "";
//	String SubType = "";
//	
//	SSRS tSSRS = new SSRS();
//	ExeSQL tExeSQL = new ExeSQL();
//	String sql = 	"select BussNoType, BussType, SubType " + 
//					"from ES_DOC_RELATION " + 
//					"where bussNo='" + tWorkNo + "'";
//	
//	tSSRS = tExeSQL.execSQL(sql);
//	if(tSSRS.getMaxRow() > 0)
//	{
//		BussNoType 	= tSSRS.GetText(1, 1);
//		BussType 	= tSSRS.GetText(1, 2);
//		SubType 	= tSSRS.GetText(1, 3);
//	}	
//	

%>
<html>
<head>
<title>扫描件查看</title>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>   
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<SCRIPT>	
	var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	window.resizeTo(intPageWidth,intPageHeight);
	window.moveTo(-1, -1);
	window.focus();	
		
	var initWidth = 0;
	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数
	
	function queryScanType() 
	{
    	var strSql = "select code1, codename, codealias from ldcode1 where codetype='scaninput'";
    	var a = easyExecSql(strSql);

    	return a;
  	} 
</SCRIPT>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="0">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="no" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="300,*" cols="*">
	    <frame id="fraPic" name="fraPic" scrolling="auto" src="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo=<%=request.getParameter("prtNo")%>&BussNoType=<%=request.getParameter("BussNoType")%>&BussType=<%=request.getParameter("BussType")%>&SubType=<%=request.getParameter("SubType")%>">
	    <frame id="fraInterface" name="fraInterface" scrolling="auto" src="EsPicDownloadShow.jsp?prtNo=<%=request.getParameter("prtNo")%>&BussNoType=<%=request.getParameter("BussNoType")%>&BussType=<%=request.getParameter("BussType")%>&SubType=<%=request.getParameter("SubType")%>&docid=<%=request.getParameter("docid")%>">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
