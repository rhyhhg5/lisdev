<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalDownloadSave.jsp
//程序功能：保单下载
//创建日期：2005-08-26
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.easyscan.ProposalDownloadUI"%>
<%
String FlagStr = "";
String Content = "";
String tOperate = "";
String sOutXmlPath = application.getRealPath("");
System.out.println("sOutXmlPath  ::" + sOutXmlPath);
//获得mutline中的数据信息
int nIndex = 0;
String tLCContGrids[] = request.getParameterValues("ContGridNo");
String tPrtNo[] = request.getParameterValues("ContGrid1");
String tContNo[] = request.getParameterValues("ContGrid2");
String tManageCom[] = request.getParameterValues("ContGrid4");
String tChecks[] = request.getParameterValues("InpContGridChk");
String tDownloadType = request.getParameter("DownloadType");
String tEXESql = request.getParameter("EXESql");
String tToday=PubFun.getCurrentDate();
System.out.println("操作日期是："+tToday);
//获得session中的人员信息
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
//操作对象及容器
ProposalDownloadUI tProposalDownloadUI = new ProposalDownloadUI();
VData tVData = new VData();
TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath);
LCContPrintSet tLCContPrintSet = new LCContPrintSet();
//判断下载模式
//-------如果DownloadType为1，则下载模式为只下载选中的----操作开始--------
if(tDownloadType.equals("1"))
{
LCContPrintSet mLCContPrintSet = new LCContPrintSet();
System.out.println("下载模式为只下载选中的");
//首先循环打印选中的个单

for(nIndex = 0; nIndex < tChecks.length; nIndex++ )
{
	//If this line isn't selected, continue，如果没有选中当前行，则继续
	if( !tChecks[nIndex].equals("1") )
	{
		continue;
	}
	//将数据放入合同保单集合
	LCContPrintSchema tLCContPrintSchema=new LCContPrintSchema();
	tLCContPrintSchema.setPrtNo(tPrtNo[nIndex]);
	System.out.println("印刷号是："+tPrtNo[nIndex]);
	tLCContPrintSchema.setOtherNo(tContNo[nIndex]);
	System.out.println("保单号是："+tContNo[nIndex]);
	tLCContPrintSchema.setManageCom(tManageCom[nIndex]);
	System.out.println("管理机构是："+tManageCom[nIndex]);
	mLCContPrintSet.add(tLCContPrintSchema);
}
if (mLCContPrintSet.size()>0){
for (int i = 1; i <= mLCContPrintSet.size(); i++) {
            LCContPrintDB tLCContPrintDB = new LCContPrintDB();
            tLCContPrintDB.setPrtNo(mLCContPrintSet.get(i).getPrtNo());
            LCContPrintSet cLCContPrintSet = tLCContPrintDB.query();
            for (int j = 1; j <= cLCContPrintSet.size(); j++) {
                tLCContPrintSet.add(cLCContPrintSet.get(j).getSchema());
            }
        }
}
}
//-------如果DownloadType为1，则下载模式为只下载选中的----操作结束--------
//-------如果DownloadType为2，则下载模式为下载全部的----操作开始--------
if(tDownloadType.equals("2"))
{
LCContPrintDB tLCContPrintDB= new LCContPrintDB();
System.out.println("下载模式为下载所有符合条件的");
tLCContPrintSet = tLCContPrintDB.executeQuery(tEXESql);
	System.out.println("EXESql:"+tEXESql);
}
//-------如果DownloadType为2，则下载模式为下载全部的----操作结束--------
//-------如果DownloadType为其他，则下载模式为错误的----操作开始--------
if(tDownloadType=="")
{
FlagStr="Fail";
Content="dsafdsafsa";
}
//-------如果DownloadType为其他，则下载模式为错误的----操作结束--------

//如果没有失败，则返回打印成功
if(tLCContPrintSet.size()>0)
{
		try
	{
  VData vData = new VData();
	vData.add(tG);
	vData.addElement(tLCContPrintSet);
	vData.addElement(tTransferData);
		if (!tProposalDownloadUI.submitData(vData, "Download")){
			Content = "保单下载失败，原因是:" + tProposalDownloadUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
	}
	catch(Exception ex)
	{
	}
}
else{
	Content = "生成下载文件失败！";
	FlagStr = "Fail";
}
if (!FlagStr.equals("Fail"))
{
	Content = "生成下载文件成功，请在弹出窗口中右键点击“downloadpic”，选择“目标另存为”，将生成文件另存到本机！";
	FlagStr = "Succ";
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	var w=(intPageWidth-300)/2;
	var h=(intPageHeight-200)/2;
	var Succ="Succ";
	var Fail="Fail";
	if (<%=FlagStr%> == Succ){
	window.open("./PicDownload.jsp?&prtNo=downloadpic","print", "height=50,width=300,top="+h+",left="+w+",toolbar=yes,status=no,menubar=no,resizable=no,z-look=no");
}
</script>
</html>
