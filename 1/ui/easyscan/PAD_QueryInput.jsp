<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期： 2018-05-07 09-22-40
		//创建人  ：renwei
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="PAD_QueryInput.js"></script>
		<%@include file="PAD_QueryInputInit.jsp"%>

		<script>
		<%-- var ManageCom = "<%=tGI.ManageCom%>"; --%>
		<%-- var tOperator = "<%=tGI.Operator%>";  --%>
		</script>
	</head>
	
	<body onload="initForm();">
		<form action="PAD_QueryInputSave.jsp" method=post name=fm target="fraSubmit">
			<div id="divQueryInput"  style="display:hidden">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divInfo);">
						</td>
						<td class=titleImg>
							查询条件
						</td>
					</tr>
				</table>
				<table class=common align='center'>
					<TR class=common>
						<TD class=title>
							业务号：<Input class=common name="printNo">
						</TD>
						<!-- <TD class=input>
							<Input class=common name="printNo">
						</TD> -->
					</TR>
				</table>
				<!-- <table>
					<input type=hidden id="fmtransact" name="fmtransact">
					<td class=button width="10%" align=left>
						<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
					</td>
				</table> -->
				<span id="spanCode"
					style="display: none; position:absolute; slategray"></span>

				<!-- 查询结果部分 -->
				<table >
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;" 	OnClick="showPage(this,divInfo);">
						</td>
						<td class=titleImg>
							影像件信息:<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
						</td>
						<input type=hidden id="fmtransact" name="fmtransact">
						<input type=hidden id="DOCCODE" name="DOCCODE">
					</tr>
				</table>

			</div>
			<!-- 信息（列表） -->
			<Div id="divInfo" align = center style="display:''" align=left>
				<table class=common >
					<tr class=common align = center>
						<td text-align:left colSpan=1>
							<span id="spanInfoGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<Div id="divButton" align=center style="display: '' ">
				<table>
					<tr>
						<td class=button width="10%" align=left>
							<INPUT CLASS=cssButton VALUE="更新最新" TYPE=button onclick="InfoUpdate()">
							<INPUT CLASS=cssButton VALUE="删除" TYPE=button onclick="InfoDelete()">
						</td>
					</tr>
				</table>
			</Div>
		</form>
	</body>
</html>
