//程序名称：ScanList.js
//程序功能：扫描件查询
//创建日期：2006-11-20 9:13
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

var timeStyle = "hh:mm:ss";


//=====================================初始化区域==========================================
//初始化输入框
function initInpBox()
{
  fm.ManageCom.value = manageCom;
  
  var sql = "  select Name "
            + "from LDCom "
            + "where comCode = '" + manageCom + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.ManageComName.value = rs[0][0];
  }
  
  if(tContNo == null || tContNo == "" || tContNo == "null")
  {
    fm.BussType.value = "BQ";
    fm.BussTypeName.value = "保全单证";
  }
  else
  {
    fm.ManageCom.value = "";
    fm.ManageComName.value = "";
  }
  
  fm.PrintDate.value = curDate;
}


//=====================================事件响应区域==========================================

//查清清单
function queryList()
{
  if(tContNo == null || tContNo == "" || tContNo == "null")
  {
    queryScanList();
  }
  else
  {
    queryScanListByCont();
  }
}

//无保单查询扫描清单
function queryScanList()
{
  if(!checkQuery())
  {
    return false;  
  }
  
  scanOperatorPart = "";
  if(fm.ScanOperator.value != "")
  {
    scanOperatorPart = "  and exists "
                      + "   (select 1 from LDUser "
                      + "   where UserCode = a.ScanOperator "
                      + "      and UserName like '%" + fm.ScanOperator.value + "%') ";
  }
  
  var tableAndCondintion = "from ES_Doc_Main a, ES_Doc_Relation b "
				    + "where a.docID = b.docID "
				    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
				    + scanOperatorPart
				    + getWherePart("b.BussType", "BussType")
				    + getWherePart("a.MakeDate", "StartDate", ">=")
				    + getWherePart("a.MakeDate", "EndDate", "<=")
				    + getWherePart("a.MakeTime ", "StartTime", ">=")
				    + getWherePart("a.MakeTime ", "EndTime", "<=");
				    
  var sql = "select a.DocID, (select count(1) from Es_Doc_Pages where DocID=a.DocID), (select Name from LDCom where ComCode = a.ManageCom), "
	          + "   b.BussNo, a.MakeDate, a.MakeTime, "
	          + "   (select UserName from LDUser where UserCode = a.ScanOperator), "
	          + "   (select Name from LDCom x, LDUser y "
	          + "   where x.ComCode = y.ComCode and y.UserCode=a.ScanOperator) "
	          + tableAndCondintion
				    + "order by a.docID ";
  fm.sql.value = sql;
  
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, ScanGrid);
	
	if(ScanGrid.mulLineCount == 0)
	{
	  alert("没有符合条件的数据");
	  return;
	}
	
	setOtherInputBox(tableAndCondintion);
}

//关联保单查询扫描清单
function queryScanListByCont()
{
  if(!checkQuery())
  {
    return false;  
  }
  
  var contField = "";
  var edorTable = "";
  if(tAppObj == "I")
  {
    contField = "ContNo";
    edorTable = "LPEdorItem";
  }
  else
  {
    contField = "GrpContNo";
    edorTable = "LPGrpEdorItem";
  }
  
  scanOperatorPart = "";
  if(fm.ScanOperator.value != "")
  {
    scanOperatorPart = "  and exists "
                      + "   (select 1 from LDUser "
                      + "   where UserCode = a.ScanOperator "
                      + "      and UserName like '%" + fm.ScanOperator.value + "%') ";
  }
  
  var tableAndCondintion = "from ES_Doc_Main a, ES_Doc_Relation b "
				    + "where a.docID = b.docID "
				    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
				    + scanOperatorPart
				    + getWherePart("b.BussType", "BussType")
				    + getWherePart("a.MakeDate", "StartDate", ">=")
				    + getWherePart("a.MakeDate", "EndDate", "<=")
				    + getWherePart("a.MakeTime ", "StartTime", ">=")
				    + getWherePart("a.MakeTime ", "EndTime", "<=");
				    
  var sql = "select a.DocID a, (select count(1) from ES_Doc_Pages where DocID = a.DocID), "
            + "   (select Name from LDCom where ComCode = a.ManageCom), "
	          + "   b.BussNo, a.MakeDate, a.MakeTime, "
	          + "   (select UserName from LDUser where UserCode = a.ScanOperator), "
	          + "   (select Name from LDCom x, LDUser y "
	          + "   where x.ComCode = y.ComCode and y.UserCode=a.ScanOperator) "
	          + tableAndCondintion
	          + "   and exists (select 1 from " + edorTable + " where " + contField + " = '" + tContNo + "' and EdorNo = b.BussNo) " //保全 
				    
				    + "union all "
				    
				    + "select a.DocID a, (select count(1) from ES_Doc_Pages where DocID = a.DocID), "
				    + "   (select Name from LDCom where ComCode = a.ManageCom), "
	          + "   b.BussNo, a.MakeDate, a.MakeTime, "
	          + "   (select UserName from LDUser where UserCode = a.ScanOperator), "
	          + "   (select Name from LDCom x, LDUser y "
	          + "   where x.ComCode = y.ComCode and y.UserCode=a.ScanOperator) "
	          + tableAndCondintion
	          + "   and exists (select 1 from LLClaimPolicy where CaseNo = b.Bussno and " + contField + " = '" + tContNo + "' ) " //理赔 
				    
				    + "union all "
				    
				    + "select a.DocID a, (select count(1) from ES_Doc_Pages where DocID = a.DocID), "
				    + "   (select Name from LDCom where ComCode = a.ManageCom), "
	          + "   b.BussNo, a.MakeDate, a.MakeTime, "
	          + "   (select UserName from LDUser where UserCode = a.ScanOperator), "
	          + "   (select Name from LDCom x, LDUser y "
	          + "   where x.ComCode = y.ComCode and y.UserCode=a.ScanOperator) "
	          + tableAndCondintion
	          + "   and b.BussNo = '" + tPrtNo + "' " //契约
				    + "order by a "
				    ;
				    
  fm.sql.value = sql;
  
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, ScanGrid);
	
	if(ScanGrid.mulLineCount == 0)
	{
	  alert("没有符合条件的数据");
	  return ;
	}
	
	fm.SumBuss.value = turnPage2.queryAllRecordCount;
	fm.SumPages.value = turnPage2.queryAllRecordCount;
}

//查询后给"总扫描页数", "  受理件数 ", "打印时间"赋值
function setOtherInputBox(tableAndCondintion)
{
  //受理件数
	sql = "select count(distinct b.BussNo) "
	      + tableAndCondintion;
  rs = easyExecSql(sql);
  if(rs)
  {
    fm.SumBuss.value = rs[0][0];
  }
  
  //总扫描页数
  var sql = "select count(1) "
	          + tableAndCondintion;
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.SumPages.value = rs[0][0];
  }
}

//打印查询到的扫描件清单
function prinScantList()
{
  //fm.action = "ScanListPrintMain.jsp";
  if(!checkPrint())
  {
    return false;
  }
  
  //var tUrl = "../task/FrameMain.jsp?InitPage=../easyscan/ScanListPrint.jsp&sql=" + fm.sql.value;
  //window.open(tUrl, "ScanPrint");
	fm.target = "_blank";
	fm.submit();
}

//=====================================校验区域==========================================

//校验打印打印清单操作的合法性
function checkPrint()
{
  if(fm.sql.value == "")
  {
    alert("请先查询");
    return false;
  }
  
  if(ScanGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  return true;
}

//校验查询条件是否合法
function checkQuery()
{
  var startTime = fm.StartTime.value;
  var endTime = fm.EndTime.value;
  
  if(startTime != "")
  {
    startTime = getStandardTime(fm.StartTime.value);
  }
  if(endTime != "")
  {
    endTime = getStandardTime(fm.EndTime.value);
  }
  
  if(startTime == null || endTime == null)
  {
    return false;
  }
  
  fm.StartTime.value = startTime;
  fm.EndTime.value = endTime;
  
  return true;
}

//得到cTime的标准时间型数据"hh:mm:ss"
//时分秒以splitOpertar分割（为方便录入，本方法支持splitOpertar="."）
//cTime中splitOpertar转换为":"后进行校验
//若时分秒只有一位，则返回自动扩张为两位，如"1:2:3"将自动装换为"01:02:03"
//若不是是时间型数据或不符合时间规则，则返回null
function getStandardTime(cTime, cSplitOpertar)
{
  var time;
  
  if(cSplitOpertar == null || cSplitOpertar == "" || cSplitOpertar == "null")
  {
    time = cTime.split(":");
  }
  else
  {
    time = cTime.split(splitOpertar);
  }
  
  if(time.length != 3)
  {
    alert(cTime + "不是时间型数据" + timeStyle);
    return null;
  }
  
  if(time[0] >= 24)
  {
    alert("时间应小于于24:00:00。");  
    return null;
  }
  
  if(time[1] > 59 || time[2] > 59)
  {
    alert("分秒都不能大于59");
    return null;
  }
  
  if(!isInteger(time[0] || !isInteger(time[1]) || !isInteger(time[2])))
  {
    alert("时间只能包含数字");
    return false;
  }
  
  for(var i = 0; i < time.length; i++)
  {
    if(time[i].length < 2)
    {
      time[i] = "0" + time[i];
    }
  }
  
  return time[0] + ":" + time[1] + ":" + time[2];
}