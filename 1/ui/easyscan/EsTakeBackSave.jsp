<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：QCManagerInputMainSave.jsp
	//程序功能：
	//创建日期：2005-12-30 11:10:36
	//创建人  ：Dongjb程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%
	String FlagStr = "";
	String Content = "";

	VData tVData = new VData();
	Es_IssueDocSchema tEs_IssueDocSchema = new Es_IssueDocSchema();
	LCContSchema tLCContSchema = new LCContSchema();
	//获取操作方法
	String tOperate = request.getParameter("Operate");
	

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("Operate", tOperate);
	

	//获得session中的人员信息
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	
	//操作对象及容器  封装页面相应字段的值到相应的容器中
	System.out.println(request.getParameter("Code"));
	tEs_IssueDocSchema.setBussNo(request.getParameter("Code"));
	tEs_IssueDocSchema.setSubType(request.getParameter("SubTpye"));
	System.out.println(request.getParameter("SubTpye"));
	
	
	tVData.add(tEs_IssueDocSchema);
	tVData.add(tG);

	EsTakeBackUI tEsTakeBackUI = new EsTakeBackUI();
	//执行后台操作 
	try {
		if (!tEsTakeBackUI.submitData(tVData, tOperate)) {
			Content = "操作失败，原因是:"
			+ tEsTakeBackUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}

		// 对于某些可以直接进行修改的扫描件，进行自动审核通过。
		} catch (Exception ex) {
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果没有失败，则返回打印成功
	if (!FlagStr.equals("Fail")) 
	{
		Content += "操作成功! ";
		FlagStr = "Succ";

	}
%>
<html>
	<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
