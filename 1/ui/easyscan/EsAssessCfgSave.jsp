<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QCManagerInputMainSave.jsp
//程序功能：
//创建日期：2005-12-30 11:10:36
//创建人  ：Dongjb程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%
String FlagStr = "";
String Content = "";

//获得input页面中取得单证号码和未通过原因
String tUserCode = request.getParameter("mUserCode");

//获得session中的人员信息
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//操作对象及容器
LDScanCheckUserSet tLDScanCheckUserSet=new LDScanCheckUserSet();
String sNo[]					= request.getParameterValues("EsUserGridNo");
String sUserCode[]    = request.getParameterValues("EsUserGrid1");
String sSubType[]		  = request.getParameterValues("EsUserGrid2"); 
int nIndex;
String tOperate="";
if(sNo==null){
		LDScanCheckUserSchema tLDScanCheckUserSchema = new LDScanCheckUserSchema();
		tLDScanCheckUserSchema.setUserCode(tUserCode);
		tLDScanCheckUserSchema.setSubType("");
		tLDScanCheckUserSet.add(tLDScanCheckUserSchema);
		tOperate="DEL";
}
else{
	for(nIndex = 0; nIndex < sNo.length; nIndex ++ ){
		LDScanCheckUserSchema tLDScanCheckUserSchema = new LDScanCheckUserSchema();
		tLDScanCheckUserSchema.setUserCode(tUserCode);
		tLDScanCheckUserSchema.setSubType(sSubType[nIndex]);
		tLDScanCheckUserSet.add(tLDScanCheckUserSchema);
	}
	tOperate="UPDATE";
}
EsAssessUI tEsAssessUI = new EsAssessUI();
VData vData = new VData();

	vData = new VData();
	vData.add(tG);
	vData.add(tLDScanCheckUserSet);
	//执行后台操作
	try
	{
		if (!tEsAssessUI.submitData(vData, tOperate)){
			Content = "操作失败，原因是:" + tEsAssessUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}


//如果没有失败，则返回打印成功
if (!FlagStr.equals("Fail"))
{
	Content = "操作成功! ";
	FlagStr = "Succ";
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>