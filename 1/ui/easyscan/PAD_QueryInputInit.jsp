<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("页面初始化失败！");
  }
}
function initInpBox()
{ 
	initInfoGrid();
	divPage.style.display="";   
	showAllCodeName();
}

var InfoGrid;
function initInfoGrid() 
{                               
  var iArray = new Array();
  try 
  {
	iArray[0]=new Array();
	iArray[0][0]="序号";         		
	iArray[0][1]="30px";         	    
	iArray[0][3]=0;
	
	iArray[1]=new Array();
	iArray[1][0]="业务号"; 
    iArray[1][1]="40px";  
    iArray[1][3]=0;  
	
    iArray[2]=new Array();
    iArray[2][0]="DocId";         		
    iArray[2][1]="30px";         	    
    iArray[2][3]=0;         		
    
    iArray[3]=new Array();
    iArray[3][0]="页数";         	  	
    iArray[3][1]="10px";          	
    iArray[3][2]=200;            	
    iArray[3][3]=0;              	
    
    iArray[4]=new Array();
    iArray[4][0]="入机日期";         	
    iArray[4][1]="40px";            	
    iArray[4][2]=200;            		 
    iArray[4][3]=0;              		 
    
    iArray[5]=new Array();
    iArray[5][0]="入机时间";         	  
    iArray[5][1]="40px";            	
    iArray[5][2]=200;            			
    iArray[5][3]=0;              			
    
    InfoGrid = new MulLineEnter("fm", "InfoGrid"); 
  	
    InfoGrid.mulLineCount = 0;
    InfoGrid.displayTitle = 1;
    InfoGrid.locked = 1;
    InfoGrid.canSel = 1;
    InfoGrid.canChk = 0;
    InfoGrid.hiddenSubtraction = 1;
    InfoGrid.hiddenPlus = 1;
   
    InfoGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
