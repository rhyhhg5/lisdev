//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var PolNo;
var manageCom;

//提交，保存按钮对应操作
function submitForm(stateflag)
{
	//stateflag=1  扫描修改申请
	//stateflag=2  审核通过
	//stateflag=3  审核不通过
	if(stateflag=="1"){
	
	    fm.all("Operate").value="Apply";
	    var tsubtype=fm.mSubTpye.value;
			
	    var isIssueType = false;
		isIssueType = checkIssueType();
    
        // 加入签单后的保单不能进行修改申请。
        if(tCanDoOfSignContFlag != "1")
        {
            if(checkSignOfCont()){
               return false;
            }               
            if("TB01"==tsubtype){
			   if(!checkTB01()){
			      return false;
			   }
			}
			if("TB04"==tsubtype){
			   if(!checkTB04()){
			      return false;
			   }
			}
        }
        // ---------------------------
        
		if(fm.all("IssueType").value==""||!isIssueType){
			alert("请选择扫描修改的申请原因!");
			return false;
		}
		if(fm.all("IssueType").value=="8"){
			var tReason = fm.all("ApplyReason").value;
			if(tReason==""||trim(tReason).length<6){
			    alert("请详细描述所要修改项目的内容。");
				return false;
			}
		}
			
		}
	else if(stateflag=="2"){
			fm.all("Operate").value="ApplyPass";
		}
	else if(stateflag=="3"){
			fm.all("Operate").value="ApplyNoPass";
			if(fm.all("ApplyResponse").value==""){
				alert("请在[申请回复]中录入审批不通过的原因!");
				return false;
				}
		}
	else{
		alert("错误的状态");
		return false;
		}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  // showSubmitFrame(mDebug);
  disableButton(true);
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < ContGrid.mulLineCount; i++ )
	{
		if( ContGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.all("printButton").disabled=true;
	fm.submit();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//无论打印结果如何，都重新激活打印按钮
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
function easyQueryClick()
{
	resetButton();
	// 初始化表格
	var SQLextra="";
	var SQLQuery="";
	var check="";
	check=fm.all("CheckType").value;
	//alert(check);
	if(check==""){
			check="9";
		}
		
	SQLQuery="'"+check+"'";
	if(check=="9"){
			//SQLQuery="(case when a.doccode in (select bussno from es_issuedoc) then (case when a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='1') then '1' else (case when a.doccode in (select bussno from es_issuedoc where status='0' and stateflag='2') then '2' else (case when a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='3') then '3' else '' end) end) end) else '0' end) ";
            SQLQuery = " case when temp.Status = '1' and temp.StateFlag = '1' then '1' "
                     + " when temp.Status = '0' and temp.StateFlag = '2' then '2' "
                     + " when temp.Status = '1' and temp.StateFlag = '3' then '3' "
                     + " else '0' end ";
		}
	if(check=="0"){
			//SQLextra=" and ((a.doccode not in (select bussno from es_issuedoc)) or (a.doccode in (select bussno from es_issuedoc where status='1' and result is not null)))";
            SQLextra = " and (ei.BussNo is null or (ei.Status = '1' and ei.Result is not null)) "
                + " and (not exists (select 1 from es_issuedoc tei where tei.BussNo = edm.DocCode and (tei.Status = '0' or (tei.Status = '1' and tei.Result is null)))) ";
		}
	else if(check=="1"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='1')";
            SQLextra = " and ei.Status = '1' and ei.StateFlag = '1' ";
		}
	else if(check=="2"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='0' and stateflag='2')";			
            SQLextra = " and ei.Status = '0' and ei.StateFlag = '2' ";
		}
	else if(check=="3"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='3')";			
            SQLextra = " and ei.Status = '1' and ei.StateFlag = '3' ";
		}	
	initApplyGrid();

	var strSQL = "";
	
	/*strSQL =  "select distinct a.docid,a.doccode,b.subtypename,a.numpages,a.makedate,a.scanoperator,a.managecom , "
				 + SQLQuery
	       + " checktype from es_doc_main a,ES_DOC_DEF b,es_doc_relation c "
	       + "where a.busstype='TB' and a.subtype = b.subtype and a.docID = c.DocID "
	       //+ "and a.doccode in (select missionprop2 t from lwmission where activityid in('0000001001','0000002001','0000002004', '0000001150', '0000002006') "
	       //+" union select missionprop1 t from lwmission where activityid in ('0000001100','0000001099', '0000002099')) "
	       + getWherePart('a.ManageCom', 'ManageCom', 'like')
	       + getWherePart('a.doccode', 'DocCode')
	       + getWherePart('c.BussNo', 'PrtNo','like')
	       + getWherePart('a.SubType', 'SubType')
	       + getWherePart('a.MakeDate', 'MakeDate')
	       + getWherePart('a.scanoperator', 'Operator')
	       + SQLextra
	       + "";*/
    strSQL = " select distinct DocID, DocCode, SubTypeName, NumPages, MakeDate, ScanOperator, ManageCom, "
        + SQLQuery
        + " from ( "
        + " select edm.docid, edm.doccode, edd.subtypename, edm.numpages, edm.makedate, "
        + " edm.scanoperator, edm.managecom, ei.Status Status, ei.StateFlag StateFlag, ei.Result Result "
        + " from es_doc_main edm "
        + " inner join es_doc_def edd on edd.SubType = edm.SubType "
        + " inner join es_doc_relation edr on edr.DocID = edm.DocID "
        + " left join es_issuedoc ei on ei.BussNo = edm.DocCode "
        + " where 1 = 1 "
        + getWherePart('edm.ManageCom', 'ManageCom', 'like')
        + getWherePart('edm.doccode', 'DocCode')
        + getWherePart('edr.BussNo', 'PrtNo','like')
        + getWherePart('edm.SubType', 'SubType')
        + getWherePart('edm.MakeDate', 'MakeDate')
        + getWherePart('edm.scanoperator', 'Operator')
        + "   and (edm.State is null or edm.State != '03') "  //已发送前置机待外包录入的扫描件不能修改
        + " and edm.busstype not like 'LP%'"
        + SQLextra
        + " ) as temp ";

	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, ApplyGrid);
	
	getESCheckTypeName();
}

function getESCheckTypeName()
{
	for(var i = 0; i < ApplyGrid.mulLineCount; i++)
	{
	  var sql = "select CodeName('eschecktype', '" + ApplyGrid.getRowColDataByName(i, "ESCcheckType") + "') from dual ";
	  var rs = easyExecSql(sql);
	  if(rs)
	  {
	    ApplyGrid.setRowColDataByName(i, "ESCheckTypeName", rs[0][0]);
	  }
	}
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}
function ApplyinitButton()
{
 
 resetButton();
 var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<ApplyGrid.mulLineCount; i++) {
    if (ApplyGrid.getSelNo(i)) { 
      checkFlag = ApplyGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	var tDocID=ApplyGrid.getRowColData(checkFlag-1,1);
  	var tDocCode=ApplyGrid.getRowColData(checkFlag-1,2);
  	var tSubTpyeName=ApplyGrid.getRowColData(checkFlag-1,3);
  	var arrRet = easyExecSql("select SubType, BussType from es_doc_def where SubTypeName = '"+tSubTpyeName+"'");
  	var tSubTpye = "";
    var tBussTpye = "";
    if(arrRet)
    {
        tSubTpye = arrRet[0][0];
        tBussTpye = arrRet[0][1];
    }
  	var tFlag=ApplyGrid.getRowColData(checkFlag-1,8);
  	//赋其他值
    fm.all("mDocID").value=tDocID;
    fm.all("mDocCode").value=tDocCode;
    fm.all("mBussTpye").value=tBussTpye;
    fm.all("mSubTpye").value=tSubTpye;
		//判断stateflag
		if(tFlag=="0"){//可申请
				//alert(Flag);
				//initButton
				fm.all("Apply").disabled=false;
        fm.all("ApplyPass").disabled=true;
        fm.all("ApplyNoPass").disabled=true;
        //赋值
        fm.all("ApplyReason").value="";
        fm.all("ApplyReason").value="";
        fm.all("IssueType").value="";
        fm.all("IssueTypeName").value="";
        fm.all("IssueType1").value="";
        fm.all("IssueType1Name").value="";
        
			}
		else if(tFlag=="1"){//待审批
				fm.all("Apply").disabled=true;
        fm.all("ApplyPass").disabled=false;
        fm.all("ApplyNoPass").disabled=false;
        arrResult=easyExecSql("select issuedesc,IssueType,codename('esmodifytype',IssueType) from ES_Issuedoc where bussno='"+tDocCode+"' and busstype='" + tBussTpye + "' and SubType='"+tSubTpye+"'",1,0);
        fm.all("ApplyReason").value=arrResult[0][0];
        fm.all("ApplyResponse").value="";
        fm.all("IssueType").value=arrResult[0][1];
        fm.all("IssueTypeName").value=arrResult[0][2];
        fm.all("IssueType1").value=arrResult[0][1];
        fm.all("IssueType1Name").value=arrResult[0][2];
        //fm.all('ApplyReason').readOnly=true;        
			}
		else if(tFlag=="2"){//审批通过
				fm.all("Apply").disabled=true;
        fm.all("ApplyPass").disabled=true;
        fm.all("ApplyNoPass").disabled=true;
        arrResult=easyExecSql("select issuedesc,checkcontent,IssueType,codename('esmodifytype',IssueType) from ES_Issuedoc where bussno='"+tDocCode+"' and busstype='" + tBussTpye + "' and SubType='"+tSubTpye+"'",1,0);
        fm.all("ApplyReason").value=arrResult[0][0];
        fm.all("ApplyResponse").value=arrResult[0][1];
        fm.all("IssueType").value=arrResult[0][2];
        fm.all("IssueTypeName").value=arrResult[0][3];
        fm.all("IssueType1").value=arrResult[0][2];
        fm.all("IssueType1Name").value=arrResult[0][3];
			}
		else if(tFlag=="3"){//审批不通过
				fm.all("Apply").disabled=false;
        fm.all("ApplyPass").disabled=false;
        fm.all("ApplyNoPass").disabled=true;
        arrResult=easyExecSql("select issuedesc,checkcontent,IssueType,codename('esmodifytype',IssueType) from ES_Issuedoc where bussno='"+tDocCode+"' and busstype='" + tBussTpye + "' and SubType='"+tSubTpye+"'",1,0);
        fm.all("ApplyReason").value=arrResult[0][0];
        fm.all("ApplyResponse").value=arrResult[0][1];	
        fm.all("IssueType").value=arrResult[0][2];
        fm.all("IssueTypeName").value=arrResult[0][3];
        fm.all("IssueType1").value=arrResult[0][2];
        fm.all("IssueType1Name").value=arrResult[0][3];			
			}
  	//fm.all("QDocID").value=tQDocID;
  	//fm.all("QDocCode").value=tQDocCode;
  	
  }
  else {
    alert("请先选择一条保单信息！"); 
  }

}
function ReApplyinitButton()
{
 resetButton();
 var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<ReApplyGrid.mulLineCount; i++) {
    if (ReApplyGrid.getSelNo(i)) { 
      checkFlag = ReApplyGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	var tDocID=ReApplyGrid.getRowColData(checkFlag-1,1);
  	var tDocCode=ReApplyGrid.getRowColData(checkFlag-1,2);
  	var tBussTpye=ReApplyGrid.getRowColData(checkFlag-1,3);
  	var tSubTpye=ReApplyGrid.getRowColData(checkFlag-1,4);
  
  	fm.all("QDocID").value=tDocID
  	fm.all("QDocCode").value=tDocCode
  	fm.all("QBussTpye").value=tBussTpye
  	fm.all("QSubTpye").value=tSubTpye
  	fm.all("UnpassReason").value=tUnpassReason;
  	if(tScanOpeState==3){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=false;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  	if(tScanOpeState==2){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=true;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=true;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=false;
  		}
  	if(tScanOpeState==1){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=true;
         fm.all("Scanerdelete").disabled=true;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  	if(tScanOpeState==""||tScanOpeState==null||tScanOpeState=="null"){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=false;
         fm.all("ScanermodifyOK").disabled=true;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}
function resetButton()
{
	fm.all("Apply").disabled=true;
  fm.all("ApplyPass").disabled=true;
  fm.all("ApplyNoPass").disabled=true;
  //赋值
  fm.all("ApplyReason").value="";
  fm.all("ApplyResponse").value="";
  fm.all("IssueType").value="";
  fm.all("IssueTypeName").value="";
  fm.all("IssueType1").value="";
  fm.all("IssueType1Name").value="";

}
function temp()
{
resetButton();

resetButton();
 var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<ApplyGrid.mulLineCount; i++) {
    if (ApplyGrid.getSelNo(i)) { 
      checkFlag = ApplyGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1);
    var	ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
    var MissionID =GrpGrid.getRowColData(checkFlag - 1, 4);
    var SubMissionID =GrpGrid.getRowColData(checkFlag - 1, 5);
		var PolApplyDate = GrpGrid.getRowColData(checkFlag - 1, 3);
    var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;

    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);

    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    if (strReturn == "1") 
    	if(type=="1")
    	{
    		window.open("./ContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures);        
 			}
 			if(type=="2")
 			{
 				window.open("./GrpContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures); 
 			}
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

function showPic()
{
	if(fm.all("QDocID").value==""||fm.all("QDocCode").value==""||fm.all("QBussTpye").value==""||fm.all("QSubTpye").value=="")
	{
		alert("请选择一条记录！");
		return;
		}
		var cDocID=fm.all("QDocID").value;
  	var cDocCode = fm.all("QDocCode").value;
    var	cBussTpye = fm.all("QBussTpye").value;
    var cSubTpye =fm.all("QSubTpye").value;
    
    window.open("./QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        

}
function QCsubmit(SubFlag)
{
	//SubFlag=1  质检通过
	//SubFlag=2  质检建议修改
	//SubFlag=3  质检建议删除
  //SubFlag=4  扫描员修改中
	//SubFlag=5  扫描员删除
	//SubFlag=6  扫描员修改完毕
	//
	if(fm.all("QDocID").value==""||fm.all("QDocCode").value==""||fm.all("QBussTpye").value==""||fm.all("QSubTpye").value=="")
	{
		alert("请选择一条记录！");
		return;
		}
	var tFlag="1";
 if(SubFlag==1){
 	fm.all("QOperate").value="QCPASS";
 	}
else if(SubFlag==2){
  fm.all("QOperate").value="QCMODIFY";
	}
else if(SubFlag==3){
	if(fm.all("UnpassReason").value==""){
		alert("请输入未通过原因！");
		return;
		}
  fm.all("QOperate").value="QCDELETE";
  }
else if(SubFlag==4){
  fm.all("QOperate").value="SCANMODIFY";
	}
else if(SubFlag==5){
  fm.all("QOperate").value="SCANDELETE";
	}
else if(SubFlag==6){
  fm.all("QOperate").value="SCANMODIFYOK";
	}
else if(SubFlag==7){
  fm.all("QOperate").value="SCANDELETEOK";
	}
else{
	fm.all("QOperate").value="";
	tFlag="0";
	}
if(tFlag==1){
	submitForm();
	}
else{
	alert("不允许该操作");
	}
}

// 禁用界面上的按钮，防止重复录入。
// true：禁用，false：启用
function disableButton(tFlag)
{
    fm.all("Apply").disabled = tFlag;
    fm.all("ApplyPass").disabled = tFlag;
    fm.all("ApplyNoPass").disabled = tFlag;
}

/**
 * 如果是TB类的单证，校验相关联的保单是否已签单。
 * true:已签单；false:未签单
 */
function checkSignOfCont()
{
    var tBussTpye = fm.mBussTpye.value;
    var tSubTpye = fm.mSubTpye.value;
    var tDocCode = fm.mDocCode.value;
    var tContBuss = new Array("TB01", "TB02", "TB03", "TB04");
    
    // 只校验契约相关扫描件。
    if(tBussTpye != "TB")
    {
        return false;
    }
    
    var i = 0;
    for(i = 0; i < tContBuss.length; i++)
    {
        if(tSubTpye == tContBuss[i])
        {
            break;
        }
    }
    if(i == tContBuss.length)
    {
        tDocCode = tDocCode.substring(0, tDocCode.length - 3);
    }
    var tStrSql = "select 1 from LCCont lcc "
        + " where lcc.PrtNo = '" + tDocCode + "' and lcc.AppFlag in ('1') ";
    var tResult = easyExecSql(tStrSql);
    if(tResult)
    {
        alert(tDocCode + "相关保单已经签单，不能申请扫描修改。");
        disableButton(true);
        return true;
    }
    return false;
}


function Crs_easyQueryClick()
{
	resetButton();
	// 初始化表格
	var SQLextra="";
	var SQLQuery="";
	var check="";
	check=fm.all("CheckType").value;
	//alert(check);
	if(check==""){
			check="9";
		}
		
	SQLQuery="'"+check+"'";
	if(check=="9"){
			//SQLQuery="(case when a.doccode in (select bussno from es_issuedoc) then (case when a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='1') then '1' else (case when a.doccode in (select bussno from es_issuedoc where status='0' and stateflag='2') then '2' else (case when a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='3') then '3' else '' end) end) end) else '0' end) ";
            SQLQuery = "''";
		}
	if(check=="0"){
			//SQLextra=" and ((a.doccode not in (select bussno from es_issuedoc)) or (a.doccode in (select bussno from es_issuedoc where status='1' and result is not null)))";
            SQLextra = " and (ei.BussNo is null or (ei.Status = '1' and ei.Result is not null)) "
                + " and (not exists (select 1 from es_issuedoc tei where tei.BussNo = edm.DocCode and (tei.Status = '0' or (tei.Status = '1' and tei.Result is null)))) ";
		}
	else if(check=="1"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='1')";
            SQLextra = " and ei.Status = '1' and ei.StateFlag = '1' ";
		}
	else if(check=="2"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='0' and stateflag='2')";			
            SQLextra = " and ei.Status = '0' and ei.StateFlag = '2' ";
		}
	else if(check=="3"){
			//SQLextra=" and a.doccode in (select bussno from es_issuedoc where status='1' and stateflag='3')";			
            SQLextra = " and ei.Status = '1' and ei.StateFlag = '3' ";
		}	
	initApplyGrid();

	var strSQL = "";
    strSQL = " select distinct DocID, DocCode, SubTypeName, NumPages, temp.MakeDate, ScanOperator, temp.ManageCom, "
        + SQLQuery
        + " from ( "
        + " select edm.docid, edm.doccode, edd.subtypename, edm.numpages, edm.makedate, "
        + " edm.scanoperator, edm.managecom, ei.Status Status, ei.StateFlag StateFlag, ei.Result Result "
        + " from es_doc_main edm "
        + " inner join es_doc_def edd on edd.SubType = edm.SubType "
        + " inner join es_doc_relation edr on edr.DocID = edm.DocID "
        + " left join es_issuedoc ei on ei.BussNo = edm.DocCode "
        + " where 1 = 1 "
        + getWherePart('edm.ManageCom', 'ManageCom', 'like')
        + getWherePart('edm.doccode', 'DocCode')
        + getWherePart('edr.BussNo', 'PrtNo','like')
        + getWherePart('edm.SubType', 'SubType')
        + getWherePart('edm.MakeDate', 'MakeDate')
        + getWherePart('edm.scanoperator', 'Operator')
        + "   and (edm.State is null or edm.State != '03') "  //已发送前置机待外包录入的扫描件不能修改
        + " and edm.busstype not like 'LP%'"
        + SQLextra
        + " ) as temp ,lccont lcc where temp.doccode= lcc.prtno and lcc.Crs_SaleChnl <> '' ";

	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, ApplyGrid);
	
	getESCheckTypeName();
}

function checkIssueType()
{
    var tStrSql = "select 1 from ldcode where code = '" + fm.all("IssueType").value + "' and codetype = 'esmodifytype' ";
    var tResult = easyExecSql(tStrSql);
    if(tResult)
    {
        return true;
    }
    return false;
}

function checkTB01(){
   var tStrSql = "select 1 from es_doc_main es where docid='"+fm.mDocID.value+"' and doccode='"+fm.mDocCode.value+"' and subtype='TB01' and State not in ('03','06')"
               + " and exists(select 1 from lwmission where processid='0000000003' and activityid='0000001099' and missionprop1=es.doccode) "  //待录入状态，除发送外包、已录入均可
               + " union "
               + " select 1 from LWMission where processid ='0000000003' and MissionProp2='"+fm.mDocCode.value+"' and activityid='0000001001' " //新单复核
               ;
   var tResult = easyExecSql(tStrSql);
   if(!tResult){
      alert("保单当前状态非待录入、新单复核状态，不能进行扫描修改。");
      return false;
   }
   return true;
}

function checkTB04(){
   var tStrSql = "select 1 from LWMission where processid ='0000000009' and MissionProp1='"+fm.mDocCode.value+"' and activityid='0000009001' "  //新单录入
               + " union "
               + " select 1 from LWMission where processid ='0000000009' and MissionProp2='"+fm.mDocCode.value+"' and activityid='0000009002' " //新单复核
               ;
   var tResult = easyExecSql(tStrSql);
   if(!tResult){
      alert("保单当前状态非待录入、新单复核状态，不能进行扫描修改。");
      return false;
   }
   return true;
}