<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QCManagerInputMainSave.jsp
//程序功能：
//创建日期：2005-12-30 11:10:36
//创建人  ：Dongjb程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%
String FlagStr = "";
String Content = "";

//获得input页面中取得单证号码和未通过原因
String tOperate =request.getParameter("Operate");  
String tDocID =request.getParameter("mDocID");  
String tDocCode =request.getParameter("mDocCode");  
String tBussTpye =request.getParameter("mBussTpye");  
String tSubTpye =request.getParameter("mSubTpye");  
String tApplyReason =request.getParameter("ApplyReason");  //申请原因
String tApplyResponse =request.getParameter("ApplyResponse");  //回复信息


//获得session中的人员信息
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//操作对象及容器
Es_IssueDocSchema tEs_IssueDocSchema = new Es_IssueDocSchema();
tEs_IssueDocSchema.setIssueDocID(tDocID);
tEs_IssueDocSchema.setBussNo(tDocCode);
tEs_IssueDocSchema.setBussType(tBussTpye);
tEs_IssueDocSchema.setSubType(tSubTpye);
tEs_IssueDocSchema.setIssueDesc(tApplyReason);
tEs_IssueDocSchema.setCheckContent(tApplyResponse);

LLEsModifyUI tLLEsModifyUI = new LLEsModifyUI();
VData vData = new VData();

	vData = new VData();
	vData.add(tG);
	vData.add(tEs_IssueDocSchema);
	//执行后台操作
	try
	{
		if (!tLLEsModifyUI.submitData(vData, tOperate)){
			Content = "操作失败，原因是:" + tLLEsModifyUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}


//如果没有失败，则返回打印成功
if (!FlagStr.equals("Fail"))
{
	Content += "操作成功! ";
	FlagStr = "Succ";
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>