<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
  //程序名称：xbtAmntUpLimitSave.jsp
  //程序功能：
  //创建日期：2010-3-3
  //创建人  ：yinjj
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="com.sinosoft.utility.TransferData"%>

<%@ page import="com.sinosoft.midplat.kernel.management.AmntUpLimitUI"%>
<%@ page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
Logger cLogger = Logger.getLogger("xbt.xbtAmntUpLimitSave_jsp");
cLogger.info("into xbtAmntUpLimitSave.jsp...");

  //接收信息，并作校验处理。输入参数

  //输出参数
  String tOperate = request.getParameter("hideOperate").trim();
  GlobalInput tG = (GlobalInput) session.getValue("GI");
  String BankCode = request.getParameter("BankCode");
	String RiskWrapCode = request.getParameter("RiskWrapCode");
  String ManageCom = request.getParameter("ManageCom");
  String AgentCom = request.getParameter("AgentCom");
	String Amnt = request.getParameter("Amnt");


  //修改前的主键值
  String hideBankCode = request.getParameter("hideBankCode");
  String hideManageCom = request.getParameter("hideManageCom");
  String hideRiskWrapCode = request.getParameter("hideRiskWrapCode");
  String hideAgentCom = request.getParameter("hideAgentCom");
  String hideAmnt = request.getParameter("hideAmnt");
  if (BankCode == null) BankCode = "";
  if (RiskWrapCode == null) RiskWrapCode = "";
  if (ManageCom == null) ManageCom = ""; //ManageCom ;
  if (AgentCom == null) AgentCom = ""; //AgentCom ;
  if (Amnt == null) Amnt = ""; //Amnt ;
 
  //修改前的主键值
  if (hideBankCode == null) hideBankCode = ""; //hideBankCode ;
  if (hideManageCom == null) hideManageCom = ""; //hideManageCom ;
  if (hideRiskWrapCode == null) hideRiskWrapCode = ""; //hideRiskWrapCode ;
  if (hideAgentCom == null) hideAgentCom = ""; //hideAgentCom ;
  if (hideAmnt == null) hideAmnt = ""; //hideAmnt ;
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("BankCode", BankCode);
  tTransferData.setNameAndValue("RiskWrapCode", RiskWrapCode);
  tTransferData.setNameAndValue("ManageCom", ManageCom);
  tTransferData.setNameAndValue("AgentCom", AgentCom);
	tTransferData.setNameAndValue("Amnt", Amnt);

  tTransferData.setNameAndValue("hideBankCode", hideBankCode);
  tTransferData.setNameAndValue("hideManageCom", hideManageCom);
  tTransferData.setNameAndValue("hideRiskWrapCode", hideRiskWrapCode);
  tTransferData.setNameAndValue("hideAgentCom", hideAgentCom);
  tTransferData.setNameAndValue("hideAmnt", hideAmnt);
  String cFlagStr = null;
	String cContent = null;
	try {
  		AmntUpLimitUI mAmntUpLimitUI = 
  			new AmntUpLimitUI(tTransferData, tG, tOperate);
  		mAmntUpLimitUI.deal();
  		cFlagStr = "Succ";
  		cContent = "操作成功！";
  	} catch (Exception ex) {
  		cLogger.error("操作失败！", ex);
  		cFlagStr = "Fail";
  		cContent = "操作失败：" + ex.getMessage();
  }

cLogger.info("out xbtAmntUpLimitSave.jsp!");
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=cFlagStr%>","<%=cContent%>","");
</script></html>