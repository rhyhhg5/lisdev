<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
		fm.all('ManageCom').value = '';
		fm.all('ContNo').value ='';
		
		fm.all('AgentComNo').value ='';
	    fm.all('ProposalcontNo').value = '';
	    fm.all('StartDate').value = '';
	    fm.all('EndDate').value = '';
	    fm.all('startcvalidate').value = '';
	    fm.all('endcvalidate').value = '';
	    //fm.all('agentcom').value = '';
	    fm.all('ContType').value = '';
	    fm.all('RiskCode').value = '';
	   
	    fm.all('ManageComName').value = '';
	    fm.all('AgentComNoName').value = '';
	    fm.all('ContTypeName').value = '';
	    fm.all('RiskName').value = '';
  }
  catch(ex)
  {
    alert("在xbtQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在xbtQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
	initInpBox();
	initSelBox();   
	initybtPolGrid();
	
	
  }
  catch(re)
  {
    alert("xbtQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initybtPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         		//列名
      iArray[1][1]="130px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[2]=new Array();
      iArray[2][0]="印刷号";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="投保人";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      
      iArray[4]=new Array();
      iArray[4][0]="被保险人";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="生效日期";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="满期日";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;  
      
      iArray[7]=new Array();
      iArray[7][0]="保费缴至日期";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;  
            
      iArray[8]=new Array();
      iArray[8][0]="期交保费";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;  
      
      iArray[9]=new Array();
      iArray[9][0]="保单状态";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;
      
      iArray[10]=new Array();
      iArray[10][0]="服务机构";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 
      
      iArray[11]=new Array();
      iArray[11][0]="服务业务员";         		//列名
      iArray[11][1]="50px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;
      
      iArray[12]=new Array();
      iArray[12][0]="中介代码";         		//列名
      iArray[12][1]="100px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;
      
      iArray[13]=new Array();
      iArray[13][0]="中介机构名称";         		//列名
      iArray[13][1]="130px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0;
      
      iArray[14]=new Array();
      iArray[14][0]="网点代码";         		//列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=0;
      
      iArray[15]=new Array();
      iArray[15][0]="网点名称";         		//列名
      iArray[15][1]="130px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=0;
      
      ybtPolGrid = new MulLineEnter( "fm" , "ybtPolGrid" ); 
      //这些属性必须在loadMulLine前
      ybtPolGrid.mulLineCount = 10;   
      ybtPolGrid.displayTitle = 1;
      ybtPolGrid.locked = 1;
      ybtPolGrid.canSel = 1;
      ybtPolGrid.hiddenPlus = 1;
      ybtPolGrid.hiddenSubtraction = 1;
      ybtPolGrid.loadMulLine(iArray); 
      
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>     