
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<%@include file="../common/jsp/UsrCheck.jsp"%>


<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");//添加页面控件的初始化。
	System.out.println("管理机构-----" + tG.ComCode);
%>

<script>
	var manageCom="<%=tG.ManageCom%>"; //记录登陆机构
	var comCode =<%=tG.ComCode%>
</script>
<html>

<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="xbtQueryInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="xbtQueryInit.jsp"%>

<title>陕西有效保单查询</title>
</head>

<body onload="initForm();initElementtype();">
	<form method=post name=fm target="fraSubmit">
		<!-- 保单信息部分 -->
		<table class=common>
			<tr>
				<td class=titleImg>请输入查询条件：</td>
			</tr>
		</table>
		<table class=common>
			<tr>
				<td class=title>管理机构</td>
				<td class=input><input name=ManageCom class=codeno
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);"
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input
					class=codename name=ManageComName readonly=true></td>

				<td class=title>保单号</td>
				<td class=input><input class=common name=ContNo></td>
			</tr>

			<tr class=common>
				<td class=title>代理机构</td>
				<td class=input><input name=AgentComNo class=codeno
					ondblclick="return showCodeList('agentcom',[this,AgentComNoName],[0,1]);"
					onkeyup="return showCodeListKey('agentcom',[this,AgentComNoName],[0,1]);"><input
					class=codename name=AgentComNoName readonly=true></td>
				
				<td class=title>投保单印刷号</td>
				<td class=input><input class=common name=ProposalcontNo></td>
			</tr>

			<tr class=common>
				<td class=title>签单起期</td>
				<td class=input><input class="cooldatepicker" verify="签单起期|NotNull" 
					dateformat="short" name=StartDate elementtype=nacessary ></td>
				<td class=title>签单止期</td>
				<td class=input><input class="cooldatepicker" verify="签单止期|NotNull"
					dateformat="short" name=EndDate elementtype=nacessary ></td>
			</tr>

			<tr class=common>
				<td class=title>生效起期</td>
				<td class=input><input class="cooldatepicker" verify="生效起期|NotNull"
					dateformat="short" name=startcvalidate elementtype=nacessary ></td>
				<td class=title>生效止期</td>
				<td class=input><input class="cooldatepicker" verify="生效止期|NotNull"
					dateformat="short" name=endcvalidate elementtype=nacessary ></td>
			</tr>
			<tr class=common>
				<td class=title>银行代码</td>
				<td class=input>
					<input class=codeno name=BankCode  verify="银行代码|NotNull" ondblclick="return showCodeList('banknum',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('banknum',[this,BankCodeName],[0,1],null,null,null,1,null,1);"><input class=codename  name=BankCodeName readonly=true elementtype=nacessary >	   
	      		</td>

				<td class=title>保单状态</td>
				<td class=input><Input name=ContType class=codeno
					CodeData="0|^0|投保^1|承保有效|^2|失效中止^3|终止"
					ondblclick="return showCodeListEx('codetype',[this,ContTypeName],[0,1]);"
					onkeyup="return showCodeListKeyEx('codetype',[this,ContTypeName],[0,1]);"><input
					name=ContTypeName class='codename' readonly=true></td>
			</tr>
			<tr class=common>
				<td class=title>险种代码</td>
				<td class=input><input class=codeno name=RiskCode
					ondblclick="return showCodeList('ybtriskcode',[this,RiskName],[0,1],null,null,null,1,null,1);"
					onkeyup="return showCodeListKey('ybtriskcode',[this,RiskName],[0,1],null,null,null,1,null,1);"><input
					class=codename name=RiskName></td>
			</tr>
		</table>

		<INPUT VALUE="查  询" class=CssButton TYPE=button
			onclick="easyQueryClick();"> <INPUT class=cssButton
			VALUE="重  置" TYPE=button onclick="return resetForm();"> <INPUT
			VALUE="导  出" class=cssbutton TYPE=button
			onclick="easyQueryPrint(2,'ybtPolGrid','turnPage');">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor:hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<Div id="divLCPol1" style="display: ''" align=center>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanybtPolGrid">
					</span></td>
				</tr>
			</table>

			<INPUT VALUE="首  页" class=CssButton TYPE=button
				onclick="turnPage.firstPage();"> <INPUT VALUE="上一页"
				class=CssButton TYPE=button onclick="turnPage.previousPage();">
			<INPUT VALUE="下一页" class=CssButton TYPE=button
				onclick="turnPage.nextPage();"> <INPUT VALUE="尾  页"
				class=CssButton TYPE=button onclick="turnPage.lastPage();">
		</div>

	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
