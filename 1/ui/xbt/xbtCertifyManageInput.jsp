<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  //程序名称：xbtCertifyManageInput.jsp
  //程序功能：
  //创建日期：2010-3-2
  //创建人  ：yinjj
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="xbtCertifyManageInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="xbtCertifyManageInputInit.jsp"%>
	<%@include file="../agent/SetBranchType.jsp"%>
	<title></title>
</head>
<body onload="initForm();initElementtype();">
<form action="./xbtCertifyManageSave.jsp" method=post name=fm target="fraSubmit">
<span id="operateButton">
  <table class="common" align=center>
    <tr align=right>
      <td class=button>&nbsp;&nbsp;</td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="查  询" name="query" TYPE=button onclick="return queryClick();">
      </td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="增  加" name="add" TYPE=button onclick="return addClick();">
      </td>
      <td class=button width="20%">
        <INPUT class=cssButton VALUE="修  改" name="update" TYPE=button onclick="return updateClick();">
        <INPUT class=cssButton VALUE="保  存" name="updateSubmit" disabled="disabled" TYPE=button onclick="return updateSubmitForm();">
      </td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="删  除" name="del" TYPE=button onclick="return delClick();">
      </td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="重  置" TYPE=button onclick="return resetForm();">
      </td>
    </tr>
  </table>
</span>
<table>
  <tr class=common>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLACom1);">
    </td>
    <td class=titleImg>单证类型&nbsp;&nbsp;&nbsp;</td>
  </tr>
</table>
<Div id="divLACom1" style="display: ''">
  <table class=common>
    <tr class=common>
     <td class=title>银行编码</td>
      <td class=input>
        <input class='codeno'  verify="银行编码|NotNull" name=BankCode ondblclick="return showCodeList('banknum',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('ybtbank',[this,BankCodeName],[0,1],null,null,null,1,null,1);">
        <input class=codename name=BankCodeName readonly=true elementtype=nacessary>  
      </td> 
 <!--     <td class=title>险种编码</td>
      <td class=input>
        <input class='codeno'  verify="险种编码|NotNull" name=RiskCode  CodeData="0|^550806|借款人B^550706|借款人A" ondblclick="return showCodeListEx('RiskCodeName',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKeyEx('RiskCodeName',[this,RiskCodeName],[0,1]);"  >
        <input class=codename name=RiskCodeName readonly=true elementtype=nacessary>  
      </td> -->
			<td class=title>套餐编码</td>
      <td class=input>
        <input class=common elementtype=nacessary verify="套餐编码|NotNull" name=RiskWrapCode>
      </td>
			 
      
    </tr>
    <tr class=common>        
			<td class=title>管理机构</td>
      <td class=input>
        <input name=ManageCom class=codeno   ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);">
        <input class=codename name=ManageComName readonly=true elementtype=nacessary>
      </td> 
      
			 <td class=title>单证编码</td>
      <td class=input>
        <input class=common name=CertifyCode verify="单证编码|NotNull" elementtype=nacessary>  
      </td> 
    </tr>
		<tr class =common>
		  <td class=title>单证类型</td>
      <td class=input>
        <input class='codeno'  verify="单证类型|NotNull" name=CertifyClass  CodeData="0|^D|定额^P|普通" ondblclick="return showCodeListEx('CertifyClassName',[this,CertifyClassName],[0,1]);" onkeyup="return showCodeListKeyEx('CertifyClassName',[this,CertifyClassName],[0,1]);"  >
        <input class=codename name=CertifyClassName readonly=true elementtype=nacessary>  
      </td>
		</tr>
	</table>
</Div>

<input type=hidden name=hideBankCode value=''>
<input type=hidden name=hideRiskWrapCode value=''>
<input type=hidden name=hideManageCom value=''>
<input type=hidden name=hideOperate value=''>

<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLACom);">
    </td>
    <td class=titleImg>单证类型配置信息</td>
  </tr>
</table>
<Div id="divLACom" style="display: ''">
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanLKCodeGrid">        </span>
      </td>
    </tr>
  </table>
</Div>
<Div id="divPage" align=center style="display: '' ">
  <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
  <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
</Div>
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
