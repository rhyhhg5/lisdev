var mOperate="";
var showInfo;

window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}

function resetForm()
{
  try {
    initForm();
  } catch(re) {
    alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//Click事件，当点击增加图片时触发该函数
function addClick() {
	fm.updateSubmit.disabled=true;

//if(fm.Remark1.value.length==0||fm.Remark3.value.length==0)
//  {
//    if (confirm("'对帐网点编码'或'对帐地区编码' 没有填写,是否返回继续填写?"))return false;
  
//  }
	if (fm.Remark5.value==null || fm.Remark5.value == "") {
		alert("请录入网点类别!");
		return false;
	}
	
	if (verifyInput() == false) return false;
	
	if(fm.Remark5.value=="0"
		&& (fm.Remark1.value==null || fm.Remark1.value==""||fm.Remark3.value==null||fm.Remark3.value=="")) {
		alert("请先录入相应的对帐网点");
		return false;
	}

	if(fm.Remark5.value=='1') {
		/**
		 * 总对总，对账网点管理机构固定为总公司(86)；总对分，对账网点管理机构为对应分公司(86xx) 针对江门邮政放开了6位，8位
		 */
		if (('86'!=fm.ManageCom1.value) //不是总公司(86)
			&& (4!=fm.ManageCom1.value.length && 6!=fm.ManageCom1.value.length && 8!=fm.ManageCom1.value.length)) {	//也不是分公司(86xx)
			alert("对帐网点对应的管理机构有误！");
			return false;
		}
		
		
		/**
		 * 存在总对总对账网点，或者对应分公司的对账网点，提示报错。
		 */
		var tSQLStr = "select * from lkcodemapping where remark5='1' and remark2 = '01'"
			+ "and bankcode='" + fm.BankCode.value + "' "
			+ "and (managecom='86' or managecom='" + fm.ManageCom1.value + "')";
		var tSQLResultStr  = easyQueryVer3(tSQLStr, 1, 1, 1);
		if (tSQLResultStr) {
			alert("该机构对帐网点已经存在！");
			return false;
		}
	}

    //判断加入的主键是否已经存在
    turnPage.queryAllRecordCount=0;
    queryClick('flag')
    //alert(turnPage.queryAllRecordCount)
    if(turnPage.queryAllRecordCount>0) {
        alert('该网点已经存在!添加失败.')
        return;
    }
   
 //if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
  //下面增加相应的代码
 
  fm.hideOperate.value="INSERT||MAIN";
  
  
  if (!confirm("您确实添加改该记录吗?"))return;
  //alert();
  submitForm();

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,tBankNode)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    if(fm.hideOperate.value=="DEL||MAIN")content="删除成功!";
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
          //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   // if(fm.hideOperate.value.length>0&&fm.hideOperate.value!="DEL||MAIN")
    queryClick();//提交后自动查询
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}

function delClick()
{fm.updateSubmit.disabled=true;
 if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
  //下面增加相应的代码
  fm.hideOperate.value="DEL||MAIN";
  //alert(mOperate);
  if (!confirm("您确实删除改该记录吗?"))return;
   var rowArr=null;
    n=checkedRowNo("LKCodeGridSel");
    //alert('n '+n)
    if(n!=-1)
    {
      rowArr=LKCodeGrid.getRowData(n);
      if(rowArr!=null)
      {
        fm.hideBankCode.value=rowArr[0];
        fm.hideZoneNo.value=rowArr[1];
        fm.hideBankNode.value=rowArr[2];
//        //fm.Operator.value=rowArr[3];
//        fm.UpAgentCom.value=rowArr[3];
//        fm.ComCode.value=rowArr[4];
//        fm.ManageCom.value=rowArr[5];
//        fm.Remark.value=rowArr[6];
        //BankCode=rowArr[0];
      }
    }
  submitForm();

}


function updateSubmitForm()
{ if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
    
    
 //   if(fm.Remark1.value.length==0||fm.Remark3.value.length==0)
//  {
//    if (confirm("'对帐网点编码'或'对帐地区编码' 没有填写,是否返回继续填写?"))return false;
  
//  }
  
    //turnPage.queryAllRecordCount=0;
    //queryClick('flag')
    //alert(turnPage.queryAllRecordCount)
    //if(turnPage.queryAllRecordCount>0)
    //{
    //    alert('主键重复,您修改的银保通机构信息已经存在!修改失败.')
    //    return;
    //}
    
    

    //下面增加相应的代码
    if (verifyInput() == false) return false;
  if ((fm.all("BankNode").value==null)||(fm.all("BankNode").value==''))
  {
    alert("请先确定银行网点机构！");
    fm.all('BankNode').focus();
  }
  else
  {
          //查询判断是否这个银行机构存在
    // if (!queryBankNode())
    //  return;
      
      //查询判断是否这个银行机构存在

        // 书写SQL语句
//        var strSQL = "";
//        strSQL = "select BankNode from LKCodeMapping where 1=1 "
//                + getWherePart('BankNode')
//                + getWherePart('ZoneNo')
//                + getWherePart('BankCode');
        //turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
//        var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

//  if (strQueryResult) {
//  alert("对不起，已经存在所要操作的银行机构！");
//    fm.all("BankNode").value = '';
//    return false;
  
//  }
      
    if (confirm("您确实想修改该记录吗?"))
    {
      fm.hideOperate.value="UPDATE||MAIN";
      //alert("aaaa");
      submitForm();
      fm.updateSubmit.disabled=true;
    }
    else
    {
      fm.hideOperate.value="";
      alert("您取消了修改操作！");
      return;
    }
  }

//  fm.action="xbtNodeManageSave.jsp";
//   var showStr="正在进行数据操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
//        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//        fm.submit();
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{//alert('.value.value.value')
  
   if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
   
  var rowArr=null;
    n=checkedRowNo("LKCodeGridSel");
    //alert('n '+n)
    if(n==-1)
    {
      alert('请先选中一条记录!');
      return ;
    }
    fm.updateSubmit.disabled=false;
    if(n!=-1)
    {
      rowArr=LKCodeGrid.getRowData(n);
      if(rowArr!=null)
      {

        fm.BankCode.value=rowArr[0];
        fm.ZoneNo.value=rowArr[1];
        fm.BankNode.value=rowArr[2];
        //fm.Operator.value=rowArr[3];
        fm.UpAgentCom.value=rowArr[3];
        fm.ComCode.value=rowArr[5];
       
        if(rowArr[11]=='1') {
          fm.ManageCom1.value=rowArr[7];  
          divNo.style.display="";
          divYes.style.display="none";
        } else {
        	fm.ManageCom.value=rowArr[7];
        	divNo.style.display="none";
          divYes.style.display="";
    		}

//        fm.Remark.value=rowArr[9];
//        fm.Operator.value=rowArr[10];
        fm.Remark1.value=rowArr[9];
        fm.Remark3.value=rowArr[10];
//        fm.FTPAddress.value=rowArr[13];
//        fm.FTPDir.value=rowArr[14];
//        fm.FTPUser.value=rowArr[15];
//        fm.FTPPassWord.value=rowArr[16];
        fm.Remark5.value = rowArr[11];
        
        //BankCode=rowArr[0];
        //修改前主键值
        fm.hideBankCode.value=rowArr[0];
        fm.hideZoneNo.value=rowArr[1];
        fm.hideBankNode.value=rowArr[2];
      }
    }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick(flag)
{fm.updateSubmit.disabled=true;
 initLKCodeGrid();
 var strSQL = "";
 var strOperate="like";
 strSQL = "select BankCode,ZoneNo,BankNode,AgentCom,(select name from lacom where agentcom=b.agentcom),ComCode,(select name from ldcom where comcode=b.comcode),ManageCom,(select name from ldcom where comcode=b.managecom),Remark1,Remark3,Remark5 from LKCodeMapping b where 1 =1 and Remark2 = '01' and managecom like '"+ manageCom +"%%'"//Operator
         
         strSQL+=  getWherePart( 'BankCode' )
        strSQL+=  getWherePart( 'ZoneNo' )
        strSQL+=  getWherePart( 'BankNode' )

        if(flag==null)
        {
        strSQL+=  getWherePart( 'Remark5' )
        strSQL+= getWherePart( 'ComCode' )
        strSQL+= getWherePart( 'ManageCom' )
        
//        strSQL+=  getWherePart( 'Operator' )
//        strSQL+=  getWherePart( 'Remark' )
//         strSQL+=   getWherePart( 'Operator' )
         strSQL+=  getWherePart( 'Remark1' )
         strSQL+=  getWherePart( 'Remark3' )
//        strSQL+=  getWherePart( 'FTPAddress' )
        //strSQL+=  getWherePart( 'FTPDir' )
//        strSQL+=  getWherePart( 'FTPUser' )
//        strSQL+=  getWherePart( 'FTPPassWord' )
        strSQL+=  getWherePart('AgentCom','UpAgentCom','like')
         strSQL+=  getWherePart('ManageCom','ManageCom1')
        }
        strSQL+=' order by BankCode,ZoneNo,BankNode ';

//updateClickalert("11111");
turnPage.pageLineNum=20;
turnPage.queryModal(strSQL, LKCodeGrid);

        return true;
}

function getstr1(Obj1,Obj2)
{
  str1='11';
  if(fm.BankCode.value.length<2)
  {
    alert('请先选择银行代码!');
    return false;
  }

  if(fm.ManageCom.value.length<4)
  {
    alert('请先选择管理机构!');
    return false;
  } else{
  
  
        var BankCode=fm.BankCode.value.substring(0,2);
        var ManageComsub=fm.ManageCom.value.substring(2,4);
        if(ManageComsub == '32')
        {
            ManageComsub = ManageComsub
        }
    		else 
        {
            ManageComsub =fm.ManageCom.value.substring(2,6);
        }
      
	
	 str1="1 and bankcode like #"+ManageComsub+BankCode+"%#";
	 
	}
 	return showCodeList('bank',[Obj1,Obj2],[0,1],null,str1,1,1,200);


}


/**
 * 查询银行编码
 */
function getBankCodeList()
{

    var  sBankCode, sManageCom, sLoginCom, sCodeData;
    try
    {
        sBankCode = document.getElementsByName("BankCode")[0].value;
        sManageCom = document.getElementsByName("ManageCom")[0].value;
        sLoginCom = manageCom; //init
    }
    catch (ex) {}
    if (sBankCode == null || sBankCode == "" || sManageCom == null || sManageCom == "")
    {
        alert("请选择银行代码和管理机构!");
        return;
    }
    else
    {
        
        if (sManageCom.length < 6)
        {
            alert("Error length");
            return;
        }
        else
        {
            var sSunManageCom = sManageCom.substring(2,4);
            if (sSunManageCom != null && sSunManageCom != "32")
                sSunManageCom = sManageCom.substring(2,6);
            //执行 SQL 查询
            var QuerySQL, arrResult;
            QuerySQL = "select BankCode, "
                     +        "BankName "
                     +   "from LDBank "
                     +  "where 1 = 1 "
//                     +    "and BankCode like '" + sSunManageCom + sBankCode + "%%' "
                     +    "and ComCode like '" + sLoginCom + "%%' "
                     +  "order by BankCode asc";
            //alert(QuerySQL);
            try
            {
                arrResult = easyExecSql(QuerySQL, 1, 0);
            }
            catch (ex)
            {
                alert("查询划款银行编码信息出现异常！ ");
                return;
            }
            if (arrResult != null)
            {
                sCodeData = "0|"
                for (var i = 0; i < arrResult.length; i++)
                {
                    sCodeData += "^" + arrResult[i][0] + "|" + arrResult[i][1];
                }
                //alert(sCodeData);
                try
                {
//                    document.getElementsByName("Operator")[0].CodeData = sCodeData;
                }
                catch (ex) {}
            }
            else
            {
                try
                {
                    alert("没有选出此管理机构匹配划款银行编码，请手工输入！");
//                    document.getElementsByName("Operator")[0].CodeData = "";
                    
                }
                catch (ex) {}
            }
        }
    }
}



//提交前的校验、计算
function beforeSubmit()
{
        return true;
}

//提交，保存按钮对应操作
function submitForm()
{
        // alert('mOperate');
//  if (!beforeSubmit())
//  {
//    return false;
//  }
//  //alert(mOperate);
//  	if (mOperate=='INSERT||MAIN'){
//    //校验代理机构代码
//
//  fm.hideOperate.value=mOperate;
//  	}
//  if (mOperate=='UPDATE||MAIN')
//  {
//    fm.hideOperate.value=mOperate;
//   }
//  //showSubmitFrame(1);
// // alert(fm.hideOperate.value);
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
  //initForm();
}

//选择银行代理机构
function SelectBankCom(){
	if(fm.BankCode.value == null || trim(fm.BankCode.value) == '')
	{
		alert("请先选择银行代码");
		return;
	}

	/**
	 * 添加银代机构代码"?BranchType=3&BranchType2=01"
	 * 原本为：var winQuery = window.open("../agentbranch/LAComQuery.html");
	 *
	 * ChenGB(陈贵菠) 2008.04.23
	 */
	var winQuery = window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
	// winQuery.fraInterface.fm.BranchType.value = '03';
	// showInfo=window.open('../treeCom/jsp/BankSelectCategory.jsp?BankType='+trim(fm.BankCode.value),'newwindow','height=400, width=800, top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-800)/2+', toolbar=no, menubar=no, scrollbars=yes, resizable=no, location=no, status=no');
}

//选择管理机构
function SelectCom(){
    showInfo=window.open('../treeCom/jsp/SelectCategory.jsp','newwindow','height=400, width=800, top='+(screen.availHeight-40)/2+',left='+(screen.availWidth-800)/2+', toolbar=no, menubar=no, scrollbars=yes, resizable=no, location=no, status=no');
  }

//选择银代机构后返回机构名称
function afterSelectBank(cAgentCom)
{
        fm.all('UpAgentCom').value = cAgentCom;
        getComName(fm.all('ComCode'),fm.all('ManageCom'));
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示


function showDiv(cDiv,cShow)
{
  if(cShow=="true")
  {
   cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function getComName(input1,input2)
{
	var sql = "select ManageCom from LACom where 1=1"
	 + getWherePart('AgentCom','UpAgentCom');

  var strResult = easyQueryVer3(sql, 1, 1, 1);
  if (strResult) {
		var arrDataSet = decodeEasyQueryResult(strResult);        
    var tArr_Record = new Array();
    tArr_Record = chooseArray(arrDataSet,[0]);
    input1.value = tArr_Record[0][0].substring(0,4);
    input2.value = tArr_Record[0][0];
   
   	if(fm.BankCode.value==null||fm.BankCode.value =="") {
    	alert("请先录入银行编码!")
    	return false;
		}       

		var sql1 = " select banknode,zoneno from lkcodemapping where 1=1"
   		+ " and bankcode = '"
			+ fm.BankCode.value 
			+ "' and remark5= '1' and managecom ='"
			+ input1.value.substring(0,2) 
			+ "'";   
    var strResult1 = easyExecSql(sql1, 1, 1, 1,1);
       
    if(strResult1) { 
      fm.Remark1.value= strResult1[1][0];
      fm.Remark3.value= strResult1[1][1];
    } else {
    	sql1 = " select banknode,zoneno from lkcodemapping where 1=1"
     		+ " and bankcode = '"
				+ fm.BankCode.value
				+ "' and remark5= '1' and managecom like '"
				+ input1.value.substring(0,4) 
				+ "%%'";   

     	strResult1 = easyExecSql(sql1, 1, 1, 1,1);
      if(strResult1) { 
        fm.Remark1.value= strResult1[1][0];
        fm.Remark3.value= strResult1[1][1];
      } else {
     		fm.Remark1.value = "";
     		fm.Remark3.value = "";
     		return;
    	}
		}
  } else {
    input1.value = "";
    input2.value = "";
           
    return false;
  }
}

function queryBankNode()
{
  //查询判断是否这个银行机构存在

        // 书写SQL语句
        var strSQL = "";
        strSQL = "select BankNode from LKCodeMapping where 1=1 "
                + getWherePart('BankNode');
        //turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
        var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  if (!strQueryResult) {
    alert("不存在所要操作的银行机构！");
    fm.all("BankNode").value = '';
    return false;
    }
    return true;
}
var oldbankcode = "";
var newbankcode = "";
function changeBankCode()
{
	newbankcode = fm.BankCode.value;
	if(oldbankcode != newbankcode)
	{
		fm.UpAgentCom.value='';
		//fm.Operator.value='';
		fm.Remark1.value='';
		fm.Remark3.value='';
	}
	oldbankcode = newbankcode;
}
//返回选中的行号,这对radio button
function checkedRowNo(name)
{
  //var isOneMore=false;
  obj=document.all[name];

  if(typeof(obj.length)!='undefined')
  {
      for(i=0;i<obj.length;i++)
      {
        if(obj[i].checked)return i;
       //alert(isOneMore)
      }
  }
  else
  {
    if(obj.checked)return 0;
  }


  return -1;
}

function afterCodeSelect( cCodeName, Field ) {
	try {
		if(cCodeName == 'bankbranchtype') {
			if(fm.all('BankCode').value=='04') {
				//fm.all('FTPAddress').readOnly = false;
	//			fm.all('FTPDir').readOnly = false;
				//fm.all('FTPUser').readOnly = false;
				//fm.all('FTPPassWord').readOnly = false;
			} else {
//        fm.all('FTPAddress').readOnly = true;
				//fm.all('FTPDir').readOnly = true;
				//fm.all('FTPUser').readOnly = true;
				//fm.all('FTPPassWord').readOnly = true;
	    }
		}

		if(cCodeName == 'NodeType') {
			if(fm.all('Remark5').value == '0') {
	      fm.all('ManageCom1').value = '';
//        fm.all('Remark').value = '';
//        fm.FTPAddress.value='';
//        fm.FTPDir.value='';
//        fm.FTPUser.value='';
//        fm.FTPPassWord.value='';
        divYes.style.display = "";
        divNo.style.display = "none";
      }

      if(fm.all('Remark5').value == '1') {
	      fm.UpAgentCom.value='';
				//fm.Operator.value='';
		    fm.Remark1.value='';
		    fm.Remark3.value='';
		    fm.all('ComCode').value = '';
        fm.all('ManageCom').value = '';
        divYes.style.display = "none";
        divNo.style.display = "";
      }
		}
	} catch(ex) {
  }	
}

function afterQuery(pSQLResult) {
	if((null!=pSQLResult) && (pSQLResult.length==1)) {
		fm.UpAgentCom.value = pSQLResult[0][0];
		fm.UpAgentName.value = pSQLResult[0][1];
		fm.ComCode.value = pSQLResult[0][8];
		fm.ManageCom.value = pSQLResult[0][8];
	
		/**
		 * 根据管理机构查询对应的对账网点。
		 */
		var mSQLStr = "select banknode, zoneno from lkcodemapping where remark5='1' "
			+ "and bankcode='" + fm.BankCode.value + "' "
			+ "and (managecom='86' or managecom='" + fm.ManageCom.value.substring(0, 4) + "')";
		var mSQLResult = easyExecSql(mSQLStr, 1, 1, 1, 1);
		if(null != mSQLResult) {
			fm.Remark1.value = mSQLResult[1][0];
			fm.Remark3.value = mSQLResult[1][1];
		} else {
			alert("未配置对应对账网点！");
			fm.Remark1.value = "";
			fm.Remark3.value = "";
		}
	} else {
		alert("请选择一个代理机构，且只能选择一个！");
	}
}