var mOperate="";
var showInfo;

window.onfocus=myonfocus;
var turnPage = new turnPageClass();

function resetForm(){
	try {
		initForm();
	} catch(re) {
		alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}
//获取Mulline选中的数据
function getMullineData(){ 
	divLACom2.style.display = "none" ;
	divState2.style.display = "none" ;
	divAgent2.style.display = "none";
	divLACom1.style.display = "" ;
	
	var num = LKFeeRateDetail.getSelNo();

	if(num<0) {
		alert("请选择一条数据");
		return false;
	}
	
	fm.ProtocolNo.value=LKFeeRateDetail.getRowColDataByName(num-1,"ProtocolNo");
	//alert(fm.ProtocolNo.value);
	fm.CommonFeeRateFlag.value = LKFeeRateDetail.getRowColDataByName(num-1,"CommonFeeRateFlag");
	fm.ManageCom.value = LKFeeRateDetail.getRowColDataByName(num-1,"ManageCom");
	fm.RiskWrapCode.value = LKFeeRateDetail.getRowColDataByName(num-1,"RiskWrapCode");
	fm.BankCode.value = LKFeeRateDetail.getRowColDataByName(num-1,"BankCode");

	if(fm.CommonFeeRateFlag.value=='1'){
		fm.AvailableState.value=LKFeeRateDetail.getRowColDataByName(num-1,"AvailableState");
		divState.style.display = "";

		if(fm.AvailableState.value=='1'){			
			fm.FeeRate.value=LKFeeRateDetail.getRowColDataByName(num-1,"FeeRate");
			fm.hideFeeRate.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRate");
			fm.FeeRateType.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateType");
			fm.hideFeeRateType.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateType");
			fm.FeeRateYear.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateYear");//此项也许赋值,不管是否是阶梯费率,因为UI进行更新此项是主键
			fm.hideFeeRateYear.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateYear");
			fm.FeeRateYearFlag.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateYearFlag");
			divFee.style.display = "";	
			fm.hideType.value = "TypeCommon";//重要
			if(fm.FeeRateType.value == 'L'){	
				//alert("LLL");
				divFeeRateYear.style.display = '';				
			}			
		}
		if(fm.AvailableState.value=='0'){
			fm.hideType.value = "TypeInvalid";//重要
		}
	}

	if(fm.CommonFeeRateFlag.value=='0'){
		fm.AgentCom.value = LKFeeRateDetail.getRowColDataByName(num-1,"AgentCom");
		fm.FeeRate.value=LKFeeRateDetail.getRowColDataByName(num-1,"FeeRate");
		fm.hideFeeRate.value=LKFeeRateDetail.getRowColDataByName(num-1,"FeeRate");

		fm.FeeRateType.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateType");
		fm.hideFeeRateType.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateType");
		fm.FeeRateYear.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateYear");
		fm.hideFeeRateYear.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateYear");
		fm.FeeRateYearFlag.value = LKFeeRateDetail.getRowColDataByName(num-1,"FeeRateYearFlag");
		
		divAgent.style.display = "";
		divFee.style.display = "";
		fm.hideType.value = "TypePersonnal";//重要

		if(fm.FeeRateType.value == 'L'){
			divFeeRateYear.style.display = '';
		}
	}
}

//Click事件，当点击增加图片时触发该函数
function addClick() {
	fm.updateSubmit.disabled=true;

	if(fm.CommonFeeRateFlag.value==null||fm.CommonFeeRateFlag.value==''){
		alert('请选择费率标志！');
		return false;
	}

	if(fm.CommonFeeRateFlag.value=='1'){//公共费率	
		fm.AgentCom.value = "PC";//公共费率默认为PC
		if(fm.RiskWrapCode.value==null||fm.RiskWrapCode.value==''){
			alert("请输入套餐编码！");
			return false;
		}

		if(fm.AvailableState.value==null||fm.AvailableState.value==''){
			alert("请选择可用状态！");
			return false;
		}
		
		if(fm.ManageCom.value.trim().length != 4){
			alert("管理机构必须为四位！");
			return false; 
		}
		
		if(fm.BankCode.value==null||fm.BankCode.value==''){
			alert("请输入银行代码！");
			return false;
		}

		if(fm.AvailableState.value=='0'){
			if(confirm("请确认输入的信息无误，您确实添加改该记录吗?")==true){
				fm.hideOperate.value="INSERT||MAIN";
				fm.hideType.value = "TypeInvalid";
				submitForm();
			}else{
				return false;
			}
		}

		if(fm.AvailableState.value=='1'){
			if(fm.FeeRate.value==null||fm.FeeRate.value==''){
				alert("请输入费率！");
				return false ;
			}
			 
			//对费率添加校验,范围 1--400
			if(fm.FeeRate.value > 400 ||fm.FeeRate.value < 1){
				alert("费率超出范围,只能是1--400之间的值!");
				return false;
			}

			if(fm.FeeRateType.value==null||fm.FeeRateType.value==''){
				alert("请输入费率类型！");
				return false;
			}else if(fm.FeeRateType.value == "M"){
				fm.FeeRateYearFlag.value = "M";
				fm.FeeRateYear.value = "-1";//默认为-1
			}else if(fm.FeeRateType.value == "Y"){
				fm.FeeRateYearFlag.value = "Y";
				fm.FeeRateYear.value = "-1";//默认为-1
			}
			
			if(fm.FeeRateType.value=='L'){
				if(fm.FeeRateYear.value==null||fm.FeeRateYear.value==''){
					alert('请输入费率期间！');
					return false;
				}else if(fm.FeeRateYear.value < 1||fm.FeeRateYear.value > 100){
					alert("费率期间只能是1-100之间的整数!");
					return false;
				}else{
					fm.FeeRateYearFlag.value = "M"; //目前阶梯费率只支持12个月的费率.不支持年的阶梯费率
				}

			}
			
			
			if(confirm("请确认输入的信息无误，您确实添加改该记录吗?")==true){
				fm.hideOperate.value="INSERT||MAIN";
				fm.hideType.value = "TypeCommon";
				submitForm();
			}else{
				return false;
			}

		}
	}

	if(fm.CommonFeeRateFlag.value=='0'){//个性费率
		fm.AvailableState.value = "1";//可用状态为可用.
		fm.hideType.value = "TypePersonnal";
		if(fm.RiskWrapCode.value==null||fm.RiskWrapCode.value==''){
			alert("请输入套餐编码！");
			return false;
		}
		
		if(fm.ManageCom.value.trim().length != 4){
			alert("管理机构必须为四位！");
			return false;
		}
		
		if(fm.BankCode.value==null||fm.BankCode.value==''){
			alert("请输入银行代码！");
			return false;
		}

		if(fm.AgentCom.value==null||fm.AgentCom.value==''){
			alert("请输入中介机构编码！");
			return false ;
		}
		
		if(fm.AgentCom.value.trim().length != 12){
			alert("中介机构长度必须是12位!");
			return false;
		}
		
		if(fm.FeeRate.value==null||fm.FeeRate.value==''){
			alert("请输入费率！");
			return false ;
		}
		
		//对费率添加校验,范围 1--400
		if(fm.FeeRate.value > 400 ||fm.FeeRate.value < 1){
			alert("费率超出范围,只能是1--400之间的值!");
			return false;
		}
		
		if(fm.FeeRateType.value==null||fm.FeeRateType.value==''){
			alert("请输入费率类型！");
			return false;
		}
		
		//FeeRateYearFlag
		if(fm.FeeRateType.value=='M'){
			fm.FeeRateYearFlag.value = "M";
			fm.FeeRateYear.value = "-1";//默认为-1
		}else if(fm.FeeRateType.value=='Y'){
			fm.FeeRateYearFlag.value = "Y";
			fm.FeeRateYear.value = "-1";//默认为-1
		}else {
			fm.FeeRateYearFlag.value = "M";
		}
		
		if(fm.FeeRateType.value=='M'||fm.FeeRateType.value=='Y'){
			if(confirm("请确认输入的信息无误，您确实添加改该记录吗?")==true){
				fm.hideOperate.value="INSERT||MAIN";
				submitForm();
			}else{
				return false;	
			}
		}

	   if(fm.FeeRateType.value=='L'){
			if(fm.FeeRateYear.value==null||fm.FeeRateYear.value==''){
				alert('请输入费率期间！');
				return false;
			}
			
			if(fm.FeeRateYear.value < 1||fm.FeeRateYear.value > 100){
				alert("费率期间只能是1-100之间的整数!");
				return false;
			}
			if(confirm("请确认输入的信息无误，您确实添加改该记录吗?")==true){
				fm.hideOperate.value="INSERT||MAIN";
				submitForm();
			}else{
				return false;
			}
		}
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,tBankNode){
		showInfo.close();

		if (FlagStr == "Fail" ){
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}else{
			if(fm.hideOperate.value=="DEL||MAIN") content="删除成功!";
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			queryClick();//提交后自动查询
		}
}

function delClick(){
	if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return false ;}

	var num = LKFeeRateDetail.getSelNo();

	if(num<0){
		alert("请选择要删除的一条数据！");
		return false;
	} 

	fm.updateSubmit.disabled=true;

	getMullineData();
	fm.hideOperate.value="DEL||MAIN";
	
	if (confirm("您确实删除改该记录吗?")){
		divLACom2.style.display = "none" ;
		divState2.style.display = "none" ;
		divAgent2.style.display = "none";
		divLACom1.style.display = "" ;
		//alert(fm.hideType.value);
		submitForm();
	}else{
		alert("你已取消删除该条数据！");
		return false;
	}
}

function updateSubmitForm(){ 
	if(turnPage.queryAllRecordCount<=0){alert('请先查询!');fm.updateSubmit.disabled=true;return false;}
	var num = LKFeeRateDetail.getSelNo();

	if(num<0){
		alert("请选择要修改的数据！");
		fm.updateSubmit.disabled=true;
		return false;
	}    
	
	if(fm.CommonFeeRateFlag.value==null||fm.CommonFeeRateFlag.value==''){
		alert('请选择费率标志！');
		return false;
	}

	if(fm.CommonFeeRateFlag.value=='1'){	
		if(fm.RiskWrapCode.value==null||fm.RiskWrapCode.value==''){
			alert("请输入套餐编码！");
			return false;
		}

		if(fm.AvailableState.value==null||fm.AvailableState.value==''){
			alert("请选择可用状态！");
			return false;
		}

		if(fm.AvailableState.value=='0'){
			/*if(confirm("您确实想修改该记录吗?")==true){
				fm.hideOperate.value="UPDATE||MAIN";
				fm.hideType.value = "TypeInvalid";
				submitForm();
				fm.updateSubmit.disabled=true;
			}else{
				fm.hideOperate.value="";
				return false;
			}*/
			//此种情况下不允许修改数据
			alert("此数据标识此管理机构下该套餐不通过费率表配置算费!如果您确实想修改此条数据,请先删除,再进行添加操作.");
			return false;
		}

		if(fm.AvailableState.value=='1'){
			fm.hideType.value = "TypeCommon";
			
			if(fm.FeeRate.value==null||fm.FeeRate.value==''){
				alert("请输入费率！");
				return false ;
			}

			if(fm.FeeRateType.value==null||fm.FeeRateType.value==''){
				alert("请输入费率类型！");
				return false;
			}

			if(fm.FeeRateType.value=='M'||fm.FeeRateType.value=='Y'){
				if(confirm("您确实想修改该记录吗?")==true){
					fm.hideOperate.value="UPDATE||MAIN";
					submitForm();
					fm.updateSubmit.disabled=true;
				}else{
					fm.hideOperate.value="";
					return false;
				}
			}

			if(fm.FeeRateType.value=='L'){
				if(fm.FeeRateYear.value==null||fm.FeeRateYear.value==''){
					alert('请输入费率期间！');
					return false;
				}
				
				if(fm.FeeRateYear.value > 100 ||fm.FeeRateYear.value <1){
					alert("费率期间只能是1-100之间的整数!");
					return false;
				}

				if(confirm("您确实想修改该记录吗?")==true){
					fm.hideOperate.value="UPDATE||MAIN";
					submitForm();
					fm.updateSubmit.disabled=true;
				}else{
					fm.hideOperate.value="";
					return false;
				}
			}
		}
	}

	if(fm.CommonFeeRateFlag.value=='0'){
		fm.hideType.value = "TypePersonnal";
		
		if(fm.RiskWrapCode.value==null||fm.RiskWrapCode.value==''){
			alert("请输入套餐编码！");
			return false;
		}

		if(fm.AgentCom.value==null||fm.AgentCom.value==''){
			alert("请输入中介机构编码！");
			return false ;
		}

		if(fm.FeeRate.value==null||fm.FeeRate.value==''){
			alert("请输入费率！");
			return false ;
		}

		if(fm.FeeRateType.value==null||fm.FeeRateType.value==''){
			alert("请输入费率类型！");
			return false;
		}

		if(fm.FeeRateType.value=='M'||fm.FeeRateType.value=='Y'){
			if(confirm("您确实想修改该记录吗?")==true){
				fm.hideOperate.value="UPDATE||MAIN";
				submitForm();
				fm.updateSubmit.disabled=true;
			}else{
				return false;	
			}
		}

	   if(fm.FeeRateType.value=='L'){
			if(fm.FeeRateYear.value==null||fm.FeeRateYear.value==''){
				alert('请输入费率期间！');
				return false;
			}

			if(confirm("您确实想修改该记录吗?")==true){
				fm.hideOperate.value="UPDATE||MAIN";
				submitForm();
				fm.updateSubmit.disabled=true;
			}else{
				fm.hideOperate.value="";
				return false;
			}
		}
	}

	
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick(){
	if(turnPage.queryAllRecordCount<=0){alert('请先查询!');fm.updateSubmit.disabled=true;return false;}

	getMullineData();
	
	fm.CommonFeeRateFlag2.value = fm.CommonFeeRateFlag.value;
	fm.RiskWrapCode2.value = fm.RiskWrapCode.value;
	fm.ManageCom2.value = fm.ManageCom.value;
	fm.BankCode2.value = fm.BankCode.value;

	divLACom1.style.display = "none";
	divLACom2.style.display = "";

	if(fm.CommonFeeRateFlag.value=='1'){		
		fm.AvailableState2.value=fm.AvailableState.value;
		divState.style.display = "none";
		divState2.style.display = "";

		if(fm.AvailableState.value=='1'){						
			divFee.style.display = "";			
			if(fm.FeeRateType.value == 'L'){
				//To-DO STH
				divFeeRateYear.style.display = '';	
			}			
							
		}else{
			//TypeIvalid不能进行修改操作
			alert("此数据标识此管理机构下该套餐不通过费率表配置算费!如果您确实想修改此条数据,请先删除,再进行添加操作.");
			return false;
		}		
	}

	if(fm.CommonFeeRateFlag.value=='0'){
		fm.AgentCom2.value = fm.AgentCom.value;
		fm.AgentComName2.value = fm.AgentComName.value;

		divAgent.style.display = "none";
		divAgent2.style.display = "";
		divFee.style.display = "";

		if(fm.FeeRateType.value == 'L'){
			divFeeRateYear.style.display = '';
		}
	}
	
	//fm.add.disabled = "";
	fm.updateSubmit.disabled=false;
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick(Flag){
	//fm.CommonFeeRateFlag.value = "";
	//fm.RiskWrapCode.value = "";
//	divState.style.display = "none";
//	divAgent.style.display = "none";
//	divFee.style.display = "none";
//	divFeeRateYear.style.display = 'none';
//	divLACom2.style.display = "none" ;
//	divState2.style.display = "none" ;
//	divAgent2.style.display = "none";
//	divLACom1.style.display = "" ;
	//alert("进入查询");
	  //判断是否查询成功
    fm.updateSubmit.disabled=true; 	
	initLKFeeRateDetail();
	
	var strSql = "";
	
	if(fm.CommonFeeRateFlag.value == ''){
		alert("费率标志不能为空!");
		return false;
	}
	
	if(fm.AvailableState.value == ''){
		alert("可用状态不能为空!");
		return false;
	}

	if(fm.AvailableState.value=='0'){//Type
		strSql = "select ProtocolNo,RiskWrapCode,ManageCom,BankCode,AgentCom,(select name from lacom where agentcom=b.agentcom),AvailableState,CommonFeeRateFlag,remark2,remark2,remark2,remark1,remark2 from lkfeerateprotocol b where 1 = 1";//部分字段为空,查询remark2代替
				strSql+=  getWherePart( 'BankCode' )
        strSql+=  getWherePart( 'ManageCom' )
        strSql+=  getWherePart( 'RiskWrapCode' )
        strSql+=  getWherePart( 'CommonFeeRateFlag' )
        strSql+=  getWherePart( 'AvailableState' )
               
        strSql+=' order by bankcode,managecom,riskwrapcode with ur';
	}else{
		strSql = "select b.ProtocolNo,RiskWrapCode,ManageCom,b.BankCode,AgentCom,(select name from lacom where agentcom=b.agentcom),AvailableState,CommonFeeRateFlag,FeeRate,FeeRateType,FeeRateYear,b.remark1,FeeRateYearFlag from lkfeerateprotocol b , lkfeeratedetails c where b.protocolno = c.protocolno";
				strSql+=  getWherePart( 'BankCode' )
        strSql+=  getWherePart( 'ManageCom' )
        strSql+=  getWherePart( 'RiskWrapCode' )
        strSql+=  getWherePart( 'AgentCom' )
        strSql+=  getWherePart( 'FeeRateType' )
        strSql+=  getWherePart( 'FeeRateYear' )
        strSql+=  getWherePart( 'CommonFeeRateFlag' )
        strSql+=  getWherePart( 'AvailableState' )
               
        strSql+=' order by bankcode,managecom,riskwrapcode with ur';
	}

	turnPage.pageLineNum=20;
	turnPage.queryModal(strSql, LKFeeRateDetail);
	//alert("chaxun");
	//alert(turnPage.queryAllRecordCount);
	return true;
}

//提交，保存按钮对应操作
function submitForm(){
	if(fm.hideOperate.value == "INSERT||MAIN"){
		//进行操作之前先进行查询,确认此条记录以前没有添加过
		turnPage.queryAllRecordCount = '0'; //初始化查询出来的记录总数为'0'
		queryClick();
		if(turnPage.queryAllRecordCount>0){
			alert(turnPage.queryAllRecordCount);
			alert("该费率记录已经存在!");
			return false;
		}
	}
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	if (fm.hideOperate.value==""){
		alert("操作控制数据丢失！");
	}
	fm.submit(); //提交
}

function afterCodeSelect( cCodeName, Field ) {
	try {
		if(cCodeName == 'FeeRateFlag') {
				if(fm.all('CommonFeeRateFlag').value == '1') {
					fm.all('AgentCom').value = '';
					fm.all('AgentComName').value = '';
					fm.all('FeeRate').value = '';
					fm.all('AvailableState').value = '';
					fm.all('stateType').value = '';
					fm.all('FeeRateType').value = '';
					fm.all('FeeRateTypeName').value = '';
					fm.all('FeeRateYear').value = '';
					divState.style.display = "";
					divAgent.style.display = "none";
					divFee.style.display = "none";
					divFeeRateYear.style.display = 'none';
				}

				if(fm.all('CommonFeeRateFlag').value == '0') {
					fm.all('AgentCom').value = '';
					fm.all('AgentComName').value = '';
					fm.all('FeeRate').value = '';
					fm.all('AvailableState').value = '1';
					fm.all('stateType').value = '';
					fm.all('FeeRateType').value = '';
					fm.all('FeeRateTypeName').value = '';
					fm.all('FeeRateYear').value = '';
					divAgent.style.display = "";
					divFee.style.display = "";
					divState.style.display = "none";
					divFeeRateYear.style.display = 'none';
				}
			}

			if(cCodeName == 'stateType') {
				if(fm.all('AvailableState').value == '0') {
					fm.all('AgentCom').value = '';
					fm.all('AgentComName').value = '';
					fm.all('FeeRate').value = '';
					fm.all('FeeRateType').value = '';
					fm.all('FeeRateTypeName').value = '';
					fm.all('FeeRateYear').value = '';
					divState.style.display = "";
					divAgent.style.display = "none";
					divFee.style.display = "none";
					divFeeRateYear.style.display = 'none';
				}

				if(fm.all('AvailableState').value == '1') {
					fm.all('AgentCom').value = '';
					fm.all('AgentComName').value = '';
					fm.all('FeeRate').value = '';
					fm.all('FeeRateType').value = '';
					fm.all('FeeRateTypeName').value = '';
					fm.all('FeeRateYear').value = '';
					divState.style.display = "";
					divAgent.style.display = "none";
					divFee.style.display = "";
					divFeeRateYear.style.display = 'none';
				}
			}

		if(cCodeName == 'FeeRateTypeName') {
				if(fm.all('FeeRateType').value == 'L') {//阶梯费率

					fm.all('FeeRateYear').value = '';
					fm.all('FeeRateYearFlag').value = 'M';
					divFeeRateYear.style.display = '';
				}
				if((fm.all('FeeRateType').value == 'M')) {//月费率
					fm.all('FeeRateYearFlag').value = 'M';
					fm.all('FeeRateYear').value = '-1';
					divFeeRateYear.style.display = 'none';
				}
				if(fm.all('FeeRateType').value == 'Y'){//年费率
					fm.all('FeeRateYearFlag').value = 'Y';
					fm.all('FeeRateYear').value = '-1';
					divFeeRateYear.style.display = 'none';
				}
		}
	} catch(ex) {}	
}