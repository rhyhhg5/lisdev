<%
	//程序名称：xbtFeeRateManageInputInit.jsp
	//程序功能：信保通费率配置
	//创建日期：2010-04-25
	//创建人  ：yinjj程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">
var manageCom = '<%= globalInput.ManageCom %>';
function initInpBox(){
	try{
		fm.all('RiskWrapCode').value = '';
		fm.all('CommonFeeRateFlag').value = '';
		if(manageCom == "86"){
			fm.all('ManageCom').value = '';
		}else{
				
		}
		fm.all('BankCode').value = '';
		fm.all('BankCodeName').value = '';
		fm.all('AvailableState').value = '';
		fm.all('AgentCom').value = '';
		fm.all('AgentComName').value = '';
		fm.all('FeeRateFlag').value='';
		fm.all('stateType').value='';
				
		fm.all('FeeRate').value = '';
		fm.all('FeeRateType').value = '';
		fm.all('FeeRateTypeName').value = '';
		fm.all('FeeRateYear').value = '';
		fm.all('FeeRateYearFlag').value = '';
      	fm.all('hideOperate').value="";
      	fm.all('hideType').value="";
      	
 		divState.style.display = "none";
     	divAgent.style.display = "none";
     	divFee.style.display = "none";
     	divFeeRateYear.style.display = 'none';
     	
     	divLACom2.style.display = "none" ;
     	divState2.style.display = "none" ;
     	divAgent2.style.display = "none";
	}catch(ex){
		alert("在LAComInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}
function initSelBox(){
	try{}catch(ex){ alert("在LAComInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");}
}
function initForm(){
	try{
		initInpBox();    
    
		initSelBox();
		initLKFeeRateDetail(); 
		turnPage.queryAllRecordCount = '0'; //初始化查询出来的记录总数为'0'
	}catch(re){
		alert("LAComInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

var LKFeeRateDetail;          //定义为全局变量，提供给displayMultiline使用
// 信息列表的初始化
function initLKFeeRateDetail(){
	var iArray = new Array();
    //var i11Array = getAgentGradeStr();
    try{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="协议编号";         		//列名
		iArray[1][1]="0px";            		//列宽
		iArray[1][2]=1;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		iArray[1][21]="ProtocolNo";

		iArray[2]=new Array();
		iArray[2][0]="套餐编码";         		//列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[2][21]="RiskWrapCode";

		iArray[3]=new Array();
		iArray[3][0]="管理机构";         		//列名
		iArray[3][1]="60px";            		//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[3][21]="ManageCom";
		
		iArray[4]=new Array();
		iArray[4][0]="银行代码";         		//列名
		iArray[4][1]="40px";            		//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[4][21]="BankCode";

		iArray[5]=new Array();
		iArray[5][0]="中介机构";         		//列名
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[5][21]="AgentCom";

		iArray[6]=new Array();
		iArray[6][0]="机构名称";         		//列名
		iArray[6][1]="100px";            		//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[6][21]="AgentName";

		iArray[7]=new Array();
		iArray[7][0]="可用状态";         		//列名
		iArray[7][1]="35px";            		//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[7][21]="AvailableState";


		iArray[8]=new Array();
		iArray[8][0]="公共费率标志";         		//列名
		iArray[8][1]="50px";            		//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[8][21]="CommonFeeRateFlag";

		iArray[9]=new Array();
		iArray[9][0]="费率";         		//列名
		iArray[9][1]="50px";            		//列宽
		iArray[9][2]=200;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[9][21]="FeeRate";

		iArray[10]=new Array();
		iArray[10][0]="费率类型";         		//列名
		iArray[10][1]="50px";            		//列宽
		iArray[10][2]=200;            			//列最大值
		iArray[10][3]=0;
		iArray[10][21]="FeeRateType";

		iArray[11]=new Array();
		iArray[11][0]="费率期间";         		//列名
		iArray[11][1]="50px";            		//列宽
		iArray[11][2]=200;            			//列最大值
		iArray[11][3]=0;
		iArray[11][21]="FeeRateYear";
		
		iArray[12]=new Array();
		iArray[12][0]="标志";         		//列名
		iArray[12][1]="0px";            		//列宽
		iArray[12][2]=1;            			//列最大值
		iArray[12][3]=3;
		iArray[12][21]="Remark1";
		
		iArray[13]=new Array();
		iArray[13][0]="费率年期类型";         		//列名
		iArray[13][1]="0px";            		//列宽
		iArray[13][2]=1;            			//列最大值
		iArray[13][3]=3;                        //值为3的时候是将此列隐藏
		iArray[13][21]="FeeRateYearFlag";

		LKFeeRateDetail = new MulLineEnter( "fm" , "LKFeeRateDetail" );
		//这些属性必须在loadMulLine前
		LKFeeRateDetail.displayTitle = 1;
		LKFeeRateDetail.locked = 0;
		LKFeeRateDetail.mulLineCount = 1;
		LKFeeRateDetail.hiddenPlus = 1;
		LKFeeRateDetail.hiddenSubtraction = 1;
		LKFeeRateDetail.canSel = 1;

		LKFeeRateDetail.loadMulLine(iArray);
     }catch(ex){
        alert(ex);
      }
}
</script>