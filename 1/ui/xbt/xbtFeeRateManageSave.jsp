<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：xbtFeeRateManageSave.jsp
//程序功能：信保通费率配置
//创建日期：2010-04-25
//创建人  ：yinjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@ page import="org.apache.log4j.Logger"%>
<%@page import="com.sinosoft.utility.TransferData"%>
<%@page import="com.sinosoft.midplat.kernel.management.FeeRateManageUI"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	Logger cLogger = Logger.getLogger("midplat.xbtFeeRateManageSave_jsp");
	cLogger.info("into xbtFeeRateManageSave.jsp...");

	//输出参数
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	TransferData tTransferData = new TransferData();
	String cFlagStr = null;
	String cContent = null;
	//收集从前台传输过来的数据
	String tBankCode = request.getParameter("BankCode").trim();
	
	String tOperate = request.getParameter("hideOperate").trim();
	String tHideFeeRate = request.getParameter("hideFeeRate").trim();
	String tHideFeeRateType = request.getParameter("hideFeeRateType").trim();
	String tHideFeeRateYear = request.getParameter("hideFeeRateYear").trim();
	String tType = request.getParameter("hideType").trim();//标识3种类型,TypeInvalid,TypeCommon,TypePersonnal
	String tProtocolNo = request.getParameter("ProtocolNo").trim();
	String tRiskWrapCode = request.getParameter("RiskWrapCode").trim();
	String tManageCom = request.getParameter("ManageCom").trim();
	String tAgentCom = request.getParameter("AgentCom").trim();
	String tAgentName = request.getParameter("AgentComName").trim();
	String tAvailableState = request.getParameter("AvailableState").trim();
	String tCommonFeeRateFlag = request.getParameter("CommonFeeRateFlag").trim();
	String tFeeRate = request.getParameter("FeeRate").trim();
	String tFeeRateType = request.getParameter("FeeRateType").trim();
	String tFeeRateYear = request.getParameter("FeeRateYear").trim();
	String tFeeRateYearFlag = request.getParameter("FeeRateYearFlag").trim();
	
	//准备将收集到的数据组装传递到后台
	tTransferData.setNameAndValue("bankcode",tBankCode);
	tTransferData.setNameAndValue("operate",tOperate);
	tTransferData.setNameAndValue("hidefeerate",tHideFeeRate);
	tTransferData.setNameAndValue("hidefeeratetype",tHideFeeRateType);
	tTransferData.setNameAndValue("hidefeerateyear",tHideFeeRateYear);
	tTransferData.setNameAndValue("protocolno",tProtocolNo);
	tTransferData.setNameAndValue("riskwrapcode",tRiskWrapCode);
	tTransferData.setNameAndValue("managecom",tManageCom);
	tTransferData.setNameAndValue("agentcom",tAgentCom);
	tTransferData.setNameAndValue("agentname",tAgentName);
	tTransferData.setNameAndValue("availablestate",tAvailableState);
	tTransferData.setNameAndValue("commonfeerateflag",tCommonFeeRateFlag);
	tTransferData.setNameAndValue("feerate",tFeeRate);
	tTransferData.setNameAndValue("feeratetype",tFeeRateType);
	tTransferData.setNameAndValue("feerateyear",tFeeRateYear);
	tTransferData.setNameAndValue("feerateyearflag",tFeeRateYearFlag);

	try {
		FeeRateManageUI tFeeRateManageUI = 
		new FeeRateManageUI(tTransferData, tG, tOperate,tType);
		tFeeRateManageUI.deal();
		cFlagStr = "Succ";
		cContent = "操作成功！";
	} catch (Exception ex) {
		cLogger.error("操作失败！", ex);
		cFlagStr = "Fail";
		cContent = "操作失败：" + ex.getMessage();
	}

	cLogger.info("out xbtFeeRateManageSave.jsp!");
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=cFlagStr%>","<%=cContent%>","");
</script></html>