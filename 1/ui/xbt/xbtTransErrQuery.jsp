<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

  <SCRIPT src="xbtTransErrQuery.js"></SCRIPT>
	<%@include file="xbtTransErrQueryInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <title>保费收入查询</title>
</head>

<%
     //添加页面控件的初始化。
  GlobalInput tG =new GlobalInput();
     tG.Operator ="001";
     tG.ComCode = "8600";
     tG.ManageCom ="86";
    
	if( session.getValue("GI") == null ) {		
		session.putValue("GI", tG);
	}
//	System.out.println(String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom.length()));
	String temp =String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom);
%>

<body onload="initForm();initElementtype();">  
  <form action="./xbtTransErrQuerySave.jsp" method=post name=fm >
    <table class= common border=0 width=100%>
    	<tr>
				<td class= titleImg align= center>请输查询时间范围：</td>
			</tr>
    </table>
    <table class= common >
    	<tr class=common>
				<td class=title>开始时间</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=StartDay verify="开始时间|NOTNULL&DATE" elementtype=nacessary>
				</td>
				<td class=title>结束时间</td>
				<td class=title>
					<Input class="coolDatePicker" dateFormat="short" name=EndDay verify="结束时间|NOTNULL&DATE" elementtype=nacessary>
				</td>
			</tr>
    </table>			
  
    <table class= common border=0 width=100%>
    	<tr>
				<td class= titleImg align= center>请输入投保单号：</td>
			</tr>
    </table>
     	
    <table  class= common align=center>      	
	   <TR  class= common>
	     <td  class= title> 
			  投保单号
			</td>
	    <TD  class= input>
	        <Input class="input"  name=ProposalNo>
	    </TD>   
	   </TR>    
  	</table>
    <table class= common border=0 width=100%>
    	<tr>
		    <td class= titleImg align= center>或输入信保通代理网点信息作为查询条件：</td>
	 		</tr>
    </table>
  <table  class= common align=center>     
   <TR  class= common>
     <td  class= title> 
		  银行代码
		</td>
    <TD  class= input>
      <Input name=BankCode class='codeno' ondblclick="return showCodeList('banknum',[this,BankCodeNamez],[0,1]);" onkeyup="return showCodeListKey('bankbranchtype',[this,BankCodeNamez],[0,1]);"><input name=BankCodeNamez class='codename' readonly=true>
    </TD>   
     <td  class= title> 
		  地区代码
		</td>
    <TD  class= input>
        <Input class="input"  name=BankNode>
    </TD> 
  </TR>    
  <TR>
     <td  class= title> 
		  分支代码
		</td>
    <TD  class= input>
        <Input class="input"  name=BankBranch>
    </TD>      
   </TR>
        
    
    <TD>
    	<input type=hidden  name="Mng" value="<%=temp%>">    			
    </TD>      
      
  </table>
  <table  class= common>
  <TR class= common> 
		<TD  class= input width="26%">
			<input class= cssbutton type=Button value="查 询" onclick="easyQueryClick3();">
		</TD>			
	</TR>
	</table>

  <table class=common>
    <tr class=common>
      <td text-align:left colSpan=1>
        <span id="spanErrQueryGrid">        
        	</span>
      </td>
    </tr>
  </table>



<center>      
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">           
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">      
</center> 

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>