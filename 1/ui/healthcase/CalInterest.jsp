<%
//Name    ：CalInterest.jsp
//Function：结算利息的处理界面
//Author   :LiuYansong
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
    <%@page import="com.sinosoft.utility.*"%>
    <%@page import="com.sinosoft.lis.schema.*"%>
    <%@page import="com.sinosoft.lis.vschema.*"%>
    <%@page import="com.sinosoft.lis.db.*"%>
    <%@page import="com.sinosoft.lis.vdb.*"%>
    <%@page import="com.sinosoft.lis.sys.*"%>
    <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.llhealthcase.*"%>
    <%@page import="com.sinosoft.lis.vbl.*"%>
    <%@page contentType="text/html;charset=GBK" %>
<%

  LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
  String mGetDutyKind= "";
  String mRealPay = "";
  String tPolAccNo[] = request.getParameterValues("ClaimPolGridNo");
  String tRadio[] = request.getParameterValues("ClaimPolGridSel");
  boolean flag = false;
  int tSelNo =0 ;
  for (int index=0; index< tRadio.length;index++)
  {
    if(tRadio[index].equals("1"))
    {
      tSelNo = index;
      flag = true;
    }
  }
  String[] strPolNo       = request.getParameterValues("ClaimPolGrid1");//获取第一列的值
  String[] strRiskCode    = request.getParameterValues("ClaimPolGrid2");//获取第二列的值
  String[] strGetDutyKind = request.getParameterValues("ClaimPolGrid6");//获得第六列的值
  String[] strRealPay     = request.getParameterValues("ClaimPolGrid8");//实际理算金额

  mLLCasePolicySchema.setPolNo(strPolNo[tSelNo].trim());
  mLLCasePolicySchema.setRiskCode(strRiskCode[tSelNo].trim());
  mGetDutyKind = strGetDutyKind[tSelNo].trim();
  mRealPay = strRealPay[tSelNo].trim();
  System.out.println("保单号码是"+mLLCasePolicySchema.getPolNo());
  System.out.println("险种代码是"+mLLCasePolicySchema.getRiskCode());
  System.out.println("责任代码是"+mGetDutyKind);

  CErrors tError = null;
  String transact = "CalInte";

  String tRela  = "";
  String FlagStr = "";
  String Content = "";

  VData tVData = new VData();
  CalInterestUI mCalInterestUI = new CalInterestUI();
  try
  {
    tVData.addElement(mGetDutyKind);
    tVData.addElement(mRealPay);
    tVData.addElement(mLLCasePolicySchema);
    mCalInterestUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (FlagStr=="")
  {
    tError = mCalInterestUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 计算成功";
      FlagStr = "Succ";
      tVData.clear();
      tVData = mCalInterestUI.getResult();
      //将计算的结果显示在界面上的过程
      LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
      mLCInsureAccSet.set((LCInsureAccSet)tVData.getObjectByObjectName("LCInsureAccSet",0));
      System.out.println("结束的数据有"+mLCInsureAccSet.size());
      for(int mCount = 1;mCount<=mLCInsureAccSet.size();mCount++)
      {
        LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
        tLCInsureAccSchema.setSchema(mLCInsureAccSet.get(mCount));
        %>
          <script language="javascript">
             parent.fraInterface.PolAccGrid.addOne("PolAccGrid")
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,12,"<%=tLCInsureAccSchema.getPolNo()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,2,"<%=tLCInsureAccSchema.getInsuAccNo()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,3,"<%=tLCInsureAccSchema.getAccType()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,4,"<%=tLCInsureAccSchema.getInsuAccBala()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,5,"<%=tLCInsureAccSchema.getSumPaym()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,6,"<%=tLCInsureAccSchema.getSumPay()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,10,"<%=tLCInsureAccSchema.getGrpPolNo()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,11,"<%=tLCInsureAccSchema.getContNo()%>");
             parent.fraInterface.PolAccGrid.setRowColData(<%=mCount-1%>,1,"<%=mLLCasePolicySchema.getPolNo()%>");
             parent.fraInterface.emptyUndefined();
          </script>
				<%
      }
    }
    else
    {
      Content = " 计算失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  else
  {
    Content = " 计算失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
 </script>
</html>