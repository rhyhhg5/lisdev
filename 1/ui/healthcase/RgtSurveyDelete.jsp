<%
//程序名称：RgtSurveyDelete.jsp
//程序功能：执行调查报告的删除操作
//创建日期：2002-11-23
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
//备注:数据传递都采取Schema的形式
%>
  <!--用户校验类-->
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Delete页面");

  //立案信息
  LLSurveySchema tLLSurveySchema = new LLSurveySchema();
  LLRegisterSet tLLRegisterSet = new LLRegisterSet();

  System.out.println("开始1");

  RgtSurveyDeleteUI tRgtSurveyDeleteUI   = new RgtSurveyDeleteUI();

  System.out.println("开始2");

  //输出参数
  CErrors tError = null;
	String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "DELETE";

  System.out.println("开始4");
  //获取立案信息
  tLLSurveySchema.setClmCaseNo(request.getParameter("CaseNo"));
  tLLSurveySchema.setType(request.getParameter("Type"));
  tLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));
  tLLSurveySchema.setRgtNo(request.getParameter("RgtNo"));
  System.out.println("开始6");

  try
  {
  	// 准备传输数据 VData
    VData tVData = new VData();

    System.out.println("开始10");
    //保存报案信息

   	tVData.addElement(tLLSurveySchema);
   	System.out.println("开始11"+transact);

    if (tRgtSurveyDeleteUI.submitData(tVData,transact))
		{
			System.out.println("开始12"+transact);
		}

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  System.out.println("开始13");
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tRgtSurveyDeleteUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 删除成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 删除失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  System.out.println("结束");
  }
  //添加各种预处理

%>
<html>
<script language="javascript">
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>