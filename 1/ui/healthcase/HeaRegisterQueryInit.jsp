<%
//Name    ：RegisterQueryInit.jsp
//Function：立案－查询列表的初始化程序
//Author  ：LiuYansong
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
//单击时查询
function RegisterDetailClick(cObj)
{
  var ex,ey;
  ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  divDetailInfo.style.left=ex;
  divDetailInfo.style.top =ey;
  divDetailInfo.style.display ='';
}

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initRegisterGrid();
  }
  catch(re)
  {
    alert("RegisterQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 报案信息列表的初始化
function initRegisterGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="立案号码";    	//列名
    iArray[1][1]="150px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="团单号码";         			//列名
    iArray[2][1]="150px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="投保单位代码";         			//列名
    iArray[3][1]="200px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="投保单位名称";         			//列名
    iArray[4][1]="150px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="立案日期";         		//列名
    iArray[5][1]="100px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[6]=new Array();
    iArray[6][0]="立案申请人名称";         		//列名
    iArray[6][1]="100px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用


    RegisterGrid = new MulLineEnter( "fm" , "RegisterGrid" );
    RegisterGrid.mulLineCount = 10;
    RegisterGrid.displayTitle = 1;
    RegisterGrid.canSel=1;
    RegisterGrid.locked = 1;
    RegisterGrid.loadMulLine(iArray);
    RegisterGrid.detailInfo="单击显示详细信息";
    RegisterGrid.detailClick=RegisterDetailClick;
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>