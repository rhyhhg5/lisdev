/******************************************************************************
 * Name    ：QueryDisease.js
 * Function:代码查询的处理程序
 * Author  :LiuYansong
 * Date    :2003-10-14
 */
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
function submitForm()
{
  initQueryCodeGrid();
  if(fm.CodeType.value==""||fm.CodeType.value==null)
  {
    alert("请您录入要查询的号码类型！");
    return;
  }
  if((fm.QueryName.value==""||fm.QueryName.value==null)
     &&(fm.SpellCode.value==""||fm.SpellCode.value==null)
     &&(fm.QueryCode.value==""||fm.QueryCode.value==null))
  {
    alert("拼音简称、名称和代码不能同时为空，请您录入正确的查询详细！");
    return;
  }

  fm.action = "./QueryDiseaseOut.jsp";
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function displayQueryResult(strResult)
{
  var filterArray          = new Array(1,3,2);
  turnPage.strQueryResult  = strResult;
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.useSimulation   = 1;
  turnPage.pageDisplayGrid = QueryCodeGrid;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }
}
function returnParent()
{
  tRow=QueryCodeGrid.getSelNo();
  if(tRow==0)
  {
    alert("请您先选择要返回的数据！");
    return;
  }
  clearData();
  tCol=2;
  var tCode = QueryCodeGrid.getRowColData(tRow-1,1);
  var tName = QueryCodeGrid.getRowColData(tRow-1,2);
  var rowNum = top.opener.DiseaseGrid.mulLineCount;
  top.opener.DiseaseGrid.addOne("DiseaseGrid");
  top.opener.DiseaseGrid.setRowColData(rowNum,1,tCode);
  top.opener.DiseaseGrid.setRowColData(rowNum,2,tName);
  top.close();
}

function clearData()
{

}
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}