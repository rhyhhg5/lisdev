/*******************************************************************************
 * Name     :SecondUWInput.jsp
 * Function :
 * Date     :2003-08-01
 * Author   :LiuYansong
*/
var showInfo;
var mDebug="1";
/*******************************************************************************
 * Name     :UWSave();
 * Function :对要进行二次核保的保单进行加锁
 * Author   :LiuYansong
 * Date     :2003-8-4
 */
function UWSave()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action = './SecondUWSave.jsp';
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //  showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
  }
}
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
/******************************************************************************
 * Name     :showUWInfo();
 * Function :显示进行二次核保的保单信息
 * Author   :LiuYansong
 * Date     :2003-8-4
 */
function showUWInfo()
{
  fm.action = "./SecondShowInfo.jsp";
  fm.submit();
}
function showSecondUWInfo()
{
	//二次核保核解约暂停的函数
	fm.action = './InitSecondInfo.jsp';
	fm.submit();
}