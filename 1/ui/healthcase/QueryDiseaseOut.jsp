
<%
/*******************************************************************************
 * Name    :QueryDiseaseOut.jsp
 * Function:1.向后台查询程序传递查询的条件
 *          2.接收查询结果并将其显示在MulLine列表中
 * Author  ：LiuYansong
 * Date    ：2003-10-14
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>
<%
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    System.out.println("开始执行.jsp");
    String strCodeType  = request.getParameter("CodeType");  //号码类型
    String strSpellCode = request.getParameter("SpellCode"); //拼音简称
    String strQueryCode = request.getParameter("QueryCode"); //代码
    String strQueryName = StrTool.unicodeToGBK(request.getParameter("QueryName")); //名称
    SSRS ssrs = new SSRS();

    VData tVData = new VData();
    tVData.addElement(strCodeType.trim());
    tVData.addElement(strSpellCode.trim());
    tVData.addElement(strQueryCode.trim());
    tVData.addElement(strQueryName.trim());
    
    QueryCodeUI tQueryCodeUI = new QueryCodeUI();
    if(!tQueryCodeUI.submitData(tVData,"QueryCode"))
    {
      Content = "查询失败，原因是："+tQueryCodeUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    }
    //查询成功接收数据
    else
    {
      tVData.clear();
      tVData = tQueryCodeUI.getResult();
      ssrs=(SSRS)tVData.getObjectByObjectName("SSRS",0);
      System.out.println("查询的结果是"+String.valueOf(ssrs.getMaxRow())+"条记录");
      int n = ssrs.getMaxRow();
      System.out.println("查询的结果集中的个数是"+n);
      String Strtest = ssrs.encode();
      System.out.println("Strtest==="+Strtest);
      %>
      <script language="javascript">
        try
        {
          parent.fraInterface.displayQueryResult('<%=Strtest%>');
        }
        catch(ex) {}
      </script>
      <%
        } // end of if

     //如果在Catch中发现异常，则不从错误类中提取错误信息
     if (FlagStr == "Fail")
     {
       tError = tQueryCodeUI.mErrors;
       if (!tError.needDealError())
       {
         Content = " 查询成功! ";
         FlagStr = "Succ";
       }
       else
       {
         Content = " 查询失败，原因是:" + tError.getFirstError();
         FlagStr = "Fail";
       }
     }
     System.out.println("------end------");
     System.out.println(FlagStr);
     System.out.println(Content);
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>