<%
//Name    ：RegisterSave.jsp
//Function：对立案主界面的信息保存
//Author   :LiuYansong
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
    <%@page import="com.sinosoft.utility.*"%>
    <%@page import="com.sinosoft.lis.schema.*"%>
    <%@page import="com.sinosoft.lis.vschema.*"%>
    <%@page import="com.sinosoft.lis.db.*"%>
    <%@page import="com.sinosoft.lis.vdb.*"%>
    <%@page import="com.sinosoft.lis.sys.*"%>
    <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.llhealthcase.*"%>
    <%@page import="com.sinosoft.lis.vbl.*"%>
    <%@page contentType="text/html;charset=GBK" %>

<%

    LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
    LLRegisterSet tLLRegisterSet = new LLRegisterSet();

    LLCaseSchema tLLCaseSchema=new LLCaseSchema();
    LLCaseSet tLLCaseSet=new LLCaseSet();

    LLRgtAffixSchema tLLRgtAffixSchema=new LLRgtAffixSchema();
    LLRgtAffixSet tLLRgtAffixSet=new LLRgtAffixSet();

    RegisterUI tRegisterUI   = new RegisterUI();
    CaseUI tCaseUI=new CaseUI();
    RgtAffixUI tRgtAffixUI=new RgtAffixUI();

    CErrors tError = null;
    String transact = "INSERT";

    String tRela  = "";
    String FlagStr = "";
    String Content = "";

    //处理转意字符
    String Path = application.getRealPath("config//Conversion.config");
		tLLRegisterSchema.setAccidentReason(StrTool.Conversion(request.getParameter("AccidentReason"),Path));
		tLLRegisterSchema.setAccidentCourse(StrTool.Conversion(request.getParameter("AccidentCourse"),Path));
		tLLRegisterSchema.setRemark(StrTool.Conversion(request.getParameter("Remark"),Path));
    tLLRegisterSchema.setAccidentSite(StrTool.Conversion(request.getParameter("AccidentSite"),Path));
    tLLRegisterSchema.setRgtReason(StrTool.Conversion(request.getParameter("RgtReason"),Path));

    tLLRegisterSchema.setRptNo(request.getParameter("RptNo"));
    tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
    tLLRegisterSchema.setCaseGetMode(request.getParameter("CaseGetMode"));
    tLLRegisterSchema.setRgtantName(request.getParameter("RgtantName"));
    tLLRegisterSchema.setRgtantSex(request.getParameter("Sex"));
    tLLRegisterSchema.setRelation(request.getParameter("Relation"));
    tLLRegisterSchema.setRgtantAddress(request.getParameter("RgtantAddress"));
    tLLRegisterSchema.setRgtantPhone(request.getParameter("RgtantPhone"));
    tLLRegisterSchema.setRgtDate(request.getParameter("RgtDate"));
    tLLRegisterSchema.setAccidentDate(request.getParameter("AccidentDate"));
    tLLRegisterSchema.setAgentCode(request.getParameter("AgentCode"));
    tLLRegisterSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLLRegisterSchema.setHandler(request.getParameter("Handler"));
    tLLRegisterSchema.setMngCom(request.getParameter("MngCom"));
    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setIDNo(request.getParameter("IDNo"));
    tLLRegisterSchema.setIDType(request.getParameter("IDType"));
    tLLRegisterSchema.setApplyType(request.getParameter("ApplyType"));
    tLLRegisterSchema.setHandler1(request.getParameter("Handler1"));
    tLLRegisterSchema.setHandler1Phone(request.getParameter("Handler1Phone"));
    tLLRegisterSchema.setAccName(request.getParameter("AccName"));
    tLLRegisterSet.add(tLLRegisterSchema);

    String[] tChk = request.getParameterValues("InpCaseGridChk");
    String[] strNumberCase=request.getParameterValues("CaseGridNo");
    String[] strCaseNo=request.getParameterValues("CaseGrid1");
    String[] strCustomerNo=request.getParameterValues("CaseGrid2");
    String[] strCustomerName=request.getParameterValues("CaseGrid3");
    String[] strCustomerSex=request.getParameterValues("CaseGrid4");
    String[] strIDType=request.getParameterValues("CaseGrid5");
    String[] strIDNo=request.getParameterValues("CaseGrid6");
    String[] strCustomerAge=request.getParameterValues("CaseGrid7");
    String[] strAccidentType=request.getParameterValues("CaseGrid8");
    String[] strSubRptNo=request.getParameterValues("CaseGrid9");

	  int intLength=0;
	  if(strNumberCase!=null)
	  intLength=strNumberCase.length;
    for(int i=0;i<intLength;i++)
    {
      if(tChk[i].equals("0"))
        continue;
      tLLCaseSchema=new LLCaseSchema();
      tLLCaseSchema.setCustomerNo(strCustomerNo[i]);
      tLLCaseSchema.setCustomerName(strCustomerName[i]);
      tLLCaseSchema.setCustomerSex(strCustomerSex[i]);
      tLLCaseSchema.setIDType(strIDType[i]);
      tLLCaseSchema.setIDNo(strIDNo[i]);
      tLLCaseSchema.setCustomerAge(strCustomerAge[i]);
      tLLCaseSchema.setIDType(strIDType[i]);
      tLLCaseSchema.setAccidentType(strAccidentType[i]);
      tLLCaseSchema.setSubRptNo(strSubRptNo[i]);
      tLLCaseSet.add(tLLCaseSchema);
    }

    String[] strNumberAffix=request.getParameterValues("AffixGridNo");
    String[] strAffixCode=request.getParameterValues("AffixGrid1");
    String[] strAffixNo=request.getParameterValues("AffixGrid3");
	 	intLength=0;
	  if(strNumberAffix!=null)
	  intLength=strNumberAffix.length;
  	for(int i=0;i<intLength;i++)
    {
			tLLRgtAffixSchema=new LLRgtAffixSchema();
			tLLRgtAffixSchema.setAffixCode(strAffixCode[i]);
			tLLRgtAffixSchema.setAffixNo(strAffixNo[i]);
			tLLRgtAffixSet.add(tLLRgtAffixSchema);

    }
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");

  	VData tVData = new VData();
    try
    {
   		tVData.addElement(tLLRegisterSet);
 			tVData.addElement(tLLCaseSet);
   		tVData.addElement(tLLRgtAffixSet);
   		tVData.addElement(tG);
      tRegisterUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = transact+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }

    if (FlagStr=="")
    {
      tError = tRegisterUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 保存成功";
        FlagStr = "Succ";
        tVData.clear();
        tVData = tRegisterUI.getResult();
        String a_RgtName="";
        String a_HandlerName = "";
        a_RgtName=(String)tVData.get(0);
        a_HandlerName=(String)tVData.get(1);
        LLRegisterSchema ttLLRegisterSchema = new LLRegisterSchema();
        LLRegisterBLSet yLLRegisterBLSet = new LLRegisterBLSet();
        yLLRegisterBLSet.set((LLRegisterBLSet)tVData.getObjectByObjectName("LLRegisterBLSet",0));
        ttLLRegisterSchema.setSchema(yLLRegisterBLSet.get(1));

        System.out.println("RegisterNo==="+ttLLRegisterSchema.getRgtNo());
        System.out.println("经办人是======"+ttLLRegisterSchema.getHandler());
%>
 <script language="javascript">
  parent.fraInterface.fm.all("Operator").value="<%=a_RgtName%>";
  parent.fraInterface.fm.all("RgtNo").value="<%=ttLLRegisterSchema.getRgtNo()%>";
  parent.fraInterface.fm.all("Handler").value = "<%=a_HandlerName%>";
	parent.fraInterface.fm.all("RgtDate").value = "<%=ttLLRegisterSchema.getRgtDate()%>";
	parent.fraInterface.fm.all("MngCom").value = "<%=ttLLRegisterSchema.getMngCom()%>";
	</script>
<%
 //反写分案号的语句
      LLCaseSet yLLCaseSet = new LLCaseSet();
      yLLCaseSet.set((LLCaseBLSet)tVData.getObjectByObjectName("LLCaseBLSet",0));
      int m = yLLCaseSet.size();
      for (int i =0;i<m;i++)
      {
      	LLCaseSchema ttLLCaseSchema = new LLCaseSchema();
      	ttLLCaseSchema.setSchema(yLLCaseSet.get(i+1));

      System.out.println("CaseNo=="+ttLLCaseSchema.getCaseNo());
      %>

	<script language="javascript">
		parent.fraInterface.CaseGrid.setRowColData(<%=i%>,1,"<%=ttLLCaseSchema.getCaseNo()%>");
	</script>
<%
    }
    		}
    		else
    		{
    				Content = " 保存失败，原因是:" + tError.getFirstError();
    				FlagStr = "Fail";
    		}
    }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
        </script>
</html>