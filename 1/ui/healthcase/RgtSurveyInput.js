//               该文件中包含客户端需要处理的函数和事件


var showInfo;
var mDebug="1";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action = './RgtSurveySave.jsp';
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
     parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	alert("您不能执行修改操作");
	return;
	/*
  if ((fm.SurveyFlag.value==1)||(fm.SurveyFlag.value==2))
  {
    alert ("您不能对本次调查报告进行修改！");
    return;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      showSubmitFrame(mDebug);
      fm.action = './RgtSurveyUpdate.jsp';
      fm.submit(); //提交
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
  */
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var varSrc = "&ClmCaseNo=" + fm.CaseNo.value;
  varSrc += "&Type=1";
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  var newWindow = window.open("./RgtSurveyQuery1.jsp?Interface=RgtSurveyQuery.jsp"+varSrc,"RgtSurveyQuery",'toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.SurveyFlag.value==1)||(fm.SurveyFlag.value==2))
		{
			alert ("您不能对本次调查报告进行删除！");
			return;
		}

  else
  {

  	if (confirm("您确实要删除该记录吗？"))
 		{
 			var i = 0;
 			var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或连接其他的界面";
 			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
 			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
 			showSubmitFrame(mDebug);
 			fm.action = './RgtSurveyDelete.jsp';
 			fm.submit();//提交
 			resetForm();
		}
		else
		{
			mOperate="";
			alert("您已经取消了修改操作！");
		}
	}

}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function submitForm1()
{
	if (fm.RptObjNo.value=="")
	{
		alert("请您收入要查询的号码类型和号码！");
 		return ;
  }
  else
  {
  	var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	initSubReportGrid();
  	fm.action = './ReportQueryOut1.jsp';
  	fm.submit(); //提交
  }
}
//完成按照”客户号“在lcpol中进行查询，显示该客户的保单明细
function showInsuredLCPol()
{
	var varInsuredNo;
	var varCount;
	varCount = SubReportGrid.getSelNo();
	varInsuredNo=SubReportGrid.getRowColData(varCount-1,1);
	if ((varInsuredNo=="null")||(varInsuredNo==""))
	{
		alert("客户号为空，不能进行查询操作！");
		return;
	}

	if (varCount==0)
    {
    		alert("请您选择一个客户号！");
    		return;
    }

	var varSrc = "&InsuredNo=" + SubReportGrid.getRowColData(SubReportGrid.getSelNo()-1,1);
	var newWindow = window.open("./FrameMainReportLCPol.jsp?Interface=ReportLCPolInput.jsp"+varSrc,"ReportLCPolInput");
}