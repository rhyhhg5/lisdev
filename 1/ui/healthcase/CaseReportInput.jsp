<html>
<%
//程序名称：银行代收对帐清单
//程序功能：
//创建日期：2003-3-25
//创建人  ：刘岩松程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="CaseReportInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CaseReportInit.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="./CaseReportPrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">
      	<table  class= common>
        	<TR  class= common>
          	<TD  class= title>	开始日期	</TD>
          	<TD> <input class="coolDatePicker" dateFormat="short" name="StartDate" > </TD>
          	<TD  class= title>	结束日期	</TD>
          	<TD>	<input class="coolDatePicker" dateFormat="short" name="EndDate" >	</TD>
		      </TR>
					<TR class= common>
					<TD  class= title>  管理机构 </TD>
          <TD>	<Input class= code name=Station ondblclick="return showCodeList('Station',[this]);" onkeyup="return showCodeListKey('Station',[this]);"> </TD>
					</tr>

					 <TR  class= common>
            <TD>	<input  type=button value="打印月报" onclick="PrintReport()"> </TD>
            <TD>	<input  type=button value="打印" onclick="PrintCaseReport()"> </TD>
          </TR>
       </table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>