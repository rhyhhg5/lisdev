<%
//程序名称：ReportUpdate.jsp
//程序功能：为"报案"模块的基本功能－修改－准备数据
//创建日期：
//创建人  ：刘岩松
//更新记录：
//更新人:
//更新日期:
//更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Update页面");

  //接收信息，并作校验处理。
  //输入参数
  //报案信息
  String a_Name="";
  LLReportSchema tLLReportSchema = new LLReportSchema();
  LLReportSet tLLReportSet=new LLReportSet();
  //分报案信息
  LLSubReportSchema tLLSubReportSchema=new LLSubReportSchema();
  LLSubReportSet tLLSubReportSet=new LLSubReportSet();

  ReportUpdateUI tReportUpdateUI   = new ReportUpdateUI();
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
	VData tVData = new VData();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  transact = request.getParameter("fmtransact");
  System.out.println("-------transact:"+transact);

    //获取报案信息(问题一：为什么//)
    String strRptNo=request.getParameter("RptNo");
    //转意字符的处理
    String Path = application.getRealPath("config//Conversion.config");
		tLLReportSchema.setAccidentReason(StrTool.Conversion(request.getParameter("AccidentReason"),Path));
		tLLReportSchema.setAccidentCourse(StrTool.Conversion(request.getParameter("AccidentCourse"),Path));
		tLLReportSchema.setRemark(StrTool.Conversion(request.getParameter("Remark"),Path));
    tLLReportSchema.setRptNo(request.getParameter("RptNo"));
    tLLReportSchema.setRptObj(request.getParameter("RptObj"));
    tLLReportSchema.setRptObjNo(request.getParameter("RptObjNo"));
    tLLReportSchema.setRptorName(request.getParameter("RptorName"));
    tLLReportSchema.setRptorAddress(request.getParameter("RptorAddress"));
    tLLReportSchema.setRptorPhone(request.getParameter("RptorPhone"));
    tLLReportSchema.setRelation(request.getParameter("Relation"));
    tLLReportSchema.setRptDate(request.getParameter("RptDate"));
    tLLReportSchema.setRptMode(request.getParameter("RptMode"));
    tLLReportSchema.setAccidentSite(request.getParameter("AccidentSite"));
    tLLReportSchema.setAccidentDate(request.getParameter("AccidentDate"));
    tLLReportSchema.setMngCom(request.getParameter("MngCom"));
    tLLReportSchema.setCustomerType(request.getParameter("PeopleType"));

    tLLReportSet.add(tLLReportSchema);

	  String[] strNumber=request.getParameterValues("SubReportGridNo");
	  String[] strCustomerNo=request.getParameterValues("SubReportGrid1");
	  String[] strCustomerName=request.getParameterValues("SubReportGrid2");
	  String[] strHospitalCode=request.getParameterValues("SubReportGrid3");
	  String[] strHospitalName=request.getParameterValues("SubReportGrid4");
	  String[] strInHospitalDate=request.getParameterValues("SubReportGrid5");
	  String[] strOutHospitalDate=request.getParameterValues("SubReportGrid6");
	  String[] strRemark=request.getParameterValues("SubReportGrid7");
	  String[] strSubRptNo=request.getParameterValues("SubReportGrid8");
	  String[] strAccidentType=request.getParameterValues("SubReportGrid9");
    int intLength=0;
	  if(strNumber!=null)
	  intLength=strNumber.length;
		for(int i=0;i<intLength;i++)
	  {
	  	tLLSubReportSchema=new LLSubReportSchema();
	  	tLLSubReportSchema.setCustomerNo(strCustomerNo[i]);
	  	tLLSubReportSchema.setCustomerName(strCustomerName[i]);
	  	tLLSubReportSchema.setHospitalCode(strHospitalCode[i]);
	  	tLLSubReportSchema.setHospitalName(strHospitalName[i]);
	  	tLLSubReportSchema.setInHospitalDate(strInHospitalDate[i]);
	  	tLLSubReportSchema.setOutHospitalDate(strOutHospitalDate[i]);
	  	tLLSubReportSchema.setRemark(strRemark[i]);
			tLLSubReportSchema.setSubRptNo(strSubRptNo[i]);
			tLLSubReportSchema.setAccidentType(strAccidentType[i]);
			tLLSubReportSet.add(tLLSubReportSchema);
			System.out.println("SubRptNO="+strSubRptNo[i]);
	  }

		System.out.println("开始9");
	  try
	  {
		   // 准备传输数据 VData

		   tVData.addElement(tLLReportSchema);
		   tVData.addElement(tLLSubReportSet);
		   tVData.addElement(tG);
		   tReportUpdateUI.submitData(tVData,transact);

    } catch(Exception ex) {
	    Content = transact+"失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }

	  //如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr=="")
	  {
	    tError = tReportUpdateUI.mErrors;
	    if (!tError.needDealError())
	    {
	      Content = " 修改成功";
	    	FlagStr = "Succ";
	    	tVData.clear();
	    	tVData = tReportUpdateUI.getResult();
	    	a_Name=(String)tVData.get(0);
	    	LLReportSchema yyLLReportSchema = new LLReportSchema();
	    	yyLLReportSchema.setSchema((LLReportSchema)tVData.getObjectByObjectName("LLReportSchema",0));
	    	%>
	    <script language="javascript">
	    parent.fraInterface.fm.all("Operator").value="<%=a_Name%>";
			parent.fraInterface.fm.all("MngCom").value="<%=yyLLReportSchema.getMngCom()%>";
			</script>
			<%

	    }
	    else
	    {
	    	Content = "ReportUpdate.jsp 修改失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  System.out.println("结束");
	  }
	  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>