<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
 * Name     :RegisterQueryOut.jsp
 * Function :根据"InsuredNo"在“LCPol”中查询出相应的数据
 * Date     :2003-7-18
 * Author   :LiuYansong
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    String tInsuredNo = request.getParameter("InsuredNo");
    
    String tInsuredName  = StrTool.unicodeToGBK(request.getParameter("CustomerName"));
    GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");

    VData tVData = new VData();
    tVData.addElement(tInsuredNo);
    tVData.addElement(tInsuredName);
		tVData.addElement(tG);
    RegisterQueryCasePolicyUI tRegisterQueryCasePolicyUI = new RegisterQueryCasePolicyUI();
    if (!tRegisterQueryCasePolicyUI.submitData(tVData,"QUERY||MAIN"))
    {
      Content = " 查询失败，原因是: " + tRegisterQueryCasePolicyUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    }
    else
    {
      tVData.clear();
      tVData = tRegisterQueryCasePolicyUI.getResult();
      LCPolSet mLCPolSet = new LCPolSet();
      mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
      int n = mLCPolSet.size();
      System.out.println("get Register "+n);

      String Strtest = "0|" + n + "^" + mLCPolSet.encode();
      System.out.println("QueryResult: " + Strtest);
    %>
    <script language="javascript">
    try
    {
      parent.fraInterface.CasePolicyGrid.clearData();
      parent.fraInterface.displayQueryResult('<%=Strtest%>');
    }
    catch(ex) {}
    </script>
  <%
    }
    if (FlagStr == "Fail")
    {
      tError = tRegisterQueryCasePolicyUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 查询成功! ";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 查询失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
    System.out.println("------end------");
    System.out.println(FlagStr);
    System.out.println(Content);
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>