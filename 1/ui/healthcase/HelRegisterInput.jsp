<html>
<%
//Name:RegisterInput.jsp
//Function：健康险立案界面的初始化程序
//Date：2004-08-12 17:44:28
//Author ：LiuYansong
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="HeaRegisterInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="HeaRgtInit.jsp"%>
</head>
<body  onload="initForm();">
  <form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table class= common>
      <TR class = common>
        <TD class=input> <input class=common type=button value="查询立案中的案件" onclick="QueryRgtInfo()"> </TD>
        <TD class=input> <input class=common type=button value="录入个人信息" onclick="submitFormSurvery()"> </TD>
        <TD class=input> <input class=common type=button value="确认团单录入完毕" onclick="submitFormSurvery()"> </TD>
        <TD class=input> <input class=common  type=button value="查询已立案的案件" onclick="PLPsqs()"> </td>
      </TR>
    </table>

    <table >
      <TR >
        <td class= titleImg> 案件信息 </TD>
    	</TR>
    </table>

    <table class=common>
      <TR class=common >
        <TD  class= title > 立案号 </TD>
        <TD  class= input>  <Input class= "readonly" readonly name=RgtNo > </TD>
        <TD  class= title > 理赔状态 </TD>
        <TD  class= input> <Input class="readonly" readonly name=ClmState > </TD>
      </TR>

      <TR class= common>
        <TD class= title> 立案受理人 </TD>
        <TD class = input> <Input class= "readonly" readonly name= RgtName > </TD>
        <TD class= title> 立案受理日期 </TD>
        <TD class = input> <Input class= "readonly" readonly name= RgtDate > </TD>
      </TR>

      <TR class= common>
        <TD class= title> 保单管理机构 </TD>
        <TD class = input> <Input class= "readonly" readonly name= PManageCom > </TD>
        <TD class= title> 理算受理人 </TD>
        <TD class = input> <Input class= "readonly" readonly name= ClmName > </TD>
      </TR>
    </TABLE>
    <table >
      <TR >
        <td class= titleImg> 团单信息 </TD>
      </TR>
    </table>

    <table class=common>
      <TR class=common >
        <TD  class= title > 团单号码 </TD>
        <TD  class= input>  <Input class= "common"  name=GrpPolNo > </TD>
        <TD class=input> <input class=common type=button value="查    询" onclick="QueryGrppol()"> </TD>
      </TR>

      <TR class=common>
        <TD  class= title > 投保单位代码 </TD>
        <TD  class= input> <Input class="readonly" readonly name=AppntName > </TD>
        <TD  class= title > 投保单位名称 </TD>
        <TD  class= input> <Input class="readonly" readonly name=RiskName > </TD>
      </TR>
      
      <TR class=common>
        <TD  class= title > 险种代码 </TD>
        <TD  class= input> <Input class="readonly" readonly name=AppntName > </TD>
        <TD  class= title > 险种名称 </TD>
        <TD  class= input> <Input class="readonly" readonly name=RiskName > </TD>
      </TR>

      <TR class= common>
        <TD class= title> 立案申请人姓名 </TD>
        <TD class = input> <Input class= "common"  name=ApplyName  > </TD>
        <TD class= title> 立案申请人电话 </TD>
        <TD class = input> <Input class= "common"  name= ApplyTel > </TD>
      </TR>

      <TR class= common>
        <TD class= title> 申请权利人证件类型 </TD>
        <TD class= input> <Input class="code" name=ApplyIDType ondblclick="return showCodeList('IDType',[this]);"onkeyup="return showCodeListKey('IDType',[this]);" > </TD>
        <TD class= title> 申请权利人证件号码 </TD>
        <TD class= input> <Input class= common name=ApplyIDNo> </TD>
      </TR>

      <TR class= common>
        <TD class= title>  立案申请人地址 </TD>
        <TD class = input> <Input class= "common"  name= ApplyAdd > </TD>
        <TD class= title>  立案申请人邮编 </TD>
        <TD class = input> <Input class= "common"  name= ApplyPost > </TD>
      </TR>

      <TR class=common>
        <td class= common> <input name="chkPrt" type="checkbox" checked>统一填写客户信息</td>
      </TR>

      <TR class= common>
        <TD class= title>  出险日期 </TD>
        <TD class = input> <Input class= "common"  name= AccidentDate > </TD>
        <TD class= title> 给付金领取方式 </TD>
        <TD class=input>
          <Input class="code" name=CaseGetMode verify="方式|NOTNULL"
            CodeData="0|^1|现金^4|银行转账"
            ondblClick="showCodeListEx('ModeSelect_1',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('ModeSelect_1',[this],[0,1]);">
        </TD>
      </TR>

      <TR class= common >
        <TD  class= title> 开户银行代码 </TD>
        <TD  class= input> <Input class="code" name=BankCode verify="银行代码|notnull&code:BankCode" ondblclick="return showCodeList('Bank',[this, BankName], [0, 1]);" onkeyup="return showCodeListKey('Bank', [this, BankName], [0, 1]);"> </TD>
        <TD  class= title> 开户银行名称 </TD>
        <TD  class= input> <Input class= "readonly" readonly name= BankName verify="银行名称|notnull&len<=12" > </TD>
      </TR>

      <TR class= common>
        <TD  class= title> 帐号 </TD>
        <TD  class= input> <Input class=common name=BankAccNo> </TD>
        <TD  class= title> 确认账号 </TD>
        <TD  class= input> <Input class= common name=RgtantMobile > </TD>
      </TR>

      <TR class= common>
        <TD  class= title> 户名 </TD>
        <TD  class= input> <Input class=common name=AccName > </TD>
      </TR>
    </TABLE>

		<table class= common>
      <TR  class= common>
        <TD  class= title> 事故经过描述 </TD>
      </TR>

    	<TR class= common>
       	<TD  class= input> <textarea name="AccidentReason" cols="110%" rows="3" witdh=25% class="common"  ></textarea> </TD>
      </TR>

      <TR class= common>
        <TD  class= title> 备注 </TD>
      </TR>
      <TR class= common>
        <TD  class= common> <textarea name="Remark" cols="110%" rows="3" witdh=25% class="common" ></textarea></TD>
      </TR>

      <TR class= common>
        <TD  class= title> 案件撤销原因 </TD>
      </TR>
      <TR>
        <TD  class= input> <textarea name="RgtReason" cols="110%" rows="3" witdh=25% class="common"></textarea></TD>
      </TR>
  </table>
  <table class=common>
      <TR class=common>
        <TD class=input> <input class=common type=button value="新增" onclick="SaveRgtInfo()"> </TD>
        <TD class=input> <input class=common type=button value="修改" onclick="CaseCancel()"> </TD>
        <TD class=input> <input class=common  type=button value="重置" onclick="PLPsqs()"> </td>
        <TD class=input> <input class=common  type=button value="案件撤销" onclick="PLPsqs()"> </td>

        
      </TR>
  </TABLE>
  </form>
</body>
</html>