<%
//程序名称:ICaseInit.jsp
//程序功能：初始化"立案"模块的“分案诊疗明细”
//创建日期：
//创建人  ：刘岩松
//更新记录：  
//更新人:刘岩松    
//更新日期:    
//更新原因/内容:
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
var turnPage = new turnPageClass();
function initInpBox()
{ 
  try
  {                                   
//    fm.all('CaseNo').value = '';
//    fm.all('AffixSerialNo').value = '';
//    fm.all('HospitalCode').value = '';
//    fm.all('HospitalName').value = '';
//    fm.all('CustomerNo').value = '';
//    fm.all('CustomerName').value = '';
//    fm.all('AccidentType').value = '';
//    fm.all('CureNo').value = '';
//    fm.all('DiseaseCode').value = '';
//    fm.all('DiseaseName').value = '';
//    fm.all('Diagnosis').value = '';
    //fm.all('ClmFlag').value = '';
    //fm.all('ManageCom').value = '';
    //fm.all('Operator').value = '';
    //fm.all('ReceiptNo').value = '';
    //fm.all('InHospitalDate').value = '';
    //fm.all('OutHospitalDate').value = '';
    //fm.all('InHospitalDays').value = '';

  }
  catch(ex)
  {
    alert("在ICaseCureInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ICaseCureInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initICaseCureGrid();
    showCaseCureInfo();
  }
  catch(re)
  {
    alert("ICaseCureInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//立案分案诊疗信息
function initICaseCureGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;
      iArray[0][4]="SerialNo";              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="医院代码";    	//列名
      iArray[1][1]="0px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="HospitalCode";
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
 			iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
 			iArray[1][9]="医院代码|NOTNULL";
 
      
      iArray[2]=new Array();
      iArray[2][0]="医院名称";    	//列名
      iArray[2][1]="170px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="HospitalName";
      
      iArray[3]=new Array();
      iArray[3][0]="费用代码";    	//列名
      iArray[3][1]="70px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;  
      iArray[3][4]="FeeItemCode";
      iArray[3][5]="3|4";
      iArray[3][6]="0|1";            			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="费用名称";    	//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="申请赔付金额";    	//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;
      
      iArray[6]=new Array();
      iArray[6][0]="实际赔付金额";    	//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
           
      iArray[7]=new Array();
      iArray[7][0]="门诊/住院号码";    	//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][4]="CureNo";
      
      iArray[8]=new Array();
      iArray[8][0]="疾病代码";    	//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][4]="DiseaseCode";
      iArray[8][5]="8|9";              	                //引用代码对应第几列，'|'为分割符
 			iArray[8][6]="0|1";              	        //上面的列中放置引用代码中第几位值
 			iArray[8][9]="疾病代码|NOTNULL";
       
      iArray[9]=new Array();
      iArray[9][0]="疾病名称";         			//列名
      iArray[9][1]="150px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][4]="DiseaseName";

      iArray[10]=new Array();
      iArray[10][0]="诊断";         			//列名
      iArray[10][1]="250px";            		//列宽
      iArray[10][2]=60;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][4]="diagnosis";

      ICaseCureGrid = new MulLineEnter( "fm" , "ICaseCureGrid" ); 
      //这些属性必须在loadMulLine前
      ICaseCureGrid.mulLineCount = 1;   
      ICaseCureGrid.displayTitle = 1;
      ICaseCureGrid.locked = 1;
      ICaseCureGrid.canSel = 0;
      ICaseCureGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ICaseCureGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }
</script>