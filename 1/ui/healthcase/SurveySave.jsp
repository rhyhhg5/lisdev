<%
//程序名称：LLSurveyInput.jsp
//程序功能：
//创建日期：2002-07-21 21:03:23
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%

  //接收信息，并作校验处理。
  //输入参数
  LLSurveySchema tLLSurveySchema   = new LLSurveySchema();
  LLSurveySet tLLSurveySet=new LLSurveySet();
  SurveyUI tSurveyUI   = new SurveyUI();
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
	
	
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact ;
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String tOpt = request.getParameter("Opt");
  //接收数据
  String strCaseNo=request.getParameter("CaseNo");             //立案号
  String strSubRptNo=request.getParameter("SubReportNo");      //报案号
  String strClmCaseNo;
  String strType;
  if(!strCaseNo.equals(""))
  {
  	strClmCaseNo=strCaseNo;     //立案号
  	strType="1";
  }
  else
  {
  	strClmCaseNo=strSubRptNo;   // 报案号
  	strType="0";
  }
  String strCustomerNo=request.getParameter("CustomerNo");
  String strCustomerName=request.getParameter("CustomerName");
  String strSerialNo=request.getParameter("SerialNo");
  String strSurveySite=request.getParameter("SurveySite");
  String strSurveyStartDate=request.getParameter("SurveyStartDate");
  String strSurveyEndDate=request.getParameter("SurveyEndDate");
  String strContent=request.getParameter("Content");
  String strResult=request.getParameter("Result");
  System.out.println("SerialNo===");
  System.out.println(strSerialNo);

  tLLSurveySchema.setClmCaseNo(strClmCaseNo.trim());
  tLLSurveySchema.setType(strType.trim());
  tLLSurveySchema.setCustomerNo(strCustomerNo.trim());
  tLLSurveySchema.setCustomerName(strCustomerName.trim());
  tLLSurveySchema.setSerialNo(strSerialNo.trim());
  tLLSurveySchema.setSurveySite(strSurveySite.trim());
  tLLSurveySchema.setSurveyStartDate(strSurveyStartDate.trim());
  tLLSurveySchema.setSurveyEndDate(strSurveyEndDate.trim());
  tLLSurveySchema.setContent(strContent.trim());
  tLLSurveySchema.setresult(strResult.trim());
  tLLSurveySet.add(tLLSurveySchema);
  System.out.println("strClmCaseNo=="+strClmCaseNo);
  System.out.println("strType=="+strType);
  System.out.println("strCustomerNo=="+strCustomerNo);
  System.out.println("strCustomerName=="+strCustomerName);
  System.out.println("strSerialNo=="+strSerialNo);
  System.out.println("strSurveyor=="+strSurveyor);
  System.out.println("strSurveySite=="+strSurveySite);
  System.out.println("strSurveyStartDate=="+strSurveyStartDate);
  System.out.println("strSurveyEndDate=="+strSurveyEndDate);
  System.out.println("strContent=="+strContent);
  System.out.println("strResult=="+strResult);
  
  
  
  if (tOpt.equals("insert")){
     transact = "INSERT";
  }
  else {
     transact = "UPDATE";
  }
  System.out.println("transact=="+transact);
  System.out.println("Type=="+strType);
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   //此处需要根据实际情况修改
   tVData.addElement(tLLSurveySet);
   tVData.addElement(tG);
   
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tSurveyUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
    System.out.println(Content);
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tSurveyUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

