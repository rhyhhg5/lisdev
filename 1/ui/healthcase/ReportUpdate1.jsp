<%
//程序名称：ReportUpdate.jsp
//程序功能：执行"修改操作"
//创建日期：2002-10-25
//创建人  ：刘岩松
//更新记录：
//更新人:
//更新日期:
//更新原因/内容:
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>
  <%@include file="../common/jsp/UsrCheck.jsp"%>



<%
　System.out.println("开始执行Update页面");

  //接收信息，并作校验处理。
  //输入参数

  LLReportSchema tLLReportSchema = new LLReportSchema();
  LLReportSet tLLReportSet = new LLReportSet();

  LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
  LLSubReportSet tLLSubReportSet = new LLSubReportSet();

  ReportUpdateUI tReportUpdateUI = new ReportUpdateUI();

  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");

  //输出参数
  CErrors tError = null;
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  transact = request.getParameter("fmtransact");
  System.out.println("-------transact:"+transact);

  //获取报案号
    String strRptNo=request.getParameter("RptNo");
    tLLReportSchema.setRptNo(request.getParameter("RptNo"));
    //后添加的
    VData tVData = new VData();
    tVData.addElement(tLLReportSchema);
    tVData.addElement(tLLSubReportSet);
    tLLReportSchema.setRptObj(request.getParameter("RptObj"));
    tLLReportSchema.setRptObjNo(request.getParameter("RptObjNo"));
    tLLReportSchema.setRptorName(request.getParameter("RptorName"));
    tLLReportSchema.setRptorAddress(request.getParameter("RptorAddress"));
    tLLReportSchema.setRptorPhone(request.getParameter("RptorPhone"));
    tLLReportSchema.setRptorMobile(request.getParameter("RptorMobile"));
    tLLReportSchema.setRelation(request.getParameter("Relation"));
    tLLReportSchema.setRptDate(request.getParameter("RptDate"));
    tLLReportSchema.setRptMode(request.getParameter("RptMode"));
    tLLReportSchema.setAccidentSite(request.getParameter("AccidentSite"));
    tLLReportSchema.setAccidentReason(request.getParameter("AccidentReason"));
    tLLReportSchema.setAccidentCourse(request.getParameter("AccidentCourse"));
    tLLReportSchema.setAccidentDate(request.getParameter("AccidentDate"));
    tLLReportSchema.setMngCom(request.getParameter("MngCom"));
    tLLReportSchema.setNoRgtReason(request.getParameter("NoRgtReason"));
    tLLReportSchema.setAgentGroup(request.getParameter("AgencyGroup"));
    tLLReportSchema.setAgentCode(request.getParameter("AgencyCode"));
    tLLReportSchema.setRemark(request.getParameter("Remark"));
    tLLReportSchema.setRgtFlag(request.getParameter("YesNo"));
    tLLReportSet.add(tLLReportSchema);
    System.out.println("立案信息获取成功！－－75");

    //获取分报案信息
    String[] strNumber=request.getParameterValues("SubReportGridNo");
    String[] strCustomerNo=request.getParameterValues("SubReportGrid1");
    String[] strCustomerName=request.getParameterValues("SubReportGrid2");
    String[] strHospitalName=request.getParameterValues("SubReportGrid4");
    String[] strInHospitalDate=request.getParameterValues("SubReportGrid5");
    String[] strOutHospitalDate=request.getParameterValues("SubReportGrid6");
    String[] strRemark=request.getParameterValues("SubReportGrid7");
    String[] strSubRptNo=request.getParameterValues("SubReportGrid8");
    System.out.println("分案信息获取成功！--85");

	  int intLength=strNumber.length;
	  for(int i=0;i<intLength;i++)
	  {
	  	tLLSubReportSchema=new LLSubReportSchema();
	  	tLLSubReportSchema.setCustomerNo(strCustomerNo[i]);
	  	tLLSubReportSchema.setCustomerName(strCustomerName[i]);
	  	tLLSubReportSchema.setHospitalName(strHospitalName[i]);
	  	tLLSubReportSchema.setInHospitalDate(strInHospitalDate[i]);
	  	tLLSubReportSchema.setOutHospitalDate(strOutHospitalDate[i]);
      tLLSubReportSchema.setRemark(strRemark[i]);
      if(strSubRptNo[i]==null||strSubRptNo[i].equals(""))
        tLLSubReportSchema.setSubRptNo("");
      else
        tLLSubReportSchema.setSubRptNo(strSubRptNo[i]);
      tLLSubReportSet.add(tLLSubReportSchema);
	  }
  try
  {
   	// 准备传输数据 VData
   	tVData.clear();
   	//保存报案信息
   	tVData.addElement(tLLReportSchema);
   	tVData.addElement(tLLSubReportSet);
   	tVData.addElement(tG);

     if (tReportUpdateUI.submitData(tVData,transact))
     {
       if (transact.equals("INSERT||MAIN"))
       {
         tVData.clear();
         tVData = tReportUpdateUI.getResult();
         LLReportSchema mLLReportSchema = new LLReportSchema();
         LLReportSet mLLReportSet=new LLReportSet();
         mLLReportSet=(LLReportSet)tVData.getObjectByObjectName("LLReportBLSet",0);
         mLLReportSchema=mLLReportSet.get(1);
   %>
   <SCRIPT language="javascript">
   parent.fraInterface.fm.all("RptNo").value ="<%=mLLReportSchema.getRptNo()%>";
   </SCRIPT>
   <%
     }
     }

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    	tError = tReportUpdateUI.mErrors;
    	if (!tError.needDealError())
    	{
      		Content = " 修改成功";
    		FlagStr = "Succ";
    	}
  else
  {
    	Content = " 修改失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
  }
}
  //添加各种预处理
  System.out.println("ReportUpdate.jsp执行结束！--154");

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

