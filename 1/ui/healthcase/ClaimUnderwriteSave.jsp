<%
//程序名称：LLClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数

  CErrors tError = null;
  String transact;
  //输出参数
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String tRela  = "";
  String FlagStr = "";
  String Content = "";



  ClaimUnderwriteUI tClaimUnderwriteUI   = new ClaimUnderwriteUI();
  ClaimUWDetailUI tClaimUWDetailUI=new ClaimUWDetailUI();
  String tOpt = request.getParameter("Opt");
  if (tOpt.equals("Autochk"))
  {
  	transact = "QUERY";
  	LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
  	//判断Radio被选中
  	String tClaimUWDetailNo[] = request.getParameterValues("ClaimUWDetailGridNo"); //序号字段
    String tRadio[] = request.getParameterValues("InpClaimUWDetailGridSel");
		//注意：radio字段的名字结构为Inp+GridName+Sel，这里GridName=ClaimUWDetailGrid
    boolean flag = false;
	int ClaimUWDetailCount = tClaimUWDetailNo.length;   //纪录的总行数
	int tSelNo =0 ;
	for (int i = 0; i < ClaimUWDetailCount; i++)
	{
		if (tRadio [i].equals("1"))  //如果该行被选中
		{
		    tSelNo = i;
		    flag = true;                     //置标记为真
		}
	}

	tLLClaimUnderwriteSchema.setClmNo(request.getParameter("ClmNo"));
	tLLClaimUnderwriteSchema.setRgtNo(request.getParameter("RgtNo"));
	String[] strPolNo=request.getParameterValues("ClaimUWDetailGrid1");
	tLLClaimUnderwriteSchema.setPolNo(strPolNo[tSelNo]);

	 //读取Session中的全局类
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");

	ClaimAutoChkUI tClaimAutoChkUI   = new ClaimAutoChkUI();
  	// 准备传输数据 VData
   	VData tVData = new VData();
	try
  	{
  		//此处需要根据实际情况修改
   		tVData.addElement(tLLClaimUnderwriteSchema);
   		tVData.addElement(tG);
   		//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   		tClaimAutoChkUI.submitData(tVData,transact);
	}
  	catch(Exception ex)
  	{
    		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
  	}
  	System.out.println("Calling is end");
  	if (FlagStr=="")
  	{
    		tError = tClaimAutoChkUI.mErrors;
    		if (!tError.needDealError())
    		{
      			Content = " 计算成功";
    			FlagStr = "Succ";
    		}
    		else
    		{
    			Content = " 计算失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		}
  	}
  	if (FlagStr == "Succ")
  	{

  		tVData.clear();
		tVData = tClaimAutoChkUI.getResult();
		LLClaimErrorSet tLLClaimErrorSet = new LLClaimErrorSet();
		System.out.println("tVData==="+tVData);
		tLLClaimUnderwriteSchema=(LLClaimUnderwriteSchema)tVData.getObjectByObjectName("LLClaimUnderwriteSchema",0);
		tLLClaimErrorSet=(LLClaimErrorSet)tVData.getObjectByObjectName("LLClaimErrorSet",0);

  %>
  		<script language="javascript">
		   		parent.fraInterface.ClaimUWDetailGrid.setRowColData(<%=tSelNo%>,5,"<%=tLLClaimUnderwriteSchema.getAutoClmDecision()%>");
				parent.fraInterface.ClaimErrorGrid.clearData();
		</script>
<%
		for (int i = 1;i<=tLLClaimErrorSet.size();i++)
		{
			LLClaimErrorSchema tLLClaimErrorSchema = new LLClaimErrorSchema();
			tLLClaimErrorSchema=tLLClaimErrorSet.get(i);
%>
		<script language="javascript">
			parent.fraInterface.ClaimErrorGrid.addOne();
			parent.fraInterface.ClaimErrorGrid.setRowColData(<%=i-1%>,1,"<%=tLLClaimUnderwriteSchema.getPolNo()%>");
			parent.fraInterface.ClaimErrorGrid.setRowColData(<%=i-1%>,2,"<%=tLLClaimErrorSchema.getClmNo()%>");
			parent.fraInterface.ClaimErrorGrid.setRowColData(<%=i-1%>,3,"<%=tLLClaimErrorSchema.getUWRuleCode()%>");
			parent.fraInterface.ClaimErrorGrid.setRowColData(<%=i-1%>,4,"<%=tLLClaimErrorSchema.getUWError()%>");
			parent.fraInterface.ClaimErrorGrid.setRowColData(<%=i-1%>,5,"<%=tLLClaimErrorSchema.getCurrValue()%>");
		</script>

<%
		}
	}
  }

  if (tOpt.equals("Manchk"))
  {
  	transact = "INSERT";
  	LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
  	//判断Radio被选中
  	String tClaimUWDetailNo[] = request.getParameterValues("ClaimUWDetailGridNo"); //序号字段
	String tRadio[] = request.getParameterValues("InpClaimUWDetailGridSel");
		//注意：radio字段的名字结构为Inp+GridName+Sel，这里GridName=ClaimUWDetailGrid
	boolean flag = false;
	int ClaimUWDetailCount = tClaimUWDetailNo.length;   //纪录的总行数
	int tSelNo =0 ;
	for (int i = 0; i < ClaimUWDetailCount; i++)
	{
		if (tRadio [i].equals("1"))  //如果该行被选中
		{
		    tSelNo = i;
		    flag = true;                     //置标记为真
		}
	}

	tLLClaimUnderwriteSchema.setClmNo(request.getParameter("ClmNo")); //赔案号
	tLLClaimUnderwriteSchema.setRgtNo(request.getParameter("RgtNo")); //立案号
	String[] strPolNo=request.getParameterValues("ClaimUWDetailGrid1");
	String[] strAutoClmDecision=request.getParameterValues("ClaimUWDetailGrid5");
	String[] strClmDecision=request.getParameterValues("ClaimUWDetailGrid6");
	tLLClaimUnderwriteSchema.setPolNo(strPolNo[tSelNo]); //保单号
	tLLClaimUnderwriteSchema.setAutoClmDecision(strAutoClmDecision[tSelNo]);
	tLLClaimUnderwriteSchema.setClmDecision(strClmDecision[tSelNo]); //核赔意见


	 //读取Session中的全局类
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	ClaimManChkUI tClaimManChkUI   = new ClaimManChkUI();
  	// 准备传输数据 VData
   	VData tVData = new VData();
	try
  	{
  		//此处需要根据实际情况修改
   		tVData.addElement(tLLClaimUnderwriteSchema);
   		tVData.addElement(tG);
   		//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   		tClaimManChkUI.submitData(tVData,transact);
	}
  	catch(Exception ex)
  	{
    		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
  	}
  	System.out.println("Calling is end");
  	if (FlagStr=="")
  	{
    		tError = tClaimManChkUI.mErrors;
    		if (!tError.needDealError())
    		{
      			Content = " 核赔成功";
    			FlagStr = "Succ";
    		}
    		else
    		{
    			Content = " 核赔失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		}
  	}
  	System.out.println("FlagStr=="+FlagStr);
  	if (FlagStr == "Succ")
  	{
  		System.out.println("33333");
  		tVData.clear();
  		System.out.println("111111");
		tVData = tClaimManChkUI.getResult();
		//tLLClaimUnderwriteSchema = new tLLClaimUnderwriteSchema();
		tLLClaimUnderwriteSchema=(LLClaimUnderwriteSchema)tVData.getObjectByObjectName("LLClaimUnderwriteSchema",0);

	}
  }

  if (tOpt.equals("Gf_ensure"))
  {
  	GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
    transact = "INSERT";
    LLClaimSchema tLLClaimSchema = new LLClaimSchema();
    tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));
    tLLClaimSchema.setRgtNo(request.getParameter("RgtNo"));
    System.out.println("GF_ensure.ClmNo==="+tLLClaimSchema.getClmNo());
    ClaimPayEnsureUI tClaimPayEnsureUI = new ClaimPayEnsureUI();
  	// 准备传输数据 VData
   	VData tVData = new VData();
	try
  	{
  		//此处需要根据实际情况修改
   		tVData.addElement(tLLClaimSchema);
   		tVData.addElement(tG);
   		//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   		System.out.println("add end");
   		tClaimPayEnsureUI.submitData(tVData,transact);
	}
  	catch(Exception ex)
  	{
    		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
  	}
  	System.out.println("Calling is end");
  	if (FlagStr=="")
  	{
    		tError = tClaimPayEnsureUI.mErrors;
    		if (!tError.needDealError())
    		{
      			Content = " 确认成功";
    			FlagStr = "Succ";
    			tVData.clear();
			tVData = tClaimPayEnsureUI.getResult();
    			LJAGetSet tLJAGetSet = new LJAGetSet();
  			tLJAGetSet.set((LJAGetSet)tVData.getObjectByObjectName("LJAGetSet",0));
  %>
  			<script language="javascript">
		   		parent.fraInterface.fm.all("GetNoticeNo").value="<%=tLJAGetSet.get(1).getActuGetNo()%>";

			</script>

  <%

    		}
    		else
    		{
    			Content = " 确认失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		}
  	}
  }


  if (tOpt.equals("Chk_ensure"))
  {
  	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	transact = "INSERT";
  	LLClaimSchema tLLClaimSchema = new LLClaimSchema();
  	tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));
  	LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
  	tLLClaimUWMainSchema.setClmNo(request.getParameter("ClmNo"));
  	System.out.println("fffffffffffffff");
  	tLLClaimUWMainSchema.setRgtNo(request.getParameter("RgtNo"));
  	String tCheck_Flag = request.getParameter("Check_Flag");
    String Path = application.getRealPath("config//Conversion.config");//转义字符的处理
    tLLClaimUWMainSchema.setRemark(StrTool.Conversion(request.getParameter("CheckRemark"),Path));

  	System.out.println("RgtNo=="+tLLClaimUWMainSchema.getRgtNo());
  	System.out.println("Chk_ensure.ClmNo==="+tLLClaimSchema.getClmNo());
	System.out.println("Chk_ensure.Remark==="+tLLClaimUWMainSchema.getRemark());
	ClaimUWEnsureUI tClaimUWEnsureUI = new ClaimUWEnsureUI();
  	// 准备传输数据 VData
   	VData tVData = new VData();
	try
  	{
  		//此处需要根据实际情况修改
   		tVData.addElement(tLLClaimSchema);
   		tVData.addElement(tLLClaimUWMainSchema);
   		tVData.addElement(tG);
   		//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   		System.out.println("add end");
   		tClaimUWEnsureUI.submitData(tVData,transact);
	}
  	catch(Exception ex)
  	{
    		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
  	}
  	System.out.println("Calling is end");
  	if (FlagStr=="")
  	{
    		tError = tClaimUWEnsureUI.mErrors;
    		if (!tError.needDealError())
    		{
      			Content = " 确认成功";
    			FlagStr = "Succ";
    			tVData.clear();
			tVData = tClaimUWEnsureUI.getResult();
			tLLClaimUWMainSchema = new LLClaimUWMainSchema();
			tLLClaimUWMainSchema=(LLClaimUWMainSchema)tVData.getObjectByObjectName("LLClaimUWMainSchema",0);
			  %>
  			<script language="javascript">
		   		parent.fraInterface.fm.all("MakeDate").value="<%=tLLClaimUWMainSchema.getMakeDate()%>";
		   		parent.fraInterface.fm.all("AppClmUWer").value="<%=tLLClaimUWMainSchema.getAppClmUWer()%>";

			</script>
<%


    		}
    		else
    		{
    			Content = " 确认失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		}
  	}
  }

  if (tOpt.equals("ReChk_ensure"))
  {
    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
    transact = "INSERT";
    LLClaimSchema tLLClaimSchema = new LLClaimSchema();
    tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));
    LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
    tLLClaimUWMainSchema.setClmNo(request.getParameter("ClmNo"));
    tLLClaimUWMainSchema.setRgtNo(request.getParameter("RgtNo"));
    String Path = application.getRealPath("config//Conversion.config");//转义字符的处理
    tLLClaimUWMainSchema.setRemark(StrTool.Conversion(request.getParameter("ReCheckRemark"),Path));
    tLLClaimUWMainSchema.setAppActionType(request.getParameter("AppActionType"));
    System.out.println("AppActionType:"+tLLClaimUWMainSchema.getAppActionType());
    System.out.println("Chk_ensure.Remark==="+tLLClaimUWMainSchema.getRemark());
    ClaimUWReEnsureUI tClaimUWReEnsureUI = new ClaimUWReEnsureUI();
    VData tVData = new VData();
    try
    {
     tVData.addElement(tLLClaimSchema);
     tVData.addElement(tLLClaimUWMainSchema);
     tVData.addElement(tG);
     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
     System.out.println("add end");
     tClaimUWReEnsureUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = transact+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
    System.out.println("Calling is end");
    if (FlagStr=="")
     {
     	tError = tClaimUWReEnsureUI.mErrors;
    	if (!tError.needDealError())
    	{
     		Content = "保存成功";
    		FlagStr = "Succ";
    		tVData.clear();
		tVData = tClaimUWReEnsureUI.getResult();
		tLLClaimUWMainSchema = new LLClaimUWMainSchema();
		tLLClaimUWMainSchema=(LLClaimUWMainSchema)tVData.getObjectByObjectName("LLClaimUWMainSchema",0);
		  %>
  		<script language="javascript">
		   		parent.fraInterface.fm.all("ReMakeDate").value="<%=tLLClaimUWMainSchema.getMakeDate()%>";
		   		parent.fraInterface.fm.all("ClmUWerRecheck").value="<%=tLLClaimUWMainSchema.getClmUWer()%>";
		</script>
<%
    	}
    	else
    	{
    		Content = "保存失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
     }

  }



%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>