<%
//程序名称：RegisterInit.jsp
//程序功能：
//创建日期：2002-07-21 17:44:28
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
      fm.all('DeclineType').value = '';
      fm.all('RelationNo').value = '';
      fm.all('Reason').value = '';
      fm.all('DeclineDate').value = '';
      fm.all('ArchiveNo').value = '';
      fm.all('Handler').value = '';
      fm.all('MngCom').value = '';
      fm.all('RgtNo').value = '';
      fm.all('Opt').value = '';

  }
  catch(ex)
  {
    alert("在ClaimDeclineInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");

  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在claimDeclineInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("ClaimDeclineInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//立案附件信息
function initAffixGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="顺序号";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="附件代码";         			//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="AffixCode"

      iArray[3]=new Array();
      iArray[3][0]="附件名称";         			//列名
      iArray[3][1]="200px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="附件号码";         		//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      AffixGrid = new MulLineEnter( "fm" , "AffixGrid" ); 
      //这些属性必须在loadMulLine前
      AffixGrid.mulLineCount = 3;   
      AffixGrid.displayTitle = 1;
      AffixGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //AffixGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }

//立案分案信息
function initCaseGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="拒赔标记";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="70px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="DeclineFlag"

      iArray[2]=new Array();
      iArray[2][0]="分案号";    	//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="出险人客户号";         			//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="出险人名称";         			//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="分报案号";         		//列名
      iArray[5][1]="200px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用


      
      CaseGrid = new MulLineEnter( "fm" , "CaseGrid" ); 
      //这些属性必须在loadMulLine前
      CaseGrid.mulLineCount = 3;   
      CaseGrid.displayTitle = 1;
      CaseGrid.canSel = 1;
      CaseGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //CaseGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }

</script>