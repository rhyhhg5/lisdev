<%
//程序名称：LLReportInput.jsp
//程序功能：
//创建日期：2002-10-21
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
System.out.println("开始执行Delete页面");

  //接收信息，并作校验处理。
  //输入参数
  //报案信息
  LLReportSchema tLLReportSchema   = new LLReportSchema();
  LLReportSet tLLReportSet=new LLReportSet();
  //分报案信息
  LLSubReportSchema tLLSubReportSchema=new LLSubReportSchema();
  LLSubReportSet tLLSubReportSet=new LLSubReportSet();
  ReportDeleteUI tReportDeleteUI   = new ReportDeleteUI();
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");


  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  transact = request.getParameter("fmtransact");
  System.out.println("-------transact:"+transact);

    //获取报案信息(问题一：为什么//)
    String strRptNo=request.getParameter("RptNo");
    tLLReportSchema.setRptNo(request.getParameter("RptNo"));
    tLLReportSchema.setRptorName(request.getParameter("RptorName"));
    tLLReportSchema.setRptorAddress(request.getParameter("RptorAddress"));
    tLLReportSchema.setRptorPhone(request.getParameter("RptorPhone"));
    tLLReportSchema.setRptorMobile(request.getParameter("RptorMobile"));
    tLLReportSchema.setRelation(request.getParameter("Relation"));
    tLLReportSchema.setRptDate(request.getParameter("RptDate"));
    tLLReportSchema.setRptMode(request.getParameter("RptMode"));
    tLLReportSchema.setAccidentSite(request.getParameter("AccidentSite"));
    tLLReportSchema.setAccidentReason(request.getParameter("AccidentReason"));
    tLLReportSchema.setAccidentCourse(request.getParameter("AccidentCourse"));
    tLLReportSchema.setAccidentDate(request.getParameter("AccidentDate"));
    tLLReportSchema.setMngCom(request.getParameter("MngCom"));
    tLLReportSchema.setRemark(request.getParameter("Remark"));
    tLLReportSet.add(tLLReportSchema);
	  String[] strNumber=request.getParameterValues("SubReportGridNo");
	  String[] strCustomerNo=request.getParameterValues("SubReportGrid1");
	  String[] strCustomerName=request.getParameterValues("SubReportGrid2");
	  String[] strHospitalName=request.getParameterValues("SubReportGrid4");
	  String[] strInHospitalDate=request.getParameterValues("SubReportGrid5");
	  String[] strOutHospitalDate=request.getParameterValues("SubReportGrid6");
	  String[] strRemark=request.getParameterValues("SubReportGrid7");
	  String[] strSubRptNo=request.getParameterValues("SubReportGrid8"); 
	  int intLength=strNumber.length;
	  for(int i=0;i<intLength;i++)
	  {
	  	tLLSubReportSchema=new LLSubReportSchema();
	  	tLLSubReportSchema.setCustomerNo(strCustomerNo[i]);
	  	tLLSubReportSchema.setCustomerName(strCustomerName[i]);
	  	tLLSubReportSchema.setHospitalName(strHospitalName[i]);
	  	tLLSubReportSchema.setInHospitalDate(strInHospitalDate[i]);
	  	tLLSubReportSchema.setOutHospitalDate(strOutHospitalDate[i]);
	  	tLLSubReportSchema.setRemark(strRemark[i]);
		tLLSubReportSet.add(tLLSubReportSchema);

System.out.println(" Debug inof");
System.out.println(" Remark info : " + strRemark[i]);
System.out.println(" InhospitalDate info : " + strInHospitalDate[i]);
	  }
System.out.println("开始9");
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   //保存报案信息
   tVData.addElement(tLLReportSchema);  
   tVData.addElement(tLLSubReportSet);  
   tVData.addElement(tG);
   
      if (tReportDeleteUI.submitData(tVData,transact))
	{		
			 if (transact.equals("INSERT||MAIN"))
		{
		    	tVData.clear();
		    	tVData = tReportDeleteUI.getResult();
	        	LLReportSchema mLLReportSchema = new LLReportSchema(); 
			LLReportSet mLLReportSet=new LLReportSet();
			mLLReportSet=(LLReportSet)tVData.getObjectByObjectName("LLReportBLSet",0);
			mLLReportSchema=mLLReportSet.get(1);
			%>
		<SCRIPT language="javascript">
			
			parent.fraInterface.fm.all("RptNo").value ="<%=mLLReportSchema.getRptNo()%>";
		</SCRIPT>
		<%
		}
	}

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tReportDeleteUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 删除成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 删除失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  System.out.println("结束");
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

