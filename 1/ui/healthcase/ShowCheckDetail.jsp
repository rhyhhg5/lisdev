<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ShowCheckDetail.jsp
//程序功能：返回对于保单和案件的审核和签批的明细信息
//创建日期：2003-12-23
//创建人  ：LiuYansong
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%

  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LLReportSchema mLLReportSchema = new LLReportSchema();//承载审核和签批结论的
  LLClaimUWDetailSet mLLClaimUWDetailSet = new LLClaimUWDetailSet();//承载保单核赔结论的
  String mRgtNo     = request.getParameter("RgtNo");
  String mClmUWNo   = request.getParameter("ClmUWNo");
  String mUpClmUWer = request.getParameter("UpClmUWer");
  System.out.println("开始执行ＳｈｏｗＣｈｅｃｋＤｅｔａｉｌ．ｊｓｐ");

  VData tVData = new VData();
  tVData.addElement(mRgtNo);
  tVData.addElement(mClmUWNo);
  tVData.addElement(mUpClmUWer);

  ShowCheckDetailUI tShowCheckDetailUI = new ShowCheckDetailUI();
  if (!tShowCheckDetailUI.submitData(tVData,"QUERY"))
	{
      Content = " 查询失败，原因是: " + tShowCheckDetailUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
    tVData = tShowCheckDetailUI.getResult();
    mLLClaimUWDetailSet.set((LLClaimUWDetailSet)tVData.getObjectByObjectName("LLClaimUWDetailSet",0));
    int n = mLLClaimUWDetailSet.size();
    System.out.println("保单核赔中共有"+n+"条记录");
    %>
    <script language = "javascript">
      top.opener.ClaimUWDetailGrid.clearData();
    </script>
    <%
    for(int i_count=1;i_count<=mLLClaimUWDetailSet.size();i_count++)
    {
     %>
      <script language="javascript">

        top.opener.ClaimUWDetailGrid.addOne("ClaimUWDetailGrid");
        top.opener.ClaimUWDetailGrid.setRowColData(<%=i_count-1%>,1,"<%=mLLClaimUWDetailSet.get(i_count).getPolNo()%>");
        top.opener.ClaimUWDetailGrid.setRowColData(<%=i_count-1%>,2,"<%=mLLClaimUWDetailSet.get(i_count).getRemark()%>");
        top.opener.ClaimUWDetailGrid.setRowColData(<%=i_count-1%>,3,"<%=mLLClaimUWDetailSet.get(i_count).getGetDutyKind()%>");
        top.opener.ClaimUWDetailGrid.setRowColData(<%=i_count-1%>,4,"<%=mLLClaimUWDetailSet.get(i_count).getRealPay()%>");
        top.opener.ClaimUWDetailGrid.setRowColData(<%=i_count-1%>,6,"<%=mLLClaimUWDetailSet.get(i_count).getContNo()%>");
        top.opener.ClaimUWDetailGrid.setRowColData(<%=i_count-1%>,7,"<%=mLLClaimUWDetailSet.get(i_count).getClmUWer()%>");
        top.opener.ClaimUWDetailGrid.setRowColData(<%=i_count-1%>,8,"<%=mLLClaimUWDetailSet.get(i_count).getMngCom()%>");
  			top.opener.emptyUndefined();
      </script>
     <%
    }
    mLLReportSchema.setSchema((LLReportSchema)tVData.getObjectByObjectName("LLReportSchema",0));
    String Path = application.getRealPath("config//Conversion.config");
    String t_Remark = (StrTool.Conversion(mLLReportSchema.getAccidentReason(),Path));
    String m_Remark = (StrTool.Conversion(mLLReportSchema.getAccidentCourse(),Path));

    %>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <script language="javascript">


      var tRemark = Conversion("<%= t_Remark%>");//存放签批退回意见ReCheckRemark
      var  mRemark = Conversion("<%= m_Remark%>");//存放审核意见CheckRemark
      top.opener.fm.all("ReCheckRemark").value = tRemark;
      top.opener.fm.all("CheckRemark").value = mRemark;
      top.opener.emptyUndefined();
    </script>
        <%
	}


  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tShowCheckDetailUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
out.println("<script language=javascript>");
out.println("top.close();");
out.println("</script>");
%>
