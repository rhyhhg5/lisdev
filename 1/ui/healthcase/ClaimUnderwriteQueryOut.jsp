<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ClaimUnderwriteQueryOut.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

System.out.println("---hahahahah---");
  // 保单信息部分
  LLClaimUnderwriteSchema tLLClaimUnderwriteSchema   = new LLClaimUnderwriteSchema();
  
    tLLClaimUnderwriteSchema.setClmUWNo(request.getParameter("ClmUWNo"));
    tLLClaimUnderwriteSchema.setClmNo(request.getParameter("ClmNo"));
    tLLClaimUnderwriteSchema.setSerialNo(request.getParameter("SerialNo"));
    
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLLClaimUnderwriteSchema);

  // 数据传输
  ClaimUnderwriteUI tClaimUnderwriteUI   = new ClaimUnderwriteUI();
	if (!tClaimUnderwriteUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tClaimUnderwriteUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tClaimUnderwriteUI.getResult();
		
		// 显示
		LLClaimUnderwriteSet mLLClaimUnderwriteSet = new LLClaimUnderwriteSet(); 
		mLLClaimUnderwriteSet.set((LLClaimUnderwriteSet)tVData.getObjectByObjectName("LLClaimUnderwriteBLSet",0));
		int n = mLLClaimUnderwriteSet.size();
		System.out.println("get ClaimUnderwrite "+n);
		for (int i = 1; i <= n; i++)
		{
		  	LLClaimUnderwriteSchema mLLClaimUnderwriteSchema = mLLClaimUnderwriteSet.get(i);
		   	%>
		   	<script language="javascript">
		   	  parent.fraInterface.ClaimUnderwriteGrid.addOne("ClaimUnderwriteGrid")
		   		parent.fraInterface.fm.ClaimUnderwriteGrid1[<%=i-1%>].value="<%=mLLClaimUnderwriteSchema.getClmUWNo()%>";
		   		parent.fraInterface.fm.ClaimUnderwriteGrid2[<%=i-1%>].value="<%=mLLClaimUnderwriteSchema.getClmNo()%>";
		   		parent.fraInterface.fm.ClaimUnderwriteGrid3[<%=i-1%>].value="<%=mLLClaimUnderwriteSchema.getSerialNo()%>";
		   		parent.fraInterface.fm.ClaimUnderwriteGrid4[<%=i-1%>].value="<%=mLLClaimUnderwriteSchema.getMngCom()%>";
		   		parent.fraInterface.fm.ClaimUnderwriteGrid5[<%=i-1%>].value="<%=mLLClaimUnderwriteSchema.getClmUWer()%>";
			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tClaimUnderwriteUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

