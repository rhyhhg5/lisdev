<%
/******************************************************************************
 * Name     ：CaseAccidentInput.jsp
 * Function :对事故/伤残界面的初始化
 * Author   :LiuYansong
 * Date     :2003-7-16
 */
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<script language="JavaScript">
var turnPage = new turnPageClass();
function initForm()
{
  try
  {
    initCaseGrid();
    initDiseaseGrid();
    initDeformityGrid();
    initDisplayFlag();//初始化显示的函数，该函数在CaseAccidentInput.js中
    showCaseInfo();
  }
  catch(re)
  {
    alert("CaseReceiptInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//事故明细
function initCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="意外事故代码";         			//列名
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=0;            			//列最大值
    iArray[1][3]=2;
    iArray[1][4]="";
    iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
    iArray[1][9]="意外事故代码|NOTNULL";

    iArray[2]=new Array();
    iArray[2][0]="意外事故名称";         			//列名
    iArray[2][1]="500px";            		//列宽
    iArray[2][2]=160;            			//列最大值
    iArray[2][3]=1;

    CaseGrid = new MulLineEnter( "fm" , "CaseGrid" );
    CaseGrid.mulLineCount = 0;
    CaseGrid.displayTitle = 1;
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}

function initDiseaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="疾病代码";         			//列名
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=0;            			//列最大值
    iArray[1][3]=2;
    iArray[1][4]="DiseaseCode";
    iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
    iArray[1][9]="疾病代码|NOTNULL";

    iArray[2]=new Array();
    iArray[2][0]="疾病名称";         			//列名
    iArray[2][1]="500px";            		//列宽
    iArray[2][2]=160;            			//列最大值
    iArray[2][3]=1;
    iArray[2][4]="DiseaseName"

    DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
    DiseaseGrid.mulLineCount = 0;
    DiseaseGrid.displayTitle = 1;
    DiseaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}

function initDeformityGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="残疾类别代码";         			//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=0;            			//列最大值
    iArray[1][3]=2;
    iArray[1][4] ="deformity";
    iArray[1][5]="1|2";
    iArray[1][6]="0|1";
    iArray[1][10]="ComCode";

    iArray[2]=new Array();
    iArray[2][0]="残疾类别名称";         			//列名
    iArray[2][1]="90px";            		//列宽
    iArray[2][2]=11;            			//列最大值
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="残疾代码";         			//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=0;            			//列最大值
    iArray[3][3]=2;
    iArray[3][4]="desc_wound";
    iArray[3][5]="3|4|5|6";              	                //引用代码对应第几列，'|'为分割符
    iArray[3][6]="0|2|4|4";              	        //上面的列中放置引用代码中第几位值
    iArray[3][9]="残疾代码|NOTNULL";
    iArray[3][15]="ComCode";
    iArray[3][17]="1";                             //依赖的列的值

    iArray[4]=new Array();
    iArray[4][0]="残疾名称";         			//列名
    iArray[4][1]="500px";            		//列宽
    iArray[4][2]=160;            			//列最大值
    iArray[4][3]=0;

    iArray[5]=new Array();
    iArray[5][0]="给付比例";         			//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=160;            			//列最大值
    iArray[5][3]=0;

    iArray[6]=new Array();
    iArray[6][0]="实际给付比例";
    iArray[6][1]="90px";
    iArray[6][2]=160;
    iArray[6][3]=1;

    iArray[7]=new Array();
    iArray[7][0]="备注";
    iArray[7][1]="150px";
    iArray[7][2]=160;
    iArray[7][3]=1;

    DeformityGrid = new MulLineEnter( "fm" , "DeformityGrid" );
    DeformityGrid.mulLineCount = 0;
    DeformityGrid.displayTitle = 1;
    DeformityGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}
</script>