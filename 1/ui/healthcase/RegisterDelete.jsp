<%
//程序名称：LLRegisterInput.jsp
//程序功能：
//创建日期：2002-10-25
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <!--用户校验类-->
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Delete页面");
  
  //立案信息
  LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
  LLRegisterSet tLLRegisterSet = new LLRegisterSet();
  
  //分立案信息
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  LLCaseSet tLLCaseSet=new LLCaseSet();
  
  //立案附件信息
  LLRgtAffixSchema tLLRgtAffixSchema = new LLRgtAffixSchema();
  LLRgtAffixSet tLLRgtAffixSet = new LLRgtAffixSet();
  
  System.out.println("开始1");
  
  RegisterDeleteUI tRegisterDeleteUI   = new RegisterDeleteUI();
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
  System.out.println("开始2");
  
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  
  System.out.println("开始4");
  
  transact = request.getParameter("fmtransact");
  
  System.out.println("开始5");
  System.out.println("-------transact:"+transact);

    //获取立案信息
    String strRgtNo=request.getParameter("RgtNo");
    tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
    tLLRegisterSet.add(tLLRegisterSchema);
  
    System.out.println("开始6");    
     
     try
      {
      // 准备传输数据 VData
      VData tVData = new VData();

      System.out.println("开始10");
      //保存报案信息
   
   tVData.addElement(tLLRegisterSchema);  
   tVData.addElement(tLLCaseSet);
   tVData.addElement(tLLRgtAffixSet);  
   
   tVData.addElement(tG);
   System.out.println("开始11"+transact);
   
      if (tRegisterDeleteUI.submitData(tVData,transact))
	{		
		System.out.println("开始12"+transact);
		 if (transact.equals("INSERT||MAIN"))
		{
		  
		    	tVData.clear();
		    	tVData = tRegisterDeleteUI.getResult();
		    	LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); 
			LLRegisterSet mLLRegisterSet=new LLRegisterSet();
			mLLRegisterSet=(LLRegisterSet)tVData.getObjectByObjectName("LLRegisterBLSet",0);
			mLLRegisterSchema=mLLRegisterSet.get(1);
			%>
		<SCRIPT language="javascript">
			
			parent.fraInterface.fm.all("RgtNo").value ="<%=mLLRegisterSchema.getRgtNo()%>";
		</SCRIPT>
		<%
		}
	}

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  System.out.println("开始13");
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tRegisterDeleteUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 删除成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 删除失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  System.out.println("结束");
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

