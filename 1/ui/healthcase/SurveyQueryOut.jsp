<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：SurveyQueryOut.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  System.out.println("开始执行SurveyQueryOut.jsp文件");
  // 保单信息部分
  LLSurveySchema tLLSurveySchema   = new LLSurveySchema();
  tLLSurveySchema.setRgtNo(request.getParameter("RgtNo"));//立案号
  tLLSurveySchema.setClmCaseNo(request.getParameter("ClmCaseNo"));//案件号
  tLLSurveySchema.setType(request.getParameter("Type"));//案件类型
  tLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));//调查次数
  tLLSurveySchema.setSurveyFlag(request.getParameter("SurveyFlag"));//调查报告类型
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLLSurveySchema);
  tVData.addElement(tG);

  // 数据传输
  SurveyUI tSurveyUI   = new SurveyUI();
	if (!tSurveyUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tSurveyUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tSurveyUI.getResult();

		// 显示
		LLSurveySet mLLSurveySet = new LLSurveySet();
		mLLSurveySet.set((LLSurveySet)tVData.getObjectByObjectName("LLSurveyBLSet",0));
		int n = mLLSurveySet.size();
		System.out.println("get Survey "+n);
		String Strtest = "0|" + n + "^" + mLLSurveySet.encode();
		System.out.println("Strtest==="+Strtest);
		String Path = application.getRealPath("config//Conversion.config");
		Strtest = StrTool.Conversion(Strtest,Path);
		System.out.println("Path==="+Path);
		System.out.println("Strtest1==="+Strtest);

		   	%>
		   	<script language="javascript">
		   	try {
		   	  parent.fraInterface.displayQueryResult('<%=Strtest%>');
		   	} catch(ex) {}
			</script>
			<%

	} // end of if

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tSurveyUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

