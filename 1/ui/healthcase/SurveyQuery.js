//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，查询按钮对应操作
function submitForm()
{
  var i = 0;
  initSurveyGrid();
  //  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  //  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //  fm.submit(); //提交
  var tSurveyFlag = fm.SurveyFlag.value;
  var strSQL = "";
  var message = "";
  if(tSurveyFlag=='1')
  {
    strSQL = "select RgtNo,ClmCaseNo,Type,SurveyFlag ,SerialNo from LLSurvey "
           +" where SurveyFlag in ('0','1') and MngCom like '"+ComCode+"%%'"
           +getWherePart('RgtNo','RgtNo')
           +getWherePart('SerialNo','SerialNo');
    message = "在该管理机构下没有需要确认的调查报告！";

  }
  if(tSurveyFlag =='2')
  {
    strSQL = "select RgtNo,ClmCaseNo,Type,SurveyFlag ,SerialNo from LLSurvey "
           +" where SurveyFlag ='2' and MngCom like '"+ComCode+"%%'"
           +getWherePart('RgtNo','RgtNo')
           +getWherePart('SerialNo','SerialNo');
    message = "在该管理机构下没有需要确认的调查报告！";
  }
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
    alert(message);
    return "";
  }
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.pageDisplayGrid = SurveyGrid;
  turnPage.strQuerySql     = strSQL;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {

    //var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

function returnParent()
{
    tRow=SurveyGrid.getSelNo();
    if (tRow==0)
    {
    	alert("请选择记录");
    	return;
    }
    tClmCaseNo = SurveyGrid.getRowColData(tRow-1,2);
    tType = SurveyGrid.getRowColData(tRow-1,3);
    tSerialNo = SurveyGrid.getRowColData(tRow-1,5);
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    top.location.href="./SurveyQueryDetail.jsp?ClmCaseNo="+tClmCaseNo+"&Type="+tType+"&SerialNo="+tSerialNo;
}

function displayQueryResult(strResult) {
  strResult = Conversion(strResult);
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray          = new Array(3,0,1,21,2);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //查询成功则拆分字符串，返回二维数组
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = SurveyGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}