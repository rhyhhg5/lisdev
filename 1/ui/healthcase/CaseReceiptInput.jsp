<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-07-21 20:09:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="CaseReceiptInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CaseReceiptInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./CaseReceiptSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏CaseReceipt1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseReceipt1);">
      </td>
      <td class= titleImg>
        收据明细信息
      </td>
    	</tr>
    </table>

    <Div  id= "divCaseReceipt1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            分案号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CaseNo >
          </TD>
          
        </TR>
        <TR  class= common>
          <TD  class= title>
            出险人客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            出险人名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
        </TR>
             </table>
    </Div>
    <!--收据明细费用信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseReceipt2);">
    		</td>
    		<td class= titleImg>
    			 收据费用明细信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divCaseReceipt2" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCaseReceiptGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
         <INPUT VALUE="保存" class=common TYPE=button onclick="submitForm();"> 					
         <INPUT VALUE="返回" class=common TYPE=button onclick="top.close();"> 					
    
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  // fm.CustomerNo.value = opener.fraInterface.fm.CustomerNo.value ;
  
//  var varWin = window.opener;
//  alert("win");
  
//  alert(varWin.document.innerHTML);

//  var varFra = varWin.fraInterface;
//  alert("frame");
  
//  var varFm = varFra.fm;
//  alert("form");
  
//  alert(varFm.CustomerNo.value);
  fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
  fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';  
}
catch(ex)
{
  alert(ex);
}
 
</script>

</html>
