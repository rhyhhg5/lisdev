<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：RgtSurveyQueryDetail.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";

    //保单信息部分
    LLSurveySchema tLLSurveySchema   = new LLSurveySchema();
    tLLSurveySchema.setClmCaseNo(request.getParameter("ClmCaseNo"));
    tLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));
    tLLSurveySchema.setType(request.getParameter("Type"));
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(tLLSurveySchema);
    // 数据传输
    RgtSurveyQueryDetailUI tRgtSurveyQueryDetailUI   = new RgtSurveyQueryDetailUI();
    if (!tRgtSurveyQueryDetailUI.submitData(tVData,"QUERY"))
    {
    		Content = " 查询失败，原因是: " + tRgtSurveyQueryDetailUI.mErrors.getError(0).errorMessage;
      		FlagStr = "Fail";
    }
    else
    {
		tVData.clear();
		tVData = tRgtSurveyQueryDetailUI.getResult();
		System.out.println("report=="+tVData.size());
		// 显示
		// 保单信息
		LLSurveySchema mLLSurveySchema = new LLSurveySchema(); 
		mLLSurveySchema=(LLSurveySchema)tVData.getObjectByObjectName("LLSurveySchema",0);
		String Path = application.getRealPath("config//Conversion.config");
		mLLSurveySchema.setContent(StrTool.Conversion(mLLSurveySchema.getContent(),Path));
    		mLLSurveySchema.setresult(StrTool.Conversion(mLLSurveySchema.getresult(),Path));
    		System.out.println("RgtNo=="+mLLSurveySchema.getRgtNo());
		System.out.println("ClmCaseNo=="+mLLSurveySchema.getClmCaseNo());
		System.out.println("InsuredNo=="+mLLSurveySchema.getCustomerNo());
		System.out.println("CustomerName=="+mLLSurveySchema.getCustomerName());
		System.out.println("Content=="+mLLSurveySchema.getContent());
		%>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    	<script language="javascript">
    		
    	
    		var tContent = Conversion("<%=mLLSurveySchema.getContent()%>");
    		var tResult =  Conversion("<%=mLLSurveySchema.getresult()%>");
    	 	top.opener.fm.all("RgtNo").value = "<%=mLLSurveySchema.getRgtNo()%>";
    	 	top.opener.fm.all("CaseNo").value = "<%=mLLSurveySchema.getClmCaseNo()%>";
    	 	top.opener.fm.all("InsuredNo").value = "<%=mLLSurveySchema.getCustomerNo()%>";
    	 	top.opener.fm.all("CustomerName").value = "<%=mLLSurveySchema.getCustomerName()%>";
    	 	top.opener.fm.all("Content").value = tContent;
    	 	top.opener.fm.all("Type").value = "<%=mLLSurveySchema.getType()%>";
    	 	top.opener.fm.all("SerialNo").value = "<%=mLLSurveySchema.getSerialNo()%>";
    	 	top.opener.fm.all("SurveyFlag").value = "<%=mLLSurveySchema.getSurveyFlag()%>";
    	 	top.opener.fm.all("Result").value = tResult;
    	 		top.opener.emptyUndefined();
    	 	
    	 	
    	 	
     	</script>
		<%		
	}
 // end of if 
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tRgtSurveyQueryDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  out.println("top.close();");
  out.println("</script>");
%>
