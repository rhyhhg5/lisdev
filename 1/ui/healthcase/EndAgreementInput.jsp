<html>
<%
/*******************************************************************************
 * Name     :SecondUWInput.jsp
 * Function :初始化“案件审核”中的“二次核保”的界面程序
 * Date     :2003-08-01
 * Author   :LiuYansong
 */
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="SecondUWInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="SecondUWInit.jsp"%>
</head>
<body  onload="initForm();">
  <form  action='./SecondUWSave.jsp' method=post name=fm target="fraSubmit">
     <table>
      <tr class=common>
      <td >
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
      </td>
      <td class= titleImg>
        解约暂停信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLLCase1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            立案号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=RgtNo >
            <Input class=common type= hidden name = OperateFlag>
          </TD>
          <td class= common>
          	<input class=common type=button value="查询所有保单" 			
								onclick="showUWInfo()">
          </td>
        </TR>

        <TR  class= common>
          <TD  class= title>
            案件号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CaseNo >
          </TD>
          <TD  class= title>
            案件类型
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Type >
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            出险人客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=InsuredNo >
          </TD>
          <TD  class= title>
            出险人名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
          <input type=hidden name="StartPhase">
        </TR>
      </table>
    </Div>

      <Table >
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
          </TD>
          <TD class= titleImg>
            解约暂停明细信息
          </TD>
        </TR>
      </Table>

    <Div  id= "divSecondUWInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanSecondUWGrid" >
            </span>
          </TD>
        </TR>
		  </table>
    </Div>

      <table class=common>
        <TR  class= common>
          <TD  class= title>
            解约暂停提示
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= input>
		        <textarea name="UWRegister" cols="100%" rows="4" witdh=25% class="common"></textarea></TD>
	        </TD>
        </TR>
      </table>

      <table class=common>
        <TR class=common>
          <TD class=common>
          <Input value="保  存" type = button class=common onclick = "UWSave()">
          <INPUT VALUE="返  回" TYPE=button class=common onclick="top.close();">
        </TD>
      </TR>
    </table>


  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.OperateFlag.value = 'END';
  fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  fm.InsuredNo.value = '<%= request.getParameter("InsuredNo") %>';
  fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';
  fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
  fm.Type.value = '<%=request.getParameter("Type") %>';
}
catch(ex)
{
  alert(ex);
}

</script>
</html>
