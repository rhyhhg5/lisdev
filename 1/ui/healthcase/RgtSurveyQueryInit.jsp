<%
//程序名称：RgtSurveyQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//单击时查询
function reportDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divDetailInfo.style.left=ex;
  	divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}

// 输入框的初始化（单记录部分）

// 下拉框的初始化

function initForm()
{
  try
  {
	initRgtSurveyGrid();
  }
  catch(re)
  {
    alert("RgtSurveyQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var RgtSurveyGrid;
// 报案信息列表的初始化
function initRgtSurveyGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="案件号";    	//列名
      iArray[1][1]="130px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="案件类型";         			//列名
      iArray[2][1]="0px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
 
      iArray[3]=new Array();
      iArray[3][0]="次数";         			//列名
      iArray[3][1]="30px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      
      iArray[4]=new Array();
      iArray[4][0]="立案号";         			//列名
      iArray[4][1]="130px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="出险人客户号";         		//列名
      iArray[5][1]="130px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[6]=new Array();
      iArray[6][0]="出险人姓名";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="调查人姓名";         		//列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      RgtSurveyGrid = new MulLineEnter( "fm" , "RgtSurveyGrid" ); 
      //这些属性必须在loadMulLine前
      RgtSurveyGrid.mulLineCount = 8;   
      RgtSurveyGrid.displayTitle = 1;
      RgtSurveyGrid.canSel=1;
      RgtSurveyGrid.loadMulLine(iArray);  
      RgtSurveyGrid.detailInfo="单击显示详细信息";
      RgtSurveyGrid.detailClick=reportDetailClick;
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>