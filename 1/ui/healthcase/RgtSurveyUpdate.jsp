<%
//程序名称：RgtSurveyUpdate.jsp
//程序功能：完成立案岗的调查报告阶段的修改操作
//创建日期：2002-11-23
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
//数据传递采用Schema的形式
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
System.out.println("开始执行Update页面");

  LLSurveySchema tLLSurveySchema   = new LLSurveySchema();

  RgtSurveyUpdateUI tRgtSurveyUpdateUI   = new RgtSurveyUpdateUI();

  //输出参数
  CErrors tError = null;
  String transact = "UPDATE";
  System.out.println("transact===="+transact);

  String tRela  = "";
  String FlagStr = "";
  String Content = "";

    //获取报案信息(问题一：为什么//)

    tLLSurveySchema.setRgtNo(request.getParameter("RgtNo"));
    tLLSurveySchema.setClmCaseNo(request.getParameter("CaseNo"));
    tLLSurveySchema.setCustomerName(request.getParameter("CustomerName"));
    tLLSurveySchema.setCustomerNo(request.getParameter("InsuredNo"));
    tLLSurveySchema.setType("1");
    tLLSurveySchema.setSurveyFlag("1");
    tLLSurveySchema.setContent(request.getParameter("Content"));
    tLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));
    System.out.println("ClmCaseNo===="+tLLSurveySchema.getClmCaseNo());
    System.out.println("CustomerNo===="+tLLSurveySchema.getCustomerNo());
    System.out.println("CustomerName===="+tLLSurveySchema.getCustomerName());

    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");

	VData tVData = new VData();
  try
  {
   tVData.addElement(tLLSurveySchema);
   tVData.addElement(tG);

   tRgtSurveyUpdateUI.submitData(tVData,transact);

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tRgtSurveyUpdateUI.mErrors;
    System.out.println("terror=="+tError.getFirstError());
    if (!tError.needDealError())
    {
      Content = " 保存成功";
    	FlagStr = "Succ";
    	tVData.clear();
    	tVData = tRgtSurveyUpdateUI.getResult();
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";

    }
  }

    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  //添加各种预处理
System.out.println("FlagStr==="+FlagStr);
System.out.println("Content==="+Content);

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>