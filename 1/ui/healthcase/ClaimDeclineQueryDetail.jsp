<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ClaimDeclineQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LLClaimDeclineSchema tLLClaimDeclineSchema   = new LLClaimDeclineSchema();
  tLLClaimDeclineSchema.setDeclineNo(request.getParameter("DeclineNo"));
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLLClaimDeclineSchema);
  // 数据传输
  ClaimDeclineUI tClaimDeclineUI   = new ClaimDeclineUI();
	if (!tClaimDeclineUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tClaimDeclineUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tClaimDeclineUI.getResult();
		System.out.println(tVData.size());
		// 显示
		// 保单信息
		LLClaimDeclineSchema mLLClaimDeclineSchema = new LLClaimDeclineSchema(); 
		LLClaimDeclineSet mLLClaimDeclineSet=new LLClaimDeclineSet();
		mLLClaimDeclineSet=(LLClaimDeclineSet)tVData.getObjectByObjectName("LLClaimDeclineBLSet",0);
		mLLClaimDeclineSchema=mLLClaimDeclineSet.get(1);
		%>
    	<script language="javascript">
    	 	top.opener.fm.all("DeclineNo").value = "<%=mLLClaimDeclineSchema.getDeclineNo()%>";
    	 	top.opener.fm.all("DeclineType").value = "<%=mLLClaimDeclineSchema.getDeclineType()%>";
    	 	top.opener.fm.all("RelationNo").value = "<%=mLLClaimDeclineSchema.getRelationNo()%>";
    	 	top.opener.fm.all("RgtNo").value = "<%=mLLClaimDeclineSchema.getRgtNo()%>";
    	 	top.opener.fm.all("Reason").value = "<%=mLLClaimDeclineSchema.getReason()%>";
    	 	top.opener.fm.all("DeclineDate").value = "<%=mLLClaimDeclineSchema.getDeclineDate()%>";
    	 	top.opener.fm.all("ArchiveNo").value = "<%=mLLClaimDeclineSchema.getArchiveNo()%>";
    	 	top.opener.fm.all("Handler").value = "<%=mLLClaimDeclineSchema.getHandler()%>";
    	 	top.opener.fm.all("MngCom").value = "<%=mLLClaimDeclineSchema.getMngCom()%>";
    	 	top.opener.emptyUndefined();
    	</script>
		<%		
	}
 // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tClaimDeclineUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
%>
