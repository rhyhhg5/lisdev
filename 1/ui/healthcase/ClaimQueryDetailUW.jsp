<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ClaimQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String Path = application.getRealPath("config//Conversion.config");
  //账户信息

  //保单信息部分
  LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
  tLLClaimSchema.setRgtNo(request.getParameter("RgtNo"));

  // 准备传输数据 VData
  VData tVData = new VData();
   LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	tLLCaseSchema.setRgtNo(request.getParameter("RgtNo"));
  // 准备传输数据 VData
  CaseUI tCaseUI = new CaseUI();
  System.out.println("RgtNo===="+tLLCaseSchema.getRgtNo());
  tVData.addElement(tLLCaseSchema);
  if (!tCaseUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tCaseUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCaseUI.getResult();
		System.out.println("LLCaseSet.size()===="+tVData.size());
		// 显示
	//	LLCaseSchema mLLCaseSchema = new LLCaseSchema();
		LLCaseSet mLLCaseSet=new LLCaseSet();
		mLLCaseSet.set((LLCaseSet)tVData.getObjectByObjectName("LLCaseBLSet",0));
		int m = mLLCaseSet.size();
		System.out.println("mLLCaseSet.size===="+m);
		%>

		<script language="javascript">
			   	  top.opener.CaseGrid.clearData();
		</script>

		<%

		for(int i=1;i<=m;i++)
		{
			  LLCaseSchema mLLCaseSchema = mLLCaseSet.get(i);
			%>
    	<script language="javascript">
       	top.opener.CaseGrid.addOne("CaseGrid")
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,1,"<%=mLLCaseSchema.getCaseNo()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,2,"<%=mLLCaseSchema.getCustomerNo()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,3,"<%=mLLCaseSchema.getCustomerName()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,4,"<%=mLLCaseSchema.getSubRptNo()%>");
    	 	top.opener.emptyUndefined();
    	</script>
		<%
		}
 //end of for
	}
//end of if

 	tVData.clear();

  tVData.addElement(tLLClaimSchema);
  // 数据传输
  System.out.println("RgtNo..."+tLLClaimSchema.getRgtNo());
  QueryPayPolUI tQueryPayPolUI   = new QueryPayPolUI();
	if (!tQueryPayPolUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tQueryPayPolUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tQueryPayPolUI.getResult();
		System.out.println("calculate end");
		System.out.println(tVData.size());
		// 显示
		// 保单信息
		LLClaimSchema mLLClaimSchema = new LLClaimSchema();
		LLClaimSet mLLClaimSet=new LLClaimSet();
		LLClaimPolicySet mLLClaimPolicySet=new LLClaimPolicySet();
		LLClaimUnderwriteSet mLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
		LLClaimDetailSet mLLClaimDetailSet=new LLClaimDetailSet();
		LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();
		LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
		LLClaimErrorSet mLLClaimErrorSet = new LLClaimErrorSet();

		mLLClaimSet=(LLClaimSet)tVData.getObjectByObjectName("LLClaimSet",0);
		mLLClaimPolicySet=(LLClaimPolicySet)tVData.getObjectByObjectName("LLClaimPolicySet",0);
		mLLClaimUnderwriteSet=(LLClaimUnderwriteSet)tVData.getObjectByObjectName("LLClaimUnderwriteSet",0);
		mLLClaimDetailSet=(LLClaimDetailSet)tVData.getObjectByObjectName("LLClaimDetailSet",0);
		mLLClaimUWMainSchema=(LLClaimUWMainSchema)tVData.getObjectByObjectName("LLClaimUWMainSchema",0);
		mLLRegisterSchema=(LLRegisterSchema)tVData.getObjectByObjectName("LLRegisterSchema",0);
		mLLClaimErrorSet=(LLClaimErrorSet)tVData.getObjectByObjectName("LLClaimErrorSet",0);

		String tRemark="";
    if(!(mLLClaimUWMainSchema.getRemark()==null||mLLClaimUWMainSchema.getRemark().equals("")))
		{
      //进行转换
			tRemark = (StrTool.Conversion(mLLClaimUWMainSchema.getRemark(),Path));
      System.out.println("进行了非空的转换"+tRemark);
    }
    else
    {
      mLLClaimUWMainSchema.setRemark("");
      tRemark = (StrTool.Conversion(mLLClaimUWMainSchema.getRemark(),Path));
      System.out.println("进行了空的转换"+tRemark);
    }
    System.out.println("begin...."+mLLClaimPolicySet);
		System.out.println("begin...."+mLLClaimUnderwriteSet);
		mLLClaimSchema=mLLClaimSet.get(1);
		System.out.println("llclaim.ClmNo=="+mLLClaimSchema.getClmNo());
		String tState=CaseFunPub.getCaseStateByRgtNo(mLLClaimSchema.getRgtNo());

		%>

    	<script language="javascript">
    	 				top.opener.fm.all("RgtNo").value = "<%=mLLClaimSchema.getRgtNo()%>";
					top.opener.fm.all("ClmNo").value = "<%=mLLClaimSchema.getClmNo()%>";
					top.opener.fm.all("ClmState").value = "<%=tState%>";
	</script>
	<%

		if (mLLClaimUWMainSchema.getAppPhase()==null || mLLClaimUWMainSchema.getAppPhase().equals("0")) //审核信息
		{
	%>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
		<script language="javascript">
      var mRemark = Conversion("<%=tRemark%>");
      top.opener.fm.all("CheckRemark").value = mRemark ;
			top.opener.fm.all("ClmUWer").value = "<%=mLLClaimUWMainSchema.getClmUWer()%>";
			top.opener.fm.all("ClmUWDate").value = "<%=mLLClaimUWMainSchema.getMakeDate()%>";
			top.opener.fm.all("MakeDate").value = "<%=mLLClaimUWMainSchema.getMakeDate()%>";
			top.opener.fm.all("AppClmUWer").value = "<%=mLLClaimUWMainSchema.getAppClmUWer()%>";
		</script>
	<%
		}
		else
		{
	%>
      <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
			<script language="javascript">
      var mRemark = Conversion("<%=tRemark%>");
			top.opener.fm.all("ReCheckRemark").value = mRemark ;
			top.opener.fm.all("ClmReUWDate").value = "<%=mLLClaimUWMainSchema.getMakeDate()%>";
			top.opener.fm.all("ClmUWerRecheck").value = "<%=mLLClaimUWMainSchema.getClmUWer()%>";
			top.opener.fm.all("ReMakeDate").value = "<%=mLLClaimUWMainSchema.getMakeDate()%>";

			</script>
	<%
		}
	%>
				<script language="javascript">

					top.opener.ClaimUWDetailGrid.clearData("ClaimUWDetailGrid");
					top.opener.ClaimUWDetailGrid.clearData("ClaimUWDetailGrid");
					top.opener.emptyUndefined();
<%
          String tAutoClmDecision = "";
					String tClmDecision = "";
					String tClmUWer = "";
					String tClmUWGrade = "";
  					System.out.println("policy.size==="+mLLClaimPolicySet.size());
  					for (int i = 1;i<=mLLClaimPolicySet.size();i++)
  					{
						tAutoClmDecision = "";
						tClmDecision = "";
						tClmUWer = "";
						tClmUWGrade = "";
						System.out.println("underwrite show begin");
						System.out.println("underwrite.size==="+mLLClaimUnderwriteSet.size());
						for (int j = 1;j<=mLLClaimUnderwriteSet.size();j++)
						{
							if (mLLClaimUnderwriteSet.get(j).getPolNo().equals(mLLClaimPolicySet.get(i).getPolNo()))
							{
								tAutoClmDecision = mLLClaimUnderwriteSet.get(j).getAutoClmDecision();
								tClmDecision = mLLClaimUnderwriteSet.get(j).getClmDecision();
								tClmUWer = mLLClaimUnderwriteSet.get(j).getClmUWer();
								tClmUWGrade = mLLClaimUnderwriteSet.get(j).getClmUWGrade();
							}
						}


%>
							top.opener.ClaimUWDetailGrid.addOne("ClaimUWDetailGrid");
						  	top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,1,"<%=mLLClaimPolicySet.get(i).getPolNo()%>");
  							top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,2,"<%=mLLClaimPolicySet.get(i).getInsuredName()%>");
  							top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,3,"<%=mLLClaimPolicySet.get(i).getGetDutyKind()%>");
  							top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimPolicySet.get(i).getRealPay()%>");
  							top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,5,"<%=tAutoClmDecision%>");
  							top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,6,"<%=tClmDecision%>");
  							top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,7,"<%=tClmUWer%>");
  							top.opener.ClaimUWDetailGrid.setRowColData(<%=i-1%>,8,"<%=tClmUWGrade%>");
  							top.opener.emptyUndefined();


<%
  					}

  					System.out.println("policy.size==="+mLLClaimPolicySet.size());

%>
  					top.opener.ClaimPolGrid.unLock();
  					top.opener.ClaimPolGrid.clearData();
 <%
  					for (int i = 1;i<=mLLClaimPolicySet.size();i++)
  					{
%>
							top.opener.ClaimPolGrid.addOne("ClaimPolGrid");
						  	top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,1,"<%=mLLClaimPolicySet.get(i).getPolNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,2,"<%=mLLClaimPolicySet.get(i).getRiskCode()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,3,"<%=mLLClaimPolicySet.get(i).getCaseNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimPolicySet.get(i).getInsuredNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,5,"<%=mLLClaimPolicySet.get(i).getCValiDate()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,6,"<%=mLLClaimPolicySet.get(i).getStandPay()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,7,"<%=mLLClaimPolicySet.get(i).getRealPay()%>");


<%
  					}

 %>
 							top.opener.ClaimPolGrid.lock();
 <%
  					System.out.println("222");
  					System.out.println("0000");
  					if (mLLClaimDetailSet.size()==0)
  					{
  						System.out.println("nullllll");
  					}
  					System.out.println("detail.size==="+mLLClaimDetailSet.size());
 %>
 					top.opener.ClaimDetailGrid.unLock();
 					top.opener.ClaimDetailGrid.clearData();
 <%
  					for (int i = 1;i<=mLLClaimDetailSet.size();i++)
  					{
%>
						top.opener.ClaimDetailGrid.addOne("ClaimDetailGrid");
						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,1,"<%=mLLClaimDetailSet.get(i).getPolNo()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,2,"<%=mLLClaimDetailSet.get(i).getGetDutyCode()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,3,"<%=mLLClaimDetailSet.get(i).getStandPay()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimDetailSet.get(i).getRealPay()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,5,"<%=mLLClaimDetailSet.get(i).getStatType()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,6,"<%=mLLClaimDetailSet.get(i).getGetDutyKind()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,7,"<%=mLLClaimDetailSet.get(i).getDutyCode()%>");

<%
					}
%>

					top.opener.ClaimDetailGrid.lock();
<%
					System.out.println("333");
					String tPayInfo[][]= new String[50][50];
					int tSumPol=0;
					int tNowRol=0;
					int tNowCol=0;
					String tFlag = "0" ;
					System.out.println("detailsize==="+mLLClaimDetailSet.size());
					for (int i = 1;i<=mLLClaimDetailSet.size();i++)
					{
						tFlag = "0";
						tNowRol=0;
						tNowCol=0;
						LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
						tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
						System.out.println("44444444");
						System.out.println("tSumPol=="+tSumPol);

						for (int j=1;j<=tSumPol;j++)
						{
							if (tPayInfo[j-1][0].equals(tLLClaimDetailSchema.getPolNo()))
							{
								tFlag = "1";
								tNowRol = j-1;
								break;
							}
						}
						if (tFlag=="0") //无保单记录
						{
							tSumPol = tSumPol + 1;
							tNowRol = tSumPol -1 ;
						}
						tPayInfo[tNowRol][0]= tLLClaimDetailSchema.getPolNo();
						if (tLLClaimDetailSchema.getStatType().equals("SC")) tNowCol =1;
						if (tLLClaimDetailSchema.getStatType().equals("SW")) tNowCol =2;
						if (tLLClaimDetailSchema.getStatType().equals("YL")) tNowCol =3;
						if (tLLClaimDetailSchema.getStatType().equals("TB")) tNowCol =4;
						if (tLLClaimDetailSchema.getStatType().equals("TF")) tNowCol =5;
						if (tLLClaimDetailSchema.getStatType().equals("QT")) tNowCol =6;
						if (tPayInfo[tNowRol][tNowCol]==null)
							tPayInfo[tNowRol][tNowCol] = Double.toString(tLLClaimDetailSchema.getRealPay());
						else
							tPayInfo[tNowRol][tNowCol] = Double.toString(tLLClaimDetailSchema.getRealPay()+Double.parseDouble(tPayInfo[tNowRol][tNowCol]));
					}

					System.out.println("tSumPol=="+tSumPol);
%>
					top.opener.ClaimPayGrid.unLock();
					top.opener.ClaimPayGrid.clearData();
<%
					for (int i =1;i<=tSumPol;i++)
					{

						for (int j = 1;j<=6;j++)
						{
							if (tPayInfo[i-1][j]==null) tPayInfo[i-1][j] = "0.00";
						}

%>


						top.opener.ClaimPayGrid.addOne("ClaimPayGrid");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,1,"<%=tPayInfo[i-1][0]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,2,"<%=tPayInfo[i-1][1]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,3,"<%=tPayInfo[i-1][2]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,4,"<%=tPayInfo[i-1][3]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,5,"<%=tPayInfo[i-1][4]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,6,"<%=tPayInfo[i-1][5]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,7,"<%=tPayInfo[i-1][6]%>");

<%
					}
%>
					top.opener.ClaimPayGrid.lock();
					top.opener.ClaimErrorGrid.unLock();
					top.opener.ClaimErrorGrid.clearData();
<%
  		for (int i = 1;i<=mLLClaimErrorSet.size();i++)
  		{
%>
			top.opener.ClaimErrorGrid.addOne("ClaimErrorGrid");
			top.opener.ClaimErrorGrid.setRowColData(<%=i-1%>,1,"<%=mLLClaimErrorSet.get(i).getPolNo()%>");
  			top.opener.ClaimErrorGrid.setRowColData(<%=i-1%>,2,"<%=mLLClaimErrorSet.get(i).getClmNo()%>");
  			top.opener.ClaimErrorGrid.setRowColData(<%=i-1%>,3,"<%=mLLClaimErrorSet.get(i).getUWRuleCode()%>");
  			top.opener.ClaimErrorGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimErrorSet.get(i).getUWError()%>");
  			top.opener.ClaimErrorGrid.setRowColData(<%=i-1%>,5,"<%=mLLClaimErrorSet.get(i).getUWGrade()%>");

<%
  		}
 %>
 		top.opener.ClaimErrorGrid.lock();
 <%
 CaseAccOperateBL tCaseAccOperateBL = new CaseAccOperateBL();
 LLCaseAccSet tLLCaseAccSet = new LLCaseAccSet();
 tLLCaseAccSet.set(tCaseAccOperateBL.getQueryData(tLLClaimSchema.getRgtNo()));

 if(tLLCaseAccSet.size()>0)
 {
   for(int index=1;index<=tLLCaseAccSet.size();index++)
   {
   %>
     top.opener.PolAccGrid.addOne("PolAccGrid");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,1,"<%=tLLCaseAccSet.get(index).getPolNo()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,2,"<%=tLLCaseAccSet.get(index).getInsuAccNo()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,3,"<%=tLLCaseAccSet.get(index).getAccType()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,4,"<%=tLLCaseAccSet.get(index).getAccBJMoney()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,5,"<%=tLLCaseAccSet.get(index).getInterestMoney()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,6,"<%=tLLCaseAccSet.get(index).getTotalMoney()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,7,"<%=tLLCaseAccSet.get(index).getBasePay()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,8,"<%=tLLCaseAccSet.get(index).getRealPay()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,9,"");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,10,"<%=tLLCaseAccSet.get(index).getGetDutyCode()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,11,"<%=tLLCaseAccSet.get(index).getGetDutyKind()%>");
     top.opener.PolAccGrid.setRowColData(<%=index-1%>,12,"<%=tLLCaseAccSet.get(index).getMasterPolNo()%>");
   <%
    }
  }
  %>
  </script>
	<%
	}


  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tQueryPayPolUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
out.println("<script language=javascript>");
out.println("top.close();");
out.println("</script>");
%>
