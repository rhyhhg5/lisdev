<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ClaimQuery.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	System.out.println("---hahahahah---");
  // 保单信息部分
  LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();
  LLCaseSchema tLLCaseSchema=new LLCaseSchema();
	tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
	tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
	
//  System.out.println(request.getParameter("RgtNo"));
//	System.out.println(request.getParameter("CaseNo"));
  if(!tLLRegisterSchema.getRgtNo().equals(""))
  {  
	  // 准备传输数据 VData
	  VData tVData = new VData();
		tVData.addElement(tLLRegisterSchema);
	  // 数据传输
	  RegisterUI tRegisterUI   = new RegisterUI();
		if (!tRegisterUI.submitData(tVData,"QUERY||MAIN"))
		{
	      Content = " 查询失败，原因是: " + tRegisterUI.mErrors.getError(0).errorMessage;
	      FlagStr = "Fail";
		}
		else
		{
			tVData.clear();
			tVData = tRegisterUI.getResult();
			
			// 显示
			LLRegisterSet mLLRegisterSet = new LLRegisterSet(); 
			mLLRegisterSet.set((LLRegisterSet)tVData.getObjectByObjectName("LLRegisterBLSet",0));
			int n = mLLRegisterSet.size();
	//		System.out.println("get register "+n);
			for (int i = 1; i <= n; i++)
			{
			  	LLRegisterSchema mLLRegisterSchema = mLLRegisterSet.get(i);
	//		  	System.out.println(mLLRegisterSchema.getPolNo());
			   	%>
			   	<script language="javascript">
			   		parent.fraInterface.fm.RgtNo.value="<%=mLLRegisterSchema.getRgtNo()%>";
			   		parent.fraInterface.fm.PolNo.value="<%=mLLRegisterSchema.getPolNo()%>";
			   		parent.fraInterface.fm.RgtantName.value="<%=mLLRegisterSchema.getRgtantName()%>";
			   		parent.fraInterface.fm.RgtantSex.value="<%=mLLRegisterSchema.getRgtantSex()%>";
			   		parent.fraInterface.fm.Relation.value="<%=mLLRegisterSchema.getRelation()%>";
			   		parent.fraInterface.fm.RgtantAddress.value="<%=mLLRegisterSchema.getRgtantAddress()%>";
			   		parent.fraInterface.fm.RgtantPhone.value="<%=mLLRegisterSchema.getRgtantPhone()%>";
			   		parent.fraInterface.fm.RgtantMobile.value="<%=mLLRegisterSchema.getRgtantMobile()%>";
			   		parent.fraInterface.fm.RgtDate.value="<%=mLLRegisterSchema.getRgtDate()%>";
			   		parent.fraInterface.fm.AccidentSite.value="<%=mLLRegisterSchema.getAccidentSite()%>";
			   		parent.fraInterface.fm.AccidentReason.value="<%=mLLRegisterSchema.getAccidentReason()%>";
			   		parent.fraInterface.fm.AccidentCourse.value="<%=mLLRegisterSchema.getAccidentCourse()%>";
			   		parent.fraInterface.fm.AccidentDate.value="<%=mLLRegisterSchema.getAccidentDate()%>";
			   		parent.fraInterface.fm.Remark.value="<%=mLLRegisterSchema.getRemark()%>";
			   		parent.fraInterface.fm.AgentCode.value="<%=mLLRegisterSchema.getAgentCode()%>";
			   		parent.fraInterface.fm.AgentGroup.value="<%=mLLRegisterSchema.getAgentGroup()%>";
			   		parent.fraInterface.fm.Handler.value="<%=mLLRegisterSchema.getHandler()%>";
			   		parent.fraInterface.fm.MngCom.value="<%=mLLRegisterSchema.getMngCom()%>";
			   		parent.fraInterface.fm.Operator.value="<%=mLLRegisterSchema.getOperator()%>";
			   		parent.fraInterface.fm.MakeDate.value="<%=mLLRegisterSchema.getMakeDate()%>";
				</script>
				<%
			} // end of for
		} // end of if
	  
	  //如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr == "Fail")
	  {
	    tError = tRegisterUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 查询成功! ";
	    	FlagStr = "Succ";
	    }
	    else                                                                           
	    {
	    	Content = " 查询失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  }
		System.out.println("------end------");
		System.out.println(FlagStr);
		System.out.println(Content);
	}
	
	if(!tLLCaseSchema.getCaseNo().equals(""))
  {  
	  // 准备传输数据 VData
	  VData tVData = new VData();
		tVData.addElement(tLLCaseSchema);
	  // 数据传输
	  CaseUI tCaseUI   = new CaseUI();
		if (!tCaseUI.submitData(tVData,"QUERY||MAIN"))
		{
	      Content = " 查询失败，原因是: " + tCaseUI.mErrors.getError(0).errorMessage;
	      FlagStr = "Fail";
		}
		else
		{
			tVData.clear();
			tVData = tCaseUI.getResult();
			
			// 显示
			LLCaseSet mLLCaseSet = new LLCaseSet(); 
			mLLCaseSet.set((LLCaseSet)tVData.getObjectByObjectName("LLCaseBLSet",0));
			int n = mLLCaseSet.size();
//			System.out.println("get case "+n);
			for (int i = 1; i <= n; i++)
			{
			  	LLCaseSchema mLLCaseSchema = mLLCaseSet.get(i);
			   	%>
			   	<script language="javascript">
			   	  parent.fraInterface.CaseGrid.addOne("CaseGrid")
			   		parent.fraInterface.fm.CaseGrid2[<%=i-1%>].value="<%=mLLCaseSchema.getCaseNo()%>";
			   		parent.fraInterface.fm.CaseGrid3[<%=i-1%>].value="<%=mLLCaseSchema.getCustomerNo()%>";
			   		parent.fraInterface.fm.CaseGrid4[<%=i-1%>].value="<%=mLLCaseSchema.getCustomerName()%>";
			   		parent.fraInterface.fm.CaseGrid5[<%=i-1%>].value="<%=mLLCaseSchema.getSubRptNo()%>";
					</script>
				<%
			} // end of for
		} // end of if
	  
	  //如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr == "Fail")
	  {
	    tError = tCaseUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 查询成功! ";
	    	FlagStr = "Succ";
	    }
	    else                                                                           
	    {
	    	Content = " 查询失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  }
		System.out.println("------end------");
		System.out.println(FlagStr);
		System.out.println(Content);
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
