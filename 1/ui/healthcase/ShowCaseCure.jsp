<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
  * Name    ：ShowCaseCure.jsp
  * Function：显示立案或者上理赔中的费用信息明细
  * Author  ：LiuYansong
  * Date    : 2003-7-31
 */
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();//接收后台的传递数据
  //String DisplayFlag = request.getParameter("DisplayFlag");//记录是在立案进入还是在理算进入的
  LLCaseSchema mLLCaseSchema = new LLCaseSchema();         //传递CaseNo
  mLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
  LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();//接收查询的数据信息
  LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();//接收查询的数据信息
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  VData tVData = new VData();
  tVData.addElement(mLLCaseSchema);
	tVData.addElement(tG);
  CaseCureUI mCaseCureUI = new CaseCureUI();

  if(!mCaseCureUI.submitData(tVData,"SHOW"))
	{
    FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = mCaseCureUI.getResult();
		System.out.println(tVData.size());
    tLLCaseSchema.setSchema((LLCaseSchema)tVData.getObjectByObjectName("LLCaseSchema",0));
    mLLCaseReceiptSet.set((LLCaseReceiptSet)tVData.getObjectByObjectName("LLCaseReceiptSet",0));
    mLLCaseCureSet.set((LLCaseCureSet)tVData.getObjectByObjectName("LLCaseCureSet",0));
    %>
    <script language="javascript">
      parent.fraInterface.fm.all("InHospitalDate").value = "<%=tLLCaseSchema.getInHospitalDate()%>";
      parent.fraInterface.fm.all("OutHospitalDate").value = "<%=tLLCaseSchema.getOutHospitalDate()%>";
      parent.fraInterface.fm.all("InHospitalDays").value = "<%=tLLCaseSchema.getInHospitalDays()%>";
      parent.fraInterface.emptyUndefined();
    </script>
    <%
    int count=mLLCaseReceiptSet.size();
    System.out.println("mLLCaseReceiptSet中的个数是"+mLLCaseReceiptSet.size());
    if(mLLCaseReceiptSet.size()>0)
    {
      %>
      <script language="javascript">
        parent.fraInterface.ICaseCureGrid.clearData();
      </script>
      <%
      for(int i=1;i<=mLLCaseReceiptSet.size();i++)
      {
        LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();
        tLLCaseReceiptSchema.setSchema(mLLCaseReceiptSet.get(i));
        %>
        <script language="javascript">
          parent.fraInterface.ICaseCureGrid.addOne("ICaseCureGrid");
          parent.fraInterface.ICaseCureGrid.setRowColData(<%=i-1%>,1,"<%=tLLCaseReceiptSchema.getHospitalName()%>");
          parent.fraInterface.ICaseCureGrid.setRowColData(<%=i-1%>,2,"<%=tLLCaseReceiptSchema.getFeeItemType()%>");
          parent.fraInterface.ICaseCureGrid.setRowColData(<%=i-1%>,3,"<%=tLLCaseReceiptSchema.getFeeItemCode()%>");
          parent.fraInterface.ICaseCureGrid.setRowColData(<%=i-1%>,4,"<%=tLLCaseReceiptSchema.getFeeItemName()%>");
          parent.fraInterface.ICaseCureGrid.setRowColData(<%=i-1%>,6,"<%=tLLCaseReceiptSchema.getFee()%>");
          parent.fraInterface.ICaseCureGrid.setRowColData(<%=i-1%>,5,"<%=tLLCaseReceiptSchema.getApplyFee()%>");
          parent.fraInterface.emptyUndefined();
        </script>
        <%
      }
    }

    LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();//装载住院的信息
    LLCaseCureSet aLLCaseCureSet = new LLCaseCureSet();//装载按费用进行赔付的手术信息
    LLCaseCureSet bLLCaseCureSet = new LLCaseCureSet();//装载按档次进行赔付的手术信息
    LLCaseCureSet jLLCaseCureSet = new LLCaseCureSet();//装载救援费用的信息
    String is_Fee="0";                                 //是否按费用进行手术赔付
    String is_Degree="0";        //是否按档次进行手术赔付
    System.out.println("mLLCaseCureSet中的个数是"+mLLCaseCureSet.size());
    if(mLLCaseCureSet.size()>0)
    {
      for(int j=1;j<=mLLCaseCureSet.size();j++)
      {
        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
        tLLCaseCureSchema.setSchema(mLLCaseCureSet.get(j));
        if(tLLCaseCureSchema.getSerialNo().substring(0,2).equals("HO"))
        {
          tLLCaseCureSet.add(tLLCaseCureSchema);//是住院信息则加载到tLLCaseCureSet中
        }
        if(tLLCaseCureSchema.getSerialNo().substring(0,2).equals("FO"))
        {
          aLLCaseCureSet.add(tLLCaseCureSchema);//是按费用进行赔付的手术信息加载到aLLCaseCureSet中
          is_Fee="1";
        }
        if(tLLCaseCureSchema.getSerialNo().substring(0,2).equals("DO"))
        {
          bLLCaseCureSet.add(tLLCaseCureSchema);//是按档次进行赔付的手术信息，加载到bLLCaseCureSet中
          is_Degree="1";
        }
        if(tLLCaseCureSchema.getSerialNo().substring(0,2).equals("JY"))
        {
          jLLCaseCureSet.add(tLLCaseCureSchema);
        }
      }
      System.out.println("住院信息的条数是"+tLLCaseCureSet.size());
      if(tLLCaseCureSet.size()>0)
      {
        %>
        <script language="javascript">
          parent.fraInterface.HospitalGrid.clearData();
        </script>
        <%
        for(int t_count=1;t_count<=tLLCaseCureSet.size();t_count++)
        {
          LLCaseCureSchema ttLLCaseCureSchema = new LLCaseCureSchema();
          ttLLCaseCureSchema.setSchema(tLLCaseCureSet.get(t_count));
          %>
          <script language="javascript">
            parent.fraInterface.HospitalGrid.addOne("HospitalGrid");
            parent.fraInterface.HospitalGrid.setRowColData(<%=t_count-1%>,1,"<%=ttLLCaseCureSchema.getHospitalName()%>");
            parent.fraInterface.HospitalGrid.setRowColData(<%=t_count-1%>,2,"<%= ttLLCaseCureSchema.getAccidentType()%>");
            parent.fraInterface.HospitalGrid.setRowColData(<%=t_count-1%>,3,"<%=ttLLCaseCureSchema.getFeeItemCode()%>");
            parent.fraInterface.HospitalGrid.setRowColData(<%=t_count-1%>,4,"<%=ttLLCaseCureSchema.getFeeItemName()%>");
            parent.fraInterface.HospitalGrid.setRowColData(<%=t_count-1%>,6,"<%=ttLLCaseCureSchema.getFee()%>");
            parent.fraInterface.HospitalGrid.setRowColData(<%=t_count-1%>,5,"<%=ttLLCaseCureSchema.getApplyFee()%>");
            parent.fraInterface.emptyUndefined();
          </script>
          <%
        }
      }


      //对救援费用进行附值处理
      if(jLLCaseCureSet.size()>0)
      {
      %>
      <script language="javascript">
        parent.fraInterface.SuccorGrid.clearData();
      </script>
      <%
        for(int j_count=1;j_count<=jLLCaseCureSet.size();j_count++)
        {
          LLCaseCureSchema jjLLCaseCureSchema = new LLCaseCureSchema();
          jjLLCaseCureSchema.setSchema(jLLCaseCureSet.get(j_count));
        %>
        <script language="javascript">
          parent.fraInterface.SuccorGrid.addOne("SuccorGrid");
          parent.fraInterface.SuccorGrid.setRowColData(<%=j_count-1%>,1,"<%=jjLLCaseCureSchema.getHospitalName()%>");
          parent.fraInterface.SuccorGrid.setRowColData(<%=j_count-1%>,2,"<%=jjLLCaseCureSchema.getDiseaseName()%>");
          parent.fraInterface.SuccorGrid.setRowColData(<%=j_count-1%>,3,"<%=jjLLCaseCureSchema.getAccidentType()%>");
          parent.fraInterface.SuccorGrid.setRowColData(<%=j_count-1%>,4,"<%=jjLLCaseCureSchema.getFeeItemCode()%>");
          parent.fraInterface.SuccorGrid.setRowColData(<%=j_count-1%>,5,"<%=jjLLCaseCureSchema.getFeeItemName()%>");
          parent.fraInterface.SuccorGrid.setRowColData(<%=j_count-1%>,6,"<%=jjLLCaseCureSchema.getFee()%>");
          parent.fraInterface.SuccorGrid.setRowColData(<%=j_count-1%>,7,"<%=jjLLCaseCureSchema.getApplyFee()%>");
          parent.fraInterface.emptyUndefined();
        </script>
        <%
        }
      }



      //对按费用进行手术赔付的信息处理
      System.out.println("费用手术信息的条数是"+aLLCaseCureSet.size());
      if(aLLCaseCureSet.size()>0)
      {
        %>
        <script language="javascript">
          parent.fraInterface.FeeOperationGrid.clearData();
        </script>
        <%
        for(int a_count=1;a_count<=aLLCaseCureSet.size();a_count++)
        {
          LLCaseCureSchema aLLCaseCureSchema = new LLCaseCureSchema();
          aLLCaseCureSchema.setSchema(aLLCaseCureSet.get(a_count));
          %>
          <script language="javascript">
            parent.fraInterface.FeeOperationGrid.addOne("FeeOperationGrid");
            parent.fraInterface.FeeOperationGrid.setRowColData(<%=a_count-1%>,1,"<%=aLLCaseCureSchema.getFeeItemCode()%>");
            parent.fraInterface.FeeOperationGrid.setRowColData(<%=a_count-1%>,2,"<%=aLLCaseCureSchema.getFeeItemName()%>");
            parent.fraInterface.FeeOperationGrid.setRowColData(<%=a_count-1%>,3,"<%=aLLCaseCureSchema.getFee()%>");
            parent.fraInterface.FeeOperationGrid.setRowColData(<%=a_count-1%>,4,"<%=aLLCaseCureSchema.getApplyFee()%>");
            parent.fraInterface.emptyUndefined();
          </script>
          <%
        }
      }

      //对按档次进行手术赔付的信息进行附值
      System.out.println("档次手术信息的条数是"+bLLCaseCureSet.size());
      if(bLLCaseCureSet.size()>0)
      {
        %>
        <script language="javascript">
          parent.fraInterface.DegreeOperationGrid.clearData();
        </script>
        <%
        for(int b_count=1;b_count<=bLLCaseCureSet.size();b_count++)
        {
          LLCaseCureSchema bLLCaseCureSchema = new LLCaseCureSchema();
          bLLCaseCureSchema.setSchema(bLLCaseCureSet.get(b_count));
          %>
          <script language="javascript">
            parent.fraInterface.DegreeOperationGrid.addOne("DegreeOperationGrid");
            parent.fraInterface.DegreeOperationGrid.setRowColData(<%=b_count-1%>,1,"<%=bLLCaseCureSchema.getFeeItemCode()%>");
            parent.fraInterface.DegreeOperationGrid.setRowColData(<%=b_count-1%>,2,"<%=bLLCaseCureSchema.getFeeItemName()%>");
            parent.fraInterface.DegreeOperationGrid.setRowColData(<%=b_count-1%>,3,"<%=bLLCaseCureSchema.getFee()%>");
            parent.fraInterface.DegreeOperationGrid.setRowColData(<%=b_count-1%>,4,"<%=bLLCaseCureSchema.getApplyFee()%>");
            parent.fraInterface.emptyUndefined();
          </script>
          <%
        }
      }
      //对手术赔付类型进行附值
      if(is_Fee.equals("1")&&is_Degree.equals("1"))
      {
        %>
        <script language="javascript">
          parent.fraInterface.fm.all("OperationType").value="0和1";
        </script>
        <%

      }
      if(is_Fee.equals("1")&&is_Degree.equals("0"))
      {
         %>
         <script language="javascript">
           parent.fraInterface.fm.all("OperationType").value="0";
         </script>
        <%
      }
      if(is_Fee.equals("0")&&is_Degree.equals("1"))
      {
        %>
         <script language="javascript">
           parent.fraInterface.fm.all("OperationType").value="1";
         </script>
        <%
      }
    }
	}
%>