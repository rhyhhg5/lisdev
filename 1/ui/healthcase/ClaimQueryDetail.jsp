<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ClaimQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：   HST
//更新记录：  更新人    更新日期       更新原因/内容
//            guoxiang   2003-7-31     添加：
//                                     1. ClaimPolGrid
//                                          Remark字段，“备注“
//                                          tAfterGet字段， 如果为“003”，即为"Y",否则即为"N".
//                                     2. ClaimDetailGrid
//                                          GetDutyName 字段，   “给付责任名称”
//                       2003-8-5      添加：
//                                     1. ClaimPolGrid
//                                            casePolType     0,1,2
//


%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String a_HandlerName="";


  //保单信息部分
  LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
  //接受账户信息的部分
  LLCaseAccSet mLLCaseAccSet = new LLCaseAccSet();

  tLLClaimSchema.setRgtNo(request.getParameter("RgtNo"));
	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	tLLCaseSchema.setRgtNo(request.getParameter("RgtNo"));
  // 准备传输数据 VData
  CaseUI tCaseUI = new CaseUI();
  System.out.println("RgtNo===="+tLLCaseSchema.getRgtNo());
  VData tVData = new VData();
  tVData.addElement(tLLCaseSchema);
  if (!tCaseUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tCaseUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCaseUI.getResult();
		System.out.println("LLCaseSet.size()===="+tVData.size());
		// 显示
	//	LLCaseSchema mLLCaseSchema = new LLCaseSchema();
		LLCaseSet mLLCaseSet=new LLCaseSet();
		mLLCaseSet.set((LLCaseSet)tVData.getObjectByObjectName("LLCaseBLSet",0));
		int m = mLLCaseSet.size();
		System.out.println("mLLCaseSet.size===="+m);
		%>

		<script language="javascript">
			   	  top.opener.CaseGrid.clearData();
		</script>

		<%

		for(int i=1;i<=m;i++)
		{
			  LLCaseSchema mLLCaseSchema = mLLCaseSet.get(i);
			%>
    	<script language="javascript">
       	top.opener.CaseGrid.addOne("CaseGrid")
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,1,"<%=mLLCaseSchema.getCaseNo()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,2,"<%=mLLCaseSchema.getCustomerNo()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,3,"<%=mLLCaseSchema.getCustomerName()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,4,"<%=mLLCaseSchema.getSubRptNo()%>");
    		top.opener.emptyUndefined();
    	</script>
		<%
		}
 //end of for
	}
//end of if





 	tVData.clear();
  tVData.addElement(tLLClaimSchema);

  // 数据传输
  System.out.println("RgtNo..."+tLLClaimSchema.getRgtNo());
  QueryPayPolUI tQueryPayPolUI   = new QueryPayPolUI();
	if (!tQueryPayPolUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tQueryPayPolUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tQueryPayPolUI.getResult();
		a_HandlerName=(String)tVData.get(0);
		System.out.println("calculate end");
		System.out.println(tVData.size());

		// 显示
		// 保单信息
		LLClaimSchema mLLClaimSchema = new LLClaimSchema();
		LLClaimSet mLLClaimSet=new LLClaimSet();
		LLClaimPolicySet mLLClaimPolicySet=new LLClaimPolicySet();
		LLClaimDetailSet mLLClaimDetailSet=new LLClaimDetailSet();
		LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();


		mLLClaimSet=(LLClaimSet)tVData.getObjectByObjectName("LLClaimSet",0);
		mLLClaimPolicySet=(LLClaimPolicySet)tVData.getObjectByObjectName("LLClaimPolicySet",0);
		mLLClaimDetailSet=(LLClaimDetailSet)tVData.getObjectByObjectName("LLClaimDetailSet",0);
		mLLRegisterSchema=(LLRegisterSchema)tVData.getObjectByObjectName("LLRegisterSchema",0);

		System.out.println("begin...."+mLLClaimDetailSet);
		mLLClaimSchema=mLLClaimSet.get(1);
		System.out.println("llclaim.ClmNo=="+mLLClaimSchema.getClmNo());
		String tState=CaseFunPub.getCaseStateByRgtNo(mLLClaimSchema.getRgtNo());
		%>
    	<script language="javascript">
    	 		top.opener.fm.all("RgtNo").value = "<%=mLLClaimSchema.getRgtNo()%>";
					top.opener.fm.all("ClmNo").value = "<%=mLLClaimSchema.getClmNo()%>";
					top.opener.fm.all("ClmState").value = "<%=tState%>";
					top.opener.fm.all("CasePayType").value = "<%=mLLClaimSchema.getCasePayType()%>";
					top.opener.fm.all("ClmUWer").value = "<%=a_HandlerName%>";
					top.opener.fm.all("MngCom").value = "<%=mLLClaimSchema.getMngCom()%>";
					top.opener.fm.all("CheckType").value = "<%=mLLClaimSchema.getCheckType()%>";
					top.opener.ClaimPolGrid.clearData("ClaimPolGrid");
					top.opener.ClaimDetailGrid.clearData("ClaimDetailGrid");
					top.opener.emptyUndefined();
					top.opener.showCodeName();

<%

  					System.out.println("policy.size==="+mLLClaimPolicySet.size());
  					System.out.println("PolNo==="+mLLClaimPolicySet.get(1).getPolNo());
  					for (int i = 1;i<=mLLClaimPolicySet.size();i++)
  					{
String tSql="#getdutykind# and code in(select getdutykind from LMDutyGetClm where getdutycode"+
" in(select getdutycode from lcget where polno=#"+mLLClaimPolicySet.get(i).getPolNo().trim()+
"# and #"+mLLClaimPolicySet.get(i).getPolType().trim()+
"#=#1#) or getdutycode in(select getdutycode from lbget where polno=#"+
mLLClaimPolicySet.get(i).getPolNo().trim()+"# and #"+mLLClaimPolicySet.get(i).getPolType().trim()+
"#=#1#)) or (code=#001# and #"+mLLClaimPolicySet.get(i).getPolType().trim()+"#=#0#)";
%>
							  top.opener.ClaimPolGrid.addOne("ClaimPolGrid");
						  	top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,1,"<%=mLLClaimPolicySet.get(i).getPolNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,2,"<%=mLLClaimPolicySet.get(i).getRiskCode()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,3,"<%=mLLClaimPolicySet.get(i).getCaseNo()%>");
  							<%
                if(mLLClaimPolicySet.get(i).getCasePolType().trim().equals("0"))
                {
                %>
  							  top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimPolicySet.get(i).getInsuredNo()%>");
                <%
                }
                else
                {
                %>
                  top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimPolicySet.get(i).getAppntNo()%>");
                <%
                }
                %>
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimPolicySet.get(i).getInsuredNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,5,"<%=mLLClaimPolicySet.get(i).getCValiDate()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,6,"<%=mLLClaimPolicySet.get(i).getGetDutyKind()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,7,"<%=mLLClaimPolicySet.get(i).getStandPay()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,8,"<%=mLLClaimPolicySet.get(i).getRealPay()%>");
  						  top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,9,"<%=tSql%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,10,"<%=mLLClaimPolicySet.get(i).getPolType()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,11,"<%=mLLClaimPolicySet.get(i).getRemark()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,12,"<%=mLLClaimPolicySet.get(i).getCasePolType()%>");
  							top.opener.emptyUndefined();

<%
  					System.out.println("----------------------------gztest"+mLLClaimPolicySet.get(i).getGetDutyKind());
  					}
  					System.out.println("222");
  					System.out.println("0000");
  					if (mLLClaimDetailSet.size()==0)
  					{
  						System.out.println("nullllll");
  					}
  					System.out.println("detail.size==="+mLLClaimDetailSet.size());


  					for (int i = 1;i<=mLLClaimDetailSet.size();i++)
  					{
            String tGetDutycode=mLLClaimDetailSet.get(i).getGetDutyCode();   //给付责任编码
            String tGetDutyKind=mLLClaimDetailSet.get(i).getGetDutyKind();   //给付责任类型

            LMDutyGetClmSchema  tLMDutyGetClmSchema=new LMDutyGetClmSchema();

            tLMDutyGetClmSchema=CaseFunPub.getGetDutyName(tGetDutycode,tGetDutyKind);
            String tGetDutyName=tLMDutyGetClmSchema.getGetDutyName();        //给负责任名称
            String tAfterGet="";
            if(tLMDutyGetClmSchema.getAfterGet()==null){
                 tAfterGet="000";
            }
            else{
                 tAfterGet=tLMDutyGetClmSchema.getAfterGet();            //险种能力
            }
            System.out.println("tAfterGet="+tAfterGet);                      //为000从数据库得到的为NULL


            if(tAfterGet.equals("003")){
                 tAfterGet="Y";

            }
            else{
                 tAfterGet="N";
            }
%>

					  	top.opener.ClaimDetailGrid.addOne("ClaimDetailGrid");
						  top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,1,"<%=mLLClaimDetailSet.get(i).getPolNo()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,2,"<%=tGetDutyName%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,3,"<%=mLLClaimDetailSet.get(i).getStandPay()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimDetailSet.get(i).getRealPay()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,5,"<%=mLLClaimDetailSet.get(i).getStatType()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,6,"<%=tGetDutyKind%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,7,"<%=mLLClaimDetailSet.get(i).getDutyCode()%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,8,"<%=tGetDutycode%>");
  						top.opener.ClaimDetailGrid.setRowColData(<%=i-1%>,9,"<%=tAfterGet%>");
  						top.opener.emptyUndefined();
<%
					}
					System.out.println("333");
					String tPayInfo[][]= new String[50][50];
					int tSumPol=0;
					int tNowRol=0;
					int tNowCol=0;
					String tFlag = "0" ;
					System.out.println("detailsize==="+mLLClaimDetailSet.size());
					for (int i = 1;i<=mLLClaimDetailSet.size();i++)
					{
						tFlag = "0";
						tNowRol=0;
						tNowCol=0;
						LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
						tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
						System.out.println("44444444");
						System.out.println("tSumPol=="+tSumPol);

						for (int j=1;j<=tSumPol;j++)
						{
							if (tPayInfo[j-1][0].equals(tLLClaimDetailSchema.getPolNo()))
							{
								tFlag = "1";
								tNowRol = j-1;
								break;
							}
						}
						if (tFlag=="0") //无保单记录
						{
							tSumPol = tSumPol + 1;
							tNowRol = tSumPol -1 ;
						}
						tPayInfo[tNowRol][0]= tLLClaimDetailSchema.getPolNo();
						if (tLLClaimDetailSchema.getStatType().equals("SC")) tNowCol =1;
						if (tLLClaimDetailSchema.getStatType().equals("SW")) tNowCol =2;
						if (tLLClaimDetailSchema.getStatType().equals("YL")) tNowCol =3;
						if (tLLClaimDetailSchema.getStatType().equals("TB")) tNowCol =4;
						if (tLLClaimDetailSchema.getStatType().equals("TF")) tNowCol =5;
						if (tLLClaimDetailSchema.getStatType().equals("QT")) tNowCol =6;
						if (tPayInfo[tNowRol][tNowCol]==null)
							tPayInfo[tNowRol][tNowCol] = Double.toString(tLLClaimDetailSchema.getRealPay());
						else
							tPayInfo[tNowRol][tNowCol] = Double.toString(tLLClaimDetailSchema.getRealPay()+Double.parseDouble(tPayInfo[tNowRol][tNowCol]));
					}

					System.out.println("tSumPol=="+tSumPol);

					for (int i =1;i<=tSumPol;i++)
					{

						for (int j = 1;j<=6;j++)
						{
							if (tPayInfo[i-1][j]==null) tPayInfo[i-1][j] = "0.00";
						}

%>


						top.opener.ClaimPayGrid.addOne("ClaimPayGrid");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,1,"<%=tPayInfo[i-1][0]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,2,"<%=tPayInfo[i-1][1]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,3,"<%=tPayInfo[i-1][2]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,4,"<%=tPayInfo[i-1][3]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,5,"<%=tPayInfo[i-1][4]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,6,"<%=tPayInfo[i-1][5]%>");
						top.opener.ClaimPayGrid.setRowColData(<%=i-1%>,7,"<%=tPayInfo[i-1][6]%>");
						top.opener.emptyUndefined();
<%
					}
%>

		      	top.opener.ClaimDetailGrid.checkBoxAll();

    	</script>
		<%
	}

  //接受案件账户的信息
  CaseAccOperateBL mCaseAccOperateBL = new CaseAccOperateBL();
  mLLCaseAccSet.set(mCaseAccOperateBL.getQueryData(request.getParameter("RgtNo")));
  if(mLLCaseAccSet.size()>=1)
  {
    //该案件是关联账户信息的，要将该账户信息显示在案件账户中
    for(int AccCount=1;AccCount<=mLLCaseAccSet.size();AccCount++)
    {
      System.out.println("接受schema");
      LLCaseAccSchema mLLCaseAccSchema = new LLCaseAccSchema();
      mLLCaseAccSchema.setSchema(mLLCaseAccSet.get(AccCount));
      %>
      <script language="javascript">
      top.opener.PolAccGrid.addOne("PolAccGrid");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,1,"<%=mLLCaseAccSchema.getPolNo()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,2,"<%=mLLCaseAccSchema.getInsuAccNo()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,3,"<%=mLLCaseAccSchema.getAccType()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,4,"<%=mLLCaseAccSchema.getAccBJMoney()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,5,"<%=mLLCaseAccSchema.getInterestMoney()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,6,"<%=mLLCaseAccSchema.getTotalMoney()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,8,"<%=mLLCaseAccSchema.getRealPay()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,10,"<%=mLLCaseAccSchema.getGetDutyCode()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,11,"<%=mLLCaseAccSchema.getGetDutyKind()%>");
      top.opener.PolAccGrid.setRowColData(<%=AccCount-1%>,12,"<%=mLLCaseAccSchema.getMasterPolNo()%>");
      top.opener.emptyUndefined();
      </script>
     <%
    }
  }

 // end of if


  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tQueryPayPolUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
%>