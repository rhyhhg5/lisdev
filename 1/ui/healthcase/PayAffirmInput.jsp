<%
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<%
//程序名称:NewClaimUnderWriteInput.jsp
//程序功能：
//创建日期：2002-11-20
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./PayAffirmInput.js"></SCRIPT>


  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./PayAffirmInit.jsp"%>

    <%@include file="../common/jsp/InputButton.jsp"%>
</head>

<body onload="initForm();" >
<form action="./ClaimPD.jsp" method=post name=fm target="fraSubmit">
	<!--form action="./ClaimPD.jsp" method=post name=fm target="_blank"-->
  	<TABLE class=common>
    	<TR  class= common>
      	<TD  class= input width="26%">
        	<Input class= common type=Button value="待确认案件查询" onclick="submitFormPA()">
        	<Input class= common type=Button value="已确认案件查询" onclick="submitFormPB()">
      	</TD>
    	</TR>
	</table>
	
    <TABLE>	
    	<TR>
    	<td>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
      	</td>
      	<td class= titleImg>赔案信息 </td>
    	</tr>
    </table>
    <Div  id= "divLCPol1" style= "display: ''">

    <table  class= common>

      <TR  class= common>
        <TD  class= title width="26%">
        	 赔案号
        </TD>
        <TD  class= input width="26%">
          <Input class= common readonly name=ClmNo >
        </TD>
        <TD  class= title width="26%">
        	 立案号
        </TD>
        <TD  class= input width="26%">
          <Input class= common readonly name=RgtNo >
        </TD>
      </TR>

      <TR  class= common>
       	<TD  class= title width="26%" >
       		事故者姓名
       	</TD>
        <TD  class= input width="26%" >
          <input class=common readonly name=CustomerName >
          <input class=common name=ClmState type=hidden >
        </TD>
      	<TD  class= title width="26%">
      		 理赔结论
      	</TD>
        <TD  class= input width="26%">
          <Input class= common readonly name=GetNoticeNo type=hidden>
          <Input class= code name=CasePayType ondblclick="return showCodeList('CasePayType',[this]);" onkeyup="return showCodeListKey('CasePayType',[this]);">
        </TD>
      </TR>
      
      <TR  class= common>
        <TD  class= title width="26%">
        	 给付金额
        </TD>
        <TD  class= input width="26%">
          <Input class= common readonly name=RealPay >
        </TD>
        <TD  class= title width="26%">
        	 保险金领取方式
        </TD>
        <TD  class= input width="26%">
          <Input class= code readonly name=CaseGetMode ondblclick="return showCodeList('paymode',[this]);" onkeyup="return showCodeListKey('paymode',[this]);">
        </TD>
      </TR>
      
      <TR  class= common>
        <TD  class= title width="26%">
        	 开户银行
        </TD>
        <TD  class= input width="26%">
          <Input class= common readonly name=BankCode >
        </TD>
        <TD  class= title width="26%">
        	 户名
        </TD>
        <TD  class= input width="26%">
          <Input class= common readonly name=AccName >
        </TD>
      </TR>
      
      <TR  class= common>
        <TD  class= title width="26%">
        	 银行账号
        </TD>
        <TD  class= input width="26%">
          <Input class= common readonly name=BankAccNo >
        </TD>
        <Input class= common name=Flag type=hidden >
      </TR>
      
      
      
     
     
     </table>
  	</Div>
       <!-- 理赔信息部分 -->
    <!-- 赔付明细信息部分（列表） -->

  	<Div  id= "divLCBnf2" style= "display: ''">
		<table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divLCBnf2);">
        </td>
        <td class= titleImg> 赔付明细</td>
      </tr>
    </table>
     <table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
				<span id="spanClaimPolGrid" >
				</span>
			</td>
		</tr>
	</table>

	<div>
		<table>
      <TR  class= common>
      	<TD  class= input >
        	<Input class= common type=Button value="给付确认" onclick="Gf_ensure()">
      	</TD>
    	</TR>

    	<TR  class= common>
      	<TD  class= input >
        	<Input class= common type=Button value="理赔批单打印" onclick="submitFormLPPDDY()">
      	</TD>
    		<TD  class= input width="26%">
        	<Input class= common type=Button value="二次核保通知书打印" onclick="submitFormTZSDY()">
        	<input type=hidden name="Opt">
      	</TD>
    	</TR>

    </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>