<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">

function initInpBox( )
{
  try {
    fm.all('RptNo').value = '';
    fm.all('RgtNo').value = '';
    fm.all('PeopleName').value = '';
    fm.all('CustomerNo').value = '';
    fm.all('RgtantName').value = '';
    fm.all('Sex').value = '';
    fm.all('Relation').value = '';
    fm.all('RgtantAddress').value = '';
    fm.all('RgtantPhone').value = '';
    fm.all('RgtantMobile').value = '';
    fm.all('RgtDate').value = '';
    fm.all('AccidentDate').value = '';
    fm.all('AccidentReason').value = '';
    fm.all('AccidentSite').value = '';
    fm.all('AccidentCourse').value = '';
    fm.all('Remark').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('Handler').value = '';
    fm.all('MngCom').value = '';
   	fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('AccName').value = '';
    fm.all('IDType').value = '';
    fm.all('IDNo').value = '';
    fm.all('ApplyType').value = '';
    fm.all('Handler1').value = '';
    fm.all('Handler1Phone').value = '';

  } catch(ex) {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常aaaaaaaaa:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常yyyyyyy:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();
    initCaseGrid();
    initAffixGrid();
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//立案附件信息
function initAffixGrid()
{
    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许



      iArray[1]=new Array();
      iArray[1][0]="附件代码";         			//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="AffixCode"

      iArray[2]=new Array();
      iArray[2][0]="附件名称";         			//列名
      iArray[2][1]="0px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="附件号码";         		//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      AffixGrid = new MulLineEnter( "fm" , "AffixGrid" );
      //这些属性必须在loadMulLine前
      AffixGrid.mulLineCount = 0;
      AffixGrid.displayTitle = 1;
      AffixGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //AffixGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }

//立案分案信息
function initCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="分案号";    	//列名
    iArray[1][1]="0px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="事故者客户号";         			//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="事故者名称";         			//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=2;
    iArray[4][4]="Sex";
    //是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]= new Array();
    iArray[5][0]="证件类型";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=2;
    iArray[5][4]="IDType";

    iArray[6]= new Array();
    iArray[6][0]="证件号码";
    iArray[6][1]="150px";
    iArray[6][2]=100;
    iArray[6][3]=1;

    iArray[7]= new Array();
    iArray[7][0]="年龄";
    iArray[7][1]="40px";
    iArray[7][2]=100;
    iArray[7][3]=1;

    iArray[8]=new Array();
    iArray[8][0]="事故类型";
    iArray[8][1]="50px";
    iArray[8][2]=100;
    iArray[8][3]=0;

    iArray[9]=new Array();
    iArray[9][0]="分报案号码";
    iArray[9][1]="0px";
    iArray[9][2]=100;
    iArray[9][3]=0;

    CaseGrid = new MulLineEnter( "fm" , "CaseGrid" );
    //这些属性必须在loadMulLine前
    CaseGrid.mulLineCount = 0;
    CaseGrid.displayTitle = 1;
    CaseGrid.canChk =1;
    CaseGrid.hiddenPlus=1;
    CaseGrid.locked = 1;
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>