<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>



<%
  LLCasePolicySchema tLLCasePolicySchema   = new LLCasePolicySchema();
  LLCasePolicySet tLLCasePolicySet=new LLCasePolicySet();
  LCPolSet tLCPolSet = new LCPolSet();
  
  TemPolInfoUI tTemPolInfoUI = new TemPolInfoUI();

  CErrors tError = null;
  String transact = "INSERT";

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
	//读取立案明细信息
  String strCaseNo=request.getParameter("CaseNo");
  String strRgtNo = request.getParameter("RgtNo1");
  System.out.println("在jsp中所得到的立案号码是===="+strRgtNo);
  
  String[] tChk = request.getParameterValues("InpTemPolInfoGridChk");
  String[] strNumber=request.getParameterValues("TemPolInfoGridNo");
  String[] strPolNo=request.getParameterValues("TemPolInfoGrid1");
  String[] strAppntName=request.getParameterValues("TemPolInfoGrid2");
  String[] strInsuredName=request.getParameterValues("TemPolInfoGrid3");
  String[] strRiskCode=request.getParameterValues("TemPolInfoGrid5");
  
  String[] strFirstPayDate=request.getParameterValues("TemPolInfoGrid7");
  String[] strCValiDate=request.getParameterValues("TemPolInfoGrid8");

  int intLength=strNumber.length;
  for(int i=0;i<intLength;i++)
  {
  	if(tChk[i].equals("0")) //未选
            continue;

  	tLLCasePolicySchema=new LLCasePolicySchema();
  	tLLCasePolicySchema.setCaseNo(strCaseNo);
  	tLLCasePolicySchema.setRgtNo(strRgtNo);
		tLLCasePolicySchema.setPolNo(strPolNo[i]);
  	tLLCasePolicySchema.setAppntName(strAppntName[i]);
  	tLLCasePolicySchema.setInsuredName(strInsuredName[i]);
  	tLLCasePolicySchema.setRiskCode(strRiskCode[i]);
  	tLLCasePolicySet.add(tLLCasePolicySchema);
  	LCPolSchema tLCPolSchema = new LCPolSchema();
  	tLCPolSchema.setFirstPayDate(strFirstPayDate[i]);
  	tLCPolSchema.setCValiDate(strCValiDate[i]);
  }

    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");

  try
  {
   VData tVData = new VData();

   tVData.addElement(tLLCasePolicySet);
   tVData.addElement(tLCPolSet);
   tVData.addElement(tG);

   tTemPolInfoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (FlagStr=="")
  {
    tError = tTemPolInfoUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>