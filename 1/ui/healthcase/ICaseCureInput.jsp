<html>
<%
/*******************************************************************************
 * Name     :ICaseCureInput.jsp
 * Function :立案－费用明细信息的初始化页面程序
 * Author   :LiuYansong
 * Date     :2003-7-23
 */
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="ICaseCureInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ICaseCureInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ICaseCureSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏ICaseCure1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divICaseCure1);">
      </td>
      <td class= titleImg>
        费用信息明细
      </td>
    	</tr>
    </table>

    <Div  id= "divICaseCure1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD class= title>
            立案号
          </TD>

          <TD class= input>
            <Input class="readonly" readonly name=RgtNo>
          </td>
          <TD  class= title>
            事故者名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
          <!--
          <TD  class= title>
            分案号
          </TD>
          <TD  class= input>
-->
            <Input class= "readonly" readonly name=CaseNo type=hidden>
<!--
          </TD>
-->
        </TR>
        <!--
        <TR  class= common>
          <TD  class= title>
            出险人客户号
          </TD>
          <TD  class= input>
        -->
            <Input class= "readonly" readonly name=CustomerNo type= hidden ><!--
          </TD>

        </TR>-->
        <TR class=common>
          <TD class= title>
            住院日期
          </TD>
          <TD class= input>
           <input class="coolDatePicker" dateFormat="short" name="InHospitalDate" >
          </TD>
          <TD class=title>
            出院日期
          </TD>
          <TD class=input>
            <input class="coolDatePicker" dateFormat="short" name="OutHospitalDate" >
          </TD>
        </TR>

        <TR class= common>
          <TD class=title>
            实际住院天数
          </TD>
          <TD class = input>
            <Input class=common name = "InHospitalDays">
          </TD>
          <TD class=title>
            手术给付类别
          </TD>
          <TD class= input>
           <Input class="code" name=OperationType verify="手术给付类型|NOTNULL"
              CodeData="0|^0|按发生的费用^1|按手术的档次"
              ondblClick="showCodeListEx('OperationTypeList',[this],[0,1]);"
              onkeyup="showCodeListKeyEx('OperationTypeList',[this],[0,1]);">
          </TD>
        </TR>
        <!--
        <TR  class= common>
          <TD  class= title>
            出险类型
          </TD>
          <TD  class= input>
            <Input class= code name=AccidentType ondblclick="return showCodeList('GetDutyKind',[this]);" onkeyup="return showCodeListKey('GetDutyKind',[this]);">
          </TD>
         </TR>
        -->
       </table>
    <!--分案诊疗信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseCure2);">
    		</td>
    		<td class= titleImg>
    			 费用类型
    		</td>
    	</tr>
    </table>

  <Div  id= "divCaseCure2" style= "display: ''">
    <table class= common>
      <tr >
        <td >
          门诊费用明细
        </td>
      </tr>
    </table>

    <table  class= common>
    	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanICaseCureGrid" >
					</span>
				</td>
			</tr>
		</table>

    <Table class= common>
      <TR>
        <TD>
          住院费用明细
        </TD>
      </TR>
    </Table>

    <table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1>
          <span id="spanHospitalGrid" >
          </span>
        </td>
      </tr>
    </table>

    <Table class= common>
      <TR>
        <TD>
          手术信息明细
        </TD>
      </TR>
    </Table>
    </DIV>
    <!--divFeeOperation 按费用录入手术的信息
        divDegreeOperation按档次录入手术的信息
    -->
    <Div id= "divFeeOperation" style = "display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanFeeOperationGrid" >
            </span>
          </td>
        </tr>
      </table>
	  </div>

    <Div id= "divDegreeOperation" style = "display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanDegreeOperationGrid" >
            </span>
          </td>
        </tr>
      </table>
    </div>

    <Div id="divSuccor" style="display:" ''>
      <Table class= common>
        <TR>
          <TD>
            救援费用明细
          </TD>
        </TR>
      </Table>
      <table class=common>
        <tr class= common>
          <td text-align: left colspan=1>
            <span id="spanSuccorGrid">
            </span>
          </td>
        </tr>
      </table>
    </div>
    
    <Div id="divSuccor" style="display:" ''>
      <Table class= common>
        <TR>
          <TD>
            其他费用明细
          </TD>
        </TR>
      </Table>
      <table class=common>
        <tr class= common>
          <td text-align: left colspan=1>
            <span id="spanOtherFeeGrid">
            </span>
          </td>
        </tr>
      </table>
    </div>


         <INPUT VALUE="保存" class=common TYPE=button onclick="submitForm();">
         <INPUT VALUE="返回" class=common TYPE=button onclick="top.close();">
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
  fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';
  fm.CustomerNo.value =' <%= request.getParameter("CustomerNo") %>';
}
catch(ex)
{
  alert(ex);
}
</script>
</html>