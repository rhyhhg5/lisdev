//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
/********
name:TemPolInfoInput.js
function:save temporary pol info
date:2003-04-09
operator:刘岩松

*******/
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug);
  fm.action = './TemPolInfoSave.jsp';
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

   // showDiv(operateButton,"true"); 
   // showDiv(inputButton,"false"); 
    //执行下一步操作

  }

}




//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LLCasePolicy.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
 // showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
 }
/***************
name :showTemPolInfo()
function show temporary pol info;
date :2003-04-09
creator :刘岩松
*/

function returnTemPolInfo()
{
	if ((fm.InsuredNo.value=="null")||(fm.InsuredNo.value==""))
  {
  	alert("客户号码不能为空！！！");
    return;
  }
	else
	{
		var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    initTemPolInfoGrid();
    fm.action = './TemPolReturnDetail.jsp';
    fm.submit(); //提交
  }
}
  
		


function showTemPolInfo()
{
	initTemPolInfoGrid();

	var strSQL = "";
	var tCaseNo=fm.CaseNo.value;
	var tRgtNo=fm.RgtNo1.value;
  strSQL = "select PolNo,AppntName,InsuredName,' ',RiskCode,' ',' ' ,CValiDate from LLCasePolicy where PolType = '0' and CaseNo='"+ tCaseNo + "' and RgtNo='"+ tRgtNo + "'";
	  
	  turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1); 
	  if (turnPage.strQueryResult) 
	  {
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  arrGrid = turnPage.arrDataCacheSet;
	  turnPage.pageDisplayGrid = TemPolInfoGrid;    
	  turnPage.strQuerySql     = strSQL; 
	  turnPage.pageIndex       = 0;  
	  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	  return true;
	}
	return false;
}




  