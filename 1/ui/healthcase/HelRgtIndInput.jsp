<html>
<%
//Name:RegisterInput.jsp
//Function：健康险立案的个人信息录入
//Date：2004-08-13 17:44:28
//Author ：LiuYansong
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="RegisterInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="HeaRegisterInit.jsp"%>
</head>
<body  onload="initForm();">
  <form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table class=common>
      <tr>
        <td> <input class=common type=button value="新增" onclick="showAccident()"> </td>
        <td> <input class=common type=button value="修改" onclick="showFeeInfo()"> </td>
        <td> <input class=common type=button value="重置" onclick="showAccident()"> </td>
        <td> <input class=common type=button value="查询个人信息" onclick="showFeeInfo()"> </td>
      </tr>
    </table>

    <table class=common>
      <TR > <td class= titleImg> 案件信息 </TD> </TR>
    </table>

    <table class=common>
      <TR class=common >
        <TD  class= title > 立案号 </TD>
        <TD  class= input> <Input class= "readonly" readonly name=RgtNo > </TD>
        <TD  class= title > 团单号码 </TD>
        <TD  class= input> <Input class="readonly" readonly name=GrpPolNo > </TD>
      </TR>

      <TR class= common>
        <TD class=title> 立案人 </TD>
        <TD class= input> <Input class = "readonly" readonly name = Operator> </TD>
        <TD  class= title> 理算审核人 </TD>
          <TD  class= input> <Input class= "readonly" readonly name=Handler > </TD>
        </TR>
      </table>

      <table >
      <TR >
        <td class= titleImg> 事故者信息查询 </TD>
      </TR>
    </table>

    <table class = common>
      <TR class= common>
        <TD class= title> 事故者证件类型 </TD>
        <TD class = input> <Input class="code" name=IDType ondblclick="return showCodeList('IDType',[this]);"
              onkeyup="return showCodeListKey('IDType',[this]);" > </TD>
        <TD class= title> 事故者证件号码 </TD>
        <TD class=input> <Input class="common" name=IndIDNo> </TD>
      </TR>

     	<TR  class= common>
        <TD  class= title> 事故者号码 </TD>
        <TD  class= input> <Input class= common name=CustomerNo > </TD>
				<TD  class= title> 事故者姓名 </TD>
        <TD  class= input> <Input class= common name=CustomerName > </TD>
      </TR>

      <TR class= common>
          <TD class= title> 保单号码 </TD>
          <TD class= input> <Input class="common" name=PolNo > </TD>
          <TD class=input> <input class=common type=button value="查询个人信息" onclick="QueryInfo()"> </TD>
      </TR>
    </table>

    <!--显示查询到的所有的客户，由操作人员进行选择-->

    <table>
      <tr>
        <td class= titleImg> 个人信息明细</td>
      </tr>
    </table>

    <table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1>
          <span id="spanCustomerGrid" > </span>
        </td>
      </tr>
		</table>

    <table class=common>
      <TR  class= common>
      <!--
        <TD  class= title> 是否需要调查 </TD>
        <TD  class= input> <Input class="code" name=SurveyFlag verify="方式|NOTNULL"
            CodeData="0|^0|不需要^1|需要"
            ondblClick="showCodeListEx('SurveyType',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('SurveyType',[this],[0,1]);">
        </TD>
        -->

        <TD  class= title> 事故者性质 </TD>
          <TD  class= input>
          	<Input class="code" name=CustomerType verify="性别|NOTNULL" CodeData="0|^0|主被保险人^1|退休人员^2|连带人员^9|其他人员"
              ondblClick="showCodeListEx('CustemerType_1',[this],[0,1,2,3]);"
              onkeyup="showCodeListKeyEx('CustemerType_1',[this],[0,1,2,3]);">
        	</TD>
        </TR>

        <TR class=common>
          <TD  class= title> 事故者现状 </TD>
          <TD  class= input> <Input class=common name=AccidentCouse> </TD>
          <TD  class= title> 事故性质 </TD>
          <TD  class= input> <Input class="code" name=AccidentType verify="方式|NOTNULL"
            CodeData="0|^0|死亡^1|其他"
            ondblClick="showCodeListEx('AccidentType',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('AccidentType',[this],[0,1]);"> </TD>
        </TR>


        <TR  class= common>
          <TD  class= title> 出险日期 </TD>
          <TD  class= input> <input class="coolDatePicker" dateFormat="short" name="AccidentDate" > </TD>
          <TD  class= title> 出险地点 </TD>
          <TD  class= input> <Input class=common name=BankAccNo> </TD>
        </TR>

        <TR  class= common>
          <TD  class= title> 给付金领取方式 </TD>
          <TD class=input> <Input class="code" name=CaseGetMode verify="方式|NOTNULL"
            CodeData="0|^1|现金^4|银行转账"
            ondblClick="showCodeListEx('ModeSelect_1',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('ModeSelect_1',[this],[0,1]);"> </TD>
          <TD  class= title> 转账户名 </TD>
          <TD  class= input> <Input class=common name=AccName > </TD>
        </TR>

        <TR class = common>
          <TD  class= title> 开户银行代码 </TD>
          <TD  class= input> <Input class="code" name=BankCode verify="银行代码|notnull&code:BankCode" ondblclick="return showCodeList('Bank',[this, BankName], [0, 1]);" onkeyup="return showCodeListKey('Bank', [this, BankName], [0, 1]);"> </TD>
          <TD  class= title> 开户银行名称 </TD>
          <TD  class= input> <Input class= "readonly" readonly name= BankName verify="银行名称|notnull&len<=12" > </TD>
        </TR>

        <TR class=common>
          <TD  class= title> 转账帐号 </TD>
          <TD  class= input> <Input class=common name=BankAccNo> </TD>
          <TD  class= title> 确认账号 </TD>
          <TD  class= input> <Input class=common name=BankAccNo> </TD>
        </TR>
    </table>

			<table class= common>
    		<TR  class= common>
         	<TD  class= title> 事故经过描述 </TD>
    		</TR>
    		<TR class= common>
         	<TD  class= input> <textarea name="AccidentReason" cols="110%" rows="3" witdh=25% class="common" readonly ></textarea></TD>
    		</TR>

        <tr class= common>
          <TD  class= title> 备注 </TD>
          </tr>
          <tr>
          <TD  class= common> <textarea name="Remark" cols="110%" rows="3" witdh=25% class="common" readonly></textarea></TD>
        </TR>

     </table>
    <!--个人信息费用（列表） -->

      <table class=common>
      <tr>
   <td>
     <input class=common type=button value="事故明细" onclick="showAccident()">
   </td>
   <td >
     <input class=common type=button value="费用明细" onclick="showFeeInfo()">
   </td>
   <td >
     <input class=common type=button value="残疾明细" onclick="showFeeInfo()">
   </td>
</tr>

</table>
    <!--立案附件信息部分（列表） -->

 <input type=hidden id="fmtransact" name="fmtransact">
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>