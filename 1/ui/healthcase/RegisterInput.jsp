<html>
<%
//Name:RegisterInput.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author ：LiuYansong
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="RegisterInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="RegisterInit.jsp"%>
</head>
<body  onload="initForm();">
  <form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table class= common>
      <TR class = common>
        <!--<TD class=input width="23%">-->
        <TD class= input >
          <input class=common type=button value="报案查询" onclick="RgtQueryRpt()">
        </TD>
        <TD class= input>
          <input class=common type=button value="提起调查报告" onclick="submitFormSurvery()">
        </TD>
        <TD class=input>
        <input class=common type=button value="撤销案件" onclick="CaseCancel()">
        </TD>
        <!--</TD>-->
		    	<!--<input class=common type=hidden value="根据报案号进行查询" onclick="submitForm1()">
 		    	<input class=common  type=hidden value="根据号码类型和号码查询" onclick="submitForm2()">-->
	      <TD class=input >
        	<input class=common  type=button value="打印理赔申请书" onclick="PLPsqs()">
        </td>
      </TR>
    </table>

    <table >
      <TR >
        <td >
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegisterInfo);">
        </td>
        <td class= titleImg>
          立案信息
        </TD>
    	</TR>
    </table>

    <Div id= "divRegisterInfo" style= "display: ''">
    <table class=common>
      <TR class=common >
        <TD  class= title >
          立案号
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name=RgtNo >
        </TD>
        <TD  class= title >
          报案号
        </TD>
        <TD  class= input>
          <Input class="readonly" readonly name=RptNo >
        </TD>
          <Input class="readonly" readonly type = hidden name=ClmState >
          <Input class=common type = hidden name = DisplayFlag >
          <!--用DisplayFlag表示是在立案界面进入的事故明细还是在理算界面上进入的残疾明细；
          两个button使用的是同一个页面
          -->
      </TR>

      <TR class= common>
        <TD class= title>
          理赔状态
        </TD>
        <TD class = input>
          <Input class= "readonly" readonly name= CaseState >
        </TD>
        <TD class= title>
          保险金领取方式
        </TD>
        <TD class=input>
          <Input class="code" name=CaseGetMode verify="方式|NOTNULL"
            CodeData="0|^1|现金^4|银行转账"
            ondblClick="showCodeListEx('ModeSelect_1',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('ModeSelect_1',[this],[0,1]);">
        </TD>
      </TR>
      <TR class= common >
      <!--
        <TD  class= title>
          事故者姓名
        </TD>
        <TD  class= input>
        -->
          <Input class="readonly" readonly name=PeopleName type= hidden>
        <!--
        </TD>
        <TD  class= title>
          客户号
        </TD>
        <TD  class= input>
        -->
          <Input class="readonly" readonly  name=CustomerNo type=hidden>
        <!--
        </TD>
        -->
      </TR>

     	<TR  class= common>
        <TD  class= title>
          申请经办人
        </TD>
        <TD  class= input>
          <Input class= common name=Handler1 >
        </TD>
				<TD  class= title>
          申请经办人电话
        </TD>
        <TD  class= input>
          <Input class= common name=Handler1Phone >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          立案日期
        </TD>
        <TD  class= input>
        	<input class="readonly" readonly name="RgtDate" >
        </TD>
        <TD  class= title>
            出险日期
          </TD>
          <TD  class= input>
          	<input class=common name="AccidentDate" >
           </TD>
        </TR>

       	<TR  class= common>
          <TD  class= title>
            申请权利人姓名
          </TD>
          <TD  class= input>
            <Input class= common name=RgtantName >
          </TD>
          <TD  class= title>
            申请权利人性别
          </TD>
          <TD  class= input>
          	<Input class="code" name=Sex verify="性别|NOTNULL" CodeData="0|^0|男^1|女^2|不详"
              ondblClick="showCodeListEx('SexRegister',[this],[0,1,2,3]);"
              onkeyup="showCodeListKeyEx('SexObjRegister',[this],[0,1,2,3]);">
        	</TD>
        </TR>

				<TR class= common>
        	<TD class= title>
        		申请权利人证件类型
        	</TD>
        	<TD class= input>
        		<Input class="code" name=IDType ondblclick="return showCodeList('IDType',[this]);"
              onkeyup="return showCodeListKey('IDType',[this]);" >
        	</TD>
        	<TD class= title>
        		申请权利人证件号码
        	</TD>
        	<TD class= input>
        		<Input class= common name=IDNo>
        	</TD>
        </TR>

        <TR class= common>
        	<TD class= title>
        		申请权利人身份
        	</TD>
        	<TD class= input>
        		<Input class="code" name=ApplyType
              ondblclick="return showCodeList('ProposerDegree',[this]);"
              onkeyup="return showCodeListKey('ProposerDegree',[this]);" >
        	</TD>
        	<TD  class= title>
            申请权利人与被保人关系
          </TD>
          <TD  class= input>
            <Input class="code" name=Relation
              ondblclick="return showCodeList('Relation',[this]);"
              onkeyup="return showCodeListKey('Relation',[this]);" >
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            申请权利人电话
          </TD>
          <TD  class= input>
            <Input class= common name=RgtantPhone >
          </TD>
          <TD  class= title>
            申请权利人地址
          </TD>
          <TD  class= input>
            <Input class= common name=RgtantAddress >
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            管辖机构
          </TD>
          <TD  class= input>
             <Input class= "readonly" readonly name= MngCom>
					</TD>

          <TD  class= title>
            户名
          </TD>
          <TD  class= input>
            <Input class=common name=AccName >
          </TD>
        </TR>

        <TR>
          <TD  class= title>
            开户银行代码
          </TD>
          <TD  class= input>
          <!--
            <Input class= code name= BankCode ondblclick="return showCodeList('Bank',[this]);" onkeyup="return showCodeListKey('Bank',[this]);">
          -->
          <Input class="code" name=BankCode verify="银行代码|notnull&code:BankCode" ondblclick="return showCodeList('Bank',[this, BankName], [0, 1]);" onkeyup="return showCodeListKey('Bank', [this, BankName], [0, 1]);">
          </TD>
          <TD  class= title>
            开户银行名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name= BankName verify="银行名称|notnull&len<=12" >
          </TD>

        </TR>
        <TR>
          <TD  class= title>
            帐号
          </TD>
          <TD  class= input>
            <Input class=common name=BankAccNo>
          </TD>
          <TD  class= title>
            确认账号
          </TD>
          <TD  class= input>
            <Input class= common name=RgtantMobile >
          </TD>
        </TR>


      <TR class= common>
        <TD class=title>
          立案人
        </TD>
        <TD class= input>
          <Input class = "readonly" readonly name = Operator>
        </TD>
        <TD  class= title>
            理算审核人
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Handler >
          </TD>
        </TR>

        <tr>
          <TD  class= input>
          	<Input class="code" type="hidden" name=AgentCode verify="代理人编码|notnull&code:AgentCode"
              ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2], null, MngCom, 'ManageCom');"
              onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2], null, MngCom, 'ManageCom');">
					</TD>
          <TD  class= input>
            <Input class="readonly" type="hidden" readonly name=AgentGroup verify="代理人组别|notnull&len<=12" >
          </TD>
        </TR>

    </table>
    </Div>

			<table class= common>
    		<TR  class= common>
         	<TD  class= title>
            事故经过描述
         </TD>
    		</TR>
    		<TR class= common>
         	<TD  class= input>
						<textarea name="AccidentReason" cols="110%" rows="3" witdh=25% class="common" readonly ></textarea></TD>
					</TD>
    		</TR>
    		<TR class= common>
         	<TD  class= title>
            出险地点
         	</TD>
        </tr>
        <tr class= common>
         	<TD  class= input>
						<textarea name="AccidentSite" cols="110%" rows="3" witdh=25% class="common" readonly ></textarea></TD>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            事故者现状
          </TD>
          </tr>
          <tr>
          <TD  class= input>
          <textarea name="AccidentCourse" cols="110%" rows="3" witdh=25% class="common" readonly></textarea></TD>

          </TD>
          </tr>
          <tr class= common>
          <TD  class= title>
            备注
          </TD>
          </tr>
          <tr>
          <TD  class= common>
          <textarea name="Remark" cols="110%" rows="3" witdh=25% class="common" readonly></textarea></TD>
   				</TD>
        </TR>

        <tr class= common>
          <TD  class= title>
            案件撤销原因
          </TD>
        </tr>
        <tr>
          <TD  class= input>
            <textarea name="RgtReason" cols="110%" rows="3" witdh=25% class="common"></textarea></TD>
           </TD>
        </TR>

</table>
    </Div>
    <!--立案分案信息部分（列表） -->
    <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCase);">
    	</td>
    	<td class= titleImg>
    	 客户信息
    	</td>
    </tr>
      </table>
      <table class=common>
      <tr>
  <td>
		<input class=common type=button value="临时保单立案险种明细" onclick="showTemPolInfo()">
	</td>

	 <td>
  	<input class=common type=button value="立案险种明细" onclick="showCasePolicy()">
   </TD>
<!--
   <TD>
  	<input class=common type=button value="门诊明细" onclick="showCaseReceipt()">
   </TD>
   <TD>
    <input class=common type=button value="住院明细" onclick="showICaseCure()">
   </TD>
   <TD>
    <input class=common type=hidden value="分案疾病伤残明细" onclick="showPCaseCure()">
   </td>
   -->
   <td>
     <input class=common type=button value="事故明细" onclick="showAccident()">
   </td>
   <td >
     <input class=common type=button value="费用明细" onclick="showFeeInfo()">
   </td>
</tr>

</table>
	<Div  id= "divCase" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCaseGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
    <!--立案附件信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRgtAffix);">
    		</td>
    		<td class= titleImg>
    			 
    		</td>
    	</tr>
    </table>
	<Div  id= "divRgtAffix" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanAffixGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>

 <input type=hidden id="fmtransact" name="fmtransact">
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>