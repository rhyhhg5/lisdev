<%
//程序名称：ClaimInput.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单信息部分
    fm.all('ClmNo').value = '';
    fm.all('RgtNo').value = '';
  
  }
  catch(ex)
  {
    alert("在ProposalInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function DShowDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divShowDetail.style.left=ex;
  	divShowDetail.style.top =ey;
    divShowDetail.style.display ='';
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initClaimUWDetailGrid();
	initPolAccGrid();
	initClaimErrorGrid();
	initClaimPolGrid();
      initClaimDetailGrid();
      initClaimPayGrid();
      initCaseGrid();
  }
  catch(re)
  {
    alert("ProposalInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 被保人信息列表的初始化
function initClaimErrorGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保单号";    	//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="赔案号";    	//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[3]=new Array();
      iArray[3][0]="核赔规则编码";    	//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="核赔警告信息";         			//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="核赔级别";         			//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      
      
      ClaimErrorGrid = new MulLineEnter( "fm" , "ClaimErrorGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimErrorGrid.mulLineCount = 3;   
      ClaimErrorGrid.displayTitle = 1;
      //ClaimErrorGrid.canSel=1;
      //ClaimUWDetailGrid.canChk=1;
      //ClaimUWDetailGrid.tableWidth = 200;
      ClaimErrorGrid.locked=1;
      ClaimErrorGrid.loadMulLine(iArray);  
      ClaimErrorGrid.detailInfo="单击显示详细信息";

      ClaimErrorGrid.detailClick=DShowDetailClick;
      
      //这些操作必须在loadMulLine后面
      //ClaimUWDetailGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}



function initClaimUWDetailGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保单号";    	//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="被保人姓名";    	//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[3]=new Array();
      iArray[3][0]="给付责任类型";    	//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="结算赔付金额";         			//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="自动核赔结果";         			//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[6]=new Array();
      iArray[6][0]="核赔意见";         			//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=2;
      iArray[6][10]="ManchkOpn1"; // 名字最好有唯一性
      iArray[6][11]= "0|^0|拒赔|^1|赔付" ;
              			//是否允许输入,1表示允许，0表示不允许
	
      iArray[7]=new Array();
      iArray[7][0]="核赔员";         			//列名
      iArray[7][1]="120px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      iArray[8]=new Array();
      iArray[8][0]="核赔级别";         			//列名
      iArray[8][1]="120px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      ClaimUWDetailGrid = new MulLineEnter( "fm" , "ClaimUWDetailGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimUWDetailGrid.mulLineCount = 3;   
      ClaimUWDetailGrid.displayTitle = 1;
      ClaimUWDetailGrid.canSel=1;
      //ClaimUWDetailGrid.canChk=1;
      //ClaimUWDetailGrid.tableWidth = 200;
      ClaimUWDetailGrid.locked=1;
      ClaimUWDetailGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ClaimUWDetailGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initClaimPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         			//列名
      iArray[1][1]="150px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[2]=new Array();
      iArray[2][0]="险种";         			//列名
      iArray[2][1]="60px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="分案号";         			//列名
      iArray[3][1]="0px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[4]=new Array();
      iArray[4][0]="被保人客户号";         			//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="保单生效日期";         			//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="核算赔付金额";         			//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="实际赔付金额";         			//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      ClaimPolGrid = new MulLineEnter( "fm" , "ClaimPolGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimPolGrid.mulLineCount = 0;   
      ClaimPolGrid.displayTitle = 1;
      //ClaimPolGrid.canSel=1;
      //ClaimPolGrid.canChk=0;
      ClaimPolGrid.locked=1;
      ClaimPolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initClaimDetailGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="150px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="给付责任编码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="150px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="核算赔付金额";         			//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;  
      
      iArray[4]=new Array();
      iArray[4][0]="实际赔付金额";         			//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;  
                  			//是否允许输入,1表示允许，0表示不允许
      iArray[5]=new Array();
      iArray[5][0]="统计类型";         			//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;  
      
      iArray[6]=new Array();
      iArray[6][0]="给付责任类型";         			//列名
      iArray[6][1]="150px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;  
      
      iArray[7]=new Array();
      iArray[7][0]="责任编码";         			//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;  
      
      

      ClaimDetailGrid = new MulLineEnter( "fm" , "ClaimDetailGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimDetailGrid.mulLineCount = 0;   
      ClaimDetailGrid.displayTitle = 1;
      //ClaimDetailGrid.canSel=0;
      //ClaimDetailGrid.canChk=0;
      ClaimDetailGrid.locked=1;
      ClaimDetailGrid.loadMulLine(iArray);      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


	function initClaimPayGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="150px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="伤残给付";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="90px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="死亡给付";         			//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="医疗给付";         			//列名
      iArray[4][1]="90px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="退保金";         			//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="退费";         			//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="其他";         			//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="给付合计";         			//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      ClaimPayGrid = new MulLineEnter( "fm" , "ClaimPayGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimPayGrid.mulLineCount = 0;   
      ClaimPayGrid.displayTitle = 1;
//      ClaimPayGrid.canSel=0;
//      ClaimPayGrid.canChk=1;
      //InsuredGrid.tableWidth = 200;
      ClaimPayGrid.locked=1;
      ClaimPayGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
//立案分案信息
function initCaseGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="分案号";    	//列名
      iArray[1][1]="0px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="事故者客户号";         			//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="事故者名称";         			//列名
      iArray[3][1]="200px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="分报案号";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      CaseGrid = new MulLineEnter( "fm1" , "CaseGrid" ); 
      //这些属性必须在loadMulLine前
      CaseGrid.mulLineCount = 1;   
      CaseGrid.displayTitle = 1;
      CaseGrid.canSel = 1;
      CaseGrid.locked=1;
      CaseGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //CaseGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }

//保险账户明细信息
function initPolAccGrid()
{
    var iArray = new Array();

    try
    {
      		iArray[0]=new Array();
      		iArray[0][0]="序号";    	           //列名
      		iArray[0][1]="30px";            		//列宽
      		iArray[0][2]=100;            			//列最大值
      		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

      		iArray[1]=new Array();
      		iArray[1][0]="保单号";         			//列名
      		iArray[1][1]="150px";         			//列宽
      		iArray[1][2]=10;          			    //列最大值
      		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      		iArray[2]=new Array();
      		iArray[2][0]="保险账户号码";         			//列名
      		iArray[2][1]="200px";         			//列宽
      		iArray[2][2]=10;          			//列最大值
      		iArray[2][3]=2;
      		iArray[2][4]="acctype";
      		              			//是否允许输入,1表示允许，0表示不允许
      		
      		iArray[3]=new Array();
      		iArray[3][0]="账户类型";         			//列名
      		iArray[3][1]="60px";            		//列宽
      		iArray[3][2]=100;            			//列最大值
      		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      		
      		
      		iArray[4]=new Array();
      		iArray[4][0]="帐户余额";       //列名
      		iArray[4][1]="110px";            	//列宽
      		iArray[4][2]=100;            			//列最大值
      		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      		iArray[5]=new Array();
      		iArray[5][0]="账户利息";         //列名
      		iArray[5][1]="90px";            		//列宽
      		iArray[5][2]=100;            			  //列最大值
      		iArray[5][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
      		
      		iArray[6]=new Array();
      		iArray[6][0]="账户合计金额";         //列名
      		iArray[6][1]="150px";            		//列宽
      		iArray[6][2]=100;            			  //列最大值
      		iArray[6][3]=0;
      		


      		iArray[7]=new Array();
      		iArray[7][0]="核算赔付金额";       //列名
      		iArray[7][1]="0px";            	//列宽
      		iArray[7][2]=0;            			//列最大值
      		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      		
      		iArray[8]=new Array();
      		iArray[8][0]="实际赔付金额";      //列名
      		iArray[8][1]="100px";            	//列宽
      		iArray[8][2]=100;            			//列最大值
      		iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      		
      		iArray[9]=new Array();
      		iArray[9][0]="备注";         		//列名
      		iArray[9][1]="100px";            		  //列宽
      		iArray[9][2]=100;            			  //列最大值
      		iArray[9][3]=1;
                    			                //是否允许输入,1表示允许，0表示不允许
      		iArray[10]=new Array();
      		iArray[10][0]="给付责任编码";           //列名
      		iArray[10][1]="100px";            		//列宽
      		iArray[10][2]=100;            			//列最大值
      		iArray[10][3]=0;
      		
      		iArray[11]=new Array();
      		iArray[11][0]="给付责任类型";         			//列名
      		iArray[11][1]="100px";            	//列宽
      		iArray[11][2]=100;            			//列最大值
      		iArray[11][3]=1;
      		
      		iArray[12]=new Array();
      		iArray[12][0]="关联保单号码";       //列名
      		iArray[12][1]="150px";            	//列宽
      		iArray[12][2]=150;            			//列最大值
      		iArray[12][3]=0;
      		
      		PolAccGrid = new MulLineEnter( "fm" , "PolAccGrid" );
      		//这些属性必须在loadMulLine前
      		PolAccGrid.mulLineCount = 0;
      		PolAccGrid.displayTitle = 1;
      		//PolAccGrid.canSel=0;
      		//PolAccGrid.canChk=0;
      		PolAccGrid.locked = 1;
      		PolAccGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}




</script>