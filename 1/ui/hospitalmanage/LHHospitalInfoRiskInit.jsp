<%
//程序名称：LHHospitalInfoRiskInit.jsp
//程序功能：
//创建日期：2002-12-25 14:25:36
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('Riskcode').value = '';   
    fm.all('Riskname').value = '';   
  }
  catch(ex)
  {
    alert("在LHHospitalInfoRiskInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LHHospitalInfoRiskInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();  
    initLHRiskInfoGrid();
  }
  catch(re)
  {
    alert("LHHospitalInfoRiskInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initLHRiskInfoGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="赔付险种代码";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="赔付险种名称";         		//列名
      iArray[2][1]="240px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      LHRiskInfoGrid = new MulLineEnter( "fm" , "LHRiskInfoGrid" ); 
      //这些属性必须在loadMulLine前
      LHRiskInfoGrid.mulLineCount = 10;   
      LHRiskInfoGrid.displayTitle = 1;
      LHRiskInfoGrid.locked = 1;
      LHRiskInfoGrid.canSel = 0;
      LHRiskInfoGrid.hiddenPlus=1;
      LHRiskInfoGrid.hiddenSubtraction=1;
      LHRiskInfoGrid.canChk =1;      
      LHRiskInfoGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>