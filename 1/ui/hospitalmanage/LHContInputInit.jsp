<%
//程序名称：LHContInput.jsp
//程序功能：
//创建日期：2005-03-19 15:05:48
//创建人  ：CrtHtml程序创建
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
     String setting = request.getParameter("setting");
     String ContraNo = request.getParameter("ContraNo");
     String ContraItemNo = request.getParameter("ContraItemNo");
     String Flag = request.getParameter("Flag");
     System.out.println("dddddddddd "+setting);
%>                            
<script language="JavaScript">
	
function initInpBox()
{ 
  try
  {                                   
   
    fm.all('ContraNo').value = "";
    fm.all('ContraName').value = "";
    fm.all('IdiogrDate').value = "";
    fm.all('ContraBeginDate').value = "";
    fm.all('ContraEndDate').value = "";
    fm.all('ContraState').value = ""; 
    fm.all('iscomefromquery').value="0";
    fm.all('DoctName').value="";
    fm.all('DoctNo').value="";
    fm.all('ContraStateName').value = "";
    fm.all('HiddenBtn').value="<%=setting%>"; //从关联页面进入时为1

    //alert(fm.all('HiddenBtn').value);
    fm.all('Flag').value="<%=Flag%>";

    if(fm.all('Flag').value=="A")//关联页面为空进入此操作
    {
    	fm.all('ContraNo2').value="<%=ContraNo%>"; 
      fm.all('ContraItemNo2').value="<%=ContraItemNo%>"; 
    }
    if(fm.all('Flag').value=="B")//关联页面修改要关联的信息时进入此操作
    {
    		fm.all('HiddenContraNo').value="<%=ContraNo%>"; 
        fm.all('HiddenContraItemNo').value="<%=ContraItemNo%>"; 
        //alert(fm.all('HiddenContraItemNo').value);
    }
  }
  catch(ex)
  {
    alert("在LHContInputInit.jsp-->InitInpBoxAAAAAAAAA函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHContInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initContGrid();
    if(fm.all('HiddenBtn').value=="1")
    {
    	//disabled=true modifyButton deleteButton querybutton saveButton使不好使
    	  fm.all('querybutton').disabled=true;
    	  fm.all('deleteButton').disabled=true;
	  }
	 
    if(fm.all('HiddenBtn').value==""|| fm.all('HiddenBtn').value=="null" || fm.all('HiddenBtn').value==null ||fm.all('HiddenBtn').value==" " )
    {
    	fm.all('querybutton').disabled=false;
    	fm.all('deleteButton').disabled=false;
   	}
   
   	if(fm.all('HiddenContraNo').value!=null && fm.all('HiddenContraNo').value!= "" &&fm.all('HiddenContraNo').value!= "null")
   	{
   		    
   		    var strSql = "select ContraNo,ContraName,DoctNo,IdiogrDate, ContraBeginDate,ContraEndDate,ContraState,MakeDate,MakeTime from LHUnitContra where 1=1 "
          +" and ContraNo='"+fm.all('HiddenContraNo').value+"'";  

          var arrContInput=easyExecSql(strSql);
          fm.all('ContraNo').value       = arrContInput[0][0];
          fm.all('ContraName').value     = arrContInput[0][1];
          fm.all('DoctNo').value         = arrContInput[0][2];
          fm.DoctName.value = easyExecSql("select DoctName from lddoctor where doctno = '"+arrContInput[0][2]+"'");
          fm.all('IdiogrDate').value     = arrContInput[0][3];
          fm.all('ContraBeginDate').value= arrContInput[0][4];
          fm.all('ContraEndDate').value  = arrContInput[0][5];
          fm.all('ContraState').value    = arrContInput[0][6];
          if(arrContInput[0][6]=="1"){fm.all('ContraStateName').value="有效";}if(arrContInput[0][6]=="2"){fm.all('ContraStateName').value="无效";}//else{fm.all('ContraStateName').value="";}
          fm.MakeDate.value              =arrContInput[0][7];
          fm.MakeTime.value              =arrContInput[0][8];
                                                     
            	var mulSql = "select (select Codename from ldcode where b.ContraType=ldcode.code  and ldcode.Codetype='hmdeftype'),"
                 	+"(select c.DutyItemName from LDContraItemDuty c where c.DutyItemCode = b.dutyItemCode),"                                  
				 					+" (select case when b.dutystate = '1' then '有效' else '无效' end from dual),"
				 					+"(select case when b.Contraflag = 'Y' then '是' else '否' end from ldcode where Codetype='yesno' and b.Contraflag=ldcode.code),"
				 					+"b.ContraType,b.DutyItemCode,b.dutystate,"
									+"b.Contraflag,b.ContraItemNo"
			  					+" from lhunitcontra a,lhcontitem b,LDContraItemDuty c "
        					+" where a.contrano ='"+fm.all('HiddenContraNo').value+"' "
        					+" and b.contrano='"+fm.all('HiddenContraNo').value+"' "
        					+" and c.DutyItemCode = b.DutyItemCode and b.ContraItemNo='"+fm.all('HiddenContraItemNo').value+"'"
        					;
        turnPage.pageLineNum = 200;
         turnPage.queryModal(mulSql,ContGrid);    
   	}
   	if(fm.all('HiddenContraNo').value=="" || fm.all('HiddenContraNo').value=="null")
    {
          // alert("请输入想要关联的个人合同信息!");
    }
  }
  catch(re)
  {
    alert("LHContInputInit.jsp-->InitFormBBBBBBBBBb函数中发生异常:初始化界面错误!");
  }
}
     
function lhUnitFLag()
{
	     ContGrid.addOne();
	     var rowNum=ContGrid. mulLineCount ; //行数 
	     alert(ContGrid.setRowColData(rowNum-1, 4,"是"));
	    // ContGrid.setRowColData(rowNum, 4)="是";
	    //  fm.all('ContGrid').value="是";
}
/*function initContGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="责任项目代码";   
		iArray[1][1]="0px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		
		iArray[2]=new Array(); 
		iArray[2][0]="责任项目名称";   
		iArray[2][1]="70px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="hmdutyitemdef";
    iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
    iArray[2][6]="0|1";     //上面的列中放置引用代码中第几位值
    iArray[2][9]="责任项目名称|len<=120";
    iArray[2][15]="DutyItemName";
    iArray[2][17]="2";
    iArray[2][19]="1" ;
		
		
		iArray[3]=new Array(); 
		iArray[3][0]="责任状态代码";   
		iArray[3][1]="0px";   
		iArray[3][2]=20;        
		iArray[3][3]=3;
		
		
		iArray[4]=new Array(); 
		iArray[4][0]="责任当前状态";   
		iArray[4][1]="75px";   
		iArray[4][2]=20;        
		iArray[4][3]=2;
		iArray[4][5]="4|3";     //引用代码对应第几列，'|'为分割符            
    iArray[4][6]="1|0";     //上面的列中放置引用代码中第几位值            
		iArray[4][4]="dutystate";
		iArray[4][17]="1"; 
		iArray[4][18]="160";
    iArray[4][19]="1" ;

		
		iArray[5]=new Array(); 
		iArray[5][0]="具体责任描述";   
		iArray[5][1]="70px";   
		iArray[5][2]=20;        
		iArray[5][3]=1;
		iArray[5][7]="inputDutyExolai";
		
		iArray[6]=new Array(); 
		iArray[6][0]="费用说明";   
		iArray[6][1]="120px";
		iArray[6][2]=20;        
		iArray[6][3]=1;
		iArray[6][7]="inputFeeExplai";
		
		iArray[7]=new Array(); 
		iArray[7][0]="合同责任项目编号";   
		iArray[7][1]="0px";
		iArray[7][2]=20;        
		iArray[7][3]=3;
		
		iArray[8]=new Array(); 
		iArray[8][0]="合同项目序号";   
		iArray[8][1]="0px";
		iArray[8][2]=20;        
		iArray[8][3]=3;
		
		

    
    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
    //这些属性必须在loadMulLine前

    ContGrid.mulLineCount = 0;   
    ContGrid.displayTitle = 1;
    ContGrid.hiddenPlus = 0;
    ContGrid.hiddenSubtraction = 0;
    
    
  

    ContGrid.loadMulLine(iArray);  
    
    
  }
  catch(ex) {
    alert(ex);
  }
}*/
function initContGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="合同责任项目类型";   
		iArray[1][1]="108px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
		iArray[1][4]="hmdeftype";
		iArray[1][5]="1|5";
		iArray[1][6]="1|0";
		//iArray[1][9]="合同责任项目类型|notnull";
		iArray[1][17]="1";
		iArray[1][18]="200"; 
		iArray[1][19]="1" ;
     
		iArray[2]=new Array(); 
		iArray[2][0]="合同责任项目名称";   
		iArray[2][1]="108px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="hmdutyitemdef";
		iArray[2][5]="2|6";
		iArray[2][6]="0|1";
		//iArray[2][10]="Dutyitemtye";
		iArray[2][15]="DutyItemName";
		iArray[2][17]="5";
		iArray[2][18]="200"; 
		iArray[2][19]="1" ;      //强制刷新数据源DutyItemName  

		
		iArray[3]=new Array(); 
		iArray[3][0]="责任当前状态";   
		iArray[3][1]="129px";   
		iArray[3][2]=20;        
		iArray[3][3]=2;
		iArray[3][4]="dutystate";
		iArray[3][5]="3|7";     //引用代码对应第几列，'|'为分割符            
    iArray[3][6]="1|0";     //上面的列中放置引用代码中第几位值     
		iArray[3][17]="1"; 
		iArray[3][18]="260";
    iArray[3][19]="1" ;
		
		iArray[4]=new Array(); 
		iArray[4][0]="关联责任标识";   
		iArray[4][1]="129px";   
		iArray[4][2]=20;        
		iArray[4][3]=2;
		iArray[4][5]="4|8";     //引用代码对应第几列，'|'为分割符            
    iArray[4][6]="1|0";     //上面的列中放置引用代码中第几位值 
   //alert(fm.all('HiddenBtn').value);
    if(fm.all('HiddenBtn').value=="1")
    {
    	 iArray[4][14]="是";
    	 iArray[4][4]="";
    }
    else
    {       
		   iArray[4][14]="否";
		 //  iArray[4][4]="yesno";
    	  
		}   
		iArray[4][17]="1"; 
		iArray[4][18]="200";
    iArray[4][19]="1" ;
		
		iArray[5]=new Array(); 
		iArray[5][0]="hmdeftype";   
		iArray[5][1]="0px";   
		iArray[5][2]=20;        
		iArray[5][3]=3;
		
		iArray[6]=new Array(); 
		iArray[6][0]="DutyItemName";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3;
		
		iArray[7]=new Array(); 
		iArray[7][0]="dutystate";   
		iArray[7][1]="0px";   
		iArray[7][2]=20;        
		iArray[7][3]=3;
		
		iArray[8]=new Array(); 
		iArray[8][0]="";   
		iArray[8][1]="0px";   
		iArray[8][2]=20;        
		iArray[8][3]=3;
		if(fm.all('HiddenBtn').value=="1")
		{
			iArray[8][14]="Y";
		}
		else
		{
        iArray[8][14]="N";
    }
		
		iArray[9]=new Array(); 
		iArray[9][0]="contraitemno";   
		iArray[9][1]="0px";   
		iArray[9][2]=20;        
		iArray[9][3]=3;  
    
    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
    //这些属性必须在loadMulLine前

   
    ContGrid.displayTitle = 1;
    //alert(fm.all('HiddenBtn').value);
    if(fm.all('HiddenBtn').value=="1")
    {
       ContGrid.hiddenPlus = 1;
       ContGrid.mulLineCount = 1;   
       ContGrid.hiddenSubtraction = 1;
    }
    else
    {
    	 ContGrid.hiddenPlus = 0;
    	 ContGrid.mulLineCount = 0;  
    	 ContGrid.hiddenSubtraction = 0; 
    }
   // ContGrid.hiddenSubtraction = 0;
    if(fm.all('HiddenBtn').value==""||fm.all('HiddenBtn').value==null||fm.all('HiddenBtn').value==" ")
    {
    	 fm.all('getAssociateSetting').disabled=true;
    	 //ContGrid.selBoxEventFuncName ="";
   	}

   	//if(fm.all('HiddenBtn').value=="1")
   	//{
       ContGrid.selBoxEventFuncName ="buttonFalse";
   // }
    ContGrid.canSel = 1;  
    ContGrid.loadMulLine(iArray);  
    
    
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
