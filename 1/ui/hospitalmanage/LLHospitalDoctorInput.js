//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------

//查询按键
var turnPage=new turnPageClass();
var turnPage1=new turnPageClass();
function queryClick()
{
	initDataGrid();
	initDataTable();
	initText();
	var strManageCom=document.all("ManageCom").value;
	var strStartDate=document.all("StartDate").value;
	var strEndDate=document.all("EndDate").value;
	
	var strSql="select Charger,Professional,HospitalName,count(ProblemDetail) as total from LLMedicalManagement where ManageCom  like'"+strManageCom+"%%'";
	if(strStartDate!=""&&strStartDate!=null)
	{
		strSql+=" and BussinessDate>='"+strStartDate+"'";
	}
	if(strEndDate!=""&&strEndDate!=null)
	{
		strSql+=" and BussinessDate<='"+strEndDate+"'";
	}
	
	strSql+=" group by Charger,Professional,HospitalName order by total desc";
	
	turnPage.strQueryResult=easyQueryVer3(strSql, 1, 0, 1);
  	if (!turnPage.strQueryResult)
    {
  		 //window.alert("数据库中无该行记录！");
  		 return false;
  	}
  	
    //查询成功则拆分字符串，返回二维数组
     var arrDataSet=decodeEasyQueryResult(turnPage.strQueryResult);
     turnPage.arrDataCacheSet=arrDataSet;
     //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
     turnPage.pageDisplayGrid = DataGrid;
     turnPage.strQueryResult = easyQueryVer3(strSql, 1, 0, 1);
     turnPage.strQuerySql=strSql;
  	
    //设置查询起始位置
     turnPage.pageIndex=0;
     
     //在查询结果数组中取出符合页面显示大小设置的数组
     var tArr = new Array();
     tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);
     //调用MULTILINE对象显示查询结果
     displayMultiline(tArr, turnPage.pageDisplayGrid);
}
//------------------------------------------------------

function showDataGrid()
{
	initDataTable();
	initText();
	var selno = DataGrid.getSelNo();
	if (selno<=0)
	{
	    return;
	}
	var strManageCom=document.all("ManageCom").value;
	if(strManageCom==null||strManageCom=="")
	{
		return false;
	}
	var strCharger = DataGrid.getRowColData(selno - 1, 1);
	//var strProfessional=DataGrid.getRowColData(selno-1,2);
	var strHospitalName=DataGrid.getRowColData(selno-1,3);
	if(strCharger==null||strCharger=="")
	{
		window.alert("数据录入错误,责任人不得为空!");
		return false;
	}
	var strSql="select Charger,Professional,HospitalName,BussinessDate,MsgChannel,SerialNo from LLMedicalManagement where ManageCom  like '"+strManageCom+"%%' and Charger='"+strCharger+"'";
	/*if(strProfessional!=""&&strProfessional!=null)
	{
		strSql+=" and Professional='"+strProfessional+"'";
	}*/
	if(strHospitalName!=""&&strHospitalName!=null)
	{
		strSql+=" and HospitalName='"+strHospitalName+"'";
	}
	strSql+=" and ProblemDetail is not NULL order by BussinessDate desc";
	turnPage1.queryModal(strSql,DataTable);
}

function showDetail()
{
	initText();
	var selno = DataTable.getSelNo();
	if (selno<=0)
	{
	    return;
	}

	//var strCharger = DataTable.getRowColData(selno - 1, 1);
	//var strProfessional=DataTable.getRowColData(selno-1,2);
	//var strHospitalName=DataTable.getRowColData(selno-1,3);
	//var strBussinessDate=DataTable.getRowColData(selno-1,4);
	//var strMsgChannel=DataTable.getRowColData(selno-1,5);
	var strSerialNo=DataTable.getRowColData(selno-1,6);
	
	/*if(strCharger==null||strCharger=="")
	{
		return;
	}*/
	var strSql="select ProblemDetail,Action,SerialNo from LLMedicalManagement where SerialNo='"+strSerialNo+"'";
	/*if(strCharger!=""&&strCharger!=null)
	{
		strSql+=" and Charger='"+strCharger+"'";
	}
	if(strProfessional!=""&&strProfessional!=null)
	{
		strSql+=" and Professional='"+strProfessional+"'";
	}
	if(strHospitalName!=""&&strHospitalName!=null)
	{
		strSql+=" and HospitalName='"+strHospitalName+"'";
	}
	if(strBussinessDate!=""&&strBussinessDate!=null)
	{
		strSql+=" and BussinessDate='"+strBussinessDate+"'";
	}
	if(strMsgChannel!=""&&strMsgChannel!=null)
	{
		strSql+=" and MsgChannel='"+strMsgChannel+"'";
	}*/
	
	turnPage1.strQueryResult=easyQueryVer3(strSql, 1, 0, 1);
  	if (!turnPage1.strQueryResult)
    {
  		 //window.alert("数据库中无该行记录！");
  		 return false;
  	}
  	
    //查询成功则拆分字符串，返回二维数组
     var arrDataSet=decodeEasyQueryResult(turnPage1.strQueryResult);
     document.all["ProblemDetail"].value=arrDataSet[0][0];
     document.all["Action"].value=arrDataSet[0][1];
	 document.all["ProblemDetail"].readOnly=true;
	 document.all["Action"].readOnly=true;
	 
	 document.all["queryscan"].value=arrDataSet[0][2];
}

function initText()
{
	document.all["ProblemDetail"].value="";
	document.all["ProblemDetail"].readOnly=true;
	document.all["Action"].value="";
	document.all["Action"].readOnly=true;
}

//查看扫描件
function queryScan()
{
    
    if(document.all["queryscan"].value!=""&&document.all["queryscan"].value!=null)
    {
   		var strSerialNo=document.all["queryscan"].value;
   		
   		//-----------------------------查看扫描件---------------------------
   		var docId = "";
		var sql = " select docid from es_doc_main where Busstype='HM' and subtype='HM23' and DocCode='" 
								+ strSerialNo + "' ";
		var arr = easyExecSql(sql);
		if(!arr) 
		{
			window.alert("数据库中无该扫描件!");
			return false;
		} 
		else 
		{
			docId = arr[0][0];
		}
	
		var url="./LLHospitalScanMain.jsp?DocID="+docId+"&DocCode=" + strSerialNo ;
		window.open(url);
    }
    else
    {
        window.alert("请选中某一行记录!");
        return false;
    }
}