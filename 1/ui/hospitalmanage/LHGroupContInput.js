//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var selectedDutyExolaiRow;
var selectedFeeExplaiRow;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	  // var d = new Date();
	  // var h = d.getYear();
	  // var m = d.getMonth(); 
	  // var day = d.getDate();  
	  // var CuttDate;       
	  // if(h<10){h = "0"+d.getYear();}  
	  // if(m<9){ m++; m = "0"+m;}
	  // else{m++;}
	  // if(day<10){day = "0"+d.getDate();}
	  // CuttDate = h+"-"+m+"-"+day;
	  // //alert(CuttDate);
	  // //alert(fm.all('ContraEndDate').value);
	  // if(compareDate(fm.all('ContraEndDate').value,CuttDate) == true)
	  // {
	  // 	  alert("您输入的合同终止时间不能晚于系统的当前日期!");
	  // 	  return false;
	  // }
	  // else
	  // {
	      var sqlContrano="select contrano from lhgroupcont where contrano='"+fm.all('ContraNo').value+"'";
        //alert(sqlContrano);
        // alert(easyExecSql(sqlContrano));
        contrano=easyExecSql(sqlContrano);
        if(fm.all('ContraNo').value==contrano)
        {
    	    alert("此合同编号的信息已经存在,不允许重复保存!");
        }
        if(contrano==null||contrano==""||contrano=="null")
        {
        	  if(checkDate() == false) return false;
           var i = 0;
           if( verifyInput2() == false ) return false;
           fm.fmtransact.value="INSERT||MAIN";
           var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
           var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
           showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
           fm.submit(); //提交
        }
    // }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDSocialInsOrgan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	  
	       if(checkDate() == false) return false;
         //下面增加相应的代码
         if (confirm("您确实想修改该记录吗?"))
         {
         var i = 0;
         if( verifyInput2() == false ) return false;
         var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
         var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
         showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
         
         //showSubmitFrame(mDebug);
         fm.fmtransact.value = "UPDATE||MAIN";
         fm.submit(); //提交
         }
         else
         {
           //mOperate="";
           alert("您取消了修改操作！");
         }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
 // showInfo=window.open("./LHGroupContQuery.html");
  showInfo=window.open("./LHGroupContQuery.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}  
function getAllService()
{   //alert(fm.all('txtFlag').value);
	  if(fm.all('txtFlag').value!=""&&fm.all('txtFlag').value!="null"&&fm.all('txtFlag').value!=null)//从医疗机构检索进入此页面txtFlag'为1,传入标记为6
    {
       			if(fm.all('ContraNo').value=="")
			{
				alert("你选择的编号为空，请重新选择!");
			}
			else
			{
          	  if (GroupContGrid.getSelNo() >= 1)
							{     // alert(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1));
									  if(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1)=="")
									  {
											  alert("你选择了空的列，请重新选择!");
						  					return false;
									  }
										else
										{
											   var ContraNo=fm.ContraNo.value;
											   var temp_no = GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,10);
										     window.open("./LHServerPriceInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=6","服务价格表设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
						        }
						  } 
						  else
						  {
						  		alert("请选择一条要传输的记录！");    
						  }
			 }
		}
		if(fm.all('txtFlag').value==""||fm.all('txtFlag').value=="null"||fm.all('txtFlag').value==null)//从医疗机构检索进入此页面为1
    {
    	getService();
    }
}
function getService()//
{
	//fm.all('ContraNo').value==""&&&&GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,7)=""&& GroupContGrid.getSelNo()=""
			if(fm.all('ContraNo').value=="")
			{
				alert("你选择的编号为空，请重新选择!");
			}
			else
			{
          	  if (GroupContGrid.getSelNo() >= 1)
							{     // alert(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1));
									  if(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1)=="")
									  {
											  alert("你选择了空的列，请重新选择!");
						  					return false;
									  }
										else
										{
											   var ContraNo=fm.ContraNo.value;
											   var temp_no = GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,10);
										     window.open("./LHServerPriceInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=1","服务价格表设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
						        }
						  } 
						  else
						  {
						  		alert("请选择一条要传输的记录！");    
						  }
			 }
} 
function getAllCharge()
{
	  if(fm.all('txtFlag').value!=""&&fm.all('txtFlag').value!="null"&&fm.all('txtFlag').value!=null)//从医疗机构检索进入此页面txtFlag'为1,传入标记为6
    {
    		  if(fm.all('ContraNo').value=="")
					{
						alert("你选择的编号为空，请重新选择!");
					}
					else
					{
								if (GroupContGrid.getSelNo() >= 1)               
								{  
									   if(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1)=="")
										 {
														alert("你选择了空的列，请重新选择!");
										  			return false;
											}
											else
											{
												 var ContraNo=fm.ContraNo.value;
											   var temp_no = GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,10);   
											  // alert(temp_no);                                        
											   window.open("./LHChargeBalanceInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=6","费用结算方式设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
								      }
								}
								else                             
							  {                                
									alert("请选择一条传输的记录！");   
							  }
					} 
		 }
		if(fm.all('txtFlag').value==""||fm.all('txtFlag').value=="null"||fm.all('txtFlag').value==null)
    {
    	getCharge();
    }
   
}
function getCharge()//
{
	if(fm.all('ContraNo').value=="")
	{
		alert("你选择的编号为空，请重新选择!");
	}
	else
	{
				if (GroupContGrid.getSelNo() >= 1)               
				{  
					   if(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1)=="")
						 {
										alert("你选择了空的列，请重新选择!");
						  			return false;
							}
							else
							{
								 var ContraNo=fm.ContraNo.value;
							   var temp_no = GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,10);   
							  // alert(temp_no);                                        
							   window.open("./LHChargeBalanceInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=1","费用结算方式设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
				      }
				}
				else                             
			  {                                
					alert("请选择一条传输的记录！");   
			  }
	}                               

}  
function getAllAssociate()
{
		if(fm.all('txtFlag').value!=""&&fm.all('txtFlag').value!="null"&&fm.all('txtFlag').value!=null)//从医疗机构检索进入此页面txtFlag'为1,传入标记为8
    {
       if(fm.all('ContraNo').value=="")
				{
					alert("你选择的编号为空，请重新选择!");
				}
				else
				{
						if (GroupContGrid.getSelNo() >= 1)               
						{     
							     if(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1)=="")
									 {
													alert("你选择了空的列，请重新选择!");
									  			return false;
										}
										else
										{
												 var ContraNo=fm.ContraNo.value;
											   var temp_no = GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,10);
											   window.open("./LHAssociateSettingInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=8","关联设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
						        }
						}
						else                             
					  {                                
							alert("请选择一条传输的记录！");   
					  }  
				}  
		}
		if(fm.all('txtFlag').value==""||fm.all('txtFlag').value=="null"||fm.all('txtFlag').value==null)//从医疗机构检索进入此页面为1
    {
      getAssociate();
    }
}
function getAssociate()//
{
	if(fm.all('ContraNo').value=="")
	{
		alert("你选择的编号为空，请重新选择!");
	}
	else
	{
			if (GroupContGrid.getSelNo() >= 1)               
			{     
				     if(GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,1)=="")
						 {
										alert("你选择了空的列，请重新选择!");
						  			return false;
							}
							else
							{
									 var ContraNo=fm.ContraNo.value;
								   var temp_no = GroupContGrid.getRowColData(GroupContGrid.getSelNo()-1,10);
								   window.open("./LHAssociateSettingInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=4","关联设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			        }
			}
			else                             
		  {                                
				alert("请选择一条传输的记录！");   
		  }  
	}  
	
}         
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{


    		arrResult = arrQueryResult;
    		//alert(arrQueryResult[0][0]);

				var mulSql = "select a.contrano,a.ContraName,a.HospitCode,"
				            +"(select  HospitName from LDHospital where LDHospital.HospitCode=a.HospitCode),"
				            +"a.IdiogrDate,a.ContraBeginDate,a.ContraEndDate,a.ContraState ,"
				            +"(select case when a.ContraState = '1' then '有效' else '无效' end from dual) "
										+" from lhgroupcont a where a.contrano ='"+arrResult[0][0]+"' ";
        //alert(mulSql);
        arrResult=easyExecSql(mulSql);
       // alert(arrResult);
		   // alert(arrResult[0][1]);
        fm.all('ContraNo').value				= arrResult[0][0];
        fm.all('HospitName').value			= arrResult[0][3];
        fm.all('HospitCode').value      = arrResult[0][2];
        fm.all('ContraName').value			= arrResult[0][1];
        fm.all('IdiogrDate').value			= arrResult[0][4];
        fm.all('ContraBeginDate').value	= arrResult[0][5];
        fm.all('ContraEndDate').value		= arrResult[0][6];
        fm.all('ContraState').value 		= arrResult[0][7];
        fm.all('ContraState_ch').value 		= arrResult[0][8];

   
   
     var mulSql2 = "select (select Codename from ldcode where b.ContraType=ldcode.code  and ldcode.Codetype='hmdeftype'),"
		            +"(select c.DutyItemName from LDContraItemDuty c where c.DutyItemCode = b.dutyItemCode),"                                  
								+"(select case when b.dutystate = '1' then '有效' else '无效' end from dual),"
								+" b.dutylinkman, "
								+"b.DutyContact,b.Contratype,b.DutyItemCode,b.dutystate,b.dutylinkman,b.ContraItemNo, "
								+" case when ( select distinct ContraItemNo from LHContraAssoSetting where ContraItemNo=b.ContraItemNo) is null then '无关联' else '有关联' end "
								+" from lhgroupcont a,lhcontitem b where a.contrano ='"+arrResult[0][0]+"' and b.contrano='"+arrResult[0][0]+"'";
				//alert(mulSql2);
				turnPage.pageLineNum = 200;  
				turnPage.queryModal(mulSql2, GroupContGrid);



	}     
}         
        
        
function inputDutyExolai(a)
{ 
	
	divDutyExolai.style.display='';
	var tempDutyExolai = fm.all(a).all('GroupContGrid3').value;
	selectedDutyExolaiRow = fm.all(a).all('GroupContGridNo').value-1;
	fm.all('textDutyExolai').value=tempDutyExolai;
	
}

function backDutyExolai()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		GroupContGrid.setRowColData(selectedDutyExolaiRow,3,fm.textDutyExolai.value);
		fm.textDutyExolai.value="";
		divDutyExolai.style.display='none';		
	}
}

function backDutyExolaiButton()
{
		GroupContGrid.setRowColData(selectedDutyExolaiRow,3,fm.textDutyExolai.value);
		fm.textDutyExolai.value="";
		divDutyExolai.style.display='none';		
}

function inputFeeExplai(a)
{ 	
	divFeeExplai.style.display='';
	var tempFeeExplai = fm.all(a).all('GroupContGrid7').value;
	selectedFeeExplaiRow = fm.all(a).all('GroupContGridNo').value-1;
	fm.all('textFeeExplai').value=tempFeeExplai;
	
}

function backFeeExplai()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		GroupContGrid.setRowColData(selectedFeeExplaiRow,7,fm.textFeeExplai.value);
		fm.textFeeExplai.value="";
		divFeeExplai.style.display='none';		
	}
}        
        
function backFeeExplaiButton()
{ 
		GroupContGrid.setRowColData(selectedFeeExplaiRow,7,fm.textFeeExplai.value);
		fm.textFeeExplai.value="";
		divFeeExplai.style.display='none';	 
}   

        
function getCopy()
{
	showInfo=window.open("./GrpContMain.jsp?ContraNo="+fm.all('ContraNo').value);
}
        
function compareDate(DateOne,DateTwo)
{
	var OneMonth = DateOne.substring(5,DateOne.lastIndexOf ("-"));
	var OneDay = DateOne.substring(DateOne.length,DateOne.lastIndexOf ("-")+1);
	var OneYear = DateOne.substring(0,DateOne.indexOf ("-"));
	
	var TwoMonth = DateTwo.substring(5,DateTwo.lastIndexOf ("-"));
	var TwoDay = DateTwo.substring(DateTwo.length,DateTwo.lastIndexOf ("-")+1);
	var TwoYear = DateTwo.substring(0,DateTwo.indexOf ("-"));
	
	if (Date.parse(OneMonth+"/"+OneDay+"/"+OneYear) >
	Date.parse(TwoMonth+"/"+TwoDay+"/"+TwoYear))
	{ 
		return true;
	}
	else
	{
		return false;
	}
}

function checkDate()
{
	if(fm.all('IdiogrDate').value != "" && fm.all('ContraBeginDate').value != "")
	{//alert("1");
			if(compareDate(fm.all('IdiogrDate').value,fm.all('ContraBeginDate').value) == true)
			{
				alert("合同签订时间应早于合同起始时间");
				return false;
			}
	}
	
	if(fm.all('ContraBeginDate').value != "" && fm.all('ContraEndDate').value != "")
	{//alert("2");
			if(compareDate(fm.all('ContraBeginDate').value,fm.all('ContraEndDate').value) == true)
			{
				alert("合同起始时间应早于合同终止时间");
				return false;
			}
	}
		if(fm.all('IdiogrDate').value != "" && fm.all('ContraEndDate').value != "")
	{//alert("2");
			if(compareDate(fm.all('IdiogrDate').value,fm.all('ContraEndDate').value) == true)
			{
				alert("合同签订时间应早于合同终止时间");
				return false;
			}
	}
}

function getNo()
{
	  if(fm.all('HospitCode').value==""||fm.all('HospitCode').value==null|fm.all('HospitCode').value=="null")
    {
      alert("请您先选择合作机构名称！");
      return false;
    }
    var sql="select max(ContraNo) from lhgroupcont where HospitCode='"+fm.all('HospitCode').value+"'";
    var result = easyExecSql(sql);
    var result2=parseInt(result)+1;
    var result3=result2.toString().substring(8);
    if(result3.length==4)
    {
      result4="1"+result3.toString();
    }
    else if(result3.length==3)
    {
    	result4="1"+"0"+result3.toString();
    }
    else if(result3.length==2)
    {
    	result4="1"+"00"+result3.toString();
    }
    else if(result3.length==1)
    {
    	result4="1"+"000"+result3.toString();
    }
    else 
    {
    	result4="11"+result3.toString();
    }
    if(result==""||result==null|result=="null")
    {
      fm.ContraNo.value=fm.all('HospitCode').value+"1"+"000"+"1";
      return false;
    }
    else
    {
	   	fm.ContraNo.value=fm.all('HospitCode').value+result4;
	  }
}



function testDutyItemType(hmconttype,iArray6) //hmconttypeName---->iArray1;hmconttypeCode---->iArray6
{
	   alert(fm.all( hmconttype).all('GroupContGrid').value );
	   if(hmconttype!=null&& hmconttype!="") //如果第2个参数parm2有效，parm2可以为空
     {
        var arr= hmconttype ;  // parm2 是一个数组，可以存放多个要传入的值
        var arrlength= arr.length; //得到数组长度
     }

	   alert(hmconttype);
	   var sql="select  a.DutyItemName,a.DutyItemCode from LDContraItemDuty a ,ldcode b"
            +"where microsecond(current timestamp)= microsecond(current timestamp) and "
            +" a.DutyItemName like '%%' and a.Dutyitemtye=b.code and  b.codetype = 'hmdeftype'"
            +" and a.Dutyitemtye='01';"
     var result=easyExecSql(sql);
}