<%
//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput m_gInput=(GlobalInput)session.getValue("GI");
	String strManageCom="";
	String strCurrentDate="";
	if(m_gInput==null)
	{
		strManageCom="Unknown";
		strCurrentDate="";
	}
	else
	{
		strManageCom=m_gInput.ManageCom;
		strCurrentDate=PubFun.getCurrentDate();
	}
	
%>
<script language="javascript" type="text/javascript">
	function initForm()
	{
		try
		{
		   initTextbox();
		}
		catch(ex)
		{
			document.write("文本框初始化失败!");
		}
	}
	
	//初始化文本框
	function initTextbox()
	{
	    var sql="select HospitCode,HospitName,ManageCom,Riskcode,Validate,CInvalidate from LDDingHospital "
		       +" where 1=1 "
		       +" and HospitCode='<%=request.getParameter("HospitCode")%>' "
		       +" and ManageCom='<%=request.getParameter("ManageCom")%>' "
		       +" and Riskcode='<%=request.getParameter("Riskcode")%>' "
		       +" with ur "; 
		var arrResult = easyExecSql(sql); 
		if(arrResult==null)
         {
           alert("该医院不存在，请重新选择！");
           return false;
         }      
		fm.all('HospitCode').value = arrResult[0][0];
		fm.all('HospitalName').value = arrResult[0][1];
		fm.all('ManageCom').value = arrResult[0][2];
		fm.all('Riskcode').value =arrResult[0][3];
		fm.all('Validate').value = arrResult[0][4];
		fm.all('CInvalidate').value = arrResult[0][5];	
		
	   var 	Riskname="select riskname from lmriskapp where riskcode='"+arrResult[0][3]+"' fetch first 1 rows only ";
	   var 	ManageCom="select name from ldcom where comCode='"+arrResult[0][2]+"' fetch first 1 rows only ";
	   var arrResultRiskname = easyExecSql(Riskname); 
	   if(arrResultRiskname==null)
	   {
	        alert("该险种名称不存在！");
	        return false;
	   }  
	   fm.all('RiskCodeName').value = arrResultRiskname[0][0];
	   
	   
	   var arrResultManageCom = easyExecSql(ManageCom); 
	   if(arrResultManageCom==null)
	   {
	        alert("该管理机构不存在！");
	        return false;
	   } 
	   fm.all('ComName').value = arrResultManageCom[0][0]; 
	   showAllCodeName();

	}

</script>