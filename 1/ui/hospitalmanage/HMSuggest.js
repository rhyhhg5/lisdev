//该文件中包含客户端需要处理的函数和事件

//程序名称：HMSuggest.js
//程序功能：专项健康建立录入
//创建日期：2010-3-17
//创建人  ：chenxw
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var DisSugGridTurnPage = new turnPageClass(); 
var NotSendCustGridTurnPage = new turnPageClass(); 
var AlSendGridTurnPage = new turnPageClass(); 
var CustNotSugGridTurnPage = new turnPageClass(); 
var CustAlSugGridTurnPage = new turnPageClass(); 
var CustAlSendGridTurnPage = new turnPageClass(); 

var mResult = "";
//返回结果用
function setResult(tResult) {
	mResult = tResult;
}


//按疾病按钮
function showDisSuggest()
{
	showDiv(ByDisDiv,true);
	showDiv(ByCustDiv,false);
}

//按客户按钮
function showCustSuggest()
{
	showDiv(ByDisDiv,false);
	showDiv(ByCustDiv,true);
}

//手工校验必须录入
function checkMastInput(object,errorInfo) {
	if(object.value == null || object.value == "") {
		alert(errorInfo);
		 object.value = "";
    	 object.className="warn";
    	 object.focus();
		return false;
	} else {
		return true;
	}
}


//***************
//^^^^^^^^^^^^^^^^^^^^疾病健康建议^^^^^^^^^^^^^^^^^^

//疾病健康建议保存按钮
function saveofDis()
{
	//校验是否录入了编码
	if(!checkMastInput(fm.DiseaseCode,"请您必须录入疾病编码！")) {
		return false;
	}
	//校验健康建议是否录入
	if(!checkMastInput(fm.DisSuggest,"请您必须录入健康建议！")) {
		return false;
	}
	
	//校验该建议是否已经录入
	var sql = " select 1 from HMSuggestInfo where BusinessType='1' and BusinessCode='" 
										+ fm.DiseaseCode.value + "' with ur ";
	var arr = easyExecSql(sql);
	if(arr) {
		alert("该疾病已经录入了健康建议,您只能在【健康建议查询及修改】处进行修改！");
		return false;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.fmtransact.value = "DISSUGGEST||MAIN";
	fm.DisDetialOperate.value = "INSERT||DIS";
	fm.action = "./HMSuggestSave.jsp"
	fm.submit(); //提交
}

//查询已经录入的疾病健康建议
function alDisSuggestQuery()
{
	//校验是否录入了时间起期
	if(!checkMastInput(fm.DisSuggInStartDate,"录入时间起期不能为空！")) {
		return false;
	}
	//校验健康建议是否录入了时间止期
	if(!checkMastInput(fm.DisSuggInEndDate,"录入时间止期不能为空！")) {
		return false;
	}
	
	var sql = " select BusinessCode,BusinessName,ModifyDate "
			+ " from HMSuggestInfo where BusinessType='1' "
			+ getWherePart("BusinessName","DiseaseName1","like")
			+ getWherePart("BusinessCode","DiseaseCode1")
	 		+ getWherePart("ModifyDate","DisSuggInStartDate",">=")
	 		+ getWherePart("ModifyDate","DisSuggInEndDate","<=");
	 DisSugGridTurnPage.queryModal(sql,DisSugGrid);  	
}

//显示疾病健康建议
function selectInfoGrid() {
	var selNoLine=DisSugGrid.getSelNo ();//得到被选中的行号
	if (selNoLine>0){
		var tDisCode  = DisSugGrid.getRowColData(selNoLine-1,1);
		if(tDisCode != null && tDisCode != "") {
			var sql = " select HealthSuggest "
							+" from HMSuggestInfo where BusinessCode='" + tDisCode 
							+ "' and BusinessType='1' with ur";
			var arr = easyExecSql(sql);
			if(arr) {
				fm.DisSuggest2.value = arr[0][0];
			} else {
				fm.DisSuggest2.value = "";
			}
		} else {
			fm.DisSuggest2.value = "";
		}
	}
}

//疾病健康建议修改
function alDisSuggestModify()
{
	var tMulCount = DisSugGrid.mulLineCount;
	if(tMulCount<=0) {
		alert("没有任何疾病健康建议信息！");
		return false;
	}
	var selNo = DisSugGrid.getSelNo();
	if(selNo <= 0) {
		alert("请选择一条记录后再点击修改！");
		return false;
	}
	var tDisCode = DisSugGrid.getRowColData(selNo-1,1);
	if(tDisCode == null || tDisCode == "") {
		alert("请选择一条有效的疾病信息！");
		return false;
	}
	//校验健康建议是否录入
	if(!checkMastInput(fm.DisSuggest2,"请您必须录入健康建议！")) {
		return false;
	}
	
	var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.fmtransact.value = "DISSUGGEST||MAIN";
	fm.DisDetialOperate.value = "UPDATE||DIS";
	fm.action = "./HMSuggestSave.jsp?DiseaseCodeForUp='" + tDisCode + "'";
	fm.submit(); //提交
	
}

//删除按钮
function alDisSuggestDelete()
{
	var tMulCount = DisSugGrid.mulLineCount;
	if(tMulCount<=0) {
		alert("没有任何疾病健康建议信息！");
		return false;
	}
	var selNo = DisSugGrid.getSelNo();
	if(selNo <= 0) {
		alert("请选择一条记录后再点击删除！");
		return false;
	}
	var tDisCode = DisSugGrid.getRowColData(selNo-1,1);
	if(tDisCode == null || tDisCode == "") {
		alert("请选择一条有效的疾病信息！");
		return false;
	}
	if(window.confirm("您确定要删除？")) {
		var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.fmtransact.value = "DISSUGGEST||MAIN";
		fm.DisDetialOperate.value = "DELETE||DIS";
		fm.action = "./HMSuggestSave.jsp?DiseaseCodeForDel='" + tDisCode + "'";
		fm.submit(); //提交
	}
}

//客户提取按钮
function custQuery()
{
	//判断疾病编码是否为空
	if(!checkMastInput(fm.DiseaseCode2,"疾病代码不能为空！")) {
		return false;
	}
	
	 var tSQL = ""
	//如果发送方式为空，那么从相关表中查询，除去已发送的客户，否则从已发送的客户中查询
	if(fm.SendType2.value == null || fm.SendType2.value == "") {
	   
  		//最近一年就诊次数
		var LastVisCountCondition = "";
		if(fm.LastVisCount2.value != null && fm.LastVisCount2.value != "") {
			LastVisCountCondition = " and coalesce(xa.Illcount,0)>=" + fm.LastVisCount2.value*1;
		}
		//最近一年医疗费用理赔额度
		var LastYearMedRealPayCondition = "";
		if(fm.HCostClaimFee2.value != null && fm.HCostClaimFee2.value != "") {
			LastYearMedRealPayCondition = " and coalesce(xa.Realpay,0)>=" + fm.HCostClaimFee2.value*1;
		}
		
		//就诊方式
		var visttypecondition ="";
	    if(fm.VistType2.value != null && fm.VistType2.value != "") {
	   		//门诊
	   		if(fm.VistType2.value == '2') {
	   			visttypecondition = 
	   				" and b.FeeType='1'";
	   		}
	   		//住院
	   		if(fm.VistType2.value == '3') {
	   			visttypecondition = 
	   				" and b.FeeType='2' ";
	   		}
	    }
		
  		tSQL = 
		 " select distinct a.insuredno, "
		       +" a.name, "
		       +" a.mobile, "
		       +" a.email, "
		       +" a.homeaddress, "
		       +" b.DiseaseCode, "
		       +" b.DiseaseName, "
		       +" xa.lastFeeDate, "
		       +" coalesce(xa.Illcount, 0), "
		       +" coalesce(xa.Realpay, 0), "
		       //+" b.FeeType "
			   +"'' "
		  +" from HMSENDCUSTDETAIL a, "
		  //+" left outer join LLCUSTOMERCLAIM xa on xa.CustomerNo = a.InsuredNo, "
		 +" LLCUSTOMERCLAIM xa, "
		 +" HMCLAIMDETAIL b "
		 +" where a.insuredno = b.customerno "
		 +" and xa.CustomerNo = a.InsuredNo "
		 +" and not exists(select 1 from HMSuggestSendTrack xe "
  			   		+ " where xe.CustomerNo=a.InsuredNo and BusinessType = '1' "
  			   				+" and xe.BusinessCode = b.DiseaseCode and xe.SendStatus='1') "
  		 
  		 + getWherePart("xa.lastFeeDate","LastVisStartDate2",">=")
	  	 + getWherePart("xa.lastFeeDate","LastVisEndDate2","<=")
	  	 + getWherePart("b.DiseaseCode","DiseaseCode2")
	  	 + LastVisCountCondition
	  	 + LastYearMedRealPayCondition
	  	 + visttypecondition
  		 + " with ur ";
	  		
	 } else {
	 		//最近一年就诊次数
			var LastVisCountCondition = "";
			if(fm.LastVisCount2.value != null && fm.LastVisCount2.value != "") {
				LastVisCountCondition = " and a.LastYearVisCount>=" + fm.LastVisCount2.value*1;
			}
			//最近一年医疗费用理赔额度
			var LastYearMedRealPayCondition = "";
			if(fm.HCostClaimFee2.value != null && fm.HCostClaimFee2.value != "") {
				LastYearMedRealPayCondition = " and a.LastYearMedRealPay>=" + fm.HCostClaimFee2.value*1;
			}
	  	tSQL = 
	  			" select distinct CustomerNo,Name,Mobile,EMail,HomeAddress,BusinessCode, "
	  			+"(select ICDName from LDDisease where ICDCode=BusinessCode ), "
	  			+" LastVisEndDate,LastYearVisCount,LastYearMedRealPay,VisType "
	  			+" from HMSuggestSendTrack a where BusinessType='1' and SendStatus='1' "
	  			+ getWherePart("a.LastVisEndDate","LastVisStartDate2",">=")
	  			+ getWherePart("a.LastVisEndDate","LastVisEndDate2","<=")
	  			+ getWherePart("a.BusinessCode","DiseaseCode2")
	  			+ getWherePart("a.SendType","SendType2")
	  			+ getWherePart("a.VisType","VistType2")
	  			+ LastVisCountCondition
	  			+ LastYearMedRealPayCondition
	  			+ " with ur";
	 }
	 NotSendCustGridTurnPage.queryModal(tSQL,NotSendCustGrid);  	
}


//校验发送方式：如果选中，那么发送方式必须录入
function checkSendType() {
	if(fm.isALSend.checked == true) {
		if(fm.SendType2.value == null || fm.SendType2.value == "") {
			alert("您选择的状态为：已发送，那么您必须选择一种发送方式！");
			fm.isALSend.checked = false;
			return false;
		}
	}
	return true;
}

//发送按钮
function disSuggestSend()
{
	//发送方式必须选择
	if(!checkMastInput(fm.SendType3,"您必须选择一种发送方式！")) {
		return false;
	}
	//校验选择疾病是否已经录入健康建议
	var tDisCode = fm.DiseaseCode2.value;
	var sql = " select HealthSuggest from HMSuggestInfo where BusinessType='1' and BusinessCode='" + tDisCode + "'";
	var arr = easyExecSql(sql);
	if(!arr) {
		alert("您选择的疾病，没有录入相应的健康建议，请先在健康建议录入处进行录入！");
		return false;
	} 
	
	//如果不是选择全部发送，那么校验是否至少选择一条记录，并且选择的信息是有效的。
	if(!fm.isAllSend.checked) {
		var gridRowNum = NotSendCustGrid.mulLineCount;
		if(gridRowNum <= 0) {
			alert("没有有效的客户信息，请改变查询条件，再次查询发送");
			return false;
		}
		var isChoose = false;
		for(i=1;i<=gridRowNum;i++) {
			if(NotSendCustGrid.getChkNo(i-1)==true) {
				var tCustomerNo = NotSendCustGrid.getRowColData(i-1,1);
				if(tCustomerNo == null || tCustomerNo == "") {
					alert("第" + i + "行的客户号不能为空！");
					return false;
				} else {
					isChoose = true;
					break
				}
			}
		}
		if(!isChoose) {
			alert("请您至少选择一条客户信息！");
			return false;
		}
		
		var showStr="正在发送，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.fmtransact.value = "DISSUGGEST||MAIN";
		fm.DisDetialOperate.value = "SUGGESTSEND||DIS";
		fm.action = "./HMSuggestSave.jsp";
		fm.submit(); //提交
		
		return true;
	
	//选择了全部发送
	} else {
		if(window.confirm("您选择了全部发送，这样数据量可能会非常庞大，是否继续？")) {
			var showStr="正在发送，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.fmtransact.value = "DISSUGGEST||MAIN";
			fm.DisDetialOperate.value = "SUGGESTSENDALL||DIS";
			fm.action = "./HMSuggestSave.jsp";
			fm.submit(); //提交
			
		} else {
			alert("您取消了全部发送");
		}
		return true;		
	}
}

//发送结果查询按钮
function disSendResultQuery()
{
	if(mResult == null || mResult == "" || mResult == 'null') {
		alert("没有查询到发送记录！请尝试使用条件进行查询。");
		return false;
	}
	var sql =
		" select distinct CustomerNo,Name,Mobile,EMail,HomeAddress,BusinessCode, "
			+"(select ICDName from LDDisease where ICDCode=BusinessCode ), "
			+" LastVisEndDate,LastYearVisCount,LastYearMedRealPay,makedate,"
			+" (case SendType when '1' then '短信发送' when '2' then '邮件发送' when '3' then '打印' end),"
			+" (case SendStatus when '1' then '发送成功' else '发送失败' end ),"
			+" Remark,SerialNo "
			+" from HMSuggestSendTrack a where BusinessType='1' "
			+" and SerialNo in(" + mResult + ") "
			+" order by a.makedate,CustomerNo ";
	 var arr = easyExecSql(sql);
	 if(!arr) {
	 	alert("没有查询到发送记录！请尝试使用条件进行查询。");
	 	return false;
	 }
	 AlSendGridTurnPage.queryModal(sql,AlSendGrid);  
}

//查询按钮
function disAlQuery()
{
	//校验是否录入了疾病
	if(!checkMastInput(fm.DiseaseCode3,"疾病编码不能为空！")) {
		return false;
	}
	
	//最近一年就诊次数
	var LastVisCountCondition = "";
	if(fm.LastVisCount3.value != null && fm.LastVisCount3.value != "") {
		LastVisCountCondition = " and a.LastYearVisCount>=" + fm.LastVisCount3.value*1;
	}
	//最近一年医疗费用理赔额度
	var LastYearMedRealPayCondition = "";
	if(fm.HCostClaimFee3.value != null && fm.HCostClaimFee3.value != "") {
		LastYearMedRealPayCondition = " and a.LastYearMedRealPay>=" + fm.HCostClaimFee3.value*1;
	}
	
	var sql =
		" select distinct CustomerNo,Name,Mobile,EMail,HomeAddress,BusinessCode, "
			+"(select ICDName from LDDisease where ICDCode=BusinessCode ), "
			+" LastVisEndDate,LastYearVisCount,LastYearMedRealPay,makedate,"
			+" (case SendType when '1' then '短信发送' when '2' then '邮件发送' when '3' then '打印' end),"
			+" (case SendStatus when '1' then '发送成功' else '发送失败' end ),"
			+" Remark,SerialNo "
			+" from HMSuggestSendTrack a where BusinessType='1' "
			+ getWherePart("a.LastVisEndDate","LastVisStartDate3",">=")
			+ getWherePart("a.LastVisEndDate","LastVisEndDate3","<=")
			+ getWherePart("a.MakeDate","sendStartDate3",">=")
			+ getWherePart("a.MakeDate","sendEndDate3","<=")
			+ getWherePart("a.SendType","SendType4")
			+ getWherePart("a.BusinessCode","DiseaseCode3")
			+ getWherePart("a.Name","CustomerName3")
			+ getWherePart("a.CustomerNo","CustomerNo3")
			+ LastVisCountCondition
			+ LastYearMedRealPayCondition
			+" order by a.makedate,CustomerNo ";
	 
	 AlSendGridTurnPage.queryModal(sql,AlSendGrid);  
}

//显示查询结果的疾病健康建议
function selectAlSendGrid() {
	var selNoLine=AlSendGrid.getSelNo ();//得到被选中的行号
	if (selNoLine>0){
		var tSerialNo  = AlSendGrid.getRowColData(selNoLine-1,15);
		if(tSerialNo != null && tSerialNo != "") {
			var sql = " select Suggest "
							+" from HMSuggestSendTrack where BusinessType='1' and SerialNo='"
							+ tSerialNo +"' with ur";
			var arr = easyExecSql(sql);
			if(arr) {
				fm.queryAfterSuggest.value = arr[0][0];
			} else {
				fm.queryAfterSuggest.value = "";
			}
		} else {
			fm.queryAfterSuggest.value = "";
		}
	}
}

//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

//客户提取按钮
function byCustQuery()
{
	 var tSQL = ""
	   
  		//最近一年就诊次数
		var LastVisCountCondition = "";
		if(fmsave.CLastVisCount.value != null && fmsave.CLastVisCount.value != "") {
			LastVisCountCondition = " and coalesce(xa.Illcount,0)>=" + fmsave.CLastVisCount.value*1;
		}
		//最近一年医疗费用理赔额度
		var LastYearMedRealPayCondition = "";
		if(fmsave.CHCostClaimFee.value != null && fmsave.CHCostClaimFee.value != "") {
			LastYearMedRealPayCondition = " and coalesce(xa.Realpay,0)>=" + fmsave.CHCostClaimFee.value*1;
		}
		
		//就诊方式
		var visttypecondition ="";
	    if(fmsave.CVistType.value != null && fmsave.CVistType.value != "") {
	   		//门诊
	   		if(fmsave.CVistType.value == '2') {
	   			visttypecondition = 
	   				" and b.FeeType='1'";
	   		}
	   		//住院
	   		if(fmsave.CVistType.value == '3') {
	   			visttypecondition = 
	   				" and b.FeeType='2' ";
	   		}
	    }
		
		//姓名
		var nameCondition = "";
		if(fmsave.CCustomerName.value != null && fmsave.CCustomerName.value != "") {
			nameCondition = " and a.name ='" + fmsave.CCustomerName.value + "'";
		}
		
		//客户号
		var customerNoCondition = "";
		if(fmsave.CCustomerNo.value != null && fmsave.CCustomerNo.value != "") {
			customerNoCondition = " and a.insuredno ='" + fmsave.CCustomerNo.value + "'";
		}
		
		//证件类型
		var iDTypeCondition = "";
		if(fmsave.CIDType.value != null && fmsave.CIDType.value != "") {
			iDTypeCondition = " and a.idtype ='" + fmsave.CIDType.value + "'";
		}
		
		//证件号码
		var idNoCondition = "";
		if(fmsave.CIDNo.value != null && fmsave.CIDNo.value != "") {
			idNoCondition = " and a.idno ='" + fmsave.CIDNo.value + "'";
		}
		
		//最近一次就诊时间结束起期
		var lastVisStartDateCondition = "";
		if(fmsave.CLastVisStartDate.value != null && fmsave.CLastVisStartDate.value != "") {
			lastVisStartDateCondition = " and xa.lastFeeDate >='" + fmsave.CLastVisStartDate.value + "'";
		}
		//最近一次就诊时间结束止期
		var lastVisEndDateCondition = "";
		if(fmsave.CLastVisEndDate.value != null && fmsave.CLastVisEndDate.value != "") {
			lastVisEndDateCondition = " and xa.lastFeeDate <='" + fmsave.CLastVisEndDate.value + "'";
		}
		
		//疾病代码
		var diseaseCodeCondition = "";
		if(fmsave.CDiseaseCode.value != null && fmsave.CDiseaseCode.value != "") {
			diseaseCodeCondition = " and b.diseasecode ='" + fmsave.CDiseaseCode.value + "'";
		}
		
  		tSQL = 
		 " select distinct a.insuredno, "
		       +" a.name, "
		       +" a.mobile, "
		       +" a.email, "
		       +" a.homeaddress, "
		       +" xa.lastFeeDate, "
		       +" coalesce(xa.Illcount, 0), "
		       +" '','', "
		       //+" (case b.FeeType when '1' then '门诊' when '2' then '住院' end ), "
		       +" coalesce(xa.Realpay, 0) "
		
		  +" from HMSENDCUSTDETAIL a, "
		  //+" left outer join LLCUSTOMERCLAIM xa on xa.CustomerNo = a.InsuredNo, "
		 +" HMCLAIMDETAIL b, "
		 +" LLCUSTOMERCLAIM xa "
		 +" where a.insuredno = b.customerno "
		 +" and xa.CustomerNo = a.InsuredNo "
		 +" and not exists(select 1 from HMSuggestInfo xe "
  			   		+ " where xe.BusinessCode=a.InsuredNo and BusinessType = '2' ) "
  		 
  		 + nameCondition
	  	 + customerNoCondition
	  	 + iDTypeCondition
	  	 + idNoCondition
  		 + lastVisStartDateCondition
	  	 + lastVisEndDateCondition
	  	 + diseaseCodeCondition
	  	 + LastVisCountCondition
	  	 + LastYearMedRealPayCondition
	  	 + visttypecondition
  		 + " with ur ";
	  		
	 CustNotSugGridTurnPage.queryModal(tSQL,CustNotSugGrid);  	
}

//保存健康建议
function cSaveSuggest()
{
   var tMulCount = CustNotSugGrid.mulLineCount;
	if(tMulCount<=0) {
		alert("没有任何客户信息！");
		return false;
	}
	var selNo = CustNotSugGrid.getSelNo();
	if(selNo <= 0) {
		alert("请选择一个客户后，再点击【保存】！");
		return false;
	}
	var tCustomerNo = CustNotSugGrid.getRowColData(selNo-1,1);
	if(tCustomerNo == null || tCustomerNo == "") {
		alert("没有有效的客户信息！");
		return false;
	}
	if(fmsave.CSuggest.value == null || fmsave.CSuggest.value == "") {
		alert("请录入健康建议后再点击【保存】");
		return false;
	}
	
	//校验该建议是否已经录入
	var sql = " select 1 from HMSuggestInfo where BusinessType='2' and BusinessCode='" 
										+ tCustomerNo + "' with ur ";
	var arr = easyExecSql(sql);
	if(arr) {
		alert("该客户已经录入了健康建议,您只能在【已建立健康建议客户提取及健康建议发送】处进行修改！");
		return false;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fmsave.custfmTransact.value = "CUSTSUGGEST||MAIN";
	fmsave.custfmDetialOperate.value = "INSERT||CUST";
	fmsave.action = "./HMSuggestSave.jsp?CCustomerNo='" + tCustomerNo + "'";
	fmsave.submit(); //提交
	
}

//单个人信息发送
function cSignleSend()
{
	var tMulCount = CustNotSugGrid.mulLineCount;
	if(tMulCount<=0) {
		alert("没有任何客户信息！");
		return false;
	}
	var selNo = CustNotSugGrid.getSelNo();
	if(selNo <= 0) {
		alert("请选择一个客户后，再点击【发送】！");
		return false;
	}
	var tCustomerNo = CustNotSugGrid.getRowColData(selNo-1,1);
	var tMobile = CustNotSugGrid.getRowColData(selNo-1,3);
	var tEmail = CustNotSugGrid.getRowColData(selNo-1,4);
	
	if(tCustomerNo == null || tCustomerNo == "") {
		alert("没有有效的客户信息！");
		return false;
	}
	
	//校验选择疾病是否已经录入健康建议
	var sql = " select 1 from HMSuggestInfo where BusinessType='2' and BusinessCode='" + tCustomerNo + "'";
	var arr = easyExecSql(sql);
	if(!arr) {
		alert("您选择的客户，没有录入相应的健康建议，请先保存该客户的健康建议！");
		return false;
	} 
	
	//发送方式必须选择
	if(!checkMastInput(fmsave.CSendType,"您必须选择一种发送方式！")) {
		return false;
	}
	
	//如果是短信发送，那么要求手机号必录；如果是mail发送，那么要求mail地址必须录入。
	if(fmsave.CSendType.value == "1") {
		if(tMobile = null || tMobile == "") {
			alert("该客户的手机号为空，您不能发送短信息！");
			return false;
		}
	}
	if(fmsave.CSendType.value == "2") {
		if(tEmail = null || tEmail == "") {
			alert("该客户的Email为空，您不能发送邮件！");
			return false;
		}
	}
	
		
	var showStr="正在发送，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fmsave.custfmTransact.value = "CUSTSUGGEST||MAIN";
	fmsave.custfmDetialOperate.value = "SUGGESTSEND||CUST";
	fmsave.action = "./HMSuggestSave.jsp?FLAG=SEL";
	fmsave.submit(); //提交
	
	return true;
}

//发送结果查询
function cSendResultQuery()
{
	if(mResult == null || mResult == "" || mResult == 'null') {
		alert("没有查询到发送记录！请尝试使用条件进行查询。");
		return false;
	}
	var sql =
		" select distinct CustomerNo,Name,Mobile,EMail,HomeAddress, "
			+" LastVisEndDate,LastYearVisCount,LastYearMedRealPay,makedate,"
			+" (case SendType when '1' then '短信发送' when '2' then '邮件发送' when '3' then '打印' end),"
			+" (case SendStatus when '1' then '发送成功' else '发送失败' end ),"
			+" Remark,SerialNo "
			+" from HMSuggestSendTrack a where BusinessType='2' "
			+" and SerialNo in(" + mResult + ") "
			+" order by a.makedate,CustomerNo ";
	 var arr = easyExecSql(sql);
	 if(!arr) {
	 	alert("没有查询到发送记录！请尝试使用条件进行查询。");
	 	return false;
	 }
	 CustAlSendGridTurnPage.queryModal(sql,CustAlSendGrid);  
}




//显示查询结果的疾病健康建议
function selectCustAlSendGrid() {
	var selNoLine=CustAlSendGrid.getSelNo ();//得到被选中的行号
	if (selNoLine>0){
		var tSerialNo  = CustAlSendGrid.getRowColData(selNoLine-1,13);
		if(tSerialNo != null && tSerialNo != "") {
			var sql = " select Suggest "
							+" from HMSuggestSendTrack where BusinessType='2' and SerialNo='"
							+ tSerialNo +"' with ur";
			var arr = easyExecSql(sql);
			if(arr) {
				fmsave.CCustSuggest.value = arr[0][0];
			} else {
				fmsave.CCustSuggest.value = "";
			}
		} else {
			fmsave.CCustSuggest.value = "";
		}
	}
}

//校验发送方式：如果选中，那么发送方式必须录入
function checkCSendType() {
	if(fmsave.isCALSend.checked == true) {
		if(fmsave.CSendType2.value == null || fmsave.CSendType2.value == "") {
			alert("您选择的状态为：已发送，那么您必须选择一种发送方式！");
			fmsave.isCALSend.checked = false;
			return false;
		}
	}
	return true;
}

//已经录入健康建议的客户进行修改
function cAlSuggestCustQuery() {
	
	//客户健康建议录入时间起期
	if(!checkMastInput(fmsave.CCustSuggestStartDate2,"客户健康建议录入时间起期必须录入！")) {
		return false;
	}
	//客户健康建议录入时间止期
	if(!checkMastInput(fmsave.CCustSuggestEndDate2,"客户健康建议录入时间止期必须录入！")) {
		return false;
	}
	
	//没有选择发送方式，那么从健康建议表中获取数据，并且不在已发送的表中存在的数据
	var tCNameCondition = "";
	var tCCustomerNoCondition = "";
	var tCSuggestStartDateCondition = "";
	var tCSuggestEndDateCondition = "";
	var tCSendTypeCondition = "";
	
	var sql = "";
	if(fmsave.CSendType2.value == null || fmsave.CSendType2.value == "") {
		
		//客户姓名
		if(fmsave.CCustomerName2.value != null && fmsave.CCustomerName2.value != "") {
			tCNameCondition = " and a.BusinessName='" + fmsave.CCustomerName2.value + "'";
		}
		
		//客户号
		if(fmsave.CCustomerNo2.value != null && fmsave.CCustomerNo2.value != "") {
			tCCustomerNoCondition = " and a.BusinessCode='" + fmsave.CCustomerNo2.value + "'";
		}
		
		//客户健康建议录入时间起期
		if(fmsave.CCustSuggestStartDate2.value != null && fmsave.CCustSuggestStartDate2.value != "") {
			tCSuggestStartDateCondition = " and a.MakeDate >='" + fmsave.CCustSuggestStartDate2.value + "'";
		}
		
		//客户健康建议录入时间止期
		if(fmsave.CCustSuggestEndDate2.value != null && fmsave.CCustSuggestEndDate2.value != "") {
			tCSuggestEndDateCondition = " and a.MakeDate <='" + fmsave.CCustSuggestEndDate2.value + "'";
		}
		
		sql = " select distinct a.BusinessName,a.BusinessCode, b.mobile,b.Email,b.homeaddress, a.MakeDate, "
							+" '',coalesce(xa.Illcount, 0),'','',coalesce(xa.Realpay, 0) "
			+ " from HMSuggestInfo a,"
				+" HMSENDCUSTDETAIL b, "
				//+" left outer join LLCUSTOMERCLAIM xa on xa.CustomerNo = b.insuredno " 
				+" LLCUSTOMERCLAIM xa "
				+ " where a.BusinessCode = b.insuredno "
					+" and xa.CustomerNo = b.insuredno "
					+" and a.BusinessType='2' "
					+" and not exists ("
					+" select 1 from HMSuggestSendTrack where BusinessType='2' "
								+" and BusinessCode=a.BusinessCode) "
			+ tCNameCondition
			+ tCCustomerNoCondition
			+ tCSuggestStartDateCondition
			+ tCSuggestEndDateCondition;
		
	} else {
		
		//客户姓名
		if(fmsave.CCustomerName2.value != null && fmsave.CCustomerName2.value != "") {
			tCNameCondition = " and b.Name='" + fmsave.CCustomerName2.value + "'";
		}
		
		//客户号
		if(fmsave.CCustomerNo2.value != null && fmsave.CCustomerNo2.value != "") {
			tCCustomerNoCondition = " and b.CustomerNo='" + fmsave.CCustomerNo2.value + "'";
		}
		
		//客户健康建议录入时间起期
		if(fmsave.CCustSuggestStartDate2.value != null && fmsave.CCustSuggestStartDate2.value != "") {
			tCSuggestStartDateCondition = " and a.MakeDate >='" + fmsave.CCustSuggestStartDate2.value + "'";
		}
		
		//客户健康建议录入时间止期
		if(fmsave.CCustSuggestEndDate2.value != null && fmsave.CCustSuggestEndDate2.value != "") {
			tCSuggestEndDateCondition = " and a.MakeDate <='" + fmsave.CCustSuggestEndDate2.value + "'";
		}
		
		//发送方式
		if(fmsave.CSendType2.value != null && fmsave.CSendType2.value != "") {
			tCSendTypeCondition = " and b.SendType ='" + fmsave.CSendType2.value + "'";
		}
		
		sql = " select distinct b.Name,b.CustomerNo,b.Mobile,b.EMail,b.HomeAddress,a.MakeDate, "
				+" b.LastVisEndDate,b.LastYearVisCount,b.VisType,'',LastYearMedRealPay "
				+" from HMSuggestInfo a,HMSuggestSendTrack b "
				+" where a.BusinessType = '2' and a.BusinessCode = b.BusinessCode and b.BusinessType='2' "
			+ tCNameCondition
			+ tCCustomerNoCondition
			+ tCSuggestStartDateCondition
			+ tCSuggestEndDateCondition
			+ tCSendTypeCondition;
	}
	CustAlSugGridTurnPage.queryModal(sql,CustAlSugGrid);
}

function selectAlSuggestGrid() {
	var selNoLine=CustAlSugGrid.getSelNo ();//得到被选中的行号
	if (selNoLine>0){
		var tCustmerNo  = CustAlSugGrid.getRowColData(selNoLine-1,2);
		if(tCustmerNo != null && tCustmerNo != "") {
			var sql = " select HealthSuggest "
							+" from HMSuggestInfo where BusinessType='2' and BusinessCode='"
							+ tCustmerNo +"' with ur";
			var arr = easyExecSql(sql);
			if(arr) {
				fmsave.CCustSuggest2.value = arr[0][0];
			} else {
				fmsave.CCustSuggest2.value = "";
			}
		} else {
			fmsave.CCustSuggest2.value = "";
		}
	}
}

//修改健康建议
function CCustSuggestUp() {
	var tMulCount = CustAlSugGrid.mulLineCount;
	if(tMulCount<=0) {
		alert("没有任何客户健康建议信息！");
		return false;
	}
	var selNo = CustAlSugGrid.getSelNo();
	if(selNo <= 0) {
		alert("请选择一条记录后再点击修改！");
		return false;
	}
	var tCustomerNo = CustAlSugGrid.getRowColData(selNo-1,2);
	if(tCustomerNo == null || tCustomerNo == "") {
		alert("请选择一条有效的客户信息！");
		return false;
	}
	//校验健康建议是否录入
	if(!checkMastInput(fmsave.CCustSuggest2,"请您必须录入健康建议！")) {
		return false;
	}
	
	var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fmsave.custfmTransact.value = "CUSTSUGGEST||MAIN";
	fmsave.custfmDetialOperate.value = "UPDATE||CUST";
	fmsave.action = "./HMSuggestSave.jsp?CCustomerNoForUp='" + tCustomerNo + "'";
	fmsave.submit(); //提交
	
}
//删除健康建议
function CCustSuggestDel() {
	var tMulCount = CustAlSugGrid.mulLineCount;
	if(tMulCount<=0) {
		alert("没有任何客户健康建议信息！");
		return false;
	}
	var selNo = CustAlSugGrid.getSelNo();
	if(selNo <= 0) {
		alert("请选择一条记录后再点击删除！");
		return false;
	}
	var tCustomerNo = CustAlSugGrid.getRowColData(selNo-1,2);
	if(tCustomerNo == null || tCustomerNo == "") {
		alert("请选择一条有效的客户信息！");
		return false;
	}
	if(window.confirm("你确定要删除？")) {
		var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fmsave.custfmTransact.value = "CUSTSUGGEST||MAIN";
		fmsave.custfmDetialOperate.value = "DELETE||CUST";
		fmsave.action = "./HMSuggestSave.jsp?CCustomerNoForDel='" + tCustomerNo + "'";
		fmsave.submit(); //提交
	}
}

//健康建议发送
function cCustSend() {
	//发送方式必须选择
	if(!checkMastInput(fmsave.CSendType3,"您必须选择一种发送方式！")) {
		return false;
	}
	
	//如果不是选择全部发送，那么校验是否至少选择一条记录，并且选择的信息是有效的。
	if(!fmsave.isCAllSend.checked) {
		var gridRowNum = CustAlSugGrid.mulLineCount;
		if(gridRowNum <= 0) {
			alert("没有有效的客户信息，请改变查询条件，再次查询发送");
			return false;
		}
		var isChoose = false;
		for(i=1;i<=gridRowNum;i++) {
			if(CustAlSugGrid.getChkNo(i-1)==true) {
				var tCustomerNo = CustAlSugGrid.getRowColData(i-1,2);
				if(tCustomerNo == null || tCustomerNo == "") {
					alert("第" + i + "行的客户号不能为空！");
					return false;
				} else {
					isChoose = true;
					break
				}
			}
		}
		if(!isChoose) {
			alert("请您至少选择一条客户信息！");
			return false;
		}
		
		var showStr="正在发送，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fmsave.custfmTransact.value = "CUSTSUGGEST||MAIN";
		fmsave.custfmDetialOperate.value = "SUGGESTSEND||CUST";
		fmsave.action = "./HMSuggestSave.jsp?FLAG=CHK";
		fmsave.submit(); //提交
		
		return true;
	
	//选择了全部发送
	} else {
		if(window.confirm("您选择了全部发送，这样数据量可能会非常庞大，是否继续？")) {
			var showStr="正在发送，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fmsave.custfmTransact.value = "CUSTSUGGEST||MAIN";
			fmsave.custfmDetialOperate.value = "SUGGESTSENDALL||CUST";
			fmsave.action = "./HMSuggestSave.jsp";
			fmsave.submit(); //提交
			
		} else {
			alert("您取消了全部发送");
		}
		return true;		
	}
}

//结果查询
function disCustAlSendResultQuery() {
	if(mResult == null || mResult == "" || mResult == 'null') {
		alert("没有查询到发送记录！请尝试使用条件进行查询。");
		return false;
	}
	var sql =
		" select distinct CustomerNo,Name,Mobile,EMail,HomeAddress, "
			+" LastVisEndDate,LastYearVisCount,LastYearMedRealPay,makedate,"
			+" (case SendType when '1' then '短信发送' when '2' then '邮件发送' when '3' then '打印' end),"
			+" (case SendStatus when '1' then '发送成功' else '发送失败' end ),"
			+" Remark,SerialNo "
			+" from HMSuggestSendTrack a where BusinessType='2' "
			+" and SerialNo in(" + mResult + ") "
			+" order by a.makedate,CustomerNo ";
	 var arr = easyExecSql(sql);
	 if(!arr) {
	 	alert("没有查询到发送记录！请尝试使用条件进行查询。");
	 	return false;
	 }
	 CustAlSendGridTurnPage.queryModal(sql,CustAlSendGrid);  
}

//已发送健康建议查询
function cCustAlSendResultQuery() {
	var tSQL = ""
	   
  		//最近一年就诊次数
		var LastVisCountCondition = "";
		if(fmsave.CLastVisCount3.value != null && fmsave.CLastVisCount3.value != "") {
			LastVisCountCondition = " and a.LastYearVisCount>=" + fmsave.CLastVisCount3.value*1;
		}
		//最近一年医疗费用理赔额度
		var LastYearMedRealPayCondition = "";
		if(fmsave.CHCostClaimFee3.value != null && fmsave.CHCostClaimFee3.value != "") {
			LastYearMedRealPayCondition = " and a.LastYearMedRealPay>=" + fmsave.CHCostClaimFee3.value*1;
		}
		
		//就诊方式
//		var visttypecondition ="";
//	    if(fmsave.CVistType.value != null && fmsave.CVistType.value != "") {
//	   		//门诊
//	   		if(fmsave.CVistType.value == '2') {
//	   			visttypecondition = 
//	   				" and b.FeeType='1'";
//	   		}
//	   		//住院
//	   		if(fmsave.CVistType.value == '3') {
//	   			visttypecondition = 
//	   				" and b.FeeType='2' ";
//	   		}
//	    }
		
		//姓名
		var nameCondition = "";
		if(fmsave.CCustomerName3.value != null && fmsave.CCustomerName3.value != "") {
			nameCondition = " and a.name ='" + fmsave.CCustomerName3.value + "'";
		}
		
		//客户号
		var customerNoCondition = "";
		if(fmsave.CCustomerNo3.value != null && fmsave.CCustomerNo3.value != "") {
			customerNoCondition = " and a.CustomerNo ='" + fmsave.CCustomerNo3.value + "'";
		}
		
//		//证件类型
//		var iDTypeCondition = "";
//		if(fmsave.CIDType.value != null && fmsave.CIDType.value != "") {
//			iDTypeCondition = " and a.idtype ='" + fmsave.CIDType.value + "'";
//		}
//		
//		//证件号码
//		var idNoCondition = "";
//		if(fmsave.CIDNo.value != null && fmsave.CIDNo.value != "") {
//			idNoCondition = " and a.idno ='" + fmsave.CIDNo.value + "'";
//		}
		
		//最近一次就诊时间结束起期
		var lastVisStartDateCondition = "";
		if(fmsave.CLastVisStartDate3.value != null && fmsave.CLastVisStartDate3.value != "") {
			lastVisStartDateCondition = " and a.LastVisEndDate >='" + fmsave.CLastVisStartDate3.value + "'";
		}
		//最近一次就诊时间结束止期
		var lastVisEndDateCondition = "";
		if(fmsave.CLastVisEndDate3.value != null && fmsave.CLastVisEndDate3.value != "") {
			lastVisEndDateCondition = " and a.LastVisEndDate <='" + fmsave.CLastVisEndDate3.value + "'";
		}
		
		//疾病代码
		var diseaseCodeCondition = "";
		if(fmsave.CDiseaseCode3.value != null && fmsave.CDiseaseCode3.value != "") {
			diseaseCodeCondition = " and exists(select 1 from HMCLAIMDETAIL b where b.customerno=a.customerno "
										+" and b.DiseaseCode='" + fmsave.CDiseaseCode3.value +"' )";
		}
		
		//健康建议发送起期
		var suggestSendStartCondition = "";
		if(fmsave.CSuggestSendStartDate3.value != null && fmsave.CSuggestSendStartDate3.value != "") {
			suggestSendStartCondition = " and a.makedate >='" + fmsave.CSuggestSendStartDate3.value + "'";
		}
		//健康建议发送止期
		var suggestSendEndCondition = "";
		if(fmsave.CSuggestSendEndDate3.value != null && fmsave.CSuggestSendEndDate3.value != "") {
			suggestSendEndCondition = " and a.makedate <='" + fmsave.CSuggestSendEndDate3.value + "'";
		}
		//发送方式
		var sendTypeCondition = "";
		if(fmsave.CCSendType3.value != null && fmsave.CCSendType3.value != "") {
			sendTypeCondition = " and a.SendType ='" + fmsave.CCSendType3.value + "'";
		}
		
  		tSQL = 
		 " select distinct a.CustomerNo, "
		       +" a.name, "
		       +" a.mobile, "
		       +" a.email, "
		       +" a.homeaddress, "
		       +" a.LastVisEndDate, "
		       +" a.LastYearVisCount, "
		       +" a.LastYearMedRealPay, "
		       +" a.makedate,"
		       +" (case a.SendType when '1' then '短信发送' when '2' then 'Mail发送' when '3' then '打印' end ),"
		       +" (case a.SendStatus when '1' then '发送成功' else '发送失败' end ), "
		       +" a.Remark,a.SerialNo "
		
		  +" from HMSuggestSendTrack a "
		 +" where a.BusinessType = '2' "
  		 
  		 + LastVisCountCondition
	  	 + LastYearMedRealPayCondition
	  	 + nameCondition
	  	 + customerNoCondition
  		 + lastVisStartDateCondition
	  	 + lastVisEndDateCondition
	  	 + diseaseCodeCondition
	  	 + suggestSendStartCondition
	  	 + suggestSendEndCondition
	  	 + sendTypeCondition
	  	 
  		 + " with ur ";
	  		
	 CustAlSendGridTurnPage.queryModal(tSQL,CustAlSendGrid);  
}


//**********************************************************************

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, OperateDetail )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    //疾病健康建议修改,删除
    if(OperateDetail == "DisSuggestModify") {
    	//调用健康建议查询
    	alDisSuggestQuery();
    }
    if(OperateDetail == "DisSuggestDel") {
    	//调用健康建议查询
    	fm.DisSuggest2.value = "";
    	alDisSuggestQuery();
    }
    
    if(OperateDetail == "CustSuggestDel") {
    	fmsave.CCustSuggest2.value = "";
    	cAlSuggestCustQuery();
    }
    
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow)
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

