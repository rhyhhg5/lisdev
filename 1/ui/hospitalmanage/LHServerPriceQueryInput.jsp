<%
//程序名称：LHServerPriceQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-03-9 15:24:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期    : 
// 更新原因/内容: 
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHServerPriceQueryInput.js"></SCRIPT> 
  <%@include file="LHServerPriceQueryInputInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  String no = request.getParameter("no");
  System.out.println("AAAAAAAAAAQQQQQQQQQQ "+no);
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>                            
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
 
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "LHContServPriceGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNo verify="合同编号|len<=50">
    </TD>
    <TD  class= title>
		      服务价格表类型
		</TD>
	  <TD   class= input>
		     <Input type=hidden name=ServicePriceCode >
		      <Input class= 'code' name=ServicePriceName  verify=" 服务价格表类型" CodeData= "0|^1|服务单价^2|体检价格"  ondblClick= "showCodeListEx('',[this,ServicePriceCode],[1,0],null,null,null,'1',200);" onkeyup=" if(event.keyCode ==13)  return showCodeListEx('',[this,ServicePriceCode],[1,0],null,fm.ServicePriceName.value,'ServicePriceName','',200);" onkeydown="return showCodeListKey('',[this,ServicePriceCode],[1,0],null,fm.ServicePriceName.value,'ServicePriceName','',200); codeClear(ServicePriceName,ServicePriceCode);">	
		</TD>
		<TD  class= title>
		      费用类型
		</TD>
		<TD  class= input>
		      <Input type=hidden name=BalanceTypeCode>
		      <Input class= 'code' name=BalanceTypeName  verify="费用类型" CodeData= "0|^1|健康体检费用^2|健康管理技术费用^3|医师费用^4|其他费用"  ondblClick= "showCodeListEx('',[this,BalanceTypeCode],[1,0],null,null,null,1,200);" onkeyup=" if(event.keyCode ==13)  return showCodeListEx('',[this,BalanceTypeCode],[1,0],null,fm.BalanceTypeName.value,'BalanceTypeName','',200);" onkeydown="return showCodeListKey('',[this,BalanceTypeCode],[1,0],null,fm.BalanceTypeName.value,'BalanceTypeName','',200); codeClear(BalanceTypeName,BalanceTypeCode);">	
		</TD>
	</TR>
</table>
<!--table class = common style= "display: 'none'">
    <TD class= title>
    	合作机构名称
    </TD>
    <TD  class= input>
    	<Input class= 'common' name=HospitCode >
    </TD>
    <TD  class= title>
      合同责任项目类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraType >
    </TD>
  </TR>
  <TR  class= common>
  	<TD  class= title>
      合同责任项目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNowName >
    </TD>  
    <TD  class= title>
      服务价格表类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ServicePriceName >
    </TD> 
    <TD  class= title>
      费用类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BalanceTypeName >
    </TD>
    </TR>
</table-->
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT class =cssbutton VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
        <INPUT class =cssbutton VALUE="返回"   TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>  
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHContServPriceGrid);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHServerPriceGrid" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHContServPriceGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
