<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDHospitalSave.jsp
//程序功能：
//创建日期：2005-01-15 14:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.hospitalmanage.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	
	
	//探访条件
	HMVisitConditionSet tHMVisitConditionSet   = new HMVisitConditionSet();
	//探访记录
	HMVisitInfoSet tHMVisitInfoSet   = new HMVisitInfoSet();
	
	HMVisitManagelUI tHMVisitManageUI   = new HMVisitManagelUI();
		
	TransferData tTransferData = new TransferData();

	System.out.println("---------接收信息，并作校验处理。------------");
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	String transact = request.getParameter("fmtransact");
	// 01-探访条件操作；02探访记录操作
	String opertype = request.getParameter("OperType");
	
	tTransferData.setNameAndValue("OperType",opertype);
	
	// 01-探访条件操作
	if("01".equals(opertype))
	{
		String tMngCom = request.getParameter("MngCom");
		tTransferData.setNameAndValue("MngCom",tMngCom);
		
		String tChk[] = request.getParameterValues("InpHMVisitConditionGridChk"); 	
		//参数格式=” Inp+MulLine对象名+Chk”		
		String tManageCom[] = request.getParameterValues("HMVisitConditionGrid1");
    	String tCondition[] = request.getParameterValues("HMVisitConditionGrid2");	
    	String tRate[] = request.getParameterValues("HMVisitConditionGrid3");	
    	String tFlowNo[] = request.getParameterValues("HMVisitConditionGrid4");	   	
    	String tMakeDate[] = request.getParameterValues("HMVisitConditionGrid5");	
    	String tMakeTime[] = request.getParameterValues("HMVisitConditionGrid6");	
    	String MngCom[] = request.getParameterValues("HMVisitConditionGrid7");	
    	    	  
        for(int index=0;index<tChk.length;index++)
        {
			if(tChk[index].equals("1"))           
			{
				HMVisitConditionSchema tHMVisitConditionSchema = new HMVisitConditionSchema();
	    		
	    		tHMVisitConditionSchema.setManageCom(tManageCom[index]);
	    		tHMVisitConditionSchema.setFlowNo(tFlowNo[index]);
	    		
	    		tHMVisitConditionSchema.setMngCom(MngCom[index]); //录入机构 
	    		tHMVisitConditionSchema.setRate(tRate[index]);
	    		tHMVisitConditionSchema.setCondition(tCondition[index]);
	    		
    			tHMVisitConditionSchema.setMakeDate(tMakeDate[index]);
    			tHMVisitConditionSchema.setMakeTime(tMakeTime[index]);	    		
	    		tHMVisitConditionSchema.setModifyDate(PubFun.getCurrentDate());
	    		tHMVisitConditionSchema.setModifyTime(PubFun.getCurrentTime());    		
	    		tHMVisitConditionSchema.setOperator(tG.Operator);
	    		
	    		System.out.println("==========="+tFlowNo[index]+"======mngcom="+MngCom[index]);
	    			    		   		
	    		tHMVisitConditionSet.add(tHMVisitConditionSchema);						
			}
        }                 	
	}
	// 02探访记录操作
	if("02".equals(opertype))
	{
		String tMngCom_Info = request.getParameter("MngCom_Info");
		//String tCustomerNo = request.getParameter("CustomerNo");
		//String tCustomerName = request.getParameter("CustomerName");
				
		String tChk[] = request.getParameterValues("InpHMVisitInfoGridChk"); 	
		//参数格式=” Inp+MulLine对象名+Chk”		
		String tCustomerNo[] = request.getParameterValues("HMVisitInfoGrid1");  //客户号码
    	String tCustomerName[] = request.getParameterValues("HMVisitInfoGrid2");//客户姓名	
    	String tHospitalName[] = request.getParameterValues("HMVisitInfoGrid3");//探访医院名称	
    	String tVisitDate[] = request.getParameterValues("HMVisitInfoGrid4");   //探访时间
    		 
    	String tIdentifiction[] = request.getParameterValues("HMVisitInfoGrid5");//是否进行客户身份确认	
    	String tCollection[] = request.getParameterValues("HMVisitInfoGrid6");	//是否收集相关资料
    	String tBed[] = request.getParameterValues("HMVisitInfoGrid7");			//有无挂/压床
    	String tLegitimacy[] = request.getParameterValues("HMVisitInfoGrid8");  //是否确认就诊断依据和诊疗项目合理性
    	String tSupport[] = request.getParameterValues("HMVisitInfoGrid9");	 //是否提供相关就医和理赔支持服务
		String tFlowNo[] = request.getParameterValues("HMVisitInfoGrid10");	  
    	String tMakeDate[] = request.getParameterValues("HMVisitInfoGrid11");	 //是否提供相关就医和理赔支持服务
		String tMakeTime[] = request.getParameterValues("HMVisitInfoGrid12");	  
				
        for(int index=0;index<tChk.length;index++)
        {
			if(tChk[index].equals("1"))           
			{
				HMVisitInfoSchema tHMVisitInfoSchema = new HMVisitInfoSchema();
				
				tHMVisitInfoSchema.setFlowNo(tFlowNo[index]);
				tHMVisitInfoSchema.setCustomerNo(tCustomerNo[index]);
				tHMVisitInfoSchema.setName(tCustomerName[index]);
				tHMVisitInfoSchema.setHospitName(tHospitalName[index]);
				tHMVisitInfoSchema.setVisitDate(tVisitDate[index]);
				tHMVisitInfoSchema.setIdentifiction(tIdentifiction[index]);
				tHMVisitInfoSchema.setCollection(tCollection[index]);
				tHMVisitInfoSchema.setBed(tBed[index]);
				tHMVisitInfoSchema.setLegitimacy(tLegitimacy[index]);
				tHMVisitInfoSchema.setSupport(tSupport[index]);
				
				tHMVisitInfoSchema.setManageCom(tMngCom_Info);
				tHMVisitInfoSchema.setOperator(tG.Operator);
				
    			tHMVisitInfoSchema.setMakeDate(tMakeDate[index]);
    			tHMVisitInfoSchema.setMakeTime(tMakeTime[index]);					
				tHMVisitInfoSchema.setModifyDate(PubFun.getCurrentDate());
				tHMVisitInfoSchema.setModifyTime(PubFun.getCurrentTime());
				
				if("".equals(tFlowNo[index])||tFlowNo[index]==null)
				{
	    			//新增记录
	    			tHMVisitInfoSchema.setFlowNo(PubFun1.CreateMaxNo("VisitInfo", 20));
	    			tHMVisitInfoSchema.setMakeDate(PubFun.getCurrentDate());
	    			tHMVisitInfoSchema.setMakeTime(PubFun.getCurrentTime());					
				}
				
				
				tHMVisitInfoSet.add(tHMVisitInfoSchema);
			}
        }                 				
	}
	
	try
	{
		// 准备传输数据 VData
		System.out.println("------准备传输数据 VData--------");
	 	VData tVData = new VData();
	 	
		tVData.add(tHMVisitConditionSet);
		tVData.add(tHMVisitInfoSet);
		tVData.add(tTransferData);
	 	tVData.add(tG);
	 	
		tHMVisitManageUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tHMVisitManageUI.mErrors;
		if (!tError.needDealError())
		{
			Content = " 操作成功! ";
			FlagStr = "Success";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

  //添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
