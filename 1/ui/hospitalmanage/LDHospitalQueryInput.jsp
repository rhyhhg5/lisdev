<%
//程序名称：LDHospitalInput.jsp
//程序功能：功能描述
//创建日期：2005-01-15 14:25:18
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="LDHospitalQueryInput.js"></SCRIPT> 
	<%@include file="LDHospitalQueryInit.jsp"%> 
	<title></title>
</head>
<%
	GlobalInput tG = new GlobalInput(); 
	tG = (GlobalInput)session.getValue("GI");
%>
<script>
	var comCode = "<%=tG.ComCode%>";
	var manageCom = "<%=tG.ManageCom%>";
	var operator = "<%=tG.Operator%>";
	var msql = "1 and char(length(trim(comcode)))=#8#" 
</script>
<body onload="initForm();">

<form method=post name=fm target=fraSubmit>
 
<!-- 导入按钮界面 -->
<%@include file="../common/jsp/InputButton.jsp"%>
<!-- 查询条件部分 -->
<!-- 查询条件Title -->
<table class=common border=0 width=100%>
	<tr>
		<td class=titleImg align=center> 请输入查询条件： </td>
	</tr>
</table>
	
<Div  id= "divLDHospitalGrid1" style= "display: ''">    
	<table  class= common align='center' >
		<TR  class= common>
			<TD class=title>管理机构</TD>
			<TD class=input>
				<Input class="codeno" name=MngCom
					ondblclick="return showCodeList('comcode',[this,MngCom_ch],[0,1],null,'','',1);"
					onkeyup="return showCodeListKey('comcode',[this,MngCom_ch],[0,1],null,'','',1);" readonly verify="管理机构|code:comcode&NOTNULL"><Input class=codename name=MngCom_ch>
			</TD>						
			<TD  class= title>医疗服务机构类型</TD>
			<TD  class= input>
			  <Input class=codeno name=HospitalType  CodeData= "0|^医疗机构|1^健康体检机构|2^健康管理公司|3^紧急救援公司|4^指定口腔医疗机构|5^其他合作机构|6" 
			  ondblClick= "showCodeListEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);" 
			  onkeyup= "showCodeListKeyEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);"><Input class= codename name=HospitalType_ch  elementtype=nacessary  verify="医疗服务机构类型|NOTNULL&len<=20">
			</TD>
			<TD  class= title>医疗服务机构名称</TD>
			<TD  class= input>   	
			  <Input class= 'code' name= HospitName ondblclick="return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300);" 
			  onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300); ">
			</TD>
		</TR>    
		<TR>    
			<TD  class= title>医疗服务机构代码</TD>
			<TD  class= input>
			     <Input class= 'common' name=HospitCode >
			</TD> 		    
		    <TD class= title>合作级别</TD>
			<TD  class= input>
		    	<Input class='codeno' name=AssociateClass 
		    	ondblclick="showCodeList('llhospiflag',[this,AssociateClassName],[0,1],null,null,null,1,200);"><Input class= codename name=AssociateClassName >  
		    </TD>
		    <TD  class= title>医院等级</TD>
		    <TD  class= input>
				<Input class= 'codeno' name=LevelCode ondblClick= "showCodeList('hospitalclass',[LevelCode,LevelName],[0,1]);" 
				onkeyup= "showCodeListKeyEx('hospitalclass',[LevelCode,LevelName],[0,1]);"><Input class= codename name=LevelName>  
		    </TD>
    	</TR>
	    <TR>
		    <TD  class= title>地区代码</TD>
		    <TD  class= input>
				<Input class= 'codeno' style="width:52%" name=AreaCodeName 
				ondblClick= "showCodeList('hmareaname',[this,AreaCode],[0,1],null,fm.AreaCodeName.value,'codename',1,193);"><Input class=codeName style="width:25%" name=AreaCode>
		    </TD>	    	
	    	<TD class=title>操作员</TD>
	    	<TD class=input>
	    		<input class=common name=Operator >
	    	</TD>
	    	<TD class=title>最后修改时间</TD>
	    	<TD class=input>
	    		<input class=coolDatePicker name=LastModifyDate>
	    	</TD>
	    </TR>
	    <TR>
	    	<TD  class= title>专科名称</TD>
			<TD  class= input>
		    	<Input class='codeno' name=SpecialCode 
		    	ondblclick="showCodeList('lhspecname',[this,SpecialName],[0,1],null,fm.SpecialCode.value,'code',1,200);"><Input class= codename name=SpecialName >  
		    </TD>	
	    	<TD  class= title>合同效力</TD>
			<TD  class= input>
				<Input class=codeno name=ContFlag  CodeData= "0|^有效|1^无效|2^全部|3"  value = '3'
					ondblClick= "showCodeListEx('LHContFlag',[this,LHContFlag_ch],[1,0],null,null,null,1);" 
			        onkeyup= "showCodeListKeyEx('LHContFlag',[this,LHContFlag_ch],[1,0],null,null,null,1);"><Input class= codename name=LHContFlag_ch  value = '全部'>		    		
		    </TD>			        
	    </TR>
	</table>
</Div>
    
<table class=common border=0 width=100%>
	<TR class=common>  
	<TD  class=title>
		<INPUT VALUE="查询"  TYPE=button   class=cssbutton onclick="easyQueryClick();">
		<INPUT VALUE="返回"  TYPE=button   class=cssbutton onclick="returnParent();"> 					
	</TD>
	</TR>
</table> 
      
<hr>   
  
  <!-- 查询结果部分 -->      
<table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDHospital1);">
  		</td>
  		<td class=titleImg>信息</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLDHospital1" style="display:''">
		<table class=common>
    		<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLDHospitalGrid"></span> 
			    </td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
		<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
   
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact"> 
  
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
