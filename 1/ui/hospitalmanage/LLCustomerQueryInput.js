/** 
 * 程序名称：LHTotalInfoQuery.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-05-23 11:48:27
 * 创建人  ：刘莹
 * 更新人  ： 郭丽颖 
 * 更新日期：2006-05-24 10:12:24
 */
 var turnPage = new turnPageClass(); 
 var turnPage2 = new turnPageClass();
 var turnPage3 = new turnPageClass();
 var turnPage4 = new turnPageClass(); 
 var turnPage5 = new turnPageClass();
 var turnPage6 = new turnPageClass(); 
 var turnPage7 = new turnPageClass();   
 var turnPage8 = new turnPageClass();   
 var turnPage9 = new turnPageClass();  
 
 var fmPreNameBack;
function ContQuery() 
{
	//此处书写SQL语句			     
	  ContStyle.style.display='';
    ServeStyle.style.display='none';
    ServerStatus.style.display='none';
    InHospitalStyle.style.display='none';
    HealthCheck.style.display='none';
    CustomHealthStatus.style.display='none';
    HealthFilePrint.style.display='none';
    divLHMessSendInfoGrid.style.display='none';
    HealthCheckPrint.style.display='none';
}
function ContCheckQuery()
{
	if(fm.CustomerNo.value==null||fm.CustomerNo.value=="")
	{
		alert("客户号码为必录项，请录入客户号码!");
		return;
	}
	else
	{
		if(fm.all('ContType').value=="1")
		{
			   var strSql=" select distinct a.contno,a.cvalidate,a.cinvalidate,a.appntno,a.appntname,b.insuredno,b.name,"
			   	   +" case b.Sex when '0' then '男' when '1' then '女' else '其他' end ,b.Birthday,b.IDNo,a.conttype"
                   +" from lccont a, lcinsured b "
                   +" where a.conttype='1' and  a.contno=b.contno "
                   +getWherePart("a.AgentCode","AgentCode")
                   +getWherePart("(select name from laagent where agentcode=a.agentcode)","AgentName","like")
                   +getWherePart("b.IDNo","IDNo")
                   +getWherePart("b.Insuredno","CustomerNo") 
                   +getWherePart("b.Name","CustomerName", "like")
                   +getWherePart("a.appntno","ACustomerNo") 
                   +getWherePart("a.appntname","ACustomerName", "like")
                   +getWherePart("a.Managecom","MngCom", "like")
                   if(fm.Caseno.value!=""&&fm.Caseno.value!=null)
                   {
                   		strSql+=" and exists (select 1 from llcase where caseno='"+fm.Caseno.value+"' and customerno=a.insuredno)"
                   }
			   turnPage.queryModal(strSql, LHHealthServPlanGrid);
			   initLHContSpecialInfo();
		}
		if(fm.all('ContType').value=="2")
		{
			   var strSql=" select distinct a.contno,a.cvalidate,a.cinvalidate,a.appntno,a.appntname,c.insuredno,c.name, "
			   	   +" case c.Sex when '0' then '男' when '1' then '女' else '其他' end ,c.Birthday,c.IDNo,a.conttype"
                   +" from lccont a, lcpol b, lcinsured c "
                   +" where a.conttype='1'  and  a.contno=b.contno  and  a.contno=c.contno  and "
                   +" b.riskcode in (select distinct riskcode from lmriskapp where risktype2 = '5')  "
                   +getWherePart("a.AgentCode","AgentCode")
                   +getWherePart("(select name from laagent where agentcode=a.agentcode)","AgentName")
                   +getWherePart("c.IDNo","IDNo")
                   +getWherePart("a.Insuredno","CustomerNo") 
                   +getWherePart("c.Name","CustomerName")
                   +getWherePart("a.appntno","ACustomerNo") 
                   +getWherePart("a.appntname","ACustomerName")
                   +getWherePart("a.Managecom","MngCom", "like")
                   if(fm.Caseno.value!=""&&fm.Caseno.value!=null)
                   {
                   		strSql+=" and exists (select 1 from llcase where caseno='"+fm.Caseno.value+"' and customerno=a.insuredno)"
                   }
         turnPage.queryModal(strSql, LHHealthServPlanGrid);
         initLHContSpecialInfo();
		}
		if(fm.all('ContType').value=="3")
		{
			   var strSql=" select distinct a.grpcontno,a.cvalidate,a.cinvalidate,a.appntno,a.appntname,b.insuredno,b.name,"
			   	   +" case b.Sex when '0' then '男' when '1' then '女' else '其他' end ,b.Birthday,b.IDNo,a.conttype"
                   +" from lccont a, lcinsured b "
                   +" where a.conttype='2' and  a.contno=b.contno "
                   +getWherePart("a.AgentCode","AgentCode")
                   +getWherePart("(select name from laagent where agentcode=a.agentcode)","AgentName","like")
                   +getWherePart("b.IDNo","IDNo")
                   +getWherePart("b.Insuredno","CustomerNo") 
                   +getWherePart("b.Name","CustomerName", "like")
                   +getWherePart("a.appntno","ACustomerNo") 
                   +getWherePart("a.appntname","ACustomerName", "like")
                   +getWherePart("a.Managecom","MngCom", "like")
                   if(fm.Caseno.value!=""&&fm.Caseno.value!=null)
                   {
                   		strSql+=" and exists (select 1 from llcase where caseno='"+fm.Caseno.value+"' and customerno=a.insuredno)"
                   }

			   turnPage.queryModal(strSql, LHHealthServPlanGrid);
			   initLHContSpecialInfo();
		}
	}
}
function ContDetailInfo()
{
	if (LHHealthServPlanGrid.getSelNo() >= 1)
	{     
		if(LHHealthServPlanGrid.getRowColData(LHHealthServPlanGrid.getSelNo()-1,11)=="")
		{
			alert("你选择了空的列，请重新选择!");
			return false;
		}
		else
		{
			var ContNo = LHHealthServPlanGrid.getRowColData(LHHealthServPlanGrid.getSelNo()-1,1);
			
			if (fm.ContType.value == '3')
  			{
  				window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ ContNo+"&ContType=1","保单详细信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
  			}
  			else if (fm.ContType.value == '1'||fm.ContType.value=='2')
  			{
  				window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + ContNo +"&IsCancelPolFlag=0"+"&ContType=1","保单详细信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');	
  			}
  			else
			{
				 alert("要传输的保单类型不能为空!");
			} 
			           
		}
	} 
	else
	{
		alert("请选择一条要传输的记录！");    
	}

}

function showOne()
{	
	var getSelNo = LHHealthServPlanGrid.getSelNo();
	var ContNo = LHHealthServPlanGrid.getRowColData(getSelNo-1, 1);
	if(fm.all('ContType').value=="1"||fm.all('ContType').value=="2")
	{
		var strSql2="select (select riskname from lmrisk where riskcode=a.riskcode),"
             +" amnt, "
             +" (select AppntName from LCAppnt where AppntNo=a.appntno and ContNo=a.contno) "
             +" from lcpol a where 1=1 and contno = '"+ContNo+"' and a.GrpContNo='00000000000000000000'"
             + getWherePart("a.Managecom","ComID", "like")
             + getWherePart("a.insuredno","CustomerNo");
	}
	else if(fm.all('ContType').value=="3")
	{
		 var strSql2=" select (select riskname from lmrisk where riskcode=a.riskcode),amnt,"
             +" (select Name from LCGrpAppnt where GrpContNo=a.GrpContNo )"
             +" from lcpol a where 1=1 and grpcontno='"+ContNo+"' "
             + getWherePart("a.Managecom","ComID", "like")
             + getWherePart("a.insuredno","CustomerNo");
	}	           
   //alert(strSql2);
   //alert(easyExecSql(strSql2));
   turnPage2.pageLineNum = 15;    
   turnPage2.queryModal(strSql2, LHContSpecialInfo); 
}
function ServeQuery()
{
	ContStyle.style.display='none';
	ServeStyle.style.display='';
	ServerStatus.style.display='none';
	InHospitalStyle.style.display='none';
	HealthCheck.style.display='none';
	CustomHealthStatus.style.display='none';
	HealthFilePrint.style.display='none';
	divLHMessSendInfoGrid.style.display='none';
	HealthCheckPrint.style.display='none';
    initLHHealthServPlanGrid();
    initLHContSpecialInfo();
    initLHServerPlanType();
    initLHPlanSpecialInfo();
    initLHServerDoStatus();
    initLHInHospitalMode();
    initLHHealthCheckupStyle();
}
function ServePlanQuery()
{ 
	 var strSql=" select ServPlanName,ServPlanLevel ,"
	           +" ContNo,a.comid||'-'||(select c.name  from ldcom c where c.comcode=a.Comid),"
               +" StartDate,EndDate,ServPrem,servplanno, "
               +" a.Customerno, (select d.Name from LDPERSON d where d.Customerno=a.Customerno )"
               +" from LHServPlan a where 1=1 "
               +getWherePart("a.ServPlancode","Riskcode") 
               +getWherePart("a.CustomerNo","CustomerNo")
               +getWherePart("a.name","CustomerName", "like")
               +getWherePart("a.comid","ComID", "like")
             ;
   //alert(strSql);
   //fm.all('CustomerName').value=strSql;
//   alert(easyExecSql(strSql));//b.SignCom=a.ComID and
	turnPage3.queryModal(strSql, LHServerPlanType);
	initLHPlanSpecialInfo();
}
function ServPlanDetail()
{
	if(LHServerPlanType.getSelNo() >= 1)
	{
	  var ContNo = LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,3);
	  if(fm.all('ContTypeIn').value == "1" || fm.all('ContTypeIn').value == "2")
    {
    	var PersonFee  = "";
    	if(fm.all('ContTypeIn').value == "1")
    	{
			   var sql = " select sum(Prem) from lcpol where  contno = '" + ContNo +"'"
					       +" and insuredno = '"+LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,9)+"' ";
			   PersonFee =  easyExecSql(sql);
		  }
	    var count = easyExecSql(" select count(c.insuredno) from lcinsured c, ldperson d where c.insuredNo = d.customerno and c.contno = '"+ContNo+"' and c.grpcontno = '00000000000000000000' ");
	    //alert(count);
	    if( count == "1" ||  count == "0" || count == "null" || count == "")
	    	inputCont(PersonFee);
	    else
	    	inputCont2(PersonFee);
	  }
	  else if(fm.all('ContTypeIn').value == "0")
	  {//对按保费查询的跳转处理
		   var sql = " select sum(Prem) from lcpol where  contno = '"+ContNo+"'"
				    +" and insuredno = '"+LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,9)+"' ";
	    	var PersonFee =  easyExecSql(sql);
		  inputCont3(PersonFee);
	  }
  }
  else
	{ 	alert("请选择一条要传输的记录！");   
  		return false;
	}
}  

//打开录入界面函数
function inputCont(PersonFee)
{
	if(fm.all('ContTypeIn').value == "1"||fm.all('ContTypeIn').value == "0")//个人保单
	{
		var TempContNo = LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,3);
		var TempPrtNo = getPrtNo(TempContNo);
		window.open("LHServPlanInputMain.jsp?ContNo="+TempContNo+"&PrtNo="+TempPrtNo+"&CustomerNo="+LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,9)+"&PersonFee="+PersonFee,"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
	}
	
	if(fm.all('ContTypeIn').value == "2")//团体保单
	{        
		window.open("LHGrpServPlanMain.jsp?ContNo="+LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,3),"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
	}
}     
function getPrtNo(TempContNo)
{
	var strSql = "select distinct PrtNo from lccont where ContNo='"+TempContNo+"'";
	prtNo = easyExecSql(strSql);
	return prtNo;
}
function inputCont2(PersonFee)
{
	if(fm.all('ContTypeIn').value == "1"||fm.all('ContTypeIn').value == "0")//个人保单
	{
		var TempContNo = LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,3);
		var TempPrtNo = getPrtNo(TempContNo);
		window.open("LHServPlanInputMain.jsp?ContNo="+TempContNo+"&PrtNo="+TempPrtNo+"&CustomerNo="+LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,9)+"&PersonFee="+PersonFee,"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
	}
}       

//按保费查询后的跳转
function inputCont3(  PersonFee )
{
	var TempContNo = LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,3);
	var TempPrtNo = getPrtNo(TempContNo);
	window.open("LHServPlanInputMain.jsp?ContNo="+TempContNo+"&PrtNo="+TempPrtNo+"&CustomerNo="+LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,9)+"&PersonFee="+PersonFee+"&flag=Fee","",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
}  
function ServRunDetail()
{   
	if (LHPlanSpecialInfo.getSelNo() >= 1)
	{     
		// alert(LHPlanSpecialInfo.getRowColData(LHPlanSpecialInfo.getSelNo()-1,1));
		if(LHPlanSpecialInfo.getRowColData(LHPlanSpecialInfo.getSelNo()-1,1)=="")
		{
			alert("你选择了空的列，请重新选择!");
			return false;
		}
		else
		{
			var Status = LHPlanSpecialInfo.getRowColData(LHPlanSpecialInfo.getSelNo()-1,3);
			if(Status=="未执行")
			{
				alert("因为服务未执行完成，所以暂时没有服务详细信息!");
			}
			else 
			{
				var ServPlanNo = LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,8);
				window.open("./LHServExeTrace.jsp?ServPlanNo="+ServPlanNo+"&flag=1","服务执行详情",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			}
		}
	} 
    else
	{
		alert("请选择一条要传输的记录！");    
	}
}
function ServRunDetail2()
{
	if (LHServerDoStatus.getSelNo() >= 1)
	{     
		// alert(LHServerDoStatus.getRowColData(LHServerDoStatus.getSelNo()-1,1));
		if(LHServerDoStatus.getRowColData(LHServerDoStatus.getSelNo()-1,1)=="")
		{
			  alert("你选择了空的列，请重新选择!");
				return false;
		}
		else
		{
			var Status = LHServerDoStatus.getRowColData(LHServerDoStatus.getSelNo()-1,4);
			if(Status=="未执行")
			{
				   alert("因为服务未执行完成，所以暂时没有服务详细信息!");
			}
			else 
			{
				var getSelNo = LHServerDoStatus.getSelNo();
	         	var Contno = LHServerDoStatus.getRowColData(getSelNo-1, 3);
                var strSql2="select servplanno from LHServPlan"
                     +"  where 1=1 and Contno = '"+Contno+"'"
           		//alert(strSql2);
           		//alert(easyExecSql(strSql2));
           		var ServPlanNo=easyExecSql(strSql2);
           		//alert(ServPlanNo);
           		if (ServItemCaseGrid.getSelNo() >= 1)
           		{
           				var getSelNo2 = ServItemCaseGrid.getSelNo();
           		    var ServCaseCode = ServItemCaseGrid.getRowColData(getSelNo2-1, 1);
           		    //alert(ServCaseCode);
           		    var ServCaseState = ServItemCaseGrid.getRowColData(getSelNo2-1, 4);
           		    //alert(ServCaseState);
           		    if(ServCaseState!="0")
           		    {
           	         window.open("./LHServCaseExecManageMain.jsp?flag="+ServCaseState+"&arrSelected="+ServCaseCode,"服务事件实施管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
				             //window.open("./LHServExeTrace.jsp?ServPlanNo="+ServPlanNo+"&flag=1","服务执行详情2",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
				          }
				          else
				          {
				          	 alert("此服务事件未进行启动设置，不能查看执行详情!");
				          	 return false;
				          }
				      }
				      else
				      {
				      	 alert("请选择一条服务事件信息!");
				      	 return false;
				      }
			}
		}
	} 
	else
	{
			alert("请选择一条要传输的记录！");    
			return false;
	}
}
function showThree()
{
	var sql = " select d.servcasecode, d.servcasename, "
	     +" (case d.ServCaseState when '0' then '未进行启动设置' when '1' then '服务事件已启动' when '2' then '服务事件已完成'  when '3' then '服务事件失败' else '无' end ),  "
	     +" d.servcasestate "
			 +" from LHServCaseDef d, LHServCaseRela r "
			 +" where d.servcasecode = r.servcasecode "
			 +" and r.servitemno = '"+LHServerDoStatus.getRowColData(LHServerDoStatus.getSelNo()-1,7)+"' "
			 + getWherePart("r.ComID","ComID", "like")
			 ;
	//alert(sql);
	turnPage8.queryModal(sql,ServItemCaseGrid)
}
function showTwo()
{
	var getSelNo = LHServerPlanType.getSelNo();                           
	var ServPlanCode = LHServerPlanType.getRowColData(getSelNo-1, 8);
	var strSql2="select (select servitemname from lhhealthservitem c where  c.servitemcode=a.servitemcode), "
				+"(select  ServItemType  from LHServItem b where b.ServItemNo=a.ServItemNo), '已结束', a.ServItemno "
				+"from   lhservitem a where  1=1 and servplanno = '"+ServPlanCode+"' "
				+"and    a.servitemno not in  (SELECT DISTINCT A.SERVITEMNO FROM LHSERVCASERELA A "
				+"where  A.ServCaseCode in (select distinct d.servcasecode from LHServCaseDef d "
				+"where  D.SERVCASESTATE = '0' OR D.SERVCASESTATE = '1')) "
				+"union "
				+"select (select servitemname from lhhealthservitem c where  c.servitemcode=a.servitemcode), "
				+"(select  ServItemType  from LHServItem b where b.ServItemNo=a.ServItemNo), '未结束', a.ServItemno  "
				+"from   lhservitem a where  1=1 and servplanno = '"+ServPlanCode+"' "
				+"and    a.servitemno in  (SELECT DISTINCT A.SERVITEMNO FROM LHSERVCASERELA A "
				+"where  A.ServCaseCode in (select distinct d.servcasecode from LHServCaseDef d "
				+"where  D.SERVCASESTATE = '0' OR D.SERVCASESTATE = '1')) "
				;
    //alert(strSql2);
    //alert(easyExecSql(strSql2));
	turnPage4.queryModal(strSql2, LHPlanSpecialInfo); 
	var ContNo = LHServerPlanType.getRowColData(LHServerPlanType.getSelNo()-1,3);
	var sql = " select a.Conttype from lccont a where  a.contno ='"+ ContNo+"'";
	fm.all('ContTypeIn').value=easyExecSql(sql);

}
function ShowQuesInfo()
{
	if (LHServerDoStatus.getSelNo() >= 1)
	{
		var getSelNo = LHServerDoStatus.getSelNo();
		var ServItemNO = LHServerDoStatus.getRowColData(getSelNo-1, 7);
		var strSqlNo="select a.Servtaskno from LHQUESIMPORTMAIN a where a.ServItemNo='"+ServItemNO+"' ";
        var taskNo=easyExecSql(strSqlNo);
        if(taskNo!=""&&taskNo!=null&&taskNo!="null")
	    {
	   		window.open("./LHQuesKineMain.jsp?ServTaskNo="+taskNo+"&custtype=6","健康问卷信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	    }
		else
		{
			alert("此服务项目无对应的健康问卷信息,不能进入问卷信息页面!");
			return false;
		}
	}
	else
	{
		alert("请选择一条服务项目信息!");
		return false;
	}
}

function ShowReportInfo()
{
	if (LHServerDoStatus.getSelNo() >= 1)
	{
		var getSelNo = LHServerDoStatus.getSelNo();
		var ServItemNo = LHServerDoStatus.getRowColData(getSelNo-1, 7);
		var strSqlNo="select a.ReportName from LHEvaReport a where a.ServItemNo='"+ServItemNo+"' ";//只考虑一个ServItemNo对应一份评估报告
        var ReportName=easyExecSql(strSqlNo);
        if(ReportName!="" && ReportName!=null && ReportName!="null")
	    {
	   		window.open("../hmpdf/"+ReportName+".pdf","查看评估报告",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
	    }
		else
		{
			alert("此服务项目无对应的健康评估报告!");
			return false;
		}
	}
	else
	{
		alert("请选择一条服务项目信息!");
		return false;
	}
}


function testQuery()
{
	 ContStyle.style.display='none';
   ServeStyle.style.display='';
   ServerStatus.style.display='none';
   InHospitalStyle.style.display='none';
   HealthCheck.style.display='none';
   CustomHealthStatus.style.display='none';
   HealthFilePrint.style.display='none';
   divLHMessSendInfoGrid.style.display='none';
   HealthCheckPrint.style.display='none';
    initLHHealthServPlanGrid();
    initLHContSpecialInfo();
    initLHServerPlanType();
    initLHPlanSpecialInfo();
    initLHServerDoStatus();
    initLHInHospitalMode();
    initLHHealthCheckupStyle();
}
function StatusQuery()
{
	var SqlState = "";
	var colState = "";
	if(fm.all('ServePlanStatusType').value == "1")
	{//未关联的服务项目，存在于0、1状态事件
		colState = "已结束";
		SqlState = " and a.servitemno not in "
			+" (SELECT DISTINCT A.SERVITEMNO FROM LHSERVCASERELA A,LHSERVCASEDEF D "
			+" WHERE  A.SERVCASECODE = D.SERVCASECODE "
			+" AND    (D.SERVCASESTATE = '0' OR D.SERVCASESTATE = '1')) "
			;
	} 
	if(fm.all('ServePlanStatusType').value == "2")
	{//已关联的服务项目，存在于0、1状态事件
		colState = "未结束";
		SqlState = " and a.servitemno in "
			+" (SELECT DISTINCT A.SERVITEMNO FROM LHSERVCASERELA A,LHSERVCASEDEF D "
			+" WHERE  A.SERVCASECODE = D.SERVCASECODE "
			+" AND    (D.SERVCASESTATE = '0' OR D.SERVCASESTATE = '1')) "
			;
	}          
	var strSql=" select a.servitemcode,(select c.servitemname from lhhealthservitem c where c.servitemcode=a.servitemcode),"
             +" (select  ServItemType  from LHServItem b where b.ServItemNo=a.ServItemNo),a.ContNo,"
             +" '"+colState+"','',a.servitemno,a.Customerno,a.Name "
             +" from LHServItem a where 1=1 "
             + getWherePart("a.servitemcode","ServItemCode")
             + getWherePart("a.Customerno","CustomerNo")
             + getWherePart("a.Name","CustomerName","like")
             + getWherePart("a.ContNo","ContNo")   
             +SqlState 
             +getWherePart("a.ComID","ComID", "like")
             ;
   //alert(strSql);
   //alert(easyExecSql(strSql));
   turnPage5.queryModal(strSql, LHServerDoStatus);
}
function ServerStatusQuery()
{
	  ContStyle.style.display='none';
    ServeStyle.style.display='none';
    ServerStatus.style.display='';
    InHospitalStyle.style.display='none';
    HealthCheck.style.display='none';
    CustomHealthStatus.style.display='none';
    HealthFilePrint.style.display='none';
    divLHMessSendInfoGrid.style.display='none';
    HealthCheckPrint.style.display='none';
    initLHHealthServPlanGrid();
    initLHContSpecialInfo();
    initLHServerPlanType();
    initLHPlanSpecialInfo();
    initLHServerDoStatus();
    initLHInHospitalMode();
    initLHHealthCheckupStyle();
}
function HealthStatusQuery()
{
	if(fm.all('CustomerNo').value==""||fm.all('CustomerNo').value=="null"||fm.all('CustomerNo').value==null)
  {
  	alert("请您先选择客户编号!");
  }
  else
	 {
	 ContStyle.style.display='none';
   ServeStyle.style.display='none';
   ServerStatus.style.display='none';
   InHospitalStyle.style.display='none';
   HealthCheck.style.display='none';
   CustomHealthStatus.style.display='';
   HealthFilePrint.style.display='none';
   divLHMessSendInfoGrid.style.display='none';
   HealthCheckPrint.style.display='none';
    initLHHealthServPlanGrid();
    initLHContSpecialInfo();
    initLHServerPlanType();
    initLHPlanSpecialInfo();
    initLHServerDoStatus();
    initLHInHospitalMode();
    initLHHealthCheckupStyle();
   // initLHCustomFamilyDiseasGrid();
   // initLHCustomGymGrid();
    
   //  var strSql="select AddDate,BloodPressLow,BloodPressHigh,"
  //	             +" Stature,Avoirdupois,AvoirdIndex,Smoke,KissCup,SitUp,DiningNoRule,BadHobby,HealthNo,MakeDate,MakeTime "
  	//             +" from LHCustomHealthStatus where healthno = (select  distinct max(healthno) from LHCustomHealthStatus where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  //	             +getWherePart("CustomerNo","CustomerNo")
  	//              ;
  	
  	var strSql1="select distinct testresult from LHCustomTest where inhospitno = (select  distinct max(inhospitno) from LHCustomTest where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  	           +getWherePart("CustomerNo","CustomerNo")
  	           + " and medicaitemcode = '110500009' "
  	           ;		
    var strSql2="select distinct testresult from LHCustomTest where inhospitno = (select  distinct max(inhospitno) from LHCustomTest where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  	           +getWherePart("CustomerNo","CustomerNo")
  	           + " and medicaitemcode = '110500010' "
  	           ;	
    var strSql3="select distinct testresult from LHCustomTest where inhospitno = (select  distinct max(inhospitno) from LHCustomTest where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  	           +getWherePart("CustomerNo","CustomerNo")
  	           + " and medicaitemcode = '110500011' "
  	           ;	
    var strSql4="select distinct testresult from LHCustomTest where inhospitno = (select  distinct max(inhospitno) from LHCustomTest where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  	           +getWherePart("CustomerNo","CustomerNo")
  	           + " and medicaitemcode = '110500012' "
 	           ;	
  	var strSql5="select distinct testresult from LHCustomTest where inhospitno = (select  distinct max(inhospitno) from LHCustomTest where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  	           +getWherePart("CustomerNo","CustomerNo")
  	           + " and medicaitemcode = '110500013' "
  	           ;	
  	var strSql6="select distinct testresult from LHCustomTest where inhospitno = (select  distinct max(inhospitno) from LHCustomTest where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  	           +getWherePart("CustomerNo","CustomerNo")
  	           + " and medicaitemcode = '110500014' "
  	           ;	
	//	var arrLHCustomHealthStatus=easyExecSql(strSql);
	//	alert(arrLHCustomHealthStatus);	
  	    	  	
  	  //  fm.AddDate.value=arrLHCustomHealthStatus[0][0];
  	 // alert(arrLHCustomHealthStatus[3][0]);
  	    fm.BloodPressLow.value=easyExecSql(strSql4)==null?0:easyExecSql(strSql4);
  	    fm.BloodPressHigh.value=easyExecSql(strSql3)==null?0:easyExecSql(strSql3);
  	    fm.Stature.value=easyExecSql(strSql1)==null?0:easyExecSql(strSql1);
  	    fm.Avoirdupois.value=easyExecSql(strSql2)==null?0:easyExecSql(strSql2);
  	    fm.AvoirdIndex.value=easyExecSql(strSql6)==null?0:easyExecSql(strSql6);
  	    //fm.Smoke.value=arrLHCustomHealthStatus[0][6];
  	    //fm.KissCup.value=arrLHCustomHealthStatus[0][7];
  	    //fm.SitUp.value=arrLHCustomHealthStatus[0][8];
  	    //fm.DiningNoRule.value=arrLHCustomHealthStatus[0][9];
  	   // fm.BadHobby.value=arrLHCustomHealthStatus[0][10];
  	    fm.Waistline.value=easyExecSql(strSql5)==null?0:easyExecSql(strSql5);
  	  //  fm.HealthNo.value=arrLHCustomHealthStatus[0][11];
  	  //  fm.MakeDate.value=arrLHCustomHealthStatus[0][12];
  	  //  fm.MakeTime.value=arrLHCustomHealthStatus[0][13];
  	    fm.BloodPress.value=Math.round(((fm.BloodPressHigh.value-fm.BloodPressLow.value)*100)/100);
  	    
  	//    strSql="select a.GymItemCode,b.codename, a.GymTime,a.GymFreque,a.CustomerGymNo "
		//	         +" from LHCustomGym a, ldcode b where a.CustomerNo = '"+fm.all('CustomerNo').value+"'"
		//	         +" and b.codetype='gymitem' and a.GymItemCode=b.code and a.CustomerGymNo = (select  distinct max(a.CustomerGymNo) from LHCustomGym a where CustomerNo = '"+fm.all('CustomerNo').value+"')"
		//	         ;
  	  
  	//     turnPage.queryModal(strSql, LHCustomGymGrid);
  	     
  	//  strSql = " select (select b.codename from ldcode b where  b.codetype='familycode' and b.code = a.Familycode),"
  	// 		 		 +" a.FamilyCode,a.ICDCode, (select c.ICDName from lddisease c where c.ICDCode = a.ICDCode), "
  //	 		 		 +" a.FamilyDiseaseNo   	 from LHCustomFamilyDiseas a  "
  //	         +" where a.CustomerNo = '"+fm.all('CustomerNo').value+"' "
  	//         +" and a.FamilyDiseaseNO = (select  distinct max(a.FamilyDiseaseNO) from LHCustomFamilyDiseas a where CustomerNo = '"+fm.all('CustomerNo').value+"')"
  //	         ;

  //	 turnPage.queryModal(strSql, LHCustomFamilyDiseasGrid);
  	}
  	 
}
function InHospitalQuery()
{
   ContStyle.style.display='none';
   ServeStyle.style.display='none';
   ServerStatus.style.display='none';
   InHospitalStyle.style.display='';
   HealthCheck.style.display='none';
   CustomHealthStatus.style.display='none';
   HealthFilePrint.style.display='none';
   divLHMessSendInfoGrid.style.display='none';
   HealthCheckPrint.style.display='none';
    initLHHealthServPlanGrid();
    initLHContSpecialInfo();
    initLHServerPlanType();
    initLHPlanSpecialInfo();
    initLHServerDoStatus();
    initLHInHospitalMode();
    initLHHealthCheckupStyle();
}
function HospitalQuery()
{
	if(fm.CustomerNo.value==null||fm.CustomerNo.value=="")
	{
		alert("客户号码为必录项，请录入客户号码!");
		return;
	}
	else
	{
		//机构为受理机构、给付确认案件
	   var strSql=" select b.riskcode,a.FeeDate,a.HospitalName,case a.FeeType when '1' then '门诊' when '2' then '住院' else '门诊特殊病' end, "
	             +" (select diseasecode||'-'||diseasename from llcasecure where CaseNo=a.caseno fetch first 1 rows only),sum(a.sumfee),"
	             +" (select nvl(sum(Fee),0) from LLCaseReceipt where caseno=a.caseno and FeeItemCode in ('104','206')),"
	             +" (select nvl(sum(Fee),0) from LLCaseReceipt where caseno=a.caseno and FeeItemCode in ('107','205','112','209')),"
	             +" (select nvl(sum(Fee),0) from LLCaseReceipt where caseno=a.caseno and FeeItemCode in ('102','121','203','204')),"
	             +" (select nvl(sum(Fee),0) from LLCaseReceipt where caseno=a.caseno and FeeItemCode in ('101','202')),"
	             +" (select nvl(sum(Fee),0) from LLCaseReceipt where caseno=a.caseno and FeeItemCode in ('118','222')),"
	             +" nvl(sum(b.realpay),0),b.caseno,a.Hospitalcode,c.customerno"
				 +" from llclaimdetail b,llcase c left join llfeemain a on c.caseno=a.caseno"
				 +" where c.caseno=b.caseno and c.rgtstate in ('11','12')"
				 +getWherePart("c.caseno","Caseno")
	             +getWherePart("c.customerno","CustomerNo") 
	             +getWherePart("c.CustomerName","CustomerName")
	             +getWherePart("c.IDNo","IDNo")             
	             +getWherePart("a.FeeType","InHospitalType") 
				 if(fm.MngCom.value!=""&&fm.MngCom.value!=null)
				 {
				 	strSql+=" and c.Mngcom like '"+fm.MngCom.value+"%'"
				 }
	            // +getWherePart("a.AgentCode","AgentCode")         
	            //+getWherePart("(select name from laagent where agentcode=a.agentcode)","AgentName","like")            
	             strSql+=" group by a.FeeDate,a.HospitalName,a.FeeType,a.caseno,b.caseno,a.Hospitalcode,c.customerno,b.riskcode with ur";
	   turnPage6.queryModal(strSql, LHInHospitalMode);
	  }
}
//就诊详细信息
function DealCheck()
{
		if (LHInHospitalMode.getSelNo() >= 1)
		{     
		    if(LHInHospitalMode.getRowColData(LHInHospitalMode.getSelNo()-1,13)=="")
		    {
				 alert("你选择了空的列，请重新选择!");
				 return false;
		    }
			else
			{			
				var varSrc="&CaseNo="+ LHInHospitalMode.getRowColData(LHInHospitalMode.getSelNo()-1,13);
				var newWindow = OpenWindowNew("../case/FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
				varSrc,"检录","left");
		     }
		} 
		else
		{
				alert("请选择一条要传输的记录！");    
		}
}
function LookScanInfo()
{
    if (LHInHospitalMode.getSelNo() >= 1)
		{  
			  var CustomerNo=LHInHospitalMode.getRowColData(LHInHospitalMode.getSelNo()-1,15);
			  var InHospitNo=LHInHospitalMode.getRowColData(LHInHospitalMode.getSelNo()-1,14);
			  var strSql = "select distinct SERIALNO from LHSCANINFO "
			             +" where RELATIONNO='"+CustomerNo+"' AND RELATIONNO2='"+ InHospitNo+"'";
	      var SERIALNO = easyExecSql(strSql);
	      //alert(SERIALNO);
	      if(SERIALNO!=""&&SERIALNO!="null"&&SERIALNO!=null)
	      {
	          var docid = "";
				    docid = easyExecSql("select distinct docid from es_doc_main where doccode = '"+SERIALNO+"'");
            window.open("./LHTotalInfoInHosptailScan.jsp?DocID="+docid+"&ScanNo=HM20&prtNo="+SERIALNO+"&BussTpye=HM","扫描件录入页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
        }
        else
        {
        	 //submenu.style.display='none';
        	 alert("此客户的就诊情况无扫描件信息!");
        	 return false;
        }
		}
		else
		{
				alert("请选择一条客户信息，才能查看相关的扫描件！");    
		}
    
}
function LookScanTestInfo()
{
	  if (LHHealthCheckupStyle.getSelNo() >= 1)
		{  
			  var CustomerNo=LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,5);
			  //alert(CustomerNo);
			  var InTestNo=LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,4);
			  //alert(InTestNo);
			  var strSql = "select distinct SERIALNO from LHSCANINFO "
			             +" where RELATIONNO='"+CustomerNo+"' AND RELATIONNO2='"+ InTestNo+"'";
	      var SERIALNO = easyExecSql(strSql);
	      //alert(SERIALNO);
	      if(SERIALNO!=""&&SERIALNO!="null"&&SERIALNO!=null)
	      {
	          var docid = "";
				    docid = easyExecSql("select distinct docid from es_doc_main where doccode = '"+SERIALNO+"'");
            window.open("./LHTotalInfoInTestScan.jsp?DocID="+docid+"&ScanNo=HM10&prtNo="+SERIALNO+"&BussTpye=HM","扫描件录入页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
        }
        else
        {
        	 //submenu.style.display='none';
        	 alert("此客户的体检情况无扫描件信息!");
        	 return false;
        }
		}
		else
		{
				alert("请选择一条客户信息，才能查看相关的扫描件！");    
		}
}
function getPrtNo(TempContNo)
{
	var strSql = "select distinct PrtNo from lccont where ContNo='"+TempContNo+"'";
	prtNo = easyExecSql(strSql);
	return prtNo;
}
function HealthCheckQuery()
{
	 ContStyle.style.display='none';
   ServeStyle.style.display='none';
   ServerStatus.style.display='none';
   InHospitalStyle.style.display='none';
   HealthCheck.style.display='';
   HealthFilePrint.style.display='none';
   divLHMessSendInfoGrid.style.display='none';
   HealthCheckPrint.style.display='none';
    initLHHealthServPlanGrid();
    initLHContSpecialInfo();
    initLHServerPlanType();
    initLHPlanSpecialInfo();
    initLHServerDoStatus();
    initLHInHospitalMode();
    initLHHealthCheckupStyle();
}
function healthQuery()
{
	var tempCom = manageCom.length==8?manageCom.substring(0,4):manageCom;
	   var strSql="select InHospitDate,(select HospitName from LDHospital where HospitCode=a.HospitCode) ,"
               +"( case InHospitMode when  '31' then '核保体检' when '32' then  '体检服务'  else '无' end ),InHospitNo , "
               +" a.Customerno , (select b.Name from LDPERSON b where b.Customerno=a.Customerno )"
               +" from LHCustomInHospital a "
               +" where inhospitmode in ('31','32') and (select distinct d.Managecom from ldhospital d where d.hospitcode = a.hospitcode) like '"+tempCom+"%%' "
               + getWherePart("CustomerNo","CustomerNo")
               + getWherePart("InHospitMode","TestModeCode" )
     //        + getWherePart("a.Managecom","ComID", "like")
     //alert(strSql);
     //alert(easyExecSql(strSql));
     turnPage7.queryModal(strSql, LHHealthCheckupStyle);
}
function HealthCheckDetail()
{
		  if (LHHealthCheckupStyle.getSelNo() >= 1)
			{     
				    //alert(LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,5));
					  if(LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,5)=="")
					  {
							  alert("你选择了空的客户信息，请重新选择!");
								return false;
					  }
						else
						{
							   var Customerno=LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,5)
							   var Healthno = LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,4);
						     window.open("./LHCustomTestInputMain.jsp?Customerno="+Customerno+"&Healthno="+Healthno+"","体检详细信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			      }
			} 
			else
			{
					alert("请选择一条要传输的记录！");    
			}
}

function submitFormA()
{
	if(fm.CustomerNo.value == null || fm.CustomerNo.value == "")
	{
			alert("请您先选择客户信息!");
			return false;
	}
	if(fm.StartDate.value == null || fm.StartDate.value == "")
	{
			alert("请输入起始时间!");
			return false;
	}
	if(fm.EndDate.value == null || fm.EndDate.value == "")
	{
			alert("请输入终止时间!");
			return false;
	}
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
  fm.action = "./HealthArchiveRpt.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
 // window.open("./HealthArchiveInput.js","客户健康档案打印",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}
function queryCustomerNo(fmPreName)
{
	fmPreNameBack  = fmPreName;
	var theNo = fmPreName+"No";
	var theName = fmPreName+"Name";

	if(fm.all(theNo).value == "")	
	{  
		var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	if(fm.all(theNo).value != "")	 
	{
		var cCustomerNo = fm.all(theNo).value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	    if (arrResult != null) 
	    {
			fm.all(theNo).value = arrResult[0][0];
			fm.all(theName).value = arrResult[0][1];
			alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    	}
    	else
    	{
    		alert("客户代码为:["+fm.all(theNo).value+"]的客户不存在，请确认!");
    	}
	}	
}
function queryCustomerNo2()
{	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户号码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";

    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else
    {
    	//fm.DiseasCode.value="";
     	alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function afterQuery(arrResult) 
{
	//alert(arrResult);
	var theNo = fmPreNameBack+"No";    
	var theName = fmPreNameBack+"Name";
    if(arrResult!=null) 
    {
		fm.all(theNo).value = arrResult[0][0];
		fm.all(theName).value = arrResult[0][1];
    } 
    else {
    alert("LHMainCustomerHealth.js->afterquery()出现错误");
  }
}
function getCustomerNo(fmPreName)
{
	//alert(fmPreName);
	fmPreNameBack  = fmPreName;
	var theNo = fmPreName+"No";
	var theName = fmPreName+"Name";

	if(fm.all(theNo).value == "")	
	{  
		var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	if(fm.all(theNo).value != "")	 
	{
		var cCustomerNo = fm.all(theNo).value;  //客户代码	
		var strSql = "select distinct AppntNo,AppntName from LCAppnt where AppntNo='" + cCustomerNo +"'"
				   +" union "
				   +" select distinct CustomerNo,Name from LCGrpAppnt where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) 
	    {
			fm.all(theNo).value = arrResult[0][0];
			fm.all(theName).value = arrResult[0][1];
			alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    	}
    	else
    	{
    		//fm.DiseasCode.value="";
    		alert("客户代码为:["+fm.all(theNo).value+"]的客户不存在，请确认!");
    	}
	}	
}
function FileInfoPrint()
{
	 ContStyle.style.display='none';
   ServeStyle.style.display='none';
   ServerStatus.style.display='none';
   InHospitalStyle.style.display='none';
   HealthCheck.style.display='none';
   CustomHealthStatus.style.display='none';
   HealthFilePrint.style.display='';
   divLHMessSendInfoGrid.style.display='none';
   HealthCheckPrint.style.display='none';
}
function submitFormRpt()
{
	if(fm.CustomerNo.value == null || fm.CustomerNo.value == "")
	{
			alert("请您先选择客户信息!");
			return false;
	}
	if( CustomerExit()== false )
	{return false;}
	if(fm.StartDate.value == null || fm.StartDate.value == "")
	{
			alert("统计起期信息不许为空!");
			return false;
	}
	if(fm.EndDate.value == null || fm.EndDate.value == "")
	{
			alert("统计止期信息不许为空!");
			return false;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./HealthArchiveRpt.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
}
                                               
//修改保存时校验客户是否存在
function CustomerExit()
{
	var cCustomerNo = fm.CustomerNo.value;  //客户代码
  var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
  var arrResult = easyExecSql(strSql);
  //	  	alert(arrResult);
  if (arrResult == null) 
  {
   	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
   	  return false;
  }
}   
function TestPrint()
{
  if(fm.CustomerNo.value == null || fm.CustomerNo.value == "")
	{
			alert("请您先选择客户信息!");
			return false;
	}
	if( CustomerExit()== false )
	{return false;}
	if(fm.StartDate.value == null || fm.StartDate.value == "")
	{
			alert("统计起期信息不许为空!");
			return false;
	}
	if(fm.EndDate.value == null || fm.EndDate.value == "")
	{
			alert("统计止期信息不许为空!");
			return false;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./HealthArchiveRptTest.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
}  
function ServPtint()
{
  if(fm.CustomerNo.value == null || fm.CustomerNo.value == "")
	{
			alert("请您先选择客户信息!");
			return false;
	}
	if( CustomerExit()== false )
	{return false;}
	if(fm.StartDate.value == null || fm.StartDate.value == "")
	{
			alert("统计起期信息不许为空!");
			return false;
	}
	if(fm.EndDate.value == null || fm.EndDate.value == "")
	{
			alert("统计止期信息不许为空!");
			return false;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./HealthArchiveRptServ.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
}  
function LHMessSendQuery()
{
	divLHMessSendInfoGrid.style.display='';
	ContStyle.style.display='none';
  ServeStyle.style.display='none';
  ServerStatus.style.display='none';
  InHospitalStyle.style.display='none';
  HealthCheck.style.display='none';
  CustomHealthStatus.style.display='none';
  HealthFilePrint.style.display='none';
  HealthCheckPrint.style.display='none';
}
function LHMessSendInfo()
{
	  var ServTaskNo="";
		if(fm.all('LHMStartDate').value != ""&&fm.all('LHMEndDate').value != "")
  	 	{
			   ServTaskNo =" and a.ServTaskNo in ( select distinct s.ServTaskNo from  LHCaseTaskRela  s"
			              +"  where  s.TaskFinishDate "
			              +"  between  '"+fm.all('LHMStartDate').value+"' and  '"+fm.all('LHMEndDate').value+"')";	
  	 	}
	   var strSql9="select distinct a.CustomerNo,(select v.Name from LDPerson v where v.CustomerNo=a.CustomerNo) ,"
               +" a.ContNo , "
               +" (select b.TaskFinishDate from LHCaseTaskRela b where b.ServTaskNo=a.ServTaskNo ),"
               +" a.HmMessCode ,"
               +"(select n.HmMessName from LHMessManage n where n.HmMessCode=a.HmMessCode),"
               +" a.ReMarkExp,'' "
               +" from LHMessSendMg a "
               +"where 1=1 "
               + getWherePart("a.CustomerNo","CustomerNo")
               + getWherePart("a.Managecom","ComID", "like")
               + getWherePart("a.HmMessCode","HmMessCode")
               +ServTaskNo
               ;
     //alert(strSql);
     //alert(easyExecSql(strSql9));
     turnPage9.queryModal(strSql9, LHMessSendMgGrid);
} 
function LHHealthCheck()
{
	divLHMessSendInfoGrid.style.display='none';
	ContStyle.style.display='none';
  ServeStyle.style.display='none';
  ServerStatus.style.display='none';
  InHospitalStyle.style.display='none';
  HealthCheck.style.display='none';
  CustomHealthStatus.style.display='none';
  HealthFilePrint.style.display='none';
  HealthCheckPrint.style.display='';
}   
function ContHealthCheck()
{

	if (LHHealthCheckupStyle.getSelNo() >= 1)
	{  
			  var CustomerNo=LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,5);
			  var InTestNo=LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,4);
			  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action="./HealthContCheckRpt.jsp";
	      fm.fmtransact.value = "PRINT";
	      fm.target = "f1print";
	      fm.submit();
	      showInfo.close();
  }
	else
	{
				alert("请选择一条客户信息，才能查看相关的体检报告！");    
	}
} 
function showFive()
{
	    var CustomerNo=LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,5);
			fm.all('CustomerNoH').value=CustomerNo;
			var InTestNo=LHHealthCheckupStyle.getRowColData(LHHealthCheckupStyle.getSelNo()-1,4);
			fm.all('InHospitNoH').value=InTestNo;
}   
//查询业务员代码
function queryAgent()
{
    if(fm.all('MngCom').value == "")
    {
        alert("请先录入管理机构信息！"); 
        return ;
    }

    if(fm.all('AgentCode').value == "")
    {          
        var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('MngCom').value
            + "&SaleChnl=" + fm.all('SaleChnl').value
            + "&AgentCom=" + fm.all('AgentCom').value
            + "&branchtype=null" ;
        var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    if(fm.all('AgentCode').value != "")
    {
        var cAgentCode = fm.AgentCode.value;  //保单号码	
        var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('MngCom').value+"'";
        var arrResult = easyExecSql(strSql);

        if (arrResult != null)
        {
            fm.AgentName.value = arrResult[0][1];
            alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentName.value="";
            alert("代码为:[" + fm.all('AgentCode').value + "]的业务员不存在，请确认!");
        }
    }
}
function queryAgent2()
{
	if(fm.all('MngCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==10 )	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('MngCom').value+"'";
    	var arrResult = easyExecSql(strSql);

    	if (arrResult != null) {
      	fm.AgentName.value = arrResult[0][1];
      	alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
   	 	}
    	else{
     	fm.AgentName.value="";
     	alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
    	 }
	}	
}   
//返回查询业务员代码   
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  	fm.AgentName.value = arrResult[0][5];
  }
}
//病例档案
function FileCase()
{
}
                   