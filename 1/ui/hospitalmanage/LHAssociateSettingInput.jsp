<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHAssociateSettingInput.jsp
//程序功能：关联设置信息管理显示页面
//创建日期：2006-03-01 9:40:10
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHAssociateSettingInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHAssociateSettingInputInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./LHAssociateSettingSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHContraAssoSettingGrid);">
    		</td>
    		<td class= titleImg>
        		 关联设置
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHContraAssoSettingGrid" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNo verify="合同编号|NOTNULL&len<=50" readonly>
    </TD>
    <TD  class= title>
      合同名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraName verify="合同名称|NOTNULL&len<=100" readonly>	
    </TD>
    <TD class= title>
    	合作机构名称    
    </TD>
    <TD  class= input>
    	<Input type=hidden name= HospitCode>
    	<Input class= 'common' name=HospitName verify="合作机构名称|len<=100" readonly>	
    </TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      合同责任项目类型
    </TD>
    <TD  class= input>
      <Input type=hidden name=ContraType>
      <Input class= 'common' name=ContraType_ch verify="合同责任项目类型|len<=100" readonly>	
    </TD>
    <TD  class= title>
      合同责任项目名称
    </TD>
    <TD  class= input>
      <Input type=hidden name=ContraNowName>
      <Input class= 'common' name=ContraNowName_ch verify="合同责任项目名称|len<=100" readonly>	
    </TD>
    <TD  class= title>
      责任当前状态
    </TD>
    <TD  class= input>
      <Input type=hidden name=ContraNowState>
      <Input class= 'common' name=ContraNowState_ch verify="责任当前状态|len<=4" readonly>	
    </TD>

   </TR>
</table>
    </Div>
      <table>
		  	<tr>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHContraAssoSettingGrid1);">
		  		</td>
		  		<td class=titleImg>
		  			 信息
		  		</td>
		  		<td style="width:690px;">
		  		</td>
		  		<td class=common>
				        <input type=button class=cssButton style="width:110px;" name=subLine value="减去选中行" onclick="deleteClick();">
	        </td>
		  		<TD align ="right">
							<Input type=button class=cssbutton style="width:110px;" name=getAssociateSetting value="关联责任设置"  onclick="getUnitInfo();">
					</TD>
		  	</tr>

		  </table>
	
		  <!-- 信息（列表） -->
		<Div id="divLHContraAssoSettingGrid1" style="display:''">
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHContraAssoSettingGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
		</div>
   <Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input  type = hidden name ="ContraItemNo">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
    <input type = hidden  name ="ContraItemNo2">
    <input type = hidden  name ="Contrano2">
   
    <input type = hidden name = HiddenBtn>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<SCRIPT language=javascript>
	function abcde()
	{
		alert("2222");
	}
	
</SCRIPT>
</html>
