//该文件中包含客户端需要处理的函数和事件

//程序名称：HMDisCaseManage.js
//程序功能：疾病案例管理
//创建日期：2010-3-4
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

var CustomerInfoTurnPage = new turnPageClass(); 

//目标客户提取按钮
function custumerFetch()
{
	//如果录入证件号码；理赔案件号，保单号 其中一个，那么就诊起始/结束时间可以不录入。否则必须录入
	if(((fm.IDType2.value == null || fm.IDType2.value=="") || (fm.IDNo2.value==null || fm.IDNo2.value==""))
			&&(fm.CaseNo2.value == null || fm.CaseNo2.value == "")
			&&(fm.CustomerNo2.value == null || fm.CustomerNo2.value == "")) {
		if(fm.VisFinStartDate2.value == null || fm.VisFinStartDate2.value == ""
			|| fm.VisFinEndDate2.value == null || fm.VisFinEndDate2.value == "") {
			alert("证件号码，理赔案件号，客户号，都没有录入，所以必须录入就诊结束起始日期！");
			return false
		}
	}
	
	//查询明细
	queryCustomerInfoGrid();	
}

//添加到客户档案按钮
function custArchives( )
{
	var tRow=CustomerInfoGrid.getSelNo();
	if(tRow==0)	{
   	
		alert("请选择一条客户信息后，再进入！");
		return false;
	}
	var tCustNo = CustomerInfoGrid.getRowColData(tRow-1,1);
	var tIDType = CustomerInfoGrid.getRowColData(tRow-1,3);
	var tIDNo = CustomerInfoGrid.getRowColData(tRow-1,5);
	if(tCustNo == null || tCustNo == "") {
		alert("您不能选择一条空的记录!");
		return false;
	}
	
	window.open("./HMAddCustDocDetailFrame.jsp?CustomerNo=" + tCustNo + "&IDType=" + tIDType + "&IDNo=" + tIDNo);
}

function queryCustomerInfoGrid()
{
   //管理机构
   var manComCondition = "";
   if(fm.ManageCom2.value != null && fm.ManageCom2.value != "") {
   		manComCondition = " and exists( select 1 from LCCont aa where aa.InsuredNo = a.InsuredNo"
   							+" and aa.ManageCom like '" + fm.ManageCom2.value + "%')";
   }
   
   //销售渠道
   var channelCondition = "";
   if(fm.ChannelType.value != null && fm.ChannelType.value != "") {
   		if(fm.ChannelType.value == "3") {  //银行渠道
   			channelCondition = " and exists( select 1 from  lcpol aa "
   					+" where aa.InsuredNo = a.InsuredNo and  aa.salechnl in('04','13')) " ;
   		}
   		if(fm.ChannelType.value == "2") { //个险
   			channelCondition = " and exists (select 1 from lcpol aa "
   					+" where aa.InsuredNo = a.InsuredNo and aa.salechnl not in('04','13') "
   						+" and aa.grpcontno='00000000000000000000') " ;
   		}
   		
   		if(fm.ChannelType.value == "1") { //团险
   			channelCondition = " and exists (select 1 from lcpol aa "
   					+" where aa.InsuredNo = a.InsuredNo and aa.salechnl not in('04','13') "
   						+" and aa.grpcontno <> '00000000000000000000') " ;
   		}
   }
   
   //投保人
   var appntCondition = "";
   if(fm.AppntName2.value != null && fm.AppntName2.value != "" ) {
   		appntCondition = " and exists (select 1 from lccont aa "
   					+" where aa.InsuredNo = a.InsuredNo and aa.AppntName like '%" 
   											+ fm.AppntName2.value + "%') ";
   }
   //就诊结束时间
   var visFinDateCondition = "";
   if(fm.VisFinStartDate2.value !== null && fm.VisFinStartDate2.value != ""
   		&& fm.VisFinEndDate2.value != null && fm.VisFinEndDate2.value != "") {
   		visFinDateCondition = " and exists(select 1 from LLFeeMain bb where bb.caseno = b.caseno "
   								+" and bb.FeeDate between '"+ fm.VisFinStartDate2.value +"' and '"
   								 + fm.VisFinEndDate2.value +"') ";
   }
   
   //理赔病种
   var disCondition = "";
   if(fm.DiseaseCode2.value != null && fm.DiseaseCode2.value != "") {
   		disCondition = " and exists (select 1 from LLCaseCure cc where b.caseno = cc.caseno and "
   							+"cc.DiseaseCode='"+ fm.DiseaseCode2.value +"' )";
   }
   
   //最近一年医疗费用理赔金额
   var newYearCureFeeCondition = "";
   if(fm.HCostClaimFeeCode2.value != null && fm.HCostClaimFeeCode2.value !="") {
   		newYearCureFeeCondition =
   			" and exists(select 1 from LLCUSTOMERCLAIM dd where dd.CustomerNo = a.InsuredNo "
   				+" and dd.Realpay >= "+ fm.HCostClaimFee.value*1 +")";
   }
   
   //疾病身故风险保额
   var deathAmntCondition = "";
   if(fm.DisDieAmountType2.value != null && fm.DisDieAmountType2.value != "" ) {
   		deathAmntCondition =
   			" and exists(select 1 from LLINSUREDRISKAMNT ee where ee.InsuredNo = a.InsuredNo "
   				+" and ee.Deathamnt >="+ fm.DisDieAmount.value*1 +")";
   }
   
   //疾病定额给付风险保额
   var disAmntCondition = "";
   if(fm.DisRationType2.value != null && fm.DisRationType2.value != "" ) {
   		disAmntCondition =
   			" and exists(select 1 from LLINSUREDRISKAMNT ff where ff.InsuredNo = a.InsuredNo "
   				+" and ff.Diseaseamnt >="+ fm.DisRationAmount.value*1 +")";
   }
   
   var tSQL = 
   		" select distinct a.insuredno, "
	               +" a.name, "
	               +" a.idtype, "
	                +" (select CodeName "
	                   +" from ldcode "
	                  +" where codetype = 'idtype' "
	                    +" and code = a.idtype), "
	                +" a.idno, "
	                +" coalesce((select xa.realhospdate from LLCUSTOMERCLAIM xa where xa.customerno = a.InsuredNo),0), "
	                +" coalesce((select xa.sumfee from LLCUSTOMERCLAIM xa where xa.customerno = a.InsuredNo),0), "
	                +" coalesce((select xa.realpay from LLCUSTOMERCLAIM xa where xa.customerno = a.InsuredNo),0), "
	                +" coalesce((select xa.deathamnt from LLINSUREDRISKAMNT xa where xa.InsuredNo = a.InsuredNo),0), "
	                +" coalesce((select xa.diseaseamnt from LLINSUREDRISKAMNT xa where xa.InsuredNo = a.InsuredNo),0) "
	                
	    +" from lcinsured a left outer join llcase b on b.CustomerNo = a.InsuredNo "
	    +" where 1=1 "
	    + manComCondition
	    + channelCondition
		+ appntCondition
		+ visFinDateCondition
		+ disCondition
		+ newYearCureFeeCondition
		+ deathAmntCondition
		+ disAmntCondition
		+" and not Exists( select 1 from HMCustDocInfo where CustomerNo=a.InsuredNo) "
		+ getWherePart("a.name","Customer2","like")
		+ getWherePart("a.InsuredNo","CustomerNo2")
	 	+ getWherePart("a.idtype","IDType2")
	 	+ getWherePart("a.idno","IDNo2")
	 	+ getWherePart("b.caseno","CaseNo2")
	 	+ " with ur ";
   CustomerInfoTurnPage.queryModal(tSQL,CustomerInfoGrid);  		
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

