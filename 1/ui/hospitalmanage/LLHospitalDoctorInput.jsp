<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="./LLHospitalDoctorInit.jsp"%>
<SCRIPT src="./LLHospitalDoctorInput.js"></SCRIPT>

</head>
<body  onload="initForm();initElementtype();" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./LLHospitalSave.jsp" method="post" name="fm" target="fraSubmit">
    <table>
	   	  <tr>
	        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospital);"></td>
	    	<td class= titleImg>查询条件</td>
	      </tr>
    </table>
    
	<div  id= "divHospital" style= "display: ''">
	<table  class= common>
	    <tr  class= common> 
		      <td  class= title> 
			       管理机构
			  </td>
			  <td  class=input> 
				   <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName>
			  </td>         
	          <td  class= title> 
	               统计起期
	          </td>
	          <td  class= input> 
	         	 <input class= "coolDatePicker" dateFormat="short" name="StartDate" verify="受理时间止期|NOTNULL" >
	          </td>
	          <td  class= title> 
              	   统计止期
              </td>
	          <td  class= input> 
	        	 <input class= "coolDatePicker" dateFormat="short" name="EndDate" verify="受理时间止期|NOTNULL" >
	          </td>
	    </tr>    
     </table>
     </div>
     
     <center>
		<table  class= "common">
		    <tr class= "common" align="left"> 
			    <td class=button>
	         	  <input class=cssButton name="querybutton" VALUE="查  询"  TYPE="button" onclick="queryClick();">
	         	</td>  
		    </tr>    
	    </table>
	 </center>
	 <br><hr width="90%" align="left">
     <table>
	     <tr class="common">
	         <td class="common">
	               <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospitalInfo);">
	         </td> 
	         <td class=titleImg>
	               医护信息
	         </td>
	     </tr>
     </table>
     <div  id= "divHospitalInfo" style= "display: ''">
     	<span id="spanDataGrid"></span>
     	<center>
     	<input value="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
        <input value="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
        <input value="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
        <input value="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
        </center>
     </div>
     <br><hr width="90%" align="left">
     <table>
	     <tr class="common">
	         <td class="common">
	               <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,HospitalDetail);">
	         </td> 
	         <td class=titleImg>
	         		医护信息明细
	         </td>
	     </tr>
     </table>
	 <div  id= "HospitalDetail" style= "display: ''">
	 	<span id="spanDataTable"></span>
	 	<center>
	 	<input value="首  页" TYPE=button onclick="turnPage1.firstPage();" class="cssButton">
        <input value="上一页" TYPE=button onclick="turnPage1.previousPage();" class="cssButton">
        <input value="下一页" TYPE=button onclick="turnPage1.nextPage();" class="cssButton">
        <input value="尾  页" TYPE=button onclick="turnPage1.lastPage();" class="cssButton">
        </center>
	 </div>
	 <br>
	 	<table>
		     <tr class="common">
		         <td class="common">
		               <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProblem);">
		         </td> 
		         <td class=titleImg>
		               问题描述
		         </td>
		     </tr>
	     </table>
	     <div  id= "divProblem" style= "display: ''">
		 	<center>
				<table  class= "common">
				    <tr class= "common"> 
				    	<td class="common" align="left" width="100%">
			             	<textarea class="common" name="ProblemDetail" cols="120" rows="4">
			             	</textarea>
			            </td> 
				    </tr>    
			    </table>
		    </center>
		</div>
		<br>
		 <table>
		     <tr class="common">
		         <td class="common">
		               <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAction);">
		         </td> 
		         <td class=titleImg>
		               采取动作及结果
		         </td>
		     </tr>
		 </table>
		 <div  id= "divAction" style= "display: ''">
		 	<center>
				<table  class= "common">
				    <tr class= "common"> 
				    	<td class="common" align="left" width="100%">
			             	<textarea class="common" name="Action" cols="120" rows="4" value="">
			             	</textarea>
			            </td> 
				    </tr>    
			    </table>
		    </center>
		 </div>
		 <table>
		 	<tr class="common">
			 	<td class=button width="10%">
			 		<input class=cssButton name="QueryScan" VALUE="查询扫描件"  TYPE=button onclick="queryScan();">
	            </td>
		 	</tr>
		 </table>
	 <!--input type="hidden" id="Action" name="Action" value=""-->
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	 <input type="hidden" id="queryscan" name="queryscan" value="">
  </form>
</body>
</html>