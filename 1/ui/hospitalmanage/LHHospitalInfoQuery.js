var turnPage = new turnPageClass();
var turnPage5 = new turnPageClass();
var turnPage6 = new turnPageClass();
var turnPage7 = new turnPageClass();

var showInfo;

function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

// 查询按钮
function easyQueryClick() 
{
	var LEN;
	if( manageCom.length == 8)  LEN = 4;
	else{LEN = manageCom.length;}
	var SpecialCode="";
	if(fm.all('SpecialCode').value != "")
	{
		SpecialCode =" and a.HospitCode in ( select distinct c.HospitCode from  LDHospitalInfoIntro c where   c.SpecialName = '"+fm.all('SpecialCode').value+"')";	
	}
	var strSql = "select a.ManageCom, a.HospitCode, a.HospitName, (select codename  from ldcode where codetype = 'hospitalclass' and code = a.LevelCode), (case when a.CommunFixFlag = '1' then '是' else '否' end), (select codename from ldcode where codetype = 'llhospiflag' and code = a.AssociateClass), (case when b.ContraState = '1' then '有效' when b.ContraState is null then '' else  '无效' end), b.IdiogrDate, b.ContraEndDate "
		+ ", a.HospitalType,b.ContraState from ldhospital a left join LHGroupCont b on  (a.HospitCode = b.HospitCode " ;
	
	if(fm.ContFlag.value=='1' || fm.ContFlag.value=='2')
	{
		strSql = strSql + " and b.ContraState='"+fm.ContFlag.value+"'";
	}
		
	strSql = strSql + ") where  a.AreaCode like '"+fm.AreaCodeQ.value+"%%' "	    
	    + getWherePart("a.HospitCode", "HospitCode")
	    + getWherePart("a.HospitName", "HospitName")
	    + getWherePart("a.Operator", "Operator")
	    + getWherePart("a.ModifyDate", "LastModifyDate")    
	    + getWherePart("a.LevelCode", "LevelCodeQ") 
	    + getWherePart("a.AssociateClass","AssociateClassQ") 
	    + SpecialCode
	    + getWherePart("a.HospitalType","HospitalType");
	if(fm.MngCom.value!=null && fm.MngCom.value!="")
	{
	    strSql = strSql+" and a.managecom like '"+fm.MngCom.value+"%%'"  ;
	}    
  	strSql = strSql+" order by b.IdiogrDate "  ; 
  		  	
	turnPage5.queryModal(strSql, LDHospitalGrid);
	
}



//caosg 医院信息查询
function HospitalInfoQuery()
{
	var index = LDHospitalGrid.getSelNo ()
	if(index==0)
	{
		alert("请选择查询到得记录！");
		return false;
	}
	LHDutyInfo1.style.display='';	
	LHGroupCont1.style.display='';	
	ComHospital1.style.display='';
	MedicalInstrument1.style.display='';				
	BasicInfo.style.display='';
	BasicInfo1.style.display='';
	MedicalInstrument.style.display='';
	ComHospital.style.display='';
	LHGroupCont.style.display='';
	LHDutyInfo.style.display='';
		
	LHInhospitalInfo.style.display='none';
	divPage.style.display='none';		

	BasicInfoQuery();
	MedicalQuery();
	CooperateQuery();

	ContQuery();	
	DutyItemQuery();
}

//基础信息
function BasicInfoQuery()
{
	var index = LDHospitalGrid.getSelNo ()-1;

	var sql="select ManageCom,Address,Phone,ZipCode,WebAddress,Route,CommunFixFlag,AdminiSortCode,EconomElemenCode,LevelCode,BusiTypeCode,AssociateClass from LDHospital a where 1=1 "
	        +"  and a.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
	        +"' and a.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
	        +"' and a.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'";
	        
	var re = easyExecSql(sql);
	//alert(re);
	fm.all('ManageCom').value = easyExecSql("select codename from ldcode where codetype = 'station' and code = '"+re[0][0]+"'");;
	fm.all('Address').value = re[0][1];
	fm.all('Phone').value = re[0][2];
	fm.all('ZipCode').value = re[0][3];
	fm.all('WebAddress').value = re[0][4];
	fm.all('Route').value = re[0][5];
	fm.all('CommunFixFlag').value = re[0][6]=="1"?"社保定点医院":"社保非定点医院";
	var b=easyExecSql("select codename from ldcode where codetype = 'hmadminisortcode' and code = '"+re[0][7]+"'");
	fm.all('AdminiSortCode').value = b==null?"":b;
	var c=easyExecSql("select codename from ldcode where codetype = 'hmeconomelemencode' and code = '"+re[0][8]+"'");
	fm.all('EconomElemenCode').value = c==null?"":c;
	var d=easyExecSql("select codename from ldcode where codetype = 'hospitalclass' and code = '"+re[0][9]+"'");
	fm.all('LevelCode').value = d==null?"":d;
	var a=easyExecSql(" select codename from ldcode where codetype ='hmbusitype' and code = '"+re[0][10]+"'")
	fm.all('BusiTypeCode').value = a==null?"":a;
	var f=easyExecSql("select codename from ldcode where codetype = 'llhospiflag' and code = '"+re[0][11]+"'");
	fm.all('AssociateClass').value = f==null?"":f;

}

//医 疗 资 源
function MedicalQuery()
{
	
	var index = LDHospitalGrid.getSelNo ()-1;
	
	var sql="select SuperiorNo,InterNo,ElementaryNo,TotalNo,BedAmount,PatientPerDay,OutHospital from LDHospital a where 1=1 "
	        +"  and a.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
	        +"' and a.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
	        +"' and a.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'";
	        
	var re = easyExecSql(sql);
	fm.all('BedAmount').value = re[0][4];
	fm.all('PatientPerDay').value = re[0][5];
	fm.all('OutHospital').value = re[0][6];
	var sql="select (select d.codename from ldcode d where d.codetype='lhspecname'and d.code=a.SpecialName),"
	        +" a.SpecialIntro ,a.SpecialName,a.Flowno from LDHospitalInfoIntro a,LDHospital b where a.HospitCode=b.HospitCode and a.SpecialClass='SpecSecDia'"
	        +"  and b.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
	        +"' and b.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
	        +"' and b.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'";

	turnPage.pageLineNum = 100;
	turnPage.queryModal(sql, SpecSecDiaGrid);
	var sql="select a.SpecialName,a.SpecialIntro from LDHospitalInfoIntro a,LDHospital b where a.HospitCode=b.HospitCode and a.SpecialClass='Instrument'"
	        +"  and b.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
	        +"' and b.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
	        +"' and b.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'";
	        
	turnPage.queryModal(sql, InstrumentGrid);

}

//合作级别
function CooperateQuery()
{
	var index = LDHospitalGrid.getSelNo ()-1;
		
	var sql="select a.AssociatClass,a.StartDate,a.EndDate from LDComHospital a,LDHospital b where a.HospitCode=b.HospitCode "
	        +"  and b.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
	        +"' and b.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
	        +"' and b.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'";

	turnPage.queryModal(sql, ComHospitalGrid);
}

//合同信息
function ContQuery()
{
	var index = LDHospitalGrid.getSelNo ()-1;

	var sql="select a.ContraNo,a.ContraName,a.IdiogrDate,a.ContraBeginDate,a.ContraEndDate,"
        	+" (case when a.ContraState = '1' then '有效' else '无效' end),'','','', (select d.CodeName from LDCode d where d.Code=c.contratype and d.Codetype='hmdeftype'), "
        	+" (select e.Dutyitemname from ldcontraitemduty e where e.Dutyitemcode=c.DutyItemCode),(case when c.DutyState = '1' then '有效' else '无效' end), "
        	+" c.Dutylinkman, c.DutyContact , case when (select distinct d.ContraItemNo from LHContraAssoSetting d where c.ContraItemNo=d.ContraItemNo) is null then '无关联' else '有关联' end "
        	
        	+" from LHGroupCont a,LDHospital b ,LHContItem c where a.HospitCode=b.Hospitcode and  a.ContraNo=c.ContraNo"
	        +"  and b.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
	        +"' and b.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
	        +"' and b.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'";

	turnPage.queryModal(sql, LHGroupContGrid);	
		
}

//责任信息
function DutyItemQuery()
{
	var index = LDHospitalGrid.getSelNo ()-1;
	
	initDutyDoneState();
	var sql="select (select c.CodeName from LDCode c where c.Code=a.contratype and c.Codetype='hmdeftype'),"
			+"(select e.Dutyitemname from ldcontraitemduty e where e.Dutyitemcode=a.DutyItemCode),"
			+" case when a.DutyState = '1' then '有效' else '无效' end  ,"
			+"b.ContraNo,b.ContraName,"
			+" case when (select distinct f.ContraItemNo from LHContraAssoSetting f where f.ContraItemNo=a.ContraItemNo) is null then '无关联' else '有关联' end ,"
			+"( select count(ServTaskno) from LHEXECFEEBALANCE f where f.Contraitemno=a.DutyItemCode "
			+" and f.Contrano=b.Contrano and f.Hospitcode=d.Hospitcode  and f.ServPlanno=a.ContraItemno "
			+" and f.Servtaskno in (select k.ServTaskno from LHCaseTaskRela k where k.Servtaskstate='3')"
			+" ),"
			+" ( select count(e.ServTaskno) from LHFEECHARGE e,LHEXECFEEBALANCE f  "
			+" where e.FeeTaskExecno=f.TaskExecno and  f.Contraitemno=a.DutyItemCode and "
			+" f.Contrano=b.Contrano and f.Hospitcode=d.Hospitcode  and f.ServPlanno=a.ContraItemno "
			+" and f.ServTaskno in (select k.ServTaskno from LHCaseTaskRela k where k.ServTaskAffirm='2')"
			+" ),"
			+" ( select value(Sum(e.FeePay),0) from LHFEECHARGE e,LHEXECFEEBALANCE f "
			+" where e.FeeTaskExecno=f.TaskExecno and  f.Contraitemno=a.DutyItemCode and "
			+" f.Contrano=b.Contrano and f.Hospitcode=d.Hospitcode  and f.ServPlanno=a.ContraItemno "
			+" and f.ServTaskno in (select k.ServTaskno from LHCaseTaskRela k where k.ServTaskAffirm='2')"
			+ " ) ,a.ContraItemNo ,''"
			+" from LHContItem a,LHGroupCont b, LDHospital d where a.ContraNo=b.ContraNo and b.HospitCode=d.HospitCode "
	        +"  and d.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
	        +"' and d.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
	        +"' and d.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'";

	turnPage.queryModal(sql, LHDutySpecialInfo);
}

//就 诊 情 况
function InHospitalQuery()
{
	var index = LDHospitalGrid.getSelNo ()	
	
	if (index==0)
	{
		  alert("请选择查询的记录!");
	}
	else
	{
		BasicInfo.style.display='none';
		MedicalInstrument.style.display='none';
		ComHospital.style.display='none';
		LHGroupCont.style.display='none';
		LHDutyInfo.style.display='none';
		LHInhospitalInfo.style.display='';
		LHInhospitalInfo1.style.display='';
		divPage.style.display='none';
	}
}



function mainQuery()
{
	var index = LDHospitalGrid.getSelNo ()-1;
	
	var sql="select a.ContraNo,a.ContraName,a.IdiogrDate,a.ContraBeginDate,a.ContraEndDate, "
        	+" (case when a.ContraState = '1' then '有效' else '无效' end),'','','', (select d.CodeName from LDCode d where d.Code=c.contratype and d.Codetype='hmdeftype'), "
        	+" (select e.Dutyitemname from ldcontraitemduty e where e.Dutyitemcode=c.DutyItemCode),(case when c.DutyState = '1' then '有效' else '无效' end), "
        	+" c.Dutylinkman, c.DutyContact , case when (select distinct d.ContraItemNo from LHContraAssoSetting d where c.ContraItemNo=d.ContraItemNo) is null then '无关联' else '有关联' end "        	
        	+" from LHGroupCont a,LDHospital b ,LHContItem c where a.HospitCode=b.Hospitcode and  a.ContraNo=c.ContraNo"
		
			+ getWherePart("a.ContraState","ContraState")
			+" and b.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
			+"' and b.HospitCode='"+LDHospitalGrid.getRowColData(index,2)+"'";

	turnPage.queryModal(sql, LHGroupContGrid);
	//initGroupContItemGrid();
}

function showOne1()
{
	var getSelNo = LHDutySpecialInfo.getSelNo();
	
	var ServItemCode = LHDutySpecialInfo.getRowColData(getSelNo-1, 2);

	var ContraItemNo = LHDutySpecialInfo.getRowColData(getSelNo-1, 10);//责任号

	var Contno = LHDutySpecialInfo.getRowColData(getSelNo-1, 4);//合同号

	var ServTaskno = LHDutySpecialInfo.getRowColData(getSelNo-1, 11);//服务任务号码
	
	var index = LDHospitalGrid.getSelNo ()-1;

	if(ServTaskno!=""&&ServTaskno!=null&&ServTaskno!="null")
	{
  	    var sql = " select distinct (select name from LDperson p where p.Customerno=a.Customerno),"
              +" (select t.TaskFinishDate from LHCaseTaskRela t where t.ServTaskno=a.ServTaskno),"
              +" ( select  l.codename  from ldcode l ,LHTASKCUSTOMERRELA t where l.code=t.Taskexecstate and  l.codetype='taskexecstuas' and   t.TaskExecno=a.TaskExecno),"
              +" f.FeeNormal,f.FeePay from LHEXECFEEBALANCE a ,LHFEECHARGE f where f.FeeTaskExecno=a.TaskExecno"
              +" and a.ServPlanno='"+ContraItemNo+"'"
              +" and a.Contrano='"+Contno+"'"
              +" and a.ServTaskno='"+ServTaskno+"'"
              +" and a.HospitCode='"+LDHospitalGrid.getRowColData(index,2)+"'";
			        
		turnPage6.queryModal(sql, DutyDoneState);
	}
	if(ServTaskno==""||ServTaskno==null||ServTaskno=="null")
	{
   	     var sql = " select distinct (select name from LDperson p where p.Customerno=a.Customerno),"
	              +"(select t.TaskFinishDate from LHCaseTaskRela t where t.ServTaskno=a.ServTaskno),"
	              +"  ( select  l.codename  from ldcode l ,LHTASKCUSTOMERRELA t where l.code=t.Taskexecstate and  l.codetype='taskexecstuas' and   t.TaskExecno=a.TaskExecno),"
	              +" f.FeeNormal, f.FeePay"
	              +" from LHEXECFEEBALANCE a ,LHFEECHARGE f where f.FeeTaskExecno=a.TaskExecno"
	              +" and a.ServPlanno='"+ContraItemNo+"' and a.Contrano='"+Contno+"' "
	              +" and a.HospitCode='"+LDHospitalGrid.getRowColData(index,2)+"'";
	              
	      turnPage6.queryModal(sql, DutyDoneState);
	}
}



function dutyQuery()
{
	var index = LDHospitalGrid.getSelNo ()-1;
	
	initDutyDoneState();
	var sql="select (select c.CodeName from LDCode c where c.Code=a.contratype and c.Codetype='hmdeftype'),"
			  +"(select e.Dutyitemname from ldcontraitemduty e where e.Dutyitemcode=a.DutyItemCode),"
				+" case when a.DutyState = '1' then '有效' else '无效' end  ,"
				+"b.ContraNo,b.ContraName,"
				+" (case when (select distinct f.ContraItemNo from LHContraAssoSetting f where f.ContraItemNo=a.ContraItemNo) is null then '无关联' else '有关联' end ),"
				+"( select count(ServTaskno) from LHEXECFEEBALANCE f where f.Contraitemno=a.DutyItemCode "
				+"    and f.Contrano=b.Contrano and f.Hospitcode=d.Hospitcode  and f.ServPlanno=a.ContraItemno "
				+"     and f.Servtaskno in "
				+"    (select k.ServTaskno from LHCaseTaskRela k where k.ServTaskAffirm='2')"
				+" ),"
				+" ( select count(e.ServTaskno) from LHFEECHARGE e,LHEXECFEEBALANCE f  "
				+"   where e.FeeTaskExecno=f.TaskExecno and  f.Contraitemno=a.DutyItemCode and "
				+"      f.Contrano=b.Contrano and f.Hospitcode=d.Hospitcode  and f.ServPlanno=a.ContraItemno "
				+"     and f.ServTaskno in (select k.ServTaskno from LHCaseTaskRela k where k.ServTaskAffirm='2')"
				+" ),"
				+" ( select value(Sum(e.FeePay),0) from LHFEECHARGE e,LHEXECFEEBALANCE f "
				+"    where e.FeeTaskExecno=f.TaskExecno and  f.Contraitemno=a.DutyItemCode and "
				+"     f.Contrano=b.Contrano and f.Hospitcode=d.Hospitcode  and f.ServPlanno=a.ContraItemno "
				+"     and f.ServTaskno in (select k.ServTaskno from LHCaseTaskRela k where k.ServTaskAffirm='2')"
				+ " ) ,a.ContraItemNo ,"
				+" ( select distinct  f.ServTaskno from LHFEECHARGE e,LHEXECFEEBALANCE f     "
				+"     where e.FeeTaskExecno=f.TaskExecno and  f.Contraitemno=a.DutyItemCode and "
				+"      f.Contrano=b.Contrano and f.Hospitcode=d.Hospitcode and f.ServPlanno=a.ContraItemno  "
				+"         and f.ServTaskno in (select k.ServTaskno from LHCaseTaskRela k where k.ServTaskAffirm='2')"
				+ " ) "
			  +" from LHContItem a,LHGroupCont b, LDHospital d where a.ContraNo=b.ContraNo and b.HospitCode=d.HospitCode "
				+ getWherePart("a.DutyState","DutyState")
				+"  and d.HospitalType='"+LDHospitalGrid.getRowColData(index,10)
				+"' and d.HospitCode='"+LDHospitalGrid.getRowColData(index,2)
				+"' and d.HospitName='"+LDHospitalGrid.getRowColData(index,3)+"'"
				;

		turnPage.queryModal(sql, LHDutySpecialInfo);
}

function contraspeinfo()
{
	var index = LDHospitalGrid.getSelNo ()-1;
	
	if (LHGroupContGrid.getSelNo() >= 1)
	{

		if(LHGroupContGrid.getRowColData(LHGroupContGrid.getSelNo()-1,1)=="")
		{
			alert("请选择正确的合同编号!");
			return false;
		}
		else
		{
			var ContNo = LHGroupContGrid.getRowColData(LHGroupContGrid.getSelNo()-1,1);
			if(ContNo!=""||ContNo!="null"||ContNo!=null)
			{
				var HospitalNo = LDHospitalGrid.getRowColData(index,2);
				
				var ContraNo = LHGroupContGrid.getRowColData(LHGroupContGrid.getSelNo()-1,1);

				window.open("./LHGroupContInputMain.jsp?HospitalNo="+HospitalNo+"&ContraNo="+ContraNo+"&ContFlag=1","医疗机构信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			}
		}
	}
	else
	{
		alert("请选择一条要传输的记录！");
	}
}
function dutyspecQuery()
{
	var index = LDHospitalGrid.getSelNo ()-1;
	
	if (LHDutySpecialInfo.getSelNo() >= 1)
	{
		
		if(LHDutySpecialInfo.getRowColData(LHDutySpecialInfo.getSelNo()-1,1)=="")
		{
			alert("请选择正确的合同编号!");
			return false;
		}
		else
		{
			var ContNo = LHDutySpecialInfo.getRowColData(LHDutySpecialInfo.getSelNo()-1,4);
			if(ContNo!=""||ContNo!="null"||ContNo!=null)
			{
				var HospitalNo = LDHospitalGrid.getRowColData(index,2);
				
				var ContraNo = LHDutySpecialInfo.getRowColData(LHDutySpecialInfo.getSelNo()-1,4);
				//alert(ContraNo);
				window.open("./LHGroupContInputMain.jsp?HospitalNo="+HospitalNo+"&ContraNo="+ContraNo+"&ContFlag=1","医疗机构信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			}
		}
	}
	else
	{
		alert("请选择一条要传输的记录！");
	}
}

//就诊情况查询
function InhospitQueryResu()
{
	var index = LDHospitalGrid.getSelNo ()-1;
	
	if(fm.all('InHospitStartDate').value != "" && fm.all('InHospitEndDate').value != "")
	{
		if(compareDate(fm.all('InHospitStartDate').value,fm.all('InHospitEndDate').value) == true)
		{
			alert("住院起始时间应早于终止时间");
			return false;
		}
	}

	//医疗费用
	var allMZ = "";
	var allZY = "";

	//拼险种查询字符串
	var sqlPol = "";
	if(fm.all('RiskcodeInfo').value=="")
	{}else{sqlPol = " and p.riskcode in ("+fm.all('RiskcodeInfo').value+") ";}

	//门诊总人次

	var sqlServ ="select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a, llclaimpolicy p,llcase c "
				+" where  a.FeeType='1' and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano and a.SumFee != 0 and c.caseno=p.caseno "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' "
				+") as X with ur "
				;

	fm.all('Timesinserv').value=easyExecSql(sqlServ);

	//住院总人次
	var sqlHospit="select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a, llclaimpolicy p,llcase c "
				+" where  a.FeeType='2' and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano and a.SumFee != 0 and a.RealHospDate != 0 "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' "
				+") as X with ur "
				;

	fm.all('Timesinhospit').value=easyExecSql(sqlHospit);

	//门诊住院人数之和
	var a=parseInt(fm.all('Timesinserv').value)+parseInt(fm.all('Timesinhospit').value);

	//平均住院日
	if (parseInt(fm.all('Timesinhospit').value)==0)
		fm.all('Averinhospit').value=0;
	else
	{
		var sqlRealDate = " select value(sum(aa),0) from ( select distinct a.caseno,a.caserelano,a.feedate,a.realhospdate as aa from llfeemain a, llclaimpolicy p,llcase c "
				+" where  a.FeeType='2' and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano and a.SumFee != 0 and a.RealHospDate != 0 "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' "
				//+ getWherePart("a.hospitalname","HospitName","like")
				+") as X with ur "  ;
				
		var T=easyExecSql(sqlRealDate)/parseInt(fm.all('Timesinhospit').value);
		fm.all('Averinhospit').value=Math.round(T*100)/100;
	}

	//平均门诊费用
	if (fm.all('Timesinserv').value==0)
		fm.all('OutpatientService').value=0;
	else
	{
		var sqlSumFee = " select value(sum(sumfee),0) from (select distinct a.caseno, a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a,"
				+" llclaimpolicy p,llcase c where  a.FeeType='1' and a.SumFee != 0 and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano and a.caseno = p.caseno  and c.caseno = a.caseno "
				+ sqlPol+getWherePart("a.Feedate","InHospitStartDate",">=")+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' "
				//+ getWherePart("a.hospitalname","HospitName","like")
				+" )as X with ur "
				;//alert(easyExecSql(sqlSumFee));
		allMZ = easyExecSql(sqlSumFee)
		var b=parseFloat(allMZ)/parseInt(fm.all('Timesinserv').value);
		fm.all('OutpatientService').value=Math.round(b*100)/100;
	}

	//平均住院费用
	if (fm.all('Timesinhospit').value==0)
		fm.all('Inhospital').value=0;
	else
	{
		var sqlSumFeeT = " select value(sum(sumfee),0) from (select distinct a.caseno,a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a, llclaimpolicy p,llcase c "
				+" where  a.FeeType='2' and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano and a.SumFee != 0 and a.RealHospDate != 0 "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' "
				//+ getWherePart("a.hospitalname","HospitName","like")
				+") as X with ur "
				;//alert(easyExecSql(sqlSumFee));
		allZY = easyExecSql(sqlSumFeeT);
	    var b=parseFloat(allZY)/parseInt(fm.all('Timesinhospit').value);
		fm.all('Inhospital').value=Math.round(b*100)/100;		//取四舍五入后的值
	}

	if(allZY == "")
		allZY = 0;
	if(allMZ == "")
		allMZ = 0;
	//费用总和
	var c=parseFloat(allZY)+parseFloat(allMZ);

	if (c=="null" || c == 0)
	{
			fm.all('Medical').value=0;
			fm.all('Check').value=0;
			fm.all('Cure').value=0;
			fm.all('Operation').value=0;
			fm.all('Other').value=0;
	}
	else
	{
		//药费占比
	    var drugFee = easyExecSql(" select value(sum (b.Fee),0) from llfeemain a, llcasereceipt b,llclaimpolicy p,llcase c "
				+" where  a.FeeType in ('1','2') and c.caseno = a.caseno and a.caserelano = p.caserelano "
				+"and ((a.FeeType = '1' and a.SumFee != 0) or (a.FeeType = '2' and a.SumFee != 0 and a.RealHospDate != 0))"
				+" and a.Mainfeeno=b.Mainfeeno and c.rgtstate in ('09','11','12') "
				+" and b.Feeitemcode in ('101','102','121','202','203','204') "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")
				+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' ") 

				+" with ur "
				;
	    var M = parseFloat(drugFee)/parseFloat(c);
		fm.all('Medical').value=Math.round(M*100)/100;

		//检查费占比
	    var testFee = easyExecSql(" select value(sum (b.Fee),0) from llfeemain a, llcasereceipt b,llclaimpolicy p,llcase c "
				+" where  a.FeeType in ('1','2') and c.caseno = a.caseno and a.caserelano = p.caserelano "
				+"and ((a.FeeType = '1' and a.SumFee != 0) or (a.FeeType = '2' and a.SumFee != 0 and a.RealHospDate != 0))"
				+" and a.Mainfeeno=b.Mainfeeno and c.rgtstate in ('09','11','12') "
				+" and b.Feeitemcode in ('205','107','207','111','209','112','110','214') "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")
				+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' ") 
				+" with ur "
				;
		var M2 = parseFloat(testFee)/parseFloat(c);
	    fm.all('Check').value=Math.round(M2*100)/100;

		//治疗费占比
	    var cureFee = easyExecSql(" select value(sum (b.Fee),0) from llfeemain a, llcasereceipt b,llclaimpolicy p,llcase c "
				+" where  a.FeeType in ('1','2') and c.caseno = a.caseno and a.caserelano = p.caserelano "
				+"and ((a.FeeType = '1' and a.SumFee != 0) or (a.FeeType = '2' and a.SumFee != 0 and a.RealHospDate != 0))"
				+" and a.Mainfeeno=b.Mainfeeno and c.rgtstate in ('09','11','12') "
				+" and b.Feeitemcode in ('206','104','221','124','210','211') "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")
				+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' ") 
				+" with ur "
				;
		var M3 = parseFloat(cureFee)/parseFloat(c);
	    fm.all('Cure').value=Math.round(M3*100)/100;

		//手术费占比
	    var opFee = easyExecSql(" select value(sum (b.Fee),0) from llfeemain a, llcasereceipt b,llclaimpolicy p,llcase c "
				+" where  a.FeeType in ('1','2') and c.caseno = a.caseno and a.caserelano = p.caserelano "
				+"and ((a.FeeType = '1' and a.SumFee != 0) or (a.FeeType = '2' and a.SumFee != 0 and a.RealHospDate != 0))"
				+" and a.Mainfeeno=b.Mainfeeno and c.rgtstate in ('09','11','12') "
				+" and b.Feeitemcode in ('208','113','217') "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")
				+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' ") 
				+" with ur "
				;
		var M4 = parseFloat(opFee)/parseFloat(c);
        fm.all('Operation').value=Math.round(M4*100)/100;

		//其他费用占比
	    var elseFee = easyExecSql(" select value(sum (b.Fee),0) from llfeemain a, llcasereceipt b,llclaimpolicy p,llcase c "
				+" where  a.FeeType in ('1','2') and c.caseno = a.caseno and a.caserelano = p.caserelano "
				+"and ((a.FeeType = '1' and a.SumFee != 0) or (a.FeeType = '2' and a.SumFee != 0 and a.RealHospDate != 0))"
				+" and a.Mainfeeno=b.Mainfeeno and c.rgtstate in ('09','11','12') "
				+" and b.Feeitemcode in ('226','201','218','219','220','222','118','223','123','224','216','212','213','106','114','225') "
				+ sqlPol + getWherePart("a.Feedate","InHospitStartDate",">=")
				+ getWherePart("a.Feedate","InHospitEndDate","<=")
				+ " and a.hospitalcode='"+LDHospitalGrid.getRowColData(index,2)+"' ")
				+" with ur "
				;
		var M5 = parseFloat(elseFee)/parseFloat(c);
        fm.all('Other').value=Math.round(M5*100)/100;
	}

  	var RiskcodeInfo="";
  	if(fm.all('RiskcodeInfo').value != "")
  	{
  	      RiskcodeInfo =" and p.riskcode in ("+fm.all('RiskcodeInfo').value+") ";
  	}
	var strSql=" select distinct m.CustomerNo ,m.Customername ,"
    		  +" (select l.codename from ldcode l  where l.codetype='hminhospitalmode' and l.code=m.FeeType ),"
    		  +" m.FeeDate,m.RealHospDate, m.SumFee,"
    		  +" value((select  r.RealPay from llclaim r where r.caseno=m.caseno),0),"//m.SelfAmnt,"
    		  +" value((select sum(t.SelfAmnt) from LLCaseReceipt t where t.Mainfeeno = m.Mainfeeno),0), "
    		  +" (select r.Riskname from lmriskapp R, llclaimpolicy p where p.RiskCode=r.Riskcode and  p.CaseNo=m.CaseNo and p.CaseRelaNo=m.CaseRelaNo fetch first 1 rows only),m.Mainfeeno"
    		  +"  from LLFeeMain m,llcase c, llclaimpolicy p where m.caseno = c.caseno and m.caserelano=p.caserelano and c.rgtstate in ('09','11','12') and m.HospitalCode='"+ LDHospitalGrid.getRowColData(index,2)+"'"
    		  + getWherePart("m.FeeDate","InHospitStartDate",">=")
    		  + getWherePart("m.FeeDate","InHospitEndDate","<=")
    		  + RiskcodeInfo
    		  + " with ur"
    		  ;
	turnPage7.queryModal(strSql, HospitalInfoGrid);
}


function diseaseQuery(){}

function afterCodeSelect(codeName,Field)
{
	if(codeName=="ldiseasecodeclassify" )
	{
		fm.all('union').value = fm.all('DiseasTypeLevel_origin').value + fm.all('ICDCode').value;
	}


}


function submitForm()
{
	if (fm.all('StartDate').value=='' || fm.all('EndDate').value=='')
	   alert("请输入查询日期范围！");
	else
	{
		fm.all('magcom').value=manageCom;
		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

		fm.action = "./LHHospitalQueryRpt.jsp";
		fm.target = "f1print";
		fm.all('op').value = '';
		fm.submit();
		showInfo.close();
   }
}
function showTech()
{

	 var SpecialCode=SpecSecDiaGrid.getRowColData(SpecSecDiaGrid.getSelNo()-1,3);
	 var NoteTech=SpecSecDiaGrid.getRowColData(SpecSecDiaGrid.getSelNo()-1,4);
	 var sql="select a.SpecialIntro from LDHospitalInfoIntro a "
	 				+" where a.SpecialClass = 'SpecSecDia'  "
	 				+" and a.SpecialName='"+ SpecialCode +"' and a.Flowno='"+NoteTech+"'"
			    ;
	 //alert(sql);
	 var Note=easyExecSql(sql);
	 //alert(Note);
	 var TechInfo=SpecSecDiaGrid.getRowColData(SpecSecDiaGrid.getSelNo()-1,2);
	 if(Note!=""&&Note!="null"&&Note!=null)
	 {
	 		TechNameInfo.style.display='';
	    fm.all('MainItem').value=TechInfo;

	 }
	 else
	 {
	 		TechNameInfo.style.display='none';
	 	 	fm.all('MainItem').value="";

	 }

}
function queryRiskInfo()
{
	 showInfo=window.open("./LHHospitalInfoRiskQuery.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}
function afterQuery(arrResult)
{
	//alert("ddddd "+arrResult.length);
	var arrRiskCode=arrResult.toString().split(",");
	var sqlRiskCode = "";
	for(var j=0; j < arrResult.length; j++)
	{
	      sqlRiskCode= sqlRiskCode+",'"+arrRiskCode[j]+"'";//给取得的值加单引号
	      //alert(" "+sqlRiskCode);
	}
	sqlRiskCode = sqlRiskCode.substring(1);
	if(arrResult!=null)
	{
		fm.RiskcodeInfo.value = sqlRiskCode;
	}
}
function LHExportHospital()
{
	var showStr="正在导出数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('fmtransact').value = "PRINT";
	fm.action="./LHHospitalQueryExp.jsp";
	fm.target = "f1print";
	fm.submit(); //提交
	showInfo.close();
}

//显示医院信息
function showHospitalInfo()
{
	var index = LDHospitalGrid.getSelNo ()-1;
	fm.HospitalType.value = LDHospitalGrid.getRowColData(index,10);
	if(fm.HospitalType.value=='1')
	{
		fm.HospitalType_ch.value = '医疗机构';
	}
	else if(fm.HospitalType.value=='2')
	{
		fm.HospitalType_ch.value = '健康体检机构';
	}
	else if(fm.HospitalType.value=='3')
	{
		fm.HospitalType_ch.value = '健康管理公司';
	}
	else if(fm.HospitalType.value=='4')
	{
		fm.HospitalType_ch.value = '紧急救援公司';
	}	
	else if(fm.HospitalType.value=='5')
	{
		fm.HospitalType_ch.value = '指定口腔医疗机构';
	}
	else if(fm.HospitalType.value=='6')
	{
		fm.HospitalType_ch.value = '其他合作机构';
	}
				
	fm.HospitName.value = LDHospitalGrid.getRowColData(index,3);
	fm.HospitCode.value	= LDHospitalGrid.getRowColData(index,2);
}


function afterSubmit2( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}
