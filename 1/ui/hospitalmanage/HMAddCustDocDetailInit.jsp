<%
  //程序名称：HMAddCustDocDetailInit.jsp
  //程序功能：添加客户档案明细
  //创建日期：2010-3-8
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm()
{
	try{
		initPolGrid();
		initExamIndGrid();
		//查询客户信息
		queryCustInfo();
		//查询有效保单信息
		queryValidPolicyInfo();
		queryAlreadyExamIndInfo();
  		queryPastHistoryInfo();
	}
	catch(re){
		alert("HMAddCustDocDetailInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//有效保单信息
function initPolGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="合同号码";
		iArray[1][1]="70px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="险种编码";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="险种名称";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="保险金额";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="生效日期";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="失效日期";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		
		iArray[7]=new Array();
		iArray[7][0]="理赔金额";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="投保人名称";
		iArray[8][1]="100px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 

		PolGrid.mulLineCount=1;
		PolGrid.displayTitle=1;
		PolGrid.canSel=0;
		PolGrid.canChk=0;
		PolGrid.hiddenPlus=1;
		PolGrid.hiddenSubtraction=1;
		PolGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

//检查阳性指标信息
function initExamIndGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
    	iArray[0][0]="序号";  					//列名（序号列，第1列）
    	iArray[0][1]="30px";  					//列宽
    	iArray[0][2]=10;      					//列最大值
    	iArray[0][3]=0;   							//1表示允许该列输入，0表示只读且不响应Tab键
    	               									// 2 表示为容许输入且颜色加深.
    	iArray[1]=new Array();
    	iArray[1][0]="检查项目名称";  	//列名（第2列）
    	iArray[1][1]="81px";  	  			//列宽
    	iArray[1][2]=10;        				//列最大值
    	iArray[1][3]=1;          				//是否允许输入,1表示允许，0表示不允许
    	
    	iArray[2]=new Array();                                          
    	iArray[2][0]="检查项目代码";   			//列名（第2列）                   
    	iArray[2][1]="60px";  	 			 	//列宽                                  
    	iArray[2][2]=10;       		 		 	//列最大值                              
    	iArray[2][3]=2;          			 	//是否允许输入,1表示允许，0表示不允许  
    	iArray[2][4]="lhtestname";
    	iArray[2][5]="2|1|5";    				//引用代码对应第几列，'|'为分割符
    	iArray[2][6]="1|0|2";    				//上面的列中放置引用代码中第几位值
    	iArray[2][9]="检查项目代码|len<=120";
    	iArray[2][15]="MedicaItemName";
    	iArray[2][17]="1";  
    	iArray[2][19]=1 ;
    	
    	iArray[3]=new Array();                   			
		iArray[3][0]="是否异常";  			//列名（第2列）                                                 
    	iArray[3][1]="40px";  	  		  		//列宽                                                    
    	iArray[3][2]=1;        					//列最大值                                                  
    	iArray[3][3]=2;
    	iArray[3][10]="Isnormal";
    	iArray[3][11]= "0|^正常|1^异常|2";  	//虚拟数据源
    	iArray[3][12]="3|10";    				//引用代码对应第几列，'|'为分割符
    	iArray[3][13]="0|1";  
    	//iArray[3][14]= "正常";  				//虚拟数据源
    	iArray[3][19]=1;               			//强制刷新
    	
    	iArray[4]=new Array();
    	iArray[4][0]="检查结果";  		  		//列名（第2列）
    	iArray[4][1]="90px";  	  		  		//列宽
    	iArray[4][2]=130;        			  	//列最大值
    	iArray[4][3]=1;     

    	
    	iArray[5]=new Array();                             
    	iArray[5][0]="标准单位";        		//列名（第2列）          
    	iArray[5][1]="70px";  	        		//列宽                   
    	iArray[5][2]=10;        	      		//列最大值                 
    	iArray[5][3]=1;   
    	                                           
		iArray[6]=new Array();                                            
    	iArray[6][0]="流水号";   				//列名（第2列）                           
    	iArray[6][1]="0px";  	   				//列宽                                   
    	iArray[6][2]=10;         				//列最大值                                
    	iArray[6][3]=3;          				//是否允许输入,1表示允许，0表示不允许   
    	
    	iArray[7]=new Array();                             
    	iArray[7][0]="3num";        			//列名（第2列）          
    	iArray[7][1]="0px";  	        		//列宽                   
    	iArray[7][2]=10;        	      		//列最大值
    	iArray[7][3]=3;
    	                   
    	iArray[8]=new Array();                             
    	iArray[8][0]="正常值";        			//列名（第2列）          
    	iArray[8][1]="70px";  	        		//列宽                   
    	iArray[8][2]=10;        	      		//列最大值
    	iArray[8][3]=1;
    	
    	iArray[9]=new Array();                             
    	iArray[9][0]="检查日期";        			//列名（第2列）          
    	iArray[9][1]="70px";  	        		//列宽                   
    	iArray[9][2]=10;        	      		//列最大值
    	iArray[9][3]=1;
    	
    	iArray[10]=new Array();                             
    	iArray[10][0]="是否异常代码";        			//列名（第2列）          
    	iArray[10][1]="60px";  	        		//列宽                   
    	iArray[10][2]=10;        	      		//列最大值
    	iArray[10][3]=3;
    	

		ExamIndGrid = new MulLineEnter( "fm" , "ExamIndGrid" ); 

		ExamIndGrid.mulLineCount=1;
		ExamIndGrid.displayTitle=1;
		ExamIndGrid.canSel=0;
		ExamIndGrid.canChk=0;
		ExamIndGrid.hiddenPlus=0;
		ExamIndGrid.hiddenSubtraction=0;
		ExamIndGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
