<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：LHAssociateSettingSave.jsp
	//程序功能：
	//创建日期：2006-03-14 15:15:48
	//创建人  ：郭丽颖
	//更新记录： 
	// 更新人 : 
	// 更新日期: 
	// 更新原因/内容:
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.hospitalmanage.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	// LHContraAssoSettingSchema tLHContraAssoSettingSchema   = new LHContraAssoSettingSchema();
	LHContraAssoSettingUI tLHContraAssoSettingUI = new LHContraAssoSettingUI();

	LHContraAssoSettingSet tLHContraAssoSettingSet = new LHContraAssoSettingSet(); //合同责任信息

	String tIndivContraItemNo[] = request.getParameterValues("LHContraAssoSettingGrid12");//关联责任号(系统流水号)   
	String tIndivContraNo[] = request.getParameterValues("LHContraAssoSettingGrid13"); //合同编号

	String tChk[] = request.getParameterValues("InpLHContraAssoSettingGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  

	//输出参数
	CErrors tError = null;
	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");

	int LHContraAssoSettingCount = 0;
	if (tIndivContraItemNo != null) 
	{
		LHContraAssoSettingCount = tIndivContraItemNo.length;
	}
	
	System.out.println(" LHContraAssoSettingCount is : "+ LHContraAssoSettingCount);

	for (int i = 0; i < LHContraAssoSettingCount; i++) 
	{

		LHContraAssoSettingSchema tLHContraAssoSettingSchema = new LHContraAssoSettingSchema();

		tLHContraAssoSettingSchema.setContraNo(request.getParameter("ContraNo"));
		tLHContraAssoSettingSchema.setContraItemNo(request.getParameter("ContraItemNo")); //合同责任项目编号(系统流水号)

		tLHContraAssoSettingSchema.setIndivContraItemNo(tIndivContraItemNo[i]); //系统流水号
		tLHContraAssoSettingSchema.setIndivContraNo(tIndivContraNo[i]); //关联责任人员编号

		tLHContraAssoSettingSchema.setOperator(request.getParameter("Operator"));
		tLHContraAssoSettingSchema.setMakeDate(request.getParameter("MakeDate"));
		tLHContraAssoSettingSchema.setMakeTime(request.getParameter("MakeTime"));

		//for(int index=0;index<tChk.length;index++)
		// {
		if (tChk[i].equals("1")) 
		{
			System.out.println("该行被选中 " + tIndivContraNo[i]);
			tLHContraAssoSettingSet.add(tLHContraAssoSettingSchema);

		}
		if (tChk[i].equals("0")) 
		{
			System.out.println("该行未被选中 " + tIndivContraNo[i]);
		}
		// }  

	}

	try {
		// 准备传输数据 VData
		VData tVData = new VData();
		//	tVData.add(tLHContraAssoSettingSchema);
		tVData.add(tLHContraAssoSettingSet);
		tVData.add(tG);
		System.out.println("- DDDDDDDDDD " + transact);
		tLHContraAssoSettingUI.submitData(tVData, transact);
		System.out.println("- SSSSSSSSSSSS " + transact);
	} 
	catch (Exception ex) 
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "") 
	{
		tError = tLHContraAssoSettingUI.mErrors;
		if (!tError.needDealError()) 
		{
			Content = " 操作成功! ";
			FlagStr = "Success";
		} 
		else 
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

	//添加各种预处理
%>
<%=Content%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	       var arrResult = new Array();
	        arrResult[0] =new Array();
	        arrResult[0][0] = parent.fraInterface.fm.all("ContraNo").value;
	        //alert(arrResult[0][0]);
	        parent.fraInterface.afterQuery2(arrResult); 
</script>
</html>
