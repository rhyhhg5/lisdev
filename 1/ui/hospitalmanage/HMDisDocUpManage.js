//该文件中包含客户端需要处理的函数和事件

//程序名称：HMDisDocUpManage.js
//程序功能：疾病档案查询变更管理
//创建日期：2010-3-10
//创建人  ：chenxw
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

var CustomerInfoTurnPage = new turnPageClass(); 

//目标客户提取按钮
function custumerFetch()
{
	//如果录入证件号码；理赔案件号，保单号 其中一个，那么就诊起始/结束时间可以不录入。否则必须录入
	if(((fm.IDType2.value == null || fm.IDType2.value=="") || (fm.IDNo2.value==null || fm.IDNo2.value==""))
			&&(fm.CaseNo2.value == null || fm.CaseNo2.value == "")
			&&(fm.CustomerNo2.value == null || fm.CustomerNo2.value == "")) {
		if(fm.DocStartDate2.value == null || fm.DocStartDate2.value == ""
			|| fm.DocEndDate2.value == null || fm.DocEndDate2.value == "") {
			alert("您证件号码，理赔案件号，客户号，都没有录入，所以必须录入初次建档时间的区间段！");
			return false
		}
	}
	//查询明细
	queryCustomerInfoGrid();	
}

//疾病档案查询变更
function disDocUp()
{
	var tRow=CustomerInfoGrid.getSelNo();
	if(tRow==0)	{
   	
		alert("请选择一条客户信息后，再进入！");
		return false;
	}
	var tCustNo = CustomerInfoGrid.getRowColData(tRow-1,1);
	var tIDType = CustomerInfoGrid.getRowColData(tRow-1,3);
	var tIDNo = CustomerInfoGrid.getRowColData(tRow-1,5);
	if(tCustNo == null || tCustNo == "") {
		alert("您不能选择一条空的记录!");
		return false;
	}
	
	window.open("./HMDisDocUpDetailFrame.jsp?CustomerNo=" + tCustNo + "&IDType=" + tIDType + "&IDNo=" + tIDNo);
}

function queryCustomerInfoGrid()
{
   //销售渠道
   var channelCondition = "";
   if(fm.ChannelType.value != null && fm.ChannelType.value != "") {
   		if(fm.ChannelType.value == "3") {  //银行渠道
   			channelCondition = " and exists( select 1 from  lcpol aa "
   					+" where aa.InsuredNo = a.CustomerNo and  aa.salechnl in('04','13')) " ;
   		}
   		if(fm.ChannelType.value == "2") { //个险
   			channelCondition = " and exists (select 1 from lcpol aa "
   					+" where aa.InsuredNo = a.CustomerNo and aa.salechnl not in('04','13') "
   						+" and aa.grpcontno='00000000000000000000') " ;
   		}
   		
   		if(fm.ChannelType.value == "1") { //团险
   			channelCondition = " and exists (select 1 from lcpol aa "
   					+" where aa.InsuredNo = a.CustomerNo and aa.salechnl not in('04','13') "
   						+" and aa.grpcontno <> '00000000000000000000') " ;
   		}
   }
   
   //投保人
   var appntCondition = "";
   if(fm.AppntName2.value != null && fm.AppntName2.value != "" ) {
   		appntCondition = " and exists (select 1 from lccont aa "
   					+" where aa.InsuredNo = a.CustomerNo and aa.AppntName like '%" 
   											+ fm.AppntName2.value + "%') ";
   }
   
   var tSQL = 
   			" select distinct c.insuredno, "
	               +" c.name, "
	               +" c.idtype, "
	                +" (select CodeName "
	                   +" from ldcode "
	                  +" where codetype = 'idtype' "
	                    +" and code = c.idtype), "
	                +" c.idno, "
	                +" a.makedate, "
	                +" (case (select distinct 1 "
                     	+" from HMCustDisDocInfo cc "
                    	+" where cc.CustomerNo = a.CustomerNo) when 1 then "
                   				+" '已经建立' "
                  			+" else "
                   				+" '未建立' "
                	+" end) "
	                
	         +" from HMCustDocInfo a left outer join HMCustDisDocInfo b  on a.CustomerNo = b.CustomerNo ,lcinsured c "
	         +" where a.CustomerNo = c.InsuredNo "
	         + channelCondition
	         + appntCondition
	         
	         + getWherePart("a.ManageCom","ManageCom2","like")
			 + getWherePart("c.name","Customer2","like")
			 
			 + getWherePart("c.idtype","IDType2")
			 + getWherePart("c.idno","IDNo2")
			 + getWherePart("b.caseno","CaseNo2")
			  + getWherePart("b.DiseaseNo","DiseaseCode2")
			 + getWherePart("a.CustomerNo","CustomerNo2")
			 
			 + getWherePart("a.makedate","DocStartDate2",">=")
			 + getWherePart("a.makedate","DocEndDate2","<=")
			 
			 + getWherePart("b.modifydate","NewDisDocStartDate2",">=")
			 + getWherePart("b.modifydate","NewDisDocEndDate2","<=")
			 
			 + " order by a.makedate desc with ur";
		   CustomerInfoTurnPage.queryModal(tSQL,CustomerInfoGrid);
}


function queryCustomerInfoGrid_bak()
{
   var condition = "";
   if(fm.ChannelType.value != null && fm.ChannelType.value != "") {
   		if(fm.ChannelType.value == "3") {  //银行渠道
   			condition = " and f.salechnl in('04','13') " ;
   		}
   		if(fm.ChannelType.value == "2") { //个险
   			condition = " and (f.salechnl not in('04','13') and c.grpcontno='00000000000000000000') " ;
   		}
   		
   		if(fm.ChannelType.value == "1") { //团险
   			condition = " and (f.salechnl not in('04','13') and c.grpcontno <> '00000000000000000000') " ;
   		}
   }
   
   var strSQL = 
		" select distinct b.insuredno, "
	               +" b.name, "
	               +" b.idtype, "
	                +" (select CodeName "
	                   +" from ldcode "
	                  +" where codetype = 'idtype' "
	                    +" and code = b.idtype), "
	                +" b.idno, "
	                +" e.makedate "
	  +" from llcase a left outer join LLCaseCure tt on a.caseno = tt.caseno, "
	       +" lcinsured b, "
	       +" LLClaimDetail c, "
	       +" LLFeeMain d, "
	       +" HMCustDocInfo e, "
	       +" lcpol f "
	 +" where a.CustomerNo = b.InsuredNo "
			+" and a.caseno=c.CaseNo "
			+" and a.caseno=d.caseno "
			//+" and a.rgtstate='12' "
			+" and e.CustomerNo=a.CustomerNo "
			+" and f.polno=c.polno"
			
			+ condition
			
	 + getWherePart("a.MngCom","ManageCom2","like")
	 + getWherePart("b.name","Customer2","like")
	 + getWherePart("d.FeeDate","VisFinStartDate2",">=")
	 + getWherePart("d.FeeDate","VisFinEndDate2","<=")
	 
	 + getWherePart("b.idtype","IDType2")
	 + getWherePart("b.idno","IDNo2")
	 + getWherePart("a.caseno","CaseNo2")
	 + getWherePart("c.ContNo","ContNo2")
	  + getWherePart("tt.DiseaseCode","DiseaseCode2")
	 + getWherePart("a.CustomerNo","CustomerNo2")
	 + getWherePart("f.AppntName","AppntName2")
	 
	 + getWherePart("e.makedate","DocStartDate2",">=")
	 + getWherePart("e.makedate","DocEndDate2","<=")
	 
	 + " with ur";
	 
   CustomerInfoTurnPage.queryModal(strSQL,CustomerInfoGrid);
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

