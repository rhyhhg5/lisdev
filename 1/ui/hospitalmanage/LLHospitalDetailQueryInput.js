//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------

//查询按键
var turnPage=new turnPageClass();
function queryClick()
{
	initDataGrid();
	var strSerialNo=document.all("SerialNo").value;
	var strManageCom=document.all("ManageCom").value;
	var strHospitalCode=document.all("HospitalCode").value;
	var strMsgChannelCode=document.all("MsgChannelCode").value;
	var strCharger=document.all("Charger").value;
	var strProfessionalCode=document.all("ProfessionalCode").value;
	var strStartDate=document.all("StartDate").value;
	var strEndDate=document.all("EndDate").value;
	
	var strSql="select SerialNo,ManageCom,HospitalName,MsgChannel,Charger,Professional,BussinessDate,ProblemDetail,Action,HospitalCode from LLMedicalManagement where ManageCom like '"+strManageCom+"%%'";
	
	
	if(strHospitalCode!=""&&strHospitalCode!=null)
	{
		strSql+=" and HospitalCode='"+strHospitalCode+"'";
	}
	if(strMsgChannelCode!=""&&strMsgChannelCode!=null)
	{
		strSql+=" and MsgChannelCode='"+strMsgChannelCode+"'";
	}
	if(strCharger!=""&&strCharger!=null)
	{
		strSql+=" and Charger='"+strCharger+"'";
	}
	if(strProfessionalCode!=""&&strProfessionalCode!=null)
	{
		strSql+=" and ProfessionalCode='"+strProfessionalCode+"'";
	}
	if(strStartDate!=""&&strStartDate!=null)
	{
		strSql+=" and MakeDate>='"+strStartDate+"'";
	}
	if(strEndDate!=""&&strEndDate!=null)
	{
		strSql+=" and ModifyDate<='"+strEndDate+"'";
	}
	
	  turnPage.queryModal(strSql, DataGrid);	
}
//------------------------------------------------------



//------------------------------------------------------
function returnClick()
{
   var arrReturn = new Array();
   var nSelectIndex=0;
   nSelectIndex = DataGrid.getSelNo();
   if(nSelectIndex == 0|| nSelectIndex == null )
   {
		window.alert( "请先选择一条记录，再点击返回按钮!" );
   }
   else
   {
		try
		{  
			arrReturn = getQueryResult();
			if(arrReturn==null)
			{
				return false;
			}
			
			//top.opener.afterQuery(arrReturn);
			parent.opener.afterQuery(arrReturn);
			parent.opener.document.all["hiddenSerialNo"].value=DataGrid.getRowColData(nSelectIndex-1,1);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口" + ex );
		}
		finally
		{
			top.close();
		}
   }
}


function getQueryResult()
{
	var arrSelected = null;
	var tRow = DataGrid.getSelNo();
    
	if(tRow == 0)
	{ 
	   return null;
	}
	else
	{
		var strSerialNo = DataGrid.getRowColData(tRow-1,1);	
		
		var strSQL="select ManageCom,HospitalName,MsgChannel,Charger,Professional,BussinessDate,ProblemDetail,Action,HospitalCode "
  	        +"from LLMedicalManagement where SerialNo='"+strSerialNo+"'";
	    
	    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if(!turnPage.strQueryResult)
        {
		    window.alert("无法查询该条记录！");
		    return null;
        }
        else
        {
			arrSelected = new Array();
			arrSelected=DataGrid.getRowData(tRow-1);
			return arrSelected;
        }
    }
}