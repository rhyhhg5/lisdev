<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHChargeBalanceSave.jsp
//程序功能：
//创建日期：2006-03-11 15:15:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.hospitalmanage.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	LHChargeBalanceUI tLHChargeBalanceUI   = new LHChargeBalanceUI();
	LHChargeBalanceSet tLHChargeBalanceSet = new LHChargeBalanceSet();
	LHChargeBalanceSchema tLHChargeBalanceSchema = new LHChargeBalanceSchema();

    //String tBalanceDate[] = request.getParameterValues("LHChargeBalanceGrid1");
	//String tBalanceRemindDate[] = request.getParameterValues("LHChargeBalanceGrid2");
	//String tBalanceType[] = request.getParameterValues("LHChargeBalanceGrid5");    
	//String tAccounts[] = request.getParameterValues("LHChargeBalanceGrid4");      
	//String tContraGlideNo[] = request.getParameterValues("LHChargeBalanceGrid6");

	//输出参数
	CErrors tError = null;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
    tLHChargeBalanceSchema.setContraNo(request.getParameter("ContraNo"));
    tLHChargeBalanceSchema.setContraItemNo(request.getParameter("ContraItemNo")); 
    tLHChargeBalanceSchema.setContBalanceNo(request.getParameter("ContBalanceNo")); 
    tLHChargeBalanceSchema.setBalanceDate(request.getParameter("BalanceDate")); 
    tLHChargeBalanceSchema.setBalanceRemindDate(request.getParameter("BalanceRemindDate")); 
    tLHChargeBalanceSchema.setBalanceType(request.getParameter("BalanceType")); 
    tLHChargeBalanceSchema.setBankCode(request.getParameter("BankCode")); 
    tLHChargeBalanceSchema.setBankName(request.getParameter("BankName")); 
    tLHChargeBalanceSchema.setAccName(request.getParameter("AccName"));
    tLHChargeBalanceSchema.setAccounts(request.getParameter("Accounts"));
    tLHChargeBalanceSchema.setHospitCode(request.getParameter("HospitCode")); 
    tLHChargeBalanceSchema.setDrawer(request.getParameter("Drawer")); 
    tLHChargeBalanceSchema.setMakeDate(request.getParameter("MakeDate"));    
    tLHChargeBalanceSchema.setMakeTime(request.getParameter("MakeTime"));    
    
	//int LHChargeBalanceCount = 0;
	//if(tContraGlideNo != null)
	//{	
	//	   LHChargeBalanceCount = tContraGlideNo.length;
	//}	
	//System.out.println(" LHChargeBalanceCount is : "+LHChargeBalanceCount);
	
	//for(int i = 0; i < LHChargeBalanceCount; i++)
	//{
	//	LHChargeBalanceSchema tLHChargeBalanceSchema = new LHChargeBalanceSchema();
	//	tLHChargeBalanceSchema.setContraNo(request.getParameter("ContraNo"));
	//	tLHChargeBalanceSchema.setContraItemNo(request.getParameter("ContraItemNo")); //合同责任项目编号
	//	tLHChargeBalanceSchema.setBalanceDate(tBalanceDate[i]);	//费用结算时间
	//	tLHChargeBalanceSchema.setBalanceRemindDate(tBalanceRemindDate[i]);	//结算提醒时间
	//	tLHChargeBalanceSchema.setBalanceType(tBalanceType[i]);	 //结算方式
	//	tLHChargeBalanceSchema.setAccounts(tAccounts[i]);	//银行账号 
	//	tLHChargeBalanceSchema.setContBalanceNo(tContraGlideNo[i]);//系统流水号
	//	tLHChargeBalanceSchema.setOperator(request.getParameter("Operator"));
	//	tLHChargeBalanceSchema.setMakeDate(request.getParameter("MakeDate"));
	//	tLHChargeBalanceSchema.setMakeTime(request.getParameter("MakeTime"));                             
	//	tLHChargeBalanceSet.add(tLHChargeBalanceSchema);
	//}
	try
	{
		// 准备传输数据 VData
	  	VData tVData = new VData();
//	  	tVData.add(tLHChargeBalanceSet);
		tVData.add(tLHChargeBalanceSchema);
	  	tVData.add(tG);
    	tLHChargeBalanceUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  	if (FlagStr=="")
  	{
    	tError = tLHChargeBalanceUI.mErrors;
    	if (!tError.needDealError())
    	{                          
    		Content = " 操作成功! ";
    		FlagStr = "Success";
    	}
    	else                                                                           
    	{
    		Content = " 操作失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
	}
//添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>