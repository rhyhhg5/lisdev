<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：HMSuggestSave.jsp
//程序功能：专项健康建立录入
//创建日期：2010-3-17
//创建人  ：chenxw
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.hospitalmanage.*"%>

<%
 //接收信息，并作校验处理。
 //准备传输数据VData
  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  //返回结果值
  VData tResult = new VData();
  String tStrResult = "";

 //输入参数
 HMSuggestInfoSchema tHMSuggestInfoSchema = new HMSuggestInfoSchema();
 HMSuggestUI tHMSuggestUI = new HMSuggestUI();
 
 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String OperateDetail = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 //tG.ClientIP = request.getRemoteAddr();
 tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
 tG.ServerIP =tG.GetServerIP();
 System.out.println("------操作员IP: "+tG.ClientIP);
 System.out.println("------服务器IP: "+tG.ClientIP);
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 
 String transact = "";
 String tDisDetialOperate = "";
 
 //疾病建议
 String tempfmtransact = request.getParameter("fmtransact");
 //客户建议
 String tempCustTransact = request.getParameter("custfmTransact");
 //疾病建议操作细类
 String tempDisDetialOperate =  request.getParameter("DisDetialOperate");
 //客户建议操作细类
 String tempCustDetialOperate =  request.getParameter("custfmDetialOperate");
 
 if(tempfmtransact != null && !"".equals(tempfmtransact) && !"null".equals(tempfmtransact)) {
	 transact = tempfmtransact;
 }
 if(tempCustTransact != null && !"".equals(tempCustTransact) && !"null".equals(tempCustTransact)) {
	 transact = tempCustTransact;
 }
 
 if(tempDisDetialOperate != null && !"".equals(tempDisDetialOperate) && !"null".equals(tempDisDetialOperate)) {
	 tDisDetialOperate = tempDisDetialOperate;
 }
 if(tempCustDetialOperate != null && !"".equals(tempCustDetialOperate) && !"null".equals(tempCustDetialOperate)) {
	 tDisDetialOperate = tempCustDetialOperate;
 }
 
 tTransferData.setNameAndValue("DisDetialOperate",tDisDetialOperate);
 
 //疾病健康建议
 if(transact != null && "DISSUGGEST||MAIN".equals(transact)) {
	 //录入
	 if(tDisDetialOperate != null && "INSERT||DIS".equals(tDisDetialOperate)) {
		 tHMSuggestInfoSchema.setBusinessCode(request.getParameter("DiseaseCode"));
		 tHMSuggestInfoSchema.setBusinessType("1");
		 tHMSuggestInfoSchema.setBusinessName(request.getParameter("DiseaseName"));
		 tHMSuggestInfoSchema.setHealthSuggest(request.getParameter("DisSuggest"));
		 tVData.clear();
		 tVData.add(tHMSuggestInfoSchema);
		 OperateDetail = "DisSuggestInput";
	 }
	 //修改
	 if(tDisDetialOperate != null && "UPDATE||DIS".equals(tDisDetialOperate)) {
		 tHMSuggestInfoSchema.setBusinessCode(request.getParameter("DiseaseCodeForUp"));
		 tHMSuggestInfoSchema.setHealthSuggest(request.getParameter("DisSuggest2"));
		 tVData.clear();
		 tVData.add(tHMSuggestInfoSchema);
		 OperateDetail = "DisSuggestModify";
	 }
	 //删除
	 if(tDisDetialOperate != null && "DELETE||DIS".equals(tDisDetialOperate)) {
		 tHMSuggestInfoSchema.setBusinessCode(request.getParameter("DiseaseCodeForDel"));
		 tVData.clear();
		 tVData.add(tHMSuggestInfoSchema);
		 OperateDetail = "DisSuggestDel";
	 }
	 
	//建议发送（未选中全部发送）
	 if(tDisDetialOperate != null && "SUGGESTSEND||DIS".equals(tDisDetialOperate)) {
		 //健康建议发送轨迹表
		 HMSuggestSendTrackSet tHMSuggestSendTrackSet = new HMSuggestSendTrackSet();
		 //发送方式
		 String tSendType = request.getParameter("SendType3");
		 tTransferData.setNameAndValue("SendType",tSendType);
		 
		 String tCHK[] = request.getParameterValues("InpNotSendCustGridChk");
		 if(tCHK != null) {
			 for(int index=0; index<tCHK.length; index++) {
				 if(tCHK[index].equals("1")) {
					 
					 String tCustomerNo[] = request.getParameterValues("NotSendCustGrid1");
					 String tName[] = request.getParameterValues("NotSendCustGrid2");
					 String tMobile[] = request.getParameterValues("NotSendCustGrid3");
					 String tEMail[] = request.getParameterValues("NotSendCustGrid4");
					 String tHomeAddress[] = request.getParameterValues("NotSendCustGrid5");
					 String tDisCode[] = request.getParameterValues("NotSendCustGrid6");
					 String tDisCodeName[] = request.getParameterValues("NotSendCustGrid7");
					 String tLastVisEndDate[] = request.getParameterValues("NotSendCustGrid8");
					 String tLastYearVisCount[] = request.getParameterValues("NotSendCustGrid9");
					 String tLastYearMedRealPay[] = request.getParameterValues("NotSendCustGrid10");
					 String tVistType[] = request.getParameterValues("NotSendCustGrid11");
					 
					 HMSuggestSendTrackSchema tHMSuggestSendTrackSchema = new HMSuggestSendTrackSchema();
					 
					 tHMSuggestSendTrackSchema.setBusinessType("1");
					 tHMSuggestSendTrackSchema.setBusinessCode(tDisCode[index]);
					 tHMSuggestSendTrackSchema.setCustomerNo(tCustomerNo[index]);
					 tHMSuggestSendTrackSchema.setSendType(tSendType);
					 tHMSuggestSendTrackSchema.setName(tName[index]);
					 tHMSuggestSendTrackSchema.setMobile(tMobile[index]);
					 tHMSuggestSendTrackSchema.setEMail(tEMail[index]);
					 tHMSuggestSendTrackSchema.setHomeAddress(tHomeAddress[index]);
					 tHMSuggestSendTrackSchema.setVisType(request.getParameter("VistType2"));
					 tHMSuggestSendTrackSchema.setLastVisEndDate(tLastVisEndDate[index]);
					 tHMSuggestSendTrackSchema.setLastYearVisCount(tLastYearVisCount[index]);
					 tHMSuggestSendTrackSchema.setLastYearMedRealPay(tLastYearMedRealPay[index]);
					 tHMSuggestSendTrackSchema.setVisType(tVistType[index]);
					 
					 tHMSuggestSendTrackSet.add(tHMSuggestSendTrackSchema);
				 }
			 }
		 }
		 tVData.clear();
		 tVData.add(tHMSuggestSendTrackSet);
		 OperateDetail = "";
	 }
	
	//建议发送（选中全部发送）
	 if(tDisDetialOperate != null && "SUGGESTSENDALL||DIS".equals(tDisDetialOperate)) {
		 
		 //查询条件
		 String tDQ_LastVisStartDate = request.getParameter("LastVisStartDate2");
		 String tDQ_LastVisEndDate = request.getParameter("LastVisEndDate2");
		 String tDQ_DiseaseCode = request.getParameter("DiseaseCode2");
		 String tDQ_LastVisCount = request.getParameter("LastVisCount2");
		 String tDQ_HCostClaimFee = request.getParameter("HCostClaimFee2");
		 String tDQ_VistType = request.getParameter("VistType2");
		 String tDQ_SendType = request.getParameter("SendType2");
		 String tDQ_IsALSend = request.getParameter("isALSend");
		 
		 //发送方式
		 String tSendType = request.getParameter("SendType3");
		 
		 tTransferData.setNameAndValue("DQ_LastVisStartDate",tDQ_LastVisStartDate);
		 tTransferData.setNameAndValue("DQ_LastVisEndDate",tDQ_LastVisEndDate);
		 tTransferData.setNameAndValue("DQ_DiseaseCode",tDQ_DiseaseCode);
		 tTransferData.setNameAndValue("DQ_LastVisCount",tDQ_LastVisCount);
		 tTransferData.setNameAndValue("DQ_HCostClaimFee",tDQ_HCostClaimFee);
		 tTransferData.setNameAndValue("DQ_VistType",tDQ_VistType);
		 tTransferData.setNameAndValue("DQ_SendType",tDQ_SendType);
		 tTransferData.setNameAndValue("DQ_IsALSend",tDQ_IsALSend);
		 //发送方式
		 tTransferData.setNameAndValue("SendType",tSendType);
		 tVData.clear();
		 OperateDetail = "";
	 }
 }
 
 //*******************************************************************************************
 
 //客户健康建议录入
 if(transact != null && "CUSTSUGGEST||MAIN".equals(transact)) { 
	 //录入
	 if(tDisDetialOperate != null && "INSERT||CUST".equals(tDisDetialOperate)) {
		 
		 String tSel[] = request.getParameterValues("InpCustNotSugGridSel");
		 if(tSel != null) {
			 for(int index=0; index<tSel.length; index++) {
				 if(tSel[index].equals("1")) {
					 
					 String tCustomerNo[] = request.getParameterValues("CustNotSugGrid1");
					 String tName[] = request.getParameterValues("CustNotSugGrid2");
					 
					 tHMSuggestInfoSchema.setBusinessCode(tCustomerNo[index]);
					 tHMSuggestInfoSchema.setBusinessType("2");
					 tHMSuggestInfoSchema.setBusinessName(tName[index]);
					 tHMSuggestInfoSchema.setHealthSuggest(request.getParameter("CSuggest"));
					 break;
				 }
			 }
		 }
		 
		 tVData.clear();
		 tVData.add(tHMSuggestInfoSchema);
		 OperateDetail = "CustSuggestInput";
	 }
	 //修改
	 if(tDisDetialOperate != null && "UPDATE||CUST".equals(tDisDetialOperate)) {
		 tHMSuggestInfoSchema.setBusinessCode(request.getParameter("CCustomerNoForUp"));
		 tHMSuggestInfoSchema.setHealthSuggest(request.getParameter("CCustSuggest2"));
		 tVData.clear();
		 tVData.add(tHMSuggestInfoSchema);
		 OperateDetail = "";
	 }
	 //删除
	 if(tDisDetialOperate != null && "DELETE||CUST".equals(tDisDetialOperate)) {
		 tHMSuggestInfoSchema.setBusinessCode(request.getParameter("CCustomerNoForDel"));
		 tVData.clear();
		 tVData.add(tHMSuggestInfoSchema);
		 OperateDetail = "CustSuggestDel";
	 }
	 
	 //建议发送
	 if(tDisDetialOperate != null && "SUGGESTSEND||CUST".equals(tDisDetialOperate)) {
		 //健康建议发送轨迹表
		 HMSuggestSendTrackSet tHMSuggestSendTrackSet = new HMSuggestSendTrackSet();
		 //发送方式
		 String tCSendType = "";
		 
		 //是否逐条发送
		 String tFlag = request.getParameter("FLAG");
		 
		 String tSel[] = null;
		 if(tFlag != null && "SEL".equals(tFlag)) {
			 tCSendType = request.getParameter("CSendType");
			 tSel = request.getParameterValues("InpCustNotSugGridSel");
			 if(tSel != null) {
				 for(int index=0; index<tSel.length; index++) {
					 if(tSel[index].equals("1")) {
						 String tCustomerNo[] = request.getParameterValues("CustNotSugGrid1");
						 String tName[] = request.getParameterValues("CustNotSugGrid2");
						 String tMobile[] = request.getParameterValues("CustNotSugGrid3");
						 String tEMail[] = request.getParameterValues("CustNotSugGrid4");
						 String tHomeAddress[] = request.getParameterValues("CustNotSugGrid5");
						 String tLastVisEndDate[] = request.getParameterValues("CustNotSugGrid6");
						 String tLastYearVisCount[] = request.getParameterValues("CustNotSugGrid7");
						 String tVistType[] = request.getParameterValues("CustNotSugGrid8");
						 String tLastYearMedRealPay[] = request.getParameterValues("CustNotSugGrid10");
						 
						 
						 HMSuggestSendTrackSchema tHMSuggestSendTrackSchema = new HMSuggestSendTrackSchema();
						 
						 tHMSuggestSendTrackSchema.setBusinessType("2");
						 tHMSuggestSendTrackSchema.setBusinessCode(tCustomerNo[index]);
						 tHMSuggestSendTrackSchema.setCustomerNo(tCustomerNo[index]);
						 tHMSuggestSendTrackSchema.setSendType(tCSendType);
						 tHMSuggestSendTrackSchema.setName(tName[index]);
						 tHMSuggestSendTrackSchema.setMobile(tMobile[index]);
						 tHMSuggestSendTrackSchema.setEMail(tEMail[index]);
						 tHMSuggestSendTrackSchema.setHomeAddress(tHomeAddress[index]);
						 tHMSuggestSendTrackSchema.setLastVisEndDate(tLastVisEndDate[index]);
						 tHMSuggestSendTrackSchema.setLastYearVisCount(tLastYearVisCount[index]);
						 tHMSuggestSendTrackSchema.setLastYearMedRealPay(tLastYearMedRealPay[index]);
						 tHMSuggestSendTrackSchema.setVisType(tVistType[index]);
						 
						 tHMSuggestSendTrackSet.add(tHMSuggestSendTrackSchema);
					 }
				 }
			 }
			 
		 }
		 if(tFlag != null && "CHK".equals(tFlag)) {
			 tCSendType = request.getParameter("CSendType3");
			 tSel = request.getParameterValues("InpCustAlSugGridChk");
			 if(tSel != null) {
				 for(int index=0; index<tSel.length; index++) {
					 if(tSel[index].equals("1")) {
						 String tCustomerNo[] = request.getParameterValues("CustAlSugGrid2");
						 String tName[] = request.getParameterValues("CustAlSugGrid1");
						 String tMobile[] = request.getParameterValues("CustAlSugGrid3");
						 String tEMail[] = request.getParameterValues("CustAlSugGrid4");
						 String tHomeAddress[] = request.getParameterValues("CustAlSugGrid5");
						 String tLastVisEndDate[] = request.getParameterValues("CustAlSugGrid7");
						 String tLastYearVisCount[] = request.getParameterValues("CustAlSugGrid8");
						 String tVistType[] = request.getParameterValues("CustAlSugGrid10");
						 String tLastYearMedRealPay[] = request.getParameterValues("CustAlSugGrid11");
						 
						 HMSuggestSendTrackSchema tHMSuggestSendTrackSchema = new HMSuggestSendTrackSchema();
						 
						 tHMSuggestSendTrackSchema.setBusinessType("2");
						 tHMSuggestSendTrackSchema.setBusinessCode(tCustomerNo[index]);
						 tHMSuggestSendTrackSchema.setCustomerNo(tCustomerNo[index]);
						 tHMSuggestSendTrackSchema.setSendType(tCSendType);
						 tHMSuggestSendTrackSchema.setName(tName[index]);
						 tHMSuggestSendTrackSchema.setMobile(tMobile[index]);
						 tHMSuggestSendTrackSchema.setEMail(tEMail[index]);
						 tHMSuggestSendTrackSchema.setHomeAddress(tHomeAddress[index]);
						 tHMSuggestSendTrackSchema.setLastVisEndDate(tLastVisEndDate[index]);
						 tHMSuggestSendTrackSchema.setLastYearVisCount(tLastYearVisCount[index]);
						 tHMSuggestSendTrackSchema.setLastYearMedRealPay(tLastYearMedRealPay[index]);
						 tHMSuggestSendTrackSchema.setVisType(tVistType[index]);
						 
						 tHMSuggestSendTrackSet.add(tHMSuggestSendTrackSchema);
					 }
				 }
			 }
		 }
		 
		 tTransferData.setNameAndValue("CSendType",tCSendType);
		 tVData.clear();
		 tVData.add(tHMSuggestSendTrackSet);
		 OperateDetail = "";
	 }
	 
	 //建议发送（选中全部发送）
	 if(tDisDetialOperate != null && "SUGGESTSENDALL||CUST".equals(tDisDetialOperate)) {
		 
		 //查询条件
		 String tCQ_CustomerName = request.getParameter("CCustomerName2");
		 String tCQ_CCustomerNo = request.getParameter("CCustomerNo2");
		 String tCQ_CCustSuggestStartDate = request.getParameter("CCustSuggestStartDate2");
		 String tCQ_CCustSuggestEndDate = request.getParameter("CCustSuggestEndDate2");
		 String tCQ_CSendType = request.getParameter("CSendType2");
		 String tCQ_isCALSend = request.getParameter("isCALSend");
		 
		 //发送方式
		 String tCCSendType = request.getParameter("CSendType3");
		 
		 tTransferData.setNameAndValue("CQ_CustomerName",tCQ_CustomerName);
		 tTransferData.setNameAndValue("CQ_CCustomerNo",tCQ_CCustomerNo);
		 tTransferData.setNameAndValue("CQ_CCustSuggestStartDate",tCQ_CCustSuggestStartDate);
		 tTransferData.setNameAndValue("CQ_CCustSuggestEndDate",tCQ_CCustSuggestEndDate);
		 tTransferData.setNameAndValue("CQ_CSendType",tCQ_CSendType);
		 tTransferData.setNameAndValue("CQ_isCALSend",tCQ_isCALSend);
		 //发送方式
		 tTransferData.setNameAndValue("CCSendType",tCCSendType);
		 tVData.clear();
		 OperateDetail = "";
	 }
	 
 }
 
 //从url中取出参数付给相应的schema
 //t××××Schema.set××××(request.getParameter("××××"));
 
 try
 {
  
  //传输schema
  //tVData.addElement(t××××Schema);
  
  tVData.add(tG);
  tVData.add(tTransferData);
  tHMSuggestUI.submitData(tVData,transact);
  tStrResult = (String)tHMSuggestUI.getResult().getObjectByObjectName("String",0);
 }
 catch(Exception ex)
 {
  	ex.printStackTrace();
	Content = "操作失败，原因是:" + ex.toString();
  	FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr=="")
 {
  tError = tHMSuggestUI.mErrors;
  if (!tError.needDealError())
  {                          
   Content = " 操作成功! ";
   FlagStr = "Success";
  }
  else                                                                           
  {
   Content = " 操作失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
//置入返回结果
parent.fraInterface.setResult("<%=tStrResult%>");
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=OperateDetail%>");
</script>
</html>

