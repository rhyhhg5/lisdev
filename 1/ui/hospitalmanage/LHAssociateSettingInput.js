//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = ""
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHAssociateSettingInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}

function getAssociate()//
{
	// window.open("./LHContraAssoSettingContQuery.html","关联责任设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	 window.open("./LHContInputMain.jsp?setting=1","关联责任设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}  
function deleteClick()
{
	   //var rowNum2=LHContraAssoSettingGrid. mulLineCount ; //行数 
     ////下面增加相应的删除代码12责任号13合同号
     //var aa = new Array();
		 //var xx = 0;
		 //for(var row=0; row < rowNum2; row++)
			//{
			//   var tChk =LHContraAssoSettingGrid.getChkNo(row); 
			//		if(tChk == true)
			//		{
			//				aa[xx++] = row;
			//	  }
			//	  var ContraNo =LHContraAssoSettingGrid.getRowColData(aa[row],13);
		 //   var ContraItemNo  =LHContraAssoSettingGrid.getRowColData(aa[row],12);         
			//}	
			//alert(aa.length); 
      if (confirm("您确实想删除这些记录吗?"))
      {
         var i = 0;
         var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
         var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
         showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
         
         //showSubmitFrame(mDebug);
         fm.fmtransact.value = "DELETE||MAIN";
         fm.submit(); //提交
         initForm();
      }
      else
      {
        alert("您取消了删除操作！");
      }
}

function getUnitInfo()
{
	 returnParent();
}
function returnParent()
{
	var rowNum=LHContraAssoSettingGrid. mulLineCount ; //行数 
	//alert(rowNum);
	if( rowNum == 0 || rowNum == null )
	{
		 //alert( "请在出现的页面输入关联的个人信息!" );
		     var ContraNo = fm.all('ContraNo').value	;
	       var ContraItemNo=fm.all('ContraItemNo').value	;
	       //alert(ContraItemNo);
		     window.open("./LHContInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+ContraItemNo+"&setting=1&Flag=A","关联责任设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	
	}
	else{
		  var aa = new Array();
		  var xx = 0;
		  for(var row=0; row < rowNum; row++)
			{
			   var tChk =LHContraAssoSettingGrid.getChkNo(row); 	
		    // alert("AAAAA "+tChk);
					if(tChk == true)
					{
							aa[xx++] = row;
				  }
			}
			//alert(aa.length); 
			if(aa.length=="0" ||aa.length=="")
			{
		     // alert( "请在出现的页面输入关联的个人信息!" );
		     var ContraNo = fm.all('ContraNo').value	;
	       var ContraItemNo=fm.all('ContraItemNo').value	;
	       //alert(ContraItemNo);
		     window.open("./LHContInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+ContraItemNo+"&setting=1&Flag=A","关联责任设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	    }
			if(aa.length=="1")
			{   
				 // alert(aa[0]);
		      var ContraNo =LHContraAssoSettingGrid.getRowColData(aa[0],13);
		      var ContraItemNo  =LHContraAssoSettingGrid.getRowColData(aa[0],12);
		      //alert(ContraNo);
		      //alert(ContraItemNo);
		      window.open("./LHContInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+ContraItemNo+"&setting=1&Flag=B","关联责任设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		      //LHContraAssoSettingGrid.delCheckTrueLine ();
		  }
		  if(aa.length >"1")
		  {
		  	alert( "请选择一条关联设置信息进行修改!");
		  	return false;
		  } 
   }
} 
function getQueryResult()
{
	var arrSelected = null;
	var rowNum=LHContraAssoSettingGrid. mulLineCount ; //行数 
  var tChk =LHContraAssoSettingGrid.getChkNo(row); 	
	//alert("111" + tChk);
	if( tChk == 0 || tChk == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LHContraAssoSettingGrid.getRowData(tRow-1);
	
	return arrSelected;
}



          
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHAssociateSettingQueryInput.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}           
//Click事件，当点击“删除”图片时触发该函数
           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function clickOpen()
{
	if(LHContraAssoSettingGrid.getSelNo()!="")
	{
		 window.open("./LHContraAssoSettingContQuery.html","服务价格表设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	}
	else
	{
		 alert("请选择一条数据!");
	}
	
}

function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrResult != null )
	{
    var strSql="select a.contraname,(select LDDoctor.Doctno from LDDoctor  where LDDoctor.Doctno=a.Doctno),"
            +"(select DoctName from LDDoctor where LDDoctor.Doctno=a.Doctno),"
            +"(select Codename from ldcode where b.ContraType=ldcode.code  and ldcode.Codetype='hmdeftype'),"
            +"(select c.DutyItemName from LDContraItemDuty c where c.DutyItemCode = b.dutyItemCode),"
            +"(select case when b.dutystate = '1' then '有效' else '无效' end from dual),"
            +"(select case when b.Contraflag = 'Y' then '是' else '否' end from ldcode where Codetype='yesno' and b.Contraflag=ldcode.code),"
		        +"b.ContraType,c.DutyItemCode,b.DutyState,b.ContraFlag,"
		        +"b.ContraItemNo,a.contrano "
		        +" from lhunitcontra a,lhcontitem b,LDContraItemDuty c "
		        +"where a.contrano ='"+arrQueryResult[0][0]+"' and"
		        +" b.contrano= '"+arrQueryResult[0][0]+"' and c.DutyItemCode = b.DutyItemCode";
		     //   alert(strSql);  
		
		        arrResult = easyExecSql(strSql); 
         //   alert(arrResult);                                             
      
        turnPage.queryModal(strSql,LHContraAssoSettingGrid); 
        
	} 
}   
function afterQuery1(arrQueryResult)
{   
	 //alert(arrQueryResult);
	var arrResult = new Array(); 
	if( arrResult != null )
	{ 
				var mulSql = "select distinct a.ContraNo,"
						            +"(select distinct contraname from LHGroupCont c where c.contrano=a.contrano )," 
								        +"(select HospitName from LDHospital where  LDHospital.HospitCode=b.HospitCode),"
		                    +"(select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=c.ContraType),"
		                    +"(select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode),"
			                  +"( case when c.DutyState = '1' then '有效' else '无效' end ),"
			                  +"(select LDHospital.HospitCode from LDHospital  where LDHospital.HospitCode=b.HospitCode),"
			                  +"(select Code from ldcode where  codetype = 'hmdeftype' and ldcode.Code=c.ContraType),"
			                  +"(select DutyItemCode from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode),c.DutyState ,"
			                  +" a.ContraItemNo "
			                  +" from LHContraAssoSetting a ,LHGroupCont b, LHContItem c  "		                  
		                    +" where a.contrano ='"+arrQueryResult[0][1]+"' and  b.contrano=c.contrano"
		                    +" and a.contrano=b.contrano and a.ContraItemNo=c.ContraItemNo";
				    // alert(mulSql);
				 		 arrResult = easyExecSql(mulSql);
				  	//  alert(arrResult[0][10]); 
				    
		        fm.all('ContraNo').value							= arrResult[0][0];
		        fm.all('ContraName').value			      = arrResult[0][1];
		        fm.all('HospitName').value     	 			= arrResult[0][2];//合作机构名称
		        fm.all('ContraType_ch').value					= arrResult[0][3]; //合同责任项目类型名称
		        fm.all('ContraNowName_ch').value			= arrResult[0][4];// 合同责任项目名称
		        fm.all('ContraNowState_ch').value     = arrResult[0][5];
		        fm.all('HospitCode').value				  	= arrResult[0][6]; // 合同责任项目编号
		        fm.all('ContraType').value		      	= arrResult[0][7];
		        fm.all('ContraNowName').value 			  = arrResult[0][8];
		        fm.all('ContraNowState').value        = arrResult[0][9];
		        fm.all('ContraItemNo').value          = arrResult[0][10];
		
		
		       var mySql2="select (select distinct contraname from lhunitcontra c where c.contrano=a.IndivContraNo ),"
		                +" (select Doctno from LDDoctor where LDDoctor.Doctno=b.Doctno),"
		                +" (select DoctName from  LDDoctor where LDDoctor.Doctno=b.Doctno),"
		                +" (select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=c.ContraType),"
		                +" (select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode),"
		                +" (select case when c.dutystate = '1' then '有效' else '无效' end from dual),"
		                +" (select case when c.Contraflag = 'Y' then '是' else '否' end from ldcode where Codetype='yesno' and c.Contraflag=ldcode.code), "
		                +" c.ContraType, "
		                +" (select DutyItemCode from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode), "
		                +" c.DutyState,c.Contraflag,a.IndivContraItemNo,a.IndivContraNo "
		                +" from LHContraAssoSetting a ,lhunitcontra b, LHContItem c "
		                +" where  b.contrano=c.contrano  and a.contrano ='"+arrQueryResult[0][1]+"' "
		                +" and a.IndivContraNo=b.contrano and a.IndivContraItemNo=c.ContraItemNo"

		             //   +" where  b.contrano=c.contrano  and a.IndivContraNo ='"+arrQueryResult[0][4]+"' "
		             //   +" and a.IndivContraNo=b.contrano and a.IndivContraItemNo=c.ContraItemNo"
		   
		    //  alert(mySql2);
			//  alert(easyExecSql(mySql2));
					 turnPage.queryModal(mySql2,LHContraAssoSettingGrid); 
  }      
	
}  

function afterQuery2(contrano,ContraItemNo)
{
  //alert(contrano);
 
	var arrResult = new Array();
	
	if( arrResult != null )
	{
		  //var strSql="select a.contraname,(select LDDoctor.Doctno from LDDoctor  where LDDoctor.Doctno=a.Doctno),"
	    //        +"(select DoctName from LDDoctor where LDDoctor.Doctno=a.Doctno),"
	    //        +"(select Codename from ldcode where b.ContraType=ldcode.code  and ldcode.Codetype='hmdeftype'),"
	    //        +"(select c.DutyItemName from LDContraItemDuty c where c.DutyItemCode = b.dutyItemCode),"
	    //        +"(select case when b.dutystate = '1' then '有效' else '无效' end from dual),"
	    //        +"(select case when b.Contraflag = 'Y' then '是' else '否' end from ldcode where Codetype='yesno' and b.Contraflag=ldcode.code),"
			//        +"b.ContraType,c.DutyItemCode,b.DutyState,b.ContraFlag,"
			//        +"b.ContraItemNo,a.contrano "
			//        +" from lhunitcontra a,lhcontitem b,LDContraItemDuty c "
			//        +"where a.contrano ='"+ contrano +"'and b.ContraItemNo='"+ ContraItemNo +"' and "
			//        +" b.contrano= '"+ contrano +"' and c.DutyItemCode = b.DutyItemCode";
			// // alert(strSql); and b.ContraItemNo='"+ ContraItemNo +"'
			
			 var ContraNoA=fm.all('ContraNo').value;
     var ContraItemNoA=fm.all('ContraItemNo').value;
		
		 var strSql="select (select distinct contraname from lhunitcontra c where c.contrano=a.IndivContraNo ),"
		                +" (select Doctno from LDDoctor where LDDoctor.Doctno=b.Doctno),"
		                +" (select DoctName from  LDDoctor where LDDoctor.Doctno=b.Doctno),"
		                +" (select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=c.ContraType),"
		                +" (select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode),"
		                +" (select case when c.dutystate = '1' then '有效' else '无效' end from dual),"
		                +" (select case when c.Contraflag = 'Y' then '是' else '否' end from ldcode where Codetype='yesno' and c.Contraflag=ldcode.code), "
		                +" c.ContraType, "
		                +" (select DutyItemCode from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode), "
		                +" c.DutyState,c.Contraflag,a.IndivContraItemNo,a.IndivContraNo "
		                +" from LHContraAssoSetting a ,lhunitcontra b, LHContItem c "
		                +" where  b.contrano=c.contrano  and a.contrano ='"+ ContraNoA +"' "
		                +" and a.ContraItemNo ='"+ ContraItemNoA +"' "
		                +" and a.IndivContraNo=b.contrano and a.IndivContraItemNo=c.ContraItemNo"
			
			 arrResult = easyExecSql(strSql); 
			  //alert(arrResult);
			 LHContraAssoSettingGrid.addOne();
			 turnPage.queryModal(strSql,LHContraAssoSettingGrid);
			  
			 
			// var rowNum=LHContraAssoSettingGrid.mulLineCount ; //行数 
		  // for(var row=0; row<arrResult[0].length; row++)
      // { 
			//   //	alert(arrResult[0][row]);  
			//   //  alert(arrResult[0][row+1]);
	    //    LHContraAssoSettingGrid.setRowColData(rowNum-1,row+1,arrResult[0][row]);
      // }
      //  
	}   
}     
