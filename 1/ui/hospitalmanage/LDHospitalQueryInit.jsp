<%
//程序名称：LDHospitalQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-15 14:25:18
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() 
{ 
	try 
	{     
	    fm.all('HospitCode').value = "";
	    fm.all('HospitName').value = "";
	    fm.all('AreaCode').value = "";
	    fm.all('LevelCode').value = "";
		fm.all('Operator').value = "";
		fm.all('LastModifyDate').value = "";
	}
	catch(ex) 
	{
		alert("在LDHospitalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}   
                                 
function initForm() 
{
	try 
	{
		initInpBox();
		initLDHospitalGrid();  
	}
	catch(re) 
	{
		alert("LDHospitalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}
//领取项信息列表的初始化
var LDHospitalGrid;
function initLDHospitalGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         			//列名
	    iArray[0][4]="station";         	//列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="管理机构";   
		iArray[1][1]="50px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;  
		
		iArray[2]=new Array(); 
		iArray[2][0]="医院代码";   
		iArray[2][1]="50px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="医院名称";   
		iArray[3][1]="200px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="医院级别";   
		iArray[4][1]="60px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
		iArray[5]=new Array(); 
		iArray[5][0]="是否社保定点";   
		iArray[5][1]="50px";   
		iArray[5][2]=20;        
		iArray[5][3]=0; 
		
		iArray[6]=new Array(); 
		iArray[6][0]="合作等级";   
		iArray[6][1]="60px";   
		iArray[6][2]=20;        
		iArray[6][3]=0;   

		iArray[7]=new Array();      
		iArray[7][0]="合同效力";    
		iArray[7][1]="60px";   
	    iArray[7][2]=20;        
	    iArray[7][3]=0;     
    
	    iArray[8]=new Array();      
	    iArray[8][0]="设立时间";    
	    iArray[8][1]="80px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=0;     
	    
	    iArray[9]=new Array(); 
		iArray[9][0]="取消时间";   
		iArray[9][1]="80px";   
		iArray[9][2]=20;        
		iArray[9][3]=0;								
				        
	    LDHospitalGrid = new MulLineEnter( "fm" , "LDHospitalGrid" ); 
	    //这些属性必须在loadMulLine前
	    
	    LDHospitalGrid.mulLineCount = 0;   
	    LDHospitalGrid.displayTitle = 1;
	    LDHospitalGrid.hiddenPlus = 1;
	    LDHospitalGrid.hiddenSubtraction = 1;
	    LDHospitalGrid.canSel = 1;
	    LDHospitalGrid.canChk = 0;
	    LDHospitalGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LDHospitalGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
	}
}
</script>
