<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>

<%
 //程序名称：HMDisCaseManageInput.jsp
 //程序功能：疾病案例管理
 //创建日期：2010-3-4
 //创建人  ：chenxw
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG1 = (GlobalInput)session.getValue("GI");
	String tManageCom = tG1.ManageCom;
%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="HMDisCaseManage.js"></SCRIPT>
  <%@include file="HMDisCaseManageInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./HMDisCaseManageSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >目标客户提取</td>
  </tr>
</table>
<table  class="common" >
  <tr class="common">
    <td class="title">管理机构</td>
    <td>
      <Input class=codeNo  name=ManageCom2 verify="管理机构|code:comcode&NOTNULL" value="<%=tManageCom%>"
      		 ondblclick="return showCodeList('comcode',[this,ManageComName2],[0,1],null,null,null,1);" 
      		 onkeyup="return showCodeListKey('comcode',[this,ManageComName2],[0,1]);"
      ><input class=codename name=ManageComName2 >
    </td>
    <td class="title">业务渠道</td>
    <td>
	    <Input class=codeNo  name=ChannelType 
	    		 CodeData= "0|^1|团险^2|个险^3|银行渠道"
	      		 ondblclick="return showCodeListEx('ChannelType',[this,ChannelTypeName],[0,1],null,null,null,1);" 
	      		 onkeyup="return showCodeListKeyEx('ChannelType',[this,ChannelTypeName],[0,1]);"
	      ><input class=codename name=ChannelTypeName >
	</td>	   
  </tr>
  <tr class="common">
	    <td class="title">就诊结束时间起期</td>
	    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="VisFinStartDate2"></td>  
	    <td class="title">就诊结束时间止期</td>
	    <td class="input"><input class="coolDatePicker" dateFormat="short" name="VisFinEndDate2" ></td>  
  </tr>
  <tr class="common">
    <td class="title">理赔病种名称</td>
    <td class="input">
    	<input class="common" name="DiseaseName2"></td>
    <td class="title">理赔病种编码</td>
    <td class="input" ><input class="code" name="DiseaseCode2"
    	ondblclick="return showCodeList('lldiseas',[this,DiseaseName2],[1,0],null,fm.DiseaseName2.value,'icdname',1);" 
      	onkeyup="return showCodeListKey('lldiseas',[this,DDiseaseName2],[1,0]);"
     ></td>
  </tr>
  <tr class="common">
    <td class="title">最近一年医疗费用理赔金额</td>
    <td class="code">
    	<input class="codeNo" name="HCostClaimFeeCode2"
    			CodeData="0|^1|1万元以上|10000^2|2万元以上|20000^3|5万元以上|50000^4|10万元以上|100000^5|15万元以上|150000"
    			ondblclick="return showCodeListEx('HCostClaimFeeCode',[this,HCostClaimFeeName,HCostClaimFee],[0,1,2],null,null,null,1);"
    			onkeyup="return showCodeListEx('HCostClaimFeeCode',[this,HCostClaimFeeName,HCostClaimFee],[0,1,2],null,null,null,1);"
    	><input class="codeName" name="HCostClaimFeeName"><input type='hidden' name="HCostClaimFee">
    </td>  
    <td class="title">投保人</td><td class="input"><input class="common" name="AppntName2" ></td>  
  </tr>
  <tr class="common">
    <td class="title">疾病身故风险保额</td>
    <td class="code">
    	<input class="codeNo" name="DisDieAmountType2"
    			CodeData="0|^1|10万元以上|100000^2|15万元以上|150000^3|20万元以上|200000^4|30万元以上|300000^5|50万元以上|500000^6|100万元以上|1000000^7|200万元以上|2000000"
    			ondblclick="return showCodeListEx('DisDieAmountType',[this,DisDieAmountName2,DisDieAmount],[0,1,2],null,null,null,1);"
    			onkeyup="return showCodeListEx('DisDieAmountType',[this,DisDieAmountName2,DisDieAmount],[0,1,2],null,null,null,1);"
    	><input class="codeName" name="DisDieAmountName2"><input type='hidden' name="DisDieAmount">
    </td>  
    <td class="title">疾病定额给付风险保额</td>
    <td class="code">
    	<input class="codeNo" name="DisRationType2"
    			CodeData="0|^1|20万元以上|200000^2|30万元以上|300000^3|50万元以上|500000^4|100万元以上|1000000^5|200万元以上|2000000"
    			ondblclick="return showCodeListEx('DisRationType2',[this,DisRationName2,DisRationAmount],[0,1,2],null,null,null,1);"
    			onkeyup="return showCodeListEx('DisRationType2',[this,DisRationName2,DisRationAmount],[0,1,2],null,null,null,1);"
    	><input class="codeName" name="DisRationName2"><input type='hidden' name="DisRationAmount">
    </td>  
    
  </tr>
  <tr>
  		<td class="title">客户姓名</td><td class="input"><input class="common" name="Customer2" ></td>
  		<td class="title">客户号码</td><td class="input"><input class="common" name="CustomerNo2" ></td>
  </tr>
  <tr>
    <td class="title">证件类型</td>
    <td class="input">
    	<input class="codeNo" name="IDType2" 
    	 ondblclick="return showCodeList('idtype',[this,IDTypeName2],[0,1],null,null,null,1);" 
	     onkeyup="return showCodeListKey('idtype',[this,IDTypeName2],[0,1]);"
	    ><input class=codename name=IDTypeName2 >
    </td>  
    <td class="title">证件号码</td><td class="input"><input class="common" name="IDNo2" ></td>  
  </tr>
   <tr class="common">
     <td class="title">理赔案件号</td><td class="input"><input class="common" name="CaseNo2" ></td>  
     
     <!--<td class="title">保单号</td><td class="input"><input class="common" name="ContNo2" ></td> --> 
  </tr>
  
</table>
<input value="目标客户提取"  onclick="return custumerFetch();" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >客户提取信息</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanCustomerInfoGrid" >
     </span> 
      </td>
   </tr>
</table>
<input class=cssbutton value="首  页" type=button onclick="CustomerInfoTurnPage.firstPage();"> 
<input class=cssbutton value="上一页" type=button onclick="CustomerInfoTurnPage.previousPage();">      
<input class=cssbutton value="下一页" type=button onclick="CustomerInfoTurnPage.nextPage();"> 
<input class=cssbutton value="尾  页" type=button onclick="CustomerInfoTurnPage.lastPage();">
<br><br>
<input value="添加到客户档案"  onclick="return custArchives();" class="cssButton" type="button" >
<br>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
