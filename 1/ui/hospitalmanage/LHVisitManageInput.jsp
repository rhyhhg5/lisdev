<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2010-01-15 14:25:18
//创建人  ：caosg程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LHVisitManageInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="LHVisitManageInputInit.jsp"%>
</head>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
%>
<script>
	var comCode = "<%=tG.ComCode%>";
	var manageCom = "<%=tG.ManageCom%>";
	var operator = "<%=tG.Operator%>";
	var msql = "1 and char(length(trim(comcode)))=#8# ";
</script>

<body onload="initForm(); initElementtype();">
	<form action="./LHVisitManageSave.jsp" method=post name=fm target="fraSubmit">		
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divVisitMng);">
				</td>
				<td class=titleImg>探访管理</td>
			</tr>
		</table>
		<Div id="divVisitMng" style="display: ''">		
	 		<table  class= common  >
	 			<TR class= common>
	 				<TD class=title>&nbsp;&nbsp&nbsp;&nbsp					
		  	 		<Input value="探访条件设定" type=button class=cssbutton onclick="VisitCondition();">&nbsp;&nbsp
		  	 		<Input value="探 访 记 录" type=button class=cssbutton  onclick="VisitInfo();"><hr>
	 	 			</TD>
	 	 		</TR>
			 </table>		
		</Div>
		<Div id="div_VisitCondition" style="display: 'none'">
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divCondition);">
					</td>
					<td class=titleImg>探访条件设定</td>
				</tr>
			</table>
			<Div id="divCondition" style="display: ''">
				<table class=common align='center'>
					<TR class=common>
						<TD class=title>管理机构</TD>
						<TD class=input>
							<Input class="codeno" name=MngCom ondblclick="return showCodeList('comcode',[this,MngCom_ch],[0,1],null,'','',1);"
								onkeyup="return showCodeListKey('comcode',[this,MngCom_ch],[0,1],null,'','',1);"	
								readonly verify="管理机构|code:comcode&NOTNULL"><Input class=codename name=MngCom_ch>
						</TD>
						<TD class=title></TD>
						<TD class=input></TD>
						<TD class=title></TD>
						<TD class=input></TD>										
					</TR>
				</table>
				<br>
					<Input value="查   询" type=button class=cssbutton onclick="queryConData();">		
				<br><br>	
			</Div>						
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divVisitCondition);">
					</td>
					<td class=titleImg>机构探访条件</td>
				</tr>
			</table>	
			<Div id="divVisitCondition" style="display:'' ">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanHMVisitConditionGrid"> </span>
						</td>
					</tr>
				</table>
			</div>	
			<br>
			<Input value="保   存" type=button name = 'save_con' class=cssbutton onclick="saveConData();">	
			<Input value="删   除" type=button name = 'dele_con' class= cssbutton onclick="deleConData();">				
		</Div>
		
		<Div id="div_VisitInfo" style="display: 'none'">
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divInfo);">
					</td>
					<td class=titleImg>探访记录</td>
				</tr>
			</table>
			<Div id="divInfo" style="display: ''">
				<table class=common align='center'>
					<TR class=common>
						<TD class=title>管理机构</TD>
						<TD class=input>
							<Input class="codeno" name=MngCom_Info	ondblclick="return showCodeList('comcode',[this,MngComName_Info],[0,1],null,'','',1);"
								onkeyup="return showCodeListKey('comcode',[this,MngComName_Info],[0,1],null,'','',1);"	
								readonly verify="管理机构|code:comcode&NOTNULL"><Input class=codename name=MngComName_Info elementtype=nacessary>
						</TD>
						<TD class=title>客户号码</TD>
						<TD class=input >
							<input class='common' name=CustomerNo verify="客户号码|NOTNULL" elementtype=nacessary >
						</TD>
						<TD class=title>客户姓名</TD>
						<TD class=input >
							<input class='common' name=CustomerName verify="客户姓名|NOTNULL" elementtype=nacessary >
						</TD>										
					</TR>
				</table>
				<br>
					<Input value="查   询" type=button class=cssbutton onclick="queryVisitInfo();">		
				<br><br>	
			</Div>						
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divVisitInfo);">
					</td>
					<td class=titleImg>探访记录信息</td>
				</tr>
			</table>	
			<Div id="divVisitInfo" style="display:'' ">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanHMVisitInfoGrid"> </span>
						</td>
					</tr>
				</table>
			</div>	
			<br>
			<Input value="保   存" type=button name = 'save_visit' class=cssbutton onclick="saveVisitInfo();">	
			<Input value="删   除" type=button name = 'dele_visit' class=cssbutton onclick="deleVisitInfo();">		
		</Div>
		
		<!-- 隐藏域 -->
		<Input type=hidden name=Count >	
		<Input type=hidden name=fmtransact >
		<Input type=hidden name=OperType ><!--  01-探访条件操作；02探访记录操作	-->
	</form>	
	
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>

</html>
