//               该文件中包含客户端需要处理的函数和事件
var ImportPath;
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var selectedSpecSecDiaRow; 
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();  
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if(!getAssociateClass())
	{
		return false;
	}
		
	var i = 0;
	
	if(fm.HospitalType.value==null||fm.HospitalType.value=="")
	{		
		alert('请选择"医疗服务机构类型" !');
		return false;
	}
	if(fm.HospitName.value==null ||fm.HospitName.value=="")
	{
		alert('请录入"医疗服务机构名称" !');
		return false;	
	}
	
	if(fm.HospitCode.value==null ||fm.HospitCode.value=="")
	{
		alert('请录入"医疗服务机构代码" !');
		return false;	
	}
	if(fm.AreaCode.value==null ||fm.AreaCode.value=="")
	{
		alert('请选择"地区名称" !');
		return false;	
	}
	if(fm.MngCom.value==null ||fm.MngCom.value=="")
	{
		alert('请选择"管理机构" !');
		return false;	
	}
	if(fm.MngCom.value.length<8)			
	{
		alert('医院归属必须到三级机构，请修改管理机构');
		return false;	
	}
	if(fm.HospitalType.value==1)
	{
		if( verifyInput2() == false ) return false;
	}
	
	//相同信息校验
	if(!uniquenessCheck())
	{
		return false;
	} 
	
	fm.fmtransact.value="INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action='./LDHospitalSave.jsp';
	fm.submit(); //提交
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
	//下面增加相应的代码
	//mOperate="INSERT||MAIN";
	showDiv(operateButton,"false"); 
	showDiv(inputButton,"true"); 
	fm.fmtransact.value = "INSERT||MAIN" ;
} 
          
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(!getAssociateClass())
	{
		return false;
	}
	
	if(fm.HospitalType.value==null||fm.HospitalType.value=="")
	{		
		alert('请选择"医疗服务机构类型" !');
		return false;
	}
	if(fm.HospitName.value==null ||fm.HospitName.value=="")
	{
		alert('请录入"医疗服务机构名称" !');
		return false;	
	}
	
	if(fm.HospitCode.value==null ||fm.HospitCode.value=="")
	{
		alert('请录入"医疗服务机构代码" !');
		return false;	
	}
	if(fm.AreaCode.value==null ||fm.AreaCode.value=="")
	{
		alert('请选择"地区名称" !');
		return false;	
	}
	if(fm.MngCom.value==null ||fm.MngCom.value=="")
	{
		alert('请选择"管理机构" !');
		return false;	
	}	
	
	if(fm.HospitalType.value==1)
	{
		if( verifyInput2() == false ) return false;
	}
	if(fm.MngCom.value.length<8)			
	{
		alert('医院归属必须到三级机构，请修改管理机构');
		return false;	
	}
	//相同信息校验
	if(!uniquenessCheck())
	{
		return false;
	} 
	if (confirm("您确实想修改该记录吗?"))
	{
		var i = 0;
		fm.fmtransact.value = "UPDATE||MAIN";
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.submit(); //提交
	}
	else
	{
		alert("您取消了修改操作！");
	}
} 

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	//下面增加相应的代码
	//mOperate="QUERY||MAIN";
	showInfo=window.open("./LDHospitalQuery.html");
}  
         
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if(fm.all('MakeDate').value == "")
	{
		alert("无法删除，请重新查询一条信息");
		return false;
	}
	//下面增加相应的删除代码
	if (confirm("您确实想删除该记录吗?"))
	{
		var i = 0;
		var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		fm.fmtransact.value = "DELETE||MAIN";
		fm.submit(); //提交
		initForm();
	}
	else
	{
		alert("您取消了删除操作！");
	}
}  

function getAssociateClass()
{
	//下面增加相应的代码 
	if(fm.all('HospitalType').value == "1")
	{	
		var ComHosGridMaxRow = ComHospitalGrid.mulLineCount;
		if(ComHosGridMaxRow == "0")
		{
			alert("请填写推荐医院记录信息");	
			return false;
		}
		else
		{
			var ComInfo = ComHospitalGrid.getRowColData(ComHosGridMaxRow-1,1);
			if(ComInfo == "推荐医院")
			{
				fm.all('AssociateClass_ch').value = ComInfo;
				fm.all('AssociateClass').value = "1";	
			}
			else if(ComInfo == "指定医院")
			{
				fm.all('AssociateClass_ch').value = ComInfo;
				fm.all('AssociateClass').value = "2";	
			} 
			else if(ComInfo == "非指定医院")
			{
				fm.all('AssociateClass_ch').value = ComInfo;
				fm.all('AssociateClass').value = "3";	
			}
			else if(ComInfo == "指定口腔医疗机构")
			{
				fm.all('AssociateClass_ch').value = ComInfo;
				fm.all('AssociateClass').value = "4";	
			} 	
			else
			{
			    alert("请填写正确医院级别信息");	
			    return false;
			}
		}
	}
	return true;
}

//信息唯一性检查
//管理机构下，医院名称、详细地址、联系电话有任意一个与系统中原有数据相同,不保存
function uniquenessCheck()
{
	
	var sql = " select count(1) from LDHospital where  ManageCom = '"+fm.MngCom.value+"' and (Phone = '"+fm.Phone.value+"' or Address='"+fm.Address.value+"' or HospitName = '"+fm.HospitName.value+"')" ;
	var count = easyExecSql(sql)
	
	if(count>=1)
	{
		alert("机构"+fm.MngCom.value+"已经存在该医院信息，请修改后重新保存！");
		return false;
	}
	return true;
}

//医院信息导入
function HospitalListUpload()
{

	getImportPath();
	
	var showStr="正在上载数据……";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fmlode.action = "./HospitalInfoSave.jsp?ImportPath="+ImportPath;
	fmlode.submit(); //提交
  
}

function getImportPath () 
{
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	
	//判断是否查询成功
	if (!turnPage.strQueryResult) 
	{
		alert("未找到上传路径");
		return;
	}
	//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	ImportPath = turnPage.arrDataCacheSet[0][0];

}

//核保体检医院
function PayQuery()
{
	if(fm.examination.checked)
	{
		fm.examination.value = '1'; //核保体检医院
	}
	else
	{
		fm.examination.value = '0';
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	initForm()
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		showDiv(operateButton,"true"); 
		showDiv(inputButton,"false"); 
		//执行下一步操作
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在LDHospital.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
                  
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	initForm();
	if( arrQueryResult != null )
	{
		//以后再添加更多的条件
		arrResult = easyExecSql("select * from LDHospital where  HospitCode='"+arrQueryResult[0][1]+"'",1,0);
        //arrResult = arrQueryResult;
        
        //名称设置
        fm.all('HospitalType').value = arrResult[0][23];  		
		if(arrResult[0][23] == 1 )fm.all('HospitalType_ch').value = "医疗机构";	
		if(arrResult[0][23] == 2 )fm.all('HospitalType_ch').value = "健康体检机构";
		if(arrResult[0][23] == 3 )fm.all('HospitalType_ch').value = "健康管理公司";
		if(arrResult[0][23] == 4 )fm.all('HospitalType_ch').value = "紧急救援公司";	
		if(arrResult[0][23] == 5 )fm.all('HospitalType_ch').value = "指定口腔医疗机构";			
		if(arrResult[0][23] == 6 )fm.all('HospitalType_ch').value = "其他合作机构";
		               
        fm.all('HospitCode').value= arrResult[0][0];
        fm.all('HospitName').value= arrResult[0][1];       
		fm.all('HospitShortName').value = arrResult[0][3] ;  
	    fm.all('MngCom').value =        arrResult[0][43];
	    fm.all('MngCom_ch').value = easyExecSql("select name from ldcom where comcode='"+arrResult[0][43]+"'");		   
        fm.all('AreaCode').value= arrResult[0][5];
        fm.all('AreaName').value = 	 arrResult[0][22] ;  
        //一般属性         
	    fm.all('Address').value = arrResult[0][10];
	    fm.all('ZipCode').value = arrResult[0][11];		    
	    fm.all('Phone').value =   arrResult[0][12];    
	    fm.all('WebAddress').value = arrResult[0][13]; 		    		    	                
	    fm.all('Route').value =  arrResult[0][45];	          		    
        fm.all('MakeDate').value = arrResult[0][27];
		fm.all('MakeTime').value = arrResult[0][28];
		
		var HospitalTypeTemp = fm.all('HospitalType').value;	
					
		//当医疗服务机构类型为医院时的操作
		if(HospitalTypeTemp == 1||HospitalTypeTemp == 5)
		{
			servPart.style.display='';
			CurrentlyInfo.style.display='';
			divcomHospital1.style.display='';
			hospitalSrcInfo.style.display='';
						
		    fm.all('SecurityNo').value = arrResult[0][46];					
			fm.all('HospitalStandardCode').value =arrResult[0][31]; 
			//examination				
		    fm.all('PatientPerDay').value = arrResult[0][32];
		    fm.all('OutHospital').value = 	arrResult[0][33];	
		    fm.all('BedAmount').value =  arrResult[0][38];			    
		    fm.all('examination').value =  arrResult[0][47];
		    if(arrResult[0][47]=="1")
		    {
		    	fm.examination.checked = true;
		    }
		    else
		    {
		    	fm.examination.checked = false;
		    }		    
		    		    
		    fm.all('CommunFixFlag').value = 	arrResult[0][4] ;
		    if(arrResult[0][4]=="0")
		    {
		    	fm.all('CommunFixFlag_ch').value="社保非定点医院";
		    }		    
		    if(arrResult[0][4]=="1")
		    {
		    	fm.all('CommunFixFlag_ch').value="社保定点医院";
		    }	
		    	    
	        fm.all('LevelCode').value= arrResult[0][6];
		    var tmepLevelCode = easyExecSql("select codename from ldcode where codetype='hospitalclass' and code = '"+arrResult[0][6]+"'");
		    fm.all('LevelCode_ch').value= tmepLevelCode==null?"":tmepLevelCode;

		    fm.all('AdminiSortCode').value =  arrResult[0][7] ;
		    var tempAdminiSortCode = easyExecSql("select codename from ldcode where codetype='hmadminisortcode' and code = '"+arrResult[0][7]+"'");
		    fm.all('AdminiSortCode_ch').value= tempAdminiSortCode==null?"":tempAdminiSortCode;	
		   
		    fm.all('BusiTypeCode').value = 	arrResult[0][8];
		    var tempBusiTypeCode = easyExecSql("select codename from ldcode where codetype='hmbusitype' and code = '"+arrResult[0][8]+"'");
		    fm.all('BusiTypeCode_ch').value= tempBusiTypeCode==null?"":tempBusiTypeCode;
		  		    
		    fm.all('EconomElemenCode').value =arrResult[0][9] ;		    
		    var tempEconomElemenCode=easyExecSql("select codename from ldcode where codetype='hmeconomelemencode' and code = '"+arrResult[0][9]+"'");
		    fm.all('EconomElemenCode_ch').value= tempEconomElemenCode==null?"":tempEconomElemenCode;

		    if( manageCom.substring(0,4)=="8653" || manageCom=="86")
		    {
			    if(arrResult[0][6]=="00" || arrResult[0][6]=="10" || arrResult[0][6]=="11" )
			    {
					UrbanName.style.display='';
					UrbanOpt.style.display='';
				    fm.all('UrbanOption').value = arrResult[0][44];
				    if(arrResult[0][44]=="0"){fm.all('UrbanOptionName').value = "非城区"}
				    if(arrResult[0][44]=="1"){fm.all('UrbanOptionName').value = "城区"}
				}
		    }
		    
			var tempAssociateClass_ch = easyExecSql("select distinct Associatclass from ldcomhospital where hospitcode = '"+arrResult[0][0]+"' and startdate = ( select distinct max(startdate) from ldcomhospital where hospitcode = '"+arrResult[0][0]+"')");
			if(tempAssociateClass_ch == "" || tempAssociateClass_ch == "null" || tempAssociateClass_ch == null)
			{
				fm.all('AssociateClass').value = arrResult[0][24];
				var tempAssociateClass = easyExecSql("select codename from ldcode where codetype='llhospiflag' and code = '"+arrResult[0][24]+"'");
				fm.all('AssociateClass_ch').value= tempAssociateClass==null?"":tempAssociateClass;	
			}
			else
			{
				fm.all('AssociateClass_ch').value = tempAssociateClass_ch;
				var tempAssociateClass = easyExecSql("select code from ldcode where codetype='llhospiflag' and codename = '"+tempAssociateClass_ch+"'");
				fm.all('AssociateClass').value= tempAssociateClass==null?"":tempAssociateClass;
			}						
					                               
		    var GridSQL1 = "select (select d.codename from ldcode d where d.code=a.SpecialName and codetype='lhspecname'),"
		                 +" SpecialIntro,SpecialClass,FlowNo,"
		                 +" SpecialName "
		                 +" from LDHospitalInfoIntro a where SpecialClass = 'SpecSecDia' " + getWherePart("HospitCode", "HospitCode");
		    turnPage.pageLineNum = 100;  
		    turnPage.queryModal(GridSQL1, SpecSecDiaGrid);
		    
		    var GridSQL2 = "select SpecialName,SpecialIntro,SpecialClass,FlowNo from LDHospitalInfoIntro where SpecialClass = 'Instrument' " + getWherePart("HospitCode", "HospitCode");
			    turnPage.pageLineNum = 100;  
			    turnPage.queryModal(GridSQL2, InstrumentGrid);
		    
		    var GridSQL3 = "select AssociatClass,StartDate,EndDate,SerialNo,HospitCode from LDComHospital where 1=1 "+ getWherePart("HospitCode", "HospitCode") + " order by startdate ";
				turnPage.pageLineNum = 20;  
				turnPage.queryModal(GridSQL3, ComHospitalGrid);
		}
	}
}  

             
function getSuperHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    fm.all("SuperHospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}        



//选择类型为医院时的处理
function afterCodeSelect(codeName,Field)
{
	if(codeName=="HospitalType")
	{
		if(fm.HospitalType.value=="1"||fm.HospitalType.value=="5")
		{
			fm.all('AssociateClass').value = '';
			fm.all('AssociateClass_ch').value = '';
			servPart.style.display='';
			hospitalSrcInfo.style.display='';
			CurrentlyInfo.style.display='';
			divcomHospital1.style.display='';
		}
		else
		{
			fm.all('AssociateClass').value = '0';
			fm.all('AssociateClass_ch').value = '0';
			fm.all('CommunFixFlag').value='';
			fm.all('LevelCode').value='';
			fm.all('AdminiSortCode').value='';
			fm.all('EconomElemenCode').value='';
			fm.all('BusiTypeCode').value='';
			servPart.style.display='none';
		 	hospitalSrcInfo.style.display='none';
		 	divcomHospital1.style.display='none';
		}
	}
	if(codeName=="hmareacode" || codeName=="FlowNo" || codeName=="CommunFixFlag" || codeName=="hmadminisortcode" || codeName=="hmeconomelemencode" || codeName=="hospitalclass" || codeName=="hmBusiType")
	{
			fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
	}
	
	if(codeName == "hospitalclass" && manageCom.substring(0,4)=="8653")
	{
		if(fm.LevelCode.value == "00" || fm.LevelCode.value == "10" || fm.LevelCode.value == "11" )
		{
			UrbanName.style.display='';
			UrbanOpt.style.display='';
		}
		else
		{
			UrbanName.style.display='none';
			UrbanOpt.style.display='none';
			fm.all('UrbanOptionName').value = "";
			fm.all('UrbanOption').value = "";
		}
	}
}

//生成医院的小流水号,    08-12-2 放弃不用。

function generateFlowNo()
{
		var str = "select max(hospitcode) from ldhospital where hospitcode like '"+fm.AreaCode.value+"%%'";
		var shortFlowNo = easyExecSql(str);
		if(fm.AreaCode.value=="")
		{
				alert("请先输入地区代码");
		}
	  else
	  {
	  	if(shortFlowNo == null || shortFlowNo=="")
				{
						fm.FlowNo.value="001";
						fm.HospitCode.value = fm.AreaCode.value+"001";
						fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
				}
				else
				{
						fm.HospitCode.value = parseInt(shortFlowNo)+1;
						fm.FlowNo.value= fm.HospitCode.value.substr(4,3);
						fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
				}
		}
}

function getNoNew()
{
	if(fm.AreaCode.value==null || fm.AreaCode.value=="")
	{
		alert("请先选择“地区名称”");
		return;
	}
	
	fm.action="./LDHospitalInputGetCode.jsp?AreaCode="+fm.AreaCode.value;
	fm.submit();
}

function inputSpecSecDia(a)
{ 
	
	divSpecSecDia.style.display='';
	var tempSpecSecDia = fm.all(a).all('SpecSecDiaGrid2').value;
	selectedSpecSecDiaRow = fm.all(a).all('SpecSecDiaGridNo').value-1;
	fm.all('SpecSecDia').value=tempSpecSecDia;
	
}
function backSpecSecDia()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		SpecSecDiaGrid.setRowColData(selectedSpecSecDiaRow,2,fm.SpecSecDia.value);
		fm.SpecSecDia.value="";
		divSpecSecDia.style.display='none';	
	}
} 
function backSpecSecDia2()
{
		SpecSecDiaGrid.setRowColData(selectedSpecSecDiaRow,2,fm.SpecSecDia.value);
		fm.SpecSecDia.value="";
		divSpecSecDia.style.display='none';	
}



function inputInstrument(a)
{ 
	
	divInstrument.style.display='';
	var tempInstrument = fm.all(a).all('InstrumentGrid2').value;
	selectedInstrumentRow = fm.all(a).all('InstrumentGridNo').value-1;
	fm.all('Instrument').value=tempInstrument;
	
}
function backInstrument()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		InstrumentGrid.setRowColData(selectedInstrumentRow,2,fm.Instrument.value);
		fm.Instrument.value="";
		divInstrument.style.display='none';	
	}
} 
function backInstrument2()
{
		InstrumentGrid.setRowColData(selectedInstrumentRow,2,fm.Instrument.value);
		fm.Instrument.value="";
		divInstrument.style.display='none';	
}
