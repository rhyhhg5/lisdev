<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDHospitalSave.jsp
//程序功能：
//创建日期：2005-01-15 14:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.hospitalmanage.*"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	
	LDAreaHospitalSet tLDAreaHospitalSet   = new LDAreaHospitalSet();
	LDAreaHospitalUI tLDAreaHospitalUI   = new LDAreaHospitalUI();
	TransferData tTransferData = new TransferData();
	
	System.out.println("---------接收信息，并作校验处理。------------");
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";	 
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	String transact = request.getParameter("fmtransact");
	String tMngCom = request.getParameter("MngCom");
	String tCount = request.getParameter("Count");
	String tComInfo = request.getParameter("comInfo");	
	tTransferData.setNameAndValue("MngCom",tMngCom);
	tTransferData.setNameAndValue("fmtransact",transact);
	tTransferData.setNameAndValue("Count",tCount);
	tTransferData.setNameAndValue("ComInfo",tComInfo);	
	System.out.println("=======cominfo:"+tComInfo);
    String tAreaName[]      = request.getParameterValues("AreaHospitalGrid3");
    String tYear[]          = request.getParameterValues("AreaHospitalGrid4");
    String tAverageCharge[] = request.getParameterValues("AreaHospitalGrid5");
    String tAverageDate[]   = request.getParameterValues("AreaHospitalGrid6");
    String tFeeScale[]    = request.getParameterValues("AreaHospitalGrid7");  //地区平均药品费占比  
    String tMakeDate[]      = request.getParameterValues("AreaHospitalGrid8");     
    String tMakeTime[]      = request.getParameterValues("AreaHospitalGrid9");
    
    String tLevelCode1[]= request.getParameterValues("AreaHospitalGrid10");
    String tLevelCode2[]= request.getParameterValues("AreaHospitalGrid11");	    
    String tAreaCode[]  = request.getParameterValues("AreaHospitalGrid12");
    String tManageCom[]  = request.getParameterValues("AreaHospitalGrid13");
	String tHospitalCount[]  = request.getParameterValues("AreaHospitalGrid14");//地区住院总人次
	String tClinicCount[]  = request.getParameterValues("AreaHospitalGrid15");  //地区门诊总人次
    String tDataSource[]    = request.getParameterValues("AreaHospitalGrid16"); 
           	
	//诊疗特色
	int AreaHospitalCount = 0;
	if (tAreaName != null)
		AreaHospitalCount = tAreaName.length;
	for(int i = 0; i < AreaHospitalCount; i++)
	{
		LDAreaHospitalSchema tLDAreaHospitalSchema   = new LDAreaHospitalSchema();
		
		System.out.println("AreaCode="+tAreaCode[i]+" ; Year="+tYear[i]);
		
		tLDAreaHospitalSchema.setAreaCode(tAreaCode[i]);
		tLDAreaHospitalSchema.setAreaName(tAreaName[i]);
		tLDAreaHospitalSchema.setYear(tYear[i]);
		
		tLDAreaHospitalSchema.setLevelCode1(tLevelCode1[i]) ;
		tLDAreaHospitalSchema.setLevelCode2(tLevelCode2[i]) ;
		
		tLDAreaHospitalSchema.setAverageCharge(tAverageCharge[i]);
		tLDAreaHospitalSchema.setAverageDate(tAverageDate[i]);
		tLDAreaHospitalSchema.setDataSource(tDataSource[i]);
		tLDAreaHospitalSchema.setHospitalCount(tHospitalCount[i]);
		tLDAreaHospitalSchema.setClinicCount(tClinicCount[i]);
		tLDAreaHospitalSchema.setFeeScale(tFeeScale[i]);
		
		tLDAreaHospitalSchema.setOperator(tG.Operator);
		tLDAreaHospitalSchema.setManageCom(tManageCom[i]);
		
		
		if("".equals(tMakeDate[i])||tMakeDate[i]==null)
		{
			tLDAreaHospitalSchema.setMakeDate(PubFun.getCurrentDate());
			tLDAreaHospitalSchema.setMakeTime(PubFun.getCurrentTime());
			tLDAreaHospitalSchema.setManageCom(tMngCom);
		}
		else
		{
			tLDAreaHospitalSchema.setMakeDate(tMakeDate[i]);
			tLDAreaHospitalSchema.setMakeTime(tMakeTime[i]);
		}
		tLDAreaHospitalSchema.setModifyDate(PubFun.getCurrentDate());
		tLDAreaHospitalSchema.setModifyTime(PubFun.getCurrentTime());
		
		tLDAreaHospitalSet.add(tLDAreaHospitalSchema);
	}    
    	
	try
	{
		// 准备传输数据 VData
		System.out.println("------准备传输数据 VData--------");
	 	VData tVData = new VData();
		tVData.add(tLDAreaHospitalSet);
		tVData.add(tTransferData);
	 	tVData.add(tG);
	 	
		tLDAreaHospitalUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tLDAreaHospitalUI.mErrors;
		if (!tError.needDealError())
		{
			Content = " 操作成功! ";
			FlagStr = "Success";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

  //添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
