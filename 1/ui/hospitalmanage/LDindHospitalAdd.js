//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2011-06-02
//创建人  ：王洪亮
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------

var showInfo=null;
var turnPage=new turnPageClass();
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    
	  showInfo.close();
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	 if(FlagStr=="Fail")
     {
       return false
     }
      top.opener.initTextbox();
	  top.opener.queryClick(); 
      top.close();

 
}

//-------------------添加时-------
function addClick()
{

	
   if( verifyInput() == false ) 
   {
      return false;
   }
   if(!checkData())
   {
	    return false;
   }
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
           var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
           showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
   fm.all("fmtransact").value="INSERT||MAIN";
   fm.submit();
 
   
}    
function checkData()
{
   var Validate= fm.all('Validate').value;
   var CInvalidate=fm.all('CInvalidate').value;
   
   var ManageCom=fm.all('ManageCom').value;
   if((trim(ManageCom)).length>4)
   {
      alert("管理机构只能到分公司，不能用8位机构的用户进行配置！");
      return false;
   }
   
   var getDate ="select current date from dual";
   var result = easyExecSql(getDate);
   if(result != "null" && result != "" && result != null)
   { 
      if(result[0][0]>Validate)
      {
         alert("医院配置属性生效日期不得早于当前日期！");
         return false;
      }
   }
   else
   {
      alert("没有找到当前日期！");
      return false;
   }
 
   if(trim(Validate)!=""&&trim(CInvalidate)!="")
   {
      if(CInvalidate<Validate)
      {
         alert("终止日期不能早于生效日期！");
         return false;
      }
   }
   
   var HospitCode=fm.all('HospitCode').value;
   var HospitalName=fm.all('HospitalName').value;
   var strSQL="select 1 from ldhospital where  hospitcode='"+HospitCode+"' and hospitname='"+HospitalName+"' ";
   var arrResult = easyExecSql(strSQL);
   if (arrResult== null||arrResult=="") 
   {
       alert("系统中没有与之匹配的医院代码及医院名称！");
       return false;
   }
   return true;
}        

