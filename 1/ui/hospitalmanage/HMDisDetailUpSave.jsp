<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：HMDisDetailUpSave.jsp
//程序功能：疾病档案查询及修改
//创建日期：2010-3-10
//创建人  ：chenxw
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.hospitalmanage.*"%>

<%
 //接收信息，并作校验处理。
 
 //输入参数
 //××××Schema t××××Schema = new ××××Schema();
 HMDisDetailUpUI tHMDisDetailUpUI = new HMDisDetailUpUI();
 
 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 transact = request.getParameter("fmtransact");
 String  tCustNo = request.getParameter("CustNo");  //客户号码
 String tName = request.getParameter("Name");
 String tCureProcess = request.getParameter("CureProcess"); //治疗经过
 String tTransTreatment = request.getParameter("TransTreatment"); //转诊情况
 
    //客户疾病档案信息
    HMCustDisDocInfoSet tHMCustDisDocInfoSet = new HMCustDisDocInfoSet();
 
	//封装疾病档案信息
	String tRadio[] = request.getParameterValues("InpDisDocGridSel");  
	String tSerialNo[]=request.getParameterValues("DisDocGrid1"); 
	String tCaseNo[]=request.getParameterValues("DisDocGrid2"); 
	String tDiseaseName[]=request.getParameterValues("DisDocGrid3");         
	String tDiseaseNo[]=request.getParameterValues("DisDocGrid4");             
	String tHospitalName[]=request.getParameterValues("DisDocGrid5");  
	String tHospitalNo[] = request.getParameterValues("DisDocGrid6");        
	String tRealHospDate[] = request.getParameterValues("DisDocGrid7");
	String tOutHospDescribe[] = request.getParameterValues("DisDocGrid8");
	String tComplication[] = request.getParameterValues("DisDocGrid9");
	String tSumFee[] = request.getParameterValues("DisDocGrid10");
	String tRealMedicinePay[] = request.getParameterValues("DisDocGrid11");
	
	int mutLineCount = 0;                                                     
	if(tRadio != null )    
	{                                 
		mutLineCount = tRadio.length;
		for(int i = 0; i < mutLineCount; i++)                                 
		{                                                                          
			if(tRadio[i].equals("1")) { //选中
				
				HMCustDisDocInfoSchema tHMCustDisDocInfoSchema = new HMCustDisDocInfoSchema();
				tHMCustDisDocInfoSchema.setSerialNo(tSerialNo[i]); 
				tHMCustDisDocInfoSchema.setCustomerNo(tCustNo);                  
				tHMCustDisDocInfoSchema.setCustomerName(tName);                   
				tHMCustDisDocInfoSchema.setCaseNo(tCaseNo[i]); 
				tHMCustDisDocInfoSchema.setDiseaseName(tDiseaseName[i]);                           
				tHMCustDisDocInfoSchema.setDiseaseNo(tDiseaseNo[i]);
				tHMCustDisDocInfoSchema.setHospitalName(tHospitalName[i]);
				tHMCustDisDocInfoSchema.setHospitalNo(tHospitalNo[i]);
				tHMCustDisDocInfoSchema.setRealHospDate(tRealHospDate[i]);
				tHMCustDisDocInfoSchema.setOutHospDescribe(tOutHospDescribe[i]);
				tHMCustDisDocInfoSchema.setComplication(tComplication[i]);
				tHMCustDisDocInfoSchema.setSumFee(tSumFee[i]);
				tHMCustDisDocInfoSchema.setRealMedicinePay(tRealMedicinePay[i]);
				tHMCustDisDocInfoSchema.setCureProcess(tCureProcess);
				tHMCustDisDocInfoSchema.setTransTreatment(tTransTreatment);
	                                              
				tHMCustDisDocInfoSet.add(tHMCustDisDocInfoSchema);
				
				break;
			}
		}   
	}
 
 try
 {
  //准备传输数据VData
  VData tVData = new VData();
  
  //传输schema
  tVData.add(tG);
  tVData.addElement(tHMCustDisDocInfoSet);
  tHMDisDetailUpUI.submitData(tVData,transact);
 }
 catch(Exception ex)
 {
  Content = "操作失败，原因是:" + ex.toString();
  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr=="")
 {
  tError = tHMDisDetailUpUI.mErrors;
  if (!tError.needDealError())
  {                          
   Content = " 操作成功! ";
   FlagStr = "Success";
  }
  else                                                                           
  {
   Content = " 操作失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

