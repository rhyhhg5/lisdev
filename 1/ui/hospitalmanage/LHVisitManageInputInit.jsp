<%
//程序名称：LHTotalInfoQueryInit.jsp
//程序功能：客户综合信息查询(初始化页面)
//创建日期：2006-05-20 16:03:30
//创建人  ：hm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%> 
<Script language=javascript>
function initForm()
{
	try
	{  
		initInpBox();
		initHMVisitConditionGrid();
		initHMVisitInfoGrid();
		
	}
	catch(re)
	{
    	alert("LDAreaHospitalInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}  

function initInpBox()
{ 
	try
	{
		
				
		fm.MngCom.value = manageCom;
		fm.MngCom_Info.value=manageCom ;
		
		var codename = easyExecSql("select name from ldcom where comcode='"+manageCom+"' ");

		fm.MngCom_ch.value      =codename==null?"":codename;			
		fm.MngComName_Info.value=codename==null?"":codename;
		
		fm.Count.value='';
		fm.OperType.value = '';
		div_VisitCondition.style.display='none';	
		div_VisitInfo.style.display='none';		
	}
	catch(ex)
	{
		alert("在LDAreaHospitalInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}	

function initHMVisitConditionGrid() 
{ 
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    iArray[0][4]="station";         		//列名	    
		
		iArray[1]=new Array(); 
		iArray[1][0]="管理机构";   
		iArray[1][1]="50px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		iArray[1][9]="管理机构|NOTNULL";		
		
		iArray[2]=new Array(); 
		iArray[2][0]="探访条件";   
		iArray[2][1]="200px";   
		iArray[2][2]=100;        
		iArray[2][3]=1;
		
		iArray[3]=new Array();
		iArray[3][0]="机构应达探访率（%）";
		iArray[3][1]="60px";         
		iArray[3][2]=20;            
		iArray[3][3]=1; 
		iArray[3][9]="机构应达探访率|NOTNULL|NUM";	
				
		iArray[4]=new Array();
		iArray[4][0]="流水号";
		iArray[4][1]="30px";         
		iArray[4][2]=20;            
		iArray[4][3]=3; 
		
		iArray[5]=new Array();
		iArray[5][0]="makedate";
		iArray[5][1]="30px";         
		iArray[5][2]=20;            
		iArray[5][3]=3; 
		
		iArray[6]=new Array();
		iArray[6][0]="maketime";
		iArray[6][1]="30px";         
		iArray[6][2]=20;            
		iArray[6][3]=3; 						              		

		iArray[7]=new Array();
		iArray[7][0]="录入机构";
		iArray[7][1]="30px";         
		iArray[7][2]=20;            
		iArray[7][3]=3; 
						
	    HMVisitConditionGrid = new MulLineEnter( "fm" , "HMVisitConditionGrid" ); 

	    HMVisitConditionGrid.mulLineCount = 0;   
	    HMVisitConditionGrid.displayTitle = 1;
	    HMVisitConditionGrid.hiddenPlus = 0;
	    HMVisitConditionGrid.hiddenSubtraction = 0;
	    HMVisitConditionGrid.canSel = 0;
	    HMVisitConditionGrid.canChk = 1;
	    //HMVisitConditionGrid.chkBoxAllEventFuncName = "showOne";	
		HMVisitConditionGrid. addEventFuncName="addCondition";	    
	    HMVisitConditionGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //HMVisitConditionGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}
function initHMVisitInfoGrid() 
{ 
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    iArray[0][4]="station";         		//列名
		
		iArray[1]=new Array(); 
		iArray[1][0]="客户号码";   
		iArray[1][1]="80px";   
		iArray[1][2]=60;        
		iArray[1][3]=1;
		iArray[1][9]="客户号码|NOTNULL";
				
		iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="80px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;
		iArray[2][9]="客户姓名|NOTNULL";
				
		iArray[3]=new Array();
		iArray[3][0]="探访医院名称";
		iArray[3][1]="80px";         
		iArray[3][2]=60;            
		iArray[3][3]=1;             

		iArray[4]=new Array();
		iArray[4][0]="探访时间";
		iArray[4][1]="80px";         
		iArray[4][2]=20;            
		iArray[4][3]=1; 
		iArray[4][9]="探访时间|DATE"		
		
		iArray[5]=new Array();
		iArray[5][0]="是否进行客户身份确认";
		iArray[5][1]="110px";         
		iArray[5][2]=20;            
		iArray[5][3]=2;
		iArray[5][10]="Ident";  //引用代码："CodeName"为传入数据的名称
		iArray[5][11]= "0|^0|否|^1|是" ; //"CodeContent" 是传入要下拉显示的代码
		//iArray[5][12]="5|10";     //引用代码对应第几列，'|'为分割符
		//iArray[5][13]="1|0";    //上面的列中放置引用代码中第几位值
		
		iArray[6]=new Array();
		iArray[6][0]="是否收集相关资料";
		iArray[6][1]="100px";         
		iArray[6][2]=20;            
		iArray[6][3]=2; 		
		iArray[6][10]="Collection";  //引用代码："CodeName"为传入数据的名称
		iArray[6][11]= "0|^0|否|^1|是" ; //"CodeContent" 是传入要下拉显示的代码
				  		
		iArray[7]=new Array();
		iArray[7][0]="有无挂/压床";
		iArray[7][1]="80px";         
		iArray[7][2]=20;            
		iArray[7][3]=2; 	
		iArray[7][10]="Bed";  //引用代码："CodeName"为传入数据的名称
		iArray[7][11]= "0|^0|无|^1|有" ; //"CodeContent" 是传入要下拉显示的代码
						
		iArray[8]=new Array();
		iArray[8][0]="是否确认就诊断依据和诊疗项目合理性";
		iArray[8][1]="120px";         
		iArray[8][2]=20;            
		iArray[8][3]=2;
		iArray[8][10]="Legitimacy";  //引用代码："CodeName"为传入数据的名称
		iArray[8][11]= "0|^0|否|^1|是" ; //"CodeContent" 是传入要下拉显示的代码
						
		iArray[9]=new Array();
		iArray[9][0]="是否提供相关就医和理赔支持服务";
		iArray[9][1]="120px";         
		iArray[9][2]=20;            
		iArray[9][3]=2;	
		iArray[9][10]="Support";  //引用代码："CodeName"为传入数据的名称
		iArray[9][11]= "0|^0|否|^1|是" ; //"CodeContent" 是传入要下拉显示的代码
			
		iArray[10]=new Array();
		iArray[10][0]="FlowNo";
		iArray[10][1]="30px";         
		iArray[10][2]=20;            
		iArray[10][3]=3;
		
		iArray[11]=new Array();
		iArray[11][0]="makedate";
		iArray[11][1]="30px";         
		iArray[11][2]=20;            
		iArray[11][3]=3;		
					
		iArray[12]=new Array();
		iArray[12][0]="maketime";
		iArray[12][1]="30px";         
		iArray[12][2]=20;            
		iArray[12][3]=3;
												
	    HMVisitInfoGrid = new MulLineEnter( "fm" , "HMVisitInfoGrid" ); 

	    HMVisitInfoGrid.mulLineCount = 0;   
	    HMVisitInfoGrid.displayTitle = 1;
	    HMVisitInfoGrid.hiddenPlus = 0;
	    HMVisitInfoGrid.hiddenSubtraction = 0;
	    HMVisitInfoGrid.canSel = 0;
	    HMVisitInfoGrid.canChk = 1;
	    //HMVisitInfoGrid.selBoxEventFuncName = "showOne";	
		HMVisitInfoGrid. addEventFuncName="addVisitInfo";	    
	    HMVisitInfoGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //HMVisitInfoGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}

</script>
  