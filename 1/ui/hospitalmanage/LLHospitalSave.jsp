<%
//程序名称：LLHospitalSave.jsp
//程序功能：
//创建日期：2010-03-04 15:39:06
//创建人  ：DINGJW程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.hospitalmanage.*"%>   
  
<%  //接收信息，并作校验处理。
  //输入参数
  System.out.println("------------------------DINGJW------------------------");
  LLMedicalManagementSchema tLLMedicalManagementSchema = new LLMedicalManagementSchema();
  LLMedicalManagementUI tLLMedicalManagementUI = new LLMedicalManagementUI();
  VData mmResult = new VData();
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");              //获得用户信息
  CErrors tError = null;                                 //错误类的获得
  String tOperate=request.getParameter("fmtransact");   //获得操作符号
  tOperate=tOperate.trim();
                                                         
  VData tVData = new VData();                            // 准备传输数据 VData
  String FlagStr="";
  String Content="";
  	String strSerialNo="";
  	tLLMedicalManagementSchema.setSerialNo(request.getParameter("hiddenSerialNo"));
  	tLLMedicalManagementSchema.setManageCom(request.getParameter("ManageCom"));
    tLLMedicalManagementSchema.setHospitalCode(request.getParameter("HospitalCode"));
    tLLMedicalManagementSchema.setHospitalName(request.getParameter("HospitalName"));
    tLLMedicalManagementSchema.setMsgChannelCode(request.getParameter("MsgChannelCode"));
    tLLMedicalManagementSchema.setMsgChannel(request.getParameter("MsgChannel"));
    tLLMedicalManagementSchema.setCharger(request.getParameter("Charger"));
    System.out.println("Charger="+request.getParameter("Charger"));
    tLLMedicalManagementSchema.setProfessionalCode(request.getParameter("ProfessionalCode"));
    tLLMedicalManagementSchema.setProfessional(request.getParameter("Professional"));
    tLLMedicalManagementSchema.setBussinessDate(request.getParameter("BussinessDate"));
    tLLMedicalManagementSchema.setProblemDetail(request.getParameter("ProblemDetail"));
    tLLMedicalManagementSchema.setAction(request.getParameter("Action"));

    tVData.add(tG);					//准备向后方传输的数据	
    tVData.addElement(tLLMedicalManagementSchema);
    
    try{
    	tLLMedicalManagementUI.submitData(tVData,tOperate); 
    	System.out.println("------------LLHospitalSave.jsp--------------");
    }catch(Exception ex)
  	{
    		Content = "保存失败，原因是:" + ex.toString();
    		System.out.println(Content);
    		FlagStr = "Fail";
  	}
    if (!FlagStr.equals("Fail"))
  	{
   	   tError = tLLMedicalManagementUI.mErrors;
    	   if (!tError.needDealError())
    		{if (tOperate.equals("INSERT||MAIN"))
                       { mmResult = tLLMedicalManagementUI.getResult();
                       LLMedicalManagementSchema schema=(LLMedicalManagementSchema)mmResult.getObjectByObjectName("LLMedicalManagementSchema",0);
                       strSerialNo=schema.getSerialNo();
                       System.out.println("SerialNo="+strSerialNo);
                       Content="保存成功";
                        }
                  if (tOperate.equals("UPDATE||MAIN"))
                       { mmResult = tLLMedicalManagementUI.getResult();
                       Content="修改成功";
                        }   
                  
                  if(tOperate.equals("DELETE||MAIN"))
                  {
                	  mmResult = tLLMedicalManagementUI.getResult();
                      Content="删除成功";
                  }
    			//Content = "保存成功!";
    			FlagStr = "Succ";
    		}
   	  else
    		{
    			Content = " 保存失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		}
  	}
    System.out.println(Content);
    System.out.println(FlagStr);
  	
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=strSerialNo%>");
</script>
</html>  	