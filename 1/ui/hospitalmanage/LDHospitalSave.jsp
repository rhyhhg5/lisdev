<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDHospitalSave.jsp
//程序功能：
//创建日期：2005-01-15 14:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.hospitalmanage.*"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	LDHospitalSchema tLDHospitalSchema   = new LDHospitalSchema();
	LDHospitalUI tLDHospitalUI   = new LDHospitalUI();
	LDHospitalInfoIntroSet tLDHospitalInfoIntroSet = new LDHospitalInfoIntroSet();
	LDComHospitalSet tLDComHospitalSet = new LDComHospitalSet();
	
	System.out.println("---------接收信息，并作校验处理。------------");
	//输出参数
	CErrors tError = null;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");

    tLDHospitalSchema.setHospitalType(request.getParameter("HospitalType"));
    tLDHospitalSchema.setHospitCode(request.getParameter("HospitCode"));
    tLDHospitalSchema.setHospitName(request.getParameter("HospitName"));
    tLDHospitalSchema.setHospitShortName(request.getParameter("HospitShortName"));
    tLDHospitalSchema.setCommunFixFlag(request.getParameter("CommunFixFlag"));
    tLDHospitalSchema.setAreaCode(request.getParameter("AreaCode"));
    tLDHospitalSchema.setLevelCode(request.getParameter("LevelCode"));
    tLDHospitalSchema.setAdminiSortCode(request.getParameter("AdminiSortCode"));
    tLDHospitalSchema.setBusiTypeCode(request.getParameter("BusiTypeCode"));
    tLDHospitalSchema.setEconomElemenCode(request.getParameter("EconomElemenCode"));
    tLDHospitalSchema.setAddress(request.getParameter("Address"));
    tLDHospitalSchema.setZipCode(request.getParameter("ZipCode"));
    tLDHospitalSchema.setPhone(request.getParameter("Phone"));
    tLDHospitalSchema.setWebAddress(request.getParameter("WebAddress"));
    tLDHospitalSchema.setAreaName(request.getParameter("AreaName"));
    tLDHospitalSchema.setHospitalStandardCode(request.getParameter("HospitalStandardCode"));
    tLDHospitalSchema.setSuperiorNo(request.getParameter("SuperiorNo"));
    tLDHospitalSchema.setInterNo(request.getParameter("InterNo"));
    tLDHospitalSchema.setBedAmount(request.getParameter("BedAmount"));
    tLDHospitalSchema.setElementaryNo(request.getParameter("ElementaryNo"));
    tLDHospitalSchema.setTotalNo(request.getParameter("TotalNo"));
    tLDHospitalSchema.setAssociateClass(request.getParameter("AssociateClass"));
    tLDHospitalSchema.setPatientPerDay(request.getParameter("PatientPerDay"));
    tLDHospitalSchema.setOutHospital(request.getParameter("OutHospital"));
    tLDHospitalSchema.setFlowNo(request.getParameter("FloNo"));
    tLDHospitalSchema.setManageCom(request.getParameter("MngCom"));
    tLDHospitalSchema.setUrbanOption(request.getParameter("UrbanOption"));
    tLDHospitalSchema.setRoute(request.getParameter("Route"));
    tLDHospitalSchema.setSecurityNo(request.getParameter("SecurityNo"));
	tLDHospitalSchema.setExamination(request.getParameter("examination"));//核保体检医院
	
    tLDHospitalSchema.setOperator(request.getParameter("Operator"));
    tLDHospitalSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDHospitalSchema.setMakeTime(request.getParameter("MakeTime"));
    tLDHospitalSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLDHospitalSchema.setModifyTime(request.getParameter("ModifyTime"));
	tLDHospitalSchema.setModifyTime(request.getParameter("ModifyTime"));

    String tSpecialName[] = request.getParameterValues("SpecSecDiaGrid5");
    String tSpecialIntro[] = request.getParameterValues("SpecSecDiaGrid2");
    String tSpecialSecDiagClass[] = request.getParameterValues("SpecSecDiaGrid3");
    String tSpecialFlwoNo[] = request.getParameterValues("SpecSecDiaGrid4");

    String tInstrumentName[] = request.getParameterValues("InstrumentGrid1");
    String tInstrumentIntro[] = request.getParameterValues("InstrumentGrid2");
    String tInstrumentIntroClass[] = request.getParameterValues("InstrumentGrid3");
    String tInstrumentFlowNo[] = request.getParameterValues("InstrumentGrid4");

    String tAssociatClass[] = request.getParameterValues("ComHospitalGrid1");
    String tStartDate[] = request.getParameterValues("ComHospitalGrid2");
    String tEndDate[] = request.getParameterValues("ComHospitalGrid3");

	if(tLDHospitalSchema.getHospitalType().equals("1"))
	{
		//诊疗特色
		int SpecSecDiaGridCount = 0;
		if (tSpecialName != null)
			SpecSecDiaGridCount = tSpecialName.length;
		for(int i = 0; i < SpecSecDiaGridCount; i++)
		{
			LDHospitalInfoIntroSchema tLDHospitalInfoIntroSchema = new LDHospitalInfoIntroSchema();
			tLDHospitalInfoIntroSchema.setHospitCode(request.getParameter("HospitCode"));
			tLDHospitalInfoIntroSchema.setSpecialName(tSpecialName[i]);
			tLDHospitalInfoIntroSchema.setSpecialIntro(tSpecialIntro[i]);
			tLDHospitalInfoIntroSchema.setSpecialClass(tSpecialSecDiagClass[i]);
			tLDHospitalInfoIntroSchema.setFlowNo(tSpecialFlwoNo[i]);
			System.out.println(request.getParameter("HospitCode")+tSpecialName[i]+tSpecialIntro[i]);
			tLDHospitalInfoIntroSet.add(tLDHospitalInfoIntroSchema);
		}
		//大型仪器设备
		int InstrumentCount = 0;
		if( tInstrumentName != null)
			InstrumentCount = tInstrumentName.length;

		for(int i = 0; i < InstrumentCount; i++)
		{
			LDHospitalInfoIntroSchema tLDHospitalInfoIntroSchema = new LDHospitalInfoIntroSchema();
			tLDHospitalInfoIntroSchema.setHospitCode(request.getParameter("HospitCode"));
			tLDHospitalInfoIntroSchema.setSpecialName(tInstrumentName[i]);
			tLDHospitalInfoIntroSchema.setSpecialIntro(tInstrumentIntro[i]);
			tLDHospitalInfoIntroSchema.setSpecialClass(tInstrumentIntroClass[i]);
			tLDHospitalInfoIntroSchema.setFlowNo(tInstrumentFlowNo[i]);
			System.out.println(tInstrumentIntroClass[i]);
			tLDHospitalInfoIntroSet.add(tLDHospitalInfoIntroSchema);
		}
		
		//合作级别
		int ComHospitalCount = 0;
		if(tAssociatClass != null)
			ComHospitalCount = tAssociatClass.length;

		for(int i = 0; i < ComHospitalCount; i++)
		{
			LDComHospitalSchema tLDComHospitalSchema = new LDComHospitalSchema();
			tLDComHospitalSchema.setHospitCode(request.getParameter("HospitCode"));
			tLDComHospitalSchema.setAssociatClass(tAssociatClass[i]);
			tLDComHospitalSchema.setStartDate(tStartDate[i]);
			tLDComHospitalSchema.setEndDate(tEndDate[i]);
			tLDComHospitalSet.add(tLDComHospitalSchema);
		}
	}	
	
	try
	{
		// 准备传输数据 VData
		System.out.println("------准备传输数据 VData--------");
	 	VData tVData = new VData();
		tVData.add(tLDHospitalSchema);
		tVData.add(tLDHospitalInfoIntroSet);
		tVData.add(tLDComHospitalSet);
	 	tVData.add(tG);
		tLDHospitalUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tLDHospitalUI.mErrors;
		if (!tError.needDealError())
		{
			Content = " 操作成功! ";
			FlagStr = "Success";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

  //添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
