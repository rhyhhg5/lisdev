<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServerPriceSave.jsp
//程序功能：服务价格表设置的数据操作
//创建日期：2006-03-03 15:38:10
//创建人  ：郭丽颖
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.hospitalmanage.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	LHContServPriceUI tLHContServPriceUI = new LHContServPriceUI();
	LHContServPriceSchema tLHContServPriceSchema = new LHContServPriceSchema();

	LDTestPriceMgtSet tLDTestPriceMgtSet = new LDTestPriceMgtSet();		//价格信息
   
   
	//检查项目信息量
	String tMedicaItemCode[] = request.getParameterValues("TestItemPriceGrid1");					//MulLine的列存储数组
	String tMedicaItemPrice[] = request.getParameterValues("TestItemPriceGrid3");					//MulLine的列存储数组
	String tExplain[] = request.getParameterValues("TestItemPriceGrid4");         //MulLine的列存储数组
	String tPriceClass[] = request.getParameterValues("TestItemPriceGrid5");         //MulLine的列存储数组
	String tSerialNo[] = request.getParameterValues("TestItemPriceGrid6");         //MulLine的列存储数组
	
	//体检机构信息
	String tMedicaItemCode2[] = request.getParameterValues("TestGrpPriceGrid1");					//MulLine的列存储数组
	String tMedicaItemPrice2[] = request.getParameterValues("TestGrpPriceGrid3");    //MulLine的列存储数组
	String tExplain2[] = request.getParameterValues("TestGrpPriceGrid4");      //MulLine的列存储数组
	String tPriceClass2[] = request.getParameterValues("TestGrpPriceGrid5");         //MulLine的列存储数组
	String tSerialNo2[] = request.getParameterValues("TestGrpPriceGrid6");         //MulLine的列存储数组	

	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
	System.out.println(" ************* "+request.getParameter("ContraType"));
  	//ContraType
  	String ContraType=request.getParameter("ContraType");
  	if(ContraType.equals("01"))
  	{
  	    tLHContServPriceSchema.setMedicaItemCode(request.getParameter("MedicaItemCode")); //体检套餐代码
  	}
	String ServicePriceCode=request.getParameter("ServicePriceCode");
		
  	if(ServicePriceCode.equals("1"))
    {
	    tLHContServPriceSchema.setContraNo(request.getParameter("ContraNo"));
	    String tContraNo=tLHContServPriceSchema.getContraNo();
	  //  System.out.println("@@@@@@@@@ "+tContraNo);
	    tLHContServPriceSchema.setContraName(request.getParameter("ContraName"));
	    tLHContServPriceSchema.setContraObj(request.getParameter("HospitCode"));//合作机构名称
	    tLHContServPriceSchema.setContraType(request.getParameter("ContraType"));//合同项目类型   
	    tLHContServPriceSchema.setDutyItemCode(request.getParameter("ContraNowName"));//合同责任项目名称
	    tLHContServPriceSchema.setServPriceType(request.getParameter("ServicePriceCode")); //服务价格表类型
	    tLHContServPriceSchema.setExpType(request.getParameter("BalanceTypeCode")); //费用类型
	    tLHContServPriceSchema.setContraItemNo(request.getParameter("ContraItemNo")); //系统流水号
	    tLHContServPriceSchema.setMoneySum(request.getParameter("MoneySum")); //服务金额
	    tLHContServPriceSchema.setMaxServNum(request.getParameter("MaxServNum")); //最大人数
	    tLHContServPriceSchema.setExplanOther(request.getParameter("ExplanOther"));//补充说明ExplanOther
				    
				  
 		tLHContServPriceSchema.setOperator(request.getParameter("Operator"));
		tLHContServPriceSchema.setMakeDate(request.getParameter("MakeDate"));
		tLHContServPriceSchema.setMakeTime(request.getParameter("MakeTime"));
//		tLHContServPriceSchema.setModifyDate(request.getParameter("ModifyDate"));
//		tLHContServPriceSchema.setModifyTime(request.getParameter("ModifyTime"));

	}
	if(ServicePriceCode.equals("2"))
	{	
		tLHContServPriceSchema.setContraNo(request.getParameter("ContraNo"));
	    String tContraNo=tLHContServPriceSchema.getContraNo();
	    System.out.println("@@@@@@@@@ "+tContraNo);
	    tLHContServPriceSchema.setContraName(request.getParameter("ContraName"));
	    tLHContServPriceSchema.setContraObj(request.getParameter("HospitCode"));//合作机构名称
	    tLHContServPriceSchema.setContraType(request.getParameter("ContraType"));//合同项目类型   
	    tLHContServPriceSchema.setDutyItemCode(request.getParameter("ContraNowName"));//合同责任项目名称
	    tLHContServPriceSchema.setServPriceType(request.getParameter("ServicePriceCode")); //服务价格表类型
	    tLHContServPriceSchema.setExpType(request.getParameter("BalanceTypeCode")); //费用类型
	    tLHContServPriceSchema.setContraItemNo(request.getParameter("ContraItemNo")); //系统流水号
				    
				  
 		tLHContServPriceSchema.setOperator(request.getParameter("Operator"));
		tLHContServPriceSchema.setMakeDate(request.getParameter("MakeDate"));
		tLHContServPriceSchema.setMakeTime(request.getParameter("MakeTime"));
//		tLHContServPriceSchema.setModifyDate(request.getParameter("ModifyDate"));
//		tLHContServPriceSchema.setModifyTime(request.getParameter("ModifyTime"));
	          
	          
	    int LHServerPriceCount = 0;
		if(tMedicaItemCode != null)
		{	
		   	LHServerPriceCount = tMedicaItemCode.length;
		}	
		System.out.println(" LHServerPriceCount is : "+LHServerPriceCount);
		
		for(int i = 0; i < LHServerPriceCount; i++)
		{
							
			LDTestPriceMgtSchema tLDTestPriceMgtSchema = new LDTestPriceMgtSchema();
							
			tLDTestPriceMgtSchema.setContraNo(request.getParameter("ContraNo")); //合同编号
			System.out.println("AAAAAAAAAAAAAAA "+tLDTestPriceMgtSchema.getContraNo());
			tLDTestPriceMgtSchema.setContraItemNo(request.getParameter("ContraItemNo")); //系统流水号
			tLDTestPriceMgtSchema.setMedicaItemPrice(request.getParameter("MoneySum")); //检查项目单价
			tLDTestPriceMgtSchema.setHospitCode(request.getParameter("HospitCode")); //合作机构代码
						
								  
			tLDTestPriceMgtSchema.setMedicaItemCode(tMedicaItemCode[i]);	
			tLDTestPriceMgtSchema.setMedicaItemPrice(tMedicaItemPrice[i]);	
			tLDTestPriceMgtSchema.setExplain(tExplain[i]);
			tLDTestPriceMgtSchema.setPriceClass(tPriceClass[i]);
			System.out.println("NNNNNNNNNNNNNNNNN "+tLDTestPriceMgtSchema.getPriceClass());
			tLDTestPriceMgtSchema.setSerialNo(tSerialNo[i]);					      
		//  System.out.println("AAAAAAAAAAAAAAAA "+tLDTestPriceMgtSchema.getSerialNo());							                              
			tLDTestPriceMgtSchema.setOperator(request.getParameter("Operator"));
			tLDTestPriceMgtSchema.setMakeDate(request.getParameter("MakeDate"));
			tLDTestPriceMgtSchema.setMakeTime(request.getParameter("MakeTime"));
			tLDTestPriceMgtSchema.setModifyDate(request.getParameter("ModifyDate"));
			tLDTestPriceMgtSchema.setModifyTime(request.getParameter("ModifyTime"));
	                       
			tLDTestPriceMgtSet.add(tLDTestPriceMgtSchema);
							                        
		}
				
		int LHServerPriceCount2 = 0;
		if(tMedicaItemCode2 != null)
		{	
		   	LHServerPriceCount2 = tMedicaItemCode2.length;
		}	
		System.out.println(" LHServerPriceCount2 is : "+LHServerPriceCount2);
		
		for(int i = 0; i < LHServerPriceCount2; i++)
		{
							
			LDTestPriceMgtSchema tLDTestPriceMgtSchema = new LDTestPriceMgtSchema();
							
			tLDTestPriceMgtSchema.setContraNo(request.getParameter("ContraNo")); //合同编号
			//  System.out.println("BBBBBBBBBBB "+tLDTestPriceMgtSchema.getContraNo());
			tLDTestPriceMgtSchema.setContraItemNo(request.getParameter("ContraItemNo")); //系统流水号
			tLDTestPriceMgtSchema.setMedicaItemPrice(request.getParameter("MoneySum")); //检查项目单价
			tLDTestPriceMgtSchema.setHospitCode(request.getParameter("HospitCode")); //合作机构代码
								  
			tLDTestPriceMgtSchema.setMedicaItemCode(tMedicaItemCode2[i]);	
			tLDTestPriceMgtSchema.setMedicaItemPrice(tMedicaItemPrice2[i]);	
			tLDTestPriceMgtSchema.setExplain(tExplain2[i]);	
			tLDTestPriceMgtSchema.setPriceClass(tPriceClass2[i]);
			System.out.println("MMMMMMMMMMMMMM "+tLDTestPriceMgtSchema.getPriceClass());
			tLDTestPriceMgtSchema.setSerialNo(tSerialNo2[i]);	
		//  System.out.println("BBBBBBBBBBBBBBBB "+tLDTestPriceMgtSchema.getSerialNo());							      
						      
			tLDTestPriceMgtSchema.setManageCom(request.getParameter("ManageCom"));                      
			tLDTestPriceMgtSchema.setOperator(request.getParameter("Operator"));
			tLDTestPriceMgtSchema.setMakeDate(request.getParameter("MakeDate"));
			tLDTestPriceMgtSchema.setMakeTime(request.getParameter("MakeTime"));
			tLDTestPriceMgtSchema.setModifyDate(request.getParameter("ModifyDate"));
			tLDTestPriceMgtSchema.setModifyTime(request.getParameter("ModifyTime"));
			                          			                       
			tLDTestPriceMgtSet.add(tLDTestPriceMgtSchema);
							                        
		}
	}
    if(ServicePriceCode.equals("3"))
    {
	    tLHContServPriceSchema.setContraNo(request.getParameter("ContraNo"));
	    String tContraNo=tLHContServPriceSchema.getContraNo();
	    tLHContServPriceSchema.setContraName(request.getParameter("ContraName"));
	    tLHContServPriceSchema.setContraObj(request.getParameter("HospitCode"));//合作机构名称
	    System.out.println("TTTTTTTTTt3 "+tLHContServPriceSchema.getContraObj());
	    tLHContServPriceSchema.setContraType(request.getParameter("ContraType"));//合同项目类型   
	    tLHContServPriceSchema.setDutyItemCode(request.getParameter("ContraNowName"));//合同责任项目名称
	    tLHContServPriceSchema.setServPriceType(request.getParameter("ServicePriceCode")); //服务价格表类型
	    tLHContServPriceSchema.setExpType(request.getParameter("BalanceTypeCode")); //费用类型
	    tLHContServPriceSchema.setContraItemNo(request.getParameter("ContraItemNo")); //系统流水号
	    tLHContServPriceSchema.setMoneySum(request.getParameter("MoneySum2")); //服务金额
	    tLHContServPriceSchema.setServPros(request.getParameter("ServPros2")); //服务时程
	    tLHContServPriceSchema.setMaxServNum(request.getParameter("MaxServNum2")); //最大人数
	   // tLHContServPriceSchema.setMedicaItemCode(request.getParameter("MedicaItemCode")); //体检套餐代码
	    tLHContServPriceSchema.setExplanOther(request.getParameter("ExplanOther2"));//补充说明ExplanOther

 		tLHContServPriceSchema.setOperator(request.getParameter("Operator"));
		tLHContServPriceSchema.setMakeDate(request.getParameter("MakeDate"));
		tLHContServPriceSchema.setMakeTime(request.getParameter("MakeTime"));
//		tLHContServPriceSchema.setModifyDate(request.getParameter("ModifyDate"));
//		tLHContServPriceSchema.setModifyTime(request.getParameter("ModifyTime"));			
	}

	if(ServicePriceCode.equals("4"))
	{
	    tLHContServPriceSchema.setContraNo(request.getParameter("ContraNo"));
	    String tContraNo=tLHContServPriceSchema.getContraNo();
	    tLHContServPriceSchema.setContraName(request.getParameter("ContraName"));
	    tLHContServPriceSchema.setContraObj(request.getParameter("HospitCode"));//合作机构名称
	    System.out.println("TTTTTTTTTt3 "+tLHContServPriceSchema.getContraObj());
	    tLHContServPriceSchema.setContraType(request.getParameter("ContraType"));//合同项目类型   
	    tLHContServPriceSchema.setDutyItemCode(request.getParameter("ContraNowName"));//合同责任项目名称
	    tLHContServPriceSchema.setServPriceType(request.getParameter("ServicePriceCode")); //服务价格表类型
	    tLHContServPriceSchema.setExpType(request.getParameter("BalanceTypeCode")); //费用类型
	    tLHContServPriceSchema.setContraItemNo(request.getParameter("ContraItemNo")); //系统流水号
	    tLHContServPriceSchema.setMoneySum(request.getParameter("MoneySum3")); //服务金额
	    tLHContServPriceSchema.setServPros(request.getParameter("ServPros3")); //服务时程
	    tLHContServPriceSchema.setMaxServNum(request.getParameter("MaxServNum3")); //最大人数
	   // tLHContServPriceSchema.setMedicaItemCode(request.getParameter("MedicaItemCode")); //体检套餐代码
	    tLHContServPriceSchema.setExplanOther(request.getParameter("ExplanOther3"));//补充说明ExplanOther

 		tLHContServPriceSchema.setOperator(request.getParameter("Operator"));
		tLHContServPriceSchema.setMakeDate(request.getParameter("MakeDate"));
		tLHContServPriceSchema.setMakeTime(request.getParameter("MakeTime"));
//		tLHContServPriceSchema.setModifyDate(request.getParameter("ModifyDate"));
//		tLHContServPriceSchema.setModifyTime(request.getParameter("ModifyTime"));
			
	}
   

	try
	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLHContServPriceSchema);
		tVData.add(tLDTestPriceMgtSet);
		tVData.add(tG);
		tLHContServPriceUI.submitData(tVData,transact);    
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
  
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
	    tError = tLHContServPriceUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 操作成功! ";
	    	FlagStr = "Success";
	    }
	    else                                                                           
	    {
	    	Content = " 操作失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	}
  
	//添加各种预处理
%>
<%=Content%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
