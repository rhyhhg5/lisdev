<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：HMSuggestInput.jsp
 //程序功能：专项健康建立录入
 //创建日期：2010-3-17
 //创建人  ：chenxw
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="HMSuggest.js"></SCRIPT>
  <%@include file="HMSuggestInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<table>
  <tr>
    <td class="titleImg" >专项健康建议录入</td>
  </tr>
</table>
<input value="按疾病"  onclick="return showDisSuggest();" class="cssButton" type="button" >
<input value="按客户"  onclick="return showCustSuggest();" class="cssButton" type="button" >
<hr>
<br>

<!-- ************************************ 按疾病健康建议发送********************************** -->
<div id="ByDisDiv" name="ByDisDiv" style="display:'none'">
<form action="#" method=post name=fm target="fraSubmit">
<table>
  <tr>
    <td class="titleImg" >按疾病健康建议录入</td>
  </tr>
</table>
<table  class="common" >
<table  class="common" >
  <tr class="common">
  	<td class="title">疾病名称</td>
    <td class="input">
    	<input class="common" name="DiseaseName"></td>
    <td class="title">疾病编码</td>
    <td class="input" ><input class="code" name="DiseaseCode"
    	ondblclick="return showCodeList('lldiseas',[this,DiseaseName],[1,0],null,fm.DiseaseName.value,'icdname',1);" 
      	onkeyup="return showCodeListKey('lldiseas',[this,DDiseaseName],[1,0]);"
     ></td>
  </tr>
</table>
</table>
<table>
  <tr>
    <td class="titleImg" >健康建议</td>
  </tr>
</table>
<textarea class="common" name="DisSuggest" cols="100%" rows="2" ></textarea>
<br><BR>
<input value="保存"  onclick="return saveofDis();" class="cssButton" type="button" >
<br>
<table>
  <tr>
    <td class="titleImg" >健康建议查询及修改</td>
  </tr>
</table>
<table  class="common" >
  <tr class="common">
  	<td class="title">疾病名称</td>
    <td class="input">
    	<input class="common" name="DiseaseName1"></td>
    <td class="title">疾病编码</td>
    <td class="input" ><input class="code" name="DiseaseCode1"
    	ondblclick="return showCodeList('lldiseas',[this,DiseaseName1],[1,0],null,fm.DiseaseName1.value,'icdname',1);" 
      	onkeyup="return showCodeListKey('lldiseas',[this,DDiseaseName1],[1,0]);"
     ></td>
  </tr>
	<tr class="common">
    <td class="title">录入时间起期</td>
    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="DisSuggInStartDate" elementtype=nacessary></td>  
    <td class="title">录入时间止期</td>
    <td class="input"><input class="coolDatePicker" dateFormat="short" name="DisSuggInEndDate" elementtype=nacessary></td>  
  </tr>
</table>
<input value="查询"  onclick="return alDisSuggestQuery();" class="cssButton" type="button" >
<input value="修改"  onclick="return alDisSuggestModify();" class="cssButton" type="button" >
<input value="删除"  onclick="return alDisSuggestDelete();" class="cssButton" type="button" >
<br>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanDisSugGrid" >
     </span> 
      </td>
   </tr>
</table>
<input class=cssbutton value="首  页" type=button onclick="DisSugGridTurnPage.firstPage();"> 
<input class=cssbutton value="上一页" type=button onclick="DisSugGridTurnPage.previousPage();">      
<input class=cssbutton value="下一页" type=button onclick="DisSugGridTurnPage.nextPage();"> 
<input class=cssbutton value="尾  页" type=button onclick="DisSugGridTurnPage.lastPage();">
<table>
  <tr>
    <td class="titleImg" >健康建议</td>
  </tr>
</table>
<textarea class="common" name="DisSuggest2" cols="100%" rows="2" ></textarea>
<br><BR>
<table>
  <tr>
    <td class="titleImg" >就诊客户发送健康建议</td>
  </tr>
</table>
<table  class="common" >
  <tr class="common">
    <td class="title">最近一次就诊时间结束起期</td>
    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="LastVisStartDate2"></td>  
    <td class="title">最近一次就诊时间结束止期</td>
    <td class="input"><input class="coolDatePicker" dateFormat="short" name="LastVisEndDate2" ></td>  
  </tr>
 <tr class="common">
  	<td class="title">疾病名称</td>
    <td class="input">
    	<input class="common" name="DiseaseName2"></td>
    <td class="title">疾病编码</td>
    <td class="input" ><input class="code" name="DiseaseCode2" elementtype=nacessary
    	ondblclick="return showCodeList('lldiseas',[this,DiseaseName2],[1,0],null,fm.DiseaseName2.value,'icdname',1);" 
      	onkeyup="return showCodeListKey('lldiseas',[this,DDiseaseName2],[1,0]);"
     ></td>
  </tr>
	<tr class="common">
    <td class="title">最近一年就诊次数>=</td><td class="input"><input class="common" name="LastVisCount2" ></td>  
    <td class="title">最近一年医疗费用理赔额度</td>
    <td class="code">
    	<input class="codeNo" name="HCostClaimFeeCode2"
    			CodeData="0|^1|1万元以上|10000^2|2万元以上|20000^3|5万元以上|50000^4|10万元以上|100000^5|15万元以上|150000"
    			ondblclick="return showCodeListEx('HCostClaimFeeCode2',[this,HCostClaimFeeName2,HCostClaimFee2],[0,1,2],null,null,null,1);"
    			onkeyup="return showCodeListEx('HCostClaimFeeCode2',[this,HCostClaimFeeName2,HCostClaimFee2],[0,1,2],null,null,null,1);"
    	><input class="codeName" name="HCostClaimFeeName2"><input type='hidden' name="HCostClaimFee2">
    </td>  
  </tr>
	<tr class="common">
	    <td class="title">就诊方式</td>
	    <td class="input">
	    	<input class="codeno" name="VistType2"
	    		CodeData="0|^1|全部^2|门诊^3|住院" 
	    		ondblclick="return showCodeListEx('VistType2',[this,VistTypeName2],[0,1],null,null,null,1);" 
	    		onkeyup="return showCodeListKeyEx('VistType2',[this,VistTypeName2],[0,1],null,null,null,1);" 
	    	><input class="codename" name="VistTypeName2">
	    </td>  
		   <td class="title">发送方式</td>
		    <td class="input">
		    	<input class="codeno" name="SendType2" 
		    		CodeData="0|^1|短信^2|电子邮件^3|打印"
	    		ondblclick="return showCodeListEx('SendType2',[this,ManageComName2],[0,1]);" 
	    		onkeyup="return showCodeListKeyEx('SendType2',[this,ManageComName2],[0,1]);" 
	    	><input class="codename" name="ManageComName2" ></td>  
    </tr>
	<tr class="common">
	<td class="title">是否已发送<input name="isALSend" onclick="return checkSendType();" type="checkbox" ></td>
  </tr>
</table>
<input value="客户提取"  onclick="return custQuery();" class="cssButton" type="button" >
<br>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanNotSendCustGrid" >
     </span> 
      </td>
   </tr>
</table>
<input class=cssbutton value="首  页" type=button onclick="NotSendCustGridTurnPage.firstPage();"> 
<input class=cssbutton value="上一页" type=button onclick="NotSendCustGridTurnPage.previousPage();">      
<input class=cssbutton value="下一页" type=button onclick="NotSendCustGridTurnPage.nextPage();"> 
<input class=cssbutton value="尾  页" type=button onclick="NotSendCustGridTurnPage.lastPage();">
<br><br>
发送方式
<input class="codeno" name="SendType3" 
		    		CodeData="0|^1|短信^2|电子邮件^3|打印"
	    		ondblclick="return showCodeListEx('SendType3',[this,ManageComName3],[0,1]);" 
	    		onkeyup="return showCodeListKeyEx('SendType3',[this,ManageComName3],[0,1]);" 
><input class="codename" name="ManageComName3" elementtype=nacessary>
	    	
是否全部发送
<input name="isAllSend" onclick="" type="checkbox" >
<br><BR>
<input value="发送"  onclick="return disSuggestSend();" class="cssButton" type="button" >
<input value="发送结果查询"  onclick="return disSendResultQuery();" class="cssButton" type="button" >
<br>
<table>
  <tr>
    <td class="titleImg" >已发送健康建议查询</td>
  </tr>
</table>
<table  class="common" >
  <tr class="common">
    <td class="title">最近一次就诊时间结束起期</td>
    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="LastVisStartDate3"></td>  
    <td class="title">最近一次就诊时间结束止期</td>
    <td class="input"><input class="coolDatePicker" dateFormat="short" name="LastVisEndDate3" ></td>  
  </tr>
	<tr class="common">
    <td class="title">健康建议发送起期</td><td class="input">
    	<input class="coolDatePicker"  dateFormat="short"name="sendStartDate3" >
    </td>  
    <td class="title">健康建议发送止期</td><td class="input">
    	<input class="coolDatePicker"  dateFormat="short" name="sendEndDate3" >
    </td>  
  </tr>
 <tr class="common">
  	<td class="title">疾病名称</td>
    <td class="input">
    	<input class="common" name="DiseaseName3"></td>
    <td class="title">疾病编码</td>
    <td class="input" ><input class="code" name="DiseaseCode3" elementtype=nacessary
    	ondblclick="return showCodeList('lldiseas',[this,DiseaseName3],[1,0],null,fm.DiseaseName3.value,'icdname',1);" 
      	onkeyup="return showCodeListKey('lldiseas',[this,DDiseaseName3],[1,0]);"
     ></td>
  </tr>
  <tr class="common">
    <td class="title">最近一年就诊次数>=</td><td class="input"><input class="common" name="LastVisCount3" ></td>  
    <td class="title">最近一年医疗费用理赔额度</td>
    <td class="code">
    	<input class="codeNo" name="HCostClaimFeeCode3"
    			CodeData="0|^1|1万元以上|10000^2|2万元以上|20000^3|5万元以上|50000^4|10万元以上|100000^5|15万元以上|150000"
    			ondblclick="return showCodeListEx('HCostClaimFeeCode3',[this,HCostClaimFeeName3,HCostClaimFee3],[0,1,2],null,null,null,1);"
    			onkeyup="return showCodeListEx('HCostClaimFeeCode3',[this,HCostClaimFeeName3,HCostClaimFee3],[0,1,2],null,null,null,1);"
    	><input class="codeName" name="HCostClaimFeeName3"><input type='hidden' name="HCostClaimFee3">
    </td>  
  </tr>
	<tr class="common">
    <td class="title">客户姓名</td><td class="input"><input class="common" name="CustomerName3" ></td>  
    <td class="title">客户号</td><td class="input"><input class="common" name="CustomerNo3" ></td>  
  </tr>
	<tr class="common">
		 <td class="title">发送方式</td>
		    <td class="input">
		    	<input class="codeno" name="SendType4" 
		    		CodeData="0|^1|短信^2|电子邮件^3|打印"
	    		ondblclick="return showCodeListEx('SendType4',[this,SendTypeName4],[0,1]);" 
	    		onkeyup="return showCodeListKeyEx('SendType4',[this,SendTypeName4],[0,1]);" 
	    ><input class="codename" name="SendTypeName4" ></td>  
    </tr>
</table>
<input value="查询"  onclick="return disAlQuery();" class="cssButton" type="button" >
<br>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanAlSendGrid" >
     </span> 
      </td>
   </tr>
</table>
<input class=cssbutton value="首  页" type=button onclick="AlSendGridTurnPage.firstPage();"> 
<input class=cssbutton value="上一页" type=button onclick="AlSendGridTurnPage.previousPage();">      
<input class=cssbutton value="下一页" type=button onclick="AlSendGridTurnPage.nextPage();"> 
<input class=cssbutton value="尾  页" type=button onclick="AlSendGridTurnPage.lastPage();">
<br><br>
<table>
  <tr>
    <td class="titleImg" >健康建议</td>
  </tr>
</table>
<textarea class="common" name="queryAfterSuggest" cols="100%" rows="2" ></textarea>
<br><BR>
<!-- 疾病操作方式-->
<input type="hidden" name="fmtransact" value="">
<!-- 疾病具体操作类型 -->
<input type="hidden" name="DisDetialOperate" value="">
</form>
</div>

<!-- ************************************ 按客户健康建议发送********************************** -->


<div id="ByCustDiv" name="ByCustDiv" style="display:'none'">
	<form action="#" method=post name=fmsave target="fraSubmit">
	
	<table>
	  <tr>
	    <td class="titleImg" >按客户健康建议录入</td>
	  </tr>
	</table>
	<table  class="common" >
	  <tr class="common">
	    <td class="title">客户姓名</td><td class="input"><input class="common" name="CCustomerName" ></td>  
	    <td class="title">客户号</td><td class="input"><input class="common" name="CCustomerNo" ></td>  
	  </tr>
	 <tr>
	    <td class="title">证件类型</td>
	    <td class="input">
	    	<input class="codeNo" name="CIDType" 
	    	 ondblclick="return showCodeList('idtype',[this,CIDTypeName],[0,1],null,null,null,1);" 
		     onkeyup="return showCodeListKey('idtype',[this,CIDTypeName],[0,1]);"
		    ><input class=codename name=CIDTypeName >
	    </td>  
	    <td class="title">证件号码</td><td class="input"><input class="common" name="CIDNo" ></td>  
	  </tr>
	<tr class="common">
	    <td class="title">最近一次就诊时间结束起期</td>
	    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="CLastVisStartDate"></td>  
	    <td class="title">最近一次就诊时间结束止期</td>
	    <td class="input"><input class="coolDatePicker" dateFormat="short" name="CLastVisEndDate" ></td>  
    </tr>
	<tr class="common">
	  	<td class="title">疾病名称</td>
	    <td class="input">
	    	<input class="common" name="CDiseaseName"></td>
	    <td class="title">疾病编码</td>
	    <td class="input" ><input class="code" name="CDiseaseCode"
			ondblclick="return showCodeList('lldiseas',[this,CDiseaseName],[1,0],null,fmsave.CDiseaseName.value,'icdname',1);" 
	      	onkeyup="return showCodeListKey('lldiseas',[this,CDiseaseName],[1,0]);"
	     ></td>
	  </tr>
		<tr class="common">
	    <td class="title">最近一年就诊次数>=</td><td class="input"><input class="common" name="CLastVisCount" ></td>  
	   <td class="title">就诊方式</td>
	    <td class="input">
	    	<input class="codeno" name="CVistType"
	    		CodeData="0|^1|全部^2|门诊^3|住院" 
	    		ondblclick="return showCodeListEx('CVistType',[this,CVistTypeName],[0,1],null,null,null,1);" 
	    		onkeyup="return showCodeListKeyEx('CVistType',[this,CVistTypeName],[0,1],null,null,null,1);" 
	    	><input class="codename" name="CVistTypeName">
	    </td>  
	  </tr>
	  <tr class="common">
		<td class="title">最近一年医疗费用理赔额度</td>
	    <td class="code">
	    	<input class="codeNo" name="CHCostClaimFeeCode"
	    			CodeData="0|^1|1万元以上|10000^2|2万元以上|20000^3|5万元以上|50000^4|10万元以上|100000^5|15万元以上|150000"
	    			ondblclick="return showCodeListEx('CHCostClaimFeeCode',[this,CHCostClaimFeeName,CHCostClaimFee],[0,1,2],null,null,null,1);"
	    			onkeyup="return showCodeListEx('CHCostClaimFeeCode',[this,HCostClaimFeeName,CHCostClaimFee],[0,1,2],null,null,null,1);"
	    	><input class="codeName" name="CHCostClaimFeeName"><input type='hidden' name="CHCostClaimFee">
	    </td>  
	  </tr>
	</table>
	<input value="客户提取"  onclick="return byCustQuery();" class="cssButton" type="button" >
	<br>
	<table  class= common>
	   <tr  class= common>
	      <td text-align: left colSpan=1>
	     <span id="spanCustNotSugGrid" >
	     </span> 
	      </td>
	   </tr>
	</table>
	<input class=cssbutton value="首  页" type=button onclick="CustNotSugGridTurnPage.firstPage();"> 
	<input class=cssbutton value="上一页" type=button onclick="CustNotSugGridTurnPage.previousPage();">      
	<input class=cssbutton value="下一页" type=button onclick="CustNotSugGridTurnPage.nextPage();"> 
	<input class=cssbutton value="尾  页" type=button onclick="CustNotSugGridTurnPage.lastPage();">
	<br><br>
	<table>
	  <tr>
	    <td class="titleImg" >健康建议</td>
	  </tr>
	</table>
	<textarea class="common" name="CSuggest" cols="100%" rows="2" ></textarea>
	<br><BR>
	<input value="保存"  onclick="return cSaveSuggest();" class="cssButton" type="button" >
	<br>
	发送方式
	<input class="codeno" name="CSendType" 
		    		CodeData="0|^1|短信^2|电子邮件^3|打印"
	    		ondblclick="return showCodeListEx('CSendType',[this,CSendTypeName],[0,1]);" 
	    		onkeyup="return showCodeListKeyEx('CSendType',[this,CSendTypeName],[0,1]);" 
	><input class="codename" name="CSendTypeName" elementtype=nacessary>
	<br><BR>
	<input value="发送"  onclick="return cSignleSend();" class="cssButton" type="button" >
	<input value="发送结果查询"  onclick=" return cSendResultQuery();" class="cssButton" type="button" >
	<br>
	<table>
	  <tr>
	    <td class="titleImg" >已建立健康建议客户提取及健康建议发送</td>
	  </tr>
	</table>
	<table  class="common" >
	  <tr class="common">
	    <td class="title">客户姓名</td><td class="input"><input class="common" name="CCustomerName2" ></td>  
	    <td class="title">客户号</td><td class="input"><input class="common" name="CCustomerNo2" ></td>  
	  </tr>
		<tr class="common">
	    <td class="title">客户健康建议录入时间起期</td>
	    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="CCustSuggestStartDate2" elementtype=nacessary></td>  
	    <td class="title">客户健康建议录入时间止期</td>
	    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="CCustSuggestEndDate2" elementtype=nacessary></td>  
	  </tr>
		<tr class="common">
	    <td class="title">发送方式</td>
	    <td class="input">
	    	<input class="codeno" name="CSendType2" 
		    		CodeData="0|^1|短信^2|电子邮件^3|打印"
	    		ondblclick="return showCodeListEx('CSendType2',[this,CSendTypeName2],[0,1]);" 
	    		onkeyup="return showCodeListKeyEx('CSendType2',[this,CSendTypeName2],[0,1]);" 
			><input class="codename" name="CSendTypeName2" >
	    </td>  
	    <td class="title">是否已发送</td>
	    <td class="input">
	    	<input name="isCALSend" onclick="return checkCSendType();" type="checkbox" >
	    </td>  
	  </tr>
	</table>
	<input value="客户提取"  onclick="return cAlSuggestCustQuery();" class="cssButton" type="button" >
	<br>
	<table  class= common>
	   <tr  class= common>
	      <td text-align: left colSpan=1>
	     <span id="spanCustAlSugGrid" >
	     </span> 
	      </td>
	   </tr>
	</table>
	<input class=cssbutton value="首  页" type=button onclick="CustAlSugGridTurnPage.firstPage();"> 
	<input class=cssbutton value="上一页" type=button onclick="CustAlSugGridTurnPage.previousPage();">      
	<input class=cssbutton value="下一页" type=button onclick="CustAlSugGridTurnPage.nextPage();"> 
	<input class=cssbutton value="尾  页" type=button onclick="CustAlSugGridTurnPage.lastPage();">
	<br><br>
	<table>
	  <tr>
	    <td class="titleImg" >健康建议</td>
	  </tr>
	</table>
	<textarea class="common" name="CCustSuggest2" cols="100%" rows="2" ></textarea>
	<br><BR>
	<input value="修改"  onclick="return CCustSuggestUp();" class="cssButton" type="button" >
	<input value="删除"  onclick="return CCustSuggestDel();" class="cssButton" type="button" >
	<br><br>
	发送方式
	<input class="codeno" name="CSendType3" 
		    		CodeData="0|^1|短信^2|电子邮件^3|打印"
	    		ondblclick="return showCodeListEx('CSendType3',[this,CSendTypeName3],[0,1]);" 
	    		onkeyup="return showCodeListKeyEx('CSendType3',[this,CSendTypeName3],[0,1]);" 
	><input class="codename" name="CSendTypeName3" >
	是否全部发送
	<input name="isCAllSend" onclick="" type="checkbox" >
	<br><BR>
	<input value="发送"  onclick="return cCustSend();" class="cssButton" type="button" >
	<input value="发送结果查询"  onclick="return disCustAlSendResultQuery();" class="cssButton" type="button" >
	<br>
	<table>
	  <tr>
	    <td class="titleImg" >已发送健康建议查询</td>
	  </tr>
	</table>
	<table  class="common" >
	  <tr class="common">
	    <td class="title">最近一次就诊时间结束起期</td>
	    <td class="input"><input class="coolDatePicker"  dateFormat="short" name="CLastVisStartDate3"></td>  
	    <td class="title">最近一次就诊时间结束止期</td>
	    <td class="input"><input class="coolDatePicker" dateFormat="short" name="CLastVisEndDate3" ></td>  
     </tr>
		<tr class="common">
	    <td class="title">健康建议发送起期</td><td class="input">
	    	<input class="coolDatePicker"  dateFormat="short"  name="CSuggestSendStartDate3" >
	    </td>  
	    <td class="title">健康建议发送止期</td><td class="input">
	    	<input class="coolDatePicker" dateFormat="short" name="CSuggestSendEndDate3" >
	    </td>  
	  </tr>
	  <tr class="common">
	  	<td class="title">疾病名称</td>
	    <td class="input">
	    	<input class="common" name="CDiseaseName3"></td>
	    <td class="title">疾病编码</td>
	    <td class="input" ><input class="code" name="CDiseaseCode3"
			ondblclick="return showCodeList('lldiseas',[this,CDiseaseName3],[1,0],null,fmsave.CDiseaseName3.value,'icdname',1);" 
	      	onkeyup="return showCodeListKey('lldiseas',[this,CDiseaseName3],[1,0]);"
	     ></td>
	  </tr>
		<tr class="common">
	    <td class="title">最近一年就诊次数>=</td><td class="input"><input class="common" name="CLastVisCount3" ></td> 
	    <td class="title">最近一年医疗费用理赔额度</td>
	    <td class="code">
	    	<input class="codeNo" name="CHCostClaimFeeCode3"
	    			CodeData="0|^1|1万元以上|10000^2|2万元以上|20000^3|5万元以上|50000^4|10万元以上|100000^5|15万元以上|150000"
	    			ondblclick="return showCodeListEx('CHCostClaimFeeCode3',[this,CHCostClaimFeeName3,CHCostClaimFee3],[0,1,2],null,null,null,1);"
	    			onkeyup="return showCodeListEx('CHCostClaimFeeCode3',[this,HCostClaimFeeName3,CHCostClaimFee3],[0,1,2],null,null,null,1);"
	    	><input class="codeName" name="CHCostClaimFeeName3"><input type='hidden' name="CHCostClaimFee3">
	    </td>  
	  </tr>
		<tr class="common">
	    <td class="title">客户姓名</td><td class="input"><input class="common" name="CCustomerName3" ></td>  
	    <td class="title">客户号</td><td class="input"><input class="common" name="CCustomerNo3" ></td>  
	  </tr>
		<tr class="common">
	    <td class="title">发送方式</td>
	    <td class="input">
	    <input class="codeno" name="CCSendType3" 
		    		CodeData="0|^1|短信^2|电子邮件^3|打印"
	    		ondblclick="return showCodeListEx('CCSendType3',[this,CCSendTypeName3],[0,1]);" 
	    		onkeyup="return showCodeListKeyEx('CCSendType3',[this,CCSendTypeName3],[0,1]);" 
		><input class="codename" name="CCSendTypeName3">
	    </td>  
	  </tr>
	</table>
	<input value="查询"  onclick="return cCustAlSendResultQuery();" class="cssButton" type="button" >
	<br>
	<table  class= common>
	   <tr  class= common>
	      <td text-align: left colSpan=1>
	     <span id="spanCustAlSendGrid" >
	     </span> 
	      </td>
	   </tr>
	</table>
	<input class=cssbutton value="首  页" type=button onclick="CustAlSendGridTurnPage.firstPage();"> 
	<input class=cssbutton value="上一页" type=button onclick="CustAlSendGridTurnPage.previousPage();">      
	<input class=cssbutton value="下一页" type=button onclick="CustAlSendGridTurnPage.nextPage();"> 
	<input class=cssbutton value="尾  页" type=button onclick="CustAlSendGridTurnPage.lastPage();">
	<br><br>
	<table>
	  <tr>
	    <td class="titleImg" >健康建议</td>
	  </tr>
	</table>
	<textarea class="common" name="CCustSuggest" cols="100%" rows="2" ></textarea>
	<br><BR>
	<!-- 客户操作类型-->
	<input type="hidden" name="custfmTransact" value="">
	<!-- 客户具体操作类型 -->
	<input type="hidden" name="custfmDetialOperate" value="">
	</form>
</div>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
