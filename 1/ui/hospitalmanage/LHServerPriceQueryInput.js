/** 
 * 程序名称：LHServerPriceQueryInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-03-09 15:27:48
 * 创建人  ：郭丽颖
 * 更新记录：
 * 更新人 : 
 * 更新日期 : 
 * 更新原因/内容: 插入新的字段
 */

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
var no;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句	
	//var no;
	if(no=="1")	
	{	 
			var LEN = manageCom.length;
		  if(LEN == 8){LEN = 4; }   
			var strSql = "select distinct a.contrano, a.contrano,a.ContraName,"
								+"(select HospitName from LDHospital where LDHospital.HospitCode=a.Contraobj),"
								+"(select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=a.ContraType),"
			          +"(select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= a.DutyItemCode),"
			          +"( case when a.ServPriceType = '1' then '服务单价' else '体检价格' end),"
			          +"( case a.ExpType when '1' then '健康体检费用' when '2' then '健康管理技术费用' when '3' then '医师费用' when '4' then '其他费用' else '未知费用' end ),"
			          +"(select HospitCode from LDHospital where LDHospital.HospitCode=a.Contraobj),"
			          +" a.ServPriceType ,a.ContraItemNo "
			          +" from LHContServPrice a ,LDTestPriceMgt b "
						    +" where  exists(select 'x' from lhgroupcont b where b.contrano=a.contrano) and  "
						    +"a.contrano=b.contrano and a.ContraItemNo=b.ContraItemNo "
						    + getWherePart("a.ContraNo", "ContraNo","like")
						    + getWherePart("a.ServPriceType", "ServicePriceCode","like")
						    + getWherePart("a.ExpType", "BalanceTypeCode","like")
						    +" and a.managecom like '"+manageCom.substring(0,LEN)+"%%' order by a.ContraNo"
		  					;
		  	//	alert(strSql);//+"( case when a.ExpType = '1' then '支票' else '转帐' end),"
			 turnPage.queryModal(strSql, LHContServPriceGrid);
	}
	if(no=="2")
	{
			var LEN = manageCom.length;
		  if(LEN == 8){LEN = 4; }   
			var strSql = "select distinct a.contrano, a.contrano,a.ContraName,"
								+"(select DoctName from LDDoctor where LDDoctor.Doctno=a.Contraobj),"
								+"(select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=a.ContraType),"
			          +"(select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= a.DutyItemCode),"
			          +"( case when a.ServPriceType = '1' then '服务单价' else '体检价格' end),"
			          //+"( case when a.ExpType = '1' then '支票' else '转帐' end),"  
			          +"( case a.ExpType when '1' then '健康体检费用' when '2' then '健康管理技术费用' when '3' then '医师费用' when '4' then '其他费用' else '未知费用' end ),"
			          +"(select Doctno from LDDoctor where LDDoctor.Doctno=a.Contraobj),"
			          +" a.ServPriceType ,a.ContraItemNo "
			          +" from LHContServPrice a ,LDTestPriceMgt b "
						    +" where  exists(select 'x' from lhunitcontra b where b.contrano=a.contrano) and "
						    +"a.contrano=b.contrano and a.ContraItemNo=b.ContraItemNo "
						    + getWherePart("a.ContraNo", "ContraNo","like")
						    + getWherePart("a.ServPriceType", "ServicePriceCode","like")
						    + getWherePart("a.ExpType", "BalanceTypeCode","like")
						    +" and a.managecom like '"+manageCom.substring(0,LEN)+"%%' order by a.ContraNo"
		  					;
		  				//	alert(strSql);
		  				//	alert(easyExecSql(strSql));
			 turnPage.queryModal(strSql, LHContServPriceGrid);
	}
}


function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHContServPriceGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				arrReturn = getQueryResult();
				//alert(arrReturn);
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHContServPriceGrid.getSelNo();
	//alert("111" + tRow);
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LHContServPriceGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
