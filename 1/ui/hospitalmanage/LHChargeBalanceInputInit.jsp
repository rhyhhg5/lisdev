<%
//程序名称：LHChargeBalanceInputInit.jsp
//程序功能：费用结算方式设置信息管理(初始化页面)
//创建日期：2006-03-01 9:21:30
//创建人  ：GuoLiying
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%> 
<%
       String no = request.getParameter("ContraNo");
       String itemno = request.getParameter("ContraItemNo");
       String groupunit=request.getParameter("groupunit");
%>                             
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ContraNo').value = "";
    fm.all('ContraName').value = "";
    fm.all('HospitName').value = "";
    fm.all('ContraType_ch').value = "";
    fm.all('ContraItemName').value = "";
    fm.all('ContraNowState_ch').value = "";
    fm.all('HiddenBtn').value="<%=groupunit%>";
    fm.all('ContraItemNo').value="<%=itemno%>";

  }
  catch(ex)
  {
    alert("在LHChargeBalanceInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try{}
  catch(ex)
  {
    alert("在LHChargeBalanceInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
	try
	{
	    initInpBox();
	    fm.all('querybutton').disabled=true;
	    fm.all('deleteButton').disabled=true;
	    var flag = "<%=groupunit%>";
	    if(flag=="1") //团体合同管理进入此页面
	    {
	       getFirstPageNo();
	    }
	    if(flag=="2")//个人合同管理进入些页面
	    {
	   	  getFirstPageNo2();
	    }
	    if(flag=="6")//从医疗机构检索中查询进入此页面
	    {
			fm.all('querybutton').disabled=true;
	        fm.all('deleteButton').disabled=true;
	        fm.all('modifyButton').disabled=true;
	        fm.all('saveButton').disabled=true;
	        document.getElementsByName('saveButton')[0].disabled=true;
	        getFirstPageNo();
	    }
	}
	catch(re)
	{
		alert("LHChargeBalanceInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}  
function getFirstPageNo()
{
	try                 
	{
		var sql="	select a.ContraNo, a.ContraName,(select h.hospitname from ldhospital h where h.hospitcode = a.hospitcode),"
			+" (select c.Codename from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
			+" (select d.DutyItemName from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode), "
		    +" ( case when e.dutystate = '1' then '有效' else '无效' end), e.ContraItemNo ,a.HospitCode, "
		    +" e.contratype,e.dutyitemcode,e.dutystate "
			+" from   LHGroupcont a,lhcontitem e "
			+" where  a.ContraNo='"+ "<%=no%>" +"' "
			+" and    e.ContraItemNo='"+ "<%=itemno%>" +"' "
			+" and    a.ContraNo=e.ContraNo" 
    		; 
	    var arrResult = easyExecSql(sql);
     	fm.all('ContraNo').value			= arrResult[0][0];	//合同号码
     	fm.all('ContraName').value			= arrResult[0][1];	//合同名称
		fm.all('HospitName').value			= arrResult[0][2];	//医院名称
		fm.all('ContraType_ch').value		= arrResult[0][3];	//合同责任项目类型 
		fm.all('ContraItemName').value		= arrResult[0][4];	//合同责任项目名称
		fm.all('ContraNowState_ch').value	= arrResult[0][5];	//责任当前状态
        fm.all('ContraItemNo').value		= arrResult[0][6];	//合同责任流水号
        fm.all('HospitCode').value			= arrResult[0][7];	//
        fm.all('ContraType').value			= arrResult[0][8];	//合同责任项目类型代码
        fm.all('ContraItemCode').value		= arrResult[0][9];	//合同责任项目代码
        fm.all('ContraNowState').value		= arrResult[0][10];	//责任当前状态
         
        
		var sqlBalance = " select "
		var sqlFee = " select ContBalanceNo,BalanceDate,BalanceRemindDate,BalanceType,BankCode,BankName, "
					+" AccName,Accounts,Drawer,MakeDate,MakeTime,"
					+" (select codename from ldcode where codetype='paymode' and code=BalanceType)" 
					+" from lhchargebalance "
					+" where Contrano = '"+ "<%=no%>" +"' and ContraItemNo = '"+ "<%=itemno%>" +"'"

		var arrFee = easyExecSql(sqlFee);
		if(arrFee == null) return false;
		fm.all('ContBalanceNo').value = arrFee[0][0];
		fm.all('BalanceDate').value = arrFee[0][1];
		fm.all('BalanceRemindDate').value = arrFee[0][2];
		fm.all('BalanceType').value = arrFee[0][3];
		fm.all('BankCode').value = arrFee[0][4];
		fm.all('BankName').value = arrFee[0][5];
		fm.all('AccName').value = arrFee[0][6];
		fm.all('Accounts').value = arrFee[0][7];
		fm.all('Drawer').value = arrFee[0][8];
		fm.all('MakeDate').value = arrFee[0][9];
		fm.all('MakeTime').value = arrFee[0][10];
		fm.all('BalanceTypeName').value = arrFee[0][11];
//			initLHChargeBalanceGrid();
//			var strSql="select BalanceDate,"
//		        +" BalanceRemindDate,( case when BalanceType = '1' then '支票' else '转帐' end ),"
//		        +" Accounts,BalanceType,ContBalanceNo from LHChargeBalance"
//		        +" where contrano ='"+ "<=no>" +"' and ContraItemNo ='"+ "<=itemno>" +"'";
//		        arrResult2 = easyExecSql(strSql);
//		        turnPage.queryModal(strSql, LHChargeBalanceGrid);
	}
	catch(ex)
	{
		alert("在LHChargeBalanceInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误1!");
	}
}

function getFirstPageNo2()
{  
	try                 
	{
		var sql=" select a.ContraNo, a.ContraName,b.DoctName,"
			 +" (select c.Codename from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
			 +" (select d.DutyItemName from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode), "
		     +" ( case when a.ContraState = '1' then '有效' else '无效' end),	"	 
		     +"  e.ContraItemNo,b.Doctno, "
		     +"(select c.code from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
		     +"(select d.DutyItemCode from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode),"
		     +" a.ContraState "
			 +"	from   LHUnitContra a,LDDoctor b, lhcontitem e  "
			 +" where  a.ContraNo='"+ "<%=no%>" +"' "
			 +"	and    a.DoctNo = b.DoctNo "
			 +" and    e.ContraItemNo='"+ "<%=itemno%>" +"' "
			 +" and    a.ContraNo=e.ContraNo" 
			;   
		var arrResult = easyExecSql(sql);
//		fm.all('ContraNo').value				  = arrResult[0][0];
//		fm.all('ContraName').value			  = arrResult[0][1];
//		fm.all('HospitName').value        = arrResult[0][2];
//		fm.all('ContraType_ch').value 		= arrResult[0][3];
//		fm.all('ContraNowName_ch').value	= arrResult[0][4];
//		fm.all('ContraNowState_ch').value	= arrResult[0][5];if(arrResult[0][5]=="1"){fm.all('ContraState_ch').value="有效";}if(arrResult[0][6]=="2"){fm.all('ContraState_ch').value="无效";}
//        fm.all('ContraItemNo').value      = arrResult[0][6];////系统流水号码
//        fm.all('HospitCode').value        = arrResult[0][7];//合同合作机构编号
//        fm.all('ContraType').value        = arrResult[0][8];//合同责任类型编号
//        fm.all('ContraNowName').value     = arrResult[0][9];//合同责任项目编号
//        fm.all('ContraNowState').value    = arrResult[0][10];//合同责任项目编号


     	fm.all('ContraNo').value			= arrResult[0][0];	//合同号码
     	fm.all('ContraName').value			= arrResult[0][1];	//合同名称
		fm.all('HospitName').value			= arrResult[0][2];	//医院名称
		fm.all('ContraType_ch').value		= arrResult[0][3];	//合同责任项目类型 
		fm.all('ContraItemName').value		= arrResult[0][4];	//合同责任项目名称
		fm.all('ContraNowState_ch').value	= arrResult[0][5];	//责任当前状态
        fm.all('ContraItemNo').value		= arrResult[0][6];	//合同责任流水号
        fm.all('HospitCode').value			= arrResult[0][7];	//
        fm.all('ContraType').value			= arrResult[0][8];	//合同责任项目类型代码
        fm.all('ContraItemCode').value		= arrResult[0][9];	//合同责任项目代码
        fm.all('ContraNowState').value		= arrResult[0][10];	//责任当前状态


          
//        initLHChargeBalanceGrid();
//        var strSql="select BalanceDate,"
//		        +" BalanceRemindDate,( case when BalanceType = '1' then '支票' else '转帐' end ),"
//		        +" Accounts,BalanceType,ContBalanceNo from LHChargeBalance"
//		        +" where contrano ='"+ "<=no>" +"' and ContraItemNo ='"+ "<=itemno>" +"'";
//		        arrResult2 = easyExecSql(strSql);
//		        turnPage.queryModal(strSql, LHChargeBalanceGrid);


		var sqlBalance = " select "
		var sqlFee = " select ContBalanceNo,BalanceDate,BalanceRemindDate,BalanceType,BankCode,BankName, "
					+" AccName,Accounts,Drawer,MakeDate,MakeTime,"
					+" (select codename from ldcode where codetype='paymode' and code=BalanceType)" 
					+" from lhchargebalance "
					+" where Contrano = '"+ "<%=no%>" +"' and ContraItemNo = '"+ "<%=itemno%>" +"'"

		var arrFee = easyExecSql(sqlFee);
		if(arrFee == null) return false;
		fm.all('ContBalanceNo').value = arrFee[0][0];
		fm.all('BalanceDate').value = arrFee[0][1];
		fm.all('BalanceRemindDate').value = arrFee[0][2];
		fm.all('BalanceType').value = arrFee[0][3];
		fm.all('BankCode').value = arrFee[0][4];
		fm.all('BankName').value = arrFee[0][5];
		fm.all('AccName').value = arrFee[0][6];
		fm.all('Accounts').value = arrFee[0][7];
		fm.all('Drawer').value = arrFee[0][8];
		fm.all('MakeDate').value = arrFee[0][9];
		fm.all('MakeTime').value = arrFee[0][10];
		fm.all('BalanceTypeName').value = arrFee[0][11];

	}
	catch(ex)
	{
		alert("在LHChargeBalanceInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误2!");
	}
}
	var d = new Date();
	var h = d.getYear();
	var m = d.getMonth(); 
	var day = d.getDate();  
	var Date1;       
	if(h<10){h = "0"+d.getYear();}  
	if(m<9){ m++; m = "0"+m;}
	else{m++;}
	if(day<10){day = "0"+d.getDate();}
	Date1 = h+"-"+m+"-"+day;
	
function initLHChargeBalanceGrid() 
{                            
	var iArray = new Array();                               
                                                           
    try 
    {                                                   
    	iArray[0]=new Array();                                
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名               
    	iArray[0][3]=0;         				//列名                 
    	iArray[0][4]="";         //列名   
    	
    	iArray[1]=new Array(); 
		iArray[1][0]="费用结算时间";   
		iArray[1][1]="113px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
    	iArray[1][4]="";         //列名  
    	iArray[1][14]=Date1; 

	    iArray[2]=new Array(); 
		iArray[2][0]="费用提醒时间";   
		iArray[2][1]="113px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;
		iArray[2][4]="";         //列名 
		iArray[2][14]=Date1;   
	
		
		iArray[3]=new Array(); 
		iArray[3][0]="结算方式";   
		iArray[3][1]="113px";   
		iArray[3][2]=20;        
		iArray[3][3]=2;
	    iArray[3][4]="hmbalancemode";  
	    iArray[3][5]="3|5";       //列名 
	    iArray[3][6]="0|1"; 
    	iArray[3][18]="230"; 
		
	    iArray[4]=new Array(); 
		iArray[4][0]="银行帐号";   
		iArray[4][1]="112px";   
		iArray[4][2]=20;        
		iArray[4][3]=1;
		iArray[4][4]="";         //列名   

	    iArray[5]=new Array(); 
		iArray[5][0]="BalanceType";   
		iArray[5][1]="0px";   
		iArray[5][2]=20;        
		iArray[5][3]=3; 
	    iArray[5][4]="";         //列名   
	  
	    iArray[6]=new Array(); 
		iArray[6][0]="ContraItemNo";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3; 
	    iArray[6][4]="";         //列名   

		LHChargeBalanceGrid = new MulLineEnter( "fm" , "LHChargeBalanceGrid" );
	    //这些属性必须在loadMulLine前

	    LHChargeBalanceGrid.mulLineCount = 0;
	    LHChargeBalanceGrid.displayTitle = 1;
	    LHChargeBalanceGrid.hiddenPlus = 0;
	    LHChargeBalanceGrid.hiddenSubtraction = 0;
	    LHChargeBalanceGrid.canSel = 1;
	    LHChargeBalanceGrid.loadMulLine(iArray);
	    //这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{ 
    	alert(ex);
	}
}

</script>
  