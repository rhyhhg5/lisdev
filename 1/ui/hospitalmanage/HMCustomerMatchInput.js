var turnPage = new turnPageClass();
var showInfo;
var ImportPath;

function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}


//识别文件下载
function fileDownload()
{
	
	//文件下载路径
	getOutFilePath() ;

	if(fm.FileName.value=null || fm.FileName.value=="")
	{
		alert("请选择导入文件！");
		return false;
	}
	
	var showStr="正在上载数据……";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action =  "./HMCustomerMatchSave.jsp?Url="+fm.Url.value+"&MngCom="+fm.MngCom.value+"&TradeChannel="
		+fm.TradeChannel.value+"&HMRiskType="+fm.HMRiskType.value;
		
	fm.submit(); //提交
}
		
//提交后自动弹出文件下载
function downAfterSubmit(cfilePath,cflag) 
{

	try { showInfo.close(); } catch(e) {}
	
	var aflag = fm.all('downflag').value;
	
	if (cflag == 0)
	{  
		fileUrl.href = cfilePath ;
		fileUrl.click();
		fm.all('downflag').value = "";		
	}
}

//获取文件下载路径
function getOutFilePath() 
{
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'ServerURL'";
  //var strSql = "select SysVarValue from LDSysVar where SysVar = 'testurl'";
  var filePath = easyExecSql(strSql)+'temp/hospitalmanage/';

  
  fm.all('Url').value = filePath;
 
  //alert(filePath);
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo.close(); } catch(e) {}
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

