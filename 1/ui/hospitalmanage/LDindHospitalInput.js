//---------------------------------------------------
//程序名称：定点医院配置
//程序功能：
//创建日期：2011-06-02
//创建人  ：王洪亮
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------

var showInfo=null;
var turnPage=new turnPageClass();
var turnPage1=new turnPageClass();
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	  showInfo.close();
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  
	  initTextbox();
	  queryClick();  
 
}

function isSelectedHostpital()
{  
    for (i = 0; i < DataGrid.mulLineCount; i++)
	{
	    
		if (DataGrid.getSelNo(i)) 
		{
		  return true; 
		}
	}
    
    alert("请选择其中的一条记录");
    return false;
      	
}

//-------------------添加时-------
function addClick()
{
  showInfo=window.open("./LDindHospitalAddmain.jsp"); 
}
           

//修改按键
function updateClick()
{
   //判断是否选中其中一条记录
   if(!isSelectedHostpital())
   {
      return false;
   }
   var selHostCode=null;
   var selManageCom=null;
   var selRiskcode=null;
   var rowNumSel=DataGrid.getSelNo()-1;
   selHostCode=DataGrid.getRowColData(rowNumSel,1);
   selManageCom=DataGrid.getRowColData(rowNumSel,3);
   selRiskcode=DataGrid.getRowColData(rowNumSel,4);
   if(trim(selHostCode)==""||trim(selHostCode)=="null"||trim(selHostCode)==null)
   {
		alert("第"+(i+1)+"行医疗机构代码不能为空");
	    return false;
   }
   if(trim(selManageCom)==""||trim(selManageCom)=="null"||trim(selManageCom)==null)
   {
		alert("第"+(i+1)+"行管理机构不能为空");
	    return false;
   }
   if(trim(selRiskcode)==""||trim(selRiskcode)=="null"||trim(selRiskcode)==null)
   {
		alert("第"+(i+1)+"行险种代码不能为空");
	    return false;
   }
   showInfo=window.open("./LDindHospitalUpdateMain.jsp?HospitCode="+selHostCode+"&ManageCom="+selManageCom+"&Riskcode="+selRiskcode);
}
//-----------------------------------------------------
//删除按键
function deleteClick()
{ 
   //判断是否选中其中一条记录
   if(!isSelectedHostpital())
   {
      return false;
   }
   if(!confirm("确认要删除该条记录"))
   {
      return false;
   }
   var selHostCode=null;
   var selManageCom=null;
   var selRiskcode=null;
   var rowNumSel=DataGrid.getSelNo()-1;
   selHostCode=DataGrid.getRowColData(rowNumSel,1);
   selManageCom=DataGrid.getRowColData(rowNumSel,3);
   selRiskcode=DataGrid.getRowColData(rowNumSel,4);

   var sql="select HospitCode,HospitName,ManageCom,Riskcode,Validate,CInvalidate from LDDingHospital "
		  +" where 1=1 "
		  +" and HospitCode='"+selHostCode+"' "
		  +" and ManageCom='"+selManageCom+"' "
		  +" and Riskcode='"+selRiskcode+"' "
		  +" with ur "; 
   var arrResult = easyExecSql(sql); 
   if(arrResult==null)
   {
        alert("该医院不存在，请重新选择！");
        return false;
   }      
   fm.all('HospitCode').value = arrResult[0][0];
   fm.all('HospitalName').value = arrResult[0][1];
   fm.all('ManageCom').value = arrResult[0][2];
   fm.all('Riskcode').value =arrResult[0][3];
   fm.all('Validate').value = arrResult[0][4];
   fm.all('CInvalidate').value = arrResult[0][5];
   
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
   fm.all("fmtransact").value="DELETE||MAIN";
   fm.submit();
   
}

function checkData()
{
   var Validate= fm.all('Validate').value;
   var CInvalidate=fm.all('CInvalidate').value;
   if(trim(Validate)!=""&&trim(CInvalidate)!="")
   {
      if(CInvalidate<Validate)
      {
         alert("终止日期不能早于生效日期！");
         return false;
      }
   }
   return true;
}
//------------------------------------------------------
//查询按键
function queryClick()
{
    document.getElementById('divDataAfter').style.display= 'none';
	if(!checkData())
    {
	    return false;
	}

    var ManageCom=fm.all("ManageCom").value;
	var HospitCode=fm.all("HospitCode").value;
	var RiskCode=fm.all("RiskCode").value;
	var ValiDate=fm.all("ValiDate").value;
	var CInValiDate=fm.all("CInValiDate").value;
	var HospitalName=fm.all("HospitalName").value;
	var strSql="";
	if(ManageCom!=""&&ManageCom!=null)
	{
		strSql+=" and ManageCom like '"+ManageCom+"%'";
	}
	if(HospitCode!=""&&HospitCode!=null)
	{
		strSql+=" and HospitCode='"+HospitCode+"'";
	}
	
	if(RiskCode!=""&&RiskCode!=null)
	{
		strSql+=" and RiskCode='"+RiskCode+"'";
	}
	
	if(ValiDate!=""&&ValiDate!=null)
	{
		strSql+=" and ValiDate>='"+ValiDate+"'";
	}
	if(CInValiDate!=""&&CInValiDate!=null)
	{
		strSql+=" and CInValiDate<='"+CInValiDate+"'";
	}
	if(HospitalName!=""&&HospitalName!=null)
	{
		strSql+=" and HospitName='"+HospitalName+"'";
	}
	
	var sql="select HospitCode,HospitName,ManageCom,Riskcode,Validate,CInvalidate,Operator,ModifyDate from LDDingHospital "
	        +" where 1=1 "
	        + strSql
	        +" with ur ";
	turnPage.queryModal(sql, DataGrid);

	if( DataGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	
 }
 
 //查询历史记录
 function afterSelect()
 {
      var selHostCode=null;
      var selManageCom=null;
      var selRiskcode=null;
      var rowNumSel=DataGrid.getSelNo()-1;
      selHostCode=DataGrid.getRowColData(rowNumSel,1);
      selManageCom=DataGrid.getRowColData(rowNumSel,3);
      selRiskcode=DataGrid.getRowColData(rowNumSel,4);
      var sql="select HospitCode,HospitName,ManageCom,Riskcode,Validate,CInvalidate , Operator,ModifyDate from LDDingHospitalB  "
		     +" where 1=1 "
		     +" and HospitCode='"+selHostCode+"' "
		     +" and ManageCom='"+selManageCom+"' "
		     +" and Riskcode='"+selRiskcode+"' "
		     +" order by modifydate desc  with ur "; 
      turnPage1.queryModal(sql, DataAfterGrid);
      if(DataAfterGrid.mulLineCount>0)
      {
         document.getElementById('divDataAfter').style.display= '';
      }
 }

