<%
//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput m_gInput=(GlobalInput)session.getValue("GI");
	String strManageCom="";
	String strCurrentDate="";
	if(m_gInput==null)
	{
		strManageCom="Unknown";
		strCurrentDate="";
	}
	else
	{
		strManageCom=m_gInput.ManageCom;
		strCurrentDate=PubFun.getCurrentDate();
	}
%>
<script language="javascript" type="text/javascript">
	function initForm()
	{
		try
		{
		   initTextbox();
		}
		catch(ex)
		{
			document.write("文本框初始化失败!");
		}
	}
	
	//初始化文本框
	function initTextbox()
	{
		document.all["ManageCom"].value="<%=strManageCom%>";
		document.all["ValiDate"].value="<%=strCurrentDate%>";
		document.all["CInValiDate"].value="<%=strCurrentDate%>";
		showAllCodeName();

	}

</script>