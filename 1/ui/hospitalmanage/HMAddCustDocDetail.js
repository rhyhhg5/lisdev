//该文件中包含客户端需要处理的函数和事件

//程序名称：HMAddCustDocDetail.js
//程序功能：添加客户档案明细
//创建日期：2010-3-8
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var polGridPage = new turnPageClass();
var ExamIndGridPage = new turnPageClass();

//查询客户详细信息
function queryCustInfo() {
	var sql = 
			" select distinct a.InsuredNo, "
	                +" a.Name, "
	                //+" a.Sex, "
	                +" (select CodeName "
                   			+" from ldcode "
                  	+" where codetype = 'sex' "
                    	  +" and code = a.Sex ), "
	                +" a.Birthday, "
	                +" b.Mobile, "
	                +" b.Phone, "
	                +" b.EMail, "
	                +" b.PostalAddress, "
	                +" b.ZipCode, "
	                +" a.modifydate, "
	                +" a.modifytime "
	  +" from LCInsured a "
	  +" left outer join LCAddress b on a.InsuredNo = b.CustomerNo "
	                             +" and a.AddressNo = b.AddressNo "
	 +" where a.InsuredNo = '"+ tCustomerNo + "' "
	 +" order by a.modifydate desc, a.modifytime desc "
	 +" fetch first 1 rows only with ur ";
	  
	var arr = easyExecSql(sql);
	//alert(arr);
	if(arr) {
		fm.Name.value = arr[0][1];
		fm.Sex.value = arr[0][2];
		fm.Birthday.value = arr[0][3];
		fm.MobileNo.value = arr[0][4];
		fm.TelNo.value = arr[0][5];
		fm.EMail.value = arr[0][6];
		fm.Addr.value = arr[0][7];
		fm.Zip.value = arr[0][8];
	}
		
}

//查询有效保单信息
function queryValidPolicyInfo() {
	var sql =
			" select ContNo, "
					 +" p.riskcode, "
					 +" (select riskname from lmrisk m where m.riskcode = p.riskcode), "
					 +" p.amnt, "
					 +" cvalidate, "
			       +" date(enddate) - 1 day,"
			       +" coalesce((select sum(RealPay) from LLClaimDetail aa where aa.PolNo = p.polno ),0), "
			       +" (select AppntName "
			          +" from lccont "
			         +" where contno = p.contno) "
			  +" from LCPol p "
			 +" where appflag = '1' "
			   +" and InsuredNo = '"+ tCustomerNo + "' "
			+" union "
			+" select ContNo, "
					 +" p.riskcode, "
					 +" (select riskname from lmrisk m where m.riskcode = p.riskcode), "
					 +" p.amnt, "
				    +" cvalidate, "
					 +" (CASE "
			         +" WHEN (SELECT date(EDORVALIDATE) - 1 day "
			                 +" FROM LPEDORITEM "
			                +" WHERE EDORNO = P.EDORNO "
			                  +" AND EDORTYPE != 'XB' "
			                  +" and contno = p.contno "
			                +" order by modifydate desc fetch first row only) is null then "
			          +" p.enddate "
			         +" else "
			          +" (SELECT EDORVALIDATE "
			             +" FROM LPEDORITEM "
			            +" WHERE EDORNO = P.EDORNO "
			              +" AND EDORTYPE != 'XB' "
			              +" and contno = p.contno "
			            +" order by modifydate desc fetch first row only) "
			       +" end), "
			       +" coalesce((select sum(RealPay) from LLClaimDetail aa where aa.PolNo = p.polno ),0), "
			       +" (select AppntName "
			          +" from lbcont "
			         +" where contno = p.contno "
			        +" union "
			        +" select appntname from lccont where contno = p.contno) "
			  +" from LBPol p "
			 +" where appflag = '1' "
			   +" and InsuredNo = '"+ tCustomerNo + "' " +" with ur ";
	polGridPage.queryModal(sql,PolGrid); 
}

//保存客户档案按钮
function saveCustDoc()
{
  //校验检查阳性指标中是规范（有重复的行，或者空的记录）
  if(!examIndisOK()) {
  		return false;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "INSERT||MAIN";
  fm.action = "./HMAddCustDocDetailSave.jsp"
  fm.submit(); //提交
}

//查询已经录入的阳性指标信息
function queryAlreadyExamIndInfo() {
	var sql = 
		" select (select MedicaItemName "
		          +" from LHCountrMedicaItem "
		         +" where MedicaItemCode = a.MedicaItemCode), "
		         +" a.MedicaItemCode, "
		         +" (case a.IsNormal when '1' then '正常' when '2' then '异常' end), "
		         +" a.TestResult, "
		         +" a.StandardMeasureUnit, "
		         +" a.TestNo, "
		         +" '', "
		         +" a.NormalValue, "
		         +" a.TestDate, "
		         +" a.IsNormal "
		  +" from HMCustExamInfo a "
		  +" where CustomerNo='" + tCustomerNo + "' "
		  +" order by a.TestNo with ur ";
	ExamIndGridPage.queryModal(sql,ExamIndGrid);
}

//查询既往病例信息
function queryPastHistoryInfo() {
	var sql = " select PastDisHistory,FamilyDisHistory,BadHabit "
					+ " from HMCustDocInfo  where CustomerNo='" + tCustomerNo +"' with ur ";
	var arr = easyExecSql(sql);
	if(arr) {
		fm.PastDisHistory.value = arr[0][0];
		fm.FamilyDisHistory.value = arr[0][1];
		fm.BadHabit.value = arr[0][2];
	}
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    initForm();
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//校验检查阳性指标中是规范（有重复的行，或者空的记录）
function examIndisOK(){
	
	var examIndGridLineCount = ExamIndGrid.mulLineCount;
	if(examIndGridLineCount == 1) {
		var tCode=ExamIndGrid.getRowColData(0,1);
		if(tCode == null || tCode == "") {
			alert("检查项目名称不能为空");
			return false;
		}
	} else {
	
		for(var previousRow=0;previousRow<=examIndGridLineCount-2;previousRow++ ) {	//从第一行开始逐行判断
			
				var tPreCode=ExamIndGrid.getRowColData(previousRow,1); //前一行的值
			
			for (var nextRow=previousRow+1; nextRow<examIndGridLineCount; nextRow++) //从指针下一行开始读
		    {
		    	var tNxtCode=ExamIndGrid.getRowColData(nextRow,1);
				//alert(tNxtCode);
				if(tNxtCode==null || tNxtCode=="")
				{
				    alert("在套餐项目列表中: 第"+(nextRow+1)+"行【检查项目名称】不能为空！");
					return false;
				}
				if(tPreCode==tNxtCode) {  //重复的行
					alert("在体检服务项目列表中: 第"+(nextRow+1)+"行与第" + (previousRow+1) + "行的【检查项目】重复！");
					return false;
				}
		    }
		}
	}
	return true;
}
