<%
//程序名称：LHServerItemPriceQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-22 16:59:28
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ServItemCode').value = "";
    fm.all('ServItemName').value = "";
    fm.all('ServPriceCode').value = "";
    fm.all('ServPriceName').value = "";
  }
  catch(ex) {
    alert("在LHServerItemPriceQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHServerItemPriceGrid();  
  }
  catch(re) {
    alert("LHServerItemPriceQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHServerItemPriceGrid;
function initLHServerItemPriceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="标准服务项目代码";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="标准服务项目名称";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="定价方式代码";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="定价方式名称";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=0;         		//列名
    
	iArray[5]=new Array();
    iArray[5][0]="comid";         		//列名
    iArray[5][1]="30px";         		//列名
    iArray[5][3]=0;         		//列名
    
	iArray[6]=new Array();
    iArray[6][0]="makedate";      		//列名
    iArray[6][1]="30px";         		//列名
    iArray[6][3]=0;         		//列名
    
	iArray[7]=new Array();
    iArray[7][0]="maketime";         	//列名
    iArray[7][1]="30px";         		//列名
    iArray[7][3]=0;         		//列名
    
    
    LHServerItemPriceGrid = new MulLineEnter( "fm" , "LHServerItemPriceGrid" ); 
	LHServerItemPriceGrid.hiddenPlus = 1;
	LHServerItemPriceGrid.canSel = 1;
		
    //这些属性必须在loadMulLine前
/*
    LHServerItemPriceGrid.mulLineCount = 0;   
    LHServerItemPriceGrid.displayTitle = 1;
    LHServerItemPriceGrid.hiddenSubtraction = 1;
    LHServerItemPriceGrid.canChk = 0;
    LHServerItemPriceGrid.selBoxEventFuncName = "showOne";
*/
    LHServerItemPriceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHServerItemPriceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
