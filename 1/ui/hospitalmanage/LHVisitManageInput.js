var turnPage = new turnPageClass();
var showInfo;

function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

//探访条件设定
function VisitCondition()
{
	div_VisitCondition.style.display='';	
	div_VisitInfo.style.display='none';

	if(manageCom.length==8 && manageCom.substring(4,8)!='0000')
	{

		fm.save_con.disabled = true;
		fm.dele_con.disabled = true;			
	}
	else
	{
		fm.save_visit.disabled = false;
		fm.dele_visit.disabled = false;		
	}	
}

//探访记录
function VisitInfo()
{	
	div_VisitCondition.style.display='none';	
	div_VisitInfo.style.display='';		
}

//探访条件查询
function queryConData()
{
	if(fm.MngCom.value==null ||fm.MngCom.value=="")
	{
		alert("请选择管理机构!");
		return;
	}
  	
	var strSql=" select ManageCom,Condition,Rate,FlowNo, makedate,maketime,MngCom "
			+" from HMVisitCondition where 1=1 and ManageCom like'"+fm.MngCom.value+"%'";

	strSql  = strSql + " order by FlowNo,makedate " ;
	
	turnPage.queryModal(strSql, HMVisitConditionGrid);
}

//探访条件保存
function saveConData()
{	
	if(HMVisitConditionGrid.checkValue()==false)return false;
	
	if(!isChecked(HMVisitConditionGrid))
	{
		alert("请选择探访条件记录记录！");
		return ;		
	}
	for(var rowNum=0;rowNum<HMVisitConditionGrid.mulLineCount;rowNum++)
	{
	    if(HMVisitConditionGrid.getChkNo(rowNum))
	    {
	      var MngCom=HMVisitConditionGrid.getRowColData(rowNum,1);
		  if(MngCom.length<8)
		  {
		    alert('医院归属必须到三级机构，请修改管理机构');
			return false;	
		  }		   
	     }
    }
	fm.fmtransact.value="DELETE&&INSERT||MAIN";
	fm.OperType.value = '01';
		
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action='./LHVisitManageSave.jsp';
	fm.submit(); //提交
}

//探访条件删除
function deleConData()
{
	if(HMVisitConditionGrid.checkValue()==false)return false;
	
	if(!isChecked(HMVisitConditionGrid))
	{
		alert("请选择操作记录！");
		return ;		
	}
	fm.fmtransact.value="DELETE||MAIN";
	
	fm.OperType.value = '01';
		
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action='./LHVisitManageSave.jsp';
	fm.submit(); //提交	

}



//探访记录查询
function queryVisitInfo()
{
  	var RiskcodeInfo="";
  	
	if( verifyInput2() == false ) return false;		
	
	var strSql= " select CustomerNo,Name,HospitName,VisitDate,Identifiction,Collection,Bed,Legitimacy,Support,FlowNo,makedate,maketime from HMVisitInfo where ManageCom like '"
	+fm.MngCom_Info.value+"%' and CustomerNo = '"+fm.CustomerNo.value+"' and Name = '"+fm.CustomerName.value+"'";	
	
	turnPage.queryModal(strSql, HMVisitInfoGrid);
}
//探访记录保存
function saveVisitInfo()
{
	if(HMVisitInfoGrid.checkValue()==false)return false;
		
	if(!isChecked(HMVisitInfoGrid))
	{
		alert("请选择操作记录！");
		return ;		
	}
		
	fm.fmtransact.value="DELETE&&INSERT||MAIN";
	
	fm.OperType.value = '02';
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action='./LHVisitManageSave.jsp';
	fm.submit(); //提交
}

//探访记录删除
function deleVisitInfo()
{
	if(HMVisitInfoGrid.checkValue()==false)return false;	
	
	if(!isChecked(HMVisitInfoGrid))
	{
		alert("请选择操作记录！");
		return ;		
	}
		
	fm.fmtransact.value="DELETE||MAIN";	
	fm.OperType.value = '02';
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action='./LHVisitManageSave.jsp';
	fm.submit(); //提交
}



//"+" 相应函数
function addCondition (parm1,parm2) 
{ 
	var count = HMVisitConditionGrid.mulLineCount;
	HMVisitConditionGrid.setRowColData(count-1,1,fm.MngCom.value)
}

//"+" 相应函数
function addVisitInfo(parm1,parm2) 
{ 
	var count = HMVisitInfoGrid.mulLineCount;
	HMVisitInfoGrid.setRowColData(count-1,1,fm.CustomerNo.value) ;
	HMVisitInfoGrid.setRowColData(count-1,2,fm.CustomerName.value);
}



function isChecked( ObjGrid )
{
	var flag = false;
	var count = ObjGrid.mulLineCount;
	for( var i = 0;i<count ;i++)
	{
		if(ObjGrid.getChkNo(i))
		{
			flag = true;
		}
	}	
	return flag;
}
		

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if(fm.OperType.value == '01')
	{
		HMVisitConditionGrid.clearData();
	}
	else
	{
		HMVisitInfoGrid.clearData();
	}
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

