<%
//程序名称：LHTotalInfoQueryInit.jsp
//程序功能：客户综合信息查询(初始化页面)
//创建日期：2006-05-20 16:03:30
//创建人  ：hm
//更新记录：  
//更新人  ： 郭丽颖 
//更新日期：2006-05-24 9:45:27
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%> 
<Script language=javascript>
function initForm()
{
	try
	{
	    initLHHealthServPlanGrid();
	    initLHContSpecialInfo();
	    initLHServerPlanType();
	    initLHPlanSpecialInfo();
	    initLHServerDoStatus();
	    initLHInHospitalMode();
	    initLHHealthCheckupStyle();
	    initServItemCaseGrid();
	    initLHMessSendMgGrid();
	    fm.all('ComID').value = manageCom.substring(0, 4);
	}
  	catch(re)
  	{
    	alert("LHTotalInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}  
////////////////////报单类型
function initLHHealthServPlanGrid() 
{
	var iArray = new Array();      
         
	try 
	{ 
   		iArray[0]=new Array();       
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名        
    	iArray[0][3]=0;         				//列名    
    	iArray[0][4]="";  
    	
    	iArray[1]=new Array();       
    	iArray[1][0]="保单号";         		//列名               
    	iArray[1][1]="80px";         		//列名            
    	iArray[1][3]=0;         				//列名    
    	iArray[1][4]="";                 
       
	    iArray[2]=new Array(); 
		iArray[2][0]="保险期间(起始)";   
		iArray[2][1]="85px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
	  
  
	    iArray[3]=new Array(); 
		iArray[3][0]="保险期间(终止)";   
		iArray[3][1]="85px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="投保人客户号";   
		iArray[4][1]="80px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
		iArray[5]=new Array(); 
		iArray[5][0]="投保人";   
		iArray[5][1]="80px";   
		iArray[5][2]=20;        
		iArray[5][3]=0; 
		
		iArray[6]=new Array();                   
		iArray[6][0]="被保人客户号";   
		iArray[6][1]="80px";            
		iArray[6][2]=20;                 
		iArray[6][3]=0;   

		iArray[7]=new Array();                   
		iArray[7][0]="被保人";   
		iArray[7][1]="80px";            
		iArray[7][2]=20;                 
		iArray[7][3]=0;  
		
		iArray[8]=new Array();                   
		iArray[8][0]="被保人性别";   
		iArray[8][1]="40px";            
		iArray[8][2]=20;                 
		iArray[8][3]=0;  
		
		iArray[9]=new Array();                   
		iArray[9][0]="出生日期";   
		iArray[9][1]="80px";            
		iArray[9][2]=20;                 
		iArray[9][3]=0;  
		
		iArray[10]=new Array();                   
		iArray[10][0]="证件号";   
		iArray[10][1]="100px";            
		iArray[10][2]=20;                 
		iArray[10][3]=0;   
		
		iArray[11]=new Array();                   
		iArray[11][0]="保单类型";   
		iArray[11][1]="100px";            
		iArray[11][2]=20;                 
		iArray[11][3]=3;         
		     
    	LHHealthServPlanGrid = new MulLineEnter( "fm" , "LHHealthServPlanGrid" );
    	//这些属性必须在loadMulLine前                   
    	      
    	LHHealthServPlanGrid.mulLineCount = 0;                 
    	LHHealthServPlanGrid.displayTitle = 1;                 
    	LHHealthServPlanGrid.hiddenPlus = 1;                   
    	LHHealthServPlanGrid.hiddenSubtraction = 1;            
    	LHHealthServPlanGrid.canSel = 1;     	
    	LHHealthServPlanGrid.selBoxEventFuncName = "showOne";                    
    	LHHealthServPlanGrid.loadMulLine(iArray); 
	}
  	catch(ex) {alert(ex);}                        
}        
///////////////////////////报单详细信息
function initLHContSpecialInfo() 
{   
	var iArray = new Array();      
         
	try 
	{  
   		iArray[0]=new Array();       
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名        
    	iArray[0][3]=0;         				//列名    
    	iArray[0][4]=""; 
   		                        
    	iArray[1]=new Array();       
    	iArray[1][0]="产品代码/名称";         		//列名               
    	iArray[1][1]="272px";         		//列名
    	iArray[1][2]="20";               
    	iArray[1][3]=0;         				//列名                 
    	   
		iArray[2]=new Array(); 
		iArray[2][0]="保障金额";   
		iArray[2][1]="150px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
	  
  
	    iArray[3]=new Array(); 
		iArray[3][0]="投保人";   
		iArray[3][1]="150px";   
		iArray[3][2]=20;        
		iArray[3][3]=3;
		
		     
    	LHContSpecialInfo = new MulLineEnter( "fm" , "LHContSpecialInfo" );
    	//这些属性必须在loadMulLine前                   
    	      
    	LHContSpecialInfo.mulLineCount = 0;                 
    	LHContSpecialInfo.displayTitle = 1;                 
    	LHContSpecialInfo.hiddenPlus = 1;                   
    	LHContSpecialInfo.hiddenSubtraction = 1;            
    	LHContSpecialInfo.canSel = 1;                       
    	               
    	//LHHealthServPlanGrid.selBoxEventFuncName = "showOne";
    	         ;           
    	LHContSpecialInfo.loadMulLine(iArray);              
    	//这些操作必须在loadMulLine后面                 
    	////LHHealthServPlanGrid.setRowColData(1,1,"asdf");                     
               
	}                        
	catch(ex) { alert(ex);  }                        
}                  
      
function initLHServerPlanType() 
{   
	var iArray = new Array();      
	try 
	{            
   		iArray[0]=new Array();       
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名        
    	iArray[0][3]=0;         				//列名    
    	iArray[0][4]=""; 
   		             
    	iArray[1]=new Array();       
    	iArray[1][0]="服务计划名称";         		//列名               
    	iArray[1][1]="140px";         		//列名
    	iArray[1][2]="20";               
    	iArray[1][3]=0;         				//列名                 
       
	    iArray[2]=new Array(); 
		iArray[2][0]="档次";   
		iArray[2][1]="40px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
	  
	    iArray[3]=new Array(); 
		iArray[3][0]="保单号";   
		iArray[3][1]="70px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="保单机构";   
		iArray[4][1]="80px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
		iArray[5]=new Array(); 
		iArray[5][0]="计划起始时间";   
		iArray[5][1]="60px";   
		iArray[5][2]=20;        
		iArray[5][3]=0;
		
		iArray[6]=new Array(); 
		iArray[6][0]="计划终止时间";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3;
		
		iArray[7]=new Array(); 
		iArray[7][0]="健管保费";   
		iArray[7][1]="0px";   
		iArray[7][2]=20;        
		iArray[7][3]=3;
		
		iArray[8]=new Array(); 
		iArray[8][0]="服务计划代码";   
		iArray[8][1]="0px";   
		iArray[8][2]=20;        
		iArray[8][3]=3;
		
		iArray[9]=new Array(); 
		iArray[9][0]="客户号";   
		iArray[9][1]="65px";   
		iArray[9][2]=20;        
		iArray[9][3]=0;
		
		iArray[10]=new Array(); 
		iArray[10][0]="客户姓名";   
		iArray[10][1]="65px";   
		iArray[10][2]=20;        
		iArray[10][3]=0;
		    
    	LHServerPlanType = new MulLineEnter( "fm" , "LHServerPlanType" );
    	//这些属性必须在loadMulLine前                   

    	LHServerPlanType.mulLineCount = 0;                 
    	LHServerPlanType.displayTitle = 1;                 
    	LHServerPlanType.hiddenPlus = 1;                   
    	LHServerPlanType.hiddenSubtraction = 1;            
    	LHServerPlanType.canSel = 1;                       
    	               
    	LHServerPlanType.selBoxEventFuncName = "showTwo";
    	         ;           
    	LHServerPlanType.loadMulLine(iArray);              
    	//这些操作必须在loadMulLine后面                 
    	////LHHealthServPlanGrid.setRowColData(1,1,"asdf");                     
	}                        
	catch(ex) {alert(ex);}                        
}  

function initLHPlanSpecialInfo() 
 {   
   var iArray = new Array();      
         
   try 
   {  
   	 iArray[0]=new Array();       
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名        
     iArray[0][3]=0;         				//列名    
     iArray[0][4]=""; 
    
     iArray[1]=new Array();       
     iArray[1][0]="服务项目名称";         		//列名               
     iArray[1][1]="240px";         		//列名
     iArray[1][2]="20";               
     iArray[1][3]=0;         				//列名                 
       
	    iArray[2]=new Array(); 
			iArray[2][0]="服务项目序号";   
			iArray[2][1]="150px";   
			iArray[2][2]=20;        
			iArray[2][3]=0;
	  
  
	    iArray[3]=new Array(); 
			iArray[3][0]="服务关联状态";   
			iArray[3][1]="140px";   
			iArray[3][2]=20;        
			iArray[3][3]=0;
			
			iArray[4]=new Array(); 
			iArray[4][0]="ServItemNO";   
			iArray[4][1]="0px";   
			iArray[4][2]=20;        
			iArray[4][3]=3;
		
		     
    LHPlanSpecialInfo = new MulLineEnter( "fm" , "LHPlanSpecialInfo" );
    //这些属性必须在loadMulLine前                   
          
    LHPlanSpecialInfo.mulLineCount = 0;                 
    LHPlanSpecialInfo.displayTitle = 1;                 
    LHPlanSpecialInfo.hiddenPlus = 1;                   
    LHPlanSpecialInfo.hiddenSubtraction = 1;            
    LHPlanSpecialInfo.canSel = 1;                       
                   
    //LHHealthServPlanGrid.selBoxEventFuncName = "showOne";
             ;           
    LHPlanSpecialInfo.loadMulLine(iArray);              
    //这些操作必须在loadMulLine后面                 
    ////LHHealthServPlanGrid.setRowColData(1,1,"asdf");                     
               
  }                        
  catch(ex) {              
    alert(ex);             
  }                        
}   

function initLHServerDoStatus() 
{   
	var iArray = new Array();      
         
	try 
	{ 
   		iArray[0]=new Array();       
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名        
    	iArray[0][3]=0;         				//列名    
    	iArray[0][4]="";   
		
		iArray[1]=new Array();       
    	iArray[1][0]="服务项目代码";         		//列名               
    	iArray[1][1]="80px";         		//列名
    	iArray[1][2]="20";               
    	iArray[1][3]=0;         				//列名
    	
    	iArray[2]=new Array();       
    	iArray[2][0]="服务项目名称";         		//列名               
    	iArray[2][1]="150px";         		//列名
    	iArray[2][2]="20";               
    	iArray[2][3]=0;         				//列名                 
                
	    iArray[3]=new Array(); 
		iArray[3][0]="服务项目序号";   
		iArray[3][1]="80px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
	            
	    iArray[4]=new Array(); 
		iArray[4][0]="保单号";   
		iArray[4][1]="70px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		        
		iArray[5]=new Array(); 
		iArray[5][0]="服务状态";   
		iArray[5][1]="60px";   
		iArray[5][2]=20;        
		iArray[5][3]=0;
		        
		iArray[6]=new Array(); 
		iArray[6][0]="服务执行时间";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3;
		        
		iArray[7]=new Array(); 
		iArray[7][0]="servitemno";   
		iArray[7][1]="0px";   
		iArray[7][2]=20;        
		iArray[7][3]=3;
		        
		iArray[8]=new Array(); 
		iArray[8][0]="客户号";   
		iArray[8][1]="70px";   
		iArray[8][2]=20;        
		iArray[8][3]=0;
		
		iArray[9]=new Array(); 
		iArray[9][0]="客户姓名";   
		iArray[9][1]="60px";   
		iArray[9][2]=20;        
		iArray[9][3]=0;
		
		     
    	LHServerDoStatus = new MulLineEnter( "fm" , "LHServerDoStatus" );
    	//这些属性必须在loadMulLine前                   
    	      
    	LHServerDoStatus.mulLineCount = 0;                 
    	LHServerDoStatus.displayTitle = 1;                 
    	LHServerDoStatus.hiddenPlus = 1;                   
    	LHServerDoStatus.hiddenSubtraction = 1;            
    	LHServerDoStatus.canSel = 1;                       
    	LHServerDoStatus.selBoxEventFuncName = "showThree";
    	LHServerDoStatus.loadMulLine(iArray);              
    	//这些操作必须在loadMulLine后面                 
    	////LHHealthServPlanGrid.setRowColData(1,1,"asdf");                     
	}                        
	catch(ex) 
	{ alert(ex);}                        
}

function initServItemCaseGrid()
{
	var iArray = new Array();      
	try 
	{ 
   		iArray[0]=new Array();       
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名        
    	iArray[0][3]=0;         				//列名    
    	iArray[0][4]="";   
		
		  iArray[1]=new Array();       
    	iArray[1][0]="服务事件编号";         		//列名               
    	iArray[1][1]="150px";         		//列名
    	iArray[1][2]="20";               
    	iArray[1][3]=0;         				//列名
    	
    	iArray[2]=new Array();       
    	iArray[2][0]="服务事件名称";         		//列名               
    	iArray[2][1]="320px";         		//列名
    	iArray[2][2]="20";               
    	iArray[2][3]=0;         				//列名                 
                
	    iArray[3]=new Array(); 
		  iArray[3][0]="服务事件状态";   
		  iArray[3][1]="110px";   
		  iArray[3][2]=20;        
		  iArray[3][3]=0;
		  
		  iArray[4]=new Array(); 
		  iArray[4][0]="ServcaseState";   
		  iArray[4][1]="0px";   
		  iArray[4][2]=20;        
		  iArray[4][3]=3;
	   
    	ServItemCaseGrid = new MulLineEnter( "fm" , "ServItemCaseGrid" );
    	//这些属性必须在loadMulLine前                   
    	      
    	ServItemCaseGrid.mulLineCount = 0;                 
    	ServItemCaseGrid.displayTitle = 1;                 
    	ServItemCaseGrid.hiddenPlus = 1;                   
    	ServItemCaseGrid.hiddenSubtraction = 1;            
    	ServItemCaseGrid.canSel = 1;                       
    	//ServItemCaseGrid.selBoxEventFuncName = "showThree";
    	ServItemCaseGrid.loadMulLine(iArray);              
    	//这些操作必须在loadMulLine后面                 
    	////LHHealthServPlanGrid.setRowColData(1,1,"asdf");                     
	}                        
	catch(ex) 
	{ alert(ex);} 
}

function initLHInHospitalMode() 
 {   
   var iArray = new Array();      
         
   try 
   {
   	 iArray[0]=new Array();       
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名        
     iArray[0][3]=0;         				//列名    
     iArray[0][4]="";   
      
     iArray[1]=new Array();       
     iArray[1][0]="险种代码";         		//列名               
     iArray[1][1]="60px";         		//列名
     iArray[1][2]="20";               
     iArray[1][3]=0;    
       
	 iArray[2]=new Array(); 
	 iArray[2][0]="就诊时间";   
	 iArray[2][1]="80px";   
	 iArray[2][2]=20;        
	 iArray[2][3]=0;
	  
  
	 iArray[3]=new Array(); 
	 iArray[3][0]="就诊医院";   
	 iArray[3][1]="100px";   
	 iArray[3][2]=20;        
	 iArray[3][3]=0;
			
			
	 iArray[4]=new Array(); 
	 iArray[4][0]="就诊方式";   
	 iArray[4][1]="60px";   
	 iArray[4][2]=20;        
	 iArray[4][3]=0;
			
			
	 iArray[5]=new Array(); 
	 iArray[5][0]="主诉";   
	 iArray[5][1]="60px";   
 	 iArray[5][2]=20;        
	 iArray[5][3]=0;
			
	 iArray[6]=new Array(); 
	 iArray[6][0]="医疗总费用";   
	 iArray[6][1]="60px";   
	 iArray[6][2]=20;        
	 iArray[6][3]=0;
			
	 iArray[7]=new Array(); 
	 iArray[7][0]="治疗费";   
	 iArray[7][1]="60px";   
	 iArray[7][2]=20;        
	 iArray[7][3]=0;
			
	 iArray[8]=new Array(); 
	 iArray[8][0]="检查化验费";   
	 iArray[8][1]="60px";   
	 iArray[8][2]=20;        
	 iArray[8][3]=0;
	 
	 iArray[9]=new Array(); 
	 iArray[9][0]="中药费";   
	 iArray[9][1]="60px";   
	 iArray[9][2]=20;        
	 iArray[9][3]=0;
			
	 iArray[10]=new Array(); 
	 iArray[10][0]="西药费";   
	 iArray[10][1]="60px";   
	 iArray[10][2]=20;        
	 iArray[10][3]=0;
	 
	 iArray[11]=new Array(); 
	 iArray[11][0]="其他费用";   
	 iArray[11][1]="60px";   
	 iArray[11][2]=20;        
	 iArray[11][3]=0;
			
	 iArray[12]=new Array(); 
	 iArray[12][0]="给付金额";   
	 iArray[12][1]="60px";   
	 iArray[12][2]=20;        
	 iArray[12][3]=0;
	 
	 iArray[13]=new Array(); 
	 iArray[13][0]="案件号";   
	 iArray[13][1]="100px";   
	 iArray[13][2]=20;        
	 iArray[13][3]=0;
	 
	 iArray[14]=new Array(); 
	 iArray[14][0]="医院编码";   
	 iArray[14][1]="100px";   
	 iArray[14][2]=20;        
	 iArray[14][3]=3;
			
	 iArray[15]=new Array();       
     iArray[15][0]="客户号";         		//列名               
     iArray[15][1]="80px";         		//列名
     iArray[15][2]="20";               
     iArray[15][3]=3;         				//列名                 
       	     
    LHInHospitalMode = new MulLineEnter( "fm" , "LHInHospitalMode" );
    //这些属性必须在loadMulLine前                   
          
    LHInHospitalMode.mulLineCount = 0;                 
    LHInHospitalMode.displayTitle = 1;                 
    LHInHospitalMode.hiddenPlus = 1;                   
    LHInHospitalMode.hiddenSubtraction = 1;            
    LHInHospitalMode.canSel = 1;                       
                   
    //LHHealthServPlanGrid.selBoxEventFuncName = "showOne";
             ;           
    LHInHospitalMode.loadMulLine(iArray);              
    //这些操作必须在loadMulLine后面                 
    ////LHHealthServPlanGrid.setRowColData(1,1,"asdf");                     
               
  }                        
  catch(ex) {              
    alert(ex);             
  }                        
}   

function initLHHealthCheckupStyle() 
 {   
   var iArray = new Array();      
         
   try 
   { 
   	 iArray[0]=new Array();       
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名        
     iArray[0][3]=0;         				//列名    
     iArray[0][4]="";  
     
     iArray[1]=new Array();       
     iArray[1][0]="体检时间";         		//列名               
     iArray[1][1]="100px";         		//列名
     iArray[1][2]="20";               
     iArray[1][3]=0;         				//列名                 
       
	    iArray[2]=new Array(); 
			iArray[2][0]="体检机构名称";   
			iArray[2][1]="150px";   
			iArray[2][2]=20;        
			iArray[2][3]=0;
	  
  
	    iArray[3]=new Array(); 
			iArray[3][0]="体检类别";   
			iArray[3][1]="120px";   
			iArray[3][2]=20;        
			iArray[3][3]=0;
			
		  iArray[4]=new Array(); 
			iArray[4][0]="体检序号";   
			iArray[4][1]="0px";   
			iArray[4][2]=20;        
			iArray[4][3]=30;
			
			iArray[5]=new Array(); 
			iArray[5][0]="客户号";   
			iArray[5][1]="100px";   
			iArray[5][2]=20;        
			iArray[5][3]=0;
			
			iArray[6]=new Array(); 
			iArray[6][0]="客户姓名 ";   
			iArray[6][1]="100px";   
			iArray[6][2]=20;        
			iArray[6][3]=0;
		
		     
    LHHealthCheckupStyle = new MulLineEnter( "fm" , "LHHealthCheckupStyle" );
    //这些属性必须在loadMulLine前                   
          
    LHHealthCheckupStyle.mulLineCount = 0;                 
    LHHealthCheckupStyle.displayTitle = 1;                 
    LHHealthCheckupStyle.hiddenPlus = 1;                   
    LHHealthCheckupStyle.hiddenSubtraction = 1;            
    LHHealthCheckupStyle.canSel = 1;                       
                   
    LHHealthCheckupStyle.selBoxEventFuncName = "showFive";          
    LHHealthCheckupStyle.loadMulLine(iArray);              
    //这些操作必须在loadMulLine后面                 
    ////LHHealthServPlanGrid.setRowColData(1,1,"asdf");  
                     
               
  }                        
  catch(ex) {              
    alert(ex);             
  }                        
}   

  function initLHCustomGymGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	
      iArray[0][1]="25px";  	
      iArray[0][2]=10;     
      iArray[0][3]=2;
      
      iArray[1]=new Array();
      iArray[1][0]="健身项目代码";
      iArray[1][1]="0px";
      iArray[1][2]=10;
      iArray[1][3]=0; 
      iArray[1][9]="健身项目代码|notnull"; 
      
      iArray[2]=new Array();
      iArray[2][0]="健身项目名称";  	
      iArray[2][1]="75px";  	
      iArray[2][2]=10;      
      iArray[2][3]=2;  
      iArray[2][4]="lhgymitem";
      iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
      iArray[2][6]="0|1";     //上面的列中放置引用代码中第几位值
      iArray[2][9]="健身项目名称|notnull&len<=120";
      iArray[2][15]="codename";
      iArray[2][17]="2";  		//Mulline中进行模糊查询时，查询输入框中内容的列数
      iArray[2][19]="1" ;
 
      iArray[3]=new Array();
      iArray[3][0]="每次健身时间（小时）";  
      iArray[3][1]="75px";  	  
      iArray[3][2]=10;        
      iArray[3][3]=1;     
      iArray[3][9]="每次健身时间（小时）|INT&len<=2";     
      
      iArray[4]=new Array();
      iArray[4][0]="健身频率（次/月）";  
      iArray[4][1]="75px";  	 
      iArray[4][2]=10;
      iArray[4][3]=1; 
      iArray[4][9]="健身频率（次/月）|INT&len<=3";         
  
      iArray[5]=new Array();
      iArray[5][0]="CustomerGymNo";  
      iArray[5][1]="0px";  	 
      iArray[5][2]=10;
      iArray[5][3]=3; 

     
      LHCustomGymGrid= new MulLineEnter( "fm" , "LHCustomGymGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            LHCustomGymGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            LHCustomGymGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //LHCustomGymGrid.canSel =1; 
           
         
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      LHCustomGymGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }         
    
    function initLHCustomFamilyDiseasGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;     	  //列最大值
      iArray[0][3]=0;  			  //1表示允许该列输入，0表示只读且不响应Tab键
                     					// 2 表示为容许输入且颜色加深. 	
      iArray[1]=new Array();
      iArray[1][0]="亲属关系名称";  	//列名（序号列，第1列）
      iArray[1][1]="75px";  					//列宽
      iArray[1][2]=10;      					//列最大值
      iArray[1][3]=2;
      iArray[1][4]="FamilyCode";
      iArray[1][5]="1|2";     				//引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    					//上面的列中放置引用代码中第几位值
      iArray[1][9]="亲属关系名称|len<=120";
      
      
      
      
      iArray[2]=new Array();
      iArray[2][0]="亲属关系代码";  	
      iArray[2][1]="0px";  	
      iArray[2][2]=10;      
      iArray[2][3]=3; 
     
      
      iArray[3]=new Array();
      iArray[3][0]="疾病代码";  	
      iArray[3][1]="75px";  	
      iArray[3][2]=10;      
      iArray[3][3]=0;  
  
      iArray[4]=new Array();
      iArray[4][0]="疾病名称";  				//列名（第2列）
      iArray[4][1]="75px";  	  				//列宽
      iArray[4][2]=10;        					//列最大值
         																//后续可以添加N列，如上设置
      iArray[4][3]=2;          					//是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="lhdisease";
      iArray[4][5]="4|3";     					//引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";    						//上面的列中放置引用代码中第几位值
      iArray[4][9]="疾病名称|len<=120";
      iArray[4][15]="ICDName";
      iArray[4][17]="4";  
      iArray[4][19]="1" ;
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";  	
      iArray[5][1]="0px";  	
      iArray[5][2]=10;      
      iArray[5][3]=3;  
     
      
    
      LHCustomFamilyDiseasGrid= new MulLineEnter( "fm" , "LHCustomFamilyDiseasGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            LHCustomFamilyDiseasGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            LHCustomFamilyDiseasGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
				    LHCustomFamilyDiseasGrid.hiddenPlus =1;
				    LHCustomFamilyDiseasGrid.hiddenSubtraction = 1;
				    //LHCustomFamilyDiseasGrid.canSel = 1;
				    LHCustomFamilyDiseasGrid.canChk = 0;         
           // LHCustomFamilyDiseasGrid.canSel =1; 
           //对象初始化区：调用对象初始化方法，属性必须在此前设置
       
      LHCustomFamilyDiseasGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }     
function initLHMessSendMgGrid() 
{                               
	var iArray = new Array();
    
	try
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="30px";         		//列名
    	iArray[0][3]=0;
    	 
    	iArray[1]=new Array();
    	iArray[1][0]="客户号";         		//列名
    	iArray[1][1]="80px";         		//列名
    	iArray[1][2]=20;        
    	iArray[1][3]=0;         		//列名
    	
    	iArray[2]=new Array(); 
		  iArray[2][0]="客户姓名";   
		  iArray[2][1]="70px";   
		  iArray[2][2]=20;        
		  iArray[2][3]=0;
		
		  iArray[3]=new Array(); 
		  iArray[3][0]="保单号";   
		  iArray[3][1]="90px";   
		  iArray[3][2]=20;        
		  iArray[3][3]=0;
		
		  iArray[4]=new Array(); 
		  iArray[4][0]="任务实施时间";   
		  iArray[4][1]="100px";   
		  iArray[4][2]=20;        
		  iArray[4][3]=0;
	    
	    iArray[5]=new Array(); 
	    iArray[5][0]="健康通讯代码";   
	    iArray[5][1]="0px";   
	    iArray[5][2]=20;        
	    iArray[5][3]=3;
	    
	    iArray[6]=new Array(); 
	    iArray[6][0]="通讯/短信名称";   
	    iArray[6][1]="160px";   
	    iArray[6][2]=20;        
	    iArray[6][3]=0;
	    
	    iArray[7]=new Array(); 
	    iArray[7][0]="通讯/短信内容";   
	    iArray[7][1]="220px";   
	    iArray[7][2]=20;        
	    iArray[7][3]=1;
	    
	    iArray[8]=new Array(); 
	    iArray[8][0]="TaskExecNo";   
	    iArray[8][1]="0px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=3;
	 
	  
		  LHMessSendMgGrid = new MulLineEnter( "fm" , "LHMessSendMgGrid" ); 
    	//这些属性必须在loadMulLine前
    	LHMessSendMgGrid.mulLineCount = 0;   
    	LHMessSendMgGrid.displayTitle = 1;
    	LHMessSendMgGrid.hiddenPlus = 1;
    	LHMessSendMgGrid.hiddenSubtraction = 1;
    	LHMessSendMgGrid.canSel = 0;
    	LHMessSendMgGrid.canChk = 0;
    	//LHMessSendMgGrid.chkBoxEventFuncName = "showOne";  
    	LHMessSendMgGrid.loadMulLine(iArray);  
    	//这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{ alert(ex);}
} 
</script>
  