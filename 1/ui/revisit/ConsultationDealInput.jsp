<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：ConsultationDealInput.jsp
 //程序功能：咨诉任务处理
 //创建日期：2009-8-6
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="ConsultationDeal.js"></SCRIPT>
  <%@include file="ConsultationDealInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./ConsultationDealSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >咨诉处理</td>
  </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >受理信息</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">来电号码</td>
    <td class="input">
    	<input class="readonly" name="TelephoneNo" readonly >
    </td>      
    <td class="title">是否后续处理</td>
    <td class="input">
    	<input class="readonly" name="NeedDealFlag" readonly >
    </td>
    <td class="title"></td>
    <td class="input">
    </td>  
  </tr>
<tr class="common">
    <td class="title">客户姓名</td>
    <td class="input">
    	<input class="readonly" name="CustomerName" readonly >
    </td>      
    <td class="title">问题件类型</td>
    <td class="input">
    	<input class="readonly" name="IssueType" readonly >
    </td>
    <td class="title"></td>
    <td class="input">
    </td>    
  </tr>
<tr class="common">
    <td class="title">咨诉类型</td>
    <td class="input">
    	<input class="readonly" name="ComplaintType" readonly >
    </td>      
    <td class="title">转交机构</td>
    <td class="input">
    	<input class="readonly" name="TransmitCom" readonly >
    </td>
    <td class="title"></td>
    <td class="input">
    </td>    
  </tr>
<tr class="common">
    <td class="title">保单合同号</td>
    <td class="input">
    	<input class="readonly" name="OtherNo" readonly >
    </td>      
    <td class="title">承办时间</td>
    <td class="input">
    	<input class="readonly" name="UndertakeDate" readonly >
    </td> 
    <td class="title"></td>
    <td class="input">
    </td>   
  </tr>
<tr class="common">
    <td class="title">客户类型</td>
    <td class="input">
    	<input class="readonly" name="CustomerNo" readonly >
    </td>      
    <td class="title">客户联系方式</td>
    <td class="input">
    	<input class="readonly" name="CustomerAddress" readonly >
    </td> 
    <td class="title"></td>
    <td class="input">
    </td>   
  </tr>
<tr class="common">
    <td class="title">来电机构</td>
    <td class="input">
    	<input class="readonly" name="TelephoneCom" readonly >
    </td>      
    <td class="title">预约时间</td>
    <td class="input">
    	<input class="readonly" name="BookingDate" readonly >
    </td> 
    <td class="title"></td>
    <td class="input">
    </td>   
  </tr>
  <tr class="common">
    <td class="title">特急案件标识</td>
    <td class="input">
    	<input class="readonly" name="UrgencySign">
    </td>      
    <td class="title"></td>    
    <td class="title"></td>    
    <td class="title"></td>
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >咨诉内容</td>
  </tr>
</table>
<textarea class="common" name="ComplaintContent" cols="100%" rows="2" value="1000字" ></textarea><BR>
<table>
  <tr>
    <td class="titleImg" >处理信息</td>
  </tr>
</table>
<textarea class="common" name="DealContent" cols="100%" rows="2" value="1000字" ></textarea><BR>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">预约日期</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="BookingDate1" >
    </td>
    <td class="title">预约时间(HH:MM)</td>  
    <td class="input">
    	<input class="common" name="BookingTime" >
    </td>
    <td class="title"></td>
    <td class="input">
    </td>     
  </tr>
<tr class="common">
    <td class="title">咨诉问题件状态</td>
    <td class="input">
    	<input class="codeno" name="IssueState" verify="咨诉问题件状态|notnull" ondblclick="return showCodeList('issuestate2',[this,IssueStateName],[0,1]);" onkeyup="return showCodeListKey('issuestate2',[this,IssueStateName],[0,1]);" ><input class="codename" name="IssueStateName" readonly>
    </td>      
  </tr>
</TABLE>
<input type="hidden" name="ComplaintID"  />
<input value="确  定" type=button  onclick="submitForm()" class="cssButton" type="button" name="update"    >
<input value="返回" type=button  onclick="GoToReturn()" class="cssButton" >
<br><br>


</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
