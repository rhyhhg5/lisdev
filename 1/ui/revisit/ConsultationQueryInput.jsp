<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>

<%
 //程序名称：ConsultationQueryInput.jsp
 //程序功能：咨诉记录查询
 //创建日期：2009-8-6
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
 <SCRIPT src="ConsultationQuery.js"></SCRIPT>
 <%@include file="ConsultationQueryInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form  method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >咨诉记录查询</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">来电电话</td>
    <td class="input">
    	<input class="common" name="TelephoneNo" >
    </td>      
    <td class="title">来电起期</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="MakeDate" >
    </td>    
    <td class="title">来电止期</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="MakeDate1" >
    </td> 
    <tr>
    <tr>  
    <td class="title">来电日期</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="MakeDate2" >
    </td> 
    <td class="title">客户姓名</td>
    <td class="input">
    	<input class="common" name="CustomerName" >
    </td>      
    <td class="title">保单合同号</td>
    <td class="input">
    	<input class="common" name="OtherNo" >
    </td>  
  </tr>
</TABLE>
<input value="查  询" type=button  onclick="queryclick()" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >记录清单</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanConsultationQueryGrid" >
     </span> 
      </td>
   </tr>
</table>
<div id="divTurnPageConsultationQueryGrid" align="center" style="display:'none'">
                <input type="button" class="cssButton" value="首  页" onclick="turnPageConsultationQueryGrid.firstPage()">
                <input type="button" class="cssButton" value="上一页" onclick="turnPageConsultationQueryGrid.previousPage()">
                <input type="button" class="cssButton" value="下一页" onclick="turnPageConsultationQueryGrid.nextPage()">
                <input type="button" class="cssButton" value="尾  页" onclick="turnPageConsultationQueryGrid.lastPage()">
</div>
<br><br>

<input type="button" class="cssButton" value=" 导  出 " onclick="turnPageConsultationQueryGrid.makeExcel('1|@^4|0.00_ ^4|@');">
<br><br>


</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
