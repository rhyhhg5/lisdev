//该文件中包含客户端需要处理的函数和事件

//程序名称：RevisitApp.js
//程序功能：回访任务申请
//创建日期：2009-7-29
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;

var TurnPageRevisitGrid = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage = new turnPageClass();


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    queryclick();
    queryclick2();
    initRevisitNum();
    //执行下一步操作
  }
}


function queryclick(){
	var strSql ="select a.answerid,"
		                          +"(select codename from ldcode where code=a.AnswerType and codetype='answertype'),"
							      +"a.AppntNo,"
							      +"a.AppntName,"
							      +"a.OtherNo,"
							      +"a.ShouldVisitDate,"
							      +"a.Bookingdate,"
							      +"(select codename from ldcode where code=a.AnswerStatus and codetype='answerstatus'),"
							      +"a.makedate,a.AnswerStatus,"
							      +"(select codename from ldcode where codetype='revisitime' and code=(select AnswerRemark from LCAppnt where contno=a.OtherNo and AppntNo=a.AppntNo)) as revisitime"
					 +" from LIAnswerInfo a"
					 +" where 1 = 1 "
					      +" and (a.operator is null or a.operator ='' )"
						  +" and a.AnswerStatus in('0','4')"
	    				+ getWherePart( 'a.SaleChnl','SaleChnlDetail' )
	    				+ getWherePart( 'a.RiskCode','RiskCode' )
	    				//+	getWherePart( 'a.AgentCode' ,'AgentCode' )
	    				//+ getWherePart( 'a.ShouldVisitDate','ShouldVisitDate' )
	          	+ getWherePart( 'a.OtherNo','ContNo' )
	    				+	getWherePart( 'a.AnswerStatus','ResvisitState' )
	    				+ getWherePart( 'a.AppntName','CustomName' )
	    			    + getWherePart('a.ManageCom','ManageCom','like')
	    				+" and a.managecom like '"+comcode+"%'";
	    if(fm.all("ShouldVisitDateBegin").value!=null&&fm.all('ShouldVisitDateBegin').value!="")
	    {
	    	strSql = strSql +" and ShouldVisitDate >='"+fm.all("ShouldVisitDateBegin").value+"' " ;
	    }
	    if(fm.all("ShouldVisitDateEnd").value!=null&&fm.all('ShouldVisitDateEnd').value!="")
	    {
	    	strSql = strSql +" and ShouldVisitDate <='"+fm.all("ShouldVisitDateEnd").value+"' " ;
	    }			
	   strSql = strSql+ " order by a.ShouldVisitDate,a.AppntNo,a.managecom,a.BookingDate,a.Bookingtime,revisitime " ; 		
	    
	TurnPageRevisitGrid.pageDivName = "divTurnPageRevisitGrid";    			   		
	//TurnPageRevisitGrid.queryModal(strSql, RevisitGrid,1,1);
	
	TurnPageRevisitGrid.queryModal(strSql, RevisitGrid);
}
function queryclick2(){
	var strSql =" select a.answerid,"
			                       +"(select codename from ldcode where code=a.AnswerType and codetype='answertype'),"
							       +"a.AppntNo,"
							       +"a.AppntName,"
							       +"a.OtherNo,"
							       +"a.ShouldVisitDate,"
							       +"a.Bookingdate,"
		                           +"(select codename from ldcode where code=a.AnswerStatus and codetype='answerstatus'),"
							       +"a.managecom,a.AnswerStatus, "
							       +"(select codename from ldcode where codetype='revisitime' and code=(select AnswerRemark from LCAppnt where contno=a.OtherNo and AppntNo=a.AppntNo)) as revisitime"
						 +" from LIAnswerInfo a"
						 +" where 1 = 1"
						    +" and a.AnswerStatus in ('1','4')"
						    +" and a.operator='"+operator+"' "
	    					+ getWherePart( 'a.SaleChnl','SaleChnlDetail' )
	    					+ getWherePart( 'a.RiskCode','RiskCode' )
	    					//+	getWherePart( 'a.AgentCode' ,'AgentCode' )
	    					//+ getWherePart( 'a.ShouldVisitDate','ShouldVisitDate' )
	          		+ getWherePart( 'a.OtherNo','ContNo' )
	    					+	getWherePart( 'a.AnswerStatus','ResvisitState' )
	    					+ getWherePart( 'a.AppntName','CustomName' )
	    					+" and a.managecom like '"+comcode+"%'"
	    			 ;   	
	    			
	     if(fm.all("ShouldVisitDateBegin").value!=null&&fm.all('ShouldVisitDateBegin').value!="")
	    {
	    	strSql = strSql +" and ShouldVisitDate >='"+fm.all("ShouldVisitDateBegin").value+"' " ;
	    }
	    if(fm.all("ShouldVisitDateEnd").value!=null&&fm.all('ShouldVisitDateEnd').value!="")
	    {
	    	strSql = strSql +" and ShouldVisitDate <='"+fm.all("ShouldVisitDateEnd").value+"' " ;
	    }		
	    
	      strSql = strSql+ " order by a.ShouldVisitDate,a.AppntNo,a.managecom,a.BookingDate,a.Bookingtime,revisitime " ; 
	  

	turnPage2.queryModal(strSql, RevisittwoGrid,1,1);
}
function queryupdate(){   
	var i = 0;
  var checkFlag = 0;
  var cAnswerID;
  var cAppntNo;
  var cMakeDate;
  for (i=0; i<RevisitGrid.mulLineCount; i++) {
    if (RevisitGrid.getSelNo(i)) { 
       checkFlag = RevisitGrid.getSelNo();
       break;
    }
  } 
  if (checkFlag) { 
    cAnswerID = RevisitGrid.getRowColData(checkFlag - 1, 1); 
    if(cAnswerID==""){
    	alert("没有选中需要申请的回访!");
    	return false;
    }
    cAppntNo=	RevisitGrid.getRowColData(checkFlag - 1, 3);
     
    cMakeDate=RevisitGrid.getRowColData(checkFlag - 1, 9);
    
    cAnswerStatus=RevisitGrid.getRowColData(checkFlag - 1, 10);
    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.action="./RevisitAppSave.jsp?AnswerID="+cAnswerID+"&AppntNo="+cAppntNo+"&MakeDate="+cMakeDate+"&AnswerStatus="+cAnswerStatus;
	  fm.submit();
	}else{
		alert("没有选中需要申请的回访!")
	}
}

function getRevisitDetail(){
	 var i = 0;
   var checkFlag = 0;
   var cAnswerID;
   var cAppntNo;
   for (i=0; i<RevisittwoGrid.mulLineCount; i++) {
     if (RevisittwoGrid.getSelNo(i)) { 
       checkFlag = RevisittwoGrid.getSelNo();
       break;
     }
   }
  if (checkFlag) { 
     cAnswerID = RevisittwoGrid.getRowColData(checkFlag - 1, 1); 
     if(cAnswerID==""){
    	 alert("没有选中需要申请的回访!");
    	 return false;
     }
    cAppntNo=	RevisittwoGrid.getRowColData(checkFlag - 1, 3);
    cAnswerType= RevisittwoGrid.getRowColData(checkFlag - 1, 2);
    cContNo= RevisittwoGrid.getRowColData(checkFlag - 1, 5);
    cManageCom= RevisittwoGrid.getRowColData(checkFlag - 1, 9);
    cShouldVisitDate= RevisittwoGrid.getRowColData(checkFlag - 1, 6);
    cAnswerStatus=RevisittwoGrid.getRowColData(checkFlag - 1, 10);
    
    var Sql_cont ="select * from LIAnswerInfo  a where a.AppntNo = '"+cAppntNo+"' and a.OtherNo <> '"+cContNo+"'  and ShouldVisitDate >= Add_Months (date '"+currentDate+"', -3 ) "+
    " and exists (select p1.riskcode  from lcpol p1 , lcpol p2 where  p1.contno = a.otherno  and  p2.contno ='"+cContNo+"'  and  p1.mainpolno = p1.polno and p2.mainpolno = p2.polno and p1.riskcode = p2.riskcode ) " ;
    
    var arrResult=easyExecSql(Sql_cont,1,0);
    if(null!=arrResult)
    {
       alert(" 该客户有相同险种保单回访记录 ,处于回访状态，请关注！");
    }
    
    
    var sFeatures = "";
	  window.open("./RevisitDetInputToMain.jsp?AppntNo="+cAppntNo+"&AnswerID="+cAnswerID+"&AnswerType="+cAnswerType+"&ContNo="+cContNo+"&ManageCom="+cManageCom+"&Operator="+operator+"&ShouldVisitDate="+cShouldVisitDate+"&AnswerStatus="+cAnswerStatus,"",sFeatures);
	}else{
		alert("没有选中要处理的回访!")
	}
	return true;
}

function returnBack()
{

  var i = 0;
  var checkFlag = 0;
   
   
   for (i=0; i<RevisittwoGrid.mulLineCount; i++) {
     if (RevisittwoGrid.getSelNo(i)) { 
       checkFlag = RevisittwoGrid.getSelNo();
       break;
     }
   }
   
  if (checkFlag) 
  { 
       
	    var cAnswerID = RevisittwoGrid.getRowColData(checkFlag - 1, 1); 
	     if(null==cAnswerID||cAnswerID==""){
	    	 alert("没有选中需要申请的回访!");
	    	 return false;
	     }
		   
		    
		     var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		  fm.action="./RevisitAppSave.jsp?AnswerID="+cAnswerID+"&OperFlag=returnBack";
		  fm.submit();
  }else
  {
  	alert(" 没有选中要处理的任务! ")
  
  }

   
}

