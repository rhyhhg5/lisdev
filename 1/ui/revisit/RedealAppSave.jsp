<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK"%>

<%
	//程序名称：RedealAppSave.jsp
	//程序功能：转办件任务申请
	//创建日期：2009-7-31
	//创建人  ：liwb
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.revisit.*"%>

<%
	//接收信息，并作校验处理。
	String tMissionID = request.getParameter("MissionID");
	System.out.println("MissionID:" + tMissionID);
	//输入参数
	RedealAppUI tRedealAppUI = new RedealAppUI();
	//输出参数
	CErrors tError = null;               
	String FlagStr = "";
	String Content = "";
	String transact = "";

	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	String tOperator = tGI.Operator;
	System.out.println("Operator:" + tOperator);
	transact = "INSERT||MAIN";

	TransferData mTransferData = new TransferData();
	mTransferData.setNameAndValue("MissionID", tMissionID);
	try {
		//准备传输数据VData
		VData tVData = new VData();
		tVData.add(tGI);
		tVData.add(mTransferData);
		tRedealAppUI.submitData(tVData, transact);
	} 
	catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	System.out.println("FlagStr:" + FlagStr);
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "") {
		tError = tRedealAppUI.mErrors;
		if (!tError.needDealError()) {
			mTransferData = (TransferData) tRedealAppUI.getResult().getObjectByObjectName("TransferData", 0);
			String tDefaultOperator = (String) mTransferData.getValueByName("DefaultOperator");
			System.out.println("DefaultOperator:" + tDefaultOperator);
			if (tDefaultOperator != null && !"".equals(tDefaultOperator) && tOperator.equals(tDefaultOperator)) {
				Content = " 申请成功! ";
			} 
			else if (tDefaultOperator != null	&& !"".equals(tDefaultOperator)	&& !"tOperator".equals(tDefaultOperator)) {
				Content = " 申请失败，因为您访问的对象下有任务被" + tDefaultOperator + "锁定！";
			}
		  else {
				Content = " 申请成功! ";
			}
			FlagStr = "Success";
		} 
		else {
			Content = " 申请失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
	System.out.println("Content:" + Content);
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

