<%
  //程序名称：RedealAppInit.jsp
  //程序功能：转办件任务申请
 	//创建日期：2009-7-31
 	//创建人  ：liwb
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String theCurrentDate = PubFun.getCurrentDate();
%>
<script language="JavaScript">
var currentDate = '<%=theCurrentDate%>';

function initForm(){
	try{
		initInputBox();
		initAllTaskGrid();
		initTaskGrid();
		TaskQuery();
	}
	catch(re){
		alert("RedealAppInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initAllTaskGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访ID";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="回访类型";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="客户号";
		iArray[3][1]="45px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="客户姓名";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="保单合同号";
		iArray[5][1]="75px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="转办类型";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="转办状态";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="转办时间";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="特急件标识";
		iArray[9][1]="75px";
		iArray[9][2]=100;
		iArray[9][3]=0;
		
		iArray[10]=new Array();
		iArray[10][0]="MissionID";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=0;
		
	    iArray[11]=new Array();
		iArray[11][0]="回访时段";
		iArray[11][1]="70px";
		iArray[11][2]=100;
		iArray[11][3]=0;
		
		AllTaskGrid = new MulLineEnter( "fm" , "AllTaskGrid" ); 

		AllTaskGrid.mulLineCount=1;
		AllTaskGrid.displayTitle=1;
		AllTaskGrid.canSel=1;
		AllTaskGrid.canChk=0;
		AllTaskGrid.hiddenPlus=1;
		AllTaskGrid.hiddenSubtraction=1;

		AllTaskGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
function initTaskGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访ID";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="回访类型";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="客户号";
		iArray[3][1]="45px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="客户姓名";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="保单合同号";
		iArray[5][1]="75px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="转办类型";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="转办状态";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="转办时间";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="特急件标识";
		iArray[9][1]="75px";
		iArray[9][2]=100;
		iArray[9][3]=0;

		iArray[10]=new Array();
		iArray[10][0]="MissionID";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=0;
		
		iArray[11]=new Array();
		iArray[11][0]="AnswerStatus";
		iArray[11][1]="0px";
		iArray[11][2]=100;
		iArray[11][3]=0;
		
	    iArray[12]=new Array();
		iArray[12][0]="回访时段";
		iArray[12][1]="70px";
		iArray[12][2]=100;
		iArray[12][3]=0;

		TaskGrid = new MulLineEnter( "fm" , "TaskGrid" ); 

		TaskGrid.mulLineCount=1;
		TaskGrid.displayTitle=1;
		TaskGrid.canSel=1;
		TaskGrid.canChk=0;
		TaskGrid.hiddenPlus=1;
		TaskGrid.hiddenSubtraction=1;
		TaskGrid.selBoxEventFuncName ="intoRedealDet"; 
		
		TaskGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
</script>
