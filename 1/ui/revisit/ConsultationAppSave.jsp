<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：RevisitAppSave.jsp
//程序功能：回访任务申请
//创建日期：2009-7-29
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.revisit.*"%>

<%
 
 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 if (tG == null) {
	 	System.out.println("页面失效,请重新登陆");
	 	FlagStr = "Fail";
	 	Content = "页面失效,请重新登陆";
 }
 else { 
	  String operator=tG.Operator;
	  String ComplaintID=request.getParameter("ComplaintID");
	  String theCurrentDate = PubFun.getCurrentDate();
	  String theCurrentTime = PubFun.getCurrentTime();
	
	  ConsultationAppUI mConsultationAppUI= new ConsultationAppUI();
		VData tVData = new VData();
		String sql="update LIComplaintsInfo set IssueState='01',dealoperator='"+operator+"', operator='"+operator+"',ModifyDate='"+theCurrentDate+"',ModifyTime='"+theCurrentTime+"'where ComplaintID='"+ComplaintID+"' and IssueState='00' ";
		tVData.add(tG);
	  tVData.add(sql);
    try{
			mConsultationAppUI.submitData(tVData,"UPDATE");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (FlagStr==""){
			tError = mConsultationAppUI.mErrors;
			if (!tError.needDealError()){                          
				 Content = " 保存成功! ";
				 FlagStr = "Success";
			}
		  else{
				 Content = " 保存失败，原因是:" + tError.getFirstError();
				 FlagStr = "Fail";
			}
		} 
}
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
   parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

