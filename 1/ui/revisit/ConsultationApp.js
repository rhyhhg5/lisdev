//该文件中包含客户端需要处理的函数和事件

//程序名称：ConsultationApp.js
//程序功能：咨诉任务申请
//创建日期：2009-8-6
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    queryclick();
    queryclick2();
  }
}

function queryclick(){
    var comcode1 = fm.TransmitCom.value;
	var strSql ="select  a.ComplaintID,"
								   +"(select codename from ldcode where codetype='complaintissuetype' and code=a.IssueType),"
							       +"a.CustomerName,"
							       +"a.OtherNo,"
					               +"(select codename from ldcode where codetype='issuestate' and code=a.IssueState),"
							       +"a.TransmitDate,"
							       +"a.BookingDate,"
							       +"a.UndertakeDate,UrgencySign"
					  +" from LIComplaintsInfo a"
						+" where 1 = 1"
						  +" and a.IssueState = '00' "
	    				+" and a.TransmitCom like '"+comcode+"%' "
	    				+ getWherePart( 'a.IssueType','IssueType' )
	    				+ getWherePart( 'a.CustomerName','CustomerName' )
	    				+	getWherePart( 'a.OtherNo' ,'OtherNo' )
	    				+ getWherePart( 'a.TransmitDate','TransmitDate' )
	          	+ getWherePart( 'a.BookingDate','BookingDate' )
	if(comcode1!=null && comcode1!="")
	{
	   strSql+=" and a.TransmitCom like '"+comcode1+"%' ";
	}
	   strSql += " order by a.UndertakeDate desc";   		

	turnPage.queryModal(strSql, ComplaintGrid,1,1);
}

function queryupdate(){   
	var i = 0;
  var checkFlag = 0;
  var cComplaintID;
  for (i=0; i<ComplaintGrid.mulLineCount; i++) {
    if (ComplaintGrid.getSelNo(i)) { 
       checkFlag = ComplaintGrid.getSelNo();
       break;
    }
  }
  if (checkFlag) { 
    cComplaintID = ComplaintGrid.getRowColData(checkFlag - 1, 1); 
    if(cComplaintID==""){
    	alert("没有选中需要申请的咨诉任务!");
    	return false;
    }
    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.action="./ConsultationAppSave.jsp?ComplaintID="+cComplaintID;
	  fm.submit();
	}
	else{
		alert("没有选中需要申请的咨诉任务!")
	}
}

function queryclick2(){
		var strSql ="select a.ComplaintID,"
							       +"(select codename from ldcode where codetype='complaintissuetype' and code=a.IssueType),"
							       +"a.CustomerName,"
							       +"a.OtherNo,"
							       +"(select codename from ldcode where codetype='issuestate' and code=a.IssueState),"
							       +"a.TransmitDate,"
							       +"a.BookingDate,"
							       +"a.UndertakeDate,UrgencySign "
							+" from LIComplaintsInfo a"
						  +" where 1 = 1"
						    +" and a.IssueState = '01' "
						    +" and a.dealoperator = '"+operator+"' "
						    +" and a.TransmitCom like '"+comcode+"%%' "
	    				  + getWherePart( 'a.IssueType','IssueType' )
	    					+ getWherePart( 'a.CustomerName','CustomerName' )
	    					+	getWherePart( 'a.OtherNo' ,'OtherNo' )
	    					+ getWherePart( 'a.TransmitDate','TransmitDate' )
	          		+ getWherePart( 'a.BookingDate','BookingDate' )
	    					+ " order by a.UndertakeDate desc";   		
	turnPage2.queryModal(strSql, ComplainttwoGrid,1,1);
}

function getComplaintDetail(){
	var i = 0;
  var checkFlag = 0;
  var cComplaintID;
  for (i=0; i<ComplainttwoGrid.mulLineCount; i++) {
    if (ComplainttwoGrid.getSelNo(i)) { 
       checkFlag = ComplainttwoGrid.getSelNo();
       break;
    }
  }
  if (checkFlag) { 
    cComplaintID = ComplainttwoGrid.getRowColData(checkFlag - 1, 1); 
    if(cComplaintID==""){
    	alert("没有选中需要申请的咨诉!");
    	return false;
    }
    var sFeatures = "";
    var viewFlag =1;
	  window.open("./ConsultationDealInputToMain.jsp?ComplaintID="+cComplaintID+"&viewFlag=" + viewFlag,"",sFeatures);
	}
	else{
		alert("没有选中要处理的咨诉!")
	}
	return true;
}

