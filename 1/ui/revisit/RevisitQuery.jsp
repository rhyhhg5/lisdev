<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <title>回访查询</title>
    <!-- 公共引用样式 -->
    <link href="../common/css/Project.css" type="text/css" rel="stylesheet">
    <link href="../common/css/mulLine.css" type="text/css" rel="stylesheet">
    <!-- 公共引用脚本 -->
    <script language="JavaScript" src="../common/Calendar/Calendar.js"></script>
    <script language="JavaScript" src="../common/javascript/Common.js"></script>
    <script language="JavaScript" src="../common/cvar/CCodeOperate.js"></script>
    <script language="JavaScript" src="../common/javascript/MulLine.js"></script>
    <script language="JavaScript" src="../common/javascript/EasyQuery.js"></script>
    <script language="JavaScript" src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script language="JavaScript" src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script language="JavaScript" src="../common/javascript/VerifyInput.js"></script>
    <!-- 私有引用脚本 -->
    <script language="JavaScript" src="RevisitQuery.js"></script>
    <%@ include file="RevisitQueryInit.jsp" %>
    
</head>
<body onload="initForm();" >
    <form name="fm" method="post" target="fraSubmit">
      <table>
			  <tr>
			    <td class="titleImg" >回访查询</td>
			  </tr>
		  </table>
        <TABLE  class="common" >
						  <tr class="common">
						    <td CLASS="title">管理机构</td>
									<td CLASS="input" COLSPAN="1">
									<Input class="codeno" name=ManageCom ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);" ><input name=ManageComName class=codename readonly=true>
						    </td>
						    <td CLASS="title">投保单类型</td>
			            <td CLASS="input" COLSPAN="1">
					           <Input class=codeno name=SaleChnlDetail   CodeData="0|^01|个人单^05|银代单^07|电销单" ondblclick="return showCodeListEx('SaleChnlDetail',[this,SaleChnlDetailName],[0,1],null,null,null,1);" verify="投保单类型|code:SaleChnlDetail" onkeyup="return showCodeListKeyEx('SaleChnlDetail',[this,SaleChnlDetailName],[0,1]);"><input class=codename  name=SaleChnlDetailName readonly=true  >
    		        </td>				 
						    <td CLASS="title">业务员代码</td>
									<td CLASS="input" COLSPAN="1">
									<Input class="code8" name=AgentCode ondblclick="return showCodeList('AgentCode',[this], [0, 2]);" onkeyup="return showCodeListKey('AgentCode', [this], [0, 2]);">
						    </td>
						    <td class="title">险种</td>
						    <td class="input">
						    	<input class="codeno" name="RiskCode" ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);" ><input class="codename" name="RiskCodeName"readonly=true >
						    </td>  
						  </tr>
						<tr class="common">
							  <td class="title">回访状态</td>
						    <td class="input">
						    	<input class="codeno" name="ResvisitState" ondblclick="return showCodeList('AnswerStatus',[this,ResvisitStateName],[0,1]);" onkeyup="return showCodeListKey('AnswerStatus',[this,ResvisitStateName],[0,1]);" nextcasing=><input class="codename" name="ResvisitStateName" >
						    </td>
						    <td class="title">客户姓名</td>
						    <td class="input">
						    	<input class="common" name="CustomName" >
						    </td>
						    <td class="title">保险合同号</td>
						    <td class="input">
						    	<input class="common" name="ContNo" >
						    </td> 
						    <td class="title">回访人员</td>
						    <td class="input">
						    	<input class="common" name="AnswerPerson" >
						    </td>
						 </tr>
						 <tr class="common">
						    <td class="title">应访起始日期</td>
						    <td class="input">
						    	<input class="coolDatePicker" dateFormat="short" name="ShouldVisitStartDate" >
						    </td>
						    <td class="title">应访截止日期</td>
						    <td class="input">
						    	<input class="coolDatePicker" dateFormat="short" name="ShouldVisitEndDate" >
						    </td>
						    <td class="title">回访日期</td>
						    <td class="input">
						    	<input class="coolDatePicker" dateFormat="short" name="AnswerDate" >
						    </td>
						    <td class="title">回访类型</td>
						    <td class="input">
						    	<input class="codeno" name="AnswerType" ondblclick="return showCodeList('answertype',[this,AnswerTypeName],[0,1]);" onkeyup="return showCodeListKey('answertype',[this,AnswerTypeName],[0,1]);" ><input class="codename" name="AnswerTypeName"readonly=true >
						    </td>
						  </tr>
						</TABLE>
            <input type="button" class="cssButton" value=" 查  询 " onclick="queryRevisitGrid()">
           
            <input type="button" class="cssButton" value=" 重  置 " onclick="resetForm()">

        <table>
            <tr>
                <td class="titleImg">记录池</td>
            </tr>
        </table>
            <table class="common">
                <tr class="common">
                    <td><span id="spanRevisitGrid"></span></td>
                </tr>
            </table>
            <!-- 交易信息结果翻页 -->
            <div id="divTurnPageRevisitGrid" align="center" style="display:'none'">
                <input type="button" class="cssButton" value="首  页" onclick="turnPageRevisitGrid.firstPage()">
                <input type="button" class="cssButton" value="上一页" onclick="turnPageRevisitGrid.previousPage()">
                <input type="button" class="cssButton" value="下一页" onclick="turnPageRevisitGrid.nextPage()">
                <input type="button" class="cssButton" value="尾  页" onclick="turnPageRevisitGrid.lastPage()">
            </div>
            <!-- 提交数据操作按钮 -->
            <br>
        <br>
         <input type="button" class="cssButton" value=" 导  出 " onclick="turnPageRevisitGrid.makeExcel('1|@^4|0.00_ ^4|@');">
         
    </form>
    <!-- 通用下拉信息列表 -->
    <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>
