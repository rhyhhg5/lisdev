<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK"%>

<%
	//程序名称：RedealDetSave.jsp
	//程序功能：转办任务处理
	//创建日期：2009-8-4
	//创建人  ：liwb
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.revisit.*"%>

<%
	//接收信息，并作校验处理。
	String tMissionID = request.getParameter("MissionID");
	System.out.println("MissionID:" + tMissionID);
	//输入参数
	LIAnswerInfoSchema tLIAnswerInfoSchema = new LIAnswerInfoSchema();
	LCAppntSchema mLCAppntSchema = new LCAppntSchema();
	LIIssuePolSet tLIIssuePolSet = new LIIssuePolSet();	
	RedealDetUI tRedealDetUI = new RedealDetUI();

	//输出参数
	CErrors tError = null;                
	String FlagStr = "";
	String Content = "";
	String transact = "";

	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	transact = "UPDATE||MAIN";

	 mLCAppntSchema.setContNo(request.getParameter("ContNo"));
	 mLCAppntSchema.setAppntNo(request.getParameter("AppntNo"));
	 mLCAppntSchema.setAnswerPhoneType(request.getParameter("RevisitPhoneType"));
	 mLCAppntSchema.setAnswerPhone(request.getParameter("RevisitPhone"));
	//从url中取出参数付给相应的schema
	tLIAnswerInfoSchema.setAnswerID(request.getParameter("AnswerID"));
	tLIAnswerInfoSchema.setAnswerStatus(request.getParameter("ResvisitState"));
	tLIAnswerInfoSchema.setRemark(request.getParameter("Remark"));
	tLIAnswerInfoSchema.setTakeMode(request.getParameter("TakeMode"));
	tLIAnswerInfoSchema.setAnswerExplain(request.getParameter("AnswerExplain"));
	tLIAnswerInfoSchema.setCloseReason(request.getParameter("CloseReason"));
	if("4".equals(request.getParameter("ResvisitState")))
	{
		tLIAnswerInfoSchema.setBookingDate(request.getParameter("BookingDate"));
		tLIAnswerInfoSchema.setBookingTime(request.getParameter("BookingTime"));
	}else
	{
		tLIAnswerInfoSchema.setBookingDate(request.getParameter(""));
		tLIAnswerInfoSchema.setBookingTime(request.getParameter(""));
	}
	
	System.out.println("AnswerID:" + request.getParameter("AnswerID"));
	System.out.println("AnswerStatus:" + request.getParameter("AnswerStatus"));//ResvisitState
	System.out.println("ResvisitState:" + request.getParameter("ResvisitState"));
	System.out.println("Remark:" + request.getParameter("Remark"));
	System.out.println("TakeMode:" + request.getParameter("TakeMode"));
	System.out.println("AnswerExplain:" + request.getParameter("AnswerExplain"));
	
	if("2".equals(request.getParameter("AnswerStatus"))) {
	    System.out.println("处理问题件信息。。。");
		  String tAnswerNum[]   = request.getParameterValues("AnswerGridNo");
	    String tIssueType[]  = request.getParameterValues("AnswerGrid1");
	    String tIssueStutas[]     = request.getParameterValues("AnswerGrid3");
	    String tIssueExplain[] = request.getParameterValues("AnswerGrid5");
	    int AnswerCount = 0;
	    if (tAnswerNum != null) {
	    	AnswerCount = tAnswerNum.length;
	    }
	    for(int i = 0; i < AnswerCount; i++) {
	    	LIIssuePolSchema tLIIssuePolSchema = new LIIssuePolSchema();	    	
	    	tLIIssuePolSchema.setAnswerID(request.getParameter("AnswerID"));
	    	tLIIssuePolSchema.setIssueType(tIssueType[i]);
	    	tLIIssuePolSchema.setIssueStutas(tIssueStutas[i]);
	    	tLIIssuePolSchema.setIssueExplain(tIssueExplain[i]);
	    	
	    	tLIIssuePolSet.add(tLIIssuePolSchema);
	    }
	    System.out.println("。。。处理问题件信息");
	}
	//传输非schema数据
	TransferData mTransferData = new TransferData();
	mTransferData.setNameAndValue("MissionID", tMissionID);
	mTransferData.setNameAndValue("AnswerStatus", request.getParameter("AnswerStatus"));
	try {
		//准备传输数据VData
		VData tVData = new VData();
		//传输schema
		tVData.add(mTransferData);
		tVData.addElement(tLIAnswerInfoSchema);
		tVData.addElement(tLIIssuePolSet);

		tVData.add(tGI);
		tVData.add(mLCAppntSchema);
		tRedealDetUI.submitData(tVData, transact);
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "") {
		tError = tRedealDetUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Success";
		} else {
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

	//添加各种预处理
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

