<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：RevisitAppSave.jsp
//程序功能：回访任务申请
//创建日期：2009-7-29
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.revisit.*"%>
<%
 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 if (tG == null) {
	 	System.out.println("页面失效,请重新登陆");
	 	FlagStr = "Fail";
	 	Content = "页面失效,请重新登陆";
 }
 else { 
	    String tOperFlag ="";
	    tOperFlag = request.getParameter("OperFlag");
		String operator=tG.Operator;
		String AppntNo=request.getParameter("AppntNo");
		String MakeDate=request.getParameter("MakeDate");
		String cAnswerStatus=request.getParameter("AnswerStatus");
		String cAnswerID=request.getParameter("AnswerID");
        LIAnswerInfoSchema tLIAnswerInfoSchema = new LIAnswerInfoSchema();
		tLIAnswerInfoSchema.setAnswerStatus(cAnswerStatus);
		tLIAnswerInfoSchema.setAnswerID(cAnswerID);
		RevisitAppUI mRevisitAppUI= new RevisitAppUI();
		VData tVData = new VData();
		String sql="update LIAnswerInfo set AnswerStatus='1', operator='"+operator+"'where AnswerID='"+cAnswerID+"'and AnswerStatus in('0','4') ";
		if(null!=tOperFlag&&tOperFlag.equals("returnBack"))
		{
			sql = " update LIAnswerInfo set AnswerStatus='0', operator = null where AnswerID='"+cAnswerID+"' " ;
		}
		TransferData tTransferData = new TransferData();
		 tTransferData.setNameAndValue("OperFlag",tOperFlag);
		 tVData.add(tG);
		 tVData.add(sql);
		 tVData.addElement(tTransferData);
		 tVData.add(tLIAnswerInfoSchema);
	  
    try{
			mRevisitAppUI.submitData(tVData,"UPDATE");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (FlagStr==""){
			tError = mRevisitAppUI.mErrors;
			if (!tError.needDealError()){                          
				 Content = " 保存成功! ";
				 FlagStr = "Success";
			}
		  else{
				 Content = " 保存失败，原因是:" + tError.getFirstError();
				 FlagStr = "Fail";
			}
		}
 }
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

