<%
  //程序名称：RevisitAppInit.jsp
  //程序功能：回访任务申请
  //创建日期：2009-7-29
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%
  String theCurrentDate = PubFun.getCurrentDate();
%>
<script language="JavaScript">

var mdate="<%=theCurrentDate%>";
function initForm(){
	try{
		initRevisitGrid();
		initRevisittwoGrid();
		queryclick2();
		initRevisitNum();	   
	}
	catch(re){
		alert("RevisitAppInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initRevisitNum(){
	var  strSQL1=" select count(*) "
								+" from lianswerinfo a "
								+" where   a.answerstatus != '0' "
										+" and a.answerstatus != '1' "
										+" and a.modifydate = '"+mdate+"' and a.managecom like '"+comcode+"%'";
	var edom1=easyExecSql(strSQL1);
	var  strSQL2=" select count(*) from lianswerinfo a where a.answerstatus ='0' and a.managecom like '"+comcode+"%'";
	var edom2=easyExecSql(strSQL2);
	var  strSQL3=" select count(*) from lianswerinfo a where a.answerstatus ='4' and a.managecom like '"+comcode+"%'";
	var edom3=easyExecSql(strSQL3);
	fm.all("RevisitNum").value = edom1;
	fm.all("RevisitNum2").value = edom2;
	fm.all("BookingNum").value = edom3;
}
function initRevisitGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访号";
		iArray[1][1]="80px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="回访类型";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="客户号";
		iArray[3][1]="55px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="客户姓名";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="保单合同号";
		iArray[5][1]="75px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="应访日期";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="预约时间";
		iArray[7][1]="70px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="回访状态";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="makedate";
		iArray[9][1]="60px";
		iArray[9][2]=100;
		iArray[9][3]=3;

	    iArray[10]=new Array();
		iArray[10][0]="回访状态";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=3;
		
	    iArray[11]=new Array();
		iArray[11][0]="回访时段";
		iArray[11][1]="70px";
		iArray[11][2]=100;
		iArray[11][3]=0;
		
		RevisitGrid = new MulLineEnter( "fm" , "RevisitGrid" ); 

		RevisitGrid.mulLineCount=1;
		RevisitGrid.displayTitle=1;
		RevisitGrid.canSel=1;
		RevisitGrid.canChk=0;
		RevisitGrid.hiddenPlus=1;
		RevisitGrid.hiddenSubtraction=1;

		RevisitGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
function initRevisittwoGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访号";
		iArray[1][1]="80px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="回访类型";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="客户号";
		iArray[3][1]="55px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="客户姓名";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="保单合同号";
		iArray[5][1]="75px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="应访日期";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="预约时间";
		iArray[7][1]="70px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="回访状态";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="管理机构";
		iArray[9][1]="0px";
		iArray[9][2]=100;
		iArray[9][3]=2;
		
		iArray[10]=new Array();
		iArray[10][0]="回访状态";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=2;
		
	    iArray[11]=new Array();
		iArray[11][0]="回访时段";
		iArray[11][1]="70px";
		iArray[11][2]=100;
		iArray[11][3]=0;

		RevisittwoGrid = new MulLineEnter( "fm" , "RevisittwoGrid" ); 

		RevisittwoGrid.mulLineCount=1;
		RevisittwoGrid.displayTitle=1;
		RevisittwoGrid.canSel=1;
		RevisittwoGrid.canChk=0;
		RevisittwoGrid.hiddenPlus=1;
		RevisittwoGrid.hiddenSubtraction=1;
		//RevisittwoGrid.selBoxEventFuncName ="getRevisitDetail";

		RevisittwoGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
</script>
