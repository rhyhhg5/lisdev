//该文件中包含客户端需要处理的函数和事件

//程序名称：ConsultationAcc.js
//程序功能：咨诉受理
//创建日期：2009-8-6
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
  }
}

function submitForm(){
	if( verifyInput2() == false ) return false;
	var tNeedDealFlag=fm.all("NeedDealFlag").value;
	var tIssueType=fm.all("IssueType").value;
	var tTransmitCom=fm.all("TransmitCom").value;
	var tCustomerAddress=fm.all("CustomerAddress").value;
	var tUndertakeDate=fm.all("UndertakeDate").value;
	var tUndertakeTime=fm.all("UndertakeTime").value;
	var tBookingDate=fm.all("BookingDate").value;
	var tBookingTime=fm.all("BookingTime").value;
	if(tNeedDealFlag=='01'){
		if(tIssueType==""){
			 alert("选择需要后续处理时，问题件类型必须选择！"); 
			 fm.IssueType.focus(); 
			 return false;
	  } 
	// if(tTransmitCom==""){ 
	//	 alert("选择需要后续处理时，转交机构必须填写！");
	//	 fm.TransmitCom.focus(); 
	//	 return false;
	// } 
	  if(tCustomerAddress==""){ 
			 alert("选择需要后续处理时，客户联系方式必须填写！"); 
			 fm.CustomerAddress.focus(); 
			 return false;
	  } 
	  if(tUndertakeDate==""){ 
			 alert("选择需要后续处理时，承保日期必须填写！"); 
			 fm.UndertakeDate.focus();
			 return false;
		}
		if(tUndertakeTime==""){
			 alert("选择需要后续处理时，承保时间必须填写！");
			 fm.UndertakeTime.focus();
			 return false;
		}
	}
	if(tUndertakeTime==""){
  }
  else{
	   if(checkV(tUndertakeTime)==false){
		   fm.UndertakeTime.focus(); 
		   return false;
		 }
  }
  if(tNeedDealFlag=='01'){
  if(tBookingDate==""){
   	if(!(tBookingTime=="")){
	   	 alert("预约日期和预约时间必须同时录入");
	   	 return false;	
   	}
  }
  else{
   	if(tBookingTime==""){
	   	 alert("预约日期和预约时间必须同时录入");
	   	 return false;	
   	}
  }
  if(tBookingTime==""){
  }
  else{
	   if(checkV(tBookingTime)==false){
		  	fm.BookingTime.focus(); 
		  	return false;
		 }
  }
  }else
  {
    if(tBookingDate !="" || tBookingTime!="")
    {  
       alert("如无需后续处理,请取消预约日期，时间栏填写！");
       return false;
    }
  }
	var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./ConsultationAccSave.jsp";
	fm.submit();
}
//时间格式校验
function checkV(temp_str){
	if(temp_str.length != 5){
	   alert("输入格式有误。");
	   return false;
	}
	var hour_s = temp_str.substr(0,2);
	var split1_s = temp_str.substr(2,1);
	var min_s = temp_str.substr(3,2);
	if(!(split1_s==":")){
	   alert("输入格式有误。");
	   return false;
	}
	if(!(hour_s>=0&&hour_s<24&&min_s>=0&&min_s<60)){
	   alert("输入时间有误。");
	   return false;
	}
}