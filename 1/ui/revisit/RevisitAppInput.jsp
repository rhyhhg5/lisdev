<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：RevisitAppInput.jsp
 //程序功能：回访任务申请
 //创建日期：2009-7-29
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
    String currentDate = PubFun.getCurrentDate() ;	
   
					
%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
  var currentDate ="<%=currentDate%>";
</script>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
 <SCRIPT src="RevisitApp.js"></SCRIPT>
 <%@include file="RevisitAppInit.jsp"%>
</head>
<body  onload="initForm();">
<form action="./RevisitAppSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >回访任务查询</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td CLASS="title">管理机构</td>
			<td CLASS="input" COLSPAN="1">
			<Input class="codeno" name=ManageCom ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);"><input name=ManageComName class=codename readonly=true>
    		</td>
    <td CLASS="title">投保单类型</td>
			<td CLASS="input" COLSPAN="1">
					<Input class=codeno name=SaleChnlDetail   CodeData="0|^01|个人单^05|银代单^07|电销单" ondblclick="return showCodeListEx('SaleChnlDetail',[this,SaleChnlDetailName],[0,1],null,null,null,1);" verify="投保单类型|code:SaleChnlDetail" onkeyup="return showCodeListKeyEx('SaleChnlDetail',[this,SaleChnlDetailName],[0,1]);"><input class=codename  name=SaleChnlDetailName readonly=true  >
    		</td>			
    <td class="title">险种</td>
    <td class="input">
    	<input class="codeno" name="RiskCode" ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);" ><input class="codename" name="RiskCodeName"readonly=true >
    </td>
    <!-- <td CLASS="title">业务员代码</td>
			<td CLASS="input" COLSPAN="1">
			<Input class="code8" name=AgentCode ondblclick="return showCodeList('AgentCode',[this], [0, 2]);" onkeyup="return showCodeListKey('AgentCode', [this], [0, 2]);">
    		</td>  -->
    <td class="title">回访状态</td>
    <td class="input">
    	<Input class=codeno name="ResvisitState"   CodeData="0|^0|待回访^4|预约" ondblclick="return showCodeListEx('AnswerStatus',[this,ResvisitStateName],[0,1],null,null,null,1);" verify="回访状态|code:ResvisitState" onkeyup="return showCodeListKeyEx('AnswerStatus',[this,ResvisitStateName],[0,1]);"><input class=codename  name="ResvisitStateName" readonly=true  >
    </td> 		
    	
  </tr>
<tr class="common">
    <td class="title">应访回访日期（起期）</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="ShouldVisitDateBegin" >
    </td>
     <td class="title">应访回访日期（止期）</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="ShouldVisitDateEnd" >
    </td>
    <td class="title">保险合同号</td>
    <td class="input">
    	<input class="common" name="ContNo" >
    </td> 
    <!--  <td class="title">回访状态</td>
    <td class="input">
    	<Input class=codeno name="ResvisitState"   CodeData="0|^0|待回访^4|预约" ondblclick="return showCodeListEx('AnswerStatus',[this,ResvisitStateName],[0,1],null,null,null,1);" verify="回访状态|code:ResvisitState" onkeyup="return showCodeListKeyEx('AnswerStatus',[this,ResvisitStateName],[0,1]);"><input class=codename  name="ResvisitStateName" readonly=true  >
    </td>-->
    <td class="title">客户姓名</td>
    <td class="input">
    	<input class="common" name="CustomName" >
    </td>  
  </tr>
</TABLE>
<input value="任务查询" type=button  onclick="queryclick()" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >回访任务公共池</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanRevisitGrid" >
     </span> 
      </td>
   </tr>
</table>
<div id="divTurnPageRevisitGrid" style="display:'none'" align="left" >
                <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="TurnPageRevisitGrid.firstPage();" />
                <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="TurnPageRevisitGrid.previousPage();" />
                <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="TurnPageRevisitGrid.nextPage();" />
                <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="TurnPageRevisitGrid.lastPage();" />
            </div>

<TABLE  class="common" >
  <tr class="common">
    <td class="title">今日总回访量</td>
    <td class="input">
    	<input class="readonly" name="RevisitNum" readonly >
    </td>      
    <td class="title">待回访量</td>
    <td class="input">
    	<input class="readonly" name="RevisitNum2" readonly >
    </td>      
    <td class="title">预约量</td>
    <td class="input">
    	<input class="readonly" name="BookingNum" readonly >
    </td>      
    <td class="title"></td>
  </tr>
</TABLE>
<input value="任务申请" type=button  onclick="queryupdate()" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >回访任务个人池</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanRevisittwoGrid" >
     </span> 
      </td>
   </tr>
</table>
<br>
<input value="回访处理" type=button  onclick="getRevisitDetail()" class="cssButton" type="button" >&nbsp;&nbsp;&nbsp;
<input value="返回申请" type=button  onclick="returnBack()" class="cssButton" type="button" >&nbsp;&nbsp;&nbsp;

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
