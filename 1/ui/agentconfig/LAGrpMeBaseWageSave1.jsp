<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2009-01-23
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LAGrpMeBaseWageUI tLAGrpMeBaseWageUI = new LAGrpMeBaseWageUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LAWageRadixAllSet tSetU = new LAWageRadixAllSet();	//用于更新 lawageradixall
      LAWageRadixAllSet tSetD = new LAWageRadixAllSet();	//用于删除
      LAWageRadixAllSet tSetI = new LAWageRadixAllSet();    //用于插入
            
      String tChk[] = request.getParameterValues("InpSetGridChk"); 
      
      String tManageCom[] = request.getParameterValues("SetGrid1");
      String tAgentGrade[] = request.getParameterValues("SetGrid3"); 
      String tBaseWage[] = request.getParameterValues("SetGrid5");
      String tIdx[] = request.getParameterValues("SetGrid6");
    
			//获取最大的ID号
//      ExeSQL tExe = new ExeSQL();
//      String tSql = "select int(max(idx)) from LAWageRadix5 order by 1 desc ";
//      String strIdx = "";
//      int tMaxIdx = 0;
//      strIdx = tExe.getOneValue(tSql);
//      if (strIdx == null || strIdx.trim().equals(""))
//      {
//         tMaxIdx = 0;
//     }
//     else
//      {
//          tMaxIdx = Integer.parseInt(strIdx);
          //System.out.println(tMaxIdx);
//      }
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{      		
      		//创建一个新的Schema lawageradixall
      		LAWageRadixAllSchema tSch = new LAWageRadixAllSchema();
			tSch.setManageCom(tManageCom[i]);
			tSch.setAgentGrade(tAgentGrade[i]);
			tSch.setRewardMoney(tBaseWage[i]);
			tSch.setWageCode("WP0001");
			tSch.setWageType("01");
			tSch.setWageName("基本收入");
			tSch.setWageNo("999999");
			tSch.setAgentType("1");
			tSch.setBranchType("2");
			tSch.setBranchType2("02");
      		if((tIdx[i] == null)||(tIdx[i].equals("")))
      		{
      			//需要插入记录
      			tSetI.add(tSch);
       		}
      		else
      		{
            //需要删除
            if(tAction.trim().equals("DELETE"))      			      	
      			{
      			tSch.setIdx(tIdx[i]);      			
      			tSetD.add(tSch);
      			}
            //需要更新
            else if(tAction.trim().equals("UPDATE"))      			      	
      			{
      			tSch.setIdx(tIdx[i]);      			
      			tSetU.add(tSch);
      			}
      		 
       		}
      	}      	
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if ((tSetD.size() == 0)&&(tSetU.size() == 0)&&(tSetI.size() == 0))
      {
        	FlagStr = "Fail";
        	Content = FlagStr + "未选中要处理的数据！";      	
      }
    else if (tSetI.size() != 0)
    	{
        	//只有插入数据
        	tVData.add(tSetI);
        	System.out.println("Start tLAGrpMeBaseWageUI Submit...INSERT");
        	tLAGrpMeBaseWageUI.submitData(tVData,"INSERT");        	    	
    	}
    else if (tSetD.size() != 0)
    	{
        	//只有更新数据
        	tVData.add(tSetD);
        	System.out.println("Start tLAGrpBaseWageUI Submit...DELETE");
        	tLAGrpMeBaseWageUI.submitData(tVData,"DELETE");        	    	
    	}
    else if (tSetU.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetU);
        	System.out.println("Start tLAGrpMeBaseWageUI Submit...UPDATE");
        	tLAGrpMeBaseWageUI.submitData(tVData,"UPDATE");        	    	
    	}
    	

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAGrpMeBaseWageUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
