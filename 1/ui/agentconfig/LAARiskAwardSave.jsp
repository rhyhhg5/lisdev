<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddBSubSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
  //输入参数
  LARewardPunishSet tLARewardPunishSet = new LARewardPunishSet();
  LAARiskAwardBL  tLAAriskAwardBL   = new LAARiskAwardBL();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");
  String mWageNo = request.getParameter("WageNo");
//准备传输数据 VData
 VData tVData = new VData();
 LARewardPunishSchema tLARewardPunishSchema = new LARewardPunishSchema();
  if(tOperate.equals("INSERT||MAIN")){
  String mManageCom  = request.getParameter("ManageCom");
  //LARewardPunishSchema
 tLARewardPunishSchema.setAgentCom(request.getParameter("AgentCom"));
 tLARewardPunishSchema.setManageCom(request.getParameter("ManageCom"));
 tLARewardPunishSchema.setWageNo(request.getParameter("WageNo")); 
     tVData.add(mManageCom);
  }else{
	  String hWageNo =  request.getParameter("HWageNo");
	  String hManageCom  = request.getParameter("HManagecom");
	  //LARewardPunishSchema
	  tLARewardPunishSchema.setAgentCom(request.getParameter("HAgentCom"));
	  tLARewardPunishSchema.setWageNo(request.getParameter("HWageNo"));
	  tLARewardPunishSchema.setManageCom(request.getParameter("HManagecom"));
	  tVData.add(hManageCom);
	  tVData.add(hWageNo);
  } 
   

    tLARewardPunishSchema.setAgentCode("0000000");
    tLARewardPunishSchema.setIdx(request.getParameter("Idx"));
    tLARewardPunishSchema.setBranchType(request.getParameter("BranchType"));
    tLARewardPunishSchema.setBranchType2(request.getParameter("BranchType2"));
    tLARewardPunishSchema.setAClass("C4");
    //-----------------------------------------------加 款记录
    //------------------------------
 //   tLARewardPunishSchema.setWageNo(request.getParameter("HWageNo"));
    tLARewardPunishSchema.setSendGrp(request.getParameter("SendGrp"));
    tLARewardPunishSchema.setNoti(request.getParameter("Noti"));
    tLARewardPunishSchema.setMoney(request.getParameter("Money"));
    tLARewardPunishSchema.setOperator(tG.Operator);
    tLARewardPunishSet.add(tLARewardPunishSchema);
 


  
  FlagStr="";
	tVData.add(tG);
	tVData.add(mWageNo);
	tVData.addElement(tLARewardPunishSet);
	
  try
  {
    tLAAriskAwardBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAriskAwardBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
