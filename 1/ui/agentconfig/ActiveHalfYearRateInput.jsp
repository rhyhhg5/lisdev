<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAAgentInput.jsp
//程序功能：个人代理增员管理
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<script>
var manageCom = <%=tG.ManageCom%>;
var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
msql=" 1";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="ActiveHalfYearRateInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="ActiveHalfYearRateInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./ActiveHalfYearRateSave.jsp" method=post name=fm target="fraSubmit">
		<table class="common" align=center>
			<tr align=right>
				<td class=button >
					&nbsp;&nbsp;
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="保  存" name=Save TYPE=button onclick="return submitForm();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="重  置" name=reset TYPE=button onclick="return initForm();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="修  改" name=Modify TYPE=button onclick="return updateClick();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="查  询 " name=reset TYPE=button onclick="return queryClick();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="标准查询"  name=Query TYPE=button onclick="return queryStandClick();">
				</td>
			</tr>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
				<td class=titleImg>
					  考核标准录入
				</td>
			</tr>
		</table>
	<Div  id= "divLAAgent1" style= "display: ''">
		<table class="common" align=center>
			<TR  class= common> 
				<TD  class= title>
					管理机构
				</TD>
				<TD  class= input>
					<Input class="codeno" name=ManageCom verify="管理机构|notnull"
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,'1',1);" 
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,'1',1);"
					><Input class=codename name=ManageComName readOnly elementtype=nacessary  > 
				</TD> 
				<TD  class= title>
					执行年
				</TD>
				<TD  class= input>
					<Input class="codeno" name=Year verify="执行年|notnull"
					ondblclick="return showCodeList('projectyear',[this,YearName],[0,1],null,null,null,1);" 
					onkeyup="return showCodeListKey('projectyear',[this,YearName],[0,1],null,null,null,1);"
					><Input class=codename name=YearName readOnly elementtype=nacessary  > 
				</TD> 
		</TR>
		<TR  class= common> 
			<TD  class= title>
				执行月
			</TD>
			<TD  class= input>
				<Input class="codeno" name=Month CodeData="0|^06|上半年^12|下半年" verify="执行月|notnull"
				ondblclick="return showCodeListEx('activemonth',[this,MonthName],[0,1]);" 
				onkeyup="return showCodeListKeyEx('activemonth',[this,MonthName],[0,1]);"
				><Input class=codename name=MonthName readOnly elementtype=nacessary > 
			</TD> 
		</TR>
	</table>
</Div>
	 <table>
	 	<tr>
	 		<td class=common>
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
	 				半年奖系数录入
 			</td>
	 	</tr>
	 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanActiveHalfYearRateGrid" >
 				</span>
 			<br></td>
 		</tr>
 	</table>
 </div>
 <font color="red">注：1.录入的系数只能为两位小数,如：0.02<br/>
 &nbsp;&nbsp;&nbsp;&nbsp;2.修改时只能修改职级对应的半年奖系数</font>
<Div  id= "divActiveHalfYearRateStand" style= "display: 'none'">
<hr/>
 	 <table>
	 	<tr>
	 		<td >
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessone);">
	 			<td class= titleImg>
		 				各个职级的半年奖系数
	 			</td>
	 	</tr>
	 </table>
	 
	 <Div  id= "divAssessone" style= "display: ''">
		 <table border="2" cellpadding="2" cellspacing="0" align="center">
			<tr class=common>
			  <th >职级</th>
			  <th >半年奖系数</th>
			</tr>
	 		<tr class= common align="center">
				<td>资深互动业务经理三级</td>
				<td>20%</td>
			</tr>
			<tr class= common align="center">
				<td>资深互动业务经理二级</td>
				<td>19%</td>
			</tr>
			<tr class= common align="center">
				<td>资深互动业务经理一级</td>
				<td>18%</td>
			</tr>
			<tr class= common align="center">
				<td>高级互动业务经理三级</td>
				<td>17%</td>
			</tr>
			<tr class= common align="center">
				<td>高级互动业务经理二级</td>
				<td>16%</td>
			</tr>
			<tr class= common align="center">
				<td>高级互动业务经理一级</td>
				<td>15%</td>
			</tr>
			<tr class= common align="center">
				<td>互动业务经理三级</td>
				<td>14%</td>
			</tr>
			<tr class= common align="center">
				<td>互动业务经理二级</td>
				<td>12%</td>
			</tr>
			<tr class= common align="center">
				<td>互动业务经理一级</td>
				<td>10%</td>
			</tr>
	 	</table>
 	</div>
 </Div>
 <Input type=hidden name=BranchType  value =<%=BranchType %>>   
 <Input type=hidden name=BranchType2 value =<%=BranchType2 %>>
 <Input type=hidden name=ManageCom2 value =<%=tG.ManageCom %>>     
 <Input type=hidden name=hideOperate >
 <Input type=hidden name=hideYear >  
 <Input type=hidden name=hideYearName >  
 <Input type=hidden name=hideMonth >
 <Input type=hidden name=hideMonthName >  
 <Input type=hidden name=hideManageCom >  
 <Input type=hidden name=hideManageComName >   
 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


