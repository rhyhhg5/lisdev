<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAAreaInput.jsp
//程序功能：地区类型录入界面
//创建时间：2003-10-16
//更新记录：更新人   更新时间   更新原因

%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LAAreaInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <SCRIPT src="LAAreaInput.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" >
 <form action="./LAAreaSave.jsp" method=post name=fm target="fraSubmit">
 <table>
 <tr>      
   	<td class=button width="10%" align=right>
   		<input type=button value="保  存" class=cssButton onclick="return DoSave();">  
  		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 
  		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
  	</td>    
  </tr>      
</table>
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 管理机构维护 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
     <td class=title>管理机构</td>
     <TD  class= input>
     <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len=4"  
     ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,1,'length(trim(comcode))=4 and char(1)',1);" 
     onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,1,'length(trim(comcode))=4 and char(1)',1);"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
     </TD> 
	   
   </tr>
   <tr>
	   <td class=title>地区类型</td>
	     <TD class= input>
	     <input name=AreaType class="codeno" verify="地区类型|notnull"  CodeData = "0|1^A|A类地区^B|B类地区^C|C类地区"
	    	 ondblclick="return showCodeListEx('AreaType1',[this,AreaTypeName],[0,1]);" 
	    	 onkeyup="return showCodeListKeyEx('AreaType1',[this,AreaTypeName],[0,1]);"><Input class=codename name=AreaTypeName readOnly elementtype=nacessary>
	     </TD> 
   </tr>
	     <input name=toperate type=hidden>
  </table>
   </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
    	</td>    
    </tr>      
  </table>
  <div id="divAreaGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanAreaGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
 
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




