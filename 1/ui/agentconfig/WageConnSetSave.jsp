<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：WageConnSetSave.jsp
//程序功能：
//创建日期：2003-9-26
//创建人  ：   zsj
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  String FlagStr = "";
  String Content = "";

  WageConnSet tWageConnSet = new WageConnSet();
  VData tVData = new VData();

  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  System.out.println("ManagCom : " + tGI.ManageCom);
  System.out.println("Operator : " + tGI.Operator);
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {     
      String tAction = request.getParameter("fmAction");
      System.out.println("--tAction--"+tAction);
      String areaType=request.getParameter("AreaType2");
      
      if ( tAction.trim().equals("del") )  {
      
       String tChk[] = request.getParameterValues("InpSetGridChk");
       String tAgentGrade[]=request.getParameterValues("SetGrid1");
       String tWageCode[]=request.getParameterValues("SetGrid2");
       String tPayperiodType[]=request.getParameterValues("SetGrid3");
       String tPayMonth[]=request.getParameterValues("SetGrid4");
       String tManageCom = request.getParameter("ManageCom");
       
       LALinkWageSet tLALinkWageSet=new LALinkWageSet();
       LALinkWageSchema tLALinkWageSchema=new LALinkWageSchema();
       
       int tR=0;
       for (int i=0;i<tChk.length;i++)
       {
       	if (tChk[i].equals("1") )
          {
           tLALinkWageSchema.setAgentGrade(tAgentGrade[i]);
           tLALinkWageSchema.setWageCode(tWageCode[i]);
           tLALinkWageSchema.setPayPeriodType(tPayperiodType[i]);
           tLALinkWageSchema.setPayMonth(tPayMonth[i]);
           tLALinkWageSchema.setManageCom(tManageCom);
           tLALinkWageSchema.setAreaType(areaType);
           tLALinkWageSet.add(tLALinkWageSchema);
           tLALinkWageSchema=new LALinkWageSchema();
           
            tR +=1;
          }
       }
    
       if (tR==0)
       {
        FlagStr = "Fail";
          Content = FlagStr + "请选中需要删除的记录";
       }
       else
       {
        VData tInputData = new VData();       
        
        tInputData.clear();
        tInputData.add(tGI);
        tInputData.add(1,tLALinkWageSet);
        if ( !tWageConnSet.deleteData(tInputData) ) {
          FlagStr = "Fail";
          Content = FlagStr + "删除失败，原因是：" + tWageConnSet.mErrors.getFirstError();
        }
        else  {
          FlagStr = "Succ";
          Content = FlagStr + "删除成功！";
        }
        }
      }
      else  
      {      
      LALinkWageSet    tLALinkWageSet = new LALinkWageSet();
      String tManageCom = "";
      String tOldManageCom = "";
      tLALinkWageSet.clear();
      int tCount = 0;
      int tIndex = 0;
      tVData.clear();
      tVData.add(tGI);
             
      tManageCom = request.getParameter("ManageCom1");
      tOldManageCom = request.getParameter("ManageCom");
      System.out.println("--tManageCom1--"+tManageCom);
            
      if ( tManageCom == null || tManageCom.trim().equals("")   )  {      
        tManageCom = request.getParameter("ManageCom");
        System.out.println("--tManageCom--"+tManageCom);
      }  
      
      
      String tWageCode[] = request.getParameterValues("SetGrid2");
      String tAgentGrade[] = request.getParameterValues("SetGrid1");
      String tPayPeriodType[] = request.getParameterValues("SetGrid3");
      String tPayMonth[] = request.getParameterValues("SetGrid4");
      String tPayMode[] = request.getParameterValues("SetGrid5");
      String tMonthLimit[] = request.getParameterValues("SetGrid6");
      String tPayInterval[] = request.getParameterValues("SetGrid7");      
      String tBaseRate[] = request.getParameterValues("SetGrid8");
      String tBaseRate1[] = request.getParameterValues("SetGrid9");
      String tStandMoney[] = request.getParameterValues("SetGrid10");
      String tStandRate[] = request.getParameterValues("SetGrid11");
      
      tCount = tWageCode.length;

      for ( tIndex=0;tIndex<tCount;tIndex++ )  {
      	LALinkWageSchema tLALinkWageSchema = new LALinkWageSchema();
        tLALinkWageSchema.setManageCom(tManageCom);
        tLALinkWageSchema.setWageCode(tWageCode[tIndex]);
        tLALinkWageSchema.setAgentGrade(tAgentGrade[tIndex]);
        tLALinkWageSchema.setPayPeriodType(tPayPeriodType[tIndex]);
        tLALinkWageSchema.setPayMonth(tPayMonth[tIndex]);
        tLALinkWageSchema.setPayMode(tPayMode[tIndex]);
        tLALinkWageSchema.setMonthLimit(tMonthLimit[tIndex]);
        tLALinkWageSchema.setPayInterval(tPayInterval[tIndex]);
        tLALinkWageSchema.setBaseRate(tBaseRate[tIndex]);
        tLALinkWageSchema.setBaseRate1(tBaseRate1[tIndex]);
        tLALinkWageSchema.setStandMoney(tStandMoney[tIndex]);
        tLALinkWageSchema.setStandRate(tStandRate[tIndex]);
        tLALinkWageSchema.setAreaType(areaType);
         

        tLALinkWageSet.add(tLALinkWageSchema);
      }
  
        tVData.add(tLALinkWageSet);
        
      if (!tWageConnSet.getInputData(tVData) ){
        FlagStr = "Fail";
        Content = FlagStr + "操作失败，原因是：" + tWageConnSet.mErrors.getFirstError();
      }
      else
      {
        if ( tAction.trim().equals("save"))  {
          if ( !tWageConnSet.saveData() ){
            FlagStr = "Fail";
            Content = FlagStr + "保存失败，原因是：" + tWageConnSet.mErrors.getFirstError();
          }
          else  {
            FlagStr = "Succ";
            Content = FlagStr + "保存成功！";
          }
        }
        else  {
          if ( !tWageConnSet.updateData(tOldManageCom) ){
            FlagStr = "Fail";
            Content = FlagStr + "更新数据失败，原因是：" + tWageConnSet.mErrors.getFirstError();
          }
          else  {
            FlagStr = "Succ";
            Content = FlagStr + "更新数据成功！";
          }
        }  
      }
    }  
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
  }// tGI!=null
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
