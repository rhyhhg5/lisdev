<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CopyDataOfFormalToTestingSave.jsp
//程序功能：把正式库数据导入到外测，用来测试外测一些大功能，像：薪资计算，考核等，需要大量的数据时
//创建日期：2017/4/28 15:26
//创建人  ：yangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
   <%@page import="com.sinosoft.lis.agentwages.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  CErrors tError = null;
//   String tOperate=request.getParameter("hideOperate");
//   tOperate=tOperate.trim();
  String tSql = request.getParameter("sql");
  String tTableName = request.getParameter("TableName");
  String tWhereSql = request.getParameter("WhereSql");
  String FlagStr = "Fail";
  String Content = "";
  String message="数据导入";
  CopyDataOfFormalToTestingBL tCopyDataOfFormalToTestingBL = new CopyDataOfFormalToTestingBL();
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("WhereSql", tWhereSql);
  tTransferData.setNameAndValue("TableName", tTableName);
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tTransferData);
  try
  {
    System.out.println("this will save the data!!!");	
    if(tCopyDataOfFormalToTestingBL.submitData(tVData,""))
    {
    	FlagStr = "Succ";
    }
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }
  if (!FlagStr.equals("Fail"))
  {
    tError = tCopyDataOfFormalToTestingBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = message+" 成功，共导入数据量： "+tCopyDataOfFormalToTestingBL.mCount;
    	FlagStr = "Succ";
    }
    else
    {
    	Content = message+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  else
  {
    tError = tCopyDataOfFormalToTestingBL.mErrors;
  	Content = message+" 失败，原因是:" + tError.getFirstError();
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
