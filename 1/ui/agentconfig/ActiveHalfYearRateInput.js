//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var tOrphanCode="";
var queryFlag = false;
window.onfocus=myonfocus;

function setActiveHalfYearRateValue()
{
	var sql ="select  gradecode,gradename,'','' from laagentgrade a where branchtype='5' and branchtype2='01' and GradeProperty2 is not null  and gradecode like 'U%' with ur ";
	turnPage.queryModal(sql, ActiveHalfYearRateGrid);
}
 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}



//提交，保存按钮对应操作
function submitForm()
{
	mOperate = "INSERT";
	if( verifyInput() == false ) return false;
	if(checkWage()) return false;
	if(checkExist()) return false;
    if(chkMulLine()== false)  return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	fm.submit();
}
function updateClick()
{
	mOperate = "UPDATE";
	if(queryFlag==false)     
	{
		alert("请先查询 ");
		return false;
	}
	if( verifyInput() == false ) return false;
	if(!check()) return false;
	if(chkMulLine()==false)  return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}
//标准查询
function queryStandClick()
{
	var divLAAgent2 = document.getElementById("divActiveHalfYearRateStand");
	showDiv(divLAAgent2,"true");
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function chkMulLine()
{
	var rowNum = ActiveHalfYearRateGrid.mulLineCount;
	var reg = new RegExp("^[0-9][\.][0-9]{2}?$");  
	for(var i=0;i<rowNum;i++)
	{
		
		var rate = ActiveHalfYearRateGrid.getRowColData(i,3);
		if(rate == null||rate == "")
		{
			ActiveHalfYearRateGrid.setRowColData(i,3,"0");
		}
		else
		{
			if(rate!=0&&!reg.test(rate))
			{
				alert(ActiveHalfYearRateGrid.getRowColData(i,2)+"的半年奖系数格式不正确,请重新录入");
				return false;
			}
			if(rate >1||rate <0)
			{
				alert(ActiveHalfYearRateGrid.getRowColData(i,2)+"的半年奖系数不在0到1之间");
				return false;
			}
		}
	}
	return true;
}
function queryClick()
{
	if( verifyInput() == false ) return false;
	var wageno = fm.all("Year").value+fm.all("Month").value
	var sql ="select agentgrade,(select gradename from laagentgrade where a.agentgrade = gradecode),RewardRate,idx from lawageradix5 a where branchtype ='5' and branchtype2='01' and wagetype='02'  " +
			"  and ManageCom ='"+fm.all("ManageCom").value+"'" +
			"  and wageno ='"+wageno+"' with ur";
	turnPage1.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage1.strQueryResult) {
		alert("没有符合条件的数据!");
		initForm();
		return false;
	}
	turnPage1.queryModal(sql, ActiveHalfYearRateGrid);
	fm.all("hideYear").value=fm.all("Year").value;
	fm.all("hideYearName").value=fm.all("YearName").value;
	fm.all("hideMonth").value=fm.all("Month").value;
	fm.all("hideMonthName").value=fm.all("MonthName").value;
	fm.all("hideManageCom").value=fm.all("ManageCom").value;
	fm.all("hideManageComName").value=fm.all("ManageComName").value;
	queryFlag = true;
}
function queryClick111()
{
	window.open("./ActiveHalfYearRateQuery.html");
}
function check()
{
	if(fm.all("hidemanagecom").value!=fm.all("ManageCom").value)
	{
		alert("修改时不能修改管理机构！");
		fm.all("ManageCom").value=fm.all("hideManageCom").value;
		fm.all("ManageComName").value=fm.all("hideManageComName").value;
		return false;
	}
	if(fm.all("hideYear").value!=fm.all("Year").value)
	{
		alert("修改时不能修改执行年！");
		fm.all("Year").value=fm.all("hideYear").value;
		fm.all("YearName").value=fm.all("hideYearName").value;
		return false;
	}
	if(fm.all("hideMonth").value!=fm.all("Month").value)
	{
		alert("修改时不能修改执行月！");
		fm.all("Month").value=fm.all("hideMonth").value;
		fm.all("MonthName").value=fm.all("hideMonthName").value;
		return false;
	}

	return true;
}
function checkExist()
{
	var wageno = fm.all("Year").value+fm.all("Month").value;
	var month=fm.all("Month").value;
	var sql ="select agentgrade from lawageradix5 a where branchtype ='5' and branchtype2='01' and wagetype='02'  " +
	"  and ManageCom ='"+fm.all("ManageCom").value+"'" +
	"  and wageno ='"+wageno+"' with ur";
	turnPage1.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
	//判断是否查询成功
	if (turnPage1.strQueryResult) {
		var message="";
		if(month ='06')
	    {
			message="上半年";
	    }
	    else  if(month ='12')
	    {
	    	message="下半年";
	    }
		alert("管理机构"+fm.all("ManageCom").value+"在"+fm.all("Year").value+"的"+message+"已经录过半年奖系数");
		return true;
	}
	return false;
}
function checkWage()
{
	var wageno = fm.all("Year").value+fm.all("Month").value;
	var sql ="select 1 from lawage where branchtype ='5' and branchtype2='01'  " +
	"  and ManageCom ='"+fm.all("ManageCom").value+"'" +
	"  and indexcalno ='"+wageno+"' with ur";
	turnPage1.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
	if (turnPage1.strQueryResult) {
		var message="";
		if(month ='06')
	    {
			message="上半年";
	    }
	    else  if(month ='12')
	    {
	    	message="下半年";
	    }
		alert("管理机构"+fm.all("ManageCom").value+"在"+fm.all("Year").value+"的"+message+"薪资已经确认");
		return true;
	}
	return false;
}
