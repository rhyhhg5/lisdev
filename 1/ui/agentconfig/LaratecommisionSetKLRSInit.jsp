<%
//程序名称：LaratecommisionSetKLRSInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and  char(length(trim(comcode)))<=#4# ";
 var StrSql=" 1 and riskcode in (select code from ldcode where codetype=#klrscode# and comcode=#1#) ";
 var msql2=" 1 and   trim(code) in(#5#,#6#,#8#,#10#)";
 var StrSql1="1 and code>=#0#  ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('CurYear').value=''; 
    fm.all('PayintvCode').value='';
    fm.all('PayintvName').value='';
    fm.all('StartDay').value='';
    fm.all('EndDay').value='';
    fm.all('Rate').value='';  
    fm.all('year').value='';
    fm.all('yearName').value='';
    fm.all('ManageCom').value='';
    fm.diskimporttype.value='LARateCommision';
    fm.BranchType.value='1';
    fm.branchtype2.value='01';
  }
  catch(ex)
  {
    alert("在LaratecommisionSetKLRSInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	  iArray[1][0]="险种编码";          		//列名
	  iArray[1][1]="180px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
	  iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	  iArray[1][6]="0|1";
	  iArray[1][9]="险种编码|code:RiskCode&notnull";  
	  iArray[1][15]= "1";              	        //校验输入是否正确
	  iArray[1][16]= StrSql;  

    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="缴费年期"; //列名
    iArray[3][1]="0px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][9]="缴费年期";           
       
    iArray[4]=new Array();
    iArray[4][0]="保单年度"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[4][9]="保单年度|NotNull&NUM";
       
    iArray[5]=new Array();
    iArray[5][0]="缴费方式"; //列名
    iArray[5][1]="60px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许
	iArray[5][4]="payintv";              	        //是否引用代码:null||""为不引用    
    iArray[5][9]="缴费方式|code:payintv&NotNull";
	iArray[5][15]= "1";  
    iArray[5][16]= StrSql1;  

    
   
    iArray[6]=new Array();
    iArray[6][0]="佣金比率"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[6][9]="佣金比率|NotNull&NUM";
        
    iArray[7]=new Array();
    iArray[7][0]="最低缴费年期"; //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[7][9]="最低缴费年期|NotNull&NUM";
    
    
    iArray[8]=new Array();
    iArray[8][0]="最高缴费年期"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许   
	iArray[8][9]="最高缴费年期|NotNull&NUM";
		
	iArray[9]=new Array();
    iArray[9][0]="序号"; //列名
    iArray[9][1]="0px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="管理机构"; //列名
    iArray[10][1]="40px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[10][4]="ComCode";
    iArray[10][9]="管理机构|code:ComCode&NotNull&NUM&len<=4";    
    iArray[10][15]="1";
    iArray[10][16]=tmanagecom;
    
    iArray[11]=new Array();
    iArray[11][0]="保险期间"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[11][4]="payyear"; 
    iArray[11][9]="保险期间|code:payyear&NotNull&NUM&VALUE>=5";    
    iArray[11][15]="1"; 
    iArray[11][16]=msql2; 

    

        
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    SetGrid.displayTitle = 1;

    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetKLRSInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
  }
  catch(re)
  {
    alert("在LaratecommisionSetKLRSInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>