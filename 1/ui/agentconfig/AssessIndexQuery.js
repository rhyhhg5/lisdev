//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

var turnPage = new turnPageClass(); 


// 查询按钮
function easyQueryClick()
{
	
	// 初始化表格

	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select branchtype,assesstype,agentgrade,destAgentGrade,assesscode,assessname,Indexcode,ATableName,AColName,CalCode from LAIndexVsAssess where 1 = 1"
					+ getWherePart('BranchType')
					+ getWherePart('AssessType')
					+ getWherePart('AgentGrade')
					+ getWherePart('IndexCode')
					+ getWherePart('AssessCode')
					+ getWherePart('AssessName');		

	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = AssessIndexGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrResult = new Array();
	var tSel = AssessIndexGrid.getSelNo();
	var i = 0;
  

	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
       for (i=1;i<AssessIndexGrid.colCount;i++)
			 {
              arrResult[i-1]=AssessIndexGrid.getRowColData(tSel-1,i);
			 }
		 	 top.opener.afterQuery( arrResult );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function querySubmit()
{
	 //var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	// showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHight:250px");

	 initAssessIndexGrid();
	 
   easyQueryClick();
}

function afterSubmit(FlagStr,content)
{
	 showInfo.close();
   if (FlagStr == "Fail" )
   {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   }
	 else
	 {
		  content = "查询成功!";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	 }
}
/*
function getQueryResult()
{
	var arrSelected = null;
	tRow = IndexGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initIndexGrid();

	// 书写SQL语句
	var strSQL = "";
	strSQL = "select BranchType,IndexType,IndexCode,IndexName,ITableName,IColName,CalType,CalCode,IndexSet from LAAssessIndex where 1=1 ";
	if( fm.all('IndexCode').value != "" || fm.all('IndexCode').value != null)
	   strSQL = strSQL + getWherePart( 'IndexCode');
  if( fm.all('BranchType').value != "" || fm.all('BranchType').value != null)
	   strSQL = strSQL + getWherePart( 'BranchType');
	if( fm.all('IndexType').value != "" || fm.all('IndexType').value != null)
	   strSQL = strSQL + getWherePart( 'IndexType');

  alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initIndexGrid();

		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				IndexGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}
*/

/** liujw */
function changeAgentGrade()
{
   var tBranchType = fm.BranchType.value;
   if (tBranchType == null || tBranchType == '')
   {
     fm.sql.value = "1 and 1 <> 1";
     fm.sql2.value = "1 and 1 <> 1";     
   }
   else
   {
     fm.sql.value = "1 and codealias = #"+tBranchType+"#";
     fm.sql2.value = "1 and (("+tBranchType+"=1 and (codealias is null or codealias = #"+tBranchType+"#)) or ("+tBranchType+"<>1 and codealias = #"+tBranchType+"#))";
   }
   return true;
}

function afterCodeSelect( cCodeName, Field )
{
	var value = Field.value;
	
	try	
	{
		if( cCodeName == 'BranchType' )	
		{			
		  var tBranchType = fm.BranchType.value;
                  if (tBranchType == null || tBranchType == "")
                  {
                    fm.sql.value = "1 and 1 <> 1";
                    fm.sql2.value = "1 and 1 <> 1";
                  }
                  else
                  {
                    fm.sql.value = "1 and codealias = #"+tBranchType+"#";
                    fm.sql2.value = "1 and (("+tBranchType+"=1 and (codealias is null or codealias = #"+tBranchType+"#)) or ("+tBranchType+"<>1 and codealias = #"+tBranchType+"#))";
		  }
		}
	}
	catch( ex ) 
	{
		alert('展业类型设置出错!');
	}
}

