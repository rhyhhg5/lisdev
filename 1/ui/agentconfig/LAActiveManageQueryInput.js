//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
    var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
      alert( "请先选择一条记录，再点击返回按钮" );
	else
	{
			try
			{	
				arrReturn = getQueryResult();
				top.opener.afterQuery(arrReturn);
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	tRow = AgentGrid.getSelNo();
	if( tRow == 0 || tRow == null)
	  return arrSelected;
	var arrSelected = new Array();
	var strSQL = "";
	strSQL = "Select "
        +"a.GroupAgentCode, "
        +"a.name, "
        +"a.ManageCom, "
        +"(select name from ldcom where comcode =a.ManageCom),"
        +"a.agentgroup, "
        +"(select name  from LABranchGroup  where a.agentgroup = agentgroup and (state<>'1' or state is null)),"
        +" a.agentcode "
        +" from LAAgent a"
        +" where 1=1 "
        +" and a.GroupAgentCode='"+AgentGrid.getRowColData(tRow-1,1)+"'";

	    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
        //判断是否查询成功
       if (!turnPage.strQueryResult) {
         alert("查询失败！");
         return false;
       }
       //查询成功则拆分字符串，返回二维数组
       arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
    if(!verifyInput()) return false;

	initAgentGrid();
	var tReturn = getManageComLimitlike("a.managecom");
	// 书写SQL语句
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "";
	strSQL = "select getunitecode(a.agentcode),b.BranchAttr,a.managecom,a.name,c.agentgrade,a.idno,a.agentstate,"
	         + "case when a.agentstate='01' then '在职'  when a.agentstate='02' then '二次增员' "
	         + " when a.agentstate='03' then '离职登记' when a.agentstate='04' then '二次离职登记' "
	         + " when a.agentstate='06' then '离职确认' when a.agentstate='07' then '二次离职确认' end "
	         + " from LAAgent a,LABranchGroup b,LATree c where 1=1 "
	         + " and a.Agentgroup = b.agentGroup and c.agentcode=a.agentcode and (b.state<>'1' or b.state is null)";
	         if(fm.all("AgentGroup").value!=null && fm.all("AgentGroup").value!='')
     	     {
	             strSQL+= " and b.branchattr like '"+fm.all('AgentGroup').value+"%'";
	         }
	         strSQL+=" and 1= 1 "
	         + tReturn
	        // + getWherePart('a.AgentCode','AgentCode')
	         + strAgent
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Name','Name')
	         + getWherePart('a.IDNo','IDNo')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         + getWherePart('a.AgentType','AgentType')
	         + getWherePart('a.IDNoType','IDNoType') ; 
	         
	         
	 var array = easyQueryVer3(strSQL,1,1,1);
	 if(!array)
	 {
		 alert("没有满足条件的数");
	 }
 	turnPage.queryModal(strSQL, AgentGrid); 
}


