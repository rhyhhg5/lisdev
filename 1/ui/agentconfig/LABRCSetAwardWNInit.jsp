<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
 var  mSQL=" 1 and  char(length(trim(comcode))) >= #4# and sign=#1# and comcode <> #86000000# " ;
 var tSQL = " 1 and BranchType=#3# and BranchType2=#01# ";
 var StrSql=" 1 and risktype4=#4# and riskcode in (select code from ldcode where codetype=#bankriskcode#)";
  </SCRIPT>       


<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('ManageCom').value='';
  	fm.all('ManageComName').value='';
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('CurYear').value=''; 
    fm.all('payyears').value='';
    fm.all('F01Code').value='';
    fm.all('payyearsName').value ='';	
    fm.all('AgentCom').value='';
    fm.all('Name').value='';
    fm.all('Rate').value='';   
    fm.all('F03Code').value='';
    fm.all('F04Code').value=''; 
    fm.all('F01Code').value=''; 
    fm.all('F01Name').value=''; 
    fm.all('BranchType').value=getBranchType();     
          
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    
    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="80px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[1][4]="RiskName";              	        //是否引用代码:null||""为不引用
	iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	iArray[1][6]="0|1";
    iArray[1][9]="险种编码|code:RiskName&notnull";
    iArray[1][15]= "1";              	        //校验输入是否正确
	iArray[1][16]= StrSql;  
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="序号"; //列名
    iArray[3][1]="0px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][9]="序号";           
//  iArray[5][10]="level";
//  iArray[5][11]="0|^02|支行|^03|分理处|^04|网点";
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[4][4]="ComCode";
    iArray[4][5]="4|5";
    iArray[4][6]="0|1";
    iArray[4][9]="管理机构|notnull";  
    iArray[4][15]="1";
    iArray[4][16]=mSQL; 
    
    iArray[5]=new Array();
    iArray[5][0]="管理机构名称"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许        

    
    iArray[6]=new Array();
    iArray[6][0]="代理机构"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许
     
  //   iArray[6][4]="agentcom";// agentcom1
  //   iArray[6][5]="6|7";
  //   iArray[6][6]="0|1";
    iArray[6][7]="getComName"; // 通过函数方式实现
  //   iArray[6][9]="代理机构";
   //  iArray[6][15]="1";
   //  iArray[6][16]=tSQL; 
   //  iArray[6][17]= "4"; 
    
    iArray[7]=new Array();
    iArray[7][0]="代理机构名称"; //列名
    iArray[7][1]="120px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许
	
	iArray[8]=new Array();
    iArray[8][0]="保费类型"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[8][9]="保费类型|NotNull";
    iArray[8][10]="F01Code";
    iArray[8][11]="0|^01|基本保费|^02|额外保费|^03|追加保费|";

    iArray[9]=new Array();
    iArray[9][0]="缴费方式"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[9][9]="缴费方式|NotNull";
    iArray[9][10]="PayintvCode";
    iArray[9][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";
    
    iArray[10]=new Array();
    iArray[10][0]="保险期间"; //列名
    iArray[10][1]="60px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=2;              //是否允许输入,1表示允许,0表示不允许 
    iArray[10][4]="payyear"; 
  //  iArray[10][5]="10|";  
  //  iArray[10][6]="0|1";  
	iArray[10][9]="保险期间|NotNull";
    
  //  iArray[11]=new Array();
   // iArray[11][0]="隐藏"; //列名
  //  iArray[11][1]="0px";        //列宽
  //  iArray[11][2]=100;            //列最大值
  //  iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    
   
      
    
    
    iArray[11]=new Array();
    iArray[11][0]="最低缴费年期"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[11][9]="最低缴费年期|NotNull&NUM";
    
    iArray[12]=new Array();
    iArray[12][0]="缴费年期小于"; //列名
    iArray[12][1]="80px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[12][9]="缴费年期小于|NotNull&NUM";
    
    iArray[13]=new Array();
    iArray[13][0]="保单年度"; //列名
    iArray[13][1]="60px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[13][9]="保单年度|NotNull&NUM";
    
    iArray[14]=new Array();
    iArray[14][0]="佣金比率"; //列名
    iArray[14][1]="80px";        //列宽
    iArray[14][2]=100;            //列最大值
    iArray[14][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[14][9]="佣金比率|NotNull&NUM";
      
	iArray[15]=new Array();
    iArray[15][0]="执行起期"; //列名
    iArray[15][1]="80px";        //列宽
    iArray[15][2]=100;            //列最大值
    iArray[15][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[15][9]="执行起期|NotNull&Date&len>8";
	
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    SetGrid.displayTitle = 1;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}



</script>