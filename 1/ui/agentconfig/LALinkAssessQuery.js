//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();
var tSql = "1 and codealias = #1# and code > #A03#";

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
  {
  	return false;
  }		
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //initPolGrid();
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LALinkAssessQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  var tManageCom=fm.all('ManageCom').value;
  var tAreaType=fm.all('AreaType').value;
  var tAgentGrade=fm.all('AgentGrade').value;
  var tArrangePeriod=fm.all('ArrangePeriod').value;	
  var tAssessRate =fm.all('AssessRate').value;
  var tLinkEndDate=fm.all('LinkEndDate').value;
  if (tManageCom ==null || tManageCom=='' )
  {
  	alert('请录入管理机构');
  	return false;
  }
  if (tAreaType ==null || tAreaType=='' )
  {
  	alert('请录入版本类型');
  	return false;
  }
  if (tAgentGrade ==null || tAgentGrade=='' )
  {
  	alert('请录入代理人职级');
  	return false;
  }
  if (tArrangePeriod ==null || tArrangePeriod=='' )
  {
  	alert('请录入筹备期');
  	return false;
  }
  if (tAssessRate ==null || tAssessRate=='' )
  {
  	alert('请录入考核比例');
  	return false;
  }
  if (tLinkEndDate ==null || tLinkEndDate=='' )
  {
  	alert('请录入筹备止期');
  	return false;
  }
  return true;
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
/*
function returnParent()
{
 
  var tRow=LinkAssessGrid.getSelNo();
   if(tRow>0)
   {
     //得到被选中的记录字符串
     tRow--;
     //alert(tRow);
     var str=fm.all("LALinkAssessInformation"+tRow).value;
     var arrRecord = str.split("|");  //拆分字符串，形成返回的数组
     for(var i=0; i<13;i++)
     {
       if(arrRecord[i]=='null')
         arrRecord[i]='';
     }
     top.opener.fm.all('AgentCode').value = arrRecord[0];
     top.opener.fm.all('DepartTimes').value = arrRecord[1];
     top.opener.fm.all('DepartDate').value = arrRecord[2];
     top.opener.fm.all('DepartRsn').value = arrRecord[3];
     top.opener.fm.all('ContractFlag').value = arrRecord[4];
     top.opener.fm.all('WorkFlag').value = arrRecord[5];
     top.opener.fm.all('PbcFlag').value = arrRecord[6];
     top.opener.fm.all('ReceiptFlag').value = arrRecord[7];
     top.opener.fm.all('LostFlag').value = arrRecord[8];
     top.opener.fm.all('CheckFlag').value = arrRecord[9];
     top.opener.fm.all('WageFlag').value = arrRecord[10];
     top.opener.fm.all('TurnFlag').value = arrRecord[11];
     top.opener.fm.all('Noti').value = arrRecord[12];
     top.opener.fm.all('Operator').value = arrRecord[13];
     top.close();
   }
   else
   {
     alert("请先选择记录！");	
   }
}*/
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LALinkAssessGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = LALinkAssessGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected;
	//alert(tRow);
	arrSelected = new Array();
	arrSelected = arrDataSet[turnPage.pageIndex % turnPage.blockPageNum * turnPage.pageLineNum+tRow-1];
	
	return arrSelected;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initLALinkAssessGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select ManageCom,AgentGrade,ArrangePeriod,AssessRate,LinkEndDate,AreaType,Operator from LALinkAssess where 1=1 "
	         + getWherePart('ManageCom')
	         + getWherePart('AgentGrade')
	         + getWherePart('ArrangePeriod')
	         + getWherePart('AssessRate')
	         + getWherePart('LinkEndDate')
	         + getWherePart('Operator') 
	         + getWherePart('AreaType')
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(tArr,[0,1,2,3,4,5]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = LALinkAssessGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr,turnPage.pageDisplayGrid);
}