<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  String querySql = request.getParameter("querySQL");
  //设置表头
  String[][] tTitle = {{"套餐编码","套餐名称","管理机构","管理机构名称","代理机构",
    "代理机构名称","手续费比例"}};
  //表头的显示属性
  int []displayTitle = {1,2,3,4,5,6,7};
    
  //数据的显示属性
  int []displayData = {1,2,3,4,5,6,7};
  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  System.out.println("errorFlag:1");

  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  System.out.println("errorFlag:2");

  int row = createexcellist.setData(tTitle,displayTitle);

  if(row ==-1) errorFlag = true;
  System.out.println("errorFlag:"+errorFlag);

  createexcellist.setRowColOffset(row,0);//设置偏移

  if(createexcellist.setData(querySql,displayData)==-1){
  	errorFlag = true;
  }

  if(!errorFlag)

  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>

