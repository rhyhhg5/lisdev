<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-07-22
//创建人  ：miaoxz
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom="  1 and char(length(trim(comcode))) in (#4#,#2#) ";
 var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and riskprop = #G#)";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value='';
    fm.all('ManageComName').value=''; 

    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value='';
    fm.all('WageSort').value='';
    fm.all('WageSortName').value='';
    fm.all('StandPremSort').value='';
    fm.all('StandPremSortName').value='';
    fm.all('BranchType').value=getBranchType();                    
  }
  catch(ex)
  {
    alert("在LAGroupStandPremInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    iArray[1]=new Array();
	  iArray[1][0]="险种编码";          		//列名
	  iArray[1][1]="80px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
	  iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	  iArray[1][6]="0|1";
	  iArray[1][9]="险种编码|code:RiskCode&notnull";  
	  iArray[1][15]= "1";              	        //校验输入是否正确
	  iArray[1][16]= StrSql;  
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
                                       
    iArray[3]=new Array();
    iArray[3][0]="提奖方式"; //列名
    iArray[3][1]="60px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[3][9]="提奖方式|NotNull";
    iArray[3][10]="WageSort";
    iArray[3][11]="0|^0|无缴费年期|^1|分缴费年期";
 
    iArray[4]=new Array();
    iArray[4][0]="折标方式"; //列名
    iArray[4][1]="60px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[4][9]="折标方式|NotNull";
    iArray[4][10]="StandPremSort";
    iArray[4][11]="0|^0|无缴费年期|^1|分缴费年期";
		
		iArray[5]=new Array();
    iArray[5][0]="序号"; //列名
    iArray[5][1]="0px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="管理机构"; //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[6][4]="ComCode";
    //iArray[6][5]="1|1";
    //iArray[6][6]="0|1";
    iArray[6][9]="管理机构|NotNull&NUM";    
    iArray[6][15]="1";
    iArray[6][16]=tmanagecom;
    
    iArray[7]=new Array();
	  iArray[7][0]="险种编码";          		//列名
	  iArray[7][1]="0px";      	      		//列宽
	  iArray[7][2]=20;            			//列最大值
	  
	iArray[8]=new Array();
	  iArray[8][0]="管理机构";          		//列名
	  iArray[8][1]="0px";      	      		//列宽
	  iArray[8][2]=20; 
	
	iArray[9]=new Array();
    iArray[9][0]="序号"; //列名
    iArray[9][1]="0px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=3;              //是否允许输入,1表示允许,0表示不允许
	
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    //SetGrid.hiddenSubtraction =0;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>