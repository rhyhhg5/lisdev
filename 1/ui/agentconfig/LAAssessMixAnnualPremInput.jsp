<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAAgentInput.jsp
//程序功能：个人代理增员管理
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
%>
<script>
var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
var manageCom = <%=tG.ManageCom%>;

</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="LAAssessMixAnnualPremInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="LAAssessMixAnnualPremInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./LAAssessMixAnnualPremSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
				<td class=titleImg>
					  查询条件录入
				</td>
			</tr>
		</table>
	<Div  id= "divLAAgent1" style= "display: ''">
		<table class="common" align=center>
			<TR  class= common> 
				<TD  class= title>
					管理机构
				</TD>
				<TD  class= input>
					<Input class="codeno" name=ManageCom verify="管理机构|notnull"
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
					><Input class=codename name=ManageComName readOnly elementtype=nacessary  > 
				</TD> 
				<TD class=title>业务员代码</TD>
				<TD class=input><Input name=GroupAgentCode class='codeno'
					ondblclick="return showCodeList('agentcode',[this,AgentName],[0,1],null,msql,'1',1);" 
					onkeyup="return showCodeListKey('agentcode',[this,AgentName],[0,1],null,msql,'1',1);"
					><Input
					class=codename name=AgentName  readOnly></TD>
				<TD class=title>月份</TD>
				 <TD class=title>
			          <Input class=common name=WageNo verify="月份|len=6"><font color="red">YYYYMM</font>
		        </TD>
				</TR>		
		</TR>
	</table>
</Div>
<br/>
   &nbsp;&nbsp;<INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class=cssbutton >
			   <INPUT VALUE="保  存" TYPE=button onclick="return submitForm();" class=cssbutton >
			   <INPUT VALUE="重  置" TYPE=button onclick="return initForm();" class=cssbutton >
			   <INPUT VALUE="修  改" TYPE=button onclick="return updateClick();" class=cssbutton >
			   <INPUT VALUE="下  载" TYPE=button onclick="return downClick();" class=cssbutton >
	 <table>
	 	<tr>
	 		<td class=common>
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
	 				交叉销售折标保费录入
 			</td>
	 	</tr>
	 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanActiveChargeGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 </div>
<font color="red">注：修改时只能修改金额，对于管理机构，业务员代码，月份不能修改!如果已考核计算请重新进行考核计算</font>
 <Input type=hidden name=BranchType  value =<%=BranchType %>>   
 <Input type=hidden name=BranchType2 value =<%=BranchType2 %>> 
 <input type=hidden class=Common name=querySql>
 <input type=hidden id="fmAction" name="fmAction">   
 <Input type=hidden name=hideOperate >   
 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


