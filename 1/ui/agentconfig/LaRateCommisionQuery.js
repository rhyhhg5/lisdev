//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	initSetGrid();
	var tManageCom  = fm.all('ManageCom').value;
	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请输入管理机构！");
		return;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,b.riskname,nvl(a.year,0),nvl(a.curyear,0),trim(a.payintv),"
	+ " nvl(a.rate,0),a.f03,a.f04,a.idx,a.ManageCom "
	+ " from laratecommisioncompare a,lmriskapp b "
	+ " where a.riskcode = b.riskcode and a.makedate=(select max(c.makedate) from laratecommisioncompare c)"
	+getWherePart('a.branchtype','BranchType')
	+getWherePart('a.branchtype2','branchtype2')
	+ getWherePart( 'a.riskcode','RiskCode')
	+ getWherePart( 'a.PayIntv','PayintvCode')
	+ getWherePart( 'a.ManageCom','ManageCom')
	+ getWherePart( 'a.curyear','CurYear','','1')
	+ getWherePart( 'a.f03','StartDay','','1')
	+ getWherePart( 'a.f04','EndDay','','1')
	+ getWherePart( 'a.rate','Rate','','1');
	//if(fm.PayintvCode.value=='1')
	//{
		//strSQL += " and A.payintv='0' ";
	//}
//	else if(fm.PayintvCode.value=='2')
//		{
//			strSQL += " and A.payintv='12' ";
//		}
//		else if(fm.PayintvCode.value=='3')
//			{
//				strSQL += " and A.payintv='6' ";
//			}
//			else if(fm.PayintvCode.value=='4')
//				{
//					strSQL += " and A.payintv='3' ";
//				}
//				else if(fm.PayintvCode.value=='5')
//					{
//						strSQL += " and A.payintv='1' ";
//					}
//					//+ " Order by a.riskcode,a.curyear,a.f03,a.f04 ";
//					//查询SQL，返回结果字符串
					turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
					//判断是否查询成功
					if (!turnPage.strQueryResult) {
						alert("没有符合条件的数据，请重新录入查询条件！");
						return false;
					}
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
					//查询成功则拆分字符串，返回二维数
					//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = SetGrid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;
					//设置查询起始位置
					turnPage.pageIndex = 0;
					//在查询结果数组中取出符合页面显示大小设置的数
					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
					}
//执行下载操作
function DoDownload(){
	var tManageCom  = fm.all('ManageCom').value;
	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请输入管理机构！");
		return;
	}
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.target = "f1print";
	fm.action="../f1print/RateCommisionQueryDownload.jsp";
	//fm.all('op').value = '';
	fm.submit();
	showInfo.close();


}					
				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}
	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}
	//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}
		//Click事件，当点击“修改”图片时触发该函数

//判断是否选择了要增加、修改或删除的行
function chkMulLine()
		{
								//alert("enter chkmulline");
								var i;
								var selFlag = true;
								var iCount = 0;
								var rowNum = SetGrid.mulLineCount;
								for(i=0;i<rowNum;i++)
								{
									if(SetGrid.getChkNo(i))
									{
										iCount++;
										if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
										{
											alert("险种代码不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										//if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3) == ""))
										//{
										//	alert("缴费年期不能为空");
										//	SetGrid.setFocus(i,1,SetGrid);
										//	selFlag = false;
										//	break;
									  //	}
										if((SetGrid.getRowColData(i,4) == null)||(SetGrid.getRowColData(i,4) == ""))
										{
											alert("保单年度不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
										{
											alert("缴费方式不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,6) == null)||(SetGrid.getRowColData(i,6) == ""))
										{
											alert("佣金比率不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
										{
											alert("有效起期不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,8) == null)||(SetGrid.getRowColData(i,8) == ""))
										{
											alert("有效止期不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										
										if((SetGrid.getRowColData(i,7)-SetGrid.getRowColData(i,8))>0)
										{
											alert("起期不能晚于止期！");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
  									    //如果Serialno为空不能删除和更新
										if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)==""))
										{
											if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
											{
												alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
												SetGrid.checkBoxAllNot();
												SetGrid.setFocus(i,1,SetGrid);
												selFlag = false;
												break;
											}
										}
										else if ((SetGrid.getRowColData(i,9) !== null)||(SetGrid.getRowColData(i,9)!==""))
											{	//如果Serialno不为空不能插入
												if(fm.fmAction.value == "INSERT")
												{
													alert("此纪录已存在，不可插入！");
													SetGrid.checkBoxAllNot();
													SetGrid.setFocus(i,1,SetGrid);
													selFlag = false;
													break;
												}
											}

										}
										else
											{//不是选中的行
												if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)==""))
												{
													alert("有未保存的新增纪录！请先保存记录。");
													SetGrid.checkBoxAllNot();
													SetGrid.setFocus(i,1,SetGrid);
													selFlag = false;
													break;
												}
											}
										} 
										if(!selFlag) return selFlag;
										if(iCount == 0)
										{
											alert("请选择要保存或删除的记录!");
											return false
										}
										return true;
									}
									//提交前的校验、计算
function beforeSubmit()
									{
									  if(!SetGrid.checkValue("SetGrid")) return false;
										if(!chkMulLine()) return false;
										return true;
									}

function showDiv(cDiv,cShow)
									{
										if (cShow=="true")
										{
											cDiv.style.display="";
										}
										else
											{
												cDiv.style.display="none";
											}
									}
