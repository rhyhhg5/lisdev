//               该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2009-07-24
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}
// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initSetGrid();
	var tRiskCode  = fm.RiskCode.value;

  var tManageCom  = fm.ManageCom.value;

	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请选择管理机构代码！");
		return;
	}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.codename,(select riskname from lmriskapp where riskcode=a.codename),"
	+"a.othersign,b.othersign,a.code,a.comcode,a.codename,a.comcode,b.code"
	+ " from ldcode a,ldcode b "
	+ " where a.codetype='GWRISKCODE' and b.codetype='GSPRISKCODE' "
	+"and a.comcode=b.comcode and a.codename=b.codename "
	+" and a.comcode like '"+tManageCom+"%' "
	+ getWherePart( 'a.codename','RiskCode')
	+ getWherePart( 'a.othersign','WageSort')
	+ getWherePart( 'b.othersign','StandPremSort')
	+" order by a.codename,a.comcode";
					//查询SQL，返回结果字符串
					turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

					//判断是否查询成功
					if (!turnPage.strQueryResult) {
						alert("没有符合条件的数据，请重新录入查询条件！");
						return false;
					}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;

					//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = SetGrid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;

					//设置查询起始位置
					turnPage.pageIndex = 0;

					//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
				}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}

	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
	function DoReset()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}




	//显示frmSubmit框架，用来调试
	function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}


		//Click事件，当点击“修改”图片时触发该函数
		function DoInsert()
		{
				fm.fmAction.value = "INSERT";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
		}
			//Click事件，当点击“修改”图片时触发该函数
		function DoUpdate()
		{
			if (confirm("您确实想修改该记录吗?"))
			{
				fm.fmAction.value = "UPDATE";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
			}
			else
				{
					fm.OperateType.value = "";
					alert("您取消了修改操作！");
				}
			}
			//Click事件，当点击“修改”图片时触发该函数
		function DoDelete()
		{
			if (confirm("您确实想删除该记录吗?"))
			{
				fm.fmAction.value = "DELETE";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
			}
			else
				{
					fm.OperateType.value = "";
					alert("您取消了删除操作！");
				}
			}

							//判断是否选择了要增加、修改或删除的行
							function chkMulLine()
							{
								//alert("enter chkmulline");
								var i;
								var selFlag = true;
								var iCount = 0;
								var rowNum = SetGrid.mulLineCount;
								var tfmAction=fm.all('fmAction').value;
								for(i=0;i<rowNum;i++)
								{
									if(SetGrid.getChkNo(i))
									{
										iCount++;
										if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
										{
											alert("险种代码不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3) == ""))
										{
											alert("提奖方式不能为空");
											SetGrid.setFocus(i,3,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,4) == null)||(SetGrid.getRowColData(i,4) == ""))
										{
											alert("折标方式不能为空");
											SetGrid.setFocus(i,4,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,6) == null)||(SetGrid.getRowColData(i,6) == ""))
										{
											alert("管理机构不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
										{
											if(tfmAction=='UPDATE'||tfmAction=='DELETE'){
												alert("第"+(i+1)+"行为新增记录，请执行新增操作。");
												SetGrid.setFocus(i,1,SetGrid);
												selFlag = false;
												break;
											}
										}else{
											if(tfmAction=='INSERT'){
												alert("第"+(i+1)+"行不是新增记录，请执行修改或删除。");
												SetGrid.setFocus(i,1,SetGrid);
												selFlag = false;
												break;
											}
											if(SetGrid.getRowColData(i,1)!=SetGrid.getRowColData(i,7)){
												alert("第"+(i+1)+"行原险种为"+SetGrid.getRowColData(i,7)+"，不能修改险种．");
												SetGrid.setFocus(i,1,SetGrid);
												selFlag = false;
												break;
											}
											if(SetGrid.getRowColData(i,6)!=SetGrid.getRowColData(i,8)){
												alert("第"+(i+1)+"行原管理机构为"+SetGrid.getRowColData(i,8)+"，不能修改管理机构．");
												SetGrid.setFocus(i,6,SetGrid);
												selFlag = false;
												break;
											}
										}
									}else{//不是选中的行
												if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5)==""))
												{
													alert("有未保存的新增纪录！请先保存记录。");
													SetGrid.checkBoxAllNot();
													SetGrid.setFocus(i,1,SetGrid);
													selFlag = false;
													break;
												}
											}
										}
										if(!selFlag) return selFlag;
										if(iCount == 0)
										{
											alert("请选择要保存或删除的记录!");
											return false
										}
										return true;
									}

//--									//提交前的校验、计算
									function beforeSubmit()
									{
										
										if(!SetGrid.checkValue("SetGrid")) return false;
										if(!chkMulLine()) return false;
										return true;
									}

									function showDiv(cDiv,cShow)
									{
										if (cShow=="true")
										{
											cDiv.style.display="";
										}
										else
											{
												cDiv.style.display="none";
											}
									}

									

						





