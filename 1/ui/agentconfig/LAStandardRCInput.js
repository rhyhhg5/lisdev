//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}
//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	initSetGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.ManageCom,a.riskcode,b.riskname,nvl(a.curyear,0),trim(a.payintv),"
	+ " nvl(a.rate,0),a.f03,a.f04 "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.riskcode = b.riskcode  and a.riskcode<>'330801' and a.managecom='86'"
	+getWherePart('a.branchtype','BranchType')
	+getWherePart('a.branchtype2','branchtype2')
	+ getWherePart( 'a.riskcode','RiskCode')
	+ getWherePart( 'a.PayIntv','PayintvCode')
	+ getWherePart( 'a.curyear','CurYear','','1')
	+ getWherePart( 'a.f03','StartDay','','1')
	+ getWherePart( 'a.f04','EndDay','','1')
	+ getWherePart( 'a.rate','Rate','','1')
	+ " Order by a.managecom ,a.riskcode,a.f03,a.f04 ,a.curyear";
    turnPage.queryModal(strSQL, SetGrid);
    if(SetGrid.mulLineCount==0){
    	alert("没有查询到任何满足条件的数据！");
    	return false;
 	}
    
}
function DoNewDownload()
{
 // 书写SQL语句
	var stSQL = "";
	strSQL = "select a.ManageCom,a.riskcode,b.riskname,nvl(a.curyear,0),trim(a.payintv),"
	+ " nvl(a.rate,0),a.f03,a.f04 "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.riskcode = b.riskcode  and a.riskcode<>'330801' and a.managecom='86' "
	+getWherePart('a.branchtype','BranchType')
	+getWherePart('a.branchtype2','branchtype2')
	+ getWherePart( 'a.riskcode','RiskCode')
	+ getWherePart( 'a.PayIntv','PayintvCode')
	+ getWherePart( 'a.curyear','CurYear','','1')
	+ getWherePart( 'a.f03','StartDay','','1')
	+ getWherePart( 'a.f04','EndDay','','1')
	+ getWherePart( 'a.rate','Rate','','1')
	+ " Order by a.managecom ,a.riskcode,a.f03,a.f04 ,a.curyear";
    fm.querySql.value = strSQL;
    fm.action = "LAStandardRCDownload.jsp";
    fm.submit();
}					
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}
	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}
	//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
		{
			if (confirm("您确实想修改该记录吗?"))
			{
				fm.fmAction.value = "UPDATE";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
			}
			else
				{
					fm.OperateType.value = "";
					alert("您取消了修改操作！");
				}
			}
							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
		{
								//alert("enter chkmulline");
								var i;
								var selFlag = true;
								var iCount = 0;
								var rowNum = SetGrid.mulLineCount;
								for(i=0;i<rowNum;i++)
								{
									if(SetGrid.getChkNo(i))
									{
										iCount++;
										if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
										{
											alert("险种代码不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										//if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3) == ""))
										//{
										//	alert("缴费年期不能为空");
										//	SetGrid.setFocus(i,1,SetGrid);
										//	selFlag = false;
										//	break;
									  //	}
										if((SetGrid.getRowColData(i,4) == null)||(SetGrid.getRowColData(i,4) == ""))
										{
											alert("保单年度不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
										{
											alert("缴费方式不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,6) == null)||(SetGrid.getRowColData(i,6) == ""))
										{
											alert("佣金比率不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
										{
											alert("有效起期不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										if((SetGrid.getRowColData(i,8) == null)||(SetGrid.getRowColData(i,8) == ""))
										{
											alert("有效止期不能为空");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
										
										if((SetGrid.getRowColData(i,7)-SetGrid.getRowColData(i,8))>0)
										{
											alert("起期不能晚于止期！");
											SetGrid.setFocus(i,1,SetGrid);
											selFlag = false;
											break;
										}
  									    //如果Serialno为空不能删除和更新
										if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)==""))
										{
											if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
											{
												alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
												SetGrid.checkBoxAllNot();
												SetGrid.setFocus(i,1,SetGrid);
												selFlag = false;
												break;
											}
										}
										else if ((SetGrid.getRowColData(i,9) !== null)||(SetGrid.getRowColData(i,9)!==""))
											{	//如果Serialno不为空不能插入
												if(fm.fmAction.value == "INSERT")
												{
													alert("此纪录已存在，不可插入！");
													SetGrid.checkBoxAllNot();
													SetGrid.setFocus(i,1,SetGrid);
													selFlag = false;
													break;
												}
											}

										}
										else
											{//不是选中的行
												if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)==""))
												{
													alert("有未保存的新增纪录！请先保存记录。");
													SetGrid.checkBoxAllNot();
													SetGrid.setFocus(i,1,SetGrid);
													selFlag = false;
													break;
												}
											}
										} 
										if(!selFlag) return selFlag;
										if(iCount == 0)
										{
											alert("请选择要保存或删除的记录!");
											return false
										}
										return true;
									}
									//提交前的校验、计算
function beforeSubmit()
									{
									  if(!SetGrid.checkValue("SetGrid")) return false;
										if(!chkMulLine()) return false;
										return true;
									}

function showDiv(cDiv,cShow)
									{
										if (cShow=="true")
										{
											cDiv.style.display="";
										}
										else
											{
												cDiv.style.display="none";
											}
									}
function diskImport()
{ 
    //alert("111111111111111");
		//alert("====="+fm.all("diskimporttype").value);
   var strUrl = "../agentconfig/DiskImportRateComminsionMain.jsp?diskimporttype="+fm.all("diskimporttype").value+"&branchtype="+fm.all("BranchType").value+"&branchtype2="+fm.all("branchtype2").value;
   showInfo=window.open(strUrl,"佣金率信息导入","width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=yes");
}
function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "RateCommisionModule.jsp";
    fm.submit();
    fm.action = oldAction;
}