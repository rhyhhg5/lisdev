<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAWageRadixSetSave.jsp
//程序功能：
//创建日期：2003-10-16
//创建人  ：   zsj
//更新记录：  更新人    更新日期     	更新原因/内容
//						gt				2005-05-18		采用Map类提交数据
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "True";

	  String tCode1=request.getParameter("ManageCom");
	  String tCode2=request.getParameter("AreaType");
	  String tCode3=null;
	  String cOperate = request.getParameter("toperate");
	  System.out.println("????????"+cOperate);
	  
	  GlobalInput tGI = new GlobalInput();
	  tGI = (GlobalInput)session.getValue("GI");
     
      LDCodeRelaSchema tLDCodeRelaSchema = new LDCodeRelaSchema();
     
      tLDCodeRelaSchema.setRelaType("comtoareatype1");
      tLDCodeRelaSchema.setCode1(tCode1);
      tLDCodeRelaSchema.setCode2(tCode2);
      tLDCodeRelaSchema.setCode3("single");
      tLDCodeRelaSchema.setDescription("管理机构与地区类型的对照关系");
      tLDCodeRelaSchema.setOtherSign("2");
      
      // 准备传输数据 VData
      VData tVData = new VData();  
      tVData.add(tLDCodeRelaSchema);
      tVData.add(tGI);
      System.out.println("Start tLAAreaUI Submit...");
      
      LAAreaUI tLAAreaUI = new LAAreaUI();
      try
      {
    	  tLAAreaUI.submitData(tVData,cOperate);
      }
      catch(Exception ex)
      {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAreaUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
    System.out.println(Content);  
  }  
 %>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
