<html>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

  <SCRIPT src="AssessIndexQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AssessIndexQueryInit.jsp"%>
</head>

<body  onload="initForm();" >
  <form action="AssessIndexQuerySubmit.jsp" method=post name=fm target="fraSubmit">
      <table>
			  <tr>
				  <td> <IMG src="../common/images/butExpand.gif" style="cursor:hand;"onclick="showPage(this,divIndexQry);">
				  </td>
				  <td class=titleImg>请您输入查询条件：</td>
				</tr>
			</table>
			<div id="divIndexQry" style="display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title> 展业类型 </TD>
          <td class=input>
           <input class=code name=BranchType 
                             ondblclick="return showCodeList('BranchType',[this]);" 
                             onkeyup="return showCodeListKey('BranchType',[this]);"
                             onchange="return changeAgentGrade();">
          </td>        
          <TD  class= title>代理人职级</TD>
          <TD  class= input>
             <input class=code name=AgentGrade 
                                ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,sql,'1');" 
                                onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,sql,'1');">
          </TD>
        </TR>   
     
       <tr>
       <td class=title>考核类型</td>
       <td class=input> <input class=code name="AssessType" CodeData="0|2^01|维持^02|晋升"
                                                ondblclick="return showCodeListEx('AssessType1',[this]);" 
                                                onkeyup="return showCodeListKeyEx('AssessType1',[this]);">
        <td class=title>目标职级</td>
        <td class=input> 
          
             <input class=code name=DestAgentGrade 
                                ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,sql2,'1');" 
                                onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,sql2,'1');">
        </td>
       </tr>
       <tr>
        <td class=title>考核代码</td>
        <td class=input> <input class=common name=AssessCode >
        </td>
        <td class=title>考核名称</td>
        <td class=input> <input class=common name=AssessName >
        </td>
       </tr>

       <tr>
       <td class=title>SQL语句编码</td>      
       <td class=input> 
         <input class=common name="CalCode" >
       </td>
        <td class=title>对应指标编码</td>
        <td class=input> <input class=common name=IndexCode >
        </td>
      </tr>
    </Table>
    </div>
     <input type=hidden name=sql value="1 and 1 <> 1">
     <input type=hidden name=sql2 value="1 and 1 <> 1">
     <INPUT VALUE="查询" TYPE=button onclick="querySubmit();">
     <INPUT VALUE="返回" TYPE=button onclick="returnParent();">

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessIndexGrid);">
    		</td>
    		<td class= titleImg>
    			 考核指标信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAssessIndexGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAssessIndexGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
		  <INPUT class = common VALUE="首页" TYPE=button onclick="getFirstPage();">
	    <INPUT class = common VALUE="上一页" TYPE=button onclick="getPreviousPage();">
	    <INPUT class = common VALUE="下一页" TYPE=button onclick="getNextPage();">
      <INPUT class = common VALUE="尾页" TYPE=button onclick="getLastPage();">
   </div>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
