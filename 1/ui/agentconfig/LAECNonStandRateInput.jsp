<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LABComNSRateCommInput.jsp
//程序功能：银代非标准业务网点手续费录入
//创建日期：2008-01-24 
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<% 
  String agentcode=request.getParameter("AgentCode");
  if(agentcode==null || agentcode.equals(""))
  {
     agentcode="";
  }
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>

<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAECNonStandRateInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAECNonStandRateInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务人员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAECNonStandRateSave.jsp" method=post name=fm target="fraSubmit">
	<table class="common" align=center>
		<tr align=right>
			<td class=button>
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return addClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="删  除"  TYPE=button onclick="return deleteClick();">
			</td>
		</tr>
	</table>
  <table>
  	<tr>
    	<td>
    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
			</td>
    	<td class= titleImg>非标录入信息</td>   		 
    </tr>
	</table>
  <Div  id= "divLAArchieve1" style= "display: ''">
	  <table  class= common align='center' >
    <TR  class= common>
      <TD  class= title>保单号</TD>
      <TD  class= input>
        <Input class= 'common'   name=ContNo verify="保单号|notnull" elementtype=nacessary>
      </TD> 
    </TR>
	  </table>
	  <BR>
    <table class=common border=0 width=100%>
      <TR class=common>  
        <TD  class=title>
          <INPUT VALUE="查询"   TYPE=button   class=cssButton onclick="easyQueryClick();">				
        </TD>
      </TR>
    </table>
	  <BR>
	  
	  <tr>
	<p> 
		<font color="#ff0000">(注：手续费比例必须为0-1的数，如23%，记为0.23!) 
		</font>
	</p>
</tr>
	  
	  <Div  id= "divArchieve" style= "display: ''">
	  <table  class= common>
    	<tr  class= common>
      	<td text-align: left colSpan=1>
    			<span id="spanArchieveGrid" >
    			</span> 
    		</td>
    	</tr>
    </table>
	</Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="fmAction" name="BranchType">
    <input type=hidden id="fmAction" name="BranchType2">
  </form>
</body>
</html>
