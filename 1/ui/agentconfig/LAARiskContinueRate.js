//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2009-01-13
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var queryFlag = false;
var strSQL ="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
if(showInfo!=null)
{
  try
  {
    showInfo.focus();  
  }
  catch(ex)
  {
    showInfo=null;
  }
}
}
//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(queryFlag ==false) 
	{
		alert("请先查询");
		return false;	
	}
	if(!beforeSubmit()) return false;
	if(!SetGrid.checkValue("SetGrid")) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

// 查询按钮
function easyQueryClick()
{    
	// 初始化表格
	initSetGrid();
	var tManageCom  = fm.all('ManageCom').value;
	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请输入管理机构！");
		return;
	}
	// 书写SQL语句
	strSQL = "select a.riskcode,b.riskname,a.ManageCom,(select name from ldcom where comcode=a.managecom),a.agentcom,(select name from lacom where agentcom = a.agentcom),a.F03,a.Year,nvl(a.rate,0),nvl(a.Curyear,0),a.PayIntv," +
			"a.idx "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.riskcode = b.riskcode  and a.ManageCom is not null and a.f02='C10201' and a.riskcode not in ('330801','331801','332701') and  b.risktype4 <>'4'    "
	+getWherePart('a.branchtype','BranchType')
	+getWherePart('a.branchtype2','branchtype2')
	+ getWherePart( 'a.riskcode','RiskCode')
	+ getWherePart( 'a.F03','F03')
	+ getWherePart( 'a.ManageCom','ManageCom')
	+ getWherePart('a.AgentCom','AgentCom')
	// + getWherePart( 'a.f05','payyears')
	+ getWherePart( 'a.curyear','CurYear','','1')
	+ getWherePart( 'a.rate','Rate','','1')
	+ " Order by a.managecom ,a.riskcode,a.f01,a.f02 ,a.curyear";
 
	turnPage.queryModal(strSQL,SetGrid);
	if (!turnPage.strQueryResult)
	  {
	  	alert('查询失败,不存在有效纪录！');
	  	return false;
	  }
	queryFlag = true;
}
				
				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}
//add 磁盘模板下载
function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "RateTraCommisionModule.jsp";
    fm.submit();
    fm.action = oldAction;
}
function clearImport(){
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
}
//add磁盘导入
function diskImport()
{ 
    //alert("111111111111111");
		//alert("====="+fm.all("diskimporttype").value);
   if(fm2.FileName.value==null||fm2.FileName.value==""){
   	 alert("请选择要导入的文件！");
   	 return false;
   }
   var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fm2.submit();
}

function queryImportResult(){
	var tbatchno=fm2.FileImportNo.value;
	var tqueryType=fm2.queryType.value;
	if(tbatchno==null||tbatchno==""){
		alert("请输入查询文件批次号！");
		return false;
	}
	if(tqueryType==null||tqueryType==""){
		alert("请选择查询类型！");
		return false;
	}
	strSQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog"
	+ " where batchno='"+tbatchno+"' ";
	if(tqueryType==1){
		strSQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		strSQL+=" and errorstate='0'";
	}
	strSQL+=" order by contid";
  //执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportTraResultGrid);
	}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{
	
	fm.fmAction.value = "UPDATE";
	if(queryFlag ==false) 
	{
		alert("请先查询");
		return false;	
	}
	if(!beforeSubmit()) return false;
	if (confirm("您确实想修改该记录吗?"))
	{
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
		showInfo.close();
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}
							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{
	//alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(SetGrid.getChkNo(i))
		{
			iCount++;
			if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
			{
				alert("险种代码不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}

			if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
			{
				alert("缴费年期不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9) == ""))
			{
				alert("佣金比率不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}

			if(SetGrid.getRowColData(i,9)<'0'||SetGrid.getRowColData(i,9)>'1')
			{
				alert("佣金比率不在0到1之间!");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,10) == null)||(SetGrid.getRowColData(i,10) == ""))
			{
				alert("保单年度不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
		
			if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3) == ""))
            {
	            alert("管理机构不能为空");
	            SetGrid.setFocus(i,1,SetGrid);
	            selFlag = false;
	            break;
            }
			if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
            {
	            alert("代理机构不能为空");
	            SetGrid.setFocus(i,1,SetGrid);
	            selFlag = false;
	            break;
            }
			if((SetGrid.getRowColData(i,11) == null)||(SetGrid.getRowColData(i,11) == ""))
			{
				alert("缴费方式不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
  			//如果Serialno为空不能删除和更新
			if((SetGrid.getRowColData(i,12) == null)||(SetGrid.getRowColData(i,12)==""))
			{
				if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
			else if ((SetGrid.getRowColData(i,12) !== null)||(SetGrid.getRowColData(i,12)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.fmAction.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((SetGrid.getRowColData(i,12) == null)||(SetGrid.getRowColData(i,12)==""))
				{
					alert("有未保存的新增纪录！请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		} 
		if(!selFlag) return selFlag;
	    if(iCount == 0)
		{
	    	var title="";
	    	if(fm.fmAction.value=="UPDATE")
	    	{
	    		title="修改";
	    	}
	    	if(fm.fmAction.value=="DELETE")
	    	{
	    		title="删除";
	    	}
	    	if(fm.fmAction.value=="INSERT")
	    	{
	    		title="新增";
	    	}
			alert("请选择要"+title+"的记录!");
			return false
		}
		return true;
}
//提交前的校验、计算
function beforeSubmit()
{
	if(!chkMulLine()) return false;
	return true;
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}
function getAgentCom(cObj,cName)
{
if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
}
else if (fm.all('ManageCom').value != null || trim(fm.all('ManageCom').value) != '') 
{
var strsql =" 1 and BranchType=#1# and BranchType2 =#02#  and managecom like #" + fm.all('ManageCom').value + "%# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,1,1);
}

}



