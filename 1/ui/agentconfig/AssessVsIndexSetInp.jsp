<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

  <SCRIPT src="AssessVsIndexInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AssessVsIndexSetInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./AssessVsIndexSetSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>   
    <table>
      <tr>
      <td>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssIndexSet);">
      </td>
      <td class= titleImg>
        考核指标设置信息
      </td>
    	</tr>
    </table>
    <Div  id= "divAssIndexSet" style= "display: ''">
    <table class=common>
         <TR  class= common>
          <TD  class= title> 展业类型 </TD>
          <td class=input>
           <input class=code name=BranchType verify = "展业类型|notnull&code:branchType"
                             ondblclick="return showCodeList('BranchType',[this]);" 
                             onkeyup="return showCodeListKey('BranchType',[this]);"
                             onchange="return changeAgentGrade();">
          </td>        
          <TD  class= title>代理人职级</TD>
          <TD  class= input>
             <input class=code name=AgentGrade verify = "代理人职级|notnull&code:AgentGrade" 
                                ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,sql,'1');" 
                                onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,sql,'1');">
          </TD>
        </TR>   
     
       <tr>
       <td class=title>考核类型</td>
       <td class=input> <input class=code name="AssessType" verify="考核类型|notnull&code:AssessType1" CodeData="0|2^01|维持^02|晋升"
                                                ondblclick="return showCodeListEx('AssessType1',[this]);" 
                                                onkeyup="return showCodeListKeyEx('AssessType1',[this]);">
        <td class=title>目标职级</td>
        <td class=input> 
          
             <input class=code name=DestAgentGrade verify = "代理人职级|notnull&code:AgentGrade" 
                                ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,sql2,'1');" 
                                onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,sql2,'1');">
        </td>
       </tr>
       <tr>
        <td class=title>考核代码</td>
        <td class=input> <input class=common name=AssessCode >
        </td>
        <td class=title>考核名称</td>
        <td class=input> <input class=common name=AssessName >
        </td>
       </tr>
       
       <tr>
        <td class=title>考核结果对应表名</td>
        <td class=input> <input class=readonly readonly name=ATableName value="LAAssess"  >
        </td>
        <td class=title nowrap>考核结果对应字段名</td>
        <td class=input> <input class=common name=AColName  >
        </td>
      </tr>    
      
      <tr>
       <td class=title>SQL语句编码</td>      
       <td class=input> <input class=common name="CalCode" verify="SQL语句编码|notnull&len<=6" >
       </td>
       <td class=title>对应指标编码</td>
       <td class=input> <input class=common name="IndexCode" verify="对应指标编码|len<=6" > </td>
       </td>
      </tr> 
    </Table>	
    </div>
   
   <input type=hidden name="fmAction" id="fmAction">
   <input type=hidden name=sql value="1 and 1 <> 1">
   <input type=hidden name=sql2 value="1 and 1 <> 1">
        
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
