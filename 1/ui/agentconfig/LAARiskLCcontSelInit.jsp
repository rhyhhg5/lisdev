<%
//程序名称：WageQueryListInit.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {  
    fm.all('ManageCom').value = '';
  
    fm.all('BranchType').value=='1'
    fm.all('BranchType2').value=='02'; 
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
    fm.all('ContNo').value = '';
    fm.all('RiskCode').value = ''; 
    fm.all('WageNo').value='';
    fm.all('AgentCom').value='';
    fm.all('GroupAgentCode').value='';
 
   <%-- fm.all('BranchType').value =  '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=Branchtype2%>'; --%> 
                           
  }
  catch(ex)
  {
    alert("在WageQueryListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化

function initSelBox()
{  
  try                 
  {
//     setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在WageQueryListInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
   initInpBox();
    initSelBox();  
    initLACommisionGrid();
   
  <%--  if (fm.all('BranchType').value=='1')
     {

		initPolGrid();	
     }	
     else if (fm.all('BranchType').value=='2')
     {
		    initGrpPolGrid();	

     }	--%> 
  }
  catch(re)
  {
    alert("WageQueryListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


// 保单信息列表的初始化
var LACommisionGrid;
function initLACommisionGrid()
  {                               
    var iArray = new Array();
      
      try
      {
     iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="管理机构编码";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="中介机构编码";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="中介机构名称";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="投保人";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="险种编码";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="险种名称";         		//列名
      iArray[7][1]="150px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="缴费年期";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 									//是否允许输入,1表示允许，0表示不允许
      
      

      iArray[9]=new Array();
      iArray[9][0]="保费";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 									//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="首期手续费";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 				
      
      
      iArray[11]=new Array();
      iArray[11][0]="首期手续费率";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0; 									//是否允许输入,1表示允许，0表示不允许     
      
      iArray[12]=new Array();
      iArray[12][0]="保单年度 ";         		//列名
      iArray[12][1]="100px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0; 	  								//是否允许输入,1表示允许，0表示不允许       
      
      iArray[13]=new Array();
      iArray[13][0]="续年手续费";         		//列名
      iArray[13][1]="100px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0; 									//是否允许输入,1表示允许，0表示不允许     
      
      iArray[14]=new Array();
      iArray[14][0]="续年手续费率";         		//列名
      iArray[14][1]="100px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=0; 	
      
      iArray[15]=new Array();                             
      iArray[15][0]="继续率奖金";         		//列名          
      iArray[15][1]="100px";            		//列宽          
      iArray[15][2]=100;            			//列最大值      
      iArray[15][3]=0; 	                                  
     
      iArray[16]=new Array();                             
      iArray[16][0]="月度奖";         		//列名          
      iArray[16][1]="100px";            		//列宽          
      iArray[16][2]=100;            			//列最大值      
      iArray[16][3]=0; 	                                  

      iArray[17]=new Array();
      iArray[17][0]="年度奖";         		//列名
      iArray[17][1]="100px";            		//列宽
      iArray[17][2]=100;            			//列最大值
      iArray[17][3]=0; 	      
    
      
      iArray[18]=new Array();
      iArray[18][0]="缴费次数";         		//列名
      iArray[18][1]="100px";            		//列宽
      iArray[18][2]=100;            			//列最大值
      iArray[18][3]=0; 	          
     
      iArray[19]=new Array();
      iArray[19][0]="签单日期";         		//列名
      iArray[19][1]="100px";            		//列宽
      iArray[19][2]=100;            			//列最大值
      iArray[19][3]=0;
      
      iArray[20]=new Array();
      iArray[20][0]="回执回销日期";         		//列名
      iArray[20][1]="100px";            		//列宽
      iArray[20][2]=100;            			//列最大值
      iArray[20][3]=0;
      
      iArray[21]=new Array();
      iArray[21][0]="交单日期";         		//列名
      iArray[21][1]="100px";            		//列宽
      iArray[21][2]=100;            			//列最大值
      iArray[21][3]=0; 	
      
      iArray[22]=new Array();
      iArray[22][0]="专员代码";         		//列名
      iArray[22][1]="100px";            		//列宽
      iArray[22][2]=100;            			//列最大值
      iArray[22][3]=0; 	 
      
      iArray[23]=new Array();
      iArray[23][0]="专员姓名";         		//列名
      iArray[23][1]="100px";            		//列宽
      iArray[23][2]=100;            			//列最大值
      iArray[23][3]=0; 	 
     
      iArray[24]=new Array();
      iArray[24][0]="人员状态";         		//列名
      iArray[24][1]="50px";            		//列宽
      iArray[24][2]=100;            			//列最大值
      iArray[24][3]=0;

      iArray[25]=new Array();
      iArray[25][0]="回访成功时间";         		//列名
      iArray[25][1]="100px";            		//列宽
      iArray[25][2]=100;            			//列最大值
      iArray[25][3]=0; 	 
     
      iArray[26]=new Array();
      iArray[26][0]="结算状态";         		//列名
      iArray[26][1]="50px";            		//列宽
      iArray[26][2]=100;            			//列最大值
      iArray[26][3]=0;
      
      iArray[27]=new Array();
      iArray[27][0]="手续费给付号";         		//列名
      iArray[27][1]="100px";            		//列宽
      iArray[27][2]=100;            			//列最大值
      iArray[27][3]=0;
       
      LACommisionGrid = new MulLineEnter( "fm" , "LACommisionGrid" ); 
      //这些属性必须在loadMulLine前
      LACommisionGrid.mulLineCount = 10;   
      LACommisionGrid.displayTitle = 1;
      LACommisionGrid.locked = 1;
      LACommisionGrid.canSel = 0;
      LACommisionGrid.hiddenPlus=1;       
      LACommisionGrid.hiddenSubtraction=1;
      LACommisionGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>