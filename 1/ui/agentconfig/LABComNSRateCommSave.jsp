<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABComNSRateCommSave.jsp
//程序功能：银代非标准业务网点手续费录入
//创建日期：2008-01-24 
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAConRateSchema tLAConRateSchema = new LAConRateSchema();
  LAConRateSet tLAConRateSet = new LAConRateSet();
  LAContRateUI tLAContRateUI = new LAContRateUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tOperate ="INSERT||MAIN";
  String mTest ="";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	System.out.println("a!~~~~~~~~~~~~~");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
	//取得Muline信息
  int lineCount = 0;
  String tRiskCode[] = request.getParameterValues("ArchieveGrid1");
  String tAgentCom[] = request.getParameterValues("ArchieveGrid6");
  String tGrpContNo[] = request.getParameterValues("ArchieveGrid3");
  String tManageCom[] = request.getParameterValues("ArchieveGrid4");
  String tRate[] = request.getParameterValues("ArchieveGrid13");
  String tGrpPolNo[] = request.getParameterValues("ArchieveGrid14");                                                             
  lineCount = tRiskCode.length; //行数
  System.out.println("length= "+String.valueOf(lineCount));
  
  for(int i=1;i<=lineCount;i++)
  {
     tLAConRateSchema = new LAConRateSchema();
     tLAConRateSchema.setGrpPolNo(tGrpPolNo[i-1]);
     tLAConRateSchema.setChargeType("99");  
     tLAConRateSchema.setAgentCom(tAgentCom[i-1]);
     tLAConRateSchema.setGrpContNo(tGrpContNo[i-1]);
     tLAConRateSchema.setRiskCode(tRiskCode[i-1]);
		 tLAConRateSchema.setManageCom(tManageCom[i-1]);		            
     tLAConRateSchema.setRate(tRate[i-1]); 
     
     tLAConRateSchema.setBranchType("04");//后台程序处理时会进行渠道转换
     
     System.out.println("ItemIdx:"+tLAConRateSchema.getRiskCode());
     tLAConRateSet.add(tLAConRateSchema);    
  }
  
  System.out.println("transact"+transact); 
  System.out.println("end 档案信息...");
  
 	// 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(tLAConRateSet);
  try
  {
    tLAContRateUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLAContRateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
