<%
//程序名称：LAWageRadixSetInit.jsp
//程序功能：
//创建日期：2003-10-16 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('AreaType').vlaue='';  
    fm.all('WageCode').vlaue='';  
    fm.all('AgentGrade').vlaue='';  
    fm.all('BranchType').vlaue='';                    
  }
  catch(ex)
  {
    alert("在LAWageRadixSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}





function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
      
    iArray[1]=new Array();
    iArray[1][0]="地区类型"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=1;              //是否允许输入,1表示允许,0表示不允许
          
      
    iArray[2]=new Array();
    iArray[2][0]="展业类型"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
            
    
    iArray[3]=new Array();
    iArray[3][0]="代理人职级"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=1;              //是否允许输入,1表示允许,0表示不允许    
   
      
    iArray[4]=new Array();
    iArray[4][0]="奖金代码"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许
    

    iArray[5]=new Array();
    iArray[5][0]="奖金名称"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[6]=new Array();
    iArray[6][0]="FYC上限"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许    
               
       
    iArray[7]=new Array();
    iArray[7][0]="FYC下限"; //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
       
    iArray[8]=new Array();
    iArray[8][0]="提取比例"; //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
   
    iArray[9]=new Array();
    iArray[9][0]="发放年数/月数"; //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    
    
    iArray[10]=new Array();
    iArray[10][0]="奖金金额"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    
    
    iArray[11]=new Array();
    iArray[11][0]="发放时间起期"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[12]=new Array();
    iArray[12][0]="发放时间止期"; //列名
    iArray[12][1]="80px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=1;              //是否允许输入,1表示允许,0表示不允许   
    
    iArray[13]=new Array();
    iArray[13][0]="其它提取比例"; //列名
    iArray[13][1]="80px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=1;              //是否允许输入,1表示允许,0表示不允许  
    
    iArray[14]=new Array();
    iArray[14][0]="保费标准"; //列名
    iArray[14][1]="80px";        //列宽
    iArray[14][2]=100;            //列最大值
    iArray[14][3]=1;              //是否允许输入,1表示允许,0表示不允许   
     
    iArray[15]=new Array();
    iArray[15][0]="育成类型"; //列名
    iArray[15][1]="80px";        //列宽
    iArray[15][2]=100;            //列最大值
    iArray[15][3]=1;              //是否允许输入,1表示允许,0表示不允许   
    
    iArray[16]=new Array();
    iArray[16][0]="顺序号"; //列名
    iArray[16][1]="0px";        //列宽
    iArray[16][2]=100;            //列最大值
    iArray[16][3]=2;              //是否允许输入,1表示允许,0表示不允许   
    
    
        
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
	SetGrid.mulLineCount = 0;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenPlus=1;
    SetGrid.hiddenSubtraction = 0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAWageRadixSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LAWageRadixSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>