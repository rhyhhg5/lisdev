<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
 GlobalInput globalInput = (GlobalInput)session.getValue("GI");
 String strManageCom = globalInput.ComCode;
 String updateFlag = request.getParameter("UpdateFlag");
%>
<script language="JavaScript">
var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
//var tmanagecom =" 1 and   char(length(trim(comcode)))<=#4# ";
// var StrSql=" 1 and riskcode  in ( #330801#,#331801#,#332701#,#333901#) ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  { 
	fm.all('AgentCom').value='';
	fm.all('Name').value='';
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('CurYear').value=''; 
    fm.all('Rate').value='';  
    fm.all('ManageCom').value='';
    fm.all('ManageComName').value='';
    fm.all('F05Code').value='';
    fm.all('F03').value='';
    fm.all('F05Name').value='';
    fm.diskimporttype.value='LARateCommision';
    fm.BranchType.value=getBranchType();
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
    //alert(fm.all('branchtype2').value);  
  }
  catch(ex)
  { 
    alert("在LAARiskSalesPackageRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    
    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="100px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
    iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[1][4]="RiskName";              	        //是否引用代码:null||""为不引用
	iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	iArray[1][6]="0|1";
	iArray[1][9]="险种编码|code:RiskName&notnull";  
//	iArray[1][15]="1";
//    iArray[1][16]=StrSql;
  
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    
    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[3][4]="ComCode";
    iArray[3][5]="3|4";
    iArray[3][6]="0|1";
    iArray[3][9]="管理机构|NotNull&NUM&len<=8";    
    iArray[3][15]="1";
    iArray[3][16]=tmanagecom;           
    
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构名称"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[5]=new Array();
    iArray[5][0]="代理机构"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[5][4]="agentcom3";
    iArray[5][5]="5|6";
    iArray[5][6]="0|1";
    iArray[5][9]="代理机构|NotNull";    
    iArray[5][15]="managecom";
    iArray[5][17]="3";           
    
    
    iArray[6]=new Array();
    iArray[6][0]="代理机构名称"; //列名
    iArray[6][1]="100px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="保费类型"; //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[7][9]="保费类型|NotNull";
    iArray[7][10]="F01Code";
    iArray[7][11]="0|^01|基本保费|^02|额外保费|^03|追加保费|";
     
    iArray[8]=new Array();
    iArray[8][0]="缴费年期"; //列名
    iArray[8][1]="100px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许    
  
    
    iArray[9]=new Array();
    iArray[9][0]="保险期间"; //列名
    iArray[9][1]="100px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=3;              //是否允许输入,1表示允许,0表示不允许 
   
    
    iArray[10]=new Array();
    iArray[10][0]="续年度服务津贴率"; //列名
    iArray[10][1]="100px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许 
	
     
    iArray[11]=new Array();
    iArray[11][0]="保单年度"; //列名
    iArray[11][1]="100px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[12]=new Array();
    iArray[12][0]="缴费方式"; //列名
    iArray[12][1]="100px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[12][9]="缴费方式|NotNull";
    iArray[12][10]="PayintvCode";
    iArray[12][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";
    
    iArray[13]=new Array();
    iArray[13][0]="序号"; //列名
    iArray[13][1]="0px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=3; 
   
    
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =0;
    SetGrid.hiddenPlus=0;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAARiskSalesPackageRateInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
  }
  catch(re)
  {
    alert("在LAARiskSalesPackageRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>