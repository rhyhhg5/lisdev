<%
//程序名称：LAAgentPromRadixQuery.js
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  //$initfield$
    fm.all('AgentGrade').value='';  
    fm.all('AreaType').value = '';
    fm.all('AssessType').value = '';
    fm.all('DestAgentGrade').value = '';
    fm.all('limitPeriod').value = '';
    
    fm.all('IndCustSum').value = '';
    fm.all('IndFYCSum').value = '';
    fm.all('MonAvgCust').value = '';  
    fm.all('MonAvgFYC').value = '';
    fm.all('IndRate').value = '';
    fm.all('DirAddCount').value = '';  
    fm.all('AddCount').value = '';
    fm.all('ReachGradeCount').value = '';
    fm.all('IndAddFYCSum').value = '';  
    fm.all('TeamAvgRate').value = '';
    fm.all('DirMngCount').value = '';
    fm.all('DirMngFinaCount').value = '';  
    fm.all('TeamFYCSum').value = '';
    fm.all('DRTeamCount').value = '';
    fm.all('DRTeamAvgRate').value = '';     
    fm.all('DRFYCSum').value = ''; 
    fm.all('RearTeamSum').value = ''; 
    fm.all('DRMonLabor').value = ''; 
    fm.all('DepFYCSum').value = ''; 
    fm.all('DepAvgRate').value = ''; 
    fm.all('RearDepCount').value = ''; 
    fm.all('DRDepLabor').value = ''; 
    fm.all('DRDepAvgRate').value = ''; 
    fm.all('RearDepSum').value = ''; 
    fm.all('DirDepMonAvgFYC').value = ''; 
    fm.all('DepFinaCount').value = '';      
    fm.all('AreaRate').value = '';  
    fm.all('DirRearAdmCount').value = '';  
    fm.all('MngAreaFinaCount').value = '';  
    fm.all('ReqDepCount').value = '';  
    
    fm.all('BranchType').value = top.opener.fm.BranchType.value;
  }
  catch(ex)
  {
    alert("在LAAgentPromRadixQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAAgentPromRadixQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initLAAgentPromRadixGrid();
  }
  catch(re)
  {
    alert("LAAgentPromRadixQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化LAAgentPromRadixGrid
 ************************************************************
 */
function initLAAgentPromRadixGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理人职级";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="地区类型";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="考核类型";         //列名
        iArray[3][1]="60px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="目标职级";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="考核期限";         //列名
        iArray[5][1]="80px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
  
        LAAgentPromRadixGrid = new MulLineEnter( "fm" , "LAAgentPromRadixGrid" ); 

        //这些属性必须在loadMulLine前
        LAAgentPromRadixGrid.mulLineCount = 0;   
        LAAgentPromRadixGrid.displayTitle = 1;
        LAAgentPromRadixGrid.locked=1;
        LAAgentPromRadixGrid.canSel=1;
        LAAgentPromRadixGrid.canChk=0;
        LAAgentPromRadixGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化LAAgentPromRadixGrid时出错："+ ex);
      }
    }


</script>