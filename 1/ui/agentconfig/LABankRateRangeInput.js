//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage1 = new turnPageClass();
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
 
  if (verifyInput() == false)
  {
     
    return false;
  }
   
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  //	 fm.all('ArchieveNo').value=ArchieveNo;
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //initArchieveGrid();
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    initForm();
  }
}

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	submitForm(); //提交

}

//提交，保存按钮对应操作
function DoInsert()
{
	 
	fm.fmAction.value = "INSERT";
	 
	if(!beforeSubmit()) return false;
	 
	 
	submitForm(); //提交

}

function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	submitForm(); //提交
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAArchieve.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
 //提交前的校验、计算
function beforeSubmit()
{
	//alert(isNaN(111));
	
	if(!chkMulLine()) return false;
	return true;
}    
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 提交画面前的验证
function doCheck()
{
	tRow = ArchieveGrid.mulLineCount;
	if(tRow == 0)
	{
		alert("请加入档案信息！");
		return false;
	}
	
	return true;
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  if (verifyInput() == false)
  {
    return false;
  }
	
	if(doCheck() == false)
	{
		return false;
	}
	
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function getname()
{
 if(fm.all('AgentCode').value!="" && !isNumeric(fm.all('AgentCode').value))
 {
 	alert("营销员编码不符合规则！");
 	return false;
 }

  var strsql="select name from laagent where 1=1 "
            + getWherePart('AgentCode','AgentCode')
            + getWherePart('BranchType','BranchType')	
	    + getWherePart('BranchType2','BranchType2');
  var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
  
  if (!strQueryResult)
  {
    alert("没有此代理人！");

    fm.all('AgentCode').value  = '';
    fm.all('AgentCodeName').value  = '';
    return;
  }
  //查询成功则拆分字符串，返回二维数组
   
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);	    
  fm.all('AgentCodeName').value=tArr[0][0];
}

function easyQueryClick() 
{ 
  
  var tRiskCode = fm.all("RiskCode").value;
  var tManageCom = fm.all("ManageCom").value;
  initArchieveGrid1();  
  var tReturn = getManageComLimitlike("ManageCom");
 //此处书写SQL语句			     			    
  var strSql = "select idx,riskcode,(select riskname from lmriskapp b where b.riskcode=a.riskcode),rate*100,managecom  from  laratecommision  a where 1=1 and managecom<>'86'"
  + tReturn
  + getWherePart('ManageCom', 'ManageCom','like')
  + getWherePart('RiskCode', 'RiskCode')
  + getWherePart('BranchType', 'BranchType')
  + getWherePart('BranchType2', 'BranchType2');     
  turnPage1.queryModal(strSql, ArchieveGrid1); 
}            

						//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{
   //alert("enter chkmulline");
   var i;
   var selFlag = true;
   var iCount = 0;
   var rowNum = ArchieveGrid1.mulLineCount;
   for(i=0;i<rowNum;i++)
   {
   	if(ArchieveGrid1.getChkNo(i))
   	{
   	    
   	    iCount++;
   	    if((ArchieveGrid1.getRowColData(i,2) == null)||(ArchieveGrid1.getRowColData(i,2) == ""))
   	    {
   	    	alert("险种代码不能为空");
   	    	ArchieveGrid1.setFocus(i,2,ArchieveGrid1);
   	    	selFlag = false;
   	    	break;
   	    }
   	    
   	     
   	    if((ArchieveGrid1.getRowColData(i,4) == null)||(ArchieveGrid1.getRowColData(i,4) == ""))
   	    {
   	    	alert("佣金比率不能为空");
   	    	ArchieveGrid1.setFocus(i,4,ArchieveGrid1);
   	    	selFlag = false;
   	    	break;
   	    }   	               
   	    //如果Serialno为空不能删除和更新
	    if((ArchieveGrid1.getRowColData(i,1) == null)||(ArchieveGrid1.getRowColData(i,1)==""))
	    {
	    	if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
	    	{
	    	    alert("必须先查询出记录，再修改和删除！");
	    	    ArchieveGrid1.checkBoxAllNot();
	    	    ArchieveGrid1.setFocus(i,2,ArchieveGrid1);
	    	    selFlag = false;
	    	    break;
	    	}
	    }
   	    else if ((ArchieveGrid1.getRowColData(i,1) != null)||(ArchieveGrid1.getRowColData(i,1)!=""))
   	    {	//如果Serialno不为空不能插入
   	    	if(fm.fmAction.value == "INSERT")
   	    	{
   	    	    alert("此纪录已存在，不可插入！");
   	    	    ArchieveGrid1.checkBoxAllNot();
   	    	    ArchieveGrid1.setFocus(i,2,ArchieveGrid1);
   	    	    selFlag = false;
   	    	    break;
   	    	}
   	    }
   	    if(!isNumeric(ArchieveGrid1.getRowColData(i,4)))
   	    {
   	       alert("佣金比例不是有效的数字！");
   	       ArchieveGrid1.checkBoxAllNot();
   	       ArchieveGrid1.setFocus(i,4,ArchieveGrid1);
   	       selFlag = false;
   	        break;
   	     }
   	    var tReturn = getManageComLimitlike("ComCode");
   	    tManageCom=ArchieveGrid1.getRowColData(i,5);
   	    var tSql="select comcode from  ldcom  where  1=1 and Sign='1' and comcode like '"+tManageCom+"%' " 
   	    + tReturn;
   	    var strQueryResult = easyQueryVer3(tSql, 1, 1, 1);
   	    if(!strQueryResult)
   	    {
   	       alert("管理机构"+tManageCom+"输入有误,请查阅或双击输入框选择！");
   	       return false;
   	    	
   	    }
            
   	 }
   	 else
   	 {//选中的行
///	    if((ArchieveGrid1.getRowColData(i,1) == null)||(ArchieveGrid1.getRowColData(i,1)==""))
///	    {
///	    	alert("有未保存的新增纪录！请先保存记录。");
///	    	ArchieveGrid1.checkBoxAllNot();
///	    	ArchieveGrid1.setFocus(i,1,ArchieveGrid1);
///	    	selFlag = false;
///	    	break;
///	    }
   	 }
   	 
    }    
  
    if(!selFlag) return selFlag;
    if(iCount == 0)
    {
       	alert("请选择要保存或删除修改的记录!");
       	return false
    }
    return true;
}