//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mAction="";
var ttWageCode="";
var ttAgentGrade="";

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}



/*********************************************************************
 *  保存指标设置信息的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
*/
function submitForm()
{
	var showStr = " 	正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;	 
        if (!verifyInput())
          return false;
          
	 if( mAction == "" )
	 {
	    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		mAction = "INSERT";
		fm.all( 'fmAction' ).value = mAction;
		mAction = "";
		fm.submit(); //提交
	}
}

/*********************************************************************
 *  保存提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
*/
function afterSubmit( FlagStr, content)
{
   showInfo.close();
   if (FlagStr == "Fail")
   {
  	   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
       	   showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else
   {
   	  content = "保存成功！";
      var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	  showDiv(operateButton,"true");
	  showDiv(inputButton,"false");
   }
   if (mAction == "INSERT")
   {
   	ttWageCode = fm.all('WageCode').value;
   	ttAgentGrade = fm.all('AgentGrade').value;	
   }
   mAction = "";
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
   try
   {
   	   initForm();
   }
   catch(re)
   {
   	   alert("在CommVsIndexInput.js-->resetForm函数中发生异常:初始化界面错误!");
   }
}


/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}


/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	showDiv( operateButton, "false" );
	showDiv( inputButton, "true" );
}


/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
		showInfo = window.open("./CommIndexQuery.html");
}


/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
   var tWageCode = "";
   var tAgentGrade = "";
   tWageCode = fm.all( 'WageCode' ).value;
   tAgentGrade = fm.all( 'AgentGrade' ).value;
   if (tWageCode != ttWageCode||tAgentGrade!=ttAgentGrade)
       {
           alert("您不能修改佣金代码和代理人职级!");
           fm.all('WageCode').value = ttWageCode;
           fm.all('AgentGrade').value = ttAgentGrade;
       }
   else
   {
         if( tWageCode == null || tWageCode == "" || tAgentGrade == null || tAgentGrade == "")
         {
                alert("请先做指标查询，再做修改!");
         }
         else
         {
                 var showStr = "正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	         var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

   	        if (mAction == "")
   	        {
   	              //showSubmitFrame(mDebug);
		      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	              mAction = "UPDATE";
		      fm.all( 'fmAction' ).value = mAction;
		      mAction = "";
		      fm.submit(); //提交
                }
          }
   }
}



/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
	var tWageCode = "";
	var tAgentGrade = "";
	tWageCode = fm.all( 'WageCode' ).value;
	tAgentGrade = fm.all( 'AgentGrade' ).value;
	if( tWageCode == null || tWageCode == "" || tAgentGrade == null || tAgentGrade == "")
		alert( "请先做指标查询操作，再进行删除!" );
	else
	{
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}



/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv( cDiv, cShow )
{
	if( cShow == "true" )
	  cDiv.style.display = "";
  else
		cDiv.style.display = "none";
}


function afterQuery(arrResult)
{
	initForm();
	fm.all('BranchType').value = arrResult[0];	
	fm.all('AgentGrade').value = arrResult[1];
	fm.all('WageCode').value = arrResult[2];
	fm.all('WageName').value = arrResult[3];
	fm.all('IndexCode').value = arrResult[4];
    fm.all('CalCode').value = arrResult[5];
	fm.all('CTableName').value = arrResult[6];
	fm.all('CColName').value = arrResult[7];
	fm.all('WageOrder').value = arrResult[8];
	fm.all('PrintFlag').value = arrResult[9];
	ttWageCode=arrResult[2];
	ttAgentGrade=arrResult[1];		
	emptyUndefined();
}
/** liujw */
function changeAgentGrade()
{
   var tBranchType = fm.BranchType.value;
   if (tBranchType == null || tBranchType == '')
     fm.sql.value = "1 and 1 <> 1";
   else
     fm.sql.value = "1 and codealias = #"+tBranchType+"#";
   return true;
}

function afterCodeSelect( cCodeName, Field )
{
	var value = Field.value;
	
	try	
	{
		if( cCodeName == 'BranchType' )	
		{
			
		  var tBranchType = fm.BranchType.value;
                  if (tBranchType == null || tBranchType == '')
                    fm.sql.value = "1 and 1 <> 1";
                  else
                    fm.sql.value = "1 and codealias = #"+tBranchType+"#";

		}
	}
	catch( ex ) 
	{
		alert('展业类型设置出错!');
	}
}
