<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LaratecommisionSetInput.jsp
//程序功能：佣金基数表设置录入界面
//创建时间：2016-4-27
//创建人  ：zhangxinjing

%>
<%@page contentType="text/html;charset=GBK" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LABRCSetWNDLInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
 
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
      <%@include file="LABRCSetWNInit.jsp"%>
<script>
   var msql2=" 1 and risktype4=#4#";
   var msql="1 and char(length(trim(comcode))) in (#4#,#2#)";
</script>
</head>
 
<body  onload="initForm();initElementtype();" >
 <form action="./LABRCSetWNDLSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
   	<TD  class= title>管理机构</TD>
    <TD  class= input>
      <Input class="codeno" name=ManageCom  verify="管理机构|notnull"
        ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,1,'sign',null,null);" 
        onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,1,'sign',null,null);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
      </TD>
   	<td  class= title> 代理机构 </td>
    <td  class= input> <input class=codeno name=AgentCom 	
         ondblclick="return getAgentCom(this,Name);"
         onkeyup="return getAgentCom(this,Name);" ><input class="codename" name = "Name"  readOnly > </td>
      </TR>       
   	
    <tr class=common>
     <TD  class= title>
       险   种
    </TD>
    <TD  class= input>
       <Input class= 'codeno' name= RiskCode verify="险种|code:RiskCode" 
       ondblclick="return showCodeList('RiskName',[this,RiskCodename],[0,1],null,1,1);" 
            onkeyup="return showCodeListKey('RiskName',[this,RiskCodename],[0,1],null,1,1);"  ><input class=codename name=RiskCodename readonly=true  >
      
    </TD>  	   	
    <TD  class= title>
            保险期间
          </TD>
          <TD  class= input>
            <Input class= "codeno"  name=payyears 
            ondblclick="return showCodeList('payyear',[this,payyearsName],[0,1]);"
            onkeyup="return showCodeList('payyear',[this,payyearsName],[0,1]);"><input class="codename" name=payyearsName readOnly=true>
          </TD>
          </tr>
   <tr class=common>		
          <td  class= title> 保单年度 </td>
          <TD  class= input> <Input class= "common" name=CurYear></TD>
   	
               
           <td  class= title>佣金比率</td>
          <TD  class= input> <Input class= "common" name=Rate></TD>      		

  </tr>
  <tr class=common>		
         <TD  class= title>
            最低缴费年期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=F03Code >
          </TD>
            <TD  class= title>
            缴费年期小于
          </TD>
          <TD  class= input>
            <Input class= "common"  name=F04Code >
          </TD>      		

  </tr>
  <tr class=common>	
  <td  class= title>保费类型</td>
          <TD  class= input> <Input class= "codeno" name=F01Code CodeData="0|^00|正常保费|^01|基本保费|^02|额外保费|^03|追加保费|" ondblclick="return showCodeListEx('F01list',[this,F01Name],[0,1]);" onkeyup="return showCodeListKeyEx('F01list',[this,F01Name],[0,1]);" onchange="" readonly=true><input class=codename name=F01Name  readonly=true ></TD> 	
   <TD  class= title>
                  缴费方式
          </TD>
          <TD  class= input>
             <Input class="codeno" name=PayintvCode  CodeData="0|^0|趸缴|^12|年缴|^6|半年缴|^3|季缴|^1|月缴" 
             ondblclick="return showCodeListEx('payintvlist',[this,PayintvName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('payintvlist',[this,PayintvName],[0,1]);" 
             onchange=""><input class=codename name=PayintvName  readonly=true >
             </TD>  
 
  </tr>
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();">  
    		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 

    	</td>
   
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>佣金计算基数设置
    		<p> 
					<font color="#ff0000">(注：佣金比率必须为0-1的数，如23%，记为0.23!) 
					</font>
				</p>
				</td>
				
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
   <!--table class=common>
    <tr class=common>
     <td class=input colspan=4 align=center><input type=button value="增     加" class=common onclick="addOneRow();">
     </td>
    </tr>
   </table-->      
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">   

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>
