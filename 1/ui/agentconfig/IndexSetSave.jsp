<%
//程序名称：IndexSetSave.jsp
//程序功能：
//创建日期：2003-01-10
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.lang.reflect.*"%>
  <%@page import="java.sql.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>

<%
    System.out.println("------ IndexSetSave.jsp ---------");
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    String tAction = "";
    String tOperate = "";
    int tCount = 0;

    LAAssessIndexSchema tLAAssessIndexSchema = new LAAssessIndexSchema();
    OLAAssessIndexUI tOLAAssessIndexUI = new OLAAssessIndexUI();


    //读取Session中的全局类
    GlobalInput tG = new GlobalInput();    
    tG=(GlobalInput)session.getValue("GI");
    
    tAction = request.getParameter("fmAction");
    String tqryFlag = request.getParameter("qryFlag");
    System.out.println("qryFlag = " + tqryFlag);

    if( tAction.equals("QUERY") && tqryFlag.equals("A") )
	tLAAssessIndexSchema.setIndexCode("");
    else
	tLAAssessIndexSchema.setIndexCode(request.getParameter("IndexCode"));
    tLAAssessIndexSchema.setIndexName(request.getParameter("IndexName"));
    tLAAssessIndexSchema.setBranchType(request.getParameter("BranchType"));
    tLAAssessIndexSchema.setIndexType(request.getParameter("IndexType")); 
    tLAAssessIndexSchema.setIndexSet(request.getParameter("IndexSet"));
    tLAAssessIndexSchema.setIColName(request.getParameter("IColName"));
    tLAAssessIndexSchema.setITableName(request.getParameter("ITableName"));
    tLAAssessIndexSchema.setCalType(request.getParameter("CalType"));
    tLAAssessIndexSchema.setCalCode(request.getParameter("CalCode"));
    tLAAssessIndexSchema.setCalPrpty(request.getParameter("CalPrpty"));

	// 准备传输数据 VData
	VData tVData = new VData();

	tVData.addElement(tLAAssessIndexSchema);
	tVData.add(tG);
	FlagStr = "";


  System.out.println("tAction = " + tAction);

  if(tAction.equals("QUERY"))
  {		
    if( tAction.equals("QUERY") && tqryFlag.equals("A") )
      tAction = "QUERY||INDEXSET";
    else
      tAction = "QUERY||MAIN";
      System.out.println("Query:"+tAction);
	if(!tOLAAssessIndexUI.submitData(tVData,tAction))
	{
		Content = "查询失败，原因是: " 	+  tOLAAssessIndexUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
		System.out.println("Content = " + Content) ;
	}
	else
	{
		 LAAssessIndexSet mLAAssessIndexSet = new LAAssessIndexSet();
		 LAAssessIndexSchema mLAAssessIndexSchema = new LAAssessIndexSchema();
	   tVData.clear();
		 tVData = tOLAAssessIndexUI.getResult();
		//显示

		 mLAAssessIndexSet.set((LAAssessIndexSet)tVData.getObjectByObjectName("LAAssessIndexSet",0));
		 tCount = mLAAssessIndexSet.size();
     System.out.println("----tCount = " + tCount);

		 for(int i=1;i<=tCount;i++)
		 {
		    mLAAssessIndexSchema = mLAAssessIndexSet.get(i);
				System.out.println("----rs----" + mLAAssessIndexSchema.getIndexCode());
			  %>
			  <script language="javascript" >
			     parent.fraInterface.RelaIndexGrid.addOne("RelaIndexGrid");
//			     parent.fraInterface.RelaIndexGrid.setRowColData(<%=i-1%>, 1, "<%=mLAAssessIndexSchema.getBranchType()%>");
//					 parent.fraInterface.RelaIndexGrid.setRowColData(<%=i-1%>, 2, "<%=mLAAssessIndexSchema.getIndexType()%>");
					 parent.fraInterface.RelaIndexGrid.setRowColData(<%=i-1%>, 1, "<%=mLAAssessIndexSchema.getIndexCode()%>");
					 parent.fraInterface.RelaIndexGrid.setRowColData(<%=i-1%>, 2, "<%=mLAAssessIndexSchema.getIndexName()%>");
					 parent.fraInterface.RelaIndexGrid.setRowColData(<%=i-1%>, 3, "<%=mLAAssessIndexSchema.getITableName()%>");
					 parent.fraInterface.RelaIndexGrid.setRowColData(<%=i-1%>, 4, "<%=mLAAssessIndexSchema.getIColName()%>");
					 parent.fraInterface.RelaIndexGrid.setRowColData(<%=i-1%>, 5, "<%=mLAAssessIndexSchema.getIndexSet()%>");
				</script>
				<%
		 }//end for
	}

	if(!FlagStr.equals("Fail"))
	{
		 System.out.println("----tCount = " + tCount);
		 if(tCount==0)
		    Content = "没有相关数据!";
	   else
 	      Content = "查询成功!";
		 FlagStr = "Succ";
	}
	}// end if (tAction="QUERY")
	else
	{
		 /*
	   if(tAction.equals("DELETE"))
		 {
		    LAAssessIndexSchema tLAAssessIndexSchema;
				tLAAssesssIndexSchema.setIndexCode(request.getParameter("Indexcode"));
        tVData.clear();
				tVData.addElement(tLAAssessIndexSchema);
				tVData.add(tG);
		 }
     */
		 if( tAction.equals( "INSERT" ) ) tOperate = "INSERT||MAIN";
		 if( tAction.equals( "UPDATE" ) ) tOperate = "UPDATE||MAIN";
		 if( tAction.equals( "DELETE" ) ) tOperate = "DELETE||MAIN";
                 
		 if(!tOLAAssessIndexUI.submitData(tVData,tOperate))
		 {
			  int eCount = tOLAAssessIndexUI.mErrors.getErrorCount();
			  if( tAction.equals( "INSERT" ) ) Content = "保存失败,原因是: " ;
		          if( tAction.equals( "UPDATE" ) ) Content = "保存失败,原因是: " ;
		          if( tAction.equals( "DELETE" ) ) Content = "删除失败,原因是: " ;
                          Content += tOLAAssessIndexUI.mErrors.getFirstError();
				
				FlagStr = "Fail";
		 }
		 else
		 {
			  if( tAction.equals( "INSERT" ) ) Content = "保存成功!" ;
		          if( tAction.equals( "UPDATE" ) ) Content = "保存成功!" ;
		          if( tAction.equals( "DELETE" ) ) Content = "删除成功!" ;
				FlagStr = "Succ";
		 }
	}
%>
<html>
   <script language="javascript">
      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
   </script>
</html>










