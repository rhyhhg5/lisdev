<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<% 
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and  char(length(trim(comcode))) = #8# and comcode<>#86000000# ";
//var tmanagecom =" 1 and   char(length(trim(comcode)))<=#4# ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('CurYear').value=''; 
    fm.all('PayintvCode').value='';
    fm.all('PayintvName').value='';
    fm.all('StartDay').value='';
    fm.all('EndDay').value='';
    fm.all('Rate').value='';  
    fm.all('F01Code').value='';
    fm.all('F01CodeName').value='';
    fm.all('ManageCom').value='';
    fm.all('ManageComName').value='';
    fm.all('payyears').value='';
    fm.all('payyearsName').value='';
    fm.diskimporttype.value='LARateCommision';
    fm2.diskimporttype.value='LARateCommision';  
    fm.BranchType.value=getBranchType();
    fm2.BranchType.value=getBranchType();
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
    fm.branchtype2.value="0"+<%= BranchType2%>;
    fm2.branchtype2.value="0"+<%= BranchType2%>;
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
    alert("在LAPerformanceAwardRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initInpBox1()
{
  try
  {
	  fm2.all('queryType').value ="";
	  fm2.all('queryName').value="";
	  fm2.all('FileImportNo').value="";
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
  
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
	  alert("在LAPerformanceAwardRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="100px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
    iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[1][4]="riskacname";              	        //是否引用代码:null||""为不引用
	iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	iArray[1][6]="0|1";
	iArray[1][9]="险种编码|code:RiskCode&notnull";  
	//iArray[1][15]="1";
    //iArray[1][16]=triskcode;

    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[3]=new Array();
    iArray[3][0]="保单年度"; //列名
    iArray[3][1]="100px";        //列宽
    iArray[3][2]=30;            //列最大值
    iArray[3][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[3][9]="保单年度|NotNull&NUM";
    
    iArray[4]=new Array();
    iArray[4][0]="缴费方式"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[4][9]="缴费方式|NotNull";
    iArray[4][10]="PayintvCode";
    iArray[4][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";
     
    iArray[5]=new Array();
    iArray[5][0]="绩效比率"; //列名
    iArray[5][1]="100px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[5][9]="绩效比率|NotNull&NUM";
     
    iArray[6]=new Array();
    iArray[6][0]="销售渠道"; //列名
    iArray[6][1]="100px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许 
	iArray[6][9]="销售渠道|NotNull";
    iArray[6][10]="branchtype3";
    iArray[6][11]="0|^1|互动直销|^2|互动开拓";
    
    iArray[7]=new Array();
    iArray[7][0]="最低缴费年期"; //列名
    iArray[7][1]="100px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[7][9]="最低缴费年期|NotNull&NUM"; 
     
    iArray[8]=new Array();
    iArray[8][0]="最高缴费年期"; //列名
    iArray[8][1]="100px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[8][9]="最高缴费年期|NotNull&NUM"; 
    
    iArray[9]=new Array();
    iArray[9][0]="管理机构"; //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[9][4]="ComCode";
    iArray[9][5]="9|10";
    iArray[9][6]="0|1";
    iArray[9][9]="管理机构|code:comcode&NotNull&NUM&len=8";    
    iArray[9][15]="1";
    iArray[9][16]=tmanagecom;
      
    iArray[10]=new Array();
    iArray[10][0]="管理机构名称"; //列名
    iArray[10][1]="0px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="序号"; //列名
    iArray[11][1]="0px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="保险期间"; //列名
    iArray[12][1]="80px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[12][4]="payyear"; 
    iArray[12][5]="12";  
   
    
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    SetGrid.hiddenPlus=0;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAPerformanceAwardRateInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	  iArray[1][0]="导入文件批次";          		//列名
	  iArray[1][1]="120px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAPerformanceAwardRateInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    initImportResultGrid();
  }
  catch(re)
  {
    alert("在LAPerformanceAwardRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>