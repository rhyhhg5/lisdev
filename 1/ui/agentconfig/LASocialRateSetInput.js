//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2009-01-13
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var queryFlag = false;
var strSQL ="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
if(showInfo!=null)
{
  try
  {
    showInfo.focus();  
  }
  catch(ex)
  {
    showInfo=null;
  }
}
}
//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(queryFlag ==false) 
	{
		alert("请先查询");
		return false;	
	}
	if(!beforeSubmit()) return false;
	if(!SetGrid.checkValue("SetGrid")) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initSetGrid();
	var tManageCom  = fm.all('ManageCom').value;
	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请输入管理机构！");
		return;
	}
	// 书写SQL语句
	strSQL = "select a.riskcode,b.riskname,nvl(a.Curyear,0),trim(a.payintv),nvl(a.rate,0)," +
			"a.F03,a.F04,a.ManageCom,(select name from ldcom where comcode=a.managecom),a.idx "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.riskcode = b.riskcode  and a.ManageCom is not null "
	+getWherePart('a.branchtype','BranchType')
	+getWherePart('a.branchtype2','branchtype2')
	+ getWherePart( 'a.riskcode','RiskCode')
	+ getWherePart( 'a.PayIntv','PayintvCode')
	+ getWherePart( 'a.ManageCom','ManageCom','like')
	+ getWherePart( 'a.curyear','CurYear','','1')
	+ getWherePart( 'a.f03','StartDay','','1')
	+ getWherePart( 'a.f04','EndDay','','1')
	+ " Order by a.managecom ,a.riskcode,a.f01,a.f02 ,a.curyear";
 
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
	queryFlag = true;
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
	//查询成功则拆分字符串，返回二维数
	//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数
	var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
	//return true;
}
				
				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{
	
	fm.fmAction.value = "UPDATE";
	if(queryFlag ==false) 
	{
		alert("请先查询");
		return false;	
	}
	if(!beforeSubmit()) return false;
	if (confirm("您确实想修改该记录吗?"))
	{
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
		showInfo.close();
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}
							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{
	//alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(SetGrid.getChkNo(i))
		{
			iCount++;
			if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
			{
				alert("险种代码不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}

			if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3) == ""))
			{
				alert("保单年度不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,4) == null)||(SetGrid.getRowColData(i,4) == ""))
			{
				alert("缴费方式不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
			{
				alert("提奖比例不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}

			if(SetGrid.getRowColData(i,5)<'0'||SetGrid.getRowColData(i,5)>'1')
			{
				alert("提奖比例不在0到1之间!");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,6) == null)||(SetGrid.getRowColData(i,6) == ""))
			{
				alert("最低缴费年期不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
			{
				alert("最高缴费年期不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if(SetGrid.getRowColData(i,6)-SetGrid.getRowColData(i,7)>0)
			{
				alert("最低缴费年期不能晚于最高缴费年期！");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,8) == null)||(SetGrid.getRowColData(i,8) == ""))
            {
	            alert("管理机构不能为空");
	            SetGrid.setFocus(i,1,SetGrid);
	            selFlag = false;
	            break;
            }
			//一些险种的提奖比例上限校验
			if(SetGrid.getRowColData(i,1)=="170401"){
				if(SetGrid.getRowColData(i,7)<=2){
					var tArr1='0.003';
				}else{
					var tArr1='0.005';
				}
			}else if(SetGrid.getRowColData(i,1)=="270101"){
				if(SetGrid.getRowColData(i,6)<=5 &&SetGrid.getRowColData(i,7)>5){
					var tArr1='0.07';
				}
				if(SetGrid.getRowColData(i,6)<=10 &&SetGrid.getRowColData(i,7)>10){
					var tArr1 = '0.1';
				}
				if(SetGrid.getRowColData(i,6)<=15 &&SetGrid.getRowColData(i,7)>15){
					var tArr1='0.11';
				}
				if(SetGrid.getRowColData(i,6)<=20 &&SetGrid.getRowColData(i,7)>20){
					var tArr1='0.12';
				}
			}else if(SetGrid.getRowColData(i,1)=="280101"&&
					((SetGrid.getRowColData(i,6)<=3 &&SetGrid.getRowColData(i,7)>3)||(SetGrid.getRowColData(i,6)<=5 &&SetGrid.getRowColData(i,7)>5))){
				if(SetGrid.getRowColData(i,4)=="0"||SetGrid.getRowColData(i,3)>1){
					var tArr1= '0.025';
				}
				if(SetGrid.getRowColData(i,4)!="0"&&SetGrid.getRowColData(i,3)=="1"){
					var tArr1='0.09';
				}
			}else{
				//部分分公司 部分险种提奖比例上限特殊处理
				var tcodetype = 'SocialRiskCode'+SetGrid.getRowColData(i,8).substring(0,4);
				var rateSql= "select comcode from ldcode where codetype ='"+tcodetype+"' and code='"+SetGrid.getRowColData(i,1)+"' and codealias=substr('"+SetGrid.getRowColData(i,8)+"',1,othersign)";
	            var tArr1  = easyExecSql(rateSql);
	            if(tArr1==null||tArr1==""){
	            	 tcodetype = 'RiskCode'+SetGrid.getRowColData(i,8).substring(0,8);
	            	 rateSql="select comcode from ldcode where codetype = '"+tcodetype+"' and code='"+SetGrid.getRowColData(i,1)+"' and codealias=substr('"+SetGrid.getRowColData(i,8)+"',1,othersign)";
	            	 tArr1 =  easyExecSql(rateSql);
	            }
	            if(tArr1==null||tArr1==""){
	            	 rateSql="select comcode from ldcode where codetype = 'SocialRiskCodeRate' and code='"+SetGrid.getRowColData(i,1)+"'";
	            	 tArr1 =  easyExecSql(rateSql);
	            }
	      
			}
            if(tArr1!=null){
	            if((SetGrid.getRowColData(i,5)-tArr1)>0){
	            	alert("无法录入该险种提奖比例，上限为:"+tArr1);
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
	            }
            }
  			//如果Serialno为空不能删除和更新
			if((SetGrid.getRowColData(i,10) == null)||(SetGrid.getRowColData(i,10)==""))
			{
				if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
			else if ((SetGrid.getRowColData(i,10) !== null)||(SetGrid.getRowColData(i,10)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.fmAction.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((SetGrid.getRowColData(i,10) == null)||(SetGrid.getRowColData(i,10)==""))
				{
					alert("有未保存的新增纪录！请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		} 
		if(!selFlag) return selFlag;
	    if(iCount == 0)
		{
	    	var title="";
	    	if(fm.fmAction.value=="UPDATE")
	    	{
	    		title="修改";
	    	}
	    	if(fm.fmAction.value=="DELETE")
	    	{
	    		title="删除";
	    	}
	    	if(fm.fmAction.value=="INSERT")
	    	{
	    		title="新增";
	    	}
			alert("请选择要"+title+"的记录!");
			return false
		}
		return true;
}
//提交前的校验、计算
function beforeSubmit()
{
	
	if(!verifyInput()) { 
		return false; 
		} 
	if(!SetGrid.checkValue("SetGrid")) return false;
//	alert(SetGrid.CheckValue());
	if(!chkMulLine()) return false;
	return true;
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}


//执行下载操作
function DoNewDownload(){
	if(queryFlag ==false) 
	{
		alert("请先查询");
		return false;	
	}
	fm.querySql.value = strSQL;
	var oldAction = fm.action;
	fm.action="LASocialRateSetDownload.jsp";
	fm.submit();
    fm.action = oldAction;
}
