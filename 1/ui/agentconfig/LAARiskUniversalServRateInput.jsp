<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：佣金基数表设置录入界面
//创建时间：2014-12-1
//创建人  ：yangyang
// above is miaoxiangzheng's new_add
String BranchType2=request.getParameter("BranchType2");
if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">
//    var msql2=" 1 and riskcode   in ( #330801#,#331801#,#332701#,#333901#) ";
    var msql=" 1 and   char(length(trim(comcode)))<=#8# ";
</script>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAARiskUniversalServRateInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="LAARiskUniversalServRateInit.jsp"%>
    <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>
   
<body  onload="initForm();initElementtype();" >
 <form action="LAARiskUniversalServRateSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
    	<TD  class= title>
            管理机构
    </TD>
      <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,'1',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,'1',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary > 
          </TD>  
           <td class=title>
     代理机构
     </td>
          <td class= input>
     	<input class=codeno name=AgentCom 	
         ondblclick="return getAgentCom(this,Name);"
         onkeyup="return getAgentCom(this,Name);" 
         ><input class="codename" name = "Name"  readOnly > 
    </td> 
          <TD class=input>
   </tr>
  
   <tr class=common> 
          <td  class= title> 
          保单年度 
          </td>
          <TD  class= input> 
          <Input class= "common" name=CurYear>
          </TD>
          <td  class= title>
   		缴费年期
   		</td>
          <TD  class= input> 
          <Input class= "common" name=F03>
          </TD>
   </tr>
    <tr>
      <td class=title>续年度服务津贴率
      </td>
       <td class= input>
         <input class="common" name = Rate>
       </td>
       <td class=title>保费类型</td>
       <TD class=input>
		  <Input class="codeno" name=F05Code
			  CodeData="0|^01|基本保费|^02|额外保费|^03|追加保费|"
			  ondblclick="return showCodeListEx('F05list',[this,F05Name],[0,1]);"
			  onkeyup="return showCodeListKeyEx('F05list',[this,F05Name],[0,1]);"
			  onchange=""><input class=codename name=F05Name readonly=true>
	 </TD>
   </tr>
   <tr>
      <td class=title>
     险种
     </td>
     <td class= input>
     	<Input class=codeno name=RiskCode verify="险种|code:RiskCode"
            ondblclick="return showCodeList('RiskName',[this,RiskCodename],[0,1],null,null,null);" 
            onkeyup="return showCodeListKey('RiskName',[this,RiskCodename],[0,1],null,null,null);"  ><input class=codename name=RiskCodename readonly=true  >
     </td>         
          
   </tr>
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="修  改" class=cssButton onclick="return DoUpdate();">    
    		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 
    	</td>
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>佣金计算基数设置</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>
  <br><hr>
  <Input class="readonly" type=hidden name=diskimporttype>
  <Input type=hidden name=branchtype2> 
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">  
  <input type=hidden class=Common name=querySql > 
  </form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




