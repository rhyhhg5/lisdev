<html> 
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="CommVsIndexInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CommVsIndexSetInit.jsp"%>
  
</head>
<body  onload="initForm();" >
  <form action="CommVsIndexSetSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>   
    <table>
      <tr>
      <td>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCommIndexSet);">
      </td>
      <td class= titleImg>
        佣金指标设置信息
      </td>
    	</tr>
    </table>
    <Div  id= "divCommIndexSet" style= "display: ''">
    <table class=common>
        <TR  class= common>
          <TD  class= title> 展业类型 </TD>
          <td class="input">
           <input class=code name=BranchType verify = "展业类型|notnull&code:branchType"
                             ondblclick="return showCodeList('BranchType',[this]);" 
                             onkeyup="return showCodeListKey('BranchType',[this]);"
                             onchange="return changeAgentGrade();">
          </td>        
          <TD  class= title>代理人职级</TD>
          <TD  class= input>
             <input class=code name=AgentGrade verify = "代理人职级|notnull&code:AgentGrade" 
                                ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,sql,'1');" 
                                onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,sql,'1');">
          </TD>
        </TR>
       <tr>
        <td class=title>佣金代码</td>
        <td class=input> <input class=common name=WageCode verify="佣金代码|notnull&len<=6">
        </td>
        <td class=title>佣金名称</td>
        <td class=input> <input class=common name=WageName verify="佣金名称|notnull&len<=20">
        </td>
       </tr>
      
       <tr>
        <td class=title>佣金对应表名</td>
        <td class=input> <input class="readonly"readonly name=CTableName value="LAWage" >
        </td>
        <td class=title>佣金对应字段名</td>
        <td class=input> <input class=common name=CColName >
        </td>
      </tr>
      
      <tr>
       <td class=title>SQL语句编码</td>      
       <td class=input> <input class=common name="CalCode" >
       </td>
       
       <td class=title>对应指标编码</td>
       <td class=input > <input class=common name="IndexCode" >
       </td>
      </tr>
      <tr>
       <td class=title>打印顺序</td>      
       <td class=input> <input class=common name="WageOrder" verify="打印顺序|INT">
       </td>
       
       <td class=title>打印标记</td>
       <td class=input >
        <input  name=PrintFlag class='code' CodeData="0|2^0|不统计^1|重新计算"
                                ondblclick="return showCodeListEx('PrintFlag',[this],[0,1]);" 
                                onkeyup="return showCodeListKeyEx('PrintFlag',[this],[0,1]);">
          
       </td>
      </tr>
    </Table>	 
    </div>
    <input type=hidden name=sql value="1 and 1 <> 1">
    <input type=hidden id="fmAction" name="fmAction">
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
