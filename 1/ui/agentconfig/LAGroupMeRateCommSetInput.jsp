<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LaratecommisionSetInput.jsp
//程序功能：佣金基数表设置录入界面
//创建时间：2009-2-22
//创建人  ：miaoxz

%>   
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">

var tsql=" 1 and char(length(trim(comcode))) in (#8#,#4#,#2#) ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LAGroupMeRateCommSetInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <SCRIPT src="LAGroupMeRateCommSetInput.js"></SCRIPT>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LAGroupMeRateCommSetSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
    	
    	
    	 <TD  class= title>
            管理机构
   </TD>
   <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary > 
   </TD> 
    	
     <TD  class= title>
       险种
    </TD>
	<TD  class= input>
       <Input class= 'codeno' name= RiskCode verify="险种|code:riskcode" 
       ondblclick="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,StrSql,1);"
       onkeyup="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,StrSql,1);"
      ><Input class=codename name=RiskCodename readOnly  > 
    </TD>  	
             
        
                			
   
   </tr>
   <tr>			
   	
       
   				 <TD  class= title>
            保险期间始期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=StartDay >
          </TD>
            <TD  class= title>
            保险期间止期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=EndDay >
          </TD>
   	
   </tr>
   
   
   <TR  class= common> 
    
    
         <td  class= title>提奖比率</td>
          <TD  class= input> <Input class= "common" name=Rate></TD>      
   </TR>

  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();">  
    		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 

    	</td>
   
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>提奖计算基数设置(<font color=red>注意：提奖比率应为0和1之间的数，包括0和1</font>)</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
   <!--table class=common>
    <tr class=common>
     <td class=input colspan=4 align=center><input type=button value="增     加" class=common onclick="addOneRow();">
     </td>
    </tr>
   </table-->      
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">   

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




