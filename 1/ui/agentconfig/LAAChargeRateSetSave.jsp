<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAAChargeWrapRateSave.jsp
//程序功能：
//创建日期：2008-01-24
//创建人  ：huxl
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  String tRiskCode=request.getParameter("RiskCode");

  LAAChargeRateSetUI tLAAChargeRateSetUI = new LAAChargeRateSetUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LARateChargeSet tSetU = new LARateChargeSet();	//用于更新
      LARateChargeSet tSetD = new LARateChargeSet();	//用于删除
      LARateChargeSet tSetI = new LARateChargeSet();//用于插入
            
      String tChk[] = request.getParameterValues("InpSetGridChk"); 
      String mRiskCode[] = request.getParameterValues("SetGrid1");
      String tSerialno[] = request.getParameterValues("SetGrid3");
      String tAgentCom[] = request.getParameterValues("SetGrid6"); 
      String tRate[] = request.getParameterValues("SetGrid11");
      String tStartDate[] = request.getParameterValues("SetGrid12");
      String tEndDate[] = request.getParameterValues("SetGrid13");
      
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{
      		//创建一个新的Schema
      	  LARateChargeSchema tSch = new LARateChargeSchema();
      	  tSch.setOtherNo(tAgentCom[i].trim());
      	  tSch.setOtherNoType("R");
      	  tSch.setRiskCode(mRiskCode[i].trim());
      	  tSch.setStartDate(tStartDate[i]);
      	  tSch.setEndDate(tEndDate[i]);
      	  tSch.setStartYear(0);
      	  tSch.setEndYear(999);
      	  tSch.setPayIntv(0);
      	  tSch.setCalType("51");
      	  tSch.setRate(tRate[i].trim());
      	System.out.println("kankan:"+tSch.getOtherNoType());
      	
      			//需要插入记录
		  if(tAction.trim().equals("INSERT"))      			      	
      	  {
      		tSetI.add(tSch);
      	  }
            //需要删除
          else  if(tAction.trim().equals("DELETE"))      			      	
      	  {	
        	 tSch.setF03(tSerialno[i].trim()); 
      		 tSetD.add(tSch);
      	  }
            //需要更新
          else if(tAction.trim().equals("UPDATE"))      			      	
      	  {   	
        	  tSch.setF03(tSerialno[i].trim()); 
      		  tSetU.add(tSch);
      	  }    		 
   
      	}      	
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if ((tSetD.size() == 0)&&(tSetU.size() == 0)&&(tSetI.size() == 0))
      {
        	FlagStr = "Fail";
        	Content = FlagStr + "未选中要处理的数据！";      	
      }
    	else if (tSetI.size() != 0)
    	{
        	//只有插入数据
        	tVData.add(tSetI);
        	System.out.println("Start LAAChargeWrapRateUI Submit...INSERT");
        	tLAAChargeRateSetUI.submitData(tVData,"INSERT");        	    	
    	}
    	else if (tSetD.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetD);
        	System.out.println("Start LAAChargeWrapRateUI Submit...DELETE");
        	tLAAChargeRateSetUI.submitData(tVData,"DELETE");        	    	
    	}
    	else if (tSetU.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetU);
        	System.out.println("Start LAAChargeWrapRateUI Submit...UPDATE");
        	tLAAChargeRateSetUI.submitData(tVData,"UPDATE");        	    	
    	}
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAChargeRateSetUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
