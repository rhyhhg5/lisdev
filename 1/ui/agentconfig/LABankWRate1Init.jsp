<%
//程序名称：DataIntoLACommisionInit.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
 var  mSQL=" 1 and  char(length(trim(comcode))) = #8# and sign=#1#" ;
 var StrSql=" 1 and risktype4=#4# and riskcode in (select code from ldcode where codetype=#bankriskcode#)";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('BranchType').value = mBranchType;
   	//fm.all('BranchType2').value = mBranchType2;
  }
  catch(ex)
  {
    alert("DataIntoLACommisionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
   // alert(121);
    initSetGrid1();
   
  }
  catch(re)
  {
    alert("DataIntoLACommisionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var SetGrid1;
var SetGrid2;
var SetGrid3;
var SetGrid4;

function initSetGrid1()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    
    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="80px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
                                       
    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构名称"; //列名
    iArray[4][1]="120px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[5]=new Array();
    iArray[5][0]="代理机构"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许        

    
    iArray[6]=new Array();
    iArray[6][0]="代理机构名称"; //列名
    iArray[6][1]="120px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
       
    iArray[7]=new Array();
    iArray[7][0]="保费类型"; //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许
	
	iArray[8]=new Array();
    iArray[8][0]="缴费方式"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="保险期间"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许 
    
    
    iArray[10]=new Array();
    iArray[10][0]="最低缴费年期"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许    
      
    iArray[11]=new Array();
    iArray[11][0]="缴费年期小于"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[12]=new Array();
    iArray[12][0]="保单年度"; //列名
    iArray[12][1]="80px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[13]=new Array();
    iArray[13][0]="佣金比率"; //列名
    iArray[13][1]="80px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=0;              //是否允许输入,1表示允许,0表示不允许    
      
    iArray[14]=new Array();
    iArray[14][0]="录入日期"; //列名
    iArray[14][1]="80px";        //列宽
    iArray[14][2]=100;            //列最大值
    iArray[14][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[15]=new Array();
    iArray[15][0]="执行起期"; //列名
    iArray[15][1]="80px";        //列宽
    iArray[15][2]=100;            //列最大值
    iArray[15][3]=0;              //是否允许输入,1表示允许,0表示不允许    
      
    SetGrid1 = new MulLineEnter( "fm" , "SetGrid1" );
    SetGrid1.canChk = 0;
  //SetGrid1.mulLineCount = 10;
    SetGrid1.displayTitle = 1;
    //SetGrid1.hiddenSubtraction =0;
    SetGrid1.hiddenPlus=1;
    //SetGrid1.locked=1;
    //SetGrid1.canSel=0;
    SetGrid1.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid1函数中发生异常:初始化界面错误!");
  }
}
function initSetGrid2()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="80px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
       
    iArray[4]=new Array();
    iArray[4][0]="管理机构名称"; //列名
    iArray[4][1]="60px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
       
    iArray[5]=new Array();
    iArray[5][0]="保费类型"; //列名
    iArray[5][1]="60px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
   iArray[6]=new Array();
    iArray[6][0]="缴费方式"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="保险期间"; //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许    
        
    iArray[8]=new Array();
    iArray[8][0]="最低缴费年期"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
	iArray[9]=new Array();
    iArray[9][0]="缴费年期小于"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    
    iArray[10]=new Array();
    iArray[10][0]="保单年度"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许   
		
    iArray[11]=new Array();
    iArray[11][0]="佣金比率"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=0;              //是否允许输入,1表示允许,0表示不允许    
      
    iArray[12]=new Array();
    iArray[12][0]="录入日期"; //列名
    iArray[12][1]="80px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[13]=new Array();
    iArray[13][0]="执行起期"; //列名
    iArray[13][1]="80px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    SetGrid2 = new MulLineEnter( "fm" , "SetGrid2" );
    SetGrid2.canChk = 0;
		//SetGrid2.mulLineCount = 10;
    SetGrid2.displayTitle = 1;
    //SetGrid2.hiddenSubtraction =0;
    SetGrid2.hiddenPlus=1;
    //SetGrid2.locked=1;
    //SetGrid2.canSel=0;
    SetGrid2.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionOnmiSetInit.jsp-->initSetGrid2函数中发生异常:初始化界面错误!");
  }
}
function initSetGrid3() {                            
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         		//列名
    iArray[1][1]="80px";         		//宽度
   	iArray[1][3]=0;         		//宽度
    
    iArray[2]=new Array();
    iArray[2][0]="代理机构";         		//列名
    iArray[2][1]="80px";         		//宽度
   	iArray[2][3]=0;         		//宽度
    
    iArray[3]=new Array();                                                 
    iArray[3][0]="险种编码";         		//列名                             
    iArray[3][1]="80px";            		//列宽                             
    iArray[3][2]=200;            			//列最大值                           
    iArray[3][3]=0;                                                        
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";         		//列名
    iArray[4][1]="80px";         		//宽度
    iArray[4][2]=200;
    iArray[4][3]=0;         		//最大长度

    iArray[5]=new Array();
    iArray[5][0]="保费类型"; //列名
    iArray[5][1]="60px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许
     	   	    
    iArray[6]=new Array();
    iArray[6][0]="缴费方式"; //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="保险期间"; //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="最低缴费年期";         		//列名
    iArray[8][1]="80px";         		//宽度
    iArray[8][2]=200;
    iArray[8][3]=0;         		//最大长度
   
    iArray[9]=new Array();
    iArray[9][0]="缴费年期小于"; //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    
    iArray[10]=new Array();
    iArray[10][0]="间接绩效提奖比例（小数）";         		//列名
    iArray[10][1]="90px";         		//宽度
    iArray[10][2]=200;
    iArray[10][3]=0;         		//最大长度
    
    iArray[11]=new Array();
    iArray[11][0]="录入日期"; //列名
    iArray[11][1]="90px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
   
   
    SetGrid3 = new MulLineEnter( "fm" , "SetGrid3" ); 
    //这些属性必须在loadMulLine前
    SetGrid3.canChk = 0;
    //SetGrid3.mulLineCount = 0;   
    SetGrid3.displayTitle = 1;
    SetGrid3.hiddenPlus = 1;
    //SetGrid3.hiddenSubtraction = 0;
    //SetGrid3.canSel = 0;
    SetGrid3.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("在RiskQuarterAssessRateInit.jsp-->initSetGrid3函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid4() {                            
	  var iArray = new Array();
	  try {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="40px";         		//宽度
	    iArray[0][2]=100;            //列最大值
	    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
	    
	    iArray[1]=new Array();
	    iArray[1][0]="管理机构";         		//列名
	    iArray[1][1]="80px";         		//宽度
	   	iArray[1][3]=0;         		//宽度
	    
	    iArray[2]=new Array();                                                 
	    iArray[2][0]="险种编码";         		//列名                             
	    iArray[2][1]="80px";            		//列宽                             
	    iArray[2][2]=200;            			//列最大值                           
	    iArray[2][3]=0;                                                        
	    
	    iArray[3]=new Array();
	    iArray[3][0]="险种名称";         		//列名
	    iArray[3][1]="80px";         		//宽度
	    iArray[3][2]=200;
	    iArray[3][3]=0;         		//最大长度

	    iArray[4]=new Array();
	    iArray[4][0]="保费类型";         		//列名
	    iArray[4][1]="80px";         		//宽度
	    iArray[4][2]=200;
	    iArray[4][3]=0;         		//最大长度
	     	   	    
	    iArray[5]=new Array();
	    iArray[5][0]="缴费方式"; //列名
	    iArray[5][1]="60px";        //列宽
	    iArray[5][2]=100;            //列最大值
	    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许
	    
	    iArray[6]=new Array();
	    iArray[6][0]="保险期间"; //列名
	    iArray[6][1]="80px";        //列宽
	    iArray[6][2]=100;            //列最大值
	    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许
	    
	    iArray[7]=new Array();
	    iArray[7][0]="最低缴费年期";         		//列名
	    iArray[7][1]="80px";         		//宽度
	    iArray[7][2]=200;
	    iArray[7][3]=0;         		//最大长度
	   
	    iArray[8]=new Array();
	    iArray[8][0]="缴费年期小于"; //列名
	    iArray[8][1]="80px";        //列宽
	    iArray[8][2]=100;            //列最大值
	    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许    
	    
	    
	    iArray[9]=new Array();
	    iArray[9][0]="间接绩效提奖比例（小数）";         		//列名
	    iArray[9][1]="90px";         		//宽度
	    iArray[9][2]=200;
	    iArray[9][3]=0;         		//最大长度
	    
	    iArray[10]=new Array();
	    iArray[10][0]="录入日期"; //列名
	    iArray[10][1]="90px";        //列宽
	    iArray[10][2]=100;            //列最大值
	    iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许
	    
	   
	   
	    SetGrid4 = new MulLineEnter( "fm" , "SetGrid4" ); 
	    //这些属性必须在loadMulLine前
	    SetGrid4.canChk = 0;
	    //SetGrid3.mulLineCount = 0;   
	    SetGrid4.displayTitle = 1;
	    SetGrid4.hiddenPlus = 1;
	    //SetGrid3.hiddenSubtraction = 0;
	    //SetGrid3.canSel = 0;
	    SetGrid4.loadMulLine(iArray);  
	  }
	  catch(ex) {
	    alert("在RiskQuarterAssessRateInit.jsp-->initSetGrid4函数中发生异常:初始化界面错误!");
	  }
	}
</script>
