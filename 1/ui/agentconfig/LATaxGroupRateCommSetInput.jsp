<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LaratecommisionSetInput.jsp
//程序功能：万能险佣金率录入界面
//创建时间：2008-07-04
//创建人  ：lihai

String BranchType2=request.getParameter("BranchType2");
if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
// above is miaoxiangzheng's new_add
%>
<%@page contentType="text/html;charset=GBK" %>
<script>
   //var msql2=" 1 and riskcode   in ( #122901#,#330801#,#331801#,#332701#,#333901#,#334701#) ";
   //var msql="1 and char(length(trim(comcode))) in (#4#,#2#)";
   var msql2=" 1 and taxoptimal=#Y# ";
   var msql="1 and char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LATaxGroupRateCommSetInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LATaxGroupRateCommSetInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LATaxGroupRateCommSetSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
     <td class=title>险种</td>
     <td class= input>
     	<Input class=codeno name=RiskCode verify="险种|code:RiskName"
            ondblclick="return showCodeList('RiskName',[this,RiskCodename],[0,1],null,msql2,1);" 
            onkeyup="return showCodeListKey('RiskName',[this,RiskCodename],[0,1],null,msql2,1);"  ><input class=codename name=RiskCodename readonly=true  >
             
     <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,'1',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,'1',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary > 
          </TD>  
   </tr>
   <tr>			
   		  <TD  class= title>
            保险期间始期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=StartDay >
          </TD>
            <TD  class= title>
            保险期间止期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=EndDay >
          </TD>
   	
   </tr>
   
   <tr class=common> 
          <td  class= title> 保单年度 </td>
          <TD  class= input> <Input class= "common" name=CurYear></TD>
           <td  class= title>佣金比率</td>
          <TD  class= input> <Input class= "common" name=Rate></TD>      			
   </tr>
   <TR  class= common> 
           <td  class= title>保费类型</td>
          <TD  class= input> <Input class= "codeno" name=F01Code CodeData="0|^04|帐户保费|^05|风险保费"
               ondblclick="return showCodeListEx('F01list',[this,F01Name],[0,1]);" onkeyup="return showCodeListKeyEx('F01list',[this,F01Name],[0,1]);" onchange=""><input class=codename name=F01Name  readonly=true ></TD> 
          <!--  Input class="codeno" name=PayintvCode  CodeData="0|^0|趸缴|^12|年缴|^6|半年缴|^3|季缴|^1|月缴" ondblclick="return showCodeListEx('payintvlist',[this,PayintvName],[0,1]);" onkeyup="return showCodeListKeyEx('payintvlist',[this,PayintvName],[0,1]);" onchange=""><input class=codename name=PayintvName  readonly=true >--> 
           
		<Input class="readonly" type=hidden name=diskimporttype>
		
   </TR>
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     	    <input type=button value="下  载" class=cssButton onclick="DoNewDownload();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();">  
    		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 

    	</td>
   
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>提奖计算基数设置   <p>
    		<font color="#ff0000">(注：提奖比例必须为0-1的数，如23%，记为0.23!) </font><br/>
    	   </p> 
    	  </td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  <br><hr>
  </div>
  <Input  type=hidden name=branchtype2> 
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <!--  Input type=hidden id="RiskType4" name=RiskType4 value=''-->
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">   
   <input type=hidden class=Common name=querySql > 
   <input type=hidden class=Common name=flag > 
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>

