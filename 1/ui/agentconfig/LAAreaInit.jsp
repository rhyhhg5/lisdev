<%
//程序名称：LAAreaInit.jsp
//程序功能：功能描述
//创建日期：　
//创建人  　
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ManageCom').value = "";
    fm.all('ManageComName').value = "";
    fm.all('AreaType').value = "";
    fm.all('AreaTypeName').value = "";
    fm.all('toperate').value = "";  
        }
  catch(ex) {
    alert("LAAreaInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initAreaGrid();
  }
  catch(re) {
    alert("LAAreaInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var initAreaGrid;
function initAreaGrid()
{
  try
  {
    var iArray = new Array();
    
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="25px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="60px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="地区类型"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

    AreaGrid = new MulLineEnter( "fm" , "AreaGrid" );
    AreaGrid.canSel = 0;
    AreaGrid.mulLineCount =  0;
    AreaGrid.displayTitle = 1;
    AreaGrid.hiddenPlus = 1;
    AreaGrid.hiddenSubtraction = 1;
    AreaGrid.locked = 1;
    AreaGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在AssessIndexQueryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}

</script>

