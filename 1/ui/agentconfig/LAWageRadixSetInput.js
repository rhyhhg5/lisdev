//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//提交，保存按钮对应操作
function DoSave()
{
    
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmAction.value = 'save';
  fm.submit(); //提交
}

function DoDel()
{
  if(fm.AreaType.value=="")
  {
    alert("请录入要删除的地区类型编码！");
    return ;
  }	
  if(fm.BranchType.value=="")
  {
    alert("请录入要删除的展业机构类型编码！");
    return ;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmAction.value = 'del';
  fm.submit(); //提交
}

// 查询按钮
function easyQueryClick()
{		
  // 初始化表格
  initSetGrid();
  
  var tAreaType  = fm.AreaType.value;
  var tBranchType  = fm.BranchType.value;
  
  if ( tAreaType == null || tAreaType == "" ) 
  {
  	alert("请选择地区类型编码！");
  	return;
  }	
  if ( tBranchType == null || tBranchType == "" ) 
  {
  	alert("请选择展业类型编码！");
  	return;
  }	
  
  		
  // 书写SQL语句
  var strSQL = "";  
  strSQL = "select areatype,branchtype,agentgrade,wagecode,wagename,fycmin,fycmax,drawrate,limitperiod,rewardmoney,drawstart,drawend,drawrateoth,premstand,reartype,idx from lawageradix where 1=1"
         + getWherePart( 'AreaType','AreaType' )			 
		 + getWherePart( 'BranchType','BranchType' )		   
		 + getWherePart( 'AgentGrade','AgentGrade' )
		 + getWherePart( 'WageCode','WageCode' );
		 
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据，请重新录入查询条件！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = SetGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    initForm();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("初始化页面错误，重置出错");
  }
}




//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,0,0,*";
  }
   else
   {
      parent.fraMain.rows = "0,0,0,0,*";
   }
}


//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{
  if (confirm("您确实想修改该记录吗?"))
   {
    fm.fmAction.value = "update";
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   
    fm.submit(); //提交
  }
  else
  {
    fm.OperateType.value = "";
    alert("您取消了修改操作！");
  }
}


function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function qryWageCode()
{
  showInfo = window.open("./WageCodeQueryInput.html");     	
}	

function afterQryWageCode(arrResult)
{
  fm.WageCode.value = '';
  fm.all('WageCode').value = arrResult[0];	
}	


function addOneRow()
{
  showInfo=window.open("./AddOneRowInput.html");
}	

function afterAddOneRow(arrResult)
{
  SetGrid.addOne("SetGrid");
  var rowNum=SetGrid.mulLineCount ;	
  
  SetGrid.setRowColData(rowNum-1,1,arrResult[0]);	
  SetGrid.setRowColData(rowNum-1,2,arrResult[1]);
  SetGrid.setRowColData(rowNum-1,3,arrResult[2]);
  SetGrid.setRowColData(rowNum-1,4,arrResult[3]);
  SetGrid.setRowColData(rowNum-1,5,arrResult[4]);
  SetGrid.setRowColData(rowNum-1,6,arrResult[5]);
  SetGrid.setRowColData(rowNum-1,7,arrResult[6]);
  SetGrid.setRowColData(rowNum-1,8,arrResult[7]);
  SetGrid.setRowColData(rowNum-1,9,arrResult[8]);
  SetGrid.setRowColData(rowNum-1,10,arrResult[9]);
  SetGrid.setRowColData(rowNum-1,11,arrResult[10]);
  SetGrid.setRowColData(rowNum-1,12,arrResult[11]);
  SetGrid.setRowColData(rowNum-1,13,arrResult[12]);
  SetGrid.setRowColData(rowNum-1,14,arrResult[13]);
  SetGrid.setRowColData(rowNum-1,15,arrResult[14]);
}	