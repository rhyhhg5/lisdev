<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAAgentPromRadixInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAAgentPromRadixInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./LAAgentPromRadixSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgentPromRadix1);">
    
    <td class=titleImg>
      考核标准信息维护
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLAAgentPromRadix1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  代理人职级 
		</td>
        <td  class= input> 
		  <input class="code" name=AgentGrade ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
		                                      onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');"
		                                      verify = "代理人职级|notnull&code:AgentGrade" > 
		</td>
        <td  class= title> 
		  地区类型 
		</td>
        <td  class= input> 
		  <input class='code' name=AreaType ondblclick="return showCodeList('AreaType',[this]);" 
		                                     onkeyup="return showCodeListKey('areatype',[this]);"
		                                     verify = "地区类型|notnull&code:AreaType"> 
		</td>
       <td class=title>
           考核类型
       </td>
       <td class=input> 
          <input class=code name="AssessType" verify="考核类型|notnull&code:AssessType1" CodeData="0|2^01|维持^02|晋升"
                                                ondblclick="return showCodeListEx('AssessType1',[this]);" 
                                                onkeyup="return showCodeListKeyEx('AssessType1',[this]);">
          </td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  目标职级 
		</td>
        <td  class= input> 
		  <input class="code" name=DestAgentGrade ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlDestAgentGrade%>','1');" 
		                                         onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlDestAgentGrade%>','1');"
		                                         verify = "目标代理人职级|notnull"> 
		</td>
         <td  class= title> 
		  考核标准期限 
		</td>
        <td  class= input> 
		  <input class='code' name=LimitPeriod CodeData="0|3^01|三个月^02|六个月^03|九个月^04|十二个月"
		                      ondblclick="return showCodeListEx('LimitPeriod',[this]);" 
		                      onkeyup="return showCodeListKeyEx('LimitPeriod',[this]);"
		                      verify = "考核期限|notnull&code:LimitPeriod"> 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		   个人累计客户数
		</td>
        <td  class= input> 
		  <input class=common name=IndCustSum verify="个人累计客户数|INT"> 
		</td>
        <td  class= title> 
		  个人累计FYC
		</td>
        <td  class= input> 
		  <input name=IndFYCSum class=common verify="个人累计FYC|NUM"> 
		</td>      
        <td  class= title> 
		   月均客户数
		</td>
        <td  class= input> 
		  <input class=common name=MonAvgCust verify="月均客户数|INT"> 
		</td>
      </tr>      
      <tr  class= common> 
        <td  class= title> 
		  月均FYC
		</td>
        <td  class= input> 
		  <input name=MonAvgFYC class=common verify="月均FYC|NUM"> 
		</td>     
        <td  class= title> 
		   个人继续率
		</td>
        <td  class= input> 
		  <input class=common name=IndRate verify="个人继续率|NUM"> 
		</td>
        <td  class= title> 
		  直接增员数
		</td>
        <td  class= input> 
		  <input name=DirAddCount class=common verify="直接增员数|INT"> 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  增员人数
		</td>
        <td  class= input> 
		  <input name=AddCount class=common verify="增员人数|INT"> 
		</td>    
        <td  class= title> 
		   增员人中含理财主任数
		</td>
        <td  class= input> 
		  <input class=common name=ReachGradeCount verify="增员人中含理财主任数|INT"> 
		</td>  
        <td  class= title> 
		   本人及所增人员合计FYC
		</td>
        <td  class= input> 
		  <input class=common name=IndAddFYCSum verify="本人及所增人员合计FYC|NUM"> 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  组平均继续率
		</td>
        <td  class= input> 
		  <input name=TeamAvgRate class=common verify="组平均继续率|NUM"> 
		</td>      
        <td  class= title> 
		 直辖人数
		</td>
        <td  class= input> 
		  <input class=common name=DirMngCount verify="直辖人数|INT"> 
		</td>   
		
        <td  class= title> 		   
                直辖人中理财主任数
		</td>  
        <td  class= input> 
		  <input name=DirMngFinaCount class=common verify="直辖人中理财主任数|INT"> 
		</td> 
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  直辖组累计FYC
		</td>
        <td  class= input> 
		  <input class=common name=TeamFYCSum verify="直辖组累计FYC|NUM"> 
		</td>  
        <td  class= title> 
		   直接育成营业组数
		</td>
        <td  class= input> 
		  <input class=common name=DRTeamCount verify="直接育成营业组数|INT"> 
		</td>  
        <td  class= title> 
		   直辖及育成组平均继续率
		</td>
        <td  class= input> 
		  <input class=common name=DRTeamAvgRate verify="直辖及育成组平均继续率|NUM"> 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  直辖及育成组累计FYC
		</td>
        <td  class= input> 
		  <input name=DRFYCSum class=common verify="直辖及育成组累计FYC|NUM"> 
		</td>      
        <td  class= title> 
		   合计育成组数
		</td>
        <td  class= input> 
		  <input class=common name=RearTeamSum verify="合计育成组数|INT"> 
		</td>
        <td  class= title> 
		  直辖及育成组月均有效人力
		</td>
        <td  class= input> 
		  <input name=DRMonLabor class=common verify="直辖及育成组月均有效人力|NUM"> 
		</td>      
      </tr>      
      <tr  class= common> 
        <td  class= title> 
		 有效人力的FYC标准
		</td>
        <td  class= input> 
		  <input class=common name=LaborFYCStand verify="有效人力的FYC标准|NUM"> 
		</td>
        <td  class= title> 
		  直辖部累计FYC
		</td>
        <td  class= input> 
		  <input name=DepFYCSum class=common verify="直辖部累计FYC|NUM"> 
		</td>      
        <td  class= title> 
		   部门平均继续率
		</td>
        <td  class= input> 
		  <input class=common name=DepAvgRate verify="部门平均继续率|NUM"> 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  直接育成营销服务部数
		</td>
        <td  class= input> 
		  <input name=RearDepCount class=common verify="直接育成营销服务部数|INT"> 
		</td>      
        <td  class= title> 
		   直辖部及育成部有效人力
		</td>
        <td  class= input> 
		  <input class=common name=DRDepLabor verify="直辖部及育成部有效人力|INT"> 
		</td>
        <td  class= title> 
		  直辖部及育成部平均继续率
		</td>
        <td  class= input> 
		  <input name=DRDepAvgRate class=common verify="直辖部及育成部平均继续率|NUM"> 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		   育成营销服务部数
		</td>
        <td  class= input> 
		  <input class=common name=RearDepSum verify="育成营销服务部数|INT"> 
		</td>
        <td  class= title> 
		  直辖部月均FYC
		</td>
        <td  class= input> 
		  <input name=DirDepMonAvgFYC class=common verify="直辖部月均FYC|NUM"> 
		</td>      
        <td  class= title> 
		   所辖营销服务部理财主任数
		</td>
        <td  class= input> 
		  <input class=common name=DepFinaCount verify="所辖营销服务部理财主任数|INT"> 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  所辖营业单位平均继续率
		</td>
        <td  class= input> 
		  <input name=AreaRate class=common verify="所辖营业单位平均继续率|NUM"> 
		</td>      
        <td  class= title> 
		   直接育成督导长数
		</td>
        <td  class= input> 
		  <input class=common name=DirRearAdmCount verify="直接育成督导长数|INT"> 
		</td>
        <td  class= title> 
		  所辖支公司理财主任数
		</td>
        <td  class= input> 
		  <input name=MngAreaFinaCount class=common verify="所辖支公司理财主任数|INT"> 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		   育成督导长数
		</td>
        <td  class= input> 
		  <input class=common name=RearAdmCount verify="育成督导长数|INT"> 
		</td>
        <td  class= title> 
		  最近12个月直接育成部数
		</td>
        <td  class= input> 
		  <input name=ReqDepCount class=common verify="最近12个月直接育成部数|INT"> 
		</td>      
      </tr>
      
    </table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
