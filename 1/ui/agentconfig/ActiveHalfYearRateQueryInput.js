//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var tOrphanCode="";
var queryFlag = false;
window.onfocus=myonfocus;

function setActiveHalfYearRateValue()
{
	var sql ="select  gradecode,gradename,'','' from laagentgrade a where branchtype='5' and gradecode like 'U%' with ur ";
	turnPage.queryModal(sql, ActiveHalfYearRateGrid);
}
 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}



//提交，保存按钮对应操作
function submitForm()
{
	mOperate = "INSERT";
	if( verifyInput() == false ) return false;
    if(chkMulLine()== false)  return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	fm.submit();
}
function updateClick()
{
	mOperate = "UPDATE";
	if( verifyInput() == false ) return false;
	if(queryFlag==false)     
	{
		alert("请先查询 ");
		return false;
	}
	if(chkMulLine()==false)  return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	}
}
//标准查询
function queryStandClick()
{
	var divLAAgent2 = document.getElementById("divActiveHalfYearRateStand");
	showDiv(divLAAgent2,"true");
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function chkMulLine()
{
	var rowNum = divActiveHalfYearRateStand.mulLineCount;
	for(var i=0;i<rowNum;i++)
	{
		if(divActiveHalfYearRateStand.getRowColData(i,3) == null||divActiveHalfYearRateStand.getRowColData(i,3) == "")
		{
			divActiveHalfYearRateStand.setRowColData(i,3,0);
		}
	}
	return true;
}
function queryClick()
{
	showInfo = window.open("./ActiveHalfYearRateQuery.html");
}
function afterQuery()
{
	
}

