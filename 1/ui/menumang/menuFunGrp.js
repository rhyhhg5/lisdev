/*****************************************************************
 *      Program NAME:                        
 *        programmer:                     
 *       Create DATE:                                          
 * Modify programmer: Minim                                    
 *       Modify DATE: 2005-9-15                            
 */
 
var showInfo;
var showDiv = "false";
var turnPage = new turnPageClass();   

//查询并列出所有菜单记录
function queryClick() {
	var sql = "select nodecode, ChildFlag, parentnodecode, nodename, runscript, nodeorder from LDMenu where 1=1 "
		      + getWherePart('nodename', 'NodeName1', 'like')
		      + getWherePart('runscript', 'RunScript1', 'like');
  if (fm.all("ParentNodeName1").value!="") {
  	sql = sql + "and nodecode in (select nodecode from ldmenu where parentnodecode in "
  	    + "(select nodecode from ldmenu where 1=1 " + getWherePart('nodename', 'ParentNodeName1') + ")) ";
  }
	sql = sql + "order by nodeorder";
	
	turnPage.pageLineNum = 15;			  
  turnPage.queryModal(sql, QueryGrpGrid);
}  		      
function onQuerySelected(parm1,parm2)
{ 
    fm.all("NodeName").value = QueryGrpGrid.getRowColData(QueryGrpGrid.getSelNo() - 1, 4);
	fm.all("RunScript").value = QueryGrpGrid.getRowColData(QueryGrpGrid.getSelNo() - 1, 5);	
	
}
function submitForm() {
  var i = 0;
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
  fm.all("RunScript").value ="";
  //queryClick();
}

/** 平行向上插入 */
function insertUpClick() {
  if (fm.all("NodeName").value == "") {
    alert("请输入菜单节点名称!");
    return;
  }

  if (QueryGrpGrid.getSelNo()<0) {
  	alert("请先选择参考菜单结点!");
  }
	
	fm.all("SelNodeCode").value = QueryGrpGrid.getRowColData(QueryGrpGrid.getSelNo()-1, 1);  
	fm.all("Action").value = "upinsert";    	
	  
	submitForm();
}

/** 平行向下插入 */
function insertDownClick() {
  if (fm.all("NodeName").value == "") {
    alert("请输入菜单节点名称!");
    return;
  }

  if (QueryGrpGrid.getSelNo()<0) {
  	alert("请先选择参考菜单结点!");
  }
	
	fm.all("SelNodeCode").value = QueryGrpGrid.getRowColData(QueryGrpGrid.getSelNo()-1, 1);  
	fm.all("Action").value = "downinsert";    	
	  
	submitForm();
}

/** 做为下级插入 */
function insertInnerClick() {
  if (fm.all("NodeName").value == "") {
    alert("请输入菜单节点名称!");
    return;
  }

  if (QueryGrpGrid.getSelNo()<0) {
  	alert("请先选择参考菜单结点!");
  }
	
	fm.all("SelNodeCode").value = QueryGrpGrid.getRowColData(QueryGrpGrid.getSelNo()-1, 1);  
	fm.all("Action").value = "innerinsert";    	
	  
	submitForm();
}

function deleteClick() {
  if (QueryGrpGrid.getSelNo()<0) {
  	alert("请先选择参考菜单结点!");
  }
      	
	if (!confirm("您确实要删除这个菜单吗？")) {
	  return;
	}
	
  fm.all("Action").value = "delete";
  fm.all("SelNodeCode").value = QueryGrpGrid.getRowColData(QueryGrpGrid.getSelNo()-1, 1);  
  submitForm();    	
}

function flashClick() {
  //top.fraMenu.window.reflesh;   	
  alert("Sorry, I can't do it");
}
function updateClick(){
     if (QueryGrpGrid.getSelNo()<0) {
  	   alert("请先选择需要修改的菜单结点!");
     }
	 if (!confirm("您确实要修改这个菜单名或者映射文件吗？")) {
	  return;
	}
  fm.all("Action").value = "update";
  fm.all("SelNodeCode").value = QueryGrpGrid.getRowColData(QueryGrpGrid.getSelNo()-1, 1);  
  submitForm();
}

function afterSubmit(FlagStr, content) {
		window.focus();
    showInfo.close();	
       
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px"); 
  	top.window.focus();
  	queryClick();  
}







