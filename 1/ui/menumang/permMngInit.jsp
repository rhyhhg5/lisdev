<%
//程序名称：permMngInit.jsp
//程序功能：
//创建日期：2005-12-13 11:10:36
//创建人  ：Zhangbin程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
    
  }
  catch(ex)
  {
    alert("在ProductQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在ProductQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
		initUserPermGrid();
		initUnderLingGrid();
		initMenuGrpGrid();
		initMenuNodeGrid();
//		initContGrid();
	
  }
  catch(re)
  {
    alert("ProductQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 产品查询信息列表的初始化
function initUserPermGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="NO.";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="用户编码";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="用户姓名";         		//列名                     
      iArray[2][1]="50px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="用户类别";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="级别编号";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="机构性质";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      /******/
      
      iArray[7]=new Array();
      iArray[7][0]="上级用户";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();                                                 
      iArray[8][0]="下级用户";         		//列名                     
      iArray[8][1]="0px";            		//列宽                             
      iArray[8][2]=100;            			//列最大值                           
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][4]="underlingname";
      iArray[8][5]="1";
      //iArray[8][6]="0";
      iArray[8][15]= "operator";  //要依赖的列的名称 
      iArray[8][17]="1"
      iArray[8][19]=1;   		//1是需要强制刷新.
      
			iArray[9]=new Array();                                                 
      iArray[9][0]="用户状态";         		//列名                     
      iArray[9][1]="50px";            		//列宽                             
      iArray[9][2]=100;            			//列最大值                           
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[10]=new Array();
      iArray[10][0]="生效日期";         		//列名
      iArray[10][1]="70px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[11]=new Array();
      iArray[11][0]="终止日期";         		//列名
      iArray[11][1]="70px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      UserPermGrid = new MulLineEnter( "fm" , "UserPermGrid" ); 
      //这些属性必须在loadMulLine前
      UserPermGrid.mulLineCount = 0;   
      UserPermGrid.displayTitle = 1;
      UserPermGrid.locked = 1;
      UserPermGrid.canSel = 1;
      UserPermGrid.hiddenPlus = 1;
      UserPermGrid.hiddenSubtraction = 1;
      UserPermGrid.loadMulLine(iArray); 
      UserPermGrid.selBoxEventFuncName = "showButtons";
      //UserPermGrid.recordNo=0;   //设置序号起始基数为10，如果要分页显示数据有用
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initUnderLingGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="NO.";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="用户编码";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="用户姓名";         		//列名                     
      iArray[2][1]="50px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="用户类别";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="级别编号";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="机构性质";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      /******/
      
      iArray[7]=new Array();
      iArray[7][0]="上级用户";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();                                                 
      iArray[8][0]="下级用户";         		//列名                     
      iArray[8][1]="0px";            		//列宽                             
      iArray[8][2]=100;            			//列最大值                           
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][4]="underlingname";
      iArray[8][5]="1";
      //iArray[8][6]="0";
      iArray[8][15]= "operator";  //要依赖的列的名称 
      iArray[8][17]="1"
      iArray[8][19]=1;   		//1是需要强制刷新.
      
			iArray[9]=new Array();                                                 
      iArray[9][0]="用户状态";         		//列名                     
      iArray[9][1]="50px";            		//列宽                             
      iArray[9][2]=100;            			//列最大值                           
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[10]=new Array();
      iArray[10][0]="生效日期";         		//列名
      iArray[10][1]="70px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[11]=new Array();
      iArray[11][0]="终止日期";         		//列名
      iArray[11][1]="70px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      UnderLingGrid = new MulLineEnter( "fm" , "UnderLingGrid" ); 
      //这些属性必须在loadMulLine前
      UnderLingGrid.mulLineCount = 0;   
      UnderLingGrid.displayTitle = 1;
      UnderLingGrid.locked = 1;
      UnderLingGrid.canSel = 0;
      UnderLingGrid.hiddenPlus = 1;
      UnderLingGrid.hiddenSubtraction = 1;
      UnderLingGrid.loadMulLine(iArray); 
      UnderLingGrid.selBoxEventFuncName = "showTKPDF";
      //UnderLingGrid.recordNo=0;   //设置序号起始基数为10，如果要分页显示数据有用
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}




function initClaimUserGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="NO.";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="用户编码";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="用户姓名";         		//列名                     
      iArray[2][1]="50px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="用户类别";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="级别编号";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="机构性质";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      /******/
      
      iArray[7]=new Array();
      iArray[7][0]="上级用户";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();                                                 
      iArray[8][0]="下级用户";         		//列名                     
      iArray[8][1]="0px";            		//列宽                             
      iArray[8][2]=100;            			//列最大值                           
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][4]="underlingname";
      iArray[8][5]="1";
      //iArray[8][6]="0";
      iArray[8][15]= "operator";  //要依赖的列的名称 
      iArray[8][17]="1"
      iArray[8][19]=1;   		//1是需要强制刷新.
      
			iArray[9]=new Array();                                                 
      iArray[9][0]="用户状态";         		//列名                     
      iArray[9][1]="50px";            		//列宽                             
      iArray[9][2]=100;            			//列最大值                           
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[10]=new Array();
      iArray[10][0]="生效日期";         		//列名
      iArray[10][1]="70px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[11]=new Array();
      iArray[11][0]="终止日期";         		//列名
      iArray[11][1]="70px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      ClaimUserGrid = new MulLineEnter( "fm" , "ClaimUserGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimUserGrid.mulLineCount = 0;   
      ClaimUserGrid.displayTitle = 1;
      ClaimUserGrid.locked = 1;
      ClaimUserGrid.canSel = 0;
      ClaimUserGrid.hiddenPlus = 1;
      ClaimUserGrid.hiddenSubtraction = 1;
      ClaimUserGrid.loadMulLine(iArray); 
      ClaimUserGrid.selBoxEventFuncName = "showTKPDF";
      //UnderLingGrid.recordNo=0;   //设置序号起始基数为10，如果要分页显示数据有用
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


function initMenuGrpGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="NO.";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="用户号";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="用户姓名";         		//列名                     
      iArray[2][1]="50px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="菜单组代码";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="菜单组名";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      MenuGrpGrid = new MulLineEnter( "fm" , "MenuGrpGrid" ); 
      //这些属性必须在loadMulLine前
      MenuGrpGrid.mulLineCount = 0;   
      MenuGrpGrid.displayTitle = 1;
      MenuGrpGrid.locked = 1;
      MenuGrpGrid.canSel = 1;
      MenuGrpGrid.hiddenPlus = 1;
      MenuGrpGrid.hiddenSubtraction = 1;
      MenuGrpGrid.loadMulLine(iArray); 
      MenuGrpGrid.selBoxEventFuncName = "showMenus";
      //MenuGrpGrid.recordNo=0;   //设置序号起始基数为10，如果要分页显示数据有用
      
      //这些操作必须在loadMulLine后面
      //MenuGrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initMenuNodeGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="NO.";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="菜单组代码";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="菜单节点代码";         		//列名                     
      iArray[2][1]="50px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="菜单节点名";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="菜单节点描述";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      MenuNodeGrid = new MulLineEnter( "fm" , "MenuNodeGrid" ); 
      //这些属性必须在loadMulLine前
      MenuNodeGrid.mulLineCount = 0;   
      MenuNodeGrid.displayTitle = 1;
      MenuNodeGrid.locked = 1;
      MenuNodeGrid.canSel = 0;
      MenuNodeGrid.hiddenPlus = 1;
      MenuNodeGrid.hiddenSubtraction = 1;
      MenuNodeGrid.loadMulLine(iArray); 
      MenuNodeGrid.selBoxEventFuncName = "";
      //MenuGrpGrid.recordNo=0;   //设置序号起始基数为10，如果要分页显示数据有用
      
      //这些操作必须在loadMulLine后面
      //MenuGrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}



</script>

<%
	CError cError = new CError();
	boolean operFlag=true;
	String Content = "";
	
	ArrayList sortList 		= new ArrayList(); //排序用户
	HashMap 	sortMap 		= new HashMap();	 //带有层级数的map
	ArrayList allList 		= new ArrayList(); //所有用户
  HashMap 	codeNameMap = new HashMap();	 //代码用户名map
  HashMap 	userMap     = new HashMap(); 	 //用于存放用户上下级关系
  
  ArrayList adminAllList 		= new ArrayList(); //所有用户
  HashMap 	adminCodeNameMap = new HashMap();
  HashMap 	adminUserMap     = new HashMap();
  ArrayList adminSortList 		= new ArrayList(); //排序用户
	HashMap 	adminSortMap 		= new HashMap();	 //带有层级数的map
  
  String userStr="";
	String uLevelStr	="";
	
	String adminUserStr=""; 
	String adminULevelStr	="";
	
	TransferData opeData = new TransferData(); //用于传operator
	opeData.setNameAndValue("OpeCode",operator);
	VData oVData = new VData(); //向java传operator
	VData aVData = new VData(); //从java得用户字符串
	oVData.addElement(opeData);
	LDUserSortUI underUI = new LDUserSortUI(); 		//用于执行LDUserSortBL中getAllUnderLing()方法后
	LDUserSortUI userSortUI = new LDUserSortUI(); //用于执行LDUserSortBL中getSortData()方法,得到sortList,sortMap 

  	
	int userLevel = 0;
	int adminUserLevel = 0;
	
	if(!underUI.submitData(oVData,"ALLUNDER")) //1.首先得到所有下级用户
	{
		operFlag=false;
    Content=underUI.mErrors.getFirstError().toString(); 

%>
<script language="javascript">	
	alert("<%=Content%>");
</script>	
<%  
	}
	else
	{
		aVData = underUI.getResult();
		TransferData allUn=(TransferData)aVData.getObjectByObjectName("TransferData",0);
		userStr				=(String)allUn.getValueByName("UNDERSTR");
		uLevelStr			=(String)allUn.getValueByName("UNDERLEVEL");
		
		adminUserStr	=(String)allUn.getValueByName("ADMINUNDERSTR");      
		adminULevelStr=(String)allUn.getValueByName("ADMINUSERLEVEL");    
		
		
		allList =(ArrayList)allUn.getValueByName("ALLLIST");
		codeNameMap=(HashMap)allUn.getValueByName("CODENAMEMAP");
		userMap =(HashMap)allUn.getValueByName("USERMAP"); 
		
		adminAllList 		 =(ArrayList)allUn.getValueByName("ADMINALLLIST");
		adminCodeNameMap =(HashMap)allUn.getValueByName("ADMINCODENAMEMAP");
		adminUserMap		 =(HashMap)allUn.getValueByName("ADMINUSERMAP"); 
		
		////userLevel = Integer.parseInt(uLevelStr);
		
		System.out.println("has get adminUserStr..........."+adminUserStr+"  adminULevelStr............ "+adminULevelStr);
	}
	
	
	VData tVData = new VData();
	VData mVData = new VData();
	
	TransferData userData = new TransferData();
	userData.setNameAndValue("UserName",allList);
	userData.setNameAndValue("UserRela",userMap);
	userData.setNameAndValue("Operator",operator);
	
	userData.setNameAndValue("AdminUserName",adminAllList);
	userData.setNameAndValue("AdminUserRela",adminUserMap);
	userData.setNameAndValue("AdminOperator",operator);
	
	
	tVData.addElement(userData);
	
	if(!userSortUI.submitData(tVData,"SORT")) //2.然后对下级用户进行排序
	{
		operFlag=false;
    Content=userSortUI.mErrors.getFirstError().toString(); 
%>
<script language="javascript">	
	alert("<%=Content%>");
	//top.close();
</script>	
<%  
 
	}
	else
	{
		mVData = userSortUI.getResult(); 
		TransferData sortUser=(TransferData)mVData.getObjectByObjectName("TransferData",0); 
		
		sortList=(ArrayList)sortUser.getValueByName("SORTLIST"); 
		sortMap =(HashMap)sortUser.getValueByName("SORTMAP"); 
	
		adminSortList=(ArrayList)sortUser.getValueByName("ADMINSORTLIST"); 
		adminSortMap =(HashMap)sortUser.getValueByName("ADMINSORTMAP"); 
						
	}
%>  

<%/***********************得到行政关系的下级用户及关系***************************/
	
	
  
  
  

%>


<script language="javascript">	

</script>	