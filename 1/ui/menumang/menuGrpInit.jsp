<%
//程序名称：menuInit.jsp
//程序功能：
//创建日期：2002-10-10 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<SCRIPT src="Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
	
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
      fm.all("MenuGrpCode").value = "";
      fm.all("MenuGrpName").value = "";
      fm.all("MenuGrpDescription").value = "";
      fm.all("MenuSign").value = "";

  }
  catch(ex)
  {
    alert("在menuGrpInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在InputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  { 
    initInpBox();
    initSelBox(); 
    initQueryGrpGrid();
    initHideMenuGrpGrid1();  
    initHideMenuGrpGrid2();  
  }
  catch(re)
  {
    alert("menuGrpInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  } 
}


function  initHideMenuGrpGrid1()
  {
     var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              	
      
      iArray[1]=new Array();
      iArray[1][0]="菜单名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="140px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="菜单节点编码";    	                //列名
      iArray[2][1]="120px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
   	
      
      HideMenuGrpGrid1 = new MulLineEnter( "fm" , "HideMenuGrpGrid1" ); 
      
      //这些属性必须在loadMulLine前
      HideMenuGrpGrid1.mulLineCount = 0;     
      HideMenuGrpGrid1.displayTitle = 1;
      HideMenuGrpGrid1.canChk =1;
      HideMenuGrpGrid1.canSel =0;
      HideMenuGrpGrid1.locked =1;            //是否锁定：1为锁定 0为不锁定
      HideMenuGrpGrid1.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      HideMenuGrpGrid1.hiddenSubtraction=0; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      HideMenuGrpGrid1.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      HideMenuGrpGrid1.loadMulLine(iArray);     
      }
      catch(ex)
      { 
        alert(ex);
      }
  } 
  
  
function initHideMenuGrpGrid2()
  {
     var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              	
      
      iArray[1]=new Array();
      iArray[1][0]="菜单名称";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="140px";         		//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="菜单节点编码";    	    //列名
      iArray[2][1]="140px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
           
      HideMenuGrpGrid2 = new MulLineEnter( "fm" , "HideMenuGrpGrid2" ); 
      
      //这些属性必须在loadMulLine前
      HideMenuGrpGrid2.mulLineCount = 0;     
      HideMenuGrpGrid2.displayTitle = 1;
      HideMenuGrpGrid2.canChk =1;
      HideMenuGrpGrid2.canSel =0;
      HideMenuGrpGrid2.locked =1;            //是否锁定：1为锁定 0为不锁定
      HideMenuGrpGrid2.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      HideMenuGrpGrid2.hiddenSubtraction=0; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      HideMenuGrpGrid2.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      HideMenuGrpGrid2.loadMulLine(iArray);     
      }
      catch(ex)
      { 
        alert(ex);
      }
  } 
  

function initQueryGrpGrid()
{

    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              	
      
      iArray[1]=new Array();
      iArray[1][0]="菜单组名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="140px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="菜单组编码";    	                //列名
      iArray[2][1]="100px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
        	
      iArray[3]=new Array();
      iArray[3][0]="菜单标志";    	                //列名
      iArray[3][1]="100px";            		        //列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

 
      iArray[4]=new Array();
      iArray[4][0]="菜单组描述";    	                //列名
      iArray[4][1]="230px";            		        //列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
 
      iArray[5]=new Array();
      iArray[5][0]="操作员";    	                //列名
      iArray[5][1]="230px";            		        //列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
 
      QueryGrpGrid = new MulLineEnter( "fm" , "QueryGrpGrid" ); 
         
      //这些属性必须在loadMulLine前
      QueryGrpGrid.mulLineCount = 0;     
      QueryGrpGrid.displayTitle = 1;
      QueryGrpGrid.canChk =0;
      QueryGrpGrid.canSel =1;
      QueryGrpGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
      QueryGrpGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      QueryGrpGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      QueryGrpGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      QueryGrpGrid.loadMulLine(iArray);     
      }
      catch(ex)
      { 
        alert(ex);
      }
  }     
</script>

<%
	ArrayList allList  = new ArrayList();
	ArrayList tempList = new ArrayList();
	
	SSRS tSSRS = new SSRS();
	String strSQL= " select usercode from lduser where  operator = '"+ Operator +"'"; 
	ExeSQL tExeSQL = new ExeSQL();
  tSSRS = tExeSQL.execSQL(strSQL);
  int count = tSSRS.getMaxRow();
  String temp[][] = tSSRS.getAllData();
	
	for(int i=0;i<temp.length;i++)
	{
		for (int j=0;j<temp[i].length;j++)
		{
			System.out.println("  "+temp[i][j]);
			allList.add(temp[i][j]);
			tempList.add(temp[i][j]);
		}
	}
	
	String opeStr="''";
	String opeStr1="''";
	
	while(tempList.size()!=0)
	{
		Iterator it = tempList.iterator();
		while(it.hasNext())
		{
			opeStr = opeStr +",'" + it.next()+"'";
		}
		
		tempList.clear(); 
		
		System.out.println("allList.clear()后的size:  " + tempList.size());
		
		strSQL =  " select usercode from lduser where  operator in ("+ opeStr +") and usercode not in (" + opeStr + ") and usercode<>'" + Operator + "'"; 
		
		tSSRS = tExeSQL.execSQL(strSQL);	
		count = tSSRS.getMaxRow();	
		
		String temp1[][] = tSSRS.getAllData();	
		
		for(int i=0;i<temp1.length;i++) 
		{
			for (int j=0;j<temp1[i].length;j++) 
			{
				System.out.println("temp1: "+temp1[i][j]);
				tempList.add(temp1[i][j]);
				allList.add(temp1[i][j]);
			}
		}
		System.out.println("**opeStr: " + opeStr);
	}
	String userStr = "'"+Operator+"'";
	Iterator it = allList.iterator(); //
	while(it.hasNext())
	{
		userStr = userStr +",'" + it.next()+"'";
	}
	System.out.println("AAAA "+userStr);
%>
