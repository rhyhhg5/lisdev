<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>

<html>

<%
        GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var date = "<%=PubFun.getCurrentDate()%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="FICertificateRBAppInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FICertificateRBAppInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./FICertificateRBAppSave.jsp" method=post name=fm target="fraSubmit">
          
      <table>   		  		
        <tr class=common>
	      <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFICertificateRBApp);"></IMG></td>
	      <td class=titleImg>凭证红冲申请信息录入：</td>
	</tr>
      </table>
   <Div  id= "divFICertificateRBApp" style= "display: ''">
      <table  class= common>
		
	<TR  class= common> 
	      <TD  class= title>申请人</TD>
	      <TD  class= input><Input class= common  name=Applicant  verify="申请人|notnull" elementtype=nacessary > </TD>
	      <TD  class= title>申请日期</TD>
	      <TD  class= input><Input class= 'coolDatePicker'   name=AppDate verify="申请日期|notnull&DATE" format='short'  elementtype=nacessary></TD>     
	</TR>	

	<TR  class= common>
		<!-- 
	      <TD  class= title>凭证类型编号</TD>
	      <TD  class= input ><Input class="codeno" name=CertificateId  
		      ondblclick="return showCodeList('certificateid',[this,CertificateIdName],[0,1]);" 
		      onkeyup="return showCodeList('certificateid',[this,CertificateIdName],[0,1]);" ><input class=codename name=CertificateIdName readonly=true elementtype=nacessary></TD>
		-->	      	
	      <TD  class= title>红冲原因类型</TD> 
	      <TD  class= input><Input class= codeno  name=ReasonType verify="红冲原因类型|notnull" 
		      ondblclick="return showCodeList('reasontype',[this,ReasonTypeName],[0,1]);" 
		      onkeyup="return showCodeList('reasontype',[this,ReasonTypeName],[0,1]);" ><input class=codename name=ReasonTypeName readonly=true elementtype=nacessary></TD>			
	</TR>

	<TR  class= common>
	      <TD class= title>业务号码类型</TD>
	      <TD class= input>
		  <Input class=codeno name=DetailIndexID verify="业务索引代码类型|NOTNULL" 
			  ondblClick="showCodeList('fiindexid',[this,DetailIndexName],[0,1],null,null,null,1);" 
			  onkeyup="showCodeList('fiindexid',[this,DetailIndexName],[0,1],null,null,null,null,1);"><input class=codename name=DetailIndexName readonly=true elementtype=nacessary>
	      </TD>			
	      <TD  class= title>业务号码</TD> 
	      <TD  class= input><Input class=common  name=BusinessNo verify="业务号码|notnull" elementtype=nacessary></TD>						  												
	</TR>
							  														
	<TR  class= common>
	      <TD  class= title>细节描述</TD>
	      <TD  class= common>
	          <textarea class= common  name=DetailReMark verifyorder="1" cols="50%" rows="5" witdh=50% verify="细节描述|notnull" elementtype=nacessary></textarea>
	      </TD>			
	</TR>	
	
     </table>
  </Div>
      <INPUT VALUE="申 请" class = cssButton TYPE=button onclick="ApplyInput();"> 
      <INPUT VALUE="申请查询" class = cssButton TYPE=button onclick="queryRBResultGrid();"> 
      <table>   		  		
        <tr class=common>
	      <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFICertificateRBAppTrace);");"></IMG></td>
	      <td class=titleImg>已申请凭证红冲信息：</td>
	</tr>
      </table>

   <Div  id= "divFICertificateRBAppTrace" style= "display: ''" align=center>
     <table  class= common>
       	<tr  class= common>
      	      <td text-align: left colSpan=1>
  		    <span id="spanRBResultGrid" >
  		    </span> 
  	      </td>
  	</tr>
      </table>
      <INPUT VALUE="首  页" class = cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="getLastPage();">    
  </Div>
  	
  	  	 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
