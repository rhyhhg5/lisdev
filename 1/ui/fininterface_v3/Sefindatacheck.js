//该文件中包含客户端需要处理的函数和事件

//程序名称：Fifindatacheck.js
//程序功能：元数据定义
//创建日期：2011/9/15
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//返回按钮对应的操作
function returnParent()
{
  var arrReturn = new Array();
	var tSel = DataCheckGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请您先选择一条记录，再点击返回按钮。" );
	else
	{

			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery1( arrReturn );
			}
			catch(ex)
			{
				//alert( "没有发现父窗口的afterQuery接口。" + ex );
				alert(ex.message);
			}
			top.close();

	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = DataCheckGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();
	//tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	//arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
		// 书写SQL语句
	var strSQL = "";
	if(fm.Ruletype.value=='01')
	{	
		strSQL = "select '01','质检规则' idname,ruledefid,CallPointID,(select codename from ldcode where codetype = 'fichecknode' and code=a.CallPointID) CallPointIDName,RuleState,(case RuleState when '1' then '启用' else '未启用' end) RuleStateName, " 
		strSQL = strSQL + " rulename,errtype,(select codename from ldcode where codetype = 'fierrortypea' and code=a.errtype) errtypeName from FIVerifyRuleDef a  where a.ruledefid ='"+DataCheckGrid.getRowColData(tRow-1,1)+"' ";	
		if(fm.CallPointID.value != '')
			strSQL = strSQL + " and a.CallPointID='"+fm.CallPointID.value+"'";
	} else
	{
		strSQL = "select '02','差异规则' idname,ruledefid,CallPointID,(select codename from ldcode where codetype = 'fichecknode' and code=a.CallPointID) CallPointIDName,RuleState,(case RuleState when '1' then '启用' else '未启用' end) RuleStateName, " 
		strSQL = strSQL + " rulename,errtype,(select codename from ldcode where codetype = 'fierrortypea' and code=a.errtype) errtypeName from FIVerifyRuleDef a  where a.ruledefid ='"+DataCheckGrid.getRowColData(tRow-1,1)+"' ";	
	}
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }  
	//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}
//关闭按钮
function closed(){

	top.close();

}

//删除按钮
function button144(){
}

//修改按钮
function button145(){
}
function RuleDY()
{
	  showInfo=window.open("./RuleInsert.jsp");
}

//查询按钮
function QueryDate(){

	if(fm.Ruletype.value==''){
			alert("请选择校验类型");
			return false;
	}
	if(fm.Ruletype.value=='01')
	{
		var strSQL = "select RuleDefID,rulename,(select codename from ldcode where codetype = 'fierrortypea' and code = errtype) from FIVerifyRuleDef where versionno='"+fm.VersionNo.value+"' and RuleType='"+fm.Ruletype.value+"' ";
		
		if(fm.CallPointID.value != '')
			strSQL = strSQL + " and CallPointID='"+fm.CallPointID.value+"'";	
	
	} else 
	{
		var strSQL = "select RuleDefID,rulename,(select codename from ldcode where codetype = 'fierrortypea' and code = errtype) from FIVerifyRuleDef where versionno='"+fm.VersionNo.value+"' and RuleType='"+fm.Ruletype.value+"'";
	}
	
 	turnPage.queryModal(strSQL,DataCheckGrid);
		//showInfo.close();
    if(DataCheckGrid.mulLineCount=="0"){
		alert("没有查询到数据");
		return false;
	} 
		
}

//定义SQL按钮
function button148(){
}

//备用定义按钮
function button147(){
}


//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//change状态
function changetype(){

	if(fm.all('Ruletype').value=="02")
	{
			
			fm.all('CallPointID').value = "";
			fm.all('CallPointIDName').value = "";
			document.getElementById("td1").style.display = "none";	
			document.getElementById("td2").style.display = "none";	
	} else if(fm.all('Ruletype').value=="01") {
	
			document.getElementById("td1").style.display = "";	
			document.getElementById("td2").style.display = "";		
	}

}
function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}
