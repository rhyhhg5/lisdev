<%
  //程序名称：FIVoucherFeeDefInit.jsp
  //程序功能：凭证费用定义
  //创建日期：2011/8/27
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">
function initForm(){
	try
	{	
		initpage();
		fm.all('pageflag').value='<%=request.getParameter("pageflag")%>';
		if("Q"==fm.pageflag.value)
		{
		
			buttonsdiv.style.display = 'none';
		} 
	}
	catch(re){
		alert("FIVoucherFeeDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initVoucherFeeGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="版本号";
		iArray[1][1]="80px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="凭证编码";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=1;

		iArray[3]=new Array();
		iArray[3][0]="凭证名称";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="费用编码";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=1;

		iArray[5]=new Array();
		iArray[5][0]="费用名称";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		iArray[6]=new Array();
		iArray[6][0]="借方科目";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=2;
		iArray[6][4]='finitem';
		iArray[6][5]="6|7";     //引用代码对应第几列，'|'为分割符
		iArray[6][6]="0|1";    //上面的列中放置引用代码中第几位值

      
		iArray[7]=new Array();
		iArray[7][0]="借方科目名称";
		iArray[7][1]="90px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="贷方科目";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=2;
		iArray[8][4]='finitem';
		iArray[8][5]="8|9";     //引用代码对应第几列，'|'为分割符
		iArray[8][6]="0|1";    //上面的列中放置引用代码中第几位值
		
		iArray[9]=new Array();
		iArray[9][0]="贷方科目名称";
		iArray[9][1]="90px";
		iArray[9][2]=100;
		iArray[9][3]=0;
		
		iArray[10]=new Array();
		iArray[10][0]="业务交易编码";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=0;


		VoucherFeeGrid = new MulLineEnter( "fm" , "VoucherFeeGrid" ); 

		VoucherFeeGrid.mulLineCount=1;
		VoucherFeeGrid.displayTitle=1;
		VoucherFeeGrid.canSel=0;
		VoucherFeeGrid.canChk=0;
		VoucherFeeGrid.hiddenPlus=1;
		VoucherFeeGrid.hiddenSubtraction=1;

		VoucherFeeGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
