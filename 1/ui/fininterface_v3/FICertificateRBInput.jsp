 <%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<html>

<%
     GlobalInput tGI = new GlobalInput();
     tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
      //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="FICertificateRBInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FICertificateRBInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >

  <form  method=post name=fm target="fraSubmit">          
  <table>   		  		
    <tr class=common>
	<td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFICertificateRBApp);"></IMG></td>
	<td class=titleImg>红冲申请记录：</td>
    </tr>
  </table>

    <Div  id= "divFICertificateRBApp" style= "display: ''" align=center>

      	<table  class= common>
       	   <tr  class= common>
      	  	<td text-align: left colSpan=1 >
  			<span id="spanRBResultGrid" ></span> 
  		</td>
  	   </tr>
    	</table>
           <INPUT VALUE="首  页" class = cssButton TYPE=button onclick="getFirstPage();"> 
           <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					
           <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();"> 
           <INPUT VALUE="尾  页" class = cssButton TYPE=button onclick="getLastPage();">          
           					
     </Div>

  <table>  
    <tr class=common>
	<td class=titleImg>凭证红冲申请明细信息：</td>
    </tr>  
  </table>  

    	 
     <table class= common>     
	<TR  class= common> 
	      <TD  class= title>申请人</TD>
	      <TD  class= input><Input class= common  name=Applicant readonly=true> </TD>
	      <TD  class= title>申请日期</TD>
	      <TD  class= input><Input class= 'coolDatePicker'   name=AppDate  format='short' readonly=true></TD>     
	      <TD class= title>业务号码类型</TD>
	      <TD class= input>
		  		<Input class=codeno name=DetailIndexID ondblClick="showCodeList('fiindexid',[this,DetailIndexName],[0,1]);" 
		  			onkeyup="showCodeList('fiindexid',[this,DetailIndexName],[0,1]);" readonly=true><input class=codename name=DetailIndexName readonly=true>
	      </TD>	
	</TR>	
	<TR  class= common>	      		
	      <TD  class= title>业务号码</TD> 
	      <TD  class= input><Input class=common  name=BusinessNo readonly=true></TD>						  												     	
	      <TD  class= title>红冲原因类型</TD> 
	      <TD  class= input>
	      		<Input class= codeno  name=ReasonType  ondblclick="return showCodeList('ReasonType',[this,ReasonTypeName],[0,1]);" 
	      		onkeyup="return showCodeList('ReasonType',[this,ReasonTypeName],[0,1]);"  readonly=true><input class=codename name=ReasonTypeName readonly=true >
	      </TD>			
	</TR>				  														
	<TR  class= common>
	      <TD  class= title>细节描述</TD>
	      <TD  class= common>
	          <textarea class= common  name=DetailReMark verifyorder="1" cols="30%" rows="3" witdh=30% readonly=true></textarea>
	      </TD>
	      <TD  class= title></TD>
	      <TD  class= input> </TD>	      			
	</TR>	     
     </table>  	

  	 <hr></hr>
  	 
     <INPUT VALUE="红冲处理" class = cssButton TYPE=button onclick="AppDeal();"> 
     <INPUT VALUE="申请撤消" class = cssButton TYPE=button onclick="AppDelete();"> 
     <INPUT VALUE="红冲确认" class = cssButton TYPE=button onclick="ReConfirm();"> 

<Input class=common value = "" name=CertificateId  type =hidden >
<input class=common value = "" name=CertificateIdName type =hidden>
<Input class=common value = "" name="AppNo" type=hidden>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
