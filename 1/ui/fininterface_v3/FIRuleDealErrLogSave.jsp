<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FIRuleDealErrLogSave.jsp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.fininterface_v3.*" %>
  <%@page contentType="text/html;charset=GBK" %>
<%
  
  request.setCharacterEncoding("GBK");
  
  String strDelErrSerialNo=request.getParameter("DelErrSerialNo");
  System.out.println("得到流水号码" + strDelErrSerialNo);

  String FlagStr = "Fail";
  String Content = "";
  
  FIRuleDealErrLogSchema oDealErrLogSchema = new FIRuleDealErrLogSchema();
  oDealErrLogSchema.setErrSerialNo(strDelErrSerialNo);
  
  System.out.println(oDealErrLogSchema.encode());
  VData oData = new VData();
  oData.addElement(oDealErrLogSchema);
  FIRuleDealErrDataBL oDealErrDataBL = new FIRuleDealErrDataBL();
  if(oDealErrDataBL.submitData(oData,"INSERT"))
  {
     //处理成功
     System.out.println("Del Succ");
     Content = " 保存成功! ";
     FlagStr = "Succ";
  }else{
  	System.out.println("Del Fail");
  	Content = " 保存失败，原因是:" + oDealErrDataBL.mErrors.getFirstError();
    FlagStr = "Fail";
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

