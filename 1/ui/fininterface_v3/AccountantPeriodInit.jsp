<%
//程序名称：AccountantPeriodInit.jsp
//程序功能：会计期间管理
//创建日期：2008-08-04
//创建人  ：范昕  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>                        
<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
  	fm.reset();     
    fm.all('Year').value = '';
    fm.all('Month').value = '';  
  	fm.all('StartDay').value = '';
    fm.all('EndDay').value='';           
    fm.all('State').value = '';   
  }
  catch(ex)
  {
    alert("在AccountantPeriodInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
  }
  catch(re)
  {
    alert("在AccountantPeriodInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
