<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>                    

<script language="JavaScript">
var pageflag = "";
var apptype = "";
var maintno = "";
var RuleID = "";
var VersionNo = "";
var RuleLevel = "";
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

	try
	{
		pageflag = '<%=request.getParameter("pageflag")%>';
		VersionNo = '<%=request.getParameter("VersionNo")%>';
		apptype = '<%=request.getParameter("apptype")%>';
		maintno = '<%=request.getParameter("maintno")%>';
		RuleID = '<%=request.getParameter("RuleID")%>';
		RuleLevel = '<%=request.getParameter("RuleLevel")%>';


		
		fm.pageflag.value = pageflag;
		fm.VersionNo.value = VersionNo;
		if("X"==pageflag)
		{
			versionquerybutton.style.display = 'none';
			finitemquerydiv.style.display = 'none';

			
		}
		else if ("C"==pageflag)
		{
			initpage();
			fm.all('updatebutton').disabled = true;		
			fm.all('deletebutton').disabled = true;  	
		}
  	  	
		       
	}
	catch(ex)
	{
    alert("在FinItemDefInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}



function initSelBox()
{  
  try                 
  {

  }
  catch(ex)
  {
    alert("在FinItemDefInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                   

function initForm()
{
  try
  {
    initInpBox(); 
    initSelBox();
  }
  catch(re)
  {
    alert("FinItemDefInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initBusTypeInfoGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=1;

		iArray[1]=new Array();
		iArray[1][0]="补充信息规则编码";
		iArray[1][1]="50px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="补充信息规则名称";
		iArray[2][1]="80px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="补充信息规则对象";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="规则处理类型";
		iArray[4][1]="50px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="规则描述";
		iArray[5][1]="50px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="版本编号";
		iArray[6][1]="50px";
		iArray[6][2]=100;
		iArray[6][3]=3;


		BusTypeInfoGrid = new MulLineEnter( "fm" , "BusTypeInfoGrid" ); 

		BusTypeInfoGrid.mulLineCount=2;
		BusTypeInfoGrid.displayTitle=1;
		BusTypeInfoGrid.canSel=1;
		BusTypeInfoGrid.canChk=0;
		BusTypeInfoGrid.hiddenPlus=1;
		BusTypeInfoGrid.hiddenSubtraction=1;			
		BusTypeInfoGrid.selBoxEventFuncName="dataselect";

		BusTypeInfoGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}


</script>