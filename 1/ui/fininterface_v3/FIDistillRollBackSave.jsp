<%
//程序名称
//程序功能：
//创建日期：
//创建人  ：jw
//更新记录：
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 
<%@page contentType="text/html;charset=GBK" %>
<%
	CErrors tError = null;
	String Content = "";
	String FlagStr = "";
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");

	String tBatchNo = request.getParameter("batchno");
	String tErrAppNo = request.getParameter("ErrAppNo");
	String tProcessNode = request.getParameter("ProcessNode");
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("BatchNo",tBatchNo);
	tTransferData.setNameAndValue("ErrAppNo",tErrAppNo);
	tTransferData.setNameAndValue("ProcessNode",tProcessNode);
	VData tVData = new VData();
	tVData.add(tG);
	tVData.add(tTransferData);

	FIDistillRollBackUI tFIDistillRollBackUI = new FIDistillRollBackUI();  
	try
	{ 
		if(!tFIDistillRollBackUI.submitData(tVData,""))
		{
		   Content =  "失败，原因是:" + tFIDistillRollBackUI.mErrors.getFirstError();
		   FlagStr = "Fail";         
		}
	}
	catch(Exception ex)
	{
		Content =  "失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
	    Content = "数据回退成功";
	    FlagStr = "Succ";
	}
%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
