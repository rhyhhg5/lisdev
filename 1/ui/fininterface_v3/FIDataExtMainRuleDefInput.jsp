<html> 
<%
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String mStartDate = "";
	String mSubDate = "";
	String mEndDate = "";
	GlobalInput tGI = new GlobalInput();
	//PubFun PubFun=new PubFun();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
	System.out.println("2"+tGI.ManageCom);
%>

<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
<script src="../common/Calendar/Calendar.js"></script>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="FIDataExtMainRuleDefInput.js"></SCRIPT>  
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="FIDataExtMainRuleDefInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >

<form name=fm target=fraSubmit method=post>
<table>
  	<tr>    		 
  		 <td class= titleImg>
      		 数据抽取主规则定义
     	 </td>   		 
  	</tr>
</table>
<hr></hr>  
<table class= common border=0 width=100%>
	<tr class= common>
		<TD class= title>主规则编号</TD>
		<TD class=input>
				<Input class=common name=RuleID  elementtype=nacessary verify="主规则编号|len<=20">
		</TD>
		<TD class= title>主规则名称</TD>
	    <TD class= input>                                            
	  		 <Input class=common name=RuleName  elementtype=nacessary verify="主规则名称|len<=40">
	    </TD>				
	</tr>
	<tr class= common>
		<TD class= title>主规则类型</TD>
	    <TD class= input>
			<Input class=codeno name= RuleType readonly=true verify="主规则类型|NOTNULL" CodeData="0|^1|资产^2|负债^6|损益" ondblClick="showCodeListEx('FinItemType',[this,FinItemTypeMame],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('FinItemType',[this,FinItemTypeMame],[0,1],null,null,null,[1]);"><input class=codename name=FinItemTypeMame readonly=true elementtype=nacessary>
	    </TD>
		<TD class= title>主规则对象</TD>
		<TD class=input>
			<Input class=common name=ItemMainCode  elementtype=nacessary verify="主规则对象|len<=20">
		</TD>		    		    
	</tr>
	<tr class= common>
		<TD class= title>描述</TD>
	    <TD class= input>
	  		<Input class=common name=ReMark verify="描述|len<=500" >
	    </TD>
	    <td class=title>处理方式</td>
		<td class=input>
			<Input class=codeno name= RuleDealMode readonly=true ondblClick="showCodeList('ruledealmode',[this,RuleDealModeName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('ruledealmode',[this,RuleDealModeName],[0,1],null,null,null,[1]);"><input class=codename name=RuleDealModeName readonly=true elementtype=nacessary>
		</td>	    		    
	</tr>
	<tr class= common>
		<td class=title>规则状态</td>
		<td class=input><input class=common name=rulestate></td>
	</tr>
</table>
<div id=sqldiv style="display:'none'">
	<td class=title>处理SQL</td>
	<textarea rows=2 cols=50% name = RuleDealSQL></textarea>
</div> 
<div id=classdiv style="display:'none'">
	<td class=title>处理类</td>
	<td class=input><input class=common name=RuleDealClass ></td>
</div> 		
<div id=filediv style="display:'none'">
	<td class=title>处理文件</td>
	<td class=input><input class=common name=RuleFileName ></td>
</div> 	 
  	<p>
  	<div id=addbuttondiv style="display:'none'">
  	<INPUT VALUE="添  加" TYPE=button class= cssbutton onclick="return addClick();">  
  	<INPUT VALUE="重  置" TYPE=button class= cssbutton name="resetbutton" onclick="return resetAgain();">
  	</div>
  	<div id=updatebuttondiv style="display:'none'">
  	<INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
  	<INPUT VALUE="重  置" TYPE=button class= cssbutton name="resetbutton" onclick="return resetAgain();">  
  	</div>
  	<div id=deletebuttondiv style="display:'none'">
  	<INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
  	<INPUT VALUE="重  置" TYPE=button class= cssbutton name="resetbutton" onclick="return resetAgain();">  
  	</div>
  	<div id=allbuttondiv style="display:'none'">
    <INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
    <INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
    <INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
    <INPUT VALUE="重  置" TYPE=button class= cssbutton name="resetbutton" onclick="return resetAgain();">   
	</div>
    <hr></hr>
    
    <INPUT VALUE="明细规则定义" TYPE=button class= cssbutton name="intobutton1" onclick="queryClick2()">   
    <INPUT VALUE="主规则补充规则定义" TYPE=button class= cssbutton name="intobutton2" onclick="return intoDetailDef();">
    <!-- 定义页面参数 -->
    <input type=hidden name=pageflag>
    <input type=hidden name=maintno>
    <input type=hidden name=apptype>
    <INPUT type=hidden name=hideOperate value=''>
    <INPUT type=hidden name=VersionNo value=''>
    <INPUT type=hidden name=VersionState value='01'> <!-- VersionState用来存版本状态：01，02，03；而VersionState2存版本状态：正常、维护、删除-->
    <INPUT type=hidden name=DealMode value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>


</body>
</html>
