//该文件中包含客户端需要处理的函数和事件

//程序名称：FIDataExtPlanExe.js
//程序功能：数据抽取计划执行
//创建日期：2011-8-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();



//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

function initQuery() 
{
	var strSql ="select versionno,ruledefid,ruledefname,remark from FIDataExtractDef a where ruletype = '00' and rulestate = '1' ";
	turnPage.queryModal(strSql, FIDataExtractGrid);
} 

function dataExt()
{
		if(fm.IndexType.value == null || fm.IndexType.value == "")
		{
			alert("校验类型号不能为空！");
			return false;
		}
		if(fm.IndexidNo.value==null||fm.IndexidNo.value=="")
		{
			alert("业务索引号码不能为空！");
			return false;
		}

	fm.action="ErrorUpdateTest0000000Save.jsp";
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    showInfo.close();
  //释放“增加”按钮

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;

    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //resetForm();
		mOperate="";
  }
  else
  {
    //alert(content);
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
//  initFIDataExtractGrid();
//	initQuery();
	initInputBox();
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}


//代码选择后处理
function afterCodeSelect( cName, Filed)
{   
	if(cName=='fiexttype')
	{
		 if('00'==Filed.value)
		 {
		 	div1.style.display='';
		    tr2.style.display='none';
		 }
		 else
		{
			 div1.style.display='none';
			 tr2.style.display='';
		}
	}
}
