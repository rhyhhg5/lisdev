//该文件中包含客户端需要处理的函数和事件

//程序名称：FIMetaDataDef.js
//程序功能：元数据属性定义
//创建日期：2011/9/13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//查询按钮
function InitPage()
{
	var QuerySql = "select MetadataNo,AttNo,AttName,(select codename from ldcode where codetype = 'fiattype' and code = AttType),(select codename from ldcode where codetype = 'fiattstate' and code = state),AttType,state from FIMetadataAttDef where MetadataNo = '"+tMetaDataNo+"' order by AttNo";
	//alert(QuerySql);
	
	turnPage.queryModal(QuerySql, MetadataAttGrid);
}


//增加按钮
function addClick()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	//判断是否是新值插入
	var checksql="select count(*) from FIMetadataAttDef where MetadataNo = '"+fm.MetadataNo.value+"' and attno = '"+fm.AttNo.value+"'";
	var re = easyExecSql(checksql);
	
	if(re!="0")
	{
		alert("元数据编码已存在！");
		return false;
	}
	//为了防止双击，点击增加后，屏蔽"增加"按钮
	mOperate="INSERT||MAIN";
	submitForm();
}

//删除按钮
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AttNo").value==null)||(trim(fm.all("AttNo").value)==''))
    alert("请确定要删除的属性编码！");
  
  else
  {

    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}

//修改按钮
function updateClick()
{
  //提交前的检验
  if (!beforeSubmit()) //beforeSubmit()函数
  {
  	return false;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

function submitForm()
{ 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.action="./FIMetaDataAttDefSave.jsp";
  //lockButton(); 
  fm.submit(); //提交
}

//保存按钮
function SaveDate()
{
	var tSel = MetadataAttGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		//top.close();
		alert( "请您先录入一条记录，再点击保存。" );
		return;
	}
}

//值域定义按钮
function gotodefin(){

	var tSel = MetadataAttGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
	{
		//top.close();
		alert( "请您先选择一条记录，再点击值域定义。" );
		return;
	}
	var MetadataNo = MetadataAttGrid.getRowColData(tSel-1,1);
	var AttNo = MetadataAttGrid.getRowColData(tSel-1,2);
	//alert(AttNo);
	showInfo=window.open("./FrameFIMetaDataValueDefInput.jsp?AttNo="+AttNo+"&MetadataNo="+MetadataNo);	
}


//提交前的校验、计算  
function beforeSubmit()
{
	if(fm.MetadataNo.value == '')
	{
		alert("元数据编码不能为空！");
		return false;
	}
	if(fm.AttNo.value == '')
	{
		alert("属性编码不能为空！");
		return false;
	}
	if(fm.AttName.value == '')
	{
		alert("属性名称不能为空！");
		return false;
	}
	if(fm.AttType.value == '')
	{
		alert("属性类型不能为空！");
		return false;
	}
	if(fm.State.value == '')
	{
		alert("属性状态不能为空！");
		return false;
	}
	return true;
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
  location.reload();
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}

function getattdata()
{
	var tSel = MetadataAttGrid.getSelNo();
	//fm.MetadataNo.value = MetadataAttGrid.getRowColData(tSel-1,1);
	fm.AttNo.value = MetadataAttGrid.getRowColData(tSel-1,2);
	fm.AttName.value = MetadataAttGrid.getRowColData(tSel-1,3);
	fm.AttType.value = MetadataAttGrid.getRowColData(tSel-1,6);
	fm.State.value = MetadataAttGrid.getRowColData(tSel-1,7);
	
	showCodeName();
}

