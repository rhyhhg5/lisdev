<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InfoFinItemAssociatedSave.jsp
//程序功能：科目关联专项定义保存页面
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
	String pageflag = request.getParameter("pageflag");
	System.out.println("pageflag============="+pageflag);
	//输入参数
	FIInfoFinItemAssociatedSchema tFIInfoFinItemAssociatedSchema = new FIInfoFinItemAssociatedSchema();
	FMInfoFinItemAssociatedSchema tFMInfoFinItemAssociatedSchema = new FMInfoFinItemAssociatedSchema();
	
	FIInfoFinItemAssociatedUI tFIInfoFinItemAssociated = new FIInfoFinItemAssociatedUI();
	FMInfoFinItemAssociatedUI tFMInfoFinItemAssociatedUI = new FMInfoFinItemAssociatedUI();

	//输出参数
	CErrors tError = null;
	String tOperate=request.getParameter("hideOperate");
	tOperate=tOperate.trim();
	String mAssociatedID = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	//
	tFIInfoFinItemAssociatedSchema.setVersionNo(request.getParameter("VersionNo"));
	tFIInfoFinItemAssociatedSchema.setFinItemID(request.getParameter("FinItemID"));  
	tFIInfoFinItemAssociatedSchema.setAssociatedID(request.getParameter("AssociatedID"));
	tFIInfoFinItemAssociatedSchema.setAssociatedName(request.getParameter("AssociatedName"));
	tFIInfoFinItemAssociatedSchema.setReMark(request.getParameter("ReMark"));
	//
	tFMInfoFinItemAssociatedSchema.setMaintNo(request.getParameter("maintno"));
	tFMInfoFinItemAssociatedSchema.setVersionNo(request.getParameter("VersionNo"));
	tFMInfoFinItemAssociatedSchema.setFinItemID(request.getParameter("FinItemID"));  
	tFMInfoFinItemAssociatedSchema.setAssociatedID(request.getParameter("AssociatedID"));
	tFMInfoFinItemAssociatedSchema.setAssociatedName(request.getParameter("AssociatedName"));
	tFMInfoFinItemAssociatedSchema.setReMark(request.getParameter("ReMark"));

	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	
	tVData.add(tG);
	try
	{
		System.out.println("come========== in "+tFIInfoFinItemAssociatedSchema.getVersionNo());
		System.out.println("come========== in "+tFIInfoFinItemAssociatedSchema.getFinItemID());  
		System.out.println("come========== in "+tFIInfoFinItemAssociatedSchema.getAssociatedID());
		System.out.println("come========== in "+tFIInfoFinItemAssociatedSchema.getAssociatedName());
		System.out.println("come========== in "+tOperate);
		if("C".equals(pageflag))
		{
			tVData.addElement(tFIInfoFinItemAssociatedSchema);
			if(!tFIInfoFinItemAssociated.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFIInfoFinItemAssociated.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		else if("X".equals(pageflag))
		{
			tVData.addElement(tFMInfoFinItemAssociatedSchema);
			if(!tFMInfoFinItemAssociatedUI.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFMInfoFinItemAssociatedUI.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		
		System.out.println("come out" + tOperate);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = tFIInfoFinItemAssociated.mErrors;
		if (!tError.needDealError())
		{
			mAssociatedID = tFIInfoFinItemAssociated.getAssociatedID();
			Content = " 操作已成功! ";
			FlagStr = "Succ";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

