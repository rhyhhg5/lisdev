<%
  //程序名称：FIBusTypeDefInit.jsp
  //程序功能：业务交易定义
  //创建日期：2011/8/25
  //创建人  ：董健
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">
// 输入框的初始化（单记录部分）
var pageflag="";
var apptype="";
var BusTypeId="";
var maintno="";
function initInpBox()
{ 
	try
	{
		
		//输入框重置
		fm.BusTypeId.value='';
		fm.BusTypeName.value='';
		fm.FIBusType.value='';
		fm.FIBusTypename.value='';
		fm.FIDetailType.value='';
		fm.FIDetailTypeName.value='';
		fm.ObjectID.value='';
		fm.ObjectName.value='';
		fm.FIIndexCode.value='';
		fm.FIIndexName.value='';
		
		fm.maintno.value = maintno;
		fm.apptype.value=apptype;
		fm.pageflag.value=pageflag;
	}
	catch(ex)
	{
		alert("在FIBusTypeDefInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}

function initForm()
{
	initInpBox();	
}

</script>
