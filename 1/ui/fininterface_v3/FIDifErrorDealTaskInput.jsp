<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>

<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var date = "<%=PubFun.getCurrentDate()%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="FIDifErrorDealTaskInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FIDifErrorDealTaskInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
<form action="./FIDifErrorDealTaskSave.jsp" method=post name=fm target="fraSubmit">

<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divQuery);"></td>
		<td class=titleImg>查询条件</td>
	</tr>
</table>

<Div id="divQuery" style="display: ''" align=center>
	<table class=common align=center>
		<TR class=common>
			
			<TD class=title>校检批次</TD>
			<TD class=input>
			  	<Input class="common" name="CheckBatchNo" ></TD>					
			<TD class=title>校检事件节点</TD>
			<TD class=input>
			  	<Input class=codeno name=CallPointID ondblClick="showCodeList('fieventnode',[this,CallPointIDName],[0,1]);" 
			  		onkeyup="showCodeList('fieventnode',[this,CallPointIDName],[0,1]);" readonly=true><input class=codename name=CallPointIDName readonly=true>
			</TD>	
			<TD class=title>状态</TD>
			<TD class=input>
			  	<Input class=codeno name=State value = '0' ondblClick="showCodeList('fierrstate',[this,StateName],[0,1]);" 
			  		onkeyup="showCodeList('fierrstate',[this,StateName],[0,1]);" readonly=true><input class=codename name=StateName value = '待分配' readonly=true>
			</TD>	
		</TR>
		<TR class=common>	
			<TD  class= title>CQ号</TD>
			<TD  class= input>
				<Input class= common  name="cqNo" > 
			</TD>			
			<TD class=title>起始日期</TD>
			<TD class=input>
				<Input class="coolDatePicker" dateFormat="short" name=StartDate verify="起始日期|date">
			</TD>
			<TD class=title>终止日期</TD>
			<TD class=input>
				<Input class="coolDatePicker" dateFormat="short" name=EndDate verify="终止日期|notnull&date">
			</TD>
			<TD class=title></TD>
			<TD class=input></TD>
		</TR>
	</table>
</Div>
<br>
<input value=" 查  询 " class=cssButton type=button onclick="QueryDate();">
 	<br><br>
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divTaskList);"></td>
		<td class=titleImg>待处理任务列表：</td>
	</tr>
</table>

<Div  id= "divTaskList" style= "display: ''" align=center>
	<table  class= common>
      		<tr  class= common>
			<td text-align: left colSpan=1>
  		    	<span id="spanRBResultGrid" ></span> 
			</td>
		</tr>
	</table>
	<INPUT VALUE="首  页" class = cssButton TYPE=button onclick="getFirstPage();"> 
	<INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					
	<INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();"> 
	<INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="getLastPage();">    
</Div> 
 
<table class="common">
	<tr class="common">
	  	<td class="title" align="right">处理方式</td>
	  	<td class="input">
		  	<Input class=codeno name=DealType verify="处理方式|NOTNULL"   
		  		ondblClick="showCodeList('fierrordealb',[this,DealTypeName],[0,1]);" 
		  		onkeyup="showCodeList('fierrordealb',[this,DealTypeName],[0,1]);" readonly=true><input class=codename name=DealTypeName readonly=true>	  	
		</td>
		<TD  class= title>CQ号</TD>
		<TD  class= input>
			<Input class= common  name=CQNo > 
		</TD>	
	  	<td></td><td></td>
	</tr>
	<TR  class= common>
		<TD  class= title>处理描述</TD>
		<TD  class= common>
		    <textarea class= common  name=DetailReMark verifyorder="1" cols="50%" rows="4" witdh=30% verify="细节描述|notnull" elementtype=nacessary></textarea>
		</TD>			
	</TR>  
</table>
<br>
    	
<input value="处理分配" class=cssButton type=button onclick="ApplyInput();">  
<Input class="common" name="ErrSerialNo" type=hidden></TD>		  	 
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
