//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();
var showInfo;
var mDebug="0";
var Action;
var tRowNo=0;

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  initMoveDataGrid();  	
  initQuery(); 
}

function initQuery() 
{

  var strSQL = "select BatchNo,MakeDate,cast(StartDate as varchar(10))||'至'||cast(EndDate as varchar(10)),Operator from FIVoucheManage where operator = 'cwjk' order by BatchNo"	  
  turnPage1.queryModal(strSQL, MoveDataGrid);
} 
 
function SubmitForm()
{
  
     var i = 0;
     var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
     fm.submit(); //提交
}

