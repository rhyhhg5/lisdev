<html>
<%
//程序名称 :FIQualityErrorRegInput.jsp
//程序功能 :
//创建人 :
//创建日期 :
//
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput tGI1 = new GlobalInput();
tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
%>
<script>
  var comcode = "<%=tGI1.ComCode%>";
  var mOperator =  "<%=tGI1.Operator%>";
</script>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
<SCRIPT src = "FIQualityErrorRegInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FIQualityErrorRegInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="" method=post name=fm target="fraSubmit">
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divErrorReg);"></td>
		<td class=titleImg>质检错误登记</td>
	</tr>
</table>	
<Div id="divErrorReg" style="display: ''" align=center>

	<table  class= common>								  														
		<TR  class= common> 
			<TD  class= title>申请人</TD>
			<TD  class= input>
				<Input class= common  name=Applicant  verify="申请人|notnull" elementtype=nacessary > 
			</TD>

			<TD class= title>业务交易编码</TD>
			<TD class= input>
				<Input class=codeno name=BusTypeCode verify="业务交易编码|NOTNULL" 
				    ondblclick="return showCodeList('fibntype',[this,BusTypeCodeName],[0,1]);" 
					onkeyup="return showCodeList('fibntype',[this,BusTypeCodeName],[0,1]);" ><input class=codename name=BusTypeCodeName readonly=true elementtype=nacessary>
			</TD>
			<TD  class= title>CQ号</TD>
			<TD  class= input>
				<Input class= common  name=CQNo > 
			</TD>							
		</TR>
		<TR  class= common>
					  		     		     
			<TD class= title>业务索引类型</TD>
			<TD class= input>
				<Input class=codeno name=DetailIndexID verify="业务索引类型|NOTNULL" 
				      ondblclick="return showCodeList('fiindexid',[this,DetailIndexName],[0,1]);" 
				      onkeyup="return showCodeList('fiindexid',[this,DetailIndexName],[0,1]);" ><input class=codename name=DetailIndexName readonly=true elementtype=nacessary>
			</TD>
			<TD  class= title>业务号码</TD> 
			<TD  class= input><Input class=common  name=BusinessNo verify="业务号码|notnull" elementtype=nacessary></TD>	
			<TD  class= title>错误节点</TD> 
			<TD class=input><Input class=codeno name=ProcessNode  verify="错误节点|NOTNULL"
				ondblclick="showCodeList('fieventnode',[this,ProcessNodeName],[0,1],null,null,null,1,null);"
				onkeyup="showCodeListKey('fieventnode',[this,ProcessNodeName],[0,1],null,null,null,1,null);"><input class=codename name=ProcessNodeName readonly=true elementtype=nacessary>
			</TD>					  												
		</TR>
		<TR  class= common>
			<TD  class= title>错误描述</TD>
			<TD  class= common>
			    <textarea class= common  name=DetailReMark verifyorder="1" cols="50%" rows="5" witdh=50% verify="细节描述|notnull" elementtype=nacessary></textarea>
			</TD>			
		</TR>
	</table>
	<br>
	<table class=common>
		<TR class=input>
			<TD class=common>
				<input type=button class=cssButton	value=" 保  存 " onclick="savebutton()">
			</TD>					
		</TR>
	</table>

</Div>
<input type=hidden name=CheckType value='00'>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>