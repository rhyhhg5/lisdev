<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIBusFeeTypeDefSave.jsp
//程序功能：业务交易费用定义
//创建日期：2011/8/25
//创建人  ： 董健
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>

<%
	//接收信息，并作校验处理。
	request.setCharacterEncoding("GBK");
	
	String tMaintNo=request.getParameter("maintno");
	String tBusTypeId=request.getParameter("BusTypeId");
	String tFIFeeCode=request.getParameter("FIFeeCode");
	String tFIFeeCodeName=request.getParameter("FIFeeCodeName");
	String tFIFeeRemark=request.getParameter("FIFeeRemark");
	String tpageflag=request.getParameter("pageflag");
	
	String transact = request.getParameter("fmtransact");
	String tFinGDType = request.getParameter("FinGDType");

	System.out.println("tMaintNo--------------->"+tMaintNo);
	System.out.println("tBusTypeId--------------->"+tBusTypeId);
	System.out.println("tFIFeeCode--------------->"+tFIFeeCode);
	System.out.println("tFIFeeCodeName--------------->"+tFIFeeCodeName);
	System.out.println("tFIFeeRemark--------------->"+tFIFeeRemark);
	System.out.println("tpageflag--------------->"+tpageflag);
	System.out.println("transact--------------->"+transact);


//输入参数

FIBusFeeTypeDefUI tFIBusFeeTypeDefUI = new FIBusFeeTypeDefUI();
//FMBusFeeTypeDefUI tFMBusFeeTypeDefUI = new FMBusFeeTypeDefUI(); 

//输出参数
CErrors tError = null;
String tRela  = "";                
String FlagStr = "";
String Content = "";

GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getAttribute("GI");


try
{
	//准备传输数据VData
	VData tVData = new VData();
	//添加全局变量
	tVData.add(tG);

	FIBnFeeTypeDefSchema tFIBusFeeTypeDefSchema = new FIBnFeeTypeDefSchema();
	FMBnFeeTypeDefSchema tFMBusFeeTypeDefSchema = new FMBnFeeTypeDefSchema();
	
	if("X".equals(tpageflag))
	{
		tFMBusFeeTypeDefSchema.setMaintNo(tMaintNo);
		tFMBusFeeTypeDefSchema.setCostID(tFIFeeCode);
		tFMBusFeeTypeDefSchema.setCostName(tFIFeeCodeName);
		tFMBusFeeTypeDefSchema.setBusinessID(tBusTypeId);
		tFMBusFeeTypeDefSchema.setCostType(tFinGDType);//归档类型
		
		tVData.add(tFMBusFeeTypeDefSchema);
		
//		tFMBusFeeTypeDefUI.submitData(tVData,transact);
	}
	else if("C".equals(tpageflag))
	{
		tFIBusFeeTypeDefSchema.setCostID(tFIFeeCode);
		tFIBusFeeTypeDefSchema.setVersionNo("00000000000000000001");
		tFIBusFeeTypeDefSchema.setCostName(tFIFeeCodeName);
		tFIBusFeeTypeDefSchema.setBusinessID(tBusTypeId);
		tFIBusFeeTypeDefSchema.setRemark(tFIFeeRemark);
		tFIBusFeeTypeDefSchema.setCostType(tFinGDType);//归档类型
		
		tVData.add(tFIBusFeeTypeDefSchema);
		
		tFIBusFeeTypeDefUI.submitData(tVData,transact);
	}
	else
	{
		
	}
	
}
catch(Exception ex)
{
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}
 
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tFIBusFeeTypeDefUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 保存成功! ";
		FlagStr = "Success";
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
 
//添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
