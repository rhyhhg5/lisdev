<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：VoucherTypeQuery.jsp
//程序功能：版本信息查询页面
//创建日期：2008-08-18
//创建人  ：FanXin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./VoucherTypeQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./VoucherTypeQueryInit.jsp"%>
<script>
	var versionno = '<%=request.getParameter("versionno")%>';
</script>


<title>版本信息查询</title>
</head>
<body onload="initForm();initElementtype();">
  <form  method=post name=fm target="fraSubmit">
  <table>
  <tr>
    <td class="titleImg" >业务类型定义</td>
  </tr>
</table>
  <table  class= common>
		<tr class= common>
         <TD class= title>
					  版本号
				 </TD>
				 <TD class=input>
				 	 <Input class=common name=VersionNo id="VersionNo" readonly>
				 </TD>
         <TD  class= title>
          凭证编码
         </TD>
         <TD  class= input>
            <Input class="common"  name="voucherid" >
         </TD>    
               <TD  class= title>
          凭证名称
         </TD>
         <TD  class= input>
            <Input class="common"  name="vouchername" >
         </TD>        
		</tr>
				
		<tr class= common>
         <TD  class= title>凭证类型</TD>
         <TD  class= input>
	    	  <input class="codeno" name="vouchertype" ondblclick="return showCodeList('fivouchertype',[this,vouchertypename],[0,1]);" 
	    		onkeyup="return showCodeListKey('fivouchertype',[this,vouchertypename],[0,1]);" readonly = "true"><input class="codename" name="vouchertypename" readonly = "true" elementtype=nacessary>
         </TD>   
      
		</tr>		
      </table>
          <INPUT VALUE="查  询" TYPE=button onclick="busTypeQuery();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
                  
   <table>    	
    <tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFIRulesVersion2);">
    		</td>
    		<td class= titleImg>
    			业务类型定义信息查询结果
    		</td>
    	</tr>
   </table>

    
  	<Div  id= "divFIRulesVersion2" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					 <span id="spanBusiTypeGrid" >
  					 </span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>  

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

