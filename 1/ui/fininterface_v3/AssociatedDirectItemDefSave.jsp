 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AssociatedDirectItemDefSave.jsp
//程序功能：科目专项定义保存页面
//创建日期：2011-9-26
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
	request.setCharacterEncoding("GBK");
	
	String pageflag = request.getParameter("pageflag");
	//输入参数
	FIAssociatedDirectItemDefSchema tFIAssociatedDirectItemDefSchema = new FIAssociatedDirectItemDefSchema();
	FMAssociatedDirectItemDefSchema tFMAssociatedDirectItemDefSchema = new FMAssociatedDirectItemDefSchema();
	
	FIAssociatedDirectItemDefUI tFIAssociatedDirectItemDef = new FIAssociatedDirectItemDefUI();
	FMAssociatedDirectItemDefUI tFMAssociatedDirectItemDefUI = new FMAssociatedDirectItemDefUI();

	//输出参数
	CErrors tError = null;
	String tOperate=request.getParameter("hideOperate");
	tOperate=tOperate.trim();
	String mAssociatedID = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");  
	//
	tFIAssociatedDirectItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFIAssociatedDirectItemDefSchema.setColumnID(request.getParameter("ColumnID"));
	tFIAssociatedDirectItemDefSchema.setSourceTableID(request.getParameter("SourceTableID"));
	tFIAssociatedDirectItemDefSchema.setSourceColumnID(request.getParameter("SourceColumnID"));  
	tFIAssociatedDirectItemDefSchema.setReMark(request.getParameter("ReMark"));  
	//
	tFMAssociatedDirectItemDefSchema.setMaintNo(request.getParameter("maintno"));
	tFMAssociatedDirectItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFMAssociatedDirectItemDefSchema.setColumnID(request.getParameter("ColumnID"));
	tFMAssociatedDirectItemDefSchema.setSourceTableID(request.getParameter("SourceTableID"));
	tFMAssociatedDirectItemDefSchema.setSourceColumnID(request.getParameter("SourceColumnID"));  
	tFMAssociatedDirectItemDefSchema.setReMark(request.getParameter("ReMark"));  

	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	
	tVData.add(tG);
	try
	{
		System.out.println("come========== in"+tFIAssociatedDirectItemDefSchema.getVersionNo());    
		System.out.println("come========== in"+tFIAssociatedDirectItemDefSchema.getColumnID()); 
		if("C".equals(pageflag))
		{
			tVData.addElement(tFIAssociatedDirectItemDefSchema);
			if(!tFIAssociatedDirectItemDef.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFIAssociatedDirectItemDef.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		else if("X".equals(pageflag))
		{
			tVData.addElement(tFMAssociatedDirectItemDefSchema);
			if(!tFMAssociatedDirectItemDefUI.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFMAssociatedDirectItemDefUI.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		
		System.out.println("come out" + tOperate);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = tFIAssociatedDirectItemDef.mErrors;
		if (!tError.needDealError())
		{
			mAssociatedID = tFIAssociatedDirectItemDef.getColumnID();
			Content = " 操作已成功! ";
			FlagStr = "Succ";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

	//添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

