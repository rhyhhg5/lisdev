<%
//程序名称：VersionRuleInputInit.jsp
//程序功能：财务规则版本控制
//创建日期：2008-08-21
//创建人  ：范昕  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                         
<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
  	fm.reset();
  	fm.all('VersionNo').value = '';
  	fm.all('VersionState').value = '';
  	fm.all('StartDate').value = '';
  	fm.all('EndDate').value = '';
  	fm.all('VersionReMark').value = '';
  	fm.all('Maintenanceno').value = '';
  	fm.all('TraceVersionNo').value = '';
  	fm.all('MaintenanceState').value = '';
  	fm.all('MaintenanceReMark').value = '';
  	fm.pageflag.value='<%=request.getParameter("pageflag")%>';
		if("Q"==fm.pageflag.value)
		{
		
			buttonsdiv.style.display = 'none';
		} 
  }
  catch(ex)
  {
    alert("程序名称：VersionRuleInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
  }
  catch(re)
  {
    alert("程序名称：VersionRuleInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>