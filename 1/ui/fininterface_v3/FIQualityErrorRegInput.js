//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mOperate="";
var turnPage = new turnPageClass();
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) //shwoInfo是什么？
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//保存按钮
function savebutton()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	
	submitform();
}

//值检测
function beforeSubmit()
{
	if(fm.Applicant.value=='')
	{
		alert("申请人不能为空！");
		return false;
	}
	if(fm.BusTypeCode.value=='')
	{
		alert("业务交易编码不能为空！");
		return false;
	}
	if(fm.DetailIndexID.value=='')
	{
		alert("业务索引类型不能为空！");
		return false;
	}
	if(fm.BusinessNo.value=='')
	{
		alert("业务号码不能为空！");
		return false;
	}
	if(fm.DetailReMark.value=='')
	{
		alert("错误描述不能为空！");
		return false;
	}
	
	return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
 	else 
 	{
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//页面提交
function submitform()
{
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.action="./FIRuleDealErrAppSave.jsp";
	//lockButton(); 
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}