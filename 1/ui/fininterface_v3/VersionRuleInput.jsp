<html>
<%
//程序名称 :VersionRuleInput.jsp
//程序功能 :财务规则版本管理
//创建人 :范昕
//创建日期 :2008-08-21
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
	  GlobalInput tGI1 = new GlobalInput();
	  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
	  String strOperator = tGI1.Operator;
 	%>
<script>
  var comcode = "<%=tGI1.ComCode%>";
</script>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
<SCRIPT src = "VersionRuleInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="VersionRuleInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
  <form action="./VersionRuleSave.jsp" method=post name=fm target="fraSubmit">
  	  <Div id= "divVersionRule" style= "display: ''">
  <table>
    	<tr>
    		 <td class= titleImg>
        		财务规则版本管理
       		 </td>   		 
    	</tr>
    </table>
    <hr></hr>
  <td class=button width="10%" align=right>
		<INPUT class=cssButton name="querybutton" VALUE="版本信息查询"  TYPE=button onclick="return RulesVersionQuery();">
	</td>
<table class= common border=0 width=100%>
  <table class= common>
	<tr class= common>
				<TD class= title>
					  版本编号
				</TD>
				<TD class=input>
				 	<input class=readonly name=VersionNo readonly=true>
				</TD>
				<TD  class= title>
          生效日期
        </TD>
        <TD  class= input>
          <Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary>
        </TD>
	</tr>
	<tr class= common>
				<TD class= title>
					  版本描述
				</TD>
				<TD class=input>
				 	<input class=common name=VersionReMark elementtype=nacessary>
				</TD>
	</tr>	
	</table>
<div id= "buttonsdiv" style= "display: '' ">
	<INPUT VALUE="添加版本" TYPE=button class= cssbutton name= addVersionbutton onclick="addVersion()">
	<INPUT VALUE="删除版本" TYPE=button class= cssbutton name= deleteVersionbutton onclick="deleteVersion()">
	<hr></hr>
	<td class=button width="10%" align=right>
		<INPUT class=cssButton name="querybutton" VALUE="账务规则版本维护轨迹查询"  TYPE=button onclick="return RulesVersionTraceQuery();">
	</td>
	<table class= common>
		<tr class= common>
				<TD class= title>
		   	   维护编号
		    </TD>
		    <TD class= input>
		  	<Input class=readonly name=Maintenanceno readonly=true>
		    </TD>
		    <TD class= title>
		   	   维护描述
		    </TD>
		    <TD class= input>
		  	<Input class=common name=MaintenanceReMark elementtype=nacessary>
		    </TD>
		</tr>
  </table>
</table>
<INPUT VALUE="申请修改" TYPE=button class= cssbutton name= applyAmendbutton onclick="applyAmend()">
<INPUT VALUE="修改完毕" TYPE=button class= cssbutton name= CompleteAmendbutton onclick="CompleteAmend()">
<INPUT VALUE="撤销申请" TYPE=button class= cssbutton name= cancelAmendbutton onclick="cancelAmend()">
</Div>
</Div>
  <hr></hr>
  <INPUT VALUE="账务规则导出" TYPE=button class= cssbutton name= applyAmendbutton onclick="UpLoadToExcel();">
 <input type=hidden id="OperateType" name="OperateType">
 <input type=hidden name=EndDate>
 <input type=hidden name=VersionState>
 <input type=hidden name=MaintenanceState>
 <input type=hidden name=TraceVersionNo>
 <input type=hidden name=pageflag>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>