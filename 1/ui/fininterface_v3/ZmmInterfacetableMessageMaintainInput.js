//程序名称：ZmmInterfacetableMessageMaintainInput.js
//程序功能：接口表信息维护
//创建日期：2017-11-06
//创建人  ：张美美
//更新记录：  更新人    更新日期     更新原因/内容

var mOperate="";
var showInfo;
var str="";
window.onfocus=myonfocus;
var mode_flag;
var tAppealFlag="0";
var turnPage = new turnPageClass();
var sql="";

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//提交前
function beforeSubmit()
{
	if ((fm.all("Batchno").value == null)||(fm.all("Batchno").value == ''))
	    {
	      alert('请填写批次号！');
	      return false;
	    }
	if ((fm.all("Costcenter").value == null)||(fm.all("Costcenter").value == '')){
	  	  	alert('请填写成本中心！');
	  	  	return false;
	  	  }
	if ((fm.all("Chinal").value == null)||(fm.all("Chinal").value == '')){
	  	  	alert('请填写渠道！');
	  	  	return false;
	  	  }
	if ((fm.all("Readstate").value == null)||(fm.all("Readstate").value == '')){
	  	  	alert('请填写读取状态！');
	  	  	return false;
	  	  }
	return true;
}
function submitForm(str)
{
	var i = 0;
    var showStr = "正在"+str+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面"
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交  
}
//提交，保存按钮对应操作
function Add(){
	if(!beforeSubmit()){
	    return false;
	  }
	fm.mOperate.value = "INSERT";
	str="添加";
	submitForm(str);
    }

//修改
  function Update(){
	 if(!beforeSubmit()){
	    return false;
	  }
	 fm.mOperate.value = "UPDATE";
	if (confirm("您确实想修改该记录吗？")){
		str="修改";
		submitForm(str);
	 }else{
	    mOperate="";
	    alert("您取消了修改操作！");
	  } 
	}

  //删除 
  function Delete(){  
    fm.mOperate.value = "DELETE";
    if (confirm("确认要删除该数据吗？"))
    {
    	str="删除";
    	submitForm(str);
    }
    else
    {
    	mOperate="";
        alert("您取消了删除操作!");
    }
  }
//查询
function Query()
{
	alert("开始查询");
	if ((fm.all("Batchno").value == null)||(fm.all("Batchno").value == ''))
    {
      alert('请填写批次号！');
      return false;
    }
	if ((fm.all("Readstate").value == null)||(fm.all("Readstate").value == '')){
  	  	alert('请填写读取状态！');
  	  	return false;
  	  }
	initInterfacetableMaintainGrid();
	
	sql = "select Batchno,Importtype,Vouchertype,DCFlag,Accountcode,Summoney,Costcenter,Riskcode,Chinal,Chargedate,Readstate,VoucherID from interfacetable"
	+ " where Batchno = '" + fm.all('Batchno').value + "'" 
	+getWherePart('Costcenter','Costcenter')
	+getWherePart('Chinal', 'Chinal')
	+getWherePart('Serialno','Serialno');
    turnPage.queryModal(sql,InterfacetableMaintainGrid); 
    /*turnPage.queryModal(strSql, multilineGrid)
                  功能说明：快速查询显示接口，只能使用默认的turnPage翻页对象，一个页面只能使用一次。
                  方法会根据传入的SQL语句查出数据，并显示在传入的MultiLine对象中，
                  省去easyQueryVer3->decodeEasyQueryResult->turnPage.getData->displayMultiline等详细控制代码。*/
    alert("查询完成");
    afterSubmit();
   }

/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ) //if (!turnPage.strQueryResult ) ,content=" + "没有查询到数据" ;
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
}

//响应单选按钮,一点击某条数据，其相应的字段值就显示在相应的字段下面
function BankSelect() {
	var tSel = InterfacetableMaintainGrid.getSelNo();
	/*使用方法：ObjGrid.getSelNo() 内部循环判断所有行,返回被选中的行的行号。 
                  注意：行号是从1开始,和数组是不一样的。如果没有选中行，返回值是0。*/
	try
	{
		/*取出指定的第几行第几列的值,使用MulLine对象的getRowColData(row,col)方法*/
		fm.all('Batchno').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 1);	
		fm.all('Costcenter').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 7);
		fm.all('Chinal').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 9);
		fm.all('Chargedate').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 10);
		fm.all('Readstate').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 11);
	}
	catch(ex)
	{
		alert( "读取数据错误,请刷新页面再使用。" + ex );
	}
}
/*function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}*/
