<%
  //程序名称：FifindatacheckInit.jsp
  //程序功能：元数据定义
  //创建日期：2011/9/15
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">

function initForm(){
	try{
	
		fm.all('updatebutton').disabled = true;		
		fm.all('deletebutton').disabled = true;
	}
	catch(re){
		alert("FifindatacheckInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}
//针对版本信息查询子窗口返回的2维数组
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('VersionNo').value = arrResult[0][0];
		fm.all('EndDate').value = arrResult[0][2];
		//fm.all('VersionReMark').value = arrResult[0][3];
		fm.all('VersionState').value = arrResult[0][9];
		
	//	fm.all('Maintenanceno').value = '';
	//	fm.all('MaintenanceReMark').value = '';
	//	fm.all('MaintenanceState').value = '';
	//	fm.all('TraceVersionNo').value = '';
	}
}
//针对版本信息查询子窗口返回的2维数组
function afterQuery1( arrQueryResult )
{
	var arrResult = new Array();
		//alert(arrQueryResult);

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		
		arrResult[0][2] = arrResult[0][2].split("@@").join("||");
		arrResult[0][2] = arrResult[0][2].split("@").join("|");
		
		//alert(arrResult);
		fm.all('Ruletype').value = arrResult[0][0];		
		fm.all('RuletypeName').value = arrResult[0][1];		
		fm.all('Ruleid').value = arrResult[0][2];	
		fm.all('CallPointID').value = arrResult[0][3];	
		fm.all('CallPointIDName').value = arrResult[0][4];
		fm.all('RuleState').value = arrResult[0][5];
		fm.all('RuleStateName').value = arrResult[0][6];
		fm.all('RuleName').value = arrResult[0][7];

		if(fm.all('Ruletype').value=="02")
		{	
			fm.all('updatebutton').disabled = false;		
			fm.all('deletebutton').disabled = true;	
			
		 } else 
		 {	
			fm.all('updatebutton').disabled = false;		
			fm.all('deletebutton').disabled = false;	
		 }
   
		//来自于Common\javascript\Common.js，根据代码选择的代码查找并显示名称
		showCodeName(); 
		//a.VersionNo,a.VersionState
	}
}


//change状态
function changetype(){

	if(fm.all('Ruletype').value=="04")
	{
			fm.all('RuleDealSQL').value = "";
			fm.all('CallPointID').value = "";
			fm.all('CallPointIDName').value = "";
			document.getElementById("tr1").style.display = "none";
			document.getElementById("td1").style.display = "none";	
			document.getElementById("td2").style.display = "none";				
		//	fm.all('updatebutton').disabled = true;		
			fm.all('deletebutton').disabled = true;
	} else if(fm.all('Ruletype').value=="03") {
	
			document.getElementById("tr1").style.display = "";	
			document.getElementById("td1").style.display = "";	
			document.getElementById("td2").style.display = "";				
			fm.all('updatebutton').disabled = false;		
			fm.all('deletebutton').disabled = false;
	}

}
</script>
