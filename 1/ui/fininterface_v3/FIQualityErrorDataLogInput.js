//               该文件中包含客户端需要处理的函数和事件

//Creator :范昕	
//Date :2008-08-18

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();


function ToExcel()
{
	if(FIRuleDealErrLogGrid.mulLineCount=="0"){
		alert("没有查询到数据");
		return false;
	} 	 	
	fm.action="./FinRuleDealCheckExcel.jsp";
	fm.target=".";
	fm.submit(); //提交
}

//初始化页面
function queryFIRuleDealErrLogGrid()
{
	try
	{
		initFIRuleDealErrLogGrid();
		var strSQL = ""; 
		strSQL = "select CheckBatchNo,(select codename from ldcode where code=CallPointID and codetype = 'fieventnode')," +
		" (select codename from ldcode where code=IndexCode and codetype = 'fiindexid'),BusinessNo,RuleID,'待处理',ErrInfo,RulePlanID,ErrSerialNo "+
		" from FIRuleDealErrLog where ";
		strSQL = strSQL + " CheckBatchNo ='"+RuleDealBatchNo+"' and DealState = '0' ";
  	    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  	    fm.ExpSQL.value = strSQL;
		if (!turnPage.strQueryResult)
		{
			return false;
		}
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		turnPage.pageLineNum = 10 ;
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象
		turnPage.pageDisplayGrid = FIRuleDealErrLogGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL ;
		arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
		//调用MULTILINE对象显示查询结果
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  }
  
  	catch(Ex)
	{
		alert(Ex.message);
	}
	 
  
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function ReturnData()
{
	var tSel = FIRuleDealErrLogGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    fm.all('CertificateID').value =  FIRuleDealErrLogGrid.getRowColData(tSel - 1,9); //arrResult[0][1];
		fm.all('ErrInfo').value = FIRuleDealErrLogGrid.getRowColData(tSel - 1,7);//arrResult[0][2];
		fm.all('businessno').value = FIRuleDealErrLogGrid.getRowColData(tSel - 1,4); //arrResult[0][3];
		fm.all('RulePlanID').value = FIRuleDealErrLogGrid.getRowColData(tSel - 1,8);//arrResult[0][4];
		fm.all('RuleID').value = FIRuleDealErrLogGrid.getRowColData(tSel - 1,5);//arrResult[0][5];
		fm.all('DealState').value = FIRuleDealErrLogGrid.getRowColData(tSel - 1,6);//arrResult[0][6];	
	}
}
//忽略错误
function DealErrdata()
{
	var tSel = FIRuleDealErrLogGrid.getSelNo();	

	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{
	
	   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
       showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
   	  fm.all('DelErrSerialNo').value =FIRuleDealErrLogGrid.getRowColData(tSel - 1,9);
   	  fm.action="./FIRuleDealErrLogSave.jsp";
   	  fm.target="fraSubmit";
   	  fm.submit();
    }
}


function afterQuery( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('CertificateID').value =  parent.fraInterface.FIRuleDealErrLogGrid.getRowColData(tSel - 1,1); //arrResult[0][1];
		fm.all('ErrInfo').value = parent.fraInterface.FIRuleDealErrLogGrid.getRowColData(tSel - 1,2);//arrResult[0][2];
		fm.all('businessno').value = parent.fraInterface.FIRuleDealErrLogGrid.getRowColData(tSel - 1,3); //arrResult[0][3];
		fm.all('RulePlanID').value = parent.fraInterface.FIRuleDealErrLogGrid.getRowColData(tSel - 1,4);//arrResult[0][4];
		fm.all('RuleID').value = parent.fraInterface.FIRuleDealErrLogGrid.getRowColData(tSel - 1,5);//arrResult[0][5];
		fm.all('DealState').value = parent.fraInterface.FIRuleDealErrLogGrid.getRowColData(tSel - 1,6);//arrResult[0][6];		
	}
}
function initFIRuleDealErrLogQuery()
{
	try
	{
		var strSQL = "select CheckBatchNo,(select codename from ldcode where code=CallPointID and codetype = 'fieventnode')," +
		" (select codename from ldcode where code=IndexCode and codetype = 'fiindexid'),BusinessNo,RuleID,'待处理',ErrInfo,RulePlanID,CertificateID "+
		" from FIRuleDealErrLog where ";
		strSQL = strSQL + " CheckBatchNo ='"+RuleDealBatchNo+"' and DealState = '0' ";
	  	turnPage.queryModal(strSQL, FIRuleDealErrLogGrid);
		fm.ExpSQL.value = strSQL;
  	    fm.all('CertificateID').value = '';
		fm.all('ErrInfo').value = '';
		fm.all('businessno').value = '';
		fm.all('RulePlanID').value = '';
		fm.all('RuleID').value = '';
		fm.all('DealState').value = '';	
	}	
	catch(Ex)
	{
		alert(Ex.message);
	}
}
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  //释放“增加”按钮

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	mOperate="";
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");


    mOperate="";
    initFIRuleDealErrLogQuery();
  }
}

