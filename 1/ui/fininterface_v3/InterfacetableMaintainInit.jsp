<% 
//程序名称：InterfacetableMaintainInit.jsp
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">


function initInpBox()
{
  try
  {
	//查询条件置空
	//fm.all('BatchNo').value = '';
	//fm.all('SerialNo').value = '';
	//fm.all('CostCenter').value = '';
	//fm.all('Chinal').value = '';
	//fm.all('ChargeDate').value = '';
	//fm.all('ReadState').value = '';
  }
  catch(ex)
  {
    alert("InterfacetableMaintainInit.jsp-->InitInpBox函数中发生异常:进行初始化时错误!");
  }      
}
                            

function initForm()
{
  try
  {
	initInpBox();
    initInterfacetableMaintainGrid();  //初始化共享工作池
  }
  catch(re)
  {
    alert("InterfacetableMaintainInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initInterfacetableMaintainGrid()
{                                  
	var iArray = new Array();      
	try
	{
		 iArray[0]=new Array();
	        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	        iArray[0][1]="30px";            		//列宽
	        iArray[0][2]=10;            			//列最大值
	        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	        iArray[1]=new Array();
	        iArray[1][0]="流水号";         		
	        iArray[1][1]="70px";            		
	        iArray[1][2]=170;            			
	        iArray[1][3]=0;              			
	        
	        iArray[2]=new Array();
	        iArray[2][0]="批次号";         		
	        iArray[2][1]="150px";            		
	        iArray[2][2]=100;            			
	        iArray[2][3]=0;             			
	        
	        iArray[3]=new Array();
	        iArray[3][0]="凭证类型";         		
	        iArray[3][1]="50px";            		
	        iArray[3][2]=100;            			
	        iArray[3][3]=0;   
	        
	        iArray[4]=new Array();
	        iArray[4][0]="机构";         		
	        iArray[4][1]="40px";            		
	        iArray[4][2]=100;            			
	        iArray[4][3]=0;              			
	        
	        
	        iArray[5]=new Array();
	        iArray[5][0]="记账日期";         		
	        iArray[5][1]="85px";            		
	        iArray[5][2]=100;            			
	        iArray[5][3]=0;                       
	              
	        iArray[6]=new Array();
	        iArray[6][0]="科目";         		
	        iArray[6][1]="80px";            		
	        iArray[6][2]=100;            			
	        iArray[6][3]=0;              			
	        
	        
	        iArray[7]=new Array();
	        iArray[7][0]="金额";         		
	        iArray[7][1]="100px";            		
	        iArray[7][2]=100;            			
	        iArray[7][3]=0;    
	        
	        iArray[8]=new Array();
	        iArray[8][0]="读取状态";         		
	        iArray[8][1]="50px";            		
	        iArray[8][2]=100;            			
	        iArray[8][3]=0;            			
	        
	        
	       iArray[9]=new Array();
	        iArray[9][0]="供应商号码";         		
	        iArray[9][1]="80px";            		
	        iArray[9][2]=100;            			
	        iArray[9][3]=0;           
         
         
         
          
      InterfacetableMaintainGrid = new MulLineEnter( "fm" , "InterfacetableMaintainGrid" ); 
      //这些属性必须在loadMulLine前
      InterfacetableMaintainGrid.mulLineCount = 5;   
      InterfacetableMaintainGrid.displayTitle = 1;
      InterfacetableMaintainGrid.locked = 1;
      InterfacetableMaintainGrid.canSel = 1;
      //InterfacetableMaintainGrid.canChk = 0;
      InterfacetableMaintainGrid.hiddenPlus = 1;
      InterfacetableMaintainGrid.hiddenSubtraction = 1;        
      InterfacetableMaintainGrid.loadMulLine(iArray); 
      InterfacetableMaintainGrid.selBoxEventFuncName = "BankSelect";  
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>
