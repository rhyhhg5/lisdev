<% 
//程序名称：ZmmInterfacetableMessageMaintainInit.jsp
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">


function initInpBox()
{
  try
  {
	//查询条件置空
	/* fm.all('BatchNo').value = '';
	fm.all('SerialNo').value = '';
	fm.all('CostCenter').value = '';
	fm.all('Chinal').value = '';
	fm.all('ChargeDate').value = '';
	fm.all('ReadState').value = ''; */
  }
  catch(ex)
  {
    alert("ZmmInterfacetableMessageMaintainInit.jsp-->InitInpBox函数中发生异常:进行初始化时错误!");
  }      
}
                            

function initForm()
{
  try
  {
	initInpBox();
    initInterfacetableMaintainGrid();  //初始化共享工作池
  }
  catch(re)
  {
    alert("ZmmInterfacetableMessageMaintainInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initInterfacetableMaintainGrid()
{                                  
	var iArray = new Array();      
	try
	{
		 iArray[0]=new Array();
	        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	        iArray[0][1]="30px";            		//列宽
	        iArray[0][2]=10;            			//列最大值
	        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	        iArray[1]=new Array();
	        iArray[1][0]="批次号";         		
	        iArray[1][1]="150px";            		
	        iArray[1][2]=100;            			
	        iArray[1][3]=0;
	        
	        iArray[2]=new Array();
	        iArray[2][0]="凭证大类";         		
	        iArray[2][1]="50px";            		
	        iArray[2][2]=100;            			
	        iArray[2][3]=0;
	        
	        iArray[3]=new Array();
	        iArray[3][0]="财务凭证类型";         		
	        iArray[3][1]="70px";            		
	        iArray[3][2]=170;            			
	        iArray[3][3]=0;              			
	        
	        iArray[4]=new Array();
	        iArray[4][0]="借贷标记";         		
	        iArray[4][1]="40px";            		
	        iArray[4][2]=100;            			
	        iArray[4][3]=0;              			
	        
	             
	        iArray[5]=new Array();
	        iArray[5][0]="科目";         		
	        iArray[5][1]="80px";            		
	        iArray[5][2]=100;            			
	        iArray[5][3]=0;              			
	        
	        
	        iArray[6]=new Array();
	        iArray[6][0]="金额";         		
	        iArray[6][1]="100px";            		
	        iArray[6][2]=100;            			
	        iArray[6][3]=0;    
	        
	        iArray[7]=new Array();
	        iArray[7][0]="成本中心";         		
	        iArray[7][1]="80px";            		
	        iArray[7][2]=100;            			
	        iArray[7][3]=0;
	        
	        iArray[8]=new Array();
	        iArray[8][0]="险种";         		
	        iArray[8][1]="50px";            		
	        iArray[8][2]=100;            			
	        iArray[8][3]=0;
	        
	        iArray[9]=new Array();
	        iArray[9][0]="渠道";         		
	        iArray[9][1]="50px";            		
	        iArray[9][2]=100;            			
	        iArray[9][3]=0;
	        
	        iArray[10]=new Array();
	        iArray[10][0]="记账日期";         		
	        iArray[10][1]="85px";            		
	        iArray[10][2]=100;            			
	        iArray[10][3]=0;
	        
	        iArray[11]=new Array();
	        iArray[11][0]="读取状态";         		
	        iArray[11][1]="50px";            		
	        iArray[11][2]=100;            			
	        iArray[11][3]=0;            			
	        
	        iArray[12]=new Array();
	        iArray[12][0]="凭证号";         		
	        iArray[12][1]="50px";            		
	        iArray[12][2]=100;            			
	        iArray[12][3]=0;
	  
      InterfacetableMaintainGrid = new MulLineEnter( "fm" , "InterfacetableMaintainGrid" ); //生成对象区
      //这些属性必须在loadMulLine前
      InterfacetableMaintainGrid.mulLineCount = 5;//行属性：设置行数=5  
      InterfacetableMaintainGrid.displayTitle = 1;//标题属性：1显示标题 (缺省值) ,0隐藏标题 
      InterfacetableMaintainGrid.locked = 1;//MulLine上的”+”和”--“标记可以禁用或恢复（即锁定或解锁）,此处是锁定
      InterfacetableMaintainGrid.canSel = 1;//初始化Radio单选框, 1 显示 ；0 隐藏（缺省值）
      //InterfacetableMaintainGrid.canChk = 0;
      InterfacetableMaintainGrid.hiddenPlus = 1;//如果不写ObjGrid.hiddenPlus=1; 则默认显示“+”号按钮,
      //ObjGrid. hiddenPlus = 0;  显示“+”号按钮,这是前提条件, 也是默认选项，
      InterfacetableMaintainGrid.hiddenSubtraction = 1;//“--”     
      InterfacetableMaintainGrid.loadMulLine(iArray); //对象初始化区
      InterfacetableMaintainGrid.selBoxEventFuncName = "BankSelect";
      /*如何在MulLine中单击RadioBox时响应开发人员外部编写的JS函数
                     在初始化时
       ObjGrid. canSel = 1;  //选中checkBox 功能,这是前提条件
       ObjGrid. selBoxEventFuncName =“functionName” //你写的JS函数名，不加扩号
       ObjGrid. selBoxEventFuncParm =”[‘str1’,number,…]” //传入的参数, */
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>
