<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FIMetaDataDefInput.jsp
 //程序功能：元数据定义
 //创建日期：2011/9/13
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIMetaDataDef.js"></SCRIPT>
  <%@include file="FIMetaDataDefInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIMetaDataDefSave.jsp" method=post name=fm target="fraSubmit">
<br>
<table>
	<tr>
		<td class="titleImg" >元数据定义</td>
	</tr>
</table>
<hr></hr>
<td class=button width="10%" align=left>
	<INPUT class=cssButton name="querybutton" VALUE="元数据查询"  TYPE=button onclick="return MetadateQuery()">
</td>	
<table  class="common" >
	<tr class="common">
		<td class="title">元数据编码</td><td class="input"><input class="common" name="MetadataNo" elementtype=nacessary></td>  
		<td class="title">元数据名称</td><td class="input"><input class="common" name="MetadataName" elementtype=nacessary></td>  
		<td class="title">物理表</td><td class="input"><input class="common" name="Object" elementtype=nacessary></td>  
    </tr>
    <tr>
		<td class="title">数据源</td>
		<td class="input"><Input class=codeno name= DataSource 
			ondblClick="showCodeList('fidatasource',[this,DataSourceName],[0,1],null,null,null,[1]);" 
			onkeyup="showCodeListKey('fidatasource',[this,DataSourceName],[0,1],null,null,null,[1]);" readonly=true><input class=codename name=DataSourceName readonly=true elementtype=nacessary> 
		</td>		
		<td class="title">元数据类型</td>
		<td class="input"><Input class=codeno name= FIMDType 
			ondblClick="showCodeList('fimdtype',[this,FIMDTypeName],[0,1],null,null,null,[1]);" 
			onkeyup="showCodeListKey('fimdtype',[this,FIMDTypeName],[0,1],null,null,null,[1]);" readonly=true><input class=codename name=FIMDTypeName readonly=true elementtype=nacessary> 
		</td>
		<td class="title">生效日期</td><td class="input"><input class="coolDatePicker" dateFormat="short" name="StartDate" elementtype=nacessary></td>  
	</tr>
    <tr>
    	<td class="title">失效日期</td><td class="input"><input class="coolDatePicker" dateFormat="short" name="EndDate" ></td>
		<td class="title">状态</td>
		<td class="input"><Input class=codeno name= State 
			ondblClick="showCodeList('fiattstate',[this,StateName],[0,1],null,null,null,[1]);" 
			onkeyup="showCodeListKey('fiattstate',[this,StateName],[0,1],null,null,null,[1]);" readonly=true><input class=codename name=StateName readonly=true elementtype=nacessary> 
		</td>
		<td class="title"></td><td class="input"></td>  
		<td class="title"></td><td class="input"></td> 
	</tr>	
	<tr class="common">
    	<td class="title">元数据描述</td><td class="input" colspan =3><textarea class="common" name="remark" cols="80%" rows="3" ></textarea></td>  
    </tr>
</table>

<br>
<INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
<INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
<INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
<INPUT VALUE="重  置" TYPE=button class= cssbutton name= resetbutton onclick="initForm()">
</div>
<hr></hr>
<br>
<input value="元数据属性定义"  onclick="GotoDataAttDefPage()" class="cssButton" type="button" >

<input type=hidden name=hideOperate>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
