<%
  //程序名称：FIBnInfoDefInit.jsp
  //程序功能：业务交易信息定义
  //创建日期：2011/8/25
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">
var BusTypeId = "";
var pageflag = "";
var maintno = "";
var VersionNo = "";

function initForm(){
	try
	{	
		initBusTypeInfoGrid();
		initinputBox();
		initpage();
		
	}
	catch(re){
		alert("FIBusInfoDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initinputBox()
{
	VersionNo='<%=request.getParameter("VersionNo")%>';
	BusTypeId='<%=request.getParameter("BusTypeId")%>';
	pageflag='<%=request.getParameter("pageflag")%>';
	maintno='<%=request.getParameter("maintno")%>';
	fm.pageflag.value=pageflag;
	fm.BusTypeId.value = BusTypeId;
}


function initBusTypeInfoGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=1;

		iArray[1]=new Array();
		iArray[1][0]="业务编号";
		iArray[1][1]="90px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="信息维度";
		iArray[2][1]="50px";
		iArray[2][2]=100;
		iArray[2][3]=3;
		
		iArray[3]=new Array();
		iArray[3][0]="维度名称";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=3;

		iArray[4]=new Array();
		iArray[4][0]="信息编码";
		iArray[4][1]="50px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="信息名称";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		iArray[6]=new Array();
		iArray[6][0]="信息描述";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=1;
		
		iArray[7]=new Array();
		iArray[7][0]="信息用途";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=1;
		
		iArray[8]=new Array();
		iArray[8][0]="信息值域";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=1;


		BusTypeInfoGrid = new MulLineEnter( "fm" , "BusTypeInfoGrid" ); 

		BusTypeInfoGrid.mulLineCount=2;
		BusTypeInfoGrid.displayTitle=1;
		BusTypeInfoGrid.canSel=1;
		BusTypeInfoGrid.canChk=0;
		BusTypeInfoGrid.hiddenPlus=1;
		BusTypeInfoGrid.hiddenSubtraction=1;			
		BusTypeInfoGrid.selBoxEventFuncName="dataselect";

		BusTypeInfoGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

function initAfterModBusGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=1;

		iArray[1]=new Array();
		iArray[1][0]="业务编号";
		iArray[1][1]="90px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="信息维度";
		iArray[2][1]="50px";
		iArray[2][2]=100;
		iArray[2][3]=2;

		iArray[3]=new Array();
		iArray[3][0]="维度名称";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="信息编码";
		iArray[4][1]="50px";
		iArray[4][2]=100;
		iArray[4][3]=2;
		
		iArray[5]=new Array();
		iArray[5][0]="信息名称";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		iArray[6]=new Array();
		iArray[6][0]="信息描述";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=1;

		AfterModBusGrid = new MulLineEnter( "fm" , "AfterModBusGrid" ); 

		AfterModBusGrid.mulLineCount=2;
		AfterModBusGrid.displayTitle=1;
		AfterModBusGrid.canSel=0;
		AfterModBusGrid.canChk=0;
		AfterModBusGrid.hiddenPlus=0;
		AfterModBusGrid.hiddenSubtraction=0;
		

		AfterModBusGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
