//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mOperate="";
var turnPage = new turnPageClass();
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) //shwoInfo是什么？
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//进入页面自动查询

function OperationLogQuery() 
{		
	var strSQL = ""; 
		
        //alert(fm.all('EventType').value);		
		strSQL = "select EventNo,EventType,LogFileName,LogFilePath,PerformState,MakeDate,MakeTime from FIOperationLog where 1=1 ";
		if(fm.all('StartDay').value != '')
		{
			strSQL = strSQL + " and MakeDate >= '"+fm.all('StartDay').value+"' ";
		}
		if(fm.all('EndDay').value != '')
		{
			strSQL = strSQL + " and MakeDate <= '"+fm.all('EndDay').value+"' ";
		}
		if(fm.all('EventClass').value != '')
		{
			strSQL = strSQL + " and EventType = '"+fm.all('EventClass').value+"' ";
		}
		if(fm.all('EventFlag').value != '')
		{
			strSQL = strSQL + " and PerformState = '"+fm.all('EventFlag').value+"' ";
		}		
		
		
		
  	//alert(strSQL);  	
  	turnPage.queryModal(strSQL, FIOperationLogGrid);
} 

function DownloadAddress()
{
	if((fm.all('EventNo1').value == '')||(fm.all('EventNo1').value == null))
	{
		alert("请先在系统操作日志查询结果中选择一条记录再做该操作！");
                return ;
	}
	Querydiv.style.display='';
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


//增加、删除、修改提交前的校验、计算
function beforeSubmit()
{			
  if (!verifyInput2())
  {
  	return false;
  }
    
    return true;
}




//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//选中mulline中一行，自动给输入框赋值
 function ReturnData()
{
 	var arrResult = new Array();

	var tSel = FIOperationLogGrid.getSelNo();	
	var strSQL="";
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录!" );
		return;
	}			
	else
	{  
		//查询成功则拆分字符串，返回二维数组
	  	arrResult = decodeEasyQueryResult(turnPage.strQueryResult);  	
	  	fm.all('EventNo').value = FIOperationLogGrid.getRowColData(tSel-1,1);
	  	fm.all('EventType').value = FIOperationLogGrid.getRowColData(tSel-1,2);
	  	fm.all('EventNo1').value = FIOperationLogGrid.getRowColData(tSel-1,1);
	  	
	  	fileUrl.href = filepath+"log/event/"+ FIOperationLogGrid.getRowColData(tSel-1,3); 
	    fileUrl.innerText =  FIOperationLogGrid.getRowColData(tSel-1,1);
	}	 
}


 function downLoad()
 {
   var row = FIOperationLogGrid.getSelNo();
   if(row == null || row == 0)
   {
     alert("请选择要下载的文件.");
     return false;
   }
   
   var fileName = FIOperationLogGrid.getRowColData(row - 1, 3);  
   if (fileName == null || fileName == "")
   {
     alert("没有附件,不能进行下载操作！")	
     return false;
   }
   //alert(fileName);

   fm.action = "./FIOperationLogQuerySave.jsp";
   fm.submit();
 }
 
 
 
 
 
 