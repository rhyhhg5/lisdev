<%
  //程序名称：FifindatacheckInit.jsp
  //程序功能：元数据定义
  //创建日期：2011/9/15
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
  String VersionNo = request.getParameter("VersionNo"); 
%>
<script type="text/javascript">

function initForm(){
	try{
	    fm.all("VersionNo").value=<%=VersionNo%>;
		initDataCheckGrid();
	}
	catch(re){
		alert("FifindatacheckInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initDataCheckGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="规则编号";
		iArray[1][1]="50px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="规则名称";
		iArray[2][1]="120px";
		iArray[2][2]=100;
		iArray[2][3]=0;

    	iArray[3]=new Array();
		iArray[3][0]="校检内容";
		iArray[3][1]="100px";
		iArray[3][2]=100;
		iArray[3][3]=0;
<%--
		iArray[3]=new Array();
		iArray[3][0]="XX";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="XX";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;
--%>

		DataCheckGrid = new MulLineEnter( "fm" , "DataCheckGrid" ); 

		DataCheckGrid.mulLineCount=2;
		DataCheckGrid.displayTitle=1;
		DataCheckGrid.canSel=1;
		DataCheckGrid.canChk=0;
		DataCheckGrid.hiddenPlus=1;
		DataCheckGrid.hiddenSubtraction=1;

		DataCheckGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
