var showInfo;
var mDebug="0";
var arrDataSet;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) //shwoInfo是什么？
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
  function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else 
	{
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

function VersionQuery()
{
	  showInfo=window.open("./FrameVersionRuleQuery.jsp");
}

function doact()
{
	if(!beforesubmit())
	{
		return false;
	}
	
	var i = 0;                                                                                                        
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";                                         
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;                                         
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function submitform()
{
	var i = 0;                                                                                                        
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";                                         
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;                                         
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = './FIRuleEngineServiceSave.jsp';                                                                      
	fm.submit(); //提交     
}

function afterSubmit( FlagStr, content )
{
	showInfo.close();
	//释放“增加”按钮

	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//resetForm();
	}
	else
	{
		//alert(content);
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		//parent.fraInterface.initForm();
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}

}

function afterQuery(arrReturn)
{
	var arrQueryResult = new Array();
	

	if( arrReturn != null )
	{
		
		//alert(arrReturn);
		fm.all('RuleDefID').value = arrReturn[0][0];
		fm.all('RuleName').value = arrReturn[0][1];		
		fm.all('ReturnRemark').value = arrReturn[0][2];				
		
	}
}

function beforesubmit()
{
	if(fm.StartDate.value == '')
	{
		alert("开始日期不能为空！");
		return false;
	}
	
	if(fm.StartDate.value == '')
	{
		alert("结束日期不能为空！");
		return false;
	}
	
	if(fm.RuleDefID.value == '')
	{
		alert("规则编码不能为空！请先查询！");
		return false;
	}
	return true;

}

//查询校检计划
function checkplanquery()
{
	window.open("./FrameFIDiversityRuleQuery.jsp");
}