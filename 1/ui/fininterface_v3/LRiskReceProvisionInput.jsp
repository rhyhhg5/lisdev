<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：LRiskReceProvisionInput.jsp
 //程序功能：长险应收计提数据提取
 //创建日期：2015-8-18
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
// ;initElementtype()
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="LRiskReceProvisionInput.js"></SCRIPT>
</head>
<body  onload="initForm();">
<form action="./LRiskReceProvisionSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >长险应收计提数据提取</td>
  </tr>
</table>
<table  class="common" >
  <tr class="common">
    <td class="title">抽取起期</td><td class="input"><input class="coolDatePicker" dateFormat="short" name="StartDate" ></td>  
    <td class="title">抽取止期</td><td class="input"><input class="coolDatePicker" dateFormat="short" name="EndDate" ></td>  
  </tr>		
  </div>
</table>
<br>
<input value="数据抽取" type=button onclick="DataExt()" class="cssButton" type="button"> 
<hr>
<div id="div1" style="display: none;">
<table>
  <tr>
    <td class="titleImg" ></td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="span" >
     </span> 
      </td>
   </tr>
</table>
</div>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
