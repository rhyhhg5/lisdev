//创建日期： 2013-05-06
//创建人   卢翰林
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;

function init()
{
	fm.Code.value = "";
	fm.CodeName.value = "";
	fm.CodeAlias.value = "";
}

function beforeSubmit()
{
	
	if (!verifyInput2()) 
	{
		return false;
	}
	
	strSql = "SELECT COUNT(code) FROM licodetrans where code =  '" + fm.Code.value + "'";
	strQueryResult = easyQueryVer3(strSql, 1, 0, 1);
	if(strQueryResult.substring(4) != "0")
	{
		alert("该业务机构代码已存在,请重新输入!");
		fm.Code.value = "";
		fm.Code.focus();
		return false;
	}
	return true;

}

function submitForm()
{
	if(!beforeSubmit())
	{
		return false;
	}
	var mOperate = "INSERT";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content)
{
	showInfo.close();
	if(FlagStr == "Fail")
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else 
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	init();
}