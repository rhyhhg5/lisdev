<html> 
<%
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String mStartDate = "";
	String mSubDate = "";
	String mEndDate = "";
	GlobalInput tGI = new GlobalInput();
	//PubFun PubFun=new PubFun();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
	System.out.println("2"+tGI.ManageCom);
%>

<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
<script src="../common/Calendar/Calendar.js"></script>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="FIDataProcessDefInput.js"></SCRIPT>  
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="FIDataProcessDefInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >

<form name=fm target=fraSubmit method=post>
<table>
  	<tr>    		 
  		 <td class= titleImg>
      		 二次处理规则定义
     	 </td>   		 
  	</tr>
</table>
<div id=versionquerybutton>
<td class=button width="10%" align=right>
	<INPUT class=cssButton name="querybutton" VALUE="版本信息查询"  TYPE=button onclick="return queryClick1();">
</td>    
</div>
<table class= common border=0 width=100%>			  	
	<tr class= common>       
		<TD class= title>版本编号</TD>
		<TD class=input>
			<Input class= readonly name=VersionNo value= '00000000000000000001' readonly=true>   					 	 
		</TD>
		<TD class= title>版本状态	</TD>
		<TD class=input>
			<Input class= readonly name=VersionState2 value = '正常' readonly=true>
		</TD>				 								
	</tr>  
</table>       

<hr></hr>  
<div id=finitemquerydiv>
<td class=button width="10%" align=right>
	<INPUT class=cssButton name="querybutton" VALUE="二次数据处理规则查询"  TYPE=button onclick="return queryClick2();">
</td>
</div>	
<table class= common border=0 width=100%>
	<tr class= common>
		<TD class= title>规则编号</TD>
		<TD class=input>
				<Input class=common name=ProcessID  elementtype=nacessary  >
		</TD>
		<TD class= title>规则名称</TD>
	    <TD class= input>                                            
	  		 <Input class=common name=ProcessName  elementtype=nacessary  >
	    </TD>				
	</tr>
	<tr class= common>
		<TD class= title>规则类型</TD>
	    <TD class= input>
			<Input class=codeno name= ProcessType readonly=true  CodeData="0|^1|生成虚拟数据^2|现金流匹配^3|账务日期确定" ondblClick="showCodeListEx('FinItemType',[this,FinItemTypeMame],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('FinItemType',[this,FinItemTypeMame],[0,1],null,null,null,[1]);"><input class=codename name=FinItemTypeMame readonly=true elementtype=nacessary>
	    </TD>
		<TD class= title>规则对象</TD>
		<TD class=input>
			<Input class=codeno name= ProcessObject readonly=true  CodeData="0|^1|业务类型^2|费用类型" ondblClick="showCodeListEx('FinItemType',[this,FinItemTypeMame1],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('FinItemType',[this,FinItemTypeMame1],[0,1],null,null,null,[1]);"><input class=codename name=FinItemTypeMame1 readonly=true elementtype=nacessary>
		</TD>		    		    
	</tr>
	<tr class= common>
		<TD class= title>规则描述</TD>
	    <TD class= input>
	  		<Input class=common name=ReMark verify="描述|len<=500" >
	    </TD>		
	    <TD class= title>规则状态</TD>
	    <TD class= input>
	  		<Input class=codeno name= RuleState readonly=true ondblClick="showCodeList('rulestate',[this,ExtTypeName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('rulestate',[this,ExtTypeName],[0,1],null,null,null,[1]);"><input class=codename name=ExtTypeName readonly=true elementtype=nacessary>
	    </TD>	    		    
	</tr>
	<tr class= common>
		<TD class= title>处理方式</TD>
	    <TD class= input>
	  		<Input class=codeno name= RuleDealMode readonly=true ondblClick="showCodeList('ruledealmode',[this,RuleDealModeName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('ruledealmode',[this,RuleDealModeName],[0,1],null,null,null,[1]);"><input class=codename name=RuleDealModeName readonly=true elementtype=nacessary>
	    </TD>		
    		    
	</tr>
	<tr class= common>
	</tr>
</table>         
<br>
<div id=sqldiv style="display:'none'">
	<td class=title>处理SQL</td>
	<textarea rows=2 cols=50% name = RuleDealSQL></textarea>
</div> 
<div id=classdiv style="display:'none'">
	<td class=title>处理类</td>
	<td class=input><input class=common name=RuleDealClass ></td>
</div> 		
<div id=filediv style="display:'none'">
	<td class=title>处理文件</td>
	<td class=input><input class=common name=RuleFileName ></td>
</div> 						 
  	<p>
    <INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
    <INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
    <INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
    <INPUT VALUE="重  置" TYPE=button class= cssbutton name="resetbutton" onclick="return resetAgain();">   

    <hr></hr>
    
    <INPUT VALUE="二次处理对象定义" TYPE=button class= cssbutton name="intobutton1" onclick="intoProcessObject();">   
    <!-- 定义页面参数 -->
    <input type=hidden name=pageflag>
    <input type=hidden name=maintno>
    <input type=hidden name=apptype>
    <INPUT type=hidden name=hideOperate value=''>
    <INPUT type=hidden name=VersionState value='01'> <!-- VersionState用来存版本状态：01，02，03；而VersionState2存版本状态：正常、维护、删除-->
    <INPUT type=hidden name=DealMode value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>


</body>
</html>
