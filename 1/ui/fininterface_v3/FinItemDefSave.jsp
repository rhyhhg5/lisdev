<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FinItemDefSave.jsp
//程序功能：科目类型定义保存页面
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
	//输入参数
	request.setCharacterEncoding("GBK");
	
	FIFinItemDefSchema tFIFinItemDefSchema = new FIFinItemDefSchema();
	FMFinItemDefSchema tFMFinItemDefSchema = new FMFinItemDefSchema();
	
	String pageflag = request.getParameter("pageflag");
	
	FIFinItemDefUI tFIFinItemDef = new FIFinItemDefUI();
	FMFinItemDefUI tFMFinItemDefUI = new FMFinItemDefUI();

	//输出参数
	CErrors tError = null;
	String tOperate=request.getParameter("hideOperate");
	tOperate=tOperate.trim();
	String mFinItemID = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	tFIFinItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFIFinItemDefSchema.setFinItemID(request.getParameter("FinItemID"));
	tFIFinItemDefSchema.setFinItemName(request.getParameter("FinItemName"));
	tFIFinItemDefSchema.setFinItemType(request.getParameter("FinItemType"));
	tFIFinItemDefSchema.setItemMainCode(request.getParameter("ItemMainCode"));
	tFIFinItemDefSchema.setDealMode(request.getParameter("DealMode"));
	tFIFinItemDefSchema.setReMark(request.getParameter("ReMark"));
	
	tFMFinItemDefSchema.setMaintNo(request.getParameter("maintno"));
	tFMFinItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFMFinItemDefSchema.setFinItemID(request.getParameter("FinItemID"));
	tFMFinItemDefSchema.setFinItemName(request.getParameter("FinItemName"));
	tFMFinItemDefSchema.setFinItemType(request.getParameter("FinItemType"));
	tFMFinItemDefSchema.setItemMainCode(request.getParameter("ItemMainCode"));
	tFMFinItemDefSchema.setDealMode(request.getParameter("DealMode"));
	tFMFinItemDefSchema.setReMark(request.getParameter("ReMark"));

	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	tVData.add(tG);
	try
	{
		if("C".equals(pageflag))
		{
			tVData.add(tFIFinItemDefSchema);
			System.out.println("come in tFIFinItemDefSchema=="+tFIFinItemDefSchema.getFinItemID());    
			if(!tFIFinItemDef.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFIFinItemDef.mErrors.getFirstError();
				FlagStr = "Fail";
			}
			System.out.println("come out" + tOperate);
		}
		else if("X".equals(pageflag))
		{
			tVData.add(tFMFinItemDefSchema);
			System.out.println("come in tFMFinItemDefSchema=="+tFMFinItemDefSchema.getFinItemID());    
			if(!tFMFinItemDefUI.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFMFinItemDefUI.mErrors.getFirstError();
				FlagStr = "Fail";
			}
			System.out.println("come out" + tOperate);
		}
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = tFIFinItemDef.mErrors;
		if (!tError.needDealError())
		{
			mFinItemID = tFIFinItemDef.getFinItemID();
			Content = " 操作已成功! ";
			FlagStr = "Succ";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
	
	//添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

