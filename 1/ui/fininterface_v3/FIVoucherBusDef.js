//该文件中包含客户端需要处理的函数和事件

//程序名称：FIVoucherFeeDef.js
//程序功能：凭证费用定义
//创建日期：2011/8/27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var mOperate="";
var turnPage = new turnPageClass();

//初始化页面
function initpage()
{
	var checksql = "select a.vouchername from FIVoucherDef a  where  a.voucherid ='"+voucherid+"' and a.versionno='"+versionno+"'" ;	
	reuslt=easyExecSql(checksql);
	
	var tsql = "select a.versionno,a.voucherid,a.vouchername,(select codename from ldcode where code=a.vouchertype and codetype = 'fivouchertype'),b.BusinessID,b.BusinessName from FIVoucherDef a ,FIVoucherBn b where a.voucherid = b.voucherid and a.voucherid ='"+voucherid+"' and a.versionno='"+versionno+"'";
	turnPage.queryModal(tsql,VoucherTypeGrid);
	
}

//添加操作
function addclick()
{
	if(!beforeSubmit())
	{
		return false;
	}
	var checksql = "select count(1) from FIVoucherBn where voucherid='"+voucherid+"' and BusinessID='"+fm.all("FIBusID").value+"'" ;	
	var reuslt=easyExecSql(checksql);
	//alert(reuslt);
	if(reuslt!="0")
	{
		alert("该业务类型已与该凭证关联存在！");
		return false;		
	}
	//为了防止双击，点击增加后，屏蔽"增加"按钮
	mOperate="INSERT";
	fm.all('hideOperate').value=mOperate;
	submitForm();
}

//删除操作
function deleteclick()
{
	var checksql = "select count(1) from FIVoucherBn where voucherid='"+voucherid+"' and BusinessID='"+fm.all("FIBusID").value+"'" ;	
	var reuslt=easyExecSql(checksql);
	//alert(reuslt);
	if(reuslt=="0")
	{
		alert("该业务类型与该凭证无关联存在，请确认！");
		return false;		
	}
	//下面增加相应的删除代码
	if ((fm.all("FIBusID").value==null)||(trim(fm.all("FIBusID").value)==''))
		alert("请确定要删除的科目编号！");
	
	else
	{
	
		if (confirm("您确实想删除该记录吗?"))
		{
			mOperate="DELETE";
			
		}
		else
		{
			mOperate="";
			alert("您取消了删除操作！");
		}
	}
	fm.all('hideOperate').value=mOperate;
	submitForm();
}

//提交前的校验、计算  
function beforeSubmit()
{
	if(fm.FIBusID.value == "")
	{
		alert("业务交易不能为空！");
		return false;
	}
	
	return true;
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交动作
function submitForm()
{
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		location.reload();
		showDiv(operateButton,"true"); 
		showDiv(inputButton,"false"); 
		//执行下一步操作
	}
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}

//选择一条数据
function selectdata()
{
	var tSel = VoucherTypeGrid.getSelNo();
	var FIBusID = VoucherTypeGrid.getRowColData(tSel-1,5);
	var FIBusIDName = VoucherTypeGrid.getRowColData(tSel-1,6);
	fm.FIBusID.value = FIBusID;
	fm.FIBusIDName.value = FIBusIDName;
}