<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIBusTypeDefSave.jsp
//程序功能：业务交易定义
//创建日期：2011/8/25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//接收信息，并作校验处理。
request.setCharacterEncoding("GBK");
//输入参数
String tVersionNo=request.getParameter("VersionNo");
String tMaintNo=request.getParameter("maintno");
String tBusType=request.getParameter("FIBusType");
String tBusTypeID=request.getParameter("BusTypeId");
String tBusTypeName=request.getParameter("BusTypeName");
String tDetailType=request.getParameter("FIDetailType");
String tObjectID=request.getParameter("ObjectID");
String tObject=request.getParameter("ObjectName");
String tIndexCode=request.getParameter("FIIndexCode");
String tIndexName=request.getParameter("FIIndexName");
//页面参数
String PageFlag=request.getParameter("pageflag"); 
String apptype = request.getParameter("apptype");
//打印参数
System.out.println("tVersionNo------------->"+tVersionNo);
System.out.println("tMaintNo------------->"+tMaintNo);
System.out.println("tBusType------------->"+tBusType);
System.out.println("tBusTypeID------------->"+tBusTypeID);
System.out.println("tBusTypeName------------->"+tBusTypeName);
System.out.println("tDetailType------------->"+tDetailType);
System.out.println("tObjectID------------->"+tObjectID);
System.out.println("tObject------------->"+tObject);
System.out.println("tIndexCode------------->"+tIndexCode);
System.out.println("tIndexName------------->"+tIndexName);

System.out.println("PageFlag------------->"+PageFlag);
System.out.println("apptype------------->"+apptype);

FIBusTypeDefUI tFIBusTypeDefUI = new FIBusTypeDefUI();
//FMBusTypeDefUI tFMBusTypeDefUI = new FMBusTypeDefUI(); 
 
  
//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = request.getParameter("transact");

GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getAttribute("GI");
 
try
{
	//准备传输数据VData
	VData tVData = new VData();
	TransferData mTransferData = new TransferData();
	
	//直接传输schema
	FMBnTypeDefSchema tFMBnTypeDefSchema = new FMBnTypeDefSchema();
	FIBnTypeDefSchema tFIBnTypeDefSchema = new FIBnTypeDefSchema();
	
	mTransferData.setNameAndValue("transact",transact);
	mTransferData.setNameAndValue("PageFlag",PageFlag);
  
	tVData.add(tG);
	tVData.add(PageFlag);
	
	if("C".equals(PageFlag))
	{
		System.out.println("FIBusTypeDefSave.jsp---->进入修改原始表后台程序");
		tFIBnTypeDefSchema.setVersionNo("00000000000000000001");
		tFIBnTypeDefSchema.setBusinessID(tBusTypeID);
		tFIBnTypeDefSchema.setBusType(tBusType);
		tFIBnTypeDefSchema.setBusinessName(tBusTypeName);
		tFIBnTypeDefSchema.setDetailType(tDetailType);
		tFIBnTypeDefSchema.setIndexCode(tIndexCode);
		tFIBnTypeDefSchema.setIndexName(tIndexName);
		tFIBnTypeDefSchema.setObject(tObject);
		tFIBnTypeDefSchema.setObjectID(tObjectID);
		tFIBnTypeDefSchema.setState("1");//状态先置01
		
		tVData.add(tFIBnTypeDefSchema);
		tVData.add(tMaintNo);
		tFIBusTypeDefUI.submitData(tVData,transact);
	}
//	if("X".equals(PageFlag))
//	{
//		System.out.println("FIBusTypeDefSave.jsp---->进入修改备份表后台程序");
//		tFMBnTypeDefSchema.setVersionNo(tVersionNo);
//		tFMBnTypeDefSchema.setMaintNo(tMaintNo);
//		tFMBnTypeDefSchema.setBusinessID(tBusTypeID);
//		tFMBnTypeDefSchema.setBusType(tBusType);
//		tFMBnTypeDefSchema.setBusinessName(tBusTypeName);
//		tFMBnTypeDefSchema.setDetailType(tDetailType);
//		tFMBnTypeDefSchema.setIndexCode(tIndexCode);
//		tFMBnTypeDefSchema.setIndexName(tIndexName);
//		tFMBnTypeDefSchema.setObject(tObject);
//		tFMBnTypeDefSchema.setObjectID(tObjectID);
//		tFMBnTypeDefSchema.setState("01");//状态先置01
//	  
//		tVData.add(tFMBnTypeDefSchema);
//		tFMBusTypeDefUI.submitData(tVData,transact);
//	}

}
catch(Exception ex)
{
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tFIBusTypeDefUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 保存成功! ";
		FlagStr = "Success";
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
