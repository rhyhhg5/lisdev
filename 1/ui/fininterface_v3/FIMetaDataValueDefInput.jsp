<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FIMetaDataValueDefInput.jsp
 //程序功能：元数据值域定义
 //创建日期：2011/9/13
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIMetaDataValueDef.js"></SCRIPT>
  <%@include file="FIMetaDataValueDefInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIMetaDataValueDefSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >属性值域信息</td>
  </tr>
</table>
<div align=center>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanMetadataValueGrid" >
     </span> 
      </td>
   </tr>
</table>
<INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
<INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
<INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
<INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
</div>
<br>

<table class=common>
	<tr class=common>	
		<td class=title>属性编码</td>
		<td class=input><input class=common name=AttNo readonly=true></td>
		<td class=title>值域</td>
		<td class=input><input class=common name=MainValues ></td>
		<td class=title></td>
		<td class=input></td>		
	</tr>
	<tr class=common>
		<td class=title>值域含义</td>
		<td class=input><input class=common name=ValueMeaning ></td>
		<td class=title>说明</td>
		<td class=input><input class=common name=Remark ></td>
		<td class=title></td>
		<td class=input></td>
	</tr>
</table>
<br>
<INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
<INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
<INPUT VALUE="重  置" TYPE=button class= cssbutton name= resetbutton onclick="initForm()">
<br>

<input type=hidden name=hideOperate>
<input type=hidden name=SerialNo>
<input type=hidden name=MetadataNo>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
