<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<%
 //程序名称：FIVoucherTempDefInput.jsp
 //程序功能：凭证模板定义
 //创建日期：2011/8/27
 //创建人  ：   曹淑国
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIVoucherTempDef.js"></SCRIPT>
  <%@include file="FIVoucherTempDefInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIVoucherTempDefSave.jsp" method=post name=fm target="fraSubmit">
<table>
  <tr>
    <td class="titleImg" >凭证模板定义</td>
  </tr>
</table>
<div id="divPageFlagC1" style="display:''">
	<td class=button width="10%" align=right>
			<INPUT class=cssButton name="querybutton" VALUE="版本信息查询"  TYPE=button onclick="return queryClick1();">
	</td>  	
</div>	
<table class= common border=0 width=100%>			  	
	<tr class= common>       
		<TD class= title>版本编号</TD>
		<TD class=input>
		 	<Input class= readonly name=VersionNo value= '' readonly=true>   					 	 
		</TD>
		 
		<TD class= title>版本状态</TD>
		<TD class=input>
			<Input class= readonly name=VersionState2 value = '' readonly=true>
		</TD>				 								
	</tr>  
</table> 
<hr></hr> 
<table>
	<tr>
		<td class="titleImg" >凭证信息定义</td>
	</tr>
</table>
<div id="divPageFlagC2" style="display:''">
	<input value="凭证类型查询"  onclick="return Vouchertypequery();" class="cssButton" type="button" >
</div>
<table  class="common" >
	<tr class="common">   
    	<td class="title">凭证编码</td>
    	<td class="input">
    		<input class="common" name="VoucherID" elementtype=nacessary>
    	</td>  
	    <td class="title">凭证名称</td>
	    <td class="input">
	    	<input class="common" name="VoucherName" elementtype=nacessary>
	    </td> 
	    <td class="title">凭证类型</td>
	    <td class="input" >
	    	<input class="codeno" name="VoucherType" ondblclick="return showCodeList('fivouchertype',[this,VoucherTypeName],[0,1]);" 
	    		onkeyup="return showCodeListKey('fivouchertype',[this,VoucherTypeName],[0,1]);" readonly = "true"><input class="codename" name="VoucherTypeName" readonly = "true" elementtype=nacessary>
	    </td> 	     
	</tr>
	<tr class="common">   
		<td class="title">凭证描述</td>
	    <td class="input">
	    	<textarea class="common" name="Remark" cols="40%" rows="4" ></textarea>
	    </td>  
	</tr>
</table>
<p>
<div id="divButton1" style="display:'none'">  	
    <INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">     
    <INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
    <INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
    <INPUT VALUE="重  置" TYPE=button class= cssbutton name="resetbutton" onclick="return resetAgain();">   
</div> 
<!-- 
<div id="divButton2" style="display:'none'"> 
	<INPUT VALUE=" 保  存 " TYPE=button class= cssbutton name="resetbutton" onclick="return copyClick();"> 
    <INPUT VALUE="生成副本数据" TYPE=button class= cssbutton name="resetbutton" onclick="return copyClick();">   
</div>
 -->
<hr></hr>
<br>
<input value="关联业务交易 "  onclick='gotovoucher()' class="cssButton" type="button" >
<input value="账务规则定义"  onclick="gotovoucherfee()" class="cssButton" type="button" >
<INPUT type=hidden name=VersionState> <!-- VersionState用来存版本状态：01，02，03；而VersionState2存版本状态：正常、维护、删除-->
<INPUT type=hidden name=PageFlag> <!-- X:版本维护  ; C:参数管理 ; Q :查询 -->
<INPUT type=hidden name=Transact> <!-- 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录-->
<INPUT type=hidden name=MaintNo>
<INPUT type=hidden name=fmTransact>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
