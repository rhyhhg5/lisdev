<%
  //程序名称：FifindatacheckInit.jsp
  //程序功能：元数据定义
  //创建日期：2011/9/15
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
 // String Code_T = request.getParameter("Code_T");  
 //arrResult = Code_T();
	//	fm.all("Code_TName").value=arrResult[0][0];
  String VersionNo = request.getParameter("VersionNo"); 
  String Ruleid = request.getParameter("Ruleid");
  
%>
<script type="text/javascript">

function initForm(){
	try{
		initDataRuleGrid();			
		fm.all("VersionNo").value=<%=VersionNo%>;
		fm.all("ruledefid").value=<%=Ruleid%>;
		initQuery1();
		var arrResult = new Array();				
	}
	catch(re){
		alert("FifindatacheckInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initDataRuleGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;
		
		iArray[1]=new Array();
		iArray[1][0]="规则编码";
		iArray[1][1]="50px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="规则名称";
		iArray[2][1]="80px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="算法编码";
		iArray[3][1]="50px";
		iArray[3][2]=100;
		iArray[3][3]=0;
		
		iArray[4]=new Array();
		iArray[4][0]="算法名称";
		iArray[4][1]="80px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="算法类型";
		iArray[5][1]="30px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="算法类型名称";
		iArray[6][1]="80px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		
		iArray[7]=new Array();
		iArray[7][0]="节点";
		iArray[7][1]="30px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="节点类型";
		iArray[8][1]="80px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="父节点";
		iArray[9][1]="30px";
		iArray[9][2]=100;
		iArray[9][3]=0;		
		
		iArray[10]=new Array();
		iArray[10][0]="规则状态";
		iArray[10][1]="50px";
		iArray[10][2]=100;
		iArray[10][3]=0;		

		
		DataRuleGrid = new MulLineEnter( "fm" , "DataRuleGrid" ); 

		DataRuleGrid.mulLineCount=2;
		DataRuleGrid.displayTitle=1;
		DataRuleGrid.canSel=1;
		DataRuleGrid.canChk=0;
		DataRuleGrid.hiddenPlus=1;
		DataRuleGrid.hiddenSubtraction=1;
		DataRuleGrid.selBoxEventFuncName = "getData";
		DataRuleGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
