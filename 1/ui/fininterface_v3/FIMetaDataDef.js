//该文件中包含客户端需要处理的函数和事件

//程序名称：FIMetaDataDef.js
//程序功能：元数据定义
//创建日期：2011/9/13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var mDebug="0";
var mOperate="";
var turnPage = new turnPageClass();

//查询按钮
function MetadateQuery()
{
	//下面增加相应的代码
	//mOperate="QUERY||MAIN";  
	showInfo=window.open("./FrameFIMetaDataQueryInput.jsp");
}


//增加按钮
function addClick()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	//判断是否是新值插入
	var checksql="select count(*) from FIMetadataDef where MetadataNo = '"+fm.MetadataNo.value+"'";
	var re = easyExecSql(checksql);
	
	if(re!="0")
	{
		alert("元数据编码已存在！");
		return false;
	}
	
	//为了防止双击，点击增加后，屏蔽"增加"按钮
	mOperate="INSERT||MAIN";
	submitForm();
}

//删除按钮
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("MetadataNo").value==null)||(trim(fm.all("MetadataNo").value)==''))
    alert("请确定要删除的元数据编码！");
  
  else
  {

    if (confirm("您确实想删除该记录吗?"))
    {
		//判断是否有原纪录
		var checksql="select count(*) from FIMetadataDef where MetadataNo = '"+fm.MetadataNo.value+"'";
		var re = easyExecSql(checksql);
		
		if(re=="0")
		{
			alert("元数据编码不存在！");
			return false;
		}
		mOperate="DELETE||MAIN";
		submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}

//修改按钮
function updateClick()
{
  //提交前的检验
  if (!beforeSubmit()) //beforeSubmit()函数
  {
  	return false;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
	   //判断是否有原纪录
		var checksql="select count(*) from FIMetadataDef where MetadataNo = '"+fm.MetadataNo.value+"'";
		var re = easyExecSql(checksql);
		
		if(re=="0")
		{
			alert("元数据编码不存在！");
			return false;
		}
		mOperate="UPDATE||MAIN";
		submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

function submitForm()
{ 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.action="./FIMetaDataDefSave.jsp";
  //lockButton(); 
  fm.submit(); //提交
}

//保存按钮
function SaveDate()
{
	var tSel = MetadataAttGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		//top.close();
		alert( "请您先录入一条记录，再点击保存。" );
		return;
	}
}

//值域定义按钮
function gotodefin(){

	var tSel = MetadataAttGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
	{
		//top.close();
		alert( "请您先选择一条记录，再点击值域定义。" );
		return;
	}
	var maintappno = MetadataAttGrid.getRowColData(tSel-1,1);
	showInfo=window.open("./FIMetaDataValueDefInput.jsp?maintappno="+maintappno);	
}


//提交前的校验、计算  
function beforeSubmit()
{
	if(fm.MetadataNo.value == '')
	{
		alert("元数据编码不能为空！");
		return false;
	}
	if(fm.MetadataName.value == '')
	{
		alert("元数据名称不能为空！");
		return false;
	}
	if(fm.Object.value == '')
	{
		alert("元数据对象不能为空！");
		return false;
	}
	if(fm.DataSource.value == '')
	{
		alert("数据源不能为空！");
		return false;
	}
	if(fm.StartDate.value == '')
	{
		alert("生效日不能为空！");
		return false;
	}
	if(fm.State.value == '')
	{
		alert("状态不能为空！");
		return false;
	}
	
	return true;
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
//针对版本信息查询子窗口返回的2维数组
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
		//alert(arrQueryResult);

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		//添加返回值信息
		fm.MetadataNo.value = arrQueryResult[0][0];
		fm.MetadataName.value = arrQueryResult[0][1];

		fm.Object.value = arrQueryResult[0][2];
		fm.DataSource.value = arrQueryResult[0][3];
				
		fm.StartDate.value = arrQueryResult[0][4];
		fm.EndDate.value = arrQueryResult[0][5];
		fm.State.value = arrQueryResult[0][6];
		fm.remark.value = arrQueryResult[0][7];
		fm.FIMDType.value = arrQueryResult[0][8];
		

		showCodeName(); 

	}
}

//进入元数据属性定义页面
function GotoDataAttDefPage()
{
	if(fm.MetadataNo.value == '')
	{
		alert("元数据编码不能为空！");
		return false;
	}
	if(fm.MetadataName.value == '')
	{
		alert("元数据名称不能为空！");
		return false;
	}
	
	var tMetadataNo = fm.MetadataNo.value;
	
	showInfo=window.open("./FrameFIMetaDataAttDefInput.jsp?MetadataNo="+tMetadataNo);
}