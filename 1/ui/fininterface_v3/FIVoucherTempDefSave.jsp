<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<% 
//程序名称：FIVoucherTempDefSave.jsp
//程序功能：凭证模板定义
//创建日期：2011/8/27
//创建人  ：   曹淑国
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%
//接收信息，并作校验处理。
//输入参数
request.setCharacterEncoding("GBK");

//××××Schema t××××Schema = new ××××Schema();
FIVoucherDefSchema tFIVoucherDefSchema = new FIVoucherDefSchema();
FMVoucherDefSchema tFMVoucherDefSchema = new FMVoucherDefSchema();

FIVoucherTempDefUI tFIVoucherTempDefUI = new FIVoucherTempDefUI();

//输出参数
CErrors tError = null;
String tRela  = "";                
String FlagStr = "";
String Content = "";
 
//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
String tTransact = request.getParameter("fmTransact");
String tPageFlag = request.getParameter("PageFlag");
 
GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getAttribute("GI");

TransferData mTransferData = new TransferData();
mTransferData.setNameAndValue("PageFlag",tPageFlag);
System.out.println("PageFlag=========:"+tPageFlag);
System.out.println("tTransact=========:"+tTransact);
//准备传输数据VData
VData tVData = new VData();
	tVData.add(tG);
	tVData.add(mTransferData);
//从url中取出参数付给相应的schema
//t××××Schema.set××××(request.getParameter("××××"));
if("C".equals(tPageFlag))
{
	tFIVoucherDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFIVoucherDefSchema.setVoucherID(request.getParameter("VoucherID"));
	tFIVoucherDefSchema.setVoucherName(request.getParameter("VoucherName"));
	tFIVoucherDefSchema.setVoucherType(request.getParameter("VoucherType"));
	tFIVoucherDefSchema.setRemark(request.getParameter("Remark"));
	tFIVoucherDefSchema.setState("01");
		try
		{
			//传输schema
			tVData.addElement(tFIVoucherDefSchema);
			tFIVoucherTempDefUI.submitData(tVData,tTransact);
		}
		catch(Exception ex)
		{
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
}
if("X".equals(tPageFlag))
{
	
}


 
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tFIVoucherTempDefUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 保存成功! ";
		FlagStr = "Success";
	}
	else 
	{
	   Content = " 保存失败，原因是:" + tError.getFirstError();
	   FlagStr = "Fail";
	}
}
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=tTransact%>");
</script>
</html>
