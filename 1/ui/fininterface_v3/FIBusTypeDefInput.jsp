<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
  <title>业务交易定义</title>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIBusTypeDef.js"></SCRIPT>
  <%@include file="FIBusTypeDefInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIBusTypeDefSave.jsp" method=post name=fm target="fraSubmit">
<table>
  <tr>
    <td class="titleImg" >业务交易定义</td>
  </tr>
</table>

<hr></hr>
<div id=bustypediv>
<input value="业务类型查询"  onclick="return bustypequery();" class="cssButton" type="button" >
<BR>
</div>
<table  class="common" >
	<tr class="common">
		<td class="title">业务交易编号</td><td class="input"><input class="common" name="BusTypeId" elementtype=nacessary ></td>  
		<td class="title">业务交易名称</td><td class="input"><input class="common" name="BusTypeName" ></td>  
		<td class="title">业务大类</td>
		<td class="input" >
			<input class="codeno" name="FIBusType" 
				ondblclick="return showCodeList('fibustype',[this,FIBusTypename],[0,1]);" 
				onkeyup="return showCodeListKey('fibustype',[this,FIBusTypename],[0,1]);" readonly = "true"><input class="codename" name="FIBusTypename" readonly = "true">
		</td> 
	</tr>
	<tr class="common">			 
	    <td class="title">业务类别</td>
	    <td class="input">
	  		<Input class=codeno name=FIDetailType 
	  			ondblClick="return showCodeList('fidetailbustype',[this,FIDetailTypeName],[0,1],null,fm.FIBusType.value,'othersign');" 
	  			onkeyup="return showCodeListKey('fidetailbustype',[this,FIDetailTypeName],[0,1],null,fm.FIBusType.value,'othersign');"><input class=codename name=FIDetailTypeName readonly=true elementtype=nacessary>
	    </td>  
	    <td class="title">业务对象</td>
	    <td class="input">
	    	<input class="codeno" name="ObjectID" 
	    		ondblclick="return showCodeList('fibusobj',[this,ObjectName],[0,1]);" 
	    		onkeyup="return showCodeListKey('fibusobj',[this,ObjectName],[0,1]);" readonly = "true"><input class="codename" name="ObjectName" readonly = "true" >
	    </td>  
	    <td class="title">索引标识</td>
	    <td class="input">
			<Input class=codeno name=FIIndexCode verify="索引标识|NOTNULL" 
				ondblclick="return showCodeList('fiindexid',[this,FIIndexName],[0,1]);" 
				onkeyup="return showCodeListKey('fiindexid',[this,FIIndexName],[0,1]);"><input class=codename name=FIIndexName readonly=true elementtype=nacessary> 
	    </td>  
	</tr>
</table>
<Div  id= "buttonsdiv" style= "display: '' "> 
<INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">
<INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
<INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
<INPUT VALUE="重  置" TYPE=button class= cssbutton onclick="return resetAgain();">  
</Div>
<hr></hr>
<br>
<input value="业务信息定义"  onclick="businfodef();" class="cssButton" type="button" >
<input value="费用信息定义"  onclick="busfeetypeinfodef();" class="cssButton" type="button" >

<input type=hidden name=maintno >
<input type=hidden name=pageflag >
<input type=hidden name=apptype >
<input type=hidden name=transact >

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
