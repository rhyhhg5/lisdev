<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIBusFeeTypeDefSave.jsp
//程序功能：业务交易费用定义
//创建日期：2011/8/25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%
 //接收信息，并作校验处理。
 PubFun pub = new PubFun();
 UpLoadWriteFinBL RI = new UpLoadWriteFinBL();
 //输入参数
 String FileName = "";
	
	//得到excel文件的保存路径
 	String ImportPath = request.getParameter("ImportPath");		
 	String path = application.getRealPath("").replace('\\','/')+'/';
 	String tVersionNo = request.getParameter("VersionNo");
 	ImportPath= path + ImportPath;
	System.out.println("ImportPath: "+ImportPath);
	System.out.println("Path: "+path);
	System.out.println("VersionNo: "+tVersionNo);

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("VersionNo",tVersionNo);
	VData tVData = new VData();
	tVData.add(tTransferData);

 //输出参数
 CErrors tError = null;        
 String FlagStr = "";
 String Content = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getAttribute("GI");
 
 try{
  
  
  	tVData.add(tG); 
 	RI.submitData(tVData,ImportPath);   
 } 
	 catch(Exception ex){
	  Content = "保存失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr==""){
  tError = RI.mErrors;
  if (!tError.needDealError()){                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }
  else  {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
window.location.href="../fininterface_v3/Financialmodel/Finnew.xls";
</script>
</html>
