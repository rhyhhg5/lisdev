<html>
<%
//程序名称 :FIRuleDealErrLogInput.jsp
//程序功能 :错误数据处理
//创建人 :范昕
//创建日期 :2008-09-09
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。 
 	%>
<script>

  var comcode = "<%=tGI1.ComCode%>";
  var RuleDealBatchNo = <%=request.getParameter("RuleDealBatchNo")%>;
  var checktype = <%=request.getParameter("checktype")%>;
  var EventNo = <%=request.getParameter("EventNo")%>;
  var RulePlanID = <%=request.getParameter("RulePlanID")%>;
</script>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src = "FIQualityErrorDataLogInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FIQualityErrorDataLogInit.jsp"%>
</head>
<body  onload="initForm();queryFIRuleDealErrLogGrid();initElementtype();">
  <form action="" method=post name=fm target="fraSubmit">
  <Div id= "divLLReport1" style= "display: ''">
  	<table class= common border=0 width=100%>
  		<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFIRuleDealErrLogGrid);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
    <Div  id= "divFIRuleDealErrLogGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanFIRuleDealErrLogGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT CLASS="cssButton" VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
      <INPUT CLASS="cssButton" VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
      <INPUT CLASS="cssButton" VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
      <INPUT CLASS="cssButton" VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
   </Div>
   
  <table class= common>
	<INPUT VALUE="导出错误清单" class = cssButton TYPE=button onclick="ToExcel();" >  
	<INPUT VALUE="忽略错误" class = cssButton TYPE=button onclick="DealErrdata();" >  
	</table>   
   <hr></hr>
  <table class= common>
	<tr class= common>
				 <TD class= title>
					  校验批次号码
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=RuleDealBatchNo readonly=true >
				</TD>
				<TD class= title>
					  事件号码
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=EventNo readonly=true >
				</TD>
				<TD class= title>
					  错误信息
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=ErrInfo readonly=true >
				</TD>
	</tr>
	<tr class= common>				
				 <TD class= title>
					  业务号码标识
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=businessno readonly=true >
				</TD>
				<TD class= title>
					  校验计划
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=RulePlanID readonly=true >
				</TD>
				 <TD class= title>
					  校验规则
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=RuleID readonly=true >
				</TD>
	</tr>
	<tr class= common>				
				<TD class= title>
					  处理状态
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=DealState readonly=true >
				</TD>

	</tr>
	</table>
  <Input type=hidden name=CertificateID readonly=true >
  <input type=hidden NAME="OperateType" VALUE=''>
  <input type=hidden NAME="DelErrSerialNo" VALUE=''>
  <input type=hidden NAME="ExpSQL" VALUE=''>
</table>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>