<%
  //程序名称：FI
  //程序功能：业务交易费用定义
  //创建日期：2011/8/25
  //创建人  ： 董健
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">
var BusTypeId="";
var pageflag="";
var maintno="";
function initForm(){
	try
	{	
		initBudgetBusGrid();		
		initBudgetGrid();
		initpage();
	}
	catch(re){
		alert("FIBudgetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}



function initBudgetBusGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=50;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="类型编码";
		iArray[1][1]="30px";
		iArray[1][2]=50;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="业务类型";
		iArray[2][1]="80px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="收付类型";
		iArray[3][1]="40px";
		iArray[3][2]=100;
		iArray[3][3]=0;
		
		BudgetBusGrid = new MulLineEnter( "fm" , "BudgetBusGrid" ); 

		BudgetBusGrid.mulLineCount=2;
		BudgetBusGrid.displayTitle=1;
		BudgetBusGrid.canSel=1;
		BudgetBusGrid.canChk=0;
		BudgetBusGrid.hiddenPlus=1;
		BudgetBusGrid.hiddenSubtraction=1;
		BudgetBusGrid.selBoxEventFuncName = "getBudget";

		BudgetBusGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initBudgetGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="收付类型";
		iArray[1][1]="30px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="业务类型一级编码";
		iArray[2][1]="30px";
		iArray[2][2]=50;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="业务类型";
		iArray[3][1]="30px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="业务类型二级编码";
		iArray[4][1]="30px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="类别";
		iArray[5][1]="80px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="现金流量码";
		iArray[6][1]="50px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		
		
		BudgetGrid = new MulLineEnter( "fm" , "BudgetGrid" ); 

		BudgetGrid.mulLineCount=2;
		BudgetGrid.displayTitle=1;
		BudgetGrid.canSel=0;
		BudgetGrid.canChk=0;
		BudgetGrid.hiddenPlus=1;
		BudgetGrid.hiddenSubtraction=1;
//		BudgetGrid.selBoxEventFuncName = "showfeeinfo";

		BudgetGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
