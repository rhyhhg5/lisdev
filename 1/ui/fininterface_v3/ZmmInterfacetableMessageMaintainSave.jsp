<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ZmmInterfacetableMessageMaintainSave.jsp
//程序功能：
//创建日期：2017-11-06
//创建人  ： 张美美
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.f1print.ZmmInterfaceTableMessageMaintainBL"%>
<%@page import="com.sinosoft.lis.f1print.ZmmInterfaceTableMessageMaintainUI" %>

<%
  System.out.println("数据维护开始执行Save页面");
  request.setCharacterEncoding("GBK");
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String mOperate = request.getParameter("mOperate");
  mOperate = mOperate.trim();
  System.out.println("操作的类型是" +  mOperate);
   CErrors tError = null;
   String FlagStr = "";
   String Content = "";
  
    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    String Batchno =request.getParameter("Batchno");
	String Serialno =request.getParameter("Serialno");
	String Costcenter =request.getParameter("Costcenter");
	String Chinal =request.getParameter("Chinal");
	String Chargedate =request.getParameter("Chargedate");
	String Readstate =request.getParameter("Readstate");
	
	System.out.println("Batchno : "+Batchno);
	System.out.println("Serialno : "+Serialno);
	System.out.println("Costcenter : "+Costcenter);
	System.out.println("Chinal : "+Chinal);
	System.out.println("Chargedate : "+Chargedate);
	System.out.println("Readstate : "+Readstate);
	
	tTransferData.setNameAndValue("Batchno",Batchno);
	tTransferData.setNameAndValue("Serialno",Serialno);
	tTransferData.setNameAndValue("Costcenter",Costcenter);
	tTransferData.setNameAndValue("Chinal",Chinal);
	tTransferData.setNameAndValue("Chargedate",Chargedate);
	tTransferData.setNameAndValue("Readstate",Readstate);

	System.out.println("--------------------------------");

  
      tVData.add(tG);
      tVData.add(tTransferData);
      
      ZmmInterfaceTableMessageMaintainUI zmmInterfacetableMessageMaintainUI = new ZmmInterfaceTableMessageMaintainUI();
  	try
	{   
      if (!zmmInterfacetableMessageMaintainUI.submitData(tVData, mOperate))
      {

          Content = "维护数据失败，原因是:" + zmmInterfacetableMessageMaintainUI.mErrors.getFirstError();
          FlagStr = "Fail";
      }
    }
	catch(Exception ex)
	{
	    Content = "失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = zmmInterfacetableMessageMaintainUI.mErrors;
		if (!tError.needDealError())
		{     
			Content = "操作已成功!";
			FlagStr = "Succ";
		}
		else
 		{
 			Content = " 操作失败，原因是:" + tError.getFirstError();
 			FlagStr = "Fail";
 		}
	}
%>

<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
window.focus;
</script>
</html>