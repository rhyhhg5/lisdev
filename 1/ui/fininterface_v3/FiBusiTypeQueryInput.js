 //               该文件中包含客户端需要处理的函数和事件
//Creator :范昕	
//Date :2008-09-17

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();


//查询按钮
function busTypeQuery()
{
	var strSQL = "select VersionNo,BusinessID,BusinessName,(select codename from ldcode where code=BusType and codetype = 'fibustype')," +
	"(select codename from ldcode where code=DetailType and codetype = 'fidetailbustype'),Object,IndexCode,IndexName,ObjectID,Remark"
	         +" from FIBnTypeDef where 1=1  "
	         +getWherePart('BusinessID','busTypeId')
	         +getWherePart('BusinessName','busTypeName')
	         +getWherePart('BusType','FIBusType')
	         +getWherePart('DetailType','FIDetailType')
	         +getWherePart('ObjectID','ObjectID')
	         +getWherePart('Indexcode','DetailIndexID');
	strSQL = strSQL+" order by BusType,DetailType,BusinessID ";        
	turnPage.queryModal(strSQL,BusiTypeGrid);

}

//返回按钮对应的操作
function returnParent()
{
	var arrReturn = new Array();
	var tSel = BusiTypeGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请您先选择一条记录，再点击返回按钮。" );
	else
	{

		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			//alert( "没有发现父窗口的afterQuery接口。" + ex );
			alert(ex.message);
		}
		top.close();
	}
}



function getQueryResult()
{
	var arrSelected = null;
	tRow = BusiTypeGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();
	//tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	//arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
		// 书写SQL语句
	var strSQL = "";	
	strSQL = "select VersionNo,BusinessID,BusinessName,BusType,DetailType,ObjectID,Object,IndexCode ,IndexName,"
		 	 +" (select codename from ldcode where codetype = 'fibustype' and code=bustype),"
	         +" (select codename from ldcode where codetype = 'fidetailbustype' and code=DetailType)"
	         +"  from FIBnTypeDef where 1=1 and BusinessID='"+BusiTypeGrid.getRowColData(tRow-1,2)+"' "
	         
		
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }  
	//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
