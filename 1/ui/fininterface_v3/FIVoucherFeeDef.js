//该文件中包含客户端需要处理的函数和事件

//程序名称：FIVoucherFeeDef.js
//程序功能：凭证费用定义
//创建日期：2011/8/27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//初始化页面
function initpage()
{
	
//	var tsql = "select versionno,voucherid,(select vouchername from FIVoucherTypeDef a where a.voucherid = FIFeeTypeDef.voucherid),feeid,feename,d_finitemid,d_finitemname,c_finitemid,c_finitemname,remark"
//		+" from FIFeeTypeDef where versionno = '"+versionno+"' and voucherid = '"+voucherid+"' and exists (select 1 from FIVoucherTypeBus b where FIFeeTypeDef.voucherid=b.voucherid and FIFeeTypeDef.bustypeid=b.bustypeid)";
/*	 var  tsql="select '"+versionno+"' versionno,a.voucherid,(select vouchername from FIVoucherDef c where 1=1 "
			+" and c.voucherid = a.voucherid),b.feeid,b.FeeName"
			+" ,(select d_finitemid from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.feeid) d_finitemid"
			+" ,(select d_finitemname from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.feeid) d_finitemid"
			+" ,(select c_finitemid from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.feeid) d_finitemid"
			+" ,(select c_finitemname from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.feeid) d_finitemid"
			+" ,a.BusinessID from FIVoucherBn a,FIVoucherBnFee b WHERE "
			+" a.voucherid='"+voucherid+"' and a.voucherid=b.voucherid and a.BusinessID=b.BusinessID";
			*/
	var  tsql="select '"+versionno+"' versionno,a.voucherid,(select vouchername from FIVoucherDef c where 1=1 "
			+" and c.voucherid = a.voucherid),b.costid,b.costname"
			+" ,(select d_finitemid from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.costid) d_finitemid"
			+" ,(select d_finitemname from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.costid) d_finitemid"
			+" ,(select c_finitemid from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.costid) d_finitemid"
			+" ,(select c_finitemname from FIVoucherBnFee d where d.voucherid=a.voucherid and d.BusinessID=a.BusinessID and d.feeid=b.costid) d_finitemid"
			+" ,a.BusinessID from FIVoucherBn a,FIBnFeeTypeDef b WHERE "
			+" a.voucherid='"+voucherid+"' and a.BusinessID=b.BusinessID";
	
	
	initVoucherFeeGrid();
	turnPage.queryModal(tsql,VoucherFeeGrid);
}


//保存按钮
function addclick(){

	var tGridNo = VoucherFeeGrid.mulLineCount;
//	var GridNo = document.getElementsByName("VoucherFeeGridNo");
//	var D_Finitemid = document.getElementsByName("VoucherFeeGrid5");
//	var C_Finitemid = document.getElementsByName("VoucherFeeGrid8");
//	var Feeid = document.getElementsByName("VoucherFeeGrid4");
//	alert(tGridNo);
	for(i=0;i<tGridNo;i++)
	{	
//		alert(VoucherFeeGrid.getRowColData(i,6));
	if((VoucherFeeGrid.getRowColData(i,6)==""||VoucherFeeGrid.getRowColData(i,6)==null)&&(VoucherFeeGrid.getRowColData(i,8)==""||VoucherFeeGrid.getRowColData(i,8)==null))
		{
			confirm("费用编号为"+VoucherFeeGrid.getRowColData(i,4)+"的费用，借贷均为空，不合法，是否确认提交？");
			
		}
	}
	fm.submit();

}


//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
  
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){

  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    location.reload();
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}
