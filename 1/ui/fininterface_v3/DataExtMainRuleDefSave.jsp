<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIDataExtractRulesSave.jsp
//程序功能：数据提取规则定义
//创建日期：2011/9/13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%> 

<%
request.setCharacterEncoding("GBK");
//接收信息，并作校验处理。 
String tVersionNo = request.getParameter("VersionNo");
String tArithmeticID = request.getParameter("ArithmeticID");
String tArithmeticName = request.getParameter("ArithmeticName");
String tRunObject = request.getParameter("RunObject");
String tSubType = request.getParameter("SubType");
String tBuType = request.getParameter("BuType");
String tDealMode = request.getParameter("DealMode");
String tDealClass = request.getParameter("DealClass");
String tFileName = request.getParameter("FileName");
String tMainSQL = request.getParameter("MainSQL");
String tReturnRemark = request.getParameter("ReturnRemark");
String tPrArithmeticID = request.getParameter("PrArithmeticID");
String tState = request.getParameter("State");

tMainSQL=tMainSQL.replaceAll("'","''");
System.out.println(tMainSQL);

//输入参数
//××××Schema t××××Schema = new ××××Schema();
FIDataExtractRulesSchema tFIDataExtractRulesSchema = new FIDataExtractRulesSchema();
FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
FIDataExtractRulesUI tFIDataExtractRulesUI = new FIDataExtractRulesUI();

//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = "";
 
GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getAttribute("GI");
 
//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = request.getParameter("hideOperate");
 
//从url中取出参数付给相应的schema

tFIDataExtractRulesSchema.setVersionNo(tVersionNo);
tFIDataExtractRulesSchema.setArithmeticID(tArithmeticID);
tFIDataExtractRulesSchema.setArithmeticName(tArithmeticName);
tFIDataExtractRulesSchema.setRunObject(tRunObject);
tFIDataExtractRulesSchema.setArithmeticType("00");
tFIDataExtractRulesSchema.setSubType(tSubType);
tFIDataExtractRulesSchema.setBuType(tBuType);
tFIDataExtractRulesSchema.setDealMode(tDealMode);
tFIDataExtractRulesSchema.setDealClass(tDealClass);
tFIDataExtractRulesSchema.setFileName(tFileName);
tFIDataExtractRulesSchema.setMainSQL(tMainSQL);
tFIDataExtractRulesSchema.setPrArithmeticID(tPrArithmeticID);
tFIDataExtractRulesSchema.setReturnRemark(tReturnRemark);
tFIDataExtractRulesSchema.setState(tState);


	try
	{
		//准备传输数据VData
		VData tVData = new VData();
		
		//传输schema
		tVData.add(tFIDataExtractRulesSchema);
		
		tVData.add(tG);
		tFIDataExtractRulesUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
 
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr==""){
	tError = tFIDataExtractRulesUI.mErrors;
	if (!tError.needDealError()){                          
	   Content = " 保存成功! ";
	   FlagStr = "Success";
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
