<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIBusFeeTypeDefSave.jsp
//程序功能：业务交易费用定义
//创建日期：2011/8/25
//创建人  ： 董健
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>

<%
//接收信息，并作校验处理。
request.setCharacterEncoding("GBK"); 
//接收信息，并作校验处理。
String tMaintNo=request.getParameter("maintno");
String tCostID=request.getParameter("costid");
String tProPertyCode=request.getParameter("FIBusInfoID");
String tProPertyCodeName=request.getParameter("FIBusInfoIDName");
String tPurPose=request.getParameter("PurPose");
String tPurPoseName=request.getParameter("PurPoseName");
String tFeeValues=request.getParameter("FeeValues");
String tpageflag=request.getParameter("pageflag");


System.out.println("tMaintNo--------------->"+tMaintNo);
System.out.println("tCostID--------------->"+tCostID);
System.out.println("tProPertyCode--------------->"+tProPertyCode);
System.out.println("tProPertyCodeName--------------->"+tProPertyCodeName);
System.out.println("tPurPose--------------->"+tPurPose);
System.out.println("tPurPoseName--------------->"+tPurPoseName);
System.out.println("tFeeValues--------------->"+tFeeValues);
System.out.println("tpageflag--------------->"+tpageflag);

//输入参数
//××××Schema t××××Schema = new ××××Schema();
FIBusFeeInfoDefUI tFIBusFeeTypeDefUI = new FIBusFeeInfoDefUI();
//FMBusFeeInfoDefUI tFMBusFeeInfoDefUI = new FMBusFeeInfoDefUI();
 
//输出参数
CErrors tError = null;
String tRela  = "";                
String FlagStr = "";
String Content = "";
String transact = "";
 
GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getAttribute("GI");
 
//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = request.getParameter("fmtransact");
 
//从url中取出参数付给相应的schema
//t××××Schema.set××××(request.getParameter("××××"));
 
try
{
	//准备传输数据VData
	VData tVData = new VData();
	tVData.add(tG);
	//传输schema
	//tVData.addElement(t××××Schema);
	FIBnFeeInfoSchema tFIBnFeeInfoSchema = new FIBnFeeInfoSchema();
	FMBnFeeInfoSchema tFMBnFeeInfoSchema = new FMBnFeeInfoSchema();
	
	if("X".equals(tpageflag))
	{
		tFMBnFeeInfoSchema.setMaintNo(tMaintNo);
		tFMBnFeeInfoSchema.setCostID(tCostID);
		tFMBnFeeInfoSchema.setPropertyCode(tProPertyCode);
		tFMBnFeeInfoSchema.setPropertyName(tProPertyCodeName);
		tFIBnFeeInfoSchema.setBusClass("02");
		tFMBnFeeInfoSchema.setPurpose(tPurPose);
		tFMBnFeeInfoSchema.setFeeValues(tFeeValues);
		
		tVData.add(tFMBnFeeInfoSchema);
		
//		tFMBusFeeInfoDefUI.submitData(tVData,transact);
	}
	else if("C".equals(tpageflag))
	{
		tFIBnFeeInfoSchema.setVersionNo("00000000000000000001");
		tFIBnFeeInfoSchema.setCostID(tCostID);
		tFIBnFeeInfoSchema.setPropertyCode(tProPertyCode);
		tFIBnFeeInfoSchema.setPropertyName(tProPertyCodeName);
		tFIBnFeeInfoSchema.setBusClass("02");
		tFIBnFeeInfoSchema.setPurpose(tPurPose);
		tFIBnFeeInfoSchema.setFeeValues(tFeeValues);
		
		tVData.add(tFIBnFeeInfoSchema);
		
		tFIBusFeeTypeDefUI.submitData(tVData,transact);
	}
	else
	{
		
	}
}
catch(Exception ex)
{
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}
 
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tFIBusFeeTypeDefUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 保存成功! ";
		FlagStr = "Success";
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
 
//添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
