<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIMetaDataValueDefSave.jsp
//程序功能：元数据值域定义
//创建日期：2011/9/13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%
request.setCharacterEncoding("GBK");
//接收信息，并作校验处理。
String tMetadataNo = request.getParameter("MetadataNo");
String tAttNo = request.getParameter("AttNo");
String tSerialNo = request.getParameter("SerialNo");
String tMainValues = request.getParameter("MainValues");
String tValueMeaning = request.getParameter("ValueMeaning");
String tRemark = request.getParameter("Remark");
 //输入参数
 FIMetadataAttValuesSchema tFIMetadataAttValuesSchema = new FIMetadataAttValuesSchema();
 FIMetaDataValueDefUI tFIMetaDataValueDefUI = new FIMetaDataValueDefUI();
 
 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getAttribute("GI");
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 transact = request.getParameter("hideOperate");
 
 System.out.println("transact============"+transact);
 
 if("INSERT||MAIN".equals(transact))
 {
	 tSerialNo=PubFun1.CreateMaxNo("FIValueSerNo",10);
 }
 System.out.println("tSerialNo==============="+tSerialNo);
 
 //从url中取出参数付给相应的schema
tFIMetadataAttValuesSchema.setMetadataNo(tMetadataNo);
tFIMetadataAttValuesSchema.setAttNo(tAttNo);
tFIMetadataAttValuesSchema.setSerialNo(tSerialNo);
tFIMetadataAttValuesSchema.setValueType("2");
tFIMetadataAttValuesSchema.setValueTypeName("非连续数据集");
tFIMetadataAttValuesSchema.setPatternType("=");
tFIMetadataAttValuesSchema.setMainValues(tMainValues);
tFIMetadataAttValuesSchema.setValueMeaning(tValueMeaning);
tFIMetadataAttValuesSchema.setRemark(tRemark);
 
 try{
  //准备传输数据VData
  VData tVData = new VData();
  
  //传输schema
  tVData.add(tFIMetadataAttValuesSchema);
  
  tVData.add(tG);
  tFIMetaDataValueDefUI.submitData(tVData,transact);
 }
 catch(Exception ex){
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr==""){
  tError = tFIMetaDataValueDefUI.mErrors;
  if (!tError.needDealError()){                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }
  else                                                                           {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
