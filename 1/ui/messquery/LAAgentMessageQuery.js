//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  //initForm();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAgentMessageQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           









//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


function returnParent()
{
  var arrReturn = new Array();
	var tSel = QualityAssessGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
				//initForm();
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	var tRow = QualityAssessGrid.getSelNo();
	if( tRow == 0 || tRow == null ||arrDataSet == null )
	  return arrSelected;
	arrSelected = new Array();
	//alert(tRow);
	var AgentCode=QualityAssessGrid.getRowColData(tRow-1,1);
	var AgentGroup=QualityAssessGrid.getRowColData(tRow-1,2);
	var idx=QualityAssessGrid.getRowColData(tRow-1,3)
	
	var strSQL = "select a.* , b.Name , c.BranchAttr from LAQualityAssess a, LAAgent b ,LABranchGroup c"
	            +" where a.AgentCode = '"+trim(AgentCode)
	            +"' and a.Idx = "+idx
	            +" and b.AgentCode = a.AgentCode"
	            +" and c.AgentGroup = a.AgentGroup";	           
	//alert(strSQL);
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	arrSelected = decodeEasyQueryResult(strQueryResult);
	//alert(arrSelected);
	
	//tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	
	//设置需要返回的数组
	
	return arrSelected;
}


// 查询按钮
function easyQueryClick()
{

  
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.* from laagent a "
	         + "where 1=1 "
	         + getWherePart('a.AgentCode','AgentCode');
  
  var arrSelected = null;
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  arrSelected = new Array();
  
  if (!strQueryResult) {
    alert("查询失败！");
    return false;
    }
  arrSelected = decodeEasyQueryResult(strQueryResult);	         	         
  
    fm.all('AgentGroup').value = arrSelected[0][1];
    fm.all('ManageCom').value = arrSelected[0][2];    
    fm.all('Name').value =arrSelected[0][5];
    
    fm.all('Sex').value = arrSelected[0][6];    
    fm.all('Birthday').value = arrSelected[0][7];
    
    fm.all('NativePlace').value = arrSelected[0][8];    
    fm.all('Nationality').value = arrSelected[0][9];
    
    fm.all('Marriage').value = arrSelected[0][10];    
    fm.all('CreditGrade').value = arrSelected[0][11];
    
    fm.all('HomeAddress').value = arrSelected[0][13];    
    fm.all('PostalAddress').value = arrSelected[0][14];
    
    fm.all('ZipCode').value = arrSelected[0][15];    
    fm.all('Phone').value = arrSelected[0][16];
    
    fm.all('BP').value = arrSelected[0][17];    
    fm.all('Mobile').value = arrSelected[0][18];
   
    fm.all('Email').value = arrSelected[0][19];    
    fm.all('IDNo').value = arrSelected[0][21];
    
    fm.all('Source').value = arrSelected[0][22];    
    fm.all('BloodType').value = arrSelected[0][23];
    
    fm.all('PolityVisage').value = arrSelected[0][24];    
    fm.all('Degree').value = arrSelected[0][25];
    
    fm.all('GraduateSchool').value = arrSelected[0][26];    
    fm.all('Speciality').value = arrSelected[0][27];
    
    fm.all('PostTitle').value = arrSelected[0][28];    
    fm.all('ForeignLevel').value = arrSelected[0][29];
    
    fm.all('WorkAge').value = arrSelected[0][30];    
    fm.all('OldCom').value = arrSelected[0][31];
    
    fm.all('OldOccupation').value = arrSelected[0][32]; 
      
    fm.all('HeadShip').value = arrSelected[0][33];
    
    fm.all('RecommendAgent').value = arrSelected[0][34]; 
      
    fm.all('Business').value = arrSelected[0][35];

    fm.all('SaleQuaf').value = arrSelected[0][36];    
    fm.all('QuafNo').value = arrSelected[0][37];
    
    fm.all('QuafStartDate').value = arrSelected[0][38];    
    fm.all('QuafEndDate').value = arrSelected[0][39];
    
    fm.all('DevNo1').value = arrSelected[0][40];    
    fm.all('DevNo2').value = arrSelected[0][41];
    
    fm.all('RetainContNo').value = arrSelected[0][42];    
    fm.all('AgentKind').value = arrSelected[0][43];
    
    fm.all('DevGrade').value = arrSelected[0][44];    
    fm.all('InsideFlag').value = arrSelected[0][45];
    
    fm.all('FullTimeFlag').value = arrSelected[0][46];    
    fm.all('NoWorkFlag').value = arrSelected[0][47];
    
    fm.all('TrainDate').value = arrSelected[0][48];    
    fm.all('EmployDate').value = arrSelected[0][49];
    
    fm.all('InDueFormDate').value = arrSelected[0][50];    
    fm.all('AgentState').value = arrSelected[0][61];
   
    fm.all('BranchCode').value = arrSelected[0][74];    
    fm.all('SmokeFlag').value = arrSelected[0][63];
    
    fm.all('RgtAddress').value = arrSelected[0][64];
   
    fm.all('BankCode').value = arrSelected[0][65];    
    fm.all('BankAccNo').value = arrSelected[0][66];
    
    fm.all('BranchType').value = arrSelected[0][72];    
    fm.all('TrainPeriods').value = arrSelected[0][73];  	         
  
  }