<%
//程序名称：LAAgentMessageQueryInit.js
//程序功能：
//创建日期：2003-01-20 14:14:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                                       

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('AgentCode').value = '<%=tG.Operator%>'; 
   
    fm.all('AgentGroup').value = '';
    
    fm.all('ManageCom').value = '';    
    fm.all('Name').value = '';
    
    fm.all('Sex').value = '';    
    fm.all('Birthday').value = '';
    
    fm.all('NativePlace').value = '';    
    fm.all('Nationality').value = '';
    
    fm.all('Marriage').value = '';    
    fm.all('CreditGrade').value = '';
    
    fm.all('HomeAddress').value = '';    
    fm.all('PostalAddress').value = '';
    
    fm.all('ZipCode').value = '';    
    fm.all('Phone').value = '';
    
    fm.all('BP').value = '';    
    fm.all('Mobile').value = '';
   
    fm.all('Email').value = '';    
    fm.all('IDNo').value = '';
    
    fm.all('Source').value = '';    
    fm.all('BloodType').value = '';
    
    fm.all('PolityVisage').value = '';    
    fm.all('Degree').value = '';
    
    fm.all('GraduateSchool').value = '';    
    fm.all('Speciality').value = '';
    
    fm.all('PostTitle').value = '';    
    fm.all('ForeignLevel').value = '';
    
    fm.all('WorkAge').value = '';    
    fm.all('OldCom').value = '';
    
    fm.all('OldOccupation').value = ''; 
      
    fm.all('HeadShip').value = '';
    
    fm.all('RecommendAgent').value = ''; 
      
    fm.all('Business').value = '';
    //alert("help7");
    fm.all('SaleQuaf').value = '';    
    fm.all('QuafNo').value = '';
    
    fm.all('QuafStartDate').value = '';    
    fm.all('QuafEndDate').value = '';
    
    fm.all('DevNo1').value = '';    
    fm.all('DevNo2').value = '';
    
    fm.all('RetainContNo').value = '';    
    fm.all('AgentKind').value = '';
    
    fm.all('DevGrade').value = '';    
    fm.all('InsideFlag').value = '';
    
    fm.all('FullTimeFlag').value = '';    
    fm.all('NoWorkFlag').value = '';
    
    fm.all('TrainDate').value = '';    
    fm.all('EmployDate').value = '';
    
    fm.all('InDueFormDate').value = '';    
    fm.all('AgentState').value = '';
   
    fm.all('BranchCode').value = '';    
    fm.all('SmokeFlag').value = '';
    
    fm.all('RgtAddress').value = '';
   
    fm.all('BankCode').value = '';    
    fm.all('BankAccNo').value = '';
    
    fm.all('BranchType').value = '';    
    fm.all('TrainPeriods').value = '';  
    
  }
  catch(ex)
  {
    alert("在LAAgentMessageQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAAgentMessageQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("LAAgentMessageQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


</script>