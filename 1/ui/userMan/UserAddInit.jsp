<%
//程序名称：OLDUserInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.menumang.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script src="./UserAdd.js"></script>
<%
     //添加页面控件的初始化。
    GlobalInput tG1 = new GlobalInput();
    tG1=(GlobalInput)session.getValue("GI");
%>                            

<Script>
  var mOperator = "<%=Operator%>";
</Script>    
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('UserCode').value = '';
    fm.all('UserName').value = '';
    fm.all('ComCode').value = '';
    fm.all('Password').value = '';
    fm.all('PasswordConfirm').value = '';
    fm.all('UserDescription').value = '';
    fm.all('UserState').value = '';
    fm.all('UWPopedom').value = '';
    fm.all('ClaimPopedom').value = '';
    fm.all('OtherPopedom').value = '';

    fm.all('PopUWFlag').value = '';
    fm.all('SuperPopedomFlag').value = '';
    fm.all('Operator').value = '<%=tG1.Operator%>';
    fm.all('ValidStartDate').value = '';
    fm.all('ValidEndDate').value = '';
  } catch(ex) {
    alert("在UserInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  } catch(ex) {
    alert("在OLDUserInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try {
    initInpBox();
    initSelBox();    
    initSelectMenuGrpGrid();
    initUnselectMenuGrpGrid();

    initHideMenuGrpGrid1(); //初始化hideMenuGrpGrid必须在unselectMenuGrpGrid后
    initUserGrid();
  } catch(re) {
    alert("OLDUserInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initUserGrid()
{
   var iArray = new Array();
      
   try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              	
      
      iArray[1]=new Array();
      iArray[1][0]="用户姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="100px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="用户编码";    	                //列名
      iArray[2][1]="100px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
          
      iArray[3]=new Array();
      iArray[3][0]="用户状态";    	                //列名
      iArray[3][1]="100px";            		        //列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[4]=new Array();
      iArray[4][0]="用户描述";    	                //列名
      iArray[4][1]="200px";            		        //列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[5]=new Array();
      iArray[5][0]="管理机构编码";    	                //列名
      iArray[5][1]="100px";            		        //列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

	  iArray[6]=new Array();
      iArray[6][0]="管理机构名称";    	                //列名
      iArray[6][1]="200px";            		        //列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0; 
      
      iArray[7]=new Array();
      iArray[7][0]="用户上级";    	                //列名
      iArray[7][1]="90px";            		        //列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 

      UserGrid = new MulLineEnter( "fm" , "UserGrid" ); 
      
      //这些属性必须在loadMulLine前
      UserGrid.mulLineCount = 0;     
      UserGrid.displayTitle = 1;
      UserGrid.canChk =0;
      UserGrid.canSel =1;
      UserGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
      UserGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      UserGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      UserGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      UserGrid.loadMulLine(iArray);  
         
	} catch(ex) { 
        alert(ex);
    }
}

function  initHideMenuGrpGrid1()
  {

     var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              	
      
      iArray[1]=new Array();
      iArray[1][0]="菜单组名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="140px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="菜单组描述";    	                //列名
      iArray[2][1]="100px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
          
      HideMenuGrpGrid1 = new MulLineEnter( "fm" , "HideMenuGrpGrid1" ); 
      
      //这些属性必须在loadMulLine前
      HideMenuGrpGrid1.mulLineCount = 8;     
      HideMenuGrpGrid1.displayTitle = 1;
      HideMenuGrpGrid1.canChk =1;
      HideMenuGrpGrid1.canSel =0;
      HideMenuGrpGrid1.locked =1;            //是否锁定：1为锁定 0为不锁定
      HideMenuGrpGrid1.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      HideMenuGrpGrid1.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      HideMenuGrpGrid1.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      HideMenuGrpGrid1.loadMulLine(iArray);  
          
  }  catch(ex)  { 
    alert(ex);
  }
}
 
function  initSelectMenuGrpGrid()
{

	var iArray = new Array();
	
	try {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              	
		
		iArray[1]=new Array();
		iArray[1][0]="菜单组编码";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1]="100px";         			//列宽
		iArray[1][2]=10;          			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="菜单组描述";    	                //列名
		iArray[2][1]="120px";            		        //列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
		
		iArray[3]=new Array();
		iArray[3][0]="具体菜单";    	                //列名
		iArray[3][1]="70px";            		        //列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
		iArray[3][7]="showSelectGridMenus";
		        
		SelectMenuGrpGrid = new MulLineEnter( "fm" , "SelectMenuGrpGrid" ); 
		
		//这些属性必须在loadMulLine前
		SelectMenuGrpGrid.mulLineCount = 8;     
		SelectMenuGrpGrid.displayTitle = 1;
		SelectMenuGrpGrid.canChk =1;
		SelectMenuGrpGrid.canSel =0;
		SelectMenuGrpGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
		SelectMenuGrpGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
		SelectMenuGrpGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
		SelectMenuGrpGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
		SelectMenuGrpGrid.mulLineNum = 2;
		SelectMenuGrpGrid.loadMulLine(iArray);           
	} catch(ex) { 
        alert(ex);
    }
} 
  

function  initUnselectMenuGrpGrid()
{
	var iArray = new Array();
      
	try {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              	
		
		iArray[1]=new Array();
		iArray[1][0]="菜单组编码";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1]="60px";         			//列宽
		iArray[1][2]=10;          			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="菜单组描述";    	                //列名
		iArray[2][1]="60px";            		        //列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
		
		
		iArray[3]=new Array();
		iArray[3][0]="具体菜单";    	                //列名
		iArray[3][1]="50px";            		        //列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
		iArray[3][7]="showUnselectGridMenus";
		
		        
		UnselectMenuGrpGrid = new MulLineEnter( "fm" , "UnselectMenuGrpGrid" ); 
		
		//这些属性必须在loadMulLine前
		UnselectMenuGrpGrid.mulLineCount = 8;     
		UnselectMenuGrpGrid.displayTitle = 1;
		UnselectMenuGrpGrid.canChk =1;
		UnselectMenuGrpGrid.canSel =0;
		UnselectMenuGrpGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
		UnselectMenuGrpGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
		UnselectMenuGrpGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
		UnselectMenuGrpGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
		UnselectMenuGrpGrid.mulLineNum = 2;
		UnselectMenuGrpGrid.loadMulLine(iArray);
	} catch(ex) { 
        alert(ex);
    }
}   

</script>

<%
	LDUserSortUI userSort = new LDUserSortUI();
	String underLing = "'"+Operator+"',"+userSort.getAllUnderLing(Operator);
	System.out.println("in sub 111...................................................................."+underLing);
	
%>
