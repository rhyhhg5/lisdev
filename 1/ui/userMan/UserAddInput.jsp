<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@include file="../common/jsp/AccessCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
       	GlobalInput tG11 = new GlobalInput();
	tG11=(GlobalInput)session.getValue("GI");
	String Operator = tG11.Operator;
	String operatorComCode = tG11.ComCode;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="UserAdd.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<script src="../menumang/treeMenu.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./UserAddInit.jsp"%>
<title>用户管理 </title>
</head>
<body  onload="initForm();" >

<form action="./userAddMan.jsp" method=post name=fm target="fraSubmit">

  <table class="common">
  <TR  class= common>
    <TD  class= title>
      用户编码
    </TD>
    <TD  class= input id="tdUserCode">
      <Input class= common name=UserCode  maxlength="6">
    </TD>
    <TD  class= input style= "display:none" id= "tdUserCodeReadOnly">
      <Input class= common name=UserCodeReadOnly  readonly maxlength="6" >
    </TD>

    <TD  class= title>
      用户姓名
    </TD>
    <TD  class= input id="tdUserName">
      <Input class= common name=UserName >
    </TD>
     <TD  class= input style= "display:none"  id= "tdUserNameReadOnly">
      <Input class= common name=UserNameReadOnly readonly maxlength="6">
    </TD>

  </TR>
  </table>

  <div id= "divHideInput", style= "display:none">
  <table class="common">

  <TR  class= common id= passwordTR style= "display:''">

    <TD  class= title>
      密码
    </TD>
    <td class= input>
      <input class= common type="Password" name=Password maxlength="8" >
    </TD>

    <TD  class= title>
      密码确认
    </TD>
    <TD  class= input>
      <Input class= common type="Password" name= PasswordConfirm maxlength="8">
    </TD>

  </TR> 
  <TR  class= common>
    <TD  class= title>
      机构编码
    </TD>
    <TD  class= input>
      <input class="code" name=ComCode
      ondblclick=" showCodeList('ComCode',[this]);"  onkeyup="return showCodeListKey('ComCode',[this]);">
    </TD>

    <TD  class= title>
      用户状态
    </TD>
    <TD  class= input>
      <Input class= "code" name=UserState
      ondblclick=" showCodeList('UserState',[this]);"  onkeyup="return showCodeListKey('UserState',[this]);">
    </TD>

  </TR>

  <tr>
      <td class= title>
      用户描述
      </td>
      <td class= input>
      <Input class= common name= UserDescription>
      </td>
			<td class= title>
      代理机构
      </td>
      <td class= input>
      <Input class= code name= AgentCom ondblclick="return showCodeList('AgentCom',[this],null,null,fm.ComCode.value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',null,null,null, fm.ComCode.value, 'ManageCom');">
      </td>
  </tr>

  <TR  class= common>
    <TD  class= title>
      核保权限
    </TD>
    <TD  class= input>
      <Input class= "code" name=UWPopedom
        ondblclick=" showCodeList('UWPopedom',[this]);"  onkeyup="return showCodeListKey('UWPopedom',[this]);">
    </TD>
    <TD  class= title>
      理赔权限
    </TD>
    <TD  class= input>
      <Input class= "code" name=ClaimPopedom
      ondblclick=" showCodeList('ClaimPopedom',[this]);"  onkeyup="return showCodeListKey('ClaimPopedom',[this]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      其它权限
    </TD>
    <TD  class= input>
      <Input class= "code" name=OtherPopedom
      ondblclick=" showCodeList('OtherPopedom',[this]);"  onkeyup="return showCodeListKey('OtherPopedom',[this]);">
    </TD>
    <TD  class= title>
      首席核保标志
    </TD>
    <TD  class= input>
      <Input class= "code" name=PopUWFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      超级权限标志
    </TD>
    <TD  class= input>
      <Input class= "code" name=SuperPopedomFlag
      ondblclick=" showCodeList('SuperPopedomFlag',[this]);"  onkeyup="return showCodeListKey('SuperPopedomFlag',[this]);">
    </TD>
    <TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= common name=Operator readonly>
    </TD>

    <TD class= input style="display:none">
     <Input class= common value=<%=operatorComCode%> name=OperatorComCode style="display:none">
    </TD>
    <TD class= input style="display:none">
     <Input class= common value=<%=Operator%> name=OperatorCode style="display:none">
    </TD>

  </TR>
  </table>
  </div>

  <table class="common">
  <TR  class= common style="display:none">
    <TD  class= title>
     入机日期
    </TD>
    <TD  class= input>
      <Input class="readonly"  name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class="readonly"  name=MakeTime >
    </TD>

  </TR>
  <TR  class= common id = "validTR">
    <TD  class= title>
      有效开始日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" verify="有效开始日期|date" dateFormat="short" name=ValidStartDate >
    </TD>
    <TD  class= title>
      有效结束日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" verify="有效结束日期|date" dateFormat="short" name=ValidEndDate >
    </TD>

    <TD  class= input style= "display: none">
      <Input class= common name=HideInitTag >
    </TD>


  </TR>
  <TR  class= common id="tdMngCom">
   <TD  class= title>  管理机构   </TD>
        <TD  class= input>
        <Input class="codeno" name=MngCom  verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1],null,null,null,1);"><input class=codename name =MngComName></TD> 
        </TD> 
    
   


  </TR>
</table>

<!-- 解锁重置密码用 Added By Qisl At 2009.06.24 Begin -->
<div id="unLockResPass" style="display:none">
<table class="common">

  <TR  class= common>
    <TD  class= title>
      密码
    </TD>
    <td class= input>
      <input class= common type="Password" name=resetPassword maxlength="8" onkeyup="upperCase()" >
    </TD>
    <TD  class= title>
      密码确认
    </TD>
    <TD  class= input>
      <Input class= common type="Password" name= resetPasswordConfirm maxlength="8">
    </TD>
  </TR>
</table>
</div>
<!-- 解锁重置密码用 Added By Qisl At 2009.06.24 End -->

<div id= "divCmdButton", style= "display:''">
<input value="用户查询" type=button onclick="queryClick()" class="cssButton">
<INPUT VALUE="用户增加" TYPE=button onclick= "insertClick()" class="cssButton">
<INPUT VALUE="用户更新" TYPE=button onclick="updateClick()" class="cssButton">
<!--<INPUT VALUE="用户删除" TYPE=button onclick= "deleteClick()" class="cssButton"> -->
<INPUT VALUE="用户解锁" TYPE=button onclick= "unLockClick()" class="cssButton">
<INPUT VALUE="用户信息下载" TYPE=button onclick= "downLoad()" class="cssButton">
</div>

<Div  id= "divUserGrid" style= "display: ''">

<table>
    	<tr>

    		<td class= titleImg>
    			 用户表结果
    		</td>
    	</tr>
</table>

<table  class= common>
       <tr class= common>
         <td text-align: left colSpan=1>
	     <span id="spanUserGrid" ></span>
	 </td>
       </tr>


</table>
	<INPUT VALUE="首  页" TYPE=button onclick="userFirstPage()" class="cssButton">
	<INPUT VALUE="上一页" TYPE=button onclick="userPageUp()" class="cssButton">
	<INPUT VALUE="下一页" TYPE=button onclick="userPageDown()" class="cssButton">
	<INPUT VALUE="尾  页" TYPE=button onclick="userLastPage()" class="cssButton">
</div>


<div id= "hide" style= "display: none">
    <table class= common>
        <tr>
          <TD  class= input>
         <Input class= common name=Action >
          </TD>
        </tr>
    </table>
</div>


<Div  id= "divMenuGrpGrid" style= "display: none">
<table class= common>
    	<tr>

    		<td class= titleImg>
    			 用户拥有的菜单组
    		</td>
                 <td class= titleImg>
    			 用户未拥有的菜单组
    		</td>

    	</tr>
</table>

<input value="用户菜单组浏览" type=button onclick="showMenuGrp()" style= "display:none" class="cssButton">

<table class= common>
	<tr  class= common style="display: ''">
  	<td text-align: left colSpan=1>
		</td>
  	<td text-align: left colSpan=1>
		    <table class= common>
        	<tr  class= common>
        	  <TD  class= title>菜单组编码</TD>
            <TD  class= input>
              <Input class= common name=MenuGrpCode style="width: 100">
            </TD>
        	  <TD  class= title>菜单组描述</TD>
            <TD  class= input>
              <Input class= common name=MenuGrpDescription style="width: 100">
            </TD>
            <TD  class= title>
              <INPUT VALUE="查  询" TYPE=button onclick="queryMenuGrpUnSelected();" class="cssButton">
            </TD>
        	</tr>
        </table>
		</td>
	</tr>
	<tr  class= common>
  	<td text-align: left colSpan=1>
		    <span id="spanSelectMenuGrpGrid" ></span>
		</td>
  	<td text-align: left colSpan=1>
		    <span id="spanUnselectMenuGrpGrid" ></span>
		</td>
	  <td text-align: left colSpan=1>
		    <span id="spanHideMenuGrpGrid1" style= "display: none"></span>
		</td>
	</tr>
</table>

<INPUT VALUE="首  页" TYPE=button onclick="selectFirstPage()" class="cssButton">
<INPUT VALUE="上一页" TYPE=button onclick="selectPageUp()" class="cssButton">
<INPUT VALUE="下一页" TYPE=button onclick="selectPageDown()" class="cssButton">
<INPUT VALUE="尾  页" TYPE=button onclick="selectLastPage()" class="cssButton">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<INPUT VALUE=">" TYPE=button  onclick="removeMenus()" class="cssButton">
<INPUT VALUE="<" TYPE=button  onclick="addMenus()" class="cssButton">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<INPUT VALUE="首  页" TYPE=button onclick="unselectFirstPage()" class="cssButton">
<INPUT VALUE="上一页" TYPE=button onclick="unselectPageUp()" class="cssButton">
<INPUT VALUE="下一页" TYPE=button  onclick="unselectPageDown()" class="cssButton">
<INPUT VALUE="尾  页" TYPE=button onclick="unselectLastPage()" class="cssButton">

<table class= common>
  <tr>
    <td class= titleImg>
		 菜单组节点明细表
	</td>
  </tr>
  <tr>
	<td text-align: left colSpan=1>
	    <span id="spanMenuTree" ></span>
	</td>
  </tr>

</table>

</div>

<div  id= "divSubCmdButton" style= "display: none">
    <INPUT VALUE="确  定" TYPE=button onclick= "okClick()" class="cssButton">
    <INPUT VALUE="退  出" TYPE=button onclick= "cancelClick()" class="cssButton">
    <input type="hidden" class= common name="oldPassword" maxlength="8" >
</div>
		<input type="hidden" name="UnderStr" value="<%=underLing%>">
		<input type="hidden" name="SelUserCode" value="">
		<input type="hidden" name="MenuGrp" value="">
		<input type="hidden" name="ManageCom" value="<%=operatorComCode%>">
		
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
