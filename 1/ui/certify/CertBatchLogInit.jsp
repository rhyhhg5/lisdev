<%
//程序名称：CertBatchLogInit.jsp
//程序功能：
//创建日期：2003-06-11
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	String strTakeBackNo = (String)session.getAttribute("TakeBackNo");
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try {
  	fm.hideTakeBackNo.value = '<%= strTakeBackNo %>';
  } catch(ex) {
    alert("在CertBatchLogInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initCertLog();
  }
  catch(re)
  {
    alert("CertBatchLogInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 起始单号、终止单号信息列表的初始化
function initCertLog()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单证名称";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="180";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="发放机构";    	    //列名
      iArray[3][1]="180";            		//列宽
      iArray[3][2]=180;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="接收机构";    	    //列名
      iArray[4][1]="180";            		//列宽
      iArray[4][2]=180;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="起始单号";    	    //列名
      iArray[5][1]="180";            		//列宽
      iArray[5][2]=180;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="终止单号";    	    //列名
      iArray[6][1]="180";            		//列宽
      iArray[6][2]=180;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="数量";    	        //列名
      iArray[7][1]="50";            		//列宽
      iArray[7][2]=50;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="错误信息"; 	        //列名
      iArray[8][1]="350";            		//列宽
      iArray[8][2]=350;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      CertLog = new MulLineEnter( "fmsave" , "CertLog" );
      //这些属性必须在loadMulLine前
      CertLog.hiddenPlus = 1;
      CertLog.hiddenSubtraction = 1;
      CertLog.displayTitle = 1;
      CertLog.loadMulLine(iArray);
      CertLog.delBlankLine("CertLog");

    } catch(ex) {
      alert(ex);
    }
}

</script>