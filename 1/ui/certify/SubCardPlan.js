
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;

//提交，保存按钮对应操作
function submitForm()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  { 
    //执行下一步操作
  }
}

function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CardPlan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//edit by guoxiang  for bug
function queryPlan(vFlag)
{
	
	// 初始化表格
	CardPlanInfo.clearData();

	// 书写SQL语句
	var strSQL = "";
        //公用的sql语句
	var sql_selectpart= "SELECT PlanID, CertifyCode,AppCount, RelaPlan, PlanState,AppCom FROM LZCardPlan";
	var sql_wherepart=" WHERE RetCom = '" + fm.cur_com.value + "'";
	//不同的查询条件
	if( vFlag == "0" ) {  // 查询所有计划
		sql_wherepart= sql_wherepart;
	} else if( vFlag == "1" ) {  // 查询所有待批复的计划
		sql_wherepart = sql_wherepart
		       + " and PlanState IN ('A', 'C')";
	} else if( vFlag == "2" ) {  // 查询所有待汇总的计划
		sql_wherepart = sql_wherepart
		       + " and PlanState IN ('A')";
	}
	var sql_orderby=" order by updatedate,updatetime";
	sql_wherepart=sql_wherepart+ getWherePart( 'PlanID' )
		     + getWherePart( 'CertifyCode' )
		     + getWherePart( 'PlanState' )
		     + getWherePart( 'MakeDate')
		     + getWherePart( 'RelaPlan' )
		     + getWherePart('AppCom','AppCom','like','0');			 
	//打印需要处理的sql语句
	var sql="select * FROM LZCardPlan"+sql_wherepart;
	sql=replaceStrSql(sql);
	sql=sql+sql_orderby;
	fm.Strsql.value=sql;
	//alert(sql);
	//easyQueryVer3查询
	//alert(sql_selectpart+sql_wherepart);
	turnPage.strQueryResult  = easyQueryVer3(sql_selectpart+sql_wherepart+sql_orderby, 1, 1, 1);  
 
  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	alert("没有要处理的下级计划！");
    return false;
	}
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = CardPlanInfo;    
          
  //保存SQL语句
  turnPage.strQuerySql     = sql_selectpart+sql_wherepart; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function retPlan() 
{
  tRow = CardPlanInfo.getSelNo();
  if (tRow==0 || tRow==null) {
  	alert("请选择要批复的计划！");
  	return null;
  }

	// 保留选中的单证计划
	var vPlanID = CardPlanInfo.getRowColData(tRow - 1, 4);
  fm.sql_where.value = " and planid = '" + vPlanID + "'";
  
  if( vPlanID == "00000000000000000000" ) {
  	vPlanID = CardPlanInfo.getRowColData(tRow - 1, 1);
  	fm.sql_where_ex.value = " and planid = '" + vPlanID + "'";
  } else {
  	fm.sql_where_ex.value = " and relaplan = '" + vPlanID + "'";
  }

  window.open("./RetCardPlan.html");
}

function sumPlan() 
{
  tRow = CardPlanInfo.getSelNo();
  if (tRow==0 || tRow==null) {
  	alert("请选择要汇总的计划！");
  	return null;
  }

	// 保留选中的单证计划
	fm.PlanID.value = CardPlanInfo.getRowColData(tRow - 1, 1);
	fm.CertifyCode.value = CardPlanInfo.getRowColData(tRow - 1, 2);
	fm.action = "./CardPlanInput.jsp";
	fm.target = "";
	fm.submit();
}


function queryBuget()
{
  tRow = CardPlanInfo.getSelNo();
  if (tRow==0 || tRow==null) {
  	alert("请选择要查看预算的计划！");
  	return null;
  }
  // 保留选中的单证计划
	fm.PlanID.value = CardPlanInfo.getRowColData(tRow - 1, 1);
	fm.CertifyCode.value = CardPlanInfo.getRowColData(tRow - 1, 2);
	fm.action = "./CardPlanBugetQuety.jsp";
	fm.target = "_blank";
	fm.submit();
	
}
function printPlan()
{
	fm.action = "./CertifyPlanPrint.jsp";
	fm.target = "_blank";
	fm.submit();
}