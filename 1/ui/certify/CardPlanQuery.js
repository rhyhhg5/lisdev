//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;
var mDebug="0";
var arrStrReturn = new Array();
var arrGrid;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	try {
		fm.sql_where.value = eval("top.opener.fm.sql_where.value");
	} catch (ex) {
		fm.sql_where.value = "";
	}
  fm.action="./CardPlanQuerySubmit.jsp";
  fm.target="fraSubmit"
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    parent.fraSubmit.getGridResult();
    
    arrGrid = null;
    if( arrStrReturn[0] == '0|0^' ) {
    	CardPlanGrid.clearData();
    	fm.page_count.value = 0;
    } else {
			arrGrid = decodeEasyQueryResult(arrStrReturn[0]);
    	useSimulationEasyQueryClick(arrStrReturn[0]);
    	fm.page_count.value = arrStrReturn[1];
    }
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function returnParent()
{
  try
  {
    top.opener.onShowResult(getQueryResult());
  }
  catch(ex)
  {
    alert("没有发现父窗口的onShowResult接口。"+ex);
  }
  top.close();
}


function getQueryResult()
{
  var arrResult=null;
  tRow = CardPlanGrid.getSelNo();
  if (tRow==0 || tRow==null || arrGrid==null)
    return null;
    
  arrResult=new Array();
  //设置需要返回的数组
  //arrResult[0]=arrGrid[tRow + CardPlanGrid.recordNo - 1];
  arrResult = CardPlanGrid.getRowData(tRow - 1);
  return arrResult;
}

function useSimulationEasyQueryClick(strData) {
  //保存查询结果字符串
  turnPage.strQueryResult  = strData;
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //拆分字符串，返回二维数组
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray          = new Array(0, 1, 4, 2, 9, 8, 5, 15);

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = CardPlanGrid;
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
}

// 翻页函数
function firstPage() {
	if( fm.page_count.value == -1 ) {
		alert("请先查询");
	} else if( fm.cur_page.value == 0 ) {
		alert("已经到了首页");
	} else {
		fm.cur_page.value = 0;
		submitForm();
	}
}

function prevPage() {
	if( fm.page_count.value == -1 ) {
		alert("请先查询");
	} else if( fm.cur_page.value == 0 ) {
		alert("已经到了首页");
	} else {
		fm.cur_page.value = parseInt(fm.cur_page.value) - 1;
		submitForm();
	}
}

function nextPage() {
	if( fm.page_count.value == -1 ) {
		alert("请先查询");
	} else if( fm.cur_page.value == parseInt(fm.page_count.value) - 1 ) {
		alert("已经到了尾页");
	} else {
		fm.cur_page.value = parseInt(fm.cur_page.value) + 1;
		submitForm();
	}
}

function lastPage() {
	if( fm.page_count.value == -1 ) {
		alert("请先查询");
	} else if( fm.cur_page.value == parseInt(fm.page_count.value) - 1 ) {
		alert("已经到了尾页");
	} else {
		fm.cur_page.value = parseInt(fm.page_count.value) - 1;
		submitForm();
	}
}
//add by guoxiang at 2004-05-13
function queryBuget()
{
  tRow = CardPlanGrid.getSelNo();
  if (tRow==0 || tRow==null) {
  	alert("请选择要查看预算的计划！");
  	return null;
  }
  // 保留选中的单证计划
	fm.PlanID.value = CardPlanGrid.getRowColData(tRow - 1, 1);
	fm.CertifyCode.value = CardPlanGrid.getRowColData(tRow - 1, 2);
	fm.action = "./CardPlanBugetQuety.jsp";
	fm.target = "_blank";
	fm.submit();
	
}

//add by guoxiang at 2004-04-11
function printPlan()
{
       var sql_wherepart=getWherePart( 'PlanID' )
		     + getWherePart( 'CertifyCode' )
		     + getWherePart( 'AppCount' )
		     + getWherePart( 'RetCom' )
		     + getWherePart( 'RelaPlan' )
		     + getWherePart( 'makeDate','MakeDateB','>=','0' )
		     + getWherePart( 'makeDate','MakeDateE','<=','0')
		     + getWherePart( 'RetState' )
		     + getWherePart( 'PlanState' );	
        var sql="select * FROM LZCardPlan where 1=1 "+sql_wherepart;
	sql=sql+" and "+eval("top.opener.fm.sql_where.value");
	fm.Strsql.value=sql;
	fm.action = "./CertifyPlanPrint.jsp";
	fm.target = "_blank";
	fm.submit();
}