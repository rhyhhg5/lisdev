<html>
	<%
		//程序名称：银行代收对帐清单
		//程序功能：
		//创建日期：2003-3-25
		//创建人  ：刘岩松程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<!--用户校验类-->
	<%@page import="com.sinosoft.utility.*"%>
	<%@page import="com.sinosoft.lis.schema.*"%>
	<%@page import="com.sinosoft.lis.vschema.*"%>
	<%@page import="com.sinosoft.lis.bank.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="CertifyMaxNoInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="CertifyMaxNoInputInit.jsp"%>
	</head>
	<script>
		var ManageCom = "<%=tGI.ManageCom%>";
		var tOperator = "<%=tGI.Operator%>"; 
	</script>
	<body onload="initForm();initElementtype();">
		<form action="./CertifyMaxNoSave.jsp" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/InputButton.jsp"%>
			<!-- 显示或隐藏LLReport1的信息 -->
				<table>
					<tr>
						<td class=titleImg>
							查询条件
						</td>
					</tr>
				</table>
				<Div id="divMaxNo" style="display: ''">
				<Table class=common>
					<tr class=common>
						<td class=title>
							管理机构
						</td>
						<td class=input>
							<input class=codeno name=ManageCom readonly style="display:''" ondblclick="return showCodeList('Station', [this,ManageComName],[0,1],null,null,null,1);"><input name=ManageComName class=codename verify="管理机构|NOTNULL"	elementtype=nacessary>
						</td>
						<td class="title">
							单证编码：
						</td>
						<td class="input">
							<input class="codeno" name="CertifyCode" ondblclick="return showCodeList('certifycodejhk', [this,CertifyCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('certifycodejhk', [this,CertifyCodeName],[0,1],null,null,null,1);"><Input class=codename name="CertifyCodeName" readonly>
						</td>
						<td class=title>
							单证类型码：
						</td>
						<td class=input>
							<Input class="common" dateFormat="short" name=SubCode>
						</td>
					</tr>
				</Table>
				<input type="hidden" class="common" name="querySql" >
				<input class="cssButton" type=button value=" 查 询 " onclick="queryData()">
				<input class="cssButton" type=button value=" 下 载 " onclick="downLoad()">
				<input class="cssButton" type=button value=" 保 存 " onclick="submitForm()">
				</Div>
				<hr>
				<table>
					<tr>
						<td class=titleImg>
							查询结果
						</td>
					</tr>
				</table>
				<!-- 信息（列表） -->
				<Div id="" style="display:''">
					<table class=common>
						<tr class=common>
							<td text-align:left colSpan=1>
								<span id="spanCertifyMaxNoGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
				<Div id="divPage" align=center style="display: 'none' ">
					<table>
						<tr>
							<td class=button>
								<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
								<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
								<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
								<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
							</td>
						</tr>
					</table>
				</Div>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
