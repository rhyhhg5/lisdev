<html>
<%
//name :CertifyDescInput.jsp
//function :Manage LMCertifyDes
//Creator :刘岩松
//date :2003-05-16
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src = "CertifyDescInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CertifyDescInit.jsp"%>
	</head>

<body  onload="initForm();" >
  <form action="./CertifyDescSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  <Div id= "divLLReport1" style= "display: ''">

   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		单证编码
   			</TD>
   			<TD class= input>
   				<Input class= common name= CertifyCode >
   			</TD>

        <TD class= title>
          附标号
        </TD>
        <TD class= input>
          <Input class= common name= SubCode >
   			</TD>
   		</TR>

      <TR class= common>
         <TD class= title>
           单证类型
         </TD>
         <TD class= input>
           <Input class= code name= CertifyClass verify="单证类型|NOTNULL"
             CodeData="0|^P|普通单证^D|定额单证"
             ondblClick="showCodeListEx('CertifyClassListNew',[this],[0,1]);"
             onkeyup="showCodeListKeyEx('CertifyClassListNew',[this],[0,1]);">
         </TD>

        <TD class= title>
          单证名称
        </TD>
        <TD class= input>
          <Input class= common name= CertifyName >
         </TD>
       </TR>

       <TR class= common>
         <TD class= title>
           单证内部编码
         </TD>
         <TD class= input>
           <Input class= common name= InnerCertifyCode >
         </TD>

        <TD class= title>
          单证号码长度
        </TD>
        <TD class= input>
          <Input class= common name= CertifyLength >
         </TD>
       </TR>

       <TR class= common>
         <TD class= title>
           险种编码
         </TD>
         <TD class= input>
           <Input class= common name= RiskCode >
         </TD>

        <TD class= title>
          状态
        </TD>
        <TD class= input>
          <Input class= code name= State verify="状态|NOTNULL"
             CodeData="0|^0|有效单证^1|无效单证"
             ondblClick="showCodeListEx('CertifyStateList',[this],[0,1]);"
             onkeyup="showCodeListKeyEx('CertifyStateList',[this],[0,1]);">
         </TD>
       </TR>

       <TR class= common>
         <TD class= title>
           重要级别
         </TD>
         <TD class= input>
           <Input class= common name= ImportantLevel >
         </TD>

        <TD class= title>
          是否是有号单证
        </TD>
        <TD class= input>
          <Input class= code name= HaveNumber verify="单证类型|NOTNULL"
             CodeData="0|^Y|有号单证^N|无号单证"
             ondblClick="showCodeListEx('CertifyHaveNumber',[this],[0,1]);"
             onkeyup="showCodeListKeyEx('CertifyHaveNumber',[this],[0,1]);">

         </TD>
       </TR>

       <TR class= common>
         <TD class= title>
           总保费
         </TD>
         <TD class= input>
           <Input class= common name= Prem >
         </TD>

        <TD class= title>
          总保额
        </TD>
        <TD class= input>
          <Input class= common name= Amnt >
         </TD>
       </TR>

       <TR class= common>
         <TD class= title>
           管理机构
         </TD>
         <TD class= input>

           <Input class="code" name= ManageCom
             ondblclick="return showCodeList('Station', [this]);"
             onkeyup="return showCodeListKey('Station', [this]);"
             verify="管理机构|NOTNULL">
         </TD>
         <Input class=common type = hidden name= OperateType >
      </TR>
    </Table>

    <Table class= common>
      <TR>
        <TD class= title>
          注释
        </TD>
        <TD class= input>
    <textarea name="Note" cols="75%" rows="3"
      witdh=25% class="common"></textarea>
	</TD>
         </TD>
       </TR>

 		</table>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>