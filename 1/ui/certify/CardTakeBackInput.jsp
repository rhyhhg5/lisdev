<% 
//清空缓存
//response.setHeader("Pragma","No-cache"); 
//response.setHeader("Cache-Control","no-cache"); 
//response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-07
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="CardTakeBackInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CardTakeBackInit.jsp"%>
</head>
<body  onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CardTakeBackSave.jsp" method=post name=fm target="fraSubmit">

    <table class="common">
    	<tr class="common">
    		<td class="input">
    		  <input class="common" type="button" value="回收单证" onclick="submitForm()" >
    		  <div id='printtitle'style="display:none">
    		  	<input name="chkPrt" type="checkbox" >打印清单</td>
    		  </div>
    	
    		<td class="input"></td>
    	</tr>
    
    </table>

    <!-- 单证信息 -->
    <div style="width:120">
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this, divCertifyInfo);"></td>
          <td class="titleImg">单证信息</td></tr></table></div>
          
    <div id="divCertifyInfo">
      <table class="common">
        <tr class="common">
          <td class="title">单证编码</td>
          <td class="input"><input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('CardCode', [this,CertifyCodeName],[0,1],null,null,null,1,350);" onkeyup="return showCodeListKey('CardCode', [this,CertifyCodeName],[0,1],null,null,null,1,350);" verify="单证编码|NOTNULL"><Input class= codename name="CertifyCodeName" ></td>
          <td class="title">附标号</td>
          <td class="input"><input class="readonly" readonly name="SubCode"></td></tr>
          
        <tr class="common">
          <td class="title">险种</td>
          <td class="input"><input class="readonly" readonly name="RiskCode"></td>
          
          <td class="title">险种版本</td>
          <td class="input"><input class="readonly" readonly name="RiskVersion"></td></tr>
          
        <tr class="common">
          <td class="title">单张保费</td>
          <td class="input"><input class="readonly" readonly name="Prem"></td>
          
          <td class="title">单张保额</td>
          <td class="input"><input class="readonly" readonly name="Amnt"></td></tr>
      </table>
    </div>
    
    <!-- 回收的信息 -->    
    <div style="width:120"><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divOtherInfo);"></td>
          <td class="titleImg">回收信息</td></tr></table></div>

    <div id="divOtherInfo">
      <table class="common">
        <tr class="common">
          <td class="title">发放者</td>
          <td class="input"><input class="common" name="SendOutCom" verify="发放者|NOTNULL"></td>
          
          <td class="title">接收者</td>
          <td class="input"><input class="common" name="ReceiveCom" verify="接收者|NOTNULL"></td></tr>
          
        <tr class="common">
          <td class="title">提货人</td>
          <td class="input"><input class="common" name="Handler"></td>
          
          <td class="title">提货日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDate"></td></tr>

        <tr class="common">
          <td class="title">操作员</td>
          <td class="input"><input class="readonly" readonly name="Operator"></td>
          
          <td class="title">当前时间</td>
          <td class="input"><input class="readonly" readonly name="curTime"></td></tr>

        <tr class="common">
          <td class="title">结算单号</td>
          <td class="input" cols=3><input class="readonly" readonly name="TakeBackNo"></td></tr>
          
      </table>
    </div>
    
    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>单证列表</td></tr>
    </table>
    
		<div id= "divCertifyList" style= "display: ''">
	      <table class="common">
	        <tr class="common">
	          <td text-align: left colSpan=1><span id="spanCertifyList" ></span></td></tr>
		  </table>
		</div>
		
		<input type=hidden name="hideOperation">

	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   
  </form>
</body>
</html>
