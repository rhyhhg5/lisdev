<%
//程序名称：CertifyTempInit.jsp
//程序功能：
//创建日期：2006-06-26
//创建人  ：Zhangbin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	String strOperator = globalInput.Operator;
	String strComCode = globalInput.ComCode;
	String strCurDate = PubFun.getCurrentDate();
%>                            

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {         
  	fm.reset();
  	fm.all('AgentCode').value == "";
    fm.Operator.value = '<%= strOperator %>';
    fm.ManageCom.value="<%=strComCode%>";
    
    fm.OpenDate.value="<%=strCurDate%>";
    
    var dt = new Date( );
  }
  catch(ex)
  {
    alert("在CertifyTempInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	initTempFeeGrid();
    initInpBox();
  }
  catch(re)
  {
    alert("TempFeeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 单证列表的初始化
function initTempFeeGrid()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		  //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30";        				//列宽
      iArray[0][2]=50;          				//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="收据方名称";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="220";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      

      iArray[2]=new Array();
      iArray[2][0]="投保单号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="金额";    	    //列名
      iArray[3][1]="50";            		//列宽
      iArray[3][2]=180;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="暂收据号";    	    //列名
      iArray[4][1]="80";            		//列宽
      iArray[4][2]=180;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="单证领用人";    	        //列名
      iArray[5][1]="80";            		//列宽
      iArray[5][2]=50;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="开票时间";    	    //列名
      iArray[6][1]="70";            		//列宽
      iArray[6][2]=80;            			//列最大值
      iArray[6][3]=0;              			//表示是代码选择
      
      
      TempFeeGrid = new MulLineEnter( "fm" , "TempFeeGrid" ); 
      //这些属性必须在loadMulLine前
      TempFeeGrid.mulLineCount = 1;     
      TempFeeGrid.displayTitle = 1;
      TempFeeGrid.hiddenPlus = 1;     
      TempFeeGrid.hiddenSubtraction = 1;
      TempFeeGrid.loadMulLine(iArray);  
      
    } catch(ex) {
      alert(ex);
    }
}

function getManageName(cCode)
{
	var strSQL = "select Name from LDCom where ComCode = '"+cCode+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult!=null)
	{
		var cName = arrResult[0];
		return cName;
	}
	else
	{
		return '';
	}
}

</script>