<%
//Name    ：LZCertifyUserSave.jsp
//Function：对单证管理岗位人员管理的save程序
//Author   :zhangbin
//Date：2006-04-17 @ PICCH
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  LZCertifyUserSchema mLZCertifyUserSchema = new LZCertifyUserSchema();

  CErrors tError = null;
  String transact = request.getParameter("fmtransact");
  System.out.println("LZCertifyUserSave.jsp本次执行的操作是"+transact);

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  
	mLZCertifyUserSchema.setUserCode(request.getParameter("UserCode"));
	String mUserName = StrTool.unicodeToGBK(request.getParameter("UserName")); //名称
	mLZCertifyUserSchema.setUserName(mUserName);
	mLZCertifyUserSchema.setComCode(request.getParameter("ComCode"));
	mLZCertifyUserSchema.setStateFlag(request.getParameter("StateFlag"));
	mLZCertifyUserSchema.setDescription(request.getParameter("UserDescription"));
	mLZCertifyUserSchema.setCertifyDeal("");
	mLZCertifyUserSchema.setUpUserCode("");
	
	System.out.println("UserCode: "+request.getParameter("UserCode"));
	System.out.println("UserName: "+request.getParameter("UserName"));
	System.out.println("ComCode: "+request.getParameter("ComCode"));
	System.out.println("StateFlag: "+request.getParameter("StateFlag"));
	System.out.println("Description: "+request.getParameter("Description"));
	 
	
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	VData tVData = new VData();
	LZCertifyUserOperateUI mLZCertifyUserOperateUI = new LZCertifyUserOperateUI();
	try
	{
	  tVData.addElement(mLZCertifyUserSchema);
	  tVData.addElement(tG);
	  mLZCertifyUserOperateUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
	  Content = transact+"失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
	
	if (FlagStr=="")
	{
	  tError = mLZCertifyUserOperateUI.mErrors;
	  if (!tError.needDealError())
	  {
	    Content = " 操作成功！";
	    FlagStr = "Succ";
	  }
	  else
	  {
	    Content = " 操作失败，原因是:" + tError.getFirstError();
	    FlagStr = "Fail";
	  }
	} 
  //处理转意字符
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>