<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        fm.ManageCom.value = "<%=globalInput.ComCode%>";
        fm.Operator.value = "<%=globalInput.Operator%>";
        //查询时间段默认为三个月
        var arrResult = easyExecSql("select current date - 3 month, current date from dual");
        if (arrResult != null) {
            fm.all('startDate').value=arrResult[0][0];
            fm.all('endDate').value=arrResult[0][1];
        }
    }
    catch(ex)
    {
        alert("在CertifyReTakeBackInit.jsp-->initInputBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initCertifyListGrid();
    }
    catch(re)
    {
        alert("CertifyReTakeBackInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

// 单证列表的初始化
function initCertifyListGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         		  //列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="50";        				//列宽
        iArray[0][2]=50;          				//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="单证类型";     		//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[1][1]="80";        			//列宽
        iArray[1][2]=80;          			//列最大值
        iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许
        iArray[1][21]="CertifyCode";

        iArray[2]=new Array();
        iArray[2][0]="单证起始号";     		//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[2][1]="80";        		  //列宽
        iArray[2][2]=80;          			//列最大值
        iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
        iArray[2][21]="StartNo";

        iArray[3]=new Array();
        iArray[3][0]="单证终止号";    	    //列名
        iArray[3][1]="80";            		//列宽
        iArray[3][2]=80;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[3][21]="EndNo";

        iArray[4]=new Array();
        iArray[4][0]="数量";    	    //列名
        iArray[4][1]="40";            		//列宽
        iArray[4][2]=40;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[4][21]="SumCount";

        iArray[5]=new Array();
        iArray[5][0]="回销人";    	        //列名
        iArray[5][1]="50";            		//列宽
        iArray[5][2]=50;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[5][21]="SendOutCom";

        iArray[6]=new Array();
        iArray[6][0]="状态";    	        //列名
        iArray[6][1]="50";            		//列宽
        iArray[6][2]=50;            			//列最大值
        iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        iArray[6][21]="StateFlag";

        CertifyListGrid = new MulLineEnter("fm", "CertifyListGrid");
        //这些属性必须在loadMulLine前
        CertifyListGrid.displayTitle = 1;
        CertifyListGrid.mulLineCount = 1;
        CertifyListGrid.hiddenPlus = 1;
        CertifyListGrid.hiddenSubtraction = 1;
        CertifyListGrid.canSel = 1;
        CertifyListGrid.canChk = 0;
        CertifyListGrid.loadMulLine(iArray);
        CertifyListGrid.selBoxEventFuncName = "certifyDetail";

    } catch(ex) {
        alert(ex);
    }
}
</script>