<html>
<%
//name :CertifyMaxInput.jsp
//function :Query Certify Info and Show Info
//Creator :刘岩松
//date :2003-04-18
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="CertifyMaxInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CertifyMaxInit.jsp"%>
	</head>

<body  onload="initForm();" >
  <form action="./CertifyMaxSave.jsp" method=post name=fm target="fraSubmit">
	<%@include file="../common/jsp/InputButton.jsp"%>
  <Div id= "divLLReport1" style= "display: ''">

   	<table class= common>
   		<tr class= common>
   			<TD class= title>
   		 		代理人编码
   			</TD>
   			<TD class= input>
   				<Input class= common name=AgentCode>
   			</TD>
   			<Input type= "hidden" name= OperateType>
   			<Input type= "hidden" name= QueryType>
        <Input type= "hidden" name= UpdateType>
   		</TR>

 		</table>
       	<input class="cssButton" type= button value="查  询" onclick="queryInfo_AgentCode()">
       	<input class="cssButton" type= button value="保  存" onclick="updateInfo_AgentCode()">
    <table>
    	<tr>
        <td class= titleImg>
    			 信息列表
    		</td>
    	</tr>
    </table>

	<Div  id= "divLLReport2" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1>
  				<span id="spanCertifyMaxGrid" >
  				</span>
  			</td>
  		</tr>
    </table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="showInfo1()">
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="showInfo2()">
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="showInfo3();">
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="showInfo4();">
  	</Div>
 	</div>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>