var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
turnPage1.pageDivName = "divPage1";

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
 }
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{
    if(fm.ReceiveCom.value=="" && fm.CardType.value=="" && fm.StateType.value==""){
        alert("请输入领用人、单证类别、状态类型中的任一值");
        return;
    }
    if (fm.StartDate.value == "" || fm.EndDate.value == "" ){
    	 alert("请录入发放日期(开始)和(结束日期)");
    	 return false;
    }
    if (fm.StartDate.value >fm.EndDate.value ){
   	 alert("开始日期不能大于结束日期");
   	 return false;
   }
    if(dateDiff(fm.StartDate.value, fm.EndDate.value, "M") > 1)
	{
		alert("统计期最多为一个月！");
		return false;
	}
    
    initCertifyMaxGrid();
    
//    if(fm.StartNo.value == "" && fm.EndNo.value != ""){
//        alert("请输入起始单号");
//        return;
//    }
    
//    if(fm.StartNo.value != "" && fm.EndNo.value == ""){
//        fm.EndNo.value = fm.StartNo.value;
//    }

    var sendDate = "";
    if (fm.StartDate.value != "" || fm.EndDate.value != "" ){
        sendDate = "and exists (select 1 from LZCardTrack d where 1 = 1"
            + getWherePart('d.HandleDate', 'EndDate', '<=')
            + getWherePart('d.HandleDate', 'StartDate', '>=')
            +" and d.CertifyCode = a.CertifyCode and d.StartNo = a.StartNo and d.EndNo = a.EndNo "
            +" and d.State in ('10','11')) ";
    }

    var state = "";
    if(fm.StateType.value == ""){
        state = " and a.State in ('0','2','3','4','5','6','8','9','10','11','12','13','14') ";
    }
    else if(fm.StateType.value == "0"){
        state = " and a.State in ('0','8') ";
    }
    else if(fm.StateType.value == "1"){
        state = " and a.State in ('10','11') ";
    }
    else if(fm.StateType.value == "7") {
    	  state = " and a.State = '9' ";
    }
    else{
        state = " and a.State = '" + fm.StateType.value + "' ";
    }
    
    var tCom = "";
//    if (fm.TempComCode.value.length<=4) {
//    	tCom = " and 1=DB2INST1.CunSendOutCom(a.CertifyCode,b.CardSerNo,'A"+managecom+"') ";
//    } else {
//    	tCom = " and 1=DB2INST1.CunSendOutCom(a.CertifyCode,b.CardSerNo,'B"+fm.TempOpe.value+"')";
//    	fm.ReceiveCom.value=fm.AgentCode.value;
//    }
    var mtsql="";
    if(fm.ManageComC.value!=""){
		
		mtsql="and ((ReceiveCom like 'A%' and ReceiveCom like '"+"A"+fm.ManageComC.value+"%' ) " +
				" or  (ReceiveCom like 'B%' and exists (select 1 from lduser where usercode=substr(char(ReceiveCom),2) " +
				" and comcode like  '"+fm.ManageComC.value+"%')) or (ReceiveCom not like 'A%' and ReceiveCom not like 'B%'" +
				" and exists (select 1 from lduser where usercode=substr(char(sendoutcom),2) " +
				" and comcode like  '"+fm.ManageComC.value+"%') )) ";
	}
    if (fm.TempComCode.value.length<=4) {
    	strt = " and exists (select 1 from LZCardTrack c where a.CertifyCode = c.CertifyCode and c.subcode=a.subcode " +
   				"and a.StartNo >= c.StartNo " +
   				"and a.EndNo <= c.EndNo  " +
   				mtsql
   				+"and (c.SendOutCom LIKE 'A"+managecom+"%' or c.ReceiveCom LIKE 'A"+managecom+"%'))";
   	
	} else {
		strt = " and exists (select 1 from LZCardTrack c where a.CertifyCode = c.CertifyCode and c.subcode=a.subcode " +
  				"and a.StartNo >= c.StartNo and a.EndNo <= c.EndNo  " +
  				mtsql+
  				"and (c.SendOutCom = 'B"+fm.TempOpe.value+"' or c.ReceiveCom = 'B"+fm.TempOpe.value+"'))";
	}
   
  
    
 	strSQL=detailSQL(tCom,state,sendDate,mtsql,strt)+" with ur "
    fm.PubSQL.value = strSQL;
 	turnPage2.queryModal(strSQL, CertifyMaxGrid);

/**********************************************************************************************************/    
    
	var tSCom = "";
	var startNo = fm.StartNo.value;
	var endNo = fm.EndNo.value;
	var tendno="";
	var noSQL="";
	var snoSQL="";
	var enoSQL="";
	var nocount=0;
	var arrResult=null;
	var arreResult=null;
	var nums=0;
	var nume=0;
	
	

	var strStateSQL="";
  //起始終止號都錄入的情況
	if(startNo!=""&&endNo!=""){
		if(parseInt(endNo,10)<parseInt(startNo,10)){
			alert("起始号不能大于终止号");
			return false ;
		}
		   var strsSQL = "SELECT endno,state,startno FROM LZCard WHERE 1=1 "
			   			+ getWherePart('CertifyCode','CertifyCode')
			   			+ getWherePart('StartNo','StartNo','<=')
			   			+ getWherePart('EndNo','StartNo','>=')
			   			+ getWherePart('SubCode', 'CardType')
			   			+" with ur"
				;
		
		arrResult = easyExecSql(strsSQL);
		if(arrResult != null)
		{
			tendno = arrResult[0][0];
			
			
			//起始終止號在同一區間的時候
			if(tendno>=endNo){
				noSQL=" and Startno<='"+startNo+"' and Endno>='"+startNo+"' ";
				nocount=parseInt(endNo,10)-parseInt(startNo,10)+1;
				nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10)+parseInt(tendno,10)-parseInt(endNo,10);
				if(nums<0){
					alert("起始或终止号录入错误，可能超出最大单证号，请检验");
					return false ;
				}
				strStateSQL="select t.certifycode,t.state,sum(t.sum) from " 
					+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",noSQL,state,sendDate,mtsql,strt)
					+"  ) " +
					"as t group by t.certifycode,t.state order by t.certifycode,t.state with ur ";
			}
			
			
			//起始終止號不在同一區間的時候
			else if(tendno<endNo){
				var startSQL="";
				var endSQL="";
				var efsql="";
				
				var streSQL = "SELECT startno,state,endno FROM LZCard WHERE 1=1 "
					+ getWherePart('CertifyCode','CertifyCode')
						+ getWherePart('StartNo','EndNo','<=')
						+ getWherePart('EndNo','EndNo','>=')
						+ getWherePart('SubCode', 'CardType')
						+" with ur"
						;
				arreResult = easyExecSql(streSQL);
				//起始号段所在区间
				snoSQL=" and Startno<='"+startNo+"' and Endno='"+tendno+"' ";
				nocount=parseInt(tendno,10)-parseInt(startNo,10)+1;
				nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10);
				if(nums<0){
					alert("起始或终止号录入错误，可能超出最大单证号，请检验");
					return false ;
				}
				if(arreResult!=null){
					var estartno=arreResult[0][0];
					//终止号段所在区间
					enoSQL=" and '"+endNo+"' between Startno and Endno ";
					nocount=parseInt(endNo,10)-parseInt(estartno,10)+1;
					nume=parseInt(arreResult[0][2],10)-parseInt(endNo,10);
					if(nume<0){
						alert("起始或终止号录入错误，可能超出最大单证号，请检验");
						return false ;
					}
					
					
				}
				else{
					alert("未查询到该终止号的单证信息");
					return false;
				}
				noSQL=" and  Startno>'"+startNo+"' and Endno<'"+endNo+"' ";
				strStateSQL="select t.certifycode,t.state,sum(t.sum) from " 
					+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",snoSQL,state,sendDate,mtsql,strt)
					+" union all "+getSumSQL(" sum(sumcount) as sum ",noSQL,tCom,state,sendDate,mtsql,strt)
					+" union all "
					+getSumSQL(" sum(sumcount)-"+nume+" as sum ",enoSQL,state,sendDate,mtsql,strt)+" ) " +
					"as t group by t.certifycode,t.state order by t.certifycode,t.state with ur ";
				
			}
		}
		else{
			alert("未查询到该起始号的单证信息");
			return false;
		}
	   }
	
	
	//只錄了起始號，沒有錄入終止號的情況
	else if(startNo!=""&&endNo==""){

		 var strsSQL = "SELECT endno,stateflag,startno FROM LZCard WHERE 1=1 "
			   + getWherePart('CertifyCode','CertifyCode')
				+ getWherePart('StartNo','StartNo','<=')
				+ getWherePart('EndNo','StartNo','>=')
				+ getWherePart('SubCode', 'CardType')
				+" with ur"
				;
			
			   arrResult = easyExecSql(strsSQL);
			   if(arrResult != null)
			   {
				   tendno = arrResult[0][0];
				   var startSQL="";
				   var endSQL="";
				   var efsql="";
				   //起始号段所在区间
				   snoSQL=" and Startno<='"+startNo+"' and Endno='"+tendno+"' ";
				   nocount=parseInt(tendno,10)-parseInt(startNo,10)+1;
				   nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10);
				   if(nums<0){
						alert("起始或终止号录入错误，可能超出最大单证号，请检验");
						return false ;
					}
				   
				   
				   noSQL=" and  Startno>'"+startNo+"' ";
				   strStateSQL="select t.certifycode,t.state,sum(t.sum) from " 
						+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",snoSQL,state,sendDate,mtsql,strt)
						+" union all "+getSumSQL(" sum(sumcount) as sum ",noSQL,state,sendDate,mtsql,strt)
						+"  ) "
						+"as t group by t.certifycode,t.state order by t.certifycode,t.state with ur ";
					
				
			   }
			   else{
					alert("未查询到该起始号的单证信息");
					return false;
				}
		   
		}
	
	
	//只錄入了終止號沒有錄入起始號的情況
	else if (endNo!=""&&startNo==""){

			var streSQL = "SELECT startno,state,endno FROM LZCard WHERE 1=1 "
							+ getWherePart('CertifyCode','CertifyCode')
							+ getWherePart('StartNo','EndNo','<=')
							+ getWherePart('EndNo','EndNo','>=')
							+ getWherePart('SubCode', 'CardType')
							+" with ur"
							;
			arreResult = easyExecSql(streSQL);
			
			if(arreResult!=null){
				var estartno=arreResult[0][0];
				//终止号段所在区间
				enoSQL=" and '"+endNo+"' between Startno and Endno ";
				nocount=parseInt(endNo,10)-parseInt(estartno,10)+1;
				nume=parseInt(arreResult[0][2],10)-parseInt(endNo,10);
				if(nume<0){
					alert("起始或终止号录入错误，可能超出最大单证号，请检验");
					return false ;
				}
				
				
			}
			else{
				alert("未查询到该终止号的单证信息");
				return false;
			}
			noSQL=" and   Endno<'"+endNo+"' ";
			strStateSQL="select t.certifycode,t.state,sum(t.sum) from " 
				+"("+getSumSQL(" sum(sumcount) as sum ",noSQL,state,sendDate,mtsql,strt)
				+" union all "
				+getSumSQL(" sum(sumcount)-"+nume+" as sum ",enoSQL,state,sendDate,mtsql,strt)+" ) " +
				"as t group by t.certifycode,t.state order by t.certifycode,t.state with ur ";
					
				
			  }
		
	
	//沒有錄入起始終止號的時候
	else{
		strStateSQL=getSumSQL(" sum(sumcount) ",noSQL,state,sendDate,mtsql,strt)+"  with ur ";
	}
   
   turnPage1.queryModal(strStateSQL, CardState);
   arrResult=null;
   arreResult=null;
  if((!turnPage2.strQueryResult)&&(!turnPage1.strQueryResult))
  {
     alert("没有符合查询条件的记录！");
     return false;
  }
}

/**
 * 定额單證明細查詢SQL
 * @param fSQL
 * @param noSQL
 * @param strz
 * @returns
 */
function detailSQL(tCom,state,sendDate,mtsql,strt){
	
		
		
	var ttsql=""
	
	if (fm.TempComCode.value.length<=4) {
		
		ttsql = " (case when (select (select comcode from lduser where usercode=substr(char(ReceiveCom),2)) " +
				"from LZCardTrack b where a.CertifyCode = b.CertifyCode and b.subcode=a.subcode and a.StartNo >= b.StartNo " +
				"and a.EndNo <= b.EndNo " +
		    		" and b.ReceiveCom like 'B%'  " +
		    		"and b.ReceiveCom <> 'A86' " +
		    		" and b.sendoutcom like 'A%' " +
		    		mtsql
		    		+" order by  makedate desc fetch first 1 rows only) is null  " +
		    				" then (select substr(char(ReceiveCom),2) " +
		    				"from LZCardTrack b where a.CertifyCode = b.CertifyCode " +
		    				" and b.subcode=a.subcode" +
		    				" and a.StartNo >= b.StartNo and a.EndNo <= b.EndNo " +
		    		" and b.ReceiveCom like 'A%'  " +
		    		"and b.ReceiveCom <> 'A86' " +
		    		mtsql
		    		+" order by  makedate desc fetch first 1 rows only) " +
		    				" else (select (select comcode from lduser " +
		    				"where usercode=substr(char(ReceiveCom),2)) " +
		    				"from LZCardTrack b where a.CertifyCode = b.CertifyCode " +
		    				" and a.StartNo >= b.StartNo and b.subcode=a.subcode" +
		    				" and a.EndNo <= b.EndNo " +
		    		" and b.ReceiveCom like 'B%'  " +
		    		"and b.ReceiveCom <> 'A86' " +
		    		" and b.sendoutcom like 'A%' " +
		    		mtsql
		    		+" order by  makedate desc fetch first 1 rows only) end)  ";
		
       	
      } else {
    	  ttsql = " (select comcode from lduser where usercode=substr(char(a.receivecom),2)) ";
//    	  if(fm.ManageComC.value!=""){
//  		
//    		  mtsql=" and exists (select 1 from lduser where usercode=substr(char(a.receivecom),2) and comcode like '"+fm.ManageComC.value+"%')";
//  	}
      }
	
    var strSQL = "select substr(a.ReceiveCom, 2), "
        + "(select HandleDate from LZCardTrack c where c.CertifyCode = a.CertifyCode and c.subcode=a.subcode and c.StartNo = a.StartNo "
        + "and c.EndNo = a.EndNo and a.State in ('10', '11') order by c.MakeDate desc fetch first 1 rows only), "
        + "b.CardNo, (select CodeName from LDCode where CodeType='certifystate' and Code = a.State), "
        + "case when a.State in ('2','3','4','5','6','12') then char(a.HandleDate) else '' end, " 
        +ttsql
        + " from LZCard a, LZCardNumber b "
        + "where a.SubCode = b.CardType and a.StartNo <= b.CardSerNo and a.EndNo >= b.CardSerNo "
        + tCom
        + getWherePart('a.CertifyCode', 'CertifyCode')
        + getWherePart('a.SubCode', 'CardType')
        + getWherePart('a.ReceiveCom', 'ReceiveCom')
        + getWherePart('b.CardSerNo', 'StartNo', '>=')
        + getWherePart('b.CardSerNo', 'EndNo', '<=')
        + state
        + mtsql
        + strt
        + sendDate
       
		return strSQL;
}

/**
 * 定额單證統計部份SQL
 * @param sumcountno
 * @param noSQL
 * @param strz
 * @returns {String}
 */
function getSumSQL(sumcountno,noSQL,state,sendDate,mtsql,strt){
	
	var strStateSQL = "";
	
	strStateSQL = "  select certifycode,"
        		+ "(case when a.state in ('0','8') then '未领用' " +
        		"  when a.State in ('10','11') then '已领用' " +
        		" when a.State = '9' then '已发放' " +
        		"else  (select codename from ldcode where code=a.state and codetype='certifystate') end ) as state,"
        		+ sumcountno
        		+ "from LZCard a "
        		+ "where 1=1 "
        		+ getWherePart('a.CertifyCode', 'CertifyCode')
        		+ getWherePart('a.SubCode', 'CardType')
        		+ getWherePart('a.ReceiveCom', 'ReceiveCom')
        		+ noSQL
        		+ state
        		+mtsql
        		+strt
        		+ sendDate ;
	strStateSQL += " Group by CertifyCode, State   ";
	return strStateSQL;
}

//更新数据的函数
function printInfo()
{
  var i = 0;
  if(CertifyMaxGrid.mulLineCount==0){
  	alert("列表缺少数据,请先查询!!!");
  	return ;
  }

  fm.ReportType.value="2";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "CardSendReportPrint";
  fm.action = "../f1print/CardSendReportPrint.jsp";
  fm.submit();
  showInfo.close();
}

function showalert(){
	alert("没有符合条件的数据！");
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	fm.ReceiveType.value="AGE";
  if(fm.all('AgentCode').value == "")	
  {  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
      fm.AgentCode.value='D'+arrResult[0][0];
      fm.ReceiveCom.value='D'+arrResult[0][0];
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}
function queryCom()
{
    fm.ReceiveType.value="AGECOM";
    showInfo = window.open("./LAComMain.jsp?ManageCom=" + fm.all('ManageCom').value.substr(0, 4) + "&BranchType=3&BranchType2=01&BankType=01");
}

function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	if( fm.chkModeBatch.checked == false ) {
	    fm.PrtNoEx.value = arrResult[0][0];
	    fm.ReceiveCom.value = 'A' + arrResult[0][11];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
	    
			var rowCount = 0;

	    CertifyList.setRowColData(rowCount, 1, arrResult[0][1]);
	    CertifyList.setRowColData(rowCount, 2, arrResult[0][25]);
	    CertifyList.setRowColData(rowCount, 3, arrResult[0][18]);
	    CertifyList.setRowColData(rowCount, 4, arrResult[0][19]);
	    
	    var cNum = parseFloat(arrResult[0][19])-parseFloat(arrResult[0][18])+1;
	    CertifyList.setRowColData(rowCount, 5, cNum+"");
	    
	  } else {
	    fm.ReceiveCom.value = arrResult[0][3];
	    fm.ReceiveCom.title = arrResult[0][1];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
		}
  }
}

function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  	if (fm.ReceiveType.value=="AGE") {
  		fm.AgentCode.value='D'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][95];
  	} else if (fm.ReceiveType.value=="AGECOM") {
  		fm.AgentCode.value='E'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][0];
  	}
	}
}
