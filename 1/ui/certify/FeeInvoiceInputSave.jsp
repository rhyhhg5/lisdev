<%
//程序名称：CertifyTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.taskservice.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;
  String szFailSet = "";

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  String tPayNo[] = request.getParameterValues("CertifyList2");
	String tInvoiceNo[] = request.getParameterValues("CertifyList3");
	String tNewInvoiceNo[] = request.getParameterValues("CertifyList4");

  //校验处理
  //内容待填充
  
  String tCertifyCode = request.getParameter("CertifyCode");
  LZCardSchema tLZCardSchema = new LZCardSchema();
  tLZCardSchema.setCertifyCode(tCertifyCode);
  LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
  
  for (int i=0;i<tInvoiceNo.length;i++) {
    LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
    tLOPRTManager2Schema.setOtherNo(tPayNo[i]);
    tLOPRTManager2Schema.setStandbyFlag4(tInvoiceNo[i]);
    tLOPRTManager2Schema.setStandbyFlag1(tNewInvoiceNo[i]);
    tLOPRTManager2Set.add(tLOPRTManager2Schema);
  }
  // 准备传输数据 VData
  VData vData = new VData();
		
	vData.addElement(globalInput);
	vData.addElement(tLOPRTManager2Set);
	vData.addElement(tLZCardSchema);
	
	InvoiceNoModifyBL tInvoiceNoModifyBL = new InvoiceNoModifyBL();

	if (!tInvoiceNoModifyBL.submitData(vData, "INSERT")) {
	   Content = " 保存失败，原因是: " + tInvoiceNoModifyBL.mErrors.getFirstError();
	   FlagStr = "Fail";
  } else {
	   Content = " 保存成功 ";
	   FlagStr = "Succ";
	}

%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
<body>
</body>
</html>

