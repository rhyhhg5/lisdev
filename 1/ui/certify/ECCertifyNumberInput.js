//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm(){
	
	var strReturn = chkValue();

	if (strReturn == '' || strReturn == null){
		//根据单证描述中的单证编码长度，自动补齐单证长度    zhangjianbao    2007-10-27 
		var sql = "select count(*) from lzcardnumber where  CardType='" + fm.all('CardType').value + "' and CardSerNo='" + LCh(fm.all('StartSerNo').value,"0",fm.all('CertifyLength').value) + "'"  ; 　

    var rs = easyExecSql(sql);    

    if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != 0)
    {     	 
    	 alert("该号码已存在，请重新输入");
  	   return ;   
    }

    if(fm.StartSerNo.value.length != fm.CertifyLength.value)
    {     	 
    	 alert("起始号长度和单证描述的长度不相符，请重新输入！");
  	   return ;   
    }

    var sql = "select 1 from LMCertifyDes where CertifyCode = '" 
        + fm.all('CertifyCode').value 
        + "' and CheckRule = '2' and OperateType in ('1','3')";
    var rs = easyExecSql(sql);
    if(rs)
    {
        alert("该单证类型描述为无校验规则的撕单或pos机，\n请直接在印刷管理中印刷！");
        return false;
    }

    if (!chkExistNo()){     	 
    	return false;
    } 
    var strExsit = chkExistPrt();     
    if(strExsit!=""){    	
    	 alert(strExsit);
       return false;
    }

    if (fm.CardAmount.value>5000) {
    	alert("单次最多生成5000张");
    	return false;
    }
    
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      
  	fm.action = "ECCertifyNumberSave.jsp";
    fm.submit(); //提交
  }else
  {  	
  	alert(strReturn);
  	return false;
  }
  
}
//提交，保存按钮对应操作
function confirmClike(){
	
   var strRet =""; 
  if(!chkMustInput()){  	
     return false;
  }  
  strRet =chkNotExistPrt();
  if(strRet !=""){  	
  	 alert(strRet);
     return false;
  }
  strRet = chkConfirm();
  if(strRet !=""){  	
  	 alert(strRet);
     return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.Operate.value = "confirm";    
	fm.action = "ECCertifyNumberSave.jsp";
  fm.submit(); //提交

  
}
//双击两位单证类型码
function querySartCard()
{
  
   if (fm.all('CardType').value == ''){
  	  alert("请输入单证类型码");
  	  return ;
  	}
  	if (trim(fm.all('CardType').value).length<2){
  	  alert("请输入两位单证类型码");
  	  return ;
  	}  	
  	// if(isNaN(fm.all('CardType').value))
    //{
    //	 	alert("两位单证类型码中不能有字符！");
    //    return ;
    //}
  	var sql = "select max(int(CardSerNo))+1 from lzcardnumber where  CardType='" + fm.all('CardType').value + "'" ; 　
    
    var rs = easyExecSql(sql);    

		//补齐单证编码长度    zhangjianbao    2007-10-27 
    if(rs && rs[0][0] != "" && rs[0][0] != "null")
    {
    	fm.all('StartSerNo').value =  LCh(rs[0][0],"0",fm.all('CertifyLength').value);
    	if(rs[0][0]=="0"){
    		 fm.all('StartSerNo').value =  LCh(1,"0",fm.all('CertifyLength').value);
    	}      
    }  	
    fm.all('MaxSerNo').value =  fm.all('StartSerNo').value;
 
}

//单证类型码变更时
function queryInfo()
{
  
   if (fm.all('CardType').value == ''){
  	  alert("请输入单证类型码");
  	  return ;
  	}
  	if (trim(fm.all('CardType').value).length<2){
  	  alert("请输入两位单证类型码");
  	  return ;
  	}  	
  	// if(isNaN(fm.all('CardType').value))
    //{
    //	 	alert("两位单证类型码中不能有字符！");
    //    return ;
    //}
    //起始号长度置为空    zhangjianbao    2007-10-27 
    fm.all('StartSerNo').value = "";

  	//改过表后用
  	var sql = "select distinct CertifyLength, CheckRule, OperateType from LMCertifyDes where SubCode='" + fm.all('CardType').value + "'" ; 　
    //var sql = "select distinct CertifyLength, CheckRule from LMCertifyDes where SubCode='" + fm.all('CardType').value + "'" ; 　
    
    var rs = easyExecSql(sql);    
  	
  	if(!rs) {
  		alert("两位单证码未描述，请先对该进行描述！");
  		return ;
  	}
    //单证流水号长度
    if(rs && rs[0][0] != "" && rs[0][0] != "null")
    {
    	fm.all('CertifyLengthV').value = 2 + parseInt(rs[0][0]);
    	fm.all('CertifyLength').value = rs[0][0];
    }  	
    //校验规则  1.有校验；2.无校验。 规范后用
    if(rs && rs[0][1] != "" && rs[0][1] != "null")
    {
    	if(rs[0][1]=="1"){
    		 fm.all('ChkRuleName').value =  "有校验";
    		 fm.all('CheckRule').value =  "1";
    	} else if(rs[0][1]=="2") {
    		 fm.all('ChkRuleName').value =  "无校验";
    		 fm.all('CheckRule').value =  "2";
    	}
    } else {
    		 fm.all('CheckRule').value =  "";
    }
//    //校验规则  1.有校验；2.无校验。 规范前用
//    if(rs && rs[0][1] == "" || rs[0][1] == "null" || rs[0][1]=="1")
//    {
//    	fm.all('CheckRule').value =  "无校验";
//    } else if(rs[0][1]=="2") {
//    	fm.all('CheckRule').value =  "有校验";
//    }
    //业务类型  0.卡单；1.撕单。
    if(rs && rs[0][2] != "" && rs[0][2] != "null")
    {
    	//alert(rs[0][2]);
    	if(rs[0][2]=='0'){
    		 fm.all('CardTypeName').value =  "卡单";
    		 fm.all('OperateType').value =  "0";
    	} else if(rs[0][2]=="1") {
    		 fm.all('CardTypeName').value =  "撕单";
    		 fm.all('OperateType').value =  "1";
      }
      else if(rs[0][2]=="2") {
    		 fm.all('CardTypeName').value =  "虚拟卡";
    		 fm.all('OperateType').value =  "2";
      }
      else if(rs[0][2]=="3") {
    		 fm.all('CardTypeName').value =  "POS机出单";
    		 fm.all('OperateType').value =  "3";
      }
      else if(rs[0][2]=="4") {
    		 fm.all('CardTypeName').value =  "网销";
    		 fm.all('OperateType').value =  "4";
      }
    } else {
    		 fm.all('CardTypeName').value =  "";
    		 fm.all('OperateType').value =  "";
    }
}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, TakeBackNo, CertifyCode )
{
  showInfo.close();

	//if( fm.chkModeBatch.checked == true ) {
		//var urlStr = "CertBatchLogQuery.html";
   // showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
     // oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	//}
	
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	    content="保存成功！";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;	    
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

  }
}
function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  	if(fm.all('ButtonNo').value=="1")
  	{
  		fm.AgentCode.value=arrResult[0][0];
  		}
  	else{
  		fm.AgentCode2.value=arrResult[0][0];
  		}
  	
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//毕录项校验
function chkValue()
{
   var strReturn ="";
   
   //单证类型码校验  
   if (fm.all('CardType').value=='')
	  {
	    strReturn = "请输入单证类型码";
	    return strReturn;
	  }
    if (trim(fm.all('CardType').value).length!=2)
	  {
	    strReturn = "请输入两位单证类型码";
	    return strReturn;
	  }
    //if(isNaN(fm.all('CardType').value))
    //{
    //	 	strReturn="两位单证类型码中不能有字符！";
    //    return strReturn;
    //}
    //单证起始号校验  
    if (fm.all('StartSerNo').value=='')
	  {
	    strReturn = "请输入单证起始号";
	    return strReturn;
	  }	  
	 
    if( isNaN(fm.all('StartSerNo').value))
    {
    	 	strReturn="单证起始号中不能有字符！";
        return strReturn;
    }
  	  //终止单证数量校验  
   if (fm.all('CardAmount').value=='')
	  {
	    strReturn = "请输入生成单证的数量";
	    return strReturn;
	  }
    if(isNaN(fm.all('CardAmount').value))
    {
    	 	strReturn="生成单证数量中不能有字符！";
        return strReturn;
    }
      //单证类型编码校验  
    if (fm.all('OperateType').value=='')
	  {
	    strReturn = "请选择单证类型编码";
	    return strReturn;
	  }
      //校验规则  2007-9-8
    if (fm.all('CheckRule').value=='')
	  {
	    strReturn = "请选择校验规则";
	    return strReturn;
	  }
	 //单证价格
    if (fm.all('CertifyPrice').value=='')
	  {
	    strReturn = "请输入单证价格";
	    return strReturn;
	  }
	 if(isNaN(fm.all('CertifyPrice').value))
    {
    	 	strReturn="单证价格中不能有字符！";
        return strReturn;
    }
    //是否生成密码
    if (fm.all('password').value=='')
	  {
	    strReturn = "请选择是否生成密码";
	    return strReturn;
	  }
	 //发放者
    if (fm.all('SendOutComEx').value=='')
	  {
	    strReturn = "请选择发放者";
	    return strReturn;
	  }
	//接收者
    if (fm.all('AgentCode').value=='')
	  {
	    strReturn = "请选择接收者";
	    return strReturn;
	  }
}

//下载清单
function printData(){
  var strReturn = chkValue();
  if (strReturn == '' || strReturn == null){
  	var strRet = chkConfirm();
	  if(strRet !=""){  	 
	  	 alert(strRet);
	     return false;
	  }
  	window.open("../certify/CertifyNumberPrint.jsp?CardType="+fm.all('CardType').value + "&PrtNo="
  	+ fm.all('PrtNo').value ); 
  	
  }else
  {
  	alert(strReturn);
  	return false;
  }
  
}

//毕录项校验
function chkExistNo()
{
	var startNo = LCh(fm.all('StartSerNo').value, "0", fm.all('CertifyLength').value);
  var endNo = LCh((parseInt(fm.all('StartSerNo').value)+ parseInt(fm.all('CardAmount').value) - 1) + "", '0', fm.all('CertifyLength').value) ;
  
  var sql = "select count(*) from lzcardnumber where CardType='" + fm.all('CardType').value + "' and CardSerNo>='" + startNo + "' and CardSerNo<='" + endNo + "'";
 	
  var rs = easyExecSql(sql);    

  if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != 0)
  { 
  	 alert("该范围的号码已生成！！！");
	   return false;   
  }  	
   return true;
  	
}
//确定按钮
function printConfirm()
{
  var endNo = LCh((parseInt(fm.all('StartSerNo').value)+ parseInt(fm.all('CardAmount').value) - 1) + "",'0',fm.all('CertifyLength').value) ;
 	var sql = "select count(*) from lzcardnumber where  CardType='" + fm.all('CardType').value + "' and CardSerNo>='" + LCh(fm.all('StartSerNo').value,"0",fm.all('CertifyLength').value) + "'"  　
  + " and CardSerNo<='"+endNo +"'";
  var rs = easyExecSql(sql);    

  if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != 0)
  { 
  	 showInfo.close();
  	 alert("该范围的号码已生成！！！");
	   return false;   
  }  	
   return true;
  	
}

//当下拉框的值被选中时触发事件，查询该单证类型的基本信息
function afterCodeSelect()
{
	queryInfo();
	querySartCard();
}
//批量发放。查询代理机构的函数。
function queryCom()
{
	//下发对象：代理机构
	fm.all('ButtonNo').value="1";
	fm.ReceiveType.value="AGECOM";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCode').value == "")	
  {  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LAComQueryInput.jsp?ManageCom="+fm.all('ManageCom').value,"LAComQueryInput",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}


function queryCom2()
{
	//下发对象：代理机构
	fm.all('ButtonNo').value="2";
	fm.ReceiveType.value="AGECOM";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCode2').value == "")	
  {  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LAComQueryInput.jsp?ManageCom="+fm.all('ManageCom').value,"LAComQueryInput",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode2').value != "")	 
	{
		var cAgentCode = fm.AgentCode2.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}


function queryCertify(){
  showInfo = window.open("./CertifyInfoQuery.jsp?certifyCode=certifycoded");
}

function queryAgent(ageType)
{
	//下发对象：代理人
	fm.all('ButtonNo').value="1";
	fm.ReceiveType.value="AGE";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}	
  if(fm.all('AgentCode').value == "")	
  {    
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&branchtype="+ageType,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select a.AgentCom,a.Name,a.ManageCom"	       
	         +" from LACom a where a.AgentCom='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理机构编码:["+arrResult[0][0]+"] 代理机构名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此代理机构！");
    } 
	}
}

function queryAgent2(ageType)
{
	//下发对象：代理人
	fm.all('ButtonNo').value="2";
	fm.ReceiveType.value="AGE";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}	
  if(fm.all('AgentCode2').value == "")	
  {    
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&branchtype="+ageType,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode2').value != "")	 
	{
		var cAgentCode = fm.AgentCode2.value;  //保单号码	
		var strSql = "select a.AgentCom,a.Name,a.ManageCom"	       
	         +" from LACom a where a.AgentCom='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理机构编码:["+arrResult[0][0]+"] 代理机构名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此代理机构！");
    } 
	}
}
