//               该文件中包含客户端需要处理的函数和事件

var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	try {
		if( verifyInput() == true && CertifyList.checkValue("CertifyList") ) {
	  	var i = 0;
	  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
			fm.SendOutCom.value = fm.SendOutComEx.value;
			fm.PrtNo.value = fm.PrtNoEx.value;
		
	  	fm.submit(); //提交
	  }
  } catch(ex) {
  	showInfo.close( );
  	alert(ex);
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, TakeBackNo )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	if( fm.chkPrt.checked == true ) {
	    var urlStr = "CertifyListPrint.jsp";
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
	      oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	  } else {
	    content="保存成功！";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	    
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
	parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

// 查询印刷号的功能
function queryPrtNo()
{
	fm.sql_where.value = " State = '1' ";
  showInfo = window.open("./CertifyPrintQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
    fm.PrtNoEx.value = arrResult[0][0];
    fm.ReceiveCom.value = 'A' + arrResult[0][11];
    
    CertifyList.clearData();
    CertifyList.addOne();
    
    var rowCount = 0;
    
    CertifyList.setRowColData(rowCount, 1, arrResult[0][1]);
    CertifyList.setRowColData(rowCount, 3, arrResult[0][18]);
    CertifyList.setRowColData(rowCount, 4, arrResult[0][19]);
  }
}
