<html>
	<%
		//程序名称：已激活单证的运维功能
		//程序功能：
		//创建日期：2011-6-7
		//创建人  ：xp
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<!--用户校验类-->
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
        <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
        <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
        <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
        <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
        <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
        <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
		<SCRIPT src="CardUpActiveDateInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="CardUpActiveDateInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./CardUpActiveDateSave.jsp" method=post name=fm
			target="fraSubmit">
			<Table class=common>
				<tr class=common>
					<td class=title>
						单证编码：
					</TD>
					<td class=input>
						<Input class="common" name=CertifyCode >
					</TD>
					<td class=title>
						单证类型码：
					</TD>
					<td class=input>
						<Input class="common" name=CardType >
					</TD>
				</tr>

				<TR class=common>
					<TD class=title>
						单证起始号：
					</TD>
					<TD class=input>
						<Input class=common name=StartNo 
							verify="单证起始号|NUM" maxlength=16>
					</TD>
					<TD class=title>
						单证终止号：
					</TD>
					<TD class=input>
						<Input class=common name=EndNo 
							verify="单证终止号|NUM" maxlength=16>
					</TD>
				</TR>
			</Table>
			<input class="cssButton" type=button value="查询" name="query" ID="query" onclick="Query()">
			<input class="cssButton" type=button value="保存" name="save" ID="save" onclick="submitForm()">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divCertifyActive);">
					</td>
					<td class=titleImg>
						单证列表
					</td>
				</tr>
			</table>
			<div id="divCertifyActive">
				<table class="common">
					<tr class="common">
						<td text-align: left colSpan=1>
							<span id="spanCertifyActiveGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<Div id= "divPage" align=center style= "display:''">
				  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage.firstPage();"> 
				  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
				  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
				  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage.lastPage();">
		  	</Div>
			<p></p>			
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
