<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="CertBalance.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertBalanceInit.jsp"%>
<title>单证结算</title>
</head>
<body  onload="initForm();" >
  <form action="./CertBalancePrint.jsp" method=post name=fm target="_blank">
    <table class="common" border=0 width=100%>
      <tr><td class="titleImg" align="center">请输入查询条件：</td></tr>
	  </table>
    
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <td class="title">发放机构</td>
        <td class="input">
          <input class="codeno" name="SendOutCom" ondblclick="return showCodeList('comcode',[this,SendOutComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,SendOutComName],[0,1]);" verify="发放机构|NOTNULL"><Input name=SendOutComName class="codename" ></td>
        <td class="title">接收机构</td>
        <td class="input">
          <input class="codeno" name="ReceiveCom" ondblclick="return showCodeList('comcode',[this,ReceiveComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ReceiveComName],[0,1]);" verify="接收机构|NOTNULL"><Input name=ReceiveComName class="codename" ></td>
      </tr>
        
      <tr class="common">
        <td class="title">开始日期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="MakeDateB"></td>

        <td class="title">结束日期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="MakeDateE"></td>
      </tr>

      <tr class="common"><td cols=4><br></td></tr>
    </table>

    <input value="打  印" type="button" onclick="submitForm();" class="cssButton">

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
