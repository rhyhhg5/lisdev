<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  System.out.println("开始执行Save页面");
  LMCertifyDesSchema mLMCertifyDesSchema = new LMCertifyDesSchema();
  LMCertifyDesSet mLMCertifyDesSet = new LMCertifyDesSet();
  LMCardRiskSet mLMCardRiskSet = new LMCardRiskSet();
  CertifyDescUI mCertifyDescUI = new CertifyDescUI();
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mCertifyClass = request.getParameter("CertifyClass");//记录单证的类型
  String mDescType = "";//将操作标志的英文转换成汉字的形式
  String subCode = request.getParameter("SubCode").trim();

  System.out.println("开始进行获取数据的操作！！！");
  mLMCertifyDesSchema.setCertifyCode(request.getParameter("CertifyCode"));					//单证编码
  mLMCertifyDesSchema.setVerifyFlag(request.getParameter("VerifyFlag"));						//业务员校验标志
  mLMCertifyDesSchema.setSubCode(subCode);																					//单证类型码
  mLMCertifyDesSchema.setRiskCode(request.getParameter("RiskCode"));
  mLMCertifyDesSchema.setCertifyName(request.getParameter("CertifyName"));					//单证名称
  mLMCertifyDesSchema.setPrem(request.getParameter("Prem"));
  mLMCertifyDesSchema.setAmnt(request.getParameter("Amnt"));
  mLMCertifyDesSchema.setCertifyClass(request.getParameter("CertifyClass"));				//单证类型
  mLMCertifyDesSchema.setNote(request.getParameter("Note"));												//备注
  mLMCertifyDesSchema.setImportantLevel(request.getParameter("ImportantLevel"));
  mLMCertifyDesSchema.setPolPeriod(request.getParameter("DutyPer"));								//保险期限    2007-9-14 10:38
  mLMCertifyDesSchema.setPolPeriodFlag(request.getParameter("DutyUinit"));					//保险期限单位    2007-9-14 10:38
  mLMCertifyDesSchema.setState(request.getParameter("State"));											//状态
  mLMCertifyDesSchema.setManageCom(request.getParameter("ManageCom"));							//管理机构
  mLMCertifyDesSchema.setInnerCertifyCode(request.getParameter("InnerCertifyCode"));
  mLMCertifyDesSchema.setHaveNumber(request.getParameter("HaveNumber"));						//是否是有号单证
  mLMCertifyDesSchema.setCertifyLength(request.getParameter("CertifyLength"));
  mLMCertifyDesSchema.setCertifyLength(mLMCertifyDesSchema.getCertifyLength() - subCode.length());			//单证号码长度
  mLMCertifyDesSchema.setUnit(request.getParameter("Unit"));												//单证单位
  mLMCertifyDesSchema.setCertifyClass2(request.getParameter("CertifyClass2"));
  mLMCertifyDesSchema.setCheckRule(request.getParameter("CheckRule"));							//校验规则  1.有校验；2.无校验。  2007-9-13 10:38
  mLMCertifyDesSchema.setOperateType(request.getParameter("Operate"));							//业务类型  0.卡单；1.撕单。  2007-9-13 10:38
System.out.println(mLMCertifyDesSchema.getCertifyLength());
  /****************************************************************************
   * 判断单证的类型，若是定额单证则要对定额单险种信息表进行描述
   ***************************************************************************/

  if(mCertifyClass.equals("D"))
  {
    String[] strNumber = request.getParameterValues("CardRiskGridNo");
    String[] strRiskCode = request.getParameterValues("CardRiskGrid1");
    String[] strPrem = request.getParameterValues("CardRiskGrid2");
    String[] strPremLot = request.getParameterValues("CardRiskGrid3");
    String[] strMult = request.getParameterValues("CardRiskGrid4");
    String[] strRiskType = request.getParameterValues("CardRiskGrid5");
    
    if(strNumber != null && strRiskCode[0]!=null&&strRiskCode[0].length()>0)
    {
      int tLength = strNumber.length;
      for(int i = 0 ;i < tLength ;i++)
      {
        System.out.println("保费：" + strPrem[i]);
        LMCardRiskSchema tLMCardRiskSchema = new LMCardRiskSchema();
        tLMCardRiskSchema.setCertifyCode(request.getParameter("CertifyCode"));
        tLMCardRiskSchema.setRiskCode(strRiskCode[i]);
        tLMCardRiskSchema.setPrem(strPrem[i]);
        tLMCardRiskSchema.setPremProp("1");
        tLMCardRiskSchema.setPremLot(strPremLot[i]);
        tLMCardRiskSchema.setMult(String.valueOf(strMult[i]));
        tLMCardRiskSchema.setRiskType(strRiskType[i]);
        mLMCardRiskSet.add(tLMCardRiskSchema);
      }
    }
  }

  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改";
    mLMCertifyDesSchema.setRiskVersion(request.getParameter("CertifyCode_1"));
    System.out.println("修改时的校验单证号码是"+mLMCertifyDesSchema.getRiskVersion());
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询";
  }

  mLMCertifyDesSet.add(mLMCertifyDesSchema);
  VData tVData = new VData();
  try
  {
    tVData.addElement(mOperateType);
    tVData.addElement(mCertifyClass);
    tVData.addElement(mLMCertifyDesSet);
    /**************************************************************************
     **判断单证的类型，若是定额单证，则向后台传递定额单险种信息
     *************************************************************************/
    if(mCertifyClass.equals("D"))
    {
      tVData.addElement(mLMCardRiskSet);
    }
    mCertifyDescUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mCertifyDescUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+" 成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>