<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：RetCardPlanInit.jsp
//程序功能：
//创建日期：2002-08-15
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strManageCom = globalInput.ManageCom;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
  	fm.reset();
  	fm.cur_com.value = '<%= strManageCom %>';
  }
  catch(ex)
  {
    alert("在RetCardPlanInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在RetCardPlanInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
		initCardPlanInfo();
		queryPlan();
  }
  catch(re)
  {
    alert("在RetCardPlanInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCardPlanInfo()
{                               
	var iArray = new Array();
      
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="计划标识";         		//列名
		iArray[1][1]="100px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="申请机构";          	//列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=100;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="单证编码";        		//列名
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=100;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="申请数量";          	//列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            		 	  //列最大值
		iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="批复数量";          	//列名
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=100;            		 	  //列最大值
		iArray[5][3]=1;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="批复状态";          	//列名
		iArray[6][1]="60px";            		//列宽
		iArray[6][2]=100;            		 	  //列最大值
		iArray[6][3]=2;              			  //是否允许输入,1表示允许，0表示不允许
    iArray[6][10]="RetState";						//批复状态
    iArray[6][11]="0|^Y|同意|^N|不同意";

		CardPlanInfo = new MulLineEnter("fm" , "CardPlanInfo"); 

    CardPlanInfo.mulLineCount = 0;   
    CardPlanInfo.displayTitle = 1;
    CardPlanInfo.hiddenPlus = 1;
		CardPlanInfo.loadMulLine(iArray);  
 
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>