<html>
<%@page contentType="text/html;charset=GBK" %>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.certify.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="CardMaxInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="CardMax_MInit.jsp"%>
</head>
<%
  GlobalInput globalInput = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">
  var manageCom = "<%=globalInput.ManageCom%>";
  var cardCode = "CardCode";
  var receiveType = "";
</script>

<body  onload="initForm();" >
    <form action="./CardMaxSave.jsp" method=post name=fm target="fraSubmit">
        <%@include file="../common/jsp/InputButton.jsp"%>
        <table>
            <tr>
                <td class= titleImg>
                    查询条件
                </td>
            </tr>
        </table>
        <table class= common>
            <tr class= common>
                <td class=title>单证类型</td>
                <td class=input><input class=codeno name=CertifyClass CodeData="0|^D|定额单证^P|普通单证"
                    ondblClick="showCodeListEx('CertifyClassList',[this,CertifyClassName],[0,1],null,null,null,1);"
                    onkeyup="showCodeListKeyEx('CertifyClassList',[this,CertifyClassName],[0,1],null,null,null,1);"><input name=CertifyClassName class=codename >
                </td>
                <td class=title>单证编码</td>
                <td class=input><input class="codeno" name="CertifyCode"
                    ondblclick="return showCodeList(cardCode, [this,CertifyName],[0,1],null,null,null,1,350);"
                    onkeyup="return showCodeListKey(cardCode, [this,CertifyName],[0,1],null,null,null,1,350);"><input name=CertifyName class=codename >
                </td>
                <td class=title>发放对象</td>
                <td class=input><input class=codeno name=AgentGrade CodeData="0|^0|代理人^1|业务员^2|代理机构^3|默认配置"
                    ondblClick="showCodeListEx('AgentGradeList',[this,AgentGradeName],[0,1],null,null,null,1);"
                    onkeyup="showCodeListKeyEx('AgentGradeList',[this,AgentGradeName],[0,1],null,null,null,1);"><input name=AgentGradeName class=codename >
                </td>
            </TR>
        </table>
        <input class="cssButton" type= button value="查  询" onclick="queryInfo()">
        <table>
            <tr>
                <td class= titleImg>
                    配置列表
                </td>
            </tr>
        </table>

        <div  id= "divLLReport2" style= "display: ''">
            <table  class= common>
                <tr  class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanCertifyMaxGrid" >
                        </span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input class=cssButton value="首页" type=button onclick="turnPage.firstPage();">
                <input class=cssButton value="上一页" type=button onclick="turnPage.previousPage();">
                <input class=cssButton value="下一页" type=button onclick="turnPage.nextPage();">
                <input class=cssButton value="尾页" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
    </form>
    <hr><br>
    <form action="" method=post name=fmSave target="fraSubmit">
        <table>
            <tr>
                <td class= titleImg>
                    配置详情
                </td>
            </tr>
        </table>
        <table class="common">
            <tr class="common">
                <td class=title>单证类型</td>
                <td class=input><input class=codeno name=CertifyClass CodeData="0|^D|定额单证^P|普通单证"
                    ondblClick="showCodeListEx('CertifyClassList',[this,CertifyClassName],[0,1],null,null,null,1);"
                    onkeyup="showCodeListKeyEx('CertifyClassList',[this,CertifyClassName],[0,1],null,null,null,1);"><input name=CertifyClassName class=codename >
                </td>
                <td class="title">单证编码</td>
                <td class="input"><input class="codeno" name="CertifyCode"
                    ondblclick="return showCodeList(cardCode, [this,CertifyName],[0,1],null,null,null,1,350);"
                    onkeyup="return showCodeListKey(cardCode, [this,CertifyName],[0,1],null,null,null,1,350);"><input name=CertifyName class=codename >
                </td>
                <td class="title">发放对象</td>
                <td class="input"><input class=codeno name=AgentGrade CodeData="0|^0|代理人^1|业务员^2|代理机构"
                    ondblClick="showCodeListEx('AgentGradeList',[this,AgentGradeName],[0,1],null,null,null,1);"
                    onkeyup="showCodeListKeyEx('AgentGradeList',[this,AgentGradeName],[0,1],null,null,null,1);"><input name=AgentGradeName class=codename >
                </td>
            </tr>
            <tr class="common">
                <td class="title">最大数量</td>
                <td class="input"><input class=common name=MaxCount ></td>
                <td class="title">回销期</td>
                <td class="input"><input class=common name=VerPeriod ></td>
            </tr>
        </table>
        <br>
        <input type="hidden" name="Operate">
        <input value="保  存" class=cssButton type=button onclick="insertForm();">
        <input value="修  改" class=cssButton type=button onclick="modifyForm();">
        <input value="删  除" class=cssButton type=button onclick="deleteForm();">
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>