<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：CertifySendOutSave.jsp
//程序功能：
//创建日期：2002-09-23
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%
//输出参数
String strTakeBackNo = "";
String FlagStr = "Fail";
String Content = "";
boolean bContinue = true;
String szFailSet = "";
String certifyCode = "aa";

GlobalInput globalInput = new GlobalInput( );
globalInput.setSchema( (GlobalInput)session.getValue("GI") );

//校验处理
//内容待填充
try {
	// 单证信息部分
	String szPrtNo				= request.getParameter("PrtNo");
	String szReceiveType		= request.getParameter("ReceiveType"); //接收者类型
	
	String szSendOutCom 	= request.getParameter("SendOutCom");
	String szReceiveCom		= "";
	if (szReceiveType.equals("AGE"))
	{
		szReceiveCom	= request.getParameter("AgentCode");
	}
	else if(szReceiveType.equals("AGECOM"))
	{
		szReceiveCom = request.getParameter("AgentCode");
	}
	else
	{
		szReceiveCom = request.getParameter("ReceiveCom");
	}
	System.out.println("ReceiveType: " + szReceiveType + "    SendOutCom: " + szSendOutCom + "    ReceiveCom: " + szReceiveCom);
	String szInvaliDate 	= request.getParameter("InvalidDate");
	String szAmnt 				= request.getParameter("Amnt");
	String szHandler			= request.getParameter("Handler");
	String szHandleDate		= request.getParameter("HandleDate");

	String szLimitFlag    = request.getParameter("LimitFlag");
	String szBatchMode		= request.getParameter("chkModeBatch"); //是否批量发放

	String szNo[]					= request.getParameterValues("CertifyListNo");
	String szCertifyCode[]= request.getParameterValues("CertifyList1");
	String szStartNo[]		= request.getParameterValues("CertifyList3");
	String szEndNo[]			= request.getParameterValues("CertifyList4");
	String szSumCount[]   = request.getParameterValues("CertifyList5");
	int nIndex = 0;

	LZCardSet setLZCard = new LZCardSet( );
	LZCardPrintSet setLZCardPrint = new LZCardPrintSet( );
    System.out.println("hihihihi 1: ");
	if( szSendOutCom.equals("00") ) {  // 如果是单证入库操作
    	if( szPrtNo == null || szPrtNo.equals("") ) {
    		throw new Exception("没有输入单证印刷号");
    	}

			LZCardPrintSchema schemaLZCardPrint = new LZCardPrintSchema( );

			schemaLZCardPrint.setPrtNo(szPrtNo);
			schemaLZCardPrint.setCertifyCode(szCertifyCode[0]);

			// 加入打印表信息
			setLZCardPrint.add(schemaLZCardPrint);

  		LZCardSchema schemaLZCard = new LZCardSchema( );

	    schemaLZCard.setCertifyCode(szCertifyCode[0]);
			schemaLZCard.setSubCode("0");
			schemaLZCard.setRiskCode("0");
			schemaLZCard.setRiskVersion("0");

	    schemaLZCard.setSendOutCom(szSendOutCom);
	    schemaLZCard.setReceiveCom(szReceiveCom);

			schemaLZCard.setSumCount(0);
			schemaLZCard.setPrem("");
	    schemaLZCard.setAmnt(szAmnt);
	    schemaLZCard.setHandler(szHandler);
	    schemaLZCard.setHandleDate(szHandleDate);
	    schemaLZCard.setInvaliDate(szInvaliDate);

			schemaLZCard.setTakeBackNo("");
			schemaLZCard.setSaleChnl("");
			schemaLZCard.setStateFlag("");
			schemaLZCard.setOperateFlag("");
			schemaLZCard.setPayFlag("");
			schemaLZCard.setEnterAccFlag("");
			schemaLZCard.setReason("");
			schemaLZCard.setState("");
			schemaLZCard.setOperator("");
			schemaLZCard.setMakeDate("");
			schemaLZCard.setMakeTime("");
			schemaLZCard.setModifyDate("");
			schemaLZCard.setModifyTime("");

			// 加入单证信息
	    setLZCard.add(schemaLZCard);

    } else {  // 如果是单证发放操作
        for( nIndex = 0; nIndex < szNo.length; nIndex ++ ) {
        
            LZCardSchema schemaLZCard = new LZCardSchema( );
            
            schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
            
            certifyCode=szCertifyCode[nIndex];
            
            schemaLZCard.setSubCode("");
            schemaLZCard.setRiskCode("");
            schemaLZCard.setRiskVersion("");
            
            schemaLZCard.setStartNo(szStartNo[nIndex].trim());
            schemaLZCard.setEndNo(szEndNo[nIndex].trim());
            
            if (szSendOutCom.substring(0,2).equals("86"))
            {
                schemaLZCard.setSendOutCom("A"+szSendOutCom);
            }
            else
            {
                schemaLZCard.setSendOutCom("B"+szSendOutCom);
            }
            
            if (szReceiveType.equals("COM"))
            {
                schemaLZCard.setReceiveCom("A"+szReceiveCom);
            }
            else if (szReceiveType.equals("USR"))
            {
            	schemaLZCard.setReceiveCom("B"+szReceiveCom);
            }
            else if (szReceiveType.equals("AGECOM"))
            {
            	schemaLZCard.setReceiveCom("E"+szReceiveCom);
            }
            else
            {
            	schemaLZCard.setReceiveCom("D"+szReceiveCom);
            }
            schemaLZCard.setSumCount(szSumCount[nIndex]);
            schemaLZCard.setPrem("");
            schemaLZCard.setAmnt(szAmnt);
            schemaLZCard.setHandler(szHandler);
            schemaLZCard.setHandleDate(szHandleDate);
            schemaLZCard.setInvaliDate(szInvaliDate);
            
            schemaLZCard.setTakeBackNo("");
            schemaLZCard.setSaleChnl("");
            schemaLZCard.setStateFlag("");
            schemaLZCard.setOperateFlag("");
            schemaLZCard.setPayFlag("");
            schemaLZCard.setEnterAccFlag("");
            schemaLZCard.setReason("");
            schemaLZCard.setState("");
            schemaLZCard.setOperator("");
            schemaLZCard.setMakeDate("");
            schemaLZCard.setMakeTime("");
            schemaLZCard.setModifyDate("");
            schemaLZCard.setModifyTime("");
            
            setLZCard.add(schemaLZCard);
        }
System.out.println("hihihihi 3: ");		  
    }

	  // 准备传输数据 VData
	  VData vData = new VData();

	  vData.addElement(globalInput);
	  vData.addElement(setLZCard);
	  vData.addElement(setLZCardPrint);
	  vData.addElement(szLimitFlag);

	  Hashtable hashParams = new Hashtable();
	  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
	  vData.addElement(hashParams);

		// 设置操作字符串
		String szOperator = "INSERT";

		if( szBatchMode != null ) {
			szOperator = "BATCH";  // 设置批量发放的操作字符串
		}

	  // 数据传输
	  CertSendOutUI tCertSendOutUI = new CertSendOutUI();

	  if (!tCertSendOutUI.submitData(vData, szOperator)) {
	    Content = " 保存失败，原因是: " + tCertSendOutUI.mErrors.getFirstError();
	    FlagStr = "Fail";

	    vData = tCertSendOutUI.getResult();

			strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
			session.setAttribute("TakeBackNo", strTakeBackNo);

	    setLZCard = (LZCardSet)vData.getObjectByObjectName("LZCardSet", 0);

			szFailSet = "parent.fraInterface.CertifyList.clearData();\r\n";
	    for(nIndex = 0; nIndex < setLZCard.size(); nIndex ++) {
	    	LZCardSchema tLZCardSchema = setLZCard.get(nIndex + 1);

	    	szFailSet += "parent.fraInterface.CertifyList.addOne();\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 1, '" + tLZCardSchema.getCertifyCode() + "');\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 3, '" + tLZCardSchema.getStartNo() + "');\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 4, '" + tLZCardSchema.getEndNo() + "');\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 5, '');\r\n";
	    }

	  } else {
	  	Content = " 保存成功 ";
	  	FlagStr = "Succ";

		  vData = tCertSendOutUI.getResult();
			strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
			session.setAttribute("TakeBackNo", strTakeBackNo);
			session.setAttribute("State", CertStatBL.PRT_STATE);

			if( szBatchMode != null ) {
				session.setAttribute("NoUseAgentTemplate", "YES");
			}
	  }

	} catch(Exception ex) 
	{
		ex.printStackTrace( );
   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   	FlagStr = "Fail";
	}
%>
<html>
<script language="javascript">
<%= szFailSet %>
parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%= strTakeBackNo %>", "<%=certifyCode%>");
</script>
<body>
</body>
</html>