<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%
    String Content = "";
    CErrors tError = null;
    String FlagStr = "Fail";
    
    //对应的操作是按照“代理人编码”进行修改的
    String certifyCode= request.getParameter("CertifyCode");
    String receive= request.getParameter("AgentGrade");
    String maxCount = request.getParameter("MaxCount");
    String verPeriod = request.getParameter("VerPeriod");
    
    //代理人最大领取表
    LDAgentCardCountSchema tLDAgentCardCountSchema = new LDAgentCardCountSchema();
    tLDAgentCardCountSchema.setCertifyCode(certifyCode);
    tLDAgentCardCountSchema.setMaxCount(maxCount);
    tLDAgentCardCountSchema.setVerPeriod(verPeriod);
    tLDAgentCardCountSchema.setManageCom("N");
    if("0".equals(receive))
    {
        tLDAgentCardCountSchema.setAgentCode("Y");
        tLDAgentCardCountSchema.setAgentCom("N");
        tLDAgentCardCountSchema.setAgentGrade("N");
    }
    if("1".equals(receive))
    {
        tLDAgentCardCountSchema.setAgentCode("N");
        tLDAgentCardCountSchema.setAgentCom("N");
        tLDAgentCardCountSchema.setAgentGrade("Y");
    }
    if("2".equals(receive))
    {
        tLDAgentCardCountSchema.setAgentCode("N");
        tLDAgentCardCountSchema.setAgentCom("Y");
        tLDAgentCardCountSchema.setAgentGrade("N");
    }
    if("3".equals(receive))
    {
        tLDAgentCardCountSchema.setManageCom("*");
        tLDAgentCardCountSchema.setAgentCode("*");
        tLDAgentCardCountSchema.setAgentCom("*");
        tLDAgentCardCountSchema.setAgentGrade("*");
    }
    
    CertifyMaxSaveBL mCertifyMaxSaveBL = new CertifyMaxSaveBL();
    VData tVData = new VData();
    tVData.addElement(tLDAgentCardCountSchema);
    
    try
    {
        if(!mCertifyMaxSaveBL.submitData(tVData, request.getParameter("Operate")))
        {
            tError = mCertifyMaxSaveBL.mErrors;
            Content = "操作失败，原因是:" + tError.getFirstError();
            FlagStr = "Fail";
        }
        else
        {
            Content = "操作成功！";
            FlagStr = "Succ";
        }
    }
    catch(Exception ex)
    {
        Content = "失败，原因是:" + ex.toString();
        FlagStr = "Fail";
        System.out.println(Content);
    }
%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>