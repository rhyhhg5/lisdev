<html>
<%
//name :OrderDescInput.jsp
//function :Manage LMCertifyDes
//Creator :
//date :2006-07-29
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "OrderDescInput.js"></SCRIPT>      
<%@include file="OrderDescInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./OrderDescSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  <Div id= "divLLReport1" style= "display: ''">
	<br>
   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		批次号
   			</TD>
   			<TD class= input>
   				<Input class= common name= SerialNo id="SerialNoId" Readonly > 
   			</TD>
   			<TD class= title>
          制定人
        </TD>
        <TD class= input>
          <Input class= common name= OrderCreator elementtype=nacessary>
        </TD>
        <td class="title">归属日期</td>
        <td class="input"><input class="coolDatePicker" name="AttachDate" verify="定单日期|DATE&NOTNULL" elementtype=nacessary></td>
      </TR> 
      
      <TR>
      	<TD class= title>
   		 		当前日期
   			</TD>
   			<TD class= input>
   				<input class="readonly" readonly name= "MakeDate" >
   			</TD>
   			<TD class= title>
        </TD>
        <TD class= input>
        </TD>
        <td class="title"></td>
        <td class="input"></td>
      </TR>
   	</Table>
   	
   	<br>
   	<input class="cssButton" type="button" value="单征类型查询" onclick="requestClick()" >
   	<br><br>
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyType);"></td>
    	<td class= titleImg>单证列表</td></tr>
    </table>
   	
   	<Div  id= "divCertifyType" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanCertifyTypeGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>
	
	<br>
	
	<table>
    <tr>
      <td class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSerialRemark);">
      </td>
  	<td class= titleImg>注释</td></tr>
  </table>
    
	<Div  id= "divSerialRemark" style= "display: ''">
	<TR  class= common>
		<TD  class= input colspan="6">
		    <textarea name="Note" cols="100%" rows="3"  class="common">
		    </textarea>
		</TD>
	</TR>
  </div> 	
  
  <input type="hidden" name="OperateType" >
  
  <input type="hidden" name="SerialNoBak" >
  <input type="hidden" name="AttachDateBak" >
   	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>