<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：CertifySearchInit.jsp
//程序功能：
//创建日期：2003-06-16
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
String strCurDate = PubFun.getCurrentDate();
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
  	fm.reset();
  	//获得当前用户的管理机构
		var comCode = '<%=tG1.ManageCom%>';
  	    
    //alert(comCode.length);
    if (comCode.length==2||comCode.length==4)
    {
    	fm.ReceiveCom.style.display="";
	  	fm.ReceiveName.style.display="";
	  	fm.AgentCode.style.display="none";
	  	fm.AgentQuery.style.display="none";
	  	fm.btnQueryCom.style.display="none";
	  	
	  	fm.AgentCode.value="";
	    if (comCode.length!=2)
	  	{
	  		fm.ReceiveType.value="USR";
	  	}
    } else {
    	fm.ReceiveCom.style.display="none";
	  	fm.ReceiveName.style.display="none";
	  	fm.GroupAgentCode.style.display="";
	  	fm.AgentQuery.style.display="";
	  	fm.btnQueryCom.style.display="";
	  	fm.ReceiveType.value="AGE";

	  	fm.ReceiveCom.value="";
	  	fm.ReceiveName.value="";
	  	
    }
  	
  }
  catch(ex)
  {
    alert("在CertifySearchInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在CertifySearchInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
		initCardInfo();
		initCardState();
		//initCardListInfo();
  }
  catch(re)
  {
    alert("CertifySearchInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 单证信息列表的初始化
function initCardInfo()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="单证类型";         		//列名
		iArray[1][1]="80px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="发放者";          		//列名
		iArray[2][1]="85px";            		//列宽
		iArray[2][2]=85;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="接收者";        		  //列名
		iArray[3][1]="85px";            		//列宽
		iArray[3][2]=85;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="起始号";          		//列名
		iArray[4][1]="120px";            		//列宽
		iArray[4][2]=100;            		 	  //列最大值
		iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="终止号";         		  //列名
		iArray[5][1]="120px";            		//列宽
		iArray[5][2]=100;            			  //列最大值
		iArray[5][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="数量";          		  //列名
		iArray[6][1]="60px";            		//列宽
		iArray[6][2]=60;            			  //列最大值
		iArray[6][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[7]=new Array();
		iArray[7][0]="单证状态";          		  //列名
		iArray[7][1]="60px";            		//列宽
		iArray[7][2]=60;            			  //列最大值
		iArray[7][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[8]=new Array();
		iArray[8][0]="操作员";         		  //列名
		iArray[8][1]="100px";            		//列宽
		iArray[8][2]=100;            			  //列最大值
		iArray[8][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[9]=new Array();
		iArray[9][0]="入机日期";       		  //列名
		iArray[9][1]="100px";            		//列宽
		iArray[9][2]=100;            			  //列最大值
		iArray[9][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[10]=new Array();
		iArray[10][0]="入机时间";       		  //列名
		iArray[10][1]="100px";            		//列宽
		iArray[10][2]=100;            			  //列最大值
		iArray[10][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[11]=new Array();
		iArray[11][0]="管理机构";       		  //列名
		iArray[11][1]="100px";            		//列宽
		iArray[11][2]=100;            			  //列最大值
		iArray[11][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[12]=new Array();
		iArray[12][0]="合同号码";       		  //列名
		iArray[12][1]="100px";            		//列宽
		iArray[12][2]=100;            			  //列最大值
		iArray[12][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		CardInfo = new MulLineEnter("fm" , "CardInfo");
		// 这些属性必须在loadMulLine前
		// CardInfo.mulLineCount = 2;
		CardInfo.displayTitle = 1;
		CardInfo.locked = 1;
		//CardInfo.canSel = 1;
      CardInfo.hiddenPlus = 1;
      CardInfo.hiddenSubtraction = 1;
		//CardInfo.selBoxEventFuncName = "boxEventHandler";
		CardInfo.loadMulLine(iArray);
		//这些操作必须在loadMulLine后面
		//CardInfo.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
		alert(ex);
	}
}

// 打印队列的初始化
function initCardListInfo()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="单证类型";         		//列名
		iArray[1][1]="80px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="发放者";          		//列名
		iArray[2][1]="85px";            		//列宽
		iArray[2][2]=85;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="接收者";        		  //列名
		iArray[3][1]="85px";            		//列宽
		iArray[3][2]=85;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="起始号";          		//列名
		iArray[4][1]="120px";            		//列宽
		iArray[4][2]=100;            		 	  //列最大值
		iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="终止号";         		  //列名
		iArray[5][1]="120px";            		//列宽
		iArray[5][2]=100;            			  //列最大值
		iArray[5][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="数量";          		  //列名
		iArray[6][1]="60px";            		//列宽
		iArray[6][2]=60;            			  //列最大值
		iArray[6][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[7]=new Array();
		iArray[7][0]="操作员";         		  //列名
		iArray[7][1]="100px";            		//列宽
		iArray[7][2]=100;            			  //列最大值
		iArray[7][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[8]=new Array();
		iArray[8][0]="入机日期";       		  //列名
		iArray[8][1]="100px";            		//列宽
		iArray[8][2]=100;            			  //列最大值
		iArray[8][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[9]=new Array();
		iArray[9][0]="入机时间";       		  //列名
		iArray[9][1]="100px";            		//列宽
		iArray[9][2]=100;            			  //列最大值
		iArray[9][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		CardListInfo = new MulLineEnter("fm_print" , "CardListInfo");
		CardListInfo.displayTitle = 1;
		CardListInfo.hiddenPlus = 1;
		CardListInfo.canSel = 1;
      CardListInfo.hiddenPlus = 1;
      CardListInfo.hiddenSubtraction = 1;
		CardListInfo.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}

function initCardState()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="单证类型";         		//列名
		iArray[1][1]="60px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="单证状态";          		  //列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=60;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许	

		iArray[3]=new Array();
		iArray[3][0]="数量";          		  //列名
		iArray[3][1]="60px";            		//列宽
		iArray[3][2]=60;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		CardState = new MulLineEnter("fm" , "CardState");
		// 这些属性必须在loadMulLine前
		// CardInfo.mulLineCount = 2;
		CardState.displayTitle = 1;
		CardState.locked = 1;
    CardState.hiddenPlus = 1;
    CardState.hiddenSubtraction = 1;
		CardState.loadMulLine(iArray);
		//这些操作必须在loadMulLine后面
		//CardInfo.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>