<html>
<%
//name :CertifyDescInput.jsp
//function :Manage LMCertifyDes
//Creator :刘岩松
//date :2003-05-16
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<SCRIPT src = "CertifyDescInput.js"></SCRIPT>      
<%@include file="CertifyDescInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./CertifyDescSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  <Div id= "divLLReport1" style= "display: ''">

   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		单证编码
   			</TD>
   			<TD class= input>
   				<Input class= common name= CertifyCode id="CertifyCodeId" elementtype=nacessary>
   			</TD>
   			<TD class= title>
          单证名称
        </TD>
        <TD class= input>
          <Input class= common name= CertifyName elementtype=nacessary>
        </TD>
   		</TR>

      <TR class= common>
         <TD class= title>
           单证类型
         </TD>
         <TD class= input>
           <Input class= codeno name= CertifyClass  CodeData="0|^P|普通单证^D|定额单证"  ondblClick="showCodeListEx('CertifyClassListNew',[this,CertifyClassName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('CertifyClassListNew',[this,CertifyClassName],[0,1],null,null,null,1);" verify="单证类型|NOTNULL"><Input class= codename name= 'CertifyClassName' ReadOnly elementtype=nacessary>
         </TD>
         
         <TD class= title>
          业务员校验标志
        </TD>
        <TD class= input>
          <Input class="codeno" readOnly name= "VerifyFlag"  
          CodeData="0|^Y|校验^N|不校验" 
          ondblClick="showCodeListEx('CertifyVerifyFlag',[this,VerifyFlagName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CertifyVerifyFlag',[this,VerifyFlagName],[0,1],null,null,null,1);"><Input class= codename name= 'VerifyFlagName'  verify="业务员校验标志|NOTNULL" elementtype=nacessary>
   			</TD>
       </TR>

       <TR class= common>
         <TD class= title>
           单证内部编码
         </TD>
         <TD class= input>
           <Input class="common" readOnly name= InnerCertifyCode >
         </TD>

        <TD class= title>
          单证号码长度
        </TD>
        <TD class= input>
          <Input class= common name=CertifyLength elementtype=nacessary>
         </TD>
       </TR>

       <TR class= common>
         <TD class= title>
           险种编码
         </TD>
         <TD class= input>
           <Input class="common" readonly name= RiskCode >
         </TD>

        <TD class= title>
          状态
        </TD>
        <TD class= input>
          <Input class= codeno name= State  ReadOnly
             CodeData="0|^0|有效^1|无效"	
             ondblClick="showCodeListEx('CertifyStateList',[this,StateName],[0,1],null,null,null,1);"
             onkeyup="showCodeListKeyEx('CertifyStateList',[this,StateName],[0,1],null,null,null,1);"><Input name=StateName class=codename verify="状态|NOTNULL" elementtype=nacessary >
         </TD>
       </TR>

       <TR class= common>
         <TD class= title>
           重要级别
         </TD>
         <TD class= input>
           <Input class="common" readOnly name= ImportantLevel >
         </TD>

        <TD class= title>
          是否是有号单证
        </TD>
        <TD class= input>
          <Input class= codeno name=HaveNumber 
             CodeData="0|^Y|有号单证^N|无号单证"  ReadOnly
             ondblClick="showCodeListEx('CertifyHaveNumber',[this,HaveNumberName],[0,1],null,null,null,1);"
             onkeyup="showCodeListKeyEx('CertifyHaveNumber',[this,HaveNumberName],[0,1],null,null,null,1);"><Input name=HaveNumberName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary>
         </TD>
       </TR>
<!--
       <TR class= common>
         <TD class= title>
           总保费
         </TD>
         <TD class= input>
           <Input class="common" readOnly name= Prem >
         </TD>

        <TD class= title>
          总保额
        </TD>
        <TD class= input>
          <Input class="common" readOnly name= Amnt >
         </TD>
       </TR>
-->
       <TR class= common>
        <TD class= title>
          单证业务类型
        </TD>
        <TD class= input>
    		<Input class=codeno name="CertifyClass2"  ReadOnly
    		ondblclick="return showCodeList('certifyclass2', [this,CertifyClass2Name],[0,1],null,null,null,1);" 
    		onkeyup="return showCodeListKey('certifyclass2', [this,CertifyClass2Name],[0,1],null,null,null,1);"><Input name=CertifyClass2Name class=codename>
				</TD>
					
				<td class="title">单证单位</td>
        <td class="input"><input class="common" name="Unit"></td>
         <Input class= common type= hidden name= OperateType >
         <Input class= common type= hidden name= CertifyCode_1 >
      </TR>
    </Table>

    <Table class= common>
      <TR>
      	<TD class= title>
      		是否选定具体管理机构
        </TD>
        <TD class= input>
        	<input type="radio" name="StateRadio"  value="1" checked onclick="showCom()"> 是 &nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="StateRadio"  value="0" onclick="showCom()"> 否
				</TD>
      	<TD class= title>
      		<div id="divComName" style="display:''">
          	管理机构
          </div>
        </TD>
        <TD class= input>
        	<div id="divComCode" style="display:''">
	          <Input class="codeno" name= ManageCom  ReadOnly style="display:''"
	            ondblclick="return showCodeList('Station', [this,ManageComName],[0,1],null,null,null,1);"
	            onkeyup="return showCodeListKey('Station', [this,ManageComName],[0,1],null,null,null,1);"
	            ><Input name=ManageComName class= codename verify="管理机构|NOTNULL" elementtype=nacessary>
	        </div>
        </TD>
    </TR>
    <TR class= common>         
        <TD class= title>
          保险期限
        </TD>
        <TD class= input>
          <Input class= common name=DutyPer elementtype=nacessary>
         </TD>
         <TD class= title>
           单位
         </TD>
         <TD class= input>
         <Input class= codeno name=DutyUinit 
             CodeData="0|^Y|年^M|月^D|日"  ReadOnly
             ondblClick="showCodeListEx('DutyUinit',[this,DutyUinitName],[0,1],null,null,null,1);"
             onkeyup="showCodeListKeyEx('DutyUinit',[this,DutyUinitName],[0,1],null,null,null,1);"><Input name=DutyUinitName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary>
    
         </TD>         
       </TR>   
       <TR>
      	<TD class= title>
          备注
        </TD>
        <TD class= input>
	        <Input class=common name=Note type="text">
				</TD>
        <TD class= title>校验规则</TD>
        <TD class= input><Input class= codeno name=ChkRule 
             CodeData="0|^1|规则1^2|规则2"  ReadOnly
             ondblClick="showCodeListEx('ChkRule',[this,ChkRuleName],[0,1],null,null,null,1);"
             onkeyup="showCodeListKeyEx('ChkRule',[this,ChkRuleName],[0,1],null,null,null,1);"><Input name=ChkRuleName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary></TD>
    </TR>
    <TR >      	
        <TD class= title>单证类型码：</TD>        
          <td class="input"><input  class=common name="SubCode" maxlength=2 verify="单证类型|NUM&NOTNULL"  elementtype=nacessary" ></td>
        <TD class= title>          
        </TD>
        <TD class= input>	     
				</TD>
    </TR>   
 		</table>
 		
    <Div  id= "divShow" style= "display: 'none'">
    <table>
      <tr>
      <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardRisk);">
    		</td>
        <td class= titleImg>
           定额单险种信息
        </td>
      </tr>
    </table>
    </Div>

<Input type='hidden' name= Prem >
<Input type='hidden' name= Amnt >

     <Div  id= "divCardRisk" style= "display: 'none'">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          <span id="spanCardRiskGrid" >
          </span>
        </td>
      </tr>
    </table>
	</div>

</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>