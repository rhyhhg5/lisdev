<%
//程序名称：LZCardBugetQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-04-05 16:08:27
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CertifyCode').value = "";     
    fm.all('ManageCom').value = "";
    fm.all('SDate').value = "";
    fm.all('EDate').value = "";
    fm.all('Buget').value = "";
    fm.all('MakeDate').value = "";
    //fm.all('MakeTime').value = "";
    //fm.all('ModifyDate').value = "";
    //fm.all('ModifyTime').value = "";
    
  }
  catch(ex) {
    alert("在LZCardBugetQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLZCardBugetGrid();  
  }
  catch(re) {
    alert("LZCardBugetQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LZCardBugetGrid;
function initLZCardBugetGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][2]=10;         		    //列名
    iArray[0][3]=0;         		    //列名
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构";            //列名
    iArray[1][1]="150px";         		//列名
    iArray[1][2]=10;         		    //列名
    iArray[1][3]=2;         		    //列名
    iArray[1][4]="station";         	//列名
 
    iArray[2]=new Array();
    iArray[2][0]="预算开始日期";         		//列名
    iArray[2][1]="180px";         		//列名
    iArray[2][2]=10;         		    //列名
    iArray[2][3]=0;         		    //列名
    
    iArray[3]=new Array();
    iArray[3][0]="预算结束日期";         		//列名
    iArray[3][1]="180px";         		//列名
    iArray[3][2]=10;         		    //列名
    iArray[3][3]=0;         		    //列名
    
    iArray[4]=new Array();
    iArray[4][0]="预算费用";         		//列名
    iArray[4][1]="200px";         		//列名
    iArray[4][2]=10;         		    //列名
    iArray[4][3]=0;         		    //列名
    
    iArray[5]=new Array();
    iArray[5][0]="入机日期";         		//列名
    iArray[5][1]="0px";         		//列名
 
    iArray[6]=new Array();
    iArray[6][0]="入机时间";         		//列名
    iArray[6][1]="0px";         		//列名
    
    
    iArray[7]=new Array();
    iArray[7][0]="修改日期";         		//列名
    iArray[7][1]="0px";         		//列名
 
    iArray[8]=new Array();
    iArray[8][0]="修改时间";         		//列名
    iArray[8][1]="0px";         		//列名
    
    
    iArray[9]=new Array();                        
    iArray[9][0]="单证编码";         		//列名 
    iArray[9][1]="0px";         		//列名     
    
    LZCardBugetGrid = new MulLineEnter( "fm" , "LZCardBugetGrid" ); 
    //这些属性必须在loadMulLine前

    LZCardBugetGrid.mulLineCount = 10;   
    LZCardBugetGrid.displayTitle = 1;
    LZCardBugetGrid.locked = 1;
    LZCardBugetGrid.canSel = 1;
    LZCardBugetGrid.selBoxEventFuncName = "showOne";
    LZCardBugetGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LZCardBugetGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
