//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  fm.OperateType.value = "QUERY";
  var i = 0;
  initCertifyDescGrid();
  var strSQL ="";
  var strcon = "";
  if (fm.Prem.value !=null && fm.Prem.value !=""){
  	strcon = " where  l=" + parseFloat(fm.Prem.value);
  	if (fm.Amnt.value !=null && fm.Amnt.value !=""){
  	    strcon = strcon + " and  h=" + parseFloat(fm.Amnt.value);
    }
  }else
  {
  	if (fm.Amnt.value !=null && fm.Amnt.value !=""){
  	    strcon = " where  h=" + parseFloat(fm.Amnt.value);
    }
  }
  var strLen = "";
  if (fm.CertifyLength.value !=null && fm.CertifyLength.value !=""){
  	    strLen = " and  a.CertifyLength=" + parseInt(fm.CertifyLength.value);
   }
  
  strSQL = "select CertifyCode,CertifyName,riskcode, CertifyClass, ImportantLevel,l,h from (select a.CertifyCode ,a.CertifyName,b.riskcode" 
   +",a.CertifyClass,a.ImportantLevel,sum(b.Prem)l,sum(b.PremLot)h"
   + " from LMCertifyDes a,LMCardRisk b"
   + " where CertifyClass <>'S' and CertifyClass='D' "
   + " and a.CertifyCode=b.CertifyCode"
   + getWherePart( 'a.CertifyCode','CertifyCode')
   + getWherePart( 'a.SubCode','SubCode')
   + getWherePart( 'a.CertifyClass','CertifyClass')
   + getWherePart( 'a.CertifyName','CertifyName')
   + getWherePart( 'a.InnerCertifyCode','InnerCertifyCode')   
   + strLen
   + getWherePart( 'b.RiskCode','RiskCode')
   + getWherePart( 'a.State','State')
   + getWherePart( 'a.ImportantLevel','ImportantLevel')
   + getWherePart( 'a.HaveNumber','HaveNumber')
   + getWherePart( 'a.ManageCom','ManageCom')   
   + " group by a.CertifyCode ,a.CertifyName,b.riskcode,a.CertifyClass,a.ImportantLevel"
   + " union all " 
   + " select a.CertifyCode ,a.CertifyName,'' riskcode " 
   +",a.CertifyClass,a.ImportantLevel,0 l,0 h"
   + " from LMCertifyDes a"
   + " where CertifyClass <>'S' and CertifyClass='D' "  
   + getWherePart( 'a.CertifyCode','CertifyCode')
   + getWherePart( 'a.SubCode','SubCode')
   + getWherePart( 'a.CertifyClass','CertifyClass')
   + getWherePart( 'a.CertifyName','CertifyName')
   + getWherePart( 'a.InnerCertifyCode','InnerCertifyCode')   
   + strLen  
   + getWherePart( 'a.State','State')
   + getWherePart( 'a.ImportantLevel','ImportantLevel')
   + getWherePart( 'a.HaveNumber','HaveNumber')
   + getWherePart( 'a.ManageCom','ManageCom') 
   + " and not exists (select * from LMCardRisk where LMCardRisk.CertifyCode = a.CertifyCode )"
   + " union all "  
   + "select a.CertifyCode ,a.CertifyName,a.riskcode" 
   +",a.CertifyClass,a.ImportantLevel,0 l,0 h"
   + " from LMCertifyDes a"
   + " where CertifyClass <>'S' and CertifyClass='P'"
   + getWherePart( 'a.CertifyCode','CertifyCode')
   + getWherePart( 'a.SubCode','SubCode')
   + getWherePart( 'a.CertifyClass','CertifyClass')
   + getWherePart( 'a.CertifyName','CertifyName')
   + getWherePart( 'a.InnerCertifyCode','InnerCertifyCode')   
   + strLen
   + getWherePart( 'a.RiskCode','RiskCode')
   + getWherePart( 'a.State','State')
   + getWherePart( 'a.ImportantLevel','ImportantLevel')
   + getWherePart( 'a.HaveNumber','HaveNumber')
   + getWherePart( 'a.ImportantLevel','ImportantLevel')
   //+ getWherePart( 'char(a.Prem)','Prem')
   //+ getWherePart( 'a.Amnt','Amnt')
   + getWherePart( 'a.ManageCom','ManageCom')   
   + " group by a.CertifyCode ,a.CertifyName,a.riskcode,a.CertifyClass,a.ImportantLevel) picc" 
   + strcon 
   ;   
  turnPage.queryModal(strSQL, CertifyDescGrid);   

}

function displayQueryResult(strResult) {
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  strResult = Conversion(strResult);
  var filterArray = new Array(0,4,2,7,9,5,6);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.pageDisplayGrid = CertifyDescGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }

  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }

}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

function ReturnData()
{
    //首先判断单证的类型是什么；
    //fm.OperateType.value = "RETURNDATA";
    var tRow=CertifyDescGrid.getSelNo();

    if (tRow==0)
    {
        alert("请您先进行选择!");
        return;
    }
    var tCol=1;
    var tCertifyCode = CertifyDescGrid.getRowColData(tRow-1,tCol);
    //var tCertifyClass = CertifyDescGrid.getRowColData(tRow-1,4);
    //if(tCertifyClass=="D")
    //{
    //    top.opener.fm.all('divShow').style.display="";
    //    top.opener.fm.all('divCardRisk').style.display="";
    //}
    //top.location.href="./CertifyDescQueryDetail.jsp?CertifyCode="+tCertifyCode+"&CertifyClass="+tCertifyClass;
    top.opener.afterQueryCertifyInfo(tCertifyCode);
    top.close(); 
}
