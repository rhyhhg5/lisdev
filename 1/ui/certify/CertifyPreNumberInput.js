//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm(){	
	
  var strRet ="";
    
  if (!chkSelect()){return false;}
  
  if(!chkMustInput()){  
     return false;
  }
  strRet =chkConfirm();
  if(strRet!=""){  	
  	 alert(strRet);
     return false;
  }
 
  strRet =chkExistPrt();
  if(strRet !="" && !confirm(strRet+"，您是否要重新生成单证号码?") ){ 	  	
     return false;
  }
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.Operate.value = "new";    
	fm.action = "CertifyNumberSave.jsp";
  fm.submit(); //提交  

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  	easyQueryClick();
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
 
  }
}

//查询按钮对应操作
function easyQueryClick(){
	 
  var strRet =""; 
  initCertifyList();
  if(!chkMustInput()){  	
     return false;
  } 
  var sql = "select prtno,min(CARDSERNO),max(CARDSERNO),count(*) from lzcardnumber where  CardType='" + fm.all('CardType').value 
  + "' and PrintFlag='0'" 
  + getWherePart("PrtNo","PrtNo1")
  + " group by prtno "
  ;   
  turnPage.queryModal(sql,CertifyList);
}
//删除按钮对应操作
function deleteClike(){
	
  
  var strRet ="";   
  if (!chkSelect()){return false;} 
 
  if(!chkMustInput()){  	
     return false;
  }  
 
  strRet = chkConfirm(); 
  	  
  if(strRet !="" && !confirm(strRet+"，您是否要删除？") ){  	
     return false;
  }
  if(confirm("您是否要删除？")){
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	  
	  fm.Operate.value = "delete";    
		fm.action = "CertifyNumberSave.jsp";
	  fm.submit(); //提 
 }else{ return false;}
  
}
//确定按钮对应操作
function confirmClike(){
	
  var strRet ="";
  if (!chkSelect()){return false;} 
  if(!chkMustInput()){  
     return false;
  }  
  
  strRet = chkConfirm();
  if(strRet !=""){  	 
  	 alert(strRet);
     return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.Operate.value = "confirm";    
	fm.action = "CertifyNumberSave.jsp";
  fm.submit(); //提交
}
//下载清单
function printData(){
	if (!chkSelect()){return false;}
  if(!chkMustInput()){  
     return false;
  } 
  var strRet = chkConfirm();
  if(strRet !=""){  	 
  	 alert(strRet);
     return false;
  }
  
	window.open("../certify/CertifyNumberPrint.jsp?CardType="+fm.all('CardType').value + "&PrtNo="
	+ fm.all('PrtNo').value ); 
}
//单选框必选提醒
function chkSelect(){
 
  if(CertifyList.mulLineCount == 0){
  	alert("请选择一条记录！");
  	return false;
  } 
  var selNo =  CertifyList.getSelNo();  
  if (selNo == 0){
  	alert("请选择一条记录！");
  	return false;
  }
  return true;
}
//单选框必选提醒
function getPrtNo(){
 var selNo =  CertifyList.getSelNo();
 fm.all('PrtNo').value = CertifyList.getRowColData(selNo - 1 ,1);
}

