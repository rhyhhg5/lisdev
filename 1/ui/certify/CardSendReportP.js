var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
 }
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{  
	// 初始化表格
	initCertifyMaxGrid();
	var str="";
	var strt="";
	var startNo = fm.StartNo.value;
	var endNo = fm.EndNo.value;
	var tendno="";
	var noSQL="";
	var snoSQL="";
	var enoSQL="";
	var nocount=0;
	var arrResult=null;
	var arreResult=null;
	var nums=0;
	var nume=0;
	var strSQL="";
	if(fm.ManageComC.value!=""){
			str=" and ((ReceiveCom like 'A%' and ReceiveCom like '"+"A"+fm.ManageComC.value+"%' ) " +
				" or  (ReceiveCom like 'B%' and exists (select 1 from lduser where usercode=substr(char(ReceiveCom),2) " +
				" and comcode like  '"+fm.ManageComC.value+"%')) or (ReceiveCom not like 'A%' and ReceiveCom not like 'B%'" +
				" and exists (select 1 from lduser where usercode=substr(char(sendoutcom),2) " +
				" and comcode like  '"+fm.ManageComC.value+"%') )) ";
			
	}
	strt=" and exists (select 1 from LZCardTrack c where b.CertifyCode = c.CertifyCode  " +
	"and b.StartNo >= c.StartNo " +
	"and b.EndNo <= c.EndNo  " +
	str
	+"and (c.SendOutCom LIKE 'A"+managecom+"%' or c.ReceiveCom LIKE 'A"+managecom+"%'))";
	//起始終止號都錄入的情況
	if(startNo!=""&&endNo!="")	{
		
		if(parseInt(endNo,10)<parseInt(startNo,10)){
			alert("起始号不能大于终止号");
			return false ;
		}
			var strsSQL = "SELECT endno,stateflag,startno FROM LZCard WHERE 1=1 "
						+ getWherePart('CertifyCode','CertifyCode')
						+ getWherePart('StartNo','StartNo','<=')
						+ getWherePart('EndNo','StartNo','>=')
						
						+" with ur";
					
			
			arrResult = easyExecSql(strsSQL);
			if(arrResult != null)
			{
				tendno = arrResult[0][0];
				//起始終止號在同一區間的時候
				if(tendno>=endNo){
					noSQL=" and cast(Startno as integer)<=cast('"+startNo+"' as integer) and cast(Endno as integer)>=cast('"+startNo+"' as integer) ";
					nocount=parseInt(endNo,10)-parseInt(startNo,10)+1;
					nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10)+parseInt(tendno,10)-parseInt(endNo,10);
					if(nums<0){
						alert("起始或终止号录入错误，可能超出最大单证号，请检验");
						return false ;
					}
					strSQL="select t.CertifyCode,t.CertifyName,sum(t.stateflag1),sum(t.stateflag2)," +
							"sum(t.stateflag3),sum(t.stateflag4),sum(t.stateflag5),t.managecom from " 
							+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",noSQL,str,strt)
							+"  ) " +
							"as t group by t.CertifyCode,t.CertifyName,t.managecom  with ur ";
					}
			
				//起始終止號不在同一區間的時候
				else{
					var startSQL="";
					var endSQL="";
					var efsql="";
					
					var streSQL = "SELECT startno,stateflag,endno FROM LZCard WHERE 1=1 "
						   + getWherePart('CertifyCode','CertifyCode')
							+ getWherePart('StartNo','EndNo','<=')
							+ getWherePart('EndNo','EndNo','>=')
							
							+" with ur"
							;
					arreResult = easyExecSql(streSQL);
					//起始号段所在区间
					snoSQL=" and cast('"+startNo+"' as integer) between cast(Startno as integer) and cast(Endno as integer) ";
					nocount=parseInt(tendno,10)-parseInt(startNo,10)+1;
					nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10);
					if(nums<0){
						alert("起始或终止号录入错误，可能超出最大单证号，请检验");
						return false ;
					}
					if(arreResult!=null){
						var estartno=arreResult[0][0];
						//终止号段所在区间
						enoSQL=" and cast('"+endNo+"' as integer) between cast(Startno as integer) and cast(Endno as integer) ";
						nocount=parseInt(endNo,10)-parseInt(estartno,10)+1;
						nume=parseInt(arreResult[0][2],10)-parseInt(endNo,10);
						if(nume<0){
							alert("起始或终止号录入错误，可能超出最大单证号，请检验");
							return false ;
						}
						
					}
					else{
						alert("未查询到该终止号的单证信息");
						return false;
					}
					noSQL=" and  cast(Startno as integer)>cast('"+startNo+"' as integer) and cast(Endno as integer)<cast('"+endNo+"' as integer) ";
					strSQL="select t.CertifyCode,t.CertifyName,sum(t.stateflag1),sum(t.stateflag2)," +
							"sum(t.stateflag3),sum(t.stateflag4),sum(t.stateflag5),t.managecom from " 
							+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",snoSQL,str,strt)
							+" union all "+getSumSQL(" sum(sumcount) as sum ",noSQL,str,strt)
							+" union all "
							+getSumSQL(" sum(sumcount)-"+nume+" as sum ",enoSQL,str,strt)+" ) " +
							"as t group by t.CertifyCode,t.CertifyName,t.managecom with ur ";
				}
					
			}
			else{
				alert("未查询到该起始号的单证信息");
				return false;
			}
	}
	
	
	//只有起始號沒有錄終止號的時候
	else if(startNo!=""&&endNo=="")	{

			var strsSQL = "SELECT endno,stateflag,startno FROM LZCard WHERE 1=1 "
							+ getWherePart('CertifyCode','CertifyCode')
							+ getWherePart('StartNo','StartNo','<=')
							+ getWherePart('EndNo','StartNo','>=')
							+" with ur"
							;
		
		   arrResult = easyExecSql(strsSQL);
		   if(arrResult != null)
		   {
			   tendno = arrResult[0][0];
			   var startSQL="";
			   var endSQL="";
			   var efsql="";
			   //起始号段所在区间
			   snoSQL=" and cast(Startno as integer)<=cast('"+startNo+"' as integer) and cast(Endno as integer)=cast('"+tendno+"' as integer) ";
			   nocount=parseInt(tendno,10)-parseInt(startNo,10)+1;
			   nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10);
			   if(nums<0){
					alert("起始或终止号录入错误，可能超出最大单证号，请检验");
					return false ;
				}
			   noSQL=" and  Startno>'"+startNo+"' ";
			   strSQL="select t.CertifyCode,t.CertifyName,sum(t.stateflag1),sum(t.stateflag2)," +
				"sum(t.stateflag3),sum(t.stateflag4),sum(t.stateflag5),t.managecom from " 
				+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",snoSQL,str,strt)
				+" union all "+getSumSQL(" sum(sumcount) as sum ",noSQL,str,strt)
				+"  ) "
				+"as t group by t.CertifyCode,t.CertifyName,t.managecom with ur ";
				
			
		   }
		   else{
				alert("未查询到该起始号的单证信息");
				return false;
			}
	
	}
		
	
	//只有終止號沒有起始號的情況
	else if(startNo==""&&endNo!="")	{


		var streSQL = "SELECT startno,stateflag,endno FROM LZCard WHERE 1=1 "
						+ getWherePart('CertifyCode','CertifyCode')
						+ getWherePart('StartNo','EndNo','<=')
						+ getWherePart('EndNo','EndNo','>=')
						+" with ur"
						;
		arreResult = easyExecSql(streSQL);
		
		if(arreResult!=null){
			var estartno=arreResult[0][0];
			//终止号段所在区间
			enoSQL=" and cast('"+endNo+"' as integer) between cast(STARTNO as integer) and cast(ENDNO as integer) ";
			nocount=parseInt(endNo,10)-parseInt(estartno,10)+1;
			nume=parseInt(arreResult[0][2],10)-parseInt(endNo,10);
			if(nume<0){
				alert("起始或终止号录入错误，可能超出最大单证号，请检验");
				return false ;
			}
			
		}
		else{
			alert("未查询到该终止号的单证信息");
			return false;
		}
		noSQL=" and   cast(Endno as integer)<cast('"+endNo+"' as integer) ";
		
		strSQL="select t.CertifyCode,t.CertifyName,sum(t.stateflag1),sum(t.stateflag2)," +
		"sum(t.stateflag3),sum(t.stateflag4),sum(t.stateflag5),t.managecom from " 
		+"("+getSumSQL(" sum(sumcount) as sum ",noSQL,str,strt)
		+" union all "
		+getSumSQL(" sum(sumcount)-"+nume+" as sum ",enoSQL,str,strt)+" ) " +
		"as t group by t.CertifyCode,t.CertifyName,t.managecom with ur ";
				
			
		  
	}
		
	
	//nothing to 錄的情況
	else {
		sumcount=" sum(sumcount) ";
		strSQL=getSumSQL(sumcount,noSQL,str,strt)+" with ur ";
	}
	arrResult=null;
	arreResult=null;
   fm.PubSQL.value = strSQL;
   turnPage.queryModal(strSQL, CertifyMaxGrid); 
	
 
}


function getSumSQL(sumcount,noSQL,str,strt){
	
		var strSQL = "";
		var selCnt = "(select " +
					sumcount+" from LZCard c where  c.CertifyCode=b.CertifyCode " 
					+ getWherePart( 'HandleDate','StartDate','>=' )
					+ getWherePart( 'HandleDate', 'EndDate','<=')
					+ getWherePart('CertifyCode','CertifyCode')
					+ noSQL
					+str
					+" and exists (select 1 from LZCardTrack d where c.CertifyCode = d.CertifyCode  " +
					"and c.StartNo >= d.StartNo " +
					"and c.EndNo <= d.EndNo  " +
					str
					+"and (d.SendOutCom LIKE 'A"+managecom+"%' or d.ReceiveCom LIKE 'A"+managecom+"%'))";
					;

		var sqlmanage="  (case when (select (select comcode from lduser where usercode=substr(char(ReceiveCom),2)) " +
						"from LZCardTrack c where c.CertifyCode = b.CertifyCode   " 
						+ getWherePart( 'HandleDate','StartDate','>=' )
						+ getWherePart( 'HandleDate', 'EndDate','<=')
						+ getWherePart('CertifyCode','CertifyCode')
						+ getWherePart('StartNo','StartNo','<=')
						+ getWherePart('EndNo', 'EndNo', '>=')
						+" and c.ReceiveCom like 'B%'  " 
						+"and c.ReceiveCom <> 'A86' " 
						+str
						+" and c.sendoutcom like 'A%' " 
						+" order by  makedate desc fetch first 1 rows only) is null  " 
						+" then (select substr(char(ReceiveCom),2) " 
						+" from LZCardTrack c where c.CertifyCode = b.CertifyCode " 
						+ getWherePart( 'HandleDate','StartDate','>=' )
						+ getWherePart( 'HandleDate', 'EndDate','<=')
						+ getWherePart('CertifyCode','CertifyCode')
						+ getWherePart('StartNo','StartNo','<=')
						+ getWherePart('EndNo', 'EndNo', '>=')
						+" and c.ReceiveCom like 'A%'  " 
						+"and c.ReceiveCom <> 'A86' " 
						+str
						+" order by  makedate desc fetch first 1 rows only) " 
						+" else (select (select comcode from lduser " 
						+"where usercode=substr(char(ReceiveCom),2)) " 
						+" from LZCardTrack c where c.CertifyCode = b.CertifyCode " 
						+ getWherePart( 'HandleDate','StartDate','>=' )
						+ getWherePart( 'HandleDate', 'EndDate','<=')
						+ getWherePart('CertifyCode','CertifyCode')
						+ getWherePart('StartNo','StartNo','<=')
						+ getWherePart('EndNo', 'EndNo', '>=')
						+" and c.ReceiveCom like 'B%'  " 
						+"and c.ReceiveCom <> 'A86' " 
						+" and c.sendoutcom like 'A%' " 
						+str
						+" order by  makedate desc fetch first 1 rows only) end) as managecom  "
						;
		strSQL = "select distinct b.CertifyCode,a.CertifyName,"
					+ selCnt+"  and c.stateflag in ('7','8') ) as stateflag1,"
					+ selCnt+"  and c.stateflag='5' ) as stateflag2,"
					+ selCnt+"  and c.stateflag='3' ) as stateflag3,"
					+ selCnt+"  and c.stateflag='2' ) as stateflag4,"
					+ selCnt+"  and c.stateflag='4' ) as stateflag5," 
					+sqlmanage    
					+ " from LMCertifyDes a,LZCard b"	   
					+ " where  a.CertifyCode=b.CertifyCode"
 					+ getWherePart('b.CertifyCode','CertifyCode')
					+ getWherePart( 'b.HandleDate','StartDate','>=' )
					+ getWherePart( 'b.HandleDate', 'EndDate','<=')
					+ noSQL
					+str
					+ " and b.stateflag in ('2','3','4','5','6','7','8','9') and a.CertifyClass='P'"  
					+strt
					+ " group by a.CertifyName,b.CertifyCode  "
  
					;	  	
		return strSQL;
}


//更新数据的函数
function printInfo()
{
  var i = 0;
  if(CertifyMaxGrid.mulLineCount==0){
  	alert("列表缺少数据,请先查询!!!");
  	return ;
  }

  if (fm.PubSQL.value== "" || fm.PubSQL.value == null){
     alert("请先查询数据!!!");
     return false;
  }  
  fm.ReportType.value="1";
    	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "CardSendReportPrint";
  fm.action = "../f1print/CardSendReportPrint.jsp";
  fm.submit();
  showInfo.close();
}

function showalert(){
	alert("没有符合条件的数据！");
}
