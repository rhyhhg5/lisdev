<%
//程序名称：SysCertTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-25
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%!
String buildMsg(boolean bFlag, String strMsg) {
	String strReturn = "";
	
	strReturn += "<html><script language=\"javascript\">";
	
	if( bFlag ) {
		strReturn += "  parent.fraInterface.afterSubmit('Succ', '操作成功完成');";
	} else {
		strReturn += "  parent.fraInterface.afterSubmit('Fail','" + strMsg + "');";
	}
	strReturn += "</script></html>";
	
	return strReturn;
}
%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;

  GlobalInput globalInput = new GlobalInput( );
  GlobalInput tempGI = (GlobalInput)session.getValue("GI");
  if( tempGI == null ) {
  	out.println( buildMsg(false, "网页超时或者没有操作员信息") );
  	return;
  } else {
    globalInput.setSchema(tempGI);
  }
  
  //校验处理
  //内容待填充
	try {
  	LZSysCertifySchema schemaLZSysCertify = new LZSysCertifySchema();
  	
		schemaLZSysCertify.setCertifyCode( request.getParameter("CertifyCode") );
		schemaLZSysCertify.setCertifyNo( request.getParameter("CertifyNo") );

		schemaLZSysCertify.setTakeBackOperator( request.getParameter("TakeBackOperator") );
		schemaLZSysCertify.setTakeBackDate( request.getParameter("TakeBackDate") );
		ExeSQL tExeSQL=new ExeSQL();
		String szSendOutCom = request.getParameter("SendOutCom");
		String tYagentcode=tExeSQL.getOneValue(" select agentcode from laagent where groupagentcode='"+szSendOutCom+"' ");
	    if(!"".equals(tYagentcode) && tYagentcode!=null){
		   szSendOutCom="D"+tYagentcode;
	    }
	    System.out.println("发放者：===="+szSendOutCom);
		schemaLZSysCertify.setSendOutCom(szSendOutCom);
		
		String szReceiveCom		= "";
		  String tagentcode=tExeSQL.getOneValue(" select agentcode from laagent where groupagentcode='"+request.getParameter("ReceiveCom")+"' ");
		  if(!"".equals(tagentcode) && tagentcode!=null){
			  szReceiveCom="D"+tagentcode;
		  }else{
			  szReceiveCom=request.getParameter("ReceiveCom");
		  }
		  System.out.println("接收者：===="+szReceiveCom);
		schemaLZSysCertify.setReceiveCom(szReceiveCom);
		
			
	  // 准备传输数据 VData
	  VData vData = new VData();
	
	  vData.addElement(globalInput);
	  vData.addElement(schemaLZSysCertify);
		
	  // 数据传输
	  SysCertTakeBackUI sysCertTakeBackUI = new SysCertTakeBackUI();
	  
//	  sysCertTakeBackUI.mErrors.clearErrors();
//		System.out.println("Debug Code=" +  request.getParameter("CertifyNo"));
	  System.out.println("sysCertTakeBackUI.submitData");
	  if (!sysCertTakeBackUI.submitData(vData, request.getParameter("hideOperation"))) {
//	  	System.out.println("DEBUG 0:" + String.valueOf(sysCertTakeBackUI.mErrors.getErrorCount()));
//	  	System.out.println("DEBUG 1:" + sysCertTakeBackUI.mErrors.getError(0).errorMessage);
//	  	System.out.println("DEBUG 2:" + sysCertTakeBackUI.mErrors.getError(1).errorMessage);
//	  	System.out.println("DEBUG 3:" + sysCertTakeBackUI.mErrors.getError(2).errorMessage);
	  	
	  	out.println( buildMsg(false, " 保存失败，原因是: " + sysCertTakeBackUI.mErrors.getFirstError()));
	  	sysCertTakeBackUI = null;
	  	return;
	  }
	  
	  out.println( buildMsg(true, "") );
	  
	} catch(Exception ex) {
		ex.printStackTrace( );
	 	out.println( buildMsg(false, " 保存失败，原因是: " + ex.toString()));
	 	return;
	}
%>