var arrDataSet;
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
 }
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{  
	// 初始化表格
	initCardStatisticGrid();
	
	if(!validateInput()) return ;

	var strSQL = getSql();
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = CardStatisticGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 	arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 	//tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
  
  showDiv(divPage, 'true');
}

//下载投保单信息清单	2007-10-22
function printList()
{
	if(!validateInput()) return ;

	setSql();
	
	fm.action = "CardStatisticCancelPrint.jsp";
	fm.target = "_blank";
	fm.submit();
}

function showalert(){
	alert("没有符合条件的数据！");
}

function validateInput()
{
	if(fm.startDate.value == null || fm.startDate.value == "")
	{
		alert("统计起期不能为空！");
		return false;
	}
	if(fm.endDate.value == null || fm.endDate.value == "")
	{
		alert("统计止期不能为空！");
		return false;
	}
	if(fm.companyCode.value == null || fm.companyCode.value == "")
	{
		alert("统计机构不能为空！");
		return false;
	}
	return true;
}

function setSql()
{
	var startDate = fm.startDate.value;
	var endDate = fm.endDate.value;
	var companyCode = fm.companyCode.value;
	var companySQL = "";
	if(companyCode == '86')
	{
		companySQL = "";
	}
	else if(companyCode.length == 4 && companyCode.substring(0, 2) == '86')
	{
		companySQL = " and sendoutcom in (select 'B'||usercode from lzcertifyuser where 'B'||usercode in (select receivecom from lzaccess where sendoutcom = 'A'||'" + companyCode + "')) ";
	}
	else
	{
		companySQL = " and sendoutcom = 'B" + companyCode + "' ";
	}

	var dhSQL = "select t.certifycode A, "
						+ "(select d.certifyname from LMCertifyDes d where d.certifycode = t.certifycode) B, "
						+ "(select codename from ldcode1 la where codetype = 'leadership' and la.code = bg.branchtype and la.code1 = bg.branchtype2) C, "
						+ "(select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) D, "
						+ "t.modifyDate E, "
						+ "t.sumcount F, "
						+ "t.sumcount G, "
						+ "0 H, 0 I, 0 J "
						+ "from laagent a, labranchgroup bg, "
						+ "(select certifycode, receivecom, sum(sumcount) sumcount, modifydate from lzcardtrack tt "
						+ "where state in ('10', '11') "
            + getWherePart('modifyDate','startDate', '>')
            + getWherePart('(modifyDate - 1 day)','endDate', '<')
						+ companySQL
						+ "and (select 1 from lzcardtrack c where c.subcode = tt.subcode and c.startno = tt.startno and c.endno = tt.endno and (c.modifydate > tt.modifydate or (c.modifydate = tt.modifydate and c.modifytime > tt.modifytime)) and (c.state in ('2', '3', '4', '5', '6', '12') or c.state is null) order by c.modifydate, c.modifytime desc fetch first 1 rows only) = 1 "
						+ "group by modifydate, certifycode, receivecom) t "
						+ "where substr(t.receivecom, 2) = a.agentcode "
						+ "and a.agentgroup = bg.agentgroup";
	var dwhSQL = "select t.certifycode A, "
						+ "(select d.certifyname from LMCertifyDes d where d.certifycode = t.certifycode) B, "
						+ "(select codename from ldcode1 la where codetype = 'leadership' and la.code = bg.branchtype and la.code1 = bg.branchtype2) C, "
						+ "(select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) D, "
						+ "t.modifyDate E, "
						+ "t.sumcount F, "
						+ "0 G, "
						+ "t.sumcount H, "
						+ "(current date - t.modifyDate) I, "
						+ "case when t.receivecom like 'D%' then (case when (current date - t.modifyDate) > 7 then (current date - t.modifyDate - 7) else 0 end) else (case when (current date - t.modifyDate) > 30 then (current date - t.modifyDate - 30) else 0 end) end J "
						+ "from laagent a, labranchgroup bg, "
						+ "(select certifycode, receivecom, sum(sumcount) sumcount, modifydate from lzcardtrack tt "
						+ "where state in ('10', '11') "
            + getWherePart('modifyDate','startDate', '>')
            + getWherePart('(modifyDate - 1 day)','endDate', '<')
						+ companySQL
						+ "and (select sum(sumcount) from lzcard c where c.subcode = tt.subcode and c.startno = tt.startno and c.endno = tt.endno and c.modifydate = tt.modifydate and c.state in ('10', '11')) = 1 "
						+ "group by modifydate, certifycode, receivecom) t "
						+ "where substr(t.receivecom, 2) = a.agentcode "
						+ "and a.agentgroup = bg.agentgroup";
	var phSQL = "select t.certifycode A, "
						+ "(select d.certifyname from LMCertifyDes d where d.certifycode = t.certifycode) B, "
						+ "(select codename from ldcode1 la where codetype = 'leadership' and la.code = bg.branchtype and la.code1 = bg.branchtype2) C, "
						+ "(select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) D, "
						+ "t.modifyDate E, "
						+ "t.sumcount F, "
						+ "t.sumcount G, "
						+ "0 H, 0 I, 0 J "
						+ "from laagent a, labranchgroup bg, "
						+ "(select certifycode, receivecom, sum(sumcount) sumcount, modifydate from lzcardtrack tt "
						+ "where state is null and receivecom like 'D%' "
            + getWherePart('modifyDate','startDate', '>')
            + getWherePart('(modifyDate - 1 day)','endDate', '<')
						+ companySQL
						+ "and (select sum(sumcount) from lzcard c where c.subcode = tt.subcode and c.startno >= tt.startno and c.endno <= tt.endno and c.modifydate = tt.modifydate and c.state is null and receivecom like 'D%') = 0 "
						+ "group by modifydate, certifycode, receivecom) t "
						+ "where substr(t.receivecom, 2) = a.agentcode "
						+ "and a.agentgroup = bg.agentgroup";
	var pwhSQL = "select t.certifycode A, "
						+ "(select d.certifyname from LMCertifyDes d where d.certifycode = t.certifycode) B, "
						+ "(select codename from ldcode1 la where codetype = 'leadership' and la.code = bg.branchtype and la.code1 = bg.branchtype2) C, "
						+ "(select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) D, "
						+ "t.modifyDate E, "
						+ "t.sumcount F, "
						+ "0 G, "
						+ "t.sumcount H, "
						+ "(current date - t.modifyDate) I, "
						+ "0 J "
						+ "from laagent a, labranchgroup bg, "
						+ "(select certifycode, receivecom, sum(sumcount) sumcount, modifydate from lzcardtrack tt "
						+ "where state is null and receivecom like 'D%' "
            + getWherePart('modifyDate','startDate', '>')
            + getWherePart('(modifyDate - 1 day)','endDate', '<')
						+ companySQL
						+ "and (select sum(sumcount) from lzcard c where c.subcode = tt.subcode and c.startno >= tt.startno and c.endno <= tt.endno and c.modifydate = tt.modifydate and c.state is null and receivecom like 'D%') > 0 "
						+ "group by modifydate, certifycode, receivecom) t "
						+ "where substr(t.receivecom, 2) = a.agentcode "
						+ "and a.agentgroup = bg.agentgroup";
	var thSQL = "select certifycode A, "
						+ "(select d.certifyname from LMCertifyDes d where d.certifycode = t.certifycode) B, "
						+ "case when receivecom like '%jcdz' then '计划财务部' when receivecom like '%ygdz' then '业务管理部' end C, "
						+ "(select a.username from LZCertifyUser a where a.usercode = substr(t.receivecom, 2)) D, "
						+ "modifydate E, "
						+ "sum(sumcount) F, "
						+ "sum(sumcount) G, "
						+ "0 H, 0 I, 0 J "
						+ "from lzcardtrack t "
						+ "where (receivecom like '%jcdz' or receivecom like '%ygdz') "
            + getWherePart('modifyDate','startDate', '>')
            + getWherePart('(modifyDate - 1 day)','endDate', '<')
						+ "and (select sum(sumcount) from lzcard c where c.subcode = t.subcode and c.startno >= t.startno and c.endno <= t.endno and c.modifydate = t.modifydate) = 0 "
						+ "group by modifydate, certifycode, receivecom ";
	var twhSQL = "select certifycode A, "
						+ "(select d.certifyname from LMCertifyDes d where d.certifycode = t.certifycode) B, "
						+ "case when receivecom like '%jcdz' then '计划财务部' when receivecom like '%ygdz' then '业务管理部' end C, "
						+ "(select a.username from LZCertifyUser a where a.usercode = substr(t.receivecom, 2)) D, "
						+ "modifydate E, "
						+ "sum(sumcount) F, "
						+ "0 G, "
						+ "sum(sumcount) H, "
						+ "(current date - t.modifyDate) I, "
						+ "0 J "
						+ "from lzcardtrack t "
						+ "where (receivecom like '%jcdz' or receivecom like '%ygdz') "
            + getWherePart('modifyDate','startDate', '>')
            + getWherePart('(modifyDate - 1 day)','endDate', '<')
						+ "and (select sum(sumcount) from lzcard c where c.subcode = t.subcode and c.startno >= t.startno and c.endno <= t.endno and c.modifydate = t.modifydate) > 0 "
						+ "group by modifydate, certifycode, receivecom ";
	
	var strSQL = "select A,B,C,D,E,sum(F),sum(G),sum(H),sum(I),sum(J) from (" + dhSQL + " union " + dwhSQL + " union " + phSQL + " union " + pwhSQL + " union " + thSQL + " union " + twhSQL  + ") as rs group by A,B,C,D,E";
	
	fm.querySql.value = strSQL;
	
}

/*
	var strSQL = "select t.certifycode, "
			+ " (select d.certifyname from LMCertifyDes d where d.certifycode = t.certifycode) certifyname, "
			+ " (case when receivecom like '%jcdz' then '计划财务部' when receivecom like '%ygdz' then '业务管理部' else (select codename from ldcode1 la where codetype = 'leadership' and la.code = bg.branchtype and la.code1 = bg.branchtype2) end) lingyongbumen, "
			+ " (select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) lingyongren, "
			+ " t.modifyDate, "
			+ " t.sumcount, "
			+ " (current date - t.modifyDate) lingyong, "
			+ " '4' chaoqi, "
			+ " (select 1 from lzcardtrack c where c.subcode = t.subcode and c.startno = t.startno and c.endno = t.endno and (c.modifydate > t.modifydate or (c.modifydate = t.modifydate and c.modifytime > t.modifytime)) and (c.state in ('2', '3', '4', '5', '6', '12') or c.state is null) order by c.modifydate, c.modifytime desc fetch first 1 rows only) 回销, "
			+ " (select sum(sumcount) from lzcard c where c.subcode = t.subcode and c.startno = t.startno and c.endno = t.endno and c.modifydate = t.modifydate and c.state in ('10', '11')) 未回销 "
			+ " from lzcardtrack t ,laagent a, labranchgroup bg "
			+ " where t.state in ('10', '11')"
			+ " and t.modifyDate > '" + startDate + "'"
			+ " and (t.modifyDate - 1 day) < '" + endDate + "'"
			+ " and substr(t.receivecom, 2) = a.agentcode "
			+ " and a.agentgroup = bg.agentgroup ";
	if(companyCode.length == 4)
		strSQL += " and sendoutcom in (select 'B'||usercode from lzcertifyuser where 'B'||usercode in (select receivecom from lzaccess where sendoutcom = 'A'||'" + companyCode + "'))";
//	else
//		strSQL += "	and sendoutcom in (select 'B'||usercode from lzcertifyuser where 'B'||usercode in (select receivecom from lzaccess where sendoutcom in "
//		+ "(select 'A'||comcode from ldcom where comcode like '" + companyCode + "%' and Sign='1' and length(trim(comcode))=4)))";
	strSQL += " order by certifycode with ur";
*/


