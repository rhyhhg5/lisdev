<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CardQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CardQueryInit.jsp"%>
  <title>定额单证查询</title>
</head>
<body  onload="initForm();" >
  <form action="./CardQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 普通单证信息部分 -->
    <table class="common" border=0 width=100%>
      <tr><td class="titleImg" align="center">请输入查询条件：</td></tr>
	</table>
    
    <table class="common" align="center">
      <tr class="common">
        <td class="title">统计状态</td>
        <td class="input" cols=3>
          <select size="1" id="Stat">
            <option value="1">库存</option>
            <option value="2">已发放</option>
            <option value="3">已回收</option>
            <option value="4">发放未回收</option>
            <option value="5">入库总量</option></select></td></tr>
        
      <tr class="common">
        <td class="title">险种</td>
        <td class="input"><input class="code" id="Kind" ondblclick=""></td>
        
        <td class="title">附标号</td>
        <td class="input"><input class="code" id="SubCode" ondblclick=""></td></tr>
       
      <tr class="common">
        <td class="title">发放/回收</td>
        <td class="input"><input class="common" id="SendOrTake"></td>

        <td class="title">接收</td>
        <td class="input"><input class="common" id="Receiver"></td></tr>
        
      <tr class="common">
        <td class="title">提货人</td>
        <td class="input"><input class="common" id="Picker"></td>
        
        <td class="title">提货日期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" id="PickDate"></td></tr>
        
      <tr class="common">
        <td class="title">操作员</td>
        <td class="input"><input class="common" id="Operator"></td>
        
        <td class="title">入机时间</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" id="EnterTime"></td></tr>
        
      <tr class="common"><td cols=4><br></td></tr>
      
      <tr class="common">
        <td class="title">清算单号</td>
        <td class="input"><input class="common" id="ReckCode"></td>
        
        <td class="title">数量</td>
        <td class="input"><input class="common" id="Count"></td></tr>
        
      <tr class="common">
        <td class="title">起始号</td>
        <td class="input"><input class="common" id="BeginCode"></td>
        
        <td class="title">终止号</td>
        <td class="input"><input class="common" id="EndCode"></td></tr>
        
      <tr class="common">
        <td class="title">单证状态</td>
        <td class="input"><input class="common" id="CertifyStat"></td>
        
        <td class="title">单证标志</td>
        <td class="input"><input class="common" id="CertifyFlag"></td></tr>

      <tr class="common"><td cols=4><br></td></tr>
      
      <tr class="common">
        <td class="title">数量合计</td>
        <td class="input" cols=3><input class="common" id="AllCount"></td></tr>
        
    </table>

    <input value="查询" type="button" onclick="submitForm( ); return false;">
    <input value="返回" type="button" onclick="returnParent( );")>

    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCertifyInfo);"></td>
    	<td class= titleImg>单证信息</td></tr>
    </table>
    
  	<div id="divCertifyInfo" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCertifyInfo"></span></td></tr>
      </table>
      
      <input value="首页" type="button">
      <input value="上一页" type="button">					
      <input value="下一页" type="button">
      <input value="尾页" type="button">					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
