var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var showInfo;

// 一些状态的含义
var vStateFlag = new ActiveXObject("Scripting.Dictionary");

vStateFlag.Add("", "入机");
vStateFlag.Add("0", "未用");
vStateFlag.Add("1", "正常回收");
vStateFlag.Add("2", "作废");
vStateFlag.Add("3", "遗失");
vStateFlag.Add("4", "销毁");

var vOperateFlag = new ActiveXObject("Scripting.Dictionary");

vOperateFlag.Add("0", "发放");
vOperateFlag.Add("1", "回收");
vOperateFlag.Add("2", "发放回退");
vOperateFlag.Add("3", "回收回退");


//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput() == true ) {
	  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	fm.submit(); //提交
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  CardInfo.clearData();  // 清空原来的数据

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySearch.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

// 在查询结束时，触发这个事件。
function onShowResult(result)
{

	if( result[0].charAt(2) == '0' ) {
		alert('没有查询到数据');
	} else {
		useSimulationEasyQueryClick(result[0]);
	}
}

function useSimulationEasyQueryClick(strData) {
  //保存查询结果字符串
  turnPage2.strQueryResult  = strData;

  //使用模拟数据源，必须写在拆分之前
  turnPage2.useSimulation   = 1;

  //拆分字符串，返回二维数组
  var tArr                 = decodeEasyQueryResult(turnPage2.strQueryResult);

  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray          = new Array(0, 6, 7, 4, 5, 8, 22, 23, 24);

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage2.arrDataCacheSet = clearArrayElements(turnPage2.arrDataCacheSet);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage2.arrDataCacheSet = chooseArray(tArr, filterArray);

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage2.pageDisplayGrid = CardInfo

  //设置查询起始位置
  turnPage2.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, turnPage2.pageLineNum);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage2.queryAllRecordCount > turnPage2.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }

  //必须将所有数据设置为一个数据块
  turnPage2.blockPageNum = turnPage2.queryAllRecordCount / turnPage2.pageLineNum;
}

// 事件响应函数，当用户改变CodeSelect的值时触发
function afterCodeSelect(cCodeName, Field)
{
	try {
	} catch(ex) {
		alert("在afterCodeSelect中发生异常");
	}
}

// 查询单证状态表的信息
function searchState()
{
	//修改原因，原先是通过后台提交查询数据的，导致mutline数据翻页有问题，因此修改
	initCardInfo();
	initCardState();
	
	divSumCount.style.display='none';
	
	var startNo = fm.StartNo.value;
	var endNo = fm.EndNo.value;
	var tendno="";
	var noSQL="";
	var snoSQL="";
	var enoSQL="";
	var nocount=0;
	var arrResult=null;
	var arreResult=null;
	var nums=0;
	var nume=0;
	var fsql="SELECT certifycode,sendoutcom,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else ReceiveCom end ReceiveCom,startno,endno,sumcount,";
	var esql=" ORDER BY CertifyCode, StartNo, EndNo, SendOutCom, ReceiveCom  ";
	
	var strz="";
	if(fm.ManageComC.value!=""){
			
		strz=" and ((ReceiveCom like 'A%' and ReceiveCom like '"+"A"+fm.ManageComC.value+"%' ) " +
		" or  (ReceiveCom like 'B%' and exists (select 1 from lduser where usercode=substr(char(ReceiveCom),2) " +
		" and comcode like  '"+fm.ManageComC.value+"%')) or (ReceiveCom not like 'A%' and ReceiveCom not like 'B%'" +
				" and exists (select 1 from lduser where usercode=substr(char(sendoutcom),2) " +
		" and comcode like  '"+fm.ManageComC.value+"%') )) ";
	}
	
	var comsql=" ( "+detailSQL(fsql,noSQL,strz)+esql+" ) ";
	var strSQL="";
   if( verifyInput() == false ) 
   {
      fm.all('CertifyCode').focus();
      return false;
   }
   //下面是普通單證明細查詢部份起始終止號的各種校驗及相應SQL
   
   //起始終止號都錄入的情況
	if(startNo!=""&&endNo!=""){
		
		if(parseInt(endNo,10)<parseInt(startNo,10)){
			alert("起始号不能大于终止号");
			return false ;
		}
		   var strsSQL = "SELECT endno,stateflag,startno FROM LZCard WHERE 1=1 "
			   + getWherePart('CertifyCode','CertifyCode')
				+ getWherePart('StartNo','StartNo','<=')
				+ getWherePart('EndNo','StartNo','>=')
				+ getWherePart('StateFlag','CertifyState')
				+" with ur"
				;
		
		arrResult = easyExecSql(strsSQL);
		if(arrResult != null)
		{
			tendno = arrResult[0][0];
			
			
			//起始終止號在同一區間的時候
			if(tendno>=endNo){
				noSQL=" and Startno<='"+startNo+"' and Endno>='"+startNo+"' ";
				nocount=parseInt(endNo,10)-parseInt(startNo,10)+1;
				nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10)+parseInt(tendno,10)-parseInt(endNo,10);
				if(nums<0){
					alert("起始或终止号录入错误，可能超出最大单证号，请检验");
					return false ;
				}
				fsql="SELECT certifycode,sendoutcom,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else ReceiveCom end receivecom,'"+startNo+"','"+endNo+"','"+nocount+"',";
				strSQL=detailSQL(fsql,noSQL,strz)+esql;
			}
			
			
			//起始終止號不在同一區間的時候
			else if(tendno<endNo){
				var startSQL="";
				var endSQL="";
				var efsql="";
				
				var streSQL = "SELECT startno,stateflag,endno FROM LZCard WHERE 1=1 "
					   + getWherePart('CertifyCode','CertifyCode')
						+ getWherePart('StartNo','EndNo','<=')
						+ getWherePart('EndNo','EndNo','>=')
						+ getWherePart('StateFlag','CertifyState')
						+" with ur"
						;
				arreResult = easyExecSql(streSQL);
				//起始号段所在区间
				snoSQL=" and Startno<='"+startNo+"' and Endno='"+tendno+"' ";
				nocount=parseInt(tendno,10)-parseInt(startNo,10)+1;
				nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10);
				if(nums<0){
					alert("起始或终止号录入错误，可能超出最大单证号，请检验");
					return false ;
				}
				efsql="SELECT certifycode,sendoutcom,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else ReceiveCom end  receivecom,'"+startNo+"',endno,"+nocount+",";
				startSQL=detailSQL(efsql,snoSQL,strz);
				if(arreResult!=null){
					var estartno=arreResult[0][0];
					//终止号段所在区间
					enoSQL=" and '"+endNo+"' between Startno and Endno ";
					nocount=parseInt(endNo,10)-parseInt(estartno,10)+1;
					nume=parseInt(arreResult[0][2],10)-parseInt(endNo,10);
					if(nume<0){
						alert("起始或终止号录入错误，可能超出最大单证号，请检验");
						return false ;
					}
					efsql="SELECT certifycode,sendoutcom,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else ReceiveCom end receivecom,startno,'"+endNo+"',"+nocount+",";
					endSQL=detailSQL(efsql,enoSQL,strz);
				}
				else{
					alert("未查询到该终止号的单证信息");
					return false;
				}
				noSQL=" and  Startno>'"+startNo+"' and Endno<'"+endNo+"' ";
				comsql=" ( "+detailSQL(fsql,noSQL,strz)+esql+" ) ";
				strSQL=startSQL+" union all "+comsql+" union all "+endSQL+"with ur";
				
			}
		}
		else{
			alert("未查询到该起始号的单证信息");
			return false;
		}
	   }
	
	
	//只錄了起始號，沒有錄入終止號的情況
	else if(startNo!=""&&endNo==""){

			   var strsSQL = "SELECT endno,stateflag,startno FROM LZCard WHERE 1=1 "
				   + getWherePart('CertifyCode','CertifyCode')
					+ getWherePart('StartNo','StartNo','<=')
					+ getWherePart('EndNo','StartNo','>=')
					+ getWherePart('StateFlag','CertifyState')
					+" with ur"
					;
			
			   arrResult = easyExecSql(strsSQL);
			   if(arrResult != null)
			   {
				   tendno = arrResult[0][0];
				   var startSQL="";
				   var endSQL="";
				   var efsql="";
				   //起始号段所在区间
				   snoSQL=" and Startno<='"+startNo+"' and Endno='"+tendno+"' ";
				   nocount=parseInt(tendno,10)-parseInt(startNo,10)+1;
				   nums=parseInt(startNo,10)-parseInt(arrResult[0][2],10);
				   if(nums<0){
						alert("起始或终止号录入错误，可能超出最大单证号，请检验");
						return false ;
					}
				   efsql="SELECT certifycode,sendoutcom,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else ReceiveCom end receivecom,'"+startNo+"',endno,"+nocount+",";
				   startSQL=detailSQL(efsql,snoSQL,strz);
				   noSQL=" and  Startno>'"+startNo+"' ";
				   comsql=" ( "+detailSQL(fsql,noSQL,strz)+esql+" ) ";
				   strSQL=startSQL+" union all "+comsql+"with ur";
					
				
			   }
			   else{
					alert("未查询到该起始号的单证信息");
					return false;
				}
		   
		}
	
	
	//只錄入了終止號沒有錄入起始號的情況
	else if (endNo!=""&&startNo==""){

			var streSQL = "SELECT startno,stateflag,endno FROM LZCard WHERE 1=1 "
							+ getWherePart('CertifyCode','CertifyCode')
							+ getWherePart('StartNo','EndNo','<=')
							+ getWherePart('EndNo','EndNo','>=')
							+ getWherePart('StateFlag','CertifyState')
							+" with ur"
							;
			arreResult = easyExecSql(streSQL);
			
			if(arreResult!=null){
				var estartno=arreResult[0][0];
				//终止号段所在区间
				enoSQL=" and '"+endNo+"' between Startno and Endno ";
				nocount=parseInt(endNo,10)-parseInt(estartno,10)+1;
				nume=parseInt(arreResult[0][2],10)-parseInt(endNo,10);
				if(nume<0){
					alert("起始或终止号录入错误，可能超出最大单证号，请检验");
					return false ;
				}
				efsql="SELECT certifycode,sendoutcom,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else ReceiveCom end receivecom,startno,'"+endNo+"',"+nocount+",";
				endSQL=detailSQL(efsql,enoSQL,strz);
			}
			else{
				alert("未查询到该终止号的单证信息");
				return false;
			}
			noSQL=" and   Endno<'"+endNo+"' ";
			comsql=" ( "+detailSQL(fsql,noSQL,strz)+esql+" ) ";
			strSQL=comsql+" union all "+endSQL+"with ur";
					
				
			  }
		
	
	//沒有錄入起始終止號的時候
	else{
		strSQL=comsql+"with ur";
	}
   
   
   
	
	fm.SearchSQL.value = strSQL;
	turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	
	if (!turnPage2.strQueryResult) {
		alert("没有符合查询条件的记录！");
		return false;
	}
	//查询成功则拆分字符串，返回二维数组
	turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage2.pageDisplayGrid = CardInfo;
	//保存SQL语句
	turnPage2.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage2.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	//调用MULTILINE对象显示查询结果
	var tArr = new Array();
	tArr = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, MAXSCREENLINES);
	displayMultiline(tArr, turnPage2.pageDisplayGrid);
	
	fm.State.value = "0";


	//上面部份是明細查詢
/*******************************************************************************************/
	
	
	
	//下面開始統計部份的起始終止號的校驗及相應SQL
		var strStateSQL = "";
		var sumcountno=" sum(sumcount) ";
		
		//起始終止號都錄入的情況
		if(startNo!=""&&endNo!="")	{
			
			
			//起始終止號在同一區間的時候
				if(tendno>=endNo){
					strStateSQL="select t.certifycode,t.stateflag,sum(t.sum) from " 
						+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",noSQL,strz)
						+"  ) " +
						"as t group by t.certifycode,t.stateflag order by t.certifycode,t.stateflag with ur ";
				}
				
				//起始終止號不在同一區間的時候
				else
				strStateSQL="select t.certifycode,t.stateflag,sum(t.sum) from " 
							+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",snoSQL,strz)
							+" union all "+getSumSQL(" sum(sumcount) as sum ",noSQL,strz)
							+" union all "
							+getSumSQL(" sum(sumcount)-"+nume+" as sum ",enoSQL,strz)+" ) " +
							"as t group by t.certifycode,t.stateflag order by t.certifycode,t.stateflag with ur ";
		}
		
		
		//只有起始號沒有錄終止號的時候
		else if(startNo!=""&&endNo=="")	
				strStateSQL="select t.certifycode,t.stateflag,sum(t.sum) from " 
					+"("+getSumSQL(" sum(sumcount)-"+nums+" as sum ",snoSQL,strz)
					+" union all "+getSumSQL(" sum(sumcount) as sum ",noSQL,strz)
					+"  ) "
					+"as t group by t.certifycode,t.stateflag order by t.certifycode,t.stateflag with ur ";
		
		//只有終止號沒有起始號的情況
		else if(startNo==""&&endNo!="")	
				strStateSQL="select t.certifycode,t.stateflag,sum(t.sum) from " 
					+"("+getSumSQL(" sum(sumcount) as sum ",noSQL,strz)
					+" union all "
					+getSumSQL(" sum(sumcount)-"+nume+" as sum ",enoSQL,strz)+" ) " +
					"as t group by t.certifycode,t.stateflag order by t.certifycode,t.stateflag with ur ";
		
		//nothing to 錄的情況
		else strStateSQL=getSumSQL(sumcountno,noSQL,strz)+" with ur ";
		
		
	turnPage1.queryModal(strStateSQL, CardState); 
	arrResult=null;
	arreResult=null;
}

/**
 * 普通單證明細查詢SQL
 * @param fSQL
 * @param noSQL
 * @param strz
 * @returns
 */
function detailSQL(fSQL,noSQL,strz){
	
		
	   var sqlno="";
	   var nonum=0;
	   var tReceiveCom="";
	   //add by zjd 集团统一工号
	   if(fm.ReceiveCom.value!=""){
		   tReceiveCom=getUnitecode(fm.ReceiveCom.value);
		   if(tReceiveCom!="" ){
			   tReceiveCom=" ReceiveCom='"+tReceiveCom+"'";
		   }else{
			   tReceiveCom=" ReceiveCom='"+fm.ReceiveCom.value+"'";
		   }
	   }
	   
		
		//查询该险种下的险种计算要素
		if (fm.TempComCode.value.length<=4)
		{
			
			strSQL =fSQL 
			    + "case StateFlag when '2' then '作废' when '3' then '遗失' when '4' then '销毁' when '5' then '正常回销' when '6' then '总部领用' when '7' then '已发放' when '8' then '已领用' when '9' then '空白回销' else '未领用' end,"
			    + "operator,makedate,maketime,(case when (select (select comcode from lduser where usercode=substr(char(ReceiveCom),2)) " +
					"from LZCardTrack b where a.CertifyCode = b.CertifyCode  and a.StartNo >= b.StartNo " +
					"and a.EndNo <= b.EndNo " +
			    		" and b.ReceiveCom like 'B%'  " +
			    		"and b.ReceiveCom <> 'A86' " +
			    		" and b.sendoutcom like 'A%' " 
			    		+strz
			    		+" order by  makedate desc fetch first 1 rows only) is null  " +
			    				" then (select substr(char(ReceiveCom),2) " +
			    				"from LZCardTrack b where a.CertifyCode = b.CertifyCode " +
			    				" " +
			    				" and a.StartNo >= b.StartNo and a.EndNo <= b.EndNo " +
			    		" and b.ReceiveCom like 'A%'  " +
			    		"and b.ReceiveCom <> 'A86' " 
			    		+strz
			    		+" order by  makedate desc fetch first 1 rows only) " +
			    				" else (select (select comcode from lduser " +
			    				"where usercode=substr(char(ReceiveCom),2)) " +
			    				"from LZCardTrack b where a.CertifyCode = b.CertifyCode " +
			    				" and a.StartNo >= b.StartNo " +
			    				" and a.EndNo <= b.EndNo " +
			    		" and b.ReceiveCom like 'B%'  " +
			    		"and b.ReceiveCom <> 'A86' " +
			    		" and b.sendoutcom like 'A%' " 
			    		+strz
			    		+" order by  makedate desc fetch first 1 rows only) end) ," +
			    					" ( case when ( select contno from lccertifytakeback where cardno = startno and state = '1' fetch first 1 rows only ) is null then " +
		    				"(case when ( select contno from lccont where proposalcontno = startno fetch first 1 rows only ) is null then" +
		    				" (select contno from lbcont where proposalcontno = startno fetch first 1 rows only ) " +
		    				" else ( select contno from lccont where proposalcontno = startno fetch first 1 rows only ) end) " +
		    				"else ( select contno from lccertifytakeback where cardno = startno and state = '1' fetch first 1 rows only ) end)  " +
			    				" FROM LZCard a WHERE 1=1 "
				+ getWherePart('CertifyCode','CertifyCode')
				+ getWherePart('SendOutCom','SendOutCom')
				+ tReceiveCom
				+ getWherePart('Handler','Handler')
				+ noSQL
				+ getWherePart('StateFlag','CertifyState')
				+ getWherePart('HandleDate','HandleDateB','>=')
				+ getWherePart('HandleDate','HandleDateE','<=')
				+strz
		        + " and exists (select 1 from LZCardTrack b where " +
		        		"a.CertifyCode = b.CertifyCode " +
		        		"and a.StartNo >= b.StartNo " +
		        		"and a.EndNo <= b.EndNo " 
		        		+strz+
		        		"and (b.SendOutCom like 'A"+tManageCom+"%' or b.ReceiveCom like 'A"+tManageCom+"%'))"
		        ;
		    		   
		}
		else
		{
			fm.ReceiveCom.value=fm.AgentCode.value;
			var strt="";
			if(fm.ManageComC.value!=""){
				strt=" and exists (select 1 from lduser where usercode=substr(char(receivecom),2) and comcode like '"+fm.ManageComC.value+"%')";
			}
			strSQL = fSQL
			    + "case StateFlag when '2' then '作废' when '3' then '遗失' when '4' then '销毁' when '5' then '正常回销' when '6' then '总部领用' when '7' then '已发放' when '8' then '已领用' when '9' then '空白回销' else '未领用' end,"
			    + "operator,makedate,maketime,(select comcode from lduser where usercode=substr(char(receivecom),2))," +
			    						" ( case when ( select contno from lccertifytakeback where cardno = startno and state = '1' fetch first 1 rows only ) is null then " +
		    				"(case when ( select contno from lccont where proposalcontno = startno fetch first 1 rows only ) is null then" +
		    				" (select contno from lbcont where proposalcontno = startno fetch first 1 rows only ) " +
		    				" else ( select contno from lccont where proposalcontno = startno fetch first 1 rows only ) end) " +
		    				"else ( select contno from lccertifytakeback where cardno = startno and state = '1' fetch first 1 rows only ) end) "+
			    				"  FROM LZCard a WHERE 1=1 "
				+ getWherePart('CertifyCode','CertifyCode')
				+ getWherePart('SendOutCom','SendOutCom')
				+ tReceiveCom
				+ getWherePart('Handler','Handler')
				+ noSQL
				+ getWherePart('StateFlag','CertifyState')
				+ getWherePart('HandleDate','HandleDateB','>=')
				+ getWherePart('HandleDate','HandleDateE','<=')
				+strz
			    + " and exists (select 1 from LZCardTrack b where a.CertifyCode = b.CertifyCode " +
		        		"and a.StartNo >= b.StartNo " +
		        		"and a.EndNo <= b.EndNo " 
		        		+ strt+
		        		" and (b.SendOutCom = 'B"+fm.TempOpe.value+"' or b.ReceiveCom = 'B"+fm.TempOpe.value+"'))"
		        ;
		   	}
		return strSQL;
}

/**
 * 普通單證統計部份SQL
 * @param sumcountno
 * @param noSQL
 * @param strz
 * @returns {String}
 */
function getSumSQL(sumcountno,noSQL,strz){
	
	var strStateSQL = "";
	var tReceiveCom="";
	   //add by zjd 集团统一工号
	   if(fm.ReceiveCom.value!=""){
		   tReceiveCom=getUnitecode(fm.ReceiveCom.value);
		   if(tReceiveCom!="" ){
			   tReceiveCom=" ReceiveCom='"+tReceiveCom+"'";
		   }else{
			   tReceiveCom=" ReceiveCom='"+fm.ReceiveCom.value+"'";
		   }
	   }
	if (fm.TempComCode.value.length<=4)
	{
		strStateSQL = "SELECT certifycode,"
		    + "(case StateFlag when '2' then '作废' when '3' then '遗失' " +
		    		"when '4' then '销毁' when '5' then '正常回销' " +
		    		"when '6' then '总部领用' when '7' then '已发放' " +
		    		"when '8' then '已领用' when '9' then '空白回销' else '未领用' end) as stateflag,"
		    + sumcountno+
		    "FROM LZCard a WHERE 1=1 "
			+ getWherePart('CertifyCode','CertifyCode')
			+ getWherePart('SendOutCom','SendOutCom')
			+ tReceiveCom
			+ getWherePart('Handler','Handler')
			+ noSQL
			+ getWherePart('StateFlag','CertifyState')
			+ getWherePart('HandleDate','HandleDateB','>=')
			+ getWherePart('HandleDate','HandleDateE','<=')
			+strz
		    + " and exists (select 1 from LZCardTrack b where " +
	        		"a.CertifyCode = b.CertifyCode and a.StartNo >= b.StartNo " +
	        		"and a.EndNo <= b.EndNo " 
	        		+strz+
	        		"and (b.SendOutCom like 'A"+tManageCom+"%' " +
	        				"or b.ReceiveCom like 'A"+tManageCom+"%'))"
	        ;
	    
	    strStateSQL += " Group by CertifyCode, StateFlag   ";
	}
	else
	{
		strStateSQL = "SELECT certifycode,"
		    + "(case StateFlag when '2' then '作废' " +
		    		"when '3' then '遗失' when '4' then '销毁' " +
		    		"when '5' then '正常回销' when '6' then '总部领用' " +
		    		"when '7' then '已发放' when '8' then '已领用' " +
		    		"when '9' then '空白回销' else '未领用' end) as stateflag,"
		    + sumcountno +
		    		"FROM LZCard a WHERE 1=1 "
			+ getWherePart('CertifyCode','CertifyCode')
			+ getWherePart('SendOutCom','SendOutCom')
			+ tReceiveCom
			+ getWherePart('Handler','Handler')
			+ noSQL
			+ getWherePart('StateFlag','CertifyState')
			+ getWherePart('HandleDate','HandleDateB','>=')
			+ getWherePart('HandleDate','HandleDateE','<=')
			+strz
		    + " and exists (select 1 from LZCardTrack b where a.CertifyCode = b.CertifyCode and a.StartNo >= b.StartNo and a.EndNo <= b.EndNo and (b.SendOutCom = 'B"+fm.TempOpe.value+"' or b.ReceiveCom = 'B"+fm.TempOpe.value+"'))"
	        ;
	        strStateSQL += " Group by CertifyCode, StateFlag   ";
	    
	}
	return strStateSQL;
}

// 查询单证轨迹表的信息
function searchTrack()
{
	initCardInfo();
	initCardState();
    if( verifyInput() == false ) 
    {
      fm.all('CertifyCode').focus();
      return false;
    }
	divSumCount.style.display='';
	var str="";
	if(fm.ManageComC.value!=""){
		str=" and (ReceiveCom = '"+"A"+fm.ManageComC.value+"%' " +
		" or exists (select 1 from lduser where usercode=substr(char(ReceiveCom),2) and comcode =  '"+fm.ManageComC.value+"')) ";
	}
	var tReceiveCom="";
	   //add by zjd 集团统一工号
	   if(fm.ReceiveCom.value!=""){
		   tReceiveCom=getUnitecode(fm.ReceiveCom.value);
		   if(tReceiveCom!="" ){
			   tReceiveCom=" ReceiveCom='"+tReceiveCom+"'";
		   }else{
			   tReceiveCom=" ReceiveCom='"+fm.ReceiveCom.value+"'";
		   }
	   }
	if (fm.TempComCode.value.length<=4)
	{
		strSQL = "SELECT sum(sumcount) FROM LZCardTrack WHERE 1=1 "
			+ getWherePart('CertifyCode','CertifyCode')
			+ getWherePart('SendOutCom','SendOutCom')
			+ tReceiveCom
			//+ getWherePart('Operator','Operator')
			+ getWherePart('Handler','Handler')
			+ getWherePart('StartNo','StartNo','>=')
			+ getWherePart('EndNo','EndNo','<=')
			+ getWherePart('StateFlag','CertifyState')
			+ str +
			" and (SendOutCom like 'A"+tManageCom+"%' or ReceiveCom like 'A"+tManageCom+"%') and (StateFlag in ('0','7','8') or StateFlag is null)"
			;
	}
	else
	{
		strSQL = "SELECT sum(sumcount) FROM LZCardTrack WHERE 1=1 "
			+ getWherePart('CertifyCode','CertifyCode')
			+ getWherePart('SendOutCom','SendOutCom')
			+ tReceiveCom
			//+ getWherePart('Operator','Operator')
			+ getWherePart('Handler','Handler')
			+ getWherePart('StartNo','StartNo','>=')
			+ getWherePart('EndNo','EndNo','<=')
			+ getWherePart('StateFlag','CertifyState')
			//+ " and (SendOutCom like 'A"+tManageCom+"%25' or ReceiveCom like 'A"+tManageCom+"%25')"
			+ " and (SendOutCom = 'B"+fm.TempOpe.value+"' or ReceiveCom = 'B"+fm.TempOpe.value+"') and (StateFlag in ('0','7','8') or StateFlag is null) "
			;
	}
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		fm.certifysumcount.value = arrResult[0][0];
	}
	else
	{
	  fm.certifysumcount.value = "0";
	}
	
	//查询该险种下的险种计算要素
	if (fm.TempComCode.value.length<=4)
	{
		strSQL = "SELECT certifycode,sendoutcom,receivecom,startno,endno,sumcount,"
		  + "case StateFlag when '2' then '作废' when '3' then '遗失' when '4' then '销毁' when '5' then '正常回销' when '6' then '总部领用' when '7' then '已发放' when '8' then '已领用' when '9' then '空白回销' else '未领用' end,"
		  + "operator,makedate,maketime,substr(char(ReceiveCom),2)," +
		    							" ( case when ( select contno from lccertifytakeback where cardno = startno and state = '1' fetch first 1 rows only ) is null then " +
		    				"(case when ( select contno from lccont where proposalcontno = startno fetch first 1 rows only ) is null then" +
		    				" (select contno from lbcont where proposalcontno = startno fetch first 1 rows only ) " +
		    				" else ( select contno from lccont where proposalcontno = startno fetch first 1 rows only ) end) " +
		    				"else ( select contno from lccertifytakeback where cardno = startno and state = '1' fetch first 1 rows only ) end) "+
		    				" FROM LZCardTrack WHERE 1=1 "
			+ getWherePart('CertifyCode','CertifyCode')
			+ getWherePart('SendOutCom','SendOutCom')
			+ tReceiveCom
			//+ getWherePart('Operator','Operator')
			+ getWherePart('Handler','Handler')
	//		+ getWherePart('MakeDate','MakeDateB','>=')
	//		+ getWherePart('MakeDate','MakeDateE','<=')
			+ getWherePart('HandleDate','HandleDateB','>=')
			+ getWherePart('HandleDate','HandleDateE','<=')
			+ getWherePart('StateFlag','CertifyState')
	//		+ getWherePart('InvalidDate','InvalidDateB','>=')
	//		+ getWherePart('InvalidDate','InvalidDateE','<=')
			+ " and (SendOutCom like 'A"+tManageCom+"%' or ReceiveCom like 'A"+tManageCom+"%') and (StateFlag in ('0','7','8') or StateFlag is null)"
			+ str +
			" ORDER BY CertifyCode, SendOutCom, ReceiveCom, StartNo, EndNo with ur ";
	}
	else
	{
		fm.ReceiveCom.value=fm.AgentCode.value;
		var strt="";
		if(fm.ManageComC.value!=""){
			strt=" and exists (select 1 from lduser where usercode=substr(char(receivecom),2) and comcode='"+fm.ManageComC.value+"')";
		}
		strSQL = "SELECT certifycode,sendoutcom,receivecom,startno,endno,sumcount,"
		  + "case StateFlag when '2' then '作废' when '3' then '遗失' when '4' then '销毁' when '5' then '正常回销' when '6' then '总部领用' when '7' then '已发放' when '8' then '已领用' when '9' then '空白回销' else '未领用' end,"
		  + "operator,makedate,maketime,(select comcode from lduser where usercode=substr(char(receivecom),2))," +
		    					" ( case when ( select contno from lccertifytakeback where cardno = startno and state = '1') is null then " +
		    				"(case when ( select contno from lccont where proposalcontno = startno ) is null then" +
		    				" (select contno from lbcont where proposalcontno = startno ) " +
		    				" else ( select contno from lccont where proposalcontno = startno ) end) " +
		    				"else ( select contno from lccertifytakeback where cardno = startno and state = '1') end)  " +
		    				"  FROM LZCardTrack WHERE 1=1 "
			+ getWherePart('CertifyCode','CertifyCode')
			+ getWherePart('SendOutCom','SendOutCom')
			+ tReceiveCom
			//+ getWherePart('Operator','Operator')
			+ getWherePart('Handler','Handler')
	//		+ getWherePart('MakeDate','MakeDateB','>=')
	//		+ getWherePart('MakeDate','MakeDateE','<=')
			+ getWherePart('HandleDate','HandleDateB','>=')
			+ getWherePart('HandleDate','HandleDateE','<=')
			+ getWherePart('StateFlag','CertifyState')
	//		+ getWherePart('InvalidDate','InvalidDateB','>=')
	//		+ getWherePart('InvalidDate','InvalidDateE','<=')
			+ " and (SendOutCom = 'B"+fm.TempOpe.value+"' or ReceiveCom = 'B"+fm.TempOpe.value+"') and (StateFlag in ('0','7','8') or StateFlag is null) "
			+strt
			+ " ORDER BY CertifyCode, SendOutCom, ReceiveCom, StartNo, EndNo with ur";
	}
//	fm.all('EndNo').value = strSQL;
  fm.SearchSQL.value = strSQL;
	turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//原则上不会失败，嘿嘿
	if (!turnPage2.strQueryResult) {
		alert("没有复合查询条件的记录！");
		return false;
	}
	//查询成功则拆分字符串，返回二维数组
	turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage2.pageDisplayGrid = CardInfo;
	//保存SQL语句
	turnPage2.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage2.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	//调用MULTILINE对象显示查询结果
	var tArr = new Array();
	tArr = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, MAXSCREENLINES);
	displayMultiline(tArr, turnPage2.pageDisplayGrid);
	fm.State.value = "1";
//	submitForm();
}

function putList()
{
	var vRow = CardInfo.getSelNo();

	if( vRow == null || vRow == 0 ) {
		alert("请先选择一条查询结果");
		return;
	}

	vRow = vRow - 1;

	var vColIndex = 0;
	var vMaxRow = CardListInfo.mulLineCount;

	CardListInfo.addOne();

	for(vColIndex = 1; vColIndex < 10; vColIndex++) {
		CardListInfo.setRowColData(vMaxRow, vColIndex, CardInfo.getRowColData(vRow, vColIndex));
	}
}

function printList()
{
	if (fm.SearchSQL.value=="") {
		alert("请先进行查询");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "CertifySearchPrint";
  fm.action = "CertifySearchPrintSave.jsp";
	fm.submit();
  showInfo.close();
}

function boxEventHandler(parm1, parm2)
{
	var vRow = CardInfo.getSelNo();

	if( vRow == null || vRow == 0 ) {
		return;
	}

	vRow = vRow - 1;

	var strSQL = "";
	if( fm.State.value == "0" ) {
		strSQL = "SELECT *,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else Receivecom end  FROM LZCard";
	} else {
		strSQL = "SELECT *,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else Receivecom end FROM LZCardTrack";
	}
	 var tReceiveCom="";
     //add by zjd 集团统一工号
	   tReceiveCom=getUnitecode(CardInfo.getRowColData(vRow, 3));
	   if(tReceiveCom!="" ){
		   tReceiveCom="' AND ReceiveCom ='"+tReceiveCom+"";
	   }else{
		   tReceiveCom="' AND ReceiveCom ='"+CardInfo.getRowColData(vRow, 3)+"";
	   }
	   
	strSQL = strSQL + " WHERE CertifyCode = '" + CardInfo.getRowColData(vRow, 1) +
	          "' AND SendOutCom = '" + CardInfo.getRowColData(vRow, 2) +
	           tReceiveCom +
	          "' AND StartNo = '" + CardInfo.getRowColData(vRow, 4) +
	          "' AND EndNo = '" + CardInfo.getRowColData(vRow, 5) +
	          "' AND MakeDate = '" + CardInfo.getRowColData(vRow, 8) +
	          "' AND MakeTime = '" + CardInfo.getRowColData(vRow, 9) + "'";

	// Use my docode function
	var myResult = myDecodeEasyQueryResult(easyQueryVer3(strSQL));

	fm.CertifyCode.value 		= myResult[0][0];
	fm.SendOutCom.value 		= myResult[0][6];
	if( fm.State.value == "0" ) {
		fm.ReceiveCom.value 		= myResult[0][28];
	}else{
		fm.ReceiveCom.value 		= myResult[0][29];
	}
	
	fm.Operator.value 			= myResult[0][22];
	fm.Handler.value 				= myResult[0][11];
	fm.HandleDateB.value 		= myResult[0][12];
	fm.HandleDateE.value 		= "";
	fm.MakeDateB.value 			= myResult[0][23];
	fm.MakeDateE.value 			= "";
	fm.TakeBackNo.value 		= myResult[0][14];
	fm.SumCount.value 			= myResult[0][8];
	fm.StartNo.value 				= myResult[0][4];
	fm.EndNo.value 					= myResult[0][5];
	fm.CertifyState.value 	= vStateFlag.Item(myResult[0][16]);
	fm.OperateFlag.value 		= vOperateFlag.Item(myResult[0][17]);
}

// EasyQuery 的这个函数会和 turnPage对象相互影响，所以就自己照着写了一个
function myDecodeEasyQueryResult(strResult) {
	var arrEasyQuery = new Array();
	var arrRecord = new Array();
	var arrField = new Array();
	var recordNum, fieldNum, i, j;

	if (typeof(strResult) == "undefined" || strResult == "" || strResult == false)	{
		return null;
	}

	//公用常量处理，增强容错性
	if (typeof(RECORDDELIMITER) == "undefined") RECORDDELIMITER = "^";
	if (typeof(FIELDDELIMITER) == "undefined") FIELDDELIMITER = "|";

	try {
	  arrRecord = strResult.split(RECORDDELIMITER);      //拆分查询结果，得到记录数组

	  recordNum = arrRecord.length;
	  for(i=1; i<recordNum; i++) {
	  	arrField = arrRecord[i].split(FIELDDELIMITER); //拆分记录，得到字段数组

	  	fieldNum = arrField.length;
	  	arrEasyQuery[i - 1] = new Array();
	  	for(j=0; j<fieldNum; j++) {
		  	arrEasyQuery[i - 1][j] = arrField[j];          //形成以行为记录，列为字段的二维数组
		  }
	  }
	}
	catch(ex) {
	  alert("拆分数据失败！" + "\n错误原因是：" + ex);
	  return null;
	}

	return arrEasyQuery;
}
function closePage()
{
  top.close();
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	fm.ReceiveType.value="AGE";
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
}
function queryCom()
{
    fm.ReceiveType.value="AGECOM";
    showInfo = window.open("./LAComMain.jsp?ManageCom=" + fm.all('ManageCom').value.substr(0, 4) + "&BranchType=3&BranchType2=01&BankType=01");
}

function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	if( fm.chkModeBatch.checked == false ) {
	    fm.PrtNoEx.value = arrResult[0][0];
	    fm.ReceiveCom.value = 'A' + arrResult[0][11];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
	    
			var rowCount = 0;

	    CertifyList.setRowColData(rowCount, 1, arrResult[0][1]);
	    CertifyList.setRowColData(rowCount, 2, arrResult[0][25]);
	    CertifyList.setRowColData(rowCount, 3, arrResult[0][18]);
	    CertifyList.setRowColData(rowCount, 4, arrResult[0][19]);
	    
	    var cNum = parseFloat(arrResult[0][19],10)-parseFloat(arrResult[0][18],10)+1;
	    CertifyList.setRowColData(rowCount, 5, cNum+"");
	    
	  } else {
	    fm.ReceiveCom.value = arrResult[0][3];
	    fm.ReceiveCom.title = arrResult[0][1];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
		}
  }
}

function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  	if (fm.ReceiveType.value=="AGE") {
  		fm.AgentCode.value='D'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][95];
  	} else if (fm.ReceiveType.value=="AGECOM") {
  		fm.AgentCode.value='E'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][0];
  	}
	}
}

function getUnitecode(mreciver){
	var tsql=" select agentcode from laagent where groupagentcode='"+mreciver+"'";
	var arrResult = easyExecSql(tsql);
    if(arrResult){
    	return  arrResult[0][0];
    }else{
    	return "";
    }
        
}