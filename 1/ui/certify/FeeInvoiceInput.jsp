<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：核销管理
//程序功能：
//创建日期：2002-10-08
//创建人  ：Javabean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="FeeInvoiceInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FeeInvoiceInputInit.jsp"%>

<script language="javascript">
</script>
</head>
<body  onload="initForm();" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./FeeInvoiceInputSave.jsp" method=post name="fm" target="fraSubmit">
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>发票与业务关联查询修改</td></tr>
    </table>
    
	<Div  id= "divCertifyList" style= "display: ''">
	    <table  class= common>
	       <TR  class= common8>
	        	<TD  class= title8 colSpan=2 >
	        		<input type="radio" name="typeRadio"  value="0" checked onclick="ChangeType();" >保单
	        		<input type="radio" name="typeRadio"  value="1" onclick="ChangeType();" >批单 
	        		<input type="radio" name="typeRadio"  value="2" onclick="ChangeType();" >结算单
	        	</td>
	        	<td class = Input colSpan=4 >
	        	   <font color="#FF0000" size="2">录入的发票号状态必须属于遗失、作废、销毁、正常回销
	        	   </font>
	        	</td>
	      </tr>
	      <TR  class= common8>
	        	<TD class= title8 id="ContNoID">保单号</td>
	        	<TD class= title8 id="EdorNoID" style= "display: 'none'" >批单号</td>
	        	<TD class= title8 id="PrtNoID" style= "display: 'none'" >结算单号</td>
	        	<TD class= input>
	        	   <Input class=common8 name=OtherNo >
	        	</TD>
	        	<TD class= title8>实收号</td>
	        	<TD class= input>
	        	   <Input class=common8 name=PayNo >
	        	</TD>
	        	<TD class= title8>发票号</td>
	        	<TD class= input>
	        	   <Input class=common8 name=InvoiceNo >
	        	</TD>
	      </tr>
	      <TR class = common8 >
           <TD class= common ><input class="cssButton" type="button" value="查  询" onclick="queryList()" > </TD>
        </TR>
      </table>  
      <table class= common>
        <tr class= common>
          <td text-align: left colSpan=1><span id="spanCertifyList" ></span>
         	</td>
       	</tr>
      </table>
	  	<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="turnPage1.lastPage();">
      <table class = common>
      <TR class = common>
           <TD class= common><input class="cssButton" type="button" value="修  改" onclick="submitForm()" > </TD>
      </TR>				
      </table>
	</div>
		
	<input type="hidden" name = "CertifyCode" value="<%=CertifyCode%>" >
	<input type="hidden" name = "ManageCom" value="<%=ManageCom%>" >
	<input type="hidden" name = "Operator" value="<%=Operator%>">
	<input type="hidden" name="AuditKind" value="">
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   
  </form>
</body>
</html>
