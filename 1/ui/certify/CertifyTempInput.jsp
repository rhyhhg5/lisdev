<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：暂收收据核销
//程序功能：
//创建日期：2006-06-26
//创建人  ：Zhangbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertifyTempInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyTempInit.jsp"%>
</head>
<body  onload="initForm();initElementtype()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CertifyTempSave.jsp" method=post name="fm" target="fraSubmit">
  	
	
	<div style="width:200"><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common">
          	<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divTempInvoiceSave);">
          </td>
          <td class="titleImg">
          	团险暂收收据核销
          </td>
        </tr>
     	</table>
	</div>
	<div id="divTempInvoiceSave">
		<table class="common">
        <tr class="common">
        	<td class="title">收据方名称</td>
          <td class="input" colspan=3 >
          	<Input class="common" name=ReceptHeading style="width:95%" elementtype=nacessary> 
        	</td>
          
         	<td class="title">投保单号</td>
         	<td class="input"><input class="common" name="ContNo" elementtype=nacessary> </td>
        </tr>
        
        <tr class="common">
         	<td class="title">金额</td>
         	<td class="input">
         		<input class="common" name="PayMoney"  elementtype=nacessary>
        	</td>
					
					<td class="title">单证领用人</td>
          <td class="input">
          	<input type="text" class="common" name=AgentCode elementtype=nacessary>
          </td>
          <td class="title">
          	<input type="button" class="button" name=AgentQuery value="领用人查询" onclick="queryAgent()">
          </td>
          <td class="input"></td>
       </tr>
          
        <tr class="common">
        	<td class="title">暂收据号</td>
          <td class="input">
          	<input type="text" class="common" name=TempFeeNo  elementtype=nacessary>
          </td>
        	
          <td class="title">开票日期</td>
          <td class="input">
          	<input class="coolDatePicker" dateFormat="short" name="OpenDate" >
          </td>
          
         	<td class="title"></td>
         	<td class="input">
         		<input name="ManageCom" value="<%=strComCode%>" class="readonly" readonly style="display:none">
        	</td>
         	
       </tr>
   </table>
  </div>
  <input class="cssButton" type="button" value="核&nbsp;&nbsp;&nbsp;&nbsp;销" onclick="submitForm()" >
  
	<br><hr>
	
	<div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common">
          	<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divTempInvoiceSearch);">
          </td>
          <td class="titleImg">
          	团险暂收收据核销查询
          </td>
        </tr>
     	</table>
	</div>
	
	<div id="divTempInvoiceSearch">
		<table class="common">
        <tr class="common">
        	<td class="title">收据方名称</td>
          <td class="input" colspan=3 >
          	<Input class="common" name="ReceptUserName1" style="width:98%" > 
        	</td>
          
         	<td class="title">投保单号</td>
         	<td class="input"><input class="common" name="ContNo1"> </td>
        </tr>
        
        <tr class="common">
         	<td class="title">暂收据号</td>
          <td class="input">
          	<input type="text" class="common" name="TempFeeNo1" >
          </td>
					<td class="title">单证领用人</td>
          <td class="input">
          	<input type="text" class="common" name="AgentCode1" >
          </td>
          <td class="title">
          	<input type="button" class="button" value="领用人查询" onclick="queryAgent1()">
          </td>
          <td class="input"></td>
       </tr>
       <tr>
          <td class="title">开票起期</td>
          <td class="input">
          	<input class="coolDatePicker" dateFormat="short" name="StartDate">
          </td>
          
         	<td class="title">开票止期</td>
          <td class="input">
          	<input class="coolDatePicker" dateFormat="short" name="EndDate">
          </td>
         	
        	<td class="title"></td>
         	<td class="input">
        	</td>
       </tr>
   </table>
  </div>
  
	<input class="cssButton" type="button" value="查&nbsp;&nbsp;&nbsp;&nbsp;询" onclick="auditCancel()" >
	<input class="cssButton" type="button" value="重&nbsp;&nbsp;&nbsp;&nbsp;置" onclick="resetSearch()" >
	
	
	<div id="divShowStore" style="display:''">
			<div id="divCertifyStroe">
	      <table class="common">
	        <tr class="common">
	          <td text-align: left colSpan=1><span id="spanTempFeeGrid"></span></td></tr>
		  	</table>
			</div>
			<Div id= "divPage" align=center style= "display:''">
			  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage.firstPage();"> 
			  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage.lastPage();">
	  	</Div>
  	</div>
	
	<input name="Operator" type="hidden">
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   
  </form>
</body>
</html>
