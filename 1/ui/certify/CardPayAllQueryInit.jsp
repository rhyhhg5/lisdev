<%
//Creator :刘岩松
//Date :2003-04-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('AgentName').value = '';
    fm.all('AgeComName').value = ''; 
    fm.all('ManageCom').value = '<%=tGI.ComCode%>';    
     
    
  }
  catch(ex)
  {
    alert("进行初始化时出现错误！！！！");
  }
}


// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CardPayInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCardPayGrid();
  }
  catch(re)
  {
    alert("3CardPayInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCardPayGrid()
{
	var iArray = new Array();
      
      try
      {
	      iArray[0]=new Array();
		    iArray[0][0]="序号";
		    iArray[0][1]="30px";
		    iArray[0][2]=10;
		    iArray[0][3]=0;
		
		    iArray[1]=new Array();
		    iArray[1][0]="起始单号";    	  //列名
		    iArray[1][1]="70";            	//列宽
		    iArray[1][2]=180;            		//列最大值
		    iArray[1][3]=1;              		//是否允许输入,1表示允许，0表示不允许
		    iArray[1][9]="起始单号|INT";
		
		    iArray[2]=new Array();
		    iArray[2][0]="终止单号";    	  //列名
		    iArray[2][1]="70";            	//列宽
		    iArray[2][2]=180;            		//列最大值
		    iArray[2][3]=1;              		//是否允许输入,1表示允许，0表示不允许
		    iArray[2][7]="countNum"  	  		//JS函数名，不加扩号 !!响应事件!!
		    iArray[2][9]="终止单号|INT";
		
		    iArray[3]=new Array();
		    iArray[3][0]="数量";    	      //列名
		    iArray[3][1]="50";            	//列宽
		    iArray[3][2]=50;            		//列最大值
		    iArray[3][3]=1;              		//是否允许输入,1表示允许，0表示不允许
		    iArray[3][7]="countNum"  	  		//JS函数名，不加扩号 !!响应事件!!
		    iArray[3][9]="数量|INT";
		    
		    iArray[4]=new Array();
		    iArray[4][0]="结算单号";    	  //列名
		    iArray[4][1]="100";            	//列宽
		    iArray[4][2]=180;            		//列最大值
		    iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许		   
		
		    iArray[5]=new Array();
		    iArray[5][0]="单证类型";    	      //列名
		    iArray[5][1]="50";            	//列宽
		    iArray[5][2]=50;            		//列最大值
		    iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许		
		    
		    iArray[6]=new Array();
		    iArray[6][0]="回销人代码";    	      //列名
		    iArray[6][1]="80";            	//列宽
		    iArray[6][2]=60;            		//列最大值
		    iArray[6][3]=0;              		//是否允许输入,1表示允许，0表示不允许		   
		    
		    iArray[7]=new Array();
		    iArray[7][0]="回销人名称";    	      //列名
		    iArray[7][1]="80";            	//列宽
		    iArray[7][2]=50;            		//列最大值
		    iArray[7][3]=0;              		//是否允许输入,1表示允许，0表示不允许		      
		  
		    iArray[8]=new Array();
		    iArray[8][0]="代理机构代码";    	      //列名
		    iArray[8][1]="90";            	//列宽
		    iArray[8][2]=50;            		//列最大值
		    iArray[8][3]=0;              		//是否允许输入,1表示允许，0表示不允许		   
		    
		    iArray[9]=new Array();
		    iArray[9][0]="代理机构名称";    	      //列名
		    iArray[9][1]="180";            	//列宽
		    iArray[9][2]=50;            		//列最大值
		    iArray[9][3]=0;              		//是否允许输入,1表示允许，0表示不允许
		    
		    iArray[10]=new Array();
		    iArray[10][0]="确认状态";    	      //列名
		    iArray[10][1]="60";            	//列宽
		    iArray[10][2]=50;            		//列最大值
		    iArray[10][3]=0;              		//是否允许输入,1表示允许，0表示不允许
		    		      
	      CardPayGrid = new MulLineEnter( "fm" , "CardPayGrid" ); 
	      CardPayGrid.mulLineCount = 0;   
	      CardPayGrid.displayTitle = 1;	      
	      
	      CardPayGrid.locked = 1;	
		    CardPayGrid.hiddenPlus = 1;
		    CardPayGrid.hiddenSubtraction = 1;
	      CardPayGrid.loadMulLine(iArray);  
	      CardPayGrid.detailInfo="单击显示详细信息";
	      //CardPayGrid.detailClick=reportDetailClick;
     
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>