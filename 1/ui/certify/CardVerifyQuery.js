var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
turnPage1.pageDivName = "divPage1";

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
 }
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}

//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{
    if(fm.CardType.value=="" ){
        alert("请输入单证类别的值");
        return;
    }
    // 初始化表格
    initCertifyMaxGrid();
    
    var state = "";
    if(fm.StateType.value == "1")
        state = "and a.State in ('10','11') ";
    else if(fm.StateType.value == "2")
        state = "and a.State in ('2','12','4','5','6','3') ";
    else if(fm.StateType.value == "3")
        state = "and a.State in ('10','11','2','12','4','5','6','3') ";
    
    var agent = "";
    if(fm.ReceiveCom.value != "")
        agent = "and exists (select 1 from LZCardTrack c where a.CertifyCode = c.CertifyCode and a.StartNo = c.StartNo and c.State in ('10', '11') and substr(c.ReceiveCom,2) = '" + fm.ReceiveCom.value + "')";
    
    var tHandleDate ="";
    if (fm.StartDate.value!=null&&fm.StartDate.value!="") {
    	tHandleDate += " and a.HandleDate >= trim('"+fm.StartDate.value+"') ";
    }
    if (fm.EndDate.value!=null&&fm.EndDate.value!="") {
    	tHandleDate += " and a.HandleDate <= trim('"+fm.EndDate.value+"') ";
    }
    
    var strSQL = "select distinct b.CardNo, "
        + "case when a.State in ('2','12','4','5','6','3') then '已回销' else '未回销' end, "
        + "(select (select Name from LAAgent where AgentCode = substr(c.ReceiveCom,2) union all select Name from LACom where AgentCom = substr(c.ReceiveCom,2)) "
        + "from LZCardTrack c where a.CertifyCode = c.CertifyCode and a.StartNo = c.StartNo and c.State in ('10', '11') and a.State in ('2','12','4','5','6') order by c.MakeDate desc fetch first 1 rows only), "
        + "HandleDate "
        + "from LZCard a, LZCardNumber b "
        + "where a.SubCode = b.CardType and trim(a.StartNo) <= b.CardSerNo and trim(a.EndNo) >= b.CardSerNo "
        + state
        + agent
        + getWherePart('a.SubCode','CardType')
        + getWherePart('b.CardSerNo','StartNo','>=')
        + getWherePart('b.CardSerNo','EndNo','<=')
        + tHandleDate
        //+ getWherePart('a.HandleDate','StartDate','>=')
        //+ getWherePart('a.HandleDate','EndDate','<=')
        + "order by CardNo with ur";
    turnPage.queryModal(strSQL, CertifyMaxGrid);
    fm.SearchSQL.value = strSQL;
    
    var strStateSQL = "select a.certifycode,'未回销',sum(sumcount)"
        + "from LZCard a "
        + "where a.State in ('10','11') "
        + state
        + agent
        + getWherePart('a.SubCode','CardType')
        + getWherePart('b.CardSerNo','StartNo','>=')
        + getWherePart('b.CardSerNo','EndNo','<=')
        + getWherePart('a.HandleDate','StartDate','>=')
        + getWherePart('a.HandleDate','EndDate','<=')
        + "group by a.certifycode";
    strStateSQL += " union select a.certifycode,'已回销',sum(sumcount)"
        + "from LZCard a "
        + "where a.State in ('2','12','4','5','6','3') "
        + state
        + agent
        + getWherePart('a.SubCode','CardType')
        + getWherePart('b.CardSerNo','StartNo','>=')
        + getWherePart('b.CardSerNo','EndNo','<=')
        + getWherePart('a.HandleDate','StartDate','>=')
        + getWherePart('a.HandleDate','EndDate','<=')
        + "group by a.certifycode with ur";
    turnPage1.queryModal(strStateSQL, CardState);
}

//更新数据的函数
function printInfo()
{
  var i = 0;
  if(CertifyMaxGrid.mulLineCount==0){
  	alert("列表缺少数据,请先查询!!!");
  	return ;
  }
  if (fm.PubSQL.value== "" || fm.PubSQL.value == null){
     alert("请先查询数据!!!");
     return false;
  }
  fm.ReportType.value="2";
  window.open("../f1print/CardSendReportPrint.jsp?StartDate="+fm.StartDate.value+"&EndDate="+fm.EndDate.value+"&ReportType="+fm.ReportType.value+"&ReceiveCom="+fm.ReceiveCom.value+"&CardType="+fm.CardType.value+"&StateType="+fm.StateType.value+"&ManCom="+managecom);

}

function showalert(){
	alert("没有符合条件的数据！");
}

/**
 * 将字符串补数,将sourString的<br>前面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理
 * <p><b>Example: </b><p>
 * <p>LCh("Minim", "0", 10) returns "00000Minim"<p>
 * @param sourString 源字符串
 * @param cChar 补数用的字符
 * @param cLen 字符串的目标长度
 * @return 字符串
 */
function LCh(sourString, cChar,cLen) {

	 var sourString = sourString+ "";
   var tLen = sourString.length;

   var i=0;
   var iMax=0;
   var tReturn = "";
   if (tLen >= cLen) {
       return sourString;
   }
   iMax = cLen - tLen;
   for (i = 0; i < iMax; i++) {
       tReturn = cChar + tReturn;
   }
   tReturn = trim(tReturn) + trim(sourString);
   return tReturn;
}

function printList()
{
	if (fm.SearchSQL.value=="") {
		alert("请先进行查询");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "CardVerifyQueryPrint";
  fm.action = "CardVerifyQueryPrintSave.jsp";
	fm.submit();
  showInfo.close();
}