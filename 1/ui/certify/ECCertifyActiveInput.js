//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm(){ 
	if(checkInput()){
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    	//fm.Operate.value = "new";    
  		fm.action = "ECCardActiveSave.jsp";
    	fm.submit(); //提交
	}  

}

function checkInput(){
	
	if(!checkContInfo()){
		return false;
	}
	if(!checkAppntInfo()){
		return false;
	}
	if(!checkInsurInfo()){
		return false;
	}
	return true;
}

function checkContInfo(){
	cardno = fm.cardNum.value;
	cardno = cardno.replace(/[ ]/g,"");
	pass = fm.cardPassword.value;	
	pass = pass.replace(/[ ]/g,"");
	cvalidate = fm.cvalidate.value;	
	cvalidate = cvalidate.replace(/[ ]/g,"");
	mult = fm.mult.value;	
	mult = mult.replace(/[ ]/g,"");
	if(cardno =='' || cardno ==null || pass == '' || pass == null){
		alert("卡号或密码为空");
		return false;
	}
	var sql="select 1 from lzcardnumber where cardno='"+cardno+"'";
			
	rs = easyExecSql(sql);
	if(rs == null){
		alert("卡号不存在");
		return false;
	}
	if(rs[0][0] !="1"){
		alert("卡号不存在");
		return false;
	}
	if(cvalidate =='' || cvalidate ==null){
		alert("请输入生效日期");
		return false;
	}
	if(mult =='' || mult ==null){
		alert("请输入档次");
		return false;
	}
	return true;
}

function checkAppntInfo(){
	appname = fm.appName.value;
	appidtype = fm.appIdType.value;
	appsex = fm.appSex.value;
	appidno = fm.appIdNo.value;
	appbirthday = fm.appBirthday.value;
	
	appname = appname.replace(/[ ]/g,"");
	appidno = appidno.replace(/[ ]/g,"");
	appbirthday = appbirthday.replace(/[ ]/g,"");
	
	if(appname == null || appname==''){
		alert("请输入投保人姓名");
		return false;
	}
	
	if(appidtype == null || appidtype==''){
		alert("请选择投保人证件类型");
		return false;
	}
	
	if(appsex == null || appsex==''){
		alert("请选择投保人性别");
		return false;
	}
	
	if(appidno == null || appidno==''){
		alert("请输入被保人证件号码");
		return false;
	}
	
	if(appbirthday == null || appbirthday==''){
		alert("请输入投保人出生日期");
		return false;
	}

	return true;
}

function checkInsurInfo(){
	var i = CertifyMaxGrid. mulLineCount;
	if(i<1){
		alert("请添加被保人信息");
		return false;
	}
	return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
 
  }

}


//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}




//确定按钮
function printConfirm()
{
  var endNo = LCh((parseInt(fm.all('StartSerNo').value)+ parseInt(fm.all('CardNum').value) - 1) + "",'0',fm.all('CertifyLength').value) ;
 	var sql = "select count(*) from lzcardnumber where  CardType='" + fm.all('CardType').value + "' and CardSerNo>='" + LCh(fm.all('StartSerNo').value,"0",fm.all('CertifyLength').value) + "'"  　
  + " and CardSerNo<='"+endNo +"'";
  var rs = easyExecSql(sql);    

  if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != 0)
  { 
  	 showInfo.close();
  	 alert("该范围的号码已生成！！！");
	   return false;   
  }  	
   return true;
  	
}

