<%
	// 防止IE缓存页面
	response.setHeader("Pragma","No-cache"); 
	response.setHeader("Cache-Control","no-cache"); 
	response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=GBK" %>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertifyAllSendOutInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyAllSendOutInit.jsp"%>
</head>

<body onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CertifyAllSendOutSave.jsp" method="post" name=fm target="fraSubmit">
    <table class="common">
    	<tr class="common">
    		<td class="input">
    			<input name="btnOp" class="cssButton" type="button" value="发放单证" onclick="submitForm()">
    			<!-- <input type="button" name="btnCertify" class="cssButton" value="单证信息查询" onclick="queryCertify()"> -->
    		</td>
    	</tr>
    </table>
     <!-- 发放的信息 -->
    <div style="width:120"><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSendOutInfo);"></td>
          <td class="titleImg">发放信息</td></tr></table></div>

    <div id="divSendOutInfo">
      <table class="common">
        <tr class="common">
          <td class="title">发放者</td>
          <td class="input">
          	<input class="codeno" name="SendOutComEx" verify="发放者|NOTNULL"
          	ondblclick="return showCodeList('sendoutcode', [this,SendOutName],[0,1],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcode', [this,SendOutName],[0,1],null,null,null,1);"
          	><input name = "SendOutName" class=codename type="text">
          	<input type="hidden" name="SendOutCom">
          </td>
			
			<td class="title">接收者</td>
          <td class="input">
          	<input class="codeno" name="ReceiveCom" verify="接收者|NOTNULL"
          	ondblclick="return showCodeList('dzwd', [this,ReceiveAgentName],[0,1],null,null,null,1);"
            onkeyup="return showCodeListKey('dzwd', [this,ReceiveAgentName],[0,1],null,null,null,1);"
          	><input name = "ReceiveAgentName" class=codename  readonly="readonly">
          </td>
          </tr>
        <tr class="common">
          <td class="title">经办人</td>
          <td class="input"><input class="common" name="Handler"></td>

          <td class="title">经办日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDate"></td></tr>

        <tr class="common">
          <td class="title">操作员</td>
          <td class="input"><input class="readonly" readonly name="Operator"></td>

          <td class="title">当前时间</td>
          <td class="input"><input class="readonly" readonly name="curTime"></td></tr>
      </table>
    </div>
    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>单证列表</td></tr>
    </table>

		<div id="divCertifyList">
      <table class="common">
        <tr class="common">
          <td text-align: left colSpan=1><span id="spanCertifyList"></span></td></tr>
	  	</table>
		</div>
		
		<br><hr><br>
		
		<table>
   	  <tr>
        <td class=common>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divShowStore);">
        </td>
    	<td class= titleImg>现有单证信息</td></tr>
    </table>
		
		<div id="divShowStore" style="display:''">
	    <table class="common">
	      <tr class="common">
	        <td text-align: left colSpan=1><span id="spanCertifyStoreList"></span></td></tr>
			</table>
		</div>
		<Div id= "divPage" align=center style= "display:''">
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage.lastPage();">
	  </Div>
	  
			<table class="common">
		    <tr class="common">
		    	<td class="title"></td><td class="input"></td><td class="title"></td>
		      <td class="input"></td>
		      <td class="title">单证总数量</td>
		      <td class="input"><input class="common" name="certifysumcount"></td>
		      <td class="title"></td>
		      <td class="input"></td>
		    </tr>
		  </table>
	  </div>	  	  
		<input type=hidden name="sql_where">
		<input type=hidden name="LimitFlag">
		<input type=hidden name="ReceiveType" value="AGECOM"><!-- 原来是COM -->
		<input type=hidden name="ManageCom" value="<%=strCom%>">
		<input type='hidden' name='ReceivePage'  value='SENDDEL'>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
    
    
