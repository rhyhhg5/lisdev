var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
turnPage1.pageDivName = "divPage1";

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
 }
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{
    if(fm.ManageCom.value=="" || fm.EndDate.value=="" ||fm.AgentCode.value==""){
        alert("请选择管理机构、查询主体、查询止期!");
        return;
    }
    
    initCertifyMaxGrid();
    var strSQL=querySQL("1");
    /*
    var state = "";
    if(fm.StateType.value == ""){
        state = " and a.State in ('0','2','3','4','5','6','8','9','10','11','12','13','14') ";
    }
    else if(fm.StateType.value == "0"){
        state = " and a.State in ('0','8') ";
    }
    else if(fm.StateType.value == "1"){
        state = " and a.State in ('10','11') ";
    }
    else if(fm.StateType.value == "7") {
    	  state = " and a.State = '9' ";
    }
    else{
        state = " and a.State = '" + fm.StateType.value + "' ";
    }
    
    var handledate="";
    if(fm.StartDate.value != null && fm.StartDate.value != ""){
        handledate=" and a.HandleDate >= '"+fm.StartDate.value+"' ";
    }
    
    var strSQL = "select substr(a.ReceiveCom, 2), "
        + " a.certifycode, "
        + " (select lra.riskname from lmriskapp lra where lra.riskcode=(select lcd.riskcode from lmcardrisk lcd where lcd.certifycode=a.certifycode fetch first 1 row only )), "
        + " (select CodeName from LDCode where CodeType='certifystate' and Code = a.State), "
        + " sum(a.sumcount) "
        + " from LZCard a where 1=1 "
        + getWherePart('a.ReceiveCom', 'AgentCode')
        + getWherePart('a.HandleDate','EndDate','<=')
        + handledate
        + state
        + " and exists (select 1 from lmcertifydes where managecom ='"+fm.ManageCom.value+"') "
        + " group by certifycode,receivecom,state with ur ";*/
  fm.PubSQL.value = strSQL;
  //alert("fm.PubSQL.value = strSQL:"+strSQL);
  turnPage.queryModal(strSQL, CertifyMaxGrid);
	/*
	var strStateSQL = "select certifycode,startno,endno,sumcount,substr(a.ReceiveCom, 2) "
        + "from LZCard a where 1=1 "
        + getWherePart('a.ReceiveCom', 'AgentCode')
        + getWherePart('a.HandleDate','EndDate','<=')
        + handledate
        + state
        + " and exists (select 1 from lmcertifydes where managecom ='"+fm.ManageCom.value+"') "
        + "order by certifycode,startno " ;*/
  //alert("strStateSQL:"+strStateSQL);
        var strStateSQL=querySQL("2");
  turnPage1.queryModal(strStateSQL, CardState);
}

//更新数据的函数
function printInfo()
{
 	 
   if (verifyInput() == false)
    return false;
   
   	 
    if( !beforeSubmit())
   {
   	return false;
   	
   	}	

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "fraSubmit";
	fm.all('op').value = '';
	
    if(fm.ManageCom.value=="" || fm.EndDate.value=="" ||fm.AgentCode.value==""){
        alert("请选择管理机构、查询主体、查询止期!");
        return;
    }    
    var strSQL=querySQL("1");
    fm.all('querySQL').value=strSQL;	
	fm.submit();
	showInfo.close();	
}

function showalert(){
	alert("没有符合条件的数据！");
}
function beforeSubmit(){

  return true;
}
function queryAgent()
{	
	fm.ReceiveType.value="AGE";
  if(fm.all('AgentCode').value == "")	
  {  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
      fm.AgentCode.value='D'+arrResult[0][0];
      fm.ReceiveCom.value='D'+arrResult[0][0];
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}
function queryCom()
{
    fm.ReceiveType.value="AGECOM";
    showInfo = window.open("./LAComMain.jsp?ManageCom=" + fm.all('ManageCom').value.substr(0, 4) + "&BranchType=3&BranchType2=01&BankType=01");
}

function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	if( fm.chkModeBatch.checked == false ) {
	    fm.PrtNoEx.value = arrResult[0][0];
	    fm.ReceiveCom.value = 'A' + arrResult[0][11];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
	    
			var rowCount = 0;

	    CertifyList.setRowColData(rowCount, 1, arrResult[0][1]);
	    CertifyList.setRowColData(rowCount, 2, arrResult[0][25]);
	    CertifyList.setRowColData(rowCount, 3, arrResult[0][18]);
	    CertifyList.setRowColData(rowCount, 4, arrResult[0][19]);
	    
	    var cNum = parseFloat(arrResult[0][19])-parseFloat(arrResult[0][18])+1;
	    CertifyList.setRowColData(rowCount, 5, cNum+"");
	    
	  } else {
	    fm.ReceiveCom.value = arrResult[0][3];
	    fm.ReceiveCom.title = arrResult[0][1];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
		}
  }
}


function querySQL(type){
    var strSQL="";
    var state = "";
    var handledate="";
    var tCertifyCode="";
    var tCertifyCode=fm.all("CertifyCode").value;
    //alert(tCertifyCode);
    if(tCertifyCode!=null && tCertifyCode!=""){
       mydiv.style.display="";
       tCertifyCode=" and CertifyCode='"+fm.CertifyCode.value+"' "      
    }else{
       mydiv.style.display="none";
    }
    if(fm.StartDate.value != null && fm.StartDate.value != ""){
        handledate=" and a.HandleDate >= '"+fm.StartDate.value+"' ";
    }
    if(fm.StateType.value == ""){
        state = " and a.State in ('0','2','3','4','5','6','8','9','10','11','12','13','14') ";
    }
    else if(fm.StateType.value == "0"){
        state = " and a.State in ('0','8') ";
    }
    else if(fm.StateType.value == "1"){
        state = " and a.State in ('10','11') ";
    }
    else if(fm.StateType.value == "7") {
    	  state = " and a.State = '9' ";
    }
    else{
        state = " and a.State = '" + fm.StateType.value + "' ";
    }      
    if(type == "1"){           
       strSQL = "select (case when exists (select 1 from laagent where agentcode = substr(a.ReceiveCom, 2)) then (select GroupAgentCode from laagent where agentcode = substr(a.ReceiveCom, 2)) else substr(a.ReceiveCom, 2) end) , "
        + " a.certifycode, "
        + " (select lra.riskname from lmriskapp lra where lra.riskcode=(select lcd.riskcode from lmcardrisk lcd where lcd.certifycode=a.certifycode fetch first 1 row only )), "
        + " (select CodeName from LDCode where CodeType='certifystate' and Code = a.State), "
        + " sum(a.sumcount) "
        + " from LZCard a where 1=1 "
        + getWherePart('a.ReceiveCom', 'AgentCode')
        + getWherePart('a.HandleDate','EndDate','<=')
        + handledate
        + state
        + tCertifyCode
        + " and exists (select 1 from lmcertifydes where managecom ='"+fm.ManageCom.value+"') "
        + " group by certifycode,receivecom,state with ur ";
    }
    if(type == "2"){
         strSQL = "select certifycode,startno,endno,sumcount,(case when exists (select 1 from laagent where agentcode = substr(a.ReceiveCom, 2)) then (select GroupAgentCode from laagent where agentcode = substr(a.ReceiveCom, 2)) else substr(a.ReceiveCom, 2) end) "
        + "from LZCard a where 1=1 "
        + getWherePart('a.ReceiveCom', 'AgentCode')
        + getWherePart('a.HandleDate','EndDate','<=')
        + handledate
        + state
        + tCertifyCode
        + " and exists (select 1 from lmcertifydes where managecom ='"+fm.ManageCom.value+"') "
        + "order by certifycode,startno " ;
    }
    return strSQL;


}
function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  	if (fm.ReceiveType.value=="AGE") {
  		fm.AgentCode.value='D'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][95];
  	} else if (fm.ReceiveType.value=="AGECOM") {
  		fm.AgentCode.value='E'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][0];
  	}
	}
}
