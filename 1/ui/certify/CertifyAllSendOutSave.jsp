<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：CertifySendOutSave.jsp
//程序功能：
//创建日期：2002-09-23
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%
//输出参数
String strTakeBackNo = "";
String FlagStr = "Fail";
String Content = "";
boolean bContinue = true;
String szFailSet = "";
String certifyCode = "aa";

GlobalInput globalInput = new GlobalInput( );
globalInput.setSchema( (GlobalInput)session.getValue("GI") );

//校验处理
//内容待填充
	// 单证信息部分
	try{
	String szReceiveCom	= request.getParameter("ReceiveCom"); //接收者
	String szSendOutCom 	= request.getParameter("SendOutCom");
	System.out.println(szSendOutCom);
    String szReceiveType		= request.getParameter("ReceiveType"); //接收者类型
	String szNo[]					= request.getParameterValues("CertifyListNo");
	String szCertifyCode[]= request.getParameterValues("CertifyList1");
	String szStartNo[]		= request.getParameterValues("CertifyList3");
	String szEndNo[]			= request.getParameterValues("CertifyList4");
	String szSumCount[]   = request.getParameterValues("CertifyList5");
	int nIndex = 0;
      //单证信 息
	
	LZCardPrintSet setLZCardPrint = new LZCardPrintSet( );
        for( nIndex = 0; nIndex < szNo.length; nIndex ++ ) {      
          LZCardSchema schemaLZCard = new LZCardSchema( );
          // 接收者信息
         String tSql="select d.code,f.code1, f.codename from ldcode1 f inner join ldcode d  on f.code=d.code where d.codetype = 'dzwd' and f.code='"+szReceiveCom+"' and f.codetype = 'certifyagentcomsend'";
         SSRS tSSRS = new ExeSQL().execSQL(tSql);
         int jianGeNum=Integer.valueOf(szSumCount[nIndex]).intValue();
         String  tStartNo  = szStartNo[nIndex];
         String  tEndNo =szStartNo[nIndex];//在CommonCertify中的getEndNo是一起始号为参数的
         for(int i=1;i<=tSSRS.MaxRow;i++){
        	 LZCardSet setLZCard = new LZCardSet( );
            String tempStartNo = CommonCertify.getStartNo(tStartNo,i,jianGeNum);
            System.out.println(tempStartNo);
            schemaLZCard.setStartNo(tempStartNo);
            String tempEndNo=CommonCertify.getEndNo(tEndNo,i,jianGeNum);
            System.out.println("终止号="+tempEndNo);
            schemaLZCard.setEndNo(tempEndNo);
            
            if (szSendOutCom.substring(0,2).equals("86"))
            {
                schemaLZCard.setSendOutCom("A"+szSendOutCom);
            }
            else
            {
                schemaLZCard.setSendOutCom("B"+szSendOutCom);
            }
            
            schemaLZCard.setReceiveCom("E"+tSSRS.GetText(i,2));//+wd
            schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
            certifyCode=szCertifyCode[nIndex];
            schemaLZCard.setSubCode("");
            schemaLZCard.setRiskCode("");
            schemaLZCard.setRiskVersion("");
            schemaLZCard.setSumCount(szSumCount[nIndex]);
            schemaLZCard.setPrem("");            
            schemaLZCard.setTakeBackNo("");
            schemaLZCard.setSaleChnl("");
            schemaLZCard.setStateFlag("");
            schemaLZCard.setOperateFlag("");
            schemaLZCard.setPayFlag("");
            schemaLZCard.setEnterAccFlag("");
            schemaLZCard.setReason("");
            schemaLZCard.setState("");
            schemaLZCard.setOperator("");
            schemaLZCard.setMakeDate("");
            schemaLZCard.setMakeTime("");
            schemaLZCard.setModifyDate("");
            schemaLZCard.setModifyTime("");
            schemaLZCard.setHandler(globalInput.Operator);
            schemaLZCard.setHandleDate(PubFun.getCurrentDate());
            setLZCard.add(schemaLZCard);   
            
            
         // 准备传输数据 VData
      	  VData vData = new VData();
      	  vData.addElement(globalInput);	
      	  vData.addElement(setLZCard);
      	  Hashtable hashParams = new Hashtable();
      	  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
      	  vData.addElement(hashParams);
      		// 设置操作字符串
      	  String szOperator = "INSERT";

      	  // 数据传输
      	  CertSendOutUI tCertSendOutUI = new CertSendOutUI();

      	  if (!tCertSendOutUI.submitData(vData, szOperator)) {
      	    Content = " 保存失败，原因是: " + tCertSendOutUI.mErrors.getFirstError();
      	    FlagStr = "Fail";

      	    vData = tCertSendOutUI.getResult();

      			strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
      			session.setAttribute("TakeBackNo", strTakeBackNo);

      	    setLZCard = (LZCardSet)vData.getObjectByObjectName("LZCardSet", 0);

      			szFailSet = "parent.fraInterface.CertifyList.clearData();\r\n";
      	    for(nIndex = 0; nIndex < setLZCard.size(); nIndex ++) {
      	    	LZCardSchema tLZCardSchema = setLZCard.get(nIndex + 1);

      	    	szFailSet += "parent.fraInterface.CertifyList.addOne();\r\n";
      	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
      	    	  ", 1, '" + tLZCardSchema.getCertifyCode() + "');\r\n";
      	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
      	    	  ", 3, '" + tLZCardSchema.getStartNo() + "');\r\n";
      	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
      	    	  ", 4, '" + tLZCardSchema.getEndNo() + "');\r\n";
      	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
      	    	  ", 5, '');\r\n";
      	    }

      	  } else {
      	  	    Content = " 保存成功 ";
      	    	FlagStr = "Succ";
      		    vData = tCertSendOutUI.getResult();
      			strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
      			session.setAttribute("TakeBackNo", strTakeBackNo);
      			session.setAttribute("State", CertStatBL.PRT_STATE);
      	}
              }   
    }
 
	  
	}catch(Exception ex) 
	{
		ex.printStackTrace( );
   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   	FlagStr = "Fail";
	}
%>
<html>
	<script language="javascript">
<%= szFailSet %>
parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%= strTakeBackNo %>", "<%=certifyCode%>");
</script>
	<body>
	</body>
</html>