<%
//程序名称：CertifyTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.taskservice.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;
  String szFailSet = "";

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );

  //校验处理
  //内容待填充
  
  String auditType = request.getParameter("AuditKind");
  
  if (auditType.equals("BATCH")) //手动批量核销
  {
  	
  	AuditCancelTask auditCancelTask = new AuditCancelTask();
  	VData tVData = new VData();
  	tVData.addElement(globalInput);
  	
  	if(!auditCancelTask.auditOperate(tVData,"HANDLE"))
  	{
  		Content = " 批量核销未全部完成,请在系统不忙时再试！";
		  FlagStr = "Fail";
  	}
  	else
  	{
  		Content = " 批量核销成功 ";
		  FlagStr = "Succ";
  	}
  } 
	else if (auditType.equals("SINGLE")) //人工单证处理
	{
		try {
		  // 单证信息部分
		  //String szPrtNo				= request.getParameter("PrtNo");
		  //String szSendOutCom 	= request.getParameter("SendOutCom");
		  //String szReceiveCom		= request.getParameter("ReceiveCom");
		  //String szInvaliDate 	= request.getParameter("InvalidDate");
		  //String szAmnt 				= request.getParameter("Amnt");
		  //String szHandler			= request.getParameter("Handler");
		  //String szHandleDate		= request.getParameter("HandleDate");
		  
		  //String szNo[]					= request.getParameterValues("CertifyListNo");
		  String szCertifyCode[]	= request.getParameterValues("CertifyList1");
		  String szStartNo[]			= request.getParameterValues("CertifyList3");
		  String szEndNo[]				= request.getParameterValues("CertifyList4");
		  String szSumCount[]  		= request.getParameterValues("CertifyList5");
		  String szStateFlag[]		= request.getParameterValues("CertifyList7");
			System.out.println("in save() -> StateFlag: "+szStateFlag[0]); 
		  int nIndex;
		  
		  LZCardSet setLZCard = new LZCardSet( );
		  if( szCertifyCode == null ) {
		  	throw new Exception("没有输入要回收的起始单证号和终止单证号");
		  }
	
		  for( nIndex = 0; nIndex < szCertifyCode.length; nIndex ++ ) 
		  {
		    //if( szStateFlag == null || szStateFlag[nIndex] == null || szStateFlag[nIndex].equals("") ) {
		    //	throw new Exception("没有输入单证回收状态................");
		    //}
		    
		    LZCardSchema schemaLZCard = new LZCardSchema( );
		    
		    schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
				//schemaLZCard.setSubCode("");
				//schemaLZCard.setRiskCode("");
				//schemaLZCard.setRiskVersion("");
	      
		    schemaLZCard.setStartNo(szStartNo[nIndex].trim());
		    schemaLZCard.setEndNo(szEndNo[nIndex].trim());
				schemaLZCard.setStateFlag(szStateFlag[nIndex].trim());
				
		    if (globalInput.ComCode.length()<=4)
		    {
		    	schemaLZCard.setSendOutCom("A"+globalInput.ComCode);
		    }
		  	else
		  	{
		  		schemaLZCard.setSendOutCom("B"+globalInput.Operator);
		  	}
		    schemaLZCard.setReceiveCom("SYS");
		    
				schemaLZCard.setSumCount(szSumCount[nIndex]);
				//schemaLZCard.setPrem("");
		    //schemaLZCard.setAmnt(szAmnt);
		    schemaLZCard.setHandler(globalInput.Operator);
		    schemaLZCard.setHandleDate(PubFun.getCurrentDate());
		    //schemaLZCard.setInvaliDate(szInvaliDate);
	
				//schemaLZCard.setTakeBackNo("");
				//schemaLZCard.setSaleChnl("");
				//schemaLZCard.setOperateFlag("");
				//schemaLZCard.setPayFlag("");
				//schemaLZCard.setEnterAccFlag("");
				//schemaLZCard.setReason("");
				//schemaLZCard.setState("");
				//schemaLZCard.setOperator("");
				//schemaLZCard.setMakeDate("");
				//schemaLZCard.setMakeTime("");
				//schemaLZCard.setModifyDate("");
				//schemaLZCard.setModifyTime("");
					    
		    setLZCard.add(schemaLZCard);
		  }
			
		  // 准备传输数据 VData
		  VData vData = new VData();
		
		  vData.addElement(globalInput);
		  vData.addElement(setLZCard);
		  vData.addElement(auditType);
		  
		  Hashtable hashParams = new Hashtable();
		  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
		  vData.addElement(hashParams);
	
		  // 数据传输
		  AuditCancelBL tCertTakeBackUI = new AuditCancelBL();
	
		  if (!tCertTakeBackUI.submitData(vData, "INSERT")) {
		    Content = " 保存失败，原因是: " + tCertTakeBackUI.mErrors.getFirstError();
		    FlagStr = "Fail";
		    
		    vData = tCertTakeBackUI.getResult();
		    setLZCard = (LZCardSet)vData.getObjectByObjectName("LZCardSet", 0);
		    
				szFailSet = "parent.fraInterface.CertifyList.clearData();\r\n";
		    for(nIndex = 0; nIndex < setLZCard.size(); nIndex ++) 	//报错返回后填充父页面的mulLine
		    {
		    	LZCardSchema tLZCardSchema = setLZCard.get(nIndex + 1);
		    
		    	szFailSet += "parent.fraInterface.CertifyList.addOne();\r\n";
		    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	  ", 1, '" + tLZCardSchema.getCertifyCode() + "');\r\n";
		    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	  ", 3, '" + tLZCardSchema.getStartNo() + "');\r\n";
		    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	  ", 4, '" + tLZCardSchema.getEndNo() + "');\r\n";
		    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	  ", 5, '" + tLZCardSchema.getSumCount() + "');\r\n";
		    	  
		    	String stateFlag="";
		    	  
		    	if (tLZCardSchema.getStateFlag().equals("1"))
		    	{
		    		stateFlag="正常回收";
		    	}else  if (tLZCardSchema.getStateFlag().equals("2"))
		    	{
		    		stateFlag="作废";
		    	}else	if (tLZCardSchema.getStateFlag().equals("3"))
		    	{
		    		stateFlag="挂失";
		    	}else	if (tLZCardSchema.getStateFlag().equals("4"))
		    	{
		    		stateFlag="销毁";
		    	}else	if (tLZCardSchema.getStateFlag().equals("5"))
		    	{
		    		stateFlag="核销";
		    	}  
		    	
		    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +  
		    	  ", 6, '"+stateFlag+"');\r\n"; 
		    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	  ", 7, '" + tLZCardSchema.getStateFlag() + "');\r\n";  
		    }
	
		  } else {
		  	Content = " 保存成功 ";
		  	FlagStr = "Succ";
		  	
		  	vData = tCertTakeBackUI.getResult();
		  	String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
		  	session.setAttribute("TakeBackNo", strTakeBackNo);
	 			session.setAttribute("State", CertStatBL.PRT_STATE);
		  }
		} catch(Exception ex) {
			ex.printStackTrace( );
	   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
	   	FlagStr = "Fail";
		}
	}else //对错误日志处理
	{
		try 
		{
				Content = " 人工核销处理成功 ";
				FlagStr = "Succ"; 
				
				LZCardSet setLZCard = new LZCardSet( );
				
				int lineCount = 0;
				String tChk[] 				= request.getParameterValues("InpErrorLogGridChk");
				
				String szCertifyCode[]= request.getParameterValues("ErrorLogGrid1");
				String szStartNo[]		= request.getParameterValues("ErrorLogGrid3");
				String szEndNo[]			= request.getParameterValues("ErrorLogGrid3");
		
				lineCount=tChk.length; //行数
				for( int i=0; i<lineCount; i++ ) 
				{
					if(tChk[i].equals("0")) continue;
					
					LZCardSchema schemaLZCard = new LZCardSchema( );
				  
				  schemaLZCard.setCertifyCode(szCertifyCode[i]);	    
				  schemaLZCard.setStartNo(szStartNo[i].trim());
				  schemaLZCard.setEndNo(szEndNo[i].trim());
				  schemaLZCard.setStateFlag("6"); //对错误日志的核销StateFlag置为"6"
				  
				  setLZCard.add(schemaLZCard);
				}
				
				 VData vData = new VData();
				 
				 vData.addElement(globalInput);
				 vData.addElement(setLZCard);
				 vData.addElement(auditType);
				 
				 Hashtable hashParams = new Hashtable();
				 hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
				 vData.addElement(hashParams);
				 
				 // 数据传输
				 AuditCancelBL tCertTakeBackUI = new AuditCancelBL();
			
				 if (!tCertTakeBackUI.submitData(vData, "INSERT")) 
				 {
				   Content = " 保存失败，原因是: " + tCertTakeBackUI.mErrors.getFirstError(); 
				   FlagStr = "Fail";
				 } else 
				 {
				 	/***************/
				 	String strSQL ="";
				 	ExeSQL tExeSQL = new ExeSQL();
				 	for( int i=0; i<lineCount; i++ ) 
					{
						if(tChk[i].equals("0")) continue;
					  
					  strSQL = "delete from LZCertifyErrorLog where certifycode='"+szCertifyCode[i]+"' and startno='"+szStartNo[i].trim()+"'";
System.out.println(strSQL);					  
					  if (!tExeSQL.execUpdateSQL(strSQL))
			      {
			        FlagStr="Succ";
			        Content="核销成功，但从错误日志表中删除数据时失败,下次批量核销时会将此日志记录删除！";
			      }
			      else
			      {
			        Content = " 核销成功! ";
				 			FlagStr = "Succ";
			      }
					  
					}
				 	
				 	
				 	/***************/
				 	vData = tCertTakeBackUI.getResult();
				 	String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
				 	session.setAttribute("TakeBackNo", strTakeBackNo);
			 		session.setAttribute("State", CertStatBL.PRT_STATE);
				 }	
		  }
		  catch (Exception ex)
		  {
		  	ex.printStackTrace( );
		   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
		   	FlagStr = "Fail";
		  }	
		
	}	
%>
<html>
  <script language="javascript">
  	<%= szFailSet %>
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
<body>
</body>
</html>

