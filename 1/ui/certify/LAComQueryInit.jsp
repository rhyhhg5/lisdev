<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
    try
    {                           
        fm.all('AgentCom').value = '';
        fm.all('ManageCom').value = '<%=ManageCom%>' ;
        fm.all('Name').value = '';
        fm.all('SellFlag').value = '';
        if("<%=BankType%>" != null && "<%=BankType%>" != "null")
        {
            fm.all('BankType').value = "<%=BankType%>";
        }
        if("<%=BranchType%>" != null && "<%=BranchType%>" != "null")
        {
            fm.all('BranchType').value = "<%=BranchType%>";
        }
        if("<%=BranchType2%>" != null && "<%=BranchType2%>" != "null")
        {
            fm.all('BranchType2').value = "<%=BranchType2%>";
        }
    }
    catch(ex)
    {
        alert("在LAComInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LAComInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initComGrid(); 
  }
  catch(re)
  {
    alert("LAComInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化ComGrid
 ************************************************************
 */
function initComGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="代理机构编码";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="代理机构名称";         //列名
    iArray[2][1]="100px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="电话";         //列名
    iArray[3][1]="100px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
 
    ComGrid = new MulLineEnter( "fm" , "ComGrid" ); 

    //这些属性必须在loadMulLine前
    ComGrid.mulLineCount = 0;   
    ComGrid.displayTitle = 1;
    ComGrid.canSel=1;
    ComGrid.hiddenPlus = 1;
    ComGrid.hiddenSubtraction = 1;
    ComGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
        alert("初始化ComGrid时出错："+ ex);
  }
}

</script>