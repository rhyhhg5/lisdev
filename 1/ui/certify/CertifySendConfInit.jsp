<%
//程序名称：CertifySendConInit.jsp
//程序功能：
//创建日期：2006-04-18
//创建人  ：Zhang Bin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	String strOperator = globalInput.Operator;
	String strComCode = globalInput.ComCode;
	String strCurDate = PubFun.getCurrentDate();
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
  	//fm.reset();
    //fm.Operator.value = '<%= strOperator %>';
    
    var dt = new Date( );
    
    fm.CurrentDate.value = '<%= strCurDate %>';
    //fm.ManageCom.value = '<%= strComCode%>';
    fm.ComCode.value = '<%= strComCode%>';
    //fm.ManageName.value =getManageName(<%= strComCode%>) ;
    //fm.Handler.value = '<%=strOperator%>';
    //fm.StartDate.value="";
    //fm.EndDate.value="";
  }
  catch(ex)
  {
    alert("在CertifyCancelInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initCertifyList();
    initCertifyShowGrid();
  }
  catch(re)
  {
    alert("CertifyCancelInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 单证列表的初始化
function initCertifyList()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		        //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30";        				      //列宽
      iArray[0][2]=50;          				      //列最大值
      iArray[0][3]=0;              			      //是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="发放者";     							//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        							//列宽
      iArray[1][2]=80;          							//列最大值
      iArray[1][3]=2;              						//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="certifysendoutcode";  		//是否引用代码:null||""为不引用
      iArray[1][5]="1|2";             				//引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";             				//上面的列中放置引用代码中第几位值
      //iArray[1][9]="发放者|code:CertifySendOutCode&NOTNULL";
      iArray[1][14]=<%=strComCode%>; 					//该列的初始化值
      iArray[1][15]= "ComCode";  							//要依赖的列的名称 
			iArray[1][17]="5"         							//MulLine中第5个字段作为参数,传入CodeQueryBL中,即参数在MulLine中的列号                     
			iArray[1][19]=1;   											//1是需要强制刷新.

      iArray[2]=new Array();
      iArray[2][0]="发放者名称";     					//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        		  				//列宽
      iArray[2][2]=180;          							//列最大值
      iArray[2][3]=0;              						//是否允许输入,1表示允许，0表示不允许
      iArray[2][14]=getManageName(<%=strComCode%>); //该列的初始化值

      iArray[3]=new Array();
      iArray[3][0]="接受者代码";    	        //列名
      iArray[3][1]="80";            		      //列宽
      iArray[3][2]=80;            			      //列最大值
      iArray[3][3]=2;              			      //是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="certifyreceivecode";      //是否引用代码:null||""为不引用
      iArray[3][5]="3|4|5";             	    //引用代码对应第几列，'|'为分割符
      iArray[3][6]="0|1|2";             	    //上面的列中放置引用代码中第几位值
			//iArray[3][9]="接受者代码|code:certifyreceivecode&NOTNULL";
      iArray[3][15]= "ComCode";  				      //要依赖的列的名称 
			iArray[3][17]="5"         				      //MulLine中第5个字段作为参数,传入CodeQueryBL中,即参数在MulLine中的列号                     
			iArray[3][19]=1;   								      //1是需要强制刷新.
      

      iArray[4]=new Array();
      iArray[4][0]="接受者名称";    	        //列名
      iArray[4][1]="80";            		      //列宽
      iArray[4][2]=180;            			      //列最大值
      iArray[4][3]=1;              			      //是否允许输入,1表示允许，0表示不允许
      
      
      iArray[5]=new Array();
      iArray[5][0]="接受者描述";    	        //列名
      iArray[5][1]="100";            					//列宽
      iArray[5][2]=180;            						//列最大值
      iArray[5][3]=3;              						//是否允许输入,1表示允许，0表示不允许

      CertifyList = new MulLineEnter( "fm" , "CertifyList" ); 
      //这些属性必须在loadMulLine前
      CertifyList.mulLineCount = 1;     
      CertifyList.displayTitle = 1;
      CertifyList.hiddenPlus = 0;     
      CertifyList.hiddenSubtraction = 0;
      CertifyList.loadMulLine(iArray);  
      
    } catch(ex) {
      alert(ex);
    }
}


function initCertifyShowGrid()
{
	var iArray = new Array();
	
	try
    {
    	iArray[0]=new Array();
      iArray[0][0]="序号";         		  //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30";        				//列宽
      iArray[0][2]=50;          				//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      iArray[1]=new Array();
      iArray[1][0]="发放者";     				//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="100";        				//列宽
      iArray[1][2]=80;          				//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      iArray[2]=new Array();
      iArray[2][0]="接收者";     				//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="100";        		  	//列宽
      iArray[2][2]=180;          				//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

/*
      iArray[3]=new Array();
      iArray[3][0]="接受者代码";    	  //列名
      iArray[3][1]="80";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="接受者名称";    	  //列名
      iArray[4][1]="180";            		//列宽
      iArray[4][2]=180;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="登录机构";    	    //列名
      iArray[5][1]="50";            		//列宽
      iArray[5][2]=50;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
*/      
      
      CertifyShowGrid = new MulLineEnter( "fm" , "CertifyShowGrid" ); 
      //这些属性必须在loadMulLine前    
      
      CertifyShowGrid.mulLineCount = 0;     
      CertifyShowGrid.displayTitle = 1;     
      CertifyShowGrid.hiddenPlus = 1;       
      CertifyShowGrid.hiddenSubtraction = 1;
      CertifyShowGrid.canSel = 1;           
      CertifyShowGrid.canChk = 0;          
      
      CertifyShowGrid.loadMulLine(iArray);  
      
    } catch(ex) {
      alert(ex);
    }
}

function getManageName(cCode)
{
	var strSQL = "select Name from LDCom where ComCode = '"+cCode+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult!=null)
	{
		var cName = arrResult[0];
		return cName;
	}
	else
	{
		return '';
	}
}


</script>