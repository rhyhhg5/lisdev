
<%
	//Creator :gzh
	//Date :20140117
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = ManageCom;
    fm.all('CertifyCode').value = '';
    fm.all('SubCode').value = '';
   
  }
  catch(ex)
  {
    alert("进行初始化时出现错误！！！！");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initCertifyMaxNoGrid();
    showAllCodeName();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var CertifyMaxNoGrid
function initCertifyMaxNoGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         	  //列名
    iArray[1][1]="80px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="单证编码";         	
    iArray[2][1]="80px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="单证类型码";         	  //列名
    iArray[3][1]="80px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="单证名称";      //列名
    iArray[4][1]="120px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="最大数量";      //列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    CertifyMaxNoGrid = new MulLineEnter("fm", "CertifyMaxNoGrid"); 
    //设置Grid属性
    CertifyMaxNoGrid.mulLineCount = 0;
    CertifyMaxNoGrid.displayTitle = 1;
    CertifyMaxNoGrid.locked = 1;
    CertifyMaxNoGrid.canSel = 0;
    CertifyMaxNoGrid.canChk = 1;
    CertifyMaxNoGrid.hiddenSubtraction = 1;
    CertifyMaxNoGrid.hiddenPlus = 1;
    CertifyMaxNoGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
