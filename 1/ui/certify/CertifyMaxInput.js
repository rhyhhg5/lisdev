var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
 }
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

function displayQueryResult(strResult) {
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置

  strResult = Conversion(strResult);
  var filterArray          = new Array(3,0,2,1,4,4);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  //alert(turnPage.arrDataCacheSet);


  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = CertifyMaxGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}

//根据起始日期进行查询出要该日期范围内的批次号码
/*
function trigger(objRadio)
{
	if( objRadio.value == "agentcode" )
  {
   	fm.QueryType.value = "A";

		fm.ManageCom.value="";
  	fm.AgentGrade.value="";
  }
  else if( objRadio.value == "managercom" )
  {
  	fm.QueryType.value = "M";
  	fm.AgentCode.value=""
  }
  else
  {
		alert("请您选择查询的类型！！");
		return;
  }
  return true;
}
*/
//根据代理人编码进行查询的函数
function queryInfo_AgentCode()
{
	fm.QueryType.value ="A";
  CertifyMaxGrid.clearData();
	queryInfo();
}
//根据管理机构和代理人级别进行查询的函数
function queryInfo_ManageCom()
{
  fm.QueryType.value = "M";
  CertifyMaxGrid.clearData();
  queryInfo();
}

function queryInfo()
{
	//判断操作的类型是QUERY还是UPDATE
	fm.OperateType.value = "QUERY";
	if(fm.QueryType.value=="")
	{
		alert("请您先选择查询的类型！！");
		return;
	}
	if(fm.QueryType.value =="A")
	{
		if(fm.AgentCode.value==""||fm.AgentCode.value=="null")
		{
			alert("请您输入代理人编码！！");
			return;
		}
	}

	if(fm.QueryType.value =="M")
	{
		if(fm.ManageCom.value==""||fm.ManageCom.value=="null")
		{
			alert("请您录入管理机构信息！！");
			return;
		}
//		if(fm.AgentGrade.value==""||fm.AgentGrade.value=="null")
//		{
//			alert("请您录入代理人级别信息！！");
//			return;
//		}
  }
	 fm.action = "./CertifyMaxSave.jsp";
   fm.submit();
}
//按照“代理人编码”进行操作的界面

function updateInfo_AgentCode()
{
  //将修改的类型附值成A，表示是“代理人编码”进行配置的操作

  fm.UpdateType.value = "A";
  updateInfo();
}

function updateInfo_ManageCom()
{
  //将修改类型附值成M，表示是按照“管理机构”进行附值的操作
  fm.UpdateType.value = "M";
  updateInfo();
}

//更新数据的函数
function updateInfo()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.OperateType.value = "UPDATE";
	fm.action = "./CertifyMaxUpdate.jsp";
  showSubmitFrame(mDebug);
 // fm.target = "_blank";
	fm.submit();
}

//首页
function showInfo1()
{
  var i = CertifyMaxGrid. mulLineCount;
  var i_count = 0;
  for(var j = 0;j < i;j++)
  {
    var maxcount = CertifyMaxGrid. getRowColData(j,5);
    var maxcount_old = CertifyMaxGrid. getRowColData(j,6);
    if(maxcount == maxcount_old)
    {
      i_count++;
    }
  }
  if(i_count==i)
  {
    turnPage.firstPage();
  }
    else
    {
      if (confirm("最大值已经进行了修改，您要保留该操作吗？"))
      {
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        fm.OperateType.value = "UPDATE";
        fm.action = "./CertifyMaxUpdate.jsp";
        showSubmitFrame(mDebug);
        fm.submit();
        if (FlagDel=="Fail")
          return;
      }
      else
      {
        mOperate="";
        alert("您已经取消了该次修改的操作！");
        turnPage.firstPage();
      }
    }
}

//前一页
function showInfo2()
{
  var i = CertifyMaxGrid. mulLineCount;
  var i_count = 0;
  for(var j = 0;j < i;j++)
  {
    var maxcount = CertifyMaxGrid. getRowColData(j,5);
    var maxcount_old = CertifyMaxGrid. getRowColData(j,6);
    if(maxcount == maxcount_old)
    {
      i_count++;
    }
  }
  if(i_count==i)
  {
    turnPage.previousPage();
  }
  else
  {
    if (confirm("最大值已经进行了修改，您要保留该操作吗？"))
    {
      var i = 0;
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.OperateType.value = "UPDATE";
      fm.action = "./CertifyMaxUpdate.jsp";
      showSubmitFrame(mDebug);
      fm.submit();
      if (FlagDel=="Fail")
        return;
    }
    else
    {
      mOperate="";
      alert("您已经取消了该次修改的操作！");
      turnPage.previousPage();
    }
  }
}
//下一页
function showInfo3()
{
  var i = CertifyMaxGrid. mulLineCount;
  var i_count = 0;
  for(var j = 0;j < i;j++)
  {
    var maxcount = CertifyMaxGrid. getRowColData(j,5);
    var maxcount_old = CertifyMaxGrid. getRowColData(j,6);
    if(maxcount == maxcount_old)
    {
      i_count++;
    }
  }
  if(i_count==i)
  {
    turnPage.nextPage();
  }
  else
  {
    if (confirm("最大值已经进行了修改，您要保留该操作吗？"))
    {
      var i = 0;
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.OperateType.value = "UPDATE";
      fm.action = "./CertifyMaxUpdate.jsp";
      showSubmitFrame(mDebug);
      fm.submit();
      if (FlagDel=="Fail")
        return;
    }
    else
    {
      mOperate="";
      alert("您已经取消了该次修改操作！！！");
      turnPage.nextPage();
    }
  }
}
//尾页
function showInfo4()
{
  var i = CertifyMaxGrid. mulLineCount;
  var i_count = 0;
  for(var j = 0;j < i;j++)
 {
    var maxcount = CertifyMaxGrid. getRowColData(j,5);
    var maxcount_old = CertifyMaxGrid. getRowColData(j,6);
    if(maxcount == maxcount_old)
    {
      i_count++;
    }
  }
  if(i_count==i)
  {
    turnPage.lastPage();
  }
  else
  {
    if (confirm("最大值已经进行了修改，您要保留该操作吗？"))
    {
      var i = 0;
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.OperateType.value = "UPDATE";
      fm.action = "./CertifyMaxUpdate.jsp";
      showSubmitFrame(mDebug);
      fm.submit();
      if (FlagDel=="Fail")
        return;
    }
    else
    {
      mOperate="";
      alert("您已经取消了该次修改操作！！！");
      turnPage.lastPage();
    }
  }
}

function showalert(){
	alert("没有符合条件的数据！");
}