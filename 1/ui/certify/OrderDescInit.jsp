<html>
<%
//name :OrderDescInput.jsp
//function :Manage LMCertifyDes
//Creator :
//date :2006-07-29
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.OrderCreator.value = "<%=Operator%>";
  	fm.AttachDate.value = "<%=CurrentDate%>";
  	fm.Note.value = "";
  	fm.MakeDate.value = "<%=CurrentDate%>";
    //getComName(<%=Comcode%>);
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCertifyTypeGrid();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCertifyTypeGrid() 
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="单证编码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=2;
    iArray[1][4]="CertifyCode";
    iArray[1][5]="1|2";             //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";             //上面的列中放置引用代码中第几位值

    iArray[2]=new Array();
    iArray[2][0]="单证名称";         	//列名
    iArray[2][1]="250px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="单价";         			//列名
    iArray[3][1]="60px";            	//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=1;              			//2表示是代码选择

    //iArray[4]=new Array();
    //iArray[4][0]="";         				//列名
    //iArray[4][1]="60px";            //列宽
    //iArray[4][2]=60;            		//列最大值
    //iArray[4][3]=1;              		//是否允许输入,1表示允许，0表示不允许

    CertifyTypeGrid = new MulLineEnter( "fm" , "CertifyTypeGrid" );
    CertifyTypeGrid.mulLineCount = 0;
    CertifyTypeGrid.displayTitle = 1;
    //CardRiskGrid.canSel=1;
    CertifyTypeGrid.loadMulLine(iArray);
    CertifyTypeGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("初始化时出错:"+ex);
  }
}

function showCom()
{
	var typeRadio="";
	for(var i=0;fm.StateRadio.length;i++)
	{
		if(fm.StateRadio[i].checked)
		{
			typeRadio=fm.StateRadio[i].value;
			break;
		}
	}
	if (typeRadio=="0")
	{
		divComName.style.display='none';
		divComCode.style.display='none';
		fm.ManageCom.value = 'A';
	}
	else
	{
		divComName.style.display='';
		divComCode.style.display='';
		fm.ManageCom.value = <%=Comcode%>;
	}
}

</script>


