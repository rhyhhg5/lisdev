<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：CertDailyInit.jsp
//程序功能：
//创建日期：2003-08-11
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	String strCurDate = PubFun.getCurrentDate();
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
  	fm.ReportDate.value = '<%= strCurDate %>';
  }
  catch(ex)
  {
    alert("在CertDailyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
  }
  catch(re)
  {
    alert("CertDailyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>