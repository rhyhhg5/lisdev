//该文件中包含客户端需要处理的函数和事件
var turnPage=new turnPageClass();
var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
    if(showInfo!=null)
    {
        try
        {
            showInfo.focus();
        }
        catch(ex)
        {
            showInfo=null;
        }
    }
}

//提交，保存按钮对应操作
function submitForm()
{
    if(veriryInput3())
    {
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

        fm.submit(); //提交
    }
}

function veriryInput3()
{
    var rowNum = CertifyList.mulLineCount ;
    if (rowNum==0)
    {
        alert("请在单证列表中录入发放信息！");
        return false;
    }
    if (CertifyList.getRowColData(rowNum-1,5) != "" && CertifyList.getRowColData(rowNum-1,5) != null)
    {
        var lineNum = CertifyList.getRowColData(rowNum-1,4) - CertifyList.getRowColData(rowNum-1,3) + 1;
        if (lineNum + "" != CertifyList.getRowColData(rowNum-1,5))
        {
            alert("第"+i+"行数量与起始单号终止单号不符！");
            return false;
        }
    }
    if (CertifyList.getRowColData(rowNum-1,5) == null || CertifyList.getRowColData(rowNum-1,5) == "")
    {
        alert("发放数量不能为空！");
        return false;
    }
    return true;
}

function countNum(parm1,parm2)
{
    var startNo = CertifyList.getRowColDataByName(0, "StartNo");
    var endNo = CertifyList.getRowColDataByName(0, "EndNo");
    var sumCount = CertifyList.getRowColDataByName(0, "SumCount");

    if(sumCount == null || sumCount == "")
    {
        if(startNo == "" || startNo == null)
        {
            CertifyList.setRowColData(parm1, 5, "");
            return;
        }
        if (endNo == "" || endNo == null)
        {
            CertifyList.setRowColData(parm1, 5, "");
            return;
        }
        if (!isNumeric(startNo))
        {
            alert("起始单号有非数字字符！");
            return false;
        }
        if (!isNumeric(endNo) && endNo != null)
        {
            alert("终止单号有非数字字符！");
            return false;
        }

        var num = parseFloat(endNo) - parseFloat(startNo) + 1;
        if (num < 0)
        {
            alert("起始单号大于终止单号！");
            return false;
        }
        CertifyList.setRowColData(parm1, 5, num+"");
    }
    else
    {
        if(startNo == "" || startNo == null)
        {
            CertifyList.setRowColData(0, 4, "");
            return;
        }
        if (sumCount == "" || sumCount == null)
        {
            CertifyList.setRowColData(0, 4, "");
            return;
        }
        if (!isNumeric(startNo))
        {
            alert("起始单号有非数字字符！");
            return false;
        }
        if (!isNumeric(sumCount) && sumCount != null)
        {
            alert("数量有非数字字符！");
            return false;
        }

        endNo = parseFloat(startNo) + parseFloat(sumCount) - 1;
        endNo = LCh(endNo, "0", startNo.length);
        CertifyList.setRowColData(parm1, 4, endNo);
    }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    showInfo.close();
    if (FlagStr == "Fail" )
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //resetPage();
    }
    else
    {
        content="保存成功！";
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        initForm();
    }
}

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

function afterCodeSelect(codeName,Field)
{
    if(codeName == "CertifyCodeReceive1")
    {
        initCertifyStoreList();
        var certifyCode = CertifyList.getRowColDataByName(0, "CertifyCode");
        var strSQL = "";
        if(fm.ManageCom.value.length <= 4)
        {
            strSQL = "select CertifyCode, StartNo, EndNo, SumCount, case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else Receivecom end "
                + "from LZCard where CertifyCode = '" + certifyCode 
                + "' and ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = 'A" 
                + fm.ManageCom.value + "') and StateFlag in ('0','7','8','9') "
                + "order by StartNo";
        }
        else
        {
            strSQL = "select CertifyCode, StartNo, EndNo, SumCount, case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else Receivecom end "
                + "from LZCard a where CertifyCode = '" + certifyCode 
                + "' and SendOutCom = 'B" + fm.Operator.value 
                + "' and (ReceiveCom like 'D%' or ReceiveCom like 'E%') and StateFlag in ('0','7','8','9') "
                + "order by StartNo";
        }
        turnPage.queryModal(strSQL, CertifyStoreList);
    }
}