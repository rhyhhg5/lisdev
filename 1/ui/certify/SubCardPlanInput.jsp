<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="SubCardPlan.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="SubCardPlanInit.jsp"%>
  <title>下级单证计划处理</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <table class="common" border=0 width=100%>
      <tr><td class="titleImg" align="center">下级单证计划：</td></tr>
	  </table>
    
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <td class="title">计划标识</td>
        <td class="input"><input class="common" name="PlanID"></td>

        <td class="title">单证编码</td>
        <td class="input">
        	<input class="codeno" name="CertifyCode"  
          	ondblclick="return showCodeList('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,250);"
	          onkeyup="return showCodeListKey('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,250);"
            verify="单证编码|NOTNULL"><input class = "codename" name ="CertifyCodeName" ></td>
      </tr>

      <tr class="common">
        <td class="title">计划状态</td>
        <td class="input">
        	<input class="codeno" name="PlanState" CodeData="0|^A|申请|^C|确认|^R|批复|^P|归档|"
            ondblclick="return showCodeListEx('PlanState', [this,PlanStateName],[0,1]);"
            onkeyup="return showCodeListKeyEx('PlanState', [this,PlanStateName],[0,1]);"><input class = codename name = PlanStateName ></td>
        
        <td class="title">关联计划</td>
        <td class="input"><input class="common" name="RelaPlan"></td></tr>

      <tr class="common">
        <td class="title">入机日期</td>
        <td class="input"><input class="coolDatePicker"  dateFormat="short" name="MakeDate"></td>
        <!--
        <td class="title">入机时间</td>
        <td class="input"><input class="common" name="MakeTime"></td>
        -->
        <td class="title">申请机构</td>
        <td class=input><input class="codeno" name="AppCom" 
            ondblclick="return showCodeListEx('comcode', [this,AppComName],[0,1]);"
            onkeyup="return showCodeListKeyEx('comcode', [this,AppComName],[0,1]);"><input class = codename name = AppComName ></td>
        </tr>

    </table>

	  <table>
	      <tr class="common">
	        <td><input value="查询计划"                type="button" class="common" onclick="queryPlan(0);"></td>
    		<td><input value="对计划进行预算查询"      type="button" class="common" onclick="queryBuget();"></td>
    		<td><input value=" 打  印"                 type="button" class="common" onclick="printPlan();"></td>
    	 </tr>
	      <tr class="common">
	        <td><input value="查询待批复的计划"   type="button" class="common" onclick="queryPlan(1);"></td>
    		<td><input value="对计划进行批复"     type="button" class="common" onclick="retPlan();"></td>
    	    <td><input value=" 重  置 "           type="button" class="common" onclick="initForm();"></td>
    	  </tr>
	      <tr class="common">
		    <td><input value="查询待汇总的计划"   type="button" class="common" onclick="queryPlan(2);"></td>
		    <td><input value="对计划进行汇总"     type="button" class="common" onclick="sumPlan();"></td>
    	  </tr>
    </table>
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCardPlanInfo);"></td>
    	<td class= titleImg>计划列表</td></tr>
    </table>
    
  	<div id="divCardPlanInfo" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCardPlanInfo"></span></td></tr>
      </table>
  	</div>
  	
  	<input value=" 首页 " class="common" type="button" onclick="turnPage.firstPage();"> 
    <input value="上一页" class="common" type="button" onclick="turnPage.previousPage();"> 					
    <input value="下一页" class="common" type="button" onclick="turnPage.nextPage();"> 
    <input value=" 尾页 " class="common" type="button" onclick="turnPage.lastPage();"> 		
  	
  	<!-- extra where condition for query page -->
  	<input type="hidden" name="sql_where" value="">
  	<input type="hidden" name="sql_where_ex" value="">
  	
  	<!-- operator flag -->
  	<input type="hidden" name="oper_flag" value="">
  	
  	<!-- current com -->
  	<input type="hidden" name="cur_com" value="">
  	<!-- query sql -->
  	<input type="hidden" name="Strsql" value="">
  	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
var codeSql = "1  and code like #"+<%= strManageCom %>+"%#";
</script>