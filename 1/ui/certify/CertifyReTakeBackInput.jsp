<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="CertifyReTakeBackInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyReTakeBackInit.jsp"%>
<title>单证状态修改</title>
</head>
<body onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
<form action="" method=post name=fm target="fraSubmit">
    <table class=common border=0 width=100%>
        <tr><td class=titleImg align= center>请输入单证查询条件：</td></tr>
    </table>
    <table class=common align=center>
        <tr class=common>
            <td class=title>单证类型</td>
            <td class=input><input class="codeNo" name=certifyCode ondblclick="return showCodeList('CertifyCode',[this,certifyName],[0,1],null,null,null,1,300);" readonly><input class=codename name=certifyName readonly ></td>
            <td class=title>单证起始号</td>
            <td class=input><input class="common" name=startNo ></td>
            <td class=title>单证终止号</td>
            <td class=input><input class="common" name=endNo ></td>
        </tr>
        <tr class=common>
            <td class=title>核销人</td>
            <td class=input><input class="common" name=sendOutCom ></td>
            <td class=title>起始日期</td>
            <td class=input><input class="coolDatePicker" name=startDate ></td>
            <td class=title>终止日期</td>
            <td class=input><input class="coolDatePicker" name=endDate ></td>
        </tr>
    </table>
    <input value="查　询" class="cssButton" type=button onclick="easyQueryClick();">
    <table>
        <tr>
            <td class=common><img src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyListGrid);"></td>
            <td class=titleImg>单证列表：</td>
        </tr>
    </table>
    <div id= "divCertifyListGrid" style= "display: ''">
        <table class=common>
            <tr class=common>
                <td text-align: left colSpan=1><span id="spanCertifyListGrid" ></span></td>
            </tr>
        </table>
        <div id= "divPage" align=center style= "display: 'none' ">
            <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
            <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
            <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
            <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
        </div>
    </div>
    <input type="hidden" name="ManageCom">
    <input type="hidden" name="Operator">
    <input type="hidden" name="oldStateFlag" value="5">
</form>
<form action="./CertifyReTakeBackSave.jsp" method=post name=fmSave target="fraSubmit">
    <table class=common border=0 width=100%>
        <tr><td class=titleImg align= center>单证状态修改：</td></tr>
    </table>
    <table class=common align=center>
        <tr class=common>
            <td class=title>单证类型</td>
            <td class=input><input class="common" name=certifyCode readonly ></td>
            <td class=title>单证起始号</td>
            <td class=input><input class="common" name=startNo ></td>
            <td class=title>单证终止号</td>
            <td class=input><input class="common" name=endNo ></td>
        </tr>
    </table>
    <input type=button class="cssButton" value="修　改" onclick="submitForm();">
    <input type="hidden" name="sumCount">
    <input type="hidden" name="oldStateFlag" value="5">
    <input type="hidden" name="newStateFlag" value="2">
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>