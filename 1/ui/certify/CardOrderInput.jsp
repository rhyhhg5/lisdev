<html>
<%
//name :CardOrderInput.jsp
//function :Manage LMCertifyDes
//Creator :
//date :2006-08-01
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "CardOrderInput.js"></SCRIPT>      
<%@include file="CardOrderInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();initOrder();" >
  <form action="./CardOrderSave.jsp" method=post name=fm target="fraSubmit">
  	
  <table>
    <tr>
      <td class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSerialSearch);">
      </td>
  	<td class= titleImg>批次查询</td></tr>
  </table>
  
  <Div id= "divSerialSearch" style= "display: ''">
	<br>
   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		季度
   			</TD>
   			<TD class= input>
   				<input class="code" name="OrderSta" ReadOnly
	            ondblclick="return showCodeList('orderquarter', [this,QuarterValue],[1,0],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('orderquarter', [this,QuarterValue],[1,0],null,null,null,1,300);">
	        <input name="QuarterValue" type="hidden">
   			</TD>
   			<TD class= title>
          批次状态
        </TD>
        <TD class= input>
          <Input class="code" readOnly name= "StateName" 
          CodeData="0|^0|征订^1|批准" 
          ondblClick="showCodeListEx('StateName',[this,State],[1,0],null,null,null,1);"
          onkeyup="showCodeListKeyEx('StateName',[this,State],[1,0],null,null,null,1);"> <!--SubCode-->
          <Input class= common name= 'State' type='hidden'>
          
        </TD>
        <TD class= title>
        </TD>
        <TD class= input>
        </TD>
      </TR> 
   	</Table>
   	
   	<br>
   	<input class="cssButton" type="button" value="查  询" onclick="queryClick()" >
   	<input class="cssButton" type="button" value="重  置" onclick="resetClick()" >
   	
   	<br><br>
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSerial);"></td>
    	<td class= titleImg>征订批次列表</td></tr>
    </table>
    
    <Div  id= "divSerial" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanSerialGrid" ></span>
        		</td>
      		</tr>
    	</table>
		</div>
   	<Div  id= "divPage" align=center style= "display: 'none' ">
     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>
   	
 </div>
 <br><hr><br>
 
	<table>
    <tr>
      <td class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardOrder);">
      </td>
  	<td class= titleImg>单证征订</td></tr>
  </table>
    
	<Div  id= "divCardOrder" style= "display: ''">
	
		<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		批次号
   			</TD>
   			<TD class= input>
   				<Input class= common name= SerialNo id="SerialNoId"> 
   			</TD>
   			<TD class= title>
          机构代码
        </TD>
        <TD class= input>
          <input class="readonly" readonly name= "ComCode" >
        </TD>
        <TD class= title>
        	订单人
        </TD>
        <TD class= input>
        	<input class="common"  name= "DescPerson" >
        </TD>
      </TR> 
      <TR class= common>
   			<TD class= title>
   		 		订单日期
   			</TD>
   			<TD class= input>
   				<input class="coolDatePicker" name="OrderDate" verify="订单日期|DATE&NOTNULL" elementtype=nacessary>
   			</TD>
   			<TD class= title></TD>
        <TD class= input></TD>
        <TD class= title></TD>
        <TD class= input></TD>
      </TR> 
   	</Table>
   	
   	<Div  id= "divCertifyType" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanCertifyTypeGrid"></span>
        		</td>
      		</tr>
    	</table>
		</div>
  </Div>
  
   	<br>
   	<table>
    <tr>
      <td class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSerialRemark);">
      </td>
  		<td class= titleImg>征订说明</td></tr>
  	</table>
  
	<Div  id= "divSerialRemark" style= "display: ''">
	<TR  class= common>
		<TD  class= input colspan="6">
		    <textarea name="Note" cols="100%" rows="3"  class="common" readonly >
		    </textarea>
		</TD>
	</TR>
  </div> 	
  <br>
  <input class="cssButton" type="button" value="提  交" onclick="submitForm()" >
  <input class="cssButton" type="button" value="修  改" onclick="updateForm()" >
  
  <input type="hidden" name="OperateType">
  <input type="hidden" name="SerialNoBak">
  <input type="hidden" name="AttachDateBak">
   	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>