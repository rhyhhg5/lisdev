<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="LZXmlListImportInput.js"></script>
    <%@include file="LZXmlListImportInit.jsp" %>
</head>

<body onload="initForm();">
    <form action="" method="post" name="fmImport" target="fraSubmit" enctype="multipart/form-data">

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">批次导入</td>
            </tr>
        </table>
        
        <hr />
        
        <div id="divFileImport" name="divFileImport" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">批次文件</td>
                    <td class="common">
                        <input class="cssfile" name="FileName" type="file" />
                    </td>
                </tr>
            </table>
            <input class="cssButton" type="button" id="btnImport" name="btnImport" value="批次导入" onclick="importBatchList();" />
        </div>
            
        <hr />
    </form>

    <br />
  
	<form action="" method="post" name="fm" target="fraSubmit">
		
        
		
        
        <hr />
        
        <br />
        <!--input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="返  回" onclick="goBack();" -->
        
        <input type="hidden" id="fmOperatorFlag" name="fmOperatorFlag" />
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
