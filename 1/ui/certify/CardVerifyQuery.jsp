<html>
<%
//name :CertifyMaxInput.jsp
//function :Query Certify Info and Show Info Based On ManageCom and AgentGrade
//Creator :刘岩松
//date :2003-04-18
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	
    GlobalInput tGI = new GlobalInput();        
    tGI = (GlobalInput)session.getValue("GI");
 	
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT>
var managecom = <%=tGI.ManageCom%>;//管理机构
</SCRIPT> 

  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="CardVerifyQuery.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CardVerifyQueryInit.jsp"%>
	</head>

<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">	
  <Div id= "divLLReport1" style= "display: ''">

   	<table class= common>
   		<tr class= common>   			
          <TD class= title>
   			   经办日期（开始）
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
           经办日期（结束）
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
        <Input type= "hidden" name= PubSQL>
        <Input type= "hidden" name= ReportType>        
        
   		</TR>
   		<tr class= common> 
   				<td class="title">单证编码</td>
        	<td class="input">
          	<input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);" onkeyup="return showCodeListKey('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);"><Input class= codename name="CertifyCodeName" readonly>
          </td>
        	<TD  class= title>单证类型码</TD>
          <TD  class= input><Input class="common" dateFormat="short" name=CardType ></TD>
   		</TR>
   		<tr class= common>   			
          <TD class= title>
   			   单证起始号
          </TD>
          <TD  class= input>
            <Input class="common" dateFormat="short" name=StartNo >
          </TD>
          <TD  class= title>
           单证终止号
          </TD>
          <TD  class= input>
            <Input class="common" dateFormat="short" name=EndNo >
          </TD> 
   		</TR>
   		<tr class= common>   			
          <TD class= title>
   			   回销状态
          </TD>
          <TD  class= input>
            <Input class="code"  name=StateName CodeData="0|^1|未回销^2|已回销^3|全部"
             ondblClick="showCodeListEx('CertifyStateList',[this,StateType],[1,0],null,null,null,1);"
             onkeyup="showCodeListKeyEx('CertifyStateList',[this,StateType],[1,0],null,null,null,1);">

          </TD>
          <TD  class= title>
          回销状态编号         
          </TD>
          <TD class= input > 
          <Input class="common"  name=StateType  >        
          </TD> 
   		</TR>
   		<tr class= common>
   			  <td class= title >代理网点编码 </td>
          <td class= input>
            <Input class="common" dateFormat="short" name=ReceiveCom >
          </td>
   		</tr>
 		</table>
       	<input class="cssButton" type= button value="查    询" onclick="queryInfo();">       
       	<input value="打印报表" type="button" onclick="printList();" class="cssButton">
    <table>
    	<tr>
        <td class= titleImg>
    			 信息列表
    		</td>
    	</tr>
    </table>

	<Div  id= "divLLReport2" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1>
  				<span id="spanCertifyMaxGrid" >
  				</span>
  			</td>
  		</tr>
    </table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
     </center>  	
  	</Div>
 	</div>
 	 	<table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCardState);"></td>
    	<td class= titleImg>单证数量统计</td></tr>
    </table>

  	<div id="divCardState" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCardState"></span></td></tr>
    </table>
    <Div  id= "divPage1" align=center style= "display: 'none' ">
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
     </center>  	
  	</Div>
    </div>
    <input type=hidden name="SearchSQL" >
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>