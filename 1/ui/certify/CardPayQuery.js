//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  fm.OperateType.value = "QUERY";
  
  if(tOperate !=null & tOperate=="AFFIRM"){  
  	
  	 if(fm.PayNo.value == null || fm.PayNo.value =="") {
  	 	alert("请输入结算单号！！！");  	 
  	  return ;
  	}
  	fm.all('Conff').disabled = false; 
  }else
  {
  	 fm.all('Conff').disabled = true;
  }
  
  if (fm.PayNo.value != null || fm.PayNo.value !=""){
  	var  strSQLPay =" select state from lzcardpay where payno='"+ fm.PayNo.value +"' and state='1'";
    var PayNoExist=easyExecSql(strSQLPay);
		if (PayNoExist){
		   alert("该结算单号已确认过！！！"); 		   
	  }
  }	
  var i = 0;
  initCardPayGrid();
  
  var  strSQL ="";
  var conStr = "";
  var strMan = fm.all('ManageCom').value;
  if(strMan != null && strMan != "" && strMan.length==8){
  	conStr = " and operator='"+tOperater + "'";
  }
  strSQL = "select StartNo,EndNo,SumCount,PayNo,CardType,(select GroupAgentCode from laagent where agentcode = HandlerCode),Handler,AgentCom,AgentComName"
  //+ ", case when state='1' then '已确认' else '未确认' end "
  + " from LZCardPay "
  + " where 1=1"	
  + conStr
  + getWherePart( 'PayNo','PayNo' ) 
  + getWherePart( 'CardType','CardType' ) 	   	   
  + getWherePart( 'HandlerCode','AgentCode' ) 
  + getWherePart( 'Handler','AgentName','like' ) 
  + getWherePart( 'AgentCom','AgentCom' ) 
  + getWherePart( 'AgentComName','AgeComName','like' ) 
  + getWherePart( 'ManageCom','ManageCom','like' )   
  + " order by StartNo";  
 
  turnPage.queryModal(strSQL,CardPayGrid);

}

function displayQueryResult(strResult) {
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  strResult = Conversion(strResult);
  var filterArray = new Array(0,4,2,7,9,5,6);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.pageDisplayGrid = CardPayGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }

  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;

}




//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

function ReturnData()
{

	//首先判断单证的类型是什么；

    fm.OperateType.value = "QUERY";
		var tRow=CardPayGrid.getSelNo();


  	if (tRow==0)
   	{
   		alert("请您先进行选择!");
  		return;
  	}
    var tPayNo = CardPayGrid.getRowColData(tRow-1,4);
    var tCardType = CardPayGrid.getRowColData(tRow-1,5);
    var tStartNo = CardPayGrid.getRowColData(tRow-1,1);
    var tEndNo = CardPayGrid.getRowColData(tRow-1,2);     
    top.location.href="./CardPayQueryDetail.jsp?PayNo="+tPayNo+"&CardType="+tCardType+"&StartNo="+tStartNo+"&EndNo="+tEndNo;
}
function queryAgent() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  fm.all('OperateObj').value="AgentCode";
  if(fm.all('AgentCode').value == "")	{
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  }  
  
}
function queryAgentBlur() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  fm.all('OperateObj').value="AgentCode";
  if(fm.all('AgentCode').value == "")	{
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  }
  if(fm.all('AgentCode').value != "")	 {

    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom like '"+fm.all('ManageCom').value+"%'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];     
    } else {    
      alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
    }
  }
}
//批量发放。查询代理机构的函数。
function queryCom()
{
	//下发对象：代理机构
	fm.all('OperateObj').value="AgentCom";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCom').value == "")	
  {  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LAComQueryInput.jsp?ManageCom="+fm.all('ManageCom').value,"LAComQueryInput",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}	
}

//批量发放。查询代理机构的函数。
function queryComBlur()
{
	//下发对象：代理机构
	fm.all('OperateObj').value="AgentCom";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCom').value != ""  && fm.all('AgentCom').value != null)	
  {  
  	 var cAgentCom = fm.AgentCom.value;  //代理机构代码
  	 var strSql = "select a.AgentCom,a.Name"	       
	         +" from LACom a where 1=1 "	        
	         + getWherePart('a.AgentCom','AgentCom')	
	         + getWherePart('a.ManageCom','ManageCom','like')
	         +" order by AgentCom";	
	    var arrResult = easyExecSql(strSql);
	   
	    if (arrResult != null) {
	      fm.AgeComName.value = arrResult[0][1];     
	     } else {     
	      alert("代码为:["+fm.all('AgentCom').value+"]的代理机构不存在，请确认!");
	      return false;
	    }    	         
	}	
}


function queryAgent2() {
  
  if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==8 )	 {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
    }
  }
}
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult) {
  if(arrResult!=null) {
  	if(fm.all('OperateObj').value =="AgentCode"){
	    fm.AgentCode.value = arrResult[0][0];
	    fm.AgentName.value = arrResult[0][5];
	    fm.GroupAgentCode.value = arrResult[0][95];
    }else if(fm.all('OperateObj').value =="AgentCom")
    {
    	 fm.AgentCom.value = arrResult[0][0];
	     fm.AgeComName.value = arrResult[0][1];
    }
    
  }
}

//确认按钮
function payAffirm()
{
	if(fm.PayNo.value==''){
  	alert("请输入结算单号！！！");
  	return false;
  }
  var strSql = "select PayNo from LZCardPay where PayNo='" + trim(fm.PayNo.value) +"' and state = '1'";
  var arrResult = easyExecSql(strSql);
  if (arrResult !=null){
  	alert("该结算单已确认！！！");
  	return ;
  }
  fm.OperateType.value = "AFFIRM"; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = './CardPayAffirmSave.jsp';
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{		
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
    initForm();
  }
}