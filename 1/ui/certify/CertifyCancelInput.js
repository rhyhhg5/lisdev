//               该文件中包含客户端需要处理的函数和事件
var turnPage=new turnPageClass(); 
var turnPage1=new turnPageClass(); 

var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{	
	var handleCount=CertifyList.mulLineCount; //得到MulLine的行数	
	if (handleCount==0)
	{
		alert("请先输入单证信息!");
		return false;
	}
	for(var i=0; i<handleCount; i++)
	{
		//var strSQL = "select HaveNumber from LMCertifyDes where CertifyCode='"+CertifyList.getRowColData(i,1)+"'";
		//var arrResult = easyExecSql(strSQL);
		//if (arrResult!=null)
		//{
		//	if(arrResult[0][0]=="N")
		//	{
		//		alert("对不起，暂时不能对无号单证进行人工处理！");
		//		return false;
		//	}
		//}
		
		if(CertifyList.getRowColData(i,5)==null||CertifyList.getRowColData(i,5)=="")
		{
			alert("特殊单证处理数量不能为空!");
			return false;
		}
		else if(!isNumeric(CertifyList.getRowColData(i,5)))
		{
			alert("特殊单证处理数量有非数字字符!");
			return false;
		}		
		
		if(CertifyList.getRowColData(i,6)==""||CertifyList.getRowColData(i,6)==null||CertifyList.getRowColData(i,7)==""||CertifyList.getRowColData(i,7)==null)
		{
			alert("处理状态不能为空!");
			return false;
		}
		
		if((CertifyList.getRowColData(i,7)=="2"&&trim(CertifyList.getRowColData(i,6))!="作废")
			||(CertifyList.getRowColData(i,7)=="3"&&trim(CertifyList.getRowColData(i,6))!="遗失")      
			||(CertifyList.getRowColData(i,7)=="5"&&trim(CertifyList.getRowColData(i,6))!="正常回销"))
		{
			alert("请通过鼠标双击后选择处理状态!");
			return false;
		}
	}

	fm.all("AuditKind").value="SINGLE";
	try
	{
		if(veriryInput3()==true)
		{
			if( verifyInput() == true )//&& CertifyList.checkValue("CertifyList") ) 
			{
				  var i = 0;
				  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
				  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
					
				  fm.submit(); //提交
			}
		}
	}
	catch(ex) 
	{
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	var rowNum = CertifyList.mulLineCount ;
	if (rowNum==0)
	{
		alert("请在单证列表中录入发放信息！");
		return false;
	} 
	for(var i=1;i<=rowNum;i++)
	{
		var strSQL = "select havenumber,certifylength from LMCertifyDes where certifycode='"+CertifyList.getRowColData(i-1,1)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,1)+"的单证");
		  return false;
		}
		else
		{
		    var length=parseInt(arrResult[0][1]);  
		    if(CertifyList.getRowColData(i-1,3).length!=length)
		    {
		       alert("起始保单号的位数不符合规定！");
		       return false;
		    }
		    if(CertifyList.getRowColData(i-1,4).length!=length)
		    {
		       alert("终止保单号的位数不符合规定！");
		       return false;
		    }
		    
			if (arrResult[0][0]=="Y")
			{
				if (CertifyList.getRowColData(i-1,5)!=""&&CertifyList.getRowColData(i-1,5)!=null)
				{
					var lineNum=CertifyList.getRowColData(i-1,4)-CertifyList.getRowColData(i-1,3)+1; 
					if (lineNum+""!=CertifyList.getRowColData(i-1,5))
					{
						alert("第"+i+"行数量与起始单号终止单号不符！");
						return false;
					}
				}
			}	
		}
	}  
	return true;
}

function batchSubmit()
{
	if (!confirm("手工批量核销仅限于核销昨日自动核销时点至此时点尚未核销单证，您确信要手动核销?"))
	{
		return false;
	}
	
	fm.all("AuditKind").value="BATCH";
	  var i = 0;
	  var showStr="正在核销数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	//if( fm.chkPrt.checked == true ) {
	  //  var urlStr = "CertifyListPrint.jsp";
	  //  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
	  //    oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	  //} else {
	  CertifyList.clearData("CertifyList");
	  CertifyStoreList.clearData("CertifyStoreList");
	  CertifyList.addOne("CertifyList");
	  
	  fm.all("AvailNum").value="";
	  fm.all("NOLen").value="";
	  fm.all("certifysumcount").value="";
	  
	  content="操作成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  errorLogQuery();
	  //}
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifyCancelInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
		parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

function afterCodeSelect(codeName,Field)
{ 
	if(codeName=="CertifyCode")
	{
		var strSQL="select sum(sumcount) from LZcard where certifycode='" + Field.value + "' and stateflag in ('0','7','8','9') ";
		availNum=easyExecSql(strSQL);
		if (availNum)
		{
			fm.all("AvailNum").value=availNum[0];
		}
		strSQL="select CertifyLength from LMCertifyDes where certifycode='"+ Field.value +"'";
		noLen=easyExecSql(strSQL);
		if (noLen)
		{
			fm.all("NOLen").value=noLen[0];
		}
	}
	if(codeName == "CertifyCodeSend1")
	{
		var rowNum=CertifyList. mulLineCount ; //行数 
		for(var i=0;i<rowNum;i++) //清空mulLine数据
		{
			CertifyList.setRowColData(i,3,"");
			CertifyList.setRowColData(i,4,"");
			CertifyList.setRowColData(i,5,"");
			CertifyList.setRowColData(i,6,"");
		}
		
		var strSQL = "";
		
		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select a.CertifyCode,a.StartNo,a.EndNo,a.SumCount,"
			 + " case when b.HaveNumber='Y' then '有号单证' else '无号单证' end,case when substr(char(a.Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(a.Receivecom),2)) else a.Receivecom end "
			 + " from LZCard a,LMCertifyDes b where a.receivecom = 'A"+fm.ManageCom.value+"' and a.CertifyCode='"+Field.value+"' and a.StateFlag in ('0','7','8','9') "
			 + " and a.CertifyCode=b.CertifyCode";      
		}
		else
		{
			strSQL = "select a.CertifyCode,a.StartNo,a.EndNo,a.SumCount,"
			 + " case when b.HaveNumber='Y' then '有号单证' else '无号单证' end,case when substr(char(a.Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(a.Receivecom),2)) else a.Receivecom end  "
			 + " from LZCard a,LMCertifyDes b where a.receivecom = 'B"+fm.Operator.value+"' and a.CertifyCode='"+Field.value+"' and a.StateFlag in ('0','7','8','9') "
			 + " and a.CertifyCode=b.CertifyCode";      
		}
		turnPage1.queryModal(strSQL, CertifyStoreList);   
		
		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field.value+"' and StateFlag in ('0','7','8','9')";      
		}
		else
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field.value+"' and StateFlag in ('0','7','8','9')";      
		}
		
		var arrResult = easyExecSql(strSQL);
		
		if(arrResult != null)
		{
			fm.certifysumcount.value = arrResult[0][0];
		}
		else
		{
		  fm.certifysumcount.value = "0";
		}
	}
}

	/**
	 *
	 *
	**/
function errorLogQuery()
{
	if (!isDate(fm.StartDate.value)&&fm.StartDate.value!="")
	{
		alert("录入日期格式不正确!");
		ErrorLogGrid.clearData();
		return false;
	}
	if (!isDate(fm.EndDate.value)&&fm.EndDate.value!="")
	{
		alert("录入日期格式不正确!");
		ErrorLogGrid.clearData();
		return false;
	}
	if (fm.EndDate.value!=""&&fm.StartDate.value!="")
	{
		if (compareDate(fm.StartDate.value,fm.EndDate.value)==1)
		{
			alert("录入日期不正确，起始日期大于终止日期!");
			ErrorLogGrid.clearData();
			return false;
		}
	}
	var strSQL = "select a.CertifyCode,b.CertifyName,a.StartNo,c.CodeName,a.PossessCom,a.CertifyCom, a.CertifymakeData, a.makedate, a.State "
	+ " from LZCertifyErrorLog a , LMCertifyDes b, LDCode c where 1=1 and a.certifycode = b.certifycode and c.codetype='certifyerror' and a.ErrorInfo=c.code" 
	+ getWherePart( 'a.CertifyCode','CertifyCode')
	+ getWherePart( 'a.CertifyCom','ManageCom','like')
	+ getWherePart('a.CertifymakeData','StartDate','>=') 
	+ getWherePart('a.CertifymakeData','EndDate','<=')
	+ getWherePart( 'a.StartNo','CertifyNo')
	+	getWherePart('a.ErrorInfo','ErrorInfoCode')
	;
	
	strSQL =strSQL + " order by a.CertifyCode" ;
	var arrResult = easyExecSql(strSQL);
	
	if(arrResult == null)
	{
		alert("查询结果为空!");
		ErrorLogGrid.clearData();
		return false;
	}
	turnPage.queryModal(strSQL, ErrorLogGrid);   
}


	/**
	 *
	 *
	**/
function handAudit()
{
	if (fm.ComCode.value!="86")
	{
		alert("您无权核销日志中的单证!"); 
		return false;
	}
	var handAuditCount=ErrorLogGrid.mulLineCount; //得到MulLine的行数	
	
	var chkFlag=false; //用于标记是否有事件被选中	
	
	for (i=0; i<handAuditCount; i++) //根据mulLineCount数循环
	{
		if(ErrorLogGrid.getChkNo(i)==true) //得到被选中的事件,就将chkFlag置为true
		{
			chkFlag=true;
			if (ErrorLogGrid.getRowColData(i,9)!='0')
			{
				alert("要核销的单证中有无效或已作废的单证(单证号:"+ErrorLogGrid.getRowColData(i,3)+")，不能进行核销!");
				return false;
			}
			//if (ErrorLogGrid.getRowColData(i,4)=='该单证没有下发给业务员')
			//{
			//	alert("要核销的单证中有未下发给业务员的单证(单证号:"+ErrorLogGrid.getRowColData(i,3)+")，\n"
			//	+"需发放到业务员后进行核销!");
			//	return false;
			//}
		}
	}
	if (ErrorLogGrid.mulLineCount==0)
	{
		alert("请先查询错误日志，选中后再进行核销!");
		return flase;
	}
	if (chkFlag==false)
	{
		alert("请选中要核销的单证!");
		return false;
	}
	
	if(confirm("所选单证未通过核销校验，确定要核销吗?"))
	{
		fm.all("AuditKind").value="HANDAUDIT";
		
		var i = 0;
	  var showStr="正在核销数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.submit();
	}	
}

	/**
	 *
	 *
	**/
function resetSearch()
{
	//fm.all("ManageName").value="";
	//fm.all("ManageCom").value="";
	fm.all("CertifyName").value="";
	fm.all("CertifyCode").value="";
	fm.all("StartDate").value="";
	fm.all("EndDate").value="";
	fm.all("ErrorInfoName").value="";
	fm.all("ErrorInfoCode").value="";
	ErrorLogGrid.clearData();
}



function countNum(parm1,parm2)
{
	var startN=CertifyList.getRowColData(parm1,3);
	var endN=CertifyList.getRowColData(parm1,4);
	
	if (CertifyList.getRowColData(parm1,1)!=""&&CertifyList.getRowColData(parm1,3)!=null)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(parm1,1)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,1)+"的单证");
		  return false;
		}
		else
		{
			if(startN==""||startN==null)
			{
				CertifyList.setRowColData(parm1,5,"");
				return;
			}
			if (endN==""||endN==null)
			{
				CertifyList.setRowColData(parm1,5,"");
				return;
			}
			
			if (!isNumeric(startN))
			{
				alert("起始单号有非数字字符！");
				return false;
			}
			if (!isNumeric(endN))
			{
				alert("终止单号有非数字字符！");
				return false;
			}
			
			var num=parseFloat(endN)-parseFloat(startN)+1;
			if (num<0)
			{
				alert("起始单号大于终止单号！");
				return false;
			}
			CertifyList.setRowColData(parm1,5,num+"");
		}
	}
}