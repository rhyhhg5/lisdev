<%
//程序名称：CertifyTakeBackSave.jsp
//程序功能：
//创建日期：2006-04-14
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.taskservice.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%


  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;
  String szFailSet = "";

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  //校验处理
  //内容待填充
  
  String operateFlag = request.getParameter("OperateFlag");

  if (operateFlag.equals("INSERT||CONFIG")) 
	{
		try {
		  //String szNo[]					= request.getParameterValues("CertifyListNo");
		  String sendOutCom[]	= request.getParameterValues("CertifyList1");
		  String receiveCom[]			= request.getParameterValues("CertifyList3");
		  int nIndex;
		  
		  LZAccessSet setLZAccess = new LZAccessSet();
System.out.println("setLZAccess.size(): "+setLZAccess.size());		  
		  if( sendOutCom == null ) 
		  {
		  	throw new Exception("没有录入发放权限信息!");
		  }
	
		  for( nIndex = 0; nIndex < sendOutCom.length; nIndex ++ ) 
		  {
		    LZAccessSchema schemaLZAccess = new LZAccessSchema( );
		    
		    schemaLZAccess.setSendOutCom("A"+sendOutCom[nIndex].trim());
		    schemaLZAccess.setReceiveCom(receiveCom[nIndex].trim());    
		    setLZAccess.add(schemaLZAccess);
		  }
			
		  // 准备传输数据 VData
		  VData vData = new VData();
		
		  vData.addElement(globalInput);
		  vData.addElement(setLZAccess);
		  vData.addElement(operateFlag);

		  // 数据传输
		  CertifySendConUI tCertifySendConUI = new CertifySendConUI();
	
		  if (!tCertifySendConUI.submitData(vData, "INSERT||CONFIG")) 
		  {
		    Content = " 保存失败，原因是: 已存在要保存的发放关系!";
		    FlagStr = "Fail";
		    
		    vData = tCertifySendConUI.getResult();
		  } 
		  else 
		  {
		  	Content = " 保存成功 ";
		  	FlagStr = "Succ";
		  	
		  	vData = tCertifySendConUI.getResult();
		  	String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
		  	session.setAttribute("TakeBackNo", strTakeBackNo);
	 			session.setAttribute("State", CertStatBL.PRT_STATE);
		  }
		} 
		catch(Exception ex) 
		{
			ex.printStackTrace( );
	   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
	   	FlagStr = "Fail";
		}
	}
	else if (operateFlag.equals("DELETE||CONFIG"))
	{
		try {
			String tRadio[] = request.getParameterValues("InpCertifyShowGridSel");
			String tSendOutCom[] = request.getParameterValues("CertifyShowGrid1");
			String tReceiveCom[] = request.getParameterValues("CertifyShowGrid2");
			
			LZAccessSchema schemaLZAccess = new LZAccessSchema();
			LZAccessSet setLZAccess = new LZAccessSet();
			
			for (int index=0; index< tRadio.length;index++)
	    {
	      if(tRadio[index].equals("1"))
	      {
	      	schemaLZAccess.setSendOutCom(tSendOutCom[index]);
	      	schemaLZAccess.setReceiveCom(tReceiveCom[index]);
	        System.out.println("sendout: "+tSendOutCom[index]+"receive: "+tReceiveCom[index]);
	      }
	      setLZAccess.add(schemaLZAccess);
	    }  
	    
	    // 准备传输数据 VData
		  VData vData = new VData();
		
		  vData.addElement(globalInput);
		  vData.addElement(setLZAccess);
		  vData.addElement(operateFlag);

		  // 数据传输
		  CertifySendConUI tCertifySendConUI = new CertifySendConUI();
	
		  if (!tCertifySendConUI.submitData(vData, "DELETE||CONFIG")) 
		  {
		    Content = " 保存失败，原因是: " + tCertifySendConUI.mErrors.getFirstError();
		    FlagStr = "Fail";
		    
		    vData = tCertifySendConUI.getResult();
		  } 
		  else 
		  {
		  	Content = " 保存成功 ";
		  	FlagStr = "Succ";
		  	
		  	vData = tCertifySendConUI.getResult();
		  	String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
		  	session.setAttribute("TakeBackNo", strTakeBackNo);
	 			session.setAttribute("State", CertStatBL.PRT_STATE);
		  }
	    
		}
		catch(Exception ex)
		{
			ex.printStackTrace( );
	   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
	   	FlagStr = "Fail";
		}
	}
%>
<html>
  <script language="javascript">
  	<%= szFailSet %>
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
<body>
</body>
</html>

