<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：核销管理
//程序功能：
//创建日期：2002-10-08
//创建人  ：Javabean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="FeeInvoiceModify.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FeeInvoiceInit.jsp"%>

<script language="javascript">
 function initDate(){
 	fm.StartDate.value="<%=afterdate%>";
  fm.EndDate.value="<%=CurrentDate%>";
  queryList();
 }
</script>
</head>
<body  onload="initForm();initDate();" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./FeeInvoiceSave.jsp" method=post name="fm" target="fraSubmit">
    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>发票状态修改</td></tr>
    </table>
    
	<Div  id= "divCertifyList" style= "display: ''">
	    <table  class= common>
        <TR  class= common>
          <TD  class= title>最终修改起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>最终修改止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR class = common >
          <TD class= title ><input class="cssButton" type="button" value="查  询" onclick="queryList()" > </TD>
          <TD>
          <font color="#FF0000" size="2">
          只能对作废和正常回销的发票进行修改 
    		  </font>
    		  </TD>
        </TR>
	  	</table>
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanCertifyList" ></span>
         	</td>
       	</tr>
	  	</table>
	  	<br>
	  <input class="cssButton" type="button" value="修  改" onclick="submitForm()" >
	</div>
	
	<br>
	
	<table>
   	  <tr>
        <td class=common>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyStroe);">
        </td>
    	<td class= titleImg>现有作废、正常回销发票信息</td></tr>
    </table>
		
		<div id="divShowStore" style="display:''">
			<div id="divCertifyStroe">
	      <table class="common">
	        <tr class="common">
	          <td text-align: left colSpan=1><span id="spanCertifyStoreList"></span></td></tr>
		  	</table>
			</div>
			<Div id= "divPage" align=center style= "display:''">
			  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage1.firstPage();"> 
			  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
			  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
			  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage1.lastPage();">
	  	</Div>
  	</div>
		
	<input type="hidden" name = "CertifyCode" value="<%=CertifyCode%>" >
	<input type="hidden" name = "ManageCom" value="<%=ManageCom%>" >
	<input type="hidden" name = "Operator" value="<%=Operator%>">
	<input type="hidden" name="AuditKind" value="">
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   
  </form>
</body>
</html>
