<html>
<%
//name :单证报表统计
//Creator :longcb
//date :2010-11-15
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
    GlobalInput tGI = new GlobalInput();        
    tGI = (GlobalInput)session.getValue("GI");
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT>
var managecom = <%=tGI.ManageCom%>;//管理机构
</SCRIPT> 

  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="CardReceive.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CardReceiveInit.jsp"%>
	</head>
<body  onload="initForm();initElementtype();" >
  <form action="./CardReceiveQueryReport.jsp" method=post name=fm target="fraSubmit">	
  <Div id= "divLLReport1" style= "display: ''">

   	<table class= common> 
   	    <tr class= common>
		<TD class= title> 管理机构 </TD>
        <TD class= input> 
        	<Input name=ManageCom class="codeno"  verify="管理机构|code:comcode&NOTNULL"
        	ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
        	onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
        	><input class=codename name=ManageComName readonly elementtype=nacessary> 
        </TD>   			
   		   <td class="title">单证编码</td>
           <td class="input">
           <input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('cardcode', [this,CertifyCodeName],[0,1,2],null,null,null,1,350);" onkeyup="return showCodeListKey('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);"><Input class= codename name="CertifyCodeName" readonly></td>
        </tr> 	
   		<tr class= common>
          <TD class= title>
   			   查询主体
          </TD>
          <TD class="input" nowrap=true>           	
          	<input class="codeno" name="AgentCode" ondblclick="return showCodeList('sendoutcodesearch', [this],[0],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcodesearch', [this],[0],null,null,null,1);" type=hidden>
            <input class="common" name="GroupAgentCode" elementtype=nacessary readonly>
          	<input type="button" class="button" name="AgentQuery" value="业务员查询" onclick="queryAgent()" >
          	<input type="button" class="button" name="btnQueryCom" value="代理机构查询" onclick="queryCom()" ></TD>
          <TD class= title>
   			   状态类型
          </TD>
          <TD  class= input>
            <Input class="codeno"  name=StateType CodeData="0|^0|未领用^1|已领用^2|正常回销^3|空白回销^4|遗失^5|销毁^6|作废^7|已发放^12|已录入^13|已导入^14|已录单"
             ondblClick="showCodeListEx('CertifyStateList',[this,StateName],[0,1],null,null,null,1);"
             onkeyup="showCodeListKeyEx('CertifyStateList',[this,StateName],[0,1],null,null,null,1);"><Input class="codename"  name=StateName >

          </TD>
      </TR>
      <TR class= common>   			
          <TD class= title>查询起期</TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>查询止期</TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary>
          </TD>  
   		</TR>
       <Input type= "hidden" name= PubSQL>
        <Input type= "hidden" name= ReportType>      
 		</table>
       	<input class="cssButton" type= button value="查    询" onclick="queryInfo();">
       	<input class="cssButton" type= button value="打印报表" onclick="printInfo();">
    <table>
    	<tr>
    	<td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLLReport2);"></td>
        <td class= titleImg>
    			 信息列表
    		</td>
    	</tr>
    </table>

	<Div  id= "divLLReport2" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1>
  				<span id="spanCertifyMaxGrid" >
  				</span>
  			</td>
  		</tr>
    </table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
     </center>  	
  	</Div>
 	</div>
 	
 	<div id="mydiv" style="display: 'none'">
 	<table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCardState);"></td>
    	<td class= titleImg >单证明细</td></tr>
    </table>

  	<div id="divCardState" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCardState"></span></td></tr>
    </table>
    <Div  id= "divPage1" align=center style= "display: 'none' ">
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
     </center>  	
  	</Div>
    </div>
    </div>
    <input name="TempComCode" type="hidden" value="<%=tGI.ComCode%>">
    <input name="TempOpe" type="hidden" value="<%=tGI.Operator%>">
    <input type=hidden name="ReceiveType" value="COM">
    <input type=hidden name="tManageCom" value="<%=tGI.ManageCom%>">
    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup value=''> 
    <input type=hidden name=querySQL value=""> 
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>