<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskGetPrint.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
		System.out.println("start");
		CError cError = new CError( );
		boolean operFlag=true;
		String tRela  = "";
		String FlagStr = "";
		String Content = "";
		String strOperation = "";
		GlobalInput tGI = new GlobalInput(); //repair:
    tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
    if(tGI==null)
	  {
	    System.out.println("页面失效,请重新登陆");   
	    FlagStr = "Fail";        
	    Content = "页面失效,请重新登陆";  
	  }
	  else //页面有效
	  {
		
			String mSOL=request.getParameter("PubSQL");	
			String mReportType=request.getParameter("ReportType");
			String mStartDate=request.getParameter("StartDate");	
			String mEndDate=request.getParameter("EndDate");	
			String mReceiveCom=request.getParameter("ReceiveCom");	
			String mCardType=request.getParameter("CardType");	
			String mStateType=request.getParameter("StateType");	
			String mManCom=request.getParameter("ManCom");
			
				
			System.out.println(mReportType);
		  System.out.println(mSOL);
		  System.out.println(mStartDate);
		  System.out.println(mEndDate);
		  System.out.println(mReceiveCom);
		  System.out.println(mCardType);
		  System.out.println(mStateType);
		   System.out.println(mManCom);
			VData mResult = new VData();
			CErrors mErrors = new CErrors();
			
			VData tVData = new VData();				
			
			TransferData tTransferData=new TransferData();
			
	    tTransferData.setNameAndValue("PubSQL",mSOL);
	    tTransferData.setNameAndValue("ReportType",mReportType);
	    tTransferData.setNameAndValue("StartDate",mStartDate);
	    tTransferData.setNameAndValue("EndDate",mEndDate);
	    tTransferData.setNameAndValue("ReceiveCom",mReceiveCom);
	    tTransferData.setNameAndValue("CardType",mCardType);
	    tTransferData.setNameAndValue("StateType",mStateType);
	    tTransferData.setNameAndValue("ManCom",mManCom);
	    
	    tVData.add(tGI);
		  tVData.add(tTransferData);
			CardSendPrintBL tCardSendPrintBL = new CardSendPrintBL();
			XmlExport txmlExport = new XmlExport();
	    if(!tCardSendPrintBL.submitData(tVData,"PRINT"))
	    {
	       	operFlag = false;
	       	Content = tCardSendPrintBL.mErrors.getFirstError().toString();
	    }
	    else
	    {
				mResult = tCardSendPrintBL.getResult();
				txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
				if(txmlExport==null)
				{
					operFlag=false;
					Content="没有得到要显示的数据文件";
				}
			}
			LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		  tLDSysVarDB.setSysVar("VTSFilePath");
		  tLDSysVarDB.getInfo();
		  String vtsPath = tLDSysVarDB.getSysVarValue();
		  if (vtsPath == null)
		  {
		    vtsPath = "vtsfile/";
		  }
		  
		  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
		  String fileName = "TASK" + FileQueue.getFileName()+".vts";
		  String realPath = filePath + fileName;
			
			if (operFlag==true)
			{
		    //合并VTS文件 
		    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
		    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
		    tcombineVts.output(dataStream);
		    
		    //把dataStream存储到磁盘文件
		    AccessVtsFile.saveToFile(dataStream, realPath);
		    response.sendRedirect("GetF1PrintJ1_new.jsp?RealPath=" + realPath);
			}
			else
			{
		    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
</script>
</html>
<%
  	}
  }		
%>