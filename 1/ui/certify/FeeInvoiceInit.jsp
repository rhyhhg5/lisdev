<%
//程序名称：CertifyTakeBackInit.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%> 
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
  String Operator=globalInput.Operator;
  String Comcode=globalInput.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
  String AheadDays="-90";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
  String ManageCom = "";
  String CertifyCode = "";
  String CertifyName = "";
  if (Comcode.length()==8) {
    ManageCom = Comcode;
    ExeSQL tExeSQL = new ExeSQL();
    String tSQL = "select a.codename,b.certifyname from ldcode a,lmcertifydes b where a.codetype='invoicetype' and a.code='"
                + ManageCom
                + "' and a.codename=b.certifycode with ur";
    SSRS tSSRS = tExeSQL.execSQL(tSQL);
    CertifyCode = tSSRS.GetText(1, 1);
    CertifyName = tSSRS.GetText(1, 2);
  }
%>
<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
  	fm.reset();
  }
  catch(ex)
  {
    alert("在CertifyCancelInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initCertifyList();
    initCertifyStoreList();
  }
  catch(re)
  {
    alert("CertifyCancelInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 单证列表的初始化
function initCertifyList()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		  //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30";        				//列宽
      iArray[0][2]=50;          				//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="管理机构";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[1][14] = "<%=ManageCom%>";
      
      
      iArray[2]=new Array();
      iArray[2][0]="单证编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        			//列宽
      iArray[2][2]=80;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[2][14] = "<%=CertifyCode%>";


      iArray[3]=new Array();
      iArray[3][0]="单证名称";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="100";        		  //列宽
      iArray[3][2]=100;          			//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[3][14] = "<%=CertifyName%>";

      iArray[4]=new Array();
      iArray[4][0]="起始单号";    	    //列名
      iArray[4][1]="180";            		//列宽
      iArray[4][2]=180;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
			iArray[4][9]="起始单号|INT";

      iArray[5]=new Array();
      iArray[5][0]="终止单号";    	    //列名
      iArray[5][1]="180";            		//列宽
      iArray[5][2]=180;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="终止单号|INT";

      iArray[6]=new Array();
      iArray[6][0]="数量";    	        //列名
      iArray[6][1]="50";            		//列宽
      iArray[6][2]=50;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][7]="countNum"  	  			//JS函数名，不加扩号 !!响应事件!!
      
      iArray[7]=new Array();
      iArray[7][0]="处理状态";    	    //列名
      iArray[7][1]="70";            		//列宽
      iArray[7][2]=80;            			//列最大值
      iArray[7][3]=0;              			//表示是代码选择
      iArray[7][14]="已发放"
      
      iArray[8]=new Array();
      iArray[8][0]="处理状态代码";    	//列名
      iArray[8][1]="80";            		//列宽
      iArray[8][2]=80;            			//列最大值
      iArray[8][3]=3;              			//表示是代码选择
      iArray[8][14]="7"; 
   

      CertifyList = new MulLineEnter( "fm" , "CertifyList" ); 
      //这些属性必须在loadMulLine前
      CertifyList.mulLineCount = 1;     
      CertifyList.displayTitle = 1;
      CertifyList.hiddenPlus = 1;     
      CertifyList.hiddenSubtraction = 1;
      CertifyList.loadMulLine(iArray);  
      
    } catch(ex) {
      alert(ex);
    }
}

// 起始单号、终止单号信息列表的初始化
function initCertifyStoreList()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      //iArray[1][4]="CertifyCode";     //是否引用代码:null||""为不引用
      //iArray[1][5]="1|2";             //引用代码对应第几列，'|'为分割符
      //iArray[1][6]="0|1";             //上面的列中放置引用代码中第几位值
      //iArray[1][9]="单证编码|code:CertifyCode&NOTNULL";
      //iArray[1][19]=1;   		//1是需要强制刷新.

      iArray[2]=new Array();
      iArray[2][0]="起始单号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="180";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="终止单号";    	    //列名
      iArray[3][1]="180";            		//列宽
      iArray[3][2]=180;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="数量";    	    //列名
      iArray[4][1]="50";            		//列宽
      iArray[4][2]=180;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="发票状态";    	        //列名
      iArray[5][1]="80";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="单证持有者";    	        //列名
      iArray[6][1]="100";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      CertifyStoreList = new MulLineEnter( "fm" , "CertifyStoreList" );
      //这些属性必须在loadMulLine前
      CertifyStoreList.displayTitle = 1;
      CertifyStoreList.mulLineCount = 0;   
      CertifyStoreList.hiddenPlus = 1;        
      CertifyStoreList.hiddenSubtraction = 1; 
      
      CertifyStoreList.loadMulLine(iArray);
      
      //CertifyList.delBlankLine("CertifyList");

    } catch(ex) {
      alert(ex);
    }
}

function getManageName(cCode)
{
	var strSQL = "select Name from LDCom where ComCode = '"+cCode+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult!=null)
	{
		var cName = arrResult[0];
		return cName;
	}
	else
	{
		return '';
	}
}

</script>