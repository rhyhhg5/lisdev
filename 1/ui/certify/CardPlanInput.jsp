<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CardPlan.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CardPlanInit.jsp"%>
  <title>本级单证计划处理</title>
</head>
<body  onload="initForm();" >
  <form action="./CardPlanSave.jsp" method=post name=fm target="fraSubmit">
    <table class="common" border=0 width=100%>
      <tr><td class="titleImg" align="center">本级单证计划：</td></tr>
	  </table>
    
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <td class="title">计划标识</td>
        <td class="input"><input class="readonly" readonly name="PlanID"></td>

        <td class="title">单证编码</td>
        <td class="input">
          <input class="codeno" name="CertifyCode"  
          	ondblclick="return showCodeList('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,250);"
	          onkeyup="return showCodeListKey('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,250);"
            verify="单证编码|NOTNULL"><input class = "codename" name ="CertifyCodeName" ></td>
      </tr>

      <tr class="common">
        <td class="title">申请机构</td>
        <td class="input"><input class="readonly" readonly name="AppCom"></td>

        <td class="title">申请数量</td>
        <td class="input"><input class="common" name="AppCount"></td>
      </tr>
        
      <tr class="common">
        <td class="title">批复机构</td>
        <td class="input"><input class="readonly" readonly name="RetCom"></td>
        
        <td class="title">关联印刷</td>
        <td class="input"><input class="common" name="RelaPrint"></td></tr>

      <tr class="common">
        <td class="title">入机日期</td>
        <td class="input"><input class="readonly" readonly name="MakeDate"></td>
        
        <td class="title">入机时间</td>
        <td class="input"><input class="readonly" readonly name="MakeTime"></td></tr>

      <tr class="common">
        <td class="title">批复状态</td>
        <td class="input"><input class="codeno" name="RetState" CodeData="0|^Y|同意|^N|不同意|"
            ondblclick="return showCodeListEx('RetState', [this,RetStateName],[0,1]);"
            onkeyup="return showCodeListKeyEx('RetState', [this,RetStateName],[0,1]);"><input class = codename name = RetStateName></td>
        
        <td class="title">计划标志</td>
        <td class="input"><input class="codeno" name="PlanState" CodeData="0|^A|申请|^C|确认|^R|批复|^P|归档|"
            ondblclick="return showCodeListEx('PlanState', [this,PlanStateName],[0,1]);"
            onkeyup="return showCodeListKeyEx('PlanState', [this,PlanStateName],[0,1]);"><input class = codename name = PlanStateName ></td>
            </tr>
    </table>

		<table>
			<tr class="common">
				<td><input value="查询计划" type="button" class="common" onclick="queryPlan('cur');"></td>
    		<td><input value=" 重  置 " type="button" class="common" onclick="initForm();"></td>
    	</tr>
    	<tr class="common">
    		<td><input value="批复计划" type="button" class="common" onclick="retPlan();"></td>
    		<td><input value="归档计划" type="button" class="common" onclick="packPlan();"></td>
    	</tr>
    	<tr class="common">
    		<td><input value="新增计划" type="button" class="common" onclick="addPlan();"></td>
    		<td><input value="修改计划" type="button" class="common" onclick="updatePlan();"></td>
    		<td><input value="删除计划" type="button" class="common" onclick="delPlan();"></td>
    	</tr>
    </table>

    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCardPlanInfo);"></td>
    	<td class= titleImg>关联计划</td></tr>
    </table>
    
  	<div id="divCardPlanInfo" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCardPlanInfo"></span></td></tr>
      </table>

			<input value="增加" type="button" class="common" onclick="queryPlan('sub');">      
  	</div>
  	
  	<!-- extra where condition for query page -->
  	<input type="hidden" name="sql_where" value="">
  	<input type="hidden" name="sql_where_ex" value="">
  	
  	<!-- operator flag -->
  	<input type="hidden" name="oper_flag" value="">
  	
  	<!-- current com -->
  	<input type="hidden" name="cur_com" value = "">
  	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
