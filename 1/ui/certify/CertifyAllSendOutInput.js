var turnPage=new turnPageClass(); 
var turnPage1=new turnPageClass(); 


// 该文件中包含客户端需要处理的函数和事件
// 发放管理


var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{   
	//try {
		 if(fm.ReceiveCom.value==""||fm.ReceiveCom.value==null){
			 alert("接收者不能为空");
			 return false;
		 } 
		 if(fm.SendOutComEx.value==""||fm.SendOutComEx.value==null){
			alert("发放者不能为空");
			return false;
		 }
		 //add by zjd 校验单证下发最大号
		 var tendno=CertifyList.getRowColData(0,4);
		 var tmanxendno=CertifyStoreList.getRowColData(0,3);
		 for(var i=0;i<CertifyStoreList.mulLineCount;i++){
			 var maxendno=CertifyStoreList.getRowColData(i,3);
			 if(parseFloat(maxendno) > parseFloat(tmanxendno)){
				 tmanxendno= maxendno;
			 }
		 }
		 if(parseFloat(tendno) > parseFloat(tmanxendno) ){
			 alert("下发单证终止号超限！");
			 return false;
		 }
		 //add by zjd 校验发放数量和接收网点数据匹配问题。
		 if(!checkcounttorecievecom()){
			 return false;
		 }
		 
		if(CertifyList.checkValue("CertifyList")) {
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.SendOutCom.value = fm.SendOutComEx.value;	
				fm.action="./CertifyAllSendOutSave.jsp";
		  	fm.submit(); //提交
	  }
 // } catch(ex) {
  	//	showInfo.close();
  //}
}

function checkcounttorecievecom(){
	 var nStart=CertifyList.getRowColData(0,3);
	 var nEnd=CertifyList.getRowColData(0,4);
	 var nCount=CertifyList.getRowColData(0,5);
	 if(nStart=="" || nStart==null){
		 alert("单证发放起始号不能为空！");
		 return false;
	 }
	 if(nEnd=="" || nEnd==null){
		 alert("单证发放终止号不能为空！");
		 return false;
	 }
	 if(nCount=="" || nCount==null){
		 alert("单证发放每个网点不能为空！");
		 return false;
	 }
	 var arrcount=nEnd-nStart+1;
	 var tsqlbankcount= " select count(*) from ldcode1 f inner join ldcode d  on f.code=d.code where d.codetype = 'dzwd' and f.code='"+fm.ReceiveCom.value+"' and f.codetype = 'certifyagentcomsend' ";
	 var arrResultcount = easyExecSql(tsqlbankcount);
	 if(arrcount!=(arrResultcount[0][0]*nCount)){
		 alert("单证数量网点不能均分，请重新录入号段信息！");
		 return false;
	 }
	return true;
	
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, TakeBackNo, CertifyCode )
{
  showInfo.close();
	
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	    content="保存成功！";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
       showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	     	    
	    if (CertifyCode == null || CertifyCode == '')
	    {
		    CertifyList.clearData("CertifyList");
	    	CertifyList.addOne("CertifyList");
	    	initForm();
	  	}
	  	else
	  	{
	  		CertifyList.setRowColData(0,3,"");
		    CertifyList.setRowColData(0,4,"");
		    CertifyList.setRowColData(0,5,"");
		    afterStore("CertifyCode",CertifyCode);
	  	}
	  }
  
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	if( fm.chkModeBatch.checked == false ) {
	    fm.PrtNoEx.value = arrResult[0][0];
	    fm.ReceiveCom.value = 'A' + arrResult[0][11];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
	    
			var rowCount = 0;

	    CertifyList.setRowColData(rowCount, 1, arrResult[0][1]);
	    CertifyList.setRowColData(rowCount, 2, arrResult[0][25]);
	    CertifyList.setRowColData(rowCount, 3, arrResult[0][18]);
	    CertifyList.setRowColData(rowCount, 4, arrResult[0][19]);
	    
	    var cNum = parseFloat(arrResult[0][19])-parseFloat(arrResult[0][18])+1;
	    CertifyList.setRowColData(rowCount, 5, cNum+"");
	    
	  } else {
	    fm.ReceiveCom.value = arrResult[0][3];
	    fm.ReceiveCom.title = arrResult[0][1];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
		}
  }
}

function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  	fm.AgentCode.value=arrResult[0][0];
	}
}



function queryCertify(){
  showInfo = window.open("./CertifyInfoQuery.jsp?certifyCode=certifycode1");
}


function countNum(parm1,parm2)
{
	var startN=CertifyList.getRowColData(parm1,3);
	var endN=CertifyList.getRowColData(parm1,4);
	var numN=CertifyList.getRowColData(parm1,5);
	if(fm.ReceiveCom.value=="" || fm.ReceiveCom.value==null){
		alert("请录入接收者！");
		return false;
	}
	var tbankcount=easyExecSql("select count(1) from ldcode1 f inner join ldcode d  on f.code=d.code where d.codetype = 'dzwd' and f.code='"+fm.ReceiveCom.value+"' and f.codetype = 'certifyagentcomsend'");
	if (CertifyList.getRowColData(parm1,1)!=""&&CertifyList.getRowColData(parm1,3)!=null)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(parm1,1)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,1)+"的单证");
		  return false;
		}
		else
		{
			var numType = arrResult[0][0];
			
			if (numN==""||numN==null)
			{
				if(startN==""||startN==null)
				{
					CertifyList.setRowColData(parm1,5,"");
					return;
				}
				if (endN==""||endN==null)
				{
					CertifyList.setRowColData(parm1,5,"");
					return;
				}
				
				if (!isNumeric(startN))
				{
					alert("起始单号有非数字字符！");
					return false;
				}
				if (!isNumeric(endN))
				{
					alert("终止单号有非数字字符！");
					return false;
				}
				
				var num=(parseFloat(endN)-parseFloat(startN)+1)%parseFloat(tbankcount[0][0]);
				if (num<0)
				{
					alert("起始单号大于终止单号！");
					return false;
				}
				if(num!=0){
					alert("单证数量网点不能均分,请重新录入！");
					return  false;
				}
				var cnum=(parseFloat(endN)-parseFloat(startN)+1)/parseFloat(tbankcount[0][0]);
				CertifyList.setRowColData(parm1,5,cnum+"");
			}
			else
			{
					if (numType=="Y")
					{
						if(startN==""||startN==null)
						{
							CertifyList.setRowColData(parm1,5,"");
							return;
						}
						if (numN==""||numN==null)
						{
							CertifyList.setRowColData(parm1,5,"");
							return;
						}
						
						if (!isNumeric(startN))
						{
							alert("起始单号有非数字字符！");
							return false;
						}
						if (!isNumeric(numN))
						{
							alert("数量栏有非数字字符！");
							return false;
						}
						var num=parseFloat(parseFloat(numN)*parseFloat(tbankcount[0][0]))+parseFloat(startN-1);
						if(num.length !=startN.length){
							do{
								num="0"+num;
							}while(num.length !=startN.length)
						}
						
						CertifyList.setRowColData(parm1,4,num+"");
					}
			}
		}
	}
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "dzwdcode")
	{
		CertifyList.setRowColData(0,3,"");
		CertifyList.setRowColData(0,4,"");
		CertifyList.setRowColData(0,5,"");
		
		var strSQL = "select havenumber from LMCertifyDes where CertifyCode='"+ Field.value +"'";
		var arrResult = easyExecSql(strSQL);
		var numType = "";
		if(arrResult != null)
		{
			numType = arrResult[0][0];
		}
		
		if (numType=="Y")
		{    
			//fm.HaveNum.value="有号单证";
		}
		else if(numType=="N")
		{
			fm.HaveNum.value="无号单证";
		}
		else
		{
			fm.HaveNum.value="无有号标记";
		}
		strSQL="select sum(SumCount) from LZCard  where  CertifyCode='"+Field.value+"' and receivecom = 'B"+fm.Operator.value+"' and StateFlag in ('0','7','8','9') ";
		var arrResult=easyExecSql(strSQL);
		if(arrResult!=null){
			fm.certifysumcount.value=arrResult[0][0];
		}
		else{
			fm.certifysumcount.value = "0";
		}
	
		
		var strSQL = "";
		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select CertifyCode,StartNo,EndNo,SumCount,Receivecom "
			 + " from LZCard where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field.value+"' and StateFlag in ('0','7','8','9') "
			 + " order by StartNo";      
		}
		else
		{
			strSQL = "select CertifyCode,StartNo,EndNo,SumCount,Receivecom "
			 + " from LZCard where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field.value+"' and StateFlag in ('0','7','8','9')"
			 + " order by StartNo";            
		}
	turnPage.queryModal(strSQL, CertifyStoreList);   
		
	}
}


	/**
   * 发放结束后重新显示现有单证信息
   * <p><b>Example: </b><p>
   * @param codeName 
   * @param Field 单证类型
   * @return 
   */
function afterStore(codeName,Field)
{
	if(codeName == "CertifyCode")
	{
		CertifyList.setRowColData(0,3,"");
		CertifyList.setRowColData(0,4,"");
		CertifyList.setRowColData(0,5,"");
		
		var strSQL = "select havenumber from LMCertifyDes where CertifyCode='"+ Field +"'";
		var arrResult = easyExecSql(strSQL);
		
		var numType = "";
		if(arrResult != null)
		{
			numType = arrResult[0][0];
		}
		
		if (numType=="Y")
		{
			fm.HaveNum.value="有号单证";
		}
		else if(numType=="N")
		{
			fm.HaveNum.value="无号单证";
		}
		else
		{
			fm.HaveNum.value="无有号标记";
		}
		
		var strSQL = "";
		if (fm.ManageCom.value.length<=4)
		{
		strSQL = "select CertifyCode,StartNo,EndNo,SumCount,Receivecom "
		 + " from LZCard where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9') "
		 + " order by StartNo";      
		}
		else
		{
			strSQL = "select CertifyCode,StartNo,EndNo,SumCount,Receivecom "
			 + " from LZCard where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9') "
			 + " order by StartNo";            
		}
		turnPage.queryModal(strSQL, CertifyStoreList); 

		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9')" ; 
		}
		else
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9')" ; 
		}
		var arrResult = easyExecSql(strSQL);
    
		if(arrResult != null)
		{
			fm.certifysumcount.value = arrResult[0][0];
		}
		else
		{
		  fm.certifysumcount.value = "0";
		}
	}
}

function computeNum(parm1,parm2){
    var nStart=CertifyList.getRowColData(parm1,3);
	var nEnd=CertifyList.getRowColData(parm1,4);
	var nCount=CertifyList.getRowColData(parm1,5);
		if(!isNumeric(nStart)){
		  alert("起始单号有非数字字符！");
	      return false
	}
	if(nStart==""||nStart==null){
			alert("起始号不能为空！");
			return false;
	}
	if(nCount==null||nCount==""){
		alert("请输入每个网点的数量");
		return false;
    }
	if(!isNumeric(nCount)){
		alert("数量有非数字字符！");
		return false
	}
	if(fm.ReceiveCom.value=="" || fm.ReceiveCom.value==null){
		alert("请录入接收者！");
		return false;
	}
	var tbankcount=easyExecSql("select count(1) from ldcode1 f inner join ldcode d  on f.code=d.code where d.codetype = 'dzwd' and f.code='"+fm.ReceiveCom.value+"' and f.codetype = 'certifyagentcomsend'");
	
	var num=parseFloat(parseFloat(nCount)*parseFloat(tbankcount[0][0]))+parseFloat(nStart-1);
	if(num.length !=nStart.length){
		do{
			num="0"+num;
		}while(num.length !=nStart.length)
	}
	
	CertifyList.setRowColData(parm1,4,num+"");
	if(nEnd!=""){
		if(nStart>nEnd){
			alert("起始号大于终止号段，请重新输入！！！");		
		      return false;
		}	
	}
	
	


}