<html>
<%
//name :CertifyMaxInput.jsp
//function :Query Certify Info and Show Info Based On ManageCom and AgentGrade
//Creator :刘岩松
//date :2003-04-18
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	
    GlobalInput tGI = new GlobalInput();        
    tGI = (GlobalInput)session.getValue("GI");
 	
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT>
var managecom = <%=tGI.ManageCom%>;//管理机构
</SCRIPT> 	
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="CardSendReportP.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CardSendReportPInit.jsp"%>
	</head>

<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">	
  <Div id= "divLLReport1" style= "display: ''">

   	<table class= common>
   	<tr class="common">
        <td class="title">单证编码</td>
        <td class="input">
          <input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,300);"><Input class= codename name="CertifyCodeName" ></td>
        	 
        <TD  class= title8>
      		管理机构
    	</TD>
    	<TD  class= input8>
      		<Input class=codeNo name=ManageComC   ondblclick="return showCodeList('comcode',[this,ManageComCName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComCName],[0,1],null,null,null,1);"><input class=codename name=ManageComCName >
    	</TD>
    	<tr class= common>   			
          <TD class= title>
   			   起始单号
          </TD>
          <TD  class= input>
            <Input class="common" dateFormat="short" name=StartNo >
          </TD>
          <TD  class= title>
           终止单号
          </TD>
          <TD  class= input>
            <Input class="common" dateFormat="short" name=EndNo >
          </TD> 
   		</TR>
   		
   		  	
   		<tr class= common>   			
          <TD class= title>
   			   经办日期（开始）
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
           经办日期（结束）
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>                     
        <Input type= "hidden" name= PubSQL>
        <Input type= "hidden" name= MagCom>
        <Input type= "hidden" name= ReportType>      
        
   		</TR>
	
 		</table>
       	<input class="cssButton" type= button value="查    询" onclick="queryInfo();">
       	<input class="cssButton" type= button value="打印报表" onclick="printInfo();">
    <table>
    	<tr>
        <td class= titleImg>
    			单证信息
    		</td>
    	</tr>
    </table>

	<Div  id= "divLLReport2" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1>
  				<span id="spanCertifyMaxGrid" >
  				</span>
  			</td>
  		</tr>
    </table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
       <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
     </center>  	
  	</Div>
 	</div>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>