<html>
	<%
		//程序名称：已激活单证的运维功能
		//程序功能：
		//创建日期：2011-6-7
		//创建人  ：xp
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<!--用户校验类-->
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="CertifyActiveModifyInput.js"></SCRIPT>
		<SCRIPT src="CertifyNumberCommon.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="CertifyActiveModifyInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./CertifyActiveModifySave.jsp" method=post name=fm
			target="fraSubmit">
			<Table class=common>
				<tr class=common>
					<td class="title">
						单证编码：
					</td>
					<td class="input">
						<input class="codeno" name="CertifyCode"
							ondblclick="return showCodeList('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);"
							onkeyup="return showCodeListKey('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);"><Input class=codename name="CertifyCodeName" readonly>
					</td>
					<td class=title>
						单证类型码：
					</TD>
					<td class=input>
						<Input class="common" name=CardType readonly>
					</TD>
				</tr>

				<TR class=common>
					<TD class=title>
						单证起始号：
					</TD>
					<TD class=input>
						<Input class=common name=StartNo 
							verify="单证起始号|NUM" maxlength=16>
					</TD>
					<TD class=title>
						单证终止号：
					</TD>
					<TD class=input>
						<Input class=common name=EndNo 
							verify="单证终止号|NUM" maxlength=16>
					</TD>
				</TR>
			</Table>
			<input class="cssButton" type=button value="查询" name="query"
				ID="query" onclick="Query()">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divCertifyActive);">
					</td>
					<td class=titleImg>
						单证列表
					</td>
				</tr>
			</table>
			<div id="divCertifyActive">
				<table class="common">
					<tr class="common">
						<td text-align: left colSpan=1>
							<span id="spanCertifyActiveGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<Div id= "divPage" align=center style= "display:''">
				  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage.firstPage();"> 
				  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
				  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
				  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage.lastPage();">
		  	</Div>
			<p><hr></p>
			<Table class=common>
							<tr class=common>
					<td class="title">
						单证编码：
					</td>
					<td class="input">
						<input class="codeno" name="CertifyCodeModify"
							ondblclick="return showCodeList('cardcode', [this,CertifyCodeNameModify,CardTypeModify],[0,1,2],null,null,null,1,350);"
							onkeyup="return showCodeListKey('cardcode', [this,CertifyCodeNameModify,CardTypeModify],[0,1,2],null,null,null,1,350);"><Input class=codename name="CertifyCodeNameModify" elementtype=nacessary readonly>
					</td>
					<td class=title>
						单证类型码：
					</TD>
					<td class=input>
						<Input class="common" name=CardTypeModify elementtype=nacessary readonly>
					</TD>
				</tr>

				<TR class=common>
					<TD class=title>
						单证起始号：
					</TD>
					<TD class=input>
						<Input class=common name=StartNoModify elementtype=nacessary
							verify="单证起始号|NUM&NOTNULL" maxlength=16>
					</TD>
					<TD class=title>
						单证终止号：
					</TD>
					<TD class=input>
						<Input class=common name=EndNoModify elementtype=nacessary
							verify="单证终止号|NUM&NOTNULL" maxlength=16>
					</TD>
				</TR>
				<TR>
					<td class="title">
						接收者
					</td>
					<td class="input" nowrap=true>
						<input type="hidden" class="codename" name=AgentCode>
						<input type="text" class="common" name=GroupAgentCode elementtype=nacessary verify="接收者|NOTNULL">
						<input name="AgentPer" class="button" type="hidden" value="代理人查询"
							onclick="queryAgent(1)">

						<input type="button" class="button" name=AgentQuery value="业务员查询"
							onclick="queryAgent()">

						<input name="btnQueryCom" class="button" type="button"
							value="代理机构查询" onclick="queryCom()">
					</td>
				</TR>
			</Table>
			<Input class=common type=hidden name=SendOutCom>
			<Input class=common type=hidden name=AgentTypeOld>
			<Input class=common type=hidden name=AgentType>
			<input type=hidden name="ManageCom" value="<%=strCom%>">
			<input type=hidden name="Operator" value="<%=strOperator%>">
			<input class="cssButton" type=button value="变更" name="modify"
				ID="modify" onclick="submitForm()">
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
