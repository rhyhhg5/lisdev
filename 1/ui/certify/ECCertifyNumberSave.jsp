<%
//程序名称：LLReportInput.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%> 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.certify.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  
  ECCertifyNumberUI mECCertifyNumberUI = new ECCertifyNumberUI();
  CErrors tError = null;
  String mPtrNo = request.getParameter("PrtNo"); 					//批次号
  String mCertifyCode = request.getParameter("CertifyCode");		//单证编码
  String mCardType = request.getParameter("CardType");				//单证类型
  String mCardSerNo  = request.getParameter("StartSerNo");			//单证起始号
  String mCardAmount = request.getParameter("CardAmount");			//生成单证数量
  String mCardTypeName = request.getParameter("CardTypeName");		//单证类型名
  String mOperateType = request.getParameter("OperateType"); 		//业务类型   
  String mChkRuleName = request.getParameter("ChkRuleName");		//校验规则名称
  String mCertifyLengthV = request.getParameter("CertifyLengthV");	//单证号码长度
  String mCertifyPrice = request.getParameter("CertifyPrice");		//单证价格
  String mCreatePass = request.getParameter("CreatePass");			//是否创建密码
  String mSendOutComEx = request.getParameter("SendOutComEx");		//发放者
  String mAgentCode = request.getParameter("AgentCode");			//接收者
  

  String mPageType = request.getParameter("PageType"); 
  String operate = request.getParameter("Operate");   
  String mCheckRule = request.getParameter("CheckRule");   //校验规则   2007-9-14 9:30
  

  
  System.out.println("开始进行获取数据的操作！！！");
  System.out.println("mCardType" + mCardType);
  System.out.println("mCardSerNo" + mCardSerNo);
  System.out.println("mOperateType" + mOperateType);
  System.out.println("mPtrNo" + mPtrNo);
  System.out.println("mPageType" + mPageType);
  System.out.println("Operate" + operate);
  System.out.println("CheckRule" + mCheckRule);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";  
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }else{  
	  VData tVData = new VData();
	  try
	  {
	    TransferData tTransferData=new TransferData();	
	    tTransferData.setNameAndValue("PrtNo", mPtrNo);
	    tTransferData.setNameAndValue("CertifyCode", mCertifyCode);
	    tTransferData.setNameAndValue("CardType", mCardType);
	    tTransferData.setNameAndValue("CardSerNo", mCardSerNo);
	    tTransferData.setNameAndValue("CardAmount", mCardAmount);
	    tTransferData.setNameAndValue("CardTypeName", mCardTypeName);
	    tTransferData.setNameAndValue("OperateType", mOperateType); 
	    tTransferData.setNameAndValue("ChkRuleName", mChkRuleName);
	    tTransferData.setNameAndValue("CertifyLengthV", mCertifyLengthV);
	    tTransferData.setNameAndValue("CertifyPrice", mCertifyPrice);
	    tTransferData.setNameAndValue("CreatePass", mCreatePass);
	    tTransferData.setNameAndValue("SendOutComEx", mSendOutComEx);
	    tTransferData.setNameAndValue("AgentCode", mAgentCode);
	    
	    tTransferData.setNameAndValue("PageType", mPageType);	
	    tTransferData.setNameAndValue("Operate", operate);   
	    tTransferData.setNameAndValue("CheckRule", mCheckRule);   //封装校验规则 2007-9-8
	      	    
	    tVData.addElement(tTransferData);
	    tVData.addElement(tGI);
	    //修改类型为 operate     zhangjianbao    2007-10-27 
	    mECCertifyNumberUI.submitData(tVData,null);
			if (!mECCertifyNumberUI.mErrors.needDealError())
	    {                          
	       Content = " 操作成功";
	       FlagStr = "Succ";
	    }
	    else                                                                           
	    {
	       Content =" 失败，原因是:" + mECCertifyNumberUI.mErrors.getFirstError();
	       FlagStr = "Fail";
	    }       
	  }
	  catch(Exception ex)
	  {
	    Content = "失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }
		
		
	}
	%>
<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>