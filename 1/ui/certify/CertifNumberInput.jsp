<html>
<%
//程序名称：银行代收对帐清单
//程序功能：
//创建日期：2003-3-25
//创建人  ：刘岩松程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.bank.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="CertifyNumberInput.js"></SCRIPT>
<SCRIPT src="CertifyNumberCommon.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyNumberInputInit.jsp"%>
<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
%>
</head>
<SCRIPT>
	var strSql =" select Maxno from LDMaxNo where Notype='CERTIFYPRTNO' ";  
	var rs = easyExecSql(strSql);
	var maxPrtNo =1;
	if(rs && rs.length>0){
	  maxPrtNo = rs[0][0];
	}
	var tManageCom = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
</SCRIPT> 
<body  onload="initForm();initElementtype();" >
	<form action="./CertifyDescQuerySave.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
	        <!-- 显示或隐藏LLReport1的信息 -->
	
		<Div id= "divLLReport1" style= "display: ''">
	
		<Table class= common>   		
			<TR class= common>
				<TD class= title>  </TD>
				<TD class= input>  </TD>
				<TD  class= title> 批次号： </TD>
				<TD class= input><Input class= common name= PrtNo readonly></TD>
			</TR>
			<tr class= common>
				<td class="title"> 单证类型： </td>
				<td class="input"><input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);" onkeyup="return showCodeListKey('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);"><Input class= codename name="CertifyCodeName" readonly></td>
				<td class= title> 单证类型码： </TD>
				<td class= input><Input class="common" dateFormat="short" name=CardType ></TD> 
			</tr>
			<TR class= common>
				<TD class= title> 单证起始号： </TD>
				<TD class= input> <Input class= common name= StartSerNo elementtype=nacessary  verify="单证起始号|NUM&NOTNULL" maxlength=16 > </TD>
				<TD class= title> 生成单证数量： </TD>
				<TD class= input> <Input class= common name= CardNum elementtype=nacessary verify="生成单证数量|NUM&NOTNULL"> </TD>
			</TR>
			<TR class= common>
				<TD class= title> 业务类型： </TD>
				<TD class= input> <Input class= common name= CardTypeName readonly > </TD>
				<TD class= title> 业务类型编码： </TD>
				<TD  class= title> <Input class= common name= 'OperateType' elementtype=nacessary readonly > </TD>
			</TR>
			<TR>
				<TD class= title>校验规则：</TD>
				<TD class= input><Input class= common name=ChkRuleName readonly><Input class= common type= hidden name= CheckRule></TD>
				<TD class= title>单证号码长度：</TD>
				<td class="input"><Input class= common name=CertifyLengthV elementtype=nacessary readonly ></td>
			</TR>
		</Table>
		<p></p>
		<Input class= common type= hidden name= MaxSerNo >
		<Input class= common type= hidden name= PageType value="0">
		<Input class= common type= hidden name= Operate>
		<Input class= common type= hidden name= CertifyLength>
		<input class="cssButton" type=button value="生成单证号码" onclick="submitForm()">
		<input class="cssButton" type=button value="下载清单" onclick="printData();">
		<input class="cssButton" type=button value="下载成功" onclick="confirmClike();">
	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
