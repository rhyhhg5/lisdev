<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String ManageCom = request.getParameter("ManageCom"); 
  String BranchType = request.getParameter("BranchType");
  String BranchType2 = request.getParameter("BranchType2");
  String BankType = request.getParameter("BankType");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAComQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAComQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>代理机构 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAComQuerySubmit.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACom1);"></IMG></td>
        <td class=titleImg> 查询条件 </td>
      </tr>
    </table>
    <Div  id= "divLACom1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 代理机构编码  </td>
        <td  class= input> <input class=code name=AgentCom 	
 	           ondblclick="return showCodeList('AgentCom',[this,Name],[0,1],null,null);" 
		         onkeyup="return showCodeListKey('AgentCom',[this,Name],[0,1],null,null);"></td>
        <td  class= title> 代理机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
      </tr>
      <!--tr  class= common>
        <td  class= title> 联系人 </td>
        <td  class= input> <input  class= common name=LinkMan> </td>
	      <td  class= title> 单位性质 </td>
        <td  class= input> <input   name=GrpNature class='codeno'
		           ondblclick="return showCodeList('GrpNature',[this,GrpNatureName],[0,1]);" 
		           onkeyup="return showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);"><Input class=codename name=GrpNatureName readOnly ></td>
      </tr-->     
      <tr  class= common>
	      <td  class= title> 级别 </td>
        <td  class= input> <input name=BankType class='codeno' 
        	   ondblclick="return showCodeList('BankType2',[this,BankTypeName],[0,1]);" 
		         onkeyup="return showCodeListKey('BankType2',[this,BankTypeName],[0,1]);"><Input class=codename name=BankTypeName readOnly > </td> 
	      <td  class= title> 销售资格 </td>
        <td  class= input> <input  name=SellFlag class= 'codeno'
		           ondblclick="return showCodeList('YesNo',[this,SellFlagName],[0,1]);" 
		           onkeyup="return showCodeListKey('YesNo',[this,SellFlagName],[0,1]);"><Input class=codename name=SellFlagName readOnly > 
	      </td>
      </tr>
      <tr  class= common>
        <td  class= title> 管理机构 </td>
        <td  class= input><Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
         </td> 
            <!--td  class= title> 代理机构类别 </td>
        <td  class= input> <input name=ACType class=codeno CodeData="0|^1|专业代理|^2|兼业代理|^3|其他" ondblClick="showCodeListEx('ACType',[this,ACTypeName],[0,1])" onkeyup="showCodeListKeyEx('ACType',[this,ACTypeName],[0,1])" ><Input class=codename name=ACTypeName readOnly > </td--> 
      </tr>
   </table>
   <BR>
    </Div>
    <div style="display: none">
        <TD  class= title>展业机构类型</TD>
        <TD  class= input><Input class='common' name=BranchType ></TD>
        <TD  class= input><Input class='common' name=BranchType2 ></TD>
    </div>
    <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <INPUT VALUE="返  回"  class = cssButton TYPE=button onclick="returnParent();">					
   <BR>
   <BR>
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divComGrid);"></td>
    	<td class= titleImg> 查询结果 </td>
      </tr>
    </table>
    <Div  id= "divComGrid" style= "display: ''">
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  	    <span id="spanComGrid" >
  	    </span> 
  	  </td>
  	</tr>
      </table>
      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 					
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
