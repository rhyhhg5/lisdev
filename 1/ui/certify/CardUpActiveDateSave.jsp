
<%
	//程序名称：CertifyActiveModifySave.jsp
	//程序功能：
	//创建日期：2011-6-13
	//创建人  ：JavaBean
	//更新记录：  更新人    更新日期     更新原因/内容
	//
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput globalInput = new GlobalInput();
	globalInput.setSchema((GlobalInput) session.getValue("GI"));
	System.out.println("有没有到达这里啊 ？");

	try {

		String tChk[] = request
		.getParameterValues("InpCertifyActiveGridChk");
		String mPrtno[] = request
		.getParameterValues("CertifyActiveGrid1");
		String tActiveate[] = request
		.getParameterValues("CertifyActiveGrid6");

		LZCardPrintSet mLZCardPrintSet = new LZCardPrintSet();
		for (int i = 0; i < tChk.length; i++) {
			if (tChk[i].equals("1")) {
		//创建一个新的Schema
		LZCardPrintSchema tLZCardPrintSchema = new LZCardPrintSchema();
		tLZCardPrintSchema.setPrtNo(mPrtno[i].trim());
		tLZCardPrintSchema.setActiveDate(tActiveate[i].trim());

		mLZCardPrintSet.add(tLZCardPrintSchema);

		System.out.println("印刷号啊" + mPrtno[i].trim());
		System.out.println("激活日期啊" + tActiveate[i].trim());
			}
		}
		VData tVData = new VData();
		tVData.addElement(globalInput);
		tVData.addElement(mLZCardPrintSet);

		CardUpActiveDateBL tCardUpActiveDateBL = new CardUpActiveDateBL();

		if (!tCardUpActiveDateBL.submitData(tVData)) {
			Content = " 保存失败，原因是: "
			+ tCardUpActiveDateBL.mErrors.getFirstError();
			FlagStr = "Fail";
		} else {
			Content = " 变更成功 ";
			FlagStr = "Succ";
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		Content = " 保存失败，原因是:" + ex.getMessage();
		FlagStr = "Fail";
	}
%>
<html>
	<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
	<body>
	</body>
</html>

