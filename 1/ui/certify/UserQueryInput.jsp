<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ComQueryInput.jsp
//程序功能：
//创建日期：2006-04-16 17:44:45
//创建人  ：Zhang Bin程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="./UserQuery.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="./UserQueryInit.jsp"%>
<title>机构信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./ComQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divComGrid1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>
  <Div  id= "divComGrid1" style= "display: ''">
  <table class=common>
					<TR>
						<TD  class= title>
							用户机构代码
						</TD>
						<TD  class= input>
							<Input class="codeno" name=ComCode verify="机构代码|notnull&code:ComCode" 
							ondblclick="return showCodeList('ComCode',[this, ComName], [0, 1],null,null,null,1);" 
							onkeyup="return showCodeListKey('ComCode', [this, ComName], [0, 1],null,null,null,1);"><Input class=codename name=ComName >
						</TD>
						<TD  class= title>
						</TD>
						<TD  class= input>
							<Input class= "readonly" readonly name= ComName  >
						</TD>
</table>
 </Div>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divComGrid);">
    		</td>
    		<td class= titleImg>
    			 单证管理员用户信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divComGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanComGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
        <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
  	
  	<Input type="hidden" name=ManageCom >
  	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
