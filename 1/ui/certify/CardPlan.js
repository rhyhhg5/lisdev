
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;

//提交，保存按钮对应操作
function submitForm()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

}

function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CardPlan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

function returnParent()
{
    tRow=PolGrid.getSelNo();
    tCol=1;
    tPolNo = PolGrid.getRowColData(tRow-1,tCol);
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    top.location.href="./CardPlanDetail.jsp?PolNo="+tPolNo;
}

// 在查询结束时，触发这个事件。
function onShowResult(result)
{
	if( result == null ) {
		return;
	}
	
	var vPlanID = result[0];
	
	if( vPlanID == null || vPlanID == "" ) {
		return;
	}

	var vSQL = "select * from lzcardplan where planid = '" + vPlanID + "'";
	
	//using my docode function
	var myResult = myDecodeEasyQueryResult(easyQueryVer3(vSQL));

	if( fm.oper_flag.value == 'cur' ) {	
		fm.PlanID.value = myResult[0][0];
		fm.CertifyCode.value = myResult[0][1];
		fm.AppCom.value = myResult[0][4];
		fm.AppCount.value = myResult[0][2];
		fm.RetCom.value = myResult[0][7];
		fm.RelaPrint.value = myResult[0][10];
		fm.MakeDate.value = myResult[0][11];
		fm.MakeTime.value = myResult[0][12];
		fm.RetState.value = myResult[0][8];
		fm.PlanState.value = myResult[0][15];
		
		vSQL = "select * from lzcardplan where relaplan = '" + vPlanID + "'";
		myResult = myDecodeEasyQueryResult(easyQueryVer3(vSQL));
		
		CardPlanInfo.clearData();
		
		if( myResult == null ) {
			return;
		}

		var vRow;

		for( vRow = 0; vRow < myResult.length; vRow ++ ) {
			CardPlanInfo.addOne();
			
			CardPlanInfo.setRowColData(vRow, 1, myResult[vRow][0]);
			CardPlanInfo.setRowColData(vRow, 2, myResult[vRow][4]);
			CardPlanInfo.setRowColData(vRow, 3, myResult[vRow][1]);
			CardPlanInfo.setRowColData(vRow, 4, myResult[vRow][2]);
		}
	} else {
		CardPlanInfo.addOne();
		
		var vRow = CardPlanInfo.mulLineCount - 1;
		
		CardPlanInfo.setRowColData(vRow, 1, myResult[0][0]);
		CardPlanInfo.setRowColData(vRow, 2, myResult[0][4]);
		CardPlanInfo.setRowColData(vRow, 3, myResult[0][1]);
		CardPlanInfo.setRowColData(vRow, 4, myResult[0][2]);
	}
}

function queryPlan(sQueryType)
{
	if( sQueryType == 'cur' ) {
		fm.sql_where.value = "AppCom = '" + fm.cur_com.value + "'";
	} else if( sQueryType == 'sub' ) {
		fm.sql_where.value = "RetCom = '" + fm.cur_com.value + "'";
	}
	
	fm.oper_flag.value = sQueryType;
	window.open("CardPlanQuery.html");
}

// EasyQuery 的这个函数会和 turnPage对象相互影响，所以就自己照着写了一个
function myDecodeEasyQueryResult(strResult) {
	var arrEasyQuery = new Array();
	var arrRecord = new Array();
	var arrField = new Array();
	var recordNum, fieldNum, i, j;

	if (typeof(strResult) == "undefined" || strResult == "" || strResult == false)	{
		return null;
	}

	//公用常量处理，增强容错性
	if (typeof(RECORDDELIMITER) == "undefined") RECORDDELIMITER = "^";
	if (typeof(FIELDDELIMITER) == "undefined") FIELDDELIMITER = "|";

	try {
	  arrRecord = strResult.split(RECORDDELIMITER);      //拆分查询结果，得到记录数组
	  
	  recordNum = arrRecord.length;
	  for(i=1; i<recordNum; i++) {
	  	arrField = arrRecord[i].split(FIELDDELIMITER); //拆分记录，得到字段数组
	  	
	  	fieldNum = arrField.length;
	  	arrEasyQuery[i - 1] = new Array();
	  	for(j=0; j<fieldNum; j++) {
		  	arrEasyQuery[i - 1][j] = arrField[j];          //形成以行为记录，列为字段的二维数组
		  }
	  }		
	} 
	catch(ex) {
	  alert("拆分数据失败！" + "\n错误原因是：" + ex);
	  return null;  
	}
  
	return arrEasyQuery;
}

function addPlan() {
	if( fm.PlanID.value != null && fm.PlanID.value != '' ) {
		alert("计划标识不为空，请重新输入计划信息！");
		return;
	}
	
	fm.oper_flag.value = "INSERT||MAIN";
	submitForm();
}

function updatePlan() {
	if( fm.PlanID.value == null || fm.PlanID.value == "" ) {
		alert("没有指定要更新计划的标识！");
		return;
	}
	
	fm.oper_flag.value = "UPDATE||MAIN";
	submitForm();
}

function delPlan() {
	if( fm.PlanID.value == null || fm.PlanID.value == "" ) {
		alert("没有指定要删除计划的标识！");
		return;
	}
	
	fm.oper_flag.value = "DELETE||MAIN";
	submitForm();
}

function packPlan() {
	if( fm.PlanID.value == null || fm.PlanID.value == "" ) {
		alert("没有指定要归档计划的标识！");
		return;
	}
	
	fm.oper_flag.value = "PACK||MAIN";
	submitForm();
}

function retPlan()
{
	if( fm.PlanID.value == null || fm.PlanID.value == "" ) {
		alert("没有指定要批复的单证计划！");
		return;
	}
	fm.sql_where.value = " and planid = '" + fm.PlanID.value + "'";
	fm.sql_where_ex.value = " and relaplan = '" + fm.PlanID.value + "'";
	window.open("./RetCardPlan.html");
}
