<%@page contentType="text/html;charset=gb2312" %>

<%
//程序名称：CertifyReportPrint.jsp
//程序功能：
//创建日期：2003-06-18
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%
        
      //输出参数
        String FlagStr = "Fail";
        String Content = "";
        boolean bContinue = true;
        
        GlobalInput globalInput = new GlobalInput( );
        globalInput.setSchema( (GlobalInput)session.getValue("GI") );
        
        String Sql=request.getParameter("Strsql");
	try {
	  
	  // 准备传输数据 VData
	  VData vData = new VData();
          vData.addElement(globalInput);
	  vData.addElement(Sql);
          // 数据传输
	  CertPlanUI tCertPlanUI = new CertPlanUI();
	  if (!tCertPlanUI.submitData(vData,"PRINT")) {
	     System.out.println("gx1111111111");
	    Content = " 保存失败，原因是: " + tCertPlanUI.mErrors.getFirstError();
	    FlagStr = "Fail";
%>
		  <script language="javascript">
			  alert('<%= Content %>');
			  window.opener = null;
			  window.close();
		  </script>
<%
	   return;

	  } else {
	  	Content = " 保存成功 ";
	  	FlagStr = "Succ";
                vData = tCertPlanUI.getResult();
                XmlExport xe = (XmlExport)vData.getObjectByObjectName("XmlExport",0);
		session.setAttribute("PrintStream", xe.getInputStream());
		System.out.println("put session value");
		response.sendRedirect("../f1print/GetF1Print.jsp");
	  }
	  
	} catch(Exception ex) {
	    ex.printStackTrace( );
   	    Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   	    FlagStr = "Fail";
	}
%>