//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm(){
	if(fm.all('CertifyCodeModify').value==""){
	alert("请选择单证编码!");
	return false;
	}
	if(!verifyInput()){
	return false;
	}
	if(!CheckState()){
	return false;
	}
	if(!CheckAgent()){
	return false;
	}
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交  
}

//单证类型码变更时
function Query()
{
	if(fm.all('CertifyCode').value==""){
	alert("请选择单证编码");
	return false;
	}
   var strSQL  = "select CertifyCode, (select certifyname from lmcertifydes where certifycode=a.certifycode), "
   			+ " (case when substr(receivecom,1,1) = 'D' then (select groupagentcode from laagent where agentcode = substr(receivecom,2)) else substr(receivecom,2) end ), "
   			+ " startno,endno,subcode "
            + "from LZCard a "
            + "where 1=1 "
            + "and a.SendOutCom = 'B" + fm.Operator.value + "' "
            + "and a.State ='14' and ActiveFlag = '0' "
            + getWherePart('a.CertifyCode', 'CertifyCode')
            + getWherePart('a.StartNo', 'StartNo','>=')
            + getWherePart('a.StartNo', 'EndNo','<=')
            + getWherePart('a.EndNo', 'StartNo','>=')
            + getWherePart('a.EndNo', 'EndNo','<=')
            + "order by a.startno ";
	turnPage.queryModal(strSQL, CertifyActiveGrid);
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
}

//批量发放。查询代理机构的函数。
function queryCom()
{
	fm.all('AgentTypeOld').value="E";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCode').value == "")	
  {  
	  var newWindow = window.open("../sys/LAComQueryInput.jsp?ManageCom="+fm.all('ManageCom').value,"LAComQueryInput",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select a.AgentCom,a.Name,a.ManageCom"	       
	         +" from LACom a where a.AgentCom='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) 
    {
      alert("查询结果:  代理机构编码:["+arrResult[0][0]+"] 代理机构名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此代理机构！");
    } 
	}
}

function queryAgent(ageType)
{
	fm.all('AgentTypeOld').value="D";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}	
  if(fm.all('AgentCode').value == "")	
  {    
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
		if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) 
    {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}

function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  		fm.AgentCode.value=arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][95];
  		fm.all('AgentType').value=fm.all('AgentTypeOld').value
	}
}

function SetModifyInfo()
{
	var tSel = CertifyActiveGrid.getSelNo();
	fm.all('CertifyCodeModify').value = CertifyActiveGrid.getRowColData(tSel-1,1);
	fm.all('CertifyCodeNameModify').value = CertifyActiveGrid.getRowColData(tSel-1,2);
	fm.all('CardTypeModify').value = CertifyActiveGrid.getRowColData(tSel-1,6);
	fm.all('StartNoModify').value = CertifyActiveGrid.getRowColData(tSel-1,4);
	fm.all('EndNoModify').value = CertifyActiveGrid.getRowColData(tSel-1,5);
}

function checknull()
{
	if(fm.all('AgentCode').value==null||fm.all('AgentCode').value==""){
	alert("请先进行查询!");
	}
}

function CheckState()
{
	var strSql1 = "select 1 from lzcard where subcode='" + fm.all('CardTypeModify').value +"' and startno='"+fm.all('StartNoModify').value+"' and endno='"+fm.all('StartNoModify').value+"' and state='14'";
    var arrResult1 = easyExecSql(strSql1);
	if(arrResult1==null){
	alert("单证起始号:"+fm.all('StartNoModify').value+"不是已激活状态,无法在此页面进行调整!");
	return false;
	}
	var strSql2 = "select 1 from lzcard where subcode='" + fm.all('CardTypeModify').value +"' and startno='"+fm.all('EndNoModify').value+"' and endno='"+fm.all('EndNoModify').value+"' and state='14'";
    var arrResult2 = easyExecSql(strSql2);
	if(arrResult2==null){
	alert("单证终止号:"+fm.all('EndNoModify').value+"不是已激活状态,无法在此页面进行调整!");
	return false;
	}
	var strSql3 = "select startno from lzcard where subcode='" + fm.all('CardTypeModify').value +"' and startno>='"+fm.all('StartNoModify').value+"' and endno<='"+fm.all('EndNoModify').value+"' and state!='14' order by startno fetch first 1 row only ";
    var arrResult3 = easyExecSql(strSql3);
	if(arrResult3!=null && arrResult3[0][0]!=""){
	alert("单证序号为("+arrResult3[0][0]+")的单证不是已激活状态,无法在此页面进行调整!");
	return false;
	}
	var strSql4 = "select startno from lzcard where subcode='" + fm.all('CardTypeModify').value +"' and startno>='"+fm.all('StartNoModify').value+"' and endno<='"+fm.all('EndNoModify').value+"' and state='14' and startno!=endno ";
    var arrResult4 = easyExecSql(strSql4);
	if(arrResult4!=null){
	alert("此批单证的数据有错误,请上报信息部人员进行处理!");
	return false;
	}
	return true;
}


function CheckAgent()
{
        var cAgentCode =  fm.all('AgentCode').value;
        var tSql202 = "select AgentCode from LAAgent where AgentCode='" + cAgentCode +"' and branchtype='2' and branchtype2='02' and managecom like '"+fm.all('ManageCom').value+"%' ";
        //alert("查询代理人是否为中介专员:"+tSql202);
        var arrResult202 = easyExecSql(tSql202);
        if(arrResult202 != null ){
            alert("不能修改为业管员,请下发到对应的代理机构！");
            return false;
        } 
          
          
        var tSql1 = "select AgentCom from LACom a where a.AgentCom='" + cAgentCode +"'";
        var arrResult = easyExecSql(tSql1);         
        if (arrResult != null )
        {
        
	        var tSql = "SELECT SUM(SUMCOUNT) FROM LZCard WHERE  StateFlag in ('0','7','8')" 
	                + " AND CertifyCode = '" + fm.all('CertifyCodeModify').value + "'"
	                + " AND ReceiveCom = 'E" + cAgentCode + "'";
	        var arrResult1 = easyExecSql(tSql);	       
	        if (arrResult1 == null){
	        	alert("查询代理机构现在持有张数错误！");
	        	return false;
	        }
	        sumSend = arrResult1[0][0];
	        
	        tSql = " select MaxCount from LDAgentCardCount where AgentCode='N' "
	        + " AND AgentGrade='N' AND ManageCom='N' AND AgentCom='Y'"
	        + " AND CertifyCode='" + fm.all('CertifyCodeModify').value + "'";	      
	        var arrResult2 = easyExecSql(tSql);	  
	        if (arrResult2 == null){
	        	alert("该代理机构最大发放张数未配置！");
	        	return false;
	        }
	        maxCount = arrResult2[0][0];	
	        norRate = 1 - sumSend / maxCount;	     
	       
	        if (norRate < 0.7){
	        	if(!confirm("该代理机构回销率未达到 70%,确定要调整吗?"))
	          {
	          	return false;	          	
	          }
	        }
	        	
        }
        return true;
}
