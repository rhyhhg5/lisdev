<% 
//清空缓存
//response.setHeader("Pragma","No-cache"); 
//response.setHeader("Cache-Control","no-cache"); 
//response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-07
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="SysCertSendOutInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="SysCertSendOutInit.jsp"%>
</head>
<body  onload="initForm()">
  <form action="./SysCertSendOutSave.jsp" method=post name=fm target="fraSubmit">
		<input class="cssButton" type="button" value="发放单证" onclick="submitForm()" >
    <!-- 单证编码 -->
    <table class="common">
      <tr class="common">
        <td class="title">单证编码</td>
        <td><input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('SysCertCode', [this,CertifyCodeName],[0,1]);" onkeyup="return showCodeListKey('SysCertCode', [this,CertifyCodeName],[0,1]);" verify="单证编码|NOTNULL"><Input class= codename name="CertifyCodeName" ></td>
      </tr>
    </table>
    
    <!-- 发放的信息 -->    
    <div style="width:120"><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSendOutInfo);"></td>
          <td class="titleImg">发放信息</td></tr></table></div>

    <div id="divSendOutInfo">
      <!-- 单证号码和经办日期 -->
      <table class="common"> 
        <tr class="common">
          <td class="title">发放机构</td>
          <td class="input"><input class="common" name="SendOutCom" verify="发放机构|NOTNULL"></td>

          <td class="title">接收机构</td>
          <td class="input"><input class="common" name="ReceiveCom" verify="接收机构|NOTNULL"></td></tr>

        <tr class="common">
          <td class="title">经办人</td>
          <td class="input"><input class="common" name="Handler"></td>

          <td class="title">经办日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDate"></td></tr>
        
      </table>
    </div>

		<input type=hidden name="hideOperation">    

    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    		<td class= titleImg>单证列表</td></tr>
    </table>

		<div id="divCertifyList">
      <table class="common">
        <tr class="common">
          <td text-align: left colSpan=1><span id="spanCertifyList"></span></td></tr>
	  	</table>
		</div>

	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

    <!-- 
      发放操作人员、发放操作日期、以及领用日期都是自动生成的，不需要用户输入 
    -->
  </form>
</body>
</html>
