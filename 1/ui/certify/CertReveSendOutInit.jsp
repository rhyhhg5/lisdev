<%
//程序名称：CertReveSendOutInit.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	String strOperator = globalInput.Operator;
	String strCurDate = PubFun.getCurrentDate();
	String strComCode = globalInput.ComCode;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
  	fm.reset();
    fm.Operator.value = '<%= strOperator %>';
    fm.Handler.value = '<%= strOperator %>';
    fm.HandleDate.value = '<%= strCurDate %>';
    fm.curTime.value = '<%= strCurDate %>';
    
    var comCode=fm.ManageCom.value;
    
    if (comCode.length==2||comCode.length==4)
    {
    	fm.SendOutCom.style.display="";
	  	fm.AgentCode.style.display="none";
	  	fm.AgentQuery.style.display="none";
	  	fm.ReceiveCom.value= comCode;
	  	fm.ReceiveName.value=getComName();
	  	
			fm.SendOutCom.value=='';

	  	fm.AgentCode.value="";
	  	if(comCode.length==4)
	  	{
	  		fm.ReceiveType.value="USR";
	  	}
    }
    else
    {
    	fm.SendOutCom.style.display="none";
	  	fm.SendOutName.style.display="none";
	  	fm.GroupAgentCode.style.display="";
	  	fm.QueryCom.style.display="";
	  	fm.ReceiveCom.value=fm.Operator.value;
	  	fm.ReceiveName.value=getUserName();
	  	//fm.ReceiveType.style.display="";
	  	fm.AgentQuery.style.display="";
	  	
	  	fm.SendOutCom.value="";
	  	fm.SendOutName.value="";
    }
  }
  catch(ex)
  {
    alert("在CertReveSendOutInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
    try
    {
        initInpBox();
        initCertifyList();
        initCertifyStoreList();
    }
    catch(re)
    {
        alert("CertReveSendOutInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

function getComName()
{
	var strSQL="select name from LDCom where comcode= '"+ fm.ManageCom.value +"' ";
	var comN = easyExecSql(strSQL);	
	return comN[0][0]; 
	
	//if (comN)
	//{	
	//	fm.all("ManageComName").value= comN[0][0];
	//}
}

function getUserName()
{
	var strSQL="select username from lduser where usercode='"+fm.Operator.value+"'";
	var userN = easyExecSql(strSQL);	
	return userN[0][0];
}

// 单证列表的初始化
function initCertifyList()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		  //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        				//列宽
      iArray[0][2]=50;          				//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证类型";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=2;              		//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="CertifyCodeReceive1";     //是否引用代码:null||""为不引用
      iArray[1][5]="1|2";             //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";             //上面的列中放置引用代码中第几位值
      iArray[1][9]="单证编码|code:CertifyCodeReceive1&NOTNULL";
      iArray[1][18]=300;
      iArray[1][19]=1;   		//1是需要强制刷新.
      iArray[1][21]="CertifyCode";

      iArray[2]=new Array();
      iArray[2][0]="单证名称";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="180";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="起始单号";    	    //列名
      iArray[3][1]="180";            		//列宽
      iArray[3][2]=180;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="起始单号|INT";

      iArray[4]=new Array();
      iArray[4][0]="终止单号";    	    //列名
      iArray[4][1]="180";            		//列宽
      iArray[4][2]=180;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][9]="终止单号|INT";

      iArray[5]=new Array();
      iArray[5][0]="数量";    	        //列名
      iArray[5][1]="50";            		//列宽
      iArray[5][2]=50;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][7]="countNum"  	  			//JS函数名，不加扩号 !!响应事件!!
      // 无号单证只有发放操作

      CertifyList = new MulLineEnter( "fm" , "CertifyList" ); 
      //这些属性必须在loadMulLine前
      CertifyList.displayTitle = 1;
      CertifyList.mulLineCount = 1;   
      CertifyList.hiddenPlus = 1;        
      CertifyList.hiddenSubtraction = 1; 
      CertifyList.loadMulLine(iArray);  
      
    } catch(ex) {
      alert(ex);
    }
}

// 起始单号、终止单号信息列表的初始化
function initCertifyStoreList()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="40";        			//列宽
        iArray[0][2]=40;          			//列最大值
        iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="单证类型";     		//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[1][1]="80";        			//列宽
        iArray[1][2]=80;          			//列最大值
        iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许
        iArray[1][21]="CertifyCode";

        iArray[2]=new Array();
        iArray[2][0]="起始单号";     		//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[2][1]="180";        		  //列宽
        iArray[2][2]=180;          			//列最大值
        iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

        iArray[3]=new Array();
        iArray[3][0]="终止单号";    	    //列名
        iArray[3][1]="180";            		//列宽
        iArray[3][2]=180;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[4]=new Array();
        iArray[4][0]="数量";    	    //列名
        iArray[4][1]="50";            		//列宽
        iArray[4][2]=180;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[5]=new Array();
        iArray[5][0]="单证持有者";    	        //列名
        iArray[5][1]="100";            		//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        CertifyStoreList = new MulLineEnter("fm", "CertifyStoreList");
        //这些属性必须在loadMulLine前
        CertifyStoreList.displayTitle = 1;
        CertifyStoreList.mulLineCount = 0;
        CertifyStoreList.hiddenPlus = 1;
        CertifyStoreList.hiddenSubtraction = 1;

        CertifyStoreList.loadMulLine(iArray);

    } catch(ex) {
        alert(ex);
    }
}
</script>