var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{  
	// 初始化表格
	initCertifyMaxGrid();
	
	var owner = "";
	if(companyCode.length = 4)
		owner = "and owner in (select usercode from lzcertifyuser where 'B'||usercode in "
					+ "(select receivecom from lzaccess where sendoutcom = 'A'||'" + companyCode + "')) ";
	var strSQL = "select sum(ReceiveNumber), sum(CallBackNumber), sum(SendOutNumber), sum(StockpileNumber), "
					+ "(select sum(certifyprice*sumcount)/sum(sumcount) from LZCardPrint where certifycode = j.CertifyCode), "
					+ "sum(NaturalNumber), sum(BlankOutNumber), sum(MarNumber), sum(LostNumber), sum(RetourNumber) "
					+ "from LZCardJourUsed j "
					+ "where jourdate = '2007-11-8' "
					+ owner
					+ "group by CertifyCode "
			;
	
	//fm.querySQL.value = strSQL;
	//turnPage.queryModal(strSQL, CertifyMaxGrid); 
	
}



//更新数据的函数
function printInfo()
{
  if(fm.startDate.value == null || fm.startDate.value == ""){
  	alert("请输入开始日期!");
  	return ;
  }
  if(fm.endDate.value == null || fm.endDate.value == ""){
     alert("请输入结束日期！");
     return false;
  }
  
	var owner = "";
	var companyCode = fm.companyCode.value;
	if(fm.include.value == 0)
	{
		owner = " and j.owner = '" + companyCode + "' ";
	}
	else if(fm.include.value == 1)
	{
		if(companyCode == '86')
		{
			
		}
		else if(companyCode.length == 4 && companyCode.substring(0, 2) == '86')
		{
			owner = " and j.owner in (select usercode from lzcertifyuser where 'B'||usercode in "
						+ "(select receivecom from lzaccess where sendoutcom = 'A'||'" + companyCode + "')) ";
		}
		else
		{
			owner = " and j.owner = '" + companyCode + "' ";
		}
	}
	var beginDateSQL = "select jourdate + 1 day from LZCardJourUsed j where 1 = 1 " + getWherePart('jourdate','startDate','<') + owner + "order by jourdate desc fetch first 1 rows only";
	var endDateSQL = "select jourdate from LZCardJourUsed j where 1 = 1 " + getWherePart('jourdate','endDate','<') + owner + "order by jourdate desc fetch first 1 rows only";
	var beginDate = easyExecSql(beginDateSQL);
	if(beginDate == null || beginDate == '')    beginDate = fm.startDate.value;
	var endDate = easyExecSql(endDateSQL);
	if(endDate == null || endDate == '')    endDate = fm.endDate.value;
	//alert(beginDate + "****" + endDate);return;
	
	var strSQL = "select c.certifycode, (select d.certifyname from lmcertifydes d where d.certifycode = c.certifycode), "
						+ "(select int(sum(StockpileNumber)) from LZCardJourUsed j where jourdate + 1 day = '" + beginDate + "' and j.certifycode = c.certifycode " + owner + "), "
						+ "(select int(sum(UnemployedNumber)) from LZCardJourUsed j where jourdate  + 1 day = '" + beginDate + "' and j.certifycode = c.certifycode " + owner + "), "
						+ "(select sum(ReceiveNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + " and jourdate + 1 day>'2007-09-01' " + owner + "), "
						+ "(select sum(CallBackNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + "), "
						+ "(select sum(SendOutNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + "), "
						+ "(select double(sum(StockpileNumber)) from LZCardJourUsed j where j.certifycode = c.certifycode and jourdate = '" + endDate + "' " + owner + " and j.certifycode = c.certifycode), "
						+ "(select double(sum(certifyprice*sumcount)/sum(sumcount)) from LZCardPrint p where c.certifycode = p.CertifyCode), "
						+ "int((select sum(StockpileNumber) from LZCardJourUsed j where j.certifycode = c.certifycode and jourdate = '" + endDate + "' " + owner + " and j.certifycode = c.certifycode)*(select sum(certifyprice*sumcount)/sum(sumcount) from LZCardPrint p where c.certifycode = p.CertifyCode)), "
						+ "(select sum(NaturalNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + "), "
						+ "(select sum(BlankOutNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + "),"
						+ "(select sum(MarNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + "), "
						+ "(select sum(LostNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + "), "
						+ "(select sum(RetourNumber) from LZCardJourUsed j where j.certifycode = c.certifycode " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + "), "
						+ "(select int(sum(UnemployedNumber)) from LZCardJourUsed j where j.certifycode = c.certifycode and jourdate = '" + endDate + "' " + owner + " and j.certifycode = c.certifycode) "
						+ "from (select distinct certifycode from LZCardJourUsed j where  1 = 1 " + getWherePart('jourdate + 1 day','startDate', '>') + getWherePart('jourdate - 1 day','endDate', '<') + owner + ") as c"
						;

	
	fm.querySQL.value = strSQL;
	
  fm.action = "CardStatisticUsedPrint.jsp";
	fm.target = "_blank";//弹出新窗口
	fm.submit();
}

