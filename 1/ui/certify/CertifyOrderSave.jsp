<%
//程序名称：CertifyPrintInput.jsp
//程序功能：
//创建日期：2002-10-14 10:20:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.Hashtable"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%!
		String handleFunction(HttpSession session, HttpServletRequest request) {
	  LZCardPrintSchema schemaLZCardPrint = new LZCardPrintSchema();
System.out.println("================================================================================================");	
	  //输出参数
	  CErrors tError = null;
	  String strOperate = request.getParameter("hideOperate").trim();
	  
		GlobalInput globalInput = new GlobalInput();
		
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	
		schemaLZCardPrint.setPrtNo(request.getParameter("PrtNo"));
		schemaLZCardPrint.setCertifyCode(request.getParameter("CertifyCode"));
		schemaLZCardPrint.setRiskCode(request.getParameter("RiskCode"));
		schemaLZCardPrint.setRiskVersion(request.getParameter("RiskVersion"));
		schemaLZCardPrint.setSubCode(request.getParameter("SubCode"));
		schemaLZCardPrint.setMaxMoney("1");
		schemaLZCardPrint.setMaxDate(request.getParameter("MaxDate"));
		schemaLZCardPrint.setComCode(request.getParameter("ComCode"));
		schemaLZCardPrint.setPhone(request.getParameter("Phone"));
		schemaLZCardPrint.setLinkMan(request.getParameter("LinkMan"));
		schemaLZCardPrint.setCertifyPrice(request.getParameter("CertifyPrice"));
		schemaLZCardPrint.setManageCom(request.getParameter("ManageCom"));
		schemaLZCardPrint.setOperatorInput(request.getParameter("OperatorInput"));
		schemaLZCardPrint.setInputDate(request.getParameter("InputDate"));
		schemaLZCardPrint.setInputMakeDate(request.getParameter("InputMakeDate"));
		schemaLZCardPrint.setGetMan(request.getParameter("GetMan"));
		schemaLZCardPrint.setGetDate(request.getParameter("GetDate"));
		schemaLZCardPrint.setOperatorGet(request.getParameter("OperatorGet"));
		schemaLZCardPrint.setStartNo(request.getParameter("StartNo"));
		schemaLZCardPrint.setEndNo(request.getParameter("EndNo"));
		schemaLZCardPrint.setGetMakeDate(request.getParameter("GetMakeDate"));
		schemaLZCardPrint.setSumCount(request.getParameter("SumCount"));
		schemaLZCardPrint.setState(request.getParameter("State"));

		String strCertifyClass = request.getParameter("CertifyClass");
			
	  // 准备传输数据 VData
	  VData vData = new VData();

		vData.addElement(schemaLZCardPrint);
		vData.add(globalInput);
		
		Hashtable hashParams = new Hashtable();
		
		hashParams.put("CertifyClass", strCertifyClass);
		
		vData.add( hashParams );
	
		CardPrintUI tCardPrintUI = new CardPrintUI();
	  try {
	    if ( !tCardPrintUI.submitData(vData, strOperate) ) {
		   	if( tCardPrintUI.mErrors.needDealError() ) {
		   		return tCardPrintUI.mErrors.getFirstError();
			  } else {
			  	return "保存失败，但是没有详细的原因";
			  }
			}
	    
	  } catch(Exception ex) {
	  	ex.printStackTrace();
			return "保存失败，原因是:" + ex.toString();	  	
	  }
		VData vData1 = new VData();
	  vData1 = tCardPrintUI.getResult();
		schemaLZCardPrint = null;
		schemaLZCardPrint = (LZCardPrintSchema)vData1.getObjectByObjectName("LZCardPrintSchema", 0);
		String prtNo = schemaLZCardPrint.getPrtNo();	
	  String certifyCode = schemaLZCardPrint.getCertifyCode();	
	  String startNo = schemaLZCardPrint.getStartNo();
		String endNo = schemaLZCardPrint.getEndNo();
    
    vData.clear();
    
    schemaLZCardPrint.setPrtNo(prtNo);
    schemaLZCardPrint.setCertifyCode(request.getParameter("CertifyCode"));
		schemaLZCardPrint.setRiskCode(request.getParameter("RiskCode"));
		schemaLZCardPrint.setRiskVersion(request.getParameter("RiskVersion"));
		schemaLZCardPrint.setSubCode(request.getParameter("SubCode"));
		schemaLZCardPrint.setMaxMoney("1");
		schemaLZCardPrint.setMaxDate(request.getParameter("MaxDate"));
		schemaLZCardPrint.setComCode(request.getParameter("ComCode"));
		schemaLZCardPrint.setPhone(request.getParameter("Phone"));
		schemaLZCardPrint.setLinkMan(request.getParameter("LinkMan"));
		schemaLZCardPrint.setCertifyPrice(request.getParameter("CertifyPrice"));
		schemaLZCardPrint.setManageCom(request.getParameter("ManageCom"));
		schemaLZCardPrint.setOperatorInput(request.getParameter("OperatorInput"));
		schemaLZCardPrint.setInputDate(request.getParameter("InputDate"));
		schemaLZCardPrint.setInputMakeDate(request.getParameter("InputMakeDate"));
		schemaLZCardPrint.setGetMan(request.getParameter("GetMan"));
		schemaLZCardPrint.setGetDate(request.getParameter("GetDate"));
		schemaLZCardPrint.setOperatorGet(request.getParameter("OperatorGet"));
		schemaLZCardPrint.setStartNo(startNo);
		schemaLZCardPrint.setEndNo(endNo);
		schemaLZCardPrint.setGetMakeDate(request.getParameter("GetMakeDate"));
		schemaLZCardPrint.setSumCount(request.getParameter("SumCount"));
System.out.println("sumcount: "+request.getParameter("SumCount"));		
		schemaLZCardPrint.setState(request.getParameter("State"));
		
    vData.addElement(schemaLZCardPrint);
		vData.add(globalInput);
	  
	  strOperate="INSERT||CONFIRM";
	  
	  tCardPrintUI = null;
	  try 
	  {
	  	tCardPrintUI = new CardPrintUI();
	 	  
	    if ( !tCardPrintUI.submitData(vData, strOperate) ) 
	    {
	    	ExeSQL tExeSQL = new ExeSQL();
		  	String strSql="delete from LZCardPrint where certifycode='"+schemaLZCardPrint.getCertifyCode()
		  		+"' and startno='"+schemaLZCardPrint.getStartNo()+"' and endno='"+schemaLZCardPrint.getEndNo()+"'"
		  		;
		  	if (!tExeSQL.execUpdateSQL(strSql))
	      {
	        return " 保存失败，原因是:提单操作失败,且不能消除定单数据！";
	      }
	    	
		   	if( tCardPrintUI.mErrors.needDealError() ) 
		   	{
		   		return tCardPrintUI.mErrors.getFirstError();
			  } else 
			  {
			  	return "保存失败，但是没有详细的原因";
			  }
			}
	  } catch(Exception ex) {
	  	ex.printStackTrace();
			return "保存失败，原因是:" + ex.toString();	  	
	  }
		/*-----------------入库---------------*/	  
		
		LZCardSet setLZCard = new LZCardSet( );
		LZCardPrintSet tsetLZCardPrint = new LZCardPrintSet( );
				
		try
		{
			// 加入打印表信息
			tsetLZCardPrint.add(schemaLZCardPrint);

  		LZCardSchema schemaLZCard = new LZCardSchema( );

	    schemaLZCard.setCertifyCode(certifyCode);
			schemaLZCard.setSubCode("0");
			schemaLZCard.setRiskCode("0");
			schemaLZCard.setRiskVersion("0");

	    schemaLZCard.setSendOutCom("00");
	    schemaLZCard.setReceiveCom("A"+globalInput.ComCode);

			schemaLZCard.setSumCount(0);
			schemaLZCard.setPrem("");
	    schemaLZCard.setAmnt("");
	    schemaLZCard.setHandler(globalInput.Operator);
	    schemaLZCard.setHandleDate(request.getParameter("GetDate"));
	    schemaLZCard.setInvaliDate(request.getParameter("GetDate"));

			schemaLZCard.setTakeBackNo("");
			schemaLZCard.setSaleChnl("");
			schemaLZCard.setStateFlag("0");
			schemaLZCard.setOperateFlag("0");
			schemaLZCard.setPayFlag("");
			schemaLZCard.setEnterAccFlag("");
			schemaLZCard.setReason("");
			schemaLZCard.setState("");
			schemaLZCard.setOperator("");
			schemaLZCard.setMakeDate("");
			schemaLZCard.setMakeTime("");
			schemaLZCard.setModifyDate("");
			schemaLZCard.setModifyTime("");

			// 加入单证信息
	    setLZCard.add(schemaLZCard);
		
		  String szLimitFlag = request.getParameter("LimitFlag");
		  vData.clear();
		  vData.addElement(globalInput);
		  vData.addElement(setLZCard);
		  vData.addElement(tsetLZCardPrint);
		  vData.addElement(szLimitFlag);
		  
		  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
		  vData.addElement(hashParams);
	
			// 设置操作字符串
			String szOperator = "INSERT";
			
			// 数据传输
		  CertSendOutUI tCertSendOutUI = new CertSendOutUI();
			
		  if (!tCertSendOutUI.submitData(vData, szOperator)) 
		  {
		  	ExeSQL tExeSQL = new ExeSQL();
		  	String strSql="delete from LZCardPrint where certifycode='"+schemaLZCardPrint.getCertifyCode()
		  		+"' and startno='"+schemaLZCardPrint.getStartNo()+"' and endno='"+schemaLZCardPrint.getEndNo()+"'"
		  		;
		  	if (!tExeSQL.execUpdateSQL(strSql))
	      {
	        return " 保存失败，原因是: 入库操作失败,且不能消除定单数据！";
	      }
		  	
		    return " 保存失败，原因是: 入库操作失败！";
		  }
	  	
	  	String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0); 
			System.out.println("abc: "+strTakeBackNo);	  
	  	
	  }
		catch(Exception ex) 
		{
	  	ex.printStackTrace();
			return "保存失败，原因是:" + ex.toString();	  	
	  }
	  
	  return "";
	}
%>

<%
	String FlagStr = "";
	String Content = "";
	
	try {
		Content = handleFunction(session, request);
		
		if( Content.equals("") ) {
			FlagStr = "Succ";
			Content = "操作成功";
		} else {
			FlagStr = "Fail";
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	}
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

