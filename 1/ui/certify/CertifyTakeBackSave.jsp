<%
//程序名称：CertifyTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;
  String szFailSet = "";

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );

  //校验处理
  //内容待填充
	try {
	  // 单证信息部分
	  String szPrtNo				= request.getParameter("PrtNo");
	  String szSendOutCom 	= request.getParameter("SendOutCom");
	  String szReceiveCom		="" ;
	  ExeSQL tExeSQL=new ExeSQL();
	  String tagentcode=tExeSQL.getOneValue(" select agentcode from laagent where groupagentcode='"+request.getParameter("ReceiveCom")+"' ");
	  if(!"".equals(tagentcode) && tagentcode!=null){
		  szReceiveCom="D"+tagentcode;
	  }else{
		  szReceiveCom=request.getParameter("ReceiveCom");
	  }
	  String szInvaliDate 	= request.getParameter("InvalidDate");
	  String szAmnt 				= request.getParameter("Amnt");
	  String szHandler			= request.getParameter("Handler");
	  String szHandleDate		= request.getParameter("HandleDate");
	  
	  String szNo[]					= request.getParameterValues("CertifyListNo");
	  String szCertifyCode[]= request.getParameterValues("CertifyList1");
	  String szStartNo[]		= request.getParameterValues("CertifyList3");
	  String szEndNo[]			= request.getParameterValues("CertifyList4");
	  String szSumCount[]   = request.getParameterValues("CertifyList5");
	  String szStateFlag[]	= request.getParameterValues("CertifyList6");
	  
	  int nIndex;
	  
	  LZCardSet setLZCard = new LZCardSet( );
	  
	  if( szNo == null ) {
	  	throw new Exception("没有输入要回收的起始单证号和终止单证号");
	  }

	  for( nIndex = 0; nIndex < szNo.length; nIndex ++ ) {

	    if( szStateFlag == null || szStateFlag[nIndex] == null || szStateFlag[nIndex].equals("") ) {
	    	throw new Exception("没有输入单证回收状态");
	    }
	    
	    LZCardSchema schemaLZCard = new LZCardSchema( );
	    
	    schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
			schemaLZCard.setSubCode("");
			schemaLZCard.setRiskCode("");
			schemaLZCard.setRiskVersion("");

	    schemaLZCard.setStartNo(szStartNo[nIndex]);
	    schemaLZCard.setEndNo(szEndNo[nIndex]);
			schemaLZCard.setStateFlag(szStateFlag[nIndex]);
	    
	    schemaLZCard.setSendOutCom(szSendOutCom);
	    schemaLZCard.setReceiveCom(szReceiveCom);
	    
			schemaLZCard.setSumCount(szSumCount[nIndex]);
			schemaLZCard.setPrem("");
	    schemaLZCard.setAmnt(szAmnt);
	    schemaLZCard.setHandler(szHandler);
	    schemaLZCard.setHandleDate(szHandleDate);
	    schemaLZCard.setInvaliDate(szInvaliDate);

			schemaLZCard.setTakeBackNo("");
			schemaLZCard.setSaleChnl("");
			schemaLZCard.setOperateFlag("");
			schemaLZCard.setPayFlag("");
			schemaLZCard.setEnterAccFlag("");
			schemaLZCard.setReason("");
			schemaLZCard.setState("");
			schemaLZCard.setOperator("");
			schemaLZCard.setMakeDate("");
			schemaLZCard.setMakeTime("");
			schemaLZCard.setModifyDate("");
			schemaLZCard.setModifyTime("");
				    
	    setLZCard.add(schemaLZCard);
	  }
		
	  // 准备传输数据 VData
	  VData vData = new VData();
	
	  vData.addElement(globalInput);
	  vData.addElement(setLZCard);
	  
	  Hashtable hashParams = new Hashtable();
	  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
	  vData.addElement(hashParams);

	  // 数据传输
	  CertTakeBackUI tCertTakeBackUI = new CertTakeBackUI();

	  if (!tCertTakeBackUI.submitData(vData, "INSERT")) {
	    Content = " 保存失败，原因是: " + tCertTakeBackUI.mErrors.getFirstError();
	    FlagStr = "Fail";
	    
	    vData = tCertTakeBackUI.getResult();
	    setLZCard = (LZCardSet)vData.getObjectByObjectName("LZCardSet", 0);
	    
			szFailSet = "parent.fraInterface.CertifyList.clearData();\r\n";
	    for(nIndex = 0; nIndex < setLZCard.size(); nIndex ++) {
	    	LZCardSchema tLZCardSchema = setLZCard.get(nIndex + 1);
	    
	    	szFailSet += "parent.fraInterface.CertifyList.addOne();\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 1, '" + tLZCardSchema.getCertifyCode() + "');\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 3, '" + tLZCardSchema.getStartNo() + "');\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 4, '" + tLZCardSchema.getEndNo() + "');\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 5, '" + tLZCardSchema.getSumCount() + "');\r\n";
	    	szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
	    	  ", 6, '" + tLZCardSchema.getStateFlag() + "');\r\n";
	    }

	  } else {
	  	Content = " 保存成功 ";
	  	FlagStr = "Succ";
	  	
	  	vData = tCertTakeBackUI.getResult();
	  	String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
	  	session.setAttribute("TakeBackNo", strTakeBackNo);
 			session.setAttribute("State", CertStatBL.PRT_STATE);
	  }
	} catch(Exception ex) {
		ex.printStackTrace( );
   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   	FlagStr = "Fail";
	}
%>
<html>
  <script language="javascript">
  	<%= szFailSet %>
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
<body>
</body>
</html>

