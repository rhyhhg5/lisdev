<%
//程序名称：CertBatchSendOutSave.jsp
//程序功能：
//创建日期：2002-09-23
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  String strTakeBackNo = "";
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  //校验处理
  //内容待填充
	try {
	  // 单证信息部分
	  String szPrtNo				= request.getParameter("PrtNo");
	  String szSendOutCom 	= request.getParameter("SendOutCom");
	  String szReceiveCom		= request.getParameter("ReceiveCom");
	  String szInvaliDate 	= request.getParameter("InvalidDate");
	  String szAmnt 				= request.getParameter("Amnt");
	  String szHandler			= request.getParameter("Handler");
	  String szHandleDate		= request.getParameter("HandleDate");
	  
	  String szLimitFlag    = request.getParameter("LimitFlag");
	  
	  String szNo[]					= request.getParameterValues("CertifyListNo");
	  String szCertifyCode[]= request.getParameterValues("CertifyList1");
	  String szStartNo[]		= request.getParameterValues("CertifyList3");
	  String szEndNo[]			= request.getParameterValues("CertifyList4");
	  String szSumCount[]   = request.getParameterValues("CertifyList5");
	  int nIndex = 0;
	  
	  LZCardSet setLZCard = new LZCardSet( );
		LZCardPrintSet setLZCardPrint = new LZCardPrintSet( );
	  
    if( szSendOutCom.equals("00") ) {  // 如果是单证入库操作
    	if( szPrtNo == null || szPrtNo.equals("") ) {
    		throw new Exception("没有输入单证印刷号");
    	}
    	
			LZCardPrintSchema schemaLZCardPrint = new LZCardPrintSchema( );
			
			schemaLZCardPrint.setPrtNo(szPrtNo);
			schemaLZCardPrint.setCertifyCode(szCertifyCode[0]);
	
			// 加入打印表信息
			setLZCardPrint.add(schemaLZCardPrint);

  		LZCardSchema schemaLZCard = new LZCardSchema( );
	    
	    schemaLZCard.setCertifyCode(szCertifyCode[0]);
			schemaLZCard.setSubCode("");
			schemaLZCard.setRiskCode("");
			schemaLZCard.setRiskVersion("");

	    schemaLZCard.setSendOutCom(szSendOutCom);
	    schemaLZCard.setReceiveCom(szReceiveCom);
	    
			schemaLZCard.setSumCount(0);
			schemaLZCard.setPrem("");
	    schemaLZCard.setAmnt(szAmnt);
	    schemaLZCard.setHandler(szHandler);
	    schemaLZCard.setHandleDate(szHandleDate);
	    schemaLZCard.setInvaliDate(szInvaliDate);

			schemaLZCard.setTakeBackNo("");
			schemaLZCard.setSaleChnl("");
			schemaLZCard.setStateFlag("");
			schemaLZCard.setOperateFlag("");
			schemaLZCard.setPayFlag("");
			schemaLZCard.setEnterAccFlag("");
			schemaLZCard.setReason("");
			schemaLZCard.setState("");
			schemaLZCard.setOperator("");
			schemaLZCard.setMakeDate("");
			schemaLZCard.setMakeTime("");
			schemaLZCard.setModifyDate("");
			schemaLZCard.setModifyTime("");
				    
			// 加入单证信息
	    setLZCard.add(schemaLZCard);

    } else {  // 如果是单证发放操作
    
		  for( nIndex = 0; nIndex < szNo.length; nIndex ++ ) {

    		LZCardSchema schemaLZCard = new LZCardSchema( );
		    
		    schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
				schemaLZCard.setSubCode("");
				schemaLZCard.setRiskCode("");
				schemaLZCard.setRiskVersion("");
	
		    schemaLZCard.setStartNo(szStartNo[nIndex]);
		    schemaLZCard.setEndNo(szEndNo[nIndex]);
		    
		    schemaLZCard.setSendOutCom(szSendOutCom);
		    schemaLZCard.setReceiveCom(szReceiveCom);
		    
				schemaLZCard.setSumCount(szSumCount[nIndex]);
				schemaLZCard.setPrem("");
		    schemaLZCard.setAmnt(szAmnt);
		    schemaLZCard.setHandler(szHandler);
		    schemaLZCard.setHandleDate(szHandleDate);
		    schemaLZCard.setInvaliDate(szInvaliDate);
	
				schemaLZCard.setTakeBackNo("");
				schemaLZCard.setSaleChnl("");
				schemaLZCard.setStateFlag("");
				schemaLZCard.setOperateFlag("");
				schemaLZCard.setPayFlag("");
				schemaLZCard.setEnterAccFlag("");
				schemaLZCard.setReason("");
				schemaLZCard.setState("");
				schemaLZCard.setOperator("");
				schemaLZCard.setMakeDate("");
				schemaLZCard.setMakeTime("");
				schemaLZCard.setModifyDate("");
				schemaLZCard.setModifyTime("");
					    
		    setLZCard.add(schemaLZCard);
		  }
    }

	  // 准备传输数据 VData
	  VData vData = new VData();
	
	  vData.addElement(globalInput);
	  vData.addElement(setLZCard);
	  vData.addElement(setLZCardPrint);
	  vData.addElement(szLimitFlag);
	  
	  Hashtable hashParams = new Hashtable();
	  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
	  vData.addElement(hashParams);
	  
	  // 数据传输
	  CertSendOutUI tCertSendOutUI = new CertSendOutUI();

	  if (!tCertSendOutUI.submitData(vData, "INSERT")) {
	    Content = " 保存失败，原因是: " + tCertSendOutUI.mErrors.getFirstError();
	    FlagStr = "Fail";
	  } else {
	  	Content = " 保存成功 ";
	  	FlagStr = "Succ";

		  vData = tCertSendOutUI.getResult();
			strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);

			session.setAttribute("TakeBackNo", strTakeBackNo);
			session.setAttribute("State", CertStatBL.PRT_STATE);
			session.setAttribute("NoUseAgentTemplate", "YES");
	  }
	  
	} catch(Exception ex) {
		ex.printStackTrace( );
   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   	FlagStr = "Fail";
	}
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%= strTakeBackNo %>");
	</script>
<body>
</body>
</html>

