var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{  
		
}



//更新数据的函数
function printInfo()
{
	if(!verifyInput()) return ;
	if(!checkStartDate()) return ;
  if(!composeSQL()) return ;
  
//  var log = log4javascript.getDefaultLogger();
//  log.trace("trace");
//  log.debug("debug");
//  log.info("info");
//  log.warn("warn");
//  log.error("error");
//  log.fatal("fatal");
	
//	alert(fm.querySql.value); return false;
  fm.action = "CardStatisticQueryPrint.jsp";
	fm.target = "_blank";//弹出新窗口
	fm.submit();
}

function checkStartDate()
{
	if((fm.sendStartDate.value == null || fm.sendStartDate.value == '') && (fm.backStartDate.value == null || fm.backStartDate.value == ''))
	{
		alert("领用日期和回销日期必须选择一个！");
		return ;
	}
	if((fm.sendStartDate.value != null && fm.sendStartDate.value != '') && (fm.backStartDate.value != null && fm.backStartDate.value != ''))
	{
		alert("领用日期和回销日期只能选择一个！");
		return ;
	}
	return true;
}

function composeSQL()
{
	var sendOutComString = "";//发放机构
	if(fm.sendOut.value == null || fm.sendOut.value == ''){
		if(fm.companyCode.value == '86')
		{
			if(fm.include.value == 0)
				sendOutComString = " and sendoutcom = 'A" + fm.companyCode.value + "'";
		}
		else if(fm.companyCode.value.substring(0, 2) == '86')
		{
			if(fm.include.value == 1)
				sendOutComString = " and (sendoutcom = 'A" + fm.companyCode.value 
				+ "' or c.operator in (select substr(receivecom, 2) from lzaccess where sendoutcom = 'A" + fm.companyCode.value + "'))";
			else
				sendOutComString = " and sendoutcom = 'A" + fm.companyCode.value + "'";
		}
		else
			sendOutComString = " and operator = '" + fm.companyCode.value + "'";
	}
	else{
		if(fm.sendOut.value.substring(0, 2) == '86')
			sendOutComString = " and sendoutcom = 'A" + fm.sendOut.value + "'";
		else
			sendOutComString = " and sendoutcom = 'B" + fm.sendOut.value + "'";
	}
	var receiveComString = "";//接受机构
	if(fm.reveive.value != null && fm.reveive.value != ''){
		if(fm.reveive.value.substring(0, 2) == '86')
			receiveComString = " and receivecom = 'A" + fm.reveive.value + "'";
		else
			receiveComString = " and receivecom = 'B" + fm.reveive.value + "'";
	}
	var takeBackComString = "";//回销机构
	if(fm.takeBack.value != null && fm.takeBack.value != ''){
		if(fm.takeBack.value.substring(0, 2) == '86')
			takeBackComString = " and receivecom = 'A" + fm.takeBack.value + "'";
		else
			takeBackComString = " and receivecom = 'B" + fm.takeBack.value + "'";
	}
	var state = fm.stateType.value;
	var stateString = "";
	var certifyClass = "";
	if(fm.cardType.value == 'D')
	{
		certifyClass = " and d.CertifyClass = 'D' ";
		if(state == 11) stateString = " and c.state in ('0', '3', '8', '9') ";//未领用
		else if(state == 12) stateString = " and c.state in ('10', '11') ";//已领用
		else if(state == 13) stateString = " and c.state in ('2', '12') ";//已录入
		else if(state != null && state != '') stateString = " and c.state = '" + state + "' ";//2-正常回销；3-空白回销；4-遗失；5-损毁；6-作废
	}
	else if(fm.cardType.value == 'P')
	{
		certifyClass = " and d.CertifyClass = 'P' ";
		if(state == 11) stateString = " and c.stateflag = '0' ";//未领用
		else if(state == 12) stateString = " and c.stateflag = '0' ";//已领用
		else if(state == 13) stateString = " and c.stateflag in ('1', '5') ";//已录入
		else if(state == 2) stateString = " and c.stateflag = '1' ";//正常回销
		else if(state == 3) {alert("普通单证没有空白回销！"); return false;}//空白回销
		else if(state == 4) stateString = " and c.stateflag = '3' ";//遗失
		else if(state == 5) stateString = " and c.stateflag = '4' ";//损毁
		else if(state == 6) stateString = " and c.stateflag = '2' ";//作废
	}
	else
	{
		if(state != null && state != '')
		{
			alert("请先选择单证类型");
			return false;
		}
	}
	
//	var strSQL = "select c.certifycode, (select d.certifyname from lmcertifydes d where d.certifycode = c.certifycode), "
//						+ "t.modifydate, t.modifydate, "
//						+ "case when t.sendoutcom like '86%' then t.sendoutcom else (select a.username from LZCertifyUser a where a.usercode = substr(t.sendoutcom, 2)) end, "
//						+ "case when t.receivecom like '86%' then t.receivecom else (select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) end, "
//						+ "case when t.receivecom like '86%' then t.receivecom else (select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) end, "
//						+ "c.state ,c.startno, c.endno, c.sumcount "
//						+ "from lzcard c, (select * from lzcardtrack where 1 = 1" + sendOutComString + receiveComString
//						+ getWherePart('CertifyCode','certifyCode')
//						+ getWherePart('StartNo','startNo', '>=')
//						+ getWherePart('EndNo','endNo', '<=')
//						+ getWherePart('ModifyDate','sendStartDate', '>=')
//						+ getWherePart('ModifyDate','sendEndDate', '<=')
//						+ ") as t "
//						+ "where c.certifycode = t.certifycode "
//						+ "and c.startno >= t.startno and c.endno <= t.endno "
//						+ getWherePart('c.State','stateType')
//						+ "";

	var strSQL = "";
	
	if(fm.contNo.value != null && fm.contNo.value != '')
	{
		var sql = "select * from LCCont where 1 = 1 " + getWherePart('ContNo','contNo');
		var num = easyExecSql(sql);
		var table = "LCCont";
		
		if(num == null || num < 1)
		{
			alert("没有查询结果");
			return false;
		}
		else if(num > 1)
		{
			table = "LCGrpCont";
		}
		
		strSQL = "select A, B, C, D, F, G, H, I, J, K, L, N, O, P, Q, R FROM "
						+ "(select c.certifycode A, (select d.certifyname from lmcertifydes d where d.certifycode = c.certifycode) B, "
						+ "'" + fm.sendStartDate.value + "' C,'" + fm.sendEndDate.value + "' D, n.cardno E, "
						+ "case when c.sendoutcom like 'A86%' then (select name from LDCom where ComCode = substr(c.sendoutcom, 2)) else (select a.username from LZCertifyUser a where a.usercode = substr(c.sendoutcom, 2)) end F, "
						+ "case when c.receivecom like 'A86%' then (select name from LDCom where ComCode = substr(c.receivecom, 2)) else (select a.name from laagent a where a.agentcode = substr(c.receivecom, 2)) end G, "
						+ "case when c.receivecom like 'A86%' then (select name from LDCom where ComCode = substr(c.receivecom, 2)) else (select a.name from laagent a where a.agentcode = substr(c.receivecom, 2)) end H, "
						+ "c.state I,c.startno J, c.endno K, c.sumcount L, "
						+ "t.PrtNo M, t.ContNo N, t.InsuredName O, t.AppntName P, t.AgentCode Q, t.Prem R "
						+ "from lzcard c, lmcertifydes d, lzcardnumber n, " + table + " t "
						+ "where c.certifycode = d.certifycode and c.subcode = n.cardtype "
						+ "and n.CardNo = t.PrtNo and t.ContType = 1 "
						+ sendOutComString + receiveComString
						+ getWherePart('c.CertifyCode','certifyCode')
						+ getWherePart('c.StartNo','startNo', '>=')
						+ getWherePart('c.EndNo','endNo', '<=')
						+ getWherePart('c.ModifyDate','sendStartDate', '>=')
						+ getWherePart('c.ModifyDate','sendEndDate', '<=')
						+ certifyClass + stateString
						+ ")";
	}
	else
	{
		var sql = "select count(1) from lzcard c, lmcertifydes d "
						+ "where c.subcode = d.subcode "
						+ sendOutComString + receiveComString
						+ getWherePart('c.CertifyCode','certifyCode')
						+ getWherePart('c.StartNo','startNo', '>=')
						+ getWherePart('c.EndNo','endNo', '<=')
						+ getWherePart('c.ModifyDate','sendStartDate', '>=')
						+ getWherePart('c.ModifyDate','sendEndDate', '<=')
						+ certifyClass + stateString;
		var num = easyExecSql(sql);
		if(num == null || num < 1)
		{
			alert("没有查询结果");
			return false;
		}
		else if(num > 200)
		{
			alert("查询条数大于500，请缩小查询范围！");
			return false;
		}
		
	//	var strSQL = "select A, B, C, D, F, G, H, I, J, K, L, N, O, P, Q, R FROM "
	//						+ "(select c.certifycode A, (select d.certifyname from lmcertifydes d where d.certifycode = c.certifycode) B, "
	//						+ "t.modifydate C, t.modifydate D, n.cardno E, "
	//						+ "case when t.sendoutcom like '86%' then t.sendoutcom else (select a.username from LZCertifyUser a where a.usercode = substr(t.sendoutcom, 2)) end F, "
	//						+ "case when t.receivecom like '86%' then t.receivecom else (select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) end G, "
	//						+ "case when t.receivecom like '86%' then t.receivecom else (select a.name from laagent a where a.agentcode = substr(t.receivecom, 2)) end H, "
	//						+ "c.state I,c.startno J, c.endno K, c.sumcount L "
	//						+ "from lzcard c, lmcertifydes d, lzcardnumber n, "
	//						+ "(select certifycode, sendoutcom, receivecom, startno, endno, modifydate from lzcardtrack where 1 = 1 " 
	//						+ sendOutComString + receiveComString
	//						+ getWherePart('CertifyCode','certifyCode')
	//						+ getWherePart('StartNo','startNo', '>=')
	//						+ getWherePart('EndNo','endNo', '<=')
	//						+ getWherePart('ModifyDate','sendStartDate', '>=')
	//						+ getWherePart('ModifyDate','sendEndDate', '<=')
	//						+ ") as t "
	//						+ "where c.certifycode = t.certifycode and c.certifycode = d.certifycode and c.subcode = n.cardtype "
	//						+ "and c.startno = n.cardserno and c.startno >= t.startno and c.endno <= t.endno "
	//						+ certifyClass + stateString
	//						+ ") AS CARD LEFT OUTER JOIN "
	//						+ "(SELECT PRTNO M, CONTNO N, INSUREDNAME O, APPNTNAME P, AGENTCODE Q, PREM R FROM LCCONT WHERE CONTTYPE = '1' "
	//						+ "UNION SELECT PRTNO M, GRPCONTNO N, GRPNAME O, GRPNAME P, AGENTCODE Q, PREM R FROM LCGRPCONT"
	//						+ ") AS CONT "
	//						+ "ON CARD.E = CONT.M ORDER BY A WITH UR ";
							
		strSQL = "select A, B, C, D, F, G, H, I, J, K, L, N, O, P, Q, R FROM "
						+ "(select c.certifycode A, (select d.certifyname from lmcertifydes d where d.certifycode = c.certifycode) B, "
						+ "'" + fm.sendStartDate.value + "' C,'" + fm.sendEndDate.value + "' D, n.cardno E, "
						+ "case when c.sendoutcom like 'A86%' then (select name from LDCom where ComCode = substr(c.sendoutcom, 2)) else (select a.username from LZCertifyUser a where a.usercode = substr(c.sendoutcom, 2)) end F, "
						+ "case when c.receivecom like 'A86%' then (select name from LDCom where ComCode = substr(c.receivecom, 2)) else (select a.name from laagent a where a.agentcode = substr(c.receivecom, 2)) end G, "
						+ "case when c.receivecom like 'A86%' then (select name from LDCom where ComCode = substr(c.receivecom, 2)) else (select a.name from laagent a where a.agentcode = substr(c.receivecom, 2)) end H, "
						+ "c.state I,c.startno J, c.endno K, c.sumcount L "
						+ "from lzcard c, lmcertifydes d, lzcardnumber n "
						+ "where c.certifycode = d.certifycode and c.subcode = n.cardtype and c.startno = n.cardserno "
						+ sendOutComString + receiveComString
						+ getWherePart('c.CertifyCode','certifyCode')
						+ getWherePart('c.StartNo','startNo', '>=')
						+ getWherePart('c.EndNo','endNo', '<=')
						+ getWherePart('c.ModifyDate','sendStartDate', '>=')
						+ getWherePart('c.ModifyDate','sendEndDate', '<=')
						+ certifyClass + stateString
						+ ") AS CARD LEFT OUTER JOIN "
						+ "(SELECT PRTNO M, CONTNO N, INSUREDNAME O, APPNTNAME P, AGENTCODE Q, PREM R FROM LCCONT WHERE CONTTYPE = '1' "
						+ "UNION SELECT PRTNO M, GRPCONTNO N, GRPNAME O, GRPNAME P, AGENTCODE Q, PREM R FROM LCGRPCONT WHERE 1 = 1 "
						+ ") AS CONT "
						+ "ON CARD.E = CONT.M ORDER BY A WITH UR ";
	}
	
	fm.querySql.value = strSQL;
	
	return true;
}

