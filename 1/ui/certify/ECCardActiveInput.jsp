<html>
<%
//Creator :zhangshuo
//date :2010-10-12
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	
    GlobalInput tGI = new GlobalInput();        
    tGI = (GlobalInput)session.getValue("GI");
 	
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
 	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
 	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
 	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
 	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
 	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 	<SCRIPT src="ECCertifyActiveInput.js"></SCRIPT>
 	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 	<%@include file="ECCertifyActiveInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="ECCardActiveSave.jsp" method=post name=fm target="fraSubmit">	

	<table>
        <tr>
          <td class= titleImg>
              保单信息
          </td>
        </tr>
    </table>
   	<table class= common>
   		<tr class= common>
   			<td class= title> 激活卡号：</TD>
			<td class="input"><Input class= common name=cardNum></td>
			<td class= title> 激活卡密码：</TD>
			<td class="input"><Input class= common name=cardPassword></td>	
   		</tr>
   		<tr class= common>
   			<td class= title> 生效日期：</TD>
			<td class="input">
				<input class="coolDatePicker" dateFormat="short" elementtype=nacessary name="cvalidate" verify="投保人出生日期|notnull&date" >
			</td>
			<td class= title> 档次：</TD>
			<td class="input">
				<input class=codeno name=mult CodeData="0|^1|1档^2|2档^3|3档^4|4档^5|5档^6|6档^7|7档^8|8档^9|9档"  readonly
                    ondblClick="showCodeListEx('mult',[this,multName],[0,1],null,null,null,1);" ><input name=multName class=codename  verify="生成密码|NOTNULL" elementtype=nacessary>
                
			</td>	
   		</tr>
 	</table>
<hr>
	<table>
        <tr>
          <td class= titleImg>
              投保人信息
          </td>
        </tr>
    </table>
    <table class= common>
   		<tr class= common>
   			<td class= title> 姓名：</td>
			<td class="input"><Input class= common name=appName></td>
			<td class= title> 证件类型：</TD>
			<TD  class= input>
            	<Input class=codeNo name="appIdType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
          	</TD>
   		</tr>
   		<tr class= common>
			<td class= title> 性别：</TD>
			<TD  class= input>
            <Input class=codeNo name=appSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
          	</TD>
			<td class= title> 证件号码：</td>
			<TD  class= input>
            	<Input class= common3 name="appIdNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
          	</TD>
   		</tr>
   		<tr class= common>
			<td class= title> 出生日期：</td>
			<TD  class= input>
          		<input class="coolDatePicker" dateFormat="short" elementtype=nacessary name="appBirthday" verify="投保人出生日期|notnull&date" >
          	</TD>
   		</tr>
 	</table>
<hr>
	<table>
        <tr>
          <td class= titleImg>
              被保人信息
          </td>
        </tr>
    </table>
    <!--  table class= common>
   		<tr class= common>
   			<  td class= title> 与投保人关系：</td>
			<td class="input"><Input class= common name=toAppRela></td>
   			<td class= title> 姓名：</td>
			<td class="input"><Input class= common name=insName></td>
			<td class= title> 性别：</TD>
			<TD  class= input>
            <Input class=codeNo name=insSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,INSUSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,INSUSexName],[0,1]);"><input class=codename name=INSUSexName readonly=true elementtype=nacessary>    
          	</TD>
   		</tr>
   		<tr class= common>
			<td class= title> 证件类型：</TD>
			<TD  class= input>
            	<Input class=codeNo name="insIdType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,INSUIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,INSUIDTypeName],[0,1]);"><input class=codename name=INSUIDTypeName readonly=true elementtype=nacessary>    
          	</TD>
			<td class= title> 出生日期：</td>
			<TD  class= input>
          		<input class="coolDatePicker" dateFormat="short" elementtype=nacessary name="insBirthday" verify="投保人出生日期|notnull&date" >
          	</TD>
   		</tr>
   		<tr class= common>
			
          	<td class= title> 证件号码：</td>
			<TD  class= input>
            	<Input class= common3 name="insIdNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
          	</TD>
			
   		</tr>
 	</table>-->
 	<Div  id= "divLLReport2" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1>
  				<span id="spanCertifyMaxGrid" >
  				</span>
  			</td>
  		</tr>
    </table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="showInfo1()">
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="showInfo2()">
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="showInfo3();">
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="showInfo4();">
  	</Div>
 	</div>
<hr>
	<table>
        <tr>
          <td class= titleImg>
              受益人信息
          </td>
        </tr>
    </table>
    <table class= common>
   		<tr class= common>
   			<td class= title>法定</td>
		
 	</table>
 <br/>
 <INPUT VALUE="激 活" class=cssButton TYPE=button onclick="submitForm()">  
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>