<%
//程序功能：
//创建日期：2002-11-25
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
	//输入参数
	CertifyStateModifyUI   tModifyAmntUI = new CertifyStateModifyUI();
	
	String mState = request.getParameter("upStateModify");
	String mStateflag = request.getParameter("upStateflagModify");
	String tRadio[] = request.getParameterValues("InpPolGridSel"); 
	String tCertifyCode[] = request.getParameterValues("PolGrid1");
	String tStartNo[] = request.getParameterValues("PolGrid2");
	String tEndNo[] = request.getParameterValues("PolGrid3");

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();
	
	for (int i=0; i< tRadio.length;i++){
		if("1".equals(tRadio[i])){
			transferData.setNameAndValue("CertifyCode", tCertifyCode[i]);
			transferData.setNameAndValue("StartNo", tStartNo[i]);
			transferData.setNameAndValue("EndNo", tEndNo[i]);
			transferData.setNameAndValue("upStateModify", mState);
			transferData.setNameAndValue("upStateflagModify", mStateflag);
			break;
		}
				
	}
	if(!"Fail".equals(FlagStr)){
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tModifyAmntUI.submitData(tVData, "");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = tModifyAmntUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>