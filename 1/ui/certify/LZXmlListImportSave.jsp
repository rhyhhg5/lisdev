<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
System.out.println("LZXmlListImportSave.jsp Begin ...");

//String tApplyBatchNo = request.getParameter("BatchNo");
//System.out.println("tApplyBatchNo：" + tApplyBatchNo);

GlobalInput tG = (GlobalInput)session.getValue("GI");

String FlagStr = "Fail";
String Content = "";

/** 文件上传成功标志。 */
boolean tSucFlag = false;

/** 是否可以上传文件标志。 */
boolean tCanUploadFlag = false;

String mImportPath = null;
String mFileName = null;

String tBatchImportDir = new ExeSQL().getOneValue("select SysVarValue from LDSysVar where SysVar = 'WiiXmlImportDir'");

String tBatchNo = "";

if(tBatchImportDir != null && !tBatchImportDir.equals(""))
{
    tCanUploadFlag = true;
}
else
{
    Content += "未找到清单上传临时目录！";
}

mImportPath = application.getRealPath("").replace('\\','/') + '/';
mImportPath += tBatchImportDir;

//mImportPath = "D:\\temp\\xml\\";

System.out.println("...开始上载文件");
System.out.println("mImportPath:" + mImportPath);

// Initialization
DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);
// maximum size that will be stored in memory?
// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096);
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(mImportPath);

//开始读取上传信息
List fileItems = null;
try
{
	fileItems = fu.parseRequest(request);
    tSucFlag = true;
}
catch(Exception ex)
{
    tSucFlag = false;
	ex.printStackTrace();
}

// 依次处理每个上传的文件
System.out.println("开始在服务器上创建文件");

Iterator iter = fileItems.iterator();
while(iter.hasNext())
{
    FileItem item = (FileItem)iter.next();
    
    //忽略其他不是文件域的所有表单信息
    if (!item.isFormField())
    {
        String name = item.getName();
        System.out.println("name:" + name);
        
        long size = item.getSize();
        
        if((name == null || name.equals("")) && size == 0)
            continue;

        mFileName = name.substring(name.lastIndexOf("\\") + 1);
        
        //保存上传的文件到指定的目录
        try
        {
            item.write(new File(mImportPath + mFileName));
            tSucFlag = true;
        }
        catch(Exception e)
        {
            tSucFlag = false;
        	e.printStackTrace();
            System.out.println("upload file error ...");
        }
    }
}

if(tSucFlag)
{
    try
    {
        tBatchNo = mFileName.substring(0, mFileName.lastIndexOf("."));

        if(tBatchNo != null)
        {
            VData tVData = new VData();
            
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("FileName", mFileName); 
            tTransferData.setNameAndValue("FilePath", mImportPath);
            tTransferData.setNameAndValue("BatchNo", tBatchNo);
            
            tVData.add(tTransferData);
            tVData.add(tG);
        
            LZBriGrpBatchImportUI tLZBriGrpBatchImportUI = new LZBriGrpBatchImportUI();
            
            if (!tLZBriGrpBatchImportUI.submitData(tVData, "Import"))
            {
                Content = " 导入清单失败! 原因是: " + tLZBriGrpBatchImportUI.mErrors.getLastError();
                // Content = " 导入清单失败! 原因详见“导入批次错误日志”！";
                FlagStr = "Fail";
            }
            else
            {
                Content = " 导入清单成功！";
                FlagStr = "Succ";
            }
        }
        else
        {
            Content = " 批次号与上传文件名不符，请修改模版命名规则后，尝试重新上传！";
            FlagStr = "Fail";
        }
    }
    catch (Exception e)
    {
        Content = " 获取批次信息失败。";
        FlagStr = "Fail";
        e.printStackTrace();
    }
}

System.out.println("LZXmlListImportSave.jsp End ...");
%>
                  
<html>
<script language="javascript">
parent.fraInterface.afterImportCertifyList("<%=FlagStr%>", "<%=Content%>", "<%=tBatchNo%>");
</script>
</html>

