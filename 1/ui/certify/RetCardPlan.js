
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;

//提交，保存按钮对应操作
function submitForm()
{
	if( CardPlanInfo.mulLineCount == 0 ) {
		alert("没有关联的下级计划，请直接返回。");
		return;
	}
	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  { 
    //执行下一步操作
  }
}

function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CardPlan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

function returnParent()
{
    tRow=PolGrid.getSelNo();
    tCol=1;
    tPolNo = PolGrid.getRowColData(tRow-1,tCol);
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    top.location.href="./CardPlanDetail.jsp?PolNo="+tPolNo;
}

function queryPlan()
{
	var vSQL = "select * from lzcardplan where 1=1";

	try {
		vSQL += eval("top.opener.fm.sql_where.value");
	} catch (e) {
		return;
	}

	//using my docode function
	var myResult = myDecodeEasyQueryResult(easyQueryVer3(vSQL));

	if( myResult != null ) {
		fm.PlanID.value = myResult[0][0];
		fm.CertifyCode.value = myResult[0][1];
		fm.AppCom.value = myResult[0][4];
		fm.AppCount.value = myResult[0][2];
		fm.RetCom.value = myResult[0][7];
		fm.RetCount.value = myResult[0][5];
		fm.MakeDate.value = myResult[0][11];
		fm.MakeTime.value = myResult[0][12];
		fm.RetState.value = myResult[0][8];
		fm.PlanState.value = myResult[0][15];
	}
	
	vSQL = "select * from lzcardplan where 1=1";

	try {
		vSQL += eval("top.opener.fm.sql_where_ex.value");
	} catch (e) {
		return;
	}

	//using my docode function
	var myResult = myDecodeEasyQueryResult(easyQueryVer3(vSQL));

	CardPlanInfo.clearData();
	
	if( myResult == null ) {
		return;
	}

	var vRow;

	for( vRow = 0; vRow < myResult.length; vRow ++ ) {
		CardPlanInfo.addOne();
		
		CardPlanInfo.setRowColData(vRow, 1, myResult[vRow][0]);
		CardPlanInfo.setRowColData(vRow, 2, myResult[vRow][4]);
		CardPlanInfo.setRowColData(vRow, 3, myResult[vRow][1]);
		CardPlanInfo.setRowColData(vRow, 4, myResult[vRow][2]);
		CardPlanInfo.setRowColData(vRow, 5, myResult[vRow][5]);
		CardPlanInfo.setRowColData(vRow, 6, myResult[vRow][8]);
	}
}

// EasyQuery 的这个函数会和 turnPage对象相互影响，所以就自己照着写了一个
function myDecodeEasyQueryResult(strResult) {
	var arrEasyQuery = new Array();
	var arrRecord = new Array();
	var arrField = new Array();
	var recordNum, fieldNum, i, j;

	if (typeof(strResult) == "undefined" || strResult == "" || strResult == false)	{
		return null;
	}

	//公用常量处理，增强容错性
	if (typeof(RECORDDELIMITER) == "undefined") RECORDDELIMITER = "^";
	if (typeof(FIELDDELIMITER) == "undefined") FIELDDELIMITER = "|";

	try {
	  arrRecord = strResult.split(RECORDDELIMITER);      //拆分查询结果，得到记录数组
	  
	  recordNum = arrRecord.length;
	  for(i=1; i<recordNum; i++) {
	  	arrField = arrRecord[i].split(FIELDDELIMITER); //拆分记录，得到字段数组
	  	
	  	fieldNum = arrField.length;
	  	arrEasyQuery[i - 1] = new Array();
	  	for(j=0; j<fieldNum; j++) {
		  	arrEasyQuery[i - 1][j] = arrField[j];          //形成以行为记录，列为字段的二维数组
		  }
	  }		
	} 
	catch(ex) {
	  alert("拆分数据失败！" + "\n错误原因是：" + ex);
	  return null;  
	}
  
	return arrEasyQuery;
}

function addPlan() {
	if( fm.PlanID.value != null && fm.PlanID.value != '' ) {
		alert("计划标识不为空，请重新输入计划信息！");
		return;
	}
	
	fm.oper_flag.value = "INSERT||MAIN";
	submitForm();
}

function updatePlan() {
	if( fm.PlanID.value == null || fm.PlanID.value == "" ) {
		alert("没有指定要更新计划的标识！");
		return;
	}
	
	fm.oper_flag.value = "UPDATE||MAIN";
	submitForm();
}

function delPlan() {
	if( fm.PlanID.value == null || fm.PlanID.value == "" ) {
		alert("没有指定要删除计划的标识！");
		return;
	}
	
	fm.oper_flag.value = "DELETE||MAIN";
	submitForm();
}