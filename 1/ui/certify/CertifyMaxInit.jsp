<%
//Creator :刘岩松
//Date :2003-04-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%

  String strCertifyCode = "Select CertifyCode,CertifyName From LMCertifyDes "
                        +"Where CertifyClass = 'P' or CertifyClass = 'D'";
  ExeSQL exesql = new ExeSQL();
  SSRS ssrs =exesql.execSQL(strCertifyCode);
  String CertifyCode = ssrs.encode();
  System.out.println(CertifyCode);

  //在GlobalInput.java中有ManageCom（当前操作员的管理机构）
  //在GlobalInput.java中有ComCode （当前的登陆机构）
  GlobalInput globalInput = (GlobalInput)session.getValue("GI");
  String strComCode = globalInput.ComCode;
  System.out.println("当前的管理机构是"+strComCode);
  String ComCode_sql = "Select AgentCode,Name From LAAgent Where ManageCom like'"
                 +strComCode+"%'";
  System.out.println("您查询代理人信息的查询语句是"+strComCode);
  SSRS ComCode_ssrs = exesql.execSQL(ComCode_sql);
  String strAgentCode = ComCode_ssrs.encode();
  System.out.println("对代理人信息进行虚拟数据源的结果是");
  System.out.println(strAgentCode);
%>
<script language="JavaScript">
function RegisterDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divDetailInfo.style.left=ex;
  	divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}
// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {

  }
  catch(ex)
  {
    alert("1在CertifyMaxInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
		initCertifyMaxGrid();
  }
  catch(re)
  {
    alert("3CertifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCertifyMaxGrid()
{
	var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[1]=new Array();
      iArray[1][0]="代理人编码";         			//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;
      iArray[1][10]="AgentCodeList"
      iArray[1][11]="<%= strAgentCode%>"

      iArray[2]=new Array();
      iArray[2][0]="单证编码";    	//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=150;            			//列最大值
      iArray[2][3]=2;
      iArray[2][10]="CertifCodeyList";
      iArray[2][11]="<%= CertifyCode%>";
      iArray[2][12]="2|4";
      iArray[2][13]="0|1";
      iArray[2][18]=350;
      iArray[2][19]=1;

      iArray[3]=new Array();
      iArray[3][0]="单证类型";         			//列名
      iArray[3][1]="0px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=3;
      iArray[3][10]="CertifyClass";
      iArray[3][11]="0|^P|普通单证^D|定额单证^S|系统单证";

      iArray[4]=new Array();
      iArray[4][0]="单证名称";         			//列名
      iArray[4][1]="250px";            		//列宽
      iArray[4][2]=250;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="最大数量";         			//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=1;

      iArray[6]=new Array();
      iArray[6][0]="代理人编码";         			//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=60;            			//列最大值
      iArray[6][3]=3;

      CertifyMaxGrid = new MulLineEnter( "fm" , "CertifyMaxGrid" );
      CertifyMaxGrid.mulLineCount = 0;
      CertifyMaxGrid.displayTitle = 1;
      CertifyMaxGrid.loadMulLine(iArray);
      CertifyMaxGrid.detailInfo="单击显示详细信息";
      CertifyMaxGrid.detailClick=RegisterDetailClick;
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>