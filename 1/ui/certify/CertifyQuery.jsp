<%@page contentType="text/html;charset=GBK" %>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="CertifyQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyQueryInit.jsp"%>
<title>普通单证查询</title>
<script>
	var tManageCom = <%=tG1.ManageCom%>;
</script>
</head>
<body  onload="initForm();" >
  <form action="./CertifyQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 普通单证信息部分 -->
    <table class="common" border=0 width=100%>
      <tr><td class="titleImg" align="center">请输入查询条件：</td></tr>
	  </table>

    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <td class="title">单证编码</td>
        <td class="input">
          <input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('CertifyCode', [this,CertifyCodeName],[0,1],null,null,null,1,300);"><Input class= codename name="CertifyCodeName" ></td>
        <td class="title">统计状态</td>
        <td class="input">
          <input class="codeno" name="State" CodeData="0|^1|库存|^2|已发放|^3|已回收|^4|发放未回收|^5|入库总量|" ondblclick="return showCodeListEx('State', [this,StateName],[0,1]);" onkeyup="return showCodeListKeyEx('State', [this,StateName],[0,1]);" verify="统计状态|NOTNULL"><Input class= codename name="StateName" ></td>
      </tr>
      <tr class="common">
        <td class="title">发放者</td>
        <td class="input">
          	<input class="codeno" name="SendOutCom" 
          	ondblclick="return showCodeList('sendoutcodesearch', [this,SendOutName],[0,1],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcodesearch', [this,SendOutName],[0,1],null,null,null,1);"
          	><input name = "SendOutName" class=codename type="text">
        </td>

        <td class="title">接收者</td>
        <td class="input" nowrap=true>
           	<input class="codeno" name="ReceiveCom"  
	          	ondblclick="return showCodeList('receivecodesearch', [this,ReceiveName],[0,1],null,fm.ManageCom.value,'ComCode',1);"
	            onkeyup="return showCodeListKey('receivecodesearch', [this,ReceiveName],[0,1],null,fm.ManageCom.value,'ComCode',1);"
	          ><input name = "ReceiveName" class=codename type="text">
          	<input class="codeno" name="AgentCode" ondblclick="return showCodeList('sendoutcodesearch', [this],[0],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcodesearch', [this],[0],null,null,null,1);"
          	style="display:'none'">
          	<input type="button" class="button" name="AgentQuery" value="业务员查询" onclick="queryAgent()" style="display:'none'">
          	<input type="button" class="button" name="btnQueryCom" value="代理机构查询" onclick="queryCom()" style="display:none"></td>
      </tr>

      <tr class="common" style="display:'none'" >
        <td class="title">经办人</td>
        <td class="input"><input class="common" name="Handler"></td></tr>

      <tr class="common">
        <td class="title">经办日期（开始）</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDateB"></td>

        <td class="title">经办日期（结束）</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDateE"></td></tr>

      <tr class="common"><td cols=4><br></td></tr>

      <tr class="common">
        <td class="title">清算单号</td>
        <td class="input"><input class="common" name="ReckCode"></td>

        <td class="title">数量</td>
        <td class="input"><input class="common" name="Count"></td></tr>

      <tr class="common">
        <td class="title">起始号</td>
        <td class="input"><input class="common" name="StartNo"></td>

        <td class="title">终止号</td>
        <td class="input"><input class="common" name="EndNo"></td></tr>

      <tr class="common">
        <td class="title">单证状态</td>
        <td class="input"><input class="codeNo" name="CertifyState" CodeData="0|^0|未领用|^2|作废|^3|遗失|^4|销毁|^5|正常回销|^6|总部领用|^7|已发放|^8|已领用|^9|空白回销" ondblclick="return showCodeListEx('CertifyState',[this,CertifyStateName],[0,1],null,null,null,1);"><input class=codename name=CertifyStateName readonly=true ></td>

        <td class="title">单证标志</td>
        <td class="input"><input class="common" name="CertifyFlag"></td></tr>

      <tr class="common"><td cols=4><br></td></tr>

      <tr class="common">
        <td class="title">数量合计</td>
        <td class="input" cols=3><input class="readonly" readonly name="SumCount"></td></tr>

    </table>

    <input value="查  询" type="button" onclick="submitForm( ); return false;" class="cssButton">
    <input value="打  印" type="button" onclick="printList( ); return false;" class="cssButton">
    <input value="返  回" type="hidden" onclick="returnParent( );") class="cssButton">

    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCertifyInfo);"></td>
    	<td class= titleImg>单证信息</td></tr>
    </table>

  	<div id="divCardInfo" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCardInfo"></span></td></tr>
      </table>

      <input VALUE="首  页" TYPE="button" onclick="turnPage.firstPage();" class="cssButton">
      <input VALUE="上一页" TYPE="button" onclick="turnPage.previousPage();" class="cssButton">
      <input VALUE="下一页" TYPE="button" onclick="turnPage.nextPage();" class="cssButton">
      <input VALUE="尾  页" TYPE="button" onclick="turnPage.lastPage();" class="cssButton">
  	</div>
  	<input type=hidden name="ReceiveType" value="COM">
		<input type=hidden name="ManageCom" value="<%=tG1.ManageCom%>">
		<input type=hidden name="SearchSQL" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
