var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{  
	var agePeriod=0;//业务员回销期
	var comPeriod=0;//代理机构回销期
	var arrReturn1 = new Array();
	var arrReturn2 = new Array();
	/*var ageSql=" select VerPeriod from LDAgentCardCount where AgentCode='Y' and CertifyCode in (select distinct CertifyCode from LMCertifyDes where CertifyClass='D') AND ManageCom='N' AND AgentGrade='N' AND AgentCom='N' ";
	arrReturn1 =easyExecSql(ageSql);
	 if (arrReturn1 == null ) {
	 	// alert("部分定额单证尚未设置业务员的回销期，请到 '定额单证发放数量配置' 处设置！！！");
	 	 //return;
	 }
	var  comSql=" select VerPeriod from LDAgentCardCount where AgentCode='N' and CertifyCode in (select distinct CertifyCode from LMCertifyDes where CertifyClass='D') AND ManageCom='N' AND AgentGrade='N' AND AgentCom='Y' ";
	 arrReturn2 =easyExecSql(comSql);
	 if (arrReturn2 == null ) {
	 	 //alert("部分定额单证尚未设置代理机构的回销期，请到 '定额单证发放数量配置' 处设置！！！");
	 	// return;
	 }*/
	// 初始化表格
	initCertifyMaxGrid();
	var selOver = " ( select to_date(current date) -(select to_date(min(HandleDate)) from lzcard e where e.CertifyCode=b.CertifyCode and e.receivecom=b.receivecom AND e.STATE IN ('10','11','12') ) - case when substr(b.receivecom,1,1)='D' then (SELECT CASE WHEN e.VerPeriod IS NULL THEN 0 ELSE e.VerPeriod END FROM LDAgentCardCount e, laagent f WHERE (e.AgentCode='N' and e.CertifyCode=b.CertifyCode AND e.ManageCom='N' AND e.AgentGrade='Y' and f.agentcode=substr(b.receivecom,2) and BranchType='2' or (e.AgentCode='Y' and e.CertifyCode=b.CertifyCode AND e.ManageCom='N' AND e.AgentGrade='N' and f.agentcode=substr(b.receivecom,2) and BranchType='1' ) )) else (SELECT CASE WHEN VerPeriod IS NULL THEN 0 ELSE VerPeriod END FROM LDAgentCardCount WHERE AgentCode='N' and CertifyCode=b.CertifyCode AND ManageCom='N' AND AgentGrade='N' AND AgentCom='Y') end from dual)";
   var selCnt = "(select sum(SumCount) from LZCard c where c.SubCode=b.SubCode and c.CertifyCode=b.CertifyCode  and c.ReceiveCom=b.ReceiveCom AND c.STATE IN ('10','11','12')"  ;
   var strSQL = "select case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2)) else substr(b.receivecom,2) end,(select name from laagent where agentcode=substr(b.receivecom,2) "  
   +" union all select name from lacom where agentcom=substr(b.receivecom,2)), b.SubCode,a.certifyname,"
     + selCnt + "  and c.state in ('10','11','12') ) ,"+   selOver     
	   + " from LMCertifyDes a,LZCard b"	   
     + " where a.SubCode=b.SubCode and a.CertifyCode=b.CertifyCode"
     + " and b.state in ('10','11','12') and a.CertifyClass='D'"
     + " and "+ selCnt+" )>0"
     + " and (select count(*)  from dual where to_date(current date) -to_date(b.HandleDate) + 5 >=(case when substr(b.receivecom,1,1)='D' then (SELECT CASE WHEN e.VerPeriod IS NULL THEN 0 ELSE e.VerPeriod END FROM LDAgentCardCount e, laagent f WHERE (e.AgentCode='N' and e.CertifyCode=b.CertifyCode AND e.ManageCom='N' AND e.AgentGrade='Y' and f.agentcode=substr(b.receivecom,2) and BranchType='2' or (e.AgentCode='Y' and e.CertifyCode=b.CertifyCode AND e.ManageCom='N' AND e.AgentGrade='N' and f.agentcode=substr(b.receivecom,2) and BranchType='1' ) )) else (SELECT CASE WHEN VerPeriod IS NULL THEN 0 ELSE VerPeriod END FROM LDAgentCardCount WHERE AgentCode='N' and CertifyCode=b.CertifyCode AND ManageCom='N' AND AgentGrade='N' AND AgentCom='Y') end))>0"
     + " and  (SELECT count(*) FROM LDUSER a WHERE a.USERCODE in (select USERCODE from LDUSER d where d.USERCODE =b.operator and d.comcode like '"+managecom +"%%') and a.comcode like '"+managecom +"%%')>0"
	 
	   + " group by b.SubCode,a.CertifyName,b.CertifyCode,b.receivecom"
	  ;	  

	turnPage.queryModal(strSQL, CertifyMaxGrid); 
 
}

//根据管理机构和代理人级别进行查询的函数
function queryComInfo()
{  
	var agePeriod=0;//业务员回销期
	var comPeriod=0;//代理机构回销期
	var arrReturn1 = new Array();
	var arrReturn2 = new Array();	
	// 初始化表格
	initCertifyComGrid();
   var strSQL = "select distinct a.agentcom,(select name from lacom where lacom.agentcom =a.agentcom )"
		+",(select to_date(current date) - to_date(max(a.enddate)) from dual )"
		+",'1',max(a.enddate),'',sum(b.sumcount),b.subcode,(select certifyname from lmcertifydes where CertifyCode =b.CertifyCode) "
		+" from lacont a , lzcard b"
		+" where a.agentcom = substr(b.receivecom,2)"
		+" and b.state in ('11','12')"
		+" and (select count(*) from dual where to_date(current date) -to_date(a.enddate) >= 10 )>0"
		+ " and  (SELECT count(*) FROM LDUSER a WHERE a.USERCODE in (select USERCODE from LDUSER d where d.USERCODE =b.operator and d.comcode like '"+managecom +"%%') and a.comcode like '"+managecom +"%%')>0"
		+" group by a.agentcom,b.receivecom,b.subcode,b.CertifyCode"
		+ " union  "
		+ " select distinct a.agentcom,a.name "
		+",(select to_date(current date) - to_date(max(a.licenseenddate)) from dual )"
		+",'1',max(a.licenseenddate),'',sum(b.sumcount),b.subcode,(select certifyname from lmcertifydes where CertifyCode =b.CertifyCode)"
		+" from lacom a , lzcard b"
		+" where a.agentcom = substr(b.receivecom,2)"
		+" and b.state in ('11','12')"
		+" and (select count(*) from dual where to_date(current date) -to_date(a.licenseenddate) >= 10 )>0"
		+ " and  (SELECT count(*) FROM LDUSER a WHERE a.USERCODE in (select USERCODE from LDUSER d where d.USERCODE =b.operator and d.comcode like '"+managecom +"%%') and a.comcode like '"+managecom +"%%')>0"
		+" group by a.agentcom,a.name,b.receivecom,b.subcode,b.CertifyCode"
		;


	turnPage1.queryModal(strSQL, CertifyComGrid); 
 
}


function showalert(){
	alert("没有符合条件的数据！");
}
