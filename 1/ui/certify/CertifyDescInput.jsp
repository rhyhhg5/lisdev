<%@page contentType="text/html;charset=GBK" %>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.certify.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src = "CertifyDescInput.js"></SCRIPT>
    <%@include file="CertifyDescInit.jsp"%>
</head>
<body onload="initForm();initElementtype();" >
    <form action="./CertifyDescSave.jsp" method=post name=fm target="fraSubmit">
        <%@include file="../common/jsp/OperateButton.jsp"%>
        <%@include file="../common/jsp/InputButton.jsp"%>
        <table class=common>
            <tr class=common>
                <td class=title>单证编码</td>
                <td class=input><input class=common name=CertifyCode id=CertifyCodeId elementtype=nacessary></td>
                <td class=title>单证名称</td>
                <td class=input><input class=common name=CertifyName elementtype=nacessary></td>
            </tr>
            <tr class=common>
                <td class=title>单证类型</td>
                <td class=input><input class=codeno name=CertifyClass CodeData="0|^P|普通单证^D|定额单证"  ondblClick="showCodeListEx('CertifyClassListNew',[this,CertifyClassName],[0,1],null,null,null,1);" verify="单证类型|NOTNULL"><input class=codename name='CertifyClassName' readonly elementtype=nacessary></td>
                <td class=title>业务员校验标志</td>
                <td class=input><input class=codeno readonly name="VerifyFlag" CodeData="0|^Y|校验^N|不校验"
                    ondblClick="showCodeListEx('CertifyVerifyFlag',[this,VerifyFlagName],[0,1],null,null,null,1);" ><input class=codename name= 'VerifyFlagName'  verify="业务员校验标志|NOTNULL" elementtype=nacessary>
                </td>
            </tr>
            <tr class=common>
                <td class=title>单证号码长度</td>
                <td class=input><input class=common name=CertifyLength elementtype=nacessary></td>
                <td class=title>状态</td>
                <td class=input><input class=codeno name=State readonly CodeData="0|^0|有效^1|无效"
                    ondblClick="showCodeListEx('CertifyStateList',[this,StateName],[0,1],null,null,null,1);" ><input name=StateName class=codename verify="状态|NOTNULL" elementtype=nacessary >
                </td>
            </tr>
            <tr class=common>
                <td class=title>是否是有号单证</td>
                <td class=input><input class=codeno name=HaveNumber CodeData="0|^Y|有号单证^N|无号单证"  readonly
                    ondblClick="showCodeListEx('CertifyHaveNumber',[this,HaveNumberName],[0,1],null,null,null,1);" ><input name=HaveNumberName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary>
                </td>
                <td class="title">单证单位</td>
                <td class="input"><input class=common name="Unit"></td>
            </tr>
        </table>

        <table class=common>
            <tr>
                <td class=title>是否选定具体管理机构</td>
                <td class=input>
                    <input type="radio" name="StateRadio" value="1" checked onclick="showCom()"> 是 &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="StateRadio" value="0" onclick="showCom()"> 否
                </td>
                <td class=title>
                    <div id="divComName" style="display:''">
                        管理机构
                    </div>
                </td>
                <td class=input>
                    <div id="divComCode" style="display:''">
                        <input class=codeno name= ManageCom readonly style="display:''"
                        ondblclick="return showCodeList('Station', [this,ManageComName],[0,1],null,null,null,1);" ><input name=ManageComName class=codename verify="管理机构|NOTNULL" elementtype=nacessary>
                    </div>
                </td>
            </tr>
            <tr>
                <td class=title>备注</td>
                <td class=input><input class=common name=Note type="text"></td>
                <td class=title> </td>
                <td class=title> </td>
            </tr>
        </table>
        <div id="divShow" style="display: 'none'">
            <table class=common>
                <tr> 
                    <td class=title>校验规则</td>
                    <td class=input><input class=codeno name=CheckRule CodeData="0|^1|有校验^2|无校验"  readonly
                        ondblClick="showCodeListEx('ChkRule',[this,ChkRuleName],[0,1],null,null,null,1);" ><input name=ChkRuleName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary></td>
                    <td class=title>单证类型码</td>
                    <td class="input"><input class=common name=SubCode maxlength=10 verify="单证类型|NUM&NOTNULL"  elementtype=nacessary" >
                    <input class=common type= hidden name="SubCodeBackup" value=""></td>
                </tr>
                <tr>
                    <td class=title>业务类型</td>
                    <td class=input><input class=codeno name=Operate verify="类型|NOTNULL" CodeData="0|^0|卡单业务^1|撕单业务^2|虚拟卡业务^3|POS机出单^4|网销" ondblClick="showCodeListEx('CertifyTypeList',[this,CardTypeName],[0,1]);" ><input class=codename name='CardTypeName' readonly elementtype=nacessary ></td>
                </tr>
            </table>
        </div>
        <div id= "divCardRisk" style="display: 'none'">
            <table>
                <tr>
                    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardRisk);"></td>
                    <td class=titleImg>定额单险种信息</td>
                </tr>
            </table>
            <table class=common>
                <tr class=common>
                    <td text-align: left colSpan=1>
                        <span id="spanCardRiskGrid" >
                        </span>
                    </td>
                </tr>
            </table>
            <table class=common>
                <tr class=common>
                    <td class=title>保险期限</td>
                    <td class=input><input class=common name=DutyPer readonly ></td>
                    <td class=title>单位</td>
                    <td class=input><input class=codeno name=DutyUinit readonly ><input name=DutyUinitName class=codename  verify="是否是有号单证|NOTNULL" readonly >
                    </td>
                </tr>
            </table>
        </div>

        <input type="hidden" name=Prem >
        <input type="hidden" name=Amnt >
        <input type="hidden" name=OperateType >
        <input type="hidden" name=CertifyCode_1 >
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>