
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;

//查询按钮所对应的操作
function submitForm()
{
	if (fm.ReceiveType.value=="AGE") {
		fm.ReceiveCom.value=fm.AgentCode.value;
	}
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.target = "fraSubmit";
	fm.action = "./CertifyReportOut.jsp"
  fm.submit(); //提交
}

//打印按钮所对应的操作
function reportPrint()
{
	fm.target = "_blank";
	fm.action = "./CertifyReportPrint.jsp"
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  CardInfo.clearData();  // 清空原来的数据

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifyReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

// 在查询结束时，触发这个事件。
function onShowResult(result)
{

	if( result[0].charAt(2) == '0' ) {
		alert('没有查询到数据');
	} else {
		strSQL = result[1];
   	fm.SearchSQL.value = strSQL;
		useSimulationEasyQueryClick(result[0]);
	}
}


function useSimulationEasyQueryClick(strData) {
  //保存查询结果字符串
  turnPage.strQueryResult  = strData;

  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;

  //拆分字符串，返回二维数组
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);

  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray          = new Array(0, 6, 7, 8);

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = CardInfo

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }

  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
}

// 事件响应函数，当用户改变CodeSelect的值时触发
function afterCodeSelect(cCodeName, Field)
{
	try {
		if( cCodeName == '' ) {
			var trObj = tbInfo.rows(1);
			var tdObj;

			if( Field.value == '1' ) {  // 库存
				trObj.cells(0).innerHTML = "";
				trObj.cells(1).innerHTML = "<input class='common' name='SendOutCom' type='hidden'>";
        trObj.cells(2).innerHTML = "统计机构";
        trObj.cells(3).innerHTML = "<input class='common' name='ReceiveCom'>";

			} else if( Field.value == '2' ) {  // 已发放
				trObj.cells(0).innerHTML = "从";
				trObj.cells(1).innerHTML = "<input class='common' name='SendOutCom'>";
        trObj.cells(2).innerHTML = "发放到";
        trObj.cells(3).innerHTML = "<input class='common' name='ReceiveCom'>";

			} else if( Field.value == '3' ) {  // 已回收
				trObj.cells(0).innerHTML = "从";
				trObj.cells(1).innerHTML = "<input class='common' name='SendOutCom'>";
        trObj.cells(2).innerHTML = "回收到";
        trObj.cells(3).innerHTML = "<input class='common' name='ReceiveCom'>";

			} else if( Field.value == '4' ) {  // 发放未回收
				trObj.cells(0).innerHTML = "从";
				trObj.cells(1).innerHTML = "<input class='common' name='SendOutCom'>";
        trObj.cells(2).innerHTML = "发放到";
        trObj.cells(3).innerHTML = "<input class='common' name='ReceiveCom'>";

			} else if( Field.value == '5' ) {  // 入库总量
				trObj.cells(0).innerHTML = "";
				trObj.cells(1).innerHTML = "<input class='common' name='SendOutCom' type='hidden'>";
        trObj.cells(2).innerHTML = "统计机构";
        trObj.cells(3).innerHTML = "<input class='common' name='ReceiveCom'>";

			} else {
				trObj.cells(0).innerHTML = "发放者";
				trObj.cells(1).innerHTML = "<input class='common' name='SendOutCom'>";
        trObj.cells(2).innerHTML = "接收者";
        trObj.cells(3).innerHTML = "<input class='common' name='ReceiveCom'>";
			}
		}
	} catch(ex) {
		alert("在afterCodeSelect中发生异常");
	}
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	fm.ReceiveType.value="AGE";
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
}
function queryCom()
{
    fm.ReceiveType.value="AGECOM";
    showInfo = window.open("./LAComMain.jsp?ManageCom=" + fm.all('ManageCom').value.substr(0, 4) + "&BranchType=3&BranchType2=01&BankType=01");
}

function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	if( fm.chkModeBatch.checked == false ) {
	    fm.PrtNoEx.value = arrResult[0][0];
	    fm.ReceiveCom.value = 'A' + arrResult[0][11];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
	    
			var rowCount = 0;

	    CertifyList.setRowColData(rowCount, 1, arrResult[0][1]);
	    CertifyList.setRowColData(rowCount, 2, arrResult[0][25]);
	    CertifyList.setRowColData(rowCount, 3, arrResult[0][18]);
	    CertifyList.setRowColData(rowCount, 4, arrResult[0][19]);
	    
	    var cNum = parseFloat(arrResult[0][19])-parseFloat(arrResult[0][18])+1;
	    CertifyList.setRowColData(rowCount, 5, cNum+"");
	    
	  } else {
	    fm.ReceiveCom.value = arrResult[0][3];
	    fm.ReceiveCom.title = arrResult[0][1];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
		}
  }
}

function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  	if (fm.ReceiveType.value=="AGE") {
  		fm.AgentCode.value='D'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][95];
  	} else if (fm.ReceiveType.value=="AGECOM") {
  		fm.AgentCode.value='E'+arrResult[0][0];
  		fm.GroupAgentCode.value = arrResult[0][0];
  	}
	}
}

function printList()
{
	if (fm.SearchSQL.value=="") {
		alert("请先进行查询");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "CertifyReportPrint";
  fm.action = "CertifyReportPrintSave.jsp";
	fm.submit();
  showInfo.close();
}