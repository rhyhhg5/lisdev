//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  var strSQL = "select serialno,attachdate,case when state='0' then '征订' else '批准' end ,"
  + " DescPerson,Note from LZOrder where 1=1 "
  + getWherePart("Serialno","Serialno")
  + getWherePart("Attachdate","StartDate",">=")
  +	getWherePart("Attachdate","EndDate","<=");
  
  strSQL = strSQL +" order by serialno DESC";
  
  var arrResult = new Array();
	//arrResult = easyExecSql(strSQL);
	
	turnPage.queryModal(strSQL, OrderDescGrid)
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

function ReturnData()
{
		var tRow=OrderDescGrid.getSelNo();


  	if (tRow==0)
   	{
   		alert("请您先进行选择!");
  		return;
  	}
  	
  	
    top.opener.fm.all('SerialNo').value=OrderDescGrid.getRowColData(tRow-1,1);
    top.opener.fm.all('AttachDate').value=OrderDescGrid.getRowColData(tRow-1,2);;
    top.opener.fm.all('OrderCreator').value=OrderDescGrid.getRowColData(tRow-1,4);
    top.opener.fm.all('Note').value=OrderDescGrid.getRowColData(tRow-1,5);
    top.opener.fm.all('SerialNoBak').value=OrderDescGrid.getRowColData(tRow-1,1);
    top.opener.fm.all('AttachDateBak').value=OrderDescGrid.getRowColData(tRow-1,2);;
    
    var strSQL = "select a.Certifycode,b.Certifyname,a.Ordermoney from LZOrderCard a,LMCertifyDes b "
			+ " where a.CertifyCode=b.CertifyCode and serialno='"+OrderDescGrid.getRowColData(tRow-1,1)+"' ";
		
		var strArray = easyExecSql(strSQL);
		
		top.opener.CertifyTypeGrid.clearData();
		
		for (var k=0;k<strArray.length;k++)
		{
			top.opener.CertifyTypeGrid.addOne("CertifyTypeGrid");
			top.opener.CertifyTypeGrid.setRowColData(k,1,strArray[k][0]);
			top.opener.CertifyTypeGrid.setRowColData(k,2,strArray[k][1]);
			top.opener.CertifyTypeGrid.setRowColData(k,3,strArray[k][2]);
		}
    
    top.close();
}

function ClosePage()
{
	top.close();
}