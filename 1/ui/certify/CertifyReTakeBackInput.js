//该文件中包含客户端需要处理的函数和事件
var turnPage=new turnPageClass();
var showInfo;
window.onfocus=myonfocus;

function easyQueryClick()
{
    if(fm.certifyCode.value == "")
    {
        alert("请选择单证类型！");
        return;
    }
    if(fm.startNo.value == "" && fm.endNo.value == "" && fm.startDate.value == "" && fm.endDate.value == "")
    {
        alert("单证起始号、单证终止号、起始日期、终止日期不能同时为空！");
        return;
    }
    var sendSQL = "";
    var comCode = fm.ManageCom.value;
    if(comCode.length == 2)
    {
        sendSQL = "";
    }
    else if(comCode.length == 4)
    {
        sendSQL = "and (SendOutCom in ("
            + "select ReceiveCom from LZAccess where SendOutCom = 'A" + comCode + "' "
            + "union select 'A" + comCode + "' from dual) "
            + "or SendOutCom like 'D%' or SendOutCom like 'E%') ";
    }
    else
    {
        alert("只有总公司及分公司操作员可以做单证状态修改！");
        return;
    }
    
    var strSQL = "select a.CertifyCode, a.StartNo, a.EndNo, a.SumCount, a.SendOutCom, a.StateFlag "
        + "from LZCard a where 1 = 1 "
        + getWherePart('a.CertifyCode', 'certifyCode')
        + getWherePart('a.StateFlag', 'oldStateFlag')
        + getWherePart('a.SendOutCom', 'sendOutCom')
        + getWherePart('a.StartNo', 'startNo', '>=')
        + getWherePart('a.StartNo', 'endNo', '<=')
        + getWherePart('a.MakeDate', 'startDate', '>=')
        + getWherePart('a.MakeDate', 'endDate', '<=')
        + sendSQL
        + "order by a.StartNo"
    ;
    
    turnPage.queryModal(strSQL, CertifyListGrid);
    fmSave.reset();
}

function certifyDetail()
{
    var sel = CertifyListGrid.getSelNo();
    if(sel == 0 || sel == null)
    {
        alert( "请先选择一条记录。" );
        return false;
    }
	fmSave.certifyCode.value = CertifyListGrid.getRowColDataByName(sel - 1, "CertifyCode");
	fmSave.startNo.value = CertifyListGrid.getRowColDataByName(sel - 1, "StartNo");
	fmSave.endNo.value = CertifyListGrid.getRowColDataByName(sel - 1, "EndNo");
	fmSave.oldStateFlag.value = CertifyListGrid.getRowColDataByName(sel - 1, "StateFlag");
}

//提交，保存按钮对应操作
function submitForm()
{
    var startNo = fmSave.startNo.value;
    var endNo = fmSave.endNo.value;
    if(fmSave.certifyCode.value == "" || startNo == "" || endNo == "")
    {
        alert("单证类型、单证起始号、单证终止号不能为空！");
        return;
    }
    if(fm.ManageCom.value.length > 4)
    {
        alert("只有总公司及分公司操作员可以做单证状态修改！");
        return;
    }
    var num = parseFloat(endNo) - parseFloat(startNo) + 1;
    if(!confirm("修改单证数量为：" + num))
        return false;
    fmSave.sumCount.value = num;
    
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fmSave.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
    showInfo.close();
    if (FlagStr == "Fail" )
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    {
        content="保存成功！";
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        easyQueryClick();
        fmSave.reset();
    }
}