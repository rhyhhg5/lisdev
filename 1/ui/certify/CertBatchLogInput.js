//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";
	strSQL = "SELECT A.CertifyCode, B.CertifyName, SendOutCom, ReceiveCom, StartNo, EndNo, SumCount, LogContent" +
					 " FROM LZCertLog A, LMCertifyDes B" +
					 " WHERE A.CertifyCode = B.CertifyCode" +
					 " AND TakeBackNo = '" + fm.hideTakeBackNo.value + "'" +
					 " ORDER BY LogSeq";
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = CertLog;
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}