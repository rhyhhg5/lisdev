var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();

function afterCodeSelect(cName, Filed)
{
    if(cName == 'CertifyClassListNew')
    {
        if(Filed.value == "D")
        {
            fm.all('divShow').style.display="";
            fm.all('divCardRisk').style.display="";
        }
        if(Filed.value == "P")
        {
            fm.all('divShow').style.display="none";
            fm.all('divCardRisk').style.display="none";
        }
    }
    else if(cName == 'ChkRule') {
        if(Filed.value=='1'){ //校验规则：有校验
            fm.CertifyLength.value='9';
            fm.CertifyLength.readOnly = true;
        }else if(Filed.value=='2'){//校验规则：无校验
            //fm.CertifyLength.value = '11';
            fm.CertifyLength.readOnly = false;
        }
    }
    else if(cName == 'CertifyStateList') {//业务类型，卡单和撕单都显示险种信息
        //if(fm.Operate.value == "0")//卡单
        //{
        //  fm.all('divCardRisk').style.display="";
        //}
        //else if(fm.Operate.value == "1")//撕单
        //{
        //  fm.all('divCardRisk').style.display="none";
        //}
    }
    else if(cName == 'CertifyHaveNumber') {//是否是有号单证
        if(Filed.value == "Y")
        {
            if(fm.CheckRule.value == '1')
            {
                fm.CertifyLength.value = "9";
                fm.CertifyLength.readOnly = true;
            }
            else
            {
                fm.CertifyLength.value = "11";
                fm.CertifyLength.readOnly = false;
            }
        }
        else
        {
            fm.CertifyLength.value = 18;
            fm.CertifyLength.readOnly = true;
        }
    }
    else if (cName == 'DutyUinit'){
        if (fm.DutyUinit.value == 'B'){
            fm.DutyPer.value = '-1';
            fm.DutyPer.readOnly = true;
        }else{
            fm.DutyPer.readOnly = false;
        }
    }
    else if (cName == "RiskWrapCode"){
        var riskWrapCode = CardRiskGrid.getRowColDataByName(0, "RiskCode");
        var strSQL = "";//查询险种、套餐后自动带出保额、保费、档次
        if(fm.Operate.value == "0")//卡单
        {
            strSQL = "select RiskWrapCode, "
                + "(select nvl(sum(double(b.calFactorValue)), 0) from LDRiskDutyWrap b where a.RiskWrapCode = b.RiskWrapCode and b.CalFactor = 'Prem' and b.CalFactorType = '1'), "
                + "(select nvl(sum(double(b.calFactorValue)), 0) from LDRiskDutyWrap b where a.RiskWrapCode = b.RiskWrapCode and b.CalFactor = 'Amnt' and b.CalFactorType = '1'), "
                + "(select nvl(sum(double(b.calFactorValue)), 0) from LDRiskDutyWrap b where a.RiskWrapCode = b.RiskWrapCode and b.CalFactor = 'Mult' and b.CalFactorType = '1'), "
                + "'W' from LDWrap a where RiskWrapCode = '" + riskWrapCode + "'";
        }
        else if(fm.Operate.value == "1")//撕单
        {
            strSQL = "select RiskWrapCode, "
                + "(select nvl(sum(double(b.calFactorValue)), 0) from LDRiskDutyWrap b where a.RiskWrapCode = b.RiskWrapCode and b.CalFactor = 'Prem'), "
                + "(select nvl(sum(double(b.calFactorValue)), 0) from LDRiskDutyWrap b where a.RiskWrapCode = b.RiskWrapCode and b.CalFactor = 'Amnt'), "
                + "(select nvl(sum(double(b.calFactorValue)), 0) from LDRiskDutyWrap b where a.RiskWrapCode = b.RiskWrapCode and b.CalFactor = 'Mult'), "
                + "'W' from LDWrap a where RiskWrapCode = '" + riskWrapCode + "'";
        }
        else
        {
            alert("请先选择单证类型！");
            return;
        }
        turnPage.queryModal(strSQL, CardRiskGrid);
        if(CardRiskGrid.mulLineCount == 0)
            initCardRiskGrid();
    }
}

//提交，保存按钮对应操作
function submitForm()
{
  if((fm.CertifyCode.value=="")||(fm.CertifyCode.value=="null"))
  {
    alert("请您录入单证编码！！！");
    return ;
  }
  if((fm.CertifyName.value=="")||(fm.CertifyName.value=="null"))
  {
    alert("请您录入单证名称！！！");
    return ;
  }
  if((fm.CertifyClass.value=="")||(fm.CertifyClass.value=="null"))
  {
    alert("请您录入单证类型！！！");
    return ;
  }
  if((fm.VerifyFlag.value=="")||(fm.VerifyFlag.value=="null"))
  {
    alert("请您录入业务员校验标志！！！");
    return ;
  }
  if(fm.CertifyLength.value < 7)
  {
    alert("请您录入单证号码长度，并确认长度大于7！");
    return ;
  }
  if((fm.State.value=="")||(fm.State.value=="null"))
  {
    alert("请您录入单证状态！！！");
    return ;
  }
  if((fm.HaveNumber.value=="")||(fm.HaveNumber.value=="null"))
  {
    alert("请您录入是否是有号单证！！！");
    return ;
  }
  if((fm.CertifyClass.value=="D")&&(fm.DutyPer.value=="null" || fm.DutyPer.value==""))
  {
    alert("请您录入保险期间！！！");
    return ;
  }
  if((fm.CertifyClass.value=="D")&&(fm.DutyUinit.value=="null" || fm.DutyUinit.value==""))
  {
    alert("请您录入保险期间的单位！！！");
    return ;
  }
  if((fm.CertifyClass.value=="D")&&(fm.SubCode.value=="null" || fm.SubCode.value==""))
  {
    alert("请您录入两位单证类型码！！！");
    return ;
  }
  if((fm.CertifyClass.value=="D")&& (trim(fm.all('SubCode').value).length!=2))
  {
    alert( "请输入两位单证类型码");
    return ;
  }
  if((fm.ManageCom.value=="")||(fm.ManageCom.value=="null"))
  {
    alert("请您录入管理机构！！！");
    return ;
  }
  if((fm.CertifyClass.value=="D") && CardRiskGrid.mulLineCount == 0){
  	alert("请您录入险种信息！！！");
    return ;
  }
  //校验单证类型码是否使用过
  if(!verifySubCode())
    return false;
  if(fm.CertifyClass.value=="D" && !verifyRisk())
    return false;

  fm.OperateType.value = "INSERT";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = './CertifyDescSave.jsp';
  fm.submit(); //提交
}

//校验单证类型码是否是生成单证号码或已印刷入库
function verifySubCode()
{
    //普通单证不校验单证类型码
    if(fm.CertifyClass.value == "P")
        return true;
    
    //单证类型码需为数字和大写字母
    if(!(/^[1-9 A-Z]/.test(fm.all('SubCode').value)))
    {
        alert("两位单证类型码需为数字和大写字母！");
        return false;
    }
    
    //校验单证类型码是否已描述
    var strSQL = "select 1 from LMCertifyDes where CertifyCode != '" 
        + fm.CertifyCode.value + "' and SubCode = '" + fm.SubCode.value + "'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {
        alert("该单证类型码已使用！");
        return false;
    }
    
    //09-4-27 注释掉关于印刷后无法关联套餐的校验
    //如果已印刷入库，不能修改单证描述
//    strSQL = "select 1 from LZCard where SubCode = '" + fm.SubCode.value + "' fetch first 1 rows only";
//    arrResult = easyExecSql(strSQL);
//    if(arrResult)
//    {
//        alert("该单证类型码单证已印刷，不能修改单证描述！");
//        return false;
//    }
    
    //如果该定额单证类型已生成单证，不能修改单证描述
    //strSQL = "select 1 from LZCardNumber where CardType = '" + fm.SubCode.value + "' fetch first 1 rows only";
    //arrResult = easyExecSql(strSQL);
    //if(arrResult)
   // {
   //     alert("该单证类型码已生成单证号，不能修改单证描述！");
   //     return false;
   // }
   return true;
}

//校验单证类型是否是生成单证号码或已印刷入库
function verifyCertifyCode()
{
    //如果已印刷入库，不能修改单证描述
    var strSQL = "select 1 from LZCard where CertifyCode = '" + fm.CertifyCode.value + "' fetch first 1 rows only";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {
        alert("该单证类型单证已印刷，不能修改单证描述！");
        return false;
    }
    
    //如果该定额单证类型已生成单证，不能修改单证描述
    //strSQL = "select 1 from LMCertifyDes where CertifyClass = 'D' and CertifyCode = '"
    //    + fm.CertifyCode.value
    //    + "' and exists (select 1 from LZCardNumber where CardType = SubCode)";
    //arrResult = easyExecSql(strSQL);
    //if(arrResult)
    //{
    //    alert("该单证类型已生成单证号，不能修改单证描述！");
    //    return false;
    //}
    return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    initForm();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initCardRiskGrid();
  }
  catch(re)
  {
    alert("初始化页面错误，重置出错");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,0,0,*";
  }
   else
   {
      parent.fraMain.rows = "0,0,0,0,*";
   }
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
    //校验单证类型是否已生成单证号或已印刷入库
    if(!verifyCertifyCode())
        return false;
    //校验单证类型码是否使用过
    if (!verifySubCode())
        return false;
    if(fm.CertifyClass.value=="D" && !verifyRisk())
        return false;
    
    if (confirm("您确实想修改该记录吗?"))
    {
        fm.OperateType.value = "UPDATE";
        var i = 0;
        var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        showSubmitFrame(mDebug);
        fm.action = './CertifyDescSave.jsp';
        
        fm.submit(); //提交
    }
    else
    {
        fm.OperateType.value = "";
        //alert("您取消了修改操作！");
    }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  fm.OperateType.value="QUERY";
  fm.CertifyCode_1.value = "";
  window.open("./FrameCertifyDescQuery.jsp");
}

function deleteClick()
{
  alert("您不能删除该记录，请将该单证置为失效！！！");
  return;
  /*if (confirm("您确实要删除该记录吗？"))
  {
    if(fm.CertifyCode.value!=fm.CertifyCode_1.value)
    {
      alert("单证号码进行了修改，您无法执行删除操作！！！");
      return;
    }
    fm.OperateType.value = "DELETE";
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或连接其他的界面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    showSubmitFrame(mDebug);
    fm.action = './CertifyDescSave.jsp';

    fm.submit();//提交
    resetForm();
  }
  else
  {
    fm.OperateType.value = "";
    alert("您已经取消了修改操作！");
  }*/
}


function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//校验单证类型码是否是生成单证号码或已印刷入库
function verifyRisk()
{
    if(CardRiskGrid.mulLineCount == 0)
    {
        alert("该单证类型码已生成单证号，不能修改单证描述！");
        return false;
    }
    if(CardRiskGrid.getRowColDataByName(0, "RiskCode") == "")
    {
        alert("险种或套餐编码不能为空！");
        return false;
    }
    if(CardRiskGrid.getRowColDataByName(0, "Prem") == "")
    {
        alert("保费不能为空！");
        return false;
    }
    if(CardRiskGrid.getRowColDataByName(0, "Amnt") == "" 
        && CardRiskGrid.getRowColDataByName(0, "Mult") == "")
    {
        alert("档次和保额不能同时为空！");
        return false;
    }
    return true;
}

function afterQueryCertifyInfo(certifyCode)
{
    var strSQL = "select CertifyCode, SubCode, CertifyName, CertifyClass, Note, State, ManageCom, "
        + "CertifyLength, HaveNumber, Unit, VerifyFlag, PolPeriod, PolPeriodFlag, CheckRule, OperateType "
        + "from LMCertifyDes where CertifyCode = '" + certifyCode + "'";
    var arr = easyExecSql(strSQL);
    if(arr)
    {
        fm.CertifyCode.readonly = true;
        fm.CertifyCode.value = arr[0][0];
        fm.CertifyName.value = arr[0][2];
        fm.CertifyClass.value = arr[0][3];
        if(fm.CertifyClass.value == "P")
            fm.CertifyClassName.value = "普通单证";
        else
            fm.CertifyClassName.value = "定额单证";
        fm.VerifyFlag.value = arr[0][10];
        if(fm.VerifyFlag.value == "Y")
            fm.VerifyFlagName.value = "校验";
        else
            fm.VerifyFlagName.value = "不校验";
        fm.State.value = arr[0][5];
        if(fm.State.value == "0")
            fm.StateName.value = "有效";
        else
            fm.StateName.value = "无效";
        fm.HaveNumber.value = arr[0][8];
        if(fm.HaveNumber.value == "Y")
            fm.HaveNumberName.value = "有号单证";
        else
            fm.HaveNumberName.value = "无号单证";
        fm.Unit.value = arr[0][9];
        fm.ManageCom.value = arr[0][6];
        if(fm.ManageCom.value == "A")
        {
            fm.StateRadio[1].checked = true;
            divComName.style.display = 'none';
            divComCode.style.display = 'none';
        }
        else
        {
            fm.StateRadio[0].checked = true;
            divComName.style.display = '';
            divComCode.style.display = '';
            fm.ManageComName.value = easyExecSql(
                "select Name from LDCOM where ComCode = '" + fm.ManageCom.value + "'")[0][0];
        }
        fm.Note.value = arr[0][4];
        if(fm.CertifyClass.value == "D")
        {
            fm.CertifyLength.value = 2 + parseFloat(arr[0][7]);
            fm.all("divCardRisk").style.display = "";
            fm.all("divShow").style.display = "";
            fm.DutyPer.value = arr[0][11];
            fm.DutyUinit.value = arr[0][12];
            if(fm.DutyUinit.value == "Y")
                fm.DutyUinitName.value = "年";
            else if(fm.DutyUinit.value == "M")
                fm.DutyUinitName.value = "月";
            else if(fm.DutyUinit.value == "D")
                fm.DutyUinitName.value = "日";
            else
                fm.DutyUinitName.value = "不定期";
            fm.CheckRule.value = arr[0][13];
            if(fm.CheckRule.value == "1")
                fm.ChkRuleName.value = "有校验";
            else
                fm.ChkRuleName.value = "无校验";
            fm.SubCode.value = arr[0][1];
            fm.Operate.value = arr[0][14];
            if(fm.Operate.value == "0")
                fm.CardTypeName.value = "卡单业务";
            else
                fm.CardTypeName.value = "撕单业务";
            //fm..value = arr[0][];
            strSQL = "select RiskCode, Prem, PremLot, Mult, RiskType "
                + "from LMCardRisk where CertifyCode = '" + certifyCode + "'";
            turnPage.queryModal(strSQL, CardRiskGrid);
            if(CardRiskGrid.mulLineCount == 0)
                initCardRiskGrid();
        }
        else
        {
            fm.CertifyLength.value = arr[0][7];
            fm.all("divCardRisk").style.display = "none";
            fm.all("divShow").style.display = "none";
        }
    }
}