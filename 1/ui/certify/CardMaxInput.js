var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

function afterSubmit( FlagStr, content )
{
    if (FlagStr == "Fail" )
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    initInputBox();
    queryInfo();
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
        parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}

//双击事件后的默认相应事件
function afterCodeSelect(codeName,Field)
{
	if(codeName == "CertifyClassList")
	{
	    if(Field.value == "D")
	    {
	        cardCode = "CardCode";
	    }
	    else if(Field.value == "P")
	    {
	        cardCode = "CertifyCode";
	    }
	}
	if(codeName == "AgentGradeList")
	{
	    if(Field.value == "0" && manageCom.length <= 4)
	    {
	        receiveType = " and AgentCode = 'Y' ";
	    }
	    else if(Field.value == "1" && manageCom.length <= 4)
	    {
	        receiveType = " and AgentGrade = 'Y' ";
	    }
	    else if(Field.value == "2" && manageCom.length <= 4)
	    {
	        receiveType = " and AgentCom = 'Y' ";
	    }
	    else if(Field.value == "3" && manageCom.length == 2)
	    {
	        receiveType = " and a.AgentCode = '*' and a.CertifyCode = '*' and a.AgentGrade = '*' and a.ManageCom = '*' ";
	    }
	    else
	    {
	        receiveType = " and 1 = 2 ";
	    }
	}
}

function queryInfo()
{
    if(fm.AgentGrade.value == null || fm.AgentGrade.value == "")
    {
        alert("请选择发放对象！");
        return;
    }
    if(fm.AgentGrade.value != "3" && (fm.CertifyClass.value == null || fm.CertifyClass.value == ""))
    {
        alert("请选择单证类型！");
        return;
    }
	var sql = "select a.CertifyCode, b.CertifyName, MaxCount, VerPeriod "
	    + "from LDAgentCardCount a left join LMCertifyDes b on a.CertifyCode = b.CertifyCode where 1=1 " 
	    + receiveType
	    + getWherePart("a.CertifyCode", "CertifyCode")
	    + getWherePart("b.CertifyClass", "CertifyClass")
	    + " order by CertifyCode"
	    ;

	turnPage.strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    turnPage.queryModal(sql, CertifyMaxGrid);

    return true;
}

//显示单证发放数量配置
function showCertifyMaxInfo()
{
    var sel = CertifyMaxGrid.getSelNo();
    if(sel == 0 || sel == null)
    {
        alert( "请先选择一条记录。" );
        return false;
    }
	fmSave.MaxCount.value = CertifyMaxGrid.getRowColDataByName(sel - 1, "MaxCount");
	fmSave.VerPeriod.value = CertifyMaxGrid.getRowColDataByName(sel - 1, "VerPeriod");
	fmSave.CertifyCode.value = CertifyMaxGrid.getRowColDataByName(sel - 1, "CertifyCode");
	fmSave.CertifyName.value = CertifyMaxGrid.getRowColDataByName(sel - 1, "CertifyName");
	fmSave.AgentGrade.value = fm.AgentGrade.value;
	fmSave.AgentGradeName.value = fm.AgentGradeName.value;
	fmSave.CertifyClass.value = fm.CertifyClass.value;
	fmSave.CertifyClassName.value = fm.CertifyClassName.value;

    return true;
}

//新增单证发放数量配置
function insertForm()
{
    if(!checkDate())
    {
        return ;
    }
    var strSql = "select 1 from LDAgentCardCount where CertifyCode = '" + fmSave.CertifyCode.value + "'" + receiveType;
    var arrResult = easyExecSql(strSql);
    if (arrResult)
    {
        alert("已有该单证类型" + fmSave.AgentGradeName.value + "的数量配置信息，请点击修改！");
        return false;
    }
    //alert();return false;
    fmSave.Operate.value = "INSERT";
    fmSave.action = "./CardMaxSave.jsp";
    fmSave.submit();
}

//修改单证发放数量配置
function modifyForm()
{
    if(!checkDate())
    {
        return ;
    }
    var strSql = "select 1 from LDAgentCardCount where CertifyCode = '" + fmSave.CertifyCode.value + "'" + receiveType;
    var arrResult = easyExecSql(strSql);
    if (!arrResult)
    {
        alert("没有该单证类型" + fmSave.AgentGradeName.value + "的数量配置信息，请点击保存！");
        return false;
    }
    //alert();return false;
    fmSave.Operate.value = "UPDATE";
    fmSave.action = "./CardMaxSave.jsp";
    fmSave.submit();
}

//删除单证发放数量配置
function deleteForm()
{
    if(!checkDate())
    {
        return ;
    }
    var strSql = "select 1 from LDAgentCardCount where CertifyCode = '" + fmSave.CertifyCode.value + "'" + receiveType;
    var arrResult = easyExecSql(strSql);
    if (!arrResult)
    {
        alert("没有该单证类型" + fmSave.AgentGradeName.value + "的数量配置信息，请确认！");
        return false;
    }
    //alert();return false;
    fmSave.Operate.value = "DELETE";
    fmSave.action = "./CardMaxSave.jsp";
    fmSave.submit();
}

//校验数据
function checkDate()
{
    if(fmSave.MaxCount.value > 200000 || fmSave.MaxCount.value < 0)
    {
        alert("可配置的最大数量值范围为[0-200000]!");
        return false;
    }
    if(manageCom.length > 4)
    {
        alert("只有总公司和分公司有单证发放数量配置权限！");
        return false;
    }
    if(fmSave.MaxCount.value == null || fmSave.MaxCount.value == "")
    {
        alert("机构代码为空！");
        return false;
    }
    if(fmSave.AgentGrade.value == null || fmSave.AgentGrade.value == "")
    {
        alert("单证类型为空！");
        return false;
    }
    return true;
}