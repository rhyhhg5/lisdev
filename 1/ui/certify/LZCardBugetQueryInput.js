/** 
 * 程序名称：LZCardBugetInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2004-04-05 16:08:27
 * 创建人  ：yangtao
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {

	var strSql = "select * from LZCardBuget where 1=1 "
    + getWherePart('ManageCom','ManageCom','like','0')
    + getWherePart('SDate')
    + getWherePart('EDate')
    + getWherePart('Buget')
    + getWherePart('MakeDate')
    + getWherePart('CertifyCode')
  ;
    
    strSql=strSql+" and ManageCom like '"+comCode+"%25'";
    //alert(strSql);
	turnPage.queryModal(strSql, LZCardBugetGrid);


}
function showOne(parm1, parm2) {	

}
// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = LZCardBugetGrid.getSelNo();
	//alert(tSel);
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
	
			arrReturn = getQueryResult();
            top.opener.afterQuery( arrReturn );
			top.close();
		}
		catch(ex)
		{
			alert( "请先选择一条非空记录，再点击返回按钮。");
			//alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		
	}
}
function getQueryResult()
{
	
	var arrSelected = null;
	var tRow = LZCardBugetGrid.getSelNo();
    if( tRow == 0 || tRow == null)
	  return arrSelected;
	arrSelected = new Array();
	arrSelected[0] = new Array();
    arrSelected[0] = LZCardBugetGrid.getRowData(tRow-1);
    return arrSelected;
}

function printPlan(){
    var sql = "select * from LZCardBuget where 1=1 "
    + getWherePart('ManageCom','ManageCom','like','0')
    + getWherePart('SDate')
    + getWherePart('EDate')
    + getWherePart('Buget')
    + getWherePart('MakeDate')
    + getWherePart('CertifyCode');
    sql=sql+" and ManageCom like '"+comCode+"%25'";
    sql=replaceStrSql(sql);
    fm.Strsql.value=sql;
    //alert(sql);
	fm.action = "./LZCardBugetQueryPrint.jsp";
	fm.target = "_blank";
    fm.submit();

}