
function submitForm()
{  
    if(fm.oldPwd.value.length == 0){
		alert("请填写原密码.");
		return false;
	}
	
	var newPwd = fm.newPwd.value;
	var confirmPwd = fm.confirmPwd.value;
    var oldPwd = fm.oldPwd.value;
	if (newPwd == "") {
	    alert("请输入新密码.");
	    return false;
	}
	
	if (newPwd != confirmPwd){
		alert("新密码与确认密码输入不一致.");
		fm.confirmPwd.value = "";
		fm.newPwd.value = "";
		return false;
	}
    
    if(oldPwd == newPwd) {
        alert("新密码与原密码不能相同。");
        fm.confirmPwd.value = "";
        fm.newPwd.value = "";
        return false;  
    }
    
	//增加密码校验功能 Added By Qisl at 2011.06.20
	if(!checkPasswd(newPwd)){
		return false;
	}
	
	fm.submit();
	return true;
}


function afterSubmit(FlagStr) 
{
	if (FlagStr == "false") {
		alert("密码更改失败，可能的原因是原密码输入有误。");
	} else {
		alert("密码更新成功！");		
	}
}

function resetForm()
{
	fm.oldPwd.value = "";
	fm.newPwd.value = "";
	fm.confirmPwd.value = "";
}

/**
 * 校验密码是否符合要求 
 * Added For Check Password by Qisl at 2011.06.20
 * @param Passwd
 * @return
 */
function checkPasswd(Passwd){
    if(Passwd.length != 8){
		alert("设置的用户密码必须为8位!");
		return false;
	}
	
	var pattern=/(\W*[A-Za-z]+\W*\d+\W*|\W*\d+\W*[A-Za-z]+\W*)/;
	if(!(pattern.exec(Passwd)))
	 {
	 	alert("您要设置的用户密码必须同时包含有字母和数字！");
	 	return false;
	 }
	return true;
}