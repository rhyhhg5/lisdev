<%@ page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
</head>
<%
 //******************************************************
 // 程序名称：LogonSubmit.jsp
 // 程序功能:：处理用户登录提交
 // 最近更新人：DingZhong
 // 最近更新日期：2002-10-15
 //******************************************************
 %>
 
<%@page import="java.util.Vector"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.menumang.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.encrypt.*"%>
<%@page import="com.sinosoft.lis.logon.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%                
	boolean bSuccess = false;
  String userInValiDate = "";
  String tLogNo = "";
  	String userCheckIp = "";
	//用户名称和密码
	String UserCode = request.getParameter("UserCode");
	System.out.println("************+" + UserCode + "+*********");
	
	String url = request.getRequestURL().toString();
	
	String testIP= request.getServerName();
	
	System.out.println("testIP:"+testIP);

	System.out.println("url::"+url);
	String Password = request.getParameter("PWD");
        
        LisIDEA tIdea = new LisIDEA();
        Password = tIdea.encryptString(Password);
	
	String StationCode = request.getParameter("StationCode");
	
	String ClientURL = request.getParameter("ClientURL");	//LQ 2004-04-19
	
//	System.out.println("Password:" + Password + "1");

	
	//用户IP
	//String Ip = request.getRemoteAddr();
	String Ip = request.getHeader("X-Forwarded-For");
		if(Ip == null || Ip.length() == 0) { 
		   Ip = request.getRemoteAddr(); 
		}
	//10.128.117.60
	String Crs_IP = new ExeSQL().getOneValue("select Sysvarvalue from LDSYSVAR WHERE sysvar ='Crs_IP'");
	if (!Crs_IP.equals(testIP))
	{	
	System.out.println("++++++++++++++++++IP :" + Ip);
	String ls;	//返回的字符串
  LDUserUI tLDUserUI = new LDUserUI();
	//通过用户编码获得本机IP
	String ip2 = new ExeSQL().getOneValue("select cientip from lduserlog where 1=1 and makedate = current date and logtype = 'S' and cientip not in '"+Ip+"' and operator = '"+UserCode+"' order by makedate,maketime desc fetch first 1 rows only ");
	System.out.println("ip2:"+ip2);
	if (Password.length() == 0 || UserCode.length() == 0) {
	    bSuccess = false;
	} 
	else  {
	    //判断是理赔用户，才进行校验
		//#3341 同一个用户不能同时在多个（包括两个）不同的IP地址上登陆的系统控制
		String sql222 = "select * from ldcode where codetype='ip_lduser' and code = '1' ";
		String tLDCode = new ExeSQL().getOneValue(sql222);
		System.out.println("tLDCode："+tLDCode);
		if(tLDCode.length() > 0){
			String sql111 = "select a.* "
					+ "from llclaimuser a "
					+ "where a.usercode = '" + UserCode + "' and not exists (select 1 from ldcode where code=a.usercode and codetype='ip_lduser_log') ";
			System.out.println("11111111"+sql111);
			LLClaimUserDB tLLClaimUserDB =new LLClaimUserDB();
			LLClaimUserSet set111 = tLLClaimUserDB.executeQuery(sql111);
			if(set111.size() == 1){        			
				//当某理赔用户登录时，查看该理赔用户上次是否在其他IP的电脑上已经登出系统。若未登出，则需提示
				System.out.println("ip2:"+ip2);
				System.out.println("Ip:"+Ip);
				if("".equals(ip2)||ip2==null){
					ip2 = Ip;//获得本机IP
				}
			  	if(!Ip.equals(ip2)){              	  		           	  		
			  		String quittime = new ExeSQL().getOneValue("select quittime from lduserlog where 1=1 and makedate = current date and logtype = 'S' and operator = '"+UserCode+"' and cientip = '"+ip2+"' order by makedate,maketime desc fetch first 1 rows only ");           	  		
			  		System.out.println("quittime:"+quittime.length());
			  		if("".equals(quittime)||quittime==null){         	 		
			  			System.out.println("该用户在IP为"+ip2+"的电脑上尚未登出，您暂时不能登录，请知悉。");
			  			bSuccess = false;
			  			userCheckIp = "ipconfig";
			  		}
			  	}
			} 	
		}
		
	if(!"ipconfig".equals(userCheckIp)){
	    VData tVData = new VData();
	    LDUserSchema tLDUserSchema = new LDUserSchema();
	
	    tLDUserSchema.setUserCode(UserCode);
            tLDUserSchema.setPassword(Password);
            tLDUserSchema.setComCode(StationCode);
            tLDUserSchema.setUserDescription(Ip); //090707 借用该字段传IP地址
	    tVData.add(tLDUserSchema);
      bSuccess=tLDUserUI.submitData(tVData,"query");
	}
		
      //校验用户是否失效
      if(bSuccess)
      {
        String currentDate = PubFun.getCurrentDate();
        String sql = "  select * "
                     + "from LDUser "
                     + "where userCode = '" + UserCode + "' "
                     + "   and (validEndDate is null "
                     + "      or date('" + currentDate + "') < validEndDate) ";
        System.out.println(sql);
        LDUserDB tLDUserDB = new LDUserDB();
        LDUserSet set = tLDUserDB.executeQuery(sql);
        if(set.size() == 0)
        {
          bSuccess = false;
          userInValiDate = "inValiDate";
        }
      }
    }
  VData tResult = tLDUserUI.getResult();
  LDUserSet tLDUserSet = null;
  if(tResult != null){
  	tLDUserSet = (LDUserSet)tResult.getObjectByObjectName("LDUserSet",0);
  	tLogNo = (String)tResult.getObjectByObjectName("String", 0);
  }
	String title=UserCode+"您好，欢迎登录本系统！"; 
	GlobalInput tG = new GlobalInput();
	tG.Operator = UserCode;
	tG.ComCode  = StationCode;
	tG.ManageCom = StationCode;
	if(tLDUserSet != null && tLDUserSet.size()>0 && tLDUserSet.get(1).getAgentCom() != null){
		tG.AgentCom = tLDUserSet.get(1).getAgentCom();
		System.out.println("当前登录用户为代理机构用户,代理机构为："+tG.AgentCom);
		String strSql = "select count(1) from lacom where AgentCom='"+tG.AgentCom+"' and state='0'";
		int chk = Integer.parseInt((new ExeSQL()).getOneValue(strSql));
		if(chk >0){
			bSuccess = false;
      userInValiDate = "inValiDate";
		}
	}

 	session.putValue("GI",tG);
 	session.putValue("LogNo", tLogNo);
 	session.putValue("ClientURL",ClientURL);	//LQ 2004-04-19
  	GlobalInput tG1 = new GlobalInput();
	tG1=(GlobalInput)session.getValue("GI");
//	System.out.println("Current Operate is "+tG1.Operator);
 //   System.out.println("Current ComCode is "+tG1.ComCode);  
    if(bSuccess == true) {
    
       //register info into lduserlog   
       //注掉日志更功能,写入后台
//       LDUserLogDB tLDUserLogDB = new LDUserLogDB();
//       String maxno = String.valueOf((new java.util.Date()).getTime());
//       tLDUserLogDB.setLogNo(maxno);
//       tLDUserLogDB.setManageCom(StationCode);
//       tLDUserLogDB.setOperator(UserCode);
//       tLDUserLogDB.setCientIP(Ip);
//       tLDUserLogDB.setMakeDate(PubFun.getCurrentDate());
//       tLDUserLogDB.setMakeTime(PubFun.getCurrentTime());
//       
//       tLDUserLogDB.insert();
       
	   out.print(title);
	   //进行解锁操作
	   System.out.println("start unlock operate...");
       VData inputData = new VData();
       inputData.addElement(tG1);
       logoutUI tlogoutUI = new logoutUI(); 
       tlogoutUI.submitData(inputData,"LogOutProcess");
       System.out.println("completed clear data");	   
       String Claim = "";
       LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
       tLLClaimUserDB.setUserCode(UserCode);
       if(tLLClaimUserDB.getInfo()){
       		String strCheckSql = "select count(1) from lduser x "
				+"where x.usercode='"+UserCode+"' "
				+"and exists( "
				+"select 1 from ldmenu a,LDMenuGrpToMenu b,LDUserToMenuGrp c "
				+"where a.runscript like '%LLClaimMainNewList.jsp%' " 
				+"and a.nodecode=b.nodecode "
				+"and b.menugrpcode=c.menugrpcode "
				+"and c.usercode=x.usercode) with ur";
			int chk = Integer.parseInt((new ExeSQL()).getOneValue(strCheckSql));
			if(chk > 0){
				Claim = "newClaim";
			}else{
				Claim = "oldClaim";
			}
			
       }
        
%>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <script language=javascript>
    	if(parent.fraMain.rows == "0,0,0,0,*")
    		parent.document.frames('fraTitle').showTitle();
    	if(parent.fraSet.cols==	"0%,*,0%")
      		parent.document.frames('fraTitle').showHideFrame();
      parent.fraMenu.window.location="./menu2.jsp?userCode=<%=UserCode%>&Ip=<%=Ip%>";	
      var UserCode=	"<%=UserCode%>";
      var Claim = "<%=Claim%>";
      if(Claim=="oldClaim"){
      		parent.fraInterface.window.location.href="../case/LLClaimMainList.jsp";	//转换连接，比直接用a的方式快一些      		
      }else if(Claim=="newClaim"){
      		parent.fraInterface.window.location.href="../case/LLClaimMainNewList.jsp";	//转换连接，比直接用a的方式快一些     		
      }
      	
      		
    </script>
<%
  
  }
  else
  {
    session.putValue("GI",null);  
%>
    <script language=javascript>
    	var ip22 = "<%=ip2%>";
    	var userCheckIp1 = "<%=userCheckIp%>";
        if("<%=userInValiDate%>" != "")
        {
          alert("用户已失效");
        } else if(userCheckIp1 != ""){
        	alert("该用户在IP为"+ip22+"的电脑上尚未登出，您暂时不能登录，请知悉。");
        }
        else
        {
          alert("用户名/密码/管理机构输入不正确!");
        }
        var tIllFlag = "<%=request.getParameter("IllFlag")%>";
        if(tIllFlag == "dd"){
        	parent.window.location ="../indexdd.jsp";
        }else{
        	parent.window.location ="../indexlis.jsp";
        } 
    </script>
<%

  }
	}else
	{
%>
		<script language=javascript>
		alert("对不起，你没有权限访问！");
		parent.window.location ="pageError.gif";
		</script>
<%		
	}
%> 
<body onload="">
   
   <p>
    <table><tr>
    <td>
    <font size="4"><font color="#ff0000">系统更新公告：</font>
    </td></tr></table>
    <div>
        <center><iframe src="../whatsnew.xml" frameborder="no" scrolling="yes" height="90%" width="100%"></iframe></center>
    </div>
</body>
</html>