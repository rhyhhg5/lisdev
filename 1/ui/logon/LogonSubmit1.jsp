<%@ page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
</head>
<%
 //******************************************************
 // 程序名称：LogonSubmit.jsp
 // 程序功能:：处理用户登录提交
 // 最近更新人：DingZhong
 // 最近更新日期：2002-10-15
 //******************************************************
 %>
 
<%@page import="java.util.Vector"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.menumang.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.encrypt.*"%>
<%@page import="com.sinosoft.lis.logon.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%                
	boolean bSuccess = false;
  String userInValiDate = "";
	//用户名称和密码
	String UserCode = request.getParameter("UserCode");
	System.out.println("************+" + UserCode + "+*********");
	
	String Password = request.getParameter("PWD");
        
        LisIDEA tIdea = new LisIDEA();
        Password = tIdea.encryptString(Password);
	
	String StationCode = request.getParameter("StationCode");
	
	String ClientURL = request.getParameter("ClientURL");	//LQ 2004-04-19
	
//	System.out.println("Password:" + Password + "1");

	
	//用户IP
	//String Ip = request.getRemoteAddr();
	String Ip = request.getHeader("X-Forwarded-For");
		if(Ip == null || Ip.length() == 0) { 
		   Ip = request.getRemoteAddr(); 
		}
	System.out.println("++++++++++++++++++IP :" + Ip);
	String ls;	//返回的字符串
  LDUserUI tLDUserUI = new LDUserUI();
	if (Password.length() == 0 || UserCode.length() == 0) {
	    bSuccess = false;
	} else {
	    VData tVData = new VData();
	    LDUserSchema tLDUserSchema = new LDUserSchema();
	
	    tLDUserSchema.setUserCode(UserCode);
            tLDUserSchema.setPassword(Password);
            tLDUserSchema.setComCode(StationCode);
            tLDUserSchema.setUserDescription(Ip); //090707 借用该字段传IP地址
	    tVData.add(tLDUserSchema);
      bSuccess=tLDUserUI.submitData(tVData,"query");
      
      //校验用户是否失效
      if(bSuccess)
      {
        String currentDate = PubFun.getCurrentDate();
        String sql = "  select * "
                     + "from LDUser "
                     + "where userCode = '" + UserCode + "' "
                     + "   and (validEndDate is null "
                     + "      or date('" + currentDate + "') < validEndDate) ";
        System.out.println(sql);
        LDUserDB tLDUserDB = new LDUserDB();
        LDUserSet set = tLDUserDB.executeQuery(sql);
        if(set.size() == 0)
        {
          bSuccess = false;
          userInValiDate = "inValiDate";
        }
      }
    }
  VData tResult = tLDUserUI.getResult();
  LDUserSet tLDUserSet = null;
  if(tResult != null){
  	tLDUserSet = (LDUserSet)tResult.getObjectByObjectName("LDUserSet",0);
  }
	String title=UserCode+"您好，欢迎登录本系统！"; 
	GlobalInput tG = new GlobalInput();
	tG.Operator = UserCode;
	tG.ComCode  = StationCode;
	tG.ManageCom = StationCode;
	if(tLDUserSet != null && tLDUserSet.size()>0 && tLDUserSet.get(1).getAgentCom() != null){
		tG.AgentCom = tLDUserSet.get(1).getAgentCom();
		System.out.println("当前登录用户为代理机构用户,代理机构为："+tG.AgentCom);
		String strSql = "select count(1) from lacom where AgentCom='"+tG.AgentCom+"' and state='0'";
		int chk = Integer.parseInt((new ExeSQL()).getOneValue(strSql));
		if(chk >0){
			bSuccess = false;
      userInValiDate = "inValiDate";
		}
	}
 	session.putValue("GI",tG);
 	session.putValue("ClientURL",ClientURL);	//LQ 2004-04-19
  	GlobalInput tG1 = new GlobalInput();
	tG1=(GlobalInput)session.getValue("GI");
//	System.out.println("Current Operate is "+tG1.Operator);
 //   System.out.println("Current ComCode is "+tG1.ComCode);  
    if(bSuccess == true) {
    
       //register info into lduserlog   
       //注掉日志更功能,写入后台
//       LDUserLogDB tLDUserLogDB = new LDUserLogDB();
//       String maxno = String.valueOf((new java.util.Date()).getTime());
//       tLDUserLogDB.setLogNo(maxno);
//       tLDUserLogDB.setManageCom(StationCode);
//       tLDUserLogDB.setOperator(UserCode);
//       tLDUserLogDB.setCientIP(Ip);
//       tLDUserLogDB.setMakeDate(PubFun.getCurrentDate());
//       tLDUserLogDB.setMakeTime(PubFun.getCurrentTime());
//       
//       tLDUserLogDB.insert();
       
	   out.print(title);
	   //进行解锁操作
	   System.out.println("start unlock operate...");
       VData inputData = new VData();
       inputData.addElement(tG1);
       logoutUI tlogoutUI = new logoutUI(); 
       tlogoutUI.submitData(inputData,"LogOutProcess");
       System.out.println("completed clear data");	   
       String Claim = "false";
       LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
       tLLClaimUserDB.setUserCode(UserCode);
       if(tLLClaimUserDB.getInfo())
        Claim = "true";
%>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <script language=javascript>
    	if(parent.fraMain.rows == "0,0,0,0,*")
    		parent.document.frames('fraTitle').showTitle();
    	if(parent.fraSet.cols==	"0%,*,0%")
      		parent.document.frames('fraTitle').showHideFrame();
      parent.fraMenu.window.location="./menu3.jsp?userCode=<%=UserCode%>&Ip=<%=Ip%>";	
      var UserCode=	"<%=UserCode%>";
      var Claim = "<%=Claim%>";
      if(Claim=="true")
      	parent.fraInterface.window.location.href="../case/LLClaimMainList.jsp";	//转换连接，比直接用a的方式快一些

      		
    </script>
<%
  
  }
  else
  {
    session.putValue("GI",null);  
%>
    <script language=javascript>
        if("<%=userInValiDate%>" != "")
        {
          alert("用户已失效");
        }
        else
        {
          alert("用户名/密码/管理机构输入不正确!");
        }
    	parent.window.location ="../indexlis.jsp";
    </script>
<%

  }
%> 
<body onload="">
   
   <p>
    <table><tr>
    <td>
    <font size="4"><font color="#ff0000">系统更新公告：</font>
    </td></tr></table>
    <div>
        <center><iframe src="../whatsnew.xml" frameborder="no" scrolling="yes" height="90%" width="100%"></iframe></center>
    </div>
</body>
</html>