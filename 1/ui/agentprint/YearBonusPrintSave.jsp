<%
//程序名称：BankPremPaySave.jsp
//程序功能：F1报表生成
//创建日期：2009-03-04
//创建人  ：XX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>


<%

 
boolean operFlag = true;
String flag = "0";
String FlagStr = "";
String Content = "";

CError cError = new CError( );
CErrors tError = null;
XmlExport txmlExport = new XmlExport();
CombineVts tcombineVts = null;

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//String tManageCom=tG.ManageCom;

String ManageCom = request.getParameter("ManageCom");

String tBranchAttr = request.getParameter("BranchAttr");
String tAgentGroup = request.getParameter("AgentGroup1");
String tAgentCode = request.getParameter("AgentCode");
String tType = request.getParameter("Type");
String tWageNo = request.getParameter("WageNo");
String tStartCalNo = request.getParameter("StartCalNo");
String tEndCalNo = request.getParameter("EndCalNo");
String tAgentName = request.getParameter("AgentName ");
System.out.println("cone here =======qwewewe==========="+tAgentGroup);
TransferData tTransferData= new TransferData();
tTransferData.setNameAndValue("tManageCom",ManageCom);
tTransferData.setNameAndValue("tBranchAttr",tBranchAttr);
tTransferData.setNameAndValue("tAgentGroup",tAgentGroup);
tTransferData.setNameAndValue("tAgentCode",tAgentCode); 
tTransferData.setNameAndValue("tType",tType);
tTransferData.setNameAndValue("tWageNo",tWageNo); 
tTransferData.setNameAndValue("tStartCalNo",tStartCalNo); 
tTransferData.setNameAndValue("tEndCalNo",tEndCalNo); 
tTransferData.setNameAndValue("tAgentName",tAgentName); 
VData tVData = new VData();
tVData.addElement(tG);
tVData.addElement(tTransferData);
          
YearBonusPrintUI tYearBonusPrintUI = new YearBonusPrintUI(); 
    
try{

    if(!tYearBonusPrintUI.submitData(tVData,"PRINT"))
    {
       operFlag = false;
       Content = tYearBonusPrintUI.mErrors.getFirstError();  
       System.out.println(Content);
    }
    
    VData mResult = tYearBonusPrintUI.getResult();
    System.out.println("=======qwewewe===========");	
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
	  System.out.println("null");
    }
    System.out.println("开始打开报表!");
    ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath +"//"+ strVFFileName;

    //合并VTS文件
   String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
   tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);

   ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
   tcombineVts.output(dataStream);

   //把dataStream存储到磁盘文件
   AccessVtsFile.saveToFile(dataStream,strVFPathName);
   System.out.println("==> Write VTS file to disk ");
   System.out.println("===strVFFileName : "+strVFFileName);
   //本来打算采用get方式来传递文件路径
   response.sendRedirect("../uw/GetF1PrintJ1.jsp?Code=07&RealPath="+strVFPathName);
}
catch(Exception ex)
{
    Content = "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
    tError = tYearBonusPrintUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 处理失败! ";
      FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    } 
}
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>