<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="TeamOrder.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="TeamOrderInit.jsp"%>
  <title>个人业务营业组竞赛排行榜 </title>   
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      	  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno"  name=ManageCom readonly onchange="getComtype(Managecomtype)"
            verify="管理机构|code:comcode&NOTNULL" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
            ><input class=codename name=ManageComName readonly=true > 
          </TD>
          <TD class= title>
          组别
          </TD>
          <TD  class= input width="25%">
            <Input class="codeno" name=Managecomtype 
            ondblclick="return showCodeList('managecomtype',[this,ManagecomtypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('managecomtype',[this,ManagecomtypeName],[0,1],null,null,null,1);"><input class=codename name=ManagecomtypeName readonly=true > 
          </TD> 
          </TR>          
          <TR>
          <TD  class= title width="25%">
            竞赛起期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=StartDay verify="竞赛起期|NOTNULL">
          </TD>      
          <TD  class= title width="25%">
            统计日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay  verify="统计日期|NOTNULL">
          </TD>  
        </TR>    
        <TR class= common>
        <TD class= title>
          入围标准（元）
          </TD>
          <TD  class= input >
            <Input class= common name=FYC verify="入围标准|NOTNULL">
          </TD> 
		<TD  class= input >
			<input class= common type=Button value="打印报表" onclick="PolPrint();">
		</TD>			
	</TR>    	 
      </table>    
      <input type=hidden id="Operate" name="Operate">
      
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
