<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {  
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('BranchAttr').value = '';
  
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
  
    fm.all('BranchType').value =  '1';
    fm.all('BranchType2').value = '01';     
                           
  }
  catch(ex)
  {
    alert("在LAFYCRCQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSetGridF()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
    iArray[1][0]="管理机构";         //列名
    iArray[1][1]="20px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         //列名
    iArray[2][1]="30px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[3]=new Array();
    iArray[3][0]="FYC(首年度佣金)";         //列名
    iArray[3][1]="20px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[4]=new Array();
    iArray[4][0]="续保佣金";         //列名
    iArray[4][1]="20px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[5]=new Array();
    iArray[5][0]="合计";         //列名
    iArray[5][1]="20px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许，0表示不允许       

        
    SetGridF = new MulLineEnter( "fm" , "SetGridF" );
    SetGridF.canChk = 0;
    SetGridF. hiddenPlus = 1;
    SetGridF.displayTitle = 1;
    SetGridF.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAFYCRCQueryInit.jsp-->initSetGridF函数中发生异常:初始化界面错误!");
  }
}                                       

function initSetGridS()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
    iArray[1][0]="管理机构";         //列名
    iArray[1][1]="20px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         //列名
    iArray[2][1]="30px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[3]=new Array();
    iArray[3][0]="销售团队代码";         //列名
    iArray[3][1]="30px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[4]=new Array();
    iArray[4][0]="销售团队名称";         //列名
    iArray[4][1]="50px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[5]=new Array();
    iArray[5][0]="FYC(首年度佣金)";         //列名
    iArray[5][1]="20px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[6]=new Array();
    iArray[6][0]="续保佣金";         //列名
    iArray[6][1]="20px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[7]=new Array();
    iArray[7][0]="合计";         //列名
    iArray[7][1]="20px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许，0表示不允许       

        
    SetGridS = new MulLineEnter( "fm" , "SetGridS" );
    SetGridS.canChk = 0;
    SetGridS. hiddenPlus = 1;
    SetGridS.displayTitle = 1;
    SetGridS.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAFYCRCQueryInit.jsp-->initSetGridS函数中发生异常:初始化界面错误!");
  }
}  


function initSetGridT()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
    iArray[1][0]="管理机构";         //列名
    iArray[1][1]="20px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         //列名
    iArray[2][1]="30px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[3]=new Array();
    iArray[3][0]="销售团队代码";         //列名
    iArray[3][1]="35px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[4]=new Array();
    iArray[4][0]="销售团队名称";         //列名
    iArray[4][1]="50px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[5]=new Array();
    iArray[5][0]="营销员编码";         //列名
    iArray[5][1]="30px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[6]=new Array();
    iArray[6][0]="营销员姓名";         //列名
    iArray[6][1]="30px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[7]=new Array();
    iArray[7][0]="FYC(首年度佣金)";         //列名
    iArray[7][1]="30px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[8]=new Array();
    iArray[8][0]="续保佣金";         //列名
    iArray[8][1]="25px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[9]=new Array();
    iArray[9][0]="合计";         //列名
    iArray[9][1]="20px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许，0表示不允许       
        
    SetGridT = new MulLineEnter( "fm" , "SetGridT" );
    SetGridT.canChk = 0;
    SetGridT. hiddenPlus = 1;
    SetGridT.displayTitle = 1;
    SetGridT.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAFYCRCQueryInit.jsp-->initSetGridT函数中发生异常:初始化界面错误!");
  }
}  

function initForm()
{
  try
  {
    initSetGridF();
    initInpBox();
  }
  catch(re)
  {
    alert("LAFYCRCQueryInitInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}



</script>
