<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
  	VData tVData = new VData();
	VData mResult = new VData();
    
    String tStartDate = request.getParameter("StartDate");
    String tEndDate = request.getParameter("EndDate");
    tVData.clear();
    tVData.addElement(tStartDate);
    tVData.addElement(tEndDate);
    tVData.addElement(tG);
    
	String strErrMsg = "";
	boolean Flag=true;	
	
    ManagerBusiReport tPrt = new ManagerBusiReport();

	if(!tPrt.getInputData(tVData)) {
		if( tPrt.mErrors.needDealError() ) {
			strErrMsg = tPrt.mErrors.getFirstError();
		} else {
			strErrMsg = "ManagerBusiReport发生错误，但是没有提供详细的出错信息";
		}
%>
		<script language="javascript">
			alert('<%= strErrMsg %>');
			window.opener = null;
			window.close();
		</script>
<%
		return;
  }
  if ( !tPrt.prepareData() ) {
    if (tPrt.mErrors.needDealError() )  {
      strErrMsg = tPrt.mErrors.getFirstError();
    }else  {
      strErrMsg = "ManagerBusiReport发生错误，但是没有提供详细的出错信息";
    }  
    System.out.println("--Error:" + strErrMsg);
    return;
  }    

  mResult = tPrt.getResult();
  
  XmlExport txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	
  if (txmlExport==null) {
	System.out.println("null");
  }
  
  session.putValue("PrintStream", txmlExport.getInputStream());
  System.out.println("put session value");
  response.sendRedirect("../f1print/GetF1Print.jsp");
%>