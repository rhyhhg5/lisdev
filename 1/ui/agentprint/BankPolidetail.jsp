<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="BankPolidetail.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BankPolidetailInit.jsp"%>
  <title>银代承保明细报表打印 </title>   
</head>
<body  onload="initForm();" >
  <form action="./BankPolidetail.jsp" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common> 
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
   
           <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
              <Input class="codeno" name=AgentCom ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null,null,null,1);"><input class=codename name=AgentComName readonly=true >         
 
                      </TD>    
        </TR>     
        
      	<TR  class= common>      
                   <td  class= title> 险种	</td>
                <td  class= input> <input name=RiskCode class="codeno" verify="险种|code:Riskcode" 
		         ondblclick="return showCodeList('riskcode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('riskcode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true > </td>
	   	<TD  class= title>
                              统计类型 
                        </TD>
            <TD  class= input>         
	    <Input class="codeno" name=tjtype  CodeData="0|^1|统计日期^2|签单日期^3|回单日期" ondblclick="showCodeListEx('tjtype',[this,tjtypeName],[0,1]);"  onkeyup="showCodeListKeyEx('tjtype',[this,tjtypeName],[0,1]);"><input class=codename name=tjtypeName readonly=true >
            </TD>       
        </TR>        
        <tr>
          <TD  class= title width="25%">
            起始日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=StartDay verify="起始日期|NOTNULL">
          </TD>      
          <TD  class= title width="25%">
            结束日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay  verify="结束日期|NOTNULL">
          </TD>        
        </TR>
    </table>
          <INPUT VALUE="打印报表" TYPE=button onclick="BillPrint();"> 
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
