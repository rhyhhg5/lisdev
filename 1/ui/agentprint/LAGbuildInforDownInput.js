var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();


window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
	if(showInfo!=null){
	  try{
	    showInfo.focus();
	  }
	  catch(ex){
	    showInfo=null;
	  }
	}
}
//重置按钮函数事件
function reset()
{
   initForm();
}
function easyQueryClick()
{
  if(!verifyInput()) return false;     
  var sql="";
  var tType = fm.all('Type').value ;
  if(tType==null||tType=='')
  {
  	sql="";
    sql=  "select " 
        +"substr(managecom,1,4),"
        +"(select name from ldcom where comcode = substr(managecom,1,4)),"
        +"managecom,"
        +"(select name from ldcom where comcode = managecom),"
        +"branchattr,"
        +"name,"
        +"applygbflag,"
        +"applygbstartdate,"
        +"gbuildflag,"
        +"gbuildstartdate,"
        +"gbuildenddate"
        +" from labranchgroup"
        +" where 1=1 and branchtype = '1' and branchtype2 = '01'";
        if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
        {
         sql += " and branchattr like '"+fm.all('BranchAttr').value+"%'";
        }
        if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!="")
        {
         sql += " and managecom like '"+fm.all('ManageCom').value+"%'";
        }
        sql +=" order by branchattr";
   }else{
   	
    sql="";
    sql=  "select " 
        +"substr(managecom,1,4),"
        +"(select name from ldcom where comcode = substr(managecom,1,4)),"
        +"managecom,"
        +"(select name from ldcom where comcode = managecom),"
        +"branchattr,"
        +"name,"
        +"applygbflag,"
        +"applygbstartdate,"
        +"gbuildflag,"
        +"gbuildstartdate,"
        +"gbuildenddate"
        +" from labranchgroup"
        +" where 1=1 and branchtype = '1' and branchtype2 = '01'"
        + getWherePart("branchlevel","Type")
        if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
        {
         sql += " and branchattr like '"+fm.all('BranchAttr').value+"%'";
        }
        if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!="")
        {
         sql += " and managecom like '"+fm.all('ManageCom').value+"%'";
        }
        sql +=" order by branchattr";
   
   }
     turnPage.strQueryResult  = easyQueryVer3(sql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有查询到符合条件的考核指标！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
turnPage.arrDataCacheSet = arrDataSet1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BranchGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = sql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);   
  }
  
function DoDownload()
{

  if(!verifyInput()) return false;     
  var sql="";
  var tType = fm.all('Type').value ;
  if(tType==null||tType=='')
  {
  	sql="";
    sql=  "select " 
        +"substr(managecom,1,4),"
        +"(select name from ldcom where comcode = substr(managecom,1,4)),"
        +"managecom,"
        +"(select name from ldcom where comcode = managecom),"
        +"branchattr,"
        +"name,"
        +"applygbflag,"
        +"applygbstartdate,"
        +"gbuildflag,"
        +"gbuildstartdate,"
        +"gbuildenddate"
        +" from labranchgroup"
        +" where 1=1 and branchtype = '1' and branchtype2 = '01'";
        if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
        {
         sql += " and branchattr like '"+fm.all('BranchAttr').value+"%'";
        }
        if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!="")
        {
         sql += " and managecom like '"+fm.all('ManageCom').value+"%'";
        }
        sql +=" order by branchattr";
   }else{
   	
    sql="";
    sql=  "select " 
        +"substr(managecom,1,4),"
        +"(select name from ldcom where comcode = substr(managecom,1,4)),"
        +"managecom,"
        +"(select name from ldcom where comcode = managecom),"
        +"branchattr,"
        +"name,"
        +"applygbflag,"
        +"applygbstartdate,"
        +"gbuildflag,"
        +"gbuildstartdate,"
        +"gbuildenddate"
        +" from labranchgroup"
        +" where 1=1 and branchtype = '1' and branchtype2 = '01'"
        + getWherePart("branchlevel","Type")
        if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
        {
         sql += " and branchattr like '"+fm.all('BranchAttr').value+"%'";
        }
        if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!="")
        {
         sql += " and managecom like '"+fm.all('ManageCom').value+"%'";
        }
        sql +=" order by branchattr";
   }
        
        fm.querySql.value = sql;	

       //定义列名
       var strSQLTitle = "select '分公司编码','分公司名称','支公司编码','支公司名称','团队编码','团队名称','申报标志','申报标志起期','团队建设类型','团队建设起期','团队建设止期'  from dual where 1=1 ";
       fm.querySqlTitle.value = strSQLTitle;
  
       //定义表名
       fm.all("Title").value="select '团建团队信息明细' from dual where 1=1  ";  
  
       fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
       fm.submit();
}