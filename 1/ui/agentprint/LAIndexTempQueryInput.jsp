<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="LAIndexTempQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAIndexTempQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>中支及中心营销服务部期交业务快报 </title>   
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<tr class= common>      
      <td class= title>
        管理机构
      </td>
      <TD class= input>
          <Input class="code" name=ManageCom readonly verify = "管理机构|notnull"
                                             ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
      </TD>
    </tr>
    
    <tr class= common>      
      <TD class= title>
        代理人职级
      </TD>
      <TD class= input>
          <Input name=AgentGrade class="code"  verify="代理人职级|notnull" 
                                               ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
                                               onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
      </TD>   
      <TD class= title>
        代理人代码
      </TD>
      <TD class= input>
          <Input name=AgentCode class=common > 
      </TD>
    </tr>    
    
    <tr class= common>      
      <TD class= title>
        计算起期
      </TD>
      <TD class= input>
          <Input name=StartDate class='coolDatePicker' dateFormat= 'short' verify="计算起期|notnull&Date"> 
      </TD>   
      <TD class= title>
        计算止期
      </TD>
      <TD class= input>
          <Input name=EndDate class='coolDatePicker' dateFormat= 'short' verify="计算止期|notnull&Date"> 
      </TD>
    </tr>
       </table>       
      <table  class= common>
        <TR class= common> 
		<TD  class= input width="26%">
			<input class= common type=Button value="打印报表" onclick="PolPrint();">
		</TD>			
	</TR>    
      </table>
      <input type=hidden id="Operate" name="Operate">
      <Input type=hidden name=BranchType > 
      
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
