 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//提交，保存按钮对应操作
function ListExecl()
{
  if (verifyInput() == false)
    return false;

  var strSQL = "";
  var tType=fm.all('Type').value;

     //4位
  if(tType=='A'){
     if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!=''){
		alert("不能录入营业部编码！");
		return false;
	}
     strSQL=" select substr(a.managecom,1,4),(select name from ldcom where comcode=substr(a.managecom,1,4)),"
           +"sum(a.f04),sum(a.f05),sum(a.f01),sum(a.f03),sum(a.f09),"
           +"sum(a.F02),sum(a.F13),sum(a.F12),sum(a.F08),"
           +"sum(a.F10),sum(a.F15),sum(a.F11),sum(a.F17),sum(a.F06),sum(a.F19),sum(a.F20),"
           +"sum(a.F22),sum(a.F23),sum(a.F30),sum(a.ShouldMoney),sum(a.K03)"
           +",sum(a.K02),sum(a.K01),sum(a.K12),sum(a.K15),sum(a.CurrMoney),sum(a.LastMoney),sum(a.SumMoney)  "
           +" from lawage a "
           +" where a.branchtype='1' and a.branchtype2='01' and a.state='1' "
           +" and a.indexcalno>='"+fm.all('StartMonth').value+"'"
           +" and a.indexcalno<='"+fm.all('EndMonth').value+"'"
           +" and a.managecom like '"+fm.all('ManageCom').value+"%'"
           +" group by substr(a.managecom,1,4) with ur  "; 
            }
     //8位
   else if(tType=='B') {
   if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!=''){
		alert("不能录入营业部编码！");
		return false;
	}
     strSQL=" select a.managecom,(select name from ldcom where comcode=a.managecom),"
           +"sum(a.f04),sum(a.f05),sum(a.f01),sum(a.f03),sum(a.f09),"
           +"sum(a.F02),sum(a.F13),sum(a.F12),sum(a.F08),"
           +"sum(a.F10),sum(a.F15),sum(a.F11),sum(a.F17),sum(a.F06),sum(a.F19),sum(a.F20),"
           +"sum(a.F22),sum(a.F23),sum(a.F30),sum(a.ShouldMoney),sum(a.K03)"
           +",sum(a.K02),sum(a.K01),sum(a.K12),sum(a.K15),sum(a.CurrMoney),sum(a.LastMoney),sum(a.SumMoney)  "
           +" from lawage a "
           +" where a.branchtype='1' and a.branchtype2='01' and a.state='1'  "
           +" and a.indexcalno>='"+fm.all('StartMonth').value+"'"
           +" and a.indexcalno<='"+fm.all('EndMonth').value+"'"
           +" and a.managecom like '"+fm.all('ManageCom').value+"%'"
           +" group by a.managecom with ur  ";
             }
   else {
    strSQL=" select substr(a.branchattr,1,10),(select name from labranchgroup  where branchattr=substr(a.branchattr,1,10) and branchtype='1' and branchtype2='01'),"
           +"sum(a.f04),sum(a.f05),sum(a.f01),sum(a.f03),sum(a.f09),"
           +"sum(a.F02),sum(a.F13),sum(a.F12),sum(a.F08),"
           +"sum(a.F10),sum(a.F15),sum(a.F11),sum(a.F17),sum(a.F06),sum(a.F19),sum(a.F20),"
           +"sum(a.F22),sum(a.F23),sum(a.F30),sum(a.ShouldMoney),sum(a.K03)"
           +",sum(a.K02),sum(a.K01),sum(a.K12),sum(a.K15),sum(a.CurrMoney),sum(a.LastMoney),sum(a.SumMoney)  "
           +" from lawage a "
           +" where a.branchtype='1' and a.branchtype2='01' and a.state='1'  "
           +" and a.indexcalno>='"+fm.all('StartMonth').value+"'"
           +" and a.indexcalno<='"+fm.all('EndMonth').value+"'"
           +" and a.managecom like '"+fm.all('ManageCom').value+"%'";
           
        if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!=''){
		 strSQL=strSQL+" and a.branchattr like '"+fm.all('BranchAttr').value+"%'  ";	}    
         
      strSQL=strSQL+" group by substr(a.branchattr,1,10) with ur  ";
   }
    
  fm.querySql.value = strSQL;
 // alert(fm.querySql.value);
  fm.submit();

  }


//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
        fm.all('StartMonth').value = '';
    fm.all('EndMonth').value = '';
   
  }

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作


}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


 
 
