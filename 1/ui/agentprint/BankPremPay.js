//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
	var tWageBegin='';
	var tWageEnd='';
	var tIndexCalNo = fm.all("IndexCalNo").value;
	//季度判断
	var year = tIndexCalNo.substring(0,4);
	var month = tIndexCalNo.substring(4,6);
	if(month>=01 && month <=03){
	     tWageBegin = year+'01';
	     tWageEnd = year+'03'
	}else if(month>=04 && month <=06){
	     tWageBegin = year+'04';
	     tWageEnd = year+'06'
	}else if(month>=07 && month <=09){
	     tWageBegin = year+'07';
	     tWageEnd = year+'09';
	}else{
	     tWageBegin = year+'10';
	     tWageEnd = year+'12';
	}
	var tSql ="";
	    tSql ="select * from (select a.managecom,a.indexcalno,b.branchattr,getUniteCode(a.AgentCode) ord,c.Name,"
			 +"(select gradename from laagentgrade where gradecode=a.agentgrade),"
	         +" case when c.agenttype='1' then '营销人员' when c.agenttype = '2' then '直销人员' else '人员类型' end,c.employdate,"
	         +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
	         +" c.outworkdate"
	         +",(select value(limitperiod,0) from LAWageRadix2 where agentgrade = d.agentgrade and wagecode='YD001' and areatype =(select assessflag from LABankIndexRadix where managecom = substr(a.managecom,1,4)))"
             +",(select value(sum(transmoney),0) from lacommision where wageno=a.indexcalno and agentcode = a.agentcode and commdire = '1' and payyear = 0 and branchtype = '3' and branchtype2 = '01'), "
	         +" case when a.agentgrade<'G51' then"
             +" (select value(sum(F1),0) from LAIndirectWage where commisionsn in (select commisionsn from lacommision where agentcode =a.agentcode and wageno =a.indexcalno and commdire = '1' and branchtype ='3'  and branchtype2 ='01'))"
             +" else "
             +"(select value(sum(F1),0) from LAIndirectWage where commisionsn in (select commisionsn from lacommision where agentgroup =b.agentgroup and wageno =a.indexcalno and commdire = '1' and branchtype ='3'  and branchtype2 ='01')) end"             
             +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
             +",(select value(sum(transmoney),0) from lacommision where wageno>='"+tWageBegin+"' and wageno<='"+tWageEnd+"' and agentcode = a.agentcode and commdire = '1' and payyear = 0 and branchtype = '3' and branchtype2 = '01'), "            
	         +" case when a.agentgrade<'G51' then"
             +" (select value(sum(F1),0) from LAIndirectWage where commisionsn in (select commisionsn from lacommision where agentcode =a.agentcode and wageno>='"+tWageBegin+"' and wageno<='"+tWageEnd+"' and commdire = '1' and branchtype ='3'  and branchtype2 ='01'))"
             +" else "
             +" (select value(sum(F1),0) from LAIndirectWage where commisionsn in (select commisionsn from lacommision where agentgroup =b.agentgroup and wageno>='"+tWageBegin+"' and wageno<='"+tWageEnd+"' and commdire = '1' and branchtype ='3'  and branchtype2 ='01')) end"     
             +",(select to_char(T36)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
	         +" from lawage a,labranchgroup b,laagent c ,latree d"
             +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
	         +" and c.agentgroup=b.agentgroup "
	         +" and c.agentcode=d.agentcode  and a.branchtype='3' and a.branchtype2= '01'"
	         +" and a.indexcalno = '"+tIndexCalNo+"'"
	         +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
	         +getWherePart('b.branchattr','BranchAttr')
	         +getWherePart('c.groupagentcode','GroupAgentCode')
	         +getWherePart('c.name','AgentName')	
	         +getWherePart('c.agenttype','AgentType')
	         +")  kk order by ord with ur";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
    fm.querySql.value = tSql;
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	fm.reset();
	showInfo.close();
}
//function getname()
//{
//	return;
//	
//	}
function checkBranchAttr()
{
	var tReturn = getManageComLimitlike("ManageCom");
  var sql=" select agentgroup,branchlevel  from labranchgroup  where branchattr='"+fm.BranchAttr.value+"'"
         +" and  branchtype='3'	 and branchtype2='01'    "
         +tReturn
         +getWherePart("ManageCom","ManageCom",'like')	;
  var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
 // alert(sql);
   if (!strQueryResult) 
    {
      alert("此管理机构没有此销售单位！");  
      fm.BranchAttr.value="";
      return;
    }	
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentGroup1').value=arr[0][0];
    
}	
function checkAgentCode()
{
	var tReturn = getManageComLimitlike("ManageCom");
  var sql=" select agentcode  from laagent   where  groupagentcode='"+fm.GroupAgentCode.value+"'"
         +" and  branchtype='3'	 and branchtype2='01'     "
         + tReturn  
         + getWherePart("ManageCom","ManageCom",'like')	;
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
   if (!strQueryResult) 
    {
     alert("此管理机构没有此销售员！");  
     fm.GroupAgentCode.value="";
     return;
    }
    // xqq 2014-11-30
        var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
//    alert(fm.all('AgentCode').value );
}	
function checkAgentName()
{
	var tReturn = getManageComLimitlike("ManageCom");
  var sql=" select agentcode  from laagent   where  name='"+fm.AgentName.value+"'"
         +" and  branchtype='3'	 and branchtype2='01'     "
         + tReturn  
         + getWherePart("ManageCom","ManageCom",'like')	;
  var sql=sql+" order by agentcode fetch first 1 rows only "
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
   if (!strQueryResult) 
    {
     alert("此管理机构没有此销售员！");  
     fm.AgentCode.value="";
     return;
    }
 var arr = decodeEasyQueryResult(strQueryResult);
 var tagentcode=arr[0][0];
 if(trim(fm.all('AgentCode').value)!=''&&trim(fm.all('AgentCode').value)!=null)
  {
  	if(tagentcode!=trim(fm.all('AgentCode').value))
  	{
  		alert("录入的业务员代码和业务员名称不符！");  
     fm.AgentCode.value="";
     return;
  	 }
  }

}	
function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	fm.reset();
	showInfo.close();
}