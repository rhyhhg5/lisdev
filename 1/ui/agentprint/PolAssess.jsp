<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");    
    String CurrentDate = PubFun.getCurrentDate();
    System.out.println(CurrentDate);    
    FDate fDate = new FDate();
    Date CurDate = fDate.getDate(CurrentDate);
    GregorianCalendar mCalendar = new GregorianCalendar();
    mCalendar.setTime(CurDate);
    int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
    int Quarter = (Months - 1) / 3 + 1;  //计算当前季度    
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="PolAssess.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PolAssessInit.jsp"%>
  <title>个人业务督导部竞赛排行榜 </title>   
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>    
         <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   

       </TR>  
       <TR>
       <TD class= title>
             考核年月
          </TD>
          <TD  class= input width="25%">
            <Input class= common name=Month >
          </TD>                  
      </TR>       
    </table>
    <table  class= common>
        <TR class= common> 
		<TD  class= input width="26%">
			<input class= common type=Button value="按月打印" onclick="PolPrint();">
		</TD>		
	</TR>    	 
      </table>
    <table  class= common align=center>
      	<TR  class= common>
      	  <TD  class= title>
            报表时间(季度)
          </TD>          
          <TD  class= input>    
              <Input class=common  name=quarter value=<%=Quarter%>>
          </TD>           
          
          </TR>            
      </table>
   <table  class= common>
        <TR class= common> 
		<TD  class= input width="26%">
			<input class= common type=Button value="按季度打印" onclick="OtherPrint();">
		</TD>		
	</TR>    	 
      </table>   
      
      <input type=hidden id="Operate" name="Operate">
      
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
