//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();


//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交

}

//提交，保存按钮对应操作
function printPol()
{
	if (!verifyInput()) 
    return false;
	if (fm.all('ManageCom').value==null||fm.all('ManageCom').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
  var i = 0;

  var arrReturn = new Array();
 // var tSel = PolGrid.getSelNo();	

		arrReturn = getQueryResult();
		//PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( arrReturn == null ) {
			alert("无效的数据");
			return;
		}
		
		//fmSave.PrtSeq.value = PrtSeq;
		//fmSave.PolNo.value = arrReturn[0][1];
		//fmSave.fmtransact.value = "CONFIRM";
		fm.target="f1print";
		fm.submit();
		
	
}

function getQueryResult()
{
	
	var arrSelected = null;
	//tRow = PolGrid.getSelNo();
	if(arrDataSet == null )
		return arrSelected;

	//arrSelected = new Array();
	//设置需要返回的数组
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrDataSet;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	if (!verifyInput()) 
    return false;
	if (fm.all('ManageCom').value==null||fm.all('ManageCom').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
	// 初始化表格
	initPolGrid();	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select getUniteCode(LAAgent.AgentCode),LAAgent.Name,b.codename,LAAgent.IDNo,"
	          +"LAAgent.employdate,c.codename,LAAgent.Mobile,e.name,"
	          +"a.codename,d.codename,LAAgent.channelname,(select count(*) from LAComToAgent where RelaType='1' and AgentCode=LAAgent.AgentCode ) "
	          +"from LAAgent,latree,labranchgroup,ldcode a,ldcode b,ldcode c,ldcode d,labranchgroup e "
	          +"where  a.codetype='agentkind' and "
	          +"a.code = LAAgent.agentkind and LAAgent.agentgroup = labranchgroup.agentgroup and "
	          +"b.code = LAAgent.Sex and b.codetype='sex' and "
	          +"c.code = LAAgent.Degree and c.codetype='degree' and "
	          +"d.code = latree.agentgrade and d.codetype='agentgrade' and "
	          +"e.branchattr=labranchgroup.branchattr "
	          +"and LAAgent.agentcode = latree.agentcode and "
	          +"LAAgent.AgentState in ('01','02') and LAAgent.branchtype='3' and (labranchgroup.state<>'1' or labranchgroup.state is null)"
	          + getWherePart('LAAgent.ManageCom','ManageCom','like')
	          +" order by LAAgent.AgentCode";
	
	  
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
 if (!turnPage.strQueryResult) {
    alert("没有符合要求的记录！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}