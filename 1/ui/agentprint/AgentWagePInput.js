//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{ 
	var IndexCalNo=fmSave.all("IndexCalNo").value;
	if (IndexCalNo==null||IndexCalNo=="")
	{
		alert("请输入打印薪资的月度！");
		return;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
		fmSave.AgentGroup.value = PolGrid.getRowColData(tSel-1,1);
		fmSave.BranchAttr.value = PolGrid.getRowColData(tSel-1,4);
		fmSave.fmtransact.value = "PRINT";
		fmSave.all('Operate').value='PRINT';
		fmSave.target = "f1print";
		fmSave.submit();
		showInfo.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");	
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
	if (verifyInput() == false)
    return false;
    
	initPolGrid();
	// 书写SQL语句
	var ManageCom=trim(fm.all('ManageCom').value)+"%%";//easyQuery对%号做了转换，所以当作转义字符用

	var strSQL = "";
	var i=0;
	i=fm.all('ManageCom').value.length+2;
	
		
	if(i<=8)
	{
		strSQL = "select  '',shortname,comcode,comcode a,'','' from ldcom where length(trim(comcode))="
		+i+" and comcode like '"+ManageCom+"'"
		+ " order by comcode";
	}
	else
	{
	    strSQL = "select AgentGroup,Name,ManageCom,BranchAttr,BranchLevel,BranchManagerName from LABranchGroup where ManageCom like '"+ManageCom+"' and branchtype='1' and endflag<>'Y' and (state<>'1' or state is null)"
					+ getWherePart( 'BranchAttr', 'BranchAttr','like' )
					+ " order by BranchAttr";
	}
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 //tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}

//提交，保存按钮对应操作
function NegativePrt()
{ 
	var IndexCalNo=fmSave.all("IndexCalNo").value;
	if (IndexCalNo==null||IndexCalNo=="")
	{
		alert("请输入打印薪资的月度！");
		return;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  //var testPol = PolGrid.getRowColData();	
  //alert(tSel);
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		fmSave.AgentGroup.value = PolGrid.getRowColData(tSel-1,1);
		fmSave.BranchAttr.value = PolGrid.getRowColData(tSel-1,4);
		fmSave.fmtransact.value = "PRINT";
		fmSave.all('Operate').value='NEGAT';
		fmSave.target = "f1print";
		fmSave.submit();
		showInfo.close();
	}
}

//提交，保存按钮对应操作
function AgentWagePrt()
{ 
	var IndexCalNo=fmSave.all("IndexCalNo").value;
	if (IndexCalNo==null||IndexCalNo=="")
	{
		alert("请输入打印薪资的月度！");
		return;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  //var testPol = PolGrid.getRowColData();	
  //alert(tSel);
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		fmSave.AgentGroup.value = PolGrid.getRowColData(tSel-1,1);
		fmSave.BranchAttr.value = PolGrid.getRowColData(tSel-1,4);
		fmSave.fmtransact.value = "PRINT";
		fmSave.all('Operate').value='WAGE';
		fmSave.target = "f1print";
		fmSave.submit();
		showInfo.close();
	}
}