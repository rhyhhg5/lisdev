<%
//程序名称：FirstPayInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = tGlobalInput.ManageCom;
%>                            

<script language="JavaScript">

var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {
  	fm.reset();                                   
  }
  catch(ex)
  {
    alert("在AgentInfoPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	manageCom = '<%= strManageCom %>';
    initInpBox();
	  initPolGrid();
  }
  catch(re)
  {
    alert("FirstPayInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 投保单信息列表的初始化
function initPolGrid()
{                               
  var iArray = new Array();
      
  try {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            	//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="员工编码";         		//列名
	  iArray[1][1]="100px";            	//列宽
	  iArray[1][2]=200;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="姓名";       		//列名
	  iArray[2][1]="50px";            	//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="性别";         	        //列名
	  iArray[3][1]="30px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[4]=new Array();
	  iArray[4][0]="身份证号码";            //列名
	  iArray[4][1]="120px";            	//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="入司时间";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[5][1]="100px";            	//列宽
	  iArray[5][2]=10;            			//列最大值
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array();
	  iArray[6][0]="学历";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[6][1]="30px";            	//列宽
	  iArray[6][2]=10;            			//列最大值
	  iArray[6][3]=0;
	  
	  iArray[7]=new Array();
	  iArray[7][0]="手机";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[7][1]="100px";            	//列宽
	  iArray[7][2]=10;            			//列最大值
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array();
	  iArray[8][0]="所属机构（体现到组）";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[8][1]="150px";            	//列宽
	  iArray[8][2]=10;            			//列最大值
	  iArray[8][3]=0;
	  
	  iArray[9]=new Array();
	  iArray[9][0]="岗位名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[9][1]="100px";            	//列宽
	  iArray[9][2]=10;            			//列最大值
	  iArray[9][3]=0;
	  
	  iArray[10]=new Array();
	  iArray[10][0]="职级";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[10][1]="130px";            	//列宽
	  iArray[10][2]=10;            			//列最大值
	  iArray[10][3]=0;
	  
	  iArray[11]=new Array();
	  iArray[11][0]="所属渠道";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[11][1]="100px";            	//列宽
	  iArray[11][2]=10;            			//列最大值
	  iArray[11][3]=0;
	  
	  iArray[12]=new Array();
	  iArray[12][0]="管理网点总数";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[12][1]="100px";            	//列宽
	  iArray[12][2]=10;            			//列最大值
	  iArray[12][3]=0;
	  
	  
	
	  PolGrid = new MulLineEnter( "fmSave" , "PolGrid" ); 
	  //这些属性必须在loadMulLine前
	  PolGrid.mulLineCount = 10;   
	  PolGrid.displayTitle = 1;
	  PolGrid.canSel = 0;
    PolGrid.locked = 1;
          PolGrid.hiddenPlus=1;
      PolGrid.hiddenSubtraction=1;
	  PolGrid.loadMulLine(iArray);  
	
	} catch(ex) {
		alert(ex);
	}
}

</script>