<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
	if (BranchType == null || BranchType.equals("")) {
		BranchType = "";
	}
	if (BranchType2 == null || BranchType2.equals("")) {
		BranchType2 = "";
	}
	System.out.println("BranchType:" + BranchType + "  BranchType2:"
			+ BranchType2);
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LAFYCRCQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LAFYCRCQueryInit.jsp"%>
<title>Sinosoft</title>
</head>

<body onload="initForm();initElementtype();">
<form action="./LAFYCRCQuerySave.jsp" method=post name=fm
	target="f1print">
<table>
	<table>
		<tr class=common>
			<td class=common><IMG src="../common/images/butExpand.gif"
				style="cursor:hand;" OnClick="showPage(this,divLAAgent1);">
			<td class=titleImg>查询条件</td>
			</td>
		</tr>
	</table>
	<div id="divLAAgent1" style="display: ''">
	<table class=common align='center'>
		<TR class=common>
			<TD class=title>统计级别</td>
			<td class=input><Input class="codeno" name=Unity
				verify="统计层级|NOTNULL"
				CodeData="0|^01|分公司|^02|支公司|^03|营业部|^04|营业区|^05|营业处|^06|营销员"
				ondblclick="return showCodeListEx('Unity',[this,UnityName],[0,1],null,null,null,1);"
				onkeyup="return showCodeListKey('Unity',[this,UnityName],[0,1],null,null,null,1);"><Input
				class="codename" name=UnityName elementtype=nacessary></td>
			<TD class=title>管理机构</TD>
			<TD class=input><Input class="codeno" name=ManageCom
				verify="管理机构|code:comcode&NOTNULL&len<9"
				ondblclick="return getChangeComCode(this,ManageComName);"><Input
				class="codename" name=ManageComName elementtype=nacessary></TD>
		</TR>
		<TR class=common>
			<TD class=title>起始日期</td>
			<td class=input><Input class='coolDatePicker' dateFormat="short"
				name=StartDate verify=" 起始日期|NOTNULL&Date" elementtype=nacessary>
			</td>
			<TD class=title>终止日期</td>
			<td class=input><Input class='coolDatePicker' dateFormat="short"
				name=EndDate verify="终止日期|NOTNULL&Date" elementtype=nacessary>
			</td>
		</TR>
		<TR>
			<TD class=title>销售团队代码</td>
			<td class=input><Input class='common' name=BranchAttr></td>
			<TD class=title></td>
			<td class=input></td>
		</TR>
	</table>
	<table class=common border=0 width=100%>
		<TR class=common>
			<TD class=button width="10%" align=left><INPUT VALUE="查 询"
				TYPE=button class=cssbutton onclick="return  easyQueryClick();">
			<INPUT VALUE="下 载" TYPE=button class=cssbutton onclick="ListExecl();">
			</TD>
		</TR>
	</table>
	<p><font color="#ff0000">注：1.此报表显示的是非审核发放的即时动态数据。<br>
	&nbsp;&nbsp;&nbsp;&nbsp;2.查询结果以统计层级为显示单位，所选查询条件必须为所选统计层级的上级公司或团队;<br>
	&nbsp;&nbsp;&nbsp;&nbsp;如统计层级为支公司时，所选查询条件为分公司；统计层级为区时,所选查询条件必须为某个支公司的部级团队，依次类推！</font></p>
	</div>
	<table>
		<tr>
			<td class=common><IMG src="../common/images/butExpand.gif"
				style="cursor:hand;" OnClick="showPage(this,divSetGridF);"></td>
			<td class=titleImg>查询结果</td>
		</tr>
	</table>

	<div id="divSetGridF" style="display: '' ">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1><span id="spanSetGridF"></span></td>
		</tr>
	</table>
	</div>
	<div id="divSetGridS" style="display:'none'">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1><span id="spanSetGridS"></span></td>
		</tr>
	</table>
	</div>
	<div id="divSetGridT" style="display:'none'">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1><span id="spanSetGridT"></span></td>
		</tr>
	</table>
	</div>
	<INPUT VALUE=" 首页 " TYPE="button" class=cssButton
		onclick="turnPage.firstPage();">
	<INPUT VALUE="上一页" TYPE="button" class=cssButton
		onclick="turnPage.previousPage();">
	<INPUT VALUE="下一页" TYPE="button" class=cssButton
		onclick="turnPage.nextPage();">
	<INPUT VALUE=" 尾页 " TYPE="button" class=cssButton
		onclick="turnPage.lastPage();">
	<br>
	<input type=hidden name=BranchType value=''>
	<input type=hidden name=BranchType2 value=''>
	<input type=hidden id="fmAction" name="fmAction">
	<input type=hidden class=Common name=querySql>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray">
	</span>
</body>
</html>



