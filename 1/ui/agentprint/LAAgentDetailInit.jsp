<%
//程序名称：LAAgentInit.jsp
//程序功能：个人代理增员管理初始化
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     String currdate = PubFun.getCurrentDate();
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {    
    fm.all('ManageCom').value = '';  
    fm.all('AgentCodeInit').value = '';    
    fm.all('NameInit').value = '';                         
    fm.all('AgentCode').value = '';
    fm.all('Name').value = '';
    fm.all('SexName').value ='';
    fm.all('Birthday').value = '';
    fm.all('IDNoTypeName').value = '';
    fm.all('IDNo').value = '';
    fm.all('NativePlaceName').value = '';
    fm.all('NationalityName').value = '';
    fm.all('RgtAddressName').value = '';
    fm.all('DegreeName').value = '';
    fm.all('GraduateSchool').value = '';
    fm.all('Speciality').value = '';
    fm.all('NationalityName').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('OldOccupation').value = '';
    fm.all('OldCom').value = '';
    fm.all('HeadShip').value = '';
    fm.all('CertifiNo').value = '';
    fm.all('QualifNo').value = '';
    fm.all('Account').value = '';
    fm.all('IntroAgency').value = '';
    fm.all('IntroAgencyName').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('BranchName').value = '';
    fm.all('MinisterCode').value = '';
    fm.all('GroupManagerName').value = '';
    fm.all('DepManagerName').value = '';
    fm.all('Minister').value = '';
    fm.all('AgentState').value = '';
    
    fm.all('EmployDate').value = '';
    fm.all('OutWorkDate').value = '';
    

    fm.all('BranchType').value = '1';
    fm.all('BranchType2').value = '01';   

   
  }
  catch(ex)
  {
    alert("在LAAgentInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LAAgentInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initWarrantorGrid();    
  }
  catch(re)
  {
    alert("LAAgentInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
// 担保人信息的初始化
function initWarrantorGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="团队名称";          		        //列名
      iArray[1][1]="80px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="团队代码";         		        //列名
      iArray[2][1]="80px";            			//列宽
      iArray[2][2]=10;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[3]=new Array();
      iArray[3][0]="姓名";      	   		//列名
      iArray[3][1]="80px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0; 
      
        
      iArray[4]=new Array();
      iArray[4][0]="代码";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="现职级名称";      	   		//列名
      iArray[5][1]="80px";            			//列宽
      iArray[5][2]=30;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      iArray[6]=new Array();
      iArray[6][0]="现职级代码";      	   		//列名
      iArray[6][1]="60px";            			//列宽
      iArray[6][2]=40;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      
      iArray[7]=new Array();
      iArray[7][0]="性别";      	   		//列名
      iArray[7][1]="60px";            			//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=0;  
      
      iArray[8]=new Array();
      iArray[8][0]="入司日期";      	   		//列名
      iArray[8][1]="80px";            			//列宽
      iArray[8][2]=20;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[9]=new Array();
      iArray[9][0]="入司职级";      	   		//列名
      iArray[9][1]="60px";            			//列宽
      iArray[9][2]=20;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[10]=new Array();
      iArray[10][0]="变更职级";      	   		//列名
      iArray[10][1]="60px";            			//列宽
      iArray[10][2]=20;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
      iArray[11]=new Array();
      iArray[11][0]="变更时间";      	   		//列名
      iArray[11][1]="80px";            			//列宽
      iArray[11][2]=20;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="推荐人";      	   		//列名
      iArray[12][1]="80px";            			//列宽
      iArray[12][2]=20;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="在职/离职状态";      	   		//列名
      iArray[13][1]="80px";            			//列宽
      iArray[13][2]=20;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      WarrantorGrid = new MulLineEnter( "fm" , "WarrantorGrid" ); 
      //这些属性必须在loadMulLine前
      WarrantorGrid.mulLineCount = 1;   
      WarrantorGrid.displayTitle = 1;
      WarrantorGrid.locked=1;   
      WarrantorGrid.canSel=0;
      WarrantorGrid.hiddenPlus=1;       
      WarrantorGrid.hiddenSubtraction=1;   
      WarrantorGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert(ex);
      }
}
}
</script>
