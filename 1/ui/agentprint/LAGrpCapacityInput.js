 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//提交，保存按钮对应操作
function ListExecl1()
{
  if (verifyInput() == false)
    return false;
    
    var tStartMonth=fm.all('StartMonth').value;
	var tEndMonth=fm.all('EndMonth').value;
	
	if(trim(tStartMonth.substring(0,4))!=trim(tEndMonth.substring(0,4)))
	{
	  alert("起期与止期必须在同一年！");  
	  return false;
	}

//定义查询的数据
var strSQL = "";
strSQL = "select aa,qq,mm,bb,cc,nn,dd,pp,"
	  +" case  when (select agentstate from laagent where agentcode = aaa.dd) < '06' then '在职' "
	   +" when (select agentstate from laagent where agentcode = aaa.dd) >= '06' then '离职'  end,"
	   + "hh,"
       + "decimal(value(sum(case when ee in ('1','9') and tt not in ('170301','170401') then ff else 0 end),0),12,2),"//商团保费（实收）
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt not in ('170301','170401') then ff else 0 end),0),12,2),"//社保保费（实收）

       
       + "decimal(value(sum(case when ee in ('1','9') and tt not in ('170301','170401') then gg else 0 end),0),12,2),"//商团保费（折算）
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt not in ('170301','170401') then gg else 0 end),0),12,2) "//社保保费（折算）

       
       + ",decimal(value(sum(case when ee in ('1','9') and tt not in ('170301','170401') then ss else 0 end),0),12,0)"//商团客户数
       + ",decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt not in ('170301','170401') then ss else 0 end),0),12,0)"//社保客户数


       + ",decimal(value(sum(case when ee in ('1','9') and tt  in ('170301','170401') then ff else 0 end),0),12,2),"//特需商团保费（实收）
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt  in ('170301','170401') then ff else 0 end),0),12,2),"//特需社保保费（实收）

       
       + "decimal(value(sum(case when ee in ('1','9') and tt  in ('170301','170401') then gg else 0 end),0),12,2),"//特需商团保费（折算）
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt  in ('170301','170401') then gg else 0 end),0),12,2) "//特需社保保费（折算）

       
       + ",decimal(value(sum(case when ee in ('1','9') and tt  in ('170301','170401') then ss else 0 end),0),12,0)"//特需商团客户数
       + ",decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt  in ('170301','170401')  then ss else 0 end),0),12,0)"//特需社保客户数
       
       + ",decimal(value(sum(case when ee in ('12','13','14','15') then ff else 0 end),0),12,2),"//大病保险保费（实收）
       + "decimal(value(sum(case when ee in ('12','13','14','15')  then gg else 0 end),0),12,2),"//大病保险保费（折算）
       + "decimal(value(sum(case when ee in ('12','13','14','15') then ss else 0 end),0),12,0)"//大病保险客户数

       
       + "from ("
       + "select "
       + "a.managecom aa "
       + ",(select name from ldcom where comcode=substr(a.managecom,1,4) ) qq "
       + ",(select name from ldcom where comcode=a.managecom) mm "
       + ",(select code2 from ldcoderela where relatype like 'comtoareatype2%'and code1=substr(a.managecom ,1,4) )  bb"
       + ",a.branchattr cc"
       + ",(select name from labranchgroup  where branchattr=a.branchattr and branchtype='2' and branchtype2='01')  nn"
       + ",(select getUniteCode(branchmanager) from labranchgroup  where branchattr=a.branchattr and branchtype='2' and branchtype2='01')  dd"
       + ",(select branchmanagername from labranchgroup  where branchattr=a.branchattr and branchtype='2' and branchtype2='01')  pp"
       + ",(select agentgrade from latree where agentcode=(select branchmanager from labranchgroup  where branchattr=a.branchattr and branchtype='2' and branchtype2='01')) hh"
       + ",b.markettype ee"
       + ",a.riskcode tt"
       + ",count(distinct a.appntno) ss"
       + ",sum(a.transmoney) ff"
       + ",sum(a.standprem ) gg "
       + "from lacommision a,lcgrpcont b "
       + "where a.branchtype='2' "
       + "and a.branchtype2='01' "
       + "and a.grpcontno=b.grpcontno "
       + "and a.wageno between '"+fm.StartMonth.value+"' and '"+fm.EndMonth.value+"' "
       + "group by a.managecom,a.branchattr,b.markettype,a.riskcode "
       + ")  "
       + "as aaa group by aa,qq,mm,bb,cc,nn,dd,pp,hh "
       + "order by aa,qq,mm,bb,cc,nn,dd,pp,hh "
       + "with ur";

     
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '机构编码','管理机构','机构名称','机构类别','营业部编码','营业部名称','营业部经理','经理名称','销售人员状态','经理职级',"
+"'商团保费（实收）','社保保费（实收）','商团保费（折算）','社保保费（折算）' ,'商团客户数','社保客户数', "
+"'特需商团保费（实收）','特需社保保费（实收）','特需商团保费（折算）','特需社保保费（折算）' ,'特需商团客户数','特需社保客户数','大病保险保费（实收）','大病保险保费（折算）','大病保险客户数' from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '按营业部查询结果显示' from dual where 1=1  ";  
      fm.action = "../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

  }

//提交，保存按钮对应操作
function ListExecl2()
{
  if (verifyInput() == false)
    return false;

//定义查询的数据
var strSQL = "";
strSQL =  "select aa,qq,mm,bb,cc,nn,"
	   +" case  when (select agentstate from laagent where agentcode = aaa.cc) < '06' then '在职' "
	   +" when (select agentstate from laagent where agentcode = aaa.cc) >= '06' then '离职'  end,"
	   +"dd,"
       + "decimal(value(sum(case when ee in ('1','9') and tt not in ('170301','170401') then ff else 0 end),0),12,2),"
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt not in ('170301','170401') then ff else 0 end),0),12,2),"
       
       + "decimal(value(sum(case when ee in ('1','9') and tt not in ('170301','170401') then gg else 0 end),0),12,2),"
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt not in ('170301','170401') then gg else 0 end),0),12,2) "
       
       + ",decimal(value(sum(case when ee in ('1','9') and tt not in ('170301','170401') then ss else 0 end),0),12,0)"
       + ",decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt not in ('170301','170401') then ss else 0 end),0),12,0)"

       + ",decimal(value(sum(case when ee in ('1','9') and tt  in ('170301','170401') then ff else 0 end),0),12,2),"
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt  in ('170301','170401') then ff else 0 end),0),12,2),"
       
       + "decimal(value(sum(case when ee in ('1','9') and tt  in ('170301','170401') then gg else 0 end),0),12,2),"
       + "decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt  in ('170301','170401') then gg else 0 end),0),12,2) "
       
       + ",decimal(value(sum(case when ee in ('1','9') and tt  in ('170301','170401') then ss else 0 end),0),12,0)"
       + ",decimal(value(sum(case when ee in ('2','3','4','5','6','7','8','99','10','11') and tt  in ('170301','170401')  then ss else 0 end),0),12,0)"
       
       + ",decimal(value(sum(case when ee in ('12','13','14','15') then ff else 0 end),0),12,2),"//大病保险保费（实收）
       + "decimal(value(sum(case when ee in ('12','13','14','15')  then gg else 0 end),0),12,2),"//大病保险保费（折算）
       + "decimal(value(sum(case when ee in ('12','13','14','15') then ss else 0 end),0),12,0)"//大病保险客户数
       + "from ("
       + "select "
       + "a.managecom aa "
       + ",(select name from ldcom where comcode=substr(a.managecom,1,4) ) qq"
       + ",(select name from ldcom where comcode=a.managecom) mm  "
       + ",(select code2 from ldcoderela where relatype like 'comtoareatype2%'and code1=substr(a.managecom ,1,4) )  bb"
       + ",getUniteCode(a.agentcode) cc "
       + ",(select name from laagent where agentcode=a.agentcode)  nn"
       + ",(select agentgrade from latree where agentcode=a.agentcode) dd "
       + ",b.markettype ee "
       + ",a.riskcode tt"
       + ",count(distinct a.appntno) ss "
       + ",sum(a.transmoney) ff "
       + ",sum(a.standprem)  gg "
       + "from lacommision a,lcgrpcont b  "
       + "where a.branchtype='2' "
       + "and a.branchtype2='01' "
       + "and a.grpcontno=b.grpcontno "
       + "and a.wageno between '"+fm.StartMonth.value+"' and '"+fm.EndMonth.value+"' "
       + "group by a.managecom,a.agentcode,b.markettype,a.riskcode "
       + ")"
       + "as aaa group by aa,qq,mm,bb,cc,nn,dd "
       + "order by aa,qq,mm,bb,cc,nn,dd "
       + "with ur";
       
       fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '机构编码','管理机构','机构名称','机构类别','销售人员','人员名称','销售人员状态','销售人员职级',"
+"'商团保费（实收）','社保保费（实收）','商团保费（折算）','社保保费（折算）' ,'商团客户数','社保客户数', "
+"'特需商团保费（实收）','特需社保保费（实收）','特需商团保费（折算）','特需社保保费（折算）' ,'特需商团客户数','特需社保客户数','大病保险保费（实收）','大病保险保费（折算）','大病保险客户数' from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '按销售人员查询结果显示' from dual where 1=1  ";  
    fm.action = "../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

  }


//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
   // AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}


//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}



function submitSave()
{
}


 
 
