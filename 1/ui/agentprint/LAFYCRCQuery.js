 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function getBranchattr(branchlevel,branchattr){
   if(branchattr==null||branchattr==''){
	   alert("请输入销售团队代码！");
	   return false;
	}
	var branchattr = fm.all('BranchAttr').value;
	var tsql = "select branchattr,branchlevel from labranchgroup where  branchtype = '1' and branchtype2 = '01' ";
	tsql += " and managecom ='"+fm.all('ManageCom').value+"' and branchattr = '"+fm.all('BranchAttr').value+"'";
    var strQueryResult = easyQueryVer3(tsql, 1, 1, 1);
	if (!strQueryResult) {
	    alert("该销售团队不存在或不隶属于该机构!");   
	    return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	if(branchlevel != arr[0][1]){
		alert('请输入统计级别的上一级团队代码！');
		return false;
	}
    return true;
}


//提交前的校验、计算
function beforeSubmit()
{
    if (verifyInput() == false)
    return false;
   
	var tManageCom  = fm.all('ManageCom').value;
	var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
	if(startDate>endDate){
	   alert("终止日期要大于或等于起始日期！");
	   return false;
	}
	return true;
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "Unity")
	{
	   fm.all('ManageCom').value = '';
	   fm.all('ManageComName').value = '';
	}
}

//查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	if(!beforeSubmit()){
	   return false;
	}
	var tManageCom  = fm.all('ManageCom').value;
	var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
     var sqlArray = new Array();
  	 var unity = fm.all('Unity').value;
  	 var branchattr = fm.all('BranchAttr').value;
	if(unity=='01'){
		fm.all('BranchAttr').value = '';
       sqlArray[0] = "	   substr(temp.managecom,1, 4),(select name from ldcom where comcode = substr(temp.managecom, 1, 4)),";
       sqlArray[1] = " substr(temp.managecom, 1, 4)";
    }else if(unity =='02'){
    	fm.all('BranchAttr').value = '';
    	sqlArray[0] = "	  temp.managecom,(select name from ldcom where comcode = temp.managecom),";
       sqlArray[1] = " temp.managecom";
    }else if(unity =='03'){
    	fm.all('BranchAttr').value = '';
    	sqlArray[0] = "'"+tManageCom+"',(select name from ldcom where comcode ='"+ tManageCom+"'),";
    	sqlArray[0] += " (select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 1, 12)), (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 1, 12)),";
       sqlArray[1] = " substr(temp.branchseries,1,12)";
    }else if(unity =='04'){
    	initSetGridS();
    	if(!getBranchattr('03',branchattr)){
    	   return false;
    	}
    	sqlArray[0] = "'"+tManageCom+"',(select name from ldcom where comcode ='"+ tManageCom+"'),";
    	sqlArray[0] += " (select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 14, 12)), (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 14, 12)),";
       sqlArray[1] = " substr(temp.branchseries,14,12)";
    }else if(unity =='05'){
        if(!getBranchattr('02',branchattr)){
    	   return false;
    	}
     	sqlArray[0] = "'"+tManageCom+"',(select name from ldcom where comcode ='"+ tManageCom+"'),";
    	sqlArray[0] += " (select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 27, 12)), (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 27, 12)),";
       sqlArray[1] = " substr(temp.branchseries,27,12)";
    }else if(unity =='06'){
        if(!getBranchattr('01',branchattr)){
    	   return false;
    	}
    	sqlArray[0] = "'"+tManageCom+"',(select name from ldcom where comcode ='"+ tManageCom+"'),";
    	sqlArray[0] += "'"+branchattr+"',"+" (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr='"+branchattr+"'),";
    	sqlArray[0] += " temp.agentcode,(select name from laagent where agentcode = temp.agentcode), ";
        sqlArray[1] = " temp.agentcode";
    }
	
	
	var strSQL = "";
	strSQL="select "+
			sqlArray[0]+
			"       sum(case"+
			"             when temp.renewcount = 0 then"+
			"              temp.fyc"+
			"             else"+
			"              0"+
			"           end),"+
			"       sum(case"+
			"             when temp.renewcount > 0 then"+
			"              temp.fyc"+
			"             else"+
			"              0"+
			"           end),"+
			"       sum(fyc)"+
			"  from (select managecom, branchattr,branchseries,agentcode, fyc, renewcount"+
			"          from lacommision"+
			"         where managecom like '"+tManageCom+"%'"+
			"           and branchtype = '1'"+
			"           and (branchtype2 = '01' or"+
			"               (branchtype2 = '03' and f3 is not null))"+
			"           and CommDire = '1' ";
			if(unity>='04'&& branchattr!=null && branchattr!=''){
				strSQL = strSQL  + " and branchattr like '" + fm.all('BranchAttr').value+"%'";
			}
			strSQL =strSQL+
			"           and (RENEWCOUNT>=1 or (payyear = 0 and RENEWCOUNT = 0))"+
			"           and tmakedate >= '"+startDate+"'"+
			"           and tmakedate <= '"+endDate+"') as temp"+
			" where 1 = 1"+
			" group by "+
			//" substr(temp.managecom, 1, 4)"
			sqlArray[1]
			; 
			
    if(unity<='02'){
        initSetGridF();
    	fm.all('divSetGridF').style.display='';
    	fm.all('divSetGridS').style.display='none';
    	fm.all('divSetGridT').style.display='';
    	turnPage.pageDisplayGrid = SetGridF;
    }else if(unity>='03'&&unity<='05'){
        initSetGridS();
        fm.all('divSetGridF').style.display='none';
    	fm.all('divSetGridS').style.display='';
    	fm.all('divSetGridT').style.display='none';
    	turnPage.pageDisplayGrid = SetGridS;
    }else if(unity='06'){
    	initSetGridT();
    	fm.all('divSetGridF').style.display='none';
    	fm.all('divSetGridS').style.display='none';
    	fm.all('divSetGridT').style.display='';
    	turnPage.pageDisplayGrid = SetGridT;
    }
  	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}


    arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
    turnPage.arrDataCacheSet = arrDataSet;
    turnPage.pageIndex = 0;
	turnPage.strQuerySql = strSQL;
	var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function getChangeComCode(cObj,cName)
{
  var unity = fm.all('Unity').value;
  if (unity ==null || trim(unity) == '')
  {
  	alert("请先选择统计层级！");
  	return false;
  } 
    var mlength; 
    if(unity=='01'){
    	mlength = 2;
    }else if(unity =='02'){
    	mlength = 4;	
    }else if(unity >= '03'){
    	mlength = 8;
    }
	var msql ="1 and char(length(trim(comcode)))= #"+mlength+"#";
	showCodeList('comcode',[cObj,cName],[0,1],null,msql,1,1);
}

//提交，保存按钮对应操作
function ListExecl()
{
  if(!beforeSubmit()){
	   return false;
	}
  var unity = fm.all('Unity').value;
  if(unity<='03'){
      fm.all('BranchAttr').value = '';
  }else if(unity=='04'){
  	if(!getBranchattr('03',fm.all('BranchAttr').value)){
    	   return false;
    }
  }else if(unity=='05'){
     if(!getBranchattr('02',fm.all('BranchAttr').value)){
    	   return false;
    	}
  }else if(unity=='06'){
     if(!getBranchattr('01',fm.all('BranchAttr').value)){
    	   return false;
    	}
  }
  fm.submit();
}


//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
