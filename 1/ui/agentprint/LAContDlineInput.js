/** 
 * 程序名称：LAContInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-20 18:05:58
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick()
{
    if (!verifyInput())
     return false;
	if (document.fm.SpareMonths.value.length>0){
	  if(!isNumeric(document.fm.SpareMonths.value))
	  {
		  alert('剩余有效月份内请填写数字!');
		  return false;
	  }
	}
	var tSysDate = document.fm.SysDate.value;
	//此处书写SQL语句
	var tReturn = parseManageComLimitlike();
	var strSql = "select a.ManageCom,a.ProtocolNo, a.AgentCom,(select name from lacom where agentcom=a.agentcom),"
	+"a.SignDate,a.StartDate,a.EndDate, (year(date(a.EndDate)-current date)*12+month(date(a.EndDate)-current date)+1 ) "
	+"from LACont a  "
	+"where a.AgentCom in (select b.AgentCom from LACom  b where b.ACType in ('02','03','04','99') and b.branchtype='2')"
	+ tReturn
    + getWherePart("ProtocolNo", "ProtocolNo")
    + getWherePart("SignDate", "SignDate")
    + getWherePart("ManageCom", "ManageCom","like")
    + getWherePart("AgentCom", "AgentCom")
    + getWherePart("EndDate", "StartDate",'>=')
    + getWherePart("EndDate", "EndDate",'<=');
  if (document.fm.SpareMonths.value.length>0)
  {
    strSql += " and date('"+tSysDate+"')+"+document.fm.SpareMonths.value+" months >= a.enddate and date('"+tSysDate+"') < a.enddate";
  }
    strSql=strSql+" order by a.managecom,a.enddate desc,a.AgentCom,a.ProtocolNo "
	turnPage.queryModal(strSql, LAContGrid);
 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据！");
    return false;
    }
}
function showOne(parm1, parm2) {
  //判断该行是否确实被选中
  //alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}

function submitForm()
{
   if (!verifyInput())
     return false;
   if (document.fm.SpareMonths.value.length>0){
	  if(!isNumeric(document.fm.SpareMonths.value))
	  {
		  alert('剩余有效月份内请填写数字!');
		  return false;
	  }
	}
  var tSysDate = document.fm.SysDate.value;
	//此处书写SQL语句
	var tReturn = parseManageComLimitlike();
  var strSQL = "";
  strSQL =  "select a.ManageCom,a.ProtocolNo, a.AgentCom,(select name from lacom where agentcom=a.agentcom),"
	+"a.SignDate,a.StartDate,a.EndDate,(year(date(a.EndDate)-current date)*12+month(date(a.EndDate)-current date) +1) "
	+"from LACont a  "
	+"where a.AgentCom in (select b.AgentCom from LACom  b where b.ACType in ('02','03','04','99') and b.branchtype='2')"
	+" and a.managecom like '"+document.fm.ManageCom.value+"%'"
    + getWherePart("ProtocolNo", "ProtocolNo")
    + getWherePart("SignDate", "SignDate")
   // + getWherePart("ManageCom", "ManageCom","like")
    + getWherePart("AgentCom", "AgentCom")
    + getWherePart("EndDate", "StartDate",'>=')
    + getWherePart("EndDate", "EndDate",'<=');
     if (document.fm.SpareMonths.value.length>0)
  {
    strSQL += " and date('"+tSysDate+"')+"+document.fm.SpareMonths.value+" months >= a.enddate and date('"+tSysDate+"') < a.enddate";
  }
  fm.querySql.value = strSQL;

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
  fm.target = "f1print";
  fm.submit();
  showInfo.close();
}