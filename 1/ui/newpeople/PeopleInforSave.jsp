<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>

<%
//程序名称：PeopleInforSave.jsp
//程序功能：
//创建日期：2018-04-13
//创建人  ：zxs
//更新记录：  
%>

<!--用户校验类-->
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.agentassess.*"%>
<%@page import="com.sinosoft.lis.newpeople.*"%>

<%
// 输出参数

  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  String tOperate=request.getParameter("hideOperate");
  System.out.println("hideOperate:----"+request.getParameter("hideOperate"));
  System.out.println("operater:--"+tOperate);
  String name  = request.getParameter("name");
  String no = request.getParameter("no");
  System.out.println("no:--"+no);
  String sexType = request.getParameter("sexType");  
  String birthday = request.getParameter("birthday");
  String RgtType = request.getParameter("RgtType");
  String idno = request.getParameter("idno");
  String comment = request.getParameter("comment");
  
  String makeDate = request.getParameter("makeDate");
  String makeTime = request.getParameter("makeTime");
  
  
  PeopleInforBL peoBL = new PeopleInforBL();
  NewPeopleTaskTableSchema tNewPeopleTaskTableSchema = new NewPeopleTaskTableSchema();
  
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  
  tNewPeopleTaskTableSchema.setName(name);
  tNewPeopleTaskTableSchema.setSex(sexType);
  tNewPeopleTaskTableSchema.setBirthday(birthday);
  tNewPeopleTaskTableSchema.setIDType(RgtType);
  tNewPeopleTaskTableSchema.setIDNo(idno);
  tNewPeopleTaskTableSchema.setRemark(comment);
  tNewPeopleTaskTableSchema.setSerialNo(no);
  if(makeDate!=null&&makeDate!=""){
	  tNewPeopleTaskTableSchema.setMakeDate(makeDate);
  }
  if(makeTime!=null&&makeTime!=""){
	  tNewPeopleTaskTableSchema.setMakeTime(makeTime);
  }
  
//准备传输数据 VData
 VData tVData = new VData();
 FlagStr="";
	tVData.addElement(tNewPeopleTaskTableSchema);
	tVData.add(tGI);
	
	
 try
 {
	 peoBL.submitData(tVData,tOperate);
	 tVData.clear();
 }
 catch(Exception ex)
 {
   Content = "保存失败，原因是:" + ex.toString();
   FlagStr = "Fail";
 }

 if (!FlagStr.equals("Fail"))
 {
   tError = tNewPeopleTaskTableSchema.mErrors;
   if (!tError.needDealError())
   {
   	Content = " 保存成功! ";
   	FlagStr = "Succ";
   }
   else
   {
   	Content = " 保存失败，原因是:" + tError.getFirstError();
   	FlagStr = "Fail";
   }
 }
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

