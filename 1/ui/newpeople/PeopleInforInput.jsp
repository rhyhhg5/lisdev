<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="PeopleInforInit.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
//程序名称：PeopleInforInput.jsp
//程序功能：人员信息查询界面
//创建时间：2018-04-12
//查询信息
%>

<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="PeopleInfor.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>人员信息查找</title>
</head>
<body onload="initForm();">
	<form action="" method=post name=fm target="fraSubmit">
		<table>
			<TR>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,queryInput);"></td>
				<td class=titleImg>查询条件</td>
			</TR>
		</table>
		<table class=common id="queryInput" style="display: ''">
			<tr class=common>
				<td class=title>姓名</td>
				<td class=input><input class=common type=text name=name></td>

				<TD class=title8>性别</TD>
				
				<TD class=input8><input class="codeno" CodeData="0|2^0|男^1|女"
					name=sexType
					ondblclick="return showCodeListEx('sexType',[this,sexTypeName],[0,1],null,null,null,1);"
					onkeydown="QueryOnKeyDown();"
					onkeyup="return showCodeListKeyEx('sexType',[this,sexTypeName],[0,1]);"><input
					class=codename name=sexTypeName></TD>
					
			<!--  
				<TD class=input8><Input class="codeno" name=sexType
					ondblclick="showCodeList('sexType', [this, sexTypeName], [0, 1]);"
					onkeyup=" showCodeListKey('sexType', [this, sexTypeName] , [0, 1]);">
					<Input class=codename name=sexTypeName></TD>
            -->
				<td class=title8>出生日期</td>
				<!--<td  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=birthday onkeydown="QueryOnKeyDown()"></td>
	  -->
				<td class=input><Input class="coolDatePicker" name=birthday>
				</td>

			</tr>

			<tr class=common>
				<TD class=title8>证件类型</TD>
				<TD class=input8><input class="codeno"
					CodeData="0|3^1|身份证^2|户口本^3|其他" name=RgtType
					ondblclick="return showCodeListEx('RgtType',[this,idtype],[0,1],null,null,null,1);"
					onkeydown="QueryOnKeyDown();"
					onkeyup="return showCodeListKeyEx('RgtType',[this,idtype],[0,1]);"><input
					class=codename name=idtype></TD>
				<td class=title>证件号码</td>
				<td class=input><input class=common type=text name=idno></td>
				<td class=title8>创建日期</td>
				<td class=input><input class="coolDatePicker"
					dateFormat="short" name=createdate onkeydown="QueryOnKeyDown()"></td>
			</tr>
		</table>
		<input type=button value="查询" class=cssButton
			onclick="easyQueryClick();">

		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divPersonGrid);">
				</td>
				<td class=titleImg>个人信息</td>
			</tr>
		</table>
		<div id="divPersonGrid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanPersonGrid"></span>
					</td>
				</tr>
			</table>
			<Div id="divPage1" align=center style="display: ''">
				<INPUT CLASS=cssButton VALUE="首页" TYPE=button
					onclick="turnPage.firstPage();"> <INPUT CLASS=cssButton
					VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
				<INPUT CLASS=cssButton VALUE="下一页" TYPE=button
					onclick="turnPage.nextPage();"> <INPUT CLASS=cssButton
					VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
			</div>
		</div>

		<table class=common>
			<tr class=common>
				<input type=button value="新建" class=cssButton
					onclick="createNewPerson()">
				</td>
				<input type=button value="修改" class=cssButton
					onclick="updatePerson()">
				</td>
			</tr>
		</table>
		<Input name=BranchType type=hidden value=''>
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
