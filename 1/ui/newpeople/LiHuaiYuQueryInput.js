//alert("--->LiHuaiYuQueryInput.js  start <---");

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var sql;

function showList(){
	var ManageCom = fm.ManageCom.value;
	var SelectType = fm.SelectType.value;
	if(ManageCom == null){
		alert("组织机构不能为空！");
		return false;
	}
	if(SelectType == null){
		alert("查询层次不能为空！");
		return false;
	}
	initList1Grid();
	initList2Grid();
	if(SelectType == 0){   //客户层
		sql = "select lca.managecom,"
			+"  (select name from ldcom where comcode = lca.managecom),"
			+"  lca.appntno,"
			+"  lca.appntname,"
			+"  (case "
			+"   when lca.appntsex = '0' then '男' "
			+"   when lca.appntsex = '1' then '女' end),"
			+"  (select codename from ldcode where codetype='idtype' and ldcode.code = lca.idtype),"
			+"  lca.idno,"
			+"  ad.mobile,"
			+"  ad.phone,"
			+"  ad.postaladdress,"
			+"  lca.idstartdate,"
			+"  lca.idenddate "
			+" from lcappnt lca,lcaddress ad "
			+" where lca.managecom like '"+ManageCom+"%' "
			+"  and (current date < lca.idstartdate or current date > lca.idenddate) "
			+"  and lca.appntno = ad.customerno "
			+"  and lca.addressno = ad.addressno "
			+"union "
			+"select lci.managecom,"
			+"  (select name from ldcom where comcode = lci.managecom),"
			+"  lci.insuredno,"
			+"  lci.name,"
			+"  (case "
			+"   when lci.sex = '0' then '男' "
			+"   when lci.sex = '1' then '女' end),"
			+"  (select codename from ldcode where codetype='idtype' and ldcode.code = lci.idtype) idtype,"
			+"  lci.idno idno,"
			+"  ad.mobile,"
			+"  ad.phone,"
			+"  ad.postaladdress,"
			+"  lci.idstartdate,"
			+"  lci.idenddate "
			+" from lcinsured lci,lcaddress ad "
			+" where lci.managecom like '"+ManageCom+"%' "
			+"  and (current date < lci.idstartdate or current date > lci.idenddate) "
			+"  and lci.insuredno = ad.customerno "
			+"  and lci.addressno = ad.addressno "
			+"with ur";
		turnPage.pageDivName = "divPage2";
		turnPage.queryModal(sql, List1Grid);
		if (List1Grid.mulLineCount == 0){
			alert("不存在证件已失效的投保人信息");
		}
	}
	if(SelectType == 1){   //保单层
		sql = "select distinct lca.managecom,"
			+"  (select name from ldcom where comcode = lca.managecom),"
			+"  lca.appntno,"
			+"  lcc.appntname,"
			+"  lcc.contno,"
			+"  (select riskname from lmrisk lmr where lmr.riskcode = lcp.riskcode),"
			+"  (select codename from ldcode where codetype='idtype' and ldcode.code = lca.idtype),"
			+"  lca.idno,"
			+"  lca.idstartdate,"
			+"  lca.idenddate,"
			+"  ad1.mobile,"
			+"  ad1.phone,"
			+"  lci.insuredno,"
			+"  lcc.insuredname,"
			+"  (select codename from ldcode where codetype='idtype' and ldcode.code = lci.idtype),"
			+"  lci.idno,"
			+"  lci.idstartdate,"
			+"  lci.idenddate,"
			+"  ad2.mobile,"
			+"  ad2.phone,"
			+"  lcc.prem,"
			+"  (select codename from ldcode where codetype='stateflag' and ldcode.code = lcc.stateflag),"
			+"  (select codename from ldcode where codetype='salechnl' and ldcode.code = lcc.salechnl),"
			+"  laa.groupagentcode,"
			+"  laa.name"
			+" from lcappnt lca,lcinsured lci,lccont lcc,lcpol lcp,lcaddress ad1,lcaddress ad2,laagent laa "
			+" where (lca.managecom like '"+ManageCom+"%' "
			+"   and (current date < lca.idstartdate or current date > lca.idenddate) "
			+"   and lca.appntno = ad1.customerno "
			+"   and lca.addressno = ad1.addressno "
			+"   and lca.appntno = lcc.appntno "
			+"   and lcc.stateflag <> '0' "
			+"   and lcc.contno = lcp.contno "
			+"   and lcc.agentcode = laa.agentcode "
			+"   and lcc.insuredno = lci.insuredno "
			+"   and lci.insuredno = ad2.customerno "
			+"   and lci.addressno = ad2.addressno) "
			+"  or (lci.managecom like '"+ManageCom+"%' "
			+"   and (current date < lci.idstartdate or current date > lci.idenddate) "
			+"   and lci.insuredno = ad2.customerno "
			+"   and lci.addressno = ad2.addressno "
			+"   and lci.insuredno = lcc.insuredno "
			+"   and lcc.stateflag <> '0' "
			+"   and lcc.contno = lcp.contno "
			+"   and lcc.agentcode = laa.agentcode "
			+"   and lcc.appntno = lca.appntno "
			+"   and lca.appntno = ad1.customerno "
			+"   and lca.addressno = ad1.addressno) "
			+"  order by lca.managecom,lca.appntno,lcc.contno,riskname,lci.insuredno "
			+"with ur";
		turnPage1.pageDivName = "divPage3";
		turnPage1.queryModal(sql, List2Grid);
		if (List2Grid.mulLineCount == 0){
			alert("不存在证件已失效的投保人信息");
		}
	}
}

function printList(){
	if (List1Grid.mulLineCount == 0 && List2Grid.mulLineCount == 0){
		alert("没有需要打印的清单");
		return false;
	}else{
		fm.all("sql").value = "";
		fm.all("sql").value = sql;
        fm.action = "./LiHuaiYuListPrintSubmit.jsp";
		fm.target = "_blank";
		fm.submit();
	}
}


//alert("--->LiHuaiYuQueryInput.js   end  <---");