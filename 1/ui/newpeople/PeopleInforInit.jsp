
<%
	//程序名称：PeopleInforInit.jsp
	//程序功能：
	//创建日期：2018-04-12 
	//创建人  ：zxs
	//
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
%>

<script language="JavaScript">
	function initPersonGrid() {
		try {
			var iArray = new Array();

			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 100; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "姓名"; //列名
			iArray[1][1] = "100px"; //列宽
			iArray[1][2] = 100; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[2] = new Array();
			iArray[2][0] = "性别"; //列名
			iArray[2][1] = "100px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "出生日期"; //列名
			iArray[3][1] = "100px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许,0表示不允许

			iArray[4] = new Array();
			iArray[4][0] = "证件类型"; //列名
			iArray[4][1] = "140px"; //列宽
			iArray[4][2] = 100; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许,0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "证件号码"; //列名
			iArray[5][1] = "100px"; //列宽
			iArray[5][2] = 100; //列最大值
			iArray[5][3] = 0; //是否允许输入,1表示允许,0表示不允许

			iArray[6] = new Array();
			iArray[6][0] = "创建日期"; //列名
			iArray[6][1] = "100px"; //列宽
			iArray[6][2] = 100; //列最大值
			iArray[6][3] = 0; //是否允许输入,1表示允许,0表示不允许

			iArray[7] = new Array();
			iArray[7][0] = "序列号"; //列名
			iArray[7][1] = "0px"; //列宽
			iArray[7][2] = 100; //列最大值
			iArray[7][3] = 0; //是否允许输入,1表示允许,0表示不允许
			iArray[7][21]='serialno';  //列名

			PersonGrid = new MulLineEnter("fm", "PersonGrid");
			PersonGrid.mulLineCount = 10;
			PersonGrid.displayTitle = 1;
			PersonGrid.hiddenPlus = 1;
			PersonGrid.hiddenSubtraction = 1;
			PersonGrid.canChk = 1; //复选框 
			PersonGrid.loadMulLine(iArray);

		} catch (ex) {
			alert("在PeopleInforInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
		}
	}

	function initForm() {
		try {
			//initInpBox();
			initPersonGrid();
		} catch (re) {
			alert("在PeopleInforInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
</script>