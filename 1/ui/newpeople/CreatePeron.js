//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var mSwitch = parent.VD.gVSwitch;
var mOperate;
window.onfocus = myonfocus;

//校验字段信息
function beforeSumbit(){
	if(fm.name.value == null || fm.name.value == "" || fm.name.value == "null"){
		alert("姓名不可以为空！");
		fm.name.focus();
		return false;
	}
	if(fm.sexTypeName.value == null || fm.sexTypeName.value == "" || fm.sexTypeName.value == "null"){
		alert("性别不可以为空！");
		fm.sexTypeName.focus();
		return false;
	}

	if(fm.birthday.value == null || fm.birthday.value == "" || fm.birthday.value == "null"){
		alert("出生日期不可以为空！");
		fm.birthday.focus();
		return false;
	}

	if(fm.idtype.value == null || fm.idtype.value == "" || fm.idtype.value == "null"){
		alert("证件类型不可以为空！");
		fm.idtype.focus();
		return false;
	}
	if(fm.idno.value == null || fm.idno.value == "" || fm.idno.value == "null"){
		alert("证件号码不可以为空！");
		fm.idno.focus();
		return false;
	}
	return true;
}

//保存人员
function saveClick(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSumbit()){
		return false;
	}
	mOperate = fm.hideOperate.value;
	if(mOperate=='INSERT||MAIN'){
		fm.submit();
	}else{
		alert("请规范操作数据！");
	}
	if (fm.hideOperate.value==""){
		alert("操作控制数据丢失！");
	}	
}

//修改人员
function updateClick(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSumbit()){
		return false;
	}	
	mOperate = fm.hideOperate.value;
	if(mOperate=='UPDATE||MAIN'){
		if (confirm("您确实想修改该记录吗?"))
		{
			fm.submit();
		}
		else
		{
			alert("您取消了修改操作！");
		}
	}else{
		alert("先选择要修改的记录！");
	}
	if (fm.hideOperate.value==""){
		alert("操作控制数据丢失！");
	}


}
//删除人员
function deleteClick(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSumbit()){
		return false;
	}	
	mOperate = fm.hideOperate.value;
	if(mOperate=='UPDATE||MAIN'){
		fm.hideOperate.value = 'DELETE||MAIN';
		mOperate=='DELETE||MAIN';
		fm.submit();
	}else{
		alert("先选择要删除的记录！");
	}
	if (fm.hideOperate.value==""){
		alert("操作控制数据丢失！");
	}	
}


function afterSubmit( FlagStr, content){
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		//执行下一步操作
	}

}
