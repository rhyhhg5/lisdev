<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certifymanage.CertifyBatchFinishBL"%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
String FlagStr = "Fail";
String Content = "";

String tBatchNo = request.getParameter("BatchNo");
GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("BatchNo", tBatchNo);
	
    tVData.add(tTransferData);
    tVData.add(tGI);
    
	CertifyBatchFinishBL tCertifyBatchFinishBL = new CertifyBatchFinishBL();
    if(!tCertifyBatchFinishBL.submitData(tVData, "finish"))
    {
    	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
        System.out.println(tCertifyBatchFinishBL.getErrors().getFirstError());
        Content = "结束征订失败，原因是: " + tCertifyBatchFinishBL.getErrors().getFirstError() + "_" + time;;
        FlagStr = "Fail";
    }
    else
    {
        Content = "处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception ex)
{
	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
    Content = "结束征订失败：" + ex.getMessage() + "_" +  time;
    FlagStr = "Fail";
    ex.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>" , "finish");
</script>
</html>
