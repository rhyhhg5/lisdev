<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.certifymanage.CertifyFeeDetailBL"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	String FlagStr = "";
	String Content = "";
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");

	String batchNo = request.getParameter("BatchNo");
	
	String tOutXmlPath = application.getRealPath("vtsfile") + "/";

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("OutXmlPath", tOutXmlPath);
	tTransferData.setNameAndValue("BatchNo", batchNo);

	try {
		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);
		
		CertifyFeeDetailBL tCertifyFeeDetailBL = new CertifyFeeDetailBL();
		if (!tCertifyFeeDetailBL.submitData(vData, "")) {
			Content = "报表下载失败，原因是:" + tCertifyFeeDetailBL.getErrors().getFirstError();
			FlagStr = "Fail";
		} else {
			tOutXmlPath = tCertifyFeeDetailBL.getZipPath();
			String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
			File file = new File(tOutXmlPath);

			response.reset();
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=" + FileName + "");
			response.setContentLength((int) file.length());

			byte[] buffer = new byte[10000];
			BufferedOutputStream output = null;
			BufferedInputStream input = null;
			//写缓冲区
			try {
				output = new BufferedOutputStream(response.getOutputStream());
				input = new BufferedInputStream(new FileInputStream(file));

				int len = 0;
				while ((len = input.read(buffer)) > 0) {
					output.write(buffer, 0, len);
				}
				input.close();
				output.close();
			} catch (Exception e) {
				e.printStackTrace();
			} // maybe user cancelled download
			finally {
				if (input != null) {
					input.close();
				}
				if (output != null) {
					output.close();
				}
				file.delete();
			}
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	}

	if (!FlagStr.equals("Fail")) {
		Content = "";
		FlagStr = "Succ";
	}
%>
<html>
	<script language="javascript">  
    	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
	</script>
</html>
