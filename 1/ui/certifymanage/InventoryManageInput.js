
// 界面初始化
var turnPage = new turnPageClass();
var detailTurnPage = new turnPageClass(); 


function init(){
	var query = "select batchno,startdate,"
			+ "case temptypes when '00' then '全国版' else '地方版' end,"
			+ "case when exists (select 1 from LZPrintList where batchno=cbs.batchno and dealstate='01') then '尚未完成入库' when exists (select 1 from LZPrintList where batchno=cbs.batchno and dealstate='02') then '入库完成' else '尚未进行印刷通知单确认' end,"
			+ "case when exists (select 1 from LZSendOutList where batchno=cbs.batchno and dealstate='01')then '尚未完成出库' when exists (select 1 from LZSendOutList where batchno=cbs.batchno and dealstate='02')then '出库完成' else '尚未进行寄发通知单确认' end,'征订结束' "
			+ "from LZcertifyBatchState cbs where batchstate='02' and comcode ='"+fm.ManageCom.value+"'and "
			+ "(exists (select 1 from LZPrintList where batchno=cbs.batchno and dealstate='01') or " 
			+ "exists (select 1 from LZSendOutList where batchno=cbs.batchno and dealstate='01'))";
	turnPage.queryModal(query, CertifyBatchGrid);
}

// 入库处理
function incomeDeal(){
	if(!check())
	{
    	return false;
  	} 
	showIncome();
}

// 出库处理
function sendOutDeal(){
	if(!check())
	{
    	return false;
  	} 
	showSendOut();
}

// 入库信息查询
function queryCCertifyList(){

	if(isNull(fm.BatchNo.value)){
		alert("请选择待处理征订批次！");
		return;
	}

	var query = "select batchno,certifycode,(select certifyname from LMCardDescription where certifycode=LZPrintList.certifycode),startno,endno,sumcount,(select name from ldcom where comcode='" + fm.ManageCom.value+ "'),case dealstate when '01' then '未入库' when '02' then '已入库' end,serialno " 
			+ " from LZPrintList "
			+ " where batchno='" + fm.BatchNo.value + "' " 
			+ getWherePart('CertifyCode','CCertifyCode')
			+ getWherePart('DealState','CState');
	detailTurnPage.queryModal(query, CertifyListGrid);
}

// 入库处理
function income(){
	fm.action = "InventoryIncomeSave.jsp";
	fm.DealType.value = "ONLY";
	submitData();
}

// 全部入库处理
function incomeAll(){
	fm.action = "InventoryIncomeSave.jsp";
	fm.DealType.value = "BATCH";
	submitData();
}

function sendOut(){
	fm.action = "InventorySendOutSave.jsp";
	fm.DealType.value = "ONLY";
	submitData();
}

function sendOutCom(){
	fm.action = "InventorySendOutSave.jsp";
	fm.DealType.value = "COMBATCH";
	submitData();
}

function sendOutAll(){
	fm.action = "InventorySendOutSave.jsp";
	fm.DealType.value = "BATCH";
	submitData();
}

// 出库信息查询
function querySCertifyList(){

	if(isNull(fm.BatchNo.value)){
		alert("请选择待处理征订批次！");
		return;
	}

	var query = "select batchno,certifycode,(select certifyname from LMCardDescription where certifycode=LZSendOutList.certifycode),startno,endno,sumcount,(select name from ldcom where comcode=LZSendOutList.comcode),case dealstate when '01' then '未出库' when '02' then '已出库' end,serialno " 
			+ " from LZSendOutList "
			+ " where batchno='" + fm.BatchNo.value + "' " 
			+ getWherePart('CertifyCode','SCertifyCode')
			+ getWherePart('DealState','SState')
			+ getWherePart('ComCode','SCertifyCom','like');
	detailTurnPage.queryModal(query, CertifyListGrid);
}

// 点击批次
function batchClick(){
	var batchNo = CertifyBatchGrid.getRowColData(CertifyBatchGrid.getSelNo()-1 , 1);
	fm.BatchNo.value = batchNo;
}

// 显示入库
function showIncome(){
	fm.all("divIncome").style.display = "block";
	fm.all("divCertifyListGrid").style.display = "block";
	
	fm.all("divSendOut").style.display = "none";
	
	clearInput();
	
	fm.CState.value = "01";
	fm.CStateName.value = "未入库";
}

// 显示出库
function showSendOut(){
	fm.all("divSendOut").style.display = "block";
	fm.all("divCertifyListGrid").style.display = "block";
	
	fm.all("divIncome").style.display = "none";
	
	clearInput();
	
	fm.SState.value = "01";
	fm.SStateName.value = "未出库";
}

// 录入框重置
function clearInput(){
	fm.SCertifyCode.value = "";
	fm.SState.value = "";
	fm.SCertifyCom.value = "";
	fm.CCertifyCode.value = "";
	fm.CState.value = "";
	fm.SStateName.value = "";
	fm.CStateName.value = "";
	
	CertifyListGrid.clearData("CertifyListGrid");
}

// 公用为空校验
function isNull(str){
	if(str == null || str.trim() == "" || str == "null"){
		return true;
	}
	return false;
}

// 提交
function submitData(){
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
/**********************校验 ************************/
function check()
{	
	if(fm.ManageCom.value.length!=2 && fm.ManageCom.value.length!= 4)
	{
		alert("您无权限进行该操作！！！");
        return false;
	}
	return true ;
}

// 返回
function afterSubmit(FlagStr, Content, Type){

	showInfo.close();
	
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	
	if(Type == "Income"){
		init();
		queryCCertifyList();
	} else {
		init();
		querySCertifyList();
	}
	
	for( i=0;i<CertifyBatchGrid.mulLineCount;i++)
	{	
		if(fm.BatchNo.value== CertifyBatchGrid.getRowColData(i, 1))
		{
			CertifyBatchGrid.radioBoxSel(i+1);
			batchClick();	
		}
	}

}