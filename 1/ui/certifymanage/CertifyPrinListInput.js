var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

/*****************初始化征订批次信息************************/
function queryBatchInfo(){

	if(fm.ComCode.value.length == 2){
		var query = "select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,"
			      + " comcode from LZcertifyBatchState cbs where batchstate='02' and temptypes='00' and comcode like '"+fm.ComCode.value +"%' ";
		turnPage.queryModal(query, CertifyBatchGrid);
	} else {
		var query = "select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,"
		          + "comcode from LZcertifyBatchState cbs where batchstate='02' and temptypes='01'and comcode like '"+fm.ComCode.value +"%'";
		turnPage.queryModal(query, CertifyBatchGrid);
	}
}


/*****************查询导入单证信息************************/
function queryCertify(batchNo)
{  
	if(!init())
	{
    	return false;
  	} 
	strSQL = "select lcb.BatchNo,lcb.CertifyCode,lcd.certifyname ,double(lcd.price) ,char(integer((select max(endno) from LZPrintList where certifycode = lcb.CertifyCode))+1),"
			+"char(((integer((select max(endno) from LZPrintList where certifycode = lcb.CertifyCode))+1) + "
			+"( select sum(SumCount) from LZCertifyBatchInfo  lzb  where certifycode = lcb.certifycode "
			+ "and batchno= lcb.batchno  and exists (select 1 from  LZCertifyBatchMain  where batchno= lzb.batchno "
			+ "and ImportBatchNo= lzb.ImportBatchNo and stateflag in  ('00','01'))))-1)"
			+ ",'',lcd.printcode,"
			 + " (select printname from  LMPrintCom  where printcode=lcd.printcode )  ,"
			 + "( select sum(SumCount) from LZCertifyBatchInfo  lzb  where certifycode = lcb.certifycode and batchno= lcb.batchno  and exists (select 1 from  LZCertifyBatchMain  where batchno= lzb.batchno and ImportBatchNo= lzb.ImportBatchNo and stateflag in  ('00','01')))" 	 
			 +" from LZCertifyBatchInfo  lcb , LMCardDescription  lcd " 
			 +" where lcb.CertifyCode= lcd.CertifyCode  and lcb.batchno='"+batchNo+"' "
			 +" group by lcb.BatchNo,lcb.CertifyCode ,lcd.certifyname, lcd.price  ,lcd.PrintCode  with ur";
		
	turnPage1.queryModal(strSQL, CertifyList);
	         
	if (!turnPage1.strQueryResult) {
		alert("该批次下尚未导入数据！！！");
		return false;
	}
}

/*****************添加印刷通知单************************/
function addCertifyBatchList()
{	
	if(!init())
	{
    	return false;
  	} 
	if(!checkLZPrintList())
	{
    	return false;
  	}
     fm.all('OperateType').value ="INSERT";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}

/*****************删除印刷通知单证信息************************/
function delCertifyBatchList()
{	if(!init())
	{
    	return false;
  	} 	
	if(!checkLZPrintList())
	{
    	return false;
  	} 
     fm.all('OperateType').value ="DELETE";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}

/*****************印刷通知单制定完毕************************/
function updateCertifyBatchList()
{	
	if(!init())
	{
    	return false;
  	} 	
	if(!checkLZPrintList())
	{
    	return false;
  	}  
     fm.all('OperateType').value ="UPDATE";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}

/*****************查询已添加印刷通知单信息************************/
function queryCertifyBatchList(batchNo)
{   
	strSQL = "select SerialNo, batchno ,CertifyCode,(select  Certifyname from   LMCardDescription  where certifycode= LZPrintList.certifycode ), "
		  + "price ,totalprice ,StartNo ,endno ,remark ,(select  printname from LMPrintCom where printcode= LZPrintList.printcode),SumCount"	
		  + "  from LZPrintList  where 1=1  and batchno='"+batchNo+"'"
		  + " with ur"	;
	
	turnPage2.queryModal(strSQL, CertifyStoreList);

}


/*****************下载印刷通知单************************/
function downPrintList()
{   
	if(!init())
	{
    	return false;
  	} 
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    if(fm.PrintCode.value== null || fm.PrintCode.value=="" ){
    	
    	alert("印刷厂不能为空！！！");
    	return false;
    }
    
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
    var tBatchNo = tRowDatas[0];
    var strSQL = "select 1 from LZPrintList where batchno = '"+tBatchNo+"' and DealState='00'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次印刷通知单尚未进行确认，您不能进行操作！！！");
        return false;
    }

    fm.action="./CertifyPrinListDown.jsp";
	fm.submit(); //提交
}


/***************** 点击征订批次************************/
function batchClick(){
	
	var batchNo = CertifyBatchGrid.getRowColData(CertifyBatchGrid.getSelNo()-1 , 1);
	fm.BatchNo.value = batchNo;
	
	CertifyList.clearData();
	CertifyStoreList.clearData();
	
	 if(!checkComCode()){
    	return false;
  	  }
	queryCertify(batchNo);
	queryCertifyBatchList(batchNo);

}

/*****************校验管理机构************************/
function checkComCode()
{   
 	var tBatchNo = CertifyBatchGrid.getRowColData(CertifyBatchGrid.getSelNo()-1 , 1);
 	var tSQL=" select comcode from  LZCertifyBatchState  where batchno='"+tBatchNo+"'";
 	
 	var arrResult = easyExecSql(tSQL);
  	
 	if(fm.ComCode.value!=arrResult[0][0]){
 		
 		alert("您没有权限对该批次进行操作，请重新选择！！！");
    	return false;
 	
 	}
	return true;
}

/*****************校验印刷通知单是否已确认************************/
function checkLZPrintList()
{	
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    var tBatchState = tRowDatas[3];
    
    var strSQL = "select 1 from LZPrintList where batchno = '"+tBatchNo+"' and DealState='01'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次印刷通知单已确认，您不能进行操作！！！");
        return false;
    }
    
    return true;
}









/**********************初始化 ************************/
function init()
{	
	if(fm.ComCode.value.length!=2 && fm.ComCode.value.length!= 4)
	{
		alert("您无权限进行该操作！！！");
        return false;
	}
	return true ;
}

/*****************提交后的操作************************/
function afterSubmit(FlagStr,Content,operate)
{  
	if (operate =="INSERT"||operate =="DELETE")
	{
	   batchClick();
	 }
	showInfo.close();
    window.focus; 
	if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" +Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" +  Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }     
	
}

/***** 时时更新 ********/
function oftenUpdate(){

	var tRow = CertifyList.getRowColData(CertifyBatchGrid.getSelNo()-1 , 10);
	alert(tRow);
}
