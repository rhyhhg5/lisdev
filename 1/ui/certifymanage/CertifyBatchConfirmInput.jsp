<%@ page contentType="text/html;charset=GBK"%>

<html>
	<head>
		<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
		<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
		<script src="../common/javascript/Common.js"></script>
		<script src="../common/cvar/CCodeOperate.js"></script>
		<script src="../common/javascript/MulLine.js"></script>
		<script src="../common/Calendar/Calendar.js"></script>
		<script src="../common/javascript/VerifyInput.js"></script>

		<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

		<script src="CertifyBatchConfirmInput.js"></script>
		<%@include file="CertifyBatchConfirmInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;"
							onclick="showPage(this, divCertifyBatchGrid)" />
					</td>
					<td class="titleImg">
						征订批次
					</td>
				</tr>
			</table>
			<div id="divCertifyBatchGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyBatchGrid"></span>
						</td>
					</tr>
				</table>
				<br>
				
				<div id="divCertifyListGridPage" style="display: ''" align="center">
					<input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />
				</div>
				
				<input class="cssButton" type="button" value="结束征订" onclick="finishBatch();" />
				<br>
				<hr>
				<br>
			</div>
			<div id="divCertifyListGrid" style="display: ''">
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;"/>
						</td>
						<td class="titleImg">
							机构导入情况
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td class="title">
							征订批次
						</td>
						<td class="title">
							<input class="readonly" name="BatchNo" readonly>
						</td>
						<td>
						</td>
						<td>
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyListGrid"></span>
						</td>
					</tr>
				</table>
				<div id="divCertifyListGridPage" style="display: ''" align="center">
					<input type="button" class="cssButton" value="首  页" onclick="comTurnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="comTurnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="comTurnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="comTurnPage.lastPage();" />
				</div>
				<br>
				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="审核确认" onclick="comConfirm();" />
							<input class="cssButton" type="hidden" value="全部确认" onclick="allConfirm();" />
						</td>
					</tr>
				</table>
				<hr>
				<br>
			</div>
			<div id="divCertifyDetailGrid" style="display: ''">
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;"/>
						</td>
						<td class="titleImg">
							机构导入明细
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyDetailGrid"></span>
						</td>
					</tr>
				</table>
				<div id="divCertifyListGridPage" style="display: ''" align="center">
					<input type="button" class="cssButton" value="首  页" onclick="detailTurnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="detailTurnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="detailTurnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="detailTurnPage.lastPage();" />
				</div>
			</div>
			<input type=hidden name=ManageCom>
			<input type=hidden name=DealType>
			<input type=hidden name=SerialNo >
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
