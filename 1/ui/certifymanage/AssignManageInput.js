
// 界面初始化
var turnPage = new turnPageClass();

var startFlag = false;

// 初始化
function init(){
	
}

function queryInventory(){
	
	if(isNull(fm.CSendCom.value)){
		alert("请录入发放机构！");
		return;
	}
	
	var wherePath = "";
	
	if(!isNull(fm.queryCertifyName.value)){
		wherePath = " and exists (select 1 from LMCardDescription where CertifyCode=ci.certifycode and CertifyName like '%" + fm.queryCertifyName.value + "%')";
	}

	var query = "select " 
		+ " certifycode,(select CertifyName from LMCardDescription where CertifyCode=ci.certifycode),startno,endno,sumcount,prtno " 
		+ " from LZCertifyInventory ci " 
		+ " where comcode='" + fm.CSendCom.value + "'" 
		+ getWherePart("certifycode","queryCertify","like")
		+ wherePath;
	turnPage.queryModal(query, InventoryGrid);
}

// 点选添加
function inventoryClick(){

	var selRowNo = InventoryGrid.getSelNo() - 1;
		
	var certify = InventoryGrid.getRowColDataByName(selRowNo,"CertifyCode");
	var start = InventoryGrid.getRowColDataByName(selRowNo,"StartNo");
		
	for(var index = 0; index < CertifyListGrid.mulLineCount; index++){
		if(certify == CertifyListGrid.getRowColDataByName(index,"CertifyCode")){
			if(start == CertifyListGrid.getRowColDataByName(index,"StartNo")){
				return;
			}
		}
	}
		
	CertifyListGrid.addOne();
		
	var maxRow = CertifyListGrid.mulLineCount - 1;
	CertifyListGrid.setRowColDataByName(maxRow,"CertifyCode",InventoryGrid.getRowColDataByName(selRowNo,"CertifyCode"));
	CertifyListGrid.setRowColDataByName(maxRow,"CertifyName",InventoryGrid.getRowColDataByName(selRowNo,"CertifyName"));
	CertifyListGrid.setRowColDataByName(maxRow,"StartNo",InventoryGrid.getRowColDataByName(selRowNo,"StartNo"));
	CertifyListGrid.setRowColDataByName(maxRow,"EndNo",InventoryGrid.getRowColDataByName(selRowNo,"EndNo"));
	CertifyListGrid.setRowColDataByName(maxRow,"SumCount",InventoryGrid.getRowColDataByName(selRowNo,"SumCount"));
	CertifyListGrid.setRowColDataByName(maxRow,"PrtNo",InventoryGrid.getRowColDataByName(selRowNo,"PrtNo"));
}

function sendCom(){
	fm.action = "SendOutManageSave.jsp";
	fm.DealType.value = "04";
	submitData();
}

// 公用为空校验
function isNull(str){
	if(str == null || str.trim() == "" || str == "null"){
		return true;
	}
	return false;
}

// 提交
function submitData(){
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

// 返回
function afterSubmit(FlagStr, Content, Type){

	showInfo.close();
	
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	
	queryInventory();
	
	if(FlagStr != "Fail"){
		CertifyListGrid.clearData("CertifyListGrid");
	}
}