
<%
	// 防止IE缓存页面
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<%@page contentType="text/html;charset=GBK"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="CertifySendListInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="CertifySendListInit.jsp"%>
	</head>
	<body onload="initForm()" style="behavior:url(#default#clientCaps)"
		id="oClientCaps">
		<form action="CertifySendListSave.jsp" method="post" name=fm
			target="fraSubmit">
			<!-- 单证列表 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divCertifyBatchGrid);">
					</td>
					<td class=titleImg>
						征订批次
					</td>
				</tr>
			</table>

			<div id="divCertifyBatchGrid">
				<table class="common">
					<tr class="common">
						<td text-align: left colSpan=1>
							<span id="spanCertifyBatchGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display:''">
				<INPUT CLASS=cssbutton VALUE="首页" TYPE=button
					onclick="turnPage.firstPage();">
				<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button
					onclick="turnPage.previousPage();">
				<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button
					onclick="turnPage.nextPage();">
				<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button
					onclick="turnPage.lastPage();">
			</Div>
			<!-- 单证列表 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divCertifyList);">
					</td>
					<td class=titleImg>
						单证列表
					</td>
				</tr>
			</table>

			<div id="divCertifyList">
				<table class="common">
					<tr class="common">
						<td text-align: left colSpan=1>
							<span id="spanCertifyList"></span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display:''">
				<INPUT CLASS=cssbutton VALUE="首页" TYPE=button
					onclick="turnPage1.firstPage();">
				<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button
					onclick="turnPage1.previousPage();">
				<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button
					onclick="turnPage1.nextPage();">
				<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button
					onclick="turnPage1.lastPage();">
			</Div>

			<tr>

				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="添  加"
								onclick="addCertifyBatchList();" />
						</td>
					</tr>

				</table>
				<hr>
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divShowStore);">
						</td>
						<td class=titleImg>
							已添加印刷通知单信息
						</td>
					</tr>
				</table>


				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="删  除"
								onclick="delCertifyBatchList();" />
						</td>
					</tr>
				</table>
				<div id="divShowStore" style="display:''">
					<table class="common">
						<tr class="common">
							<td text-align: left colSpan=1>
								<span id="spanCertifyStoreList"></span>
							</td>
						</tr>
					</table>
				</div>
				<Div id="divPage" align=center style="display:''">
					<INPUT CLASS=cssbutton VALUE="首页" TYPE=button
						onclick="turnPage2.firstPage();">
					<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button
						onclick="turnPage2.previousPage();">
					<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button
						onclick="turnPage2.nextPage();">
					<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button
						onclick="turnPage2.lastPage();">
				</Div>
				<hr>
				<p>
				<div id="divSendOutInfo">
						<table class=common>
					<tr class=common>
						<td class=title>
							印刷厂
						</td>
						<td class=input><input class=codeno name=PrintCode style="display:''"ondblclick="return showCodeList('PrintCode', [this,PrintCodeName1],[0,1]);"onkeyup="return showCodeListKeyEx('PrintCode', [this,PrintCodeName1],[0,1]);"><input name=PrintCodeName1 class=codename></td>
						<td class=title></td>
						<td class=input></td>
						</tr>
				</table>
				</div>
				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="寄发通知单确认"
								onclick="updateSendOutList();" />
						</td>
						<td class="common">
							<input class="cssButton" type="button" value="下载寄发通知单"
								onclick="downSendOutList();" />
						</td>
					</tr>
				</table>
				<input type=hidden name=OperateType class=common>
				<input type=hidden name=ComCode class=common>
				<input type=hidden name=BatchNo class=common>
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>

	</body>
</html>
