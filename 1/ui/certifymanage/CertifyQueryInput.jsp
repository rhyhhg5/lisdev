<%@ page contentType="text/html;charset=GBK"%>

<html>
	<head>
		<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
		<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
		<script src="../common/javascript/Common.js"></script>
		<script src="../common/cvar/CCodeOperate.js"></script>
		<script src="../common/javascript/MulLine.js"></script>
		<script src="../common/Calendar/Calendar.js"></script>
		<script src="../common/javascript/VerifyInput.js"></script>

		<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

		<script src="CertifyQueryInput.js"></script>
		<%@include file="CertifyQueryInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
		
			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;"
							onclick="showPage(this, divInventoryGrid)" />
					</td>
					<td class="titleImg">
						库存信息
					</td>
				</tr>
			</table>
			<div id="divInventoryGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td class="title">
							单证编码
						</td>
						<td class="input">
							<input class="common" name="CertifyCode"/>
						</td>
						<td class="title">
							管理机构
						</td>
						<td class="input">
							<input class="codeNo" name="ComCode" ondblclick="return showCodeList('certifymanageall',[this,ComCodeName],[0,1]);" onkeyup="return showCodeListKey('certifymanageall',[this,ComCodeName],[0,1]);"/><input class="codename" name="ComCodeName" readonly="readonly" elementtype="nacessary" />
						</td>
					</tr>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="查  询" onclick="queryInventory();" />
						</td>
					</tr>
				</table>
			</div>
			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;"
							onclick="showPage(this, divInventoryBatchGrid)" />
					</td>
					<td class="titleImg">
						库存汇总信息
					</td>
				</tr>
			</table>
			<div id="divInventoryBatchGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanInventoryGrid"></span>
						</td>
					</tr>
				</table>
				<div align="center">
					<input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />
				</div>
				<br>
			</div>
			
			<div id="divCertifyListGrid" style="display: ''">
				<hr>
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;"/>
						</td>
						<td class="titleImg">
							单证列表
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyListGrid"></span>
						</td>
					</tr>
				</table>
				<div align="center">
					<input type="button" class="cssButton" value="首  页" onclick="detailTurnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="detailTurnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="detailTurnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="detailTurnPage.lastPage();" />
				</div>
			</div>
			
			<input type=hidden name=ManageCom>
			<input type=hidden name=ManageComName>
			<input type=hidden name=DealType>
			
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
