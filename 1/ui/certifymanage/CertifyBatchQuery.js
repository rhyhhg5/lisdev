

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询批次信息。
 */
function queryCertifyBatch(){
	if(fm.ComCode.value.length == 2){
		var query = "select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,comcode from LZcertifyBatchState cbs where batchstate='01' and temptypes='00' and batchno = (select max(batchno) from LZcertifyBatchState ) ";
		turnPage.queryModal(query, CardRiskGrid);
	} else {
		var query = "select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,comcode from LZcertifyBatchState cbs where batchstate='01' and batchno = (select max(batchno) from LZcertifyBatchState ) ";
		turnPage.queryModal(query, CardRiskGrid);
	}
}
function ShowDetail(){
    	
	queryCertifyBatchMain();
}

function queryCertifyBatchMain(){	
	
	var rowNum = CardRiskGrid.getSelNo();
	var tBatchNo = CardRiskGrid.getRowColData(rowNum-1 , 1); 
	 
	 var tStrSql = ""
        + " select BatchNo,ComCode,StateFlag,(case StateFlag when '00'then '未审核' when '01' then '已审核'   else '其他' end ) "
        + " from LZCertifyBatchMain "
        + " where 1 = 1  and batchno= '"+ tBatchNo+"' "
        + "  group by ComCode,StateFlag,BatchNo  with ur "
	turnPage1.pageDivName = "divCertifyStoreListPage";
    turnPage1.queryModal(tStrSql, CertifyStoreList);
    return true; 
}

/**
 * 进入批次导入界面。
 */
function importBatch()
{
    var tRow = CardRiskGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    if(fm.ComCode.value=="86")
    {
    	alert("总公司不能进行导入操作！！！");
        return false;
    }
    var tRowDatas = CardRiskGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    var tBatchState = tRowDatas[3];
    var tComCode = tRowDatas[5];
    var str = fm.ComCode.value.substr(0,4);
    if(tComCode!='86'&&tComCode!=str)
    {
    	alert("该批次征订机构为"+tComCode+",您不能进行导入操作！！！");
        return false;
    }
    
    var strSQL = "select 1 from LZCertifyBatchMain where batchno = '"+tBatchNo+"'  and comcode='"+fm.ComCode.value+"'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
   	{   
        alert("该机构已导入数据，不能再次进行导入！！");
        return false;
   	}
    var tStrUrl = "./CertifyListEximportInput.jsp"
        + "?BatchNo=" + tBatchNo
        ;

    window.location = tStrUrl;
}


/**
 * 下载清单
 */
function downCertifyBatch()
{   
    var tRow = CardRiskGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = CardRiskGrid.getRowData(tRow); 
    var tBatchNo = tRowDatas[0];
    
    fm.all('mBatchNo').value =tBatchNo;
    fm.submit();
}


/**
 * 查询已申请批次号。
 */
function queryCertifyBatchList()
{	
    if(fm.ComCode.value.length == 2){
    var tStrSql = ""
        + " select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,comcode from LZcertifyBatchState cbs where batchstate='01' and temptypes='00' "
        + getWherePart("BatchNo", "BatchNo")
        + " with ur "
        ;
	turnPage.queryModal(tStrSql, CardRiskGrid);    
	}else{
	var query = "select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,comcode from LZcertifyBatchState cbs where batchstate='01'"
	+ getWherePart("BatchNo", "BatchNo")
	+ "with ur";
		turnPage.queryModal(query, CardRiskGrid);
	}
    if (!turnPage.strQueryResult)
    {
        alert("未查询到批次信息！");
        return false;
    }
    
    return true;
}

