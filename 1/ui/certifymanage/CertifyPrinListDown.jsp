<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：BatchPayQueryList.jsp
	//程序功能：清单下载
	//创建日期：2009-06-18
	//创建人  ：yanjing
	//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	System.out.println("进入下载界面CertifyPrinListDown.jsp");
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));

	//取得前台传入数据
	String tBatchno = request.getParameter("BatchNo");
	String tPrintCode = request.getParameter("PrintCode");

	//生成文件
	CreateExcelListEx createexcellist = new CreateExcelListEx("");//指定文件名
	createexcellist.createExcelFile();
	String[] sheetName = { "list" };
	createexcellist.addSheet(sheetName);

	String downLoadFileName = "";
	String filePath = "";
	String tOutXmlPath = "";
	String querySql = "";

	//文件名
	downLoadFileName = "印刷通知单" + "" + tBatchno + "_" + tPrintCode + "_"
			+ min + sec + ".xls";
	filePath = application.getRealPath("temp");
	tOutXmlPath = filePath + File.separator + downLoadFileName;
	System.out.println("OutXmlPath:" + tOutXmlPath);

	//隐藏字段提供查询sql

	querySql = "select lzp.CertifyCode,(select certifyname  from  LMCardDescription  where certifycode = lzp.certifycode ), "
			+ " lzp.price, lzp.TotalPrice ,lzp.startno ,lzp.endno, lzp.remark ,(select printname from  LMPrintCom where printcode = lzp.printcode),"
			+ " lzp.sumcount"
			+ " from LZPrintList lzp "
			+ " where lzp.Batchno ='"
			+ tBatchno
			+ "' and  lzp.printcode='"
			+ tPrintCode
			+ "' "
			+ " and lzp.dealstate='01'  with ur ";

	System.out.println(querySql);

	//设置表头
	String[] tTitle = { "单证编码", "单证名称", "单价", "总价", "印刷起号", "印刷止号",
			"备注", "印刷厂", "征订数量" };

	//数据的显示属性(指定对应列是否显示在清单中)
	int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8 , 9};
	if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
		errorFlag = true;
	}

	if (!errorFlag) {
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}

	out.clear();
	out = pageContext.pushBody();
%>

