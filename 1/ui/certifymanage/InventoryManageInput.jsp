<%@ page contentType="text/html;charset=GBK"%>

<html>
	<head>
		<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
		<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
		<script src="../common/javascript/Common.js"></script>
		<script src="../common/cvar/CCodeOperate.js"></script>
		<script src="../common/javascript/MulLine.js"></script>
		<script src="../common/Calendar/Calendar.js"></script>
		<script src="../common/javascript/VerifyInput.js"></script>

		<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

		<script src="InventoryManageInput.js"></script>
		<%@include file="InventoryManageInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;"
							onclick="showPage(this, divCertifyBatchGrid)" />
					</td>
					<td class="titleImg">
						征订批次
					</td>
				</tr>
			</table>
			<div id="divCertifyBatchGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyBatchGrid"></span>
						</td>
					</tr>
				</table>
				
				<div id="divCertifyListGridPage" style="display: ''" align="center">
					<input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />
				</div>
				
				<input class="cssButton" type="button" value="入库处理" onclick="incomeDeal();" />
				<input class="cssButton" type="button" value="出库处理" onclick="sendOutDeal();" />
			</div>
			<br>
			<hr>
			<br>
			<div id="divIncome" style="display:'none'">
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;" />
						</td>
						<td class="titleImg">
							入库处理
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td class="title">
							单证编码
						</td>
						<td class="input">
							<input class="common" name="CCertifyCode" />
						</td>
						<td class="title">
							入库状态
						</td>
						<td class="input">
							<input class="codeNo" name="CState" CodeData="0|^01|未入库^02|已入库"
								ondblclick="return showCodeListEx('Income',[this,CStateName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('Income',[this,CStateName],[0,1],null,null,null,1);"
								readonly="readonly" /><input class="codename" name="CStateName" readonly="readonly" elementtype="nacessary" />
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="查  询" onclick="queryCCertifyList();" />
							<input class="cssButton" type="button" value="入  库" onclick="income();" />
							<input class="cssButton" type="hidden" value="全部入库" onclick="incomeAll();" />
						</td>
					</tr>
				</table>
				<br>
			</div>
			<div id="divSendOut" style="display:'none'">
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;" />
						</td>
						<td class="titleImg">
							出库处理
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td class="title">
							单证编码
						</td>
						<td class="input">
							<input class="common" name="SCertifyCode" />
						</td>
						<td class="title">
							征订机构
						</td>
						<td class="input">
							<input class="codeNo" name="SCertifyCom" 
								ondblclick="return showCodeList('CertifyComName',[this,SCertifyComName],[0,1],null,null,fm.BatchNo.value,1);"
								onkeyup="return showCodeList('CertifyComName',[this,SCertifyComName],[0,1],null,null,fm.BatchNo.value,1);"
								/><input class="codename" name="SCertifyComName" readonly="readonly" elementtype="nacessary" />
						</td> 
						<td class="title">
							出库状态
						</td>
						<td class="input">
							<input class="codeNo" name="SState" CodeData="0|^01|未出库^02|已出库"
								ondblclick="return showCodeListEx('Income',[this,SStateName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('Income',[this,SStateName],[0,1],null,null,null,1);"
								readonly="readonly" /><input class="codename" name="SStateName" readonly="readonly" elementtype="nacessary" />
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="查  询" onclick="querySCertifyList();" />
							<input class="cssButton" type="button" value="出  库" onclick="sendOut();" />
							<input class="cssButton" type="hidden" value="机构全部出库" onclick="sendOutCom();" />
							<input class="cssButton" type="hidden" value="全部出库" onclick="sendOutAll();" />
						</td>
					</tr>
				</table>
				<br>
			</div>
			<div id="divCertifyListGrid" style="display: 'none'">
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;"/>
						</td>
						<td class="titleImg">
							单证列表
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyListGrid"></span>
						</td>
					</tr>
				</table>
				<div id="divCertifyListGridPage" style="display: ''" align="center">
					<input type="button" class="cssButton" value="首  页" onclick="detailTurnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="detailTurnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="detailTurnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="detailTurnPage.lastPage();" />
				</div>
			</div>
			<input type=hidden name=BatchNo>
			<input type=hidden name=ManageCom>
			<input type=hidden name=DealType>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
