<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="CertifyTemplateBatchApply.js"></script>
    <%@include file="CertifyTemplateBatchApplyInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="CertifyTemplateBatchApplySave.jsp" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">请输入查询条件：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">批次号</td>
                    <td class="input8">
                        <input class="common" name="BatchNo" />
                    </td>
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('certifycomcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('certifycomcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    </tr>
                  <tr class=common>
            		<td class=title>起始日期</td>
            		<td class=input><input class="coolDatePicker" name=startDate ></td>
           			<td class=title>终止日期</td>
            		<td class=input><input class="coolDatePicker" name=endDate ></td>
        		  </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryCertifyBatchList();" /> 	
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnBatchNoApply" name="btnBatchNoApply" value="申  请" onclick="applyImportBatch();" />    
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportBatchListGrid);" />
                </td>
                <td class="titleImg">待处理批次</td>
            </tr>
        </table>
        <div id="divImportBatchListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanImportBatchListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>        
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnImport" value="制定模版" onclick="importBatch();" />   
                </td>
            </tr>
        </table>
        
         <input type=hidden name= OperateType  class=common>
         <input type=hidden name= ComCode  class=common>
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
