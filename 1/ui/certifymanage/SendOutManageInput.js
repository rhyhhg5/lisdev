
// 界面初始化
var turnPage = new turnPageClass();
var startFlag = false;

// 初始化
function init() {
	queryInventory();
}
function queryInventory() {
	var wherePath = "";
	if (!isNull(fm.queryCertifyName.value)) {
		wherePath = " and exists (select 1 from LMCardDescription where CertifyCode=ci.certifycode and CertifyName like '%" + fm.queryCertifyName.value + "%')";
	}
	var query = "select " + " certifycode,(select CertifyName from LMCardDescription where CertifyCode=ci.certifycode),startno,endno,sumcount,prtno " + " from LZCertifyInventory ci " + " where comcode='" + fm.ManageCom.value + "'" + getWherePart("certifycode", "queryCertify", "like") + wherePath;
	turnPage.queryModal(query, InventoryGrid);
}

// 发放机构
function sendComDeal() {
	if (!check()) {
		return false;
	}
	showSendCom();
	startFlag = true;
}

// 发放业务员
function sendPersonDeal() {
	if (!check()) {
		return false;
	}
	showSendPerson();
	startFlag = true;
}

// 显示机构
function showSendCom() {
	fm.all("divSendCom").style.display = "block";
	fm.all("divCertifyListGrid").style.display = "block";
	fm.all("divSendPerson").style.display = "none";
	clearInput();
	fm.CSendCom.value = fm.ManageCom.value;
	fm.CSendComName.value = fm.ManageComName.value;
}

// 显示业务员
function showSendPerson() {
	fm.all("divSendPerson").style.display = "block";
	fm.all("divCertifyListGrid").style.display = "block";
	fm.all("divSendCom").style.display = "none";
	clearInput();
	fm.PSendCom.value = fm.ManageCom.value;
	fm.PSendComName.value = fm.ManageComName.value;
}

// 录入框重置
function clearInput() {
	fm.CSendCom.value = "";
	fm.CSendComName.value = "";
	fm.CReceiveCom.value = "";
	fm.CReceiveComName.value = "";
	fm.PSendCom.value = "";
	fm.PSendComName.value = "";
	fm.PReceivePerson.value = "";
	fm.PReceivePersonName.value = "";
	CertifyListGrid.clearData("CertifyListGrid");
}

// 点选添加
function inventoryClick() {
	if (startFlag) {
		var selRowNo = InventoryGrid.getSelNo() - 1;
		var certify = InventoryGrid.getRowColDataByName(selRowNo, "CertifyCode");
		var start = InventoryGrid.getRowColDataByName(selRowNo, "StartNo");
		for (var index = 0; index < CertifyListGrid.mulLineCount; index++) {
			if (certify == CertifyListGrid.getRowColDataByName(index, "CertifyCode")) {
				if (start == CertifyListGrid.getRowColDataByName(index, "StartNo")) {
					return;
				}
			}
		}
		CertifyListGrid.addOne();
		var maxRow = CertifyListGrid.mulLineCount - 1;
		CertifyListGrid.setRowColDataByName(maxRow, "CertifyCode", InventoryGrid.getRowColDataByName(selRowNo, "CertifyCode"));
		CertifyListGrid.setRowColDataByName(maxRow, "CertifyName", InventoryGrid.getRowColDataByName(selRowNo, "CertifyName"));
		CertifyListGrid.setRowColDataByName(maxRow, "StartNo", InventoryGrid.getRowColDataByName(selRowNo, "StartNo"));
		CertifyListGrid.setRowColDataByName(maxRow, "EndNo", InventoryGrid.getRowColDataByName(selRowNo, "EndNo"));
		CertifyListGrid.setRowColDataByName(maxRow, "SumCount", InventoryGrid.getRowColDataByName(selRowNo, "SumCount"));
		CertifyListGrid.setRowColDataByName(maxRow, "PrtNo", InventoryGrid.getRowColDataByName(selRowNo, "PrtNo"));
	}
}
function sendCom() {
	fm.action = "SendOutManageSave.jsp";
	fm.DealType.value = "02";
	submitData();
}
function sendPerson() {
	fm.action = "SendOutManageSave.jsp";
	fm.DealType.value = "03";
	submitData();
}

// 公用为空校验
function isNull(str) {
	if (str == null || str.trim() == "" || str == "null") {
		return true;
	}
	return false;
}

// 提交
function submitData() {
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
function check() {
	if (fm.ManageCom.value.length == 2) {
		alert("\u603b\u516c\u53f8\u4e0d\u80fd\u8fdb\u884c\u53d1\u653e\u5355\u8bc1\u64cd\u4f5c\uff01\uff01!");
		return false;
	}
	return true;
}
// 返回
function afterSubmit(FlagStr, Content, Type) {
	showInfo.close();
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + Content;
	showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	queryInventory();
	if (FlagStr != "Fail") {
		CertifyListGrid.clearData("CertifyListGrid");
	}
}

