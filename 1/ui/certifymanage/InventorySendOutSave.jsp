<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certifymanage.InventorySendOutBL"%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
String FlagStr = "Fail";
String Content = "";

String tBatchNo = request.getParameter("BatchNo");
String tDealType = request.getParameter("DealType");
String tCertifyCom = request.getParameter("SCertifyCom");

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("BatchNo", tBatchNo);
    tTransferData.setNameAndValue("CertifyCom", tCertifyCom);
    
    String tChk[] = request.getParameterValues("InpCertifyListGridChk");
  	String tSerialno[] = request.getParameterValues("CertifyListGrid9");

	LZSendOutListSet tLZSendOutListSet = new LZSendOutListSet();

	if(tChk != null && tSerialno != null){
		for(int i = 0; i < tChk.length; i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				LZSendOutListSchema tLZSendOutListSchema = new LZSendOutListSchema();
				tLZSendOutListSchema.setSerialNo(tSerialno[i]);
			
				tLZSendOutListSet.add(tLZSendOutListSchema);
			}
		}
	}
	
    tVData.add(tTransferData);
    tVData.add(tGI);
    tVData.add(tLZSendOutListSet);
    
    InventorySendOutBL tInventorySendOutBL = new InventorySendOutBL();
    if(!tInventorySendOutBL.submitData(tVData, tDealType))
    {
    	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
        System.out.println(tInventorySendOutBL.getErrors().getFirstError());
        Content = "入库失败，原因是: " + tInventorySendOutBL.getErrors().getFirstError() + "_" + time;;
        FlagStr = "Fail";
    }
    else
    {
        Content = "处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception ex)
{
	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
    Content = " 入库失败：" + ex.getMessage() + "_" + time;;
    FlagStr = "Fail";
    ex.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "SendOut");
</script>
</html>
