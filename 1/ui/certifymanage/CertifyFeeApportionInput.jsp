<%@ page contentType="text/html;charset=GBK"%>

<html>
	<head>
		<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
		<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
		<script src="../common/javascript/Common.js"></script>
		<script src="../common/cvar/CCodeOperate.js"></script>
		<script src="../common/javascript/MulLine.js"></script>
		<script src="../common/Calendar/Calendar.js"></script>
		<script src="../common/javascript/VerifyInput.js"></script>

		<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

		<script src="CertifyFeeApportionInput.js"></script>
		<%@include file="CertifyFeeApportionInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;"
							onclick="showPage(this, divCertifyBatchGrid)" />
					</td>
					<td class="titleImg">
						征订批次
					</td>
				</tr>
			</table>
			<table class="common">
				<tr class="common">
					<td class="title">
						征订批次号
					</td>
					<td class="input">
						<input class="common" name="BatchNo" />
					</td>
					   <td class="title">征订日期</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="StartDate" verify="申请日期|date" />
                    </td>
				</tr>
			</table>
			<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="查  询" onclick="query();" />
						</td>
					</tr>
				</table>
				
				
				
			<div id="divCertifyBatchGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyBatchGrid"></span>
						</td>
					</tr>
				</table>
				<br>
			</div>
			
			<div id="divInventoryBatchGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanInventoryGrid"></span>
						</td>
					</tr>
				</table>
				<div align="center">
					<input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />
				</div>
				<br>
			</div>
			
			
			<input class="cssButton" type="button" value="下载清单" onclick="download();" />
			<input type=hidden name=ManageCom>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
