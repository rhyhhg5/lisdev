<%
//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
    	fm.all('ManageCom').value = '<%=strManageCom%>';
    	
    	initCertifyBatchGrid();
        initCertifyListGrid();
        initCertifyDetailGrid();
        if(!check())
		{
    	return false;
  		} 
        init();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

function initCertifyBatchGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="征订批次号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="征订日期";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="征订结束日期";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="全国/地方版";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="征订状态";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="征订机构";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;



        CertifyBatchGrid = new MulLineEnter("fm", "CertifyBatchGrid");

        CertifyBatchGrid.mulLineCount = 0;   
        CertifyBatchGrid.displayTitle = 1;
        CertifyBatchGrid.canSel = 1;
        CertifyBatchGrid.hiddenSubtraction = 1;
        CertifyBatchGrid.hiddenPlus = 1;
        CertifyBatchGrid.canChk = 0;
        CertifyBatchGrid.loadMulLine(iArray);
        CertifyBatchGrid.selBoxEventFuncName = "batchClick";
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}


function initCertifyListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="导入批次号";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="导入机构编码";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="导入机构名称";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="审核状态";
        iArray[4][1]="100";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="操作序号";
        iArray[5][1]="0px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        

        CertifyListGrid = new MulLineEnter("fm", "CertifyListGrid");

        CertifyListGrid.mulLineCount = 0;   
        CertifyListGrid.displayTitle = 1;
        CertifyListGrid.canSel = 1;
        CertifyListGrid.hiddenSubtraction = 1;
        CertifyListGrid.hiddenPlus = 1;
        CertifyListGrid.canChk = 0;
        CertifyListGrid.loadMulLine(iArray);
        CertifyListGrid.selBoxEventFuncName = "comBatchClick";
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}


function initCertifyDetailGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="导入批次号";
        iArray[1][1]="150px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="单证编码";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="单证名称";
        iArray[3][1]="150px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="序列号";
        iArray[4][1]="0px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="单证审批数量";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=1;
        
        iArray[6]=new Array();
        iArray[6][0]="单证申请数量";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="ID";
        iArray[7][1]="0px";
        iArray[7][2]=100;
        iArray[7][3]=3;

        CertifyDetailGrid = new MulLineEnter("fm", "CertifyDetailGrid");

        CertifyDetailGrid.mulLineCount = 0;   
        CertifyDetailGrid.displayTitle = 1;
        CertifyDetailGrid.canSel = 0;
        CertifyDetailGrid.hiddenSubtraction = 1;
        CertifyDetailGrid.hiddenPlus = 1;
        CertifyDetailGrid.canChk = 0;
        CertifyDetailGrid.loadMulLine(iArray);
        
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}
</script>

