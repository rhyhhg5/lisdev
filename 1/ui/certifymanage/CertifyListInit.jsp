<%
//程序名称：
//程序功能：征订模版管理
//创建日期：2013-07-15
//创建人  ：WDK
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strOperator = globalInput.Operator;
	String strCurTime = PubFun.getCurrentDate();
	String strCom = globalInput.ComCode;
	String strBatchNo = request.getParameter("BatchNo");
	String strTempTypes = request.getParameter("TempTypes");
%>

<script language="JavaScript">

var mBatchNo = "<%=strBatchNo%>";
var mTempTypes = "<%=strTempTypes%>";


function initForm()
{
  try
  { 
    initCertifyList();
    initCertifyStoreList()
    fm.BatchNo.value= mBatchNo;
    fm.TempTypes.value= mTempTypes; 
    queryCertify();
    queryCertifyBatchList();
 
  }
  catch(re)
  {
    alert("CertifySendOutInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 起始单号、终止单号信息列表的初始化
function initCertifyList()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="单证名称";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="60";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="是否有价";    	  //列名
      iArray[3][1]="60";            	//列宽
      iArray[3][2]=180;            		//列最大值
      iArray[3][3]=1;              		//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="适用机构";    	  //列名
      iArray[4][1]="80";            	//列宽
      iArray[4][2]=180;            		//列最大值
      iArray[4][3]=1;              		//是否允许输入,1表示允许，0表示不允许

       
     
      iArray[5]=new Array();
      iArray[5][0]="单证类型";    	      //列名
      iArray[5][1]="80";            	//列宽
      iArray[5][2]=50;   //列最大值
      iArray[5][3]=1;              		//是否允许输入,1表示允许，0表示不允许

     
      iArray[6]=new Array();
      iArray[6][0]="印刷厂代码";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[6][1]="0";            	//列宽
      iArray[6][2]=50;            		//列最大值
      iArray[6][3]=1;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[7]=new Array();
      iArray[7][0]="印刷厂";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[7][1]="80";            	//列宽
      iArray[7][2]=50;            		//列最大值
      iArray[7][3]=1;              		//是否允许输入,1表示允许，0表示不允许  
      
      CertifyList = new MulLineEnter( "fm" , "CertifyList" );
      //这些属性必须在loadMulLine前
      CertifyList.displayTitle = 1;
      CertifyList.mulLineCount = 1;   
      CertifyList.hiddenPlus = 1;        
      CertifyList.hiddenSubtraction = 1; 
      CertifyList.canChk = 1; 
      CertifyList.unlocked=0;   
      CertifyList.loadMulLine(iArray);

      
      
      //CertifyList.delBlankLine("CertifyList");

    } catch(ex) {
      alert(ex);
    }
}

function initCertifyStoreList()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="流水号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="0";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单证编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="60";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="单证名称";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="60";        			//列宽
      iArray[3][2]=80;          			//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许


      CertifyStoreList = new MulLineEnter( "fm" , "CertifyStoreList" );
      //这些属性必须在loadMulLine前
      CertifyStoreList.displayTitle = 1;
      CertifyStoreList.mulLineCount = 0;   
      CertifyStoreList.hiddenPlus = 1;        
      CertifyStoreList.hiddenSubtraction = 1; 
      CertifyStoreList.canChk = 1; 
      CertifyStoreList.unlocked=0;   
      CertifyStoreList.loadMulLine(iArray);
      
      //CertifyList.delBlankLine("CertifyList");

    } catch(ex) {
      alert(ex);
    }
}


function getComName(comC)
{
	var strSQL="select name from LDCom where comcode= '"+ comC +"' ";
	var comN = easyExecSql(strSQL);	
	return comN[0][0];
	//if (comN)
	//{	
	//	fm.all("ManageComName").value= comN[0][0];
	//}
}

</script>