<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：BatchPayQueryList.jsp
	//程序功能：清单下载
	//创建日期：2009-06-18
	//创建人  ：yanjing
	//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));
	
    //取得前台传入数据
	String tBatchno = request.getParameter("mBatchNo");
	
	System.out.println(tBatchno);
	
	
	//生成文件
	CreateExcelListEx createexcellist = new CreateExcelListEx("");//指定文件名
	createexcellist.createExcelFile();
	String[] sheetName = { "list" };
	createexcellist.addSheet(sheetName);
	
	String downLoadFileName = "";
	String filePath = "" ;
	String tOutXmlPath = "";
	String querySql = "";
	
		//文件名
		downLoadFileName = ""+tBatchno +"_" + tG.Operator + "_" + min + sec
				+ ".xls";
		filePath = application.getRealPath("temp");
		tOutXmlPath = filePath + File.separator + downLoadFileName;
		System.out.println("OutXmlPath:" + tOutXmlPath);

		//隐藏字段提供查询sql
		
		querySql = "select lct.CertifyCode,lcd.certifyname,(case lcd.HavePrice when 'Y' then '是' when 'N' then '否' else '其他' end ) ,"
			      +" (case lcd.CertifyType when '00' then '全国' when '01' then '地方' else '其他' end ) ,'',lcd.price,"
			      +" (select PrintName from LMPrintCom where printcode= lcd.printcode), "
			      + " (select sum(sumcount) from LZCertifyInventory where LZCertifyInventory.certifycode = lct.certifycode and comcode = '"+ tG.ComCode +"' ) , " 
			      +" '','' "
			  	  +" from LZCertifyTemplate lct ,LMCardDescription lcd "
			 	  +" where lct.CertifyCode = lcd.CertifyCode "
			      +" and lct.Batchno ='"+tBatchno+"' "
			      +" with ur ";
		
		System.out.println(querySql);
		
		//设置表头
		String[] tTitle = { "单证编码", "单证名称", "是否有价", "适用机构", "单证号码", "印刷价格","印刷厂","库存数量","征订数量","征订机构"};

		//数据的显示属性(指定对应列是否显示在清单中)
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7,8,9,10};
		if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
			errorFlag = true;
		}	
	
	if (!errorFlag) {
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}

	out.clear();
	out = pageContext.pushBody();
%>

