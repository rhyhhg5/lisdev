<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certifymanage.SendOutManageBL"%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<!-- 发放和调拨都使的这一个页面 -->

<%
String FlagStr = "Fail";
String Content = "";

String tDealType = request.getParameter("DealType");

String tCSendCom = request.getParameter("CSendCom");
String tCReceiveCom = request.getParameter("CReceiveCom");
String tPSendCom = request.getParameter("PSendCom");
String tPReceivePerson = request.getParameter("PReceivePerson");

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("CSendCom", tCSendCom);
    tTransferData.setNameAndValue("CReceiveCom", tCReceiveCom);
    tTransferData.setNameAndValue("PSendCom", tPSendCom);
    tTransferData.setNameAndValue("PReceivePerson", tPReceivePerson);
    
  	String tCertifyCode[] = request.getParameterValues("CertifyListGrid1");
	String tStartNo[] = request.getParameterValues("CertifyListGrid3");
	String tEndNo[] = request.getParameterValues("CertifyListGrid4");
	String tSumCount[] = request.getParameterValues("CertifyListGrid5");
	String tPrtNo[] = null;
	System.out.println(request.getParameterValues("CertifyListGrid6"));
	if(request.getParameterValues("CertifyListGrid6") != null){
		tPrtNo = request.getParameterValues("CertifyListGrid6");
	}
	System.out.println("kk" + tPrtNo[0]);
	

	LZCertifyInventorySet tLZCertifyInventorySet = new LZCertifyInventorySet();

	if(tCertifyCode != null){
		for(int i = 0; i < tCertifyCode.length; i++){
			LZCertifyInventorySchema tLZCertifyInventorySchema = new LZCertifyInventorySchema();
			tLZCertifyInventorySchema.setCertifyCode(tCertifyCode[i]);
			System.out.println("222222222");
			if(tStartNo[i]!= null && !tStartNo.equals("")){
				
				tLZCertifyInventorySchema.setStartNo(tStartNo[i]);
				System.out.println("33333333333");
			}
			if(tEndNo[i] != null && !tEndNo.equals("")){
				tLZCertifyInventorySchema.setEndNo(tEndNo[i]);
				System.out.println("44444444");
			}
			tLZCertifyInventorySchema.setSumCount(tSumCount[i]);
			System.out.println("5555555555");

			if(request.getParameterValues("CertifyListGrid6") != null){
				tLZCertifyInventorySchema.setPrtNo(tPrtNo[i]);
				System.out.println("66666666666");
				System.out.println("1111111111111" + tPrtNo[i] + "数量" + tSumCount);
			}
			System.out.println("99999999");
			tLZCertifyInventorySet.add(tLZCertifyInventorySchema);
			System.out.println("+++++++++++++++++");
		}
	}
	
    tVData.add(tTransferData);
    tVData.add(tGI);
    tVData.add(tLZCertifyInventorySet);
    
    SendOutManageBL tSendOutManageBL = new SendOutManageBL();
    // tDealType 02-下发机构 03-下发业务员 04-调拨
    if(!tSendOutManageBL.submitData(tVData, tDealType))
    {
    	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
        System.out.println(tSendOutManageBL.getErrors().getFirstError());
        
        Content = "入库失败，原因是: " + tSendOutManageBL.getErrors().getFirstError() + "_" + time;;
        FlagStr = "Fail";
        
    }
    else
    {
        Content = "处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception ex)
{
	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
    Content = " 入库失败：" + ex.getMessage() + "_" + time;;
    FlagStr = "Fail";
    ex.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
