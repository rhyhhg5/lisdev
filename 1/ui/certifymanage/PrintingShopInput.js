var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

/**********************添加印刷厂信息 ************************/
function InsertPrintCom()
{  
	if(!init())
    {   
    	return false;
    }
	if (fm.PrintName.value == null || fm.PrintName.value == "")
	{
		alert('印刷厂名称不能为空！');
		return;
	}
	if (fm.EffectiveState.value == null ||fm.EffectiveState.value == "")
	{
		alert('印刷厂状态不能为空！');
		return;
	}
	
	fm.OperateType.value = "INSERT";
 	var i = 0;
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); 
}

/**********************查询印刷厂信息 ************************/
function QueryPrintCom()
{   
	if(!init())
    {
    	return false ;
    }
	var strSQL=" select  printcode ,printname , EffectiveState,(case EffectiveState when '0' then '有效' when '1' then '无效' else'其他' end ), remark ,linkman , phone , postaladdress from  LMPrintCom where 1=1 "
	         + getWherePart('PrintCode','mPrintCode')
	         + getWherePart('PrintName','mPrintName')  
	         " with ur"; 
  turnPage.queryModal(strSQL, CardRiskGrid);
	
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
}

/**********************修改印刷厂信息 ************************/
function UpdatePrintCom(){
	
	if(!init())
    {
    	return false ;
    }
	var tRow = CardRiskGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    if (fm.tPrintName.value == null || fm.tPrintName.value == "")
	{
		alert('印刷厂名称不能为空！');
		return;
	}
	if (fm.tEffectiveState.value == null ||fm.tEffectiveState.value == "")
	{
		alert('印刷厂状态不能为空！');
		return;
	}
    
	fm.OperateType.value = "UPDATE";
 	var i = 0;
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); 

}

/**********************显示修改明细 ************************/
function ShowDetail(){
	 var tSel = 0;
	 tSel = CardRiskGrid.getSelNo();
		try
		{	
		  if (tSel !=0 )
		  	{
				divLLMainAskInput5.style.display = "";
			}else{
				 divLLMainAskInput5.style.display = "none";
		    }
				 		 
		  fm.all('tPrintCode').value=CardRiskGrid.getRowColData(tSel-1,1);
		  fm.all('tPrintName').value=CardRiskGrid.getRowColData(tSel-1,2);
		  fm.all('tEffectiveState').value=CardRiskGrid.getRowColData(tSel-1,3);
		  fm.all('tRemark').value=CardRiskGrid.getRowColData(tSel-1,5);
		  fm.all('tLinkMan').value=CardRiskGrid.getRowColData(tSel-1,6);
		  fm.all('tPhone').value=CardRiskGrid.getRowColData(tSel-1,7);
   	      fm.all('tPostalAddress').value=CardRiskGrid.getRowColData(tSel-1,8);
		  if(fm.all('tEffectiveState').value == "0"){
		  	fm.StateName.value = "有效";
		  }
		  if(fm.all('tEffectiveState').value == "1"){
		  	fm.StateName.value = "无效";
		  }
		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
}

/**********************初始化 ************************/
function init()
{	
	if(fm.ComCode.value.length!=2 && fm.ComCode.value.length!= 4)
	{
		alert("您无权限进行该操作！！！");
        return false;
	}
	return true ;
}


function afterSubmit(FlagStr,code,operate)
{  
	
	if (operate =="INSERT")
	{
	   fm.PrintCode.value = code;
	 
	 }
	showInfo.close();
    window.focus; 
	if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "操作失败" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" +  "操作成功";
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    QueryPrintCom();
  }     
	
}