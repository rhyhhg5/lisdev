var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var mManageCom= "";

/*****************初始化征订批次信息************************/
function queryBatchInfo(){

	if(fm.ComCode.value.length == 2){
		var query = " select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,"
		          + " comcode from LZcertifyBatchState cbs where batchstate='02' and temptypes='00' and comcode like '"+fm.ComCode.value +"%' ";
		turnPage.queryModal(query, CertifyBatchGrid);
	} else {
		var query = " select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,"
				  + " comcode from LZcertifyBatchState cbs where batchstate='02'and temptypes='01'and comcode like '"+fm.ComCode.value +"%' ";
		turnPage.queryModal(query, CertifyBatchGrid);
	}
}

/*****************查询导入单证信息************************/
function queryCertify(batchNo)
{   
	  mManageCom= fm.ComCode.value;
	  var strSQL="";
  
    if(mManageCom=="86"){
    	
    	strSQL = "select lcb.BatchNo,lcb.CertifyCode,lcd.certifyname ,double(lcd.price) ,"
			 + " ( select sum(SumCount) from LZCertifyBatchInfo lzb where certifycode = lcb.certifycode and batchno = lcb.batchno   and substr(comcode, 1, 4) = substr(lcb.comcode, 1, 4) and exists (select 1 from  LZCertifyBatchMain  where batchno= lzb.batchno and ImportBatchNo= lzb.ImportBatchNo and stateflag in('00','01') )) , "
			 + " substr(lcb.comcode ,1,4), (select name  from ldcom where comcode = substr(lcb.comcode ,1,4)) ,lcd.printcode," 
			 + " (select printname  from  LMPrintCom  where printcode = lcd.printcode ), "
			  + " (select startno from LZPrintList where  batchno= '"+ batchNo +"' and certifycode = lcb.certifycode  ), "
			 +"(select endno from LZPrintList where  batchno= '" + batchNo +"' and certifycode = lcb.certifycode  )  " 
			 + " from LZCertifyBatchInfo  lcb , LMCardDescription  lcd "
			 + " where lcb.CertifyCode= lcd.CertifyCode and lcb.comcode like '"+mManageCom+"%'  and lcb.batchno='"+batchNo+"'"
			 +" group by lcb.BatchNo,lcb.CertifyCode , lcd.certifyname, substr(lcb.comcode ,1,4), lcd.price ,lcd.PrintCode   with ur";
    }else{
    	
    	strSQL = "select lcb.BatchNo,lcb.CertifyCode,lcd.certifyname ,double(lcd.price) ,"
			 + " ( select sum(SumCount) from LZCertifyBatchInfo lzb  where certifycode = lcb.certifycode and batchno = lcb.batchno   and comcode = lcb.comcode and exists (select 1 from  LZCertifyBatchMain  where batchno= lzb.batchno and ImportBatchNo= lzb.ImportBatchNo and stateflag in('00','01'))) , "
			 + " lcb.comcode , (select name  from ldcom where comcode = lcb.comcode) ,lcd.printcode," 
			 + " (select printname  from  LMPrintCom  where printcode = lcd.printcode ), "
			  + " (select startno from LZPrintList where  batchno= '"+ batchNo +"' and certifycode = lcb.certifycode  ), "
			 +"(select endno from LZPrintList where  batchno= '" + batchNo +"' and certifycode = lcb.certifycode  )  " 
			 + " from LZCertifyBatchInfo  lcb , LMCardDescription  lcd "
			 + " where lcb.CertifyCode= lcd.CertifyCode and lcb.comcode like '"+mManageCom+"%'  and lcb.batchno='"+batchNo+"'"
			 +" group by lcb.BatchNo,lcb.CertifyCode , lcd.certifyname, lcb.comcode, lcd.price ,lcd.PrintCode   with ur";
    }
	
	turnPage1.queryModal(strSQL, CertifyList);
	         
	if (!turnPage1.strQueryResult) {
		alert("该批次尚未导入单证信息！！！！");
		return false;
	}
}


/*****************添加寄发通知单************************/
function addCertifyBatchList()
{	 
	if(!init())
	{
    	return false;
  	} 
	if(!checkLZPrintList())
	{
    	return false;
  	} 
     fm.all('OperateType').value ="INSERT";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}
/*****************删除单证信息************************/
function delCertifyBatchList()
{	
	if(!init())
	{
    	return false;
  	} 
	 if(!checkLZPrintList())
	{
    	return false;
  	} 
     fm.all('OperateType').value ="DELETE";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}
/*****************寄发通知单确认************************/
function updateSendOutList()
{	
	if(!init())
	{
    	return false;
  	} 
	if(!checkLZPrintList())
	{
    	return false;
  	} 
  	if(!checkLZPrintNo()){
  		return false;
  	}
  	/**
  	if(!checkLZPrintNo1()){
  		return false;
  	}
  	
  	if(!checkLZPrintNo2()){
  		return false;
  	}
  	if(!checkLZPrintNo3()){
  		return false;
  	}
  	**/
     fm.all('OperateType').value ="UPDATE";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}

/*****************查询已添加印刷通知单信息************************/
function queryCertifyBatchList(batchNo)
{   
   var strSQL="";
   
	strSQL = "select SerialNo, batchno ,CertifyCode,(select  Certifyname from   LMCardDescription  where certifycode= LZSendOutList.certifycode ), "
		  + "price ,totalprice ,SumCount,comcode , (select name  from ldcom where comcode = LZSendOutList.comcode) ,"
		  + "printcode,(select  printname from LMPrintCom where printcode= LZSendOutList.printcode), StartNo ,endno "	
		  + "from LZSendOutList  where 1=1  and batchno='"+batchNo+"'"
          + "with ur"	;
	
	turnPage2.queryModal(strSQL, CertifyStoreList);
	         
}

/*****************下载寄发通知单************************/
function downSendOutList()
{   
	if(!init())
	{
    	return false;
  	} 
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    if(fm.PrintCode.value== null || fm.PrintCode.value=="" ){
    	
    	alert("印刷厂不能为空！！！");
    	return false;
    }
    
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    
    var strSQL = "select 1 from LZSendOutList where batchno = '"+tBatchNo+"' and DealState='00'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次寄发通知单尚未确认，您不能进行操作！");
        return false;
    }

    fm.action="./CertifySendListDown.jsp";
	fm.submit(); //提交
}


/***************** 点击征订批次************************/
function batchClick(){

	var batchNo = CertifyBatchGrid.getRowColData(CertifyBatchGrid.getSelNo()-1 , 1);
	fm.BatchNo.value = batchNo;
	
	CertifyList.clearData();
	CertifyStoreList.clearData();
	
	if(!checkComCode()){
    	return false;
  	  }
	queryCertify(batchNo);
	queryCertifyBatchList(batchNo);

}


/*****************校验管理机构************************/
function checkComCode()
{   
 	var tBatchNo = CertifyBatchGrid.getRowColData(CertifyBatchGrid.getSelNo()-1 , 1);
 	var tSQL=" select comcode from  LZCertifyBatchState  where batchno='"+tBatchNo+"'";
 	
 	var arrResult = easyExecSql(tSQL);
  	
 	if(fm.ComCode.value!=arrResult[0][0]){
 		
 		alert("您没有权限对该批次进行操作，请重新选择！！");
    	return false;
 	
 	}
	return true;
}

/*****************校验印刷通知单是否已确认************************/
function checkLZPrintList()
{	
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    
   
    
    var strSQL = "select 1 from LZSendOutList where batchno = '"+tBatchNo+"' and DealState='01'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次寄发通知单已确认，您不能进行操作！！！");
        return false;
    }
    
    return true;
}

/********* 校验寄发号是否重复(相等情况)  ************/
function checkLZPrintNo(){
	
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    
 
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    
   
    
    var strSQL = "select certifycode , startno , endno  ,count(1) from lzsendoutlist where  batchno = '"+ tBatchNo +"' and dealstate = '00' "
				+ "group by startno ,certifycode ,endno "
 				+ "having count(1) > 1 " 
				+ "with ur";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次单证号有重复，您不能进行操作！");
        return false;
    }
    
    return true;
	
}



/********* 校验寄发号是否重复(左边相等情况)  ************/
function checkLZPrintNo3(){
	
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    
 
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    
   
    
    var strSQL = "select certifycode , startno  ,count(1) from lzsendoutlist where  batchno = '"+ tBatchNo +"' and dealstate = '00' "
				+ "group by startno ,certifycode  "
 				+ "having count(1) >  1 " 
				+ "with ur";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次单证号有重复，您不能进行操作！！");
        return false;
    }
    
    return true;
	
}


/********* 校验寄发号是否重复(右边相等情况)  ************/
function checkLZPrintNo2(){
	
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    
 
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    
   
    
    var strSQL = "select certifycode , endno  ,count(1) from lzsendoutlist where  batchno = '"+ tBatchNo +"' and dealstate = '00' "
				+ "group by certifycode , endno   "
 				+ "having count(1)  > 1 " 
				+ "with ur";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次单证号有重复，您不能进行操作！！！");
        return false;
    }
    
    return true;
	
}



/********* 校验寄发号是否重复(在区间内)  ************/
function checkLZPrintNo1(){
	
	var tRow = CertifyBatchGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    } 
    
 
    var tRowDatas = CertifyBatchGrid.getRowData(tRow);
   
    var tBatchNo = tRowDatas[0];
    
   
    
    var strSQL = "select certifycode ,startno,endno from lzsendoutlist f where batchno = '"+ tBatchNo +"' " 
				+ "and certifycode in (select l.certifycode from lzsendoutlist l where l.batchno = '"+ tBatchNo +"' and certifycode = f.certifycode )  "
				+ "and startno > (select min(startno)  from lzsendoutlist lz where lz.batchno = '"+ tBatchNo +"' and certifycode = f.certifycode ) "
				+ " and startno < (select max(endno)  from lzsendoutlist lz where lz.batchno = '"+ tBatchNo +"' and certifycode = f.certifycode ) "
				+ " and endno > (select min(startno)  from lzsendoutlist lz where lz.batchno = '"+ tBatchNo +"' and certifycode = f.certifycode ) "
				+ " and endno < (select max(endno)  from lzsendoutlist lz where lz.batchno = '"+ tBatchNo +"' and certifycode = f.certifycode ) "
				+ " and dealstate = '00' "; 
				+ "with ur";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次单证号有重复，您不能进行操作！！！！");
        return false;
    }
    
    return true;
	
}


/**********************初始化 ************************/
function init()
{	
	if(fm.ComCode.value.length!=2 && fm.ComCode.value.length!= 4)
	{
		alert("您无权限进行该操作！！！");
        return false;
	}
	return true ;
}
/***************** 提交后续操作************************/
function afterSubmit(FlagStr,Content,operate)
{   
	if (operate =="INSERT"||operate =="DELETE")
	{
	   batchClick();
	 }
	showInfo.close();
    window.focus; 
	if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }     
	
}

function checkSum(){

	 for (i=0; i<CertifyList.mulLineCount; i++) {
    	var	sumCount = CertifyList.getRowColData(i,5);
    	var startNo = CertifyList.getRowColData(i , 10);
    	var endNo = CertifyList.getRowColData(i, 11);
    	
    	if(startNo != "" ){
    		if(parseInt(endNo,10) != (parseInt(sumCount,10) + parseInt(startNo,10) - 1)){
    			alert("第"+ (i+1) +"行起止号数量不匹配，请确定后重新输入！");
    		}
    		CertifyList.setRowColData(i, 11, (parseInt(sumCount,10) + parseInt(startNo,10) - 1).toString());
    	}
    	
  	}

}

