<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ScanContInit.jsp
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
PubFun tpubFun = new PubFun();
String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String Comcode = globalInput.ComCode;
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
                            

function initForm()
{
  try
  {
  	fm.all('ComCode').value = '<%=Comcode%>';
    initCardRiskGrid();
    initCertifyStoreList();
    queryCertifyBatch();
   
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initCardRiskGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="征订批次号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="征订日期";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="征订结束日期";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="全国/地方版";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="征订状态";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="征订机构";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
    

    
  
    CardRiskGrid = new MulLineEnter("fm", "CardRiskGrid");
    CardRiskGrid.mulLineCount = 0;
    CardRiskGrid.displayTitle = 1;
    CardRiskGrid.hiddenPlus = 1;
    CardRiskGrid.hiddenSubtraction = 1;
    CardRiskGrid.canSel = 1;
    CardRiskGrid.canChk = 0;
    CardRiskGrid.loadMulLine(iArray);
    CardRiskGrid.selBoxEventFuncName = "ShowDetail";
  }
  catch(ex)
  {
    alert("初始化时出错2003-05-21"+ex);
  }
}

function initCertifyStoreList()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=1;              		//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="分公司编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="100";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=1;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="批次状态代码";    	    //列名
      iArray[3][1]="0";            		//列宽
      iArray[3][2]=180;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="批次状态";    	    //列名
      iArray[4][1]="80";            		//列宽
      iArray[4][2]=180;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      CertifyStoreList = new MulLineEnter( "fm" , "CertifyStoreList" );
      //这些属性必须在loadMulLine前
      CertifyStoreList.displayTitle = 1;
      CertifyStoreList.mulLineCount = 0;   
      CertifyStoreList.hiddenPlus = 1;        
      CertifyStoreList.hiddenSubtraction = 1; 
      CertifyStoreList.unlocked=0;   
      
      CertifyStoreList.loadMulLine(iArray);
      
      //CertifyList.delBlankLine("CertifyList");

    } catch(ex) {
      alert(ex);
    }
}

</script>