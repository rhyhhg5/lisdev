<%@page contentType="text/html;charset=GBK" %>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.certify.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="CertifyDescribeInput.js"></SCRIPT>
		<%@include file="CertifyDescribeInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./CertifyDescSave.jsp" method=post name=fm
			target="fraSubmit">
			<table>
				<TR>
					<TD>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLLMainAskInput);">
					</TD>
					<TD class=titleImg>
						添加印刷单证描述
					</TD>
				</TR>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>单证编码</td>
					<td class=input><input class=common name=CertifyCode verify="单证编码|NOTNULL" elementtype=nacessary></td>
					<td class=title>单证名称</td>
					<td class=input><input class=common name=CertifyName verify="单证名称|NOTNULL"elementtype=nacessary></td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>单证类型</td>
               	    <td class=input><input class=codeno name=CertifyClass CodeData="0|^P|普通单证^D|定额单证"  ondblClick="showCodeListEx('CertifyClassListNew',[this,CertifyClassName],[0,1],null,null,null,1);" verify="单证类型|NOTNULL"><input class=codename name='CertifyClassName' readonly elementtype=nacessary></td>
					<td class=title><div id="divSubCodeName" style="display:''">单证类型码</div></td>
					<td class=input><div id="divSubCode" style="display:''"><input class=common name=SubCode id=SubCodeId elementtype=nacessary ></div></td>
					</tr>
			<table class=common>
				<tr class=common>
					<td class=title>单证所属类型</td>
               	    <td class=input><input class=codeno name=CertifyType CodeData="0|^00|全国版单证^01|地方版单证"  ondblClick="showCodeListEx('CertifyType',[this,CertifyTypeName],[0,1],null,null,null,1);" verify="单证所属类型|NOTNULL"><input class=codename name='CertifyTypeName' readonly elementtype=nacessary></td>
					<td class=title>管理机构</td>
          <td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('certifycomcode',[this,ManageComName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('certifycomcode',[this,ManageComName2],[0,1],null,null,null,1);" verify="管理机构|NOTNULL" readonly><Input class=codename readonly name=ManageComName2 elementtype=nacessary></td>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>印刷厂</td>
					<td class=input><input class=codeno name= PrintCode  style="display:''"ondblclick="return showCodeList('PrintCode', [this,PrintCodeName],[0,1]);" onkeyup="return showCodeListKeyEx('PrintCode', [this,PrintCodeName],[0,1]);"verify="印刷厂为空|NOTNULL" ><input name=PrintCodeName class=codename  elementtype=nacessary ></td>
					<td class=title>预警线</td>
					<td class=input><input class=common name=WarningNo type="text" verify="预警线|NOTNULL"elementtype=nacessary></td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>是否为有价单证</td>
 		 			<td class=input><Input class=codeno name=HavePrice verify="是否为有价单证|NOTNULL" CodeData="0|^Y|有价单证^N|无价单证" ondblClick="showCodeListEx('HavePrice',[this,HavePriceName],[0,1]);" onkeyup="showCodeListKeyEx('HavePrice',[this,HavePriceName],[0,1]);"><input class=codename name=HavePriceName  elementtype="nacessary"></td>
					<td class=title>印刷单价</td>
					<td class=input><input class=common name=Price  verify="印刷单价|NOTNULL" type="text" elementtype="nacessary"></td>
				</tr>
			</table>
			<table class=common>
				<tr>
					<td class=title>是否为有号单证</td>
					<td class=input><Input class=codeno name=HaveNumber verify="是否为有号单证|NOTNULL" CodeData="0|^Y|有号单证^N|无号单证" ondblClick="showCodeListEx('HaveNumber',[this,HaveNumberName],[0,1]);" onkeyup="showCodeListKeyEx('HaveNumber',[this,HaveNumberName],[0,1]);"><input class=codename name=HaveNumberName  elementtype="nacessary"></td>
					<td class=title>单证号码长度</td>
                	<td class=input><input class=common name=CertifyLength  verify="单证号码长度|NOTNULL"  elementtype="nacessary"></td>
				</tr>
			</table>
			<P>
				<INPUT VALUE="增  加" class=cssButton TYPE=button onclick="InsertCertifyCode();">
			<P>
			<HR>
			<table>
				<TR>
					<TD>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLLMainAskInput3);">
					</TD>
					<TD class=titleImg>
						印刷单证查询及修改
					</TD>
				</TR>
			</table>
			<Div id="divLLMainAskInput3" style="display: ''">
				<table class=common>
					<tr class=common>
						<td class=title>单证编码</td>
						<td class=input><input class=common name=mCertifyCode></td>
						<td class=title>单证名称</td>
						<td class=input><input class=common name=mCertifyName></td>
					</tr>
				</table>
				<INPUT VALUE="查  询" class=cssButton TYPE=button
					onclick="QueryCertifyCode();">
				<INPUT VALUE="修  改" class=cssButton TYPE=button
					onclick="UpdateCertifyCode();">
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divLCGrp1);">
						</td>
						<td class=titleImg>
							印刷单证信息
						</td>
					</tr>
				</table>
				<br>
			<Div id="divLLMainAskInput4" align=center style="display: ''">
				<Table class=common>
					<TR class=common>
						<TD text-align: left colSpan=1>
							<span id="spanCardRiskGrid"></span>
						</TD>
					</TR>
				</Table>
				<INPUT CLASS=cssButton VALUE="首  页" TYPE=button
					onclick="turnPage.firstPage();">
				<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
					onclick="turnPage.previousPage();">
				<INPUT CLASS=cssButton VALUE="下一页" TYPE=button
					onclick="turnPage.nextPage();">
				<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button
					onclick="turnPage.lastPage();">
			</Div>
			<Div id="divLLMainAskInput5" style="display: 'none'">
				<table class=common>
					<tr class=common>
						<td class=title>单证编码</td>
						<td class=input><input class=common name=tCertifyCode readonly></td>
						<td class=title>单证名称</td>
						<td class=input><input class=common name=tCertifyName ></td>
					</tr>
				</table>
				<table class=common>
					<tr class=common>
						<td class=title>管理机构</td>
						<td  class= input><Input class= "codeno"  name=tManageCom  ondblclick="return showCodeList('certifycomcode',[this,ManageComName1],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('certifycomcode',[this,ManageComName1],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageComName1></td>
						<td class=title>预警线</td>
						<td class=input><input class=common name=tWarningNo></td>
					</tr>
				</table>
				<table class=common>
					<tr class=common>
						<td class=title>印刷厂</td>
						<td class=input><input class=codeno name= tPrintCode  style="display:''"ondblclick="return showCodeList('PrintCode', [this,PrintCodeName1],[0,1]);" onkeyup="return showCodeListKeyEx('PrintCode', [this,PrintCodeName1],[0,1]);" ><input name=PrintCodeName1 class=codename  ></td>
						<td class=title>单证所属类型</td>
						<td class=input><Input class=codeno name=tCertifyType verify="单证类型|NOTNULLL" CodeData="0|^00|全国版单证^01|地方版单证" ondblClick="showCodeListEx('CertifyType',[this,CertifyTypeName1],[0,1]);" onkeyup="showCodeListKeyEx('CertifyType',[this,CertifyTypeName1],[0,1]);"><input class=codename name=CertifyTypeName1  elementtype="nacessary"></td>
					</tr>
				</table>
				<table class=common>
					<tr class=common>
						<td class=title>印刷单价</td>
						<td class=input><input class=common name=tPrice type="text"></td>
						<td class="title"></td><td class="input"><input class="readonly" readonly name=""></td>
					</tr>
				</table>
			</Div>
			<input name=OperateType type=hidden class=common>
			<input name=SerialNo type=hidden class=common>
			<input name=ComCode type=hidden class=common>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
