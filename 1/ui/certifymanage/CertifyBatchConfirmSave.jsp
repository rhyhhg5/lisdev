<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certifymanage.CertifyBatchConfirmBL"%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
String FlagStr = "Fail";
String Content = "";

String tBatchNo = request.getParameter("BatchNo");
String tDealType = request.getParameter("DealType");
String tSerialNo = request.getParameter("SerialNo");

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("BatchNo", tBatchNo);
    tTransferData.setNameAndValue("SerialNo",tSerialNo );
    String tSel[] = request.getParameterValues("InpCertifyListGridSel");
  	String importBatchNo[] = request.getParameterValues("CertifyListGrid1");
  	

	if(tSel != null && importBatchNo != null){
		for(int i = 0; i < tSel.length; i++){
			if(tSel[i]!=null && tSel[i].equals("1")){
				tTransferData.setNameAndValue("ImportBatchNo", importBatchNo[i]);
			}
		}
	}
	
	String comcode[] = request.getParameterValues("CertifyListGrid2");
	
	
	request.setCharacterEncoding("UTF-8");
	String seriano[] =  request.getParameterValues("CertifyDetailGrid4");
	String imBatch[] = request.getParameterValues("CertifyDetailGrid1");
	String certifyCode[] = request.getParameterValues("CertifyDetailGrid2");
	String sum[] = request.getParameterValues("CertifyDetailGrid5");
	String sumYW[] = request.getParameterValues("CertifyDetailGrid6");
	//String certifyName[] = request.getParameterValues("CertifyListGrid3");
	
	
	LZCertifyBatchInfoSet tLZCertifyBatchInfoSet = new LZCertifyBatchInfoSet();
	
	
	for(int i = 0 ; i < sum.length ; i++){
		LZCertifyBatchInfoSchema tLZCertifyBatchInfoSchema = new LZCertifyBatchInfoSchema();
		tLZCertifyBatchInfoSchema.setComCode(comcode[0]);
		tLZCertifyBatchInfoSchema.setImportBatchNo(imBatch[i]);
		tLZCertifyBatchInfoSchema.setCertifyCode(certifyCode[i]);
		tLZCertifyBatchInfoSchema.setSumCount(Integer.parseInt(sum[i]));
		tLZCertifyBatchInfoSchema.setSerialNo(seriano[i]);
		tLZCertifyBatchInfoSchema.setSumCountYW(sumYW[i]);
		
		tLZCertifyBatchInfoSet.add(tLZCertifyBatchInfoSchema);
		
	}
	
	tVData.addElement(tLZCertifyBatchInfoSet);
		
    tVData.add(tTransferData);
    tVData.add(tGI);
    
	CertifyBatchConfirmBL tCertifyBatchConfirmBL = new CertifyBatchConfirmBL();
    if(!tCertifyBatchConfirmBL.submitData(tVData, tDealType))
    {
    	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
        System.out.println(tCertifyBatchConfirmBL.getErrors().getFirstError());
        Content = "审核确认失败，原因是: " + tCertifyBatchConfirmBL.getErrors().getFirstError() + "_" + time;;
        FlagStr = "Fail";
    }
    else
    {
        Content = "处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception ex)
{
	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
    Content = "审核确认失败：" + ex.getMessage() + "_" +  time;
    FlagStr = "Fail";
    ex.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>" , "Income");
</script>
</html>
