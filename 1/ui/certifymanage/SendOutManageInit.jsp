<%
//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;

ExeSQL tExeSQL = new ExeSQL();
String strManageComName = tExeSQL.getOneValue("select name from ldcom where comcode='" + strManageCom + "'");

%>

<script language="JavaScript">

function initForm()
{
    try
    {
    	fm.all('ManageCom').value = '<%=strManageCom%>';
        fm.all('ManageComName').value = '<%=strManageComName%>';
    	initInventoryGrid();
        initCertifyListGrid();
        if(!check())
		{
    		return false;
  		}
        init();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

function initCertifyListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="单证编码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="CertifyCode";
        
        iArray[2]=new Array();
        iArray[2][0]="单证名称";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        iArray[2][21]="CertifyName";
        
        iArray[3]=new Array();
        iArray[3][0]="起始号";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=1;
        iArray[3][21]="StartNo";
        
        iArray[4]=new Array();
        iArray[4][0]="终止号";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=1;
        iArray[4][21]="EndNo";
        
        iArray[5]=new Array();
        iArray[5][0]="单证数量";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=1;
        iArray[5][21]="SumCount";
        
        iArray[6]=new Array();
        iArray[6][0]="单证数量";
        iArray[6][1]="0px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        iArray[6][21]="PrtNo";

        CertifyListGrid = new MulLineEnter("fm", "CertifyListGrid");

        CertifyListGrid.mulLineCount = 0;   
        CertifyListGrid.displayTitle = 1;
        CertifyListGrid.canSel = 0;
        CertifyListGrid.hiddenSubtraction = 1;
        CertifyListGrid.hiddenPlus = 1;
        CertifyListGrid.canChk = 0;
        CertifyListGrid.hiddenSubtraction = 0;
        CertifyListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}

function initInventoryGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="单证编码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="CertifyCode";
        
        iArray[2]=new Array();
        iArray[2][0]="单证名称";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        iArray[2][21]="CertifyName";
        
        iArray[3]=new Array();
        iArray[3][0]="起始号";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        iArray[3][21]="StartNo";
        
        iArray[4]=new Array();
        iArray[4][0]="终止号";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        iArray[4][21]="EndNo";
        
        iArray[5]=new Array();
        iArray[5][0]="单证数量";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        iArray[5][21]="SumCount";
        
        
        iArray[6]=new Array();
        iArray[6][0]="单证数量";
        iArray[6][1]="0px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        iArray[6][21]="PrtNo";

        InventoryGrid = new MulLineEnter("fm", "InventoryGrid");

        InventoryGrid.mulLineCount = 0;   
        InventoryGrid.displayTitle = 1;
        InventoryGrid.canSel = 1;
        InventoryGrid.hiddenSubtraction = 1;
        InventoryGrid.hiddenPlus = 1;
        InventoryGrid.canChk = 0;
        InventoryGrid.loadMulLine(iArray);
        InventoryGrid.selBoxEventFuncName = "inventoryClick";
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}
</script>

