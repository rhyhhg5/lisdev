
var showInfo;

window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
var turnPage4 = new turnPageClass(); 

function queryCertify()
{   
	divBatchListGrid.style.display = "none";
	divCertifyListGrid.style.display = "";
	
 	var strSQL = " select  a.certifycode ,a.CertifyName,a.count, a.WarningNo from  "
 			  +	 " (select lci.certifycode  certifycode, lcd.CertifyName CertifyName, sum(lci.sumcount) count ,lcd.WarningNo WarningNo"
 			  +  " from LZCertifyInventory lci ,LMCardDescription  lcd  "
 			  +  " where lci.certifycode= lcd.certifycode  and lci.comcode='"+ fm.ComCode.value +"' "
 			  +  " group by  lci.certifycode ,lcd.CertifyName ,lcd.WarningNo) as a "
 			  +  " where a.count<= a.WarningNo with ur";	  
	turnPage1.queryModal(strSQL, CertifyListGrid);
	         
	if (!turnPage1.strQueryResult) {
		alert("现系统中无单证库存量低于预警线！！");
		return false;
	}
}

function queryCertifyBatchList()
{   
	if(fm.BatchState.value== null ||fm.BatchState.value== "" )
	{
		alert("批次状态不能为空！！");
		return false;
	}
	var mManageCom = fm.ComCode.value;
	divBatchListGrid.style.display = "";
	divCertifyListGrid.style.display = "none";
	var strSQL =  " select BatchNo, makedate, BatchState,(case BatchState when '00'then '制定模版' when '01' then '开始征订'  when '02' then ' 征订结束' else '其他' end ), "
       		+ " ComCode,TempTypes,(case TempTypes when '00' then '全国版' when '01'then '地方版' else '其他' end  ) "
			+ " from LZcertifyBatchState "
            + " where 1 = 1  and   ComCode like '"+mManageCom +"%'" 
            + getWherePart("BatchNo", "BatchNo")
        	+ getWherePart("makedate", "ApplyDate")
        	+ getWherePart("TempTypes", "Type")
        	+ getWherePart("BatchState", "BatchState")
             "with ur ";
	turnPage.queryModal(strSQL, BatchListGrid);
	         
	if (!turnPage.strQueryResult) 
	{
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
}

// 导入预警
function importBatch()
{
    var tRow = BatchListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = BatchListGrid.getRowData(tRow);
    var tBatchNo = tRowDatas[0];
    var tComCode = tRowDatas[4];
    var tBatchState = tRowDatas[2];
    divCertifytimportGrid.style.display = "";
    divCertifyOutBounGrid.style.display = "none";
    divCertifyIntBounGrid.style.display = "none";
    
    if(tBatchState=="02")
    {
    	alert("该批次征订已结束不能进行导入预警！！！");
        return false;
    }
     
    var strSQL="";
    if(tComCode=='86'){
      	strSQL = " select x.comcode ,(select name from ldcom where comcode= x.comcode) from"
 			  +  " (select distinct substr(comcode, 1, 4) comcode from ldcom  where  Sign = '1' and comcode like '"+tComCode+"%' and comcode not in ('86', '86000000')) as x   "
 			  +  " where x.comcode not in(select substr(comcode ,1,4) from LZCertifyBatchMain  where batchno='"+tBatchNo+"')   "
 			  +  " order by x.comcode  with ur";	 
    
		}else{
		  strSQL = " select x.comcode ,(select name from ldcom where comcode= x.comcode) from"
 			  +  " (select  comcode from ldcom  where  Sign = '1' and comcode like '"+tComCode+"%' ) as x   "
 			  +  " where x.comcode not in(select comcode from LZCertifyBatchMain  where batchno='"+tBatchNo+"')   "
 			  +  " order by x.comcode  with ur";	
		
		}

    turnPage2.queryModal(strSQL, CertifytimportGrid);
    
  	if (!turnPage2.strQueryResult) {
		alert("批次"+tBatchNo+" 下所有分公司已导入！！！");
		return false;
	}

}

// 出库预警
function outBoundWarning()
{
    var tRow = BatchListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = BatchListGrid.getRowData(tRow);
    var tBatchNo = tRowDatas[0];
    var tComCode = tRowDatas[4];
    var tBatchState = tRowDatas[2];
    divCertifyOutBounGrid.style.display = "";
    divCertifytimportGrid.style.display = "none";
    divCertifyIntBounGrid.style.display = "none";
    
    if(tBatchState=="01")
    {
    	alert("该批次正在征订，不能进行出库预警操作！！！");
        return false;
    }
    
     var strSQL=" select certifycode ,(select  certifyname from  LMCardDescription  where certifycode = LZSendOutList.certifycode ),"
             + " comcode ,(select name from ldcom where comcode = LZSendOutList.comcode )"
			 + " from  LZSendOutList  where  DealState ='01' and batchno ='"+tBatchNo +"' with ur";


    turnPage4.queryModal(strSQL, CertifyOutBounGrid);
    
  	if (!turnPage4.strQueryResult) {
		alert("批次"+tBatchNo+" 下所有单证已出库！！！");
		return false;
	}

}

// 入库预警
function intBoundWarning()
{
    var tRow = BatchListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = BatchListGrid.getRowData(tRow);
    var tBatchNo = tRowDatas[0];
    var tComCode = tRowDatas[4];
    var tBatchState = tRowDatas[2];
    divCertifyIntBounGrid.style.display = "";
    divCertifytimportGrid.style.display = "none";
    divCertifyOutBounGrid.style.display = "none";
    
    if(tBatchState=="01")
    {
    	alert("该批次正在征订，不能进行入库预警操作！！！");
        return false;
    }
    
    var strSQL=" select certifycode ,(select  certifyname from  LMCardDescription  where certifycode = LZPrintList.certifycode )"
			 + " from  LZPrintList  where  DealState ='01' and batchno ='"+tBatchNo +"' with ur";

    turnPage3.queryModal(strSQL, CertifyIntBounGrid);
    
  	if (!turnPage3.strQueryResult) {
		alert("批次"+tBatchNo+" 下所有单证已入库！！！");
		return false;
	}

}


