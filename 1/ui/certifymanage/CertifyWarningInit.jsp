
<%
	//程序名称：CertifySendOutInit.jsp
	//程序功能：发放管理
	//创建日期：2002-08-07
	//创建人  ：周平
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput) session.getValue("GI");
	String Comcode = globalInput.ComCode;
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）

function initForm()
{
  try
  {
    fm.ComCode.value = <%=Comcode%>;
	initBatchListGrid();
	initCertifyListGrid();
	initCertifytimportGrid();
	initCertifyIntBounGrid();
	initCertifyOutBounGrid();

  }
  catch(re)
  {
    alert("CertifySendOutInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initBatchListGrid()
{  
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="申请日期";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="批次状态代码";
        iArray[3][1]="0px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="批次状态";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="征订机构";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="征订类型代码";
        iArray[6][1]="0px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="征订类型";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        BatchListGrid = new MulLineEnter("fm", "BatchListGrid"); 

        BatchListGrid.mulLineCount = 0;   
        BatchListGrid.displayTitle = 1;
        BatchListGrid.canSel = 1;
        BatchListGrid.hiddenSubtraction = 1;
        BatchListGrid.hiddenPlus = 1;
        BatchListGrid.canChk = 0;
        BatchListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ImportBatchListGrid时出错：" + ex);
    }
}

// 起始单号、终止单号信息列表的初始化
function initCertifyListGrid()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="单证名称";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="库存数量";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="80";        		  //列宽
      iArray[3][2]=180;          			//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="预警线";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[4][1]="80";        		  //列宽
      iArray[4][2]=180;          			//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许


      CertifyListGrid = new MulLineEnter( "fm" , "CertifyListGrid" );
      //这些属性必须在loadMulLine前
      CertifyListGrid.displayTitle = 1;
      CertifyListGrid.mulLineCount = 1;   
      CertifyListGrid.hiddenPlus = 1;        
      CertifyListGrid.hiddenSubtraction = 1; 
      CertifyListGrid.canChk = 0; 
      CertifyListGrid.unlocked=0;   
      CertifyListGrid.loadMulLine(iArray);

      
      
      //CertifyList.delBlankLine("CertifyList");

    } catch(ex) {
      alert(ex);
    }
}

function initCertifytimportGrid()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=1;              		//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="状态";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        		  //列宽
      iArray[2][2]=180;          			//列最大值
      iArray[2][3]=1;              		//是否允许输入,1表示允许，0表示不允许

 


      CertifytimportGrid = new MulLineEnter( "fm" , "CertifytimportGrid" );
      //这些属性必须在loadMulLine前
      CertifytimportGrid.displayTitle = 1;
      CertifytimportGrid.mulLineCount = 1;   
      CertifytimportGrid.hiddenPlus = 1;        
      CertifytimportGrid.hiddenSubtraction = 1; 
      CertifytimportGrid.canChk = 0; 
      CertifytimportGrid.unlocked=1;   
      CertifytimportGrid.loadMulLine(iArray);


    } catch(ex) {
      alert(ex);
    }
}


function initCertifyIntBounGrid()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="单证编码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="单证名称";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        

      CertifyIntBounGrid = new MulLineEnter( "fm" , "CertifyIntBounGrid" );
      //这些属性必须在loadMulLine前
      CertifyIntBounGrid.displayTitle = 1;
      CertifyIntBounGrid.mulLineCount = 0;   
      CertifyIntBounGrid.hiddenPlus = 1;        
      CertifyIntBounGrid.hiddenSubtraction = 1; 
      CertifyIntBounGrid.canChk = 0; 
      CertifyIntBounGrid.unlocked=1;   
      CertifyIntBounGrid.loadMulLine(iArray);

    } catch(ex) {
      alert(ex);
    }
}


function initCertifyOutBounGrid()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="单证编码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="单证名称";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="征订机构";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="征订机构名称";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        

      CertifyOutBounGrid = new MulLineEnter( "fm" , "CertifyOutBounGrid" );
      //这些属性必须在loadMulLine前
      CertifyOutBounGrid.displayTitle = 1;
      CertifyOutBounGrid.mulLineCount = 0;   
      CertifyOutBounGrid.hiddenPlus = 1;        
      CertifyOutBounGrid.hiddenSubtraction = 1; 
      CertifyOutBounGrid.canChk = 0; 
      CertifyOutBounGrid.unlocked=1;   
      CertifyOutBounGrid.loadMulLine(iArray);


    } catch(ex) {
      alert(ex);
    }
}



</script>
