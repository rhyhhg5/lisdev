
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 	String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('CertifyCode').value = '';
    fm.all('CertifyName').value = '';     
    fm.all('ManageCom').value = <%=Comcode%>;
    fm.all('ComCode').value = <%=Comcode%>;
    
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;
function RegisterDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divDetailInfo.style.left=ex;
  	divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}


function initForm()
{
  try
  {
    initInpBox();
    initCardRiskGrid();
    if(!init()){
    	return false ;
    }
	
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCardRiskGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="流水号";
    iArray[1][1]="0px";
    iArray[1][2]=100;
    iArray[1][3]=1;

    iArray[2]=new Array();
    iArray[2][0]="单证编码";         			//列名
    iArray[2][1]="80px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许  


    iArray[3]=new Array();
    iArray[3][0]="单证名称";         	//列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
   
    
    iArray[4]=new Array();
    iArray[4][0]="单证类型码";         	//列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    
    iArray[5]=new Array();
    iArray[5][0]="所属机构";         	//列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=60;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="预警线";         	//列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=60;            			//列最大值
    iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="印刷厂编码";         	//列名
    iArray[7][1]="0px";            	//列宽
    iArray[7][2]=60;            			//列最大值
    iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许
  
    
    iArray[8]=new Array();
    iArray[8][0]="印刷厂名称";         	//列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=60;            			//列最大值
    iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="单证类型状态";         	//列名
    iArray[9][1]="0px";            	//列宽
    iArray[9][2]=60;            			//列最大值
    iArray[9][3]=1;   
    
    iArray[10]=new Array();
    iArray[10][0]="单证类型";         	//列名
    iArray[10][1]="80px";            	//列宽
    iArray[10][2]=60;            			//列最大值
    iArray[10][3]=1;   
    
    iArray[11]=new Array();
    iArray[11][0]="单价";         	//列名
    iArray[11][1]="80px";            	//列宽
    iArray[11][2]=60;            			//列最大值
    iArray[11][3]=1;   
  
    CardRiskGrid = new MulLineEnter("fm", "CardRiskGrid");
    CardRiskGrid.mulLineCount = 0;
    CardRiskGrid.displayTitle = 1;
    CardRiskGrid.hiddenPlus = 1;
    CardRiskGrid.hiddenSubtraction = 1;
    CardRiskGrid.canSel = 1;
    CardRiskGrid.canChk = 0;
    CardRiskGrid.loadMulLine(iArray);
    CardRiskGrid.selBoxEventFuncName = "ShowDetail";
  }
  catch(ex)
  {
    alert("初始化时出错2003-05-21"+ex);
  }
}


</script>


