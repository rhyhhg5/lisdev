<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="CertifyWarningInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="CertifyWarningInit.jsp"%>
	</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">请输入查询条件：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">批次号</td>
                    <td class="input8">
                        <input class="common" name="BatchNo" />
                    </td>
                    <td class=title>批次状态</td>
 					 <td class=input><Input class=codeno name=BatchState CodeData="0|^01|开始征订^02|征订结束" ondblClick="showCodeListEx('BatchState',[this,BatchStateName],[0,1]);" onkeyup="showCodeListKeyEx('BatchState',[this,BatchStateName],[0,1]);"><input class=codename name=BatchStateName readonly=true ></td>
                    </tr>
                    <tr class="common">
                     <td class=title>征订类型</td>
 					 <td class=input><Input class=codeno name=Type CodeData="0|^00|全国版^01|地方版" ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true ></td>
                    <td class="title8">申请日期</td>
                    <td class="input8">
                    <input class="coolDatePicker" dateFormat="short" name="ApplyDate" verify="申请日期|date" />
                	</tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="批次查询" onclick="queryCertifyBatchList();" /> 
                    <input name="btnOp" class="cssButton" type="button" value="库存预警"onclick="queryCertify()">	
                </td>
            </tr>
            
        </table>
        
        <br />
        <div id="divBatchListGrid" style="display: ''">
             <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divBatchListGrid);" />
                </td>
                <td class="titleImg">待处理批次</td>
            </tr>
        	</table>
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanBatchListGrid"></span> 
                    </td>
                </tr>
            </table>
            <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />                     
            </div>
        </div> 
        
            <div id="divCertifyListGrid" style="display: 'none'">
               <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCertifyListGrid);" />
                </td>
                <td class="titleImg">单证信息</td>
            </tr>
        </table>
            
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifyListGrid"></span> 
                    </td>
                </tr>
            </table>
            <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>        
        <table class="common">
				<tr class="common">
					<td class="input">
						<input type="button" name="btnCertify" class="cssButton"value="导入预警" onclick="importBatch()">
						<input type="button" name="btnCertify" class="cssButton"value="入库预警" onclick="intBoundWarning()">
						<input type="button" name="btnCertify" class="cssButton"value="出库预警" onclick="outBoundWarning()">
					</td>
				</tr>
			</table>
		  <div id="divCertifytimportGrid" style="display: 'none'">
		   <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCertifyListGrid);" />
                </td>
                <td class="titleImg">未导入机构信息</td>
            </tr>
        </table>
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifytimportGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                     
            </div>
        </div>
        
          <div id="divCertifyIntBounGrid" style="display: 'none'">
          
         <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCertifyListGrid);" />
                </td>
                <td class="titleImg">未入库单证信息</td>
            </tr>
        </table>
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifyIntBounGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage3.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage3.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage3.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage3.lastPage();" />                     
            </div>
        </div>
        
          <div id="divCertifyOutBounGrid" style="display: 'none'">
           <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCertifyListGrid);" />
                </td>
                <td class="titleImg">未出库单证信息</td>
            </tr>
        </table>
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifyOutBounGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage4.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage4.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage4.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage4.lastPage();" />                     
            </div>
        </div>
        
        <input type=hidden name=ComCode class=common>
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
