 <%
//程序名称：DataMaintenanceInit.jsp
//程序功能：初始化界面
//创建日期：2011-02-28
//创建人  ：[OoO？]杨天政
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String CurrentDate = PubFun.getCurrentDate();
  String CurrentTime = PubFun.getCurrentTime();
%>  
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
    fm.all('MaintenanceMan').value = '';
    fm.all('TableName').value = '';
    // fm.all('MakeDate').value = '';
    fm.all('FieldName').value = '';  
    fm.all('Condition').value = '';
    fm.all('DataSize').value = '';
    
   
  }
  catch(ex)
  {
    alert("在DatamaintenanceInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    // initSelBox();
    initTempletGrid();
    initFeedGrid();
    initFileGrid();
    
      // initElementtype();
  }
  catch(re)
  {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}

// 暂收费信息列表的初始化
function initTempletGrid()
  {                               
    var iArray = new Array();      
    try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="20px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[0][21]="orderno";
      
      
      iArray[1]=new Array();
      iArray[1][0]="流水号";      	   		//列名
      iArray[1][1]="60px";            			//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="SerialNo";
      
      iArray[2]=new Array();
      iArray[2][0]="表名";      	   		//列名
      iArray[2][1]="50px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="TableName";
      
      iArray[3]=new Array();
      iArray[3][0]="字段名";      	   		//列名
      iArray[3][1]="40px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="FieldName";
      
      iArray[4]=new Array();
      iArray[4][0]="条件";      	   		//列名
      iArray[4][1]="30px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][21]="Condition";

      iArray[5]=new Array();
      iArray[5][0]="提出日期";      	   		//列名
      iArray[5][1]="25px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][21]="MakeDate";
      
      
      
      iArray[6]=new Array();
      iArray[6][0]="数据量";      	   		//列名
      iArray[6][1]="20px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][21]="DataSize";
      
      iArray[7]=new Array();
      iArray[7][0]="维护人";      	   		//列名
      iArray[7][1]="40px";            			//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][21]="MaintenanceMan";
      
      
       iArray[8]=new Array();
      iArray[8][0]="维护原因";      	   		//列名
      iArray[8][1]="60px";            			//列宽
      iArray[8][2]=20;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][21]="Reason";
      
       iArray[9]=new Array();
      iArray[9][0]="cq号";      	   		//列名
      iArray[9][1]="50px";            			//列宽
      iArray[9][2]=20;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][21]="cq";
      
       iArray[10]=new Array();
      iArray[10][0]="cq状态";      	   		//列名
      iArray[10][1]="50px";            			//列宽
      iArray[10][2]=20;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][21]="state23";
      
     TempletGrid = new MulLineEnter( "fm" , "TempletGrid" ); 
      //这些属性必须在loadMulLine前
      TempletGrid.mulLineCount = 0;   //初始显示0行
      TempletGrid.displayTitle = 1;
      TempletGrid.hiddenPlus = 1;
      TempletGrid.hiddenSubtraction = 1;
      TempletGrid.canSel = 1;
      TempletGrid.canChk = 0;
      TempletGrid.loadMulLine(iArray);  
      TempletGrid.selBoxEventFuncName = "selectOne";
      // TempletGrid.checkBoxAll ();
       TempletGrid.locked = 1;
      }
    catch(ex)
    {
      alert(ex);
    }
  }
    function initFeedGrid()
  {                               
    var iArray = new Array();      
    try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;  
              
                  			//是否允许输入,1表示允许，0表示不允许
      iArray[0][21]="orderno";
      
      
      iArray[1]=new Array();
      iArray[1][0]="查询的反馈号";      	   		//列名
      iArray[1][1]="70px";            			//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="FeedId1";
      
      iArray[2]=new Array();
      iArray[2][0]="反馈模块";      	   		//列名
      iArray[2][1]="100px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="FeedBackModule";
      
      iArray[3]=new Array();
      iArray[3][0]="模块负责人";      	   		//列名
      iArray[3][1]="80px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=1;     
      
               			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="Principal";
      
      iArray[4]=new Array();
      iArray[4][0]="回复日期";      	   		//列名
      iArray[4][1]="50px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][21]="RestoreDate";

      iArray[5]=new Array();
      iArray[5][0]="回复状态";      	   		//列名
      iArray[5][1]="50px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=3;   
      
      iArray[5][10]="State2";       
      iArray[5][11]="0|^Y|同意|^No|不同意|^N|未回应";  
            			//是否允许输入,1表示允许，0表示不允许
      iArray[5][21]="State";
      
      
      
      iArray[6]=new Array();
      iArray[6][0]="反馈意见";      	   		//列名
      iArray[6][1]="400px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][21]="Attitude";
      
      
     FeedGrid = new MulLineEnter( "fm0" , "FeedGrid" ); 
      //这些属性必须在loadMulLine前
      FeedGrid.mulLineCount = 0;   //初始显示0行
      FeedGrid.displayTitle = 1;
      FeedGrid.hiddenPlus = 1;
      FeedGrid.hiddenSubtraction = 1;
      FeedGrid.canSel = 1;
      FeedGrid.canChk = 0;
      FeedGrid.loadMulLine(iArray);  
      FeedGrid.selBoxEventFuncName = "selectOne2";
      // TempletGrid.checkBoxAll ();
       FeedGrid.locked = 1;
 
      }
    catch(ex)
    {
      alert(ex);
    }
}

function initFileGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=30;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="文件编号";         			//列名
		iArray[1][1]="30px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="文件名";         			//列名
		iArray[2][1]="100px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="维护人";         			//列名
		iArray[3][1]="50px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;      
		        			//是否允许输入,1表示允许，0表示不允许
		        			
		iArray[4]=new Array();
		iArray[4][0]="维护原因";         			//列名
		iArray[4][1]="320px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;      
		        			//是否允许输入,1表示允许，0表示不允许        
		        			
		        			
		 iArray[5]=new Array();
		iArray[5][0]="数据量";         			//列名
		iArray[5][1]="50px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;                         //是否允许输入,1表示允许，0表示不允许       
		
		
		 iArray[6]=new Array();
		iArray[6][0]="CQ状态";         			//列名
		iArray[6][1]="50px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;                         //是否允许输入,1表示允许，0表示不允许  
		     
		       						
		//iArray[3]=new Array();
		//iArray[3][0]="文件代码";         			//列名
		//iArray[3][1]="50px";            		//列宽
		//iArray[3][2]=100;            			//列最大值
		//iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		//iArray[4]=new Array();
		//iArray[4][0]="文件类型";         			//列名
		//iArray[4][1]="70px";            		//列宽
		//iArray[4][2]=100;            			//列最大值
		//iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		//iArray[5]=new Array();
		//iArray[5][0]="文件细类";         			//列名
		//iArray[5][1]="70px";            		//列宽
		//iArray[5][2]=100;            			//列最大值
		//iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		//iArray[6]=new Array();
		//iArray[6][0]="其它号码";         			//列名
		//iArray[6][1]="50px";            		//列宽
		//iArray[6][2]=100;            			//列最大值
		//iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		//iArray[7]=new Array();
		//iArray[7][0]="其它号码类型";         			//列名
		//iArray[7][1]="50px";            		//列宽
		//iArray[7][2]=100;            			//列最大值
		//iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		//iArray[8]=new Array();
		//iArray[8][0]="相对路径";         			//列名
		//iArray[8][1]="50px";            		//列宽
		//iArray[8][2]=100;            			//列最大值
		//iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		//iArray[9]=new Array();
		//iArray[9][0]="文件描述";         			//列名
		//iArray[9][1]="200px";            		//列宽
		//iArray[9][2]=100;            			//列最大值
		//iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		//iArray[10]=new Array();
		//iArray[10][0]="上传机构";         			//列名
		//iArray[10][1]="70px";            		//列宽
		//iArray[10][2]=100;            			//列最大值
		//iArray[10][3]=3;  
		
		//iArray[11]=new Array();
		//iArray[11][0]="上传人";         		//列名
		//iArray[11][1]="50px";            		//列宽
		//iArray[11][2]=100;            			//列最大值
		//iArray[11][3]=3;  
		
		//iArray[12]=new Array();
		//iArray[12][0]="上传时间";         		//列名
		//iArray[12][1]="150px";            		//列宽
		//iArray[12][2]=100;            			//列最大值
		//iArray[12][3]=3;  
		
		//iArray[13]=new Array();
		//iArray[13][0]="维护人";         		//列名
		//iArray[13][1]="150px";            		//列宽
		//iArray[13][2]=100;            			//列最大值
		//iArray[13][3]=0;  
		
		FileGrid = new MulLineEnter( "fmdown" , "FileGrid" ); 
		//这些属性必须在loadMulLine前
		FileGrid.mulLineCount = 0;   
		FileGrid.displayTitle = 1;
		FileGrid.locked = 1;
		FileGrid.canSel = 1;
		FileGrid.hiddenPlus = 1;
		FileGrid.hiddenSubtraction = 1;
		FileGrid.loadMulLine(iArray);
		
		FileGrid.selBoxEventFuncName = "FileSelect"; 
	}
  catch(ex)
  {
    alert(ex);
  }
}

</script>
