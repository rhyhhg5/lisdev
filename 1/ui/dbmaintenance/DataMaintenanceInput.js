//程序名称：DataMaintenanceInput.js
//程序功能：CQ数据维护和备案
//创建日期：2011-2-28 22:22
//创建人  ：【OoO？】杨天政
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 

var showInfo;
//输入框的初始化 
function initInpBox()
{
  try
  {
	fm.all('TableName').value = "";
    fm.all('FieldName').value = "";
    fm.all('Condition').value = "";
    fm.all('DataSize').value = "";
   
    fm.all('Route').value = "";
    fm.all('MaintenanceMan').value = ""; 
    
    fm0.all('FeedBackId').value ="";
    fm0.all('FeedBackModule').value ="";
    fm0.all('Principal').value ="";
    fm0.all('Attitude').value = "";
    
  }
  catch(ex)
  {
    alert("在DataMaintenanceInput.js-->InitInpBox函数中发生异常:初始化界面错误!" + ex.message);
  }
}


function fileanddata()
{


saveLog();
upData();

}
//提交，保存按钮对应操作
function saveLog()
{
  if(!verifyInput2())
 {
    return false;
 }
  
    if (confirm("您确实想保存该记录吗?"))
  {	
    fm.fmtransact.value = "INSERT";
    fm.fileName.value=fmupfile.fileName.value;
   
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //将打开新的窗口来显示此信息
 
    fm.action = "DataMaintenanceSave.jsp";
    
    fm.submit(); //提交
   
 }
  
}

//提交后操作,服务器数据返回后执行的操作
function feedsave(){
if(!verifyInput2())
 {
    return false;
 }
  
  if (confirm("您确实想保存该记录吗?"))
  {	
    fm0.fmtransact.value = "INSERT";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //将打开新的窗口来显示此信息
    fm0.action = "FeedBackMindSave.jsp";
    fm0.submit(); //提交
   
    
  }
  }
function afterSubmit(FlagStr, content)
{

   
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr2="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr2,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
   }
 
   feedqueryLog();
   queryLog();
}

//Click事件，当点击“修改”图片时触发该函数
function updateLog()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {	
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "UPDATE";
    fm.action = "DataMaintenanceSave.jsp";
    fm.submit(); //提交
    
  }
  else
  {
    alert("您取消了修改操作！");
  }
}   
function feedupdataLog()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {	
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm0.fmtransact.value = "UPDATE";
    fm0.action = "feedupdata.jsp";
    fm0.submit(); //提交
    
  }
  else
  {
    alert("您取消了修改操作！");
  }
}   

//删除日志
function deleteLog()
{
  //下面增加相应的删除代码
  
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "DELETE";
    fm.action = "DataMaintenanceSave.jsp";
    
    fm.submit(); //提交
    
  }
  else
  {
    alert("您取消了删除操作！");
  }
} 
function feeddeleteLog()
{
  //下面增加相应的删除代码
  
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm0.fmtransact.value = "DELETE";
    fm0.action = "FeedBackMindSave.jsp";
    fm0.submit(); //提交
    
  }
  else
  {
    alert("您取消了删除操作！");
  }
} 
//查询日志
function queryLog() 
{

  var sql1 = "select SerialNo,TableName,FieldName,Condition,MakeDate,DataSize,MaintenanceMan,Reason,cq,state"
          + " from DataMaintenance "
         + " where 1 = 1 "
         // + getWherePart("cq","cq")
          + getWherePart('state','State','like')
             +getWherePart('cq','cq','like')
         //+ " where cq='"+ fm.all('cq').value+"'"
          + " order by SerialNo";
          //+ "where MakeDate like '2006-%-%'"

  
  
  turnPage.queryModal(sql1,TempletGrid);
  
  turnPage.pageDivName = "divPage";
  
  var arrInsuredResult2=easyExecSql(sql1,1,0);
  
  fm0.all('FeedBackId').value=arrInsuredResult2[0][0];
  if(TempletGrid.mulLineCount == 0)
  {
    
    
    return false;
    
  }
  
  fmdown.all('fileName').value=fm.all('cq').value;
  fm0.all('cq').value=fm.all('cq').value;
  
  feedqueryLog();
  return true;
 
 
}
function feedqueryLog() 
{
  var sql2 = "select FeedBackId,FeedBackModule,Principal,RestoreDate,State,Attitude "
          + "from feedbackmind "
          + "where FeedBackId in"
          + " ( select SerialNo from DataMaintenance where cq = '"+fm0.all('cq').value+"'"
          + " )"
          //+ "where 1 = 1 ";
          //+ getWherePart("FeedBackModule")
          //+ getWherePart("Principal")
          //+ getWherePart("RestoreDate")
          //+ getWherePart("Attitude")
          //  + getWherePart("MaintenanceMan");
          //+ "where MakeDate like '2006-%-%'"
          + " order by FeedBackId";
  
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql2, FeedGrid);
  
  var arrInsuredResult=easyExecSql(sql2,1,0);
  
  fm0.all('FeedBackId').value=arrInsuredResult[0][0];
  
  
  if(FeedGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    
    return false;
  }
  
  return true;
}
//选择一条日志时触发的动作
function selectOne() 
{
  var row = TempletGrid.getSelNo();
  fm.all('SerialNo').value = TempletGrid.getRowColDataByName(row - 1 , 'SerialNo');
  fm.all('cq').value=TempletGrid.getRowColDataByName(row - 1 , 'cq');
  fm.all('MaintenanceMan').value = TempletGrid.getRowColDataByName(row - 1 , 'MaintenanceMan');
  fm.all('TableName').value = TempletGrid.getRowColDataByName(row - 1, 'TableName');
  fm.all('FieldName').value = TempletGrid.getRowColDataByName(row - 1, 'FieldName');
  fm.all('Condition').value = TempletGrid.getRowColDataByName(row - 1, 'Condition');
 //fm.SeriNo.value = TempletGrid.getRowColDataByName(row - 1, "ColName");
  fm.all('DataSize').value = TempletGrid.getRowColDataByName(row - 1, 'DataSize');
  fm.all('Reason').value = TempletGrid.getRowColDataByName(row - 1, 'Reason');
  //fm.all('fileName').value = TempletGrid.getRowColDataByName(row - 1, 'fileName');
  fm0.all('FeedBackId').value = TempletGrid.getRowColDataByName(row - 1, 'SerialNo');
}
//选择一条日志时触发的动作
function selectOne2() 
{
  var row1 = FeedGrid.getSelNo();
  
  fm0.all('FeedId1').value = FeedGrid.getRowColDataByName(row1 - 1 , 'FeedId1');
   fm0.all('FeedBackModule').value = FeedGrid.getRowColDataByName(row1 - 1, 'FeedBackModule');
     fm0.all('Principal').value = FeedGrid.getRowColDataByName(row1 - 1, 'Principal');
  fm0.all('State').value = FeedGrid.getRowColDataByName(row1 - 1, 'State');
 //fm.SeriNo.value = TempletGrid.getRowColDataByName(row - 1, "ColName");
  fm0.all('Attitude').value = FeedGrid.getRowColDataByName(row1 - 1, 'Attitude');
  //fm.all('Reason').value = FeedGrid.getRowColDataByName(row - 1, 'Reason');
//fm.all('Route').value = FeedGrid.getRowColDataByName(row - 1, 'Route');
//fm.all('FeedBackId').value = FeedGrid.getRowColDataByName(row - 1, 'SerialNo');

}
function upData()
{
	if(fmupfile.all('filename').value=="")
		{
		alert("请选择要上载的文件.");
		return false;
	}
	
	if (verifyInput() == false)
    return false;
	
	var showStr="正在上载数据……";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//fm.all('submitbutton').disabled=true;
	showInfo.close();
	fmupfile.action="fileUpLoadMSave.jsp";
	fmupfile.submit();
}