<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DataMaintenceSave.jsp
//程序功能：万能险帐户结算利率录入
//创建日期：2011-2-27
//创建人  ：【OoO?】杨天政
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput" %>
<%@page import="com.sinosoft.lis.dbmaintenance.DataMaintenanceUI" %>


<%
  //输出参数               
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");  //操作员信息，GI已在登录时放入session
  String transact = request.getParameter("fmtransact");
  
  System.out.println("transact=" + transact);
  
  //定义一个与页面需要处理数据对应的表的Schema存储页面数据
  
  String t=request.getParameter("Condition");
  
  t = t.replaceAll("'", "\"");
  
  t=t.replaceAll("\r\n\r\n","<br>"); 
   t=t.replaceAll("\r\n","<br>");
   String a=request.getParameter("FieldName");
   a = a.replaceAll("'", "\"");
   a=a.replaceAll("\r\n\r\n","<br>");
    a=a.replaceAll("\r\n","<br>"); 
    String b=request.getParameter("TableName");
    b = b.replaceAll("'", "\"");
    b=b.replaceAll("\r\n\r\n","<br>"); 
    b=b.replaceAll("\r\n","<br>"); 
     String c=request.getParameter("Reason");
     c = c.replaceAll("'", "\"");
     c=c.replaceAll("\r\n\r\n","<br>"); 
     c=c.replaceAll("\r\n","<br>"); 
      String d=request.getParameter("DataSize");
      d = d.replaceAll("'", "\"");
      d=d.replaceAll("\r\n\r\n","<br>"); 
      d=d.replaceAll("\r\n","<br>"); 
       String e=request.getParameter("MaintenanceMan");
        e = e.replaceAll("'", "\"");
       e=e.replaceAll("\r\n\r\n","<br>"); 
        e=e.replaceAll("\r\n","<br>"); 
  DataMaintenanceSchema tDataMaintenanceSchema = new DataMaintenanceSchema();
  tDataMaintenanceSchema.setSerialNo(request.getParameter("SerialNo"));

  tDataMaintenanceSchema.setTableName(b);

  tDataMaintenanceSchema.setFieldName(a);
  tDataMaintenanceSchema.setCondition(t);
  tDataMaintenanceSchema.setDataSize(d);
  tDataMaintenanceSchema.setReason(c);
  
  String name=request.getParameter("fileName");
  String fileName;
  
  fileName = name.substring(name.lastIndexOf("\\") + 1);
  tDataMaintenanceSchema.setRoute(fileName);
  //tDataMaintenanceSchema.setRoute(request.getParameter("fileName"));
  
  
  tDataMaintenanceSchema.setMaintenanceMan(e);
  tDataMaintenanceSchema.setState(request.getParameter("State"));
  tDataMaintenanceSchema.setcq(request.getParameter("cq"));
   System.out.println(request.getParameter("SerialNo"));
    System.out.println(request.getParameter("TableName")+"tablename 没保存");
     System.out.println(request.getParameter("FieldName"));
      System.out.println(t);
       System.out.println(request.getParameter("DataSize"));
        System.out.println(request.getParameter("Reason"));
         System.out.println(request.getParameter("fileName"));
         System.out.println(request.getParameter("MaintenanceMan"));
         System.out.println(request.getParameter("State"));
  //将需要处理的数据对象放入统一的接口数据集合VData
  VData tVData = new VData();
	tVData.add(tDataMaintenanceSchema);
    //System.out.println("保存VData成功");
	System.out.println("保存VData成功");
	//System.out.println("保存VData成功");
	tVData.add(tG);
	
	//System.out.println("保存tg session成功");
	System.out.println("保存tg session成功");
	//System.out.println("保存tg session成功");
	//调用后台处理类接口
  DataMaintenanceUI tDataMaintenanceUI = new DataMaintenanceUI();
  if(!tDataMaintenanceUI.submitData(tVData,transact))
  { System.out.println("保存失败了");
    Content = "操作失败，原因是:" + tDataMaintenanceUI.mErrors.getFirstError();
    FlagStr = "Fail";
  }
  else
  {                 
    System.out.println("操作成功啦");
    Content = "操作成功! ";
    FlagStr = "Success";
  }
  
%>
<html>
<script language="javascript">
  //后台处理完毕后，调用父页面的afterSubmit页面显示后台提示信息
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
