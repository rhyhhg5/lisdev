<html> 
<%
//程序名称：DataMaintenanceInput.jsp
//程序功能：CQ数据维护和备案
//创建日期：2011-2-27
//创建人  ：【OoO？】杨天政
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="DataMaintenanceInput.js"></SCRIPT>
  <SCRIPT src="datamdownload.js"></SCRIPT>
  <%@include file="DataMaintenanceInit.jsp"%> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
</head>
<body  onload="initForm()">
<h1><font color="#ff0000">录入数据后,输入相应的CQ号，可以查到相应的SQL文件并可以进行下载</font></h1>
	
	
	<form method="POST"  name="fmupfile" target="fraSubmit" ENCTYPE="multipart/form-data">
			<!--form method="POST" action="fileUploadSave.jsp" name="fm"-->
			<table class=common align=center border="0" width="30%">
				<tr class=common>
					<td class=titleImg colspan = "4">
						上传维护SQL文件：
					</td>
				</tr>
				<tr>
					<td class=input colspan = "4">
						<input class=common type="file" name="fileName" size="100">
					</td>
				</tr>
				

			</table>
			<input type="hidden" name=filePath>
		</form>
	 
	
<form method="post" name="fm"  target="fraSubmit" >

	 <h1><font color="#ff0000"> 录入数据的地方</font></h1>
<table width="400" border="0" class= common>
  <tr class= common width="400">
    <td class= title>流水号:</td>
      <td class=title>CQ号</td>
   <td class= title>维护人员:</td>
   <td class= title></td>
   
    
  </tr>
  <tr class= common width="400">
    <td class= title><input class="readonly" name="SerialNo" readonly></td>
      <td class=title><input class= "common" name="cq"></td>
     <td class= title><input class= "common" name="MaintenanceMan" type="text" size="10" maxlength="100" /></td>
    
    <td class= title><input  class= "common" id="State" name="State" type=hidden ></td>
    
  </tr>
  <tr>
  <td class= title>需要维护的表:</td> 
  <td class= title>需要维护的字段:</td>
  <td class= title>维护条件：</td>
    <td class= title>涉及数据量:</td>
  </tr>
  <tr>
   <td class= title><textarea class= "common" name="TableName"  cols="20" rows="8"></textarea></td>
  <td class= title><textarea class= "common" name="FieldName"  cols="20" rows="8"></textarea></td>
    <td class= title align="left"><font color="#ff0000" size="5">维护条件不需要录入，详情请看SQL文件。</font><input type=hidden class= "common" name="Condition" value="Y" /></td>
    <td class= title><textarea class= "common" name="DataSize"  cols="20" rows="8"></textarea></td>
  </tr>
</table>
<table class= common>
<tr class=title>
<td class=common>
数据维护原因：
<br>
  <textarea name="Reason" cols="80" rows="8">由于输入错误、程序错误导致需要维护数据。</textarea>


</td>
</tr>
<tr class= title>
<td class= common>

  </td>
  </tr>
<input type="hidden" name=fileName />

		
  <table width="200">
  		<tr>
  			<td width="100">
  				<INPUT class=cssButton name="saveButton" VALUE="数据维护信息保存"  TYPE=button onclick="fileanddata();">
  			</td>		
  			<td width="100">
  				<INPUT class=cssButton name="querybutton" VALUE="数据维护信息查询"  TYPE=button onclick="queryLog();">
  			</td>			
  		</tr>
  </table>
  	
  <table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divTemplet);">
        </td>
  	    <td class=titleImg>数据维护信息查询结果</td>
  	  </tr>
  </table>
 <div id="divTemplet" style="display:''">
   <table class=common>
       <tr class=common>
  	     <td align=left colSpan=1>
  	       <span id="spanTempletGrid">
  	       </span>
  	     </td>
  	   </tr>
  	 </table>
 </div>	 
 
 	<div  id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();">
   </div> 	
</table>
 <input type=hidden id="fmtransact" name="fmtransact">
</form>
<h1><font color="#ff0000">反馈意见，一定要比对反馈号和查询到的反馈号是否相同，否则将无法更新</font></h1>
		<form method=post name=fmdown target="fraSubmit">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询的文件名：
					</td>
				</tr>
			</table>
			<table class=common align=center border="0" width="30%">
				<tr>
					<td class=title>
						文件名称:
					</td>
					<td class=input>
						<input class=common type="text" name="fileName">
					</td>
						<input class="codeno" name="fileType" value="1" type="hidden" />
						<input class="codename" readonly="readonly" name="fileTypeName" elementtype=nacessary value="契约" type="hidden" />
						<input class="codeno" name="fileDetailType" value="1" type="hidden" />
						<input class="codename" type=hidden name="fileDetailTypeName" value="契约团单" elementtype=nacessary />
				</tr>
			</table>
			<input value="查  询" class=cssButton type=button onclick="querydownload();">
			<input class=common type=hidden name="FileCode">
			<!-- 查询结果 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFile);">
					</td>
					<td class=titleImg>
						查询结果列表
					</td>
				</tr>
			</table>
<div id="divFile" style="display:''">
<table class=common>
      <tr class=common>
  	    <td text-align=left colSpan=1>
  	   <span id="spanFileGrid">
  	    
  	   </span>
  	    </td>
  	  </tr>
  	</table>	
</div>
<div id= "divPage4" align=center style= "display:''">
      <input CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage4.firstPage();"> 
      <input CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage4.previousPage();"> 					
      <input CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage4.nextPage();"> 
      <input CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage4.lastPage();">
</div> 	

	<table class=common>
		<tr>
			<td>
				<!--div id="filesList" style="margin-top:10px;"></div-->	
				<input value="下  载" class=cssButton type=button onclick="downLoad();">
			</td>
       </tr>
	</table>
			<!--div><a href="../temp/upload/3_8.rar">3_8.rar</a></div-->				
</form>		
<form method="post" name="fm0"  target="fraSubmit">
<br>
<table class= common>
<tr>
<td>
保全
<input type="checkbox" name="FeedBackModule1" value="保全" />
</td>
<td>
契约
<input type="checkbox" name="FeedBackModule2" value="契约" />
</td>
<td>
财务
<input type="checkbox" name="FeedBackModule3" value="财务" />
</td>
<td>
销售
<input type="checkbox" name="FeedBackModule4" value="销售" />
</td>
<td>
再保	
<input type="checkbox" name="FeedBackModule5" value="再保" />
</td>
<td>
理赔
<input type="checkbox" name="FeedBackModule6" value="理赔" />
</td>
<td>
配置管理
<input type="checkbox" name="FeedBackModule7" value="配置" />
</td>
</tr>
</table>

 
<input class="readonly" name="FeedBackId" type=hidden >
   <input class="readonly" name="FeedId1" type=hidden >
 <br>
<table width="200">
  		<tr>
  			<td width="100">
  				<INPUT class=cssButton name="feedsaveButton" VALUE="选择需要的模块"  TYPE=button onclick="feedsave();">
  			</td>
  			<td width="100">
  				<INPUT class=cssButton name="feedquerybutton" VALUE="查询模块回复记录"  TYPE=button onclick="feedqueryLog();">
  			</td>			
  		</tr>
  	</table>
<table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFeed);">
        </td>
  	    <td class=titleImg><h1><font color="#ff0000">相应模块输入意见</font></h1></td>
  	  </tr>
    </table>
<div id="divFeed" style="display:''" >
<table class=common>
      <tr class=common>
  	    <td text-align=left colSpan=1>
  	      <span id="spanFeedGrid">
  	      </span>
  	    </td>
  	    
  	  </tr>
  	  <tr>
  	  <td width="100">
  				<INPUT class=cssButton name="feedmodifyButton" VALUE="保存意见"  TYPE=button onclick="feedupdataLog();">
  			</td>
  			</tr>	
  	</table>	
  			
</div>	 
<div  id= "divPage2" align=center style= "display: '' ">
      <input CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage2.firstPage();"> 
      <input CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage2.previousPage();"> 					
      <input CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage2.nextPage();"> 
      <input CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage2.lastPage();">
</div> 	
<input type=hidden name="Attitude"  value="请您填写意见" id="Attitude" >
<input  type=hidden name="Principal" id="Principal" value="_" >
<input  type=hidden id="State1" name="State1" value="N" >

<input type=hidden id="FeedbackModule" name="FeedbackModule" >

<input type=hidden id="cq" name="cq">
<input type=hidden id="fmtransact" name="fmtransact">
<input  type=hidden id="State" name="State" >
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>