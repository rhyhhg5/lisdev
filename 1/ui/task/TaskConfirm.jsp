<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskConfirm.jsp
//程序功能：工单管理工单转交页面
//创建日期：2005-01-17
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskConfirm.js"></SCRIPT>
  <%@include file="TaskConfirmInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单转交</title>
</head>
<body onload="initForm();" >
<form action="TaskConfirmSave.jsp" method=post name=fm target="fraSubmit">
	<%@include file="TaskCommonView.jsp"%>
	<table class= common>
		<tr class="common">
			<td class= title>审批意见</td>
			<td class= input colspan="5">
				<textarea class= common name="Remark" cols="117%" rows="3"></textarea> 
			</td>
		</tr>
		<tr class="common" >
			<td  class= input colspan="6">
				<input type="radio" name="ConfirmFlag" id="ConfirmFlag" value="99" checked>同意
				<input type="radio" name="ConfirmFlag" id="ConfirmFlag" value="88" >不同意
			</td>
		</tr>
	</table>
	<br>
    <input type="button" class=cssButton name="Send" value="保  存" onclick="save();">&nbsp;
    <input type=hidden id="fmtransact" name="fmtransact" value="">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
