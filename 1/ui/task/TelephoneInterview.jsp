<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TelephonInterview.jsp
//程序功能：电话回访页面
//创建日期：2005-03-28
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 

  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>电话回访 </title>
</head>

<body" >
 <form action="" method=post name=fm target="fraSubmit">
 	<%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
 	
 	<br><br>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divReceive);"> 
      </td>
      <td width="100%" class= titleImg>一、谢谢。请问您是否已经签收了保单和发票？ </td>
    </tr>
  </table>
  <Div  id= "divReceive" style= "display: ''" align=center>  
  	<table  class= common>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="receiveRadio" value="0">收到了
					<input type=radio name="receiveRadio" value="1">没收到
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="receiveRemark" cols="70%" rows="3"></textarea>
				</td>
			</tr>
	</table>
  </Div>
  
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divSigniture);"> 
      </td>
      <td width="100%" class= titleImg>二、请问投保书是由您和被保人亲自签的名，是吗？</td>
    </tr>
  </table>
  <Div  id= "divSigniture" style= "display: ''" align=center>  
  	<table  class= common>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="signatureRadio" value="0">是
					<input type=radio name="signatureRadio" value="1">不是
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="signatureRemark" cols="70%" rows="3"></textarea>
				</td>
			</tr>
	</table>
  </Div>
  
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUnderstand);"> 
      </td>
      <td width="100%" class= titleImg>三、请问您现在是否清楚地了解您这份保险的保障内容？</td>
    </tr>
  </table>
  <Div  id= "divUnderstand" style= "display: ''" align=center>  
  	<table  class= common>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="understandRadio" value="0">是
					<input type=radio name="understandRadio" value="1">不是
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="understandRemark" cols="70%" rows="3"></textarea>
				</td>
			</tr>
	</table>
  </Div>
  
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPeriod);"> 
      </td>
      <td width="100%" class= titleImg>四、顺便提醒您（这句话视回访状况也可以不说），在您收到保单后享有10天犹豫期。</td>
    </tr>
  </table>
  <Div  id= "divPeriod" style= "display: ''" align=center>  
  	<table  class= common>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="receiveRadio" value="0">知道犹豫期
					<input type=radio name="receiveRadio" value="1">不知道犹豫期
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="periodRemark" cols="70%" rows="3"></textarea>
				</td>
			</tr>
	</table>
  </Div>
  
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divConfirm);"> 
      </td>
      <td width="100%" class= titleImg>五、为了和您保持顺畅的联系，再次与您确认：您的收信地址是……，邮编是……，联系电话是……对吗？</td>
    </tr>
  </table>
  <Div  id= "divConfirm" style= "display: ''" align=center>  
  	<table  class= common>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="confirmRadio" value="0">是
					<input type=radio name="confirmRadio" value="1">不是
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="confirmRemark" cols="70%" rows="3"></textarea>
				</td>
			</tr>
	</table>
  </Div>
  
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSatisfaction);"> 
      </td>
      <td width="100%" class= titleImg>六、我们想了解，您对我们的业务员***的服务是否感到满意？</td>
    </tr>
  </table>
  <Div  id= "divSatisfaction" style= "display: ''" align=center>  
  	<table  class= common>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="satisfactionRadio" value="0">满意
					<input type=radio name="satisfactionRadio" value="1">不满意
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="satisfactionRemark" cols="70%" rows="3"></textarea>
				</td>
			</tr>
	</table>
  </Div>
  
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSuggest);"> 
      </td>
      <td width="100%" class= titleImg>七、另外，我们想知道您对我们的服务还有哪些意见或建议。</td>
    </tr>
  </table>
  <Div  id= "divSuggest" style= "display: ''" align=center>  
  	<table  class= common>
  		<tr class=common>
				<td class=input valign="top">
					&nbsp;建议
				</td>
				<td>
					<textarea classs ="common" name="suggestContent" cols="70%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="suggestRemark" cols="70%" rows="3"></textarea>
				</td>
			</tr>
	</table>
  </Div>
  </Form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>