<%
//文件名：GroupInfo.jsp
//程序功能：团体客户信息页面
//创建日期：2005-05-09
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<table id="GroupTable" style="display: none">
    <tr>
      <td  class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divGroupInfo)">
      </td>
    <td class= titleImg>客户基本信息&nbsp;</td>
    </tr>
  </table>
  <Div id="divGroupInfo" style="display: none"> 
  <table  class= common>
    <TR  class= common>           
	    <TD  class= title>
	      单位名称
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GGrpName readonly >
	    </TD>
	    <TD  class= title>
	      行业性质
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GBusinessType readonly >
	    </TD>
	    <TD  class= title>
	      企业类型
	    </TD>
	    <TD  class= input>
	      <Input class="readonly"  name=GGrpNature readonly >
	    </TD>
	 </TR>
     <TR class= common>
	    <TD  class= title>
	      总人数
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GPeoples readonly >
	    </TD>
	    <TD  class= title>
	      注册资本
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GRgtMoney readonly >
	    </TD>
	    <TD  class= title>
	      资产总额
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GAsset readonly >
	    </TD>
	 </TR>
	 <TR  class= common>
		<TD  class= title>
	      主营业务
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GMainBussiness readonly >
	    </TD>
	    <TD  class= title>
	      法人
	    </TD>
   		<TD  class= input>
	      <Input class="readonly" name=GCorporation readonly >
	    </TD>
	    <TD  class= title>
	      传真
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GFax readonly >
	    </TD>
	 </TR>
	 <TR>
	    <TD  class= title>
	      单位电话
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GPhone readonly >
	    </TD>
	    <TD  class= title>
	      负责人
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GSatrap readonly >
	    </TD>
	    <TD  class= title>
	      E_mail
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GEMail readonly >
	    </TD>
	  </TR>
	  <TR>
	    <TD  class= title>
	      成立日期
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GFoundDate readonly >
	    </TD>
	    <TD  class= title>
	      银行帐号
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GBankAccNo readonly >
	    </TD>
	    <TD  class= title>
	      状态
	    </TD>
			<TD  class= input>
	      <Input class="readonly" name=GState readonly >
	    </TD>
    </table>
  </Div>