<%
//程序名称：TaskPostPurviewInit.jsp
//程序功能：项目权限设置界面
//创建日期：2005-07-05
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="javascript">
var turnPage = new turnPageClass();
var loadFlag = "<%=request.getParameter("loadFlag")%>";
var ProjectPurviewNo = "<%=request.getParameter("ProjectPurviewNo")%>";

function initForm()
{
	initElementtype();
	showCodeName();
}

function initBox()
{
	var sql = 	"select ProjectPurviewNo, ProjectNo, ProjectType, DealOrganization, PostNo, "
				+"		MoneyPurview, ExcessPurview, TimePurview, NeedOtherAudit, Remark "
				+ "from LGProjectPurview "
				+ "where ProjectPurviewNo='" + ProjectPurviewNo + "' ";
	var result = easyExecSql(sql);
	
	if(result)
	{
		fm.ProjectPurviewNo.value = ProjectPurviewNo;
		fm.ProjectNo.value = result[0][1];
		fm.ProjectNoName.value = result[0][1];
		fm.UProjectType.value = result[0][2];
		fm.hidden1.style.display = "none";
		fm.hidden2.style.display = "";
		fm.UDealOrganization.value = result[0][3];
		fm.hidden3.display = "none";
		fm.hidden4.display = "";
		fm.PostNo.value = result[0][4];
		fm.PostNoName.value = result[0][4];
		fm.MoneyPurview.value = result[0][5];
		fm.ExcessPurview.value = result[0][6];
		fm.TimePurview.value = result[0][7];
		fm.Remark.value = result[0][9];
	}
}
</script>