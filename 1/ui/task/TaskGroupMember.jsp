<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskGroupMember.jsp
//程序功能：
//创建日期：2005-02-23 09:59:55
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskGroupMember.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="TaskGroupMemberInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./TaskGroupMemberSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGWorkBox1);">
    		</td>
    		 <td class= titleImg>
        		 小组成员信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLGWorkBox1" style= "display: ''">
		<table  class= common align='center' >
		  <TR  class= common>
		    <TD  class= title>
		      成员编号
		    </TD>
		    <TD  class= input>
		      <input class="readonly" id="MemberNoBak" name="MemberNoBak" style="display:'none'" readonly >
		      <input class="codeNo" id="MemberNo" name="MemberNo" ondblclick="return showCodeList('TaskMemberNo', [this,MemberNoName], [0,1]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,MemberNoName],[0,1]);" ><input class="codeName" id="MemberNoName" name="MemberNoName"  elementtype="nacessary" >
		    </TD>
		    <TD class= title>
		      成员所在小组
		    </TD>
		    <TD  class= input>
		      <input class="code" name="GroupNoBak" type="hidden">
		      <input class="codeNo" name="GroupNo" ondblclick="return showCodeList('AcceptCom', [this,GroupNoName], [0,1]);" onkeyup="return showCodeListKey('AcceptCom', [this,GroupNoName],[0,1]);" verify="成员所在小组|notnull&code:AcceptCom"><input class="codeName" name="GroupNoName" elementtype="nacessary" >
		    </TD>
		  </TR>
		  <TR  class= common>
		    <TD  class= title>
		      成员岗位
		    </TD>
		    <TD  class= input>
		      <input class="codeNo" name="PostNo"  ondblclick="return showCodeList('position', [this,PostNoName], [0,1]);" onkeyup="return showCodeListKey('position', [this,PostNoName],[0,1]);" verify="成员岗位|code:position"><input class="codeName" name="PostNoName">
		    </TD>
		     <TD  class= title style="display: ''" id="SuperManagerTitle">
		      业务上级
		    </TD>
		    <TD  class= input style="display: ''" id="SuperManagerContent">
		      <input class="codeNo" name="SuperManager" ondblclick="return showCodeList('TaskMemberNo', [this,SuperManagerName], [0,1]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,SuperManagerName],[0,1]);" name="SuperManager"><input class="codeName" name="SuperManagerName" >
		    </TD>
		    <TD  class= title style="display: 'none'" id="SuperManager1Title">
		      个险业务上级
		    </TD>
		    <TD  class= input style="display: 'none'" id="SuperManager1Content">
		      <input class="codeNo" name="SuperManager1" ondblclick="return showCodeList('TaskMemberNo', [this,SuperManager1Name], [0,1]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,SuperManager1Name],[0,1]);" name="SuperManager1"><input class="codeName" name="SuperManager1Name" >
		    </TD>
		  </TR>
		  <TR  class= common style="display: 'none'" id="SuperManager2Content">
		     <TD  class= title>
		      团险业务上级
		    </TD>
		    <TD  class= input>
		      <input class="codeNo" name="SuperManager2" ondblclick="return showCodeList('TaskMemberNo', [this,SuperManager2Name], [0,1]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,SuperManager2Name],[0,1]);" name="SuperManager2"><input class="codeName" name="SuperManager2Name" >
		    </TD>
		    <TD  class= title>
		      银代业务上级
		    </TD>
		    <TD  class= input>
		      <input class="codeNo" name="SuperManager3" ondblclick="return showCodeList('TaskMemberNo', [this,SuperManager3Name], [0,1]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,SuperManager3Name],[0,1]);" name="SuperManager3"><input class="codeName" name="SuperManager3Name" >
		    </TD>
		  </TR>
		</table>
    </Div>
    
    <table>			
		<tr>
		  <td width="17"> 
		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divpostPurview);"> 
		  </td>
		  <td width="185" class= titleImg>项目权限规则管理</td>
		</tr>
	</table>
	<div id="divpostPurview" style="display: ''">
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
					<span id="spanPurviewGrid" >
					</span> 
			  	</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: 'none' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage(); showCodeName();"> 
			<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage(); showCodeName();"> 					
			<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage(); showCodeName();"> 
			<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage(); showCodeName();"> 
		</Div>
	</div>
	
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
