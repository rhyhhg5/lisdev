<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskUniteSave.jsp
//程序功能：
//创建日期：2005-04-14
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
	//输入参数	
	String workNo = request.getParameter("workNo");	//选中的radio的值
	String[] workNos = new String[2];	//radion中各选项的值
	workNos[0] = request.getParameter("workNo1");
	workNos[1] = request.getParameter("workNo2");
	  
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  LGWorkSchema tLGWorkSchema = new LGWorkSchema();
  LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
  TaskUniteBL tTaskUniteBL = new TaskUniteBL();
  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");    
  
  if(transact.equals("INSERT||MAIN"))
  {
  	//
  }
  else if(transact.equals("UPDATE||MAIN"))
  {
  	tLGWorkSchema.setWorkNo(workNo);
  }
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tG);
		tVData.add(tLGWorkSchema);
		tVData.add(workNos);
		tVData.add(request.getParameter("remark"));
		
    tTaskUniteBL.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tTaskUniteBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";    	
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
