//程序名称：TaskRemark.js
//程序功能：工单管理工单批注页面
//创建日期：2005-01-22 16:17:40
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

/* 提交表单 */

var showInfo;

function submitForm()
{
  if(!verifyInput2())
  {
      return false; 
  }
  
  fm2.WorkNo.value = fm.WorkNo.value;
  
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm2.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
    	top.close();
    	top.opener.focus();
	}
}      