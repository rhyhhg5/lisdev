<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PhoneInterviewSave.jsp
//程序功能：
//创建日期：2005-03-31
//创建人  ：Yang Yalin
//更新记录：更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>

<%               
	String FlagStr = "";
	String Content = "";
	
	//得到前页传入的WorkNo
	String phoneWorkNo = request.getParameter("WorkNo");
	String typeNo = request.getParameter("TypeNo");
	if(phoneWorkNo == null )
	{
		System.out.println("phoneWorkNO==null");
		phoneWorkNo = request.getParameter("phoneWorkNo");
	}
	if(phoneWorkNo == null || phoneWorkNo.equals(""))
	{
		//得到新建电话回访时生成的工单号
		phoneWorkNo = (String) session.getAttribute("VIEWWORKNO");
	}
	GlobalInput tG=(GlobalInput)session.getValue("GI");
	String transact = request.getParameter("fmtransact");
	
	//每条工单对应其各问题,每个问题对应一个LGPhoneAnwserSchema
	LGPhoneAnwserSet tLGPhoneAnwserSet = new LGPhoneAnwserSet();
	
	String contNo = request.getParameter("contNo");
	int count = 0;
	
	for(int i = 1; i < 11; i++)
	{
		String answer = "answer" + i; 	//Connecting the name of answer for each question
		String remark = "remark" + i;		//Connecting the name of remark fof each question
		String item = "item" + i;
		
		String answerValue = request.getParameter(answer);
		String remarkValue = request.getParameter(remark);
		
		if(answerValue != null || !remarkValue.equals(""))
		{
			System.out.println(i + ":" + answerValue + "\t" + remarkValue);
			LGPhoneAnwserSchema tLGPhoneAnwserSchema = new LGPhoneAnwserSchema();
			tLGPhoneAnwserSchema.setPhoneItemNo(request.getParameter(item)); 
			tLGPhoneAnwserSchema.setPhoneWorkNo(phoneWorkNo); 	
			tLGPhoneAnwserSchema.setPhoneAnwer(answerValue);
			tLGPhoneAnwserSchema.setPhoneRemark(remarkValue);
			tLGPhoneAnwserSchema.setPhoneTypeNo(request.getParameter("TypeNo"));
			tLGPhoneAnwserSchema.setContNo(contNo);
			tLGPhoneAnwserSet.add(tLGPhoneAnwserSchema);  
			count++;		
		}	  
	}
	if(count == 0)
	{
		LGPhoneAnwserSchema phoneSchema = new LGPhoneAnwserSchema();
		phoneSchema.setPhoneItemNo("1"); 
		phoneSchema.setPhoneWorkNo(phoneWorkNo); 	
		phoneSchema.setPhoneAnwer(null);
		phoneSchema.setPhoneRemark("");
		phoneSchema.setPhoneTypeNo("05");
		phoneSchema.setContNo(contNo);
		tLGPhoneAnwserSet.add(phoneSchema);
	}
	
  
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGPhoneAnwserSet);
	tVData.add(tG);
	  	
	PhoneAnwserBL tPhoneAnwserBL = new PhoneAnwserBL();
	if (tPhoneAnwserBL.submitData(tVData, transact) == false)
	{
		FlagStr = "Fail";
		Content = "数据保存失败！";
	}
	else
	{
		System.out.println("数据保存成功！");
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}  
%>
<html>
<script language="javascript">
  if("<%=transact%>" == "UPDATE||MAIN")
  {
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>")
  }
</script>
</html>
