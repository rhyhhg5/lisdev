
<%@page import="com.sinosoft.lis.schema.LGWorkSchema"%>
<%@page import="com.sinosoft.task.PWSubServiceTypeMUI"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PWSubServiceTypeMSave.jsp
//程序功能：
//创建日期：2016-12-29 10:03:33
//创建人  ：Yu ZhiWei
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%

	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	
	String acceptNo = request.getParameter("AcceptNo");
	String typeNo = request.getParameter("TypeNo");
	//String managementOrganization = request.getParameter("ManagementOrganization");
	String lgWorkAcceptCom = request.getParameter("lgWorkAcceptCom");
	String transact = request.getParameter("fmtransact");
	String statusNo = request.getParameter("StatusNo");
	GlobalInput tG = new GlobalInput(); 
	  tG=(GlobalInput)session.getValue("GI");
	PWSubServiceTypeMUI pwSubServiceTypeMUI = new PWSubServiceTypeMUI();
	
	LGWorkSchema schema = new LGWorkSchema();
	schema.setWorkNo(acceptNo);
	schema.setAcceptNo(acceptNo);
	schema.setTypeNo(typeNo);
	//schema.setStatusNo(statusNo); 
	schema.setDetailWorkNo(acceptNo);
	schema.setAcceptCom(lgWorkAcceptCom);
	
	try
	  {
	  // 准备传输数据 VData
	  
	  	VData tVData = new VData();
		tVData.add(schema);
	  	tVData.add(tG);
	  	pwSubServiceTypeMUI.submitData(tVData,transact);
	  }
	  catch(Exception ex)
	  {
	    Content = "修改失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }
	  
	  if (FlagStr=="")
	  { 
	    tError = pwSubServiceTypeMUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 修改成功! ";
	    	FlagStr = "Success";
	 
	    }
	    else                                                                           
	    {
	    	Content = " 修改失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  }

 %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
</script>
</html>