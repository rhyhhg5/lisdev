<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskDeliverSava.jsp
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-01-19 20:14:18
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	
	//输入参数
	String[] tWorkNo;
	String loadFlag = request.getParameter("loadFlag");
	System.out.println("loadFlag: " + loadFlag);
	if(loadFlag != null && loadFlag.equals("TaskViewTopButton"))
	{
	  tWorkNo = new String[1];
	  tWorkNo[0] = request.getParameter("WorkNo");
	  
	  if(tWorkNo[0] == null || tWorkNo[0].equals(""))
	  {
	    tWorkNo[0] = (String)session.getAttribute("VIEWWORKNO");
	  }
	}
  else
  {
	  tWorkNo = (String[]) session.getAttribute("WORKNO");
	  //System.out.println("WORKNO=" + tWorkNo[0]);
	    System.out.println(tWorkNo);
	}
	
	LGWorkRemarkSchema tLGWorkRemarkSchema = new LGWorkRemarkSchema();
	tLGWorkRemarkSchema.setRemarkContent(request.getParameter("RemarkContent"));
	
	//从session中得到全局参数
	GlobalInput tGI = (GlobalInput) session.getValue("GI");	

	VData tVData = new VData();
  tVData.add(tGI);
	tVData.add(tWorkNo);
	tVData.add(tLGWorkRemarkSchema);
	
	TaskRemarkBL tTaskRemarkBL = new TaskRemarkBL();
	if (tTaskRemarkBL.submitData(tVData, "ok") == false)
	{
		FlagStr = "Fail";
		Content = "工单批注失败。";
	}
	else 
	{
		Content = "工单批注成功。";
	}
	//设置显示信息
	VData tRet = tTaskRemarkBL.getResult();
    System.out.println(Content);
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

