var customerAnwserLength = 0;
var mOperateFalg = "";

//新建函件
function create()
{
	//控制保存操作，若为0则是新建函件，可以保存，为1则是察看函件不可以保存
	fm.showLetterFlag.value = "0";
	
	document.all("lettertype1").style.display="";
	document.all("lettertype2").style.display="";
	document.all("sendCommonId").style.display = "";
	fm.letterType.value = "1";  //非核保函件
	
	//隐藏扫描件区
	document.all("scanId").style.display = "none";
	
	//隐藏函件正文预览区
	document.all("pictureId").style.display = "none";
	
	//隐藏反馈意见区
	document.all("feedBack").style.display = "none";
	
	//隐藏客户回复区
	document.all("divAnwser").style.display = "none";

	document.all("createLetterId").style.display = "none";
	
	fm.letterInfo.value = "";
	fm.letterInfo.readOnly = false;
}

//选择函件类型触发的事件
function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "letterType")
	{
		document.all("createLetterId").style.display = "";
		document.all("saveLetter").style.display = "";
		document.all("letterInfo").style.display = "";
	}
	
	if (cCodeName == "backFlag1")
	{
	  if(fm.backFlag.value == "1")
	  {
		  document.all("bakeDateID").style.display = "";
		  document.all("bakeDateID2").style.display = "";
	  }
	  if(fm.backFlag.value == "0")
	  {
		  document.all("bakeDateID").style.display = "none";
		  document.all("bakeDateID2").style.display = "none";
	  }
	}
}

//选择函件引发的事件
function showLetter()
{
	if((LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 15))=="0"&&(LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 6))=="" )
	{
		fm.showLetterFlag.value = "0";
		fm.letterType.value = "0";  //核保函件
		fm.SerialNumber.value = LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2);  
   	document.all("lettertype1").style.display="none";
    document.all("lettertype2").style.display="none";
   	
		document.all("saveLetter").style.display = "none";
   	document.all("sendCommonId").style.display = "";
	  document.all("pictureId").style.display = "";
	}
	else
	{
		fm.showLetterFlag.value = "1";
		fm.SerialNumber.value = LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2);
		document.all("saveLetter").style.display = "none";
		document.all("sendCommonId").style.display = "none";
		document.all("pictureId").style.display = "";
  }
	
	//显示编辑区，在此处显示函件
	var sql = "  select state, letterType, FeedBackInfo, letterInfo, "
	          + "    addressNo, backFlag , backDate "
			  + "from LGLetter "
			  + "where edorAcceptNo='" + fm.workNo.value + "' "
			  + "	and serialNumber='" 
			  + 		LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2) + "' ";
	result = easyExecSql(sql);
	
	if(result)
	{
    if(result[0][0] != "0")
    {
      fm.backDate.readOnly = true;
    }
		if(result[0][0] == "3" || result[0][0] == "4")
		{
			if(result[0][1] == "0")
			{
				//若核保函件已回销或已强制回销，则显示客户答复
				document.all("divAnwser").style.display = "";
				document.all("saveAnwser").style.display = "none";
			}
			else
			{
				//若非核保函件已回销或已强制回销，则显示反馈信息
				document.all("feedBack").style.display = "";
				fm.feedBackInfo.value = result[0][2];
				document.all("idSaveFeedBack").style.display = "none";
			}
		}
		
		fm.letterType.value = result[0][1];
		fm.addressNo.value = result[0][4];
		fm.letterInfo.value = result[0][3];
		fm.letterInfo.readOnly = true;
    fm.backDate.value = result[0][6];
    fm.backFlag.value = result[0][5];
    if(result[0][5] == "0")
    {
        fm.backFlagName.value = "否";
    }
    else
    {
        fm.backFlagName.value = "是";
    }
		
		//隐藏新建函件区域
		//fm.picture.value = result[0][3];
		document.all("createLetterId").style.display = "";
		document.all("letterInfoId").style.display = "none";
		
		//隐藏扫描件
		document.all("scanId").style.display = "none";
	}

	var appletWidth = screen.availWidth - 50;
	var appletHeight = 250;
	document.all("pictureId").width = appletWidth;
	document.all("pictureId").height = appletHeight;
	document.all("pictureId").src = "TaskLetterContent.jsp?"
			+ "edorAcceptNo=" + fm.workNo.value 
			+ "&serialNumber=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2)
			+ "&letterType=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 15)
			+ "&appletWidth=" + appletWidth
			+ "&appletHeight=" + appletHeight;
}

//下发函件
function sendLetter()
{
    if(!checkOperate())
	{
		return false;	
	}
	var letterType = LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 15);
	if (letterType == "0" && fm.backFlag.value == "0")
	{
		alert("核保函件必须回销！");
		return false;
	}
		
	if(LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 13) != "0")
	{
	    alert("函件不是待下发函件，不能下发");
	    return false;
	}
	
	window.open("TaskLetterSendMain.jsp?loadFlag=SHOWLETTER&edorAcceptNo=" + fm.workNo.value 
		+ "&serialNumber=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2) 
		+ "&letterType=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 15)
		+ "&backFlag=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 9)
		+ "&backDate=" + fm.backDate.value, 
		"print", "height=200,width=500,top=200,left=250,toolbar=yes,status=yes,menubar=yes");
}  

//回销函件
function takeBack()
{
  if(!checkOperate())
  {
  	return false;	
  }
  
  
  fm.takeBackType.value = "FeedBack";
  
  
  //查询函件的类型，若为1，显示反馈信息框； 若为0，显示客户答复框
  
  var sql = "select LetterType "
  		  + "from LGLetter "
  		  + "where edorAcceptNo='" + fm.workNo.value + "' "
  		  + "	and serialNumber='" 
  		  + 	LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2) + "' ";
  var result = easyExecSql(sql);
  
  if(result)
  {
  	//非核保函件
  	if(result[0][0] == "1")
  	{
  		document.all("feedBack").style.display = "";
  	}
  	//核保函件
  	else
  	{
  
  		document.all("divAnwser").style.display = "";
  	
  		document.all("saveAnwser").style.display = "";
  	}
  }
  
	return true;
}

//根据保全项目编号查询项目名
function getEdorTypeName(edorCode)
{
	var sql = "  select edorName "
			  + "from LMEdorItem "
			  + "where edorCode = '" + edorCode + "' "
	result = easyExecSql(sql);
	
	if(result)
	{
		return result[0][0];
	}
	else
	{
		"+-1";
	}
}

//响应填写客户答复时的取消按钮事件
function cancelCustomerAnswer()
{
	//将填写了的客户答复清空
	for(var i = 0; i < customerAnwserLength; i++)
	{
		var Answer = "AnswerUWNo" + i;
		
		fm.all(Answer)[0].checked = false;
		fm.all(Answer)[1].checked = false;
	}
}

// 保存客户答复，并回销 核保函件
function saveCustomerAnswer()
{
	fm.action = "TaskLetterBackUWSave.jsp?loadFlag=" + fm.takeBackType.value
			+ "&edorAcceptNo=" + fm.workNo.value 
			+ "&serialNumber=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2)
			+ "&answerLength=" + customerAnwserLength;
	submitForm();
}

//强制回销
function forceTakeBack()
{
	
	if(!takeBack())
	{
		return false;
	}
	
	fm.takeBackType.value = "ForceFeedBack";
}



//查看扫描建
var scanOpenFlag = 0;
function viewScan()
{
	if(scanOpenFlag == 0)
	{
		//隐藏函件编辑区
		document.all("sendCommonId").style.display = "none";
		document.all("createLetterId").style.display = "none";
		document.all("divCustomerAnwser").style.display = "none";
	
		//显示扫描件
		document.all("scanId").style.display = "";
		scanOpenFlag = 1;
	}
	else
	{
		document.all("scanId").style.display = "none";
		scanOpenFlag = 0;
	}
}

//选择扫描件引起的事件
function showScan()
{
	var row = ScanGrid.getSelNo();
	
	try
	{
		document.frames["scanIframe"].document.getElementById("service").src = 
	        document.frames["scanIframe"].arrPicName[row - 1];
	}
	catch(e)	
	{
		alert(e);
	}
		
}

//检验操作的合法性
function checkOperate()
{
	var row = LetterGrid.getSelNo() - 1;
	
	if(row < 0)
	{
		alert("请选择函件");
		
		return false;
	}
	
	return true;
}

function cancel()
{
	fm.letterInfo.value = "";
	document.all("createLetterId").style.display = "none";
	document.all("sendCommonId").style.display = "none";
}

//预览新建的函件
function preview()
{
	fm.target="_blank";
	fm.action = "TaskPreviewLetterMain.jsp";
	fm.submit();
}

//保存函件
function saveDate()
{
  if (fm.sendObjct.value == "")
  {
    alert("请选择下发对象！");
    fm.sendObjct.focus();
    return false;
  }
  if (fm.letterSubType.value == ""&&(LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 15))!="0")
  {
    alert("请选择函件类型！");
    fm.letterSubType.focus();
    return false;
  }
 // if (fm.addressNo.value == "" )
 // {
 //   alert("请选择地址！");
 //   fm.addressNo.focus();
 //   return false;
 // }
 
  if (fm.letterInfo.value == "" &&(LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 15))!="0")
  {
    alert("函件内容不能为空！");
    fm.letterInfo.focus();
    return false;
  }
  if (!verifyInput2())
  {
    return false;
  }
	fm.target = "fraSubmit";
	fm.action = "TaskLetterSave.jsp";
	submitForm();
}

function cancelFeedBack()
{
	fm.feedBackInfo.value = "";
}

//函件回销保存  非核保
function saveFeedBack()
{

  if (fm.feedBackInfo.value == "")
  {
    alert("反馈意见不能为空！");
    fm.feedBackInfo.focus();
    return false;
  }
  
  if (!verifyInput2())
  {
    return false;
  }

	fm.action = "TaskLetterOperateSave.jsp?loadFlag=" + fm.takeBackType.value
			+ "&edorAcceptNo=" + fm.workNo.value 
			+ "&serialNumber=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2);
	submitForm();
}

function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();
	
}


//提交数据后的操作
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
	  if (mOperateFalg == "DELETE")
	  {
	    content = "数据删除成功！";
	    mOperateFalg = "";
	  }
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
		//若执行函件回销操作，则返回到保全申请界面
		if(fm.takeBackType.value == "FeedBack" || fm.takeBackType.value == "ForceFeedBack"
		    || fm.takeBackType.value == "Delete")
		{
			//top.close();
			//top.opener.top.focus();
			top.location.reload();//新加
			top.opener.top.fraInterface.location.replace("TaskPersonalBoxSave.jsp?WorkNo="+fm.workNo.value);
			top.opener.top.opener.location.reload();
		}
		else
		{
			top.focus();
			top.location.reload();
		}
	}
}

function printLetter()
{
    if(!checkOperate())
    {
        return false;
    }
    
    var sql = "  select letterType, addressNo, backFlag, backDate "
              + "from LGLetter "
              + "where edorAcceptNo = '" 
              +     LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 1) + "' "
              + "   and serialNumber = '" 
              +     LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2) + "' ";
    var result = easyExecSql(sql);
    if(result)
    {
        var win = window.open("TaskPreviewLetter.jsp?loadFlag=SHOWLETTER&showToolBar=true"
        	+ "&edorAcceptNo=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 1)
        	+ "&serialNumber=" + LetterGrid.getRowColData(LetterGrid.getSelNo() - 1, 2)
        	+ "&letterType=" + result[0][0]
        	+ "&addressNo=" + result[0][1]
        	+ "&backFlag=" + result[0][2]
        	+ "&backDate=" + result[0][3]);
        win.focus();
    }
}

function deleteLetter()
{
    var row = LetterGrid.getSelNo() - 1;
	if(row < 0)
	{
		alert("请选择函件");
		
		return false;
	}
	mOperateFalg = "DELETE";
    fm.action = "TaskLetterOperateSave.jsp?loadFlag=Delete"
			+ "&edorAcceptNo=" + fm.workNo.value 
			+ "&serialNumber=" + LetterGrid.getRowColData(row, 2);
	submitForm();
}