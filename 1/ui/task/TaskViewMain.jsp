<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：TaskViewMain.jsp
//程序功能：工单管理框架页面
//创建日期：2005-04-03
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<%
	//把WorkNo添加到session中;
	String tCustomerNo = request.getParameter("CustomerNo");
	String tWorkNo = request.getParameter("WorkNo");
	String DetailWorkNo = request.getParameter("DetailWorkNo");	
	
	//查询扫描件相关类型
	String BussNoType = "";
	String BussType = "";
	String SubType = "";
	
	SSRS tSSRS = new SSRS();
	ExeSQL tExeSQL = new ExeSQL();
	String sql = 	"select BussNoType, BussType, SubType "
					+ "from ES_DOC_RELATION "
					+ "where bussNo='" + tWorkNo + "' "
					+ "    and subType = 'BQ01' ";
	
	tSSRS = tExeSQL.execSQL(sql);
	if(tSSRS.getMaxRow() > 0)
	{
		BussNoType 	= tSSRS.GetText(1, 1);
		BussType 	= tSSRS.GetText(1, 2);
		SubType 	= tSSRS.GetText(1, 3);
	}	
	
	//if ((tWorkNo != null) && (!tWorkNo.equals("")))
	//{
	//	session.setAttribute("VIEWWORKNO", tWorkNo);
	//}
	if ((tCustomerNo != null) && (!tCustomerNo.equals("")))
	{
		session.setAttribute("CUSTOMERNO", tCustomerNo);
	}
	else
	{
		System.out.println("WorkNo is null");
	}
	
	String initPage = request.getParameter("initPage");
	if(initPage == null || initPage.equals(""))
	{
		initPage = "./TaskView.jsp";
	}
	String title = "查看工单";
	String loadFlag = request.getParameter("loadFlag");
	if ((loadFlag != null) && (loadFlag.equals("PERSONALBOX")))
	{
	  title = "经办";
	}
%>
<html>
<head>
<title><%=title%></title>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>   
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<SCRIPT>	
	var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	window.resizeTo(intPageWidth,intPageHeight);
	window.moveTo(-1, -1);
	window.focus();	
	
	var initWidth = 0;
	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数
	
	function queryScanType() 
	{
	  var a;
    var strSql = "select code1, codename, codealias from ldcode1 where codetype='scaninput'";
    try
    {
      a = easyExecSql(strSql);
    }catch(ex)
    {
      alert(ex.message);
    }
    
    return a;
 } 
 var loadFlag = "<%=request.getParameter("loadFlag")%>";
</SCRIPT>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="no" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="40,0,0,*,0" cols="*">
	    <frame id="fraMenu" name="fraMenu" noresize scrolling="no" src="./TaskViewTopButton.jsp?WorkNo=<%=tWorkNo%>">
	    <frame id="fraScanList" name="fraScanList" scrolling="auto" src="./TaskScanList.jsp?WorkNo=<%=tWorkNo%>&DetailWorkNo=<%=DetailWorkNo%>">
	    <frame id="fraPic" name="fraPic" scrolling="auto" src="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo=<%=tWorkNo%>&BussNoType=<%=BussNoType%>&BussType=<%=BussType%>&SubType=<%=SubType%>">
	    <frame id="fraInterface" name="fraInterface" scrolling="auto" src= "<%=initPage%>?loadFlag=<%=loadFlag%>&WorkNo=<%=tWorkNo%>&CustomerNo=<%=tCustomerNo%>">
		  <frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>

