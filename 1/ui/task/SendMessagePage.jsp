<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>
	<%
		//程序名称：SendMessagePage.jsp
		//程序功能：信息服务界面
		//创建日期：2008-03-3 11:39:42
		//创建人  ：hyy
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
		request.setAttribute("currentDate", PubFun.getCurrentDate());
	%>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="./SendMessagePage.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<title>客户服务短信</title>
	</head>
	<body onload="">
		<form action="./SendMessagePageSave.jsp" method=post name=fm target="_self">
			<Div id="divTaskInfo" style="display: ''">
				<table class=common>
					<tr class=common>
						<TD class=title>
							客户号码
						</TD>
						<TD class=input>
							<Input class=common name="CustomerNo"
								onkeydown="queryByCustomerNo();" style="width: 100">
							<input type="button" Class="cssButton" name="search" value="客户查询"
								onclick="queryCustomerNo();">
						</TD>
						<TD class=title>
							客户姓名
						</TD>
						<TD class=input>
							<Input class=common name="CustomerName" onkeydown=""
								style="width: 100">
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>

						</TD>

					</tr>
					<tr>
						<TD class=title>
							手机号码
						</TD>
						<TD class=input>
							<Input class=common name="mobile" onkeydown="" style="width: 100">
						</TD>
						<TD class=title>
							短信类型
							<br>
						</TD>
						<TD class=input>
							<input value="客户短信通知" class="code" name="TypeNoname" readonly
								ondblclick="return showCodeList('TaskTypeNo', [this,TypeNo], [1,0], null, subTypeNoCondition, 'SuperTypeNo', 1);"
								onkeyup="return showCodeListKey('TaskTypeNo', [this,TypeNo], [1,0], null, subTypeNoCondition, 'SuperTypeNo', 1);">
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>

						</TD>
					</tr>
					<tr>
						<TD class=title>
							发送时间
						</TD>
						<TD class=input>
							<Input class=common name="sendTime" onkeydown=""
								style="width: 100"
								value="<%=request.getAttribute("currentDate")%>">
						</TD>
						<TD class=title>
							邮件地址
						</TD>
						<TD class=input>
							<Input class=common name="email" onkeydown="" style="width: 100">
						</TD>
						<TD class=title>
							<br>
						</TD>
						<TD class=input>
							&nbsp;
							<br>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							内容（尽可能不要多于70个字）
						</TD>
						<TD colspan="7" class=input>
							<textarea class="common" name="content" cols="111%" rows="2"></textarea>
						</TD>
					</tr>
					<tr>
						<td>
							<INPUT class=cssbutton VALUE="发送短邮" TYPE="submit"
								onclick="return checkData();" />
						</td>
						<td>
							<INPUT class=cssbutton VALUE="返   回" TYPE=button
								onclick="closePage();">
							<INPUT class=cssbutton VALUE="取   消" TYPE=button
								onclick="closePage();">
						</td>
					</tr>
				</table>
		</form>
	</body>
</html>