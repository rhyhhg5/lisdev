<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：TaskPhoneHastenSave.jsp
//程序功能：
//创建日期：2005-03-31
//创建人  ：Yang Yalin
//更新记录：更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>

<%              
	String FlagStr = "";
	String Content = "";
	String transact = request.getParameter("fmtransact");
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	
	//接收参数
	if(transact.equals("CancelApp"))
	{
		LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
		tLGPhoneHastenSchema.setWorkNo(request.getParameter("WorkNo"));
		tLGPhoneHastenSchema.setFinishType("0");
		
		VData v = new VData();
		v.add(tG);
		v.add(tLGPhoneHastenSchema);
		
		TaskChangeHastenStatusUI ui = new TaskChangeHastenStatusUI();
		if(!ui.submitData(v, transact))
		{
			FlagStr = "Fail";
			if(ui.mErrors.needDealError())
			{
				Content = ui.mErrors.getLastError();
			}
			else
			{
				Content = "数据保存失败！";
			}
		}
		else
		{
			System.out.println("保全申请撤销成功！");
			FlagStr = "Succ";
			Content = "保全申请撤销成功！";
		}
	}
	else
	{	
		
		String remark = request.getParameter("Remark");
		TransferData tTransferData = new TransferData();
		
		tTransferData.setNameAndValue("workNo", request.getParameter("WorkNo"));
		tTransferData.setNameAndValue("edorAcceptNo", request.getParameter("EdorAcceptNo"));
		if(request.getParameter("CheckedFlag") != null)
		{
			//重设缴费时间
			System.out.println(request.getParameter("WorkNo") + " " 
				+ request.getParameter("PayMode") + " " + request.getParameter("PayDate"));
			tTransferData.setNameAndValue("payMode", request.getParameter("PayMode"));
			tTransferData.setNameAndValue("payDate", request.getParameter("PayDate"));
		}
		
		//若电话未通且没有录入备注
		if(transact.equals("CreateHistory") && (remark == null || remark.equals("")))
		{
			remark = "自动批注：做电话催缴任务时，电话未打通。";
		}
		tTransferData.setNameAndValue("remark", remark);
    	
		// 准备传输数据 VData
		VData tVData = new VData();  	
		tVData.add(tTransferData);
		tVData.add(tG);
		  	
		TaskHastenUI tTaskHastenUI = new TaskHastenUI();
		if (!tTaskHastenUI.submitData(tVData, transact))
		{
			FlagStr = "Fail";
			if(tTaskHastenUI.mErrors.needDealError())
			{
				Content = tTaskHastenUI.mErrors.getLastError();
			}
			else
			{
				Content = "数据保存失败！";
			}
		}
		else
		{
			FlagStr = "Succ";
			Content = "数据保存成功！";
		}  
	}//else

  //添加各种预处理
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
