<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskInputPay.jsp
//程序功能：工单管理理赔信息录入页面
//创建日期：2005-02-21 16:48:16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskInputPay.js"></SCRIPT>
  <%@include file="TaskInputPayInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单录入 </title>
  <script>
  	var operator = "<%=tGI.Operator%>";  //操作员编号
  </script>
</head>
<body  onload="initForm();">
  <form action="./TaskInputSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskInfo);">
	      </td>
        <td class= titleImg>工单信息&nbsp;</td>
	    </tr>
    </table>
  <span id="operateButton">
	<table class="common" align="center" id="table1">
		<tr align="right">
			<td class="button">&nbsp;&nbsp; </td>
			<td class="button" align="right" width="10%">
			<input class="cssButton" onclick="return submitForm();" type="button" value="增  加"> 
			</td>
			<td class="button" align="right" width="10%">
			<input class="cssButton" onclick="return updateClick();" type="button" value="修  改"> 
			</td>
			<td class="button" align="right" width="10%">
			<input class="cssButton" onclick="return queryClick();" type="button" value="查  询"> 
			</td>
			<td class="button" align="right" width="10%">
			<input class="cssButton" onclick="return deleteClick();" type="button" value="删  除"> 
			</td>
		</tr>
	</table>
	</span>
	<div id="inputButton" style="DISPLAY: none">
		<table class="common" align="center" id="table2">
			<tr align="right">
				<td class="button">&nbsp;&nbsp; </td>
				<td class="button" width="10%">
				<input class="cssButton" onclick="return cancelForm();" type="button" value="取  消"> 
				</td>
				<td class="button" width="10%">
				<input class="cssButton" onclick="return resetForm();" type="button" value="重  置"> 
				</td>
				<td class="button" width="10%">
				<input class="cssButton" onclick="return submitForm();" type="button" value="保  存"> 
				</td>
			</tr>
		</table>
	</div>
	<table id="table3">
		<tr>
			<td>
			<img style="CURSOR: hand" onclick="showPage(this,divLLReport1);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
			</td>
			<td class="titleImg">报案信息 </td>
		</tr>
	</table>
	<div id="divLLReport1">
		<table class="common" id="table4">
			<tr class="common">
				<td class="title8">报案号 </td>
				<td class="input8">
				<input class="readonly" readOnly name="RptNo"> </td>
				<td class="title8">理赔状态 </td>
				<td class="input8">
				<input class="readonly" readOnly name="CaseState"> </td>
				<td class="title8">事故者类型 </td>
				<td class="input8">
				<input class="code" ondblclick="showCodeListEx('PeopleType_1',[this],[0,1,2,3]);" onkeyup="showCodeListKeyEx('PeopleType_1',[this],[0,1,2,3]);" name="PeopleType" verify="事故者类别|NOTNULL" CodeData="0|^0|被保险人^1|投保人^2|被保险人的配偶^3|被保险人的子女"> 
				</td>
			</tr>
			<tr class="common">
				<td class="title8">号码类型 </td>
				<td class="input8">
				<input class="code" ondblclick="showCodeListEx('RptObjReport',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('RptObjReport',[this],[0,1,2]);" name="RptObj" verify="号码类型|NOTNULL" CodeData="0|^0|团单^1|个单^2|客户号"> 
				</td>
				<td class="title8">号码 </td>
				<td class="input8"><input class="common" name="RptObjNo"> </td>
				<td class="title">
				<input class="cssButton" onclick="submitForm1()" type="button" value="查  询"> 
				</td>
				<td class="input8">　</td>
			</tr>
			<tr class="common8">
				<td class="title8">报案人姓名 </td>
				<td class="input8"><input class="common" name="RptorName"> </td>
				<td class="title8">报案人电话 </td>
				<td class="input8"><input class="common" name="RptorPhone"> 
				</td>
				<td class="title8">报案人通讯地址 </td>
				<td class="input8"><input class="common" name="RptorAddress"> 
				</td>
			</tr>
			<tr class="common">
				<td class="title8">出险日期 </td>
				<td class="input8">
				<input class="coolDatePicker" name="AccidentDate" dateFormat="short" timerflag="1"><img onclick="calendar(AccidentDate)" src="file:///E:/文档/gongdanpage1/gongdanpage/common/images/calendar.gif" vspace="1"> 
				</td>
				<td class="title8">出险地点 </td>
				<td class="input8"><input class="common" name="AccidentSite"> 
				</td>
				<td class="title">报案人与事故人关系 </td>
				<td class="input8">
				<input class="code" ondblclick="return showCodeList('Relation',[this]);" onkeyup="return showCodeListKey('Relation',[this]);" name="Relation"> 
				</td>
			</tr>
			<tr class="common">
				<td class="title8">报案方式 </td>
				<td class="input8">
				<input class="code" ondblclick="return showCodeList('RptMode',[this]);" onkeyup="return showCodeListKey('RptMode',[this]);" name="RptMode"> 
				</td>
				<td class="title8">管辖机构 </td>
				<td class="input8">
				<input class="readonly" readOnly name="MngCom"> </td>
				<td class="title8">报案受理日期 </td>
				<td class="input8">
				<input class="readonly" readOnly name="RptDate"> </td>
			</tr>
			<tr class="common">
				<td class="title8">报案受理人 </td>
				<td class="input8">
				<input class="readonly" readOnly name="Operator"> 
				</td>
			</tr>
		</table>
		<table class="common" id="table5">
			<tr class="common">
				<td class="title8">事故经过描述 </td>
			</tr>
			<tr class="common">
				<td class="input8">
				<textarea class="common" name="AccidentReason" rows="3" cols="100" witdh="25%"></textarea> 
				</td>
			</tr>
			<tr class="common">
				<td class="title8">事故者现状 </td>
			</tr>
			<tr class="common">
				<td class="input8">
				<textarea class="common" name="AccidentCourse" rows="3" cols="100" witdh="25%"></textarea> 
				</td>
			</tr>
			<tr class="common">
				<td class="title8">备注 </td>
			</tr>
			<tr class="common">
				<td class="title8">
				<textarea class="common" name="Remark" rows="3" cols="100" witdh="25%"></textarea> 
				</td>
			</tr>
		</table>
		<table class="common" id="table6">
			<tr>
				<td left text-align:><span id="spanNormPayCollChooseGrid">
				</span></td>
			</tr>
		</table>
	</div>
	<span id="spanLJSPayPersonCode" style="POSITION: absolute"></span>
	<table id="table7">
		<tr>
			<td class="common">
			<img style="CURSOR: hand" onclick="showPage(this,divLLSubReport);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
			</td>
			<td class="titleImg">客户信息 </td>
		</tr>
	</table>
	<div id="divLLSubReport">
		<table class="common" id="table8">
			<tr class="common">
				<td left text-align:><span id="spanSubReportGrid">
				<table class="muline" cellSpacing="0" cellPadding="0" border="0" id="table9">
					<tr aline="left">
						<td class="mulinetitle" style="WIDTH: 20px">
						<input class="title" style="WIDTH: 20px" onclick=" SubReportGrid.checkAll(this,10);" type="checkbox" name="checkAllSubReportGrid"></td>
						<td class="mulinetitle" style="WIDTH: 30px">
						<input class="mulinetitle" style="WIDTH: 30px" tabIndex="-1" readOnly value="序号" name="T1" size="20"> 
						</td>
						<td class="mulinetitle" style="WIDTH: 232px">
						<input class="mulinetitle" onmouseover="_showtitle(this);" style="WIDTH: 232px" tabIndex="-1" readOnly value="事故者客户号" name="T2" size="20"><input type="hidden" name="SubReportGridverify1"> 
						</td>
						<td class="mulinetitle" style="WIDTH: 154px">
						<input class="mulinetitle" onmouseover="_showtitle(this);" style="WIDTH: 154px" tabIndex="-1" readOnly value="事故者姓名" name="T3" size="20"><input type="hidden" name="SubReportGridverify2"> 
						</td>
						<td class="mulinetitle" style="DISPLAY: none">
						<input type="hidden" value="医院代码|NOTNULL" name="SubReportGridverify3">
						</td>
						<td class="mulinetitle" style="DISPLAY: none">
						<input type="hidden" name="SubReportGridverify4"></td>
						<td class="mulinetitle" style="DISPLAY: none">
						<input type="hidden" name="SubReportGridverify5"></td>
						<td class="mulinetitle" style="DISPLAY: none">
						<input type="hidden" name="SubReportGridverify6"></td>
						<td class="mulinetitle" style="WIDTH: 154px">
						<input class="mulinetitle" onmouseover="_showtitle(this);" style="WIDTH: 154px" tabIndex="-1" readOnly value="备注" name="T4" size="20"><input type="hidden" name="SubReportGridverify7"> 
						</td>
						<td class="mulinetitle" style="DISPLAY: none">
						<input type="hidden" name="SubReportGridverify8"></td>
						<td class="mulinetitle" style="WIDTH: 77px">
						<input class="mulinetitle" onmouseover="_showtitle(this);" style="WIDTH: 77px" tabIndex="-1" readOnly value="事故类型" name="T5" size="20"><input type="hidden" name="SubReportGridverify9"> 
						</td>
						<td class="mulinetitle" style="DISPLAY: none" width="15">
						<input class="mulinetitle" style="WIDTH: 15px" readOnly type="hidden" value="-">
						</td>
					</tr>
				</table>
				<span id="spanSubReportGridField"></span>
				<div align="left">
					<input class="button" style="DISPLAY: none" disabled onclick=" SubReportGrid.addOne('SubReportGrid');SubReportGrid.moveFocus() ;" type="button" value="+" name="SubReportGridaddOne"></div>
				</span></td>
			</tr>
		</table>
	</div>
	<input id="fmtransact" type="hidden" name="fmtransact">
	<input class="cssButton" onclick="showInsuredLCPol()" type="button" value="保单查询">
	<input class="cssButton" onclick="showCustomerInfo()" type="button" value="客户资料查询"> 

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 
</body>
</html>
