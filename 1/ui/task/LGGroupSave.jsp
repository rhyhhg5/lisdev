<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LGGroupSave.jsp
//程序功能：
//创建日期：2005-02-22 17:32:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LGGroupSchema tLGGroupSchema   = new LGGroupSchema();
  OLGGroupUI tOLGGroupUI   = new OLGGroupUI();
  
  //填加信箱
  LGWorkBoxSchema tLGWorkBoxSchema   = new LGWorkBoxSchema();
  //LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
  OLGWorkBoxUI tOLGWorkBoxUI   = new OLGWorkBoxUI();
  
  
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  
  
  //生成小组编号
  String tSuperGroupNo = request.getParameter("SuperGroupNo");
  
  
  transact = request.getParameter("fmtransact");
  //生成信箱号
  String tWorkBoxNo;
  if (transact.equals("INSERT||MAIN"))
  {
  	tWorkBoxNo = PubFun1.CreateMaxNo("TASKBOX", 6);
  	System.out.println("tWorkBoxNo="+tWorkBoxNo);
  }
  else
  {
  	tWorkBoxNo = request.getParameter("GroupNo");	
  }
  
   
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  
  String tGroupNo;
  if (transact.equals("INSERT||MAIN"))
  {
  	tGroupNo = tSuperGroupNo + "_" + PubFun1.CreateMaxNo("TASKGROUP" + tSuperGroupNo, 6);
  	System.out.println("tGroupNo="+tGroupNo);
  }
  else
  {
  	tGroupNo = request.getParameter("GroupNo");
  }
  
	tLGGroupSchema.setGroupNo(tGroupNo);
	//之前不知道为什么要改这块，获取到的不是小组名称，肯定是改错了，现在改正确
	tLGGroupSchema.setGroupName(request.getParameter("GroupName"));
	tLGGroupSchema.setGroupInfo(request.getParameter("GroupInfo"));
	tLGGroupSchema.setSuperGroupNo(request.getParameter("SuperGroupNo"));
	tLGGroupSchema.setWorkTypeNo(request.getParameter("WorkTypeNo"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLGGroupSchema);
  	tVData.add(tG);
    tOLGGroupUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  { 
    tError = tOLGGroupUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	if (transact.equals("INSERT||MAIN"))
    	{
    		VData tRet = tOLGGroupUI.getResult();
  			tLGGroupSchema.setSchema((LGGroupSchema) tRet.getObjectByObjectName("LGGroupSchema", 0));
    		Content += "新增小组编号为" + tLGGroupSchema.getGroupNo(); 
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //----------------------------------------------------------------------------------
  if(FlagStr.equals("Success")){
      tLGWorkBoxSchema.setWorkBoxNo(tWorkBoxNo);
      tLGWorkBoxSchema.setOwnerTypeNo("1");
      tLGWorkBoxSchema.setOwnerNo(tGroupNo);
	    System.out.println("WorkBoxNo:"+tWorkBoxNo);
	    System.out.println("OwnerTypeNo:"+"1");
	    System.out.println("OwnerNo:"+tGroupNo);
	  
	  try
    {
      // 准备传输数据 VData
  	  //VData tVData = new VData();
  	    VData tVData1 = new VData();
	      tVData1.add(tLGWorkBoxSchema);
  	    tVData1.add(tG);
        tOLGWorkBoxUI.submitData(tVData1,transact);
    }
    catch(Exception ex)
    {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
    }
  }
  else
	{
	    Content = "保存失败，原因是: 该用户已有信箱，不能新建" ;
    	FlagStr = "Fail";
	}
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (!FlagStr.equals("Fail"))
  {
    tError = tOLGWorkBoxUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	if (transact.equals("INSERT||MAIN"))
    	{
    		VData tRet1 = tOLGWorkBoxUI.getResult();
  			tLGWorkBoxSchema.setSchema((LGWorkBoxSchema) tRet1.getObjectByObjectName("LGWorkBoxSchema", 0));
    		Content += "新增信箱编号为" + tLGWorkBoxSchema.getWorkBoxNo(); 
    	}
    }
    else                                                                           
    {
    	Content += " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
</script>
</html>
