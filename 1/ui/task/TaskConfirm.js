//程序名称：TaskConfirm.js
//程序功能：工单管理工单录入页面
//创建日期：2005-07-09
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;

function save()
{
  if(!checkData())
  {
    return false;
  }
  
	fm.fmtransact.value = "INSERT||MAIN";
	submitForm();
}

function checkData()
{
  var sql = "select StatusNo "
            + "from LGWork "
            + "where WorkNo = '" + fm.WorkNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != "" && rs[0][0] != "null")
  {
    if(rs[0][0] != "4")
    {
      alert("该工单不是待审批状态，不能审批");
      return false;
    }
  }
  
  return true;
}

function beforeSubmit()
{
	return true;
}

/* 提交表单 */
function submitForm()
{
	if (beforeSubmit())
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.submit(); //提交
	}
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		top.close();
  	    top.opener.location.reload();
  	    parent.opener.top.focus();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		top.close();
  	    top.opener.location.reload();
     	parent.opener.top.focus();
	}
}