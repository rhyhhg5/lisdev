<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-02-26 15:30:19
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="LGWorkTypeInput.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LGWorkTypeInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LGWorkTypeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGWorkType1);">
    		</td>
    		 <td class= titleImg>
        		 业务类型信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLGWorkType1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      业务分类编号
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=WorkTypeNo readonly >
    </TD>
    <TD  class= title>
      上级分类
    </TD>
    <TD  class= input>
      <Input class="codeNo" name="SuperTypeNo" verify="上级分类|notnull&code:TaskTopTypeNo" ondblclick="return showCodeList('TaskTopTypeNo',[this,SuperTypeNoName], [0,1]);" onkeyup="return showCodeListKey('TaskTopTypeNo',[this,SuperTypeNoName], [0,1]);" ><Input class="codeName" name="SuperTypeNoName"  readonly elementtype="nacessary" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      业务分类名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WorkTypeName elementtype="nacessary" verify="业务分类名称|NOTNULL&len<=100">
    </TD>
    <TD  class= title>
      完成时限
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DateLimit elementtype="nacessary" verify="完成时限|notnull&int&len<=3">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
