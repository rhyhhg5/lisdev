<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskDeliverSava.jsp
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-01-19 20:14:18
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	String tCopyFlag = request.getParameter("CopyFlag");
	String tDeliverType = request.getParameter("DeliverType");
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	
	
	String[] tWorkNo;
	String loadFlag = request.getParameter("loadFlag");
	System.out.println("loadFlag: " + loadFlag);
	if(loadFlag != null && loadFlag.equals("TaskViewTopButton"))
	{
	  tWorkNo = new String[1];
	  tWorkNo[0] = request.getParameter("WorkNo");
	  
	  if(tWorkNo[0] == null || tWorkNo[0].equals(""))
	  {
	    tWorkNo[0] = (String)session.getAttribute("VIEWWORKNO");
	  }
	}
  else
  {
	  tWorkNo = (String[]) session.getAttribute("WORKNO");
	  System.out.println("WORKNO=" + tWorkNo[0]);
	}
	
	System.out.println(tWorkNo == null ? null : tWorkNo[0]);
	
	//输入参数
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
	LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
	
	if (tCopyFlag.equals("Y"))
	{
		tLGWorkSchema.setTypeNo(request.getParameter("TypeNo"));
		if(tLGWorkSchema.getTypeNo().equals(""))
		{
		    tLGWorkSchema.setTypeNo(request.getParameter("TopTypeNo"));
		}
		tLGTraceNodeOpSchema.setOperatorType("2");
	}
	else
	{
		tLGTraceNodeOpSchema.setOperatorType("1");
	}
	System.out.println("TypeNo:" + request.getParameter("TypeNo"));
	tLGWorkTraceSchema.setWorkBoxNo(request.getParameter("WorkBoxNo"));
	tLGTraceNodeOpSchema.setRemark(request.getParameter("RemarkContent"));
	
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tWorkNo);
	tVData.add(tCopyFlag);
	tVData.add(tDeliverType);
	tVData.add(tLGWorkSchema);
	tVData.add(tLGWorkTraceSchema);
	tVData.add(tLGTraceNodeOpSchema);
	
	TaskDeliverBL tTaskDeliverBL = new TaskDeliverBL();
	if (tTaskDeliverBL.submitData(tVData, "ok") == false)
	{
		FlagStr = "Fail";
		Content = "工单转交失败，原因如下：\n" + tTaskDeliverBL.mErrors.getLastError();
	}
	else 
	{
	  FlagStr = "Succ";
		Content = "工单转交成功";
	}
	//设置显示信息
	VData tRet = tTaskDeliverBL.getResult();
	Content = PubFun.changForHTML(Content);
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

