
//打印函件
function printLetter()
{
	//即时打印
	if(fm.printMode[0].checked == true)
	{
		//将函件置为待回销
		fm.action = "TaskLetterOperateSave.jsp?loadFlag=SendOut"
			+ "&edoracceptNo=" + fm.edorAcceptNo.value 
			+ "&serialNumber=" + fm.serialNumber.value
			+ "&backFlag=" + fm.backFlag.value
			+ "&backDate=" + fm.backDate.value;
		submitForm();
	}
	//批量打印
	else if(fm.printMode.value == "1")
	{}
    else
    {
        alert("请选择打印方式。");
        return false;
    }
}

function printLater()
{
    fm.printType.value = "printSoon";
    
    //将函件置为待回销
	fm.action = "TaskLetterOperateSave.jsp?loadFlag=SendOut"
		+ "&edoracceptNo=" + fm.edorAcceptNo.value 
		+ "&serialNumber=" + fm.serialNumber.value
		+ "&backFlag=" + fm.backFlag.value
		+ "&backDate=" + fm.backDate.value;
	submitForm();
}

function goBack()
{
	top.close();
	top.opener.top.focus();
}

function submitForm()
{
	fm.submit();
}

//提交数据后的操作
function afterSubmit(FlagStr, content)
{
	window.focus();
	
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		top.close();
		top.opener.top.focus();
	}
	else if(fm.printType.value != "printSoon")
	{
		top.close();
		top.opener.top.location.reload();
		
		var win = window.open("TaskPreviewLetter.jsp?loadFlag=SHOWLETTER&showToolBar=true"
			+ "&edorAcceptNo=" + fm.edorAcceptNo.value 
			+ "&serialNumber=" + fm.serialNumber.value 
			+ "&letterType=" + fm.letterType.value
			+ "&addressNo=" + fm.addressNo.value
			+ "&backFlag=" + fm.backFlag.value
			+ "&backDate=" + fm.backDate.value);
		win.focus();
	}
    else
    {
		top.close();
		top.opener.top.location.reload();
    }
}