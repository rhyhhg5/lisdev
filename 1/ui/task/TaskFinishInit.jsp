<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskFinishInit.jsp
//程序功能：工单管理工单结案页面初始化
//创建日期：2005-01-19 18:14:12
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

//初始化表单
function initForm()
{
	strSql = "select WorkNo, AcceptNo, CustomerNo, ContNo, TypeNo, " +
			 "       DateLimit, ApplyTypeNo, PriorityNo, AcceptWayNo, " +
			 "       AcceptDate, AcceptWayNo, AcceptorNo, Remark " +
			 "from   LGWork " +
			 "where  WorkNo = '<%=request.getParameter("WorkNo")%>' ";
	arrResult = easyExecSql(strSql);
	
	if (!arrResult)
	{
		alert("没有找到符合条件的记录!");
		return false;
	}
	
	fm1.all("WorkNo").value = arrResult[0][0];
	fm1.all("AcceptNo").value = arrResult[0][1];
	fm1.all("CustomerNo").value = arrResult[0][2];
	fm1.all("ContNo").value = arrResult[0][3];
	fm1.all("TypeNo").value = arrResult[0][4];
	fm1.all("DateLimit").value = arrResult[0][5];
	fm1.all("ApplyTypeNo").value = arrResult[0][6];
	fm1.all("PriorityNo").value = arrResult[0][7];
	fm1.all("AcceptWayNo").value = arrResult[0][8];
	fm1.all("AcceptDate").value = arrResult[0][9];
	fm1.all("AcceptWayNo").value = arrResult[0][10];
	fm1.all("Acceptor").value = arrResult[0][11];
	fm1.all("Remark").value = arrResult[0][12];
	
	fm2.all("WorkNo").value = arrResult[0][0];
}
</script>