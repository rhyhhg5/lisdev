<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskPrintQueryBox.jsp
//程序功能：电话回访修改页面
//创建日期：2005-04-03
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="TaskPrintQueryBox.js"></SCRIPT> 
  <%@include file="TaskPrintQueryBoxInit.jsp"%>
  <title>信箱信息查询</title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLGWorkBoxGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      信箱编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WorkBoxNo >
    </TD>
    <TD  class= title>
      信箱类型
    </TD>
    <TD  class= input>
       <input class="code" name="OwnerTypeNo" ondblclick="return showCodeList('taskboxtype', [this]);" onkeyup="return showCodeListKey('taskboxtype', [this]);" verify="信箱类型|notnull&code:taskboxtype">
    </TD>
    <TD  class= title>
      拥有者
    </TD>
    <TD  class= input>
       <input class="code" name="OwnerNo" elementtype="nacessary" ondblclick="return showCodeList(mCodeName, [this]);" onkeyup="return showCodeListKey(mCodeName, [this]);" verify="拥有者|notnull" >
    </TD>
  </TR>
</table>
  </Div>
    
    <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQueryClick();"> 
    <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="returnParent();">   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGWorkBox1);">
  		</td>
  		<td class=titleImg>
  			 信箱信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLGWorkBox1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLGWorkBoxGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
