<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskUnite.jsp
//程序功能：合并作业
//创建日期：2005-04-14
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <script src="TaskUnite.js"></script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>作业合并</title>
</head>
<body onload="">
	<center>
	<form action="TaskUniteSave.jsp" name="fm" target=fraSubmit method=post>
		<table width="50%">
			<tr class=common width="80%>
				<td class=common colspan="2" width="100%">请选择合并后的工单号</td>
			</tr>
			<tr>
				<td class=common colspan="2" width="100%">
					<input type="radio" name="workNo" value="<%=request.getParameter("WorkNo1")%>"><%=request.getParameter("WorkNo1")%>					
				</td>
			</tr>
			<tr class=common>
				<td class=common colspan="2" width="100%">
					<input type="radio" name="workNo" value="<%=request.getParameter("WorkNo2")%>"><%=request.getParameter("WorkNo2")%>
				</td>
			</tr>			
			<!--<tr class=common>				
				<td class=common width="100%">备注
				</td>
				<td class=common>
					<textarea classs ="common" name="remark" cols="40" rows="3"></textarea>
				</td>
			</tr>-->
			<tr class=common>
				<td class=common width="100%">
					<input type=button class=cssbutton value="合  并" onclick="submitForm();">
					<!--<input type="button" class=cssButton name="Return" value="返  回" onclick="parent.parent.window.reload();">-->
				</td>
			</tr>
		</table>	
		<input type=hidden id="fmtransact" name="fmtransact" value="UPDATE||MAIN">	
		<input type=hidden id=hidden name="workNo1" value="<%=request.getParameter("WorkNo1")%>">	
		<input type=hidden id=hidden name="workNo2" value="<%=request.getParameter("WorkNo2")%>">
  </form>
	</center>
</body>
</html>
