<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PhoneAnswerViewInit.jsp
//程序功能：电话回访问卷初始化页面
//创建日期：2005-04-04
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>


<SCRIPT>
//初始化工单信息

var DetailWorkNo = "<%=request.getParameter("DetailWorkNo")%>";

function initForm()
{
	var strSql = "select * " +
			 		"from LGPhoneAnwser " +
			 		"where phoneWorkNo= '" + DetailWorkNo + "' " +
			 		"order by int(phoneItemNo) ";
	var arrResult = easyExecSql(strSql);
	
	if (!arrResult)
	{	
		alert("没有找到符合条件的记录!");
		return false;
	}
	
	
	var answer;
	var remark;
	var checkedFlag;
	
	for(var k = 0; k < arrResult.length; k++)
	{
		//设置第k个问题的答案
		answer = "answer";
		answer = answer + arrResult[k][1];
		checkedFlag = arrResult[k][3];
		if(checkedFlag == "0")
		{
			fm.all(answer).value = "是";			
		}
		else if(checkedFlag == "1")
		{
			fm.all(answer).value = "  否";
		}
	    else if(checkedFlag == "2")
	    {
	        //第三题，非一年期险种
	        if(k == 2)
	        {
	            fm.all(answer).value = "  非一年期险种";
	        }
	        //第五题，非一年期险种
	        if(k == 4)
	        {
	            fm.all(answer).value = "  未收到保单";
	        }
	        //第十题，非一年期险种
	        if(k == 9)
	        {
	            fm.all(answer).value = "  客户有预约 ";
	        }
	    }
		else if(k < (arrResult.length - 1))
		{
			fm.all(answer).value = "  此项未填";
		}
		
		
		//设置第k个问题的备注
		remark = "remark";
		remark = remark + arrResult[k][1];
		fm.all(remark).value = arrResult[k][4];
	}
	
	
	initLCPolInfo();
	setContInfoForInterview(arrResult[0][10]);
	
	return true;
}

function back()
{	
	top.close();
	top.opener.onfocus();
}

function doBusiness()
{
	window.location.href = "PhoneAnswerUpdate.jsp?DetailWorkNo=" + DetailWorkNo;
}


function initLCPolInfo()
{
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[0][21]="index";
      
      iArray[1]=new Array();
      iArray[1][0]="被保人";         	  //列名
      iArray[1][1]="30px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="insuredName";
      
      iArray[2]=new Array();
      iArray[2][0]="险种代码";         	  //列名
      iArray[2][1]="30px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="riskCode";
      
      iArray[3]=new Array();
      iArray[3][0]="险种名称";         	  //列名
      iArray[3][1]="120px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="riskName";
      
      iArray[4]=new Array();
      iArray[4][0]="期交保费";          //列名
      iArray[4][1]="30px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[4][21]="prem";
      
      iArray[5]=new Array();
      iArray[5][0]="缴费年期";         	//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[5][21]="payYears";
    
      LCPolGrid = new MulLineEnter("fm", "LCPolGrid"); 
      //设置Grid属性
      LCPolGrid.mulLineCount = 0;
      LCPolGrid.displayTitle = 1;
      LCPolGrid.locked = 1;
      LCPolGrid.canSel = 0;
      LCPolGrid.canChk = 0;
      LCPolGrid.hiddenSubtraction = 1;
      LCPolGrid.hiddenPlus = 1;
      LCPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}

</SCRIPT>