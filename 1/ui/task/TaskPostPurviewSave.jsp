<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPostPurviewSave.jsp
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-07-06 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput();
	
	//获得公共输入信息
	tG=(GlobalInput)session.getValue("GI");
	transact = request.getParameter("fmtransact");
	System.out.println("..transact: " + transact);

	LGProjectPurviewSchema tLGProjectPurviewSchema = new LGProjectPurviewSchema();
	LGProjectPurviewSet tLGProjectPurvieSet = new LGProjectPurviewSet();
	
	tLGProjectPurviewSchema.setProjectPurviewNo(request.getParameter("ProjectPurviewNo"));
	tLGProjectPurvieSet.add(tLGProjectPurviewSchema);
	
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGProjectPurvieSet);
	tVData.add(tG);
	
	//调用后台提交数据
	TaskPostPurviewUI tTaskPostPurviewUI = new TaskPostPurviewUI();
  	if (tTaskPostPurviewUI.submitData(tVData, transact) == false)
	{
		//失败
		FlagStr = "Fail";
		
		//需要处理后台错误
		if(tTaskPostPurviewUI.mErrors.needDealError())
		{
			System.out.println(tTaskPostPurviewUI.mErrors.getLastError());
			
			Content = "数据保存失败:"+tTaskPostPurviewUI.mErrors.getLastError();
		}
		else
		{
			Content = "数据保存失败!";
		}
		
		System.out.println("数据保存失败!"+FlagStr+Content);
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";			
		System.out.println("数据保存成功！");
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");	
</script>
</html>