<%
//程序名称：LGWorkBoxQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-23 09:59:56
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('WorkBoxNo').value = "";
    fm.all('OwnerTypeNo').value = "";
    fm.all('OwnerNo').value = "";
  }
  catch(ex) {
    alert("在LGWorkBoxQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}        
                            
function initForm() {
  try {
    initInpBox();
    initLGWorkBoxGrid();  
  }
  catch(re) {
    alert("LGWorkBoxQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//领取项信息列表的初始化
var LGWorkBoxGrid;
function initLGWorkBoxGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
	iArray[1][0]="信箱编号";         	  //列名
	iArray[1][1]="50px";            	//列宽
	iArray[1][2]=200;            			//列最大值
	iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
	iArray[2]=new Array();
	iArray[2][0]="信箱类型";         	
	iArray[2][1]="100px";            	
	iArray[2][2]=200;            		 
	iArray[2][3]=0;    
	iArray[2][4]="taskboxtype";
	iArray[2][5]="3";
	iArray[2][9]="信箱类型|code:taskboxtype&NOTNULL";
	iArray[2][18]=250;
	iArray[2][19]= 0 ;           		 
	
	iArray[3]=new Array();           
	iArray[3][0]="拥有者编码";         	//列名
	iArray[3][1]="100px";            	//列宽
	iArray[3][2]=200;                   //列最大值
	iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	iArray[4]=new Array();
	iArray[4][0]="拥有者";         	
	iArray[4][1]="80px";            	
	iArray[4][2]=200;            		 
	iArray[4][3]=2; 
	iArray[4][4]="ownerName";
	iArray[4][5]="3";
	iArray[4][9]="拥有者|code:ownerName&NOTNULL";
	iArray[4][18]=250;
	iArray[4][19]= 0 ;   
	
	LGWorkBoxGrid = new MulLineEnter("fm", "LGWorkBoxGrid"); 
	//设置Grid属性
	LGWorkBoxGrid.mulLineCount = 10;
	LGWorkBoxGrid.displayTitle = 1;
	LGWorkBoxGrid.locked = 1;
	LGWorkBoxGrid.canSel = 1;
	LGWorkBoxGrid.canChk = 0;
	LGWorkBoxGrid.hiddenSubtraction = 1;
	LGWorkBoxGrid.hiddenPlus = 1;
	LGWorkBoxGrid.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
