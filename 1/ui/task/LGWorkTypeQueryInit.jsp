<%
//程序名称：LGWorkTypeQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-26 15:30:19
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('WorkTypeNo').value = "";
    fm.all('WorkTypeName').value = "";
    fm.all('SuperTypeNo').value = "";
    fm.all('DateLimit').value = "";
  }
  catch(ex) {
    alert("在LGWorkTypeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLGWorkTypeGrid();  
  }
  catch(re) {
    alert("LGWorkTypeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LGWorkTypeGrid;
function initLGWorkTypeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
	iArray[1][0]="业务分类编号";         	  //列名
	iArray[1][1]="50px";            	//列宽
	iArray[1][2]=200;            			//列最大值
	iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
	iArray[2]=new Array();
	iArray[2][0]="业务分类名称";         	
	iArray[2][1]="100px";            	
	iArray[2][2]=200;            		 
	iArray[2][3]=0;              		 
	
	iArray[3]=new Array();
	iArray[3][0]="上级分类";         	//列名
	iArray[3][1]="100px";            	//列宽
	iArray[3][2]=200;                   //列最大值
	iArray[3][3]=2;              		//是否允许输入,1表示允许，0表示不允许
	iArray[3][4]="tasktoptypeno";
	iArray[3][5]="3";
	iArray[3][9]="上级分类|code:tasktoptypeno&NOTNULL";
	iArray[3][18]=250;
	iArray[3][19]= 0 ;
	
	iArray[4]=new Array();
	iArray[4][0]="完成时限";         	
	iArray[4][1]="100px";            	
	iArray[4][2]=200;            		 
	iArray[4][3]=0;    
	
	LGWorkTypeGrid = new MulLineEnter("fm", "LGWorkTypeGrid"); 
	//设置Grid属性
	LGWorkTypeGrid.mulLineCount = 10;
	LGWorkTypeGrid.displayTitle = 1;
	LGWorkTypeGrid.locked = 1;
	LGWorkTypeGrid.canSel = 1;
	LGWorkTypeGrid.canChk = 0;
	LGWorkTypeGrid.hiddenSubtraction = 1;
	LGWorkTypeGrid.hiddenPlus = 1;
	LGWorkTypeGrid.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
