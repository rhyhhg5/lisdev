//程序名称：TaskPersonalBox.js
//程序功能：工单管理个人信箱页面
//创建日期：2005-02-22 13:48:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

function easyQueryClick()
{
  var strSQL;
  var condition = " ";
	if (fm.OwnerTypeNo.value == "1")
	{
		if(fm.GroupNo.value != "")
		{
			condition = " and OwnerNo = '" + fm.GroupNo.value + "' ";
  	}
  	strSQL = "select w.WorkBoxNo, w.OwnerTypeNo, w.OwnerNo,w.OwnerNo  " +
  				 " from   LGWorkBox w " +
  				 "where 1 = 1 " +
  				 getWherePart('WorkBoxNo') +
  				 getWherePart('OwnerTypeNo') +
  				 condition + 
  				 "order by w.WorkBoxNo ";
	}
	else if (fm.OwnerTypeNo.value == "2")
	{
		if(fm.GroupNo.value != "")
		{
			condition = " and GroupNo = '" + fm.GroupNo.value + "' ";
  	}
  	strSQL = "select w.WorkBoxNo, w.OwnerTypeNo, w.OwnerNo, b.GroupNo " +
  				 " from   LGWorkBox w, LGGroupMember b " +
  				 "where w.OwnerNo = b.MemberNo " +
  				 getWherePart('WorkBoxNo') +
  				 getWherePart('OwnerTypeNo') +
  				 getWherePart('OwnerNo') +
  				 condition + 
  				 "order by w.WorkBoxNo ";
  }
  else if (fm.OwnerTypeNo.value == "0")
	{
   strSQL = "select w.WorkBoxNo, w.OwnerTypeNo, w.OwnerNo, '' " +
  				 " from   LGWorkBox w " +
  				 "where 1 = 1 " +
  				 getWherePart('WorkBoxNo') +
  				 getWherePart('OwnerTypeNo') +
  				 getWherePart('OwnerNo') +
  				 condition + 
  				 "order by w.WorkBoxNo ";	
  }
  else 
  {
	alert("信箱类型不能为空");
	return ;
}
//	document.all("show").innerHTML = 	strSQL;
	turnPage.queryModal(strSQL, BoxGrid);      
	showCodeName();	//将编码转换为字 
}

// 数据返回父窗口
function returnParent()
{
	var result;
	var tSel = BoxGrid.getSelNo();
	
	if(tSel == 0 || tSel == null)
	{
		alert("请先选择一条记录，再点击返回按钮。");
	}
	else
	{
		result = getQueryResult();
		top.opener.afterQuery(result);
		top.close();
	}
}

function getQueryResult()
{
	//获取正确的行号
	var tSel = BoxGrid.getSelNo();
	var tWorkBoxNo = BoxGrid.getRowColData(tSel - 1, 1);
	return tWorkBoxNo;
}

function lcReload()
{
window.location.reload();
}