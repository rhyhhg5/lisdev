<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskTeamBoxSava.jsp
//程序功能：工单管理小组信箱数据保存页面
//创建日期：2005-01-24 14:30:25
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	String tNum = request.getParameter("Num");
	
	//输入参数
	VData tVData = new VData();
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	tVData.add(tGI);
	tVData.add(tNum);
	TaskFatchBL tTaskFatchBL = new TaskFatchBL();
	if (tTaskFatchBL.submitData(tVData, "ok") == false)
	{
		FlagStr = "Fail";
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功";
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>

