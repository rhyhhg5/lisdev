<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：UpdateTaskSave.jsp
//程序功能：工单管理工单查看页面
//创建日期：2005-05-21
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%          
	String FlagStr = "";
	String Content = "";
	
	GlobalInput tG=(GlobalInput)session.getValue("GI");
	
	String typeNo = request.getParameter("TypeNo");
	System.out.println("1: "+ typeNo);
	if(typeNo == null || typeNo.equals(""))
	{
	  typeNo = request.getParameter("TopTypeNo");
	}
	
	String remark = request.getParameter("RemarkContent");
	if(remark == null || remark == "null")
	{
	  remark = "";
	}
	remark = "先扫描后受理工单信息录入" + remark;
	
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	tLGWorkSchema.setWorkNo(request.getParameter("WorkNoEdit"));
	tLGWorkSchema.setCustomerNo(request.getParameter("CustomerNo"));
	tLGWorkSchema.setTypeNo(typeNo);
	tLGWorkSchema.setDateLimit(request.getParameter("DateLimit"));
	tLGWorkSchema.setApplyTypeNo(request.getParameter("ApplyTypeNo"));
	tLGWorkSchema.setApplyName(request.getParameter("ApplyName"));
	tLGWorkSchema.setPriorityNo(request.getParameter("PriorityNo"));
	tLGWorkSchema.setAcceptWayNo(request.getParameter("AcceptWayNo"));
	tLGWorkSchema.setAcceptDate(request.getParameter("AcceptDate"));
	tLGWorkSchema.setAcceptCom(request.getParameter("AcceptCom"));
	tLGWorkSchema.setAcceptorNo(request.getParameter("AcceptorNo"));
	tLGWorkSchema.setRemark(request.getParameter("Remark"));
	
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGWorkSchema);
	tVData.add(tG);
	  	
	TaskUpdateBL tTaskUpdateBL = new TaskUpdateBL();
	if (tTaskUpdateBL.submitData(tVData, "UPDATE||MAIN") == false)
	{  
		FlagStr = "Fail";
		Content = tTaskUpdateBL.mErrors.getErrContent();
	} 
	else                                               
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}  
	
	Content = PubFun.changForHTML(Content);

  //添加各种预处理
%>
<html>
<script language="javascript">
	var frm = "<%=request.getParameter("fmtransact")%>";

	if(frm=="UPDATE||MAIN")
	{
		parent.fraInterface.afterEditSubmit("<%=FlagStr%>","<%=Content%>");
	}
</script>
</html>