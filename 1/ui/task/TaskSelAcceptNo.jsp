<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskSelAcceptNo.jsp
//程序功能：作业合并选择受理号页面
//创建日期：2005-03-15
//创建人  ：邱杨
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="TaskSelAcceptNo.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<body>
  <form active="TaskUniteSave.jsp" name="fm" >
  <br><br>
  <table align="center">
	<tr>
	  <td class="common">
	    <p>请选择合并后的受理号:</p>
	  </td>
	</tr>
	<tr>
	  	<td class="common">
	  	<SCRIPT>
			initSelAcceptNo();
		</SCRIPT>
	  	</td>
	 </tr>
	 <tr>
	 	<td class="common">
	 		<br>
	 		<INPUT VALUE="确  定" TYPE=button Class="cssButton" name="ok" onclick="submitForm();">&nbsp;
      		<INPUT VALUE="取  消" TYPE=button Class="cssButton" name="cancel" onclick="cancelUnite();">
	 	<tr>
	 </tr>
   </table>
   </form>
</body>
</html>