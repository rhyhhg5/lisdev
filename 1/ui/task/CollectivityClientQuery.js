//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initCollectivityGrid();
	
	var grpName = fm.GrpName.value;
	var grpNature = fm.GrpNature.value;
	var SqlCondition = ""
	
	if(grpName != null && grpName != "")
	{
		SqlCondition = " and a.GrpName='" + grpName + "' "
	}
	if(grpNature != null && grpNature != "")
	{
		SqlCondition = SqlCondition + " and a.GrpNature='" + grpNature + "' ";
	}
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select distinct a.customerNo, a.GrpName, a.GrpNature from LDGrp a, LCGrpCont b "
	         +" where 1=1 and a.customerNo=b.appntNo "
	       + "and AppFlag = '1' "
				 + getWherePart( 'customerNo','GrpNo' )
				 + SqlCondition
				 + "order by customerNo ";
//alert(strSQL);
	turnPage.queryModal(strSQL, CollectivityGrid);
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initCollectivityGrid();
		CollectivityGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		CollectivityGrid.loadMulLine(CollectivityGrid.arraySave);
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				CollectivityGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//CollectivityGrid.delBlankLine();
	} // end of if
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = CollectivityGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
			top.close();
		}
		catch(ex)
		{
			//alert( "请先选择一条非空记录，再点击返回按钮。");
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	
	tRow = CollectivityGrid.getSelNo();
	if( tRow == 0 || tRow == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = CollectivityGrid.getRowData(tRow-1);
	
	return arrSelected;
	alert(arrSelected);
}

