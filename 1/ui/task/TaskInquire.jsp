<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：TaskInquire.jsp
//程序功能：工单管理客户咨询
//创建日期：2005-03-28
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="TaskInquire.js"></SCRIPT>
  <%@include file="TaskInquireInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>客户咨询 </title>
</head>
<body  onload="initForm();">
  <form action="TaskInquireSave.jsp" method=post name=fm target="fraSubmit">
	<table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divInquire);">
	      </td>
          <td class= titleImg>客户咨询&nbsp;</td>
	  </tr>
    </table>
	<Div id="divInquire" style="display: ''">
    <table class=common>
      <tr class=common>
      	<TD class= title> 机构 </TD>
        <TD class= input>
	    	<Input class="code" name="ManageCom" ondblclick="return showCodeList('station', [this]);" onkeyup="return showCodeListKey('station', [this]);" verify="机构|&code:station">
	    </TD>
        <TD class= title> 姓名 </TD>
        <TD class= input>
	    	<Input class="common" name="Name" elementtype="nacessary" verify="姓名|notnull">
	    </TD>
	    <TD class= title> 性别 </TD>
        <TD class= input>
	    	<Input class="code" name="Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" verify="客户性别|&code:Sex">
	    </TD>
	  </tr>
      <tr class=common>
        <TD class= title> 电子邮件 </TD>
        <TD class= input>
        	<input class="common" name="Email">
        </TD>
        <TD class= title> 联系电话 </TD>
        <TD class= input>
        	<input class="common" name="Phone">
        </TD>
        <TD class= title> 主题项目 </TD>
        <TD class= input>
        	<input class="common" name="Title">
        </TD>
      </tr>
      <tr class=common>
        <TD class= title> 详细内容 </TD>
        <TD class= input colspan="7">
        	<textarea class="common" name="Content" cols="92%" rows="5"></textarea> 
        </TD>
      </tr>
   </table>
   </Div>
 </form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

