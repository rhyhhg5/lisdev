<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskInput.jsp
//程序功能：工单管理工单录入页面
//创建日期：2005-01-17 11:39:42
//创建人  ：QiuYang
//更新记录：  更新人 qulq   更新日期  2008-1-4 13:44   更新原因/内容 保全回退
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="TaskInput.js"></SCRIPT>
  <%@include file="TaskInputInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单录入 </title>
</head>
<body  onload="initForm();">
  <form action="./TaskInputSave.jsp" method=post name=fm target="fraSubmit">
  <!--
    <span style="display: ''; position:relative; float:left; top:1px; slategray">
    <input type="button" class=cssButton name="do" value="经  办" onclick="doBusiness();">&nbsp;
    <input type="button" class=cssButton name="save" value="保  存" onclick="saveTask();">&nbsp;
    <input type="button" class=cssButton name="deliver" value="转  交" onclick="deliverTask();">&nbsp;
    <input type="button" class=cssButton name="wait" value="转为待办" onclick="toWait();">&nbsp;
    <input type="button" class=cssButton name="input" value="简易保全" onclick="submitForm();">&nbsp;
    <input type="button" class=cssButton name="finish" value="结  案" onclick="finishTask();">&nbsp;
    <input type="button" class=cssButton name="return" value="返  回" onclick="javascript:location.href='TaskPersonalBox.jsp'">
    </span>
    -->
    <!-- 
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskInfo);">
	      </td>
        <td class= titleImg>工单信息&nbsp;</td>
	    </tr>
    </table>
    -->
  	<Div id="divTaskInfo" style="display: ''"> 
    <table class=common>
      <tr class=common>
        <TD class= title> 客户号码 </TD>
        <TD class= input>
	    	<Input class=common name="CustomerNo" onkeydown="queryByCustomerNo();" style="width:100">
	    	<input type="button" Class="cssButton" name="search" value="个人" onclick="queryCustomerNo();" >
	    	<input type="button" Class="cssButton" name="searchGrp" value="团体" onclick="queryGrpCustomerNo();" > 
	    </TD>
	    <!--
	    <TD class= title> 保单号码 </TD>
        <TD class= input>
	    	<Input class=common name="ContNo" onkeydown="queryByContNo();">
	    	<input type="button" Class="cssButton" name="search" value="查  询" onclick="queryContNo();" > 
	    </TD>
	    -->
	    <TD class= title> 证件号码 </TD>
        <TD class= input>
	    	<Input class=common name="IDNo" onkeydown="queryByIDNo();">
	    </TD>
        <TD class= title> 优先级别 </TD>
        <TD class= input>
        	<input class="code" name="PriorityNo" type="hidden">
        	<input class="code" name="PriorityNoName" readonly ondblclick="return showCodeList('PriorityNo', [this,PriorityNo], [1,0]);" onkeyup="return showCodeListKey('PriorityNo', [this,PriorityNo], [1,0]);" verify="优先级别|&code:PriorityNo">
        </TD>
      
      </tr>
      <tr>
        <TD class= title> 业务类型 </TD>
        <TD class= input>
        	<input class="code" name="TopTypeNo" type="hidden">
        	<input class="code" name="TopTypeNoname" readonly elementtype="nacessary" ondblclick="return showCodeList('TaskTopTypeNo', [this,TopTypeNo], [1,0]);" onkeyup="return showCodeListKey('TaskTopTypeNo', [this,TopTypeNo],[1,0]);" verify="业务类型|notnull&code:TaskTopTypeNo">
        </TD> 
        <TD class= title> 子业务类型 </TD>
        <TD class= input>
        	<input class="code" name="TypeNo" type="hidden">
        	<input class="code" name="TypeNoname" readonly ondblclick="return showCodeList('TaskTypeNo', [this,TypeNo], [1,0], null, subTypeNoCondition, 'SuperTypeNo', 1);" onkeyup="return showCodeListKey('TaskTypeNo', [this,TypeNo], [1,0], null, subTypeNoCondition, 'SuperTypeNo', 1);">
        </TD> 
        <TD class= title> 时限 </TD>
        <TD class= input>
        	<input class="common" name="DateLimit">
        </TD>
      </tr>
      <tr>
      	<TD class= title> 受理途径 </TD>
        <TD class= input>
        	<input class="code" name="AcceptWayNo" type="hidden">
        	<input class="code" name="AcceptWayNoname" readonly ondblclick="return showCodeList('AcceptWayNo', [this,AcceptWayNo], [1,0]);" onkeyup="return showCodeListKey('AcceptWayNo', [this,AcceptWayNo],[1,0]);" verify="受理途径|&code:AcceptWayNo">
        </TD>
        <TD class= title> 申请人类型 </TD>
        <TD class= input>
        	<input class="code" name="ApplyTypeNo" type="hidden">
        	<input class="code" name="ApplyTypeNoName" readonly  elementtype=nacessary ondblclick="return showCodeList('ApplyTypeNo', [this,ApplyTypeNo],[1,0]);" onkeyup="return showCodeListKey('ApplyTypeNo', [this,ApplyTypeNo], [1,0]);" verify="申请人类型|NOTNULL"  >
        </TD>
        <TD class= title> 申请人姓名 </TD>
        <TD class= input>
        	<input class="common" name="ApplyName" style="width:100">
        	<input VALUE="业务员查询" TYPE=button Class="cssButton" name="searchAgent" onclick="queryAngent();">
         </TD> 
      </tr>
       <tr>
      	<TD class= title> 是否代理 </TD>
        <TD class= input>
        	<input type="checkbox" id="isProxy" name="Proxy" lementtype=nacessary value="是否代理" onclick="showProxyInfo();" >
        	<input type="hidden" name ="AgentFlag" value="1" />
        </TD>
      </tr>
      <tr style="display:none;">
        <TD class= title>受理日期 </TD>
        <TD class= input colspan="7">
        	<Input class="coolDatePicker" name="AcceptDate"  dateFormat="short"  verify="受理日期|date">
        </TD>    
      </tr>
      <tr>
        <TD  class= title> 备注</TD>
        <TD colspan="7"  class= input>
        	<textarea class="common" name="Remark" cols="111%" rows="2"></textarea> 
        </TD>
      </tr>
    </table>
    <table class=common id="ProxyInfo" style="display: 'none'" >
    <tr >
      	<TD class= title> 代理人姓名 </TD>
        <TD class= input>
        	<input class="common" name="ProxyName" elementtype="nacessary" >
        </TD>
        <TD class= title> 有效证件类型 </TD>
        <TD class= input>
        	<Input class= codeNo name="ProxyIDType" ondblclick="return showCodeList('IDType',[this,IDTypeName], [0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName], [0,1]);"><Input class= codeName name="IDTypeName" elementtype=nacessary readonly >
        </TD>
        <TD class= title> 证件号码 </TD>
        <TD class= input>
        	<Input class= common name="ProxyIDNo" elementtype="nacessary">
         </TD> 
      </tr>
      <tr style="display:" >
      	<TD class= title> 证件生效日期 </TD>
        <TD class= input>
        	<Input class="coolDatePicker" name="ProxyIDStartDate"  elementtype="nacessary">
        </TD>
        <TD class= title> 证件失效日期 </TD>
        <TD class= input>
        	<Input class="coolDatePicker" name="ProxyIDEndDate" elementtype="nacessary">
        </TD>
        <TD class= title> 联系电话 </TD>
        <TD class= input>
        	<Input class= common name="ProxyPhone" elementtype="nacessary">
         </TD> 
      </tr>
     </table>
    <table width="100%">
      <tr >
        <td  colspan="6" align="right">
          <input type="button" class="cssButton" style="display:none" name="saveButton" value="保  存" onclick="saveTaskAfterEdit();">
        </td>
      </tr>
    </table>
 	</div>
  <table id="CustomerTable" style="display: ''">
    <tr>
      <td  class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCustomerInfo)">
      </td>
    <td class= titleImg>客户基本信息&nbsp;</td>
    </tr>
  </table>
  <Div id="divCustomerInfo" style="display: ''"> 
  <table  class= common>
    <TR  class= common>           
	    <TD  class= title>
	      姓名
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=Name readonly >
	    </TD>
	    <TD  class= title>
	      性别
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=Sex readOnly >
	    </TD>
	    <TD  class= title>
	      出生日期
	    </TD>
	    <TD  class= input>
	      <Input class="readonly"  name=Birthday readonly >
	    </TD>
	 </TR>
     <TR class= common>
	    <TD  class= title>
	      职业
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=Occupation readonly >
	    </TD>
	    <TD  class= title>
	      证件类型
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=IDType readOnly >
	    </TD>
	    <TD  class= title>
	      证件号码
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=IDNo2 readonly >
	    </TD>
	 </TR>
	 <TR  class= common>
		<TD  class= title>
	      家庭地址
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=HomeAddress readonly >
	    </TD>
	    <TD  class= title>
	      邮编
	    </TD>
   		<TD  class= input>
	      <Input class="readonly" name=ZipCode readonly >
	    </TD>
	    <TD  class= title>
	      通讯地址
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=PostalAddress readonly >
	    </TD>
	 </TR>
	 <TR>
	    <TD  class= title>
	      住宅电话
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=HomePhone readonly >
	    </TD>
	    <TD  class= title>
	      单位编码
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=GrpNo readonly >
	    </TD>
	    <TD  class= title>
	      单位地址
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=CompanyAddress readonly >
	    </TD>
	  </TR>
	  <TR>
	    <TD  class= title>
	      单位电话
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=CompanyPhone readonly >
	    </TD>
	    <TD  class= title>
	      e_mail
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=EMail readonly >
	    </TD>
	    <TD  class= title>
	      移动电话
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=Mobile readonly >
	    </TD>
	   </TR>
	  <TR style="display:none;">
	   	<TD  class= title>
	      婚姻状况
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=Marriage ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
	    </TD>
	    <TD  class= title>
	      职业代码
	    </TD>
	    <TD  class= input>
		<Input class="readonly" name=OccupationCode ondblclick="return showCodeList('occupationcode',[this]);" onkeyup="return showCodeListKey('occupationcode',[this]);">
	    </TD>
	    <TD  class= title>
	      户籍所在地
	    </TD>
	    <TD  class= input>
	      <Input class="readonly" name=RgtAddress ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);" >
	    </TD>
	   </TR>
    </table>
  </Div>
  <%@ include file="GroupInfo.jsp"%>
  <table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskList);"> 
      </td>
      <td class= titleImg>客户服务信息 </td>
      <td class=common>
        <INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewTask();"> 
      </td>
    </tr>
  </table>
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanCustomerGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
	<Div id= "divPage" align=center style= "display: 'none' ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage(); showCodeName();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage(); showCodeName();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage(); showCodeName();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage(); showCodeName();"> 
	</Div>
  </Div>
  <!--个单信息-->
  <table id="ContTable" style="display: ''">
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContList);"> 
      </td>
      <td class= titleImg>客户保单信息 </td>
      <td>
        <INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewCont();"> 
        <INPUT VALUE="退保试算" TYPE=button Class="cssButton" name="pBudgetButton" onclick="pEdorCalZTTest();">
      </td>
    </tr>
  </table>
  <Div  id= "divContList" style= "display: ''" align=center>  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanContGrid" >
				</span> 
		  	</td>
			</tr>
		</table>
		<Div id= "divPage2" align=center style= "display: 'none' ">
		  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage(); showCodeName();"> 
		  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); showCodeName();"> 					
		  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); showCodeName();"> 
		  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); showCodeName();"> 
		</Div>
  </Div>
  
  <!--团单信息-->
  <table id="GrpContTable" style="display: none">
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpContList);"> 
      </td>
      <td class= titleImg>客户保单信息 </td>
      <td>
        <INPUT VALUE="查  看" TYPE=button Class="cssButton" name="viewGrp" onclick="viewGrpCont();"> 
        <INPUT VALUE="退保试算" TYPE=button Class="cssButton" name="gBudgetButton" onclick="gEdorCalZTTest();">
      </td>
    </tr>
  </table>
  <Div  id= "divGrpContList" style= "display: none" align=center>  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanGrpContGrid" >
				</span> 
		  	</td>
			</tr>
		</table>
		<Div id= "divPage3" align=center style= "display: 'none' ">
		  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage(); showCodeName();"> 
		  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage(); showCodeName();"> 					
		  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage(); showCodeName();"> 
		  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage(); showCodeName();"> 
		</Div>
  </Div>
  
  <input class="common" name="ApplyNameHidden" type="hidden">
  <input class="common" name="WorkNoEdit" type="hidden">
  <input class="common" name="ScanBeforeInput" type="hidden">
  <input class="common" name="AcceptCom" type="hidden">
  <input class="common" name="AcceptorNo" type="hidden">
  <input class="common" name="fmtransact" type="hidden">
  </form>
  <table>
    <td> 
        <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divDetailTaskInfo);"> 
    </td>
	<td>
		<td class="titleImg">业务信息录入&nbsp;</td>
	</td>
  </table>
  <Div  id= "divDetailTaskInfo" style= "display: ''" align=center>  
  <iframe width="100%" height="0" frameborder="0" id="fraInterface" name="fraInterface" scrolling="auto" src=""></iframe>
  <iframe width="0" height="0" frameborder="0" name="VD" src="../common/cvar/CVarData.html"></iframe>
  <iframe width="0" height="0" frameborder="0" name="EX" src="../common/cvar/CExec.jsp"></iframe>
  </Div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
