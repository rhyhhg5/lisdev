 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskPurviewConfirmSave.jsp
//程序功能：快速新建岗位级别确认权保存数据页面
//创建日期：2005-07-06 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%             
	String FlagStr = "";
	String Content = "";
	
	String transact = request.getParameter("fmtransact");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	LGProjectPurviewSchema tLGProjectPurviewSchema = null;
	LGProjectPurviewSet tLGProjectPurviewSet = new LGProjectPurviewSet();
	
	try
	{    	
		for(int i = 1; i <= 9; i++)
		{
			tLGProjectPurviewSchema = new LGProjectPurviewSchema();
			//获取输入信息
			tLGProjectPurviewSchema.setProjectNo(request.getParameter("ProjectNo"));
			tLGProjectPurviewSchema.setProjectType(request.getParameter("ProjectType"));
			tLGProjectPurviewSchema.setRemark(request.getParameter("Remark"));
    		tLGProjectPurviewSchema.setNeedOtherAudit("0");
    	
			String confirmFlag = request.getParameter("ConfirFlag0" + i);
			tLGProjectPurviewSchema.setConfirmFlag((confirmFlag == null) ? "0" : "1");
			tLGProjectPurviewSchema.setPostNo("PA0" + i);
			tLGProjectPurviewSchema.setMoneyPurview("0");
			tLGProjectPurviewSchema.setExcessPurview("0");
			tLGProjectPurviewSchema.setTimePurview("0");
			tLGProjectPurviewSchema.setMemberNo(request.getParameter("00000000000000000000"));
			
			tLGProjectPurviewSet.add(tLGProjectPurviewSchema);
		}
		
		// 准备传输数据 VData
		VData tVData = new VData();  	
		tVData.add(tLGProjectPurviewSet);
		tVData.add(tG);
		
	  	//调用后台提交数据
		TaskPostPurviewConfirmUI tTaskPostPurviewConfirmUI = new TaskPostPurviewConfirmUI();
  		if (tTaskPostPurviewConfirmUI.submitData(tVData, transact) == false)
		{
			//失败
			FlagStr = "Fail";
			
			//需要处理后台错误
			if(tTaskPostPurviewConfirmUI.mErrors.needDealError())
			{
				System.out.println(tTaskPostPurviewConfirmUI.mErrors.getLastError());
				
				Content = tTaskPostPurviewConfirmUI.mErrors.getLastError();
			}
			else
			{
				Content = "数据保存失败!";
			}
		}
		else
		{
			FlagStr = "Succ";
			Content = "数据保存成功！";			
		}
		
	}
	catch(Exception e)
	{
		FlagStr = "Fail";
		Content = "获取输入数据失败";
		System.out.println("TaskPurviewInputSave.获取输入数据失败");
		e.printStackTrace();
	}
	Content = PubFun.changForHTML(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>