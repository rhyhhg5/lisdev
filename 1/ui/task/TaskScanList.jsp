<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskScanList.jsp
//程序功能：工单管理扫描列表页面
//创建日期：2005-04-10
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src=""></SCRIPT>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <%@include file="TaskScanListInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单查看</title>
</head>
<body onload="initForm();">
	<form action="" name="fm" target=fraSubmit method=post>
  <!--  <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divScanlist);">
	      </td>
        <td class= titleImg>扫描件 </td>
        <td class=common>(共X件)</td>
	    </tr>
    </table>	
    -->
     <Div  id= "divScanlist" style= "display: ''"> 
	  	<table class= common>
	   		<tr class= common>
	  	  		<td text-align: left colSpan=1>
					<span id="spanScanGrid" >
					</span> 
			  	</td>
			</tr>
	    </table>
	  <Div id= "divPage" align="center" style= "display: 'none' ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 
	</Div>
	</div>
	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>