var turnPage = new turnPageClass();

function easyQueryClick()
{
	// 初始化表格
	initGrid();   
	var strSQL = "select MemberNo, GroupNo, PostNo, ComCode, MemberNo "
				+ "from LGGroupMember a, LDUser b "
				+ "where a.MemberNo=b.UserCode "
				+ getWherePart("MemberNo")
				+ getWherePart("GroupNo")
				+ getWherePart("PostNo")
				+ getWherePart("ComCode");
				//+ "		and MemberNo='" + fm.MemberNo.value + "' "

	turnPage.queryModal(strSQL, MemberGrid);       
	
	showCodeName();	//将编码转换为字 
}

function clearData()
{
    initForm();
    
    fm.MemberNo.value = "";
    fm.MemberNoName.value = "";
    fm.GroupNo.value = "";
    fm.GroupNoName.value = "";
    fm.PostNo.value = "";
    fm.PostNoName.value = "";
    fm.ComCode.value = "";
    fm.ComCodeName.value = "";
}

// 数据返回父窗口
function returnParent()
{
	if(MemberGrid.mulLineCount == 0)
	{
	    alert("请先查询并选择一条记录后返回。");
	    return false;
	}
	var tSel = MemberGrid.getSelNo();
	
	if(tSel == 0 || tSel == null)
	{
		alert("请先选择一条记录，再点击返回按钮。");
	}
	else
	{
		var result = MemberGrid.getRowColData(tSel - 1, 5);
		
		top.opener.afterQuery(result);
		top.close();
	}
}