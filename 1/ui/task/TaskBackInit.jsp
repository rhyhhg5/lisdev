<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskBackInit.jsp
//程序功能：工单管理工单收回页面初始化
//创建日期：2005-01-18 18:33:36
//创建人  ：qiuyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>              

<script language="JavaScript">
//表单初始化
function initForm()
{
  try
  {
    initInpBox();
    initGrid();
  }
  catch(re)
  {
    alert("TaskPersonalBoxInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 输入框的初始化（单记录部分）
function initInpBox()
{
}

// 保单信息列表的初始化
function initGrid()
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="作业号";         	  //列名
      iArray[1][1]="80px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="受理号";         	  //列名
      iArray[2][1]="110px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="优先级";          //列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[3][4]="PriorityNo";
      iArray[3][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[3][9]="优先级|code:PriorityNo&NOTNULL";
      iArray[3][18]=250;
      iArray[3][19]= 0 ;
      
      iArray[4]=new Array();
      iArray[4][0]="业务类型";         	//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[4][4]="TaskTypeNo";
      iArray[4][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[4][9]="业务类型|code:TaskTypeNo&NOTNULL";
      iArray[4][18]=250;
      iArray[4][19]= 0 ;
  
      iArray[5]=new Array();
      iArray[5][0]="客户号";         	//列名
      iArray[5][1]="80px";            	//列宽
      iArray[5][2]=200;            		  //列最大值
      iArray[5][3]=0;              		  //是否允许输入,1表示允许，0表示不允许 
      
      iArray[6]=new Array();
      iArray[6][0]="姓名";         	//列名
      iArray[6][1]="80px";            	//列宽
      iArray[6][2]=200;            		  //列最大值
      iArray[6][3]=3;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="记录";         	//列名
      iArray[7][1]="80px";            	//列宽
      iArray[7][2]=200;            		  //列最大值
      iArray[7][3]=3;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="受理人";         	//列名
      iArray[8][1]="60px";            	//列宽
      iArray[8][2]=200;            		  //列最大值
      iArray[8][3]=2;              		  //是否允许输入,1表示允许，0表示不允许
      iArray[8][4]="TaskMemberNo";
	  iArray[8][5]="3";              	                //引用代码对应第几列，'|'为分割符
	  iArray[8][9]="受理人|code:TaskMemberNo&NOTNULL";
	  iArray[8][18]=250;
	  iArray[8][19]= 0 ;           
      
      iArray[9]=new Array();
      iArray[9][0]="受理人机构";         		  //列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][4]="AcceptCom";
      iArray[9][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[9][9]="受理机构|code:AcceptCom";
      iArray[9][18]=250;
      iArray[9][19]= 0 ;
      
      iArray[10]=new Array();
      iArray[10][0]="受理日期";              //列名
      iArray[10][1]="100px";            	//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
  	  iArray[11]=new Array();
      iArray[11][0]="时间限制";         		  //列名
      iArray[11][1]="60px";            	  //列宽
      iArray[11][2]=200;            		  //列最大值
      iArray[11][3]=0;              		  //是否允许输入,1表示允许，0表示不允许  
      
      iArray[12]=new Array();
      iArray[12][0]="处理状态";         		  //列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[12][4]="TaskStatusNo";
      iArray[12][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[12][9]="处理状态|code:TaskStatusNo&NOTNULL";
      iArray[12][18]=250;
      iArray[12][19]= 0 ;
      
      iArray[13]=new Array();
      iArray[13][0]="批注";         	  //列名
      iArray[13][1]="40px";            	//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="扫描";         	  //列名
      iArray[14][1]="40px";            	//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    

      TaskGrid = new MulLineEnter("fm", "TaskGrid"); 
      //设置Grid属性
      TaskGrid.mulLineCount = 10;
      TaskGrid.displayTitle = 1;
      TaskGrid.locked = 1;
      TaskGrid.canSel = 0;
      TaskGrid.canChk = 1;
      TaskGrid.hiddenSubtraction = 1;
      TaskGrid.hiddenPlus = 1;
      TaskGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}

</script>