<%
    //程序名：CreatNextFeeHastenSvae.jsp
    //程序功能：生成续期催缴任务
    //创建日期：2005-11-11
    //创建人  ：Yang Yalin
    //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    String flag = null;
    String content = null;
    
    NextFeeHastenTask task = new NextFeeHastenTask();
    task.oneCompany((GlobalInput)session.getValue("GI"));
    if(task.mErrors.needDealError())
    {
        flag = "Fail";
        content = PubFun.changForHTML(task.mErrors.getErrContent());
    }
    else
    {
        flag = "Succ";
        content = "续期催缴成功";
    }
%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit1("<%=flag%>", "<%=content%>");
</script>
</html>