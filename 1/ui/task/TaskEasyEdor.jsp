<html> 
<% 
//程序名称：TaskEasyEdor.jsp
//程序功能：简易保全
//创建日期：2005-04-06
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./TaskEasyEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="TaskEasyEdorInit.jsp"%>
  <title>简易保全 </title> 
</head>
<body  onload="initForm();" >
  	<form action="./TaskEasyEdorSave.jsp" method=post name=fm target="fraSubmit"> 
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskInfo);">
	      </td>
        <td class= titleImg>工单信息&nbsp;</td>
	    </tr>
    </table>
  	<Div id="divTaskInfo" style="display: ''"> 
    <table class=common>
      <tr class=common>
        <TD class= title> 客户号码 </TD>
        <TD class= input>
	    	<Input class=common name="CustomerNo" onkeydown="queryByCustomerNo();" style="width:120">
	    	<input type="button" Class="cssButton" name="search" value="查  询" onclick="window.open('./LDPersonQueryMain.jsp');" >
	    </TD>
	    <TD class= title> 证件号码 </TD>
        <TD class= input>
	    	<Input class=common name="IDNo" onkeydown="queryByIDNo();">
	    </TD>
        <TD class= title> 优先级别 </TD>
        <TD class= input>
        	<Input class="codeNo" name="PriorityNo" ondblclick="return showCodeList('PriorityNo',[this,PriorityNoName],[0,1]);" onkeyup="return showCodeListKey('PriorityNo',[this,PriorityNoName],[0,1]);"><Input name="PriorityNoName" class="codeName" readonly >
        </TD>
      </tr>
      <tr>
        <TD class= title> 业务类型 </TD>
        <TD class= input>
        	<input class="readonly" name="TopTypeNo" readonly>
        </TD> 
        <TD class= title> 子业务类型 </TD>
        <TD class= input>
        	<input class="readonly" name="TypeNo" readonly ondblclick="return showCodeList('TaskTypeNo', [this], null, null, fm.all('TopTypeNo').value, 'SuperTypeNo', 1);" onkeyup="return showCodeListKey('TaskTypeNo', [this], null, null, fm.all('TopTypeNo').value, 'SuperTypeNo', 1);" verify="子业务类型|code:TaskTypeNo">
        </TD> 
        <TD class= title> 时限 </TD>
        <TD class= input>
        	<input class="common" name="DateLimit">
        </TD>
      </tr>
      <tr>
        <TD class= title> 申请人类型 </TD>
        <TD class= input>
        	<Input class="codeNo" name="ApplyTypeNo" ondblclick="return showCodeList('ApplyTypeNo',[this,ApplyTypeNoName],[0,1]);" onkeyup="return showCodeListKey('ApplyTypeNo',[this,PriorityNoName],[0,1]);"><Input name="ApplyTypeNoName" class="codeName" readonly >
        </TD>
        <TD class= title> 申请人姓名 </TD>
        <TD class= input>
        	<input class="common" name="ApplyName">
         </TD> 
        <TD class= title> 受理途径 </TD>
        <TD class= input>
        	<Input class="codeNo" name="AcceptWayNo" ondblclick="return showCodeList('AcceptWayNo',[this,AcceptWayNoName],[0,1]);" onkeyup="return showCodeListKey('AcceptWayNo',[this,AcceptWayNoName],[0,1]);"><Input name="AcceptWayNoName" class="codeName" readonly >
        </TD>
      </tr>
      <tr style="display:none;">
        <TD class= title>受理日期 </TD>
        <TD class= input colspan="7">
        	<Input class="coolDatePicker" name="AcceptDate"  dateFormat="short"  verify="受理日期|date">
        </TD>    
      </tr>
      <tr>
        <TD  class= title> 备注</TD>
        <TD colspan="7"  class= input>
        	<textarea class="common" name="Remark" cols="111%" rows="2"></textarea> 
        </TD>
      </tr>
    </table>
 	</div>  
		  <table>
		   <tr>
		      <td>
		        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
		      </td>
		      <td class= titleImg>
		       客户基本信息
		      </td>
		   </tr>	
	  	</table>
	  	<Div  id= "divDetail" style= "display: ''">
	      <table  class= common>
	      	<TR  class= common>
	          <TD  class= title>
	            客户姓名
	          </TD>
	          <TD class= input>
	            <Input class= "readonly" readonly name=Name>
	          </TD>
			      <TD class= title>
			        性别
			      </TD>
			      <TD  class= input>
			         <Input class= "readonly" name=Sex readonly ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
			      </TD>
			      <TD  class= title>
			        出生日期
			      </TD>
			      <TD  class= input>
			      	<Input class= "readonly" readonly name=Birthday>
			      </TD>
		      </TR>
			    <TR  class= common>
			      <TD  class= title>
		           证件类型
		        </TD>
		        <TD  class= input>
		          <Input class= "readonly" readonly name="IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
		        </TD>
		        <TD  class= title>
		           证件号码
		        </TD>
		        <TD  class= input>
		          <Input class= "readonly" readonly name="IDNoReadonly" onblur="checkidtype();getBirthdaySexByIDNo(this.value);getallinfo();" verify="被保险人证件号码|len<=20"  >
		        </TD>
		        <TD  class= title>
		           婚姻状况
		        </TD>
		        <TD  class= input>
		          <Input class= "readonly" readonly name="Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
		        </TD>    
		      </TR>
		    </table>
	    </Div>
		<table id="ContTable" style="display: ''">
			<tr> 
				<td> 
				  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContList);"> 
				</td>
				<td class= titleImg>客户投保信息 </td>
				<td>
				  <INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewContInfo();"> 
				</td>
			</tr>
		</table>
		<Div  id= "divContList" style= "display: ''" align=center>  
			<table  class= common>
				<tr class= common>
			  	<td text-align: left colSpan=1>
						<span id="spanContGrid" ></span> 
			  	</td>
				</tr>
			</table>
		</Div>
	   <table>
		   <tr>
		      <td>
		        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAddress);">
		      </td>
		      <td class= titleImg>
		       客户保单地址信息
		      </td>
		   </tr>	
	  	</table>
	    <Div  id= "divAddress" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            家庭地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="HomeAddress" verify="家庭地址|len<=100" >
          </TD>
          <TD  class= title>
            家庭邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="HomeZipCode" verify="家庭邮政编码|zipcode" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            家庭电话
          </TD>
          <TD  class= input>
          <input class= common name="HomePhone" verify="家庭电话|len<=18" >
          </TD>
          <TD  class= title>
            家庭传真
          </TD>
          <TD  class= input colspan="3">
            <Input class= common name="HomeFax" verify="家庭传真|len<=15" >
          </TD>
        </TR>        
        <TR  class= common>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="CompanyAddress" verify="单位地址|len<=100" >
          </TD> 
          <TD  class= title>
            单位邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="CompanyZipCode" verify="单位邮政编码|zipcode" >
          </TD>                   
        </TR>        
        <TR  class= common>
          <TD  class= title>
            单位电话
          </TD>
          <TD  class= input>
            <Input class= common name="CompanyPhone" verify="单位电话|len<=18" >
          </TD>       
          <TD  class= title>
            单位传真
          </TD>
          <TD  class= input colspan="3">
            <Input class= common name="CompanyFax" verify="单位传真|len<=15" >
          </TD>                    
        </TR>
        <TR  class= common>        
          <TD  class= title>
            联系地址选择
          </TD>
          <TD  class= input>
             <Input class="codeNo" name="CheckPostalAddress" CodeData="0|^1|单位地址^2|家庭地址^3|其它"  ondblclick="return showCodeListEx('CheckPostalAddress',[this,CheckPostalAddressName], [0,1]);" onkeyup="return showCodeListKeyEx('CheckPostalAddress',[this,CheckPostalAddressName], [0,1]);"><Input class="codeName" name="CheckPostalAddressName" readonly >                  
          </TD>   
          <TD  class= title colspan=5>
            <font color=red>(在填写单位地址或家庭地址后实现速填)</font>
          </TD> 
        </TR>               
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="PostalAddress" verify="联系地址|len<=100" >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="ZipCode" verify="邮政编码|zipcode" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
          <input class= common name="Phone" verify="联系电话|len<=18" >
          </TD>
          <TD  class= title>
            通讯传真
          </TD>
          <TD  class= input>
            <Input class= common name="Fax" verify="传真|len<=15" >
          </TD>           
          <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class= common name="Mobile" verify="移动电话|len<=15" >
          </TD>         
        </TR>
        <TR class= common>
        	<TD class= title>
        		E-MAIL	
        	</TD>
        	<TD class= input>
        		<Input class= "common" name="E-MAIL" verify="E-MAIL|EMAIL">	
        	</TD>
        <TR>     
      </table> 
	    </DIV>
	    <br>
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);">
				</td>
				<td class= titleImg>
					关联保单
				</td>
			</tr>
		</table>
		<div  id= "divLCCont" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanLCContGrid" >
						</span>
					</td>
				</tr>
			</table>
		</div>
	</div>
	    <div >
		    <input type=button class=cssbutton value="保  存" onclick="submitForm();">
		    <input type=button class=cssbutton value="取  消" onclick="queryAddressInfo();">
		    <input type=button class=cssbutton value="返  回" onclick="top.window.close();">
	  	</div>
	  	<input type=hidden name="fmtransact">
	  	<input type=hidden name="ContNo">
  	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
