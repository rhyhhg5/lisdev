<%
//程序名称：TaskQueryMemberInit.jsp
//程序功能：工单管信箱查询页面初始化
//创建日期：2005-01-25 14:59:53
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="JavaScript">
//表单初始化
function initForm()
{
  try
  {
    initGrid();
  }
  catch(re)
  {
    alert("TaskSearchMemberInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrid()
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="成员名";         	  //列名
      iArray[1][1]="80px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[1][4]="TaskMemberNo";
      iArray[1][5]="3";
      iArray[1][9]="成员名|code:TaskMemberNo&NOTNULL";
      iArray[1][18]=250;
      iArray[1][19]= 0 ; 
      
      iArray[2]=new Array();
      iArray[2][0]="所属小组";         	
      iArray[2][1]="30px";            	
      iArray[2][2]=200;            		 
      iArray[2][3]=2;              		 
      iArray[2][4]="AcceptCom";
      iArray[2][5]="3";
      iArray[2][9]="信箱类型|code:AcceptCom&NOTNULL";
      iArray[2][18]=250;
      iArray[2][19]= 0 ; 
      
      iArray[3]=new Array();
      iArray[3][0]="岗位级别";         	  //列名
      iArray[3][1]="80px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="position";
      iArray[3][5]="3";
      iArray[3][9]="岗位级别|code:position&NOTNULL";
      iArray[3][18]=250;                  
      iArray[3][19]= 0 ;                   
                        
      iArray[4]=new Array();
      iArray[4][0]="所属机构";              //列名
      iArray[4][1]="80px";            	//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="station";
      iArray[4][5]="3";
      iArray[4][9]="所属机构|code:station&NOTNULL";
      iArray[4][18]=250;
      iArray[4][19]= 0 ; 
      
      iArray[5]=new Array();
      iArray[5][0]="成员编号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=3;       

      MemberGrid = new MulLineEnter("fm", "MemberGrid"); 
      //设置Grid属性
      MemberGrid.mulLineCount = 0;
      MemberGrid.displayTitle = 1;
      MemberGrid.locked = 1;
      MemberGrid.canSel = 1;
      MemberGrid.canChk = 0;
      MemberGrid.hiddenSubtraction = 1;
      MemberGrid.hiddenPlus = 1;
      MemberGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}

</script>