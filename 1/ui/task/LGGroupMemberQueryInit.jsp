<%
//程序名称：LGGroupMemberQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-23 10:20:45
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('GroupNo').value = "";
    fm.all('MemberNo').value = "";
  }
  catch(ex) {
    alert("在LGGroupMemberQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLGGroupMemberGrid();  
  }
  catch(re) {
    alert("LGGroupMemberQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LGGroupMemberGrid;
function initLGGroupMemberGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][2]=200;            		//列最大值
    iArray[0][3]=0;         		    //列名
    
    iArray[1]=new Array();
	iArray[1][0]="小组";         	  //列名
	iArray[1][1]="50px";            	//列宽
	iArray[1][2]=200;            			//列最大值
	iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	iArray[2]=new Array();
	iArray[2][0]="成员";         	
	iArray[2][1]="100px";            	
	iArray[2][2]=200;            		 
	iArray[2][3]=3;  
	
	iArray[3]=new Array();
	iArray[3][0]="岗位";         	
	iArray[3][1]="100px";            	
	iArray[3][2]=200;            		 
	iArray[3][3]=3;  
	
	iArray[4]=new Array();
	iArray[4][0]="个险业务上级";         	
	iArray[4][1]="100px";            	
	iArray[4][2]=200;            		 
	iArray[4][3]=3;  
	
	iArray[5]=new Array();
	iArray[5][0]="团险业务上级";         	
	iArray[5][1]="100px";            	
	iArray[5][2]=200;            		 
	iArray[5][3]=3;  
	
	iArray[6]=new Array();
	iArray[6][0]="银代业务上级";         	
	iArray[6][1]="100px";            	
	iArray[6][2]=200;            		 
	iArray[6][3]=3;  
	
	iArray[7]=new Array();
	iArray[7][0]="小组";         	  //列名
	iArray[7][1]="150px";            	//列宽
	iArray[7][2]=200;            			//列最大值
	iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
	iArray[8]=new Array();
	iArray[8][0]="成员";         	
	iArray[8][1]="100px";            	
	iArray[8][2]=200;            		 
	iArray[8][3]=0;  
	
	iArray[9]=new Array();
	iArray[9][0]="岗位";         	
	iArray[9][1]="150px";            	
	iArray[9][2]=200;            		 
	iArray[9][3]=0;  
	
	iArray[10]=new Array();
	iArray[10][0]="个险业务上级";         	
	iArray[10][1]="100px";            	
	iArray[10][2]=200;            		 
	iArray[10][3]=0;  
	
	iArray[11]=new Array();
	iArray[11][0]="团险业务上级";         	
	iArray[11][1]="100px";            	
	iArray[11][2]=200;            		 
	iArray[11][3]=0;  
	
	iArray[12]=new Array();
	iArray[12][0]="银代业务上级";         	
	iArray[12][1]="100px";            	
	iArray[12][2]=200;            		 
	iArray[12][3]=0;  
	
	LGGroupMemberGrid = new MulLineEnter("fm", "LGGroupMemberGrid"); 
	//设置Grid属性
	LGGroupMemberGrid.mulLineCount = 10;
	LGGroupMemberGrid.displayTitle = 1;
	LGGroupMemberGrid.locked = 1;
	LGGroupMemberGrid.canSel = 1;
	LGGroupMemberGrid.canChk = 0;
	LGGroupMemberGrid.hiddenSubtraction = 1;
	LGGroupMemberGrid.hiddenPlus = 1;
	LGGroupMemberGrid.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
