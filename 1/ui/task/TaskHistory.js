//程序名称：TaskPersonalBox.js
//程序功能：工单管理作业历史页面
//创建日期：2005-01-27 15:24:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

/* 查询工单在列表中显示结果 */
function easyQueryClick()
{
	//初始化表格
	initGrpGrid();
	
	//查询SQL语句
	var strSQL;
	strSQL = "select t.WorkNo, t.WorkNo, (select OwnerNo from LGWorkBox where WorkBoxNo = t.WorkBoxNo), " +
			 "       '', char(t.InDate) || ' ' || t.InTime " +
			 "from   LGWorkTrace t " +
	         "where  t.WorkNo = '" + WorkNo + "'" +
			 "order by t.NodeNo ";

	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		return "";
	}
	
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = TaskGrid;    
	      
	//保存SQL语句
	turnPage.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage.pageIndex = 0;
	
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	return true;
}