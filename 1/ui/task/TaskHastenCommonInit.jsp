<%
//程序名称：TaskHastenCommonInit.jsp
//程序功能：工单管理工单收回页面初始化
//创建日期：2005-07-22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<script language="JavaScript">

function initForm()
{
	initCustomerInfo();
}

//初始化催缴记录的信息
//function initHastenInfo()
//{
//	var sql = 	"select a.MakeDate, a.WorkNo, a.PayReason, b.Name, a.CustomerNo, EdorNo,  "
//				+ "from LGPhoneHasten a, LDPerson b, LPEdorMain c "
//				+ "where a.CustomerNo=b.CustomerNo "
//				+ "		and a.WorkNo=c.EdorAcceptNo "
//				+ "		and WorkNo='" + "<%=request.getParameter("WorkNo")%>" + "' "
//				+ "order by int(TimesNo) desc ";
//	var result = easyExecSql(sql);
//	
//	fm.MakeDate.value = result[0][0];
//	fm.WorkNo.value = result[0][1];
//	fm.PayReason.value = result[0][2];
//	fm.Name.value = result[0][3];
//	fm.AppntNo.value = result[0][4];
//	fm.PrtNo.value = result][0][5];
//}

//初始化客户信息
function initCustomerInfo()
{
	var sql =
		"	select HomePhone, CompanyPhone, Mobile "
		+ "	from LCAddress a, LCAppnt b "
		+ "	where a.CustomerNo = b.AppntNo "
		+ "		and a.AddressNo = b.AddressNo "
		+ "		and b.ContNo="
		+ "			(select ContNo "
		+ "			from LPEdorMain "
		+ "			where edorAcceptNo='" + <%=request.getParameter("WorkNo")%> + "')";
		
	var result = easyExecSql(sql);
	
	if(result)
	{
		fm.HomePhone.value = result[0][0];
		fm.CompanyPhone.value = result[0][1];
		fm.MobilePhone.value = result[0][2];
	}
}
</script>