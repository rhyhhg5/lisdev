<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="TaskCheckPersonalBox.jsp"%>
<%
//程序名称：TaskBack.jsp
//程序功能：工单管理工单收回页面
//创建日期：2005-01-18 18:34:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskBack.js"></SCRIPT>
  <%@include file="TaskBackInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%
  	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
  %>
  <script>
  	var operator = "<%=tGI.Operator%>";  //操作员编号
  </script>
</head>
<body  onload="easyQueryClick();" >
 <form action="./TaskBackSave.jsp" method=post name=fm target="fraSubmit">
    <input type="button" Class="cssButton" value="查  看" name="view" onclick="viewTask();">
 	<input type="button" Class="cssButton" value="收  回" name="back" onclick="backTask();">
 	<br><br>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskList);"> 
      </td>
      <td width="185" class= titleImg>待收回工单列表 </td>
    </tr>
  </table>
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanTaskGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 
  </Div>
  </Form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>