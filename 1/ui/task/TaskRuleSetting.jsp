<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskRuleSetting.jsp
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-05-26 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskRuleSetting.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="TaskRuleSettingInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./TaskRuleSettingSave.jsp" method=post name=fm target="fraSubmit">
    <table  class=common align=center>
		<tr align=right>
			<td class=button >
				<INPUT class=cssButton VALUE="新  建"  TYPE=button onclick="window.location.reload();">
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return save();">
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>			
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>			
			<td class=button width="10%" align=right>
				<INPUT class=cssButton VALUE="删  除"  TYPE=button onclick="return deleteClick();">
			</td>
		</tr>
	</table>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRuleSetting);">
    		</td>
    		 <td class= titleImg>
        		 自动转发规则管理
       		 </td>
    	</tr>
    </table>
    <Div  id= "divRuleSetting" style= "display: ''">
		<table  class= common align='center' >
  			<TR  class= common>
    			<TD  class= title>
    			  规则内容
    			</TD>
    			<TD  class= input>
    			  	<Input class= 'common' name="ruleContent">
    			</TD>
    			<TD  class= title>
    			  来源机构
    			</TD>
    			<TD  class= input>
    			  	
			  		<input class="codeNo" name="SourceComCode"  elementtype="nacessary" ondblclick="return showCodeList('station', [this,SourceComCodeName], [0,1]);" onkeyup="return showCodeListKey('station', [this,SourceComCodeName],[0,1]);"><input class="codename" name="SourceComCodeName" >
    			</TD>
  			</TR>
  			<TR  class= common>
    			 <TD  class= title>
    			  规则类型
    			</TD>
    			<TD  class= input>
<!--					  
			  		<input class="codeNo" name="goalType" readonly CodeData="0|^1|扫描^2|保全未收费催缴^3|续期催缴^4|特需满期结算^6|定期结算^7|个险续保核保受理^8|个单续期续保电话催缴" ondblclick="return showCodeListEx('goalType1', [this,goalTypeName], [0,1]);" onkeyup="return showCodeListKey('goalType1', [this,goalTypeName],[0,1]);"><input class="codename" name="goalTypeName" >
-->
			  		<input class="codeNo" name="goalType" readonly ondblclick="return showCodeList('ruletype', [this,goalTypeName], [0,1]);" onkeyup="return showCodeListKey('ruletype', [this,goalTypeName],[0,1]);"><input class="codename" name="goalTypeName" >

    			</TD>	
    			 <TD  class= title>
    			  目标
    			</TD>
    			<TD  class= input>
    			   	<input class= 'common' name="target" readonly >
    			   	<INPUT VALUE="查  询" TYPE=button Class="cssButton" onclick="queryTarget();">
    			</TD>
  			</TR>
  			<TR  class= common>
  			<TD  class= input colspan="4" >
  			<font color='red'>目标信箱现不可手动录入，点击目标后的查询按钮，在弹出的新页面中找到目标信箱，选中后点击返回即可</font>
  			</TD>
  			</TR>
  			<TR  class= common>
  				
  			  	<TD  class= title>
  			  	  规则描述
  			  	</TD>
  			  	<td class=input colspan="3">
					<textarea classs ="common" name="ruleDescription" cols="80%" rows="3"></textarea>
				</td>     
  			</TR>
		</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact" value="INSERT||MAIN">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="ruleNo" name="ruleNo" value="">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>