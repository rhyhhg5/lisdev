//程序名称：TaskPersonalBox.js
//程序功能：工单管理小组信箱页面
//创建日期：2005-01-24 14:10:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var mType = 0;

/* 改变查询条件 */
function changeCondition() 
{
	mType = Number(fm.condition.value);
}

/* 查询工单在列表中显示结果 */
function easyQueryClick()
{		
	//初始化表格
	initGrid();
	
	//查询SQL语句
	var strSQL;
	strSQL = "select a.memberno, b.usercode, b.username "
	       + "from lggroupmember a, lduser b "
 	       + "    where a.memberno = b.usercode "
	       + "        and a.groupno = (select groupno from lggroupmember where memberno = '" + operator + "') "
	       + "        and a.memberno != '" + operator + "' "
	       + "        and (select ownerno from lgworkbox where ownerno = a.memberno) is not null "
	       + "and int(substr(a.PostNo, length (a.PostNo)-1, length(a.PostNo))) > 5"
	       + "    order by a.memberno ";
  turnPage.queryModal(strSQL, TaskGrid);
	showCodeName();
	
	return true;
}
/* 查看选中的工单 */
//-------------------------------------------------------------------------------

function viewTask()
{
	var pageName = "TaskView.jsp";
	var param;
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	var DetalWorkNo;
	
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
				DetailWorkNo = WorkNo ;  
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	
	if (WorkNo)
	{
	    var width = screen.availWidth - 10;
	    var height = screen.availHeight - 28;
		win = window.open("TaskViewMain.jsp?loadFlag=PERSONALBOX&WorkNo="+WorkNo 
							+"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
		win.focus();
	}
}

//-------------------------------------------------------------------------------

/* 检查表单 */
function beforeSubmit()
{
	var tNum = fm.all("Num").value;
	if (tNum == '')
	{
		alert("请输入取单个数!");
		fm.all("Num").focus();
		return false;
	}
	if ((!isInteger(tNum)) || (Number(tNum) == 0))
	{
		alert("取单个数输入有误！");
		fm.all("Num").focus();
		return false;
	}
	if (Number(tNum) > TaskGrid.mulLineCount)
	{
		alert("取单数不能超过现有工单数！");
		fm.all("Num").focus();
		return false;
	}
	return true;
}

/* 提交表单 */
function submitForm()
{
	
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.submit();
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content,queryType,statusType)
{
	/*
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
	}*/
	opener.afterSubmit(FlagStr, content);
}    


function getItem() {
		var chkNum = 0;
	
	  for (i = 0; i < TaskGrid.mulLineCount; i++)
	  {
		    if (TaskGrid.getChkNo(i)) 
		    {
		  	    chkNum = chkNum + 1;
		    }
	  }
	  if (chkNum == 0)
	  {
		    alert("请选择你要分配工单的组员！");
		    return false;
	  }
		
		var memberNo = "";
    for(i=0; i<TaskGrid.mulLineCount; i++)
    {
    	 	if(TaskGrid.getChkNo(i))
        	{
        	    memberNo += TaskGrid.getRowColData(i,1)+"@";
        	}
    }	
		
    var checkData=opener.getCheckData();

    var workNo="";
    for (i=0;i<checkData.length;i++)
    {
    	workNo+=checkData[i]+"@";
    }	
    fm.workNo.value = workNo;
    fm.memberNo.value = memberNo;
    
    submitForm();
}