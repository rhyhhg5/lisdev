<%
//程序名称：LGGroupQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-22 17:32:49
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    
<%
   	GlobalInput tGI = (GlobalInput) session.getValue("GI");	
%>              
<script language="JavaScript">
function initInpBox()
{     
  fm.all('GroupNo').value = "";
  fm.all('GroupName').value = "";
  fm.all('GroupInfo').value = "";
  fm.all('WorkTypeNo').value = ""; 
}       
                             
function initForm() 
{
  try 
  {
    initInpBox();
    initLGGroupGrid();  
  }
  catch(re) 
  {
    alert("操作错误！请关掉上次登陆打开的页面");
  }
}

//领取项信息列表的初始化
var LGGroupGrid;
function initLGGroupGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="小组编号";         	  //列名
    iArray[1][1]="50px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="小组名称";         	
    iArray[2][1]="100px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="小组描述";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="上级机构代码";      //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[5]=new Array();
    iArray[5][0]="上级机构";              //列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][4]="station";
    iArray[5][5]="3";
    iArray[5][9]="上级机构|code:station&NOTNULL";
    iArray[5][18]=250;
    iArray[5][19]= 0 ; 
    
    iArray[6]=new Array();
    iArray[6][0]="业务类型代码";              //列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="业务类型";              //列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][4]="TaskTopTypeNo";
    iArray[7][5]="3";
    iArray[7][9]="业务类型|code:TaskTopTypeNo";
    iArray[7][18]=250;
    iArray[7][19]= 0 ; 
    
    LGGroupGrid = new MulLineEnter("fm", "LGGroupGrid"); 
    //设置Grid属性
    LGGroupGrid.mulLineCount = 10;
    LGGroupGrid.displayTitle = 1;
    LGGroupGrid.locked = 1;
    LGGroupGrid.canSel = 1;
    LGGroupGrid.canChk = 0;
    LGGroupGrid.hiddenSubtraction = 1;
    LGGroupGrid.hiddenPlus = 1;
    LGGroupGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
