/* 查询个人客户保单信息 */
function queryContInfo()
{
	var customerNo = fm.all("CustomerNo").value;
	if(customerNo.length != 9)
	{
		return;
	}

	//书写SQL语句
	var strSQL;  //分投保人，主被保人和其它被保人三种情况
	strSQL = "select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, '', " +
	         "       a.PaytoDate, a.Prem, a.Amnt, a.AppFlag, b.PostalAddress, case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end " +
	         "from   LCCont a, LCAddress b " +
	         "where  a.AppntNo = b.CustomerNo " +
	         "and    (a.AppntNo = '" + customerNo + "' " +
	         " or    a.InsuredNo = '" + customerNo + "') " +
	         "and    a.AppFlag = '1' " +
	         "and    b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) " +
	         "union " +  
	         "select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, '', " +
	         "       a.PaytoDate, a.Prem, a.Amnt, a.AppFlag, b.PostalAddress, case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end " +
	         "from   LCCont a, LCAddress b " +
	         "where  a.AppntNo = b.CustomerNo " +
	         "and    a.ContNo in (select ContNo from LCInsured where InsuredNo = '" + customerNo + "') " +
	         "and    a.AppFlag = '1' " +
	         "and    b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ";     
	turnPage5.pageDivName = "divPage5";
	turnPage5.queryModal(strSQL, ContGrid);
	
	document.all("divContInfo").style.display = "";
	document.all("divGrpContInfo").style.display = "none";
	
	return true;
}

//显示团体客户团单信息
function qureyGrpContInfo()
{
	var customerNo = fm.all("CustomerNo").value;
	if(customerNo.length != 8)
	{
		return;
	}
	var sql = "select a.GrpContNo, a.PrtNo, a.GrpName, a.Peoples2, " +
					"        a.PolApplyDate, a.CValiDate, a.PayMode, a.Prem, a.Amnt, " +
					"        case a.state when '03030001' then '正在满期结算' " +
          " when '03030002' then '满期终止' when '00060001' then '续保有效' end,  b.GrpAddress " +
					"from LCGrpCont a, LCGrpAddress b " +
					"where a.AppntNo = b.CustomerNo " +
					"and   a.AddressNo = b.AddressNo " +
					"and   a.AppntNo ='" + customerNo + "' " +
					"and   a.AppFlag = '1'";
	
	turnPage6.pageDivName = "divPage6";
	turnPage6.queryModal(sql, GrpContGrid);
	
	document.all("divGrpContInfo").style.display = "";
	document.all("divContInfo").style.display = "none";
}

//查看个单信息
function viewCont()
{
	var selNo = ContGrid.getSelNo();
	if (selNo) 
	{
		var	ContNo = ContGrid.getRowColData(selNo - 1, 1);
		var PrtNo = ContGrid.getRowColData(selNo - 1, 2);
		if (ContNo)
		{
			win = window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&IsCancelPolFlag=0&LoadFlag=TASK&ContType=1", "ContWin");
			win.focus();
		}
	}
	else
	{
		alert("请先选择一条工单！"); 
	}
}

//查看团单信息
function viewGrpCont()
{
	var selNo = GrpContGrid.getSelNo();
	if (selNo) 
	{
		var	GrpContNo = GrpContGrid.getRowColData(selNo - 1, 1);
		if (GrpContNo)
		{
			//劉鑫
			var strSql = "SELECT prtno from lcgrppol l where grpcontno='"+GrpContNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) ";
	        var arrResult = easyExecSql(strSql);
	        if (arrResult != null) 
	        {
   			    win = window.open("../sys/GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo="+GrpContNo+"&prtNo="+arrResult[0][0]+"&ContType=1", "", "status=no,resizable=yes,scrollbars=yes ");    
	        }else
	        {
			    win = window.open("../sys/GrpPolDetailQueryMain.jsp?LoadFlag=TASK&ContNo="+GrpContNo + "&&LoadFlag=TASK&ContType=1", "ContWin");
   	        }
			win.focus();
		}
	}
	else
	{
		alert("请先选择一条工单！"); 
	}
}
