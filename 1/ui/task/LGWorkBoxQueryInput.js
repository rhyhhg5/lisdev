/** 
 * 程序名称：LGWorkBoxInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-02-23 09:59:56
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mCodeName;
window.onfocus=myonfocus;

//根据信箱类型判断mCodeName
function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "taskboxtype")
	{
		var tBoxType = fm.all("OwnerTypeNo").value;
		switch (tBoxType)
		{
		case "0":
			mCodeName = "";
			break;
		case "1":
			mCodeName = "acceptcom";
			break;
		case "2":
			mCodeName = "taskmemberno";
			break;
		default:
			mCodeName = "";
		}
	}
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() 
{		    
    var ownerNos = getOwnerNo(fm.OwnerNoName.value);
    if(ownerNos == "ALL")
    {
        ownerNos = "";
    }
    //该名称没有相应的代码
    else if(ownerNos == "")
    {
        ownerNos = "  and 1 != 1 "
    }
    else
    {
        ownerNos = "  and ownerNo in (" + ownerNos + ") ";
    }
    
	var strSql = " select WorkBoxNo, OwnerTypeNo, OwnerNo, ownerNo "
				+ "from LGWorkBox where 1=1 "
    			+ getWherePart("WorkBoxNo", "WorkBoxNo")
    			+ getWherePart("OwnerTypeNo", "OwnerTypeNo")
    			+ getWherePart("OwnerNo", "OwnerNo")
    			+ ownerNos
    			+ " order by WorkBoxNo ";

	turnPage.queryModal(strSql, LGWorkBoxGrid);
	
	showCodeName();
}

function getOwnerNo(ownerName)
{
    //未录入名称则查询所有
    if(ownerName == "")
    {
        return "ALL";
    }
    
    var sql2 = "  select userCode "
              + "from LDUser "
              + "where userName like '%%" + ownerName + "%%' ";
    var sql1 = "  select groupNo "
              + "from LGGroup "
              + "where groupName like '%%" + ownerName + "%%' "
    
    var sql;
    var result;
    //选择了个人信箱
    if(fm.OwnerTypeNo.value == "2")
    {
        sql = sql2;
    }
    //选择了小组信箱
    else if(fm.OwnerTypeNo.value == "1")
    {
        sql = sql1;
    }
    //选择了部门信箱
    else if(fm.OwnerTypeNo.value == "0")
    {
        return "";
    }
    //根据名称查信箱
    else
    {
        sql = sql1 + " union " + sql2;
    }
    
    result = easyExecSql(sql);
    var condition = "";
    if(result)
    {
        for(var i = 0; i < result.length; i++)
        {
            condition = condition + " '" + result[i][0] + "', ";
        }
    }
    condition = condition.substring(0, condition.length - 2); //去除最后一","号
    return condition;
}

//晴空当前录入数据
function clearData()
{
    initForm();
    fm.all('OwnerTypeNoName').value = "";
    fm.all('OwnerNoName').value = "";
}

function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
    var arrReturn = new Array();
	var tSel = LGWorkBoxGrid.getSelNo();
	

	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{
		try
		{	
			//alert(tSel);
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LGWorkBoxGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LGWorkBoxGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
