<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AppealSave.jsp
//程序功能：
//创建日期：2005-04-09
//创建人  ：Yang Yalin
//更新记录：更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput();
  
//得到前页传入的WorkNo
  String AppealNo = request.getParameter("WorkNo");
  String typeNo = request.getParameter("TypeNo");
  System.out.println("\n\n\n\n\n\n\n\nWorkNo is: " + AppealNo);

  if(AppealNo == null || AppealNo.equals(""))
  {
  	//得到新建电话回访是生成的工单号
  	AppealNo = (String) session.getAttribute("VIEWWORKNO");
  }
  
  tG=(GlobalInput)session.getValue("GI");
  transact = request.getParameter("fmtransact");

	//每条投诉对应一个LGAppealSchema
	LGAppealSchema tLGAppealSchema = new LGAppealSchema();
	
  tLGAppealSchema.setAppealNo(AppealNo);	//暂用WorkNo
	tLGAppealSchema.setAppealWayNo(request.getParameter("AppealWayNo"));
	tLGAppealSchema.setAcceptorNo(request.getParameter("AcceptorNo"));
	tLGAppealSchema.setAcceptorName(request.getParameter("AcceptorName"));
	tLGAppealSchema.setAcceptorPost(request.getParameter("AcceptorPost"));
	tLGAppealSchema.setAppealObjNo(request.getParameter("AppealObjNo"));
	tLGAppealSchema.setAppealObjName(request.getParameter("AppealObjName"));
	tLGAppealSchema.setAppealObjPhone(request.getParameter("AppealObjPhone"));
	tLGAppealSchema.setAppealObjDep(request.getParameter("AppealObjDep"));
	tLGAppealSchema.setDirectBoss(request.getParameter("DirectBoss"));
	tLGAppealSchema.setAppealContent(request.getParameter("AppealContent"));
	tLGAppealSchema.setAppealFeeback(request.getParameter("AppealFeeback"));
	tLGAppealSchema.setCustomerOpinion(request.getParameter("CustomerOpinion"));
	tLGAppealSchema.setDealTwiseFlag(request.getParameter("DealTwiseFlag"));
	tLGAppealSchema.setServiceMangerOpinion(request.getParameter("ServiceMangerOpinion"));

  
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGAppealSchema);
	tVData.add(tG);
	  	
	AppealBL tAppealBL = new AppealBL();
  if (tAppealBL.submitData(tVData, transact) == false)
	{
		FlagStr = "Fail";
		Content = "数据保存失败！";
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}  

  //添加各种预处理
%>
<html>
<script language="javascript">
	var frm = "<%=request.getParameter("fmtransact")%>";
	if(frm=="UPDATE||MAIN")
	{
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
	else
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
</script>
</html>
