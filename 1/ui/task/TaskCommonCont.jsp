<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCommonCont.jsp
//程序功能：工单管理作业历史信息页面
//创建日期：2005-03-20 16:47:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<%@include file="TaskCommonContInit.jsp"%>
<script src="TaskCommonCont.js"></script>

	<div id="divContInfo" style='display: none'>
  		<!--个单信息-->
		<table id="ContTable" style="display: ''">
			<tr> 
			  <td> 
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContList);"> 
			  </td>
			  <td class= titleImg>客户保单信息 </td>
			  <td>
			    <INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewCont();"> 
			  </td>
			</tr>
		</table>
		<Div  id= "divContList" style= "display: ''" align=center>  
			<table  class= common>
		 		<tr  class= common>
			  		<td text-align: left colSpan=1>
					<span id="spanContGrid" >
					</span> 
			  	</td>
				</tr>
			</table>
			<Div id= "divPage5" align=center style= "display: 'none' ">
			  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage5.firstPage(); showCodeName();"> 
			  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage5.previousPage(); showCodeName();"> 					
			  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage5.nextPage(); showCodeName();"> 
			  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage5.lastPage(); showCodeName();"> 
			</Div>
		</Div>
	</div>
		 
	<div id="divGrpContInfo" style='display: none'>
		<!--团单信息-->
		<table id="GrpContTable" style="display: ''">
			<tr> 
			  <td> 
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpContList);"> 
			  </td>
			  <td class= titleImg>客户保单信息 </td>
			  <td>
			    <INPUT VALUE="查  看" TYPE=button Class="cssButton" name="viewGrp" onclick="viewGrpCont();"> 
			  </td>
			</tr>
		</table>
		<Div  id= "divGrpContList" style= "display: ''" align=center>  
			<table  class= common>
		 		<tr  class= common>
			  		<td text-align: left colSpan=1>
					<span id="spanGrpContGrid" >
					</span> 
			  	</td>
				</tr>
			</table>
			<Div id= "divPage6" align=center style= "display: 'none' ">
			  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage6.firstPage(); showCodeName();"> 
			  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage6.previousPage(); showCodeName();"> 					
			  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage6.nextPage(); showCodeName();"> 
			  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage6.lastPage(); showCodeName();"> 
			</Div>
		</Div>
	</div>