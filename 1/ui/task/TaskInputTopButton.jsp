<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskTopButtonInput.jsp
//程序功能：工单管理工单新建页面顶部悬浮操作按钮
//创建日期：2005-04-03
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskInputTopButton.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>操作按钮 </title>
</head>
<body>
<form name=fm action="" target=fraSubmit method=post>
    <div style="display: ''">
    <input type="button" class=cssButton name="do" value="经  办" id="doBuss" onclick="doBusiness();">
    <input type="button" class=cssButton name="save" value="保  存" id="save" onclick="saveTask();">
    <input type="button" class=cssButton name="deliver" value="转  交" onclick="deliverTask();">
    <input type="button" class=cssButton name="wait" value="转为待办" onclick="toWait();">
    <input type="button" class=cssButton name="edor" value="简易保全" onclick="easyEdor();">
    <input type="hidden" class=cssButton name="finish" value="结  案" onclick="finishTask();">
    <input type="button" class=cssButton name="return" value="返  回" onclick="returnTask();">
    </div>
</form>
</body>
</html>