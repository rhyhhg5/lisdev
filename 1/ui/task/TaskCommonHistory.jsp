<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCommonHistory.jsp
//程序功能：工单管理作业历史信息页面
//创建日期：2005-03-20 16:47:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<%@include file="TaskCommonHistoryInit.jsp"%>
<script src="TaskCommonHistory.js"></script>
  <table>
    <tr> 
      <td class= common> 
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divHistoryList);"> 
      </td><br>
      <td class= titleImg>作业历史信息 </td>
      <td class= common> 
        &nbsp;&nbsp;
        <!--<INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewHistory();">  -->
      </td>
    </tr>
  </table>
  <Div  id= "divHistoryList" style= "display: ''" align="center">  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanHistoryGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
	<Div id= "divPage2" align="center" style= "display: 'none' ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();	showCodeName();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); showCodeName();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); showCodeName();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); showCodeName();"> 
	</Div>
  </Div>