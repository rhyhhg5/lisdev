<%
//程序名称：
//程序功能：功能描述
//创建日期：2005-05-27 10:20:45
//创建人  ：Yang Yalin
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('RuleContent').value = "";
    fm.all('RuleTarget').value = "";
  }
  catch(ex) {
    alert("在LGGroupMemberQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLGGroupMemberGrid();  
  }
  catch(re) {
    alert("LGGroupMemberQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LGGroupMemberGrid;
function initLGGroupMemberGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][2]=200;            		//列最大值
    iArray[0][3]=0;         		    //列名
    
    iArray[1]=new Array();
	  iArray[1][0]="规则编号";         	  //列名
	  iArray[1][1]="30px";            	//列宽
	  iArray[1][2]=200;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	  iArray[1][21] = "ruleNo";
      
    iArray[2]=new Array();
    iArray[2][0]="规则内容";         	  //列名
	  iArray[2][1]="100px";            	//列宽
	  iArray[2][2]=200;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[3]=new Array();
	  iArray[3][0]="来源机构";         	  //列名
	  iArray[3][1]="100px";            	//列宽
	  iArray[3][2]=200;            			//列最大值
	  iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
	  iArray[3][4]="station";
	  
	  iArray[4]=new Array();
	  iArray[4][0]="规则类型";         	  //列名
	  iArray[4][1]="70px";            	//列宽
	  iArray[4][2]=200;            			//列最大值
	  iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
	  iArray[4][4]="ruleType";
	  
	  iArray[5]=new Array();
	  iArray[5][0]="目标";         	
	  iArray[5][1]="60px";            	
	  iArray[5][2]=200;            		 
	  iArray[5][3]=0;  
	  iArray[5][21] = "target";
	  
	  iArray[6]=new Array();
	  iArray[6][0]="拥有者";         	
	  iArray[6][1]="150px";            	
	  iArray[6][2]=200;            		 
	  iArray[6][3]=2;
	  iArray[6][4]="ownerName"; 
	  
	  iArray[7]=new Array();
	  iArray[7][0]="规则描述";         	
	  iArray[7][1]="150px";            	
	  iArray[7][2]=200;            		 
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array();
	  iArray[8][0]="默认";         	
	  iArray[8][1]="30px";            	
	  iArray[8][2]=200;            		 
	  iArray[8][3]=0;  
	  
	  iArray[9]=new Array();
	  iArray[9][0]="机构来源";         	
	  iArray[9][1]="150px";            	
	  iArray[9][2]=200;            		 
	  iArray[9][3]=3;
	  iArray[9][21] = "SourceComCode";
	  
	  iArray[10]=new Array();
	  iArray[10][0]="规则类型";         	
	  iArray[10][1]="25px";            	
	  iArray[10][2]=200;            		 
	  iArray[10][3]=3;  
	  iArray[10][21] = "goalType";
	  
	  iArray[11]=new Array();
	  iArray[11][0]="拥有者";         	
	  iArray[11][1]="60px";            	
	  iArray[11][2]=200;            		 
	  iArray[11][3]=3; 
	  
	  LGGroupMemberGrid = new MulLineEnter("fm", "LGGroupMemberGrid"); 
	  //设置Grid属性
	  LGGroupMemberGrid.mulLineCount = 0;
	  LGGroupMemberGrid.displayTitle = 1;
	  LGGroupMemberGrid.locked = 1;
	  LGGroupMemberGrid.canSel = 1;
	  LGGroupMemberGrid.canChk = 0;
	  LGGroupMemberGrid.hiddenSubtraction = 1;
	  LGGroupMemberGrid.hiddenPlus = 1;
	  LGGroupMemberGrid.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
