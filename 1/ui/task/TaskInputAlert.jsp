<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskInputAlert.jsp
//程序功能：工单管理保全信息录入页面
//创建日期：2005-02-21 16:43:30
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskInputAlert.js"></SCRIPT>
  <%@include file="TaskInputAlertInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单录入 </title>
  <script>
  	var operator = "<%=tGI.Operator%>";  //操作员编号
  </script>
</head>
<body  onload="initForm();">
  <form action="./TaskInputSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskInfo);">
	      </td>
        <td class= titleImg>工单信息&nbsp;</td>
	    </tr>
    </table>
  <span id="operateButton">
<table class="common" align="center" id="table1">
	<tr align="right">
		<td class="button">&nbsp;&nbsp; </td>
		<td class="button" align="right" width="10%">
		<input class="cssButton" onclick="return submitForm();" type="button" value="增  加"> 
		</td>
		<td class="button" align="right" width="10%">
		<input class="cssButton" onclick="return updateClick();" type="button" value="修  改"> 
		</td>
		<td class="button" align="right" width="10%">
		<input class="cssButton" onclick="return queryClick();" type="button" value="查  询"> 
		</td>
		<td class="button" align="right" width="10%">
		<input class="cssButton" onclick="return deleteClick();" type="button" value="删  除"> 
		</td>
	</tr>
</table>
</span>
<div id="inputButton" style="DISPLAY: none">
	<table class="common" align="center" id="table2">
		<tr align="right">
			<td class="button">&nbsp;&nbsp; </td>
			<td class="button" width="10%">
			<input class="cssButton" onclick="return cancelForm();" type="button" value="取  消"> 
			</td>
			<td class="button" width="10%">
			<input class="cssButton" onclick="return resetForm();" type="button" value="重  置"> 
			</td>
			<td class="button" width="10%">
			<input class="cssButton" onclick="return submitForm();" type="button" value="保  存"> 
			</td>
		</tr>
	</table>
</div>
<!--用户校验类-->
<form name="fm1" method="post" target="fraSubmit" action="file:///E:/鏂囨。/gongdanpage1/gongdanpage/task/LCPolQueryDir.jsp">
	<table class="common" id="table3">
		<tr class="common">
			<td class="title" width="11%">保单号</td>
			<td class="input" width="26%">
			<input class="common" name="ContNo1" verify="个人保单号|NOTNULL" size="20"> 
			</td>
			<td class="input" width="26%">
			<input class="cssButton" onclick="queryCont()" type="button" value="查询"> 
			</td>
		</tr>
	</table>
</form>
<form name="fm" method="post" target="fraSubmit" action="file:///E:/鏂囨。/gongdanpage1/gongdanpage/task/PEdorSave.jsp">
	<table id="table4">
		<tr>
			<td>
			<img style="CURSOR: hand" onclick="showPage(this,divLCPol);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
			</td>
			<td class="titleImg">申请保单信息 </td>
		</tr>
	</table>
	<div id="divLCPol">
		<table class="common" id="table5">
			<tr class="common">
				<td class="title">保单号 </td>
				<td class="input">
				<input class="readonly" readOnly name="ContNo"> </td>
				<td class="title">投保人 </td>
				<td class="input">
				<input class="readonly" readOnly name="AppntName"> </td>
				<td class="title">主被保人 </td>
				<td class="input">
				<input class="readonly" readOnly name="InsuredName"> </td>
			</tr>
			<tr>
				<td class="title">实际保费 </td>
				<td class="input"><input class="readonly" readOnly name="Prem"> 
				</td>
				<td class="title">实际保额 </td>
				<td class="input"><input class="readonly" readOnly name="Amnt"> 
				</td>
				<td class="title">业务员 </td>
				<td class="input">
				<input class="readonly" readOnly name="AgentCode"> </td>
			</tr>
			<tr class="common">
				<td class="title">保单送交日期 </td>
				<td class="input">
				<input class="readonly" readOnly name="GetPolDate"> </td>
				<td class="title">生效日期 </td>
				<td class="input">
				<input class="readonly" readOnly name="ValiDate"> </td>
				<td class="title">交至日期 </td>
				<td class="input">
				<input class="readonly" readOnly name="PaytoDate"> 
				</td>
			</tr>
		</table>
	</div>
	<div id="divLPEdorApp">
		保全受理号 <input class="readonly" readOnly name="EdorAcceptNo"> 申请人姓名 
		<input class="readonly" readOnly name="EdorAppName"> 申请方式 
		<input class="readonly" readOnly name="AppType"> 申请号码 
		<input class="readonly" readOnly name="OtherNo"> 申请号码类型 
		<input class="readonly" readOnly name="OtherNoType"> </div>
	<table class="common" id="table6">
		<tr>
			<td class="input" width="26%">
			<div id="divappconfirm" style="DISPLAY: none">
				<input class="cssButton" onclick="edorAppConfirm()" type="button" value="申请确认"> 
			</div>
			</td>
		</tr>
	</table>
	<!--个人保全信息部分（列表） -->
	<table id="table7">
		<tr>
			<td class="common">
			<img style="CURSOR: hand" onclick="showPage(this,divLPPol);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
			</td>
			<td class="titleImg">保全申请 </td>
		</tr>
	</table>
	<div id="divLPPol">
		<table class="common" id="table8">
			<tr class="common">
				<td class="title">批单号 </td>
				<td class="input">
				<input class="readonly" readOnly name="EdorNo"> </td>
				<td class="title">操作员 </td>
				<td class="input">
				<input class="readonly" readOnly name="Operator"> </td>
				<td class="title">保全申请日期 </td>
				<td class="input">
				<input class="coolDatePicker" name="EdorAppDate" dateFormat="short" timerflag="1"><img onclick="calendar(EdorAppDate)" src="file:///E:/文档/gongdanpage1/gongdanpage/common/images/calendar.gif" vspace="1"> 
				</td>
			</tr>
			<tr class="common">
				<td class="title">变动保费 </td>
				<td class="input">
				<input class="readonly" readOnly name="ChgPrem"> </td>
				<td class="title">补/退费金额 </td>
				<td class="input">
				<input class="readonly" readOnly name="GetMoney"> </td>
				<td class="title">保全生效日期 </td>
				<td class="input">
				<input class="coolDatePicker" name="EdorValiDate" dateFormat="short" timerflag="1"><img onclick="calendar(EdorValiDate)" src="file:///E:/文档/gongdanpage1/gongdanpage/common/images/calendar.gif" vspace="1"> 
				</td>
			</tr>
		</table>
	</div>
	<table id="table9">
		<tr>
			<td>
			<img style="CURSOR: hand" onclick="showPage(this,divLCGrpPol);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
			</td>
			<td class="titleImg">保全项目信息 </td>
		</tr>
	</table>
	<div id="divLPGrpPol">
		<table class="common" id="table10">
			<tr class="common">
				<td class="title">批改类型 </td>
				<td class="input"><!--<Input class= "code" name=EdorType  ondblclick="return showCodeList('EdorCode',[this], null, null, RiskCode, 'RiskCode');" onkeyup="return showCodeListKey('EdorCode',[this], null, null, RiskCode, 'RiskCode');" >-->
				<input class="code" ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);" name="EdorType" verify="批改类型|notnull&amp;code:EdorCode"> 
				</td>
				<td class="title">操作员 </td>
				<td class="input">
				<input class="readonly" readOnly name="Operator1"> </td>
			</tr>
			<!--tr  class= common>
    	  	 <TD  class= title>
            		变动保费
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=ChgPrem >
          	 </TD>
          	 <TD  class= title>
            		补/退费金额
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=GetMoney >
          	 </TD>
		</tr>
	
     	 	</TR-->
		</table>
	</div>
	<div id="divAddDelButton" align="right">
		<input class="cssButton" onclick="addRecord();" type="button" value="添加保全项目"> 
<!--input type =button class=cssButton value="删除保全项目" onclick="deleteRecord();"-->
		<input class="cssButton" onclick="detailEdorType();" type="button" value="保全项目明细"> 
	</div>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 
</body>
</html>
