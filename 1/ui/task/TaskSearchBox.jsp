<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：TaskSearchBox.jsp
//程序功能：工单管理信箱查询界面
//创建日期：2005-01-25 14:39:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskSearchBox.js"></SCRIPT>
  <%@include file="TaskSearchBoxInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>信箱查询</title>
</head>
<body  onload="initForm(); " >
<form name=fm target=fraSubmit method=post>
  <span id="show"></div>
	<br></br>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 信箱号码 </TD>
          <TD  class= input> <Input class= common name=WorkBoxNo > </TD>
          <TD  class= title> 信箱类型 </TD>
          <TD  class= input> 
          	<input class="codeNo" name="OwnerTypeNo" ondblclick="return showCodeList('taskboxtype', [this, OwnerTypeNoName], [0, 1]);" onkeyup="return showCodeListKey('taskboxtype', [this, OwnerTypeNoName], [0, 1]);"><input class="codeName" name="OwnerTypeNoName" readonly> 
          </TD>
          <TD  class= title> 拥有者 </TD>
          <TD  class= input> <Input class= common name=OwnerNo > </TD> 
        </TR>
        <TR  class= common>   
          <TD  class= title> 所属小组 </TD>
          <TD  class= input >  
          	<input class="codeNo" name="GroupNo" ondblclick="return showCodeList('AcceptCom', [this, GroupNoName], [0, 1]);" onkeyup="return showCodeListKey('AcceptCom', [this, GroupNoName], [0, 1]);"><input class="codeName" name="GroupNoName" > 
          </TD>
           <TD> 
				    <INPUT VALUE="重 置" TYPE=button Class="cssButton" name="query" onclick="lcReload();">  
				    </TD>
       </TR>             
   </Table>  
      <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divBox);">
    		</TD>
    		<TD class= titleImg>
    			 客户信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divBox" style= "display: ''" align=center>
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanBoxGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="getFirstPage(); showCodeName();"> 
      <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="getPreviousPage(); showCodeName();"> 					
      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="getNextPage(); showCodeName();"> 
      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="getLastPage(); showCodeName();"> 
 </Div>	
</form>				
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
