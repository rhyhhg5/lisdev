<%
//程序名称：TaskGroupMemberInit.jsp
//程序功能：
//创建日期：2005-02-23 10:20:45
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="javascript">
var turnPage = new turnPageClass();

function initForm()
{
	initPurviewGrid();
	initElementtype();
}

// 项目权限信息列表的初始化
function initPurviewGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="项目权限编号";         	  //列名
		iArray[1][1]="0px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="项目";         	  //列名
		iArray[2][1]="30px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="项目名称";         	  //列名
		iArray[3][1]="120px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]= 2;              			//是否允许输入,1表示允许，0表示不允许		
		iArray[3][4]="edorcode";
		iArray[3][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[3][9]="保全项目|code:edorcode&NOTNULL";
		iArray[3][18]=250;
		iArray[3][19]= 0 ; 
		
		iArray[4]=new Array();
		iArray[4][0]="项目类型";         	  //列名
		iArray[4][1]="55px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="机构类型";         	  //列名
		iArray[5][1]="55px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="岗位";         	//列名
		iArray[6][1]="150px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[6][4]="position";
		iArray[6][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[6][9]="操作类型|code:position&NOTNULL";
		iArray[6][18]=250;
		iArray[6][19]= 0 ; 
		
		iArray[7]=new Array();
		iArray[7][0]="岗位";              //列名
		iArray[7][1]="70px";            	//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=0; 
		
		iArray[8]=new Array();
		iArray[8][0]="确认权";              //列名
		iArray[8][1]="70px";            	//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=0; 
		
		iArray[9]=new Array();
		iArray[9][0]="金额权限";              //列名
		iArray[9][1]="70px";            	//列宽
		iArray[9][2]=200;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[10]=new Array();
		iArray[10][0]="超额权限";         	//列名
		iArray[10][1]="70px";            	//列宽
		iArray[10][2]=200;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="时间权限";              //列名
		iArray[11][1]="55px";            	//列宽
		iArray[11][2]=200;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[12]=new Array();
		iArray[12][0]="是否需互核";              //列名
		iArray[12][1]="20px";            	//列宽
		iArray[12][2]=200;            			//列最大值
		iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[13]=new Array();
		iArray[13][0]="备注";              //列名
		iArray[13][1]="250px";            	//列宽
		iArray[13][2]=200;            			//列最大值
		iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[14]=new Array();
		iArray[14][0]="经办人";              //列名
		iArray[14][1]="190px";            	//列宽
		iArray[14][2]=200;            			//列最大值
		iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[15]=new Array();
		iArray[15][0]="修改时间";              //列名
		iArray[15][1]="190px";            	//列宽
		iArray[15][2]=200;            			//列最大值
		iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		PurviewGrid = new MulLineEnter("fm", "PurviewGrid"); 
		//这些属性必须在loadMulLine前
		PurviewGrid.mulLineCount = 0;
		PurviewGrid.displayTitle = 1;
		PurviewGrid.locked = 1;
		PurviewGrid.canSel = 1;
		PurviewGrid.canChk = 0;
		PurviewGrid.hiddenSubtraction = 1;
		PurviewGrid.hiddenPlus = 1;
		//PurviewGrid.addEventFuncName = "initBnfGrid";
		PurviewGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("项目权限信息列表初始化错误" + ex);
	}
}

</script>