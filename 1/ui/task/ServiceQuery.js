//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPersonGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{	
	var strSQL = "";
	
	
	
	// 初始化表格
	initPersonGrid();
	strSQL = "select distinct sr.CustomerNo,sr.Name,sr.mobile,sr.RealSendTime, "
	                 +"sr.Email,sr.FunctionCode,sr.Bussno,sr.content " 
                     +"From LIServiceRecord sr  "
                     +"where 1=1 "
                     + getWherePart( 'sr.CustomerNo','CustomerNo')
                     + getWherePart( 'sr.Name ',' CustomerName','like')
                     + getWherePart( 'sr.MessageId','MessageId')
                     + getWherePart( 'sr.RealSendTime','SendTime')
                     + getWherePart( 'sr.mobile','Mobile')
                     + getWherePart( 'sr.Email','Email')
                     + getWherePart( 'sr.FunctionCode','ServiceType')
                     + getWherePart( 'sr.Bussno','assoNo')
                     + "order by sr.RealSendTime";
    turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, PersonGrid); 
}

// 数据返回父窗口
function returnParent()
{
	top.close() ;
}

function getQueryResult()
{
	//获取正确的行号
	tRow = PersonGrid.getSelNo() - 1;

	arrSelected = new Array();
	//设置需要返回的数组
	strSql = "select * from LDPerson where 1=1"
	       + " and CustomerNo = '" + PersonGrid.getRowColData(tRow, 1) + "'"
//	       + " and Name = '" + PersonGrid.getRowColData(tRow, 2) + "'"
//	       + " and Sex = '" + PersonGrid.getRowColData(tRow, 3) + "'"
//	       + " and Birthday = '" + PersonGrid.getRowColData(tRow, 4) + "'"
//	       + " and IDType = '" + PersonGrid.getRowColData(tRow, 5) + "'"
//	       + " and IDNo = '" + PersonGrid.getRowColData(tRow, 6) + "'"
	       ;
	
	//alert(strSql);
	var arrResult = easyExecSql(strSql);
	return arrResult;
}

function isEnterDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if (keycode == "13")
	{
		return true;
	}
	else
	{
		return false;
	}
}

function queryWhenEnterDown()
{
	if(isEnterDown())
	{
		easyQueryClick();
	}
}

//按证件号查询客户
function queryByIDNo()
{
	queryWhenEnterDown();
}

//按客户号查询
function queryByCustomerNo()
{
	queryWhenEnterDown();
}

//按姓名查询
function queryByName()
{
	queryWhenEnterDown();
}

//出生日期查询
function queryByBirthday()
{
	queryWhenEnterDown();
}