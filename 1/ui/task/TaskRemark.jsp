<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskRemark.jsp
//程序功能：工单管理工单批注页面
//创建日期：2005-01-19
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskRemark.js"></SCRIPT>
  <%@include file="TaskRemarkInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单批注</title>
</head>
<body  onload="initForm();" >
<% 
   if (WorkNo.length == 1) 
   {
%>
  <form method=post name=fm>
  <%@include file="TaskCommonView.jsp"%>
  </form>
<% 
  }
%>  
   <form action="./TaskRemarkSave.jsp" method=post name=fm2 target="fraSubmit">
    <table>
      <tr>
        <td class=common>
	      	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskRemark);">
	      </td>
        <td class= titleImg>工单批注&nbsp;</td>
        <td class=common>
        <% if (WorkNo.length != 1) 
           {
        	  out.print("(工单数:"+WorkNo.length+")");
           }
        %>
        </td>
	    </tr>
    </table>
  <Div id="divTaskRemark" style="display: ''"> 
    <table class= common>
      <tr class= common> 
        <td  class=title> 批注内容</td>
        <td colspan="3"  class= input>
		      <textarea class=common name="RemarkContent" elementtype=nacessary cols="118%" rows="3"  verify="批注|notnull&len<330"></textarea> 
        </td>
      </tr>
    </table>
    	<input type="button" class=cssButton name="Send" value="保  存" onclick="submitForm();">&nbsp;
		<input type="button" class=cssButton name="Return" value="返  回" onclick="parent.window.close();">
	</div>
	<input type=hidden name="loadFlag" value="">
	<input type=hidden name="WorkNo" value="">
  </form>
 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
