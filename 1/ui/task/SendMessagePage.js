
function checkCellNumber(s) {
	var patrn = /^0{0,1}13[0-9]{9}$|^0{0,1}15[0-9]{9}$|^\\+8613[0-9]{9}$|^\\+8615[0-9]{9}$/;
	if (!patrn.exec(s)) {
		return false;
	}
	return true;
}
function checkmail(email) {
	var patrn = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
	if (!patrn.exec(s)) {
		return false;
	}
	return true;
}
function closePage() {
	top.close();
}
function checkData() {
	CustomerNo = fm.all("CustomerNo").value;
	CustomerName = fm.all("CustomerName").value;
	content = fm.all("content").value;
	email = fm.all("email").value;
	mobile = fm.all("mobile").value;
	if (CustomerNo == "" || CustomerNo == null) {
		alert("the customerNo is null");
		return false;
	}
	if (CustomerName == "" || CustomerName == null) {
		alert("the customerName is null");
		return false;
	}
	if (content == "" || content == null) {
		alert("the content is null");
		return false;
	}
	if ((email == "" || email == null) && (mobile == "" || mobile == null)) {
		alert("the email or mobile is null");
		return false;
	}
	if ((mobile != "") && !(checkCellNumber(mobile))) {
       //alert(checkCellNumber(mobile));
		alert("the cellNumber is error !!!!");
		return false;
	}
	if ((email != "" || email != null) && !(checkmail(email))) {
		alert("the email address error !!!!!!");
		return false;
	}
    return true;
}

/* 查询个单客户号 */
function queryCustomerNo() {
	showInfo = window.open("LDPersonQueryMain.jsp");
}
function afterQuery(arrQueryResult) {
	fm.all("CustomerNo").value = arrQueryResult[0][0];
	fm.all("CustomerName").value = arrQueryResult[0][1];
	var sql = "select lc.Mobile,lc.EMail from LCAddress lc where lc.CustomerNo = '" + arrQueryResult[0][0] + "'";
	var result = easyExecSql(sql);
	if (result) {
		fm.mobile.value = result[0][0];
		fm.email.value = result[0][1];
	}
	window.focus();
}
/* 保存完成后的操作，由子窗口调用 */
function afterSubmit4(FlagStr, content) {
	top.close();
	window.focus();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

