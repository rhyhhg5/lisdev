//ModifyBankInfoInput.js

function beforeSubmit(){
//	if (fm.AcceptNo.value == "")
//	{
//		alert("请先输入受理号查询！");
//		return false;
//	}

	if (!verifyInput2()) {
		return false;
	}
	return true;
}

// 查询按钮
function querySubServiceType() {
  if (fm.AcceptNo.value == "") {
	  fm.TopTypeNo.value = "";
	  fm.TopTypeNoname.value = "";
	  fm.TypeNo.value = "";
	  fm.TypeNoname.value = "";
  }
  
  if (beforeSubmit()) {
	// 书写SQL语句     
	  var strSql = "select TypeNo,AcceptCom,StatusNo from LGWork where acceptcom like '" 
		  + fm.ManagementOrganization.value + "%' and WorkNo = '" + fm.AcceptNo.value + "'";
	  
	  var arrResult = easyExecSql(strSql);
	  if (arrResult == null) {
		alert("该机构下未查询到该受理号");
		return false;
	  }
	  fm.lgWorkAcceptCom.value = arrResult[0][1];
	  var typeNo = arrResult[0][0];
	  if (typeNo == "09" || typeNo == "04" || typeNo == "08" || typeNo == "05" || typeNo == "01" 
		  || typeNo == "02" || typeNo == "03" || typeNo == "06" || typeNo == "07") {
		  fm.TopTypeNo.value = typeNo;
		  var topWorkTypeName = "select WorkTypeName from LGWorkType where WorkTypeNo = '" + typeNo + "'";
		  var tempRes = easyExecSql(topWorkTypeName);
		  fm.TopTypeNoname.value = tempRes[0][0];
		  return;
	  }
	  var topTypeNo;
	  if (typeNo != null) {
		  topTypeNo = typeNo.substring(0,2);
		  fm.TypeNo.value = typeNo;
		  var workTypeName = "select WorkTypeName from LGWorkType where WorkTypeNo = '" + typeNo + "'";
		  var arrRes1 = easyExecSql(workTypeName);
		  fm.TypeNoname.value = arrRes1[0][0];
	  }
	  if (topTypeNo != null) {
		  fm.TopTypeNo.value = topTypeNo;
		  var topWorkTypeName = "select WorkTypeName from LGWorkType where WorkTypeNo = '" + topTypeNo + "'";
		  var arrRes2 = easyExecSql(topWorkTypeName);
		  fm.TopTypeNoname.value = arrRes2[0][0];
	  }
  }
}

//修改按钮
function modifySubServiceType (){
	if (fm.AcceptNo.value == "" || fm.TopTypeNoname.value == "") {
		alert("请先查询!");
		return;
	}
	if (fm.TypeNo.value == "" || fm.TypeNoname.value == "") {
		alert("请选择子业务类型！");
		return;
	}
	var strSql = "select StatusNo from LGWork where acceptcom like '" 
		  + fm.ManagementOrganization.value + "%' and WorkNo = '" + fm.AcceptNo.value + "'";
	var arrResult = easyExecSql(strSql);
	var statusNo = arrResult[0][0];
	if (statusNo == "5") {
		alert("该工单已经结案!");
	    return false;
	}
	if (confirm("您是否确定修改该记录?")){
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		
		fm.fmtransact.value = "UPDATE||MAIN";
		fm.submit(); //提交
	} else {
		alert("您取消了修改操作！");
	}
}

function afterSubmit(FlagStr, content, transact){
	showInfo.close();
	if (FlagStr == "Fail" ){             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//执行下一步操作
		querySubServiceType();
	}
}

