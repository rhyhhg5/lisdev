<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：UpdateTaskSave.jsp
//程序功能：工单管理工单查看页面
//创建日期：2005-05-21
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
	CErrors tError = null;              
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput();  
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
	
	tG=(GlobalInput)session.getValue("GI");
	transact = request.getParameter("fmtransact");
	tLGWorkSchema.setWorkNo(request.getParameter("AcceptNo"));
	tLGTraceNodeOpSchema.setRemark(request.getParameter("Remark"));
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("ConfirmFlag",request.getParameter("ConfirmFlag"));
	
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGWorkSchema);
	tVData.add(tLGTraceNodeOpSchema);
	tVData.add(tTransferData);
	tVData.add(tG);
	  	
	TaskConfirmBL tTaskConfirmBL = new TaskConfirmBL();
	if (tTaskConfirmBL.submitData(tVData, transact) == false)
	{  
		FlagStr = "Fail";
		
		if(tTaskConfirmBL.mErrors.needDealError())
		{
			Content = tTaskConfirmBL.mErrors.getLastError();
		}
		else
		{
			Content = "数据保存失败！";                    
		} 
	} 
	else                                               
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}  

  //添加各种预处理
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>