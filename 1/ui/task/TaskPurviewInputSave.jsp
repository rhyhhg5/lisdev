 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskAddPurviewSave.jsp
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-07-06 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%             
	String FlagStr = "";
	String Content = "";
	
	String transact = request.getParameter("fmtransact");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	LGProjectPurviewSet tLGProjectPurviewSet = new LGProjectPurviewSet();
	LGProjectPurviewSchema tLGProjectPurviewSchema = new LGProjectPurviewSchema();
	
	try
  {
  	//获取输入信息
    tLGProjectPurviewSchema.setProjectNo(request.getParameter("ProjectNo"));
    tLGProjectPurviewSchema.setProjectType(request.getParameter("ProjectType"));
    tLGProjectPurviewSchema.setPostNo(request.getParameter("PostNo"));
    tLGProjectPurviewSchema.setMemberNo(request.getParameter("MemberNo"));
    tLGProjectPurviewSchema.setRemark(request.getParameter("Remark"));
    tLGProjectPurviewSchema.setRiskCode(request.getParameter("RiskCode"));
    tLGProjectPurviewSchema.setNeedOtherAudit("0");
    System.out.println("\n\n\n\n\n\n\n" + request.getParameter("MoneyPurviewFull"));
    	
    String confirmFlag = request.getParameter("ConfirmFlag");
    if(confirmFlag != null && confirmFlag.equals("2"))
    {
    	tLGProjectPurviewSchema.setConfirmFlag("2");
    	//金额权限
    	if(request.getParameter("MoneyPurview").equals(""))
      {
      	if(request.getParameter("MoneyPurviewFull") != null)
      		tLGProjectPurviewSchema.setMoneyPurview("9999999999");
      	else
      		tLGProjectPurviewSchema.setMoneyPurview("0");
      }
      else
      {
      	tLGProjectPurviewSchema.setMoneyPurview(
      		Double.parseDouble(request.getParameter("MoneyPurview")));
      }
      //超额权
      if(request.getParameter("ExcessPurview").equals(""))
      {
      	if(request.getParameter("ExcessPurviewFull") != null)
      		tLGProjectPurviewSchema.setExcessPurview("9999999999");
      	else
      		tLGProjectPurviewSchema.setExcessPurview("0");
      }
      else
      {
      	tLGProjectPurviewSchema.setExcessPurview(
      		Double.parseDouble(request.getParameter("ExcessPurview")));
      }
      //时间权
      if(request.getParameter("TimePurview").equals(""))
      {
      	if(request.getParameter("TimePurviewFull") != null)
      		tLGProjectPurviewSchema.setTimePurview("9999999999");
      	else
      		tLGProjectPurviewSchema.setTimePurview("0");
      }
      else
      {
      	tLGProjectPurviewSchema.setTimePurview(request.getParameter("TimePurview"));
      }
    }
    //确认权
    else
    {
    	tLGProjectPurviewSchema.setConfirmFlag(confirmFlag);
    	tLGProjectPurviewSchema.setMoneyPurview("0");
    	tLGProjectPurviewSchema.setExcessPurview("0");
    	tLGProjectPurviewSchema.setTimePurview("0");
    }
    
    
    tLGProjectPurviewSet.add(tLGProjectPurviewSchema);
    
    // 准备传输数据 VData
    VData tVData = new VData();  	
    tVData.add(tLGProjectPurviewSet);
    tVData.add(tG);
    
    	//调用后台提交数据
    TaskPostPurviewUI tTaskPostPurviewUI = new TaskPostPurviewUI();
    	if (tTaskPostPurviewUI.submitData(tVData, transact) == false)
    {
    	//失败
    	FlagStr = "Fail";
    	
    	//需要处理后台错误
    	if(tTaskPostPurviewUI.mErrors.needDealError())
    	{
    		System.out.println(tTaskPostPurviewUI.mErrors.getLastError());
    		
    		Content = tTaskPostPurviewUI.mErrors.getLastError();
    	}
    	else
    	{
    		Content = "数据保存失败!";
    	}
    }
    else
    {
    	FlagStr = "Succ";
    	Content = "数据保存成功！";			
    }
  	
  }
  catch(Exception e)
  {
  	FlagStr = "Fail";
  	Content = "获取输入数据失败";
  	System.out.println("TaskPurviewInputSave.获取输入数据失败");
  	e.printStackTrace();
  }
%>
<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>