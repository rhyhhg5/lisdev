<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskDeliverInit.jsp
//程序功能：工单管理工单转交页面初始化
//创建日期：2005-01-19 18:01:34
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String superManager = "";
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	
	System.out.println("\n\n\n转交时间: " + PubFun.getCurrentDate() + " "
	  + PubFun.getCurrentTime() + ", 操作人" + tGI.Operator);
	
	String sql = "select workBoxNo "
				 + "from LGWorkBox a, LGGroupMember b "
				 + "where a.ownerNo = b.superManager "
				 + "	and b.memberNo = '" + tGI.Operator + "' ";
	ExeSQL e = new ExeSQL();
	SSRS s = e.execSQL(sql);
	if(s != null && s.getMaxRow() > 0)
	{
		superManager = s.GetText(1, 1);
	}
	
	String[] WorkNo = null;
	String loadFlag = request.getParameter("loadFlag");
	//从察看页面调用
	if(loadFlag != null && loadFlag.equals("TaskViewTopButton"))
	{
    WorkNo = new String[1];
    WorkNo[0] = request.getParameter("WorkNo");
    
    if(WorkNo[0] == null || WorkNo[0].equals(""))
    {
	    WorkNo[0] = (String)session.getAttribute("VIEWWORKNO");
	  }
	}
  else
  {
      WorkNo = (String[])session.getAttribute("WORKNO");
  }
%>
<script language="JavaScript">
var operator = "<%=tGI.Operator%>";  //操作员编号
var WorkNo = "<%=WorkNo[0]%>";

//初始化表单
function initForm()
{
	initElementtype();
<%
  //只有一条记录时显示记录
  if (WorkNo.length == 1) 
  {
%>
	initTaskInfo();
<%
  }
%>
	fm2.WorkBoxNo.value = "<%=superManager%>";
	fm2.loadFlag.value = "<%=loadFlag%>";
}
</script>