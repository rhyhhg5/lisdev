<%
//程序名称：TaskLetterInit.jsp
//程序功能：
//创建日期：2005-08-17 16:14:56
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
      
<script language="JavaScript">
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
//表单初始化
function initForm()
{
	try
	{
		initBox();
		initLetterGrid();
		queryLetter();
		initScanGrid();
		queryScan();
		initCustomerAnwserGrid();

		queryCustomerAnwserr();
		initElementtype();
		
		showCodeName();
	
	}
	catch(re)
	{
		alert("初始化界面错误，请关闭后重新打开!");
	}
}

//初始化输入框
function initBox()
{
	fm.workNo.value = "<%= request.getParameter("workNo")%>";
	
	var sql = "select customerNo "
			  + "from LGWork "
			  + "where workNo='" + fm.workNo.value + "' ";
	var result = easyExecSql(sql);
	
	if(result)
	{
		fm.customerNo.value = result[0][0];
	}
}

function queryLetter()
{
	var sql = "select EdorAcceptNo, SerialNumber, LetterType, LetterSubType, LetterSubType, "
			  + "	SendObj, SendObj, SendDate, BackFlag, "
			  + "	case BackFlag "
			  + "		when '0' then '否' "
			  + "		when '1' then '是' "
			  +	"	end, "
			  + "	BackDate, RealBackDate, State, State ,LetterType "
			  + "from LGLetter "
			  + "where EdorAcceptNo='" + "<%= request.getParameter("workNo")%>" + "' "
			  + "order by int(SerialNumber) ";
	
	turnPage.pageDivName = "divPage";
	turnPage.pageLineNum = 4;
	turnPage.queryModal(sql, LetterGrid);
	
	
}

function queryScan()
{
	var sql = 	"select ES_Doc_Main.DocID, docCode, bussType, ES_Doc_Main.manageCom, "
				+ "	scanOperator, ES_Doc_Main.makeDate, ES_Doc_Pages.pageId "
				+ "from ES_Doc_Main, ES_Doc_Pages "
				+ "where	ES_Doc_Main.docID=ES_Doc_Pages.docID "
				+ "		and ES_Doc_Main.subType='BQ02' "
				+ "		and ES_Doc_Main.docID in "
				+ "		( select docID from ES_DOC_RELATION where bussNo='" + fm.workNo.value + "') ";
	
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, ScanGrid);
	

}

function queryCustomerAnwserr()
{
	var sql = "select insuredName, (select riskCode from LCPol where PolNo = a.PolNo union select riskCode from LBPol where PolNo = a.PolNo), " 
	      + " edorType, polno,UWNo, CustomerReply, DisagreeDeal "
				+ "from LPUWMaster a "
				+ " where EdorNo = '"+"<%= request.getParameter("workNo")%>"+"' "
				+ "		and autoUWFlag = '2' "
				+ "   and PassFlag = '4' "
				+ "order by edorType";
	turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sql, CustomerAnwserGrid);
	return true;
}

// 保单信息列表的初始化
function initLetterGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="受理号";         	  //列名
		iArray[1][1]="80px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=3; 
		
		iArray[2]=new Array();
		iArray[2][0]="序号";         	  //列名
		iArray[2][1]="50";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0; 
		
		
		iArray[3]=new Array();
		iArray[3][0]="函件分类";         	  //列名
		iArray[3][1]="80px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0; 
  	iArray[3][4]="mainlettertype"; 
		
		iArray[4]=new Array();
		iArray[4][0]="函件类型";         	  //列名
		iArray[4][1]="90px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=3;  
		
		iArray[5]=new Array();
		iArray[5][0]="函件类型";         	  //列名
		iArray[5][1]="80px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=2; 
		iArray[5][4]="letterType";               			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="函件下发对象";         	  //列名
		iArray[6][1]="40px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=3;           	
		
		iArray[7]=new Array();
		iArray[7][0]="函件下发对象";         	  //列名
		iArray[7][1]="100px";            	//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=2;  
		iArray[7][4]="sendObject";            	
		
		iArray[8]=new Array();
		iArray[8][0]="下发时间";          //列名
		iArray[8][1]="100px";            		//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="是否需回销";         	//列名
		iArray[9][1]="80px";            		//列宽
		iArray[9][2]=200;            			//列最大值
		iArray[9][3]=3;
		
		iArray[10]=new Array();
		iArray[10][0]="是否需回销";         	//列名
		iArray[10][1]="100px";            		//列宽
		iArray[10][2]=200;            			//列最大值
		iArray[10][3]=0;   
		
		iArray[11]=new Array();
		iArray[11][0]="应回销时间";         	//列名
		iArray[11][1]="100px";            	//列宽
		iArray[11][2]=200;            		  //列最大值
		iArray[11][3]=0;              		  //是否允许输入,1表示允许，0表示不允许 
		
		iArray[12]=new Array();
		iArray[12][0]="回销时间";         	//列名
		iArray[12][1]="100px";            	//列宽
		iArray[12][2]=200;            		  //列最大值
		iArray[12][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
		
		iArray[13]=new Array();
		iArray[13][0]="状态";         	//列名
		iArray[13][1]="90px";            	//列宽
		iArray[13][2]=200;            		  //列最大值
		iArray[13][3]=3;    
		
		iArray[14]=new Array();        
		iArray[14][0]="状态";         	
		iArray[14][1]="80px";          
		iArray[14][2]=200;            	
		iArray[14][3]=2;              	
		iArray[14][4]="letterState";   
		
    iArray[15]=new Array();
		iArray[15][0]="函件分类";         	  //列名
		iArray[15][1]="80px";            	//列宽
		iArray[15][2]=200;            			//列最大值
		iArray[15][3]=3; 
  	
  
		
		LetterGrid = new MulLineEnter("fm", "LetterGrid"); 
		//设置Grid属性
		LetterGrid.mulLineCount = 0;
		LetterGrid.displayTitle = 1;
		LetterGrid.locked = 1;
		LetterGrid.canSel = 1;
		LetterGrid.canChk = 0;
		LetterGrid.hiddenSubtraction = 1;
		LetterGrid.selBoxEventFuncName = "showLetter";
		LetterGrid.hiddenPlus = 1;
		LetterGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		  alert(ex);
	}
}

//初始化扫描件列表
function initScanGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="扫描页";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="40px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="单证编号";  
		iArray[1][1]="100px";   
		iArray[1][2]=200;     
		iArray[1][3]=1;
		
		iArray[2]=new Array();
		iArray[2][0]="单证号码";  
		iArray[2][1]="150px";   
		iArray[2][2]=200;     
		iArray[2][3]=0;		
		
		iArray[3]=new Array();
		iArray[3][0]="扫描件类型";         	  //列名
		iArray[3][1]="100px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[3][4]="bq_scan";
    	iArray[3][5]="3";              	                //引用代码对应第几列，'|'为分割符
    	iArray[3][9]="扫描件类型|code:bq_scan";
    	iArray[3][18]=250;
    	iArray[3][19]= 0 ;
		
		iArray[4]=new Array();
		iArray[4][0]="扫描机构";              //列名
		iArray[4][1]="222px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[4][4]="AcceptCom";
    	iArray[4][5]="3";              	                //引用代码对应第几列，'|'为分割符
    	iArray[4][9]="扫描机构|code:station";
    	iArray[4][18]=250;
    	iArray[4][19]= 0 ;
		
		
		iArray[5]=new Array();
		iArray[5][0]="扫描人";              //列名
		iArray[5][1]="100px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[5][4]="Scanoperater";
    	iArray[5][5]="3";              	                //引用代码对应第几列，'|'为分割符
    	iArray[5][9]="扫描人|code:Scanoperater";
    	iArray[5][18]=250;
    	iArray[5][19]= 0 ;
		
		iArray[6]=new Array();
		iArray[6][0]="扫描日期";         	//列名
		iArray[6][1]="100px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="扫描日期";         	//列名
		iArray[7][1]="200px";            	//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		iArray[7][4]="Makedate";
    	iArray[7][5]="3";              	                //引用代码对应第几列，'|'为分割符
    	iArray[7][9]="扫描日期|code:Makedate";
    	iArray[7][18]=250;
    	iArray[7][19]= 0 ;
		
		iArray[8]=new Array();
		iArray[8][0]="扫描日期";         	//列名
		iArray[8][1]="200px";            	//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		iArray[8][4]="Makedate";
    	iArray[8][5]="3";              	                //引用代码对应第几列，'|'为分割符
    	iArray[8][9]="扫描日期|code:Makedate";
    	iArray[8][18]=250;
    	iArray[8][19]= 0 ; 
  
		
		ScanGrid = new MulLineEnter("fm", "ScanGrid"); 
		//设置Grid属性
		ScanGrid.mulLineCount = 0;
		ScanGrid.displayTitle = 1;
		ScanGrid.locked = 1;
		ScanGrid.canSel = 1;
		ScanGrid.canChk = 0;
		ScanGrid.hiddenSubtraction = 1;
		ScanGrid.selBoxEventFuncName = "showScan";
		ScanGrid.hiddenPlus = 1;
		ScanGrid.loadMulLine(iArray);
	}
	catch(e)
	{
		alert("初始化扫描件时出错" + e);
	}
}

// 核保信息查询
function initCustomerAnwserGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="被保险人";         	  //列名
		iArray[1][1]="80px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=0; 
		
		iArray[2]=new Array();
		iArray[2][0]="险种编码";         	  //列名
		iArray[2][1]="50";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0; 
		
		
		iArray[3]=new Array();
		iArray[3][0]="项目名";         	  //列名
		iArray[3][1]="80px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0; 

		
		iArray[4]=new Array();
		iArray[4][0]="险种号";         	  //列名
		iArray[4][1]="90px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=3;  
		
		iArray[5]=new Array();
		iArray[5][0]="核保顺序号";         	  //列名
		iArray[5][1]="80px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=3; 

		
		iArray[6]=new Array();
		iArray[6][0]="客户意见";         	  //列名
		iArray[6][1]="80px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=2;           	
		iArray[6][4]="customerreply"; 

    iArray[7]=new Array();
		iArray[7][0]="不同意处理";         	  //列名
		iArray[7][1]="80px";            	//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=3;           	
		iArray[7][4]="disagreedeal";
		
		CustomerAnwserGrid = new MulLineEnter("fm", "CustomerAnwserGrid"); 
    CustomerAnwserGrid.mulLineCount = 0;   
    CustomerAnwserGrid.displayTitle = 1;
    CustomerAnwserGrid.canChk = 0;
    CustomerAnwserGrid.hiddenPlus = 1;
    CustomerAnwserGrid.hiddenSubtraction = 1;
    CustomerAnwserGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		  alert(ex);
	}
}

</script>