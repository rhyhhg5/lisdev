<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskGrpEndorItem.jsp
//程序功能：保全项目列表
//创建日期：2005-06-28
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>  

<%@include file="TaskGrpEndorItemInit.jsp"%>
<SCRIPT src="../bq/PEdor.js"></SCRIPT>
<table>
  <tr> 
    <td class= common> 
    	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divEdorItemGrid);"> 
    </td><br>
    <td class= titleImg>保全项目信息</td>
    <td class= common> 
      &nbsp;&nbsp;
    </td>
  </tr>
</table>
<Div  id= "divEdorItemGrid" style= "display: ''" align="center">  
	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpEdorItemGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>	
	<Div id= "divPage3" align="center" style= "display: 'none' ">
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage4.firstPage();	showCodeName();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage4.previousPage(); showCodeName();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage4.nextPage(); showCodeName();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage4.lastPage(); showCodeName();"> 
	</Div>
	<table  class= common>
 		<tr  class= common>
	  		<td text-align: left colSpan=1>
				<Input type =button class=cssButton value="保全项目明细" onclick="showDetailEdorType();"> 
				<Input type =button class=cssButton value="查看理算结果" onclick="edorAppConfirm()">
				<Input type =button class=cssButton id="showOtherPrintButton2" style="display: none" value="查看非保单服务结果" onclick="showOtherPrint()">
	  		</td>
		</tr>
	</table>		
</Div>
<input type=hidden name=loadFlagForItem value="TASK">
<!--AC 团单-->
<input type=hidden name=EdorAcceptNo>
<input type=hidden name=EdorType>
<input type=hidden name=EdorNo>
<input type=hidden name=ContType value=2>
<input type=hidden name=GrpContNo>

