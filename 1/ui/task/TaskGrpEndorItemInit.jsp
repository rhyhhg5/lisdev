<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskGEndorItemInit.jsp
//程序功能：保全项目列表
//创建日期：2005-06-27
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>  

<%
//	String workNo = (String) session.getAttribute("VIEWWORKNO");
		String workNo = request.getParameter("WorkNo");
%>

<SCRIPT>
var turnPage4 = new turnPageClass();

//查询团单项目信息	
function setGrpEdorItem()
{
    var tEdorAcceptNo = "<%=workNo%>";
    fm.all('EdorAcceptNo').value = tEdorAcceptNo;
    
    var tEdorNo = "<%=workNo%>";
    fm.all('EdorNo').value = tEdorNo;
    
    //险种信息初始化
	if(tEdorAcceptNo != null && tEdorNo!= "")
	{
		var tableName = "";
		var sql = "select statusNo "
				  + "from LGWork "
				  + "where workNo = '" + WorkNo + "' ";
		var result = easyExecSql(sql);
		
		//工单未撤销
		if(result[0][0] != "8")
		{
			tableName = "LPGrpEdorItem";
		}
		else
		{
			tableName = "LOBGrpEdorItem";
		}
		
		var strSQL =
			"  select distinct a.EdorType, a.GrpContNo, c.CustomerNo, a.MakeDate, a.ModifyDate, a.Operator, "
			+ "		case c.statusNo "
			+ "			when '8' then '已撤销' "
			+ "			else case EdorState "
			+ "					when '1' then '录入完毕' "
			+ "					when '2' then '理算完成' "
			+ "					when '3' then '待录入' "
			+ "					when '0' then '保全确认' "
			+ "         when '9' then '理算完成' "
			+ "				 end "
			+ "		end, a.EdorType, a.EdorNo "
			+ "from " + tableName + " a, LGWork c "
			+ "where a.EdorAcceptNo = c.workNo "
			+ "		and a.EdorAcceptNo = '" + tEdorAcceptNo + "' ";
	}
	
	turnPage4.pageDivName = "divPage4";
	turnPage4.queryModal(strSQL, GrpEdorItemGrid);
	
	return true;
}

//得到选中行的数据并初始化部分隐藏域的值
function getRowData()
{
	rowData = GrpEdorItemGrid.getRowData(GrpEdorItemGrid.getSelNo() - 1);
	fm.EdorType.value = rowData[7];
	fm.EdorNo.value = rowData[8];
	fm.GrpContNo.value = rowData[1];
}

function getLGWorkInfo()
{
    var sql = "  select * "
              + "from LGWork "
              + "where workNo = '" + WorkNo + "' ";
    return easyExecSql(sql);
}

function showDetailEdorType()
{
    if(GrpEdorItemGrid.mulLineCount == 0)
    {
      alert("没有保全项目。");
      return false;
    }
    
    if(fm.all('EdorType').value == "")
    {
      alert("请选择一个项目。");
      return false;
    }
    
    var result = getLGWorkInfo();
    if(result == null)
    {
        alert("没有查询到工单信息，请退出重试。");
        return false;
    }
    
    if(result[0][4] == "5" || result[0][4] == "6")
    {
        alert("本工单已结案，请通过\"查看理算结果\"的方式查看变更明细");
        return false;
    }
    /*
    if(result[0][4] == "7")
    {
        alert("本工单已被合并，无法察看其明细");
        return false;
    }
    */
    if(result[0][4] == "8")
    {
        alert("本工单已撤销，无法察看其明细");
        return false;
    }
    
    detailEdorType();
}

//查看理算结果
function edorAppConfirm()
{
  /*
  var workInfo = checkOperate(WorkNo);
  if(workInfo)
  {
    if(workInfo[0][12] != "0" && workInfo[0][12] != "2")
    {
      alert("保全还未理算，请通过\'保全项目明细\'查看项目信息。");
      return false;
    }
  }*/
  
	//window.open("../bq/ShowGrpEdorInfo.jsp?EdorNo=<%=(String)session.getAttribute("VIEWWORKNO")%>", "");
	window.open("../bq/ShowGrpEdorInfo.jsp?EdorNo="+fm.all('EdorNo').value, "");
	
}

//查询保全受理信息
function getOneLPEdorAppInfo(workNo)
{
  var sql = "  select * "
            + "from LPEdorApp "
            + "where edorAcceptNo = '" + workNo + "' ";
  return easyExecSql(sql);
}

function initGrpEdorItemGrid()
{
    var iArray = new Array();
      
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许            			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="批改类型";         		//列名
		iArray[1][1]="80px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[1][4]="EdorCodeForView";
		iArray[1][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[1][9]="批改类型汉字|code:EdorCodeForView&NOTNULL";
		iArray[1][18]=250;
		iArray[1][19]= 0 ;
		
		iArray[2]=new Array();
		iArray[2][0]="保单号码";         		//列名
		iArray[2][1]="80px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="客户号";         		//列名
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="生成时间";         		//列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="最后保存时间";         		//列名
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="最后处理人";         		//列名
		iArray[6][1]="80px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[6][4]="TaskMemberNo";
		
		iArray[7]=new Array();
		iArray[7][0]="处理状态";         		
		iArray[7][1]="60px";            		
		iArray[7][2]=100;            			
		iArray[7][3]=0;              			
		
		iArray[8]=new Array();
		iArray[8][0]="项目类型";         		
		iArray[8][1]="60px";            		
		iArray[8][2]=100;            			
		iArray[8][3]=3;
		
		iArray[9]=new Array();
		iArray[9][0]="批单号";         		
		iArray[9][1]="60px";            		
		iArray[9][2]=100;            			
		iArray[9][3]=3;
		
		GrpEdorItemGrid = new MulLineEnter( "fm" , "GrpEdorItemGrid" ); 
		//这些属性必须在loadMulLine前
		GrpEdorItemGrid.mulLineCount = 0;   
		GrpEdorItemGrid.displayTitle = 1;
		GrpEdorItemGrid.canSel =1;
		GrpEdorItemGrid.selBoxEventFuncName ="getRowData" ;     //点击RadioBox时响应的JS函数
		GrpEdorItemGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		GrpEdorItemGrid.hiddenSubtraction=1;
		GrpEdorItemGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
		alert(ex);
	}
	
	setGrpEdorItem();
}

</SCRIPT>