<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PhoneAnswerView.jsp
//程序功能：工单管理工单基本信息显示页面
//创建日期：2005-04-04
//创建人  Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<%@include file="PhoneAnswerViewInit.jsp"%>
	<SCRIPT src="PhoneInterview.js"></SCRIPT>
	
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>查看电话回访问卷 </title>
</head>

<body onload="initForm();">
 <form action="PhoneInterviewSave.jsp" method=post name=fm target="fraSubmit">
	<br></br>
 	<input type=button class="cssbutton" value="返  回" onclick="back();">
 	<input type=hidden name="TypeNo" value="">
 	<br><br>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divStart);"> 
      </td>
      <td width="100%" class= titleImg>开篇语 </td>
    </tr>
  </table>
  <Div  id= "divStart" style= "display: ''" align=center>  
  	<table  class= common>
  		<tr class=common>
  			<td colspan=2 class=commom>
  				<div id = "question1">一、您好，请问您是***先生/女士/小姐吗？</div>
  			</td>
  		</tr>
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					&nbsp;&nbsp;<input class="readonly" name="answer1" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark1" readonly cols="95%" rows="3"></textarea>
					<!--<textarea classs ="common" name="remark1" cols="80%" rows="3"></textarea>-->
				</td>
			</tr>
  		<tr class=common>
  		<td colspan=2>二、我是人保健康险公司电话回访人员,代表人保健康公司和您的业务员,
  			感谢您购买我们的产品，您的保单已经生效，为了维护您的权益，与您做一次电话回访，
  			现在可以吗？</td>
  		</tr>
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer2" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark2" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
		</table>
	</div>
	 <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQuestionary);"> 
      </td>
      <td width="100%" class= titleImg>问卷 </td>
    </tr>
  </table>
  <Div  id= "divQuestionary" style= "display: ''" align=center>  
  	<table  class= common>
	
  		<tr class=common>
  			<td class=common colspan=2>
  				三、那您在《人身保险投保提示函》上签过字了吧？
  			</td>
  		</tr>
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer3" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark3" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=commmon colspan=2>
					四、请问投保书是您和被保人亲自签名的吗？
				</td>
			</tr>
    	<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer4" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark4" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=common colspan=2>
					五、请问您已签收保单了吗？
				</td>
			</tr>
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer5" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark5" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=common colspan=2>
					<table>
            <tr> 
              <td class= common> 
              	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divQuestion6);"> 
              </td><br>
              <td class= titleImg>六、请问您购买的是以下保险吗？</td>
              <td class= common> 
                &nbsp;&nbsp;
              </td>
            </tr>
          </table>
					<div id = "divQuestion6">
					  <table  class= common>
           		<tr  class= common>
          	  		<td text-align: left colSpan=1>
          				<span id="spanLCPolGrid" >
          				</span> 
          	  		</td>
          		</tr>
          	</table>
          	<Div id= "divPage" align="center" style= "display: 'none' ">
          		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
          		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
          		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
          		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 
          	</Div>
					</div>
				</td>
    	</tr>  
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer6" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark6" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=common colspan=2>
					七、请问您对这份保险的内容是否了解呢？特别是关于保险责任、责任免除及退保方面的内容？
				</td>
    	</tr>  
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer7" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark7" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=common colspan=2>
					八、顺便提醒您，在您收到保单后享有十天犹豫期
				</td>
    	</tr>  
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer8" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark8" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=common colspan=2>
					<div id="question9">九、为了方便以后联系，再与您确认一下，您的联系地址是……邮编是……
				  </div>
				</td>
    	</tr>  
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer9" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark9" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>
			<tr class=common>
				<td class=common colspan=2>
					十、请问您对我公司还有什么意见或建议吗？(客户要求预约服务的请记录)
				</td>
    	</tr>  
			<tr class=common>
				<td class=input>
					客户回答
				</td>
				<td class=input>
					<input class="readonly" name="answer10" readonly >
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;&nbsp;备注
				</td>
				<td>
					<textarea class="readonly" style="background-color: #F7F7F7; border: none; overflow: hidden;" name="remark10" readonly cols="95%" rows="3"></textarea>
				</td>
			</tr>  		
		</table>
  </Div>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="fmAction" name="fmAction">
  <input type=hidden id="contNo" name="contNo">
  </Form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>