var mDebug="0";
var mOperate="";
var showInfo;
var mCodeName;
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function beforeSubmit()
{
	if(fm.all("workNo")[0].checked == false && fm.all("workNo")[1].checked == false)
	{
		alert("请选择合并项");
		return false;
	}
	
	return true;
}

function submitForm()
{
	if (beforeSubmit())
	{
		fm.fmtransact.value = "UPDATE||MAIN" ;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.submit(); //提交
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{		
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		//window.close();
		//parent.parent.window.reload();
		///parent.window.close();
		
		top.close();
		parent.opener.focus();
		top.opener.location.reload();
	}
}