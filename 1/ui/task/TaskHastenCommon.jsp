<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPhoneHastenCommon.jsp
//程序功能：工单管理个人信箱主界面
//创建日期：2005-07-15
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="TaskHastenCommon.js"></SCRIPT>
	<%@include file="TaskHastenCommonInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>电话催缴 </title>
</head>
<body  onload="initForm();">
	<form name=fm action="" target=fraSubmit method=post>
		<table class=common>
			<tr class=common>
				<TD class= title> 回访日期 </TD>
				<TD class= input>
					<Input class=readonly readonly name="MakeDate">
				</TD>
				<TD class= title> 回访记录号 </TD>
				<TD class= input>
					<Input class=readonly readonly name="WorkNo">
				</TD>
				<TD class= title> 缴费原因 </TD>
				<TD class= input>
					<input class="code" name="PayReason" value="保单服务收费">
				</TD>
			</tr>
			<tr>
				<TD class= title> 投保人姓名 </TD>
				<TD class= input>
					<input class=readonly readonly name="Name" readonly>
				</TD> 
				<TD class= title> 客户号码 </TD>
				<TD class= input>
					<input class=readonly readonly name="AppntNo" style="width:100">
					<input type="button" Class="cssButton" name="search" value="查询" onclick="queryCustomerNo();" >
				</TD> 
				<TD class= title></TD>
				<TD class= input>
				</TD>
			</tr>
			<tr>
				<TD class= title> 住宅电话 </TD>
				<TD class= input>
					<input class=readonly readonly name="HomePhone">
				</TD>
				<TD class= title> 单位电话 </TD>
				<TD class= input>
					<input class=readonly readonly name="CompanyPhone">
				 </TD> 
				<TD class= title> 手机号码 </TD>
				<TD class= input>
					<input class=readonly readonly name="MobilePhone">
				</TD>
			</tr>
			<tr style="">
				<TD class= title>批单号</TD>
				<TD class= input colspan=5>
					<input class=readonly readonly name="PrtNo">
				</TD
			</tr>
		</table>
		<Input type="hidden" class= common name="">
	</Form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
