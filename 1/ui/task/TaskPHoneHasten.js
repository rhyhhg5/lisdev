
//程序名称：TaskPhoneHasten.js
//程序功能：
//创建日期：2002-07-22 16:49:22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

//点击现金触发的事件
function payMoney()
{
	//显示现金新缴费期限
	fm.all("hide1").style.display= "";
	fm.all("hide2").style.display= "";
	//隐藏银行转帐日期
	fm.all("hide3").style.display= "none";
	fm.all("hide4").style.display= "none";
	fm.PayDate.value = "";
}

//点击银行转帐触发的事件
function payBank()
{
	//隐藏现金新缴费期限
	fm.all("hide1").style.display= "none";
	fm.all("hide2").style.display= "none";
	//显示银行转帐日期
	fm.all("hide3").style.display= "";
	fm.all("hide4").style.display= "";
	fm.PayLimit.value = "";
}

function setPayInfo()
{
	if(fm.CheckedFlag.checked == true)
	{
		for(var i = 1; i < 8; i++)
		{
			var idName = "id" + i; 
			fm.idName.style.display = "";
		}
	}
	else
	{
		for(var i = 1; i < 8; i++)
		{
			var idName = "id" + i; 
			fm.idName.style.display = "none";
		}
		fm.PayDate.value = "";
	}
}

function queryTaskInfo()
{
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	win = window.open("TaskViewMain.jsp?WorkNo=" + fm.EdorAcceptNo.value + "&CustomerNo=" + fm.AppntNo.value + "&DetailWorkNo=" + fm.EdorAcceptNo, 
					  "ViewWin",
					  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
	win.focus();
}


function cancelApp()
{
	window.open('TaskBQAppCancelMain.jsp?EdorAcceptNo=' + fm.EdorAcceptNo.value, 'EdorAppCancel');	
}

function afterAppCancel()
{
	fm.fmtransact.value = "CancelApp";
	fm.action = "TaskPhoneHastenSave.jsp?workNo=" + fm.WorkNo.value;
	submitForm();
}

//保存修改后的缴费信息
function saveHastenInfo()
{
	if(fm.CheckedFlag.checked == false)
	{
		alert("请选中重设缴费时间选项，并填写新缴费信息。");
		
		return false;
	}
	if((fm.CheckedFlag.checked == true) && (fm.PayDate.value == ""))
	{
		alert("请输入新缴费日期");
		
		return false;
	}
	
	fm.fmtransact.value = "1";	//SetPayInfo类中，mOperator=1表示收费
	submitForm();
}

function createHistory()
{
	fm.fmtransact.value = "CreateHistory";
	submitForm();
}

function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.close();
		top.opener.focus();
		top.opener.location.reload();
	}
}

//查询受理件信息
function goBack()
{
	top.close();
	top.opener.top.focus();
}