<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：TaskQueryMember.jsp
//程序功能：工单管理信箱查询界面
//创建日期：
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskQueryMember.js"></SCRIPT>
  <%@include file="TaskQueryMemberInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>信箱查询</title>
</head>
<body  onload="initForm(); " >
<form name=fm target=fraSubmit method=post>
	<br></br>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 成员名 </TD>
          <TD>
          	  <input class="code" name="MemberNo" type="hidden" value="">
		      <input class="code" name="MemberNoName" readOnly elementtype="nacessary" ondblclick="return showCodeList('TaskMemberNo', [this,MemberNo], [1,0]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,MemberNo],[1,0]);" verify="成员编号|notnull&code:TaskMemberNo">
          </TD> 
          <TD  class= title> 所属小组 </TD>
          <TD  class= input> 
          	<input class="code" name="GroupNo" type=hidden value=""> 
            <input class="code" name="GroupNoName" readOnly ondblclick="return showCodeList('AcceptCom', [this, GroupNo], [1, 0]);" onkeyup="return showCodeListKey('AcceptCom', [this, GroupNo], [1, 0]);" verify="信箱类型|notnull&code:AcceptCom"> 
          </TD>
          <TD  class= title> 岗位级别 </TD>
          <TD  class= input>
          	<input class="code" name="PostNo" type="hidden">
    		<input class="code" name="PostNoName" readonly ondblclick="return showCodeList('position', [this,PostNo], [1,0]);" onkeyup="return showCodeListKey('position', [this,PostNo], [1,0]);" verify="优先级别|&code:position">
          </TD> 
        </TR>
        <TR  class= common>   
          <TD  class= title> 所属机构 </TD>
          <TD  class= input colspan="5">  
          	<input class="code" name="ComCode" type=hidden value=""> 
            <input class="code" name="ComCodeName" readOnly ondblclick="return showCodeList('station', [this, ComCode], [1, 0]);" onkeyup="return showCodeListKey('station', [this, ComCode], [1, 0]);" verify="所属机构|notnull&code:station"> 
          </TD>
       </TR>             
   </Table>  
      <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="清  空" class= cssButton TYPE=button onclick="clearData();"> 
      <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divBox);">
    		</TD>
    		<TD class= titleImg>
    			 客户信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divBox" style= "display: ''" align=center>
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanMemberGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="getFirstPage(); showCodeName();"> 
      <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="getPreviousPage(); showCodeName();"> 					
      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="getNextPage(); showCodeName();"> 
      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="getLastPage(); showCodeName();"> 
 </Div>	
</form>				
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
