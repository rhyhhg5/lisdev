<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskInputAppeal.jsp
//程序功能：工单管理投诉信息录入页面
//创建日期：2005-02-21 16:17:42
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskInputAppeal.js"></SCRIPT>
  <%@include file="TaskInputAppealInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单录入 </title>
  <script>
  	var operator = "<%=tGI.Operator%>";  //操作员编号
  </script>
</head>
<body  onload="initForm();">
  <form action="./TaskInputSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskInfo);">
	      </td>
        <td class= titleImg>投诉信息&nbsp;</td>
	    </tr>
    </table>
	<div id="divLDPerson1">
		<table id="table13">
			<tr>
				<td width="18">
				<img style="CURSOR: hand" onclick="showPage(this,divLDPerson1);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
				</td>
				<td class="titleImg" width="197">客户资料</td>
			</tr>
		</table>
		<div id="divLDPerson2">
			<table class="common" id="table14">
				<tr>
					<td class="title" width="19%" height="22">客户号</td>
					<td class="input" width="27%">
					<input class="common" readOnly name="CustomerNo1">
					<input type="button" value="客户查询" name="Button22"> </td>
					<td class="title" width="22%">保单号</td>
					<td class="input" width="32%">
					<input class="common" readOnly name="CustomerNo"> 
					</td>
				</tr>
				<tr>
					<td class="title" width="19%" height="22">客户姓名</td>
					<td class="input" width="27%">
					<input class="common" readOnly name="CustomerNo2"></td>
					<td class="title" width="22%">性别</td>
					<td class="input" width="32%">
					<input class="common" readOnly name="CustomerNo3"></td>
				</tr>
				<tr>
					<td class="title" width="19%" height="22">年龄</td>
					<td class="input" width="27%">
					<input class="common" readOnly name="CustomerNo4"></td>
					<td class="title" width="22%">联系电话（手机）</td>
					<td class="input" width="32%">
					<input class="common" readOnly name="CustomerNo5"></td>
				</tr>
				<tr>
					<td class="title">家庭电话</td>
					<td class="input">
					<input class="common" readOnly name="CustomerNo8"> </td>
					<td class="title">单位电话</td>
					<td class="input">
					<input class="common" readOnly name="CustomerNo9"></td>
				</tr>
				<tr>
					<td class="title">地址</td>
					<td class="input">
					<input class="common" readOnly name="CustomerNo11"> </td>
					<td class="title">代理人姓名</td>
					<td class="input">
					<input class="common" readOnly name="CustomerNo12"></td>
				</tr>
				<tr>
					<td class="title">代理人编号</td>
					<td class="input">
					<input class="common" readOnly name="CustomerNo6"> </td>
					<td class="title">代理人联系电话</td>
					<td class="input">
					<input class="common" readOnly name="CustomerNo7"></td>
				</tr>
				<tr>
					<td colSpan="4">&nbsp;&nbsp;&nbsp; </td>
				</tr>
			</table>
		</div>
		<table id="table15">
			<tr>
				<td width="18">
				<img style="CURSOR: hand" onclick="showPage(this,divLDPerson1);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
				</td>
				<td class="titleImg" width="197">投诉对象资料</td>
			</tr>
		</table>
		<div id="divLDPerson3">
			<table class="common" id="table16">
				<tr>
					<td class="title" width="19%" height="22">姓名</td>
					<td class="input" width="27%">
					<input class="common" readOnly name="CustomerNo13">&nbsp; 
					</td>
					<td class="title" width="22%">编号</td>
					<td class="input" width="32%">
					<input class="common" readOnly name="CustomerNo14"> 
					</td>
				</tr>
				<tr>
					<td class="title" width="19%" height="22">联系电话</td>
					<td class="input" width="27%">
					<input class="common" readOnly name="CustomerNo15"></td>
					<td class="title" width="22%">所属部门</td>
					<td class="input" width="32%">
					<input class="common" readOnly name="CustomerNo16"></td>
				</tr>
				<tr>
					<td class="title" width="19%" height="22">上级部门</td>
					<td class="input" width="27%">
					<input class="common" readOnly name="CustomerNo17"></td>
					<td class="title" width="22%">　</td>
					<td class="input" width="32%">　</td>
				</tr>
				<tr>
					<td colSpan="4">&nbsp;&nbsp;&nbsp; </td>
				</tr>
			</table>
			<table id="table17">
				<tr>
					<td width="18">
					<img style="CURSOR: hand" onclick="showPage(this,divLDPerson1);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
					</td>
					<td class="titleImg" width="197">受理人</td>
				</tr>
			</table>
			<div id="divLDPerson5">
				<table class="common" id="table18">
					<tr>
						<td class="title" width="19%" height="22">姓名</td>
						<td class="input" width="27%">
						<input class="common" readOnly name="CustomerNo18">&nbsp; 
						</td>
						<td class="title" width="22%">编号</td>
						<td class="input" width="32%">
						<input class="common" readOnly name="CustomerNo19"> 
						</td>
					</tr>
					<tr>
						<td class="title" width="19%" height="22">岗位</td>
						<td class="input" width="27%">
						<input class="common" readOnly name="CustomerNo20"></td>
						<td class="title" width="22%">　</td>
						<td class="input" width="32%">　</td>
					</tr>
					<tr>
						<td colSpan="4">&nbsp;&nbsp;&nbsp; </td>
					</tr>
				</table>
			</div>
			<table id="table19">
				<tr>
					<td width="18">
					<img style="CURSOR: hand" onclick="showPage(this,divLDPerson1);" src="file:///E:/鏂囨。/gongdanpage1/gongdanpage/common/images/butExpand.gif"> 
					</td>
					<td class="titleImg" width="197">投诉信息</td>
				</tr>
			</table>
			<div id="divLDPerson4">
				<table class="common" id="table20" width="102%">
					<tr class="common">
						<td class="title" width="22%" height="22">投诉来源</td>
						<td class="input" width="76%">&nbsp;&nbsp;
						<span style="FONT-SIZE: 9pt; FONT-FAMILY: 宋体">
						<input type="radio" CHECKED value="V17" name="R1">电话<input type="radio" value="V18" name="R1">亲访<input type="radio" value="V19" name="R1"><input type="radio" value="V20" name="R1">代理人反馈<input type="radio" value="V21" name="R1">信函<input type="radio" value="V22" name="R1">传真<input type="radio" value="V23" name="R1">网络</span><span lang="EN-US" style="FONT-SIZE: 9pt; FONT-FAMILY: Times New Roman">/</span><span style="FONT-SIZE: 9pt; FONT-FAMILY: 宋体">媒体或消协转办<input type="radio" value="V24" name="R1">员工反馈</span></td>
					</tr>
					<tr>
						<td class="title" width="22%" height="22">投诉内容</td>
						<td class="input" width="76%">
						<textarea name="textarea1" rows="7" cols="73"></textarea></td>
					</tr>
					<tr>
						<td class="title" width="22%" height="22">投诉处理</td>
						<td class="input" width="76%">
						<textarea name="textarea2" rows="6" cols="73"></textarea></td>
					</tr>
					<tr>
						<td class="title" width="22%">处理意见</td>
						<td class="input" width="76%">
						<textarea name="textarea3" rows="6" cols="73"></textarea></td>
					</tr>
					<tr>
						<td class="title" width="22%">客户意见</td>
						<td class="input" width="76%">
						<textarea name="textarea4" rows="6" cols="73"></textarea></td>
					</tr>
					<tr>
						<td class="title" width="22%">客服总监意见</td>
						<td class="input" width="76%">
						<textarea name="textarea5" rows="6" cols="73"></textarea></td>
					</tr>
					<tr>
						<td colSpan="2">&nbsp;&nbsp;&nbsp; </td>
					</tr>
				</table>
			</div>
		</div>
	</div>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 
</body>
</html>
