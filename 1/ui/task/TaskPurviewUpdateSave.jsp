<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskAddPurviewSave.jsp
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-07-06 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput();
	
	//获得公共输入信息
	tG=(GlobalInput)session.getValue("GI");
	transact = request.getParameter("fmtransact");
	System.out.println("..transact: " + transact);

	LGProjectPurviewSchema tLGProjectPurviewSchema = new LGProjectPurviewSchema();
	LGProjectPurviewSet tLGProjectPurviewSet = new LGProjectPurviewSet();
	
	try
	{
		//获取输入信息	
		String confirmFlag = request.getParameter("ConfirmFlag");
		boolean flag = confirmFlag.equals("2");	//选择确认权
		
		tLGProjectPurviewSchema.setProjectPurviewNo(request.getParameter("ProjectPurviewNo"));
    	tLGProjectPurviewSchema.setNeedOtherAudit("0");
    	tLGProjectPurviewSchema.setRiskCode(request.getParameter("RiskCode"));
    	tLGProjectPurviewSchema.setConfirmFlag(request.getParameter("ConfirmFlag"));
		tLGProjectPurviewSchema.setRemark(request.getParameter("Remark"));
		
		if(flag)
		{
			//金额权
			String moneyPurvieFull = request.getParameter("MoneyPurviewFull");
			if(moneyPurvieFull != null)
			{
				tLGProjectPurviewSchema.setMoneyPurview("9999999999");
			}
			else if(moneyPurvieFull == null&& request.getParameter("MoneyPurview").equals(""))
			{
				tLGProjectPurviewSchema.setMoneyPurview("0");
			}
			else
			{
				tLGProjectPurviewSchema.setMoneyPurview(
					Double.parseDouble(request.getParameter("MoneyPurview")));
			}
			//超额权
			String excessPurviewFull = request.getParameter("ExcessPurviewFull");
			if(excessPurviewFull != null)
			{
				tLGProjectPurviewSchema.setExcessPurview("9999999999");
			}
			else if(excessPurviewFull == null && request.getParameter("ExcessPurview").equals(""))
			{
				tLGProjectPurviewSchema.setExcessPurview("0");
			}
			else
			{
				tLGProjectPurviewSchema.setExcessPurview(
					Double.parseDouble(request.getParameter("ExcessPurview")));
			}
			//超期权
			String timePurviewFull = request.getParameter("TimePurviewFull");
			if(timePurviewFull != null)
			{
				tLGProjectPurviewSchema.setTimePurview("9999999999");
			}
			else if(timePurviewFull == null && request.getParameter("TimePurview").equals(""))
			{
				tLGProjectPurviewSchema.setTimePurview("0");
			}
			else
			{
				tLGProjectPurviewSchema.setTimePurview(request.getParameter("TimePurview"));
			}
		}
		else
		{
			tLGProjectPurviewSchema.setExcessPurview(0);
			tLGProjectPurviewSchema.setExcessPurview(0);
			tLGProjectPurviewSchema.setTimePurview("0");
		}
		
		tLGProjectPurviewSet.add(tLGProjectPurviewSchema);
		
		// 准备传输数据 VData
		VData tVData = new VData();  	
		tVData.add(tLGProjectPurviewSet);
		tVData.add(tG);

	  	//调用后台提交数据
		TaskPostPurviewUI tTaskPostPurviewUI = new TaskPostPurviewUI();
  		if (tTaskPostPurviewUI.submitData(tVData, transact) == false)
		{
			//失败
			FlagStr = "Fail";
			
			//需要处理后台错误
			if(tTaskPostPurviewUI.mErrors.needDealError())
			{
				System.out.println(tTaskPostPurviewUI.mErrors.getLastError());
				
				Content = tTaskPostPurviewUI.mErrors.getLastError();
			}
			else
			{
				Content = "数据保存失败!";
			}
			System.out.println("数据保存失败!");
		}
		else
		{
			FlagStr = "Succ";
			Content = "数据保存成功！";			
		}
		
	}
	catch(Exception e)
	{
		FlagStr = "Fail";
		Content = "获取输入数据失败";
		System.out.println("TaskPurviewInputSave.jsp数据处理失败");
		e.printStackTrace();
	}
%>
<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>