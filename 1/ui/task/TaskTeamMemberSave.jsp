<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskTeamBoxSava.jsp
//程序功能：工单管理小组信箱数据保存页面
//创建日期：2005-01-24 14:30:25
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	
	String workNo = request.getParameter("workNo");
	String memberNo = request.getParameter("memberNo");
	System.out.println(workNo);
	System.out.println(memberNo);
	
	String[] workNoList = workNo.split("@");
	String[] memberNoList = memberNo.split("@");
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("workNoList",workNoList);
	tTransferData.setNameAndValue("memberNoList",memberNoList);
	//输入参数
	VData tVData = new VData();
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	tVData.add(tGI);
	tVData.add(tTransferData);
	TaskAllocationBL tTaskAllocationBL = new TaskAllocationBL();
	if (tTaskAllocationBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
		System.out.println(FlagStr);
	}
	else
	{ 
	  String[][] result = tTaskAllocationBL.getTAllocationResult();
	  String mResult = "";
	  for(int i = 0; i<result.length; i++) {
	      mResult += result[i][1]+"工单分配给用户"+result[i][0]+";";
	  }
		FlagStr = "Succ";
		Content = mResult;
		System.out.println(Content);
	}
	
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
	/*parent.afterSubmit("<%=FlagStr%>", "<%=Content%>");*/
</script>
</html>

