<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2005-06-02
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskPostPurview.js"></SCRIPT>
  <SCRIPT src="TaskPurviewInput.js"></SCRIPT>
  <%@include file="TaskPostPurviewInit.jsp"%>
  
    <Script>
var msql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and  subriskflag=#M# and riskprop=#I#) ";
</Script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>岗位权限管理</title>
</head>
<body  onload="initForm();">
	<form name=fm action="" target=fraSubmit method=post>
		<table  class=common align=center>
			<tr align=right>
				<td class=button colspan=6>
					<INPUT class=cssButton VALUE="新  建"  TYPE=button onclick="window.open('TaskPurviewInputMain.jsp', '');">
					<INPUT class=cssButton name="querybutton" VALUE="查  询" TYPE=button onclick="queryPurview();">
					<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="updateDate();">
					<INPUT class=cssButton VALUE="删  除"  TYPE=button onclick="deleteData();">
				</td>
			</tr>
			<tr align=right>
				<td class=title >排序方式选择
				</td>
				<td class= input>
					<select name="orderFlag" style="width: 160; height: 23" onchange="changeOrder();" >
						<option value = "ProjectNo">项目</option>
						<option value = "PostNo">岗位级别</option>
						<option value = "MemberNo">岗位</option>
					</select>
				</td>
				<td class= input colspan=4>
				</td>
			</tr>
			<tr align=right>
				<td class=title>项目名称</td>
				<td class= input>
					
    			  	<input class="codeNo" name="ProjectNo" elementtype="nacessary" readonly ondblclick="return showCodeList('edortypecode', [this,ProjectNoName], [0,1]);" onkeyup="return showCodeListKey('edortypecode', [this,ProjectNoName], [0,1]);" verify="优先级别|&code:edortypecode"><input class="codename" name="ProjectNoName" >
				</td>
				<td class=title >项目类型
				</td>
				<td class= input>
					<select name="ProjectType" elementtype="nacessary" style="width: 160; height: 23">
    					<option value = "9"></option>
      					<option value = "0">个险</option>
      					<option value = "1">团险</option>      					
      				</select>  
				</td>
				<td class=title >机构类型
				</td>
				<td class= input>
					<select name="DealOrganization" style="width: 160; height: 23">
    					<option value = "9"></option>
      					<option value = "0">总公司</option>
      					<option value = "1">分公司</option>      					
      				</select>
				</td>
			</tr>
			<tr align=right>
				<td class=title>岗位级别</td>
				<td class= input>
					
    			  	<input class="codeNo" name="PostNo" readonly ondblclick="return showCodeList('position', [this,PostNoName], [0,1]);" onkeyup="return showCodeListKey('position', [this,PostNoName], [0,1]);" verify="优先级别|&code:position"><input class="codename" name="PostNoName" >
				</td>
				<td class=title >权限类型
				</td>
				<td class= input>
					<input type=checkBox name="MoneyPurview">金额权
					<input type=checkBox name="ConfirmPurview">确认权
				</td>
				<td class=title >岗位
				</td>
				<td class= input>
					
		      		<input class="codeNo" name="MemberNo" readOnly style="width: 60" ondblclick="return showCodeList('PAMemberInGroup', [this,MemberNoName], [0,1]);" onkeyup="return showCodeListKey('PAMemberInGroup', [this,MemberNoName],[0,1]);" verify="成员编号|notnull&code:MemberInGroup "><input class="codename" style="width: 45" name="MemberNoName" >
    			  	<input type="button" Class="cssButton" name="search" value="查  询" onclick="queryMember();">
				</td>
			</tr>
			<tr>
<%--				<td class=title>渠道</td>--%>
<%--				<td class= input>--%>
<%--					<select name="SaleChnl" elementtype="nacessary" style="width: 160; height: 23">--%>
<%--    					<option value = "9">公共</option>--%>
<%--      					<option value = "0">个险</option>--%>
<%--      					<option value = "1">银保</option>      					--%>
<%--      				</select>  --%>
<%--				</td>					--%>
				<td class=title>险种</td>
					
<%--    			  <TD  class= input><Input class=codeno  name="RiskCode" onClick="showCodeList('riskprop',[this,RiskName],[0,1],null,1,msql,1);" onkeyup="showCodeListKey('riskprop',[this,RiskName],[0,1],null,1,msql,1);" ><Input class=codename name= RiskName></TD>--%>
    			  <TD  class= input><Input class=codeno  name="RiskCode" ondblclick="return showCodeList('riskcode',[this,RiskName],[0,1],null,msql,1);" onkeyup="return showCodeListKey('riskcode',[this,RiskName],[0,1],null,msql,1);" ><Input class=codename name= RiskName></TD>
			</tr>
		</table>
		
		<table id="TableId" style="display: none">			
    		<tr>
    		  <td width="17"> 
    		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divpostPurview);"> 
    		  </td>
    		  <td width="185" class= titleImg>项目权限规则管理</td>
    		</tr>
    	</table>
    	<div id="divpostPurview" style="display: ''">
    		<table  class= common>
   				<tr  class= common>
  			  		<td text-align: left colSpan=1>
						<span id="spanPurviewGrid" >
						</span> 
				  	</td>
				</tr>
			</table>
			<Div id= "divPage" align=center style= "display: 'none' ">
	  			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage(); showCodeName(); changePurviewFull()"> 
	  			<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage(); showCodeName(); changePurviewFull()"> 					
	  			<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage(); showCodeName(); changePurviewFull();"> 
	  			<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage(); showCodeName(); changePurviewFull();"> 
	 		</Div>
    	</div>
  		<input type=hidden id="fmtransact" name="fmtransact" value="">
	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
