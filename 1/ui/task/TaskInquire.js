//程序名称：TaskInquire.js
//程序功能：工单管理客户咨询
//创建日期：2005-03-28
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

/* 由工单管理调用的接口函数，在工单提交时校验数据 */
function invokeCheck()
{
	if (!beforeSubmit())
	{
		return false;
	}
	return true;
}

/* 由工单管理调用的接口函数，在工单提交时保存业务数据 */
function invokeSubmit()
{
	if (!submitForm())
	{
		return false;
	}
	return true;
}

/* 提交表单前的校验 */
function beforeSubmit()
{
	if (!verifyInput2()) 
	{
		return false;
	}
	return true;
}

/* 提交表单 */
function submitForm()
{	
	if (!beforeSubmit())
	{
		return false;
	}
	//var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	//showInfo = window.showModelessDialog(urlStr, window, 
	//	"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
	return true;
}