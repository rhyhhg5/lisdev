<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCustomerInit.jsp
//程序功能：工单管理客户业务页面初始化
//创建日期：2005-01-20 17:10:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>              

<script language="JavaScript">
//表单初始化
function initForm()
{
	try
	{
		initCustomerGrid();
	}
	catch(re)
	{
		alert("TaskPersonalBoxInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

</script>