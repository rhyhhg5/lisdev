<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：TaskInquireView.jsp
//程序功能：工单管理客户咨询
//创建日期：2005-03-29
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <%@include file="TaskInquireViewInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>客户咨询查看 </title>
</head>
<body  onload="initForm();">
  <form action="" method=post name=fm target="fraSubmit">
  	<Input type="hidden" class= common name="InquireNo">
	<table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divInquire);">
	      </td>
        <td class= titleImg>客户咨询&nbsp;</td>
	    </tr>
    </table>
	<Div id="divInquire" style="display: ''">
    <table class=common>
      <tr class=common>
      	<TD class= title> 机构 </TD>
        <TD class= input>
	    	<Input class="readonly" readonly name="ManageCom" ondblclick="return showCodeList('station', [this]);" onkeyup="return showCodeListKey('station', [this]);" verify="机构|&code:station">
	    </TD>
        <TD class= title> 姓名 </TD>
        <TD class= input>
	    	<Input class="readonly" readonly name="Name" elementtype="nacessary" verify="姓名|notnull">
	    </TD>
	    <TD class= title> 性别 </TD>
        <TD class= input>
	    	<Input class="readonly" readonly  name="Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" verify="客户性别|code:Sex">
	    </TD>
	  </tr>
      <tr class=common>
        <TD class= title> 电子邮件 </TD>
        <TD class= input>
        	<input class="readonly" readonly name="Email">
        </TD>
        <TD class= title> 联系电话 </TD>
        <TD class= input>
        	<input class="readonly" readonly name="Phone">
        </TD>
        <TD class= title> 主题项目 </TD>
        <TD class= input>
        	<input class="readonly" readonly name="Title">
        </TD>
      </tr>
      <tr class=common>
        <TD class= title> 详细内容 </TD>
        <TD class= input colspan="7">
        	<textarea class="readonly" readonly style="border: none; overflow: hidden; background-color: #F7F7F7;" name="Content" cols="92%" rows="5"></textarea> 
        </TD>
      </tr>
   </table>
   </Div>
 </form>
 <iframe width="100%" height="300" frameborder="0" id="remark" name="remark" scrolling="auto" noresize src="./TaskViewRemark.jsp?WorkNo=<%=request.getParameter("DetailWorkNo")%>"></iframe>
</body>
</html>

