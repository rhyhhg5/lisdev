<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskInputSava.jsp
//程序功能：工单管理工单收回数据保存页面
//创建日期：2005-01-21 11:18:43
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	String[] workNo = request.getParameterValues("TaskGridChk");
	int failTotal = 0;
	
	String errMsg = "";
	for(int i = 0; i < workNo.length; i++)
	{
		VData tVData = new VData();
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
    tLGWorkSchema.setWorkNo(workNo[i]);
    
    //从session中得到全局参数
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput) session.getValue("GI");
    
    tVData.add(tLGWorkSchema);
    tVData.add(tGI);
    System.out.println("ok");
    TaskBackBL tTaskBackBL = new TaskBackBL();
    if (tTaskBackBL.submitData(tVData, "ok") == false)
    {
    	FlagStr = "Fail";
    	failTotal++;
    	errMsg += workNo[i] + "：" + tTaskBackBL.mErrors.getLastError() + "\n";
    }
	}
    
  if(FlagStr.equals("Succ"))
  {
	  Content = "工单已成功收回";
	}
	else
	{
		Content = "共有 " + failTotal + " 工单回收失败，他们是：\n" + errMsg;
	}
	Content = PubFun.changForHTML(Content);
	
	//设置显示信息
	
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

