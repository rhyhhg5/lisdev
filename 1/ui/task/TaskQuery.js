/** 
 * 程序名称：TaskQuery.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-2005.3.23
 * 创建人  ：Yang Yalin
 * 更新人  ：
 */
 
var WorkNo;
var CustomerNo; 
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}

// 查询按钮
function easyQueryClick() 
{ 
	var strSql;
	if(fm.Businessno.value == "03"){
  strSql = "select edoracceptno, AcceptNo, PriorityNo, TypeNo, CustomerNo, 0, 0, " +
			"AcceptorNo, AcceptCom, AcceptDate, DateLimit, StatusNo ,b.sendpersonno,c.edorstate " + 
			"from LGWork a,LGWorkTrace b ,lpedorapp c " +
			"where 1=1 and a.WorkNo=b.WorkNo  and  a.acceptno=c.EdorAcceptNo and a.nodeNo = b.nodeNo and a.TypeNo like '"
			+ fm.Businessno.value+"%%' " 
			+getWherePart('a.AcceptNo', 'AcceptNo','like',0) 
			+getWherePart('a.CustomerNo', 'CustomerNo','like',0) 
			+getWherePart('a.AcceptorNo', 'usercode' ) 	
			+getWherePart('a.AcceptCom', 'Groupno') 
			+getWherePart('b.sendpersonno','CurrentHandlePerpon')
		  +getWherePart('b.sendcomno','CurrentHandleGroup')
		  +getWherePart('c.edorstate','BQStatusNo')
		  +getWherePart('a.TypeNo','SubBusinessno')
		  +getWherePart('a.Statusno','TaskStatusNo')
		  +getWherePart('a.acceptDate','startDate','>=',0)
		  +getWherePart('a.acceptDate','endDate','<=',0)
		  +" union "
		  +"select edoracceptno, AcceptNo, PriorityNo, TypeNo, CustomerNo, 0, 0, " +
			"AcceptorNo, AcceptCom, AcceptDate, DateLimit, StatusNo ,b.sendpersonno,c.edorstate " + 
			"from LGWork a,LGWorkTrace b ,lobedorapp c " +
			"where 1=1 and a.WorkNo=b.WorkNo  and  a.acceptno=c.EdorAcceptNo and a.nodeNo = b.nodeNo and a.TypeNo like '"
			+ fm.Businessno.value+"%%' " 
			+getWherePart('a.AcceptNo', 'AcceptNo','like',0) 
			+getWherePart('a.CustomerNo', 'CustomerNo','like',0) 
			+getWherePart('a.AcceptorNo', 'usercode' ) 	
			+getWherePart('a.AcceptCom', 'Groupno') 
			+getWherePart('b.sendpersonno','CurrentHandlePerpon')
		  +getWherePart('b.sendcomno','CurrentHandleGroup')
		  +getWherePart('c.edorstate','BQStatusNo')
		  +getWherePart('a.TypeNo','SubBusinessno')
		  +getWherePart('a.Statusno','TaskStatusNo')
		  +getWherePart('a.acceptDate','startDate','>=',0)
		  +getWherePart('a.acceptDate','endDate','<=',0)
			+" order by edoracceptno desc";		
		}
	else{
		 strSql = "select a.WorkNo, AcceptNo, PriorityNo, TypeNo, CustomerNo, 0, 0, " +
			"AcceptorNo, AcceptCom, AcceptDate, DateLimit, StatusNo ,b.sendpersonno,'' " + 
			"from LGWork a,LGWorkTrace b  " +
			"where 1=1 and a.WorkNo=b.WorkNo  and a.nodeNo = b.nodeNo and a.typeno like '"+
			fm.Businessno.value+ "%%' " 
			+getWherePart('a.AcceptNo', 'AcceptNo','like',0) 
			+getWherePart('a.CustomerNo', 'CustomerNo','like',0) 
			+getWherePart('a.AcceptorNo', 'usercode' ) 
			+getWherePart('a.AcceptCom', 'Groupno') 
			+getWherePart('b.sendpersonno','CurrentHandlePerpon')
			+getWherePart('b.sendcomno','CurrentHandleGroup')
		  +getWherePart('c.edorstate','BQStatusNo')
		  +getWherePart('a.TypeNo','SubBusinessno')
		  +getWherePart('a.Statusno','TaskStatusNo')
		  +getWherePart('a.acceptDate','startDate','>=',0)
		  +getWherePart('a.acceptDate','endDate','<=',0)
			+"order by a.WorkNo desc";					
	}
	turnPage.queryModal(strSql, ResultGrid);
	showCodeName();
}

//选中一条工单后跳转到相应的处理页面
function linkToPage(pageUrl)
{
	var selNum = 0;
	//var WorkNo;
	//var CustomerNo;
	
	selNum = ResultGrid.getSelNo();	
	if (selNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	WorkNo = ResultGrid.getRowColData(selNum - 1, 1);	
	CustomerNo = ResultGrid.getRowColData(selNum - 1, 5);

	if (WorkNo)
	{
		location.replace(pageUrl + "WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo);
	}
}

function viewTask()
{
	//linkToPage("TaskViewMain.jsp?loadFlag=QUERY&");
	
	var selNum = 0;
	
	selNum = ResultGrid.getSelNo();	
	if (selNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	WorkNo = ResultGrid.getRowColData(selNum - 1, 1);	
	CustomerNo = ResultGrid.getRowColData(selNum - 1, 5);
	DetailWorkNo = WorkNo;
	
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	win = window.open("TaskViewMain.jsp?WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
					  "ViewWin",
					  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
	win.focus();
}


function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
}
function returnParent()
{
    var arrReturn = new Array();
	var tSel = LGGroupGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{
		try
		{	
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LGGroupGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LGGroupGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
  //返回
function GoBack()
{
		top.close();
		top.opener.focus();
		top.opener.location.reload();
	
}
  //刷新
function clearData()
{
    fm.all('AcceptNo').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('usercode').value = "";
    fm.all('username').value = "";
    fm.all('Groupno').value = "";
    fm.all('Groupname').value = "";
    fm.all('Businessno').value="";
    fm.all('Businessname').value="";         
    fm.all('CurrentHandlePerpon').value="";         
    fm.all('CurrentHandleGroup').value="";   
    fm.all('BQStatusNo').value="";
    fm.all('BQStatusName').value="";
    fm.all('SubBusinessno').value="";
    fm.all('SubBusinessName').value="";      
    fm.all('TaskStatusName').value="";      
    fm.all('TaskStatusNo').value="";
    fm.all('startDate').value="";
    fm.all('endDate').value="";
}