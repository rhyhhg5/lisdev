<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPhoneHasten.jsp
//程序功能：工单管理个人信箱主界面
//创建日期：2005-07-15
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<!--<SCRIPT src="TaskPhoneHasten.js"></SCRIPT>-->
	<%@include file="TaskPhoneHastenInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>电话催缴 </title>
</head>
<body  onload="initForm();">
	<form name=fm action="TaskPhoneHastenSave.jsp" target=fraSubmit method=post>
		<table class=common>
			<tr class=common>
				<TD class= title> 回访日期 </TD>
				<TD class= input>
					<Input class=readonly readonly name="MakeDate">
				</TD>
				<TD class= title> 回访记录号 </TD>
				<TD class= input>
					<Input class=readonly readonly name="WorkNo">
				</TD>
				<TD class= title> 缴费原因 </TD>
				<TD class= input>
					<input class="readonly" readonly name="PayReason" value="">
				</TD>
			</tr>
			<tr>
				<TD class= title> 投保人姓名 </TD>
				<TD class= input>
					<input class=readonly readonly name="Name" readonly>
				</TD> 
				<TD class= title> 客户号码 </TD>
				<TD class= input>
					<input class=readonly readonly name="AppntNo" style="width:100">
				</TD> 
				<TD class= title></TD>
				<TD class= input>
				</TD>
			</tr>
			<tr>
				<TD class= title> 住宅电话 </TD>
				<TD class= input>
					<input class=readonly readonly name="HomePhone">
				</TD>
				<TD class= title> 单位电话 </TD>
				<TD class= input>
					<input class=readonly readonly name="CompanyPhone">
				 </TD> 
				<TD class= title> 手机号码 </TD>
				<TD class= input>
					<input class=readonly readonly name="MobilePhone">
				</TD>
			</tr>
			<tr style="">
				<TD class= title>批单号</TD>
				<TD class= input colspan=5>
					<input class=readonly readonly name="PrtNo" style="width:100">
					<input type="button" Class="cssButton" name="search" value="查询" onclick="queryTaskInfo();" >
				</TD>
			</tr>
		</table>
		<Input type="hidden" class= common name="">
		<!--嵌入显示批单的页面-->
		<IFRAME src="../bq/ShowEdorInfo.jsp?EdorNo=<%=request.getParameter("EdorAcceptNo")%>" width="900" height="400" margwidth="0" margheight="0" scrolling="auto" frameborder="0"></IFRAME>
		<table class=common>
			<tr class=common>
				<TD class= title> 客户未交费原因 </TD>
				<TD class= input colspan=5>
					<input type=radio name="DelayReason" value="0">客户撤销保全申请
					<input type=radio name="DelayReason" value="1">客户遗忘或错过交费时间
					<input type=radio name="DelayReason" value="2">客户认为收费不合理
				</TD>
			</tr>
		</table>
		
		<%@include file="TaskCommonCustomer.jsp"%>
		
		<table class=common>
			<tr class=common>	
				<TD class= input colspan="1">
					<Input type=checkBox name="CheckedFlag" value="1">重设缴费时间
				</TD>
				<TD class= title> 交费方式 </TD>
				<TD class= input>
					<input type=radio id=PayMode1 name=PayMode value="1">现金
					<input type=radio id=PayMode2 name=PayMode value="4">银行转帐
				</TD>
				<TD class= title> 原交费期限 </TD>
				<TD class= input>
					<Input class="readonly" name="OldPayDate" readonly value="">
				</TD>
				<TD class= title> 新交费期限 </TD>
				<TD class= input>
					<Input class="coolDatePicker" name="PayDate" value="" DateFormat="short" verify="新交费期限|date">
				</TD>
			</tr>
			
			<!---注掉内容：多行动态显示
			<tr class=common >	
				<TD class= input colspan="3">
				</TD>
				<TD class= input>
					<input type=radio name=PayMode value="0" onclick="payBank();">银行转帐
				</TD>
				<TD class= title id=hide3 style="display: none"> 转帐日期 </TD>
				<TD class= input id=hide4 style="display: none">
					<Input class="coolDatePicker" name="PayDate" value="" DateFormat="short">
				</TD>
				<td type=title colspan="2"></td>
			</tr>
			<tr class=common >	
				<TD class= title>收费记录号
				</TD>
				<TD class= input colspan="5">
					<input class=readonly readonly name="NoticeNo">
				</TD>
				<td type=title colspan="2"></td>
			</tr>-->
			<tr class=common >	
				<TD class= title>收费记录号
				</TD>
				<TD class= input colspan="6">
					<Input class="readonly" readonly name="GetNoticeNo" value="">
				</TD>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td colspan="6">
					<textarea classs ="common" name="Remark" cols="80%" rows="3"></textarea>
				</td>
			</tr>	
			<tr class=common >	
				<TD class= title colspan=7>
					<input type="button" class=cssButton name="cancel" value="受理撤销" onclick="cancelApp();">
					<input type="button" class=cssButton name="saveHasten" value="保存交费信息" onclick="saveHastenInfo();">
					<input type="button" class=cssButton name="" value="返  回" onclick="goBack();">
					<input type="button" class=cssButton name="create" value="电话未通" onclick="createHistory();">
				</TD>
			</tr>			
		</table>
		<input type=hidden name="EdorAcceptNo" value="">
		<input type=hidden name="fmtransact" value="">
		<div id = "xx">
		</div>
	</Form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>


<script language=javascript>
	
function setPayInfo()
{

}

function queryTaskInfo()
{
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	win = window.open("TaskViewMain.jsp?WorkNo=" + fm.EdorAcceptNo.value + "&CustomerNo=" + fm.AppntNo.value + "&DetailWorkNo=" + fm.EdorAcceptNo, 
					  "ViewWin", 
					  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
	win.focus();
}


function cancelApp()
{
	window.open('TaskBQAppCancelMain.jsp?EdorAcceptNo=' + fm.EdorAcceptNo.value, 'EdorAppCancel');	
}

function afterAppCancel()
{
	fm.fmtransact.value = "CancelApp";
	fm.action = "TaskPhoneHastenSave.jsp?workNo=" + fm.WorkNo.value;
	submitForm();
}

//保存修改后的缴费信息
function saveHastenInfo()
{
    if(!checkData())
    {
        return false;
    }
	if(fm.CheckedFlag.checked == false)
	{
		alert("请选中重设缴费时间选项，并填写新缴费信息。");
		return false;
	}
	
	if((fm.CheckedFlag.checked == true) && (fm.PayDate.value == ""))
	{
		alert("请输入新缴费日期");
		return false;
	}
	
	fm.fmtransact.value = "1";	//SetPayInfo类中，mOperator=1表示收费
	submitForm();
}

function checkData()
{
    if(!isDate(fm.PayDate.value))
    {
        alert("日期录入有误。")
        return false;
    }
    return true;
}

function createHistory()
{
	fm.fmtransact.value = "CreateHistory";
	submitForm();
}

function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.close();
		top.opener.focus();
		top.opener.location.reload();
	}
}

//查询受理件信息
function goBack()
{
	top.close();
	top.opener.top.focus();
}

</script>
</html>
