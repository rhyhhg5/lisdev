//程序名称：TaskBack.js
//程序功能：工单管理工单录入页面
//创建日期：2005-01-20 13:38:50
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mType;

//是否复制选择是
function selCopy()
{
  try
  {
	  document.all("hideInput").style.display = "";
	}
	catch(ex){}
}

//是否复制选择否
function selNotCopy()
{
  try
  {
	  document.all("hideInput").style.display = "none";
  }
	catch(ex){}
}

/* 设置转交类型 */
function setDeliverType(type)
{
	if (type == 0)
	{
		fm2.all("DeliverType").value = "0";  //一般转交
	}
	else
	{
		fm2.all("DeliverType").value = "1";  //审批
	}
}

function beforeSubmit()
{
	//if (!verifyInput2()) 
	//{
	//	return false;
	//}

	var strSql;
	strSql = "select * from LGWorkBox " +
			 "where WorkBoxNo = '" + fm2.all("WorkBoxNo").value + "' ";
	arrResult = easyExecSql(strSql);
	if (arrResult == null)
	{
		alert("找不到该信箱，请确认信箱号输入正确！");
		return false;
	}
	
	strSql = "select WorkBoxNo from LGWorkBox " +
				 "where OwnerNo = '" + operator + "' " +
				 "and   OwnerTypeNo = '2' ";
	arrResult = easyExecSql(strSql);
	if (arrResult)
	{
		var tWorkBoxNo = arrResult[0][0];
		if ((tWorkBoxNo != null) && (tWorkBoxNo == fm2.all("WorkBoxNo").value))
		{
			alert("输入错误，转交信箱不能为本人!");
			return false;
		}
	}
	
	if(fm2.all("CopyFlag")[0].checked == true && fm2.TopTypeNo.value == "")
	{
	    alert("新业务类型不能为空。");
	    return false;
	}
	return true;
}

/* 提交表单 */
function submitForm()
{
	if (beforeSubmit())
	{
	  fm2.WorkNo.value = fm.WorkNo.value;
	  
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm2.submit(); //提交
	}
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		//打印工单
		var newWindow = window.open("TaskGetPrintMain.jsp?workNo=" + fm.AcceptNo.value);
		newWindow.focus();
		
		top.close();
		top.opener.location.reload();
		parent.opener.top.focus();
	}
}      

/* 查询信箱号 */
function queryBoxNo()
{
	showInfo = window.open("./TaskSearchBox.html"); 
}


/* 设置信箱,由信箱查询页面返回时调用 */
function afterQuery(result)
{
	fm2.WorkBoxNo.value = result;
}