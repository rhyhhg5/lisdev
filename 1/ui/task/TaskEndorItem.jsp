<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskEndorItem.jsp
//程序功能：保全项目列表
//创建日期：2005-06-21
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>  

<%@include file="TaskEndorItemInit.jsp"%>
<SCRIPT src="../bq/PEdor.js"></SCRIPT>
<table>
  <tr> 
    <td class= common> 
    	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divEdorItemGrid);"> 
    </td><br>
    <td class= titleImg>保全项目信息</td>
    <td class= common> 
      &nbsp;&nbsp;
    </td>
  </tr>
</table>
<Div  id= "divEdorItemGrid" style= "display: ''" align="center">  
	<table  class= common>
 		<tr  class= common>
	  		<td text-align: left colSpan=1>
				<span id="spanEdorItemGrid" >
				</span> 
	  		</td>
		</tr>
	</table>
	<Div id= "divPage3" align="center" style= "display: 'none' ">
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();	showCodeName();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage(); showCodeName();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage(); showCodeName();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage(); showCodeName();"> 
	</Div>
	<table  class= common>
 		<tr  class= common>
	  		<td text-align: left colSpan=1>
				<Input type =button class=cssButton value="保全项目明细" onclick="showDetailEdorType();"> 
				<Input class=cssButton type=Button value="查看理算结果" onclick="edorAppConfirm()">
				<Input type =button class=cssButton value="查看非保单服务结果" onclick="showOtherPrint()">
				<Input type =button class=cssButton value="查看批单" onclick="showEdor()">
	  		</td>
		</tr>
	</table>		
</Div>
<input type=hidden name=loadFlagForItem value="TASK">
<!--AD 个单-->
<input type=hidden name=EdorType>
<input type=hidden name=ContType>
<input type=hidden name=EdorNo>
<input type=hidden name=EdorAcceptNo>
<input type=hidden name=ContNo>
<!--BC个单-->
<input type=hidden name=CustomerNoBak>
<!--CM 个单-->
<input type=hidden name=ContNoBak>
<input type=hidden name=AppntNo>
<input type=hidden name=GrpContNo value = "">