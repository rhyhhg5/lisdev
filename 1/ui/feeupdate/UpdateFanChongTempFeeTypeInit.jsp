<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UpdateFanChongTempFeeTypeInit.jsp
//程序功能：暂收查询
//创建日期：20170112
//创建人  ：ys
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）


  function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('TempFeeNo').value = '';
  }
  catch(ex)
  {
    alert("在UpdateFanChongTempFeeTypeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在UpdateFanChongTempFeeTypeInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                         

function initForm()
{
  try
  {
    initInpBox();   
    initSelBox();   
    initTempFeeGrid();
   /*  initPolStatuGrid() */
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initTempFeeGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="暂收号";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="暂收类型";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="其他号";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      

      iArray[4]=new Array();
      iArray[4][0]="其他号类型";         		//列名
      iArray[4][1]="30px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[5]=new Array();
      iArray[5][0]="交费方式";         		//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="交费金额";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="财务确认信息";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      TempFeeGrid = new MulLineEnter( "fm" , "TempFeeGrid" ); 
      //这些属性必须在loadMulLine前
      TempFeeGrid.mulLineCount =1;   
      TempFeeGrid.displayTitle = 1;
      TempFeeGrid.locked = 1;
      TempFeeGrid.canSel = 1;
      TempFeeGrid.hiddenPlus = 1;
      TempFeeGrid.hiddenSubtraction = 1;
      TempFeeGrid.canChk = 0;
      TempFeeGrid.selBoxEventFuncName = "TempFeeinit";
      TempFeeGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>