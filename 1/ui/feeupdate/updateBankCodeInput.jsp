<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>";// 记录管理机构
    var comcode = "<%=tGI.ComCode%>";// 记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<title>银联通联互转功能</title>

<!-- 自己的输入验证 js -->
<script src="updateBankCodeInput.js"></script>
<!-- 自己的初始化 mulline -->
<%@include file="updateBankCodeInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="updateBankCodeSave.jsp" method="post" name="fm" target="fraSubmit">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>

		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>银 行 编 码 :</td>
				<td class=input><input class=common name=BankCodeNo /></td>
			</tr>
		</table>
		<INPUT VALUE="查 询" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif" style="cursor: hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>银联信息</td>
			</tr>
		</table>
		
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1>
						<span id="spanBankGrid"></span>
					</td>
				</tr>
			</table>
			<!-- 
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
			 -->
		</div>
		<br />
		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>请选择修改为：</td>
				<td class=input>
				<Input class=codeNo name=afterBankUniteCode CodeData="0|^7705|银联^7706|通联" ondblClick="showCodeListEx('afterBankUniteCode',[this,afterBankUniteName],[0,1]);" onkeyup="showCodeListKeyEx('afterBankUniteCode',[this,afterBankUniteName],[0,1]);">
				<input class=codename name=afterBankUniteName readonly=true>
				</td>
				<td class=title></td>
				<td class=input></td>
				<td class=title></td>
				<td class=input></td>
			</tr>
		</table>
		<INPUT VALUE="修 改" class="cssButton" TYPE=button onclick="updateBankUniteCode();">
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
