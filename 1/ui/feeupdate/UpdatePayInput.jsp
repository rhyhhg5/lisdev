<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：UpdateFanChongTempFeeTypeInit.jsp
	//程序功能：收费流转
	//创建日期：20170412
	//创建人  ：ys
%>
<html>
<%
	//个人下个人

	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>

	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="UpdatePayInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="UpdatePayInit.jsp"%>
<title>付费号查询</title>
</head>
<body onload="initForm();">
	
	<form method=post name=fm target="fraSubmit" action="UpdatePaySave.jsp">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>
		<table class=common align=center>
			<TR class=common>
				<TD class=title>付费号</TD>
				<TD class=input><Input class=common name=ActuGetNo value="">
				</TD>
			</TR>
		</table>
		<INPUT VALUE="查  询" class=CssButton TYPE=button
			onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,div1);"></td>
				<td class=titleImg>付费信息</td>
			</tr>

		</table>
		<Div id="div1" style="display: ''" align=center>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanPayGrid">
					</span></td>

				</tr>

			</table>
			<INPUT VALUE="首  页" class=CssButton TYPE=button
				onclick="getFirstPage();"> <INPUT VALUE="上一页"
				class=CssButton TYPE=button onclick="getPreviousPage();"> <INPUT
				VALUE="下一页" class=CssButton TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class=CssButton TYPE=button
				onclick="getLastPage();">
		</div>

		</tr>
		</table>
		
		<INPUT VALUE="直接修改" class="cssButton" TYPE=button onclick="updatePay();">
		<INPUT VALUE="反冲修改" class="cssButton"TYPE=button onclick="FanChongupdatePay();">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="aActuGetNo" name="aActuGetNo" value="a">
		<input type=hidden id="bManageCom" name="bManageCom" value="b">
		<input type=hidden id="cInsBankCode" name="cInsBankCode" value="">
		<input type=hidden id="dInsBankAccNo" name="dInsBankAccNo" value="">
		<input type=hidden id="eConfDate" name="eConfDate" value="">
		<input type=hidden id="flag" name="flag" value="0">

	</form>
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>  
	
</body>
</html>