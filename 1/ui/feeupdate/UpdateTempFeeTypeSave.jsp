<%@page import="com.sinosoft.lis.feeupdate.UpdateTempFeeTypeUI"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	UpdateTempFeeTypeUI tempFeeTypeUI = new UpdateTempFeeTypeUI();

	String afterTempFeeType = request.getParameter("afterTempFeeType");// 取修改后市场类型的值
	String otherNoType = request.getParameter("afterOtherNoType");// 取修改后市场类型的值
	String[] tGridNo = request.getParameterValues("PolGridNo"); // 得到 MulLine 中序号列的所有值
	String[] tTempFeeNo = request.getParameterValues("PolGrid1"); // 得到第1列的所有值

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();

	// 从 mulline 中取得的是一列的值,所以遍历取出一行的值,然后把参数放入 TransferData 容器?
	for (int i = 0; i < tGridNo.length; i++) {
		transferData.setNameAndValue("tTempFeeNo", tTempFeeNo[i]);
	}
	transferData.setNameAndValue("AfterTempFeeType", afterTempFeeType);
	transferData.setNameAndValue("afterOtherNoType", otherNoType);

	if (!"Fail".equals(FlagStr)) {
		// 准备向后台传输数据 VData
		VData tVData = new VData();// Vector List 实现类之一,支持线程的同步
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tempFeeTypeUI.submitData(tVData, "");// 提交数据和要进行的操作
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = tempFeeTypeUI.mErrors;
			if (!tError.needDealError()) {
				Content = "修改成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>