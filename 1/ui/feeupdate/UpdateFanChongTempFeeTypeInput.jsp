<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：UpdateFanChongTempFeeTypeInit.jsp
	//程序功能：收费流转
	//创建日期：20170412
	//创建人  ：ys
%>
<html>
<%
	//个人下个人

	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>

	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>
	"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="UpdateFanChongTempFeeTypeInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="UpdateFanChongTempFeeTypeInit.jsp"%>
<title>暂收号查询</title>
</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit"
		action="./UpdateFanChongTempFeeTypeChk.jsp">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>
		<table class=common align=center>
			<TR class=common>
				<TD class=title>暂收号</TD>
				<TD class=input><Input class=common name=TempFeeNo value="">
				</TD>
				<td></td>
				<td></td>

			</TR>
		</table>
		<br>
		<INPUT VALUE="查  询" class=CssButton TYPE=button
			onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,div1);"></td>
				<td class=titleImg>暂收信息</td>
			</tr>
		</table>
		<Div id="div1" style="display: ''" align=center>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanTempFeeGrid">
					</span></td>
				</tr>
			</table>
			<INPUT VALUE="首  页" class=CssButton TYPE=button
				onclick="getFirstPage();"> <INPUT VALUE="上一页"
				class=CssButton TYPE=button onclick="getPreviousPage();"> <INPUT
				VALUE="下一页" class=CssButton TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class=CssButton TYPE=button
				onclick="getLastPage();">
		</div>


		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>修改前暂收收据类型</td>
				<td class=input><Input class=readonly readOnly=true
					name=beforeTempFeeType></td>
				<td class=title>修改前其他号类型</td>
				<td class=input><Input class=readonly readOnly=true
					name=beforeOtherNoType></td>
			</tr>
			<tr class=common>
				<td class=title>修改后暂收收据类型</td>
				<td class=input><Input class=codeNo readonly=true
					name=afterTempFeeType verify="暂交费类型|code:TempFeeType"
					ondblclick="return showCodeList('TempFeeType',[this,afterTempFeeTypename],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('TempFeeType',[this,afterTempFeeTypename],[0,1]);"><input
					class=codename name=afterTempFeeTypename readonly=true></td>
					<td class=title>修改后其他号类型</td>
				<td class=input><Input class=codeNo readonly=true
					name=afterOtherNoType verify="暂交费类型|code:TempFeeType"
					ondblclick="return showCodeList('OtherNoType',[this,afterOtherNoTypename],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('OtherNoType',[this,afterOtherNoTypename],[0,1]);"><input
					class=codename name=afterOtherNoTypename readonly=true></td>
			</tr>
		</table>
		<INPUT VALUE="流转冲负修改" class="cssButton" TYPE=button
			onclick="updateTempFeeType();"> <input type=hidden
			id="fmtransact" name="fmtransact"> <input type=hidden
			id="flag" name="flag" value="">
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>