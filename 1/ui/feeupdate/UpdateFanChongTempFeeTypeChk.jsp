<%@page import="com.sinosoft.lis.feeupdate.UpdateFanChongTempFeeTypeUI"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	UpdateFanChongTempFeeTypeUI tempFeeTypeUI = new UpdateFanChongTempFeeTypeUI();

	String afterTempFeeType = request.getParameter("afterTempFeeType");// 
	String afterOtherNoType = request.getParameter("afterOtherNoType");// 
	String[] tTempFeeNo = request.getParameterValues("TempFeeGrid1"); // 得到第1列的所有值
	
	String flag1 = request.getParameter("flag");
	int flag = Integer.parseInt(flag1) - 1;
	//输出参数
	
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();

	transferData.setNameAndValue("tTempFeeNo", tTempFeeNo[flag]);
	transferData.setNameAndValue("AfterTempFeeType", afterTempFeeType);
	transferData.setNameAndValue("AfterOtherNoType", afterOtherNoType);

	if (!"Fail".equals(FlagStr)) {
		// 准备向后台传输数据 VData
		VData tVData = new VData();// Vector List 实现类之一,支持线程的同步
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tempFeeTypeUI.submitData(tVData, "");// 提交数据和要进行的操作
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = tempFeeTypeUI.mErrors;
			if (!tError.needDealError()) {
				Content = "修改成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>