<%@page import="com.sinosoft.lis.feeupdate.UpdateBankCodeUI"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	UpdateBankCodeUI bankCodeUI = new UpdateBankCodeUI();
	
	String[] tGridNo = request.getParameterValues("BankGridNo"); // 得到 MulLine 中序号列的所有值
	String[] tBankCodeNo = request.getParameterValues("BankGrid3"); // 得到第3列银行编码的所有值
	String afterBankUniteCode = request.getParameter("afterBankUniteCode");// 取修改后银联编码的值
	String afterBankUniteName = request.getParameter("afterBankUniteName");// 取修改后银联名称的值
	
	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();

	// 从 mulline 中取得的是一列的值,所以遍历取出一行的值,然后把参数放入 TransferData 容器?
	for (int i = 0; i < tGridNo.length; i++) {
		transferData.setNameAndValue("tBankCodeNo", tBankCodeNo[i]);
	}
	transferData.setNameAndValue("AfterBankUniteCode", afterBankUniteCode);
	transferData.setNameAndValue("AfterBankUniteName", afterBankUniteName);

	if (!"Fail".equals(FlagStr)) {
		// 准备向后台传输数据 VData
		VData tVData = new VData();// Vector List 实现类之一,支持线程的同步
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			bankCodeUI.submitData(tVData, "");// 提交数据和要进行的操作
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = bankCodeUI.mErrors;
			if (!tError.needDealError()) {
				Content = "修改成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		} 
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>