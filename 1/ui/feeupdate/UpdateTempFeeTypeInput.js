var arrDataSet
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
var PolNo;

window.onfocus = myonfocus;

// 查询按钮
function easyQueryClick() {
	initPolGrid();
	if (!verifyInput2())// 检验之后将光标置于错误出，并且颜色变黄
		return false;
	if (fm.TempFeeNo.value == "") {// 至少有一个是不为空的
		alert("请输入暂交费收据号码！");
		return false;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL = " select tf.TempFeeNo, tf.TempFeeType, tf.otherno, tf.othernotype, tfc.paymode, tfc.paymoney, tfc.confmakedate " +
			" from ljtempfee tf, ljtempfeeclass tfc "
			+ " where tf.TempFeeNo = tfc.TempFeeNo "
			+ getWherePart('tf.TempFeeNo', 'TempFeeNo');
	turnPage.pageLineNum = 50;
	turnPage.strQueryResult = easyQueryVer3(strSQL);

	if (!turnPage.strQueryResult) {
		alert("没有查询到要处理的业务数据！");
		return false;
	}

	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	// 设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = PolGrid;
	// 保存SQL语句
	turnPage.strQuerySql = strSQL;
	// 设置查询起始位置
	turnPage.pageIndex = 0;
	// 在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	fm.querySql.value = strSQL;
}

// 初始化查询数据
function Polinit() {
	var i = 0;
	var checkFlag = 0;

	for (i = 0; i < PolGrid.mulLineCount; i++) {
		if (PolGrid.getSelNo(i)) {
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var beforeMarkettype = PolGrid.getRowColData(checkFlag - 1, 2);
		fm.all('beforeTempFeeType').value = beforeMarkettype;
		var beforeOtherNoType = PolGrid.getRowColData(checkFlag - 1, 4);
		fm.all('beforeOtherNoType').value = beforeOtherNoType;
	} else {
		alert("请先选择一条保单信息！");
	}
}

function updateTempFeeType() {
	var mSelNo = PolGrid.getSelNo();// 判断该行的 Radio 单选框被选中,行号是从1开始,如果没有选中行,返回值是0
	if (mSelNo == 0) {
		alert("请选择一条数据！");
		return false;
	}
	var afterTempFeeType = "";
	afterTempFeeType = fm.afterTempFeeType.value;
	if (afterTempFeeType == "") {
		alert("请选择要修改的市场类型！");
		return false;
	}
	var afterOtherNoType = "";
	afterOtherNoType = fm.afterOtherNoType.value;
	if (afterOtherNoType == "") {
		alert("请选择要修改的其它号码类型！");
		return false;
	}
	fm.fmtransact.value = "UPDATE";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showContInfo();
	}
}
