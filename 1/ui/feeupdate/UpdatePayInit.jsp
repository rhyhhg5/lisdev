<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src=" src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
//程序名称：UpdatePayInit.jsp
//程序功能：暂收查询
//创建日期：20170112
//创建人  ：ys
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）


  function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('ActuGetNo').value = '';
  }
  catch(ex)
  {
    alert("在UpdatePayInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在UpdatePayInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                         

function initForm()
{
  try
  {
    initInpBox();   
   // initSelBox();   
    initPayGrid();
   /*  initPolStatuGrid() */
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

var PayGrid; //定义为全局变量，Multiline使用
// 保单信息列表的初始化
function initPayGrid()
  {               

    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="付费号码";         		//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     /*  iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[1][10]=" CodeTest "; //名字最好有唯一性
       iArray[1][11]= "0|^1|86|^2|86000000|^3" ; // 以 ^ 分割每个记录，以 | 分割每个纪录中的字段
 */

      iArray[2]=new Array();
      iArray[2][0]="其它号码";         		//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="付费机构";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    /*  iArray[3][10]=" CodeTest "; //名字最好有唯一性
       iArray[3][11]= "0|^1|^2|^3" ; // 以 ^ 分割每个记录，以 | 分割每个纪录中的字段
 */

      iArray[4]=new Array();
      iArray[4][0]="保险公司帐号开户行";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="保险公司开户帐号";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="银行帐号";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="总给付金额  ";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="财务确认日期 ";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=2;              			//是否允许输入,1表示允许，0表示不允许

      PayGrid = new MulLineEnter( "fm" , "PayGrid" ); 
      //这些属性必须在loadMulLine前
      PayGrid.mulLineCount =0;   
      PayGrid.displayTitle = 1;
      PayGrid.locked = 1;
      PayGrid.canSel = 1;
      PayGrid.hiddenPlus = 1;
      PayGrid.hiddenSubtraction = 1;
      PayGrid.canChk = 0;
      //PayGrid.selBoxEventFuncName = "Payinit"; 
      PayGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>