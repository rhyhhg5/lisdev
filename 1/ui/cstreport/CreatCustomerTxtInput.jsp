<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">
	<%@include file="CreatCustomerTxtInit.jsp"%>
    <script src="CreatCustomerTxtInput.js"></script>
</head>

<body onload="initForm();initElementtype();" >
	<form action="CreatCustomerTxtSave.jsp" method="post" name="fm" target="fraSubmit">
		<div id="divCondCode" style="display:''">
            <table class="common">
            	<tr class="common">
                    <TD class= title>生成上报文件日期</TD>
                	<TD class= input><input class="coolDatePicker" name="CstDate" elementtype=nacessary /></TD>
                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" name = "btnPerCreat" value="生成个人客户数据" onclick="creatTxtData('psn');" />
                    <input class="cssButton" type="button" name = "btnGrpCreat" value="生成机构客户数据" onclick="creatTxtData('grp');" />
                    <input class="cssButton" type="button" name = "btnSumCreat" value="生成汇总数据" onclick="creatTxtData('Summary');" />
                    <input type="hidden" name = "operateType"/>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
