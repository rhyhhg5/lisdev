//程序名称：
//程序功能：
//创建日期：2011-11-01
//创建人  ：GZH
//更新记录:  更新人   更新日期    更新原因/内容 


var showInfo;
window.onfocus = myonfocus;
/**
 * 提交表单
 */
function creatTxtData(wFlag)
{
    if(fm.CstDate.value == "" || fm.CstDate.value ==null){
    	alert("请选择生成上报文件日期！");
    	return false;
    }
    
    var tSql = "select 1 from LCTempCustomerDetail where serno = '0000000000' ";
    var arr=easyExecSql(tSql);
    if(!arr){
    	alert("正在生成待上报数据，请稍后再试！");
    	return false;
    }
    
    fm.btnPerCreat.disabled = true;
    fm.btnGrpCreat.disabled = true;
    fm.btnSumCreat.disabled = true;
    
    fm.operateType.value = wFlag;
    
    var i = 0;
    //var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    //var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    //showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}

/**
 * 导入清单提交后动作。
 */
function afterdownloda(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "false")
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //alert("导入批次失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    fm.btnPerCreat.disabled = false;
    fm.btnGrpCreat.disabled = false;
    fm.btnSumCreat.disabled = false;
}