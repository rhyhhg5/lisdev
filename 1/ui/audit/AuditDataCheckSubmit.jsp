<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：AuditDataCheckSubmit.jsp
	//程序功能：保监会稽核提数数据校验提交处理
	//创建日期：2009-06-18
	//创建人  : Qisl
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.audit.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	boolean flag = true;
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	if (tG == null)
	{
		out.println("session has expired");
		return;
	}

	//接收信息
	TransferData tTransferData = new TransferData();
	String tStartDate = request.getParameter("StartDate");
	String tEndDate = request.getParameter("EndDate");
	String tManageCom = request.getParameter("ManageCom");

	if (tManageCom == "" || tManageCom.equals(""))
	{
		tManageCom = "86";
	}

	System.out.println("机构:" + tManageCom);

	if (tStartDate == "" || tEndDate == "")
	{
		Content = "请录入日期信息!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{
		if (tStartDate != null && tEndDate != null)
		{
			tTransferData.setNameAndValue("StartDate", tStartDate);
			tTransferData.setNameAndValue("EndDate", tEndDate);
			tTransferData.setNameAndValue("ManageCom", tManageCom);
		}
		else
		{
			Content = "传输数据失败!";
			flag = false;
		}
	}

	System.out.println("StartDate:" + tStartDate);
	System.out.println("EndDate:" + tEndDate);
	System.out.println("ManageCom:" + tManageCom);

	try
	{
		if (flag == true)
		{
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add(tTransferData);
			tVData.add(tG);

			// 数据传输
			AuditDataCheckBL tAuditDataCheckBL = new AuditDataCheckBL();
			System.out.println("before AuditDataCheckBL!!!!");
			if (!tAuditDataCheckBL.submitData(tVData, ""))
			{
				int n = tAuditDataCheckBL.mErrors.getErrorCount();
				for (int i = 0; i < n; i++){
					System.out.println("Error: " + tAuditDataCheckBL.mErrors.getError(i).errorMessage);
				}
				Content = "  保监会稽核数据校验失败，原因是: " + tAuditDataCheckBL.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			//如果在Catch中发现异常，则不从错误类中提取错误信息
			if (FlagStr == "Fail")
			{
				tError = tAuditDataCheckBL.mErrors;
				if (!tError.needDealError()){
					Content = " 保监会稽核数据校验成功! ";
					FlagStr = "Succ";
				}
				else{
					FlagStr = "Fail";
				}
			}
		}
	}
	catch (Exception e)
	{
		e.printStackTrace();
		Content = Content.trim() + "提示：异常终止!";
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
