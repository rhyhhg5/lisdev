<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：AuditDataCheckInit.jsp
	//程序功能：保监会稽核数据质量检查界面初始化
	//创建日期：2009-06-18
	//创建人  : Qisl
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
  }
  catch(ex)
  {
    alert("在AuditDataCheckInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {  
  }
  catch(ex)
  {
    alert("在AuditDataCheckInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
	initCheckResultGrid();
  }
  catch(re)
  {
    alert("AuditDataCheckInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initCheckResultGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		  //列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		

		iArray[1]=new Array();  
		iArray[1][0]="校验结果";         		  //列名
		iArray[1][1]="200px";            		  //列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许		
	    
		iArray[2]=new Array();
		iArray[2][0]="错误记录数";          	      //列名
		iArray[2][1]="40px";            		  //列宽
		iArray[2][2]=100;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

      CheckResultGrid = new MulLineEnter( "fm" , "CheckResultGrid" ); 
      //这些属性必须在loadMulLine前
      CheckResultGrid.mulLineCount =1;   
      CheckResultGrid.displayTitle = 1;
      CheckResultGrid.locked=1;
      CheckResultGrid.hiddenPlus = 1;
      CheckResultGrid.hiddenSubtraction=1;
      CheckResultGrid.canChk =0; // 1为显示CheckBox列，0为不显示 (缺省值)
      CheckResultGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
