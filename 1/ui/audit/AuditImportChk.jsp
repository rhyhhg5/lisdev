<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ReportCWImportChk.jsp
//程序功能：保监会报表财务数据导入
//创建日期：2002-06-19 11:10:36
//创建人  :ck
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
   <%@page import="com.sinosoft.audit.*"%>

   <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.workflowengine.*"%>
      <%@page import="com.sinosoft.lis.db.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
  
  	//接收信息
    //接收信息
  	TransferData tTransferData = new TransferData();
    String tStartDate = request.getParameter("StartDate");
	String tEndDate = request.getParameter("EndDate");
	
	String tManageCom=request.getParameter("ManageCom");
	if(tManageCom==""||tManageCom.equals(""))
	{
		tManageCom="86";	
	}
	System.out.println("机构:"+tManageCom);
    String tItemCode="";
    String tItemNum="";
    String tItemType="";
	tItemType = request.getParameter("fmItemType");


	if (tStartDate==""||tEndDate=="")
	{
		Content = "请录入日期信息!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tStartDate != null && tEndDate != null  )
	      {
            tTransferData.setNameAndValue("StartDate",tStartDate);
	        tTransferData.setNameAndValue("EndDate",tEndDate);
	        tTransferData.setNameAndValue("ManageCom",tManageCom);
	        tTransferData.setNameAndValue("NeedItemKey","0"); 	      	  
	      }else
		{
			Content = "传输数据失败!";
			flag = false;
		}
	}
	
	//较验输入的日期是否正确
//	
	
	String tSql="";
   	String tGrid1[] = request.getParameterValues("PolGrid1"); //得到第1列的所有值
   	String tChk[] = request.getParameterValues("InpPolGridChk"); //参数格式=" Inp+MulLine对象名+Chk"  
    int checkCount=0;
 
    for(int index=0;index<tChk.length;index++)
    {
          if(tChk[index].equals("1"))
          {           
            System.out.println( "该行被选中");
            tSql+=",'"+tGrid1[index].substring(0,2)+"'";
            checkCount++;
            System.out.println(tSql);
          }
          if(tChk[index].equals("0"))   
          {   
            System.out.println("该行未被选中");
          }
    }  
                                                   
    System.out.println("选中的的记录数:"+checkCount); 
	System.out.println(tSql);  
	
	String tOperate=tItemCode+" ||"+tItemNum+"  ||"+tItemType; 
	System.out.println("tOperate"+ tOperate);

	System.out.println("StartDate:" + tStartDate );  
	System.out.println("EndDate:" + tEndDate );  
	System.out.println("ManageCom:" + tManageCom );  
	System.out.println("NeedItemKey:" + "0" );  
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		AuditEngineUI tAuditEngineUI= new AuditEngineUI();
  			System.out.println("before AuditEngineUI!!!!");			
		 if (!tAuditEngineUI.submitData(tVData,tOperate))//执行财务数据导入工作流节点0000000204
		{
			int n = tAuditEngineUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tAuditEngineUI.mErrors.getError(i).errorMessage);
			Content = "  保监会稽核数据导入失败，原因是: " + tAuditEngineUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tAuditEngineUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 保监会稽核数据导入成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}

	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
} 
%>                     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
