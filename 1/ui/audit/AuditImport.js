//程序名称：ReportCWImport.js
//程序功能：保监会报表财务数据导入
//创建日期：2004-07-08 11:10:36
//创建人  :ck
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
turnPage.pageLineNum+=7;
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}


//初始化mulline中的内容
function initContent(ItemClass)
{
  try
  {
	var vSQL = "select distinct itemtype, case itemtype when 'A01' then '新保单信息表' when 'A02' then '缴费信息表' when 'A03' then '批单信息表' when 'A04' then '给付信息表' when 'A05' then '报案信息表' "
	+" when 'A06' then '赔案信息表' when 'A07' then '险种代码表' when 'A08' then '营销员信息表' when 'A09' then '中介机构信息表' when 'A10' then '分支机构信息表' when 'A11' then '员工信息表' when 'A12' then '银保专管员信息表' when 'A13' then '财务总账科目代码表' "
	+" when 'A14' then '财务明细科目代码表' when 'A15' then '财务凭证信息表' when 'A16' then '单证代码表' when 'A17' then '单证信息表' end,'',desttablename "
	+"  from LFDesbMode where Dealtype='S' AND ItemType like '"+ItemClass+"%%' order by itemtype ";
	turnPage.queryModal(vSQL, PolGrid); 

  }
  catch(re)
  {
    alert("ReportEngine.js-->InitContent函数中发生异常:初始化界面错误!");
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterDelSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);     
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    alert(content);

   //执行下一步操作
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function autochk()
{
  
	if (confirm("是否需要先做数据删除？已经做了或不需要做点选【确定】，如果需要做数据删除操作，点选【取消】"))
	{
		var  tSql="";
		var  tCount = 0;
		var rowNum=PolGrid.mulLineCount;   
		var today=new Date();
		var month=today.getMonth()+1.0;
		var curTime=today.getYear()+"-"+month+"-"+today.getDate()+" "+today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();  
		var curTime1=today.getHours()+":"+today.getMinutes()+":"+today.getSeconds(); 
		
		for(i=0;i<rowNum;i++)
		{                                                                   
			if(PolGrid.getChkNo(i))
			{
				PolGrid.setRowColData(i,3,curTime);
				tCount = i+1;
				//tSql+=",'A" + tCount + "'";//列表展现和lfdesbmode表的itemtype的顺序一致
				tSql+=",'"+PolGrid.getRowColData(i,1)+"'";
			}
		}
		
		//if(curTime1 >= '07:00:00' && curTime1 <= '17:00:00')
		//{
			//alert("上午7点到下午17点之间不允许进行保监会稽核系统数据导入");
			//return ;
		//}
   
		if(fm.all('StartDate').value==''||fm.all('EndDate').value=='')
		{
			alert("请输入完整的日期！");
			return ;
		}                                                   
    
		if(fm.all('ManageCom').value=='')
		{
			alert("请输入提数机构！");
			return ;
		} 
    
		if (tCount==0)
		{
			flag = false;
			alert("数据提交不完整，必须至少选中一列提数类型！");
			return ;
		} 

		var tItemType=" AND ItemType In ('&'"+tSql+")"; 
		//隐藏列
		fm.all('fmItemType').value= tItemType;

		var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action = "AuditImportChk.jsp"
		fm.submit(); //提交  
	}
	else
	{
		alert("您取消了导入操作！");
	}
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function easyQueryAddClick(parm1,parm2)
{
	
	if(fm.all(parm1).all('InpPolGridSel').value == '1' )
	{
	//当前行第1列的值设为：选中
   		fm.PrtNoHide.value =  fm.all(parm1).all('PolGrid2').value;
  	}
} 

function deleteData()
{
  var  tTableName="";
  var rowNum=PolGrid.mulLineCount;
  var  tCount = 0;

  for(i=0;i<rowNum;i++)
   {                                                                   
     if(PolGrid.getChkNo(i))
     {
     	  tCount = i+1;
          tTableName+=","+PolGrid.getRowColData(i,4);
      }
   }
   
   if (tCount==0)
	{
		flag = false;
		alert("数据提交不完整，必须至少选中一列提数类型！");
		return ;
	} 
  
  fm.all('fmTableName').value= tTableName;
  fm.action = "deleteDataSubmit.jsp";
  var i = 0;
  var showStr="正在重提数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
  fm.submit();
}
function getImportPath ()
{
		// 书写SQL语句
	var strSQL = "";

	strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	alert("未找到上传路径");
    return;
	}
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  ImportPath = turnPage.arrDataCacheSet[0][0];
}
function uploadData()
{

  var i = 0;
  //getImportPath(); 
  var showStr="正在提取数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
  //ImportFile = fm.all('FileName').value;
  //parent.fraInterface.fm.action = "./UploadDataSave.jsp?ImportPath="+ImportPath;
 // fm.action = "UploadDataSave.jsp";
  fmup.submit(); //提交
}
