<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：保监会稽核数据导入到SQLSERSER
//创建日期：2009-02-17
//创建人  :sq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.audit.*"%>

  <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="java.io.ByteArrayOutputStream"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  
  System.out.println("start...........");
//	System.out.println("NeedItemKey:" + "0" );  //没有扩充元素
try
{

    //String strRealPath = application.getRealPath("/").replace('\\','/');
    String strRealPath = application.getRealPath("/");
    
    System.out.println(strRealPath);
    String temp=strRealPath.substring(strRealPath.length()-1,strRealPath.length());
    if(!temp.equals("/"))
		{
		    strRealPath=strRealPath+"/"; 
		}
	
	//if(!temp.equals("\\"))
		//{
		//    strRealPath=strRealPath+"\\"; 
		//}
			
    System.out.println("strRealPath=" + strRealPath);

    // 准备传输数据 VData
    TransferData tTransferData = new TransferData();
    VData tVData = new VData();

  	if (flag == true)
  	{
		
		// 数据传输
  		AuditTransfBL tAuditTransfBL= new AuditTransfBL();
  		tTransferData.setNameAndValue("strRealPath", strRealPath);
  		tVData.add(tTransferData);
  		System.out.println("before AuditTransfBL!!!!");			
		 if (!tAuditTransfBL.submitData(tVData,null))
		{
			int n = tAuditTransfBL.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tAuditTransfBL.mErrors.getError(i).errorMessage);
			Content = "  保监会稽核数据导入到SQLSERVER失败，原因是: " + tAuditTransfBL.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tAuditTransfBL.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 保监会稽核数据导入到SQLSERVER成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}

	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
} 
%>                     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
