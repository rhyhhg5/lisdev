<%
//程序名称：LCRecoilInit.jsp
//程序功能：共保抽档
//创建日期：2008-10-29 16:18:29
//创建人  ：fuxin
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">

function initForm()
{

  try
  {
  	fm.all('ManageCom').value = managecom;
    initLCISPayGetGrid();
   //SearchLCISPayGet();
  }
  catch(re)
  {
    alter("在GrpContInvaliedate.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initLCISPayGetGrid()
{

   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    
    iArray[1]=new Array();
    iArray[1][0]="任务号";         		//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
    iArray[1][21]= "BatchNoQ" ;
    
    iArray[2]=new Array("共保公司","120px","0",0);
    iArray[3]=new Array("业务类型","80px","0",0);
    iArray[4]=new Array("抽档金额","80px","0",0);
    //iArray[5]=new Array("反冲金额","80px","0",0);
    
    iArray[5]=new Array();
    iArray[5][0]="反冲金额";         		//列名
    iArray[5][1]="70px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
    iArray[5][21]= "Claimmoney" ;
    
    iArray[6]=new Array();
    iArray[6][0]="类型";         		//列名
    iArray[6][1]="70px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许   
    iArray[6][21]= "Type" ;
    
    iArray[7]=new Array("状态","60px","0",0);
    
    LCISPayGetGrid = new MulLineEnter( "fm" , "LCISPayGetGrid" );
    LCISPayGetGrid.mulLineCount = 0;
    LCISPayGetGrid.displayTitle = 1;
    LCISPayGetGrid.locked = 1;
    LCISPayGetGrid.canChk = 0;
    LCISPayGetGrid.canSel = 1;
    LCISPayGetGrid.hiddenPlus=1;  
    LCISPayGetGrid.hiddenSubtraction=1; 
    LCISPayGetGrid.loadMulLine(iArray);  
    LCISPayGetGrid.selBoxEventFuncName ="getpoldetail";  
  } 
  catch(ex)
  {
    alert(ex);
  }    
}


 </script>