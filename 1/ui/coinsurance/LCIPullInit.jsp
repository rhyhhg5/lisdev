<%
//程序名称：LCIPullInit.jsp
//程序功能：共保抽档
//创建日期：2008-10-24
//创建人  ：MN
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">

function initForm()
{

  try
  {
    initLCISPayGetGrid();
  }
  catch(re)
  {
    alter("在GrpContInvaliedate.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initLCISPayGetGrid()
{

   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("抽档号","80px","0",0);
    iArray[2]=new Array("共保公司","60px","0",0);
    iArray[3]=new Array("共保公司名称","80px","0",0);
    iArray[4]=new Array("保单号","80px","0",0);
    iArray[5]=new Array("业务类型","50px","0",0);
    iArray[6]=new Array("抽档金额","60px","0",0);
    iArray[7]=new Array("从共保方金额","60px","0",0);
    iArray[8]=new Array("抽档状态","40px","0",0);
    iArray[9]=new Array("抽档类型","0px","0",3);
    
    LCISPayGetGrid = new MulLineEnter( "fm" , "LCISPayGetGrid" );
    LCISPayGetGrid.mulLineCount =7;
    LCISPayGetGrid.displayTitle = 1;
    LCISPayGetGrid.locked = 1;
    LCISPayGetGrid.canChk = 0;
    LCISPayGetGrid.canSel = 1;
    LCISPayGetGrid.hiddenPlus=1;  
    LCISPayGetGrid.hiddenSubtraction=1; 
    LCISPayGetGrid.loadMulLine(iArray);
  } 
  catch(ex)
  {
    alert(ex);
  }    
}


 </script>