<%
//程序名称：LCIPullSave.jsp
//程序功能：共保抽档
//创建日期：2008-10-24
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.coinsurance.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LCISPayGetSchema tLCISPayGetSchema = new LCISPayGetSchema();
  LCISPayGetSet tLCISPayGetSet = new LCISPayGetSet();
	
  LCISPullUI tLCISPullUI = new LCISPullUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String operator=tG.Operator;
  
  String tempAgentCom = request.getParameter("AgentCom");
  String tSql = "insert into ldcode values('lcipull','"+tempAgentCom+"',null,null,null,null)";
  boolean flag = new ExeSQL().execUpdateSQL(tSql);
  
  if(flag){
  
  	if ("INSERT".equals(tOperate)){
       String tOtherNoType = request.getParameter("OtherNoType");
       String tAgentCom = request.getParameter("AgentCom");
       String tEndDate = request.getParameter("EndDate");
       LCISPayGetSchema aLCISPayGetSchema = new LCISPayGetSchema();
       aLCISPayGetSchema.setOtherNoType(tOtherNoType);
       aLCISPayGetSchema.setAgentCom(tAgentCom);
       //抽档终止日期
       aLCISPayGetSchema.setMakeDate(tEndDate);
       tLCISPayGetSet.add(aLCISPayGetSchema);
  	}
  	if ("CONFIRM".equals(tOperate)){
     String tRadio[] = request.getParameterValues("InpLCISPayGetGridSel");
     String tGrid1[] = request.getParameterValues("LCISPayGetGrid1");
     for (int index=0; index< tRadio.length;index++){
        if(tRadio[index].equals("1")){
           LCISPayGetSchema aLCISPayGetSchema = new LCISPayGetSchema();
           aLCISPayGetSchema.setBatchNo(tGrid1[index]);
           tLCISPayGetSet.add(aLCISPayGetSchema);
        }
     }
  	}

  	VData tVData = new VData();
  	FlagStr="";
  	tVData.addElement(tG);
  	tVData.addElement(tLCISPayGetSet);
     
  	try {
     tLCISPullUI.submitData(tVData,tOperate);
  	} catch (Exception ex) {
     Content = "操作失败，原因是:" + ex.toString();
     System.out.println(Content);
     FlagStr = "Fail";
  	}

  
  	if (!FlagStr.equals("Fail"))
  	{
    	tError = tLCISPullUI.mErrors;
    	if (!tError.needDealError())
    	{                          
    		Content = " 操作成功! ";
    		System.out.println(Content);
    		FlagStr = "Succ";
    	}
    	else                                                                           
    	{
    		Content = " 操作失败，原因是:" + tError.getFirstError();
    		//Content = " 保存失败，请检查数据是否正确";
    		FlagStr = "Fail";
    	}
  	}
  
  	//添加各种预处理

  	String tDelSql = "delete from ldcode where codetype = 'lcipull' and code = '"+tempAgentCom+"'";
  	new ExeSQL().execUpdateSQL(tDelSql);
  
  }else{
	  //System.out.println("该共保公司正在抽挡，不能再次抽挡");
	  Content = "该共保公司正在抽挡，不能再次抽挡";
	  FlagStr = "Fail";
  }
  
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

