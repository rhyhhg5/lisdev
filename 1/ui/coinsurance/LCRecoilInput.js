
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var tSaveFlag = "0";
var mDebug="1";
var mAction = "";
var tSaveType="";

var turnPage = new turnPageClass(); 


function Confirm()
{	  
		if (LCISPayGetGrid.mulLineCount == 0)
		{
			alert("没有可反冲的信息！");
			return false;
		}
   	if (fm.all('State').value == 2 )
		{
			alert("已反冲的记录，不能执行该操作！");
			return false ;
		}
		if (fm.all("PayMode").value ==0 || fm.all("PayMode").value == null || fm.all("PayMode").value =="")
		{
			alert("请录入收付费方式！");
			return false ;
		}
	  if (verifyInput() == false)
	  {
       return false;
    }
    fm.all("operate").value='CONFIRM';
    var showStr="正在保存数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   
	  fm.submit();
}

function easyQueryClick()
{
	var SQL =    " select batchno,(select name from lacom where a.agentcom=agentcom),'投保',sum(sumactumoney),sum(claimmoney),'TB' "
						 + ",(case when state = '1' then '待确认'  when state='2' then '已反冲' else '未抽档确认'  end) "
             + " from lcispayget a where 1=1 and othernotype='TB' "
						 + getWherePart( 'batchno', 'Batchno' )
						 + getWherePart( 'State', 'State' )   
						 + getWherePart( 'makedate', 'EndDate' ) 
 			 + "and  managecom like '" + managecom + "%'"  
 			 + "and  managecom like '" + fm.ManageCom.value + "%'"     
             + " group by batchno,agentcom ,state "
             + " union "
 						 + " select batchno,(select name from lacom where b.agentcom=agentcom),'续期',sum(sumactumoney),sum(claimmoney),'XQ' "
						 + ",(case when state = '1' then '待确认'  when state='2' then '已反冲' else '未抽档确认'  end) "
             + " from lcispayget b where 1=1 and othernotype='XQ' "
						 + getWherePart( 'batchno', 'Batchno' )
						 + getWherePart( 'State', 'State' )    
						 + getWherePart( 'makedate', 'EndDate' )  
 			 + "and  managecom like '" + managecom + "%'"  
 			 + "and  managecom like '" + fm.ManageCom.value + "%'"           
             + " group by batchno,agentcom ,state "
             + " union "
 						 + " select batchno,(select name from lacom where c.agentcom=agentcom),'保全',sum(sumactumoney),sum(claimmoney),'BQ' "
						 + ",(case when state = '1' then '待确认'  when state='2' then '已反冲' else '未抽档确认'  end) "
             + " from lcispayget c where 1=1 and othernotype='BQ' "
						 + getWherePart( 'batchno', 'Batchno' )
						 + getWherePart( 'State', 'State' )       
						 + getWherePart( 'makedate', 'EndDate' )
 			 + "and  managecom like '" + managecom + "%'"  
 			 + "and  managecom like '" + fm.ManageCom.value + "%'"     
             + " group by batchno,agentcom ,state "
             + " union "
				 		 + " select batchno,(select name from lacom where d.agentcom=agentcom),'理赔',sum(sumactumoney),sum(claimmoney),'LP' "
						 + ",(case when state = '1' then '待确认'  when state='2' then '已反冲' else '未抽档确认'  end) "
             + " from lcispayget d where 1=1  "
             + " and othernotype in ('5','F','C') "
						 + getWherePart( 'batchno', 'Batchno' )
						 + getWherePart( 'State', 'State' )       
						 + getWherePart( 'makedate', 'EndDate' )
 			 + "and  managecom like '" + managecom + "%'"  
 			 + "and  managecom like '" + fm.ManageCom.value + "%'"   
             + " group by batchno,agentcom ,state "
             ;
	turnPage.queryModal(SQL,LCISPayGetGrid);
	if (LCISPayGetGrid.mulLineCount == 0)
	{
		alert("没有可反冲的信息！");
		return false;
	}
}


function afterSubmit(FlagStr, content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
  
}

//获得选中行信息
function getpoldetail()
{ 
  var tRow = LCISPayGetGrid.getSelNo() - 1;	
	fm.all("Claimmoney").value =  LCISPayGetGrid.getRowColDataByName(tRow, "Claimmoney");
	fm.all("Type").value = LCISPayGetGrid.getRowColDataByName(tRow, "Type");
	fm.all("BatchNo1").value = LCISPayGetGrid.getRowColDataByName(tRow, "BatchNoQ");
}
function CIPrint()
{
	var SelNo = LCISPayGetGrid.getSelNo()
    if (SelNo==0)
    {
	  	window.alert("需要操作数据!");
	   	return  false;
    }
	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    //fm.action = "LCRecoilReport.jsp";
    fm.action = "NewLCRecoilReport.jsp";
    fm.target = "LCRecoiReport";
	fm.submit();
	showInfo.close();
	fm.target = "fraSubmit";
}
