<%
//程序名称：LCRecoilSave.jsp
//程序功能：共保抽档
//创建日期：2008-10-29 16:18:22
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.coinsurance.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LCRecoilUI tLCRecoilUI = new LCRecoilUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operate");
  String tBatchNo = request.getParameter("BatchNo1"); 
  String tClaimmoney = request.getParameter("Claimmoney");
  String tType = request.getParameter("Type"); 
  String tPayMode = request.getParameter("PayMode");
  String tBankCode = request.getParameter("BankCode");
  String tBankAccNo = request.getParameter("BankAccNo");
  
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";
  System.out.println("xxxxxxxxxxxxxxxxx++"+tBatchNo);
  System.out.println("xxxxxxxxxxxxxxxxx++"+tClaimmoney);
	System.out.println("xxxxxxxxxxxxxxxxxx:"+tPayMode);
	System.out.println("xxxxxxxxxxxxxxxxx++"+tType);
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String operator=tG.Operator;
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("Claimmoney", tClaimmoney);
	tTransferData.setNameAndValue("Type", tType);
	tTransferData.setNameAndValue("BatchNo",tBatchNo);
	tTransferData.setNameAndValue("PayMode",tPayMode);
	tTransferData.setNameAndValue("BankCode",tBankCode);
	tTransferData.setNameAndValue("BankAccNo",tBankAccNo);


  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tG);
  tVData.add(tTransferData);
     
  try {
     tLCRecoilUI.submitData(tVData,tOperate);
  } catch (Exception ex) {
     Content = "操作失败，原因是:" + ex.toString();
     System.out.println(Content);
     FlagStr = "Fail";
  }

  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLCRecoilUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	//Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

