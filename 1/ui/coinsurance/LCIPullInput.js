
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var tSaveFlag = "0";
var mDebug="1";
var mAction = "";
var tSaveType="";

var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
function Confirm()
{
    var SelNo = LCISPayGetGrid.getSelNo()
    if (SelNo==0){
    	alert("请选择结算数据");
    	return  false;
    }
    /**
     * 在抽档的时候容易并发出现重复数据 责任王冬冬
     * 在这里我们添加一个校验，对于抽档确认选中的数据进行判断，如果有重复数据存在就加上校验，让其重复抽档！
     * 不要连续点击 
     **/
    
    //获取我们需要的值
    var newSelNo = SelNo-1;
    var checkGrpcontno = LCISPayGetGrid.getRowColData(newSelNo,4);
    
    //alert("选中的列号："+SelNo+"\r\n"+"选中的保单号："+checkGrpcontno);
    var checkSql = "select 1 from lcispayget a, lcispayget b"+
    				" where a.grpcontno = '"+checkGrpcontno+"'"+
    				" and a.grpcontno = b.grpcontno "+
    				" and a.agentcom = b.agentcom "+
    				" and a.otherno = b.otherno "+
    				" and a.grppolno = b.grppolno "+
    				" and a.othernotype = b.othernotype "+
    				" and a.state = '0' "+
    				" and b.state = '0' "+
    				" and a.batchno <> b.batchno "+
    				" and a.riskcode = b.riskcode with ur ";
      
    var checkArr = easyExecSql(checkSql);
    //alert(checkArr);
    if(checkArr){
    	alert("共保抽档有重复数据存在，请重新抽档后确认！");
    	return false;
    }
   
    fm.all("operate").value='CONFIRM';
    var showStr="正在保存数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

 	fm.action="./LCIPullSave.jsp";
	fm.submit();
}
function SearchLCISPayGet(){
  if(!verifyInput()) return false;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句
  var strSql;
  if (fm.OtherNoType.value!=null&&fm.OtherNoType.value!="") {
  	strSql = "select a.batchno,a.agentcom,(select name from lacom where agentcom=a.agentcom),a.grpcontno,'"+fm.TypeName.value+"',sum(a.sumactumoney),sum(a.claimmoney),(case when a.state='1' then '已确认' else '待确认' end ),'"+fm.OtherNoType.value+"' "
             + " from lcispayget a where a.state in ('0') and a.othernotype in ('"+fm.SearchType.value+"') "+getWherePart("a.agentcom","AgentCom")+" group by a.batchno,a.agentcom,a.grpcontno,a.state ";
  } else {
  	strSql = " select a.batchno,a.agentcom,(select name from lacom where agentcom=a.agentcom),a.grpcontno,'新单',sum(a.sumactumoney),sum(a.claimmoney),(case when a.state='1' then '已确认' else '待确认' end ),'01' "
             + " from lcispayget a where a.state in ('0') and a.othernotype='TB' "+getWherePart("a.agentcom","AgentCom")+" group by a.batchno,a.agentcom,a.grpcontno,a.state "
             + " union "
             + " select a.batchno,a.agentcom,(select name from lacom where agentcom=a.agentcom),a.grpcontno,'续期',sum(a.sumactumoney),sum(a.claimmoney),(case when a.state='1' then '已确认' else '待确认' end ),'02' "
             + " from lcispayget a where a.state in ('0') and a.othernotype='XQ' "+getWherePart("a.agentcom","AgentCom")+" group by a.batchno,a.agentcom,a.grpcontno,a.state "
             + " union "
             + " select a.batchno,a.agentcom,(select name from lacom where agentcom=a.agentcom),a.grpcontno,'保全',sum(a.sumactumoney),sum(a.claimmoney),(case when a.state='1' then '已确认' else '待确认' end ),'03' "
             + " from lcispayget a where a.state in ('0') and a.othernotype='BQ' "+getWherePart("a.agentcom","AgentCom")+" group by a.batchno,a.agentcom,a.grpcontno,a.state "
             + " union "
             + " select a.batchno,a.agentcom,(select name from lacom where agentcom=a.agentcom),a.grpcontno,'理赔赔款',sum(a.sumactumoney),sum(a.claimmoney),(case when a.state='1' then '已确认' else '待确认' end ),'04' "
             + " from lcispayget a where a.state in ('0') and a.othernotype ='5' "+getWherePart("a.agentcom","AgentCom")+" group by a.batchno,a.agentcom,a.grpcontno,a.state "
             + " union "
             + " select a.batchno,a.agentcom,(select name from lacom where agentcom=a.agentcom),a.grpcontno,'理赔调查费',sum(a.sumactumoney),sum(a.claimmoney),(case when a.state='1' then '已确认' else '待确认' end ),'04' "
             + " from lcispayget a where a.state in ('0') and a.othernotype in ('F','C') "+getWherePart("a.agentcom","AgentCom")+" group by a.batchno,a.agentcom,a.grpcontno,a.state with ur ";
  }
  turnPage1.queryModal(strSql,LCISPayGetGrid);
  showInfo.close();
  gatherAmount();

}
function PullGrpCont()
{
	  if (verifyInput() == false) {
	  	return false;
	  }
	  var tSql = "select 1 from ldcode ldc  " 
		  	   + "where ldc.codetype = 'lcipull' and ldc.code = '"+fm.AgentCom.value+"' " ;
	  var arrResult = easyExecSql(tSql);
	  if(arrResult){
		  alert("该共保公司正在抽挡，不能再次抽挡");
		  return false;
	  }
		  
    fm.all('operate').value = 'INSERT';
    var showStr="正在抽取数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

 	fm.action="./LCIPullSave.jsp";
	fm.submit();
}

function afterSubmit(FlagStr, content)
{
  showInfo.close();
  SearchLCISPayGet();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
  
}

function CIPrint(){
	var SelNo = LCISPayGetGrid.getSelNo()
  if (SelNo==0){
  	alert("请选择结算数据");
   	return  false;
  }

	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "LCIPullReport.jsp";
   fm.target = "LCIPullPeport";
	fm.submit();
	showInfo.close();
	fm.target = "fraSubmit";
}

function SearchStarDate(){
	var tType = fm.OtherNoType.value;
	if (tType=="04") {
		tType="5";
	} else if (tType=="05") {
		tType="C','F";
	} else if (tType=="01") {
		tType="TB";
	} else if (tType=="02") {
		tType="XQ";
	} else if (tType=="03") {
		tType="BQ";
	}
	fm.SearchType.value=tType;

	var strSQL = "select max(makedate) from lcispayget where state='1' and agentcom='"+fm.AgentCom.value+"' and OtherNoType in ('"+tType+"') ";
	var arrResult = easyExecSql(strSQL);
	fm.StartDate.value = arrResult[0][0];
}

function afterCodeSelect( cCodeName, Field ){
		if((cCodeName == "OtherNoType"||cCodeName == "coinsurancecom")&&fm.AgentCom.value!=null&&fm.OtherNoType.value!=null )
		{
			SearchStarDate();
		}
}

function gatherAmount(){
	
	var strSql;
	
	if (fm.OtherNoType.value!=null&&fm.OtherNoType.value!="") {
		strSql = "select sum(a.claimmoney) "
        + " from lcispayget a where a.state in ('0') and a.othernotype in ('"+fm.SearchType.value+"') "+getWherePart("a.agentcom","AgentCom");
	  } else {
	  	strSql = " select sum(sub.cmoney) from " 
	  		+ "(select sum(a.claimmoney) cmoney"
            + " from lcispayget a where a.state in ('0') and a.othernotype='TB' "+getWherePart("a.agentcom","AgentCom")
            + " union all"
            + " select sum(a.claimmoney) cmoney"
            + " from lcispayget a where a.state in ('0') and a.othernotype='XQ' "+getWherePart("a.agentcom","AgentCom")
            + " union all"
            + " select sum(a.claimmoney) cmoney"
            + " from lcispayget a where a.state in ('0') and a.othernotype='BQ' "+getWherePart("a.agentcom","AgentCom")
            + " union all"
            + " select sum(a.claimmoney) cmoney"
            + " from lcispayget a where a.state in ('0') and a.othernotype ='5' "+getWherePart("a.agentcom","AgentCom")
            + " union all"
            + " select sum(a.claimmoney) cmoney"
            + " from lcispayget a where a.state in ('0') and a.othernotype in ('F','C') "+getWherePart("a.agentcom","AgentCom")+" ) sub with ur";
	  }
	
	var arrResult = easyExecSql(strSql);
	if(arrResult){
		fm.TotalAmount.value = arrResult[0][0];
	}
}

