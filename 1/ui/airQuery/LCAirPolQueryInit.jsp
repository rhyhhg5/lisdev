<%
//程序名称：LCAirPolQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-04-16 13:56:09
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('Bdate').value = "";
    fm.all('Edate').value = "";
  }
  catch(ex) {
    alert("在LCAirPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLCAirPolGrid();  
  }
  catch(re) {
    alert("LCAirPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LCAirPolGrid;
function initLCAirPolGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="保单号码";         		//列名
    iArray[0+1][1]="302px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="被保人姓名";         		//列名
    iArray[1+1][1]="212px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="打印日期";         		//列名
    iArray[2+1][1]="183px";         		//列名
 
    
    LCAirPolGrid = new MulLineEnter( "fm" , "LCAirPolGrid" ); 
    LCAirPolGrid.mulLineCount = 1; 
    LCAirPolGrid.displayTitle = 1; 
    LCAirPolGrid.locked = 0; 
    LCAirPolGrid.canSel = 0; 
    LCAirPolGrid.loadMulLine(iArray);  
 
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
