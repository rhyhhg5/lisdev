<%@ page contentType="text/html;charset=GBK" %>

<%
//程序名称：UploadImageFile.jsp
//程序功能：EasyScan上载图像文件处理
//创建日期：2004-03-31
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!-- -->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.easyscan.*"%>
  <%@ page language="java" import="com.jspsmart.upload.*"%>

<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />

<HTML>
<BODY>

<%
	System.out.println("-------------UploadImageFile.jsp Begin---------------------");
	try{
		// Initialization
		myUpload.initialize(pageContext);
		
		System.out.println("UploadImageFile: Before upload");
		
		// Upload
		myUpload.upload();
		
		System.out.println("UploadImageFile: After upload");

	//获得请求的字符串参数:把多个参数串拼接成一个字符串
    String strXML = "";
    int iCount = 0;
    // Retreive Requests' names
    java.util.Enumeration e = myUpload.getRequest().getParameterNames();
    //get Key Count
    while (e.hasMoreElements()) {
      iCount++;
      e.nextElement();
    }
    // Retreive parameters and link the Paramenter String
    StringBuffer xmlBuf = new StringBuffer(1024);
    for(int i=0;i<iCount;i++)
    {
      String key = "IndexXML" ;   //IndexXML,IndexXML1,IndexXML2, ......,IndexXMLn
      if (i>0)
      {
        key = key + String.valueOf(i);
      }
      String[] values = myUpload.getRequest().getParameterValues(key);
      //System.out.println(values[0]);
      xmlBuf.append(values[0]);
    }

    strXML = xmlBuf.toString();
    //System.out.println("IndexXML=" + strXML );

		System.out.println("UploadImageFile: Convert");

		ParameterDataConvert convert = new ParameterDataConvert();
		VData vData = new VData();

		//字符串参数转换为vData
		convert.xMLToVData(strXML,vData);

		//创建索引处理对象
		UploadImageFile indexData = new UploadImageFile(vData);

		System.out.println("UploadImageFile: Before save");

		//保存文件
		System.out.println("RealPath    = " + request.getRealPath(""));
		System.out.println("ContextPath = " + request.getContextPath());
		for (int i=0;i<myUpload.getFiles().getCount();i++){	
			//File uploadFile = myUpload.getFiles().getFile(i);
			String strSaveAsName;
			strSaveAsName = indexData.getSaveAsName(myUpload.getFiles().getFile(i).getFieldName(),
								myUpload.getFiles().getFile(i).getFileName(),
								request.getContextPath());
			System.out.println("UploadImageFile:SourceFileName = " + myUpload.getFiles().getFile(i).getFileName());
			System.out.println("UploadImageFile:SaveAsFileName = " + strSaveAsName);

			if (!myUpload.getFiles().getFile(i).isMissing() && !strSaveAsName.equals("")){
				myUpload.getFiles().getFile(i).saveAs(strSaveAsName,1);
			}
			else{
				//错误处理
				System.out.println("保存文件出现错误!");
				out.println("保存文件出现错误!");
			}
		}

		//获得返回数据
		//vData;

		//vData转换为字符串参数
		StringBuffer bufXML = new StringBuffer(1024);
		convert.vDataToXML(bufXML,vData);

		out.println("<IndexXML>" + bufXML.toString() + "</IndexXML>");
	}	catch (Exception err) {
      err.printStackTrace();
    }
    

    
	System.out.println("-------------UploadImageFile.jsp End ---------------------");
%>

</BODY>
</HTML>



