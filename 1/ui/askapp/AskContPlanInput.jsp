<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
String GrpContNo = request.getParameter("GrpContNo");
String LoadFlag = request.getParameter("LoadFlag");
String EnterKind = request.getParameter("EnterKind");
%>
<head>
<script>
GrpContNo="<%=GrpContNo%>";
LoadFlag="<%=LoadFlag%>";
EnterKind="<%=EnterKind%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="AskContPlan.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="AskContPlanInit.jsp"%>
<title>团体保障计划定制 </title>
</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit" action="AskContPlanSave.jsp">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>合同信息</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
				<!--<TD  class= title>集体合同号</TD>
				<TD  class= input>-->
					<Input class=readonly readonly name=GrpContNo value="<%=GrpContNo%>" type=hidden>
					<Input type=hidden name=ProposalGrpContNo>
					<input type=hidden name=mOperate>
				</TD>
				<TD  class= title>管理机构</TD>
				<TD  class= input>
					<Input class=readonly readonly name=ManageCom>
				</TD>
				<TD  class= title>团体客户号</TD>
				<TD  class= input>
					<Input class= readonly readonly name=AppntNo>
				</TD>

				
				<TD  class= title>投保单位名称</TD>
				<TD  class= input>
					<Input class= readonly readonly name=GrpName>
				</TD>
			</TR>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>已存在保障计划</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanCodeGrid" ></span>
				</td>
			</tr>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>保障计划定制</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
				<TD  class= title>保障计划</TD>
				<TD  class= input>
					<Input name=ContPlanCodeName CLASS="code" type="hidden">
					<Input class="code" name=ContPlanCode  maxlength="2" onchange="ChangePlan();" CodeData="0|^1|A^2|B^3|C^4|D^5|E" ondblclick="showCodeListEx('ContPlanCode',[this,ContPlanCodeName],[1,0],null,null,null,1);" onkeyup="showCodeListEx('ContPlanCodeName',[this,ContPlanCode],[1,0],null,null,null,1);">
				</TD>
				<TD  class= title>人员类别</TD>
				<TD  class= input>
					<input class=common name=ContPlanName>
				</TD>
			</tr>
			<tr class=common>
				<TD  class= title>参保人数</TD>
				<TD  class= input>
					<Input class=common name=Peoples3>
				</TD> 		
				<TD  class= title>应保人数</TD>
				<TD  class= input>
					<Input class=common name=Peoples2>
				</TD> 			
					<Input type=hidden name=PlanSql>
			</TR>
			<TR>
				

				<TD id=divriskcodename class= title>险种代码</TD>
				<TD id=divriskcode  class= input>
					<Input class=codeNo name=RiskCode ondblclick="return showCodeList('GrpRisk',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpRisk',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="RiskCodeName" readonly=true>
					<input type=hidden name=RiskFlag>
				</TD>
				<TD id=divmainriskname style="display:none" class=title>主险编码</TD>
				<TD id=divmainrisk style="display:none" class=input>
					<Input class=codeno maxlength=6 name=MainRiskCode ondblclick="return showCodeList('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="MainRiskCodeName" readonly=true>
				</TD>
				<TD id=divcontplanname style="display:none" class=title>保险套餐</TD>
				<TD id=divcontplan style="display:none" class=input>
					<Input class=codeno maxlength=6 name=RiskPlan ondblclick="return showCodeList('RiskPlan',[this,RiskPlanName],[0,1],null,'','',1);" onkeyup="return showCodeListKey('RiskPlan',[this,RiskPlanName],[0,1],null,'','');"><Input class=codename name="RiskPlanName" readonly=true>
				</TD>
				<TD  class= title>总保费</TD>
				<TD  class= input>
					<Input class=common name=SumPrem>
				</TD>
			</TR>
		</table>
		<Input VALUE="险种责任查询" class=cssButton TYPE=button onclick="QueryDutyClick();">
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanDutyGrid" ></span>
				</td>
			</tr>
		</table>
		<Input VALUE="定义保障计划要素" class=cssButton  TYPE=button onclick="AddContClick();">
		<!--<INPUT VALUE="定制管理费" class="cssButton"  TYPE=button onclick="ShowManageFee();">-->
		<!--table class=common>
			<TR  class= common>
				<TD>
					<Input VALUE="添加险种到保障计划" class=cssButton  TYPE=button onclick="AddContClick();">
				</TD>
			</TR>
			<TR  class= common>
				<TD  class= title>保障计划类型</TD>
				<TD  class= input>
					<Input class="code" name=ContPlanType ondblclick="showCodeList('contplantype',[this]);" onkeyup="showCodeListKey('contplantype',[this]);">
				</TD>
			</TR>
		</table-->
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>保障计划详细信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanGrid" ></span>
				</td>
			</tr>
		</table>
		<div id="divManageFee" style="display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanManageFeeGrid" ></span>
					</td>
				</tr>
			</table>
		</div>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanRiskPremGrid" ></span>
				</td>
			</tr>
		</table>
		<hr/>
		<Div  id= "divRiskPlanSave" style= "display: ''" align= right> 
	
		<INPUT VALUE="保障计划查询" class="cssButton"  style= "display: 'none'" TYPE=button onclick="">
		<INPUT VALUE="保障计划保存" class="cssButton"   TYPE=button onclick="submitForm();">
		<INPUT VALUE="保障计划修改" class="cssButton"   TYPE=button onclick="UptContClick();">
		<INPUT VALUE="保障计划删除" class="cssButton"   TYPE=button onclick="DelContClick();">

		</Div>
		<hr/>
		<Div  id= "divRiskPlanRela" style= "display: ''" align= left> 
		<INPUT VALUE="上一步" class="cssButton"  TYPE=button onclick="returnparent();">
		<!--<INPUT VALUE="保障计划要约录入" class="cssButton"  TYPE=button onclick="nextstep();">-->
		</Div>
		<div id="div1" style="display : 'none'">2005-5-13 14:29
		<TD  class= title>计划类别</TD>
				<TD  class= input>
					<Input class=codeno name=RiskPlan1 CodeData='0|^0|非固定计划^1|保险套餐' value=0 ondblclick="return showCodeListEx('RiskPlan1',[this,RiskPlan1Name],[0,1]);" onkeyup="return showCodeListKeyEx('RiskPlan1',[this,RiskPlan1Name],[0,1]);"><Input class=codename name="RiskPlan1Name" readonly=true value="非固定计划">
				</TD>
			</div>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>