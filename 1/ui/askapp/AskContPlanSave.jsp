<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpFeeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数
LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
LCContPlanUI tLCContPlanUI = new LCContPlanUI();

//输出参数
CErrors tError = null;
String tRearStr = "";
String tRela = "";
String FlagStr = "Fail";
String Content = "";

//全局变量
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

System.out.println("begin ...");

String tOperate=request.getParameter("mOperate");	//操作模式
String GrpContNo = request.getParameter("GrpContNo");	//集体合同号码
String ProposalGrpContNo = request.getParameter("ProposalGrpContNo");	//集体投保单号码
String ContPlanCode = request.getParameter("ContPlanCode");	//保障级别
String ContPlanName = request.getParameter("ContPlanName");	//保障说明
String PlanSql = request.getParameter("PlanSql");	//分类说明
String Peoples3 = request.getParameter("Peoples3");	//参保人数
String Peoples2 = request.getParameter("Peoples2");	//应保人数
System.out.println("sdfajf;ajfa;fja;fja;sfja;fj;asfdj;sfjsfdjs;dfks;f"+Peoples2);
String PlanType = "0";	//计划类型
String SumPrem = request.getParameter("SumPrem");

//保险计划要素信息
int lineCount = 0;
String arrCount[] = request.getParameterValues("ContPlanGridNo");
if (arrCount != null){
	String tChk[] = request.getParameterValues("InpContPlanGridChk");
	String tRiskCode[] = request.getParameterValues("ContPlanGrid2");	//险种编码
	String tDutyCode[] = request.getParameterValues("ContPlanGrid3");	//责任编码
	String tCalFactor[] = request.getParameterValues("ContPlanGrid5");	//计算要素码
	String tCalFactorValue[] = request.getParameterValues("ContPlanGrid8");	//计算要素值
	String tRemark[] = request.getParameterValues("ContPlanGrid9");	//特别说明
	String tRiskVersion[] = request.getParameterValues("ContPlanGrid10");	//险种版本
	String tGrpPolNo[] = request.getParameterValues("ContPlanGrid11");	//集体保单险种号码
	String tMainRiskCode[] = request.getParameterValues("ContPlanGrid12");	//主险编码
	String tCalFactorType[] = request.getParameterValues("ContPlanGrid13");	//类型
	String tSumPrem[] = request.getParameterValues("ContPlanGrid15");
	String tPayPlanCode[] = request.getParameterValues("ContPlanGrid16");
	String tGetDutyCode[] = request.getParameterValues("ContPlanGrid17");
	String tInsuAccNo[] = request.getParameterValues("ContPlanGrid18");

	lineCount = arrCount.length; //行数

	for(int i=0;i<lineCount;i++){
		tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
		tLCContPlanDutyParamSchema.setGrpContNo(GrpContNo);
		tLCContPlanDutyParamSchema.setProposalGrpContNo(ProposalGrpContNo);
		tLCContPlanDutyParamSchema.setContPlanCode(ContPlanCode);
		tLCContPlanDutyParamSchema.setContPlanName(ContPlanName);
		tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[i]);
		System.out.println("**********");
		tLCContPlanDutyParamSchema.setDutyCode(tDutyCode[i]);
		tLCContPlanDutyParamSchema.setCalFactor(tCalFactor[i]);
		tLCContPlanDutyParamSchema.setCalFactorValue(tCalFactorValue[i]);
		tLCContPlanDutyParamSchema.setRemark(tRemark[i]);
		tLCContPlanDutyParamSchema.setRiskVersion(tRiskVersion[i]);
		tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo[i]);
		tLCContPlanDutyParamSchema.setMainRiskCode(tMainRiskCode[i]);
		tLCContPlanDutyParamSchema.setMainRiskVersion(tRiskVersion[i]);
		tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType[i]);
		tLCContPlanDutyParamSchema.setPlanType("0");
		tLCContPlanDutyParamSchema.setPayPlanCode(tPayPlanCode[i]);
		tLCContPlanDutyParamSchema.setGetDutyCode(tGetDutyCode[i]);
		tLCContPlanDutyParamSchema.setInsuAccNo(tInsuAccNo[i]);
		
		tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
		//总保费问题
		LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
		System.out.println("在总保费不为空的情况下 tRiskCode : "+ tRiskCode[i]);
		tLCContPlanRiskSchema.setRiskCode(tRiskCode[i]);
		System.out.println("234234234234");
		System.out.println("在总保费不为空的情况下 tSumPrem : "+ tSumPrem[i]);
		tLCContPlanRiskSchema.setRiskPrem(tSumPrem[i]);
		tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

		System.out.println("记录"+i+"放入Set！tGrpPolNo[i] is "+tCalFactor[i]);
	}
}
String FeeCount[] = request.getParameterValues("ManageFeeGridNo");
String mChk[] = request.getParameterValues("InpManageFeeGridChk");
String tFeeCode[] = request.getParameterValues("ManageFeeGrid1");	//险种编码
String tFeeValue[] = request.getParameterValues("ManageFeeGrid3");	//险种编码
String tInsuAccNo[] = request.getParameterValues("ManageFeeGrid4");	//责任编码
String tRiskCode[] = request.getParameterValues("ManageFeeGrid5");	//责任编码
String tPayPlanCode[] = request.getParameterValues("ManageFeeGrid6");	//责任编码
if( tFeeCode !=null)
{
	int Count = mChk.length;
	for (int i = 0; i < Count; i++)  {
		if(!tFeeCode[i].equals("") && mChk[i].equals("1"))
		{
			System.out.println("进入管理费操作~~！"+tFeeCode[i]);
			LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
			tLCGrpFeeSchema.setFeeCode(tFeeCode[i]);
			tLCGrpFeeSchema.setInsuAccNo(tInsuAccNo[i]);
			tLCGrpFeeSchema.setFeeValue(tFeeValue[i]);
			tLCGrpFeeSchema.setRiskCode(tRiskCode[i]);
			tLCGrpFeeSchema.setPayPlanCode(tPayPlanCode[i]);
			tLCGrpFeeSet.add(tLCGrpFeeSchema);
		}
	}
}
System.out.println("end ...");

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.addElement(tLCContPlanDutyParamSet);
tVData.addElement(ProposalGrpContNo);
tVData.addElement(GrpContNo);
tVData.addElement(ContPlanCode);
tVData.addElement(PlanType);
tVData.addElement(PlanSql);
tVData.addElement(Peoples3);
tVData.addElement(Peoples2);
tVData.addElement(tLCContPlanRiskSet);
tVData.addElement(tLCGrpFeeSet);

try{
	System.out.println("this will save the data!!!");
	tLCContPlanUI.submitData(tVData,tOperate);
}
catch(Exception ex){
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (!FlagStr.equals("Fail")){
	tError = tLCContPlanUI.mErrors;
	if (!tError.needDealError()){
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>