<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
String GrpContNo = request.getParameter("GrpContNo");
String LoadFlag = request.getParameter("LoadFlag");
String EnterKind = request.getParameter("EnterKind");
%>
<head>
<script>
GrpContNo="<%=GrpContNo%>";
LoadFlag="<%=LoadFlag%>";
EnterKind="<%=EnterKind%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="AskContPlan.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="AskContPlanInit.jsp"%>
<title>询价保障计划定制</title>
</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit" action="AskContPlanSave2.jsp">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style="cursor:hand;">
				</td>
				<td class= titleImg>合同信息</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
					<Input class=readonly readonly name=GrpContNo value="<%=GrpContNo%>" type=hidden>
					<Input type=hidden name=ProposalGrpContNo>
					<input type=hidden name=mOperate>
				</TD>
				<TD  class= title>管理机构</TD>
				<TD  class= input>
					<Input class=readonly readonly name=ManageCom>
				</TD>
				<TD  class= title>团体客户号</TD>
				<TD  class= input>
					<Input class= readonly readonly name=AppntNo>
				</TD>				
				<TD  class= title>投保单位名称</TD>
				<TD  class= input>
					<Input class= readonly readonly name=GrpName>
				</TD>
			</TR>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style="cursor:hand;"">
				</td>
				<td class= titleImg>已存在保障计划</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanCodeGrid" ></span>
				</td>
			</tr>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>保障计划定制</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
				<TD  class= title>保障计划</TD>
				<TD  class= input>
					<Input name=ContPlanCodeName CLASS="code" type="hidden">
					<Input class="code" name=ContPlanCode  maxlength="2" onchange="ChangePlan();" CodeData="0|^1|A^2|B^3|C^4|D^5|E" ondblclick="showCodeListEx('ContPlanCode',[this,ContPlanCodeName],[1,0],null,null,null,1);" onkeyup="showCodeListEx('ContPlanCodeName',[this,ContPlanCode],[1,0],null,null,null,1);">
				</TD>
				<TD  class= title>人员类别</TD>
				<TD  class= input>
					<input class=common name=ContPlanName>
				</TD>
			</TR>
			<TR class=common>
				<TD  class= title>参保人数</TD>
				<TD  class= input>
					<Input class=common name=Peoples2>
				</TD> 				
					<Input type=hidden name=PlanSql>
			</TR>
			<TR>
				<TD id=divriskcodename class= title>险种代码</TD>
				<TD id=divriskcode  class= input>
					<Input class=codeNo name=RiskCode ondblclick="return showCodeList('GrpRisk',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpRisk',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="RiskCodeName" readonly=true>
					<input type=hidden name=RiskFlag>
				</TD>
				<TD id=divmainriskname style="display:none" class=title>主险编码</TD>
				<TD id=divmainrisk style="display:none" class=input>
					<Input class=codeno maxlength=6 name=MainRiskCode ondblclick="return showCodeList('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="MainRiskCodeName" readonly=true>
				</TD>
				<TD id=divcontplanname style="display:none" class=title>保险套餐</TD>
				<TD id=divcontplan style="display:none" class=input>
					<Input class=codeno maxlength=6 name=RiskPlan ondblclick="return showCodeList('RiskPlan',[this,RiskPlanName],[0,1],null,'','',1);" onkeyup="return showCodeListKey('RiskPlan',[this,RiskPlanName],[0,1],null,'','');"><Input class=codename name="RiskPlanName" readonly=true>
				</TD>
				<TD  class= title>总保费</TD>
				<TD  class= input>
					<Input class=common name=SumPrem>
				</TD>
	    </TR> 
	   </TABLE>
	   <TABLE> 
        <TR  class= common>                                              
          <TD  class= title8>险种备注</TD>                             
        </TR>                                                            
        <TR  class= common>                                              
          <TD  class= title8>                                            
            <textarea name="Remark" cols="138" rows="5" class="common" > 
            </textarea>                                                  
          </TD>                                                                                                                    			
			  </TR>
		</TABLE>
		<!--table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanRiskPremGrid" ></span>
				</td>
			</tr>
		</table-->
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=2>
					<span id="spanContPlanRemarkGrid" ></span>
				</td>
			</tr>
		</table>
		<Div  id= "divRiskPlanRemarkSave" style= "display: ''" align= left> 
			<INPUT VALUE="增加险种" class="cssButton"   TYPE=button onclick="addRiskType();"> 
			<!--INPUT VALUE="修改险种" class="cssButton"   TYPE=button onclick="modifyRiskType();"--> 
			<br/>
		</DIV> 	
		<Div  id= "divRiskPlanSave" style= "display: ''" align= right> 		 
			<INPUT VALUE="保障计划保存" class="cssButton"   TYPE=button onclick="submitForm();">
			<INPUT VALUE="保障计划修改" class="cssButton"   TYPE=button onclick="UptContClick();">
			<INPUT VALUE="保障计划删除" class="cssButton"   TYPE=button onclick="DelContClick();">
		</Div> 
		<hr/>
		<Div  id= "divRiskPlanRela" style= "display: ''" align= right> 
		<INPUT VALUE="上一步" class="cssButton"  TYPE=button onclick="returnparent();">
		</Div>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
    