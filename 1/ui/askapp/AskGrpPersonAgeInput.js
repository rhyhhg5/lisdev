//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();  

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  resetForm();
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LCPersonAgeDisItem.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
          
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{

  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LCPersonAgeDisItemQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
        fm.all('SerialNo').value= arrResult[0][0];
        fm.all('GrpContNo').value= arrResult[0][1];
        fm.all('PrtNo').value= arrResult[0][2];
        fm.all('StartAge').value= arrResult[0][3];
        fm.all('EndAge').value= arrResult[0][4];
        fm.all('MaleCount').value= arrResult[0][5];
        fm.all('FemalCount').value= arrResult[0][6];
        fm.all('OnWorkMCount').value= arrResult[0][7];
        fm.all('OnWorkFCount').value= arrResult[0][8];
        fm.all('OffWorkMCount').value= arrResult[0][9];
        fm.all('OffWorkFCount').value= arrResult[0][10];
        fm.all('MateMCount').value= arrResult[0][11];
        fm.all('MateFCount').value= arrResult[0][12];
        fm.all('YoungMCount').value= arrResult[0][13];
        fm.all('YoungFCount').value= arrResult[0][14];
        fm.all('OtherMCount').value= arrResult[0][15];
        fm.all('OtherFCount').value= arrResult[0][16];
        fm.all('MakeDate').value= arrResult[0][17];
        fm.all('MakeTime').value= arrResult[0][18];
        fm.all('ModifyDate').value= arrResult[0][19];
        fm.all('ModifyTime').value= arrResult[0][20];
	}
}               
        

function diaplayPerSonAge()
{
	var tGrpContNo = fm.GrpContNo.value;
	var strSql = "select StartAge,EndAge,OnWorkMCount,OnWorkFCount,OffWorkMCount,"
								+"OffWorkFCount,MateMCount,MateFCount,YoungMCount,YoungFCount,OtherMCount,"
								+"OtherFCount from LCPersonAgeDisItem where GrpContNo='"+tGrpContNo+"'"
								+" order by integer(StartAge) ";
	turnPage.queryModal(strSql, PersonAgeGrid);
}

function diaplayAveAndAllPerSonAge()
{
var cRowNum=PersonAgeGrid.mulLineCount;
if(cRowNum>0){
	for(i=3;i<=12;i++){
		var sumage=0;
		var allsumage=0;
		var aveage=0;
		for(j=0;j<cRowNum;j++)
		{
			sumage=sumage+parseInt(PersonAgeGrid.getRowColData(j,i));
			allsumage=allsumage+parseInt(PersonAgeGrid.getRowColData(j,i))*(parseInt(PersonAgeGrid.getRowColData(j,2))+parseInt(PersonAgeGrid.getRowColData(j,1)))/2;
		}
		  if(sumage==0)
		  {
		  	aveage=0;
		  	}
		  else{
		  aveage=allsumage/sumage;
		  }
		  var caveage=""+aveage;
		  caveage=caveage.substring(0,caveage.indexOf(".")+3)
			AllPersonAgeGrid.setRowColData(0,i,""+sumage);
			AvePersonAgeGrid.setRowColData(0,i,""+caveage);
			
		}
	}
else{
	AvePersonAgeGrid.setRowColData(0,3,"0");
	AvePersonAgeGrid.setRowColData(0,4,"0");
	AvePersonAgeGrid.setRowColData(0,5,"0");
	AvePersonAgeGrid.setRowColData(0,6,"0");
	AvePersonAgeGrid.setRowColData(0,7,"0");
	AvePersonAgeGrid.setRowColData(0,8,"0");
	AvePersonAgeGrid.setRowColData(0,9,"0");
	AvePersonAgeGrid.setRowColData(0,10,"0");
	AvePersonAgeGrid.setRowColData(0,11,"0");	
	AvePersonAgeGrid.setRowColData(0,12,"0");
	
	AllPersonAgeGrid.setRowColData(0,3,"0");
	AllPersonAgeGrid.setRowColData(0,4,"0");
	AllPersonAgeGrid.setRowColData(0,5,"0");
	AllPersonAgeGrid.setRowColData(0,6,"0");
	AllPersonAgeGrid.setRowColData(0,7,"0");
	AllPersonAgeGrid.setRowColData(0,8,"0");
	AllPersonAgeGrid.setRowColData(0,9,"0");
	AllPersonAgeGrid.setRowColData(0,10,"0");
	AllPersonAgeGrid.setRowColData(0,11,"0");
	AllPersonAgeGrid.setRowColData(0,12,"0");
	
	}
}