<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：GroupUW.jsp
//程序功能：团体保单人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tContNo = "";
	String tPrtNo = "";
	String tGrpContNo = "";
	String tMissionID = "";
	String tSubMissionID = "";	
	tPrtNo = request.getParameter("PrtNo");
	tGrpContNo = request.getParameter("GrpContNo");
	tMissionID = request.getParameter("MissionID");
	tSubMissionID  = request.getParameter("SubMissionID");	
        GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
	System.out.println(tGrpContNo+"asdf"+tPrtNo);
%>
<script>
	var PrtNo = "<%=tPrtNo%>";
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";	
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="AskGroupTrack.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="AskGroupTrackInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./AskUWGrpManuNormChk.jsp" method=post name=fmQuery target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCont);">
    		</td>
    		<td class= titleImg>
    			团体保单信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrpCont" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title8 style="display:'none'">
            团体投保单号码
          </TD>
          <TD  class= input8 style="display:'none'">
            <Input class="readonly" readonly name=GrpContNo >
          </TD>
          <TD  class= title8>
            印刷号码
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=PrtNo>
          </TD>
          <TD  class= title8>
            管理机构
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=ManageCom>
          </TD>

          
          <TD  class= title8>
            销售渠道
          </TD> 
          <TD  class= input8>
          	<Input class="readonly" readonly name=SaleChnl>
          </TD>
                  </TR>
        <TR  class= common8>
          <TD  class= title8>
            中介机构名称
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=AgentCom>
          </TD>
          <TD  class= title8>
            业务员代码
          </TD>
          <TD  class= input8>
      <Input class="readonly" readonly NAME=AgentCode>
         </TD>
        <TD  class= title8>
            业务员名称
          </TD>
          <TD  class= input8>
      <Input class="readonly" readonly NAME=AgentName>
         </TD>
                   </TR>
        <TR  class= common>     
          <TD  class= title8 style="display:'none'">
            代理人组别
          </TD>
          <TD  class= input8 style="display:'none'">
            <Input class="readonly" readonly name=AgentGroup>
          </TD>          
      
           <TD  class= title8>
            团体客户代码
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=AppntNo >
          </TD>
                     <TD  class= title8>
            团体客户名称
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=AppntName >
          </TD>
      
          <TD  class= title8>
            VIP标记
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=VIPValue >
          </TD>
                    </TR>
        <TR  class= common>    
           <TD  class= title8>
           黑名单标记
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=BlacklistFlag >           
          </TD>          
          <TD  class= title>
            跟踪状态
          </TD>
          <TD  class= input>
            <Input class="code" readonly name=QState value= " " CodeData= "0|^0|未结案^1|需求改变，重新询价结案^2|业务已流失结案^4|业务跟踪完成，状态设为已重新询价^9|业务跟踪完成，状态设为已投保" ondblClick="showCodeListEx('State',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('State',[this,''],[0,1]);">
          </TD>
          </TR>
      </table>
    </Div>
   
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			团体投保单查询结果
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>    	
    </div>
	<br>    
	
	
	<INPUT type = "hidden" VALUE = "询价投保单明细信息" Class="cssButton" TYPE=button onclick= "showAskGrpCont();">
	<INPUT type = "hidden" VALUE = "既往询价信息" Class="cssButton" TYPE=button onclick= "showAskApp();">
	<INPUT type = "hidden" VALUE = "既往理赔信息" Class="cssButton" TYPE=button onclick= "ClaimGetQuery2();">
	<INPUT type = "hidden" VALUE = "既往保全信息" Class="cssButton" TYPE=button onclick= "Prt();">	
	
	<INPUT  type = "hidden" VALUE = "询价跟踪查询" Class="cssButton" TYPE=button onclick= "askInfoQ();">
	<INPUT  type = "hidden" VALUE = "查询产品定价需求表" Class="cssButton" TYPE=button onclick= "showGrpCont();">     
        
  <INPUT  type = "hidden" VALUE = "询价跟踪录入" Class="cssButton" TYPE=button onclick= "askInfo();">
  <INPUT  type = "hidden" VALUE = "录入产品定价需求表" Class="cssButton" TYPE=button onclick= "showGrpCont();">
  <INPUT  VALUE = "发询价跟踪通知书" Class="cssButton" TYPE=button onclick= "askTrack();">
        
       
    <!-- 集体单核保结论 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>团体客户回复 ：</td>
	</tr>
    </table>
    <table  class= common border=0 width=100%>
      	<TR  class= common>
          <TD  height="29" class= title>
            跟踪回复
            <Input class="code" name=AskTrack CodeData= "0|^b|客户正式投保^c|重新询价^d|业务流失^e|手动撤单" ondblclick= "showCodeListEx('AskTrack',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('AskTrack',[this,''],[0,1]);">
            <!--input class="code" name=t ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"-->
          </TD>
        </TR>
          <tr></tr>
          <TD  class= title>
            回复说明
          </TD>
          <tr></tr>
          <TD  class= input> <textarea name="GUWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
    </table>
    	  		
          <INPUT VALUE="确  认" Class="cssButton" TYPE=button onclick="gmanuchk();">
          <INPUT VALUE="返  回" Class="cssButton" TYPE=button onclick="GoBack();">
          <input type="hidden" name= "WorkFlowFlag" value= "">
          <input type="hidden" name= "MissionID" value= "<%=tMissionID%>">
          <input type="hidden" name= "SubMissionID" value= "">
          <input type="hidden" name= "PrtNoHide" value= "">
          <input type="hidden" name= "GrpProposalContNo" value= "">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
