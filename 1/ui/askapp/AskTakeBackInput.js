//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
var tSearch = 0;
window.onfocus=myonfocus;
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  var strSql = "select * from LZSysCertify where CertifyCode='" + fm.CertifyCode.value + "' and CertifyNo='"+ fm.CertifyNo.value + "' and stateflag='1'";
  var arrResult = easyExecSql(strSql);
  if (arrResult != null) {
  	alert("警告：该单证已回收，不容许进行二次回收！");
    return false;
  }
  if(fm.TakeBackDate.value=='')
  {
  	 alert('请输入回收时间');
  	 return;
  }
  strSql = "select lwmission.missionid,lwmission.submissionid,lwmission.missionprop1 from lwmission where lwmission.missionprop7='" + fm.CertifyNo.value +"'";
	turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
  if(!turnPage.strQueryResult)
  {
  	  alert('不能够回收单证！！');
  	  return;
  }
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.MissionID.value=turnPage.arrDataCacheSet[0][0];
	fm.SubMissionID.value=turnPage.arrDataCacheSet[0][1];
	fm.ContNo.value=turnPage.arrDataCacheSet[0][2];
	fm.hideOperation.value = "INSERT||MAIN";
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    content="保存成功！";
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
	parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperation = "QUERY||MAIN";
  fm.sql_where.value = " and StateFlag = '1' ";
  showInfo = window.open("./AskTakeBackQueryInput.html");
}

function query()
{
	mOperation = "QUERY||MAIN";
	fm.sql_where.value = " and StateFlag = '0' ";
	showInfo = window.open("AskTakeBackQueryInput.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{   
	try {
	  if(arrResult!=null)
	  {
			fm.CertifyCode.value = arrResult[0][0];
			fm.CertifyNo.value = arrResult[0][1];
			fm.ValidDate.value = arrResult[0][5];
			fm.SendOutCom.value = arrResult[0][3];
			fm.ReceiveCom.value = arrResult[0][2];
			fm.Handler.value = arrResult[0][6];
			fm.HandleDate.value = arrResult[0][7];
			fm.SendNo.value = arrResult[0][8];
			fm.TakeBackNo.value = arrResult[0][9];
			fm.Operator.value = arrResult[0][4];
			fm.MakeDate.value = arrResult[0][10];
		}
	} catch (ex) {
		alert("在afterQuery中发生错误");
	}
}

