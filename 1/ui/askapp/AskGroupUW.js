//程序名称：GroupUW.js
//程序功能：集体人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容


/*********************************************************************
 *  //该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var mSwitch = parent.VD.gVSwitch;



/*********************************************************************
 *  提交对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);
  fmQuery.submit(); //提交
  alert("submit");
}



/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    alert(content);
  }
  else
  {

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
    alert(content);
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}



/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
   parent.fraMain.rows = "0,0,50,82,*";
  }
 else
 {
   parent.fraMain.rows = "0,0,0,82,*";
 }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}


/*********************************************************************
 *  查询团体单下个人单既往投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  var cInsureNo = fmQuery.InsuredNo.value;
  if(cProposalNo==""||cProposalNo==null|| cInsureNo==""||cInsureNo==null)
  {
  	showInfo.close();
  	alert("请选择个人保单,后查看其信息!");
  	return ;
  	}
  //showModalDialog("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单下个人单以往核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==""||cProposalNo==null)
  {
  	showInfo.close();
  	alert("请选择个人保单,后查看其信息!");
  	return ;
  	}
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单下个人单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==""||cProposalNo==null)
  {
  	showInfo.close();
  	alert("请先选择个人保单,后查看其信息!");
  	return ;
  	}
  window.open("./UWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGNewUWSub()
{
  var cGrpContNo=fmQuery.GrpContNo.value;
  if(cGrpContNo==""||cGrpContNo==null)
  {
  	alert("请先选择一个团体投保单!");
  	return ;
  	}
  window.open("./UWGErrMain.jsp?GrpContNo="+cGrpContNo,"window1");
}



/*********************************************************************
 *  查询团体下的附属于各团单的个人保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==null||cProposalNo=="")
  {
  	showInfo.close();
  	alert("请先选择个人保单,后查看其信息!");

  }
  else{
  mSwitch.deleteVar( "PolNo" );
  mSwitch.addVar( "PolNo", "", cProposalNo );
  window.open("../app/ProposalMain.jsp?LoadFlag=4");
  showInfo.close();}
}





/*********************************************************************
 *  对团体单下的个人加费承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  tUWIdea = fmQuery.all('UWIdea').value;
  if (cProposalNo != "")
  {
  	window.open("./UWGSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag=2&UWIdea="+tUWIdea,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择一个个人投保单!");
  }
}


/*********************************************************************
 *  对团体单特约承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.GrpProposalContNo.value;
  var tUWIdea = fmQuery.GUWIdea.value;
  if (cProposalNo != "")
  {
  	window.open("./UWGrpSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag=1&UWIdea="+tUWIdea,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择一个团体保单!");
  }
}
/*********************************************************************
 *  询价补充资料录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function askInfo()
{
var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cGrpContNo = fmQuery.GrpContNo.value;
  
 
  var cPrtNo = fmQuery.PrtNo.value;

  if (cGrpContNo != "")
  {
  	
  	window.open("./AskInfoMain.jsp?GrpContNo1="+cGrpContNo+"&PrtNo="+cPrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}


/*********************************************************************
 *  询价补充资料查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
//补充资料查询
function askInfoQ()
{
	
	var arrSelected = new Array();
  var cGrpContNo = fmQuery.GrpContNo.value;
  var cPrtNo = fmQuery.PrtNo.value;
	var strSQL = "";
	var mScanDocNo = "";
	strSQL="select lwmission.MissionProp7 from lwmission where 1=1"
	           + " and LWMission.ProcessID = '0000000006' " 
             + " and LWMission.ActivityID = '0000006010' " 
             +" and MissionProp2='"+cPrtNo+"' "
             ;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
		if(arrSelected!=null)
		{
	   mScanDocNo = arrSelected[0][0];
     }
	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  if (cGrpContNo!= ""  )
  {
  	
  	window.open("./AskInfoQMain.jsp?GrpContNo="+cGrpContNo+"&ScanDocNo="+mScanDocNo+"&PrtNo="+cPrtNo,"window1");
  	showInfo.close();

  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //initPolGrid();
  fmQuery.submit(); //提交
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  showModalDialog("./ProposalDuty.jsp",window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=13cm");

}

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  showModalDialog("./ProposalFee.jsp",window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");

}


/*********************************************************************
 *  显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


/*********************************************************************
 *  查询团体单下主附团体投保单单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function querygrp()
{  
	// 书写SQL语句
	
	var strsql = "";
//	strsql = "select LCGrpPol.GrpProposalContNo,LCGrpPol.prtNo,LCGrpPol.GrpName,LCGrpPol.RiskCode,LCGrpPol.RiskVersion,LCGrpPol.ManageCom from LCGrpPol,LCGUWMaster where 1=1 "
//			 	 + " and LCGrpPol.AppFlag='0'"
//				 + " and LCGrpPol.ApproveFlag in('2','9') "
//				 + " and LCGrpPol.UWFlag in ('2','3','5','6','8','7')"
//				 + " and LCGrpPol.contno = '00000000000000000000'"				          //自动核保待人工核保
//				 + " and LCGrpPol.PrtNo = '"+PrtNo+"'"
//				 + " and LCGrpPol.GrpPolNo = LCGUWMaster.GrpPolNo"
//				 + " and (LCGUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') or LCGUWMaster.appgrade is null)"
//				 + " order by LCGrpPol.makedate ,LCGrpPol.maketime" ;
	strsql = "select GrpContNo,PrtNo,GrpName,RiskCode,UWFlag,ManageCom from LCGrpPol where 1=1 and GrpContNo='"+GrpContNo+"'";
	
       
	 //execEasyQuery( strSQL );
	//查询SQL，返回结果字符串
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    alert("没有符合条件集体单！");
    return "";
    }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;

  //保存SQL语句
  turnPage.strQuerySql = strsql;

  //设置查询起始位置
  turnPage.pageIndex = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

//alert(1)
	var arrSelected = new Array();
	var strSQL = "select lcgrpcont.GrpContNo,lcgrpcont.PrtNo,(select name from ldcom where comcode=lcgrpcont.ManageCom),(select codename from ldcode where codetype='salechnl' and code=lcgrpcont.SaleChnl),(select name from lacom where agentcom=lcgrpcont.AgentCom),lcgrpcont.AgentCode,lcgrpcont.AgentGroup,LDGrp.VIPValue,LDGrp.BlacklistFlag,(select name from laagent where agentcode=lcgrpcont.AgentCode),lcgrpcont.appntno,lcgrpcont.Grpname,lcgrpcont.managecom from lcgrpcont,LDGrp where lcgrpcont.grpcontno='"+GrpContNo+"'"
		+" and lcgrpcont.AppntNo = LDGrp.CustomerNo  " 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	if(arrSelected)
	{
		fmQuery.all('GrpContNo').value = arrSelected[0][0];
		fmQuery.all('PrtNo').value = arrSelected[0][1];
		fmQuery.all('ManageCom').value = arrSelected[0][2];
		fmQuery.all('SaleChnl').value = arrSelected[0][3];
		fmQuery.all('AgentCom').value = arrSelected[0][4];
   	fmQuery.all('AgentCode').value = arrSelected[0][5];
		fmQuery.all('AgentGroup').value = arrSelected[0][6];
		fmQuery.all('VIPValue').value = arrSelected[0][7];
		fmQuery.all('BlacklistFlag').value = arrSelected[0][8];
//		fmQuery.all('QState').value = arrSelected[0][9];
		fmQuery.all('AgentName').value = arrSelected[0][9];
		fmQuery.all('AppntNo').value = arrSelected[0][10];
		fmQuery.all('GrpNo').value = arrSelected[0][10];		
		fmQuery.all('AppntName').value = arrSelected[0][11];
		fmQuery.all('ManageCom1').value = arrSelected[0][12];
	}

  return true;
}


/*********************************************************************
 *  查询团体单下自动核保未通过的个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPol()
{
	var GrpProposalContNo = fmQuery.all('GrpProposalContNo').value;
	var mOperate = fmQuery.all('Operator').value;
	var strsql = "";

	// 初始化表格
	showDiv(divPerson,"false");
	initPolBox();
	//initPolGrid();
	// 书写SQL语句

	strsql = "select LCPol.ProposalNo,LCPol.AppntName,LCPol.RiskCode,LCPol.RiskVersion,LCPol.InsuredName,LCPol.ManageCom from LCPol where 1=1 "
			  + "and LCPol.GrpPolNo = '"+GrpProposalContNo+"'"
 	    	  + " and LCPol.AppFlag='0'"
         	  + " and LCPol.UWFlag in ('3','5','6','8','7')"         //自动核保待人工核保
			  + " and LCPol.contno = '00000000000000000000'"
			  + " order by LCPol.makedate ";

 	//alert(strsql);
    //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("此单下个人单核保均已全部通过！");
    return "";
  }

  showDiv(divPerson,"true");
  //个人体检资料录入只针对团体主险投保单下的个人单
  if(GrpProposalContNo == fmQuery.all('GrpMainProposalNo').value)
  {
  	showDiv(divBoth,"true");
  	showDiv(divAddFee,"false");
  }
  else
  {
  	showDiv(divBoth,"false");
  	showDiv(divAddFee,"true");
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strsql;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


/*********************************************************************
 *  查询集体下未过个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getGrpPolGridCho()
{
	fmQuery.PolTypeHide.value = '2';
	fmQuery.submit();
}



/*********************************************************************
 *  选择要人工核保保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolGridCho()
{
	fmQuery.PolTypeHide.value = '1';
	fmQuery.submit();
}


/*********************************************************************
 *  个人单核保确认
 *  UWState:(0 未核保 1拒保 2延期 3条件承保 4通融 5自动 6待上级 7问题件 8核保通知书 9正常 a撤单 b保险计划变更)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function manuchk()
{
	var cProposalNo=fmQuery.ProposalNo.value;
	var flag = fmQuery.all('UWState').value;
	var tUWIdea = fmQuery.all('UWIdea').value;

	if (flag == "0"||flag == "1"||flag == "4"||flag == "9")
	{
	   //showModalDialog("./UWManuNormMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		fmQuery.action = "./UWManuNormChk.jsp";
		fmQuery.submit();
	}
	if (flag == "")
	{
		alert("选择核保结论！")
	}

	initPolBox();
	//initPolGrid();
}


/*********************************************************************
 *  团体整单核保确认
 *  (0 未核保 1拒保 2延期 3条件承保 4通融 5自动 6待上级 7问题件 8核保通知书 9正常 a撤单 b保险计划变更)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function gmanuchk()
{
	var cGrpContNo=fmQuery.GrpContNo.value;
	var flag = fmQuery.all('GUWState').value;
	var tUWIdea = fmQuery.all('GUWIdea').value;
	//alert("flag:"+flag);
	if( cGrpContNo == "" || cGrpContNo == null )
	{
		alert("请先选择一个团体投保单!");
	    cancelchk();
		return ;
	}

	if (flag == null || flag == "")
	{
		alert("请先选择核保结论!");
	    cancelchk();
		return ;
	}

	//撤单,上报,拒保,发加费通知书均是针对团体单中的主险投保单进行
	if(flag == "1"||flag == "6"||flag == "a"||flag == "7"||flag == "8")
	{
/*		if( cGrpContNo != fmQuery.GrpMainProposalNo.value)
		{
			alert("请先选择一个团体主险投保单!");
	        cancelchk();
		    return ;
		    }
*/
	}
	//操作功能实现
	if (flag == "1"||flag == "4"||flag == "6"||flag == "9"||flag == "8"||flag == "a")
	{
		fmQuery.WorkFlowFlag.value = "0000002004";
		fmQuery.MissionID.value = MissionID;
		fmQuery.SubMissionID.value = SubMissionID;
		//alert(flag);
		//showModalDialog("./AskUWGrpManuNormMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fmQuery.action = "./AskUWGrpManuNormChk.jsp";
		fmQuery.submit(); //提交
	}

	if (flag == "2") //延期
	{
		window.open("./UWManuDateMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}

	if (flag == "3") //条件承保
	{
		window.open("./UWManuSpecMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}

	if (flag == "7")//将问题件发送到相应的处理岗位待(管理中心或问题修改或复核修改)处理.
	{
		showModalDialog("./UWGrpManuNormMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	}

	
}






/*********************************************************************
 *  个单问题件查询(目前只是预留,功能已实现,只是未在用户界面上未显示查询按钮)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
	var cProposalNo = fmQuery.ProposalNo.value;//个单投保单号
	if(cProposalNo==""||cProposalNo==null)
	{
  		alert("请先选择一个个人投保单!");
  		return ;
    }
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
}








/*********************************************************************
 *  点击取消按钮,初始化界面各隐藏变量
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelchk()
{
	fmQuery.all('GUWState').value = "";
	fmQuery.all('GUWIdea').value = "";
}


/*********************************************************************
 *  返回到自动核保查询主界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function GoBack()
 {
 	 if(ActivityID=="0000006004")
 	 {
			location.href  = "../uw/ManuUWAll.jsp?type=3";
	 }
	 else if(ActivityID=="0000006011")
	 {
			location.href  = "./AskUWMakePriceInput.jsp";
	 }
 }

function temp()
{
	alert("此功能尚缺！");
}
/*********************************************************************
 *  点击扫描件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function ScanQuery2()
{
	var arrReturn = new Array();

	var prtNo=fmQuery.all("PrtNo").value;
	if(prtNo==""||prtNo==null)
	{
	  	alert("请先选择一个团体投保单!");
  		return ;
  	}
	window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

function GrpContQuery(cGrpContNo){
	window.open("./GroupContMain.jsp?GrpContNo=" + cGrpContNo,"GroupContQuery");
}
/*********************************************************************
 *  进入团体合同
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showAskGrpCont()
{
     var cGrpContNo=fmQuery.GrpContNo.value;
     var cPrtNo = fmQuery.PrtNo.value;    
    //var newWindow=window.open("../app/GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+cGrpContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1"); 
    window.open("../askapp/AskGroupPolApproveInfo.jsp?LoadFlag=14&polNo="+cGrpContNo+"&prtNo="+cPrtNo,"window1");
}

// 该投保单理赔给付查询
function ClaimGetQuery2()
{
	var arrReturn = new Array();
	var tSel = PolAddGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = PolAddGrid.getRowColData(tSel - 1,2);				
		if (cPolNo == "")
		    return;
		  window.open("../sys/ClaimGetQueryMain.jsp?PolNo=" + cPolNo);										
	}	
}

/*********************************************************************
 *  查询既往询价信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function showAskApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cGrpContNo=fmQuery.GrpContNo.value;
  cAppntNo = fmQuery.AppntNo.value;
  
  if (cGrpContNo != "")
  {
  	
  	window.open("./AskUWAppMain.jsp?GrpContNo1="+cGrpContNo+"&AppntNo1="+cAppntNo,"window2");
  	showInfo.close();
  }
  
}             

/*********************************************************************
 *  录入产品定价需求表
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGrpCont()
{
	window.open("./AskMakePriceMain.jsp?GrpContNo="+GrpContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+ActivityID,"window2");
}
function showUWAsk()
{
	var cPrtNo = fmQuery.PrtNo.value;   
	window.open("./AskUWNoteMain.jsp?GrpContNo="+GrpContNo+"&PrtNo="+cPrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+ActivityID,"window2");
}
/*********************************************************************
 *  工作流更新
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initWorkFlow()
{
	fmQuery.SubMissionID.value= SubMissionID;
	fmQuery.MissionID.value= MissionID;
	fmQuery.PrtNo.value= PrtNo;
	fmQuery.action = "./AskUWAuto.jsp";
	fmQuery.submit();
}
/*********************************************************************
 *  产品定价请求
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function InfoRequest()
 { 
 	if( confirm("是否发送定价请求？是请点击“确定”，否则点“取消”。"))
 	{
  var strSQL = "";
	strSQL = "select missionprop1 from lwmission where 1=1 "
				 + " and activityid = '0000006011' "
				 + " and processid = '0000000006'" 
				 + " and missionprop2='"+fmQuery.PrtNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if(turnPage.strQueryResult)
	{ 
		 alert("已经提出定价请求");
		 return ;
	} 
 var cstrSQL  = "select noteid from LCAskUWNote where 1=1 "
         + " and stateflag='0' "
				 + " and grpcontno = '"+fmQuery.GrpContNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(cstrSQL, 1, 0, 1);
	if(!turnPage.strQueryResult)
	{
		 alert("请先在产品定价需求表中录入核保意见！");
		 return;
	}
	
 	fmQuery.SubMissionID.value= SubMissionID;
	fmQuery.MissionID.value= MissionID;
	fmQuery.PrtNo.value= PrtNo;
	
	var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fmQuery.WorkFlowFlag.value="0000006020";
	fmQuery.action = "./AskRequestSave.jsp";
	fmQuery.submit();
}
 }

/*********************************************************************
 *  初始化定价或核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initUWorMP()
{
	if(ActivityID=="0000006004")
	{
		divUWIdea.style.display = "";
	}
	else if(ActivityID=="0000006011") 
	{
		divMPIdea.style.display = "";
		divMPButton.style.display = "none";
	}
}

/*********************************************************************
 *  定价确认，触发工作流节点
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function MakePrice()
{
	var strSQL = "";
	strSQL = "select noteid from LCAskUWNote where 1=1 "
	       +" and int(stateflag)<2 "
				 + " and GrpContNo='"+fmQuery.GrpContNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if(turnPage.strQueryResult)
	{ 
		 alert("请先回复核保意见，然后再进行确认！");
		 return;
	} 
	var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fmQuery.WorkFlowFlag.value="0000006011";
	fmQuery.action = "./AskRequestSave.jsp";
	fmQuery.submit();	
}

/*********************************************************************
 *  既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showRejiggerApp()
{
    window.open("../uw/GrpUWRejiggerCont.jsp?pmGrpContNo="+GrpContNo+"&pmAppntNo="+fmQuery.AppntNo.value,"window3");
}

/*********************************************************************
 *  既往理陪信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsuApp()
{
    window.open("../uw/GrpUWInsuCont.jsp?pmGrpContNo="+GrpContNo+"&pmAppntNo="+fmQuery.AppntNo.value,"window3");
}
function showChangePlan() {
     var cGrpContNo=fmQuery.GrpContNo.value;
     var cPrtNo = fmQuery.PrtNo.value;    
    //var newWindow=window.open("../app/GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+cGrpContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1"); 
    window.open("../askapp/AskGroupPolApproveInfo.jsp?LoadFlag=23&polNo="+cGrpContNo+"&prtNo="+cPrtNo,"window1");


}