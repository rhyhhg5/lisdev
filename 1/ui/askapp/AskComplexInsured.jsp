<DIV id=DivLCInsuredButton STYLE="display:''">
<!-- 被保人信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">
</td>
<td class= titleImg>
被保人信息（客户号：<Input class= common name=InsuredNo >
<INPUT id="butBack" VALUE="查  询" class=cssButton TYPE=button onclick="queryInsuredNo();"> 首次投保客户无需填写客户号）
<Div  id= "divSamePerson" style="display:'none'">
<font color=red>
如投保人为被保险人本人，可免填本栏，请选择
<INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
</font>
</div>
</td>
</tr>
</table>

</DIV>
<DIV id=DivLCInsured STYLE="display:''">
    <table  class= common>
        <TR  class= common>        
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name elementtype=nacessary verify="被保险人姓名|notnull&len<=20" onblur="getallinfo();">
          </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="code" name="IDType"  verify="被保险人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);" onblur="getallinfo();" >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name="IDNo"  onblur="checkidtype();getBirthdaySexByIDNo(this.value);getallinfo();" verify="被保险人证件号码|len<=20"  >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="code" name=Sex elementtype=nacessary verify="被保险人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="coolDatePicker" elementtype=nacessary dateFormat="short" name="Birthday" verify="被保险人出生日期|notnull&date" >
          </TD>
        </TR>
    </Table>      
<DIV id=DivGrpNoname STYLE="display:''">        
    <table  class= common>        
        <TR class= common>
          <TD CLASS=title>
            保单类型标记
          </TD>
          <TD CLASS=input COLSPAN=1>
            <Input NAME=PolTypeFlag VALUE="0" CLASS=code ondblclick="showCodeList('PolTypeFlag', [this]);" onkeyup="showCodeListKey('PolTypeFlag', [this]);" verify="保单类型标记|notnull" >
          </TD>        
            <TD CLASS=title>
                被保人人数
            </TD>
            <TD CLASS=input COLSPAN=1>
                <Input NAME=InsuredPeoples VALUE="" readonly=true CLASS=common verify="被保人人数|num" >
            </TD>
            <TD CLASS=title>
                被保人年龄
            </TD>
            <TD CLASS=input COLSPAN=1>
                <Input NAME=InsuredAppAge VALUE="" readonly=true CLASS=common verify="被保人年龄|num" >
            </TD>
	    </TR>  
      </table>
</Div>
   
   <Div STYLE="display:'none'">
   <table  class= common>   	    
        <TR  class= common>  
            <TD  class= title>
                地址代码
            </TD>                  
            <TD  class= input>
                <Input class="code" name="AddressNo"  ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', true);" onkeyup="return showCodeListKeyEx('GetAddressNo',[this],[0],'', '', '', true);" onfocus="getdetailaddress();">
            </TD>
	    </TR>             	       
        <TR  class= common>
            <TD  class= title>
                联系地址
            </TD>
            <TD  class= input colspan=3 >
                <Input class= common3 name="PostalAddress"  verify="被保险人联系地址|len<=80" >
            </TD>
            <TD  class= title>
                邮政编码
            </TD>
            <TD  class= input>
                <Input class= common name="ZipCode"  MAXLENGTH="6" verify="被保险人邮政编码|zipcode" >
            </TD>
        </TR>
        <TR  class= common>
            <TD  class= title>
                联系电话
            </TD>
            <TD  class= input>
                <input class= common name="Phone"  verify="被保险人家庭电话|len<=18" >
            </TD>
            <TD  class= title>
                移动电话
            </TD>
            <TD  class= input>
                <Input class= common name="Mobile" verify="被保险人移动电话|len<=15" >
            </TD>
            <TD  class= title>
                电子邮箱
            </TD>
            <TD  class= input>
                <Input class= common name="EMail" verify="被保险人电子邮箱|len<=20&Email" >
            </TD>
        </TR>
	    <TR class= common>
	        <TD  class= title>
                与主被保险人关系</TD>
            <TD  class= input>
                
              <Input class="code" name="RelationToMainInsured"  verify="主被保险人关系|code:Relation" ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);"></TD>
            <TD  class= title>
                职业代码
            </TD>
            <TD  class= input>
                <Input class="code" name="OccupationCode"  elementtype=nacessary verify="被保险人职业代码|code:OccupationCode" ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);" onfocus="getdetailwork();">
            </TD>
            <TD  class= title>
                职业类别
            </TD>
            <TD  class= input>
                <Input class="code" name="OccupationType"  verify="被保险人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
            </TD>
            </TR>     
   </TABLE>
   </Div>
   
</DIV>
<TABLE class= common>
    <TR class= common>
        <TD  class= title>
            <DIV id="divContPlan" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            保险计划
                    </TD>
                    <TD  class= input>
                        <Input class="code" name="ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this],[0],'', '', '', true);">
                    </TD>
		
                    </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
            <DIV id="divExecuteCom" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            处理机构
                        </TD>
                        <TD  class= input>
                            <Input class="code" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this],[0],'', '', '', true);">
                        </TD>
		            </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
        </TD>		
    </TR>
</TABLE>
<DIV id="divLCInsuredPerson" style="display:'none'"> 
  <TABLE class= common>       
        <TR  class= common>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class="code" name="Marriage"  ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);"></TD>
           <!-- <Input class="code" name="Marriage" verify="被保险人婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);"></TD>-->
          <TD  class= title>
            与投保人关系</TD>
          <TD  class= input>
            <Input class="code" name="RelationToAppnt"  ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);"></TD>
           <!-- <Input class="code" name="RelationToAppnt" verify="与投保人关系|code:Relation" ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);"></TD>-->
          <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <input class="code" name="NativePlace"  ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
          <!--<input class="code" name="NativePlace" verify="被保险人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">-->
          </TD>
        </TR>
        
        <TR class= common>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class= common name="RgtAddress" >
            <!--<Input class= common name="RgtAddress" verify="被保险人户口所在地|len<=80" >-->
          </TD>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
          <input class="code" name="Nationality"  ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
         <!-- <input class="code" name="Nationality" verify="被保险人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">-->
          </TD>
          <TD  class= title>
            学历
          </TD>
          <TD  class= input>
            <Input class="code" name="Degree"  ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
            <!--<Input class="code" name="Degree" verify="被保险人学历|code:Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">-->
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            工作单位
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="GrpName"  >
            <!--<Input class= common3 name="GrpName" verify="被保险人工作单位|len<=60" >-->
          </TD>
          <TD  class= title>
            单位电话
          </TD>
          <TD  class= input>
            <Input class= common name="GrpPhone"  >
            <!--<Input class= common name="GrpPhone" verify="被保险人单位电话|len<=18" >-->
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="GrpAddress"  >
            <!--<Input class= common3 name="GrpAddress" verify="被保险人单位地址|len<=80" >-->        
          </TD>
          <TD  class= title>
            单位邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="GrpZipCode"  >
            <!-- <Input class= common name="GrpZipCode" verify="被保险人单位邮政编码|zipcode" >-->
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            职业（工种）
          </TD>
          <TD  class= input>
            <Input class= common name="WorkType"  >
            <!--<Input class= common name="WorkType" verify="被保险人职业（工种）|len<=10" >-->
          </TD>
          <TD  class= title>
            兼职（工种）
          </TD>
          <TD  class= input>
            <Input class= common name="PluralityType"  >
            <!--<Input class= common name="PluralityType" verify="被保险人兼职（工种）|len<=10" >-->
          </TD>                  
          <TD  class= title>
            是否吸烟
          </TD>
          <TD  class= input>
            <Input class="code" name="SmokeFlag"  ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
            <!--<Input class="code" name="SmokeFlag" verify="被保险人是否吸烟|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">-->
          </TD>
       </TR>
      </table>  
      <INPUT  type= "hidden" name= AppFlag value= "8">
</DIV>