<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：GroupUW.jsp
	//程序功能：团体保单人工核保
	//创建日期：2002-06-19 11:10:36
	//创建人  ：WHN
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tContNo = "";
	String tPrtNo = "";
	String tGrpContNo = "";
	String tMissionID = "";
	String tSubMissionID = "";	
	String tActivityid = "";
	tPrtNo = request.getParameter("PrtNo");
	tGrpContNo = request.getParameter("GrpContNo");
	tActivityid = request.getParameter("ActivityID");
	tMissionID = request.getParameter("MissionID");
	tSubMissionID  = request.getParameter("SubMissionID");	
        GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
	System.out.println(tGrpContNo+"asdf"+tPrtNo);
%>
<script>
	var PrtNo = "<%=tPrtNo%>";
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
	var ActivityID = "<%=tActivityid%>"
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";	
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="AskGroupUW.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="AskGroupUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./AskUWGrpManuNormChk.jsp" method=post name=fmQuery target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCont);">
    		</td>
    		<td class= titleImg>
    			团体保单信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrpCont" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title8 style="display:'none'">
            团体投保单号码
          </TD>
          <TD  class= input8 style="display:'none'">
            <Input class="readonly" readonly name=GrpContNo >
          </TD>
          <TD  class= title8>
            印刷号码
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=PrtNo>
          </TD>
          <TD  class= title8>
            管理机构
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=ManageCom>
          </TD>

          
          <TD  class= title8>
            销售渠道
          </TD> 
          <TD  class= input8>
          	<Input class="readonly" readonly name=SaleChnl>
          </TD>
                  </TR>
        <TR  class= common8>
          <TD  class= title8>
            中介机构
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=AgentCom>
          </TD>
          <TD  class= title8>
            业务员代码
          </TD>
          <TD  class= input8>
      <Input class="readonly" readonly NAME=AgentCode>
         </TD>
        <TD  class= title8>
            业务员姓名
          </TD>
          <TD  class= input8>
      <Input class="readonly" readonly NAME=AgentName>
         </TD>
                   </TR>
        <TR  class= common>     
          <TD  class= title8 style="display:'none'">
            代理人组别
          </TD>
          <TD  class= input8 style="display:'none'">
            <Input class="readonly" readonly name=AgentGroup>
          </TD>          
      
           <TD  class= title8>
            团体客户号
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=AppntNo >
          </TD>
                     <TD  class= title8>
            团体客户名称
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=AppntName >
          </TD>
      
          <TD  class= title8>
            VIP标记
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=VIPValue >
          </TD>
                    </TR>
        <TR  class= common>    
           <TD  class= title8>
           黑名单标记
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=BlacklistFlag >           
          </TD>          
          </TR>
        </TR>
      </table>
    </Div>
   
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			团体投保单查询结果
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>    	
    </div>
	<br>    
	<hr></hr>
	
	
	<INPUT VALUE = "询价投保单明细信息" Class="cssButton" TYPE=button onclick= "showAskGrpCont();">
	<INPUT VALUE = "既往询价信息" Class="cssButton" TYPE=button onclick= "showAskApp();">
	<INPUT VALUE = "既往理赔信息" Class="cssButton" TYPE=button onclick= "showInsuApp();">
	<INPUT VALUE = "既往保全信息" Class="cssButton" TYPE=button onclick= "showRejiggerApp();">

	<hr></hr>
		<INPUT VALUE = "补充材料回销" Class="cssButton" TYPE=button onclick= "askInfoQ();">
		<INPUT VALUE = "产品定价需求表" Class="cssButton" TYPE=hidden onclick= "showGrpCont();">
		<INPUT VALUE = "产品定价需求表" Class="cssButton" TYPE=button onclick= "showUWAsk();">
  <hr></hr>
  
	<div id=divMPButton style="display:''">   
	<INPUT VALUE = "询价计划变更" Class="cssButton" TYPE=button onclick= "showChangePlan();">
	<INPUT VALUE = "补充材料录入" Class="cssButton" TYPE=button onclick= "askInfo();">
	<INPUT VALUE = "产品定价请求" Class="cssButton" TYPE=button onclick= "InfoRequest();">
  </div>
    <!-- 集体单核保结论 -->
    <div id=divUWIdea style="display:'none'">
    		<table class= common border=0 width=100%>
    				<tr>
									<td class= titleImg align= center>团体保单核保结论：</td>
						</tr>
    		</table>
    		<table  class= common border=0 width=100%>
    		  	<TR  class= common>
    		      <TD  height="29" class= title>
    		        团体保单核保结论
    		        <Input class=codeNo name=GUWState CodeData= "0|^1|拒保^4|通融承保^6|待上级核保^9|正常承保^a|撤销投保单" ondblclick= "showCodeListEx('cond',[this,GUWStateName],[0,1]);" onkeyup="showCodeListKeyEx('cond',[this,GUWStateName],[0,1]);"><input class=codename name=GUWStateName readonly=true > 
    		        <!--input class="code" name=t ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"-->
    		      </TD>
    		    </TR>
    		      <tr></tr>
    		      <TD  class= title>
    		        团体保单核保意见
    		      </TD>
    		      <tr></tr>
    		      <TD  class= input> <textarea name="GUWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
    		</table>
        <INPUT VALUE="团体保单整单确认" Class="cssButton" TYPE=button onclick="gmanuchk();">
        <INPUT VALUE="返回" Class="cssButton" TYPE=button onclick="GoBack();">
   </div>
   <div id=divMPIdea style="display:'none'">
        <INPUT VALUE="团体保单定价确认" Class="cssButton" TYPE=button onclick="MakePrice();">
        <INPUT VALUE="返  回" Class="cssButton" TYPE=button onclick="GoBack();">
   </div>
          <input type="hidden" name= "WorkFlowFlag" value= "">
          <input type="hidden" name= "MissionID" value= "">
          <input type="hidden" name= "SubMissionID" value= "">
          <input type="hidden" name= "PrtNoHide" value= "">
          <input type="hidden" name= "GrpProposalContNo" value= "">
          <input type="hidden" name= "GrpNo" value= "">
          <input type="hidden" name= "GRPNAME" value= "">
          <input type="hidden" name= "ManageCom1" value = "">
          
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
