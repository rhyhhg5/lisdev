<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<%  
 
//得到界面的调用位置,默认为1,表示个人保单直接录入.
// 1 -- 个人投保单直接录入
// 2 -- 集体下个人投保单录入
// 3 -- 个人投保单明细查询
// 4 -- 集体复核
// 5 -- 复核
// 6 -- 查询
// 7 -- 保全新保加人
// 8 -- 保全新增附加险
// 9 -- 无名单补名单
// 10-- 浮动费率
// 13-- 团单复核修改
// 14-- 团单核保修改
// 23-- 承包计划变更
// 16-- 团单明细查询
// 99-- 随动定制

	String tLoadFlag = "";
	String tGrpContNo = "";
	
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		tGrpContNo = request.getParameter( "GrpContNo" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "2";//LoadFlag本身的 意义关键就在于个单部分
	}
	catch( Exception e1 )
	{
		tLoadFlag = "2";
	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
        System.out.println("LoadFlag:" + tLoadFlag);
         System.out.println("扫描类型:" + request.getParameter("scantype"));       
%> 
<script>
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var polNo = "<%=request.getParameter("polNo")%>";
	var ManageCom = "<%=request.getParameter("ManageCom")%>";
	var scantype = "<%=request.getParameter("scantype")%>";
	var MissionID = "<%=request.getParameter("MissionID")%>";
	var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
        var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
        if (ScanFlag == null||ScanFlag=="null") 
        ScanFlag="0";
	if (polNo == "null") polNo = "";
	if (prtNo == "null") prtNo = "";
	var LoadFlag ="<%=tLoadFlag%>";
	//保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
	var BQFlag = "<%=request.getParameter("BQFlag")%>";
	if (BQFlag == null||BQFlag=="null") 
	BQFlag = "0";
	//保全调用会传险种过来
	var BQRiskCode = "<%=request.getParameter("riskCode")%>";
	var currentDate = "<%=PubFun.getCurrentDate()%>";
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AskContPolInit.jsp"%>
  <SCRIPT src="AskContPolInput.js"></SCRIPT>
  <SCRIPT src="AskProposalAutoMove.js"></SCRIPT>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
   
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./AskContPolSave.jsp" method=post name=fm target="fraSubmit">

    <!-- 合同信息部分 AskGroupPolSave.jsp-->

  <DIV id=DivLCContButton STYLE="display:''">
  <table id="table1">
  		<tr>
  			<td>
  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroupPol1);">
  			</td>
  			<td class="titleImg">业务信息
  			</td>
  		</tr>
  </table>
</DIV>
     
<Div  id= "divGroupPol1" style= "display: ''">
   <table  class= common>
      <TR  class= common>
	 
          <!--TD  class= title8>
            集体投保单号码
          </TD>
          <TD  class= input8>
            <Input class="common" name=ProposalGrpContNo readonly TABINDEX="-1"  MAXLENGTH="40">
          </TD-->
          <TD  class= title8>
            印刷号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=PrtNo  TABINDEX="-1" MAXLENGTH="14" verify="印刷号码|notnull&len=11" >
          </TD>
          <TD  class= title8>
            管理机构
          </TD>
          <TD  class= input8>
            <!--<Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">-->
            <Input class="codeNo" name=ManageCom  verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
          </TD>
          <TD  class= title8>
            询价号码
          </TD>
          <TD  class= input8>
            <Input class= readonly name=AskGrpContNo  TABINDEX="-1" MAXLENGTH="14" readonly >
          </TD>
          <!--<Div  id= "div3" style= "display: 'none'">     
          <TD  class= title8>
            印刷号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=PrtNo elementtype=nacessary TABINDEX="-1" MAXLENGTH="14" verify="印刷号码|notnull&len=11" >
          </TD>
          <TD  class= title8>
            管理机构
          </TD>
          <TD  class= input8>
            <!--<Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">-->
            <!--<Input class="code8" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
          </TD>
         </Div>-->
          
      </TR>
        <TR  class= common8>
          <!--<TD  class= title8>
            代理机构
          </TD>
          <TD  class= input8>
            <Input class="code8" name=AgentCom ondblclick="return showCodeList('AgentCom',[this],null,null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this],null,null, fm.all('ManageCom').value, 'ManageCom');">
          </TD>-->
            <TD  class= title8>
            市场类型 
            </TD>
            <TD  class= input>
            <Input class=codeno name=MarketType CodeData="0|^1|商业型^2|结合型^3|其它" ondblclick="return showCodeListEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"verify="市场类型|notnull"><input class=codename name=MarketTypeName readonly=true elementtype=nacessary>
            </TD>
            <TD  class= title8>
            销售渠道
            </TD>
            <TD  class= input8>
            <Input class="codeNo" name=AgentType ondblclick="return showCodeList('salechnl',[this,AgentTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('salechnl',[this,AgentTypeName],[0,1],null,null,null,1);" verify="销售渠道|notnull"><input class=codename name=AgentTypeName readonly=true elementtype=nacessary>
            </TD>
            <TD  class= title8>
            业务员代码
            </TD>
            <TD  class= input8>
      			<Input NAME=AgentCode VALUE="" MAXLENGTH=8 CLASS=code8 elementtype=nacessary ondblclick="return queryAgent();"onkeyup="return queryAgent2();" verify="代理人编码|notnull">
            </TD>
        
        
          <!--TD  class= title8>
            业务员组别
          </TD>
          <TD  class= input8-->
            <Input class="readonly"  type="hidden" readonly name=AgentGroup verify="代理人组别|len<=12" >
          <!--/TD--> 
   
        </TR>

        <TR  class= common>
        	  <TD  class= title8>
            业务员姓名
            </TD>
            <TD  class= input8>
      			<Input NAME=AgentName VALUE=""  CLASS=readonly readonly=true>
            </TD>
        	  <TD  class= title8>
            联系电话
            </TD>
            <TD  class= input8>
            <Input class= readonly  name=AgentPhone  readonly=true>
            </TD> 
           
         </TR>  
            <Input class= common8 type=hidden name=AgentCode1 >
            <Input class= common8 type=hidden name=MissionID >
            <Input class= common8 type=hidden name=SubMissionID >      
            <Input class="common" type=hidden name=ProposalGrpContNo> 
          <!--/TD-->
        
     </table> 
</Div>
<Div  id= "divAgentFee" style= "display: 'none'">
     <table class= common>
         <TR  class= common>
         	 <TD  class= title8>
            中介公司代码
            </TD>
            <TD  class= input8>
            <Input class="code" name=AgentCom  style="display:'none'" ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" >
            </TD>
        	  <TD  class= title8>
            中介代理公司名称
            </TD>
            <TD  class= input8>
            <Input class="common" style="display:'none'"  name=AgentComName readonly >
            </TD>
            <TD  class= title8>
            手续费
            </TD>
            <TD  class= input8>
            <Input class="common"  name=AgentFee>（%）
            </TD>  
          </TR>
          <!--<Div  id= "div4" style= "display: 'none'">
           <TD  class= title8>
            投保申请日期 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" elementtype=nacessary dateFormat="short" name=PolApplyDate verify="投保申请日期|notnull&date" >
          
          </TD> 
          <TD  class= title8>
            保单生效日期 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" elementtype=nacessary dateFormat="short" name=CValiDate    verify="保单生效日期|notnull&date" >
          </TD> 
          <TD CLASS=title>
      			溢交保费方式
    			</TD>
    			<TD CLASS=input COLSPAN=1>
      			<Input NAME=OutPayFlag VALUE="1" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('OutPayFlag', [this]);" onkeyup="return showCodeListKey('OutPayFlag', [this]);">
    			</TD>  
          <!--TD  class= title8 >
            联合代理人代码
          </TD-->
        <!--</Div>-->
          <!--TD  class= input8 -->
      </table>
</Div>

    
<table>
    <tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol2);">
    		  </td>
    		  <td class= titleImg>
    			 团体资料（团体保障号 <Input class= common8 name=GrpNo><INPUT id="butGrpNoQuery" class=cssButton VALUE="查  询" TYPE=button onclick="showAppnt();"> ）(首次投保单位无需填写团体保障号)
    		  </td>
    </tr>
</table>
    
<Div  id= "divGroupPol2" style= "display: ''">
    <table  class= common>
       <TR>
          <TD  class= title8>
            投保单位名称
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpName verify="投保单位名称|notnull" elementtype=nacessary onchange=checkuseronly(this.value) verify="单位名称|notnull&len<=60">
          </TD>  
          <TD  class= title8>
            联系人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1  >
          </TD>   
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8  name=Phone1  >
          </TD>  
        </TR>
        
     <TR  class= common>
          <!--<TD  class= title8>
            单位性质
          </TD>
          <TD  class= input8>
            <Input class=code8 name=GrpNature elementtype=nacessary verify="单位性质|notnull&code:grpNature&len<=10" ondblclick="showCodeList('GrpNature',[this]);" onkeyup="showCodeListKey('GrpNature',[this]);">
          </TD>-->
          <!--
          <TD  class= title8>
            单位地址编码
          </TD>
          <TD  class= input8>
             <Input class="code" name="GrpAddressNo"  ondblclick="getaddresscodedata();return showCodeListEx('GetGrpAddressNo',[this],[0],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetGrpAddressNo',[this],[0],'', '', '', true);">
          </TD>								
           -->
          <Input class=common8 name=GrpAddressNo  type="hidden" >
          <TD  class= title8>
            投保单位地址
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpAddress   >
          </TD>
          <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8 >
            <Input class= common8 name=GrpZipCode verify="邮政编码|len=6" >
          </TD> 
          
      </TR>
        <TR  class= common>
					<TD  class= title8>
            行业类别
          </TD>
          <TD  class= input8>
            <Input class="codeno" name=BusinessType  verify="行业类别|notnull&code:BusinessType&len<=20" ondblclick="return showCodeList('BusinessType',[this,BusinessTypeName],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('BusinessType',[this,BusinessTypeName],[0,1],null,null,null,null,300);"><input class=codename name=BusinessTypeName elementtype=nacessary>
          </TD> 
          <TD  class= title8>
            企业类型
          </TD>
          <TD  class= input8>
            <Input class=codeNo name=GrpNature  verify="单位性质|notnull&code:grpNature&len<=10" ondblclick="showCodeList('GrpNature',[this,GrpNatureName],[0,1]);" onkeyup="showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);"><Input class=codeName name=GrpNatureName  readonly elementtype=nacessary>          
          </TD> 
          
        	  <TD  class= title8>
            员工总人数
            </TD>
            <TD  class= input8>
            <Input class= common8 name=Peoples  elementtype=nacessary verify="单位总人数|int&notnull">
            </TD>
            </TR> 
            <TR  class= common> 
            <TD  class= title8>
            在职人数
            </TD>
            <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=AppntOnWorkPeoples   verify="在职人数|int&notnull">
            </TD>
            <TD  class= title8>
            退休人数
            </TD>
            <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=AppntOffWorkPeoples   verify="退休人数|int&notnull">
            </TD>                         
          <!--<TD  class= title8  >
            保险联系人一
          </TD>-->       
             <TD  class= title8>
        	   其它人员人数
             </TD>
             <TD  class= input8>
             <Input class= common8 elementtype=nacessary name=AppntOtherPeoples   verify="其它人员人数|int&notnull">
             </TD>               
        </TR>
      </table>
<div id ="div1" style="display: ''">
<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol2);">
    		  </td>
    		  <td class= titleImg>
    			 保障方案
    		  </td>
    	</tr>
</table>
</div>

<Div  id= "divGroupPol2" style= "display: ''">
    <table  class= common>
       <TR class= common>
       	  <TD  class= title8>
            被保险人数（成员）
          </TD>
          <TD  class= input8>
          <Input name=Peoples3 class= common8 elementtype=nacessary verify="被保险人数（成员）|int&notnull">
          </TD>           
          <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=OnWorkPeoples  verify="在职人数|int&notnull">
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=OffWorkPeoples   verify="退休人数|int&notnull">
          </TD>
        </TR>
       <TR  class= common>

        	 <TD  class= title8>
        	 其它人员人数
           </TD>
           <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=OtherPeoples   verify="其它人员人数|int&notnull">
           </TD>
           <TD  class= title8>
            连带被保险人数（家属）
           </TD>
           <TD  class= input8>
           <Input name=RelaPeoples elementtype=nacessary class= common8 verify="连带被保险人数（家属）|int&notnull">
           </TD>  
           <TD  class= title8>
            配偶人数
           </TD>
           <TD  class= input8>
           <Input name=RelaMatePeoples elementtype=nacessary class= common8 verify="配偶人数|int&notnull">
           </TD>                  
         </TR>  
         <TR  class= common>             
            <TD  class= title8>
            子女人数
            </TD>
            <TD  class= input8>
            <Input name=RelaYoungPeoples elementtype=nacessary class= common8 verify="子女人数|int&notnull">
            </TD>                                                    
            <TD  class= title8>
            其它人员人数
            </TD>
            <TD  class= input8>
            <Input name=RelaOtherPeoples elementtype=nacessary class= common8 verify="其它人员人数|int&notnull">
            </TD>
            <TD  class= title8>
            保障计划区分标准
            </TD>
            <TD  class= input8>
            <Input class="codeNo" name=EnterKind ondblclick="return showCodeList('enterkind',[this,EnterKindName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('enterkind',[this,EnterKindName],[0,1],null,null,null,1);"><Input class=codeName name=EnterKindName  readonly >
            </TD>
          </TR>
          <TR  class= common>  
          	<TD  class= title8>
            缴费频次
            </TD>
            <TD  class= input8>
      	    	<Input class=codeno name=GrpContPayIntv  verify="缴费频次|notnull" ondblclick="return showCodeList('grpPayIntv',[this,GrpContPayIntvName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('grpPayIntv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"><input class=codename name=GrpContPayIntvName readonly=true elementtype=nacessary>
            </TD>                                                         
          </TR>
          <TR>
            <TD class = title8>
            预估总保费
            </TD>
            <TD class = input8>
            <Input class=common8  name=PremScope elementtype=nacessary verify="预交总保费|int&notnull">
            </TD>
            <TD  class= title8>
            保险责任生效日 
            </TD>
            <TD  class= input8>
            <Input class="coolDatePicker" elementtype=nacessary dateFormat="short" name=CValiDate    verify="保险责任生效日|notnull&date"  onkeyup="return ChangDateFormate(this,this.value)">
            </TD>
            <TD  class= title8>
            保险责任终止日 
            </TD>
            <TD  class= input8>
            <Input class="coolDatePicker" elementtype=nacessary dateFormat="short" name=CInValiDate    verify="保险责任终止日|notnull&date" onkeyup="return ChangDateFormate(this,this.value)">
            </TD>
          </TR> 
      </table> 
          <div id="divBookingPayInty" style="display: 'none'">
    	<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanBookingPayIntyGrid" >
						</span>
					</td>
				</tr>
			</table>
    </div> 
</Div>
<div id="divLCImpart11" style="display:''">					
	  <table class = common>
				<tr class = common>
					<td>
					投保前参保人员医疗保障形式（可多选）.<input type="checkbox" name="ImpartCheck1" verify="投保前参保人员医疗保障形式（可多选）|notnull" class="box">&nbsp;&nbsp;&nbsp;&nbsp;社会基本医疗保险&nbsp;&nbsp;&nbsp;&nbsp;参加年份
					  <input class="common6" name="ImpartCheck1">&nbsp;&nbsp;&nbsp;&nbsp;
					  <input type="checkbox" name="ImpartCheck1" class="box">&nbsp;&nbsp;&nbsp;&nbsp;商业医疗保险&nbsp;&nbsp;&nbsp;&nbsp;
					  <input type="checkbox" name="ImpartCheck1" class="box">&nbsp;&nbsp;&nbsp;&nbsp;单位报销&nbsp;&nbsp;&nbsp;&nbsp;
					  <input type="hidden" name="" class="box">&nbsp;&nbsp;&nbsp;&nbsp;其它					  
					  <input class="common6" name="ImpartCheck1">
				  </td>			
				</tr>
				<tr class = common>
					<td>
					  参保形式.<input type="checkbox" name="ImpartCheck4" verify="参保形式|notnull" class="box" onclick="ImpartCheck4Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;团体统一投保&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  <input type="checkbox" name="ImpartCheck4" class="box" onclick="ImpartCheck4Radio2();">&nbsp;&nbsp;&nbsp;&nbsp;成员自愿投保		  
				  </td>			
				</tr>
				<tr class = common>
					<td>
						缴费主体.<input type="checkbox"  name="ImpartCheck2" verify="缴费主体|notnull" class="box" onclick="ImpartCheck2Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;投保人全额承担&nbsp;&nbsp;&nbsp;&nbsp;
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio2();">&nbsp;&nbsp;&nbsp;&nbsp;被保险人全额承担&nbsp;&nbsp;&nbsp;&nbsp;
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio3();">&nbsp;&nbsp;&nbsp;&nbsp;双方共担，其中投保人承担
						  <input class="common6" name="ImpartCheck2">%,被保险人承担
						  <input class="common6" name="ImpartCheck2">%
					</td>			
				</tr>
				<tr class = common>
					<td>
						职业分类人数.一类<input class="common6" name="ImpartCheck3">人&nbsp;&nbsp;&nbsp;&nbsp;
						             二类<input class="common6" name="ImpartCheck3">人&nbsp;&nbsp;&nbsp;&nbsp;
						             三类<input class="common6" name="ImpartCheck3">人&nbsp;&nbsp;&nbsp;&nbsp;
						             四类<input class="common6" name="ImpartCheck3">人&nbsp;&nbsp;&nbsp;&nbsp;
						             五类<input class="common6" name="ImpartCheck3">人&nbsp;&nbsp;&nbsp;&nbsp;
						             六类<input class="common6" name="ImpartCheck3">人
					</td>			
				</tr>
			</table>
</div>		
 


   
<!--<DIV id=DivLCImpart STYLE="display:''">
<!-- 告知信息部分（列表） 
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
</td>
<td class= titleImg>
团体告知信息
</td>
</tr>
</table>-->

<div  id= "divLCImpart1" style= "display: 'none'">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartGrid" >
</span>
</td>
</tr>
</table>
</div>

<table>
    <tr>
         <td class=common>
             <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart2);">
         </td>
         <td class= titleImg>
         既往保障情况告知
         </td>
    </tr>
</table>
<div  id= "divLCImpart2" style= "display: ''">
     <table  class= common>
          <tr  class= common>
             <td text-align: left colSpan=1>
                <span id="spanHistoryImpartGrid" >
                </span>
             </td>
          </tr>
     </table>
</div>

<table>
    <tr>
         <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart3);">
         </td>

         <td class= titleImg>
         参保人员严重疾病告知
         </td>
         </tr>
</table>
<div  id= "divLCImpart3" style= "display: ''">
       <table  class= common>
             <tr  class= common>
                 <td text-align: left colSpan=1>
                       <span id="spanDiseaseGrid" >
                       </span>
                 </td>
             </tr>
        </table>
</div>
<table class=common>
         <TR  class= common> 
           <TD  class= title8> 特别说明 </TD>
         </TR>
         <TR  class= common>
           <TD  class= title8>
             <textarea name="Remark" cols="110" rows="3" class="common" >
             </textarea>
           </TD>
         </TR>
    
            <Input type=hidden name=EmployeeRate verify="雇员自付比例|num&len<=5">
            <Input type=hidden name=FamilyRate verify="家属自付比例|num&len<=80">
            
            <div  id="div1" style="display:''">
<table  class= common>
       <TR>
           <TD  class= title8>
            提交人签字
           </TD>
           <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=HandlerName verify="提交人签字|notnull">
           </TD>  
           <TD  class= title8>
            信息来源
           </TD>
           <TD  class= input8>
           <Input name=InfoSource elementtype=nacessary class= common8 verify="信息来源|notnull">
           </TD>                  
           <TD  class= title8>
              提交时间 </TD>
           <TD  class= input8>
           <Input name=AgentDate class= "coolDatePicker" elementtype=nacessary dateFormat="short" onkeyup="return ChangDateFormate(this,this.value)" verify="提交时间|notnull&date"> 
           </TD>
        </TR>
        <TR>
         	  <TD  class= title8>
            业务主管审批签字
            </TD>
            <TD  class= input8>
            <Input name=OperationManager elementtype=nacessary class= common8 verify="业务主管审批签字|notnull">
            </TD>
            <TD  class= title8>
            审批时间
            </TD>
            <TD  class= input8>
            <Input name=FirstTrialDate class= "coolDatePicker" elementtype=nacessary dateFormat="short" onkeyup="return ChangDateFormate(this,this.value)" verify="审批时间|notnull&date">
            </TD>
            <TD  class= title8>
            接收人签字
            </TD>
            <TD  class= input8>
            <Input name=ReceiveOperator elementtype=nacessary class= common8 verify="接收人签字|notnull">
            </TD>
            </TR>
        <TR>
            <TD  class= title8>
            接收时间
            </TD>
            <TD  class= input8>
            <Input name=ReceiveDate class= "coolDatePicker" elementtype=nacessary dateFormat="short" onkeyup="return ChangDateFormate(this,this.value)" verify="接收时间|notnull&date">
            </TD>
         </TR>                                                                          
</table>
</div>
            
      <Div  id= "divButton" style= "display: ''" align= left >
 	<!--INPUT class=cssButton VALUE="查 询"  TYPE=button onclick="queryClick()"> 
 	<INPUT class=cssButton VALUE="修 改"  TYPE=button onclick="updateClick()"> 
 	<INPUT class=cssButton VALUE="删 除"  TYPE=button onclick="deleteClick()"> 
 	<INPUT class=cssButton VALUE="保 存"  TYPE=button onclick="submitForm();"--> 
 	<%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>   
   </DIV>   
</table>

<Div  id= "divhiddeninfo" style= "display: 'none'"> 
	<!--Yangming 添加 需求变更，页面调整-->
	<TR   class= common>
        	  <TD  class= title8>
            员工总人数
            </TD>
            <TD  class= input8>
            <Input class= common8 name=Peoplesq  elementtype=nacessary verify="单位总人数|int">
            </TD>
            <TD  class= title8>
            在职人数
            </TD>
            <TD  class= input8>
            <Input class= common8 name=AppntOnWorkPeoplesq   verify="在职人数|int">
            </TD>
            <TD  class= title8>
            退休人数
            </TD>
            <TD  class= input8>
            <Input class= common8 name=AppntOffWorkPeoplesq   verify="退休人数|int">
            </TD>                         
          <!--<TD  class= title8  >
            保险联系人一
          </TD>-->       
        </TR>
        <TR  class= common>
        	   <TD  class= title8>
        	   其它人员人数
             </TD>
             <TD  class= input8>
             <Input class= common8 name=GrpOtherPeoples   verify="其它人员人数|int">
             </TD>               
          <!--<TD  class= title8>
            姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1 elementtype=nacessary verify="保险联系人一姓名|notnull&len<=10">
          </TD>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone1 elementtype=nacessary verify="保险联系人一联系电话|notnull&NUM&len<=30">
          </TD-->
          <TD  class= title8>
            E-MAIL
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail1 verify="保险联系人一E-MAIL|len<=60&Email">
          </TD>           
        </TR>
     
        <!--TR  class= common>
          <TD  class= title8>
            保险联系人二
          </TD>       
        </TR-->
        
        <TR  class= common>
          <TD  class= title8>
            姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan2 verify="保险联系人二姓名|len<=10">
          </TD>
          <!--TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone2 verify="保险联系人二联系电话|NUM&len<=30">
          </TD>
          <TD  class= title8>
            E-MAIL
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail2 verify="保险联系人二E-MAIL|len<=60&Email">
          </TD>
        </TR>     
        <TR  class= common>
          <TD  class= title8>
            付款方式
          </TD>
          <TD  class= input8>
            <Input class="code8" name=GetFlag verify="付款方式|code:PayMode" ondblclick="return showCodeList('PayMode',[this]);" onkeyup="return showCodeListKey('PayMode',[this]);">
          </TD>
          <TD  class= title8>
            开户银行
          </TD>
          <TD  class= input8>
            <Input class=code8 name=BankCode verify="开户银行|code:bank&len<=24" ondblclick="showCodeList('bank',[this]);" onkeyup="showCodeListKey('bank',[this]);">
          </TD>
          <TD  class= title8>
            帐号
          </TD>
          <TD  class= input8>
            <Input class= common8 name=BankAccNo verify="帐号|len<=40">
          </TD>-->       
        </TR>
	<TD  class= title8>
            单位总机
          </TD>
          <TD  class= input8>
            <Input class= common8  name=Phone elementtype=nacessary verify="单位总机|len<=30">
          </TD>
      
          <TD  class= title8>
            单位传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax >
          </TD>
          <TD  class= title8>
            成立时间
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" dateFormat="short" name=FoundDate >
          </TD>       
        </TR>
          <TR>
<table class= common>
	<TR class=common>
	  <TD  class= title8>
            销售渠道
          </TD> 
          <TD  class= input8>
            <!--<Input class="readonly" readonly name=SaleChnl verify="销售渠道|code:SaleChnl&notnull" >-->
          	<Input class="code8"  name=SaleChnl elementtype=nacessary  ondblclick="return showCodeList('SaleChnl',[this]);" onkeyup="return showCodeListKey('SaleChnl',[this]);">
          </TD>
        </TR>
<TR  class= common>
          <TD  class= title8>
            注册资本金
          </TD>
          <TD  class= input8>
            <Input class= common8 name=RgtMoney verify="注册资本金|num&len<=17">
          </TD>
          <TD  class= title8>
            资产总额
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Asset verify="资产总额|num&len<=17">
          </TD>
          <TD  class= title8>
            净资产收益率
          </TD>
          <TD  class= input8>
            <Input class= common8 name=NetProfitRate verify="净资产收益率|num&len<=17">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            主营业务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=MainBussiness verify="主营业务|len<=60">
          </TD>
          <TD  class= title8>
            单位法人代表
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Corporation verify="单位法人代表|len<=20">
          </TD>
          <TD  class= title8>
            机构分布区域
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ComAera verify="机构分布区域|len<=30">
          </TD>       
        </TR>
                <TR  class= common>
           <TD  class= title8>
            部门
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Department1 verify="保险联系人一部门|len<=30">
          </TD>     
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax1 verify="保险联系人一传真|len<=30">
          </TD>       
        </TR>
                <TR  class= common>
          <TD  class= title8>
            职务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=HeadShip1 verify="保险联系人一职务|len<=30">
          </TD>                
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax2 verify="保险联系人一传真|len<=30">
          </TD>       
        </TR>
         <TR  class= common>
          <TD  class= title8>
            币种
          </TD>
          <TD  class= input8>
            <Input class=code8 name=Currency verify="币种|len<=2" ondblclick="showCodeList('currency',[this]);" onkeyup="showCodeListKey('currency',[this]);">
          </TD>
                 
        </TR>     
        <tr class=common>
        <td class= titleImg>
    			
    		</td>
        </TR> 
      </table>
</DIV>   

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol3);">
    		</td>
    		<td class= titleImg>
    			 团体保单险种信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGroupPol3" style= "display: ''">
    <Div  id= "divGroupPol5" style= "display: 'none'">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
            集体合同号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GrpContNo  >
          </TD>
    
        </TR>
  </table>   
  </Div>     
  <!--录入的暂交费表部分 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanRiskGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
    <table> 
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol4);">
    		</td>
    		<td class= titleImg>
    			 险种信息
    		</td>
    		
    	</tr>
    </table>
<Div  id= "divGroupPol4" style= "display: ''">
      <table  class= common>
      <TR class=common>
      <TD  class= title8>
        险种编码
      </TD>
      <TD  class= input8>
        <Input class=code8 name=RiskCode  ondblclick="return showCodeList('RiskGrp',[this]);" onkeyup="return showCodeListKey('RiskGrp',[this]);">
      </TD>
      <!--TD  class= title8>
        交费周期
      </TD>
      <TD  class= input8>
        <Input class=code8 name=PayIntv  ondblclick="return showCodeList('RiskPayIntv',[this],null,null,fm.RiskCode.value,'a.RiskCode');">
      </TD-->
      <TD  class= title8> 
     保费计算方式
      </TD>
      <TD  class= input8>
        <Input class=codeNo name=CalRule CodeData="0|^0|表定费率^2|表定费率折扣^3|约定费率^4|约定差异费率"  ondblclick="return showCodeListEx('CalRule',[this,CalRuleName],[0,1],null,null,null,1);" ><input class=codename name=CalRuleName readonly=true elementtype=nacessary> 
      </TD>    
     	<!--TD  class= title>
        应保人数
      </TD>
      <TD  class= input8>
        <Input class=common name=ExpPeoples  >
      </TD--> 
    </TR>        
     <Div  id= "divRiskDeal" style= "display: ''">
	    <table>
		    <TR >     
		     <TD >
		      <input type =button class=cssButton value="添加险种"  name=AddRiskCode onclick="addRecord();">      
		     </TD>
		     <TD >
		      <input type =button class=cssButton value="删除险种" name=DeleteRiskCode onclick="deleteRecord();">      
		     </TD>
		    </TR> 
	    </table> 
		</Div>  
   </TD>     
</TR>
       </table>
</Div>
</DIV>
    
<Div  id= "divHidden" style= "display: 'none'">
  <table class=common>
      <TR  class= common>          
          <TD  class= title>
            封顶线
          </TD>
          <TD  class= input>
            <Input class= common name=PeakLine >
          </TD>
        </TR>
        <TR  class= common>          
          <TD  class= title>
            医疗费用限额
          </TD>
          <TD  class= input>
            <Input class= common name=MaxMedFee >
          </TD>
        </TR>
      </table>  
  </Div>    
    <hr/>
    <Div  id= "divnormalbtn" style= "display: ''" > 
            
      <INPUT class=cssButton VALUE="保障计划定制"  TYPE=button onclick="grpRiskPlanInfo()">
    <div  id="div5" style="display:'none'">
      <input type =button class=cssButton value="添加被保人" onclick="grpInsuInfo();">
      <INPUT class=cssButton VALUE="被保人清单"  TYPE=button onclick="grpInsuList()">
    </div>
      <!--<INPUT class=cssButton VALUE="分单定制"  TYPE=button onclick="grpSubContInfo()">-->
     <INPUT class=cssButton VALUE="年龄分布"  TYPE=button onclick="grpPersonAge()">
 </Div>
    <Div  id= "divapprovenormalbtn" style= "display: 'none'" > 
      <INPUT class=cssButton VALUE="保险计划定制"  TYPE=button onclick="grpRiskPlanInfo()">   
      <INPUT class=cssButton VALUE="被保人清单"  TYPE=button onclick="grpInsuList()">
      <!--<INPUT class=cssButton VALUE="分单定制"  TYPE=button onclick="grpSubContInfo()">-->
    </Div>
         <Div  id= "divnormalquesbtn11" style= "display: 'none'" align= right> 
           <INPUT class=cssButton VALUE="险种信息"  TYPE=button onclick="grpFeeInput()"> 
             </div>   
     <Div  id= "divnormalquesbtn" style= "display: 'none'" align= right> 
      <hr/>
           
      <INPUT VALUE="重新询价"    class=cssButton TYPE=button onclick="GrpInputReConfirm();">
      <INPUT VALUE="录入完毕"    class=cssButton TYPE=button onclick="GrpInputConfirm(1);">
    <div  id="div6" style="display:'none'">
      <INPUT VALUE="团体问题件查询"    class=cssButton TYPE=button onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件录入"    class=cssButton TYPE=button onclick="GrpQuestInput();">
    </div>
      </Div>
      <!--如果是从扫描录入进来，则显示下面按钮-->
     </Div>
     <Div  id= "divnormalquesbtn2" style= "display: 'none'" align= right> 
      <hr/>            
      <INPUT VALUE="重新询价"    class=cssButton TYPE=button onclick="GrpInputReConfirm();">
      <INPUT VALUE="录入完毕"    class=cssButton TYPE=button onclick="GrpInputConfirm(100);">
    <div  id="div62" style="display:'none'">
      <INPUT VALUE="团体问题件查询"    class=cssButton TYPE=button onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件录入"    class=cssButton TYPE=button onclick="GrpQuestInput();">
    </div>
      </Div>
      
      <Div  id= "divapprovebtn" style= "display: 'none'" align= right> 
      <hr/>            
      <INPUT VALUE="团体复核通过" class=cssButton TYPE=button onclick="gmanuchk();">      
      <INPUT VALUE="团体问题件查询"    class=cssButton TYPE=button onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件录入"    class=cssButton TYPE=button onclick="GrpQuestInput();">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div>
      <Div  id= "divapproveupdatebtn" style= "display: 'none'" align= right> 
      <hr/>
      <INPUT VALUE="修  改" class=cssButton TYPE=button onclick="updateClick();">            
      <INPUT VALUE="复核修改确认" class=cssButton TYPE=button onclick="GrpInputConfirm(2);">    
      <INPUT VALUE="团体问题件查询"    class=cssButton TYPE=button onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件录入"    class=cssButton TYPE=button onclick="GrpQuestInput();">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div>
      <Div  id= "divuwupdatebtn" style= "display: 'none'" align= left> 
      <hr/>          
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div>
      <Div  id= "AskChangePlan" style= "display: 'none'" align= right>   
      <hr/> 
      <INPUT VALUE="修  改" class=cssButton TYPE=button onclick="updateClick();">
      </Div>
      <Div  id= "divquerybtn" style= "display: 'none'" align= right> 
      <hr/>  
      <INPUT VALUE="团体问题件查询"    class=cssButton TYPE=button onclick="GrpQuestQuery();">        
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div> 
      <div id="autoMoveButton" style="display: none">
	<input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove('099');" class=cssButton>
	<input type="button" name="Next" value="下一步" onclick="location.href='AskContInsuredInput.jsp?LoadFlag='+LoadFlag+'&prtNo='+prtNo+'&scantype='+scantype" class=cssButton>	
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="top.close();">       
        <input type=hidden id="" name="autoMoveFlag">
        <input type=hidden id="" name="autoMoveValue">   
        <input type=hidden id="" name="pagename" value="121">                         
      </div>         
      <input type=hidden id="fmAction" name="fmAction">  
      <input type=hidden id="" name="WorkFlowFlag">      
      <!--INPUT class=cssButton VALUE="导入被保人清单"  TYPE=button onclick=""-->   
      <!--<INPUT class=cssButton VALUE="进入保单险种信息"  TYPE=button onclick="grpRiskInfo()">-->   
  		
  		<input type=hidden name=LoadFlag>
  		<input type=hidden name=AppFlag value= "8">
  	
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
