<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuRReportChk.jsp
//程序功能：询价通知书发送
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.ask.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
  	// 投保单列表
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	LCGrpContSet tLCGrpContSet = new LCGrpContSet();

	
	String tGrpContNo = request.getParameter("GrpContNo");
	String MissionID = request.getParameter("MissionID");
	String SubMissionID = request.getParameter("SubMissionID");
	
	System.out.println("GrpContNo:"+tGrpContNo);
	System.out.println("MissionID:"+MissionID);
	tLCGrpContSchema.setGrpContNo(tGrpContNo);
	tLCGrpContSet.add(tLCGrpContSchema);

	
try
{
  	TransferData mTransferData = new TransferData();
  	mTransferData.setNameAndValue("MissionID",MissionID);
  	mTransferData.setNameAndValue("SubMissionID",SubMissionID);
  	//mTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
  	//mTransferData.setNameAndValue("PrtNo", request.getParameter("PrtNo"));
   // mTransferData.setNameAndValue("AgentCode", request.getParameter("AgentCode"));
   // mTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
   // mTransferData.setNameAndValue("GrpNo", request.getParameter("GrpNo"));
   // mTransferData.setNameAndValue("GrpName","1234");
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpContSet);
		tVData.add( mTransferData);
		tVData.add( tG );
		
		// 数据传输
		AskWorkFlowUI tAskWorkFlowUI   = new AskWorkFlowUI();
		if (tAskWorkFlowUI.submitData(tVData,"0000006016") == false)
		{
			int n = tAskWorkFlowUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			Content = " 发询价跟踪通知书失败，原因是: " + tAskWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tAskWorkFlowUI.mErrors;
		    //tErrors = tAskWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 发询价跟踪通知书成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 发询价跟踪通知书失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    
		    }
		}
	
}
catch(Exception e)
{
	
}
%>              
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
