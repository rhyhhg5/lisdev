<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuHealthChk.jsp
//程序功能：承保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.ask.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tGlobalInput = new GlobalInput();
  tGlobalInput=(GlobalInput)session.getValue("GI");	  
  if(tGlobalInput == null) {
	out.println("session has expired");
	return;
  }

       
  	// 投保单列表
	LCInformationSchema tLCInformationSchema = new LCInformationSchema();
	LCInformationSet tLCInformationSet = new LCInformationSet();
	LCInformationItemSet tLCInformationItemSet = new LCInformationItemSet();
	
	String tGrpContNo = request.getParameter("GrpContNo");
	String tPrtNo = request.getParameter("PrtNo");
	
	
	String tSerialNo[] = request.getParameterValues("HealthGridNo");
	String thealthcode[] = request.getParameterValues("HealthGrid1");
	String thealthname[] = request.getParameterValues("HealthGrid2");
	
	String tIfCheck[] = request.getParameterValues("HealthGrid3");
	
	
	System.out.println("GrpContNo:"+tGrpContNo);
	System.out.println("PtrNo:"+tPrtNo);
	//System.out.println("tSerialNo[]:"+tSerialNo[1]);
	
	boolean flag = true;
	int ChkCount = 0;
	if(tSerialNo != null)
	{		
		ChkCount = tSerialNo.length;
	}
	System.out.println("count:"+ChkCount);
	if (ChkCount == 0 )
	{
		Content = "体检资料信息录入不完整!";
		FlagStr = "Fail";
		flag = false;
		System.out.println("111");
	}
	else
	{
		System.out.println("222");
	    //体检资料一
		tLCInformationSchema.setGrpContNo(tGrpContNo);
		tLCInformationSchema.setPrtNo(tPrtNo);	    		
	    	tLCInformationSet.add(tLCInformationSchema);

	    	System.out.println("chkcount="+ChkCount);
	    	if (ChkCount > 0)
	    	{
	    		for (int i = 0; i < ChkCount; i++)
			{
				if (!thealthcode[i].equals(""))
				{
		  			LCInformationItemSchema tLCInformationItemSchema = new LCInformationItemSchema();
		  			tLCInformationItemSchema.setGrpContNo(tGrpContNo);
					tLCInformationItemSchema.setInformationCode( thealthcode[i]);
					tLCInformationItemSchema.setInformationName( thealthname[i]);
					tLCInformationItemSchema.setRemark( tIfCheck[i]);	   	    
			    	tLCInformationItemSet.add( tLCInformationItemSchema );
			        System.out.println("i:"+i);
	    		    System.out.println("healthcode:"+thealthcode[i]);	    	
			   		flag = true;
				}
			}
			    
		}
		else
		{
			Content = "传输数据失败!";
			flag = false;
		}
	}
	
	System.out.println("flag:"+flag);
  	if (flag == true)
  	{
		// 准备传输数据 VData
	   VData tVData = new VData();
	   
     TransferData mTransferData = new TransferData();
     mTransferData.setNameAndValue("MissionID", request.getParameter("MissionID"));
     mTransferData.setNameAndValue("SubMissionID", request.getParameter("SubMissionID"));
     mTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
     mTransferData.setNameAndValue("PrtNo", request.getParameter("PrtNo"));
     //mTransferData.setNameAndValue("AgentCode", request.getParameter("AgentCode"));
    // mTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
    // mTransferData.setNameAndValue("GrpNo", request.getParameter("GrpNo"));
   //  mTransferData.setNameAndValue("GrpName","1234");
     
	   tVData.add(tGlobalInput);
	   tVData.add(tLCInformationSet);
	   tVData.add(tLCInformationItemSet);
	   tVData.add(mTransferData);
	   AskWorkFlowUI tAskWorkFlowUI = new AskWorkFlowUI();
		if (tAskWorkFlowUI.submitData(tVData,"0000006007") == false)
		{
			int n = tAskWorkFlowUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			Content = " 自动核保失败，原因是: " + tAskWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tAskWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 人工核保成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 人工核保失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
		
	} 
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
