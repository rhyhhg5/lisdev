//程序名称：GroupMainUW.js
//程序功能：集体人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var cPrtNo = "";
var cGrpContNo = "";
	
var cMissionID = "";
var cSubMissionID = "";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    alert(content);
  }
  else
  { 

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    alert(content);
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
/*function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}*/
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//查询集体单
function querygrp()
{
	// 初始化表格                                                                                                                                                           
	//initInpBox();
    //initPolBox();	
	initGrpGrid();	
	
	// 书写SQL语句
	var str = "";	
	var mOperate = fm.all('QOperator').value;	
	
	var strsql = "";
	var strsql2 = "";
	/*
	strsql = "select a.GrpContNo,a.PrtNo,a.GrpName,a.ManageCom from LCGrpCont a where 1=1 "
				 + getWherePart( 'a.ManageCom','QManageCom' )
				 + getWherePart( 'a.AgentCode','QAgentCode' )
				 + getWherePart( 'a.AgentGroup','QAgentGroup' )
				  
				 + getWherePart( 'a.PrtNo','QPrtNo')
				 + " and a.AppFlag='0'"
				 + " and a.ManageCom like '" + manageCom + "%%'"
                 + " and ( select distinct 1 from LCGCUWMaster where  Operator='"+mOperate+"'  and GrpContNo = a.GrpContNo    and appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"')   or  appgrade is null   ) = 1  ";
			
   if(fm.QState.value=='1')//团体单处于未人工核保处
	{ strsql2 = " and a.ApproveFlag in('9')"
       	         + " and a.UWFlag in ('5')"
       	         + " and (select distinct 1 from lcpol,lcuwmaster  where "
      	         +        " lcpol.PrtNo=a.PrtNo "
       	         +        " and lcpol.proposalno=lcuwmaster.proposalno"
       	         +        " and (lcpol.appflag <> '0' or lcpol.approveflag <> '9' or LCUWMaster.SpecFlag <> '0' or LCUWMaster.HealthFlag <>'0')"
       	         +      ") is null";
			
	}
	if(fm.QState.value=='2')//团体单处于核保已回复处							 				 
	{
		 strsql2 = " and a.ApproveFlag in('9')"
                 +  " and a.UWFlag not in ('0','1','4','9')"
       	         +  " and (select distinct 1 from lcpol,lcuwmaster  where "
       	         +        " lcpol.PrtNo=a.PrtNo "
       	         +        " and lcpol.proposalno=lcuwmaster.proposalno"
       	         +        " and(LCUWMaster.SpecFlag = '1' "//团体下个人单核保主表中该字段改为特约
                 +        " or LCUWMaster.healthflag='1')"
       	         +      ") is null "
                 +  " and  a.prtno  in (select distinct (prtno) from lcgrppol where GrpProposalNo in  " //团体下个人单有已回复的特约或体检件
       	         +      "((select distinct(lcpol.grppolno) from lcpol,lcuwmaster  where "
       	         +      " lcpol.proposalno=lcuwmaster.proposalno"
       	         +      " and(LCUWMaster.SpecFlag = '2' "//团体下个人单核保主表中该字段改为特约
                 +      " or LCUWMaster.healthflag='2')"
       	         +      ") union (select distinct(proposalno) from LCIssuePol where replyman is not null)))";
	}
	if(fm.QState.value=='3')//团体单处于核保未回复处
       {
       	 strsql2 = " and a.UWFlag not in ('0','1','4','9')"
                 + " and (a.ApproveFlag in('2')"
       	         + " or (select distinct 1 from lcpol,lcuwmaster  where "
       	         +        " lcpol.PrtNo=a.PrtNo "
       	         +        " and lcpol.proposalno=lcuwmaster.proposalno"
       	         +        " and( LCUWMaster.SpecFlag = '1'"//团体下个人单核保主表中该字段改为用预留
                 +        " or LCUWMaster.HealthFlag ='1')"
       	         +   ") = 1 )";  
        }
        
	strsql = strsql + strsql2;
	strsql = strsql +" order by  a.GrpContNo ";
	*/
 //团单暂时没有核保状态
 /*
  strsql = "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop7,lwmission.missionprop4,lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
				 + " and activityid = '0000002004' "
				 + " and processid = '0000000004'"
				 + getWherePart('missionprop1','QGrpProposalNo')
				 + getWherePart('missionprop2','QPrtNo')
				 + getWherePart('missionprop5','QAgentCode')
				 + getWherePart('missionprop4','QManageCom')
				 + " and LWMission.MissionProp4 like '" + comcode + "%%'";  //集中权限管理体现	
				 + " order by lwmission.missionprop2"
					;
*/
	strSQL = "select lwmission.missionprop1,lwmission.missionprop2,a.name,lwmission.missionprop4,b.GrpName,lwmission.missionid,lwmission.submissionid from lwmission,laagent a,ldgrp b where"
	           + " processid = '0000000006' " 
             + " and activityid = '0000006006' "
             + " and lwmission.missionprop3=a.agentcode "
             + " and lwmission.missionprop5=b.customerno " 
             + getWherePart('lwmission.missionprop3','QAgentCode')
             + getWherePart('lwmission.missionprop4','QManageCom','like')
             + getWherePart('lwmission.missionprop2','QPrtNo');
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件集体单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


//
function CallGroupUW(parm1,parm2)
{
	
	if(fm.all(parm1).all('InpGrpGridSel').value == '1' )
	{
		//当前行第1列的值设为：选中
   		cPrtNo = fm.all(parm1).all('GrpGrid2').value;	//流水号
   		cGrpContNo = fm.all(parm1).all('GrpGrid1').value;	//集体合同号
   		cMissionID = fm.all(parm1).all('GrpGrid6').value;
   		cSubMissionID = fm.all(parm1).all('GrpGrid7').value;
   		//alert(cGrpContNo);
   	}
 }  	
 
function PepoleUW()
{
	var tSel = GrpGrid.getSelNo();
		
  cPrtNo = GrpGrid.getRowColData(tSel - 1,2);	//流水号
  cGrpContNo =GrpGrid.getRowColData(tSel - 1,1);//集体合同号
  cMissionID = GrpGrid.getRowColData(tSel - 1,6);
  cSubMissionID = GrpGrid.getRowColData(tSel - 1,7);
	if(cPrtNo == null || cPrtNo == "" || cGrpContNo == null || cGrpContNo == "")
	   	{
	   		alert("请选择一条有效的团体单!");
	   	}
	   	else
	   	{	   		
	   		window.open("./AskGroupTrackSubMain.jsp?GrpContNo="+cGrpContNo+"&PrtNo="+cPrtNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window3");
	   	}	
}	
	
		

	   	
   	
