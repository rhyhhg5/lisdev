//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
var tSearch = 0;
var arrRisk = new Array();
var arrDuty = new Array();
var arrCode = new Array();
var PlanCode;
window.onfocus=myonfocus;
/*************************************
/**询价保单操作来源判断
**************************************
*/
function initButtons(){
	if(ActivityID=='0000006004'){//人工核保
		fm.MPSave.disabled = true;
		fm.PlanTypeNew.value='4';
		
	}else if(ActivityID='0000006011'){//产品定价
		fm.UWSave.disabled = true;
		fm.UWModify.disabled = true;
		fm.PlanTypeNew.value='5';
	}
}

/***************************************
**核保师“保存”按钮事件
****************************************
*/
function UWPlanSave()
{
	var strSQL = "";
	strSQL = "select noteid from LCAskUWNote where 1=1 "
	       +" and stateflag<>'3' "
				 + " and GrpContNo='"+fm.GrpContNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if(turnPage.strQueryResult)
	{ 
		 alert("有未回复的信息，不允许发送新的核保意见！");
		 return;
	} 
	if(fm.all('UWNote').value==""){
		alert("核保意见不许为空！");
		return;
		}
	
	fm.all('mOperate').value = 'INSERT||MAIN';
	fm.all('OperFlag').value = 'SAVEUWPLAN';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}
function UWPlanModify()
{
	if(AskUWNoteGrid.getSelNo()==""){
		alert("请选中一条记录");
		return;
		}
	if(fm.all('UWNote').value==""){
		alert("核保意见不许为空！");
		return;
		}
		 var cstrSQL  = "select noteid from LCAskUWNote where 1=1 "
         + " and stateflag='0' "
         + " and noteid='"+AskUWNoteGrid.getSelNo()+"'"
				 + " and grpcontno = '"+fm.GrpContNo.value+"'";
	   turnPage.strQueryResult  = easyQueryVer3(cstrSQL, 1, 0, 1);
	   if(!turnPage.strQueryResult)
	     { 
		     alert("只有未发送状态的记录可以修改！");
		     return;
	     }
//		var flag=AskUWNoteGrid.getRowColData(AskUWNoteGrid.getSelNo() - 1,5);
//	if(flag!="未发送")
//	 {
//		alert("只有未发送状态的记录可以修改！");
//		return;
//		}
	fm.all('NoteID').value =AskUWNoteGrid.getSelNo();
	fm.all('mOperate').value = 'UPDATE||UWPLAN';
	fm.all('OperFlag').value = 'UPDATEUWPLAN';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}
/***************************************
**产品定价“保存”按钮事件
****************************************
*/
function MPPlanSave(){
	if(AskUWNoteGrid.getSelNo()==""){
		alert("请选中一条记录");
		return;
		}
	if(fm.all('MPNote').value=="")
	{
		alert("产品意见不能为空！");
		return;
		}
	fm.all('NoteID').value =AskUWNoteGrid.getSelNo();
	fm.all('mOperate').value = 'UPDATE||MAIN';
	fm.all('OperFlag').value = 'SAVEPROPLAN';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

function afterCodeSelect( cCodeName, Field ){
	//判定双击操作执行的是什么查询
	if (cCodeName=="GrpRisk"){
		var tRiskFlag = fm.all('RiskFlag').value;
		//由于附加险不带出主险录入框，因此判定当主附险为S的时候隐藏
		if (tRiskFlag!="S"){
			divmainriskname.style.display = 'none';
			divmainrisk.style.display = 'none';
			fm.all('MainRiskCode').value = fm.all('RiskCode').value;
		}
		else{
			divmainriskname.style.display = '';
			divmainrisk.style.display = '';
			fm.all('MainRiskCode').value = "";
		}
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
	parent.fraMain.rows = "0,0,0,0,*";
}

//数据查询

//数据提交（保存）
function submitForm(){
	if (!beforeSubmit()){
		return false;
	}
	fm.all('mOperate').value = "INSERT||MAIN";
	if (fm.all('mOperate').value == "INSERT||MAIN"){
		if (!confirm('计划 '+fm.all('ContPlanCode').value+' 下的全部险种要素信息是否已录入完毕，您是否要确认操作？')){
			return false;
		}
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	QueryCount = 0;	//重新初始化查询次数
	fm.submit(); //提交
}

//返回上一步
function returnparent(){
	parent.close();
}

//数据提交（删除）
function afterSubmit(FlagStr,content){
	showInfo.close();
	if( FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		content = "操作成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//	    initContPlanGrid();
	    tSearch = 0;
	    QueryCount = 0;
	}
	fm.all('mOperate').value = "";
	fm.all('UWNote').value = "";
	fm.all('MPNote').value = "";
	easyQueryClick();
}

/*********************************************************************
 *  查询险种下责任
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick(){
	initAskUWNoteGrid();
	
	//查询该险种下的险种计算要素	
	strSQL = "select UWoperator,makedate,Prooperator,(case when int(stateflag)>1 then modifydate end ), "
	  +"(case when stateflag='0' then '未发送' when stateflag='1' then '未回复' when stateflag='2' then '回复中' when stateflag='3' then '已回复' end ) "
		+ ", UWplan, proplan from LCaskUWnote "
		+ "where 1=1 "
		+ "and GrpContNo = '"+fm.all('GrpContNo').value+"' order by int(noteid)";
		//alert(strSQL);
	//fm.all('ContPlanName').value = strSQL;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//如果没数据也无异常
	if (!turnPage.strQueryResult) {
		//return false;
	}
	else{
		//QueryCount = 1;
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = AskUWNoteGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
	}
	//不管前面的查询什么结果，都执行下面的查询
	//如果发生参数错误，导致查询失败，这里程序会出错
  strSQL = "select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LCGrpCont where GrpContNo = '" +GrpContNo+ "'";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.all('GrpContNo').value = turnPage.arrDataCacheSet[0][0];
	fm.all('ProposalGrpContNo').value = turnPage.arrDataCacheSet[0][1];
	fm.all('ManageCom').value = turnPage.arrDataCacheSet[0][2];
	fm.all('AppntNo').value = turnPage.arrDataCacheSet[0][3];
	fm.all('GrpName').value = turnPage.arrDataCacheSet[0][4];
	
}

//单选框点击触发事件
function ShowContPlan(parm1,parm2){
	if(fm.all(parm1).all('InpContPlanCodeGridSel').value == '1'){
		//当前行第1列的值设为：选中
		var cContPlanCode = fm.all(parm1).all('ContPlanCodeGrid1').value;	//计划编码
		var cContPlanName = fm.all(parm1).all('ContPlanCodeGrid2').value;	//计划名称
		var cPlanSql = fm.all(parm1).all('ContPlanCodeGrid3').value;	//分类说明
		fm.all('ContPlanCode').value = cContPlanCode;
		fm.all('ContPlanName').value = cContPlanName;
		fm.all('PlanSql').value = cPlanSql;
		//alert(cContPlanCode);
		var cGrpContNo = fm.all('GrpContNo').value;

		//查询该险种下的险种计算要素
		strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,d.CalFactorValue,d.Remark,b.RiskVer,d.GrpPolNo,d.MainRiskCode,d.CalFactorType,c.CalMode "
			+ "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCContPlanDutyParam d "
			+ "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
			+ "and a.DutyCode = d.DutyCode and a.CalFactor = d.CalFactor and b.RiskVer = d.RiskVersion "
			+ "and ContPlanCode = '"+fm.all('ContPlanCode').value+"'"
			+ "and GrpContNO = '"+GrpContNo+"' order by a.RiskCode,d.MainRiskCode,a.DutyCode";
		//fm.all('PlanSql').value = strSQL;
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//原则上不会失败，嘿嘿
		//alert("===========================");
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = ContPlanGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
		QueryCount = 1;
		tSearch = 1;
	}
}

//计划变更则修改查询状态变量

/*********************************************************************
 *  查询计划下险种信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

/*
********************************************************
**核保师“保存”按钮事件业务逻辑处理
********************************************************
*/
function SaveRemark()
{
	fm.all('OperFlag').value = 'remark';
	fm.all('RiskFlag').value = 'UW';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}
/*
************************************************************
//产品定价“保存”按钮事件业务逻辑处理
************************************************************
*/
function SaveRemark2()
{
	fm.all('OperFlag').value = 'remark';
	fm.all('RiskFlag').value = 'MP';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}
/*********************************************************************
 *  险种定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveRiskFactor()
{
	fm.all('OperFlag').value = 'risk';
	fm.all('RiskFlag').value = 'UW';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  险种定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveMPRiskFactor()
{
	fm.all('OperFlag').value = 'risk';
	fm.all('RiskFlag').value = 'MP';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  责任定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveDutyFactor()
{
	var tSel = RiskInfoGrid.getSelNo();
	fm.RiskCode.value = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	fm.ContPlanCode.value = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	
	var tDutySel =ContPlanDutyGrid.getSelNo();
	fm.DutyCode.value = ContPlanDutyGrid.getRowColData(tContSel - 1,1);	

	fm.all('OperFlag').value = 'duty';
	fm.all('DutyFlag').value = 'UW';
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  责任定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveMPDutyFactor()
{
	var tSel = RiskInfoGrid.getSelNo();
	fm.RiskCode.value = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	fm.ContPlanCode.value = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	
	var tDutySel =ContPlanDutyGrid.getSelNo();
	fm.DutyCode.value = ContPlanDutyGrid.getRowColData(tContSel - 1,1);	

	fm.all('OperFlag').value = 'duty';
	fm.all('DutyFlag').value = 'MP';
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  将产品险种要素移动到核保险种要素
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function moveRiskFactor()
{
	var lineCount = MPRiskFactorGrid.mulLineCount;
	for(var i=0;i<lineCount;i++)
	{
		RiskFactorGrid.setRowColData(i,7,MPRiskFactorGrid.getRowColData(i,7));
	}
}
/*********************************************************************
 *  将核保意见显示在文本框中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ShowAskUWNoteInfo()
{
  var tSel = AskUWNoteGrid.getSelNo();
	var UWPlan = AskUWNoteGrid.getRowColData(tSel - 1,6);
	var ProPlan = AskUWNoteGrid.getRowColData(tSel - 1,7);
	fm.all.UWNote.value=UWPlan;
	fm.all.MPNote.value=ProPlan;
}