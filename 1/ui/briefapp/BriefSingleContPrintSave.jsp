<%--
    保存简易保单信息 2005-09-05 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.File.*"%>
<%@page import="java.util.*"%>
<%
    String FlagStr="";      //操作结果
    String Content = "";    //控制台信息
    String tAction = "";    //操作类型：delete update insert
    String tOperate = "";   //操作代码
    String mLoadFlag="";
    CErrors tError = null;
    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
    tG.ServerIP = tG.GetServerIP();
    //tG.ClientIP = request.getRemoteAddr();
    tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
    tAction = request.getParameter( "fmAction" );
    String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
	String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
	String tContNo = request.getParameter("mContNo");
	System.out.println("mContNo +"+tContNo);
	String tCardFlag = request.getParameter("mCardFlag");
	String tIntlFlag = request.getParameter("IntlFlag");
	String tPrintServerInterface = (new ExeSQL()).getOneValue("select sysvarvalue from LDSYSVAR where sysvar='PrintServerInterface'");
	System.out.println("数据路径szTemplatePath +"+szTemplatePath);
	System.out.println("数据路径sOutXmlPath +"+sOutXmlPath);
	LCContSchema tLCContSchema = new LCContSchema();
	tLCContSchema.setContNo(tContNo);
	tLCContSchema.setCardFlag(tCardFlag);
	tLCContSchema.setIntlFlag(request.getParameter("IntlFlag"));
	//VData tVData = new VData();
	//TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
    tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );

    // 加入简易件打印时，保险卡与保单的区别标志。
    String tStrPrtFlag = (String)request.getParameter("PrtFlag");
    tTransferData.setNameAndValue("prtFlag", tStrPrtFlag);

    // 判断该保单是否为首次打印。
    boolean isFirstPrint = false;
    String tSql = "select PrintCount from LCCont where ContNo = '" + tContNo + "'";
    String tTmpResult = new ExeSQL().getOneValue(tSql);
    isFirstPrint = "0".equals(tTmpResult);

    //如果是首次打印，生成待合同接收信息。
    if((isFirstPrint && ("6".equals(tCardFlag) || "5".equals(tCardFlag) || "2".equals(tCardFlag))) || "1".equals(request.getParameter("firstPrint")))
    {
        TransferData tTmpTransferData = new TransferData();
        tTmpTransferData.setNameAndValue("ContNo", tContNo);

        VData tTmpVData = new VData();
        tTmpVData.add(tG);
        tTmpVData.add(tTmpTransferData);

        ContReceiveUI tContReceiveUI = new ContReceiveUI();

        if (!tContReceiveUI.submitData(tTmpVData, "CreateReceive"))
        {
            if (tContReceiveUI.mErrors.needDealError())
            {
                System.out.println(tContReceiveUI.mErrors.getFirstError());
            }
        }
        else
        {
            // 如果成功生成接收轨迹，目前银保险种需要自动进行合同接收。
            if (!tContReceiveUI.submitData(tTmpVData, "ReceiveCont"))
            {
                if (tContReceiveUI.mErrors.needDealError())
                {
                    System.out.println(tContReceiveUI.mErrors.getFirstError());
                }
            }
            else
            {
                System.out.println("BriefPrint ReceiveCont Success...");
            }
        }
    }

    //打印保单
    LCSuccorNewUI tLCSuccorUI = null;
	try{
        tVData.add( tLCContSchema );
        tVData.add( tTransferData );
        tVData.add( tG );
        System.out.println("BriefPrint start");

        tLCSuccorUI = new LCSuccorNewUI();
        tLCSuccorUI.submitData(tVData,tAction);
    }
    catch(Exception ex)
    {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
    }
    //如果在Catch中发现异常，则不从错误类中提取错误信息
    if (FlagStr=="")
    {
        tError = tLCSuccorUI.mErrors;
        if (!tError.needDealError())
        {
        	Content = " 保存成功! ";
        	FlagStr = "Success";
        }
        else
        {
        	Content = " 保存失败，原因是:" + tError.getFirstError();
        	FlagStr = "Fail";
        }
    }

    //添加各种预处理
    if(FlagStr.equals("Success")){
  	    //处理成功，传送xml；
  	    String tprtflag = request.getParameter("PrtFlag");
		String tCardName = "J04-"+tContNo;
		String tFileName = "J03-"+tContNo;
		String sOutXmlPath_pdf = application.getRealPath("printdata/data/briefpdf/")+"/";

		/** 交通意外 */
		if(tCardFlag.equals("2")){
			tCardName = "J05-"+tContNo;
			tprtflag = "Notice";
		}

		/** 意外险平台 */
		if(tCardFlag.equals("5")){
			tCardName = "J010-"+tContNo;
			tprtflag = "Notice";
		}

		if(tCardFlag.equals("6")){
			tCardName = "J006-"+tContNo;
			tprtflag = "Notice";
		}
		if("1".equals(tIntlFlag)){
			tCardName = "C001-"+tContNo;
			tprtflag = "Notice";
		}

		System.out.println("\n\n\n\n" + tprtflag);
		//if (tprtflag.equals("Notice")&&"5".equals(tCardFlag))
		//{
    //        String url=tPrintServerInterface+"&filename=" + tCardName + "&action=preview";

    //        System.out.println(url);
    //        response.sendRedirect(url);
            //out.println("<script>var win=window.open("+url+");</script>");
            //out.println("<script>var win=window.open("+url+");</script>");
    //        System.out.println("aaaaaaaaaaaaaaaaaaaaaaa");
    //        FlagStr = "Succ";
		//}//else if(tprtflag.equals("POL")){
     //       String url=tPrintServerInterface+"&filename=" + tFileName + "&action=preview";
     //       System.out.println(url);
     //       response.sendRedirect(url);
     //       //System.out.println();
     //       System.out.println("lsdkjsdlfjsdfjsdlfjldljfs");
     //       FlagStr = "Succ";
		//}
    }
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>