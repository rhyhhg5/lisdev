//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var arrDataSet;
var ImportPath;
var ImportState = "no";
window.onbeforeunload = beforeAfterInput;
window.onunload= AfterInput;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm() {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  ChangeDecodeStr();
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
	UnChangeDecodeStr();
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
  if(fm.all('CalPremButton').disabled){
  	fm.all('CalPremButton').disabled=false;  
  }
  if(fm.all('ReCalPremButton'.disabled)){
    fm.all('ReCalPremButton').disabled=false;
  }
  initForm();
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
  try {
    initForm();
  } catch(re) {
    alert("在LCInuredList.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm() {
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit() {
  //添加操作
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
//Click事件，当点击增加图片时触发该函数
function addClick() {
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
  fm.fmtransact.value = "INSERT||MAIN" ;
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick() {
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?")) {
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fmSave.fmtransact.value = "UPDATE||INSURED";
    fmSave.action = "./DiskInsuredInputSave.jsp";
    ChangeDecodeStr();
    fmSave.submit(); //提交
  } else {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LCInuredListQuery.jsp?GrpContNo="+GrpContNo);
}
//Click事件，当点击“删除”图片时触发该函数
function deleteClick() {
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?")) {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmtransact.value = "DELETE||MAIN";
    fm.submit(); //提交
    initForm();
  } else {
    alert("您取消了删除操作！");
  }
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
  if (cShow=="true") {
    cDiv.style.display="";
  } else {
    cDiv.style.display="none";
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
	try{
		var insuredID=arrQueryResult[0][1];
		var grpcontno=arrQueryResult[0][0];
	  var arr = new Array();
	  var strSql = "select * from LCInsuredList where  InsuredID='"+insuredID+"' and grpcontno='"+grpcontno+"'";
//	  alert(strSql);
		var arr = easyExecSql (strSql);
		//afterQuery(arr);

	  if( arr != null ) {
	    //arr = arrQueryResult;
	//    alert(arr);
	    fmSave.all('GrpContNo').value= arr[0][0];
	    fmSave.all('InsuredID').value= arr[0][1];
	    fmSave.all('State').value= arr[0][2];
	  	fmSave.all('ContNo').value= arr[0][3];
	    fmSave.all('BatchNo').value= arr[0][4];
	    fmSave.all('InusredNo').value= arr[0][5];
	    fmSave.all('Retire').value= arr[0][6];
	    fmSave.all('EmployeeName').value= arr[0][7];
	    fmSave.all('InsuredName').value= arr[0][8];
	    fmSave.all('Relation').value= arr[0][9];
	    fmSave.all('Sex').value= arr[0][10];
	    fmSave.all('Birthday').value= arr[0][11];
	    fmSave.all('IDType').value= arr[0][12];
	    fmSave.all('IDNo').value= arr[0][13];
	    fmSave.all('ContPlanCode').value= arr[0][14];
	    fmSave.all('OccupationType').value= arr[0][15];
	    fmSave.all('BankCode').value= arr[0][16];
	    fmSave.all('BankAccNo').value= arr[0][17];
	    fmSave.all('AccName').value= arr[0][18];
	    fmSave.all('Operator').value= arr[0][19];
	    fmSave.all('MakeDate').value= arr[0][20];
	    fmSave.all('MakeTime').value= arr[0][21];
	    fmSave.all('ModifyDate').value= arr[0][22];
	    fmSave.all('ModifyTime').value= arr[0][23];
			showAllCodeName();
	  }
	}catch(ex){
		alert(ex.message);
	}
}

function InsuredListUpload() {
  if ( ImportState =="Succ") {
    if ( !confirm("确定您要再次磁盘投保吗?") ) {
      return false ;
    }
  }
  var i = 0;
  getImportPath();

  ImportFile = fm.all('FileName').value;
  var prtno = getPrtNo();
  var tprtno = ImportFile;

  if ( tprtno.indexOf("\\")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("\\")+1);
  if ( tprtno.indexOf("/")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("/")+1);
  if ( tprtno.indexOf("_")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("_"));
  if ( tprtno.indexOf(".")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("."));

  if ( prtno!=tprtno ) {
    alert("文件名与印刷号不一致,请检查文件名!");
    return ;
  } else {

    var showStr="正在上载数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./DiskApplySave.jsp?ImportPath="+ImportPath+"&GrpContNo="+GrpContNo;
    fm.submit(); //提交
  }
}
function getImportPath () {
  // 书写SQL语句
  var strSQL = "";

  strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];

}
function AfterInput() {
  if ( ImportState=="Importing" ) {
    return false;
  }
}
function beforeAfterInput() {
  if ( ImportState=="Importing" ) {
    alert("磁盘投保尚未完成，请不要离开!");
    return false;
  }
}
function getPrtNo () {
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select PrtNo from lcgrpcont where GrpContNo ='"+ GrpContNo +"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查找到保单:" + GrpContNo);
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  return turnPage.arrDataCacheSet[0][0];
}

function CalInsuredPrem()
{
	//var n = LCInuredListGrid.getSelNo();
	//if(n<0)
	//{
	//	alert()
	//}
//	var mBatchNo = "";
//	var strSql = "select distinct BatchNo from LCInsuredList where GrpContNo='"
//							+GrpContNo+"' and State='0'";
//	var arr = easyExecSql(strSql);
//	if(arr)
//	{
//		if(arr.length>1)
//		{
//			alert("此团体合同下存在多批次号，暂时不支持多批次导入！");
//			return;
//		}
//		mBatchNo = arr[0][0];
//	}
//else
//	{
//		return;
//	}
	
	if(!ChkHoldInsuredPeople())
	{
    return;		
	}
	var strSql = "select * from ldsystrace where PolNo='" + getPrtNo() + "' and PolState=1007 ";
	var arrResult = easyExecSql(strSql);
	if (arrResult!=null && arrResult[0][1]!=operator) {
	  alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	  return;
	}
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + getPrtNo() + "&CreatePos=保费计算&PolState=1007&Action=INSERT";
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
	fm.all('CalPremButton').disabled=true;
	fm.fmtransact.value="INSERT||DATABASE";
	var mBatchNo = null;
	var showStr="保费计算过程将会在后台处理,预计将会处理几分钟,此页面可关闭<br>处理进度请参看本页面导入信息";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./LCInuredListSave.jsp?BatchNo="+mBatchNo+"&fmtransact="+fm.fmtransact.value+"&GrpContNo="+GrpContNo;
	//ChangeDecodeStr();
	fm.submit();
}

function ReCalInsuredPrem()
{
	if(!ChkHoldInsuredPeople())
	{
    return;		
	}
	var strSql = "select * from ldsystrace where PolNo='" + getPrtNo() + "' and PolState=1007 ";
	var arrResult = easyExecSql(strSql);
	if (arrResult!=null && arrResult[0][1]!=operator) {
	  alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	  return;
	}
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + getPrtNo() + "&CreatePos=保费计算&PolState=1007&Action=INSERT";
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
	fm.all('ReCalPremButton').disabled=true;	
	var showStr="保费计算过程将会在后台处理,预计将会处理几分钟,此页面可关闭<br>处理进度请参看本页面导入信息";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./LCReCalInuredListSave.jsp?GrpContNo="+GrpContNo;	
	fm.submit();
}

function QueryInsuredList()
{
	var strSql = "select distinct '','','','',BatchNo,'','','','','','',''"
							+",'','','','','','','',Operator from LCInsuredList where GrpContNo='"
							+GrpContNo+"' and State='0'";
	turnPage.queryModal(strSql, LCInuredListGrid);
}

function showInsuredInfo()
{
	var BatchNo = "";
	var strSql = "select distinct BatchNo from LCInsuredList where GrpContNo='"
							+GrpContNo+"' and State='0'";
//var arr = easyExecSql(strSql);
//if(arr)
//{
//	if(arr.length>1)
//	{
//		alert("此团体合同下存在多批次号，暂时不支持多批次导入！");
//		return;
//	}
//	BatchNo = arr[0][0];
//}
//else
//{
//	return;
//}
		var strSql_1 = "select count(1) from LCInsuredList where 1=1"
									+" and GrpContNo='"+GrpContNo+"' and insuredid not in ('C','D')";
									
		var strSql_2 = " select count(1) from LCInsuredList where 1=1"
									+" and GrpContNo='"+GrpContNo+"' and State='1' and insuredid not in ('C','D')";

		var strSql_3 = " select count(1) from LCInsuredList where 1=1"
									+" and GrpContNo='"+GrpContNo+"' and State='0' and insuredid not in ('C','D')";
									
		var strSql_4 = " select count(1) from LCGrpImportLog where 1=1"
									+" and GrpContNo='"+GrpContNo+"' and ErrorState='1' ";
		var arr = easyExecSql(strSql_1);
		if(arr)
		{
			fm.SumInsured.value = arr[0][0];
		}
		var arr = easyExecSql(strSql_2);
		if(arr)
		{
			fm.SuccInsured.value = arr[0][0];
		}
		var arr = easyExecSql(strSql_3);
		if(arr)
		{
			fm.HoldInsured.value = arr[0][0];
		}
		var arr = easyExecSql(strSql_4);
		if(arr)
		{
			fm.FailInsured.value = arr[0][0];
		}
		easyQueryClick();
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function easyQueryClick(BatchNo)
{
  initDiskErrQueryGrid();
  // 书写SQL语句
  var strSql = "select GrpContNo,BatchNo,ContID,InsuredID,InsuredName,ErrorInfo from LCGrpImportLog where 1=1 "
				 + "and Errorstate='1' ";
				 if(GrpContNo != null&& GrpContNo !="")
				 {
				 strSql=strSql + "and GrpContNo='"+GrpContNo+"'" ;
				 }
			     if(BatchNo !=null&& BatchNo !="")
				 {
				  strSql=strSql+ "and BatchNo='"+BatchNo+"'";
				 }
				  strSql=strSql+ " Order by BatchNo,integer(InsuredID)";
//alert(strSql);
		turnPage.queryModal(strSql, DiskErrQueryGrid);
}
function showList()
{
	var row = DiskErrQueryGrid.getSelNo()-1;
	if(row>=0)
	{
		var ContID = DiskErrQueryGrid.getRowColData(row,3);
//		alert(ContID);
		var strSql = "select * from LCInsuredList where GrpContNo='"+GrpContNo+"' and ContNo='"+ContID+"'";
		var arr = easyExecSql (strSql);
		afterQuery(arr);
		showAllCodeName();
	}
}
//判断投保单录入人数与被导入人数是否相等
function ChkHoldInsuredPeople()
{ 
	var result=easyExecSql("select peoples3 from LCGrpCont where GrpContNo = '"+GrpContNo+"'");
	if(fm.SumInsured.value !=result)
	{
  if(!confirm("被导入人数和投保单录入人数不符,是否计算保费？"))
		{
			return false;
		} 	
  }
  return true;
}

function NoNameCont()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		
	fm.action = "./NoNameContSave.jsp?GrpContNo="+GrpContNo;	
	fm.submit();
}
function BackTopWindows()
{
	//alert();
		top.opener.location.reload();
		top.close();
}