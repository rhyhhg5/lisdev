<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BriefFinFeeSave.jsp
//程序功能：
//创建日期：2006-7-13 16:49
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.brieftb.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG=(GlobalInput)session.getValue("GI");
  BriefFinConfirmUI tBriefFinConfirmUI   = new BriefFinConfirmUI();
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("BatchNo",request.getParameter("BatchNo_Fin"));
  try
  {
 		// 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tG);
  	tVData.add(tTransferData);
    tBriefFinConfirmUI.submitData(tVData,null);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="")
  {
    tError = tBriefFinConfirmUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>