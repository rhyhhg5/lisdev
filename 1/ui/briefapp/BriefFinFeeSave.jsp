<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BriefFinFeeSave.jsp
//程序功能：
//创建日期：2006-7-13 16:49
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.brieftb.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG=(GlobalInput)session.getValue("GI");
  BriefCreatFinUI tBriefCreatFinUI   = new BriefCreatFinUI();
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("AgentCom",request.getParameter("AgentCom"));
	tTransferData.setNameAndValue("ManageCom",request.getParameter("ManageCom"));
	tTransferData.setNameAndValue("StartDate",request.getParameter("StartDate"));
	tTransferData.setNameAndValue("EndDate",request.getParameter("EndDate"));
	
	String tChk[]=request.getParameterValues("InpGrpContGridChk");
	String tGetNoticeNo[]=request.getParameterValues("GrpContGrid10");
	String tOtherNo[]=request.getParameterValues("GrpContGrid1");
  String getNoticeNoStr=" (";
  String otherNoStr=" (";
	if(tChk!=null){
		for(int i=0;i<tChk.length;i++){
			if(tChk[i].equals("1")){
				getNoticeNoStr+="'"+tGetNoticeNo[i]+"'," ;
				otherNoStr+="'"+tOtherNo[i]+"'," ;
			}
		}
	}
  getNoticeNoStr+="'')";
  otherNoStr+="'')";
	System.out.println("GetNoticeNo:"+getNoticeNoStr);
	System.out.println("OtherNo:"+otherNoStr);
 	tTransferData.setNameAndValue("GetNoticeNo",getNoticeNoStr);
 	tTransferData.setNameAndValue("OtherNo",otherNoStr);
  try
  {
 		// 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tG);
  	tVData.add(tTransferData);
   	tBriefCreatFinUI.submitData(tVData,null);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="")
  {
    tError = tBriefCreatFinUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
//添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>