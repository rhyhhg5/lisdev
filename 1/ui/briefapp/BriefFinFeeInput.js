//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
parent.fraMain.rows = "0,0,0,0,*";
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDisease.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDiseaseQuery.html");
}           


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//查询合同信息
function queryContInfo(){
	if(fm.ManageCom.value == ""){
		alert("请录入管理机构");
		fm.ManageCom.focus();
		return;
	}
	if(fm.AgentCom.value == ""){
		alert("请录入代理机构");
		fm.AgentCom.focus();
		return;
	}
	if(fm.StartDate.value == ""){
		alert("请录入抽档开始时间");
		fm.StartDate.focus();
		return;
	}
	if(fm.EndDate.value == ""){
		alert("请录入抽档结束时间");
		fm.EndDate.focus();
		return;
	}
	showContInfo();
}

function showContInfo(){
	var strSql = "select a.otherno,b.prtno,b.grpname,b.CValiDate,a.SumDuePayMoney,a.paydate,b.ManageCom,b.AgentCom,getUniteCode(b.agentCode),a.getnoticeno from ljspay a,lcgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo is null  "
	+" and b.ManageCom='"+fm.ManageCom.value+"' "
	+" and b.AgentCom='"+fm.AgentCom.value+"' "
	+" and a.paydate>='"+fm.StartDate.value+"' "
	+" and a.paydate<='"+fm.EndDate.value+"' "
	+" union ALL"
	+" select a.otherno,b.prtno,b.grpname,b.CValiDate,a.SumDuePayMoney,a.paydate,b.ManageCom,b.AgentCom,getUniteCode(b.agentCode),a.getnoticeno from ljspay a,lbgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo is null  "
	+" and b.ManageCom='"+fm.ManageCom.value+"' "
	+" and b.AgentCom='"+fm.AgentCom.value+"' "
	+" and a.paydate>='"+fm.StartDate.value+"' "
	+" and a.paydate<='"+fm.EndDate.value+"' ";
	turnPage.queryModal(strSql,GrpContGrid);
	strSql = "select to_zero(sum(a)) from ("
	+" select a.SumDuePayMoney A from ljspay a,lcgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo is null  "
	+" and b.ManageCom='"+fm.ManageCom.value+"' "
	+" and b.AgentCom='"+fm.AgentCom.value+"' "
	+" and a.paydate>='"+fm.StartDate.value+"' "
	+" and a.paydate<='"+fm.EndDate.value+"' "
	+" union ALL "
	+" select a.SumDuePayMoney A from ljspay a,lbgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo is null  "
	+" and b.ManageCom='"+fm.ManageCom.value+"' "
	+" and b.AgentCom='"+fm.AgentCom.value+"' "
	+" and a.paydate>='"+fm.StartDate.value+"' "
	+" and a.paydate<='"+fm.EndDate.value+"' "
	+" ) as x";
	fm.SumPrem.value = easyExecSql(strSql);
}

function creatFin(){
	if(fm.ManageCom.value == ""){
		alert("请录入管理机构");
		fm.ManageCom.focus();
		return;
	}
	if(fm.AgentCom.value == ""){
		alert("请录入代理机构");
		fm.AgentCom.focus();
		return;
	}
	if(fm.StartDate.value == ""){
		alert("请录入抽档开始时间");
		fm.StartDate.focus();
		return;
	}
	if(fm.EndDate.value == ""){
		alert("请录入抽档结束时间");
		fm.EndDate.focus();
		return;
	}
	var flag=0;
	for(var index=0;index<GrpContGrid.mulLineCount;index++){
		if(GrpContGrid.getChkNo(index)){
			flag=1;
		}
	}
	if(flag==0){
		alert("请选择一项记录");
		return;
	}
	
	var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./BriefFinFeeSave.jsp";
  fm.submit(); //提交
}

function queryFinInfo(){
	var strSql = "select a,i,b1,b2,c,e,case when f='1' then '已核销' else '未核销' end,case when g='0' then '未打印' else '已打印' end,h from ("
	+" select a.prtseq a,a.makedate i,"
	+"(select varchar(sum(SumDuePayMoney)) from ljspayB where SerialNo=a.prtseq) b1,"
	+"(select varchar(to_zero(sum(PayMoney))) from ljtempfee where tempfeeno=a.prtseq) b2,"
	+"a.ManageCom c,a.standbyflag1 e,"
	+"case when (select distinct confflag from ljspayb where SerialNo=a.prtseq) is null then '0' else '1' end f,"
	+"a.stateflag g,(select max(enteraccdate) from ljtempfee where tempfeeno=a.prtseq) h "
	+"from loprtmanager a where ManageCom like '"+manageCom+"%%' and othernotype='06' and code='15' "
	+") as x where 1=1 "
	+ getWherePart('c','ManageCom_Fin')
	+ getWherePart('e','AgentCom_Fin')
	+ getWherePart('a','BatchNo_Fin')
	+	getWherePart('f','FinishFlag')
	+	getWherePart('g','StateFlag')
	+	getWherePart('h','EnterAccDate')
	;
	turnPage2.queryModal(strSql,FinGrid);
}

function inputConfirm(){
	var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./BriefFinFeeConfirm.jsp";
  fm.submit(); //提交
}
function initBatchNo(){
	var selNo = FinGrid.getSelNo();
	fm.BatchNo_Fin.value = FinGrid.getRowColData(selNo-1,1);
	var strSql = "select a.otherno,b.prtno,b.grpname,b.CValiDate,a.SumDuePayMoney,a.paydate,b.ManageCom,b.AgentCom,getUniteCode(b.agentCode) from ljspay a,lcgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo='"+fm.BatchNo_Fin.value+"'  "
	+" union ALL"
	+" select a.otherno,b.prtno,b.grpname,b.CValiDate,a.SumDuePayMoney,a.paydate,b.ManageCom,b.AgentCom,getUniteCode(b.agentCode) from ljspay a,lbgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo='"+fm.BatchNo_Fin.value+"'  "
	turnPage.queryModal(strSql,GrpContGrid);
	strSql = "select to_zero(sum(a)) from ("
	+" select a.SumDuePayMoney A from ljspay a,lcgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo='"+fm.BatchNo_Fin.value+"'  "
	+" union ALL "
	+" select a.SumDuePayMoney A from ljspay a,lbgrpcont b where "
	+" b.grpcontno=a.getnoticeno and a.othernotype='14' and SerialNo='"+fm.BatchNo_Fin.value+"'  "
	+" ) as x";
	fm.SumPrem.value = easyExecSql(strSql);
}
function printCont(){
	var selNo = FinGrid.getSelNo();
	if(selNo!=""){
	        fm.BatchNo_Fin.value = FinGrid.getRowColData(selNo-1,1);
	        if(selNo < 0){
		    alert("请选择一个应收清单打印");
	      	return;
	        }
	        fm.action="./BriefGrpTravelAgentSave.jsp";
            fm.target="f1print1";
            fm.submit(); //提交
            fm.action="./BriefGrpTravelAgentSave.jsp?FinTitle=（财务联）";
            fm.target="f1print2";
            fm.submit(); //提交
   }else{
            alert("请选择一个应收清单打印");
	      	return;
   }
}