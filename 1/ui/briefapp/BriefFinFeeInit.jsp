<%
//程序名称：BriefFinFeeInit.jsp
//程序功能：
//创建日期：2006-7-13 11:53
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initBox();
    initSelBox();
    initGrpContGrid();
    initFinGrid();
    showAllCodeName();
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

function initSelBox()
{
  try
  {}
  catch(ex)
  {
    alert(ex.message);
  }
}
function initBox()
{
  try
  {}
  catch(ex)
  {
    alert(ex.message);
  }
}
// 保单信息列表的初始化
function initFinGrid()
{
  var iArray = new Array();

  try
  {
  	iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="批次号";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="生成日期";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="批次应收金额";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="批次实收金额";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="管理机构";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=2;
    iArray[5][4]="comcode";              	        //是否引用代码:null||""为不引用
    iArray[5][5]="6";              	                //引用代码对应第几列，'|'为分割符
    iArray[5][9]="出单机构|code:comcode&NOTNULL";
    iArray[5][18]=250;
    iArray[5][19]= 0 ;

    iArray[6]=new Array();
    iArray[6][0]="代理机构";         		//列名
    iArray[6][1]="70px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="核销状态";         		//列名
    iArray[7][1]="50px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="打印状态";         		//列名
    iArray[8][1]="70px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="财务到帐日期";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许


    FinGrid = new MulLineEnter( "fm" , "FinGrid" );
    //这些属性必须在loadMulLine前
    FinGrid.mulLineCount = 0;
    FinGrid.displayTitle = 1;
    FinGrid.locked = 1;
    FinGrid.canSel = 1;
    FinGrid.hiddenPlus = 1;
    FinGrid.hiddenSubtraction = 1;
    FinGrid.loadMulLine(iArray);
    FinGrid.selBoxEventFuncName ="initBatchNo";
  }
  catch(ex)
  {
    alert("GrpDueFeeBatchInit.jsp-->initGrpContGrid函数中发生异常:初始化界面错误!");
  }
}
// 保单信息列表的初始化
function initGrpContGrid()
{
  var iArray = new Array();

  try
  {
    
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="印刷号";         		//列名
    iArray[2][1]="0px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="投保单位";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="生效时间";         		//列名
    iArray[4][1]="70px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="应交保费";         		//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="应收时间";         		//列名
    iArray[6][1]="70px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许


    iArray[7]=new Array();
    iArray[7][0]="管理机构";         		//列名
    iArray[7][1]="60px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=2;
    iArray[7][4]="comcode";              	        //是否引用代码:null||""为不引用
    iArray[7][5]="6";              	                //引用代码对应第几列，'|'为分割符
    iArray[7][9]="出单机构|code:comcode&NOTNULL";
    iArray[7][18]=250;
    iArray[7][19]= 0 ;

    iArray[8]=new Array();
    iArray[8][0]="代理机构";         		//列名
    iArray[8][1]="70px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="代理人编码";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="getnoticeno";         		//列名
    iArray[10][1]="0px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" );
    //这些属性必须在loadMulLine前
    GrpContGrid.mulLineCount = 0;
    GrpContGrid.displayTitle = 1;
    GrpContGrid.locked = 1;
    GrpContGrid.canSel = 0;
	GrpContGrid.canChk = 1;
    GrpContGrid.hiddenPlus = 1;
    GrpContGrid.hiddenSubtraction = 1;
    GrpContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("GrpDueFeeBatchInit.jsp-->initGrpContGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
