<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： FirstPayPrint.jsp
//程序功能：
//创建日期：2007-10-22
//创建人  ：张建宝
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%		
    System.out.println("in to BriefSingleContPrint.jsp");	
    boolean operFlag = true;
		String FlagStr = "";
		String Content = "";
		String[] title = {"个人合同号", "印刷号", "投保人名称", "保单类型", "cardfalg", "营销机构", "业务员代码"};
		XmlExport xmlExport = null;   
		GlobalInput tG = (GlobalInput)session.getValue("GI");

		request.setCharacterEncoding("gb2312");
		String sql = request.getParameter("querySql");
		//System.out.println(request.getParameter("querySql"));
		
    TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("sql", sql);	//查询的 SQL 
		tTransferData.setNameAndValue("vtsName", "BriefSingleContPrint.vts");//模板名
		tTransferData.setNameAndValue("printerName", "printer");//打印机名
		tTransferData.setNameAndValue("title", title);//表头
		tTransferData.setNameAndValue("tableName", "BB");//表名
		
		VData tVData = new VData();
    tVData.addElement(tG);
		tVData.addElement(tTransferData);
    
    PrintList printList = new PrintList(); 
    if(!printList.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = printList.mErrors.getError(0).errorMessage;
    }
    else
    {    
			VData result = printList.getResult();			
	  	xmlExport=(XmlExport)result.getObjectByObjectName("XmlExport",0);

	  	if(xmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";
	  	}
		}
		//System.out.println(operFlag);
		if (operFlag==true)
		{
  		ExeSQL exeSQL = new ExeSQL();
      //获取临时文件名
      String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
      String strFilePath = exeSQL.getOneValue(strSql);
      String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
      //获取存放临时文件的路径
      //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
      //String strRealPath = exeSQL.getOneValue(strSql);
      String strRealPath = application.getRealPath("/").replace('\\','/');
      String strVFPathName = strRealPath + "//" +strVFFileName;
      
      CombineVts tcombineVts = null;	
      
      String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
    	tcombineVts = new CombineVts(xmlExport.getInputStream(),strTemplatePath);
    
    	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    	tcombineVts.output(dataStream);
        	
    	//把dataStream存储到磁盘文件
    	System.out.println("存储文件到"+strVFPathName);
    	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	    System.out.println("==> Write VTS file to disk ");
            
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
		}
		else
		{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%= Content %>");
	top.close();
</script>
</html>
<%
  	}
%>