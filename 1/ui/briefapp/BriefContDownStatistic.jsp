<%
//程序名称：银行代收对帐清单
//程序功能：
//创建日期：2007-6-20 11:12
//创建人  ：XUN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.brieftb.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="BriefContDownStatistic.js"></SCRIPT>	
</head>

<body>
	<form action="./BriefContDownStatisticSub.jsp" method=post name=fm target="fraSubmit">
	<%@include file="../common/jsp/InputButton.jsp"%>
      <!-- 显示或隐藏LLReport1的信息 -->
  		
  <Div  id= "divLLReport1" style= "display: ''">
  	<table class=common>
        <tr class= titleImg>
          <TD class= titleImg > 先收费清单 </TD>
        </tr>
    </table>
   	<table  class= common>
      	<TR  class= common>
        	<TD  class= title> 起始日期 </TD>
        	<TD  class= input> <input class="coolDatePicker" dateFormat="short" name="StartDate" verify="起始日期|notnull" elementtype=nacessary></TD>
        	<TD  class= title> 结束日期 </TD>
        	<TD  class= input> <input class="coolDatePicker" dateFormat="short" name="EndDate" verify="结束日期|notnull" elementtype=nacessary> </TD>
	      </TR>	      
				<TR  class= common>
					<TD  class= title>
			      银行代码
			    </TD>
			    <TD  class= input>
			      <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank" ><input class=codename name=BankCodeName readonly=true >
			    </TD>    				
			    <TD class= title></TD>
  				<TD class=input ></TD>
      	</TR>
	</table>
	
	<table class=common>
    <TR  class= common>         
       <TD class=input align=right> <input class=common type=button value="打印清单" onclick="PrintBill()"> </TD>           
    </TR>
  </table>

     	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
