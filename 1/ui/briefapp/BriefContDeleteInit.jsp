<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BriefContDeleteInit.jsp
//程序功能：简易保单整单删除初始化
//创建日期：2007-11-22
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	String strManageCom = globalInput.ComCode;
	String strOperator = globalInput.Operator;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
    // 保单查询条件  
    fm.all('PrtNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentCodeName').value = '';
    fm.all('ContType').value = '';
    fm.all('ContTypeName').value = '';      
    fm.all('DeleteReason').value = '';                                  
  }
  catch(ex)
  {
    alert("在ContDeleteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value ==86)
    {
    	fm.all('ManageCom').readOnly=false;
    }
    else
    {
    	fm.all('ManageCom').readOnly=true;
    }
    if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null)
			{
            	fm.all('ManageComName').value=arrResult[0][0];
            } 
    }
    initInpBox();
    initBriefGrid();
    initElementtype();
  }
  catch(re)
  {
    alert("ContUWInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


// 保单信息列表的初始化
function initBriefGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         			//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="合同号";         			//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="投保人";         			//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;  

      iArray[4]=new Array();
      iArray[4][0]="管理机构";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;  
      
      iArray[5]=new Array();
      iArray[5][0]="保单类型";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;  
      
      BriefGrid = new MulLineEnter( "fm" , "BriefGrid" ); 
      //这些属性必须在loadMulLine前
      BriefGrid.mulLineCount = 3;   
      BriefGrid.displayTitle = 1;
      BriefGrid.locked = 1;
      BriefGrid.canSel = 1;
      BriefGrid.hiddenPlus = 1;
      BriefGrid.hiddenSubtraction = 1;
      BriefGrid.loadMulLine(iArray);
      
      //BriefGrid.selBoxEventFuncName = "ContSelect"; 
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>