<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
String tContNo = "";
try{
	tContNo = request.getParameter( "ContNo" );
	//默认情况下为集体投保单
	if( tContNo == null || tContNo.equals( "" ))
		tContNo = "00000000000000000000";
}
catch( Exception e1 ){
	tContNo = "00000000000000000000";
}
%>
<script>
var contNo = "<%=tContNo%>";  //个人单的查询条件.
var mPrtNo ="<%=request.getParameter("PrtNo")%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="GroupPolPrintEC.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="GroupPolPrintInitEC.jsp"%>
<title>团体保单查询 </title>
</head>
<body onload="initForm();">
	<form action="./BriefGrpContPrintECSave.jsp" method=post name=fm target=fraSubmit>
		<!-- 保单信息部分 -->
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>
		<table class=common align=center>
			<TR class=common>
				<TD class=title>生效日期</TD>
				<TD class=input>
					<Input class=cooldatepicker  name=CValiDate >
				</TD>
			</TR>
			<TR class=common>
				<TD class=title>保单号</TD>
				<TD class=input>
					<Input class=common name=ContNo>
				</TD>
				<TD class=title>印刷号码</TD>
				<TD class=input>
					<Input class=common name=PrtNo>
				</TD>
			</TR>
			<!--TR class=common>
				<TD class=title>险种编码</TD>
				<TD class=input>
					<Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">
				</TD>
			</TR-->
			<TR class=common>
				<TD class=title STYLE="display:'none'">展业机构</TD>
				<td class="input" nowrap="true" STYLE="display:'none'">
					<Input class="common" name="BranchGroup">
					<input name="btnQueryBranch" class="common" type="button" value="?" onclick="queryBranch()" style="width:20">
				</TD>
				<TD class=title>被保人姓名</TD>
				<TD class=input>
					<Input class=common name=InsuredName >
				</TD>
			</TR>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE="button" onclick="easyQueryClick();">
		<input value="打印保单" class="cssButton" type="button" onclick="printContPol();" name='printButton'>
		
		<table>
			<tr>
				<td class=common>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
				</td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<Div id="divLCPol1" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanContGrid"></span>
					</td>
				</tr>
			</table>
			<INPUT VALUE="首页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾页" class="cssButton" TYPE=button onclick="getLastPage();">
		</div>
		<td>
			<Input class=common name=PrtFlag type= hidden>
			<input type="hidden" name="fmAction" >
		</td>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html>
