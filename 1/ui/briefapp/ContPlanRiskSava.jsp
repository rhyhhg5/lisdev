<%--
    保存简易保单险种计划信息 2005-09-07 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  CErrors tError = null;
  GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
  //取出mulline中的信息
  String[] tNo      			=  request.getParameterValues("ContPlanGridNo");
	String[] tRiskName      =  request.getParameterValues("ContPlanGrid1");
	String[] tRiskCode	    =  request.getParameterValues("ContPlanGrid2");
	String[] tDutyCode      =  request.getParameterValues("ContPlanGrid3");
	String[] tDutyName	    =  request.getParameterValues("ContPlanGrid4");
	String[] tCalfactor	    =  request.getParameterValues("ContPlanGrid5");
	String[] tFactorName    =  request.getParameterValues("ContPlanGrid6");
	String[] tFactornoti	  =  request.getParameterValues("ContPlanGrid7");
	String[] tCalvalue      =  request.getParameterValues("ContPlanGrid8");
	String[] tSpec          =  request.getParameterValues("ContPlanGrid9");
	String[] tRiskVer	      =  request.getParameterValues("ContPlanGrid10");
	String[] tCalfactorType =  request.getParameterValues("ContPlanGrid11");
	String[] tCalMode       =  request.getParameterValues("ContPlanGrid12");
	String[] tPayPlanCode   =  request.getParameterValues("ContPlanGrid13");
	String[] tGetDutyCode   =  request.getParameterValues("ContPlanGrid14");
	String[] tInsuAccNo     =  request.getParameterValues("ContPlanGrid15");
	String[] tGrpPolNo     	=  request.getParameterValues("ContPlanGrid19");
	//和同信息
	String tGrpContNo    =  request.getParameter("GrpContNo");
				 tOperate		=  request.getParameter("fmAction");
	System.out.println("tOperate : "+tOperate);
	//要素
	LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
  
	for(int i=0 ; i<tNo.length ; i++)
	{
		LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
		tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
		tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
		tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[i]);
		tLCContPlanDutyParamSchema.setMainRiskVersion(tRiskVer[i]);
		tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[i]);
		tLCContPlanDutyParamSchema.setRiskVersion(tRiskVer[i]);
		tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
		tLCContPlanDutyParamSchema.setContPlanName("默认计划");
		tLCContPlanDutyParamSchema.setDutyCode(tDutyCode[i]);
		tLCContPlanDutyParamSchema.setCalFactor(tCalfactor[i]);
		tLCContPlanDutyParamSchema.setCalFactorType(tCalfactorType[i]);
		tLCContPlanDutyParamSchema.setCalFactorValue(tCalvalue[i]);
		tLCContPlanDutyParamSchema.setRemark(tSpec[i]);
		tLCContPlanDutyParamSchema.setPlanType("0");
		tLCContPlanDutyParamSchema.setPayPlanCode(tPayPlanCode[i]);
		tLCContPlanDutyParamSchema.setGetDutyCode(tGetDutyCode[i]);
		tLCContPlanDutyParamSchema.setInsuAccNo(tInsuAccNo[i]);
		tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo[i]);
		tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
	}
	//生效日期,失效日期的填充
	String CValiDate = request.getParameter("CValiDate");
	String CInValiDate = request.getParameter("CInValiDate");
	// 准备传输数据 VData
	VData tVData = new VData();
	TransferData tTransferData = new TransferData();
	
	tTransferData.setNameAndValue("CValiDate",CValiDate);
	tTransferData.setNameAndValue("CInValiDate",CInValiDate);
	
	tVData.add(tTransferData);
	tVData.add(tLCContPlanDutyParamSet);
	tVData.add(tG);
	BriefContPlanRiskUI tBriefContPlanRiskUI = new BriefContPlanRiskUI();
	if(!tBriefContPlanRiskUI.submitData(tVData,tOperate))
	{
		Content = "保存失败，原因是:" + tBriefContPlanRiskUI.mErrors.getContent();
		FlagStr = "Fail";
	}
	if (!FlagStr.equals("Fail")){
		tError = tBriefContPlanRiskUI.mErrors;
		if (!tError.needDealError()){
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>