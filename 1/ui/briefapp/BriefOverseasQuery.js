var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

function easyQueryClick()
{
    if (!verifyInput2()) 
    {
        alert("Species can not be null");
        return false;
    }
    var strSql = "";
    if(fm.ContType.value == '1')
    {
        strSql = "select distinct lcc.GrpContNo, lcc.ContNo, lcc.PrtNo, lcc.InsuredName, (select OthidNo from lcinsured lci where lci.InsuredNo = lcc.InsuredNo and lci.prtno = lcc.prtno), lcc.Cvalidate, lcc.Cinvalidate,lcc.insuredno, lcc.amnt, lcc.conttype " 
            + " from lccont lcc "
            + " where lcc.appflag in ('1', '9') and lcc.cardflag = '1' and lcc.cinvalidate >= current date "
            + getWherePart('lcc.ContType', 'ContType')
            + getWherePart('lcc.CONTNO', 'ContNo')
            + getWherePart('lcc.InsuredName', 'Insured_Name')
            + getWherePart('lcc.InsuredSex', 'Insured_Sex')
            + getWherePart('lcc.InsuredBirthday', 'Insured_Birthday');
        if((fm.Insured_EnglishName.value != null && fm.Insured_EnglishName.value != "") || (fm.Insured_OthidNo.value != null && fm.Insured_OthidNo.value != ""))
            strSql += " and exists (select 1 from lcinsured lci where lci.prtno = lcc.prtno " 
                + getWherePart('lci.EnglishName', 'Insured_EnglishName') 
                + getWherePart('lci.OthidNo', 'Insured_OthidNo') + ") ";
        if(fm.NationCode.value != null && fm.NationCode.value != "")
        {
            strSql += " and exists (select 1 from lcnation lcn where lcn.CONTNO = lcc.CONTNO "
                + getWherePart('lcn.NationNo', 'NationCode') + ") ";
        }
        strSql += " with ur ";
    }
    else if(fm.ContType.value == '2')
    {
        strSql = "select distinct lcc.GrpContNo, lcc.ContNo, lcc.PrtNo, lcc.InsuredName, (select OthidNo from lcinsured lci where lci.InsuredNo = lcc.InsuredNo and lci.prtno = lcc.prtno), lcc.Cvalidate, lcc.Cinvalidate,lcc.insuredno, lcc.amnt, lcc.conttype " 
            + " from lccont lcc "
            + " inner join lcgrpcont lgc on lgc.grpcontno = lcc.grpcontno "
            + " where lcc.appflag in ('1', '9') and lcc.cinvalidate >= current date and lgc.prtno like '12%' "
            + getWherePart('lcc.ContType', 'ContType')
            + getWherePart('lgc.GrpContNo', 'ContNo')
            + getWherePart('lcc.InsuredName', 'Insured_Name')
            + getWherePart('lcc.InsuredSex', 'Insured_Sex')
            + getWherePart('lcc.InsuredBirthday', 'Insured_Birthday');
        if((fm.Insured_EnglishName.value != null && fm.Insured_EnglishName.value != "") || (fm.Insured_OthidNo.value != null && fm.Insured_OthidNo.value != ""))
            strSql += " and exists (select 1 from lcinsured lci where lci.prtno = lcc.prtno " 
                + getWherePart('lci.EnglishName', 'Insured_EnglishName') 
                + getWherePart('lci.OthidNo', 'Insured_OthidNo') + ") ";
        if(fm.NationCode.value != null && fm.NationCode.value != "")
        {
            strSql += " and exists (select 1 from lcnation lcn where lcn.GrpContNo = lcc.GrpContNo "
                + getWherePart('lcn.NationNo', 'NationCode') + ") ";
        }
        strSql += " with ur ";
    }
    if(strSql != "")
    {
        turnPage1.queryModal(strSql, BriefOverseasContInfoGrid);
    }
    else
    {
        alert("查询失败");
    }
}

function getNationList()
{
    var strSql = "select nationno,chinesename,englishname from ldnation where travelinsuranceflag='Y' order by nationno";
    var arr = easyExecSql(strSql);
    var nationList = "";
    if(arr != null)
    {
        nationList += "0|";
        for(index in arr)
        {
            if(arr[index] != "")
            {
                nationList += "^" + arr[index][0];
                nationList += "|" + arr[index][1] + "-" + arr[index][2];
            }
        }
    }
    fm.NationCode.CodeData = nationList;
}

function showContInfo()
{
    var tRow = BriefOverseasContInfoGrid.getSelNo() -1;

    if( tRow == -1 || tRow == null )
	{
        return false;
	}
    
    var tmpResult = BriefOverseasContInfoGrid.getRowData(tRow);
    fm.R_PolicyNO.value = tmpResult[1];
    fm.R_PolicyContType.value = tmpResult[9];
    
    var strSql = "select name, englishname, case sex when '0' then '男' else '女' end, birthday, othidno from lcinsured where insuredno = '" + tmpResult[7] + "'";
    var arr = easyExecSql(strSql);
    
    if(arr == null)
    {
        alert("该保单被保人信息缺失。");
        return false;
    }
    try
    {
        fm.R_Insured_Name.value = arr[0][0];
        fm.R_Insured_EnglishName.value = arr[0][1];
        fm.R_Insured_Sex.value = arr[0][2];
        fm.R_Insured_Birthday.value = arr[0][3];
        fm.R_Insured_OthidNo.value = arr[0][4];
        
        var strSql1 = "select lcp.contno, lcp.insuredname, "
            + " (select lmr.riskname from lmrisk lmr where lmr.riskcode = lcp.riskcode) as riskname, "
            + " (select lmr.riskenname from lmrisk lmr where lmr.riskcode = lcp.riskcode) as riskenname, "
            + " (select lmg.getdutyname from lmdutyget lmg where lmg.getdutycode = lcg.getdutycode) as getdutyname, "
            + " (select lmg.GetDutyEnglishName from lmdutyget lmg where lmg.getdutycode = lcg.getdutycode) as GetDutyEnglishName "
            + " from lcpol lcp "
            + " inner join lcduty lcd on lcd.polno = lcp.polno "
            + " inner join lcget lcg on lcg.polno = lcp.polno "
            + " where lcp.contno = '" + tmpResult[1] + "'"
            + " order by lcp.contno, riskname, getdutyname "
            + " with ur "
        turnPage2.queryModal(strSql1, InsuracePlanGrid);
        fm.R_Insured_ToNation.value = getContToNation(tmpResult[1]);
        fm.all.divShowInfo.style.display = "";
    }
    catch(e)
    {
        alert("保单信息查询有误。");
    }
}

function getContToNation(sContno)
{
    var strContToNation = "";
    var arrContTypeInfo = getContTypeInfo(sContno);
    
    if(arrContTypeInfo == null)
    {
        alert("保单类型查询失败。");
        return "";
    }
    var strSql = "select chinesename,englishname from lcnation where " + arrContTypeInfo[0] + " = '" + arrContTypeInfo[1] + "' order by nationno with ur ";
    var arr = easyExecSql(strSql);
    if(arr != null)
    {
        for(index in arr)
        {
            if(arr[index] != "")
            {
                strContToNation += arr[index][0] + "-" + arr[index][1] + "; ";
            }
        }
    }
    return strContToNation;
}

function getContTypeInfo(sContno)
{
    var arrContTypeInfo = null;
    
    var strSqlContType = "select conttype, grpcontno, contno from lccont where contno = '" + sContno + "' with ur ";
    var arrContType = easyExecSql(strSqlContType);   
    
    if(arrContType != null)
    {
        arrContTypeInfo = new Array();
        if(arrContType[0][0] == 2)
        {
            arrContTypeInfo[0] = "grpcontno";
            arrContTypeInfo[1] = arrContType[0][1];
        }
        else
        {
            arrContTypeInfo[0] = "contno";
            arrContTypeInfo[1] = arrContType[0][2];
        }
    }
    return arrContTypeInfo;
}
