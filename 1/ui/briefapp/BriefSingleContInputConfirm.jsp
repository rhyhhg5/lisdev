<%--
    保存简易保单信息 2005-11-21 9:56 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*" %>
<%
  String FlagStr="";      //操作结果
  String PrintFlag = "";
  String Content = "";    //控制台信息
  String tActivityID = "";    //操作类型：delete update insert
  String tContType="";
  String tLoadFlag="INPUT_PAGES";
  CErrors tError;
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  tActivityID = request.getParameter( "ActivityID" );
  tContType=request.getParameter( "MissionProp5" );
  //if(tContType.equals("1")){
  //  PrintFlag = "N";
  //}
	//else if(tContType.equals("2")){
	//	PrintFlag = "Y";
	//	}
	PrintFlag = "N";
  System.out.println("进入Sava页面ttContType:"+tContType);
  System.out.println("进入Sava页面"+tActivityID);
  TransferData tTransferData = new TransferData();
 	tTransferData.setNameAndValue("MissionID",request.getParameter("MissionID"));
  tTransferData.setNameAndValue("SubMissionID",request.getParameter("SubMissionID"));
  tTransferData.setNameAndValue("ContNo",request.getParameter("ContNo"));
  tTransferData.setNameAndValue("PrtNo",request.getParameter("PrtNo"));
  tTransferData.setNameAndValue("ManageCom",request.getParameter("ManageCom"));
  tTransferData.setNameAndValue("AgentCode",request.getParameter("AgentCode"));
  tTransferData.setNameAndValue("InputDate",request.getParameter("InputDate"));
  tTransferData.setNameAndValue("Operator",request.getParameter("Operator"));
  tTransferData.setNameAndValue("ContType",request.getParameter("MissionProp5"));
  tTransferData.setNameAndValue("LoadFlag",tLoadFlag);  
  
  //为了兼容签单处理因此需要调用此信息。
  LCContSchema tLCContSchema = new LCContSchema();
  tLCContSchema.setContNo( request.getParameter("ContNo") );
	String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
	String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
	tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
  tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
  
  VData tVData = new VData();
  tVData.add(tTransferData);
  tVData.add( tLCContSchema );
  tVData.add(tG);
  
  //江苏中介机构校验
  boolean tFlag=false;
  String tManageCom=request.getParameter("ManageCom").substring(0,4);
  String tContNo=request.getParameter("ContNo");
   ExeSQL mExeSQL=new ExeSQL();
   if(tManageCom.equals("8632")){
     String tSaleChnl = request.getParameter("SaleChnl");
     String  tSQL=" select code from ldcode where codetype='JSZJsalechnl' and code='"+tSaleChnl+"'";
     SSRS tSSRS=mExeSQL.execSQL(tSQL);
     int i=tSSRS.MaxRow;
     if(tSSRS!=null&&i>0){
        for(int j=1;j<=i;j++){
            if(tSaleChnl.equals(tSSRS.GetText(1,j))){
               ContInputAgentcomChkBL tContInputAgentcomChkBL =new ContInputAgentcomChkBL();
               TransferData mTransferData =new TransferData();
                mTransferData.setNameAndValue("ContNo",tContNo);
                VData mVData =new VData();
                mVData.add(mTransferData);
                if(!tContInputAgentcomChkBL.submitData(mVData,"check")){
                  Content =tContInputAgentcomChkBL.mErrors.getError(0).errorMessage;
                  FlagStr="Fail";
                  tFlag=true;
                }
             }
         }
      }
   }
  if(!tFlag){
  TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
  if(!tTbWorkFlowUI.submitData(tVData,tActivityID))
  {
      Content = " 处理失败，原因是: " + tTbWorkFlowUI.mErrors.getFirstError();
      FlagStr = "Fail";
  }else {
      Content = " 处理成功！";
      FlagStr = "Succ";
      System.out.println("PrintFlag:"+PrintFlag);
      
      tVData.clear();
      TransferData ttTransferData = new TransferData();
      ttTransferData.setNameAndValue("ContNo",request.getParameter("ContNo"));
      ttTransferData.setNameAndValue("PrtNo",request.getParameter("PrtNo"));
      tVData.add(ttTransferData);
      RecordFlagBL tRecordFlagBL= new RecordFlagBL();
      tRecordFlagBL.submitData(tVData, "");
  }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","","<%=PrintFlag%>");
</script>
</html>