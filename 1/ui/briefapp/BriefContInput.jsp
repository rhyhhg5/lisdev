<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<html>
	<script>
		var prtNo = "<%=request.getParameter("prtNo")%>";
		var polNo = "<%=request.getParameter("polNo")%>";
		var scantype = "<%=request.getParameter("scantype")%>";
		var MissionID = "<%=request.getParameter("MissionID")%>";
		var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
		var ManageCom = "<%=request.getParameter("ManageCom")%>";
		var PolApplyDate = "<%=request.getParameter("PolApplyDate")%>";
		var LoadFlag = "<%=request.getParameter("LoadFlag")%>";
		var AgentType = "<%=request.getParameter("AgentType")%>";
		var AgentCom = "<%=StrTool.cTrim(tGI.AgentCom)%>";
		var CurrentDate = "<%=PubFun.getCurrentDate()%>";
		var tsql=" 1 and code in (select code from ldcode where codetype=#jgrppaymode#) ";		
	</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BriefContInputInit.jsp"%>
  <SCRIPT src="BriefContInput.js"></SCRIPT>
  <SCRIPT src="InitDatabaseToPol.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BriefContSave.jsp" method=post name=fm target="fraSubmit">
	  <table id="table1">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
	  			</td>
	  			<td class="titleImg">管理信息
	  			</td>
	  		</tr>
	  </table>
  	<div id="ManageInfoDiv" style="display: ''">
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title>
	  				印刷号
	  			</td>
	  			<td class=input>
	  				<Input class= common8 name=PrtNo elementtype=nacessary TABINDEX="-1" MAXLENGTH="11" verify="印刷号码|notnull&len=11" >
	  			</td>
	  			<td class=title>
	  				管理机构
	  			</td>
	  			<td class=input>
	  				<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
	  			</td>
	  			<td CLASS="title" id="SaleChnlTitleID" style="display: ''">销售渠道</td>
		        <td CLASS="input" id="SaleChnlInputID" COLSPAN="1" style="display: ''">
		        	<!--<Input class=codeNo name=SaleChnl verify="销售渠道|code:salechnl&notnull" ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1],null,'1 and code in (#02#,#03#,#14#,#15#)','1',1);" onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1],null,'1 and code in (#02#,#03#,#14#,#15#)','1',1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>-->
		        	<Input class=codeNo name=SaleChnl verify="销售渠道|notnull" ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
		        </td>
	  		</tr>
            <!--tr>
                <td colspan="6"><font color="red">对于非银行渠道的交叉销售业务，销售渠道选择“中介”，交叉销售渠道选择“财代健”或者“寿代健”；对于银行渠道的交叉销售，则填写“银行代理”渠道</font></td>
            </tr>
            <tr>
                <td colspan="6"><font color="red">以下“交叉销售渠道”，“交叉销售业务类型”，“对方机构代码”，“对方业务员代码”，“对方业务员姓名”信息，仅集团交叉销售业务需要填写。</font></td>
            </tr-->
            <tr>
            <td colspan="6"><font color="black">如果是交叉销售,请选择</font><INPUT TYPE="checkbox" NAME="MixComFlag" onclick="isMixCom();"></td>
	        </tr>
	        <%@include file="../sys/MixedSalesAgent.jsp"%>
	        <!--  
	        <tr class="common8" id="GrpAgentComID" style="display: none">
	            <td class="title8">交叉销售渠道</td>
	            <td class="input8">
	                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype=nacessary/>
	            </td>
	            <td class="title8">交叉销售业务类型</td>
	            <td class="input8">
	                <input class="codeNo" name="Crs_BussType" id="Crs_BussType" verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" elementtype=nacessary/>
	            </td>
	            <td class="title8">&nbsp;</td>
	            <td class="input8">&nbsp;</td>
	        </tr>	
			<tr class=common id="GrpAgentTitleID" style="display: 'none'">
		    		<td CLASS="title" >对方机构代码</td>
					<td CLASS="input" COLSPAN="1" >
		    	      <Input class="code" name="GrpAgentCom" elementtype=nacessary ondblclick="return showCodeList('grpagentcom',[this],null,null, '1 and #1# = #1# and comp_cod =(case #' + fm.all('Crs_SaleChnl').value + '# when #01# then #000002# when #02# then #000100# end )', '1');" onkeyup="return showCodeListKey('grpagentcom',[this],null,null, 1 and #1# = #1# and comp_cod =(case #' + fm.all('Crs_SaleChnl').value + '# when #01# then #000002# when #02# then #000100# end)', '1');" onfocus="GetGrpAgentName();" onchange="getAgentName();">
		    	    </td>
		    	    <td  class= title>对方机构名称</td>
			        <td  class= input>
			          <Input class="common" name="GrpAgentComName" elementtype=nacessary TABINDEX="-1" readonly >
			        </td>  
					<td CLASS="title">对方业务员代码</td>
					<td CLASS="input" COLSPAN="1">
					  	<input NAME="GrpAgentCode" CLASS="common" elementtype=nacessary>
		    	  </td>
		        </tr>
		        <tr class=common id="GrpAgentTitleIDNo" style="display: 'none'">
		            <td  class="title" >对方业务员姓名</td>
			        <td  class="input" COLSPAN="1">
			            <Input  name=GrpAgentName CLASS="common" elementtype=nacessary>
			        </td>
			        <td CLASS="title">对方业务员身份证</td>
			     	<td CLASS="input" COLSPAN="1">
						<input NAME="GrpAgentIDNo" CLASS="common" elementtype=nacessary>
			        </td>
	            </tr>
	            -->
	            
	  		<TR class=common>
		    	<TD  class= title8>
	          业务员代码
	        </TD>
	        <TD  class= input8>
	      		<Input NAME=GroupAgentCode VALUE="" MAXLENGTH=0 CLASS=code8 elementtype=nacessary ondblclick="return queryAgent();"onkeyup="return queryAgent2();" verify="代理人编码|notnull">
	      		<Input NAME=AgentCode VALUE="" MAXLENGTH=0 CLASS=code8 type='hidden'>
	       </TD>
	        <TD  class= title8>
	          业务员名称
	        </TD>
	        <TD  class= input8>
	      		<Input NAME=AgentName VALUE=""  CLASS=common >
	       </TD>         
	       <TD  class= title8>
            中介公司代码
          </TD>
          <!--TD  class= input8>
            <Input class="code" name=AgentCom ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');">
          </TD-->
          <TD  class= input8>
            <Input class="code" name=AgentCom onblur="queryAgentCom()">
          </TD>
	        <!--TD  class= title8>
	          业务员组别
	        </TD>
	        <TD  class= input8>
	          <Input class="readonly"  readonly name=AgentGroup verify="业务员组别notnull&len<=12" >
	        </TD-->       
	      </TR>
	      <!-- 新增代理销售业务员信息 20120517 by  gzh-->
        <TR class=common>
	      	<TD  class= title8 >
            代理销售业务员编码
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentSaleCode VALUE="" MAXLENGTH=10 CLASS=code8 ondblclick="return queryAgentSaleCode();"onblur="return queryAgentSaleCode2();">
         </TD>
          <TD  class= title8>
            代理销售业务员姓名
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentSaleName VALUE=""  readonly CLASS=common >
         </TD>         
          <TD  class= title8>
          </TD>
          <TD  class= input8>
          </TD>       
        </TR>
	      <tr class=common>
	      <!--TD  class= title8>
            投保单位章
          </TD>
          <TD  class= input8>
            <Input name=HandlerPrint class="common" >
          </TD> 
	      <TD  class= title8>
            投保单填写日期
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker"  dateFormat="short" elementtype=nacessary verify="投保单填写日期|notnull" name=HandlerDate   onfocus="fm.PolApplyDate.value=this.value" onkeyup="fm.PolApplyDate.value=this.value" onkeydown="fm.PolApplyDate.value=this.value">
          </TD>
	      	<TD  class= title8>
            收件日期
          </TD>
          <TD  class= input8>
            <Input class=coolDatePicker name=ReceiveDate verify="收单日期|date">
          </TD-->
          
	      </tr>
	      <tr class=common >
	      <!--TD  class= title8>
            收件人
          </TD>
          <TD  class= input8>
            <Input class="common" name=ReceiveOperator  elementtype=nacessary verify="收件人|notnull">
          </TD-->
	      	
          <!--TD  class= title8>
            中介公司名称
          </TD>
          <TD  class= input8>
            <Input class="common" name=AgentComName readonly >
          </TD-->
 
	      </tr>
	  	</table>
	  </div>
	  <table class="common">
        	<tr>
            	<td colspan="6"><font color="black">综合开拓标示</font><INPUT TYPE="checkbox" NAME="ExtendFlag" onclick="isExtend();"></td>
	        </tr>
	        <tr class="common8" id="ExtendID" style="display: none">
	            <td class="title8">协助销售渠道</td>
	            <td class="input8">
	                <input class="codeNo" name="AssistSaleChnl" id="AssistSaleChnl" verify="协助销售渠道|code:AssistSaleChnl" ondblclick="return showCodeList('AssistSaleChnl',[this,AssistSalechnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AssistSaleChnl',[this,AssistSalechnlName],[0,1],null,null,null,1);" /><input class="codename" name="AssistSalechnlName" readonly="readonly" elementtype=nacessary/>
	            </td>
	            <td CLASS="title">协助销售人员代码</td>
	    		<td CLASS="input" COLSPAN="1">
				<input NAME="AssistAgentCode" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return queryAssistAgent();" >
	    		</td>
	            <td  class="title" >协助销售人员姓名</td>
		        <td  class="input" COLSPAN="1">
		            <Input  name=AssistAgentName CLASS="common" elementtype=nacessary readonly>
		        </td>
	        </tr>
        </table>
        <hr />		
	  <table id="table2">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,AppntInfoDiv);">
	  			</td>
	  			<td class="titleImg">投保人信息
	  			</td>
	  		</tr>
	  </table>
	  <div id="AppntInfoDiv" style="display: ''">
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  				投保单位
	  			</td>
	  			<td class=input COLSPAN="3">
	  				<Input class= common3 name=GrpName onblur="QueryOnKeyDown()" elementtype=nacessary verify="单位名称|notnull&len<=60">
	  			</td>
	  			<td class=title COLSPAN="1">
	  				英文
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class=common name=GrpEnglishName verify="投保单位英文" >
	  			</td>
	  		</tr>
	  		<tr class=common>
	  			<TD  class= title8 COLSPAN="1">
	          联系人
	        </TD> 
	        <TD  class= input8 COLSPAN="1">
	        	<Input class= common8 name=LinkMan1 elementtype=nacessary verify="保险联系人一姓名|notnull&len<=10">
	        </TD>
	        <!--TD  class= title8 COLSPAN="1">
	          拼音
	        </TD> 
	        <TD  class= input8 COLSPAN="1"-->
	        	<Input type=hidden class= common8 name=LinkManEnglishName1  verify="保险联系人一拼音|len<=20">
	        <!--/TD-->
	        <TD  class= title8>
            移动电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Mobile1  >
          </TD>
	      </tr>
	      <tr class=common>
	      	<td class=title8 COLSPAN="1">
	联系地址
	      	</td>
	      	<td class=input8 COLSPAN="3">
		      	<input class= common3 name=LinkManAddress1 elementtype=nacessary verify="保险联系人一联系地址|notnull" readonly="readonly">
		      </td>
		      <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkManZipCode1  elementtype=nacessary  verify="邮政编码|notnull&zipcode">
          </TD>
	      </tr>
	      <tr>
        	<TD  class= title8>
            省（自治区直辖市）
          </TD>
          <TD  class= input8>
      		<Input name=ProvinceID class=codeNo ondblclick="return showCodeList('Province1',[this,Province],[0,1],null,'0','Code1',1);"  onkeyup="return showCodeListKey('Province1',[this,Province],[0,1],null,'0','Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=Province class= codeName  elementtype=nacessary readonly="readonly">
          </TD>
          <TD  class= title8>
            市
          </TD>
          <TD  class= input8>
              <Input name=CityID class=codeNo ondblclick="return showCodeList('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);"  onkeyup="return showCodeListKey('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=City class= codeName  elementtype=nacessary readonly="readonly">
          </TD>
          <TD  class= title8>
            县
          </TD>
          <TD  class= input8>
              <Input name=CountyID class=codeNo ondblclick="return showCodeList('County1',[this,County],[0,1],null,fm.CityID.value,'Code1',1);"  onkeyup="return showCodeListKey('County1',[this,County],[0,1],null,fm.CityID.value,'Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=County class= codeName  elementtype=nacessary readonly="readonly">
          </TD>
          <TD  class= title8>
            详细地址
          </TD>
          <TD  class= input8>
      		<Input class= common8 name=DetailAddress elementtype=nacessary onblur="FillAddress()">
          </TD>
        </tr>
	      <tr class=common8 >
	      	<TD  class= title8 COLSPAN="1">
            联系电话
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=Phone1 elementtype=nacessary  verify="保险联系人一联系电话|notnull">
          </TD>
          <TD  class= title8 COLSPAN="1">
            传真
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=Fax1 verify="保险联系人一传真|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            电子邮箱
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=E_Mail1 verify="保险联系人一E-MAIL|Email">
          </TD>
	      </tr>
	      <tr>
	      <TD  class= title8>
            纳税人类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=TaxpayerType verify="纳税人类型|code:GrpTaxpayerType" ondblclick="return showCodeList('GrpTaxpayerType',[this,TaxpayerTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('GrpTaxpayerType',[this,TaxpayerTypeName],[0,1],null,null,null,1);"><input class=codename name=TaxpayerTypeName readonly=true > 		     		
          </TD>                      
          <TD  class= title8>
	 客户开户银行
		  </TD>
		  <TD  class= input8>
			<Input name=CustomerBankCode class=codeNo ondblclick="return showCodeList('bank',[this,CustomerBankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,CustomerBankCodeName],[0,1],null,null,null,1);"><input class=codename name=CustomerBankCodeName readonly=true >  
		  </TD>
		  <TD  class= title8>
	客户银行账户
	      </TD>
	      <TD  class= input8>
			 <Input class="common8" name=CustomerBankAccNo verify="客户银行账户|NUM">
          </TD>  
		  </tr>
		  <TD  class= title8>
            组织机构代码
          </TD>
          <TD  class= input8>
             <Input class= common8 name=OrgancomCode verify="组织机构代码|len=10">
          </TD>               
		  <TD class="title8">
           统一社会信用代码
          </TD>
          <TD class="input8">
              <input class="common" name="UnifiedSocialCreditNo" verify="统一社会信用代码|len=18" />
          </TD>
		  <TD  class= title8>
            税务登记证号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=TaxNo>
          </TD>   
          <TD  class= title>
   授权使用客户信息
    </TD>
    <TD  class= input>
    	<input class="codeNo" name="auth" readonly=true verify="授权使用客户信息|notnull" ondblclick="return false;return showCodeList('auth',[this,authName],[0,1],null,null,null,1);" onkeyup="return false;return showCodeList('auth',[this,authName],[0,1],null,null,null,1);"><input class="codename" name="authName" readonly="readonly" elementtype="nacessary" />
    	
    </TD>
		  <tr>
		  </tr>        
	  	</table>
	  </div>
	 	<div id="PayInfoTableDiv" style="display: ''">
		  <table id="table5">
		  		<tr>
		  			<td>
		  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,PayInfoDiv);">
		  			</td>
		  			<td class="titleImg">缴费信息
		  			</td>
		  		</tr>
		  </table>
		</div>
	  <div id="PayInfoDiv" style="display: ''">
	  	<hr>
		  	<table class="common">
          <font size=2 color="#ff0000">
	        <b>新、旧缴费方式对照说明</b> 
		      &nbsp;&nbsp;1现金--->1现金、3转账支票--->3支票
		  		<tr class=common8>
				  	<TD  class= title8 COLSPAN="1">
			        缴费频次
			      </TD>
			      <TD  class= input8 COLSPAN="1">
			        <Input class=codeNo value=0 name=GrpContPayIntv  CodeData="0|^0|趸缴" ondblclick="return showCodeListEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"><input class=codename name=GrpContPayIntvName readonly=true >  
			      </TD>
			      <TD  class= title8 COLSPAN="1">
			        缴费方式
			      </TD>
			      <TD  class= input8 COLSPAN="1">
			        <Input class=codeNo name=PayMode  VALUE="1" MAXLENGTH=1 verify="付款方式|notnull&code:PayMode" ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><input class=codename name=PayModeName readonly=true elementtype=nacessary>
			      </TD>
			      <TD  class= title8 COLSPAN="1">
	            开户银行
	          </TD>
	          <TD  class= input8 COLSPAN="1">
	            <Input class=codeNo  name=BankCode style="display:'none'"  MAXLENGTH=1 verify="开户银行|code:bank&len<=24" ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1],null,null,null,1);"><input class=codename name=BankCodeName style="display:'none'" readonly=true >
	          </TD>
			    </tr>
			    <tr class=common8>
			    	<TD  class= title8>
	            账&nbsp;&nbsp;&nbsp;号
	          </TD>
	          <TD  class= input8>
	            <Input class= common8 name=BankAccNo style="display:'none'" verify="帐号|len<=40">
	          </TD>
	          <TD  class= title8>
	            户&nbsp;&nbsp;&nbsp;名
	          </TD>
	          <TD  class= input8>
	            <Input class= common8 name=AccName style="display:'none'" verify="户名|len<=40">
	          </TD>   
			    </tr>
	    	</table>
	    </div>
    	<hr>
    <table id="table7" class=common>
	  		<tr>
	  			<td COLSPAN="1">
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,NationGridDiv);">
	  			</td>
	  			<td class="titleImg" COLSPAN="1">抵达国家
	  			</td>
	  			<div id="NationGridDiv" style="display: ''">
			    <TD  class= title8 COLSPAN="1">
				    旅行类型
				  </TD>
				  <TD  class= input8 COLSPAN="1">
				    <Input class=codeNo name=DegreeType CodeData="0|^0|单次旅行^1|多次旅行" ondblclick="return showCodeListEx('StandbyFlag1',[this,StandbyFlag1Name],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StandbyFlag1',[this,StandbyFlag1Name],[0,1],null,null,null,1);"><input class=codename name=StandbyFlag1Name readonly=true verify="旅行类型|notnull" elementtype=nacessary> 
				  </TD>	
			    <TD  class=title8 COLSPAN="1">
			    </TD>
			    <TD  class=input COLSPAN="1">
			    </TD>
			    <TD  class=title8 COLSPAN="1">
			    </TD>
			    <!--TD  class= title8 COLSPAN="1">
			      目的
			    </TD-->
			    <TD  class= input8 COLSPAN="1">
			      <Input type=hidden class=codeNo name=Intention CodeData="0|^1|商务^2|旅行^3|其它" ondblclick="return showCodeListEx('Intention',[this,IntentionName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('Intention',[this,IntentionName],[0,1],null,null,null,1);"><input class=codename type=hidden name=IntentionName readonly=true >  
			    </TD>
	  		</tr>
	  </table>
	  <div id="NationGridDiv" style="display: ''">
    	<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanNationGrid" >
						</span>
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common8>
					<TD  class= title8 COLSPAN="1">
				    责任生效日期
				  </TD>
				  <TD  class= input8 COLSPAN="1">
				    <Input class=coolDatePicker name=CValiDate verify="责任生效日期|notnull"  elementtype=nacessary onkeyup="queryDays();" onfocus="queryDays();">
				  </TD>
				  <TD  class= title8 COLSPAN="1">
				    责任终止日期
				  </TD>
				  <TD  class= input8 COLSPAN="1">
				    <Input class=coolDatePicker name=CInValiDate verify="责任终止日期|notnull"  elementtype=nacessary onkeyup="queryDays();" onfocus="queryDays();">
				  </TD>
				  <TD  class= title8 COLSPAN="1">
				    责任天数
				  </TD>
				  <TD  class= input8 COLSPAN="1">
				    <Input class=readonly readonly name=Days  >
				  </TD>
				</tr>
			</table>
    </div> 

        <hr />
        
	  </div>
          <table id="table6">
        	<tr>
        		<td>
        		<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,RemarkDiv);">
        		</td>
        		<td class="titleImg">备注栏
        		</td>
        	</tr>
        </table>
        
        <div id="RemarkDiv" style="display:''">
            <table class="common">
                <tr class="common"> 
                    <td class="title8">特别约定</td>
                </tr>
                <tr class="common">	
                    <td class= title8-->
                        <textarea class="common3" name="Remark" cols="110" rows="3"></textarea>
                    </td>
                </tr>
            </table>
        </div>
        
    	<hr>
	  </div>
	  <div id="BriefModifyDiv" style="display: 'none'" align=right>
			<INPUT class=cssButton VALUE="修    改"  TYPE=button onclick="updateClick()">
			<INPUT class=cssButton VALUE="录入完毕"  TYPE=button onclick="inputConfirm()">
			<INPUT class=cssButton name="deleteButton" VALUE="下一步"  TYPE=button onclick="return intoInsured();">
		</div>
	  <span id="operateButton" >
			<table  class=common align=center>
				<tr align=right>
					<td class=button >
						&nbsp;&nbsp;
					</td>
					<td class=button width="10%" align=right>
						<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="return submitForm();">
					</td>
					<td class=button width="10%" align=right>
						<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
					</td>			
					<!--td class=button width="10%" align=right>
						<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="return queryClick();">
					</td-->			
					<!--td class=button width="10%" align=right>
						<INPUT class=cssButton name="deleteButton" VALUE="删  除"  TYPE=button onclick="return deleteClick();">
					</td-->
					<td class=button width="10%" align=right>
						<INPUT class=cssButton name="deleteButton" VALUE="下一步"  TYPE=button onclick="return intoInsured();">
					</td>
				</tr>
			</table>
		</span>
		<div id="hiddenDiv" style="display: 'none'">
		  <table id="table4">
		  		<tr>
		  			<td>
		  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ContPlanDiv);">
		  			</td>
		  			<td class="titleImg">保险计划信息
		  			</td>
		  		</tr>
		  </table>
		  <div id="ContPlanDiv" style="display: 'none'">
		  	<hr>
			  	<table class="common">
				    <tr class=common8>
				    	<TD  class= title8 COLSPAN="1">
					  	  险种编码
					  	</TD>
					  	<TD  class= input8 COLSPAN="1">
					  	  <Input class=codeNo name=RiskCode  ondblclick="return showCodeList('BriefRisk',[this,RiskCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RiskGrp',[this,RiskCodeName],[0,1],null,null,null,1);"><input class=codename name=RiskCodeName readonly=true elementtype=nacessary> 
					  	</TD>	
				    </tr>
		    	</table>
		  </div>
		  <div id="RiskDutyGridDiv" style="display: 'none'">
	    	<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanRiskDutyGrid" >
							</span>
						</td>
					</tr>
				</table>
	    </div> 
	    <div id="ContPlanGridDiv" style="display: 'none'">
	    	<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanContPlanGrid" >
							</span>
						</td>
					</tr>
				</table>
	    </div> 
	    <div id="ContPlanOperateDiv" style="display: 'none'">
	    	<table class=common >
		    	<tr class=common>
		    		<td colSpan=200></td>
			    	<td class=input colSpan=1>
				    	<INPUT class=cssButton name="saveButton" VALUE="保存险种计划"  TYPE=button onclick="return saveRiskPlan();">
				    </td>
				    <td class=input colSpan=1>
				    	<INPUT class=cssButton name="saveButton" VALUE="修改险种计划"  TYPE=button onclick="return modifyRiskPlan();">
				    </td>
				    <td class=input colSpan=1>
				    	<INPUT class=cssButton name="saveButton" VALUE="删除险种计划"  TYPE=button onclick="return deleteRiskPlan();">
				    </td>
				  </tr>
			  </table>
			</div>
		   	<table id="table3">
		  		<tr>
		  			<td>
		  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
		  			</td>
		  			<td class="titleImg">被保险人信息
		  			</td>
		  		</tr>
		  </table>
		   <div id="InsuredInfoDiv" style="display: 'none'">
		  	<hr>
			  	<table class="common">
			  		<tr class=common8>
					  	<TD  class= title8 COLSPAN="1">
				        被保险人总人数
				      </TD>
				      <TD  class= input8 COLSPAN="1">
				        <Input class= common8 name=InsuredNum elementtype=nacessary>
				      </TD>
		  <td>
			<INPUT class=cssButton VALUE="录入被保险人"  TYPE=button onclick="intoInsured()">
			</td>
				    </tr>
		    	</table>

			</td>
			</tr>
			</table>
		</div>
	  <!--隐藏域-->

	  	<table	>
	  		 <tr class=common>
	  			<td class=title COLSPAN="4">
	  				团体基本资料（团体客户号 <Input class= common8 name=GrpNo><INPUT id="butGrpNoQuery" class=cssButton VALUE="查  询" TYPE=button onclick="showAppnt();"> ）(首次投保单位无需填写客户号)
	  			</td>
	  			<TD  class= title8>
            单位地址编码
          </TD>
          <TD  class= input8>
             <Input class="code" name="GrpAddressNo"  ondblclick="getAddressCodeData();return showCodeListEx('GetGrpAddressNo',[this],[0],'', '', '', true);" onkeyup="getAddressCodeData();return showCodeListKeyEx('GetGrpAddressNo',[this],[0],'', '', '', true);">
          </TD>		
	  		</tr>
	  	</table>
		</div>
	  <%@include file="BriefContHidden.jsp"%>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>