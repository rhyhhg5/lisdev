<%--
    保存简易保单信息 2005-11-11 16:31 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  String tappntchangecontnoFlag=request.getParameter("appntchangecontnoFlag");
	String tinsuredchangecontnoFlag=request.getParameter("insuredchangecontnoFlag");
	String tinsured_RelationToAppnt=request.getParameter("insured_RelationToAppnt");
	String tappnt_IDType=request.getParameter("appnt_IDType");
  CErrors tError;
  //VData tVData = new VData();                     
  //TransferData tTransferData = new TransferData();
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  tAction = request.getParameter( "fmAction" );
  //抵达国家
  LCNationSet tLCNationSet = new LCNationSet();
  //国家代码
  String[] tNationNo      =  request.getParameterValues("NationGrid1");
	String[] tNationName	    =  request.getParameterValues("NationGrid2");
	//地址编码
	int MulCount = 0;
	if(tNationNo != null ) MulCount = tNationNo.length;
	for(int n=0 ; n < MulCount ; n++){
	System.out.println("国家代码 "+tNationNo[n]);
		LCNationSchema tLCNationSchema = new LCNationSchema();
		tLCNationSchema.setGrpContNo("00000000000000000000");
		tLCNationSchema.setNationNo(tNationNo[n]);
		tLCNationSchema.setContNo(request.getParameter("ContNo"));
		//tLCNationSchema.setNationNo(tNationNo[n]);
		tLCNationSchema.setChineseName(tNationName[n]);
		tLCNationSet.add(tLCNationSchema);
	}
	System.out.println("完成国家信息录入");
	//合同信息录入
	LCContSchema tLCContSchema = new LCContSchema();
	try{
		tLCContSchema.setContNo(request.getParameter("ContNo"));
		tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
		tLCContSchema.setAppFlag(request.getParameter("AppFlag"));
		tLCContSchema.setCValiDate(request.getParameter("CValiDate"));
		tLCContSchema.setCInValiDate(request.getParameter("CInValiDate"));
		tLCContSchema.setManageCom(request.getParameter("ManageCom"));
	}catch(Exception ex){
		ex.printStackTrace();
		System.out.println("出错了！");
	}
	//处理被保人
	
	LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
	try{
			tLCInsuredSchema.setGrpContNo("00000000000000000000");
			tLCInsuredSchema.setContNo(request.getParameter("ContNo"));
			tLCInsuredSchema.setInsuredNo(request.getParameter("insured_InsuredNo"));
			tLCInsuredSchema.setPrtNo(request.getParameter("PrtNo"));
			tLCInsuredSchema.setName(request.getParameter("insured_Name"));
			tLCInsuredSchema.setEnglishName(request.getParameter("insured_EnglishName"));
			tLCInsuredSchema.setSex(request.getParameter("insured_Sex"));
			tLCInsuredSchema.setBirthday(request.getParameter("insured_Birthday"));
			tLCInsuredSchema.setIDType(request.getParameter("insured_IDType"));
			tLCInsuredSchema.setIDNo(request.getParameter("insured_IDNo"));
			if(tinsured_RelationToAppnt.equals("00")&tappnt_IDType.equals("1")){
			tLCInsuredSchema.setOthIDNo(request.getParameter("appnt_IDNo"));
			System.out.println("zhuzhu2");
			}else{
			tLCInsuredSchema.setOthIDNo(request.getParameter("insured_OthIDNo"));
			}
			tLCInsuredSchema.setRelationToAppnt(request.getParameter("insured_RelationToAppnt"));
			tLCInsuredSchema.setManageCom(request.getParameter("ManageCom"));
			tLCInsuredSchema.setExecuteCom(request.getParameter("insured_ExecuteCom"));
			System.out.println("处理完成被保人信息！");
	}catch(Exception ex){
		ex.printStackTrace();
		System.out.println("出错！！！");
	}
	//处理投保人
	
	LCAppntSchema tLCAppntSchema = new LCAppntSchema();
	try{
		tLCAppntSchema.setGrpContNo("00000000000000000000");
		tLCAppntSchema.setContNo(request.getParameter("ContNo"));
		tLCAppntSchema.setPrtNo(request.getParameter("PrtNo"));
		tLCAppntSchema.setAppntNo(request.getParameter("appnt_AppntNo"));
		if(tinsured_RelationToAppnt.equals("00")&tappnt_IDType.equals("1")){
		tLCAppntSchema.setOthIDNo(request.getParameter("appnt_IDNo"));
		System.out.println("zhuzhu1");
		}else{
		tLCAppntSchema.setOthIDNo(request.getParameter("appnt_OthIDNo"));
		}
		tLCAppntSchema.setEnglishName(request.getParameter("appnt_EnglishName"));
		tLCAppntSchema.setAppntName(request.getParameter("appnt_AppntName"));
		tLCAppntSchema.setAppntSex(request.getParameter("appnt_AppntSex"));
		tLCAppntSchema.setAppntBirthday(request.getParameter("appnt_AppntBirthday"));
		tLCAppntSchema.setIDType(request.getParameter("appnt_IDType"));
		tLCAppntSchema.setIDNo(request.getParameter("appnt_IDNo"));
	}catch(Exception ex){
		ex.printStackTrace();
		System.out.println("出错了~~~~");
	}
	System.out.println("处理完成投保人信息！");
	System.out.println("开始处理地址信息~");
	LCAddressSchema mAppntAddressSchema = new LCAddressSchema();
	LCAddressSchema mInsuredAddressSchema = new LCAddressSchema();
	/** 
   * 1、联系地址 PostalAddress
   * 2、邮政编码 ZipCode
   * 3、家庭电话 HomePhone
   * 4、移动电话 Mobile
   * 5、办公电话 CompanyPhone
   * 6、电子邮箱 EMail
   */
   try{          
   		mAppntAddressSchema.setCustomerNo(request.getParameter("appnt_AppntNo"));
   		mAppntAddressSchema.setAddressNo(request.getParameter("appnt_AddressNo"));
   		mAppntAddressSchema.setPostalAddress(request.getParameter("appnt_PostalAddress"));
   		mAppntAddressSchema.setZipCode(request.getParameter("appnt_ZipCode"));
   		mAppntAddressSchema.setHomePhone(request.getParameter("appnt_HomePhone"));
   		mAppntAddressSchema.setMobile(request.getParameter("appnt_Mobile"));
   		mAppntAddressSchema.setCompanyPhone(request.getParameter("appnt_CompanyPhone"));
   		mAppntAddressSchema.setEMail(request.getParameter("appnt_EMail"));
   }catch (Exception ex) {
   		ex.printStackTrace();
   		System.out.println("出错！");
   }
   System.out.println("开始处理被保人地址信息");
   try{
   		mInsuredAddressSchema.setCustomerNo(request.getParameter("insured_InsuredNo"));
   		mInsuredAddressSchema.setAddressNo(request.getParameter("insured_AddressNo"));
   		mInsuredAddressSchema.setPostalAddress(request.getParameter("insured_PostalAddress"));
   		mInsuredAddressSchema.setZipCode(request.getParameter("insured_ZipCode"));
   		mInsuredAddressSchema.setHomePhone(request.getParameter("insured_HomePhone"));
   		mInsuredAddressSchema.setMobile(request.getParameter("insured_Mobile"));
   		mInsuredAddressSchema.setCompanyPhone(request.getParameter("insured_CompanyPhone"));
   		mInsuredAddressSchema.setEMail(request.getParameter("insured_EMail"));
   }catch (Exception ex) {
   		ex.printStackTrace();
   		System.out.println("出错了！");
   }
   System.out.println("处理完成");
   System.out.println("开始处理险种信息");
   //完成全部的封装，开始递交．
   BriefSingleContModifyUI tBriefSingleContModifyUI = new BriefSingleContModifyUI();

   		VData tVData = new VData();
   		TransferData tTransferData = new TransferData();
   		tTransferData.setNameAndValue("AppntAddress",mAppntAddressSchema);
   		tTransferData.setNameAndValue("InsuredAddress",mInsuredAddressSchema);
   		tTransferData.setNameAndValue("appntchangecontnoFlag",tappntchangecontnoFlag );
			tTransferData.setNameAndValue("insuredchangecontnoFlag",tinsuredchangecontnoFlag );
   		tVData.add(tLCContSchema);
   		tVData.add(tLCInsuredSchema);
   		tVData.add(tLCAppntSchema);
   		tVData.add(tLCNationSet);
   		tVData.add(tG);
   		tVData.add(tTransferData);
   		
   		System.out.println("开始递交数据！"+tAction);
   		System.out.println("递交数据到BriefSingleContModifyUI！");
   		try{
   			tBriefSingleContModifyUI.submitData(tVData,tAction);
   		}
   		catch(Exception ex){
   			ex.printStackTrace();
   			Content = " 保存失败，原因是: " + ex.getMessage();
				FlagStr = "Fail";
   		}

 
   if (FlagStr=="")
	 {
	   tError = tBriefSingleContModifyUI.mErrors;
	   if (!tError.needDealError())
	   {                          
	   	Content = " 保存成功! ";
	   	FlagStr = "Success";
	   }
	   else                                                                           
	   {
	   	Content = "保存失败，原因是:" + tError.getFirstError();
	   	FlagStr = "Fail";
	   }
	 }
	 VData tResult = tBriefSingleContModifyUI.getResult();
	 /*
	 LCContSchema resultLCContSchema = new LCContSchema();
	 String ContNo ="";
	 if(FlagStr.equals("Success")){
	 		resultLCContSchema = (LCContSchema) tResult.getObjectByObjectName("LCContSchema",0);
	 		ContNo = resultLCContSchema.getContNo();
	 }
*/   
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>