/**
 * 程序名称：BriefContQueryInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-08-18 11:51:23
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}

//初始化页面控件
function initInpBox()
{ 
  try 
  {     
    fm.all('PrtNo').value = "";

    fm.all('InsuredName').value = "";
    fm.all('AgentCode').value = "";
    fm.all('AgentCom').value = "";
    
    if(Type=="Agent" )
    {
    	if(AgentCom!=null && AgentCom!="" && AgentCom!="null")
    	{
    		fm.all('AgentCom').value = AgentCom;
      }
    	fm.all('AgentCom').readOnly = "true";
    	fm.all('AgentCom').className="readOnly";
    }
  }
  catch(ex) 
  {
    alert("在BriefContQueryQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
  
  fm.InputEndDate.value = tCurrentDate;
  
  var sql = "select date('" + tCurrentDate + "') - 1 month from dual ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.InputStartDate.value = rs[0][0];
  }
}      
                              
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
//function easyQueryClick() {
//	//此处书写SQL语句
///*
//	var strSql = "select a.GetNoticeNo, a.otherno, b.name, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
//	           + " from LJSPay a, ldperson b where a.appntno=b.customerno and OtherNoType='2' "
//          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
//          	 + getWherePart("a.OtherNo", "OtherNo")
//          	 + getWherePart("a.BankCode", "BankCode")
//          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
//
//  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')";
//  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select polno from lcpol where prtno='" + fm.PrtNo.value + "')";
//*/
//	//A 印刷号
//	//B 合同号
//	//F 生效日期
//	//H 出单日期
//	//D 投保人
//	//G 保费
//	//J 代理人
//	//I 中介机构
//	//C 被保人
//	initBriefContQueryGrid();
//
//	var strSql =  " select sum(Y) from ( "
//								+" select SumDuePayMoney Y, "
//								+"   case when OtherNoType='14' then "
//								+"       (select PrtNo from LCGrpCont where GrpContNo=a.OtherNo "
//								+"       union "
//								+"       select PrtNo from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"       (select PrtNo from LCCont where ContNo=a.OtherNo "
//								+"       union "
//								+"       select PrtNo from LBCont where ContNo=a.OtherNo) end A, "
//								+"   a.OtherNo B,a.AgentCom I, "
//								+"   case when OtherNoType='14' then "
//								+"     (select AgentCode from LCGrpCont where GrpContNo=a.OtherNo "
//								+"     union "
//								+"     select AgentCode from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"     (select AgentCode from LCCont where ContNo=a.OtherNo "
//								+"     union "
//								+"     select AgentCode from LBCont where ContNo=a.OtherNo) end J, "
//								+"   case when OtherNoType='14' then "
//								+"       (select CValiDate from LCGrpCont where GrpContNo=a.OtherNo "
//								+"       union "
//								+"       select CValiDate from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"       (select CValiDate from LCCont where ContNo=a.OtherNo "
//								+"       union "
//								+"       select CValiDate from LBCont where ContNo=a.OtherNo) end F "
//								+"from LJSPayB a    where 1=1 and OtherNoType in ('14','15') "
//								+" ) as x "
//								+" where 1=1 "
//								+getWherePart("x.a","PrtNo")
//								+getWherePart("x.B","ContNo")
//								+getWherePart("x.J","AgentCode")
//								+getWherePart("x.I","AgentCom")
//								+getWherePart("x.F","CValiDateStart",">=")
//								+getWherePart("x.F","CValiDateEnd","<=")
//								+getWhereInsuredNamePart("x.B",fm.InsuredName.value);
//	 var arr = easyExecSql(strSql);
//	 if(arr){
//		 	fm.SumPrem.value=arr;
//	 }
//	 		var strSql =  " select count(1) from ( "
//								+" select "
//								+"   case when OtherNoType='14' then "
//								+"       (select PrtNo from LCGrpCont where GrpContNo=a.OtherNo "
//								+"       union "
//								+"       select PrtNo from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"       (select PrtNo from LCCont where ContNo=a.OtherNo "
//								+"       union "
//								+"       select PrtNo from LBCont where ContNo=a.OtherNo) end A, "
//								+"   a.OtherNo B,a.AgentCom I, "
//								+"   case when OtherNoType='14' then "
//								+"     (select AgentCode from LCGrpCont where GrpContNo=a.OtherNo "
//								+"     union "
//								+"     select AgentCode from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"     (select AgentCode from LCCont where ContNo=a.OtherNo "
//								+"     union "
//								+"     select AgentCode from LBCont where ContNo=a.OtherNo) end J, "
//								+"   case when OtherNoType='14' then "
//								+"       (select CValiDate from LCGrpCont where GrpContNo=a.OtherNo "
//								+"       union "
//								+"       select CValiDate from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"       (select CValiDate from LCCont where ContNo=a.OtherNo "
//								+"       union "
//								+"       select CValiDate from LBCont where ContNo=a.OtherNo) end F "
//								+"from LJSPayB a    where 1=1 and OtherNoType in ('14','15') "
//								+" ) as x "
//								+" where 1=1 "
//								+getWherePart("x.a","PrtNo")
//								+getWherePart("x.B","ContNo")
//								+getWherePart("x.J","AgentCode")
//								+getWherePart("x.I","AgentCom")
//								+getWherePart("x.F","CValiDateStart",">=")
//								+getWherePart("x.F","CValiDateEnd","<=")
//								+getWhereInsuredNamePart("x.B",fm.InsuredName.value);
//	 var arr = easyExecSql(strSql);
//	 if(arr){
//		 	fm.Count.value=arr;
//	 }
//	var strSql =  " select A,B,F,H,D,C,G,J,I from ( "
//								+"   select "
//								+"   case when OtherNoType='14' then "
//								+"       '--' "
//								+"     else "
//								+"       (select InsuredName from LCCont where ContNo=a.OtherNo "
//								+"       union "
//								+"       select InsuredName from LBCont where ContNo=a.OtherNo) end C, "
//								+"   case when OtherNoType='14' then "
//								+"       (select PrtNo from LCGrpCont where GrpContNo=a.OtherNo "
//								+"       union "
//								+"       select PrtNo from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"       (select PrtNo from LCCont where ContNo=a.OtherNo "
//								+"       union "
//								+"       select PrtNo from LBCont where ContNo=a.OtherNo) end A, "
//								+"   a.OtherNo B, "
//								+"   case when OtherNoType='14' then "
//								+"     (select GrpName from LDGrp where CustomerNo=a.AppntNo) "
//								+"      else "
//								+"      (select Name from LDPerson where CustomerNo=a.AppntNo) end D, "
//								+"   case when OtherNoType='14' then "
//								+"   (select count(distinct ContNo) from LJSPayPersonB where GrpContNo=a.OtherNo) "
//								+"   else "
//								+"   1  end E, "
//								+"   case when OtherNoType='14' then "
//								+"       (select CValiDate from LCGrpCont where GrpContNo=a.OtherNo "
//								+"       union "
//								+"       select CValiDate from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"       (select CValiDate from LCCont where ContNo=a.OtherNo "
//								+"       union "
//								+"       select CValiDate from LBCont where ContNo=a.OtherNo) end F, "
//								+"   a.SumDuePayMoney G,a.PayDate H,a.AgentCom I, "
//								+"   case when OtherNoType='14' then "
//								+"     (select AgentCode from LCGrpCont where GrpContNo=a.OtherNo "
//								+"     union "
//								+"     select AgentCode from LBGrpCont where GrpContNo=a.OtherNo) "
//								+"     else "
//								+"     (select AgentCode from LCCont where ContNo=a.OtherNo "
//								+"     union "
//								+"     select AgentCode from LBCont where ContNo=a.OtherNo) end J "
//								+" from LJSPayB a    where 1=1 and OtherNoType in ('14','15') "
//								//+" and "
//								+" ) as x "
//								+" where 1=1 "
//								+getWherePart("x.a","PrtNo")
//								+getWherePart("x.B","ContNo")
//								+getWherePart("x.J","AgentCode")
//								+getWherePart("x.I","AgentCom")
//								+getWherePart("x.F","CValiDateStart",">=")
//								+getWherePart("x.F","CValiDateEnd","<=")
//								+getWhereInsuredNamePart("x.B",fm.InsuredName.value);
//  //alert(strSql);
//	if(fm.InsuredName.value != null && fm.InsuredName.value != "" && fm.InsuredName.value != "null"){
//		turnPage.queryModal(strSql, BriefContQueryGrid,null,null,1);
//	}else{
//		turnPage.queryModal(strSql, BriefContQueryGrid);
//	}
//}

// 查询按钮                  
function easyQueryClick() {
  
  if(fm.PrtNo.value == null || fm.PrtNo.value == "")
  {
    if(!checkData())
    {
      return false;
    }
  }

  //保单状态
	var contState = fm.state.value;
	var stateSql = "";
	if(contState != null && !contState == "")
	{
	  if(contState == "0")
	  {
	    stateSql = " and 1 = 2 ";
	  }
	  else if(contState == "1")
	  {
	    stateSql = " and AppFlag <> '1' and not exists (select * from LWMission where processid ='0000000007' and activityid='0000007002' and MissionProp2=prtno) ";
	  }
	  else if(contState == "2")
	  {
	    stateSql = " and AppFlag <> '1' and exists (select 1 from LWMission where processid ='0000000007' and activityid='0000007002' and MissionProp2=prtno) "
	            + "and not exists (select 1 from ljtempfee ljt where ljt.otherno = prtno and othernotype = '4' and ljt.enteraccdate is not null and confdate is null) ";
	  }
	  else if(contState == "3")
	  {
	    stateSql = " and AppFlag <> '1' and exists (select 1 from LWMission where processid ='0000000007' and activityid='0000007002' and MissionProp2=prtno) "
	            + "and exists (select 1 from ljtempfee ljt where ljt.otherno = prtno and othernotype = '4' and ljt.enteraccdate is not null and confdate is null) ";
	  }
	  else if(contState == "4")
	  {
	    stateSql = " and AppFlag = '1' ";
	  }
	  else if(contState == "6")
	  {
	    stateSql = " and UWFlag = 'a' and AppFlag <> '1' ";
	  }
	}
	
	//银行销售保单查询  增加对电话销售保单的查询   增加对意外险保单的查询
	var strSql =  "select missionprop1,'未录单',missionprop3,'未录单','未录单','未录单','未录单',"
	             + "missionprop2,'未录单','未录单','未录单','未录单',0,'未录单','未录单','未录单' "
	             + "from lwmission where missionprop5 in ('2','5', '6') and activityid in ('0000007001', '0000007003') "	
	             + getWherePart("missionprop1","PrtNo")
	             + getWherePart("missionprop3","ManageCom",'like',null,"%")
	             //+getWherePart("missionprop2","InputDate")
	             + getWherePart("missionprop2","InputStartDate",">=")
	             + getWherePart("missionprop2","InputEndDate","<=")
                 ;
    if(contState == "1" || contState == "2" || contState == "3" || contState == "4" || contState == "6")
    {
        strSql += " and 1 = 2 ";
    }
    // 加入渠道、网点条件
    if(fm.SaleChnlCode.value != "" || fm.AgentComBank.value != "")
    {
        strSql += " and 1 = 2 ";
    }
    var s = "";
    if(fm.all('AgentCode').value!=""){
    	s = " and lccont.agentcode in ( select agentcode from laagent where groupagentcode = '"+fm.all('AgentCode').value+"' )"
    }
    strSql  +=" and not exists(select 'x' from lccont where prtno = missionprop1)"
            +" union "
            +" select prtno,ContNo,managecom,agentcom,(select groupagentcode from laagent where laagent.agentcode = lccont.agentcode ),"
            +" appntname ,InputOperator,"
            +" polapplydate,varchar(CValidate),bankcode,bankaccno,accname,prem,"
            +" case appflag when '1' then '已承保' else case UWFlag when 'a' then '已撤单' else case "
            +" (select count(1) from LWMission where processid ='0000000007' and activityid='0000007002' and MissionProp2=prtno) "
            +" when 1 then case (select count(1) from ljtempfee ljt where ljt.otherno = prtno and othernotype = '4' and ljt.enteraccdate is not null and confdate is null) "
            +" when 0 then '待收费' else '待签单' end else '录入中' end end end, "
            +" CodeName('lcsalechnl', SaleChnl), AgentCom "
            +" from lccont where cardflag in ('2','5', '6') "
            + s
            + stateSql
            + getWherePart("prtno","PrtNo")
            + getWherePart("managecom","ManageCom",'like',null,"%")
            //+getWherePart("polapplydate","InputDate")
            + getWherePart("polapplydate","InputStartDate",">=")
            + getWherePart("polapplydate","InputEndDate","<=")
            + getWherePart("AgentCom","AgentCom")
            //+ getWherePart("AgentCode","AgentCode")
            + getWherePart("AppntName","AppntName")
            // 加入渠道、网点条件
            + getWherePart("SaleChnl","SaleChnlCode")
            + getWherePart("AgentCom","AgentComBank")
            + getWherePart("InsuredName","InsuredName")
            + getWherePart("BankCode","BankCode")
            + getWherePart("Bankaccno","BankAccNo")
            + getWherePart("AccName","AccName")
            + " with ur ";

  //万能险保单查询
	if(fm.CardFlag.value == "8")
	{
	  if(contState == "0")
	  {
	    strSql = "select missionprop1,'未录单',missionprop3,'未录单','未录单','未录单','未录单',"
	             + "missionprop2,'未录单','未录单','未录单','未录单',0,'未录单','未录单','未录单'"
	             + "from lwmission where activityid = '0000009001'"	
	             + getWherePart("missionprop1","PrtNo")
	             + getWherePart("missionprop3","ManageCom",'like',null,"%")
	             + getWherePart("missionprop2","InputStartDate",">=")
	             + getWherePart("missionprop2","InputEndDate","<=")
               + " and not exists(select 'x' from lccont where prtno = missionprop1) ";
    }
    else
    {
    	if(contState != null && !contState == "")
    	{
    	  if(contState == "1")
    	  {
    	    stateSql = " and exists (select 1 from LWMission where MissionProp1 = PrtNo and ActivityID = '0000009001') ";
    	  }
    	  else if(contState == "5")
    	  {
    	    stateSql = " and exists (select 1 from LWMission where MissionProp2 = PrtNo and ActivityID = '0000009002') ";
    	  }
    	  else if(contState == "2")
    	  {
    	    stateSql = " and exists (select 1 from LWMission where MissionProp2 = PrtNo and ActivityID = '0000009004') and not exists(select 1 from LJTempFee where OtherNo = PrtNo and ConfMakeDate is not null) ";
    	  }
    	  else if(contState == "3")
    	  {
    	    stateSql = " and exists (select 1 from LWMission where MissionProp2 = PrtNo and ActivityID = '0000009004') and exists(select 1 from LJTempFee where OtherNo = PrtNo and ConfMakeDate is not null) ";
    	  }
    	  else if(contState == "4")
    	  {
    	    stateSql = " and AppFlag = '1' ";
    	  }
	      else if(contState == "6")
	      {
	        stateSql = " and UWFlag = 'a' and AppFlag <> '1' ";
	      }
    	}
	
      strSql  = "select prtno,ContNo,managecom,agentcom,(select groupagentcode from laagent where laagent.agentcode = lccont.agentcode ),"
    	      +"appntname ,InputOperator,"
              + "  polapplydate,varchar(CValidate),bankcode,bankaccno,accname,prem,"
              + "  case appflag "
              + "    when '1' then '已承保' "
              + "    else case when UWFlag = 'a' then '已撤单' "
              + "              when exists (select 1 from LWMission where MissionProp1 = PrtNo and ActivityID = '0000009001') then '录入中' "
              + "              when exists (select 1 from LWMission where MissionProp2 = PrtNo and ActivityID = '0000009002') then '待复核' "
              + "              when exists (select 1 from LWMission where MissionProp2 = PrtNo and ActivityID = '0000009004') and not exists(select 1 from LJTempFee where OtherNo = PrtNo and ConfMakeDate is not null) then '待收费' "
              + "              when exists (select 1 from LWMission where MissionProp2 = PrtNo and ActivityID = '0000009004') and exists(select 1 from LJTempFee where OtherNo = PrtNo and ConfMakeDate is not null) then '待签单' "
              + "         end "
              + "  end, "
              + "  CodeName('lcsalechnl', SaleChnl), AgentCom "
              + "from lccont where cardflag = '8'"
              + s
              + stateSql
              + getWherePart("prtno","PrtNo")
              + getWherePart("managecom","ManageCom",'like',null,"%")
              + getWherePart("polapplydate","InputStartDate",">=")
              + getWherePart("polapplydate","InputEndDate","<=")
              
              + getWherePart("AgentCom","AgentCom")
              //+ getWherePart("AgentCode","AgentCode")
              + getWherePart("AppntName","AppntName")
                
              // 加入渠道、网点条件
              + getWherePart("SaleChnl","SaleChnlCode")
              + getWherePart("AgentCom","AgentComBank")
              
              + getWherePart("InsuredName","InsuredName")
              + getWherePart("BankCode","BankCode")
              + getWherePart("Bankaccno","BankAccNo")
              + getWherePart("AccName","AccName");
	  }
	}
    
  var result = easyQueryVer3(strSql, 1, 0, 1); 
  if (!result) {
    alert("没有查询到保单信息！");
    initBriefContQueryGrid();
    return "";
  }
  turnPage.pageLineNum = 20;
	turnPage.queryModal(strSql, BriefContQueryGrid);

}

function checkData()
{
  if(fm.InputStartDate.value == "" || fm.InputEndDate.value == "")
  {
    alert("保单申请起、止日期均不能为空");
    return false;
  }
  
  var sql = "select 1 from Dual "
          + "where date('" + fm.InputEndDate.value + "') - 3 month > date('" + fm.InputStartDate.value + "') ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    if(!confirm("保单申请起、止日期均超过3个月，数据较多，可能查询较慢，继续？"))
    {
      return false;
    }
  }
  
  return true;
}

function showOne(parm1, parm2) {
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = BriefContQueryGrid.getSelNo();



	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{

			try
			{
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();

	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = BriefContQueryGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;

	arrSelected = new Array();

	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = BriefContQueryGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}
function getWhereInsuredNamePart(tField,tValue){
	if(tValue != "" && tValue != null && tValue != "null"){
		var strSql = " in (select ContNo from lcinsured where Name='"+tValue
		          +"' union select GrpContNo from lcinsured where Name='"+tValue
		          +"' union select ContNo from lbinsured where Name='"+tValue
		          +"' union select GrpContNo from lbinsured where Name='"+tValue+"') ";
		return " and "+tField + strSql;
	}
	return "";
}

/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    var tTmpUrl = "../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01";
    showInfo=window.open(tTmpUrl);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}
/**
 * EXCEL下载
 */
function queryDown()
{
  if(BriefContQueryGrid.mulLineCount == 0)
  {
    alert("没有需要下载的数据");
    return false;
  }
  fm.sql.value = turnPage.strQuerySql;
  //fm.target='_blank';
  fm.action = "BriefContQueryDown.jsp";
  fm.submit();
}