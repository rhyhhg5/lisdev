<%--
    境外救援险种信息录入 2006-6-27 14:28 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  CErrors tError = null;
  GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	System.out.println("处理完成");
	System.out.println("开始处理险种信息");
	String tRiskNum[] = request.getParameterValues("RiskGridNo");
	String tRiskCode[] = request.getParameterValues("RiskGrid1");            //险种编码
	String tMultAndAmnt[] = request.getParameterValues("RiskGrid3");            //险种编码
	String tFloatRate[] = request.getParameterValues("RiskGrid4");            //险种编码
	String tRiskPrem[] = request.getParameterValues("RiskGrid5");            //险种编码
	String tChk[] = request.getParameterValues("InpRiskGridChk");      
	String tGrpContNo    =  request.getParameter("GrpContNo");                
	System.out.println("险种个数 ："+tChk.length);
    
    // 附加责任选择标志。
    String tAppendDutyFlag = request.getParameter("AppendDutyFlag");
    if(tAppendDutyFlag == null || "".equals(tAppendDutyFlag))
    {
        // 默认为“0：不选择”
        tAppendDutyFlag = "0";
    }
    // ----------------------------
    
    // 附加责任选择标志
    String tPlanCode = request.getParameter("PlanCode");
    System.out.println("PlanCode:" + tPlanCode);
    // ----------------------------
    
	LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = null;
	LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
	tLCGrpContDB.setGrpContNo(tGrpContNo);
	VData tVData = new VData();
	if(!tLCGrpContDB.getInfo()){
		FlagStr = "Fail";
	}else{
		String tDegreeType = tLCGrpContDB.getDegreeType();
		for(int index=0;index<tChk.length;index++)
		{
		  if(tChk[index].equals("1"))
		  {
				//先保存保额/档次
				tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
				tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
				tLCContPlanDutyParamSchema.setContPlanName("默认计划");
				if(Integer.parseInt(tMultAndAmnt[index])>20){
					tLCContPlanDutyParamSchema.setCalFactor("Amnt");
		    	tLCContPlanDutyParamSchema.setCalFactorValue(tMultAndAmnt[index]);
		    }else{
		    	tLCContPlanDutyParamSchema.setCalFactor("Mult");
		    	tLCContPlanDutyParamSchema.setCalFactorValue(tMultAndAmnt[index]);
		    }
		    tLCContPlanDutyParamSchema.setPayPlanCode("000000");
				tLCContPlanDutyParamSchema.setGetDutyCode("000000");
				tLCContPlanDutyParamSchema.setInsuAccNo("000000");
				tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
				
				//保存计算方向,全部默认为表定费率折扣
				tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
				tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
				tLCContPlanDutyParamSchema.setContPlanName("默认计划");
				System.out.println("保费这种 ："+tRiskPrem[index]);
				if(!tRiskPrem[index].equals("")){
				System.out.println("保费这种10 ："+tRiskPrem[index]);
		    tLCContPlanDutyParamSchema.setCalFactor("CalRule");
		    tLCContPlanDutyParamSchema.setCalFactorValue("3");				
				}else{
				System.out.println("保费这种20 ："+tRiskPrem[index]);
		    tLCContPlanDutyParamSchema.setCalFactor("CalRule");
		    tLCContPlanDutyParamSchema.setCalFactorValue("2");
		    }
		    tLCContPlanDutyParamSchema.setPayPlanCode("000000");
				tLCContPlanDutyParamSchema.setGetDutyCode("000000");
				tLCContPlanDutyParamSchema.setInsuAccNo("000000");
				tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
				
				//处理折扣/保费问题
				tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
				tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
				tLCContPlanDutyParamSchema.setContPlanName("默认计划");
		    System.out.println("保费这种10 ："+tRiskPrem[index]);
		    tLCContPlanDutyParamSchema.setCalFactor("Prem");
		    tLCContPlanDutyParamSchema.setCalFactorValue(tRiskPrem[index]);				
		    tLCContPlanDutyParamSchema.setPayPlanCode("000000");
				tLCContPlanDutyParamSchema.setGetDutyCode("000000");
				tLCContPlanDutyParamSchema.setInsuAccNo("000000");
				tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
				
								//处理折扣/保费问题
				tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
				tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
				tLCContPlanDutyParamSchema.setContPlanName("默认计划");
		    tLCContPlanDutyParamSchema.setCalFactor("FloatRate");
		    tLCContPlanDutyParamSchema.setCalFactorValue(tFloatRate[index]);
		    tLCContPlanDutyParamSchema.setPayPlanCode("000000");
				tLCContPlanDutyParamSchema.setGetDutyCode("000000");
				tLCContPlanDutyParamSchema.setInsuAccNo("000000");
				tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
				
				//单次多次
				tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
				tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
				tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[index]);
				tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
				tLCContPlanDutyParamSchema.setContPlanName("默认计划");
		    tLCContPlanDutyParamSchema.setCalFactor("StandbyFlag1");
		    tLCContPlanDutyParamSchema.setCalFactorValue(tDegreeType);
		    tLCContPlanDutyParamSchema.setPayPlanCode("000000");
				tLCContPlanDutyParamSchema.setGetDutyCode("000000");
				tLCContPlanDutyParamSchema.setInsuAccNo("000000");
				tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
                
                // 1502需保存附加责任选择要素。
                if("1502".equals(tRiskCode[index]))
                {
                    tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
                    tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
                    tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
                    tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[index]);
                    tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[index]);
                    tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
                    tLCContPlanDutyParamSchema.setContPlanName("默认计划");
                    tLCContPlanDutyParamSchema.setCalFactor("StandbyFlag2");
                    tLCContPlanDutyParamSchema.setCalFactorValue(tAppendDutyFlag);
                    tLCContPlanDutyParamSchema.setPayPlanCode("000000");
                    tLCContPlanDutyParamSchema.setGetDutyCode("000000");
                    tLCContPlanDutyParamSchema.setInsuAccNo("000000");
                    tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
                }
                // --------------------------
                
            // 套餐方案编码
            if(tPlanCode != null && !"".equals(tPlanCode))
            {
                tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
                tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
                tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
                tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode[index]);
                tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[index]);
                tLCContPlanDutyParamSchema.setContPlanCode("00");//默认计划
                tLCContPlanDutyParamSchema.setContPlanName("默认计划");
                tLCContPlanDutyParamSchema.setCalFactor("PlanCode");
                tLCContPlanDutyParamSchema.setCalFactorValue(tPlanCode);
                tLCContPlanDutyParamSchema.setPayPlanCode("000000");
                tLCContPlanDutyParamSchema.setGetDutyCode("000000");
                tLCContPlanDutyParamSchema.setInsuAccNo("000000");
                tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
            }
            // --------------------------
		  }
		}
                
		tVData.add(tLCContPlanDutyParamSet);
		tVData.add(tLCGrpContDB.getSchema());
		tVData.add(tG);
	}
		BriefInsuredUI tBriefInsuredUI = new BriefInsuredUI();
	if(!tBriefInsuredUI.submitData(tVData,tOperate))
	{
		Content = "保存失败，原因是:" + tBriefInsuredUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
	if (!FlagStr.equals("Fail")){
		tError = tBriefInsuredUI.mErrors;
		if (!tError.needDealError()){
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>