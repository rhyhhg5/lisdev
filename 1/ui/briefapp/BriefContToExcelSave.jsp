<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDDiseaseNameSave.jsp
//程序功能：
//创建日期：2005-10-9 20:45
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.brieftb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.AccessVtsFile"%>
  <%@page import="java.util.*"%>
	<%@page import="java.io.*"%>
<%
	//输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tempExcelUrl ="";
  transact = request.getParameter("fmtransact");
  String Sql = request.getParameter("Sql");
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String sOutPutPath = application.getRealPath("");	//Excel文件输出路径
  
  BriefGrpContToExcelUI tBriefGrpContToExcelUI   = new BriefGrpContToExcelUI();
	try
  {
  	//准备传输数据 VData
  	VData tVData = new VData();
  	TransferData tTransferData = new TransferData();
  	tTransferData.setNameAndValue("Sql",Sql);
  	tTransferData.setNameAndValue("OutPutPath",sOutPutPath);
  	tVData.add(tG);
  	tVData.add(tTransferData);
  	System.out.println(transact);
    tBriefGrpContToExcelUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tBriefGrpContToExcelUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%
	if(FlagStr.equals("Success")){
		  VData tempVData = tBriefGrpContToExcelUI.getResult();
  		TransferData tempTransferData = (TransferData) tempVData.getObjectByObjectName("TransferData", 0);
  		tempExcelUrl = (String)tempTransferData.getValueByName("ExcelUrl");
  		String ExcelFileName = (String)tempTransferData.getValueByName("ExcelFileName");
  		FileOutputStream ExcelFileOut = null;
  		InputStream ins = null;
  		try {
        ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
 				AccessVtsFile.loadToBuffer(dataStream,sOutPutPath+"/ExcelFile/"+ExcelFileName);
 				byte[] bArr = dataStream.toByteArray(); 
 				ins = new ByteArrayInputStream(bArr);
      }
      catch(Exception ex) {
      	Content = " 保存失败，原因是:" + ex.getMessage();
    		FlagStr = "Fail";
      }
		  response.setContentType("application/octet-stream");
  		response.setHeader("Content-Disposition","attachment; filename="+ExcelFileName);
  		OutputStream ous = response.getOutputStream();
  		System.out.println("ins "+ins);
  		int n = 0;
      //采用缓冲池的方式写文件，针对I/O修改
	    byte[] c = new byte[4096];
	    while ((n = ins.read(c)) != -1) {
	        ous.write(c, 0, n);
	    }
    	ous.flush();
    	ous.close();
    	ins.close();
    	ins = null;
    	System.out.println(" End ");
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.showInfo.close();
</script>