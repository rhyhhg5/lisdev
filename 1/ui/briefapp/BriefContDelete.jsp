<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：BriefContDelete.jsp
//程序功能：简易保单删除界面
//创建日期：2007-11-21
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>

<head >
<meta http-equiv="Content-Type" content="text/html charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="BriefContDelete.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BriefContDeleteInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>	印刷号	</TD>
          <TD  class= input>
            <Input class=common name="PrtNo" verify="印刷号码|notnull&int" elementtype=nacessary>
          </TD>
          <TD  class= title>	管理机构	</TD>
          <TD  class= input>
          	<Input class=codeNo readonly=true name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
          </TD>          
		</TR>
		<TR>
		  <TD class= common id=ContType_TD name=ContType1>	保单类型	</TD>
          <TD class=input>
          	<Input class=codeno name=ContType verify="保单类型|notnull" CodeData="0|^1|境外救援|^2|意外险平台^3|高原疾病^6|银行保险" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);"><input class=codename name=ContTypeName readonly=true elementtype=nacessary>
          </TD>
          <TD  class= title>	业务员代码	</TD>
          <TD  class= input>
            <Input class=codeNo name=AgentCode  verify="业务员代码|code:AgentCodet1" ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
          </TD>
        </TR>
    </table>
       <INPUT VALUE="查  询" Class="cssButton" TYPE=button onclick="return query();">
       <INPUT VALUE="重  置" Class="cssButton" TYPE=button onclick="resetForm();">
       <INPUT type= "hidden" name= "Operator" value= "">
       <INPUT type= "hidden" name= "ContNo" value= "">
    <!-- 查询未过单（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCcont);">
    		</td>
    		<td class= titleImg>
    			 投保单查询结果
    		</td>
    	</tr>
    </table>
    
    <!-- 信息（列表） -->
    <div id="divLCcont" style="display:''">
        <span id="spanBriefGrid"></span>
    </div>
    <div id= "divPage" align="center" style= "display:'none'">
        <input class="cssButton" value="首  页" title="The First Page" type="button" onclick="turnPage.firstPage();" /> 
        <input class="cssButton" value="上一页" title="Back" type="button" onclick="turnPage.previousPage();" /> 					
        <input class="cssButton" value="下一页" title="Next" type="button" onclick="turnPage.nextPage();" /> 
        <input class="cssButton" value="尾  页" title="The Last Page" type="button" onclick="turnPage.lastPage();" />
    </div>
    	
    <table class = common>  
    <TR  class= common> 
    	<TD  class= title> 删除原因 </TD>
    </TR>
    <TR  class= common>
    	<TD  class= title><textarea name="DeleteReason" cols="80" rows="3" class="common"></textarea></TD>
    </TR>
    </table>
    <table  class= common>
            <INPUT VALUE="个单简易保单删除" Class="cssButton" TYPE=button onclick="deleteCont();"> 
    </table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
