<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolPrintInit.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*" %>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox(){
  try{
    fm.reset();
  }
  catch(ex)
  {
    alert("在GroupPolPrintInitEC.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
		initContGrid();
		manageCom = '<%= strManageCom %>';
  }
  catch(re)
  {
    alert("GroupPolPrintInitEC.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initContGrid(){
	var iArray = new Array();

	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="合同号";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="印刷号";
		iArray[2][1]="100px";            	
		iArray[2][2]=100;       
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="保费";
		iArray[3][1]="80px";
		iArray[3][2]=200;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="被保人姓名";
		iArray[4][1]="300px";
		iArray[4][2]=200;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="生效日期";
		iArray[5][1]="80px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="打印次数";
		iArray[6][1]="0px";
		iArray[6][2]=100;
		iArray[6][3]=3;

		ContGrid = new MulLineEnter( "fm" , "ContGrid" );
		//这些属性必须在loadMulLine前
		ContGrid.mulLineCount = 0;
		ContGrid.displayTitle = 1;
		ContGrid.hiddenPlus = 1;
		ContGrid.hiddenSubtraction = 1;
		ContGrid.canSel = 1;
		ContGrid.locked = 1;
		ContGrid.canChk = 0;
		ContGrid.loadMulLine(iArray);

		//这些操作必须在loadMulLine后面
		//GrpContGrid.setRowColData(1,1,"asdf");
	}
	catch(ex){
		alert(ex);
	}
}
</script>