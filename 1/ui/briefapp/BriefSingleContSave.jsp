
<%--
  保存简易保单信息 2005-11-11 16:31 Yangming
--%>
<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
  String FlagStr = ""; //操作结果
  String Content = ""; //控制台信息
  String tAction = ""; //操作类型：delete update insert
  String tOperate = ""; //操作代码
  String mLoadFlag = "";
  CErrors tError;
  //VData tVData = new VData();
  //TransferData tTransferData = new TransferData();
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  tAction = request.getParameter("fmAction");
  String tContType = request.getParameter("ContType");
  String tMissionProp5 = request.getParameter("MissionProp5");
  //抵达国家
  LCNationSet tLCNationSet = new LCNationSet();
  //国家代码
  String[] tNationNo = request.getParameterValues("NationGrid1");
  String[] tNationName = request.getParameterValues("NationGrid2");
  String tRiskWrapFlag = request.getParameter("RiskWrapFlag");
  //地址编码
  int MulCount = 0;
  if (tNationNo != null)
    MulCount = tNationNo.length;
  for (int n = 0; n < MulCount; n++) {
    System.out.println("国家代码 " + tNationNo[n]);
    LCNationSchema tLCNationSchema = new LCNationSchema();
    tLCNationSchema.setGrpContNo("00000000000000000000");
    tLCNationSchema.setNationNo(tNationNo[n]);
    tLCNationSchema.setContNo(request.getParameter("ContNo"));
    //tLCNationSchema.setNationNo(tNationNo[n]);
    tLCNationSchema.setChineseName(tNationName[n]);
    tLCNationSet.add(tLCNationSchema);
  }
  System.out.println("完成国家信息录入");
  //合同信息录入
  LCContSchema tLCContSchema = new LCContSchema();
  try {
    tLCContSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLCContSchema.setContNo(request.getParameter("ContNo"));
    tLCContSchema.setProposalContNo(request.getParameter("ProposalContNo"));
    tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCContSchema.setContType(request.getParameter("ContType"));
    tLCContSchema.setFamilyType(request.getParameter("FamilyType"));
    tLCContSchema.setFamilyID(request.getParameter("FamilyID"));
    tLCContSchema.setPolType(request.getParameter("PolType"));
    tLCContSchema.setCardFlag(request.getParameter("CardFlag"));
    tLCContSchema.setManageCom(request.getParameter("ManageCom"));
    tLCContSchema.setExecuteCom(request.getParameter("ExecuteCom"));
  	tLCContSchema.setAgentCom(request.getParameter("AgentCom"));
    tLCContSchema.setAgentCode(request.getParameter("AgentCode"));
    tLCContSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLCContSchema.setAgentCode1(request.getParameter("AgentCode1"));
    tLCContSchema.setAgentType(request.getParameter("AgentType"));
    tLCContSchema.setSaleChnl(request.getParameter("SaleChnl"));
    tLCContSchema.setHandler(request.getParameter("Handler"));
    tLCContSchema.setPassword(request.getParameter("Password"));
    tLCContSchema.setAppntNo(request.getParameter("AppntNo"));
    tLCContSchema.setAppntName(request.getParameter("AppntName"));
    tLCContSchema.setAppntSex(request.getParameter("AppntSex"));
    tLCContSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
    tLCContSchema.setAppntIDType(request.getParameter("AppntIDType"));
    tLCContSchema.setAppntIDNo(request.getParameter("AppntIDNo"));
    tLCContSchema.setInsuredNo(request.getParameter("InsuredNo"));
    tLCContSchema.setInsuredName(request.getParameter("InsuredName"));
    tLCContSchema.setInsuredSex(request.getParameter("InsuredSex"));
    tLCContSchema.setInsuredBirthday(request.getParameter("InsuredBirthday"));
    tLCContSchema.setInsuredIDType(request.getParameter("InsuredIDType"));
    tLCContSchema.setInsuredIDNo(request.getParameter("InsuredIDNo"));
    tLCContSchema.setPayIntv(request.getParameter("PayIntv"));
    tLCContSchema.setPayMode(request.getParameter("PayMode"));
    tLCContSchema.setPayLocation(request.getParameter("PayLocation"));
    tLCContSchema.setDisputedFlag(request.getParameter("DisputedFlag"));
    tLCContSchema.setOutPayFlag(request.getParameter("OutPayFlag"));
    tLCContSchema.setGetPolMode(request.getParameter("GetPolMode"));
    tLCContSchema.setSignCom(request.getParameter("SignCom"));
    tLCContSchema.setSignDate(request.getParameter("SignDate"));
    tLCContSchema.setSignTime(request.getParameter("SignTime"));
    tLCContSchema.setConsignNo(request.getParameter("ConsignNo"));
    tLCContSchema.setBankCode(request.getParameter("BankCode"));
    tLCContSchema.setBankAccNo(request.getParameter("BankAccNo"));
    System.out.println("投保人的银行帐号" + request.getParameter("BankAccNo"));
    tLCContSchema.setAccName(request.getParameter("AccName"));
    tLCContSchema.setPrintCount(request.getParameter("PrintCount"));
    tLCContSchema.setLostTimes(request.getParameter("LostTimes"));
    tLCContSchema.setLang(request.getParameter("Lang"));
    tLCContSchema.setCurrency(request.getParameter("Currency"));
    tLCContSchema.setRemark(request.getParameter("Remark"));
    tLCContSchema.setPeoples(request.getParameter("Peoples"));
    tLCContSchema.setMult(request.getParameter("Mult"));
    tLCContSchema.setPrem(request.getParameter("Prem"));
    tLCContSchema.setAmnt(request.getParameter("Amnt"));
    tLCContSchema.setSumPrem(request.getParameter("SumPrem"));
    tLCContSchema.setDif(request.getParameter("Dif"));
    tLCContSchema.setPaytoDate(request.getParameter("PaytoDate"));
    tLCContSchema.setFirstPayDate(request.getParameter("FirstPayDate"));
    tLCContSchema.setCValiDate(request.getParameter("CValiDate"));
    tLCContSchema.setInputOperator(request.getParameter("InputOperator"));
    tLCContSchema.setInputDate(request.getParameter("InputDate"));
    tLCContSchema.setInputTime(request.getParameter("InputTime"));
    tLCContSchema.setApproveFlag(request.getParameter("ApproveFlag"));
    tLCContSchema.setApproveCode(request.getParameter("ApproveCode"));
    tLCContSchema.setApproveDate(request.getParameter("ApproveDate"));
    tLCContSchema.setApproveTime(request.getParameter("ApproveTime"));
    tLCContSchema.setUWFlag(request.getParameter("UWFlag"));
    tLCContSchema.setUWOperator(request.getParameter("UWOperator"));
    tLCContSchema.setUWDate(request.getParameter("UWDate"));
    tLCContSchema.setUWTime(request.getParameter("UWTime"));
    tLCContSchema.setAppFlag(request.getParameter("AppFlag"));
    tLCContSchema.setPolApplyDate(request.getParameter("PolApplyDate"));
    tLCContSchema.setGetPolDate(request.getParameter("GetPolDate"));
    tLCContSchema.setGetPolTime(request.getParameter("GetPolTime"));
    tLCContSchema.setCustomGetPolDate(request.getParameter("CustomGetPolDate"));
    tLCContSchema.setState(request.getParameter("State"));
    tLCContSchema.setOperator(request.getParameter("Operator"));
    tLCContSchema.setMakeDate(request.getParameter("MakeDate"));
    tLCContSchema.setMakeTime(request.getParameter("MakeTime"));
    tLCContSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLCContSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLCContSchema.setFirstTrialOperator(request.getParameter("FirstTrialOperator"));
    tLCContSchema.setFirstTrialDate(request.getParameter("FirstTrialDate"));
    tLCContSchema.setFirstTrialTime(request.getParameter("FirstTrialTime"));
    tLCContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));
    tLCContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
    tLCContSchema.setReceiveTime(request.getParameter("ReceiveTime"));
    tLCContSchema.setTempFeeNo(request.getParameter("TempFeeNo"));
    tLCContSchema.setProposalType(request.getParameter("ProposalType"));
    tLCContSchema.setSaleChnlDetail(request.getParameter("SaleChnlDetail"));
    tLCContSchema.setContPrintLoFlag(request.getParameter("ContPrintLoFlag"));
    tLCContSchema.setContPremFeeNo(request.getParameter("ContPremFeeNo"));
    tLCContSchema.setCustomerReceiptNo(request.getParameter("CustomerReceiptNo"));
    tLCContSchema.setCInValiDate(request.getParameter("CInValiDate"));
    tLCContSchema.setCopys(request.getParameter("Copys"));
    tLCContSchema.setDegreeType(request.getParameter("DegreeType"));
    tLCContSchema.setGrpAgentCom(request.getParameter("GrpAgentCom"));
    tLCContSchema.setGrpAgentIDNo(request.getParameter("GrpAgentIDNo"));
    tLCContSchema.setDueFeeMsgFlag(request.getParameter("DueFeeMsgFlag"));
    tLCContSchema.setGrpAgentCode(request.getParameter("GrpAgentCode"));
    tLCContSchema.setGrpAgentName(request.getParameter("GrpAgentName"));
    tLCContSchema.setAgentSaleCode(request.getParameter("AgentSaleCode"));
    
    // 集团交叉业务
    tLCContSchema.setCrs_SaleChnl(request.getParameter("Crs_SaleChnl"));
    tLCContSchema.setCrs_BussType(request.getParameter("Crs_BussType"));
    // --------------------
    
    // 缴费模式
    tLCContSchema.setPayMethod(request.getParameter("PayMethod"));
    // --------------------
  }
  catch (Exception ex) {
    ex.printStackTrace();
    System.out.println("出错了！");
  }
  //处理被保人
  LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
  try {
    tLCInsuredSchema.setGrpContNo(request.getParameter("insured_GrpContNo"));
    tLCInsuredSchema.setContNo(request.getParameter("insured_ContNo"));
    tLCInsuredSchema.setInsuredNo(request.getParameter("insured_InsuredNo"));
    tLCInsuredSchema.setPrtNo(request.getParameter("insured_PrtNo"));
    tLCInsuredSchema.setAppntNo(request.getParameter("insured_AppntNo"));
    tLCInsuredSchema.setManageCom(request.getParameter("insured_ManageCom"));
    tLCInsuredSchema.setExecuteCom(request.getParameter("insured_ExecuteCom"));
    tLCInsuredSchema.setFamilyID(request.getParameter("insured_FamilyID"));
//     tLCInsuredSchema.setRelationToMainInsured(request.getParameter("insured_RelationToMainInsured"));
    tLCInsuredSchema.setRelationToMainInsured("00");
    tLCInsuredSchema.setAddressNo(request.getParameter("insured_AddressNo"));
    tLCInsuredSchema.setSequenceNo(request.getParameter("insured_SequenceNo"));
    tLCInsuredSchema.setNativePlace(request.getParameter("insured_NativePlace"));
    tLCInsuredSchema.setNationality(request.getParameter("insured_Nationality"));
    tLCInsuredSchema.setRgtAddress(request.getParameter("insured_RgtAddress"));
    tLCInsuredSchema.setMarriage(request.getParameter("insured_Marriage"));
    tLCInsuredSchema.setMarriageDate(request.getParameter("insured_MarriageDate"));
    tLCInsuredSchema.setHealth(request.getParameter("insured_Health"));
    tLCInsuredSchema.setStature(request.getParameter("insured_Stature"));
    tLCInsuredSchema.setAvoirdupois(request.getParameter("insured_Avoirdupois"));
    tLCInsuredSchema.setDegree(request.getParameter("insured_Degree"));
    tLCInsuredSchema.setCreditGrade(request.getParameter("insured_CreditGrade"));
    tLCInsuredSchema.setBankCode(request.getParameter("insured_BankCode"));
    tLCInsuredSchema.setBankAccNo(request.getParameter("insured_BankAccNo"));
    tLCInsuredSchema.setAccName(request.getParameter("insured_AccName"));
    tLCInsuredSchema.setJoinCompanyDate(request.getParameter("insured_JoinCompanyDate"));
    tLCInsuredSchema.setStartWorkDate(request.getParameter("insured_StartWorkDate"));
    tLCInsuredSchema.setPosition(request.getParameter("insured_Position"));
    tLCInsuredSchema.setSalary(request.getParameter("insured_Salary"));
    tLCInsuredSchema.setOccupationType(request.getParameter("insured_OccupationType"));
    tLCInsuredSchema.setOccupationCode(request.getParameter("insured_OccupationCode"));
    tLCInsuredSchema.setWorkType(request.getParameter("insured_WorkType"));
    tLCInsuredSchema.setPluralityType(request.getParameter("insured_PluralityType"));
    tLCInsuredSchema.setSmokeFlag(request.getParameter("insured_SmokeFlag"));
    tLCInsuredSchema.setContPlanCode(request.getParameter("insured_ContPlanCode"));
    tLCInsuredSchema.setOperator(request.getParameter("insured_Operator"));
    tLCInsuredSchema.setInsuredStat(request.getParameter("insured_InsuredStat"));
    tLCInsuredSchema.setMakeDate(request.getParameter("insured_MakeDate"));
    tLCInsuredSchema.setMakeTime(request.getParameter("insured_MakeTime"));
    tLCInsuredSchema.setModifyDate(request.getParameter("insured_ModifyDate"));
    tLCInsuredSchema.setModifyTime(request.getParameter("insured_ModifyTime"));
    tLCInsuredSchema.setUWFlag(request.getParameter("insured_UWFlag"));
    tLCInsuredSchema.setUWCode(request.getParameter("insured_UWCode"));
    tLCInsuredSchema.setUWDate(request.getParameter("insured_UWDate"));
    tLCInsuredSchema.setUWTime(request.getParameter("insured_UWTime"));
    tLCInsuredSchema.setBMI(request.getParameter("insured_BMI"));
    tLCInsuredSchema.setInsuredPeoples(request.getParameter("insured_InsuredPeoples"));
    tLCInsuredSchema.setContPlanCount(request.getParameter("insured_ContPlanCount"));
    tLCInsuredSchema.setDiskImportNo(request.getParameter("insured_DiskImportNo"));
    tLCInsuredSchema.setGrpInsuredPhone(request.getParameter("insured_GrpInsuredPhone"));
    tLCInsuredSchema.setName(request.getParameter("insured_Name"));
    tLCInsuredSchema.setEnglishName(request.getParameter("insured_EnglishName"));
    tLCInsuredSchema.setSex(request.getParameter("insured_Sex"));
    tLCInsuredSchema.setIDStartDate(request.getParameter("IDStartDate"));
    tLCInsuredSchema.setIDEndDate(request.getParameter("IDEndDate"));
    tLCInsuredSchema.setBirthday(request.getParameter("insured_Birthday"));
    tLCInsuredSchema.setIDType(request.getParameter("insured_IDType"));
    tLCInsuredSchema.setIDNo(request.getParameter("insured_IDNo"));
    tLCInsuredSchema.setOthIDNo(request.getParameter("insured_OthIDNo"));
    tLCInsuredSchema.setOthIDType(request.getParameter("insured_OthIDType"));
    tLCInsuredSchema.setRelationToAppnt(request.getParameter("insured_RelationToAppnt"));
    tLCInsuredSchema.setSequenceNo("1");  //只有一个被保人，置为第一被保人
    tLCInsuredSchema.setNativePlace(request.getParameter("insured_NativePlace"));
    tLCInsuredSchema.setNativeCity(request.getParameter("insured_NativeCity"));
	//modify by zxs
	tLCInsuredSchema.setAuthorization(request.getParameter("InsuredAuth"));
    System.out.println("处理完成被保人信息！");
  }
  catch (Exception ex) {
    ex.printStackTrace();
    System.out.println("出错！！！");
  }
  //处理投保人
  LCAppntSchema tLCAppntSchema = new LCAppntSchema();
  try {
    tLCAppntSchema.setGrpContNo(request.getParameter("appnt_GrpContNo"));
    tLCAppntSchema.setContNo(request.getParameter("appnt_ContNo"));
    tLCAppntSchema.setPrtNo(request.getParameter("appnt_PrtNo"));
    tLCAppntSchema.setAppntNo(request.getParameter("appnt_AppntNo"));
    tLCAppntSchema.setAppntGrade(request.getParameter("appnt_AppntGrade"));
    tLCAppntSchema.setAddressNo(request.getParameter("appnt_AddressNo"));
    tLCAppntSchema.setNativePlace(request.getParameter("appnt_NativePlace"));
    tLCAppntSchema.setNationality(request.getParameter("appnt_Nationality"));
    tLCAppntSchema.setRgtAddress(request.getParameter("appnt_RgtAddress"));
    tLCAppntSchema.setMarriage(request.getParameter("AppntMarriage"));
    tLCAppntSchema.setMarriageDate(request.getParameter("appnt_MarriageDate"));
    tLCAppntSchema.setHealth(request.getParameter("appnt_Health"));
    tLCAppntSchema.setStature(request.getParameter("appnt_Stature"));
    tLCAppntSchema.setAvoirdupois(request.getParameter("appnt_Avoirdupois"));
    tLCAppntSchema.setDegree(request.getParameter("appnt_Degree"));
    tLCAppntSchema.setCreditGrade(request.getParameter("appnt_CreditGrade"));
    tLCAppntSchema.setBankCode(request.getParameter("appnt_BankCode"));
    tLCAppntSchema.setBankAccNo(request.getParameter("appnt_BankAccNo"));
    tLCAppntSchema.setAccName(request.getParameter("appnt_AccName"));
    tLCAppntSchema.setJoinCompanyDate(request.getParameter("appnt_JoinCompanyDate"));
    tLCAppntSchema.setStartWorkDate(request.getParameter("appnt_StartWorkDate"));
    tLCAppntSchema.setPosition(request.getParameter("appnt_Position"));
    tLCAppntSchema.setSalary(request.getParameter("appnt_Salary"));
    tLCAppntSchema.setOccupationType(request.getParameter("appnt_OccupationType"));
    tLCAppntSchema.setOccupationCode(request.getParameter("appnt_OccupationCode"));
    tLCAppntSchema.setWorkType(request.getParameter("appnt_WorkType"));
    tLCAppntSchema.setPluralityType(request.getParameter("appnt_PluralityType"));
    tLCAppntSchema.setSmokeFlag(request.getParameter("appnt_SmokeFlag"));
    tLCAppntSchema.setOperator(request.getParameter("appnt_Operator"));
    tLCAppntSchema.setManageCom(request.getParameter("appnt_ManageCom"));
    tLCAppntSchema.setMakeDate(request.getParameter("appnt_MakeDate"));
    tLCAppntSchema.setMakeTime(request.getParameter("appnt_MakeTime"));
    tLCAppntSchema.setModifyDate(request.getParameter("appnt_ModifyDate"));
    tLCAppntSchema.setModifyTime(request.getParameter("appnt_ModifyTime"));
    tLCAppntSchema.setOthIDType(request.getParameter("appnt_OthIDType"));
    tLCAppntSchema.setOthIDNo(request.getParameter("appnt_OthIDNo"));
    tLCAppntSchema.setEnglishName(request.getParameter("appnt_EnglishName"));
    tLCAppntSchema.setAppntName(request.getParameter("appnt_AppntName"));
    tLCAppntSchema.setAppntSex(request.getParameter("appnt_AppntSex"));
    tLCAppntSchema.setAppntBirthday(request.getParameter("appnt_AppntBirthday"));
    tLCAppntSchema.setIDType(request.getParameter("appnt_IDType"));
    tLCAppntSchema.setIDStartDate(request.getParameter("appnt_IDStartDate"));
    tLCAppntSchema.setIDEndDate(request.getParameter("appnt_IDEndDate"));
    tLCAppntSchema.setIDNo(request.getParameter("appnt_IDNo"));
    tLCAppntSchema.setNativePlace(request.getParameter("appnt_NativePlace"));
    tLCAppntSchema.setNativeCity(request.getParameter("appnt_NativeCity"));
	//modify by zxs 
	tLCAppntSchema.setAuthorization(request.getParameter("AppntAuth"));
    
  }
  catch (Exception ex) {
    ex.printStackTrace();
    System.out.println("出错了~~~~");
  }
  System.out.println("处理完成投保人信息！");
  System.out.println("开始处理地址信息~");
  LCAddressSchema mAppntAddressSchema = new LCAddressSchema();
  LCAddressSchema mInsuredAddressSchema = new LCAddressSchema();
  /**
   * 1、联系地址 PostalAddress
   * 2、邮政编码 ZipCode
   * 3、家庭电话 HomePhone
   * 4、移动电话 Mobile
   * 5、办公电话 CompanyPhone
   * 6、电子邮箱 EMail
   */
  try {
    mAppntAddressSchema.setCustomerNo(request.getParameter("appnt_AppntNo"));
    mAppntAddressSchema.setGrpName(request.getParameter("WorkName"));
    mAppntAddressSchema.setAddressNo(request.getParameter("appnt_AddressNo"));
    mAppntAddressSchema.setPostalAddress(request.getParameter("appnt_PostalAddress"));
    mAppntAddressSchema.setZipCode(request.getParameter("appnt_ZipCode"));
    mAppntAddressSchema.setPhone(request.getParameter("appnt_Phone"));
    mAppntAddressSchema.setHomePhone(request.getParameter("appnt_HomePhone"));
    mAppntAddressSchema.setMobile(request.getParameter("appnt_Mobile"));
    mAppntAddressSchema.setCompanyPhone(request.getParameter("appnt_CompanyPhone"));
    mAppntAddressSchema.setEMail(request.getParameter("appnt_EMail"));
    mAppntAddressSchema.setHomeCode(request.getParameter("appnt_HomeCode"));
    mAppntAddressSchema.setHomeNumber(request.getParameter("appnt_HomeNumber"));
    mAppntAddressSchema.setPostalCity(request.getParameter("City"));
    mAppntAddressSchema.setPostalCommunity(request.getParameter("appnt_PostalCommunity"));
    mAppntAddressSchema.setPostalCounty(request.getParameter("County"));
    mAppntAddressSchema.setPostalProvince(request.getParameter("Province"));
    mAppntAddressSchema.setPostalStreet(request.getParameter("appnt_PostalStreet"));
  }
  catch (Exception ex) {
    ex.printStackTrace();
    System.out.println("出错！");
  }
  System.out.println("开始处理被保人地址信息");
  try {
    mInsuredAddressSchema.setCustomerNo(request.getParameter("insured_InsuredNo"));
    mInsuredAddressSchema.setGrpName(request.getParameter("insured_WorkName"));
    mInsuredAddressSchema.setAddressNo(request.getParameter("insured_AddressNo"));
    mInsuredAddressSchema.setPostalAddress(request.getParameter("insured_PostalAddress"));
    mInsuredAddressSchema.setZipCode(request.getParameter("insured_ZipCode"));
    mInsuredAddressSchema.setPhone(request.getParameter("insured_Phone"));
    mInsuredAddressSchema.setHomePhone(request.getParameter("insured_HomePhone"));
    mInsuredAddressSchema.setMobile(request.getParameter("insured_Mobile"));
    mInsuredAddressSchema.setCompanyPhone(request.getParameter("insured_CompanyPhone"));
    mInsuredAddressSchema.setEMail(request.getParameter("insured_EMail"));
    //mInsuredAddressSchema.setHomeCode(request.getParameter("insured_HomeCode"));
    //mInsuredAddressSchema.setHomeNumber(request.getParameter("insured_HomeNumber"));
    mInsuredAddressSchema.setPostalCity(request.getParameter("City2"));
    mInsuredAddressSchema.setPostalCommunity(request.getParameter("insured_PostalCommunity"));
    mInsuredAddressSchema.setPostalCounty(request.getParameter("County2"));
    mInsuredAddressSchema.setPostalProvince(request.getParameter("Province2"));
    mInsuredAddressSchema.setPostalStreet(request.getParameter("insured_PostalStreet"));    
  }
  catch (Exception ex) {
    ex.printStackTrace();
    System.out.println("出错了！");
  }
  System.out.println("处理完成");
  System.out.println("开始处理险种信息");
  LCPolSet tLCPolSet = new LCPolSet();
  String tRiskNum[] = request.getParameterValues("RiskGridNo");
  String tRiskCode[] = request.getParameterValues("RiskGrid1"); //险种编码
  String tCopys[] = request.getParameterValues("RiskGrid4");
  String tMultAndAmnt[] = request.getParameterValues("RiskGrid3"); //险种编码
  String tChk[] = request.getParameterValues("InpRiskGridChk");
  //System.out.println("险种个数 ："+tChk.length);
  if (tChk != null && !StrTool.cTrim(tRiskWrapFlag).equals("1")) {
    for (int index = 0; index < tChk.length; index++) {
      if (tChk[index].equals("1")) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        System.out.println("Riskcode" + tRiskCode[index] + ":" + request.getParameter("GrpContNo"));
        tLCPolSchema.setRiskCode(tRiskCode[index]);
        if (!StrTool.cTrim(tRiskCode[index]).equals("")) {
          //System.out.println("测试 又没有保存档次"+tMultAndAmnt[index]);
          if (Integer.parseInt(tMultAndAmnt[index]) > 10) {
            tLCPolSchema.setAmnt(tMultAndAmnt[index]);
          }
          else {
            System.out.println("测试 又没有保存档次" + tMultAndAmnt[index]);
            tLCPolSchema.setMult(tMultAndAmnt[index]);
          }
        }
        tLCPolSchema.setRiskCode(tRiskCode[index]);
        tLCPolSchema.setCopys(tCopys[index]);
        tLCPolSet.add(tLCPolSchema);
      }
    }
  }
  //处理受益人信息
  LCBnfSchema tLCBnfSchema = null;
  String bnf_Name = request.getParameter("bnf_Name");
  String bnf_Sex = request.getParameter("bnf_Sex");
  String bnf_IDType = request.getParameter("bnf_IDType");
  String bnf_IDNo = request.getParameter("bnf_IDNo");
  if (!StrTool.cTrim(bnf_Name).equals("") && !StrTool.cTrim(bnf_Sex).equals("")) {
    try {
      tLCBnfSchema = new LCBnfSchema();
      tLCBnfSchema.setName(bnf_Name);
      tLCBnfSchema.setSex(bnf_Sex);
      tLCBnfSchema.setIDType(bnf_IDType);
      tLCBnfSchema.setIDNo(bnf_IDNo);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      System.out.println("出错");
    }
  }
  //处理多受益人
  // 受益人信息
  LCBnfSet tLCBnfSet = new LCBnfSet();
  String tBnfNum[] = request.getParameterValues("BnfGridNo");
  String tBnfType[] = request.getParameterValues("BnfGrid1");
  String tName[] = request.getParameterValues("BnfGrid2");
  String tIDType[] = request.getParameterValues("BnfGrid4");
  String tSex[] = request.getParameterValues("BnfGrid3");
  String tIDNo[] = request.getParameterValues("BnfGrid5");
  String tBnfRelationToInsured[] = request.getParameterValues("BnfGrid6");
  String tBnfLot[] = request.getParameterValues("BnfGrid7");
  String tBnfGrade[] = request.getParameterValues("BnfGrid8");
  String tNativePlace[] = request.getParameterValues("BnfGrid9");
  String tOccupationCode[] = request.getParameterValues("BnfGrid10");
  String tPhone[] = request.getParameterValues("BnfGrid11");
  String tAddress[] = request.getParameterValues("BnfGrid12");
  String tIDStartDate[] = request.getParameterValues("BnfGrid13");
  String tIDEndDate[] = request.getParameterValues("BnfGrid14");
  int BnfCount = 0;
  if (tBnfNum != null)
    BnfCount = tBnfNum.length;
  for (int i = 0; i < BnfCount; i++) {
    if (tName[i] == null || tName[i].equals(""))
      break;
    LCBnfSchema xLCBnfSchema = new LCBnfSchema();
    xLCBnfSchema.setBnfType(tBnfType[i]);
    xLCBnfSchema.setName(tName[i]);
    xLCBnfSchema.setSex(tSex[i]);
    xLCBnfSchema.setIDType(tIDType[i]);
    xLCBnfSchema.setIDNo(tIDNo[i]);
    //tLCBnfSchema.setBirthday(tBirthday[i]);
    xLCBnfSchema.setRelationToInsured(tBnfRelationToInsured[i]);
    xLCBnfSchema.setBnfLot(tBnfLot[i]);
    xLCBnfSchema.setBnfGrade(tBnfGrade[i]);
  	System.out.println("受益人信息性别..."+xLCBnfSchema.getSex());
  	System.out.println("受益人信息身份证..."+xLCBnfSchema.getIDType());
    xLCBnfSchema.setNativePlace(tNativePlace[i]);
    xLCBnfSchema.setOccupationCode(tOccupationCode[i]);
    xLCBnfSchema.setPhone(tPhone[i]);
    xLCBnfSchema.setPostalAddress(tAddress[i]);
    xLCBnfSchema.setIDStartDate(tIDStartDate[i]);
    xLCBnfSchema.setIDEndDate(tIDEndDate[i]);
    tLCBnfSet.add(xLCBnfSchema);
  }
  System.out.println("jdieoahfeioipwpjfio" + tLCBnfSet.size());
  System.out.println("end set schema 受益人信息...");

  //客户告知
  LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
  tLCCustomerImpartSchema.setGrpContNo("00000000000000000000");
  tLCCustomerImpartSchema.setProposalContNo(request.getParameter("ProposalContNo"));
  tLCCustomerImpartSchema.setContNo(request.getParameter("ContNo"));
  tLCCustomerImpartSchema.setPrtNo(request.getParameter("PrtNo"));
  tLCCustomerImpartSchema.setCustomerNo(tLCInsuredSchema.getInsuredNo());
  tLCCustomerImpartSchema.setCustomerNoType("I");
  tLCCustomerImpartSchema.setImpartCode("001");
  tLCCustomerImpartSchema.setImpartContent("□商务　□旅行　□其它");
  tLCCustomerImpartSchema.setImpartParamModle(request.getParameter("IntentionName"));
  tLCCustomerImpartSchema.setImpartVer("013");
  
	//客户告知
    String tImpartNum[] = request.getParameterValues("ImpartGridNo");
    String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
    String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
    String tImpartContent[] = request.getParameterValues("ImpartGrid3");        //告知内容
    String tImpartParamModle[] = request.getParameterValues("ImpartGrid4");    //告知客户号码
    LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
    if(tImpartCode != null && tImpartCode.length > 0)
    {
        for (int i = 0; i < tImpartNum.length; i++)    
        {
            LCCustomerImpartSchema tTmpCusImpartSchema = new LCCustomerImpartSchema();
            
            tTmpCusImpartSchema.setProposalContNo(request.getParameter("ContNo"));
            tTmpCusImpartSchema.setCustomerNoType("I");
            //tTmpCusImpartSchema.setCustomerNo(tInsuredID);
            //tTmpCusImpartSchema.setImpartVer(tImpartVer[i]);
            tTmpCusImpartSchema.setImpartVer("001");
            tTmpCusImpartSchema.setImpartCode(tImpartCode[i]);
            tTmpCusImpartSchema.setImpartContent(tImpartContent[i]);
            tTmpCusImpartSchema.setImpartParamModle(tImpartParamModle[i]);
            //tTmpCusImpartSchema.setDiseaseContent(tImpartContent[i]);
            tLCCustomerImpartSet.add(tTmpCusImpartSchema);
        }
    }

    //客户告知明细
    String tImpartDetailNum[] = request.getParameterValues("ImpartDetailGridNo");
    String tImpartDetailVer[] = request.getParameterValues("ImpartDetailGrid1");            //告知版别
    String tImpartDetailCode[] = request.getParameterValues("ImpartDetailGrid2");           //告知编码
    String tImpartDetailContent[] = request.getParameterValues("ImpartDetailGrid3");        //告知内容
    String tImpartDetailDiseaseContent[] = request.getParameterValues("ImpartDetailGrid4");    //告知客户号码  
                    String tImpartDetailStartDate[] = request.getParameterValues("ImpartDetailGrid5");      
                    String tImpartDetailEndDate[] = request.getParameterValues("ImpartDetailGrid6");
                String tImpartDetailProver[] = request.getParameterValues("ImpartDetailGrid7");    
                String tImpartDetailCurrCondition[] = request.getParameterValues("ImpartDetailGrid8");  
                String tImpartDetailIsProved[] = request.getParameterValues("ImpartDetailGrid9");          
    LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();
    if(tImpartDetailNum != null && tImpartDetailNum.length > 0)
    {
        for (int i = 0; i < tImpartDetailNum.length; i++)  
        {
            LCCustomerImpartDetailSchema tLCCustomerImpartDetailSchema = new LCCustomerImpartDetailSchema();

            tLCCustomerImpartDetailSchema.setProposalContNo(request.getParameter("ContNo"));
            tLCCustomerImpartDetailSchema.setCustomerNoType("I");
            //tLCCustomerImpartDetailSchema.setCustomerNo(tInsuredID);
            //tLCCustomerImpartDetailSchema.setImpartVer(tImpartDetailVer[i]);
            tLCCustomerImpartDetailSchema.setImpartVer("001");
            tLCCustomerImpartDetailSchema.setImpartCode(tImpartDetailCode[i]);
            tLCCustomerImpartDetailSchema.setImpartDetailContent(tImpartDetailContent[i]);
            //tLCCustomerImpartDetailSchema.setImpartParamModle(tImpartDetailParamModle[i]);
            tLCCustomerImpartDetailSchema.setDiseaseContent(tImpartDetailDiseaseContent[i]);
            tLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
        }
    }
  
  //处理套餐信息
  LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
  String tWrapChk[] = request.getParameterValues("InpRiskWrapGridChk");
  int tWrapCol = Integer.parseInt(StrTool.cTrim(request.getParameter("RiskWrapCol")));
  //   String[][] tParam = new String[tWrapChk.length][tWrapCol];
  String tRiskWrapCode[] = request.getParameterValues("RiskWrapGrid1");
  String tWrapCode = request.getParameter("RiskWrapGrid1");
  if(tWrapChk != null){
	  for (int index = 0; index < tWrapChk.length; index++) {
	    if (tWrapChk[index].equals("1")) {
	      for (int i = 3; i < tWrapCol; i++) {
	        if (StrTool.cTrim(request.getParameterValues("RiskWrapGrid" + i)[index]).equals("Factor")) {
	          System.out.println("yyyyyyyyyyyyyyyyyy ..." + tContType);
	          LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
	          tLCRiskDutyWrapSchema.setRiskWrapCode(tRiskWrapCode[index]);
	          tLCRiskDutyWrapSchema.setCalFactor(request.getParameterValues("RiskWrapGrid" + (i + 1))[index]);
	          tLCRiskDutyWrapSchema.setCalFactorValue(request.getParameterValues("RiskWrapGrid" + (i + 2))[index]);
	          tLCRiskDutyWrapSchema.setRiskCode("000000");
	          tLCRiskDutyWrapSchema.setDutyCode("000000");
	          //tLCRiskDutyWrapSchema.setCalFactorType(request.getParameterValues("RiskWrapGrid" + (i + 3))[index]);
	          mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
	        }
	      }
	    }
	    System.out.println("mmmmmmmmmmmmm..." + mLCRiskDutyWrapSet.size());
	  }
  }
  
  	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
	//#1855 客户类型
	LCContSubSchema tLCContSubSchema = new LCContSubSchema();
	tLCContSubSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCContSubSchema.setCountyType(request.getParameter("appnt_CountyType"));
	tLCContSubSchema.setFamilySalary(request.getParameter("FamilySalary"));
	
  //处理LDPerson信息
  String tWorkName = request.getParameter("WorkName");
  System.out.println("工作单位是..." + tWorkName);
  //完成全部的封装，开始递交．
  BriefSingleContInputUI tBriefSingleContInputUI = new BriefSingleContInputUI();
  try {
    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("AppntAddress", mAppntAddressSchema);
    tTransferData.setNameAndValue("InsuredAddress", mInsuredAddressSchema);
    tTransferData.setNameAndValue("WorkName", tWorkName);
    tTransferData.setNameAndValue("MissionProp5", tMissionProp5);
    tTransferData.setNameAndValue("WrapCode", tWrapCode);
    tTransferData.setNameAndValue("ContType", tContType);
	  //modify by zxs 
    tTransferData.setNameAndValue("InsuredAuth", request.getParameter("InsuredAuth"));
    tTransferData.setNameAndValue("AppntAuth", request.getParameter("AppntAuth"));
    tVData.add(tLCContSchema);
    tVData.add(tLCInsuredSchema);
    tVData.add(tLCBnfSchema);
    tVData.add(tLCAppntSchema);
    tVData.add(tLCNationSet);
    tVData.add(tLCPolSet);
    tVData.add(tLCCustomerImpartSchema);
    
    tVData.add(tLCCustomerImpartSet);
    tVData.add(tLCCustomerImpartDetailSet);
    
    tVData.add(tLCExtendSchema);
    //#1855
    tVData.add(tLCContSubSchema);
    
    tVData.add(tG);
    tVData.add(tTransferData);
    tVData.add(tLCBnfSet);
    tVData.add(mLCRiskDutyWrapSet);
    System.out.println("开始递交数据！" + tAction);
    tBriefSingleContInputUI.submitData(tVData, tAction);
  }
  catch (Exception ex) {
    ex.printStackTrace();
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr == "") {
    tError = tBriefSingleContInputUI.mErrors;
    if (!tError.needDealError()) {
      Content = " 保存成功! ";
      FlagStr = "Success";
    }
    else {
      Content = "保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

  VData tResult = tBriefSingleContInputUI.getResult();
  LCContSchema resultLCContSchema = new LCContSchema();
  String ContNo = "";
  if (FlagStr.equals("Success")) {
    resultLCContSchema = (LCContSchema) tResult.getObjectByObjectName("LCContSchema", 0);
    ContNo = resultLCContSchema.getContNo();
  }
  
%>
<html>
<script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=ContNo%>");
</script>
</html>
