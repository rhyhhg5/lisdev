<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWAutoInit.jsp
//程序功能：个人自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('CardType').value = '';
    fm.all('CardNo').value = '';
    fm.all('HandleDate').value = '';
    fm.all('State').value = '';
    fm.all('AppntNo').value = '';
    fm.all('InsuredName').value = ''; 
    fm.all('ManageCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('CValiDate1').value = '';
    fm.all('CValiDate2').value = '';
    fm.all('Operator').value = '';
    fm.all('MakeDate1').value = '';
    fm.all('MakeDate2').value = '';
  }
  catch(ex)
  {
    alert("CardQueryInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                    

function initForm()
{
  try
  {
    initInpBox(); 
    initCardGrid();   	  
  }
  catch(re)
  {
    alert("CardQueryInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 查询结果的初始化
function initCardGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="卡号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="类型";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="面值";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[4]=new Array();
      iArray[4][0]="状态";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="被保险人";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="生效日期";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="终止日期";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="录入人员";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

	    iArray[9]=new Array();
      iArray[9][0]="录入日期";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=200;            	        //列最大值
      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

	    CardGrid = new MulLineEnter( "fm" , "CardGrid" ); 
      //这些属性必须在loadMulLine前
      CardGrid.mulLineCount =0;   
      CardGrid.displayTitle = 1;
      CardGrid.hiddenPlus = 1;
      CardGrid.hiddenSubtraction = 1;
      //JisPayGrid.locked = 1;
//      JisPayGrid.canSel = 1;
      CardGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("CardQueryInputInit.jsp-->initJisPayGrid函数中发生异常:初始化界面错误!");
      }
}


</script>