<%--
    简易保单插入备份表操作 2005-9-15 11:19 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%	
	String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();      //集体保单
  tLCGrpContSchema.setGrpContNo(request.getParameter("GrpContNo"));
  
  VData tVData = new VData();
  tVData.add( tLCGrpContSchema );
  tVData.add( tG );
  BriefGroupContDeleteUI tBriefGroupContDeleteUI = new BriefGroupContDeleteUI();
  tOperate = request.getParameter("fmAction");
	System.out.println("操作符 "+tOperate);
	if( tBriefGroupContDeleteUI.submitData( tVData, tOperate ) == false )
	{
		Content = " 保存失败，原因是: " + tBriefGroupContDeleteUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	System.out.println("操作结束！");
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>