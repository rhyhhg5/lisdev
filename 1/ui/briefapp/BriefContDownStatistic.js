var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

function afterSubmit( FlagStr, content ){
    showInfo.close();
    if(FlagStr == "Fail" ){
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }else{
    	showDiv(inputButton,"false");
    }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}
 

//根据选中的批次号、银行编码、管理机构进行查询并且执行打印功能；
function PrintBill()
{
 
	 if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value=="null")||(fm.EndDate.value=="null")){
    	alert("请输入起始日期和结束日期！");
    	return;
   }
	 if((fm.BankCode.value=="")||(fm.BankCode.value=="null")){
			alert("请输入银行代码!!!");
			return;
	 }else{
    	fm.target="f1print";
    	fm.submit();    
   }
}
