<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.File.*"%>
<%@page import="java.util.*"%>
<%
    String flagStr="";      //操作结果
    String Content = "";    //控制台信息
    String tOperate = "";   //操作代码
    String mLoadFlag="";
    CErrors tError = null;
    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
    tG.ServerIP = tG.GetServerIP();
    String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
    String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
    System.out.println("数据路径 TemplatePath : "+szTemplatePath);
    System.out.println("数据路径 OutXmlPath : "+sOutXmlPath);
    
    
	String tChk[] = request.getParameterValues("InpContGridChk");
	String[] tContNos = request.getParameterValues("ContGrid1");
    if(tChk != null && tChk.length > 0)
    {
        for(int i = 0; i < tChk.length; i++)
        {
            if(!"1".equals(tChk[i]))  continue;
            
            String tContNo = tContNos[i];
            
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(tContNo);
            LCContSchema tLCContSchema = tLCContDB.query().get(1);
            String tCardFlag = tLCContSchema.getCardFlag();
            String tIntlFlag = tLCContSchema.getIntlFlag();
            System.out.println(tCardFlag + " **** " + tIntlFlag);
            
            //判断该保单是否为首次打印。
            boolean isFirstPrint = (tLCContSchema.getPrintCount() == 0);
            
            LCSuccorUI tLCSuccorUI = null;
            try{
                VData tVData = new VData();
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
                tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
                tTransferData.setNameAndValue("prtFlag", request.getParameter("PrtFlag"));
                tVData.add(tLCContSchema);
                tVData.add(tTransferData);
                tVData.add(tG );
                 
                tLCSuccorUI = new LCSuccorUI();
                tLCSuccorUI.submitData(tVData, "PRINT");
            }
            catch(Exception ex)
            {
                Content = "保存失败，原因是:" + ex.toString();
                flagStr = "Fail";
            }
            //如果在Catch中发现异常，则不从错误类中提取错误信息
            if (flagStr.equals(""))
            {
                tError = tLCSuccorUI.mErrors;
                if (!tError.needDealError())
                {                          
                    Content = " 保存成功! ";
                    flagStr = "Success";
                }
                else                                                                           
                {
                	   Content = " 保存失败，原因是:" + tError.getFirstError();
                	   flagStr = "Fail";
                }
            }
        
            //添加各种预处理
            if(flagStr.equals("Success")){
                //处理成功，传送xml；
                String tprtflag = request.getParameter("PrtFlag");
        		String tCardName = "J04-"+tContNo;
        		String sOutXmlPath_pdf = application.getRealPath("printdata/data/briefpdf/")+"/";
        		
                // 如果是首次打印，生成待合同接收信息。
                if(isFirstPrint && "5".equals(tCardFlag))
                {
                    TransferData tTmpTransferData = new TransferData();
                    tTmpTransferData.setNameAndValue("ContNo", tContNo);
            
                    VData tTmpVData = new VData();
                    tTmpVData.add(tG);
                    tTmpVData.add(tTmpTransferData);
            
                    ContReceiveUI tContReceiveUI = new ContReceiveUI();
            
                    if (!tContReceiveUI.submitData(tTmpVData, "CreateReceive"))
                    {
                        if (tContReceiveUI.mErrors.needDealError())
                        {
                            System.out.println(tContReceiveUI.mErrors.getFirstError());
                        }
                    }
                    else
                    {
                        // 如果成功生成接收轨迹
                        if (!tContReceiveUI.submitData(tTmpVData, "ReceiveCont"))
                        {
                            if (tContReceiveUI.mErrors.needDealError())
                            {
                                System.out.println(tContReceiveUI.mErrors.getFirstError());
                            }
                        }
                        else
                        {
                            System.out.println("BriefPrint ReceiveCont Success...");
                        }
                        System.out.println("BriefPrint Success...");
                    }
                }
        		/** 电话销售 */
        		if(tCardFlag.equals("5")){
        			tCardName = "J010-"+tContNo;
        			
        			String printServerInterface = (new ExeSQL()).getOneValue("select sysvarvalue from LDSYSVAR where sysvar='PrintServerInterface'");
                    String url = printServerInterface +"&filename=" + tCardName + "&action=preview";
        			System.out.println(url);
        			response.sendRedirect(url);
        			flagStr = "Succ";
        		}
                
            }
        }
    }
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flagStr%>","<%=Content%>");
</script>
</html>