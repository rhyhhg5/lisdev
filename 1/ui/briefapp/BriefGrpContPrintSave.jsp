<%--
    保存简易保单信息 2005-09-05 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.File.*"%>
<%@page import="java.util.*"%>
<%
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  CErrors tError = null;
  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  tG.ServerIP = tG.GetServerIP();
  tAction = request.getParameter( "fmAction" );
  String tPrintServerInterface = (new ExeSQL()).getOneValue("select sysvarvalue from LDSYSVAR where sysvar='PrintServer'");
  
  String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
	String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
	String tGrpContNo = request.getParameter("GrpContNo");
	System.out.println("数据路径szTemplatePath +"+szTemplatePath);
	System.out.println("数据路径sOutXmlPath +"+sOutXmlPath);
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	tLCGrpContSchema.setGrpContNo(tGrpContNo);
	//VData tVData = new VData();
	//TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
  tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
  
    // 加入简易件打印时，保险卡与保单的区别标志。
    String tStrPrtFlag = (String)request.getParameter("PrtFlag");
    tTransferData.setNameAndValue("prtFlag", tStrPrtFlag);
    // ----------------
  
  LCGrpSuccorUI tLCGrpSuccorUI = null;
	try{
	  tVData.add( tLCGrpContSchema );
	  tVData.add( tTransferData );
	  tVData.add( tG );
	   
	  tLCGrpSuccorUI = new LCGrpSuccorUI();
	  tLCGrpSuccorUI.submitData(tVData,tAction);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLCGrpSuccorUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
  if(FlagStr.equals("Success")){
  	//处理成功，传送xml；
  	String tprtflag = request.getParameter("PrtFlag");
		String tCardName = "J02-"+tGrpContNo;
		String tFileName = "J01-"+tGrpContNo;
		String sOutXmlPath_pdf = application.getRealPath("printdata/data/briefpdf/")+"/";
		if (tprtflag.equals("Notice"))
		{

				String url=tPrintServerInterface+"&filename=" + tFileName + "&action=preview";
				System.out.println(url);
				response.sendRedirect(url);
				FlagStr = "Succ";
		}else if(tprtflag.equals("Card")){
				String url=tPrintServerInterface+"&filename=" + tCardName + "&action=preview";
				System.out.println(url);
				response.sendRedirect(url);
				//System.out.println();
				System.out.println("lsdkjsdlfjsdfjsdlfjldljfs");
				FlagStr = "Succ";
		}
  }
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>