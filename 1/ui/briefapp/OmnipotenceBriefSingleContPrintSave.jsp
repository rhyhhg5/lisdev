<%--
    保存简易保单信息 2005-09-05 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.File.*"%>
<%@page import="java.util.*"%>
<%
    String FlagStr = "Succ";      //操作结果
    String Content = "";    //控制台信息
    String tAction = null;    //操作类型：delete update insert
    
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    tG.ServerIP = tG.GetServerIP();
    
    String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
	String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
	
	String tCardFlag = request.getParameter("mCardFlag");
	String tIntlFlag = request.getParameter("IntlFlag");
	
	ExeSQL tExeSQL = new ExeSQL();
	String tPrintServerInterface = tExeSQL.getOneValue("select sysvarvalue from LDSYSVAR where sysvar='PrintServerInterface'");
	
	String tChk[] = request.getParameterValues("InpContGridChk");
	String[] tContNos = request.getParameterValues("ContGrid1");
    String[] contPrintFlag = request.getParameterValues("ContGrid9");
	
	if(tChk != null && tChk.length > 0)
	{
	  for(int i = 0; i < tChk.length; i++)
	  {
	    if(!"1".equals(tChk[i]))
	    {
	      continue;
	    }
	    
	    String sql = "select 1 from LCCont where ContNo = '" + tContNos[i] + "' and PrintCount > 0 ";
	    String temp = tExeSQL.getOneValue(sql);
	    //if("1".equals(temp))
	    //{
	    //  tAction = "REPRINT";
	    //}
	    //else
	    //{
	      tAction = "PRINT";
	    //}
	    
  	    LCContSchema tLCContSchema = new LCContSchema();
    	tLCContSchema.setContNo(tContNos[i]);
    	tLCContSchema.setCardFlag(tCardFlag);
    	tLCContSchema.setIntlFlag(request.getParameter("IntlFlag"));
    	
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("SetPrtTimes", ("2".equals(tIntlFlag) ? "0" : "1"));
        
        VData tVData = new VData();
        tVData.add(tG);
    	tVData.addElement(tLCContSchema);
    	tVData.add(szTemplatePath);
    	tVData.add(sOutXmlPath);
	    tVData.add(contPrintFlag[i]);
    	tVData.add(tTransferData);
    	
        LCContF1PUI tLCContF1PUI = new LCContF1PUI();
    	try
    	{
    	  if(!tLCContF1PUI.submitData(tVData,tAction))
    	  {
    	    Content += tLCContSchema.getContNo() + "打印失败:" + tLCContF1PUI.mErrors.getFirstError() + "：\n";
        	FlagStr = "Fail";
    	  }
    	  else
    	  {
    	    Content += tLCContSchema.getContNo() + "打印成功；\n";
    	  }
          System.out.println(FlagStr + ",1 " + Content);
        }
        catch(Exception ex)
        {
          Content += tLCContSchema.getContNo() + "打印失败：" + ex.toString() + "；\n";
          FlagStr = "Fail";
        }
      }
	}
	
	Content = PubFun.changForHTML(Content);
	
  
  //添加各种预处理
  //if(FlagStr.equals("Success")){
  //	//处理成功，传送xml；
  //	String tprtflag = request.getParameter("PrtFlag");
  //	String templateType = "";
	//	String tCardName = "J04-"+tContNo;
	//	String tFileName = "J03-"+tContNo;
	//  if(tIntlFlag.trim().equals("2")){
  //		templateType = "J008-";
  //		tprtflag = "Notice";	
	//	}
	//	if(tIntlFlag.trim().equals("3")){
  //		templateType = "J009-";
  //		tprtflag = "Notice";	
	//	}
	//	
	//	tCardName = templateType + tContNo;
	//	
  //	
  //	//由于LCContF1PUI把xml放在/printdata/data/目录下，但pdf打印要求放在/printdata/data/briefpdf下
  //	try
  //	{
  //  	File tFile = new File(sOutXmlPath + "/printdata/data/" + tContNo + ".xml");
  //  	FileInputStream fis = new FileInputStream(tFile);
  //  	
  //  	String tOuputPath = sOutXmlPath + "/printdata/data/brief/" + templateType + tContNo + ".xml";
  //  	System.out.println(tOuputPath);
  //  	FileOutputStream fos = new FileOutputStream(tOuputPath);
  //  	byte[] c = new byte[4096];
  //  	int n;
  //    while ((n = fis.read(c)) != -1) {
  //        fos.write(c, 0, n);
  //        
  //        System.out.println("转移文件中...");
  //    }
  //    fos.flush();
  //    fos.close();
  //    fis.close();
  //    tFile.delete();
  //  }catch(Exception ex)
  //  {
  //    ex.printStackTrace();
  //  }
	//	
	//	System.out.println("\n\n\n\n" + tprtflag);
	//	
	//			String url=tPrintServerInterface+"&filename=" + tCardName + "&action=preview";
	//			
	//	//		System.out.println(url);
	//	//		response.sendRedirect(url);
	//			//out.println("<script>var win=window.open("+url+");</script>");
	//			//out.println("<script>var win=window.open("+url+");</script>");
	//			FlagStr = "Succ";
	//			Content = "打印成功! ";	
  //}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>