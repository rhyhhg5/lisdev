<%
//程序名称：BriefContInputInit.jsp
//程序功能：
//创建日期：2005-09-05 11:48:43
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">
var strSqlMain ="";	
	
function query()
{                            
	turnPage.pageLineNum         = 100;
	turnPage.queryModal(strSqlMain,RiskWrapGrid);

	for(var i = 0 ; i<RiskWrapGrid.mulLineCount ; i++){
		for(var m = 1 ; m<RiskWrapGrid.colCount ; m++){
			//alert("行"+i+"列"+m+":"+RiskWrapGrid.getRowColData(i,m));
			if(RiskWrapGrid.getRowColData(i,m)=="null" || RiskWrapGrid.getRowColData(i,m)=="0"){
				RiskWrapGrid.setRowColData(i,m,"");
			}
		}
	}
//	alert(RiskWrapGrid.mulLineCount)  ;                     
}	
function initForm()
{
  try
  {    
    initBox();
    initNationGrid();
    initRiskGrid();
    initBnfGrid();
    initRiskWrapGrid();
    initTrackAccident();
    //showRiskWrapInfo();
    //showRiskInfo();
    //initRiskDutyGrid();
    initExtend();
    query();
    queryContInfo();
    displayAppntMarriage();//增加投保人婚姻选项
    queryImpart();
	showAllCodeName();
	afterCodeSelect("Relation",fm.insured_RelationToAppnt);
	//fm.insured_InsuredNo.value ="";
	//fm.appnt_AppntNo.value = "";
	chosebox();
	// 2007-04-28 update LY
    getAge();
    // end update
    getComcodeList();
    getImpartAll();
    
    if(ContType!="6"&&ContType!="2")
    {
  	    fm.all('ShowMsgFlag').style.display='none';
  	    fm.all("DueFeeMsgFlag").value = "Y";
  	}

    if(ContType == "6")
    {
        fm.all("divLCImpart3").style.display="";
    }
    else
    {
        fm.all("divLCImpart3").style.display="none";
    }
    
  }
  catch(ex)
  {
    alert("在初始化过程中出错："+ex.message)
  }
}
function initBox()
{
  try
  {

    if(ContType == "6")
    {
        qryAppAccInfo();
        fm.all("divAppntAccInfo").style.display = "";
    }
    else
    {
        fm.all("divAppntAccInfo").style.display = "none";
    }

   if(ContType=="5"){
    fm.all('SaleChnl').value = '02';   	
   	}
  else{
  	fm.all('SaleChnl').value = '01';   	
  	}
  	fm.MissionProp5.value = MissionProp5;
    //fm.PolApplyDate.value=PolApplyDate;
    fm.MissionID.value=MissionID;
    fm.SubMissionID.value=SubMissionID;
    fm.PrtNo.value = prtNo;
    fm.ManageCom.value = ManageCom;
	 //modify by zxs 
    fm.AppntAuth.value = '1';
    fm.InsuredAuth.value = '1';
    if(fm.all('ManageCom').value.length>=4){
        if(fm.all('ManageCom').value.substring(0,4)=='8691'){
      	   fm.all('DLXSInput1').style.display="";
        }
    }
    fm.all('Remark').value = "";
//    fm.all('SaleChnl').value = '01';
    fm.all('insured_RelationToAppnt').value = "00";
    //fm.InputDate.value=easyExecSql("select InputDate from lccont where prtno='"+prtNo+"'") ; 
   // alert(fm.InputDate.value);
    //if(fm.InputDate.value=="null"||fm.InputDate.value==""){
    //fm.InputDate.value=CurrentDate;
  	//}
    fm.CValiDate.value=easyExecSql("select date('"+CurrentDate+"') + 1 day from dual") ;    
    fm.ReceiveDate.value=CurrentDate;
    fm.querybutton.disabled=true;
    fm.VideoFlag.value=tVideoFlag;

  }
  catch(ex)
  {
    alert("初始化控件错误！");
    alert(ex);
  }
}

function initRiskGrid()
{
  var iArray = new Array();
	try{
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";         			//列宽
		  iArray[0][2]=10;          			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		  iArray[1]=new Array();
		  iArray[1][0]="险种代码";    	//列名
		  iArray[1][1]="40px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		  iArray[2]=new Array();
		  iArray[2][0]="险种名称";    	//列名
		  iArray[2][1]="200px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
		 
	  	
		
		  iArray[3]=new Array();
		  //境外救援
		  if(MissionProp5 == "1" || MissionProp5 == "4" ){
				iArray[3][0]="保额/档次";
			}
			//交通意外
		  if(MissionProp5 == "2"){
			  iArray[3][0]="档次";    	//列名
			}
			//高原疾病
			if(MissionProp5 == "3"){
				iArray[3][0]="保额";
			}
		  iArray[3][1]="100px";            		//列宽
		  iArray[3][2]=100;            			//列最大值
		  iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		  
		  	 
			iArray[4]=new Array();
			iArray[4][0]="份数";    	//列名
			iArray[4][1]="100px";            		//列宽
			iArray[4][2]=100;            			//列最大值
			//境外救援 高原疾病
			if(MissionProp5 == "1" || MissionProp5 == "3"){	
			  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  	}
			if(MissionProp5 == "2"){	
			  iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  	}
		  
		  iArray[5]=new Array();
		  iArray[5][0]="保费";    	//列名
		  iArray[5][1]="100px";            		//列宽
		  iArray[5][2]=100;            			//列最大值
		  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		 
		  RiskGrid = new MulLineEnter( "fm" , "RiskGrid" );
		  //这些属性必须在loadMulLine前
		  RiskGrid.mulLineCount = 0;
		  RiskGrid.displayTitle = 1;
		  RiskGrid.locked = 0;
		  RiskGrid.canSel = 0;
		  RiskGrid.canChk = 1;
		  //RiskGrid.chkBoxEventFuncName = "showRiskDutyFactor";
		  RiskGrid.hiddenPlus = 1;
		  RiskGrid.hiddenSubtraction = 1;
		  RiskGrid.loadMulLine(iArray);
	}catch (ex){
		alert(ex);
	}
}

function initRiskDutyGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="责任代码";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[2]=new Array();
  iArray[2][0]="责任名称";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[3]=new Array();
  iArray[3][0]="险种代码";    	//列名
  iArray[3][1]="100px";            		//列宽
  iArray[3][2]=100;            			//列最大值
  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  RiskDutyGrid = new MulLineEnter( "fm" , "RiskDutyGrid" );
  //这些属性必须在loadMulLine前
  RiskDutyGrid.mulLineCount = 0;
  RiskDutyGrid.displayTitle = 1;
  RiskDutyGrid.locked = 0;
  RiskDutyGrid.canSel = 0;
  RiskDutyGrid.canChk = 1;
  RiskDutyGrid.chkBoxEventFuncName = "showRiskDutyFactor";
  RiskDutyGrid.hiddenPlus = 1;
  RiskDutyGrid.hiddenSubtraction = 0;
  RiskDutyGrid.loadMulLine(iArray);
}

// 要约信息列表的初始化
function initContPlanGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="险种名称";    	        //列名
    iArray[1][1]="200px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=3;                       //是否允许输入,1表示允许，0表示不允许 2表示代码选择

    iArray[2]=new Array();
    iArray[2][0]="险种编码";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=150;            			//列最大值
    iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="责任编码";         		//列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]= 60;            			//列最大值
    iArray[3][3]= 3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][10]="DutyCode";


    iArray[4]=new Array();
    iArray[4][0]="险种责任";         		//列名
    iArray[4][1]="200px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


    iArray[5]=new Array();
    iArray[5][0]="计算要素";         		//列名
    iArray[5][1]="150px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=3;
    iArray[5][10]="FactorCode";            			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="要素名称";         		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=150;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="要素说明";         		//列名
    iArray[7][1]="350px";            		//列宽
    iArray[7][2]=150;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="要素值";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=150;            			//列最大值
    iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][9]="要素值|num";

    iArray[9]=new Array();
    iArray[9][0]="特别说明";         		//列名
    iArray[9][1]="200px";            		//列宽
    iArray[9][2]=150;            			//列最大值
    iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[10]=new Array();
    iArray[10][0]="险种版本";         		//列名
    iArray[10][1]="100px";            		//列宽
    iArray[10][2]=10;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[11]=new Array();
    iArray[11][0]="集体保单险种号码";         		//列名
    iArray[11][1]="100px";            		//列宽
    iArray[11][2]=10;            			//列最大值
    iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[12]=new Array();
    iArray[12][0]="主险编码";         		//列名
    iArray[12][1]="100px";            		//列宽
    iArray[12][2]=10;            			//列最大值
    iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[13]=new Array();
    iArray[13][0]="类型";         		//列名
    iArray[13][1]="100px";            		//列宽
    iArray[13][2]=10;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[14]=new Array();
    iArray[14][0]="计算方法";         		//列名
    iArray[14][1]="100px";            		//列宽
    iArray[14][2]=10;            			//列最大值
    iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[15]=new Array();
    iArray[15][0]="总保费";         		//列名
    iArray[15][1]="100px";            		//列宽
    iArray[15][2]=10;            			//列最大值
    iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[16]=new Array();
    iArray[16][0]="缴费计划编码";         		//列名
    iArray[16][1]="100px";            		//列宽
    iArray[16][2]=10;            			//列最大值
    iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[17]=new Array();
    iArray[17][0]="给付责任编码";         		//列名
    iArray[17][1]="100px";            		//列宽
    iArray[17][2]=10;            			//列最大值
    iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[18]=new Array();
    iArray[18][0]="账户号码";         		//列名
    iArray[18][1]="100px";            		//列宽
    iArray[18][2]=10;            			//列最大值
    iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[19]=new Array();
    iArray[19][0]="险种代码";         		//列名
    iArray[19][1]="100px";            		//列宽
    iArray[19][2]=10;            			//列最大值
    iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    ContPlanGrid = new MulLineEnter( "fm" , "ContPlanGrid" );
    //这些属性必须在loadMulLine前
    ContPlanGrid.mulLineCount = 0;
    ContPlanGrid.displayTitle = 1;
    ContPlanGrid.hiddenPlus = 1;
    ContPlanGrid.hiddenSubtraction = 0;
    ContPlanGrid.canChk=0;
    ContPlanGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
function initNationGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="国家代码";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
  iArray[1][4]="ldnation";
  iArray[1][5]="1|2";
  iArray[1][6]="0|1";
  iArray[1][9]="国家代码|len<=20";
  iArray[1][15]="chinesename";
  iArray[1][17]="2";
  iArray[1][18]=250;

  iArray[2]=new Array();
  iArray[2][0]="国家名称";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


  NationGrid = new MulLineEnter( "fm" , "NationGrid" );
  //这些属性必须在loadMulLine前
  NationGrid.mulLineCount = 0;
  NationGrid.displayTitle = 1;
  NationGrid.locked = 0;
  NationGrid.canSel = 0;
  NationGrid.canChk = 0;
  NationGrid.hiddenPlus = 0;
  NationGrid.hiddenSubtraction = 0;
  NationGrid.loadMulLine(iArray);
}
// 受益人信息列表的初始化
function initBnfGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="受益人类别"; 		//列名
    iArray[1][1]="60px";		//列宽
    iArray[1][2]=40;			//列最大值
    iArray[1][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="BnfType";
    iArray[1][9]="受益人类别|notnull&code:BnfType";

    iArray[2]=new Array();
    iArray[2][0]="姓名"; 	//列名
    iArray[2][1]="60px";		//列宽
    iArray[2][2]=30;			//列最大值
    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[2][9]="姓名|notnull&len<=20";//校验
    
    iArray[3]=new Array();
    iArray[3][0]="性别"; 	//列名
    iArray[3][1]="30px";		//列宽
    iArray[3][2]=30;			//列最大值
    iArray[3][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="sex";
    iArray[3][9]="性别|notnull&len<=20";//校验

    iArray[4]=new Array();
    iArray[4][0]="证件类型"; 		//列名
    iArray[4][1]="45px";		//列宽
    iArray[4][2]=40;			//列最大值
    iArray[4][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][4]="IDType";
    iArray[4][9]="证件类型|notnull&code:IDType";

    iArray[5]=new Array();
    iArray[5][0]="证件号码"; 		//列名
    iArray[5][1]="120px";		//列宽
    iArray[5][2]=80;			//列最大值
    iArray[5][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][9]="证件号码|notnull&len<=20";

    iArray[6]=new Array();
    iArray[6][0]="与被保人关系"; 	//列名
    iArray[6][1]="60px";		//列宽
    iArray[6][2]=60;			//列最大值
    iArray[6][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][4]="Relation";
    iArray[6][9]="与被保人关系|notnull&code:Relation";

    iArray[7]=new Array();
    iArray[7][0]="受益比例"; 		//列名
    iArray[7][1]="45px";		//列宽
    iArray[7][2]=40;			//列最大值
    iArray[7][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][9]="受益比例|Decimal&len<=10";

    iArray[8]=new Array();
    iArray[8][0]="受益顺序"; 		//列名
    iArray[8][1]="45px";		//列宽
    iArray[8][2]=40;			//列最大值
    iArray[8][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][4]="OccupationType";
    iArray[8][9]="受益顺序|code:OccupationType";
    
    iArray[9]=new Array();
    iArray[9][0]="国籍"; 	//列名
    iArray[9][1]="30px";		//列宽
    iArray[9][2]=30;			//列最大值
    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[9][4]="NativePlace";
    iArray[9][9]="国籍|code:NativePlace";//校验
    
    iArray[10]=new Array();
    iArray[10][0]="职业"; 	//列名
    iArray[10][1]="40px";		//列宽
    iArray[10][2]=100;			//列最大值
    iArray[10][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[10][4]="occupationcode";
    iArray[10][9]="职业|code:occupationcode";
    iArray[10][18]=300;
    

    iArray[11]=new Array();
    iArray[11][0]="联系电话"; 		//列名
    iArray[11][1]="80px";		//列宽
    iArray[11][2]=100;			//列最大值
    iArray[11][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[11][9]="联系电话|len<=18";
    
    iArray[12]=new Array();
    iArray[12][0]="联系地址"; 		//列名
    iArray[12][1]="160px";		//列宽
    iArray[12][2]=100;			//列最大值
    iArray[12][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[12][9]="联系地址|len<=80";
    
    iArray[13]=new Array();
    iArray[13][0]="证件生效日期"; 		//列名
    iArray[13][1]="65px";		//列宽
    iArray[13][2]=100;			//列最大值
    iArray[13][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[13][9]="证件生效日期|Date&len<=80";
    
    iArray[14]=new Array();
    iArray[14][0]="证件失效日期"; 		//列名
    iArray[14][1]="65px";		//列宽
    iArray[14][2]=100;			//列最大值
    iArray[14][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[14][9]="证件失效日期|Date&len<=80";
    

    iArray[15]=new Array();
    iArray[15][0]="速填"; 		//列名
    iArray[15][1]="30px";		//列宽
    iArray[15][2]=30;			//列最大值
    iArray[15][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[15][4]="customertype";

    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" );
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0;
    BnfGrid.displayTitle = 1;
    BnfGrid.addEventFuncName="addInit";
    BnfGrid.loadMulLine(iArray);
		
    //这些操作必须在loadMulLine后面
    //BnfGrid.setRowColData(0,8,"1");
    //BnfGrid.setRowColData(0,9,"1");
  }
  catch(ex)
  {
    alert("在ProposalInit.jsp-->initBnfGrid函数中发生异常:初始化界面错误!");
  }
}
// 保障计划列表的初始化
function initRiskWrapGrid()
{
try{
	//and a.RiskCode='000000' and a.DutyCode='000000'
  var strsql = "select distinct 'Factor',ChooseFlag,CalFactor,CalFactorName,function,factorType "
              +"from LDRiskDutyWrap a,LDWrap b "
              +"where 1=1 and a.RiskWrapCode=b.RiskWrapCode and b.WrapType='"
              +MissionProp5
              +"' and b.WrapProp='Y'"

	var arr = easyExecSql(strsql);	
	//alert(arr)
	if(!arr){
		arr = new Array();
		arr[0] = new Array();
	}
	
	var iArray = new Array();
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=10;
	iArray[0][3]=0;
	
	iArray[1]=new Array();
	iArray[1][0]="套餐编码";
	iArray[1][1]="60px";
	iArray[1][2]=10;
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="套餐名称";
	iArray[2][1]="160px";
	iArray[2][2]=10;
	iArray[2][3]=0;
	
	 strSqlMain = "select a.RiskWrapCode,a.WrapName"
	var i = 2;
	for(var m=0 ; m<arr.length ; m++){
	//	alert(arr[m]);
		for(var n=0 ; n<arr[m].length ; n++){
			if(arr[m][n]=="Factor"){
				var display = 0;
				if(arr[m][n+1] == "6"){
					display = 1;
				}
				if(arr[m][n+1] == "7"){
					display = 3;
				}
			}else{
				break
			}
			var ChooseFlag = arr[m][n+1];
			var CalFactor = arr[m][n+2];
			var CalFactorName = arr[m][n+3];
			var tFunction = arr[m][n+4];
			var FactorType = arr[m][n+5];

			i++;
			iArray[i]=new Array();
			iArray[i][0]="标志位";
			iArray[i][1]="30px";
			iArray[i][2]=10;
			iArray[i][3]=3;
			
			i++;
			iArray[i]=new Array();
			iArray[i][0]="要素";
			iArray[i][1]="30px";
			iArray[i][2]=10;
			iArray[i][3]=3;
			
			i++;
			iArray[i]=new Array();
			iArray[i][0]=CalFactorName
			iArray[i][1]="150px";
			iArray[i][2]=10;
			iArray[i][3]=display;//控制是否显示此要素
			
		//	alert(arr[m][n+4]);
		  if(FactorType == "2"){//险种要素
		  	strSqlMain += ",'Factor','"+CalFactor+"',''";
		  }else if(FactorType == "1"){
		  	strSqlMain += ",'Factor','"+CalFactor+"',(select ";
				strSqlMain += tFunction;
				strSqlMain += "(d."+CalFactor+")";
		    strSqlMain +=  " from LCPol c,LCRiskDutyWrap b,LCDuty d where c.ContNo=d.ContNo and b.DutyCode=d.DutyCode and b.CalFactor='"+CalFactor+"' and c.prtno='"+prtNo+"' and b.RiskWrapCode=a.RiskWrapCode and c.ContNo=b.ContNo and c.Riskcode=b.RiskCode and c.Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=a.RiskWrapCode))";
		  }
		  //strSqlMain += ",'Factor','"+arr[m][n+2]+"',(select ";
			//strSqlMain += arr[m][n+4];
			//strSqlMain += "("+arr[m][n+2]+")";
	    //strSqlMain +=  " from LCPol where  prtno='"+prtNo+"' and Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=a.RiskWrapCode))";
		}
	}
		strSqlMain += " from LDWrap a where a.WrapType='"+MissionProp5+"' and a.WrapProp='Y' ";
        
        // 增加对套餐机构的控制。
        strSqlMain += " and (a.WrapManageCom = '" + ManageCom + "' or a.WrapManageCom = substr('" + ManageCom + "', 1, 2) or a.WrapManageCom = substr('" + ManageCom + "', 1, 4)) ";
        //排除不在核心出单的套餐
        strSqlMain += " and not exists (select 1 from ldcode where codetype='NotHXWrapCode' and code=a.RiskWrapCode )  ";
        strSqlMain += " order by a.RiskWrapCode ";
        // --------------------
        
		fm.RiskWrapCol.value=i;		  
		RiskWrapGrid = new MulLineEnter( "fm" , "RiskWrapGrid" );
		//这些属性必须在loadMulLine前
		RiskWrapGrid.mulLineCount = 0;
		RiskWrapGrid.displayTitle = 1;
		RiskWrapGrid.locked = 0;
		RiskWrapGrid.canSel = 0;
		RiskWrapGrid.canChk = 1;
		RiskWrapGrid.hiddenPlus = 1;
		RiskWrapGrid.hiddenSubtraction = 1;
		RiskWrapGrid.loadMulLine(iArray);
	}catch (ex){
		alert(ex.message);
	}
}


</script>
