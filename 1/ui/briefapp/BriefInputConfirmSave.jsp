<%--
    保存简易保单险种计划信息 2005-09-07 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
  tLCGrpContSchema.setGrpContNo(request.getParameter("GrpContNo"));
  //所有的非代理点出单都是预打保单。
  tLCGrpContSchema.setAppFlag("9");
  TransferData tTransferData = new TransferData();
 	tTransferData.setNameAndValue("MissionID",request.getParameter("MissionID"));
  tTransferData.setNameAndValue("SubMissionID",request.getParameter("SubMissionID"));
  tTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
  tTransferData.setNameAndValue("PrtNo",request.getParameter("PrtNo"));
  tTransferData.setNameAndValue("ManageCom",request.getParameter("ManageCom"));
  tTransferData.setNameAndValue("AgentCode",request.getParameter("AgentCode"));
  
  VData tVData = new VData();
  tVData.add(tTransferData);
  tVData.add(tLCGrpContSchema);
  tVData.add(tG);
  
  GrpBriefTbWorkFlowBL tGrpBriefTbWorkFlowBL = new GrpBriefTbWorkFlowBL();
  //江苏团险校验
  boolean tJSFlag = false;
  {
	  String tJSPrtNo = request.getParameter("PrtNo");
	  String tSaleChnl = new ExeSQL().getOneValue("select salechnl from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tJSGrpContNo = new ExeSQL().getOneValue("select grpcontno from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
			  	  + "and code = '"+tSaleChnl+"'";
	  SSRS tSSRS = new ExeSQL().execSQL(tSql);
	  String tSubManageCom = request.getParameter("ManageCom").substring(0,4);
	  if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
		  ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
    	  TransferData tJSTransferData = new TransferData();
    	  tJSTransferData.setNameAndValue("ContNo",tJSGrpContNo);
    	  VData tJSVData = new VData();
    	  tJSVData.add(tJSTransferData);
    	  if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
    		  Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
    		  FlagStr = "Fail";
    		  tJSFlag = true;
    	  }
	  }else{
			System.out.println("没有销售渠道信息或管理机构信息出错！");
	  }
  }
  if(!tJSFlag){
  	if(!tGrpBriefTbWorkFlowBL.submitData(tVData,"0000005001")){
      Content = " 处理失败，原因是: " + tGrpBriefTbWorkFlowBL.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
  	}else{
      Content = " 处理成功！";
      FlagStr = "Succ";
  	}
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>