<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContDeleteDown.jsp
//程序功能：清单下载
//创建日期：2008-1-15 17:25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  String querySql = request.getParameter("sql");
  querySql = querySql.replaceAll("%25","%");
  //设置表头
  String[][] tTitle = {{"印刷号","保单号","管理机构","代理机构","代理人","投保人姓名","录入人员代码",
	"保单申请日期","保单生效日期","银行编码","帐号","户名","保费","状态","销售渠道","网点"}};
  //表头的显示属性
  int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    
  //数据的显示属性
  int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  int row = createexcellist.setData(tTitle,displayTitle);
  if(row ==-1) errorFlag = true;
  createexcellist.setRowColOffset(row,0);//设置偏移
  if(createexcellist.setData(querySql,displayData)==-1)
  	errorFlag = true;
  if(!errorFlag)
  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>

