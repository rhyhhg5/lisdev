<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="BriefLCInuredListInput.js"></SCRIPT>
  <script>
  	var GrpContNo ="<%=request.getParameter("GrpContNo")%>";
  	var LoadFlag ="<%=request.getParameter("LoadFlag")%>";
  	var MissionID ="<%=request.getParameter("MissionID")%>";
  	var SubMissionID ="<%=request.getParameter("SubMissionID")%>";
  	var AgentType ="<%=request.getParameter("AgentType")%>";
  	var mPrtNo ="<%=request.getParameter("PrtNo")%>";
  </script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BriefLCInuredListInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BriefLCInuredListSave.jsp" method=post name=fmSave target="fraSubmit" >
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInuredList);">
    		</td>
    		 <td class= titleImg>
        		 被保人清单表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLCInuredList" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    
    <TD  class= title>
      被保人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InsuredName verify="被保人姓名|notnull" elementtype=nacessary>
    </TD>
    <TD  class= title>
      英文名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EnglishName verify="英文名称|notnull" elementtype=nacessary>
    </TD>
    <TD  class= title>
      护照号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OthIDNo>
    </TD>
  </TR>
  <TR  class= common>
    
   <TD  class= title>
      性别
   </TD>
   <TD  class= input>
   <Input class=codeNo name=Sex  verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input class=codename name=SexName readonly=true elementtype=nacessary>    
    </TD>
   <TD  class= title>
      出生日期
   </TD>
   <TD  class= input>
   <input class="coolDatePicker" dateFormat="short" elementtype=nacessary name="Birthday" verify="出生日期|notnull&date" >
   </TD>          
    <TD  class= title>
      身份证号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDNo verify="身份证号|IDNo" >
    </TD>
  </TR>
</table>
</Div>
<hr>
<Div id="divLCInuredList1" style="display:'none'">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLCInuredListGrid">
  				</span> 
		    </td>
			</tr>
		</table>
</div>
<div id="hiddenDiv" style="display: 'none'">
	<TR  class= common>
    <TD  class= title>
      联系电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone >
    </TD>
  </tr>
	<TR  class= common>
    <TD  class= title>
      职业类别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationType >
    </TD>
    <TD  class= title>
      理赔金转帐银行
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankCode >
    </TD>
    <TD  class= title>
      帐号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankAccNo >
    </TD>
    <TD  class= title>
      户名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccName >
    </TD>
  </TR>
	<TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDType >
    </TD>
	<TD  class= title>
      与员工关系
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Relation >
    </TD>
	<TD  class= title>
      其它证件类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OthIDType >
    </TD>
	<TR  class= common>
		<TD  class= title>
      在职/退休
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Retire >
    </TD>
    <TD  class= title>
      员工姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EmployeeName >
    </TD>
    <TD  class= title>
      集体合同号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpContNo >
    </TD>
    <TD  class= title>
      被保人序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InsuredID >
    </TD>
    <TD  class= title>
      状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=State >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      合同号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContNo >
    </TD>
    <TD  class= title>
      批次号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BatchNo >
    </TD>
    <TD  class= title>
      被保人客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InusredNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>	
  	<TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
    <TD  class= title>
      保险计划
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContPlanCode >
    </TD>
</div>
<span id="operateButton" >
	<table  class=common align=center>
		<tr align=left>
			<td class=button width="10%" align=left>
				<INPUT class=cssButton name="saveButton" VALUE="新  增"  TYPE=button onclick="return submitForm();">
				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
				<INPUT class=cssButton name="deleteButton" VALUE="删  除"  TYPE=button onclick="return deleteClick();">
			  <INPUT class=cssButton name="pisdbutton1" VALUE="导入被保人清单" TYPE=button onclick="getin();"> 
			</td>
		</tr>
	</table>
</span>
<input type=hidden id="" name="fmtransact">
    <input type=hidden id="" name="fmAction">
	    <Div  id= "divDiskErr" style= "display: ''">
	   <Table  class= common>
	       <TR  class= common>
	        <TD text-align: left colSpan=1>
		         <span id="spanPersonInsuredGrid" >
						 </span>
	  			</TD>
	      </TR>
	      <TR  class= common>
	      	<TD text-align: left colSpan=1>
			      <p align="left">
			    	<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
			      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
			      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
			      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"></p>	
			    </TD>
	      </TR>	
	    </Table>	
	 </Div>
<span id="operateButtonModify" style="display: 'none'" >
	<table  class=common align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="queryButton" VALUE="修  改"  TYPE=button onclick="return insuredModify();">
			</td>
		</tr>
	</table>
</span>
<div id="divPlanCode">
<table>
        <tr>
            <td>
                 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProject);">
            </td>
             <td class= titleImg>
                 方案录入
             </td>           
        </tr>
</table>
<div id="divProject">
<table class="common">
    <tr class="common">
        <td class="title">境外业务承保方案</td>
        <td class="input">
            <Input class=codeNo name=PlanCode ondblclick="return showCodeList('wrapplancode',[this,PlanName],[0,1],null,'prtno',<%=request.getParameter("PrtNo")%>);" onkeyup="return showCodeListKey('wrapplancode',[this,PlanName],[0,1],null,'prtno',<%=request.getParameter("PrtNo")%>);"><input class=codename name=PlanName readonly=true>
        </td>
        <td class="title">&nbsp;</td>
        <td class="input">&nbsp;</td>
        <td class="title">&nbsp;</td>
        <td class="input">&nbsp;</td>
        <td class="title">&nbsp;</td>
        <td class="input">&nbsp;</td>
    </tr>
</table>
</div>
</div>
<table>
	<tr>
		<td>
		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInuredList);">
		</td>
		 <td class= titleImg>
    		 保障计划信息
   		 </td>        
	</tr>
</table>
<table class="common">
    <tr class="common">
        <td class="title">是否选择附加责任</td>
        <td class="input">
            <input class="codeNo" name="AppendDutyFlag" CodeData="0|^0|不选择^1|选择" ondblclick="return showCodeListEx('AppendDutyFlag',[this,AppendDutyFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('AppendDutyFlag',[this,AppendDutyFlagName],[0,1],null,null,null,1);" /><input class="codename" name="AppendDutyFlagName" readonly="readonly" />
        </td>
        <td class="title">&nbsp;</td>
        <td class="input">&nbsp;</td>
        <td class="title">&nbsp;</td>
        <td class="input">&nbsp;</td>
        <td class="title">&nbsp;</td>
        <td class="input">&nbsp;</td>
    </tr>
</table>
	 <!--险种信息-->
<div id="RiskInfoGridDiv" style="display: ''">
	<table  class= common>
		<tr  class= common>
			<td text-align: left colSpan=1>
				<span id="spanRiskGrid" >
				</span>
			</td>
		</tr>
	</table>
</div> 
<div id="RiskInfoTableDiv" style="display: ''">
	<table  class=common align=center>
		<tr align=right>	
			<td class=button width="10%" align=right>
				<INPUT class=cssButton  VALUE="保存保障计划"  TYPE=button onclick="return saveRiskPlan();">
			</td>
		</tr>
	</table>
</div>
	<!--险种信息-->
	 <hr>
	<table class= common>
		<tr class=common>
			<TD  class= title>
			   生效日期
			</TD>
			<TD  class= input>
			  <Input class= 'readonly' readonly name=CValiDate >
			</TD>
			<TD  class= title>
			   失效日期
			</TD>
			<TD  class= input>
			  <Input class= 'readonly' readonly name=CInValiDate >
			</TD>
			<TD  class= title>
			    责任天数
			</TD>
			<TD  class= input>
			  <Input class= 'readonly' readonly name=Days >
			</TD>
		</tr>
		<tr class=common>
			<TD  class= title>
			   被保人总数
			</TD>
			<TD  class= input>
			  <Input class= 'readonly' readonly name=SumInsured >
			</TD>
			<TD  class= title>
			   总保费
			</TD>
			<TD  class= input>
			  <Input class= 'readonly' readonly name=SumPrem >
			</TD>
		</tr>
		<tr class=common>
			<TD  class= common>
			</TD>
			<TD  class= common>
			</TD>
			<TD  class= common>
			</TD>
			<TD  class= common>
			</TD>
		</tr>
	</table>
	<p align="right">
		<TD  class= common align="right"> 
			<INPUT class=cssButton name="confirmButton1" VALUE="录入完毕"  TYPE=button onclick="return signCont();">
			<!--INPUT class=cssButton  VALUE="测试签单后保单打印"  TYPE=button onclick="return testPrintCont();"-->
		</TD>
		<TD  class= common align="right"> 
			<INPUT class=cssButton name="confirmButton2" VALUE="录入完毕"  TYPE=button onclick="return inputConfirm();">
			<!--INPUT class=cssButton  VALUE="测试签单后保单打印"  TYPE=button onclick="return testPrintCont();"-->
		</TD>
		<td class=button width="10%" align=right>
			<INPUT class=cssButton VALUE="上一步"  TYPE=button onclick="return previous();">
		</td>
	</p>
	
</form>
<!--form action="./BriefLCInuredListSave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
<div id="divDiskInput" style="display:''">
    <table class = common >
    	<TR>
      <TD  colspan=2>
        
      </TD>
	  	</TR>
    <TR  >
      <TD  width='8%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <Input  type="file"　width="100%" name=FileName class= common>
        <INPUT  VALUE="上载被保险人清单" class="cssbutton" TYPE=button onclick = "InsuredListUpload();" >
        <INPUT  VALUE="计算保费" class="cssbutton" TYPE=button onclick = "CalInsuredPrem();" >
        <Input  type="hidden"　width="100%" name="insuredimport" value ="1">
      </TD>
    </TR>
    
	    <input type=hidden name=ImportFile>
	</table>
	<div id="divInsuredInfo" style="display: ''">
	<table class=common>
		<tr class=common >
			<TD  class= title>
		     被保人总数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=SumInsured >
		  </TD>
		  <TD  class= title>
		     待导入人数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=HoldInsured >
		  </TD>
		  <TD  class= title>
		     导入成功数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=SuccInsured >
		  </TD>
		  <TD  class= title>
		     导入失败数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=FailInsured >
		  </TD>
		</tr>
	</table>
</div>
   <Div  id= "divDiskErr" style= "display: ''">
	   <Table  class= common>
	       <TR  class= common>
	        <TD text-align: left colSpan=1>
	            <span id="spanDiskErrQueryGrid" ></span> 
	  	</TD>
	      </TR>
	    </Table>	
	    <p align="center">
	    	<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"></p>				
	      
	 </Div>		
</div>
	    <Div  id= "divDiskErr" style= "display: ''">
	   <Table  class= common>
	   	<TR>
        <TD>
          <INPUT class=cssButton  value="查  询" onclick="queryperinsure();" type=button>
        </TD>
      </TR>
	       <TR  class= common>
	        <TD text-align: left colSpan=1>
		         <span id="spanPersonInsuredGrid" >
						 </span>
	  			</TD>
	      </TR>
	    </Table>	
	    <p align="center">
	    	<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();"></p>				
	      
	 </Div>
    <input type=hidden id="" name="fmtransact">
    <input type=hidden id="" name="fmAction">
  </form-->
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
