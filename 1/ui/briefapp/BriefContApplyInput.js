//程序名称：BriefContApplyInput.js
//程序功能：简易投保
//创建日期：2005-09-05 11:10:36
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var k = 0;
parent.fraMain.rows = "0,0,0,0,*";
var cContType;
var AgentType;
/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick(){
	//alert();
	// 初始化表格
	var activityWherePart="";
	if(type=="1"){
    
        // 加入保单类型的校验
        var cContType=fm.ContType.value;
        if(cContType == "")
        {
            alert("请选择保单类型！");
            return false;
        }
        // -------------------------
        
	initPersonGrid();
		activityWherePart=" and activityid='0000007001' ";
		fm.ActivityID.value="7999999999";
        
	var strSQL = "" 
        + " select lwm.MissionProp1, lwm.MissionProp3, lwm.MissionProp4, lwm.MissionProp2, "
        + " lwm.MissionID, lwm.subMissionID, lwm.MissionProp5, lcc.PolApplyDate "
        + " from LWMission lwm "
        + " left join LCCont lcc on lwm.MissionProp1 = lcc.PrtNo and lcc.ContType = '1' "
        + " where 1 = 1 "
        + " and lwm.ActivityId in ('0000007001', '0000007003') "
        + " and not exists(select 1 from BPOMissionState bpoms where bpoms.BussNoType = 'TB' and bpoms.BussNo = lwm.MissionProp1 and bpoms.State != '06') "
        + getWherePart('missionprop1','PrtNo')
        + getWherePart('missionprop2','InputDate')
        + getWherePart('missionprop3','ManageCom','like')
        + getWherePart('missionprop5','ContType')
        +" order by missionprop2 desc";
        
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有该保单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PersonGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;	
		
	}else{
	initGrpGrid();
	activityWherePart=" and activityid='0000005001' ";
	fm.ActivityID.value="5999999999";	
    
	if(AgentCom != "null" && AgentCom != "")
    {
    	var strSQL = ""
            + " select lwm.MissionProp1, lwm.MissionProp3, lwm.MissionProp4, "
            + " lwm.MissionProp2, lwm.MissionID, lwm.subMissionID, "
            + " lgc.PolApplyDate "
            + " from LWMission lwm "
            + " left join LCGrpCont lgc on lgc.PrtNo = lwm.MissionProp1 "
            + " where 1 = 1 "
        	+ " and lwm.MissionProp6 = '" + AgentCom + "' "
        	+ activityWherePart
        	+ getWherePart("lwm.missionprop1", "PrtNo")
        	+ getWherePart("lwm.missionprop2", "InputDate")
        	+ getWherePart("lwm.missionprop3", "ManageCom", "like")
        	+ " order by missionprop2 desc ";
	}
    else
    {
        var strSQL = ""
            + " select lwm.MissionProp1, lwm.MissionProp3, lwm.MissionProp4, "
            + " lwm.MissionProp2, lwm.MissionID, lwm.subMissionID, "
            + " lgc.PolApplyDate "
            + " from LWMission lwm "
            + " left join LCGrpCont lgc on lgc.PrtNo = lwm.MissionProp1 "
            + " where 1 = 1 "
            + activityWherePart
        	+ getWherePart("lwm.missionprop1", "PrtNo")
        	+ getWherePart("lwm.missionprop2", "InputDate")
        	+ getWherePart("lwm.missionprop3", "ManageCom", "like")
            + " order by missionprop2 desc ";
	}
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
		//alert(strSQL);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有该保单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
	}
}


/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入扫描随动页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GoToInput()
{
  var i = 0;
  var checkFlag = 0;
  var state = "0";
 	if(type=="2"){ 
  for (i=0; i<GrpGrid.mulLineCount; i++) {
    if (GrpGrid.getSelNo(i)) { 
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1); 	
    var	ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
    var MissionID =GrpGrid.getRowColData(checkFlag - 1, 5);
    var SubMissionID =GrpGrid.getRowColData(checkFlag - 1, 6);
		var PolApplyDate = GrpGrid.getRowColData(checkFlag - 1, 4);
//		cContType = GrpGrid.getRowColData(checkFlag - 1, 7);
		var urlStr;
		//alert(fm.ActivityID.value);
		
        // 针对用户对保单进行加锁。目前先注释掉。
        //var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and PolState=1002 ";
        //var arrResult = easyExecSql(strSql);
        //if (arrResult!=null && arrResult[0][1]!=operator) {
        //alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
        //return;
        //}
        
        //var urlStrLock = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
        //showModalDialog(urlStrLock,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
        // ------------------------------------
		
		if(fm.ActivityID.value=="5999999999"){
    	urlStr = "./BriefContInputMain.jsp?prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PolApplyDate="+PolApplyDate+"&AgentType="+tAgentType+"&AgentCom="+AgentCom;
  	}
    var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
    //申请该印刷号
    //var strReturn = OpenWindowNew(urlStr, "简易投保", "left");
    window.open(urlStr,"",sFeatures);
  }
  else {
    alert("请先选择一条保单信息！"); 
  			}
	}else{
  for (i=0; i<PersonGrid.mulLineCount; i++) {
    if (PersonGrid.getSelNo(i)) { 
      checkFlag = PersonGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	prtNo = PersonGrid.getRowColData(checkFlag - 1, 1); 	
    var	ManageCom = PersonGrid.getRowColData(checkFlag - 1, 2);
    var MissionID =PersonGrid.getRowColData(checkFlag - 1, 5);
    var SubMissionID =PersonGrid.getRowColData(checkFlag - 1, 6);
		var PolApplyDate = PersonGrid.getRowColData(checkFlag - 1, 4);
		cContType = PersonGrid.getRowColData(checkFlag - 1, 7);
		
		var urlStr;
		//alert(fm.ActivityID.value);
		 if(fm.ActivityID.value=="7999999999"){
  		//if(cContType=="1"&&ManageCom!="86110000"){
			//	alert("此机构录入申请境外救援的保单！");
			// 	return false;
			//}
			 if (prtNo != "") {
			    	var srtPrtno = CheckPrtno(prtNo);
			     	if (srtPrtno != "") {
			     		alert(srtPrtno);
			    	  return false;
			     	}
				}
  		if(cContType=="")
			{
				alert("保单类型不能为空!");
			 	return false;
			}
			if(cContType=="2"){
				AgentType = "1";
			}
  		urlStr = "./BriefSingleContInputMain.jsp?prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PolApplyDate="+PolApplyDate+"&cContType="+cContType+"&AgentType="+AgentType+"&AgentCom="+AgentCom;
  	}
  	
        // 针对用户进行保单锁定，目前注释掉。
        //var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and PolState=1002 ";
        //var arrResult = easyExecSql(strSql);
        //if (arrResult!=null && arrResult[0][1]!=operator) {
        //alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
        //return;
        //}
        
        //var urlStrLock = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
        //showModalDialog(urlStrLock,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
        // -------------------------------

    var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
    //申请该印刷号
    //var strReturn = OpenWindowNew(urlStr, "简易投保", "left");
    window.open(urlStr,"",sFeatures);
  }
  else {
    alert("请先选择一条保单信息！"); 
  			}		
	}
}
function submitData()
{
	var cPrtNo = fm.PrtNo.value;
	var cManageCom = fm.ManageCom.value;
	var cContType=fm.ContType.value;
	//alert(AgentCom);
	fm.AgentCom.value=AgentCom;
	if(!fm.ActivityID.value=="5999999999"){
		if( verifyInput2() == false ) return false;
		if(cPrtNo == "")
		{
			alert("请录入印刷号！");
			return;
		}
		if(cManageCom == "")
		{
			alert("请录入管理机构！");
			return;		
		}
        
        // 加入保单类型的校验
        if(cContType == "")
        {
            alert("请选择保单类型！");
            return false;
        }
        // -------------------------
        
		//if(isNumeric(cPrtNo)==false){
		//	 alert("印刷号应为数字");
		//	 return false;
		//}
		//alert(cContType);
		//alert(cManageCom);		
		//	if(cContType=="1"&&cManageCom!="86110000"){
		//	alert("此机构不能申请境外救援的保单！");
		//	 return false;
		//}
		var strSql = "select 1 from lcgrpcont where prtno='"+cPrtNo+"'"
									+" union "
									+"select 1 from lccont where prtno='"+cPrtNo+"'"
									+" union "
									+"select 1 from lbcont where prtno='"+cPrtNo+"'"
									+" union "
									+"select 1 from lwmission where MissionProp1='"+cPrtNo+"' with ur";
		if(easyExecSql(strSql)){
			alert("印刷号已存在");
			return false;
		}
	}
	if(fm.ActivityID.value=="5999999999"){
		fm.action="BriefContApplyInputSave.jsp";
		fm.btnSubmit.disabled = true;
		fm.submit();
	}
	else if(fm.ActivityID.value=="7999999999"){
		if(cContType == "")
		{
			alert("请录入保单类型！");
			return;		
		}
				if(cPrtNo == "")
		{
			alert("请录入印刷号！");
			return;
		}else{
	    	var srtPrtno = CheckPrtno(cPrtNo);
	     	if (srtPrtno != "") {
	     		alert(srtPrtno);
	    	  return false;
	     	}
		}
		if(cManageCom == "")
		{
			alert("请录入管理机构！");
			return;		
		}
		//if(isNumeric(cPrtNo)==false){
		//	 alert("印刷号应为数字");
		//	 return false;
		//}
		
//对印刷号的校验：前10位相加取个位，再与11位比较。		

    if(cContType=="6"){
	    if(cPrtNo.length!=12 && cPrtNo.length!=13)
	    {
	    	alert("印刷号位数不正确！");
	    	return;
    	}
	    var sum = 0;
	    var sum2 = 0;
	    var last = 0;
	    if(cPrtNo.length == 12){
	    	for(var i=0; i<11; i++) 
		    {
		        sum = sum + cPrtNo.substr(i,1)*1;
		    }
	        sum = sum+" ";
	        sum2 = sum.substring(sum.length-2,sum.length-1);
	        last = cPrtNo.substr(11,1);
            if(sum2!=last)
            {
      	       alert("录入印刷号错误！");
      	       return;
      	    }
	    }
	    if(cPrtNo.length == 13){
	    	for(var i=0; i<12; i++) 
		    {
		        sum = sum + cPrtNo.substr(i,1)*1;
		    }
	        sum = sum+" ";
	        sum2 = sum.substring(sum.length-2,sum.length-1);
	        last = cPrtNo.substr(12,1);
            if(sum2!=last)
            {
      	       alert("录入印刷号错误！");
      	       return;
      	    }
	    }
    }
    
	var strSql = "select 1 from lcgrpcont where prtno='"+cPrtNo+"'"
							+" union "
							+"select 1 from lccont where prtno='"+cPrtNo+"'"
							+" union "
							+"select 1 from lbcont where prtno='"+cPrtNo+"'"
							+" union "
							+"select 1 from lwmission where MissionProp1='"+cPrtNo+"' with ur";
		if(easyExecSql(strSql)){
			alert("印刷号已存在");
			return false;
		}
		//if(fm.ContType.value == "2"){
		//	var strSQL1="select 1 from ljtempfee where otherno='"+cPrtNo+"'";
		//	if(!easyExecSql(strSQL1)){
		//		alert("没有缴费,不能生成保单!");
		//		return false;
		//	}	
		//}
		fm.action="BriefContSingleApplyInputSave.jsp";
		fm.btnSubmit.disabled = true;
		fm.submit();
	}
}
function afterSubmit(FlagStr, content)
{
  window.focus();
  
  if("Fail" == FlagStr)
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
	  easyQueryClick();
	}
	fm.btnSubmit.disabled = false;
}



