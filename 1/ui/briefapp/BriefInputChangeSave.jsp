<%--
    保存简易保单保单维护换号处理  2005-9-17 16:38 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  
  String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
	String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
  tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
  tAction = request.getParameter("fmAction");
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
  tLCGrpContSchema.setGrpContNo(request.getParameter("GrpContNo"));
  VData tVData = new VData();
  tVData.add(tLCGrpContSchema);
  tVData.add(tG);
  tVData.add(tTransferData);
  GrpBriefChangeNoUI tGrpBriefChangeNoUI = new GrpBriefChangeNoUI();
  //江苏团险校验
  boolean tJSFlag = false;
  {
	  String tJSPrtNo = request.getParameter("PrtNo");
	  String tSaleChnl = new ExeSQL().getOneValue("select salechnl from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tJSGrpContNo = new ExeSQL().getOneValue("select grpcontno from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
			  	  + "and code = '"+tSaleChnl+"'";
	  SSRS tSSRS = new ExeSQL().execSQL(tSql);
	  String tManageCom = new ExeSQL().getOneValue("select managecom from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tSubManageCom = tManageCom.substring(0,4);
	  if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
		  ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
    	  TransferData tJSTransferData = new TransferData();
    	  tJSTransferData.setNameAndValue("ContNo",tJSGrpContNo);
    	  VData tJSVData = new VData();
    	  tJSVData.add(tJSTransferData);
    	  if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
    		  Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
    		  FlagStr = "Fail";
    		  tJSFlag = true;
    	  }
	  }
  }
  if(!tJSFlag){
  	if(!tGrpBriefChangeNoUI.submitData(tVData,tAction)){
      Content = " 处理失败，原因是: " + tGrpBriefChangeNoUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
  	}else{
      Content = " 处理成功！";
      FlagStr = "Succ";
  	}
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>