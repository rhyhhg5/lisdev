var showInfo;
var mDebug="0";
var manageCom;
var turnPage = new turnPageClass();
parent.fraMain.rows = "0,0,0,0,*";
function easyQueryClick1()
{
    //如果没有印刷号或保单号，则校验日期是否为空    2008-7-18
	if(fm.PrtNo.value == "" && fm.ContNo.value == "" && verifyInput() == false)
        return false;
	/*if(dateDiff(fm.SignStartDate.value, fm.SignEndDate.value,"M") > 3)
	{
		alert("统计期最多为三个月！");
		return false;
	}*/
	
    var consql="";
    if(LoadFlag=="1"){
        consql = " and printcount<=1 ";
    }
    var conSQL="";
    if(fm.CardFlag.value == '1')
    {
    	conSQL = " and exists (select 1 from LCPol lcp inner join LDCode1 ldc1 on ldc1.Code1 = lcp.RiskCode where lcp.ContNo = a.ContNo and ldc1.Code = a.CardFlag and ldc1.codetype = 'bcprint' and ldc1.othersign='1' ) ";
    }
    if(fm.CardFlag.value == '2'||fm.CardFlag.value == '5'||fm.CardFlag.value == '6')
    {
    	conSQL = " and exists ((select 1 from LCRiskDutyWrap lcrdw inner join LDCode1 ldc1 on ldc1.Code1 = lcrdw.RiskWrapCode where lcrdw.ContNo = a.ContNo and ldc1.Code = a.CardFlag and ldc1.codetype = 'bcprint'  and ldc1.othersign='1' ) union "
    	       + " (select 1 from ldcode1 where codetype='bcprintcom' and code='comcode' and code1=a.managecom and othersign='1'))";
    }
    if(fm.CardFlag.value == "")
    {
    	conSQL = " and exists ((select 1 from LCPol lcp inner join LDCode1 ldc1 on ldc1.Code1 = lcp.RiskCode where lcp.ContNo = a.ContNo and ldc1.Code = a.CardFlag and ldc1.codetype = 'bcprint'  and ldc1.othersign='1' ) union " 
    	       + " (select 1 from LCRiskDutyWrap lcrdw inner join LDCode1 ldc1 on ldc1.Code1 = lcrdw.RiskWrapCode where lcrdw.ContNo = a.ContNo and ldc1.Code = a.CardFlag and ldc1.codetype = 'bcprint'  and ldc1.othersign='1') union "
    	       + " (select 1 from ldcode1 where codetype='bcprintcom' and code='comcode' and code1=a.managecom and othersign='1') )";
    }
    var AgentStr = "";
    if(fm.AgentCode.value != ""){
    	AgentStr = " and a.AgentCode in (select AgentCode from LAAgent  where Groupagentcode = '"+fm.AgentCode.value+"')";
    }
    // 加入是否打印的条件
    var tPrintState = fm.PrintState.value;
    switch(tPrintState)
    {
        case "0":
            consql += " and PrintCount = 0 ";
            break;
        case "1":
            consql += " and PrintCount >= 1 ";
            break;
    }

    var strSql = " select contno,prtno,appntname,CodeName('cardflag', a.CardFlag),cardflag,"
               + " (select BranchAttr from LABranchGroup labg where labg.AgentGroup = a.AgentGroup) BranchAttr, "
               + " (select Groupagentcode from LAAgent la where  la.AgentCode= a.AgentCode) AgentCode "
               + " from lccont a where a.cardflag in ('1','2','5','6') and a.appflag = '1' and a.conttype='1' and a.uwflag in ('4','9') "
               + " and ManageCom like '" + comcode + "%'"	//集中权限管理体现
               + " and ManageCom like '" + fm.ManageCom.value + "%'"
               +  AgentStr
               + " and not exists (select 1 from LCRiskDutyWrap lcrdw inner join LDCode1 ldc1 on ldc1.Code1 = lcrdw.RiskWrapCode where lcrdw.ContNo = a.ContNo and ldc1.Code = a.CardFlag and ldc1.codetype = 'bcprintdisabled')  "
               + conSQL
               + consql
               + getWherePart('a.SaleChnl' ,'SaleChnl' )
               + getWherePart('a.ContNo','ContNo')
               + getWherePart('a.PrtNo', 'PrtNo')
               + getWherePart('a.CardFlag', 'CardFlag')
               //+ getWherePart('a.AgentCode', 'AgentCode')
               + getWherePart('a.SignDate', 'SignStartDate', '>=')
               + getWherePart('a.SignDate', 'SignEndDate', '<=')
               + " order by BranchAttr, AgentCode, ContNo with ur ";
    fm.querySql.value = strSql;//用于报表的打印    2008-3-12
    turnPage.queryModal(strSql,ContPageGrid);

}
function printGroupPol()
{
	
  var rowNo = ContPageGrid.getSelNo()-1;
  if(rowNo<0)
  {
    alert("请选择一个保单！");
    return false;
  }

  fm.mContNo.value = ContPageGrid.getRowColData(rowNo,1);
  fm.mCardFlag.value = ContPageGrid.getRowColData(rowNo,5);

  if(fm.all('mContNo').value =="null" || fm.all('mContNo').value==null ||fm.all('mContNo').value=="")
  {
    alert("请选择一个保单");
    return false;
  }
  
  /**
	 *   进行影像件校验
	 */
	//开始校验未存在影像件的pad保单
	var PrtNo = ContPageGrid.getRowColData(rowNo,2);
	var isPadFlag = PrtNo.substring(0,2);
	//1、判断被选中保单是否为PAD出单
	if(isPadFlag == "PD") {
		/**
		 * 新增问题件校验20180702
		 */
		var checkSQL = "select ScanCheckFlag from lccontsub where prtno = '"
				+ PrtNo
				+ "' with ur";
		var result = easyExecSql(checkSQL);
		if ("1" != result[0][0]) {
			alert("此保单尚未完成影像件复查，无法打印！");
			return false;
		}
		//2、判断该PAD保单是否存在影像件
		var checkSql = "select * from es_doc_main where doccode = '"+PrtNo+"' and Subtype='TB28' with ur";
		turnPage.strQueryResult = easyQueryVer3(checkSql);
		if (!turnPage.strQueryResult) {
			alert("印刷号为："+PrtNo+"的保单不存在PAD影像件，不能打印！");
			return false;
		}
	}

  if(LoadFlag=="1"||LoadFlag=="3" )
  {
    fm.fmAction.value="PRINT";

  }
  if(LoadFlag=="2")
  {
    fm.fmAction.value="REPRINT";
  }
  fm.PrtFlag.value="POL";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}
function printGroupPolCard()
{

  var rowNo = ContPageGrid.getSelNo()-1;
  if(rowNo<0)
  {
    alert("请选择一个保单！");
    return false;
  }

  fm.mContNo.value = ContPageGrid.getRowColData(rowNo,1);
  fm.mCardFlag.value = ContPageGrid.getRowColData(rowNo,5);

  if(fm.all('mContNo').value =="null" || fm.all('mContNo').value==null ||fm.all('mContNo').value=="")
  {
    alert("请选择一个保单");
    return false;
  }
  if(LoadFlag=="1")
  {
    fm.fmAction.value="PRINT";

  }
  if(LoadFlag=="2")
  {
    fm.fmAction.value="REPRINT";
  }
  fm.PrtFlag.value="Notice";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}

//下载投保单信息清单	2008-3-12
function printList()
{
	if(fm.querySql.value != null && fm.querySql.value != "" && ContPageGrid.mulLineCount > 0)
	{
		fm.action = "BriefSingleContPrint.jsp";
		fm.target = "_blank";
		fm.submit();
	}
	else
	{
		alert("请先执行查询操作，再打印！");
		return ;
	}
}

//首次打印
function firstPrintCont()
{
    fm.firstPrint.value = "1";
    return printGroupPol();
}