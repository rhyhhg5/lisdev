  //               该文件中包含客户端需要处理的函数和事件
//控制界面上的mulLine的显示条数
var mulLineShowCount = 15;
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var bankflag="";
//提交，保存按钮对应操作

//排盘导入
function submitForm()
{    
	var rowNum = FeeGrid.mulLineCount;
  
	if(rowNum==null || rowNum<1){
		alert('请先添加一条信息,然后再提交');
		return false;
	}
	var ChkFlag=0;
	for(var i=0;i<rowNum;i++){
		if(FeeGrid.getChkNo(i)){
			ChkFlag=1;
		}
	}
	if(ChkFlag==0){
		alert('请至少选择一行记录！');
		return false;
	}
  
    fm.action = "./FirstPaySave.jsp";
    
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    //showInfo=window.open(urlStr,"",'height=250,width=500,top=250,left=270,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   
    fm.all("ConfirmButton").disabled = true;
    
    fm.submit(); 
    
}

function checkValue()
{
	 var rowNum = FeeGrid.mulLineCount;
	for(var i=0;i<rowNum;i++){
		
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result )
{    
	  showInfo.close();
      if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     
    //执行下一步操作
  }
   initForm();
   
    if(fm.ConfirmButton.disabled == true)
    {
    	fm.ConfirmButton.disabled = false;
    }
}
//查询银行编码
function querybank()
{
		bankflag="0";
	showInfo = window.open("../bq/LDBankQueryMain.jsp");
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！");
		 return;
	}
    if(fm.all('AgentCode').value == "")
    {
		  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  }
	if(fm.all('AgentCode').value != "")
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码
		var strSql = "select AgentCode,Name from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
      }
	}

}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  }
}

function queryClick(Result)
{
    QueryGrpGrid.clearData();
    userCurPage = 0;
		
    userArray = decodeEasyQueryResult(Result);
 
    if (userArray == null) {
    	alert("没有银行导入信息。");
    	return;
    }

    userArrayLen = userArray.length;
    fillUserGrid();
}


function fillFeeGrid()
{    
       if(fm.ManageCom.value=='null'||trim(fm.ManageCom.value)==''){
         alert("管理机构不能为空！");
         return;
        }
        if(fm.AgentCode.value=='null'||trim(fm.AgentCode.value)==''){
         alert("请录入业务员编码！");
          return;
        }
        if(fm.BankCode.value=='null'||trim(fm.BankCode.value)==''){
         alert("请输入对方开户银行！");
          return;
        }
         if(fm.BankaccNo.value=='null'||trim(fm.BankaccNo.value)==''){
         alert("请输入银行帐号！");
          return;
        }
         if(fm.BankaccName.value=='null'||trim(fm.BankaccName.value)==''){
         alert("请输入银行帐户名！");
          return;
        }
         if(fm.PrtNo.value=='null'||trim(fm.PrtNo.value)==''){
         alert("印刷号不能为空！");
          return;
        }
        if(fm.PayMoney.value=='null'||trim(fm.PayMoney.value)==''||trim(fm.PayMoney.value)=='0'){
         alert("金额不能为空或者0元！");
          return;
        }
	   var tPrtNo=fm.PrtNo.value;
	    if(tPrtNo!=''){
	    var sql = "select sum(paymoney) from ljtempfee where  otherno='"+tPrtNo+"'and confmakedate is null with ur";
	    var rs = easyExecSql(sql); 
	   if(rs!='null'&&rs!=''&&rs!=null)
	    {
	      alert("印刷号为"+tPrtNo+"的数据目前已经处于银行在途状态或者待银行提盘状态，总金额为'"+rs+"'元，请确认是否需要再次录入收费数据！");
	    }
	     sql=null;
	     rs=null;
	     sql="select sum(paymoney) from ljtempfee where  otherno='"+tPrtNo+"' and confmakedate is not null and confdate is null with ur";
	     rs = easyExecSql(sql);  
	    if(rs!='null'&&rs!=''&&rs!=null)
	    {
	      alert("印刷号为"+tPrtNo+"的数据目前已成功收费'"+rs+"'元，请确认是否需要再次收费！");
	    }
  	}
     var TempRecordCount = 0;
     TempRecordCount = FeeGrid.mulLineCount; 
        if(TempRecordCount>=20){
      alert("您录入的信息超过20条，为保证数据质量，请先对已录入信息进行提交，然后再进行录入，谢谢");
     }else{
   	    FeeGrid.addOne("FeeGrid");
   	    FeeGrid.setRowColData(TempRecordCount,1,fm.PrtNo.value);
   	    FeeGrid.setRowColData(TempRecordCount,2,fm.PayMoney.value);
   	    FeeGrid.setRowColData(TempRecordCount,3,fm.BankCode.value);
   	    FeeGrid.setRowColData(TempRecordCount,4,fm.BankaccNo.value);
   	    FeeGrid.setRowColData(TempRecordCount,5,fm.BankaccName.value);
   	    FeeGrid.setRowColData(TempRecordCount,6,fm.ManageCom.value);
   	    FeeGrid.setRowColData(TempRecordCount,7,fm.AgentCode.value);
   	  //  QueryGrpGrid.setRowColData(i,5,userArray[offset][4]);
     //alert(userArray[offset][1]);
     	
    }
}




