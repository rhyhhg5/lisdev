//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var arrDataSet;

function submitForm()
{
	fm.action='./PayManageConfirmList.jsp';
	fm.submit();
}

function changePaymode()
{
 if (PolGrid.getSelNo()) 
 { 
  //fm.all("workType").value = "PAYMODE";
  //fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  //fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  var ActuGetNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 14)=="发送银行途中")
  {
  	alert("银行在途不能修改");
  	return false;
  }
  showInfo=window.open("./PayManageFrame.jsp?ActuGetNo="+ActuGetNo);
  }
 else
 {
 	  alert("请选择一条记录!");
 }
}

function changeAcc()
{
  if (PolGrid.getSelNo()) 
 {  
  fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
    
  if (fm.all("BankCode").value=="" || fm.all("AccName").value=="" || fm.all("AccNo").value=="")
  {
  	alert("请录入银行代码、账户、账号");
  	return false;
  }
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 14)=="发送银行途中")
  {
  	alert("银行在途不能修改");
  	return false;
  }  
	fm.all("workType").value = "ACC";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./PayManageConfirmSave.jsp";
	fm.submit();
 }
}

function lock()
{
  if (PolGrid.getSelNo()) 
 {  
  fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
    
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="无法锁定")
  {
  	alert("此数据无法锁定");
  	return false;
  }
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="已锁定")
  {
  	alert("此数据已经锁定");
  	return false;
  }
	fm.all("workType").value = "LOCK";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./PayManageConfirmSave.jsp";
	fm.submit();
 }
}

function unlock()
{
  if (PolGrid.getSelNo()) 
 {  
  fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="无法锁定")
  {
  	alert("此数据无法解除锁定");
  	return false;
  }
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="未锁定")
  {
  	alert("此数据未锁定");
  	return false;
  }
	fm.all("workType").value = "UNLOCK";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./PayManageConfirmSave.jsp";
	fm.submit();
 }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {   
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");     
  }
  else
  { 
  	
	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");   
 	initForm();
  }
}

function addInfo(addFlag,error)
{
	if (addFlag=="Fail")
	{
		alert("向打印队列添加数据失败!原因是："+error);
	}else
	{
		alert("成功添加进打印队列!");
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function resetFm()
{
	fm.reset();
	fm.PayMode.value="4";
	fm.PayModeName.value="银行转账";
	fm.CanSendBank.value = '1';
	fm.CanSendBankName.value = '已锁定';
	fm.ManageCom.value = '86';
	fm.ManageComName.value = '总公司';
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{	
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var strSQL1 = "";
	var strSQL2 = "";
	var strSQL3 = "";	
 if (fm.PayMode.value == '4')
 {
 strSQL = "select distinct a.actugetno,a.otherno,a.Drawer,(select codename from ldcode where codetype = 'paymode' and code = a.paymode ), "
        + "a.sumgetmoney,a.bankcode,a.bankaccno,a.accname, "
        + "case a.cansendbank  when '1' then '已锁定' else (case a.bankonthewayflag when '1' then '无法锁定' else '未锁定' end) end, "
        + "a.sendbankcount,(select d.codename from lyreturnfrombankb c, ldcode1 d where c.paycode=a.actugetno and d.codetype='bankerror' and d.code=c.bankcode and d.code1=c.banksuccflag and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.actugetno)),ManageCom,ShouldDate, "
        + "case a.bankonthewayflag when '1' then '发送银行途中' else '未发送' end ";
 }	
 else
 {
 strSQL = "select distinct a.actugetno,a.otherno,a.Drawer,(select codename from ldcode where codetype = 'paymode' and code = a.paymode ), "
        + "a.sumgetmoney,a.bankcode,a.bankaccno,a.accname, "
        + "case a.cansendbank when '1' then '已锁定' else '未锁定' end, "
        + "a.sendbankcount,(select d.codename from lyreturnfrombankb c, ldcode1 d where c.paycode=a.actugetno and d.codetype='bankerror' and d.code=c.bankcode and d.code1=c.banksuccflag and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.actugetno)),ManageCom,ShouldDate,'' ";
 }
        
 strSQL = strSQL + "from ljaget a where 1=1 and a.confdate is null and paymode <> '8' "
        + getWherePart('paymode','PayMode')
        + getWherePart('actugetno','ActuGetNo')
        + getWherePart('Otherno','CaseNo')
        + getWherePart('Drawer','Drawer');
 if(fm.CanSendBank.value == '1')
 {
 	strSQL = strSQL + " and CanSendBank = '1'";
 }
 else
 {
 	strSQL = strSQL + " and (CanSendBank = '0' Or CanSendBank is null)";
 }
  strSQL = strSQL + getWherePart( 'ManageCom','ManageCom','like' ) + " Order by ShouldDate desc ";
  
	
  fm.strSQL.value = strSQL;
  
  //查询SQL，返回结果字符串
  
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 

     
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    //alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
    
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  PolGrid.SortPage=turnPage;  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);   
}

function showBankInfo(parm1, parm2) {
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 4)=='银行转账')
  {
  fm.all("divLCPol2").style.display = "";
  fm.all("BankInfoButt").style.display = "";
	var strSql = "select a.paymode,(select codename from ldcode where codetype = 'paymode' and code = a.paymode ), "
             + "a.bankcode,(select bankname from ldbank where bankcode = a.bankcode),accname,bankaccno "
             + "from ljaget a where actugetno = '"+PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1)+"'";
          
  var arrResult = easyExecSql(strSql);

  fm.all("PayMode2").value = arrResult[0][0];
  //fm.all("PayModeName2").value = arrResult[0][1];
  fm.all("BankCode").value = arrResult[0][2];
  fm.all("BankCodeName").value = arrResult[0][3];  
  fm.all("AccName").value = arrResult[0][4];
  fm.all("AccNo").value = arrResult[0][5];
}
else
{
	 fm.all("divLCPol2").style.display = "none";
	 fm.all("BankInfoButt").style.display = "none";
}	 
    
}
