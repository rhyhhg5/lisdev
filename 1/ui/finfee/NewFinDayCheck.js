//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() && CheckDate(fm.StartDay.value) && CheckDate(fm.EndDay.value))
	{
	   if(fm.EndDay.value<fm.StartDay.value){
					alert("起始时间大于终止时间,请重新输入!");
					fm.EndDay.focus();
					return false;
	　    }	
	  if(fm.EndDay.value>fm.StartDay.value){
	 		var days = parseInt((new Date(fm.EndDay.value.replace(/-/g, "/")).getTime() - new Date(fm.StartDay.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
	 			//	alert(days);
	 		if(days>31) {
				alert("因将近年底各分公司均在做凭证核对，所以起始时间与结束时间相差不能大于一个月,请重新输入按月统计，非常感谢!");
				fm.EndDay.focus();
				return false;
			}
	　    }
	
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  //fm.hideOperate.value=mOperate;
	  //if (fm.hideOperate.value=="")
	  //{
	  //  alert("操作控制数据丢失！");
	  //}
	  //showSubmitFrame(mDebug);
	  fm.submit(); //提交
	}
}






//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

//收费打印Pay
function ORPrint()
{
	fm.action="../f1print/NewFinDayCheckSave.jsp";
	fm.target="f1print";
	fm.fmtransact.value="PRINTPAY"; //代表收费
	fm.Opt.value="Pay";//收费的类型是暂收
	submitForm();
	showInfo.close();
}
/*******************************************************************************
 * name:YSPrint
 * type:function
 * date:2003-05-28
 * author:Liuyansong
 * function :预收日结打印
 */
function YSPrint()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value = "YSPay";//收费类型是预收
  submitForm();
  showInfo.close();
}

function BQYingFu_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTGET";
  fm.Opt.value = "BQYingFu";//收费类型是预收
  submitForm();
  showInfo.close();	
	}
//付费打印Get
function OMPrint()
{
  fm.action="../f1print/NewFinDayCheckSave.jsp";
  fm.target="f1print";
  fm.fmtransact.value="PRINTGET";//代表付费
  fm.Opt.value="Get";
  submitForm();
  showInfo.close();
}



//--------------------日结单打印-------------------------

//财务接口日结以方式来打印财务收费日结单 新增
function FMPrint()
{
  fm.action="../f1print/FFChargeDayModeF1PSave.jsp?mBankFlag=NOTBANK";
  fm.target="f1print";
  fm.fmtransact.value="PRINT";
  submitForm();
  showInfo.close();
}
//	X1-实收日结单-Excel
function FMPrintX()
{
  fm.action="../f1print/FFChargeDayModeF1PExcelSave.jsp";
  fm.target="f1print";
  fm.fmtransact.value="PRINT";
  fm.Opt.value="X1SS";//操作的标志
  submitForm();
  showInfo.close();
}

//付费日结打印财务收费日结单
function printPay()
{
	fm.action="../f1print/FinPayDayF1PSave.jsp";
	fm.target="f1print";
	fm.fmtransact.value="PRINT";
	submitForm();
	showInfo.close();
}
//	X2-实付日结单-Excel
function printPayX()
{
	fm.action="../f1print/FinPayDayF1PExcelSave.jsp";
	fm.target="f1print";
	fm.fmtransact.value="PRINT";
	fm.Opt.value="X2SF";//操作的标志
	submitForm();
	showInfo.close();
}

//打印核保日结清单  X3首期续期收入日结单 新增
function HeBao_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="HeBao";//操作的标志
  submitForm();
  showInfo.close();
}
//打印核保日结清单  X3首期续期收入日结单 新增
function FenHeBao_Print()
{
  //fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.action = "../f1print/NewFinDayCheckExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="FenHeBao";//操作的标志
  submitForm();
  showInfo.close();
}

//打印保全保费日接单 X4保全日结单 新增
//function BQPrem_Print()
//{
//  fm.action = "../f1print/NewFinDayCheckSave.jsp";
//  fm.target = "f1print";
//  fm.fmtransact.value = "PRINTPAY";
//  fm.Opt.value="BQPrem";//操作的标志
//  submitForm();
//  showInfo.close();
//}
//打印保全保费日接单X4保全日结单 新增
function FenBQPrem_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="FenBQPrem";//操作的标志
  submitForm();
  showInfo.close();
}
//	X4-保全保费日结单-Excel
function FenBQPrem_PrintX()
{
  fm.action = "../f1print/NewFinDayCheckBQExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="FenBQPrem";//操作的标志
  submitForm();
  showInfo.close();
}

// X5-理赔应付日结单
function FenClaim_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="FenClaim";//操作的标志
  submitForm();
  showInfo.close();
}
//	X5-理赔应付日结单-Excel
function FenClaim_PrintX()
{
  fm.action = "../f1print/NewFinDayCheckLPYFExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="FenClaim";//操作的标志
  submitForm();
  showInfo.close();
}

// 新增加	 X6异地理赔日结单
function YDClaimSF_Print(){
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="LPYDSF";//操作的标志
  submitForm();
  showInfo.close();
}
//	X6-异地理赔日结单-Excel
function YDClaimSF_PrintX(){
  fm.action = "../f1print/NewFinDayCheckYDSFExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="LPYDSF";//操作的标志
  submitForm();
  showInfo.close();
}

// 新增加 X7银保通异地代收日结单
function YBTYDDS_Print(){
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";//注意这里
  fm.Opt.value="YBTYDDS";//操作的标志
  submitForm();
  showInfo.close();	
}
//	X7-银保通异地代付日结单-Excel
function YBTYDDS_PrintX(){
  fm.action = "../f1print/NewFinDayCheckYbtExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";//注意这里
  fm.Opt.value="YBTYDDS";//操作的标志
  submitForm();
  showInfo.close();	
}
//X9手续费日结单
function SXDDS_Print(){
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";//注意这里
  fm.Opt.value="SXFDDS";//操作的标志
  submitForm();
  showInfo.close();	
}
//	X9-手续费日结单-Excel
function SXDDS_PrintX(){
  fm.action = "../f1print/NewFinDayCheckSXFExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";//注意这里
  fm.Opt.value="SXFDDS";//操作的标志
  submitForm();
  showInfo.close();	
}
// 新增加 X8佣金月结单
function Commision_Print(){
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT"; //注意这里
  fm.Opt.value="CommisionMonth"; //操作的标志
  monthSubmitForm();
  showInfo.close();	
}
// X8-佣金月结单-Excel
function Commision_PrintX(){
  fm.action = "../f1print/CommisionMonthExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT"; //注意这里
  fm.Opt.value="CommisionMonth"; //操作的标志
  monthSubmitForm();
  showInfo.close();	
}
// 新增加 X9-再保月结单（分公司）
function ZBF_Print(){
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT"; //注意这里
  fm.Opt.value="ReinsureFilialeMonth"; //操作的标志
  monthSubmitForm();
  showInfo.close();	
}
//	X9-再保月结单（分公司）-Excel
function ZBF_PrintX(){
  fm.action = "../f1print/ReinsureMonthFilialeExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT"; //注意这里
  fm.Opt.value="ReinsureFilialeMonth"; //操作的标志
  monthSubmitForm();
  showInfo.close();	
} 
 //代付手续费
function DFSXF_Print(){
  fm.action = "../f1print/FinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";//注意这里
  fm.Opt.value="DFSXF";//操作的标志
  submitForm();
  showInfo.close();	
}
function DFSXF_PrintX(){
  fm.action = "../f1print/NewFinDayCheckDFSXFExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";//注意这里
  fm.Opt.value="DFSXF";//操作的标志
  submitForm();
  showInfo.close();	
}

 // 新增加 X10再保月结单（总公司1000）
function ZBZ_Print(){
   fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT"; //注意这里
  fm.Opt.value="ReinsureParentMonth"; //操作的标志
  
  if( document.getElementById("curManageCom").value == "86"){
     monthSubmitForm();
  showInfo.close();	
  }else{
  	  alert("只有总公司才能提,请使用总公司登陆。");
  }
}
//	X10再保月结单（总公司1000）-Excel
function ZBZ_PrintX(){
   fm.action = "../f1print/ReinsureParentMonthExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT"; //注意这里
  fm.Opt.value="ReinsureParentMonth"; //操作的标志
  
  if( document.getElementById("curManageCom").value == "86"){
     monthSubmitForm();
  showInfo.close();	
  }else{
  	  alert("只有总公司才能提,请使用总公司登陆。");
  }
}

function monthSubmitForm()
{
	if ( CheckDate( document.getElementById("StartMonth").value ) )
	{
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
	}
	else{
		fm.StartMonth.focus();
	}
}
//--------------------------------------------------------------------------------------------------------


//打印定期结算保费日接单
function DJPrem_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="DJPrem";//操作的标志
  submitForm();
  showInfo.close();		
	}
//打印定期结算收付日接单打印
function DJSF_Print()	
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="DJYF";//操作的标志
  submitForm();
  showInfo.close();			
	}
//打印异地理赔日接单
function YDClaim_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTGET";
  fm.Opt.value="YDClaim";//操作的标志
  submitForm();
  showInfo.close();			
	}
//一年期期交收费日接单打印
function PayIntvPay_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="PayIntvPay";//操作的标志
  submitForm();
  showInfo.close();				
	}	
//一年期期交保费日接单打印
function PayIntvPrem_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="PayIntvPrem";//操作的标志
  submitForm();
  showInfo.close();				
	}		
//一年期续期收费日接单打印
function PayIntvXQ_Print()
{
	}
//应收保费转保费收入日结单
function YSToPrem_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="YSToPrem";//操作的标志
  submitForm();
  showInfo.close();
}
//应收保费收费日结单
function YSPrem_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="YSPrem";//操作的标志
  submitForm();
  showInfo.close();
}


//财务应付日结清单打印函数
function YingFu_Print()
{
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="YingFu";//操作的标志
  submitForm();
  showInfo.close();
}
//
function Claim_Print()
{
	 fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="Claim";//操作的标志
  submitForm();
  showInfo.close();
}

function Card_Print()
{
	 fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="Card";//操作的标志
  submitForm();
  showInfo.close();	
}

function PayModePrint()
{
  fm.action = "../f1print/FinDayCheckModeSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTPAY";
  fm.Opt.value="PRINTPAY";//操作的标志
  submitForm();
  showInfo.close();
}

function GetModePrint()
{
  fm.action = "../f1print/FinDayCheckModeSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTGET";
  fm.Opt.value="PRINTGET";//操作的标志
  submitForm();
  showInfo.close();
}

function ActuPay_Print(){
  fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTGET";
  fm.Opt.value="ShiFu";//操作的标志
  submitForm();
  showInfo.close();	
	}
function TeXu_Print(){
	fm.action = "../f1print/NewFinDayCheckSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINTGET";
  fm.Opt.value="TeXu";//操作的标志
  submitForm();
  showInfo.close();	
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
// ----日期格式的验证------------------
function CheckDate(strDate)
{
    var reg=/^(\d{4})([-])(\d{2})([-])(\d{2})/;
    if(!reg.test(strDate))
    {
        alert("日期格式不正确!\n 正确格式为:yyyy-mm-dd");
        return false;
    }
    return true;
}
 