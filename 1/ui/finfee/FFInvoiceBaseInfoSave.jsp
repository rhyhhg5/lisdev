<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2007-05-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GB2312" %>
<%@page import="com.sinosoft.utility.*" %>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.lis.finfee.*" %>
<%@page import="java.util.List" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%
    System.out.println("start FFInvoiceBaseInfoSave ----");

    GlobalInput tGI = (GlobalInput) session.getValue("GI");

    String FlagStr = "";
    String Content = "";
    String transact = "";

    String tComCode = (String) request.getParameter("ComCode");
    String tTaxpayerNo = (String) request.getParameter("TaxpayerNo");
    String tTaxpayerName = (String) request.getParameter("TaxpayerName");
    String tInvoiceCode = (String) request.getParameter("InvoiceCode");
    String tInvoiceCodeExp = (String) request.getParameter("InvoiceCodeExp");
    String tInvoiceStartNo = (String) request.getParameter("InvoiceStartNo");
    String tInvoiceEndNo = (String) request.getParameter("InvoiceEndNo");

    LJInvoiceInfoSchema tLJInvoiceInfoSchema = new LJInvoiceInfoSchema();
    tLJInvoiceInfoSchema.setComCode(tComCode);
    tLJInvoiceInfoSchema.setInvoiceCode(tInvoiceCode);
    tLJInvoiceInfoSchema.setInvoiceCodeExp(tInvoiceCodeExp);
    tLJInvoiceInfoSchema.setInvoiceEndNo(tInvoiceEndNo);
    tLJInvoiceInfoSchema.setInvoiceStartNo(tInvoiceStartNo);
    tLJInvoiceInfoSchema.setTaxpayerName(tTaxpayerName);
    tLJInvoiceInfoSchema.setTaxpayerNo(tTaxpayerNo);

    VData tVData = new VData();
    tVData.add(tLJInvoiceInfoSchema);
    tVData.add(tGI);

    FFInvoiceBaseInfoUI tFFInvoiceBaseInfoUI = new FFInvoiceBaseInfoUI();

    try
    {
        if(!tFFInvoiceBaseInfoUI.submitData(tVData,"INSERT"))
        {
            FlagStr = "Fail";
            Content = tFFInvoiceBaseInfoUI.mErrors.getFirstError().toString();
        }
    }
    catch(Exception ex)
    {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
    }

    if (FlagStr=="")
    {
        CErrors tError = tError = tFFInvoiceBaseInfoUI.mErrors;
        if (!tError.needDealError())
        {
            Content = " 保存成功! ";
            FlagStr = "Success";
            transact = "";
            //request.setAttribute("result", tFFIvoiceExportUI.getResult());
            List result = tFFInvoiceBaseInfoUI.getResult();
            if(result.size() > 0)
            {
                for(int i=0; i<result.size(); i++)
                {
                    transact += result.get(i) + "|";
                }
            }
        }
        else
        {
            Content = " 保存失败，原因是: " + tError.getFirstError();
            FlagStr = "Fail";
        }
    }

    System.out.println("end FFInvoiceBaseInfoSave ----");
%>

<html>
<script language="javascript">
    try
    {
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
    }
    catch(e)
    {
    }
</script>
</html>