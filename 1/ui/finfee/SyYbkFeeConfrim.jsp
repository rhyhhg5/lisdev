<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：SyYbkFeeConfrim.jsp
//程序功能：医保卡结算
//创建日期：2017-11-24 11:10:36
//创建人  ：JL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="SyYbkFeeConfrim.js"></SCRIPT>
  <%@include file="SyYbkFeeConfrimInit.jsp"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <title>医保卡结算确认</title>
</head>

<script>
  var currentDate = "<%=PubFun.getCurrentDate()%>";
</script>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          
         <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class="codeNo"  name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
          </TD>
           <TD  class= title>
            是否结算
          </TD>
          <TD  class= input>
            <Input class=codeno name=ConfrimFlag verify="是否结算|NOTNULL" CodeData="0|^1|未结算^2|已结算" ondblClick="showCodeListEx('ConfrimFlag',[this,ConfrimFlagName],[0,1]);" onkeyup="showCodeListKeyEx('ConfrimFlag',[this,ConfrimFlagName],[0,1]);"><input class=codename name=ConfrimFlagName readonly=true elementtype="nacessary"></td>
		  </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            交费收据号
          </TD>
          <TD  class= input>
            <Input class= common name=TempFeeNo>
          </TD>
          
          <td  class="title">
		 	结算日期
		</td>
		<td  class= input>
		     <input class= "coolDatePicker" dateFormat="short" name="ValidateDate" verify="结算日期|NOTNULL" >
		</td>
        </TR>
        <TR  class= common>
          <TD  class= title>
        </TR>
    </table> 
    <table align=right>
      <tr>
        <td> 
          <INPUT VALUE="查询" class=cssButton TYPE=button onclick="easyQueryClick();">
        </td>
        <td> 
          <INPUT VALUE="下载" class=cssButton TYPE=button onclick="downLoad();">
          <input type=hidden class=Common name=querySql >
          <input type=hidden class=Common name=ImportPath >
        </td>
      </tr>
    </table>
    <br>
    <br>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 医保数据信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
    	<Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="getLastPage();"> 	
      </Div>			
  	</div>
  	<p>
     <table class = common>
    <TR  class= common>
      <TD class = title >
        选择导入的文件：
      </TD>
      <TD class = common width=30%>
        <Input  type="file" name=FileName size=30>
      </TD>
    </TR>
    
    <TR class = common >
      <td class = common colspan=2>
        <INPUT class=cssButton VALUE="结算批量导入" TYPE=button onclick = "submitForm();" > 
               
      </td>
    </TR>
</table>

  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
