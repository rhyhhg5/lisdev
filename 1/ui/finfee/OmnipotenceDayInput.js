//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput())
	{
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  //fm.hideOperate.value=mOperate;
	  //if (fm.hideOperate.value=="")
	  //{
	  //  alert("操作控制数据丢失！");
	  //}
	  //showSubmitFrame(mDebug);
	  fm.submit(); //提交
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//付费日结打印财务收费日结单
function printPay()
{
    var startDate = fm.startDate.value;
    var endDate = fm.endDate.value;
    var manageCom = fm.manageCom.value;
    var strSQL = ""
        + " select  "
        + " '合计', '', '', '',  "
        + " to_zero(sum(PayMoney)) PayMoney, "
        + " to_zero(sum(JJFee)) JJFee, "
        + " to_zero(sum(LXFee)) LXFee, "
        + " to_zero(sum(FirstManageFee)) FirstManageFee, "
        + " to_zero(sum(TFGetMoney)) TFGetMoney, "
        + " to_zero(sum(TBGetMoney)) TBGetMoney, "
        + " to_zero(sum(CheckFee)) CheckFee, "
        + " to_zero(sum(PolicyManageFee)) PolicyManageFee, "
        + " to_zero(sum(VentureFee)) VentureFee, "
        + " to_zero(sum(PartGetMoney)) PartGetMoney, "
        + " to_zero(sum(SCFee)) SCFee, "
        + " to_zero(sum(SWFee)) SWFee, "
        + " to_zero(sum(summoney)) summoney  "
        + " from  "
        + " ( "
        + " select  "
        + " ManageCom, "
        + " ComName, "
        + " RiskCode, "
        + " RiskName, "
        + " sum(PayMoney) PayMoney, "
        + " sum(JJFee) JJFee, "
        + " sum(LXFee) LXFee, "
        + " sum(FirstManageFee) FirstManageFee, "
        + " sum(TFGetMoney) TFGetMoney, "
        + " sum(TBGetMoney) TBGetMoney, "
        + " sum(CheckFee) CheckFee, "
        + " sum(PolicyManageFee) PolicyManageFee, "
        + " sum(VentureFee) VentureFee, "
        + " sum(PartGetMoney) PartGetMoney, "
        + " sum(SCFee) SCFee, "
        + " sum(SWFee) SWFee, "
        + " sum((PayMoney + JJFee + LXFee - FirstManageFee - TFGetMoney - TBGetMoney - CheckFee - PolicyManageFee -VentureFee - PartGetMoney - SCFee - SWFee)) as summoney "
        + "  from ( "
        + " select  "
        + "     oid.ManageCom, "
        + "     (select ldc.Name from LDCom ldc where ldc.ComCode = oid.ManageCom) as ComName, "
        + "     oid.RiskCode, "
        + "     (select LMRiskApp.RiskName from LMRiskApp where RiskCode = oid.RiskCode) as RiskName, "
        + "     PayMoney, "
        + "     JJFee, "
        + "     LXFee, "
        + "     FirstManageFee, "
        + "     TFGetMoney, "
        + "     TBGetMoney, "
        + "     CheckFee, "
        + "     PolicyManageFee, "
        + "     VentureFee, "
        + "     PartGetMoney, "
        + "     0.00 as SCFee, "
        + "     0.00 as SWFee "
        + " from OmnipotenceIntfData oid "
        + " where 1 = 1 and BussType in ('W2','W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10')  "
        + " and BussDate between '" + startDate + "' and '" + endDate + "'"
        + " and oid.ManageCOm like '" + manageCom + "%' "
        + " union all "
        + " select  "
        + "     oid.ManageCom, "
        + "     (select ldc.Name from LDCom ldc where ldc.ComCode = oid.ManageCom) as ComName, "
        + "     oid.RiskCode, "
        + "     (select LMRiskApp.RiskName from LMRiskApp where RiskCode = oid.RiskCode) as RiskName, "
        + "     PayMoney, "
        + "     JJFee, "
        + "     LXFee, "
        + "     FirstManageFee, "
        + "     TFGetMoney, "
        + "     TBGetMoney, "
        + "     CheckFee, "
        + "     PolicyManageFee, "
        + "     VentureFee, "
        + "     PartGetMoney, "
        + "     SHYLGetMoney as SCFee, "
        + "     0.00 as SWFee "
        + " from OmnipotenceIntfData oid,LJAGetClaim claim "
        + " where 1 = 1 and BussType in ('W11')  "
        + " and claim.otherno = oid.otherno "
        + " and claim.feefinatype = 'SC'  "
        + " and BussDate between '" + startDate + "' and '" + endDate + "'"
        + " and oid.ManageCOm like '" + manageCom + "%' "
        + " union all "
        + " select  "
        + "     oid.ManageCom, "
        + "     (select ldc.Name from LDCom ldc where ldc.ComCode = oid.ManageCom) as ComName, "
        + "     oid.RiskCode, "
        + "     (select LMRiskApp.RiskName from LMRiskApp where RiskCode = oid.RiskCode) as RiskName,  "
        + "     PayMoney, "
        + "     JJFee, "
        + "     LXFee, "
        + "     FirstManageFee, "
        + "     TFGetMoney, "
        + "     TBGetMoney, "
        + "     CheckFee, "
        + "     PolicyManageFee, "
				+ "     VentureFee, "
        + "     PartGetMoney, "
        + "     0.00 AS SCFee, "
        + "     SHYLGetMoney as SWFee "
        + " from OmnipotenceIntfData oid,LJAGetClaim claim "
        + " where 1 = 1 and BussType in ('W11')  "
        + " and claim.otherno = oid.otherno "
        + " and claim.feefinatype = 'SW'  "
        + " and BussDate between '" + startDate + "' and '" + endDate + "'"
        + " and oid.ManageCOm like '" + manageCom + "%' "
        + " ) as aa "
        + " group by ManageCom, "
        + " ComName, "
        + " RiskCode, "
        + " RiskName "
        + " ) as bb "
        + " union all "
        + " select  "
        + " ManageCom, "
        + " ComName, "
        + " RiskCode, "
        + " RiskName, "
        + " sum(PayMoney) PayMoney, "
        + " sum(JJFee) JJFee, "
        + " sum(LXFee) LXFee, "
        + " sum(FirstManageFee) FirstManageFee, "
        + " sum(TFGetMoney) TFGetMoney, "
        + " sum(TBGetMoney) TBGetMoney, "
        + " sum(CheckFee) CheckFee, "
        + " sum(PolicyManageFee) PolicyManageFee, "
        + " sum(VentureFee) VentureFee, "
        + " sum(PartGetMoney) PartGetMoney, "
        + " sum(SCFee) SCFee, "
        + " sum(SWFee) SWFee, "
        + " sum((PayMoney + JJFee + LXFee - FirstManageFee - TFGetMoney - TBGetMoney - CheckFee - PolicyManageFee - VentureFee - PartGetMoney - SCFee - SWFee)) as summoney "
        + "  from ( "
        + " select  "
        + "     oid.ManageCom, "
        + "     (select ldc.Name from LDCom ldc where ldc.ComCode = oid.ManageCom) as ComName, "
        + "     oid.RiskCode, "
        + "     (select LMRiskApp.RiskName from LMRiskApp where RiskCode = oid.RiskCode) as RiskName, "
        + "     PayMoney, "
        + "     JJFee, "
        + "     LXFee, "
        + "     FirstManageFee, "
        + "     TFGetMoney, "
        + "     TBGetMoney, "
        + "     CheckFee, "
        + "     PolicyManageFee, "
        + "     VentureFee, "
        + "     PartGetMoney, "
        + "     0.00 as SCFee, "
        + "     0.00 as SWFee "
        + " from OmnipotenceIntfData oid "
        + " where 1 = 1 and BussType in ('W2','W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10')  "
        + " and BussDate between '" + startDate + "' and '" + endDate + "'"
        + " and oid.ManageCOm like '" + manageCom + "%' "
        + " union all "
        + " select  "
        + "     oid.ManageCom, "
        + "     (select ldc.Name from LDCom ldc where ldc.ComCode = oid.ManageCom) as ComName, "
        + "     oid.RiskCode, "
        + "     (select LMRiskApp.RiskName from LMRiskApp where RiskCode = oid.RiskCode) as RiskName, "
        + "     PayMoney, "
        + "     JJFee, "
        + "     LXFee, "
        + "     FirstManageFee, "
        + "     TFGetMoney, "
        + "     TBGetMoney, "
        + "     CheckFee, "
        + "     PolicyManageFee, "
        + "     VentureFee, "
        + "     PartGetMoney, "
        + "     SHYLGetMoney as SCFee, "
        + "     0.00 as SWFee "
        + " from OmnipotenceIntfData oid,LJAGetClaim claim "
        + " where 1 = 1 and BussType in ('W11')  "
        + " and claim.otherno = oid.otherno "
        + " and claim.feefinatype = 'SC'  "
        + " and BussDate between '" + startDate + "' and '" + endDate + "'"
        + " and oid.ManageCOm like '" + manageCom + "%' "
        + " union all "
        + " select  "
        + "     oid.ManageCom, "
        + "     (select ldc.Name from LDCom ldc where ldc.ComCode = oid.ManageCom) as ComName, "
        + "     oid.RiskCode, "
        + "     (select LMRiskApp.RiskName from LMRiskApp where RiskCode = oid.RiskCode) as RiskName,  "
        + "     PayMoney, "
        + "     JJFee, "
        + "     LXFee, "
        + "     FirstManageFee, "
        + "     TFGetMoney, "
        + "     TBGetMoney, "
        + "     CheckFee, "
        + "     PolicyManageFee, "
        + "     VentureFee, "
        + "     PartGetMoney, "
        + "     0.00 AS SCFee, "
        + "     SHYLGetMoney as SWFee "
        + " from OmnipotenceIntfData oid,LJAGetClaim claim "
        + " where 1 = 1 and BussType in ('W11')  "
        + " and claim.otherno = oid.otherno "
        + " and claim.feefinatype = 'SW'  "
        + " and BussDate between '" + startDate + "' and '" + endDate + "'"
        + " and oid.ManageCOm like '" + manageCom + "%' "
        + " ) as aa "
        + " group by ManageCom, "
        + " ComName, "
        + " RiskCode, "
        + " RiskName "
        + " with ur "
        ;
        
	fm.querySql.value = strSQL;
	fm.action="./OmnipotenceDayPrint.jsp";
	
	fm.target="f1print";
	fm.fmtransact.value="PRINT";
	submitForm();
	showInfo.close();
}



//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}





//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

