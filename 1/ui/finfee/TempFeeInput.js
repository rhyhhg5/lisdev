 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();

//存放添加动作执行的次数
var addAction = 0;
//暂交费总金额
var sumTempFee = 0.0;
//暂交费信息中交费金额累计
var tempFee = 0.0;
//暂交费分类信息中交费金额累计
var tempClassFee = 0.0;
//单击确定后，该变量置为真，单击添加一笔时，检验该值是否为真，为真继续，然后再将该变量置假
var confirmFlag=false;
//
var arrCardRisk;

var todayDate = new Date();
var sdate = todayDate.getDate();
var smonth= todayDate.getMonth() +1;
var syear= todayDate.getYear();
var sToDate = syear + "-" +smonth+"-"+sdate
window.onfocus=myonfocus;
//提交，保存按钮对应操作
function submitForm()
{
 
 if(fm.all('TempFeeType').value=="2" )
  {
  	  fm.all("divErrorMes").innerHTML = "";
	  var strSQL = "";

	  strSQL="select OtherNoType from LJSPay where GetNoticeNo='"+fm.InputNo4.value+"'";

	  var arrResult = easyExecSql(strSQL);
	  if (arrResult==null)
	  {
	  	var notice ="";
	  	var resultData = checkNotice(fm.InputNo4.value);
	  	
	  	if(resultData==null)
	  	{
	  		
	  		var rsSQL = "select (select codename from ldcode where codetype ='feecancelreason' and code = cancelreason) from ljspayb where Getnoticeno = '"+fm.InputNo4.value+"'";
	  		var rs = easyExecSql(rsSQL);
	  		if(rs==null)
	  		{
	  			alert("交费通知书号码有误！");
	  		}
	  		else
	  		{
	  			notice =fm.InputNo4.value+"号通知书已经因"+rs+"作废。";
	  			alert(notice);
	  		}
	  	}
			else
	  	{
	  		if(resultData[0][0]=='0')
	  		{
	  			notice = resultData[0][1]+"号通知书已经因"+resultData[0][2]+"作废，新交费通知号码为"+resultData[0][3]+"。";
	  			alert(notice);
				}
	  		if(resultData[0][0]=='1')
	  		{
	  			notice = resultData[0][1]+"号通知书已经因"+resultData[0][2]+"作废，该次期交保费已经由"+resultData[0][3]+"号通知书交费。";
	  			alert(notice);
	  		}
	  	}
	  	return false;
	  }
	  
  }

	
 if(addAction>0)//如果提交数据活动存在
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmSave.submit(); //提交

 }
  else alert("条件不满足");
  window.focus;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    var prtNo = TempToGrid.getRowColData(0, 10);
    //如果是卡折业务，缴费后进行签单
    var SQL = "select a.GrpContNo, b.MissionID, b.SubMissionID "
        + "from LCGrpCont a, LWMission b "
        + "where a.PrtNo = '" + prtNo + "' and CardFlag = '2' "
        + "and b.ProcessID = '0000000011' and b.ActivityID = '0000011002'"
        + "and b.MissionProp2 = a.PrtNo with ur";
    var arr = easyExecSql(SQL);
	if(arr)
	{
	    fmCertify.GrpContNo.value = arr[0][0];
	    fmCertify.MissionID.value = arr[0][1];
	    fmCertify.SubMissionID.value = arr[0][2];
        showStr="正在进行卡单结算，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	    fmCertify.submit();
	}
  }
  clearFormData();
}
function PayQueryClick(){   

 	window.open("./BatchPayQueryMain.jsp");

}
function showFinance(cashValue1,chequeValue1)
{

	showInfo.close();
	var urlStr="./FinanceInfo.jsp?cashValue="+cashValue1+"&chequeValue="+chequeValue1 ;
	window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:1000px;dialogHeight:400px");
}

//提交后操作,服务器数据返回后执行的操作
function afterQuery( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    //var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//关于管理机构的改造
function getbankaccno3(row,obj){ 
	var man=fm.ManageCom.value;
	var yinghang=document.all(row).all('TempClassGrid10').value;
	document.all(row).all('TempClassGrid11').value='';
	var strsqls="   ManageCom=#"+man+"# "+" and BankCode=#"+yinghang+"#";
	
	showCodeList('getbankaccno3',[document.all(row).all('TempClassGrid11'),document.all(row).all('banknames')],[0,1],null,strsqls,"bankcode");
	}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  	    window.open("./TempFeeQuery.html");
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function queryTempfee()
{
	window.open("./TempFeeQuery.html");
}

//验证字段的值
function confirm()
{      
	var typeFlag=false;
	if(fm.all('TempFeeType').value == "6"){
		if( fm.all('InputNo12').value == ""){
			alert("缴费类型是定额单证,但没有录入卡单类型和结算批次号!");
			return false;
		}
		var arr = easyExecSql("select distinct handlercode,agentcom from lzcardpay where payno='"+fm.all('InputNo12').value+"'");
		if(arr){
			if(!arr[0][0] == ""){
				fm.ManageCom.value = easyExecSql("select ManageCom from laagent where agentcode='"+arr[0][0]+"'");
				fm.AgentCode.value = arr[0][0];
			}else{
				fm.ManageCom.value = easyExecSql("select ManageCom from lacom where agentcom='"+arr[0][1]+"'");
			}
		}else{
			alert("此结算批次号码不存在!");
			return false;
		}
		var arr = easyExecSql("select '1' from ljtempfee where tempfeetype='6' and not exists (select '1' from ljagettempfee where tempfeeno =ljtempfee.tempfeeno) and otherno='"+fm.all('InputNo12').value+"' with ur");
		if(arr){
			alert("此结算批次号已经结算!");
			return false;
		}
		var arr1 = easyExecSql("select '1' from ljtempfee where tempfeeno='"+fm.all('InputNo18').value+"' with ur");
		if(arr1){
			alert("暂收收据号已经存在，请换其他收据号!");
			return false;
		}
	}
	
  //首先检验录入框
  if (!verifyInput()) return false;
  if((fm.all('ManageCom').value).length<8)
  {
  	alert("管理机构必须是8位");
    return false;
  }
  
  if(fm.all('TempFeeType').value=="2" )
  {
  	  fm.all("divErrorMes").innerHTML = "";
	  var strSQL = "";

	  strSQL="select OtherNoType from LJSPay where GetNoticeNo='"+fm.InputNo4.value+"'";

	  var arrResult = easyExecSql(strSQL);
	  if (arrResult==null)
	  {
	  	var notice ="";
	  	var resultData = checkNotice(fm.InputNo4.value);
	  	
	  	if(resultData==null)
	  	{
	  		
	  		var rsSQL = "select (select codename from ldcode where codetype ='feecancelreason' and code = cancelreason) from ljspayb where Getnoticeno = '"+fm.InputNo4.value+"'";
	  		var rs = easyExecSql(rsSQL);
	  		if(rs==null)
	  		{
	  			alert("交费通知书号码有误！");
	  		}
	  		else
	  		{
	  			notice =fm.InputNo4.value+"号通知书已经因"+rs+"作废。";
	  			alert(notice);
	  		}
	  	}
			else
	  	{
	  		if(resultData[0][0]=='0')
	  		{
	  			notice = resultData[0][1]+"号通知书已经因"+resultData[0][2]+"作废，新交费通知号码为"+resultData[0][3]+"。";
	  			alert(notice);
				}
	  		if(resultData[0][0]=='1')
	  		{
	  			notice = resultData[0][1]+"号通知书已经因"+resultData[0][2]+"作废，该次期交保费已经由"+resultData[0][3]+"号通知书交费。";
	  			alert(notice);
	  		}
	  	}
	  	return false;
	  }
	  
  }

   if (fm.all('TempFeeType').value=="1" || fm.all('TempFeeType').value=="3"||fm.all('TempFeeType').value=="11"||fm.all('TempFeeType').value=="20")
  {
    if (fm.all('AgentCode').value=='')
    {
    	alert("代理人不能为空!");
    	return false;
    }
  }

  //团体新单交费，录入暂交费号
  if (fm.all('TempFeeType').value=="1")
  {
  	var sql=" select '1' from lcgrpcont where prtno='" + fm.all('InputNo1').value + "' and uwflag in ('9', '4') with ur"
    var tarrResult = easyExecSql(sql);
    if(!tarrResult){
    	alert('保单不存在或者没有核保通过'); 
      return false ; 
    }
  	 //add by yingxl begin
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('InputNo1').value + "' "
              + " and confdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode in ('1','11','10','13','3','2','12','6','5'))";
    var arrResult = easyExecSql(sql);
    if(arrResult)
    {
        alert('已经收过费，缴费凭证号是：'+arrResult[0][0]); 
        return false ; //存在
    }
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('InputNo1').value + "' "
              + " and confdate is  null and enteraccdate is not null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode ='4')";

    var arrResult1 = easyExecSql(sql);
    if(arrResult1)
    {
        alert('已经收过费，缴费凭证号是：'+arrResult1[0][0]); 
        return false ; //存在
    }
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('InputNo1').value + "' "
              + " and confdate is  null and enteraccdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode ='4')";

    var arrResult2 = easyExecSql(sql);
    if(arrResult2)
    {
        alert('该保单缴费方式是银行转帐，缴费凭证号是：'+arrResult2[0][0]); 
        return false ; //存在
    }
    //end   modify  yingxl   again date 2008-04-07
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("投保单印刷号码|NOTNULL", fm.all('InputNo1').value) != true) {
      return false;
    }
    if (verifyElement("暂交费收据号|NOTNULL&len>=11", fm.all('InputNo2').value) != true) {
      return false;
    }

    //repair:类型1：新单交费输入的是印刷号，不验证格式
    //验证格式处理，需要按照中英模式修改
    //校验单证发放
    if (!verifyTempfeeNo(fm.all('InputNo2').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo1').value)) return false;

    fm.all('TempFeeNo').value = fm.all('InputNo2').value;
    typeFlag=true;

    if(verifyAgent1(fm.all('InputNo1').value)==false)
    {
    	return false;
    }
    if(fm.all('InputNoName').value=="1")
    {
  //  	    alert(fm.all('InputNo1').value);
   fillgrid(fm.all('InputNo1').value);
   TempGrid.unLock();
   TempClassGrid.unLock();
    }
    else {
   TempGrid.unLock();
   TempGrid.addOne("TempGrid");
   TempClassGrid.unLock();
   TempClassGrid.addOne("TempClassGrid");
  }
  }

  //续期催收交费，录入暂交费号
  if(fm.all('TempFeeType').value=="2")
  {
    //校验录入
    if (verifyElement("保单号码|NOTNULL", fm.all('InputNo3').value) != true) {
      return false;
    }
    if (verifyElement("交费通知书号码|NOTNULL", fm.all('InputNo4').value) != true) {
      return false;
    }
    //校验录入代理人是否和保单代理人一致
    //if(verifyAgent(fm.all('InputNo3').value)==false)
    //{
    //	return false;
    //}
    
    var sql = "select a.edorAcceptNo "
              + "from LPEdorApp a, LPEdorItem b "
              + "where a.edorAcceptNo = b.edorNo "
              + "   and b.contNo = '" + fm.all('InputNo3').value + "' "
              + "   and a.edorState not in('0')";
    var rs = easyExecSql(sql);
    if(rs)
    {
      alert("保单正在做保全，不能收费");
      return false;
    }
    
    var sql = "select a.edorAcceptNo "
              + "from LPEdorApp a, LPGrpEdorItem b "
              + "where a.edorAcceptNo = b.edorNo "
              + "   and b.grpContNo = '" + fm.all('InputNo3').value + "' "
              + "   and a.edorState not in('0')";
    var rs = easyExecSql(sql);
    if(rs)
    {
      alert("保单正在做保全，不能收费");
      return false;
    }
    
	 /**
		 * 约定缴费财务收付配合修改 wdd
		 * cansendbank字段置为Y。对这样的数据不进行收费
		 */
    
		var sql = "select '1' from ljspay  where otherno = '"+fm.all('InputNo3').value+"' and getnoticeno = '"+fm.InputNo4.value+"' and cansendbank = 'Y' with ur";
		var arr2 = easyExecSql(sql);
		if(arr2){
			alert("该笔收费处理约定缴费的待审批状态，不能进行付费操作！");
			return false;
		}

    //类型2：续期催收交费，输入交费收据号(31)或者保单号(21,22)
    //验证格式：提交查询时
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //initTempGrid();
    initTempGridReadOnly();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");
    fm.submit();//后台可以根据暂交费类型判断
    var strSql="select bankcode from lccont where contno='" + fm.all('InputNo3').value + "' "
					+"Union select bankcode from lcgrpcont where grpcontno='" + fm.all('InputNo3').value + "' ";
    var arrResult = easyExecSql(strSql);
    if(TempClassGrid.mulLineCount==0){
       TempClassGrid.unLock();
       TempClassGrid.addOne("TempClassGrid");
    }
    if(arrResult!=null && arrResult.length>0){
    	if(TempClassGrid.mulLineCount>0){
    		TempClassGrid.setRowColData(0,7,arrResult[0][0]);
    	}
    } 
    
    typeFlag=true;
  }

  //续期非催收交费，生成暂交费号
  if(fm.all('TempFeeType').value=="3")
  {
    //repair:判断号码类型
    //类型3：续期无催收交费：录入保单号（21，22）验证格式
    //验证格式：包括合同号
    initTempGrid();
    //initTempGridReadOnly();
    initTempClassGrid();

    //校验录入
    if (verifyElement("保单号码|NOTNULL&len=18|len=20", fm.all('InputNo5').value) != true) {
      return false;
    }

    //校验录入代理人是否和保单代理人一致
    //if(verifyAgent(fm.all('InputNo5').value)==false)
    //{
    //	return false;
    //}

    if(getCodeType(fm.all('InputNo5').value)=="21") //个人保单
    {
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      fmTypeQuery.all('QueryNo').value = fm.all('InputNo5').value;
      fmTypeQuery.all('QueryType').value = "1";
      fmTypeQuery.submit();//去后台查询该号码是否合格

    }
    else if(getCodeType(fm.all('InputNo5').value)=="22") //集体保单
    {
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      fmTypeQuery.all('QueryNo').value = fm.all('InputNo5').value;
      fmTypeQuery.all('QueryType').value = "2";
      fmTypeQuery.submit();//去后台查询该号码是否合格
    }
    else
    {
      alert("号码类型不符合!请检查");
      return ;
    }

    typeFlag=true;
  }

  //保全交费，录入交费收据号
  if(fm.all('TempFeeType').value=="4")
  {
    //校验录入
    if (verifyElement("批单号码|NOTNULL", fm.all('InputNo7').value) != true) {
      return false;
    }
    if (verifyElement("交费收据号码|NOTNULL&len>=11", fm.all('InputNo8').value) != true) {
      return false;
    }

    //类型4：保全交费，录入交费收据号（31）验证格式
    //验证格式：提交查询时
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    initTempGridReadOnly();
    initTempClassGrid();
    fm.submit();//后台可以根据暂交费类型判断
		var tSql ="select othernotype from ljspay where otherno='"+fm.all('InputNo7').value+"'";
    var tResult = easyExecSql(tSql, 1, 1, 1);
    var tNo="";
    if(tResult[0][0]=="13"){
    	tSql ="select grpcontno,contno from ljagetendorse where actugetno in( "
					+"select Btactuno from ljaedorbaldetail where actuno in "
					+"(select getnoticeno from ljspay where othernotype='13' and otherno='"+fm.all('InputNo7').value+"')) ";
    	tResult = easyExecSql(tSql, 1, 1, 1);
    	if(tResult!=null && tResult.length>0){
    		var tGrpContNo = tResult[0][0];
    		var tContNo = tResult[0][1];
    		if(tGrpContNo=="00000000000000000000" || tGrpContNo==""){
    			tNo=tContNo;
    		}else{
    			tNo=tGrpContNo;
    		}
    	}
    }else{
    	tSql = "select grpcontno,contno from ljsgetendorse where endorsementno='"+fm.all('InputNo7').value+"' "+
    				" Union select grpcontno,contno from ljagetendorse where endorsementno='"+fm.all('InputNo7').value+"' ";
      tResult = easyExecSql(tSql, 1, 1, 1);
      if(tResult!=null && tResult.length>0){
      	var tGrpContNo = tResult[0][0];
      	var tContNo = tResult[0][1];
      	if(tGrpContNo=="00000000000000000000" || tGrpContNo==""){
    		 tNo=tContNo;
      	}else{
    		 tNo=tGrpContNo;
      	}
    	}
    }   
    var strSql="select bankcode from lccont where contno='" + tNo + "' "
					+"Union select bankcode from lcgrpcont where grpcontno='" + tNo + "' "
					+"Union select bankcode from lbcont where contno='" + tNo + "' "
					+"Union select bankcode from lbgrpcont where grpcontno='" + tNo + "' ";
    var arrResult = easyExecSql(strSql);
    if(arrResult!=null && arrResult.length>0){
    	if(TempClassGrid.mulLineCount>0){
    		TempClassGrid.setRowColData(0,7,arrResult[0][0]);
    	}
    }
    typeFlag=true;
  }

   //银行扣款，录入印刷号
   if(fm.all('TempFeeType').value=="5")
   {

    initTempGrid();
    initTempClassGridType5();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("投保单印刷号码|NOTNULL", fm.all('InputNo9').value) != true) {
      return false;
    }
    if (verifyElement("投保单印刷号码|NOTNULL", fm.all('InputNo10').value) != true) {
      return false;
    }
    if (fm.all('InputNo9').value != fm.all('InputNo10').value) {
      alert("印刷号前后输入不一致，请确认！");
      return false;
    }

    //校验单证发放
    if (!verifyPrtNo(fm.all('InputNo9').value)) return false;

    //repair:类型1：新单交费输入的是印刷号，不验证格式
    fm.all('TempFeeNo').value = fm.all('InputNo10').value;
    typeFlag = true;
    TempGrid.unLock();
    TempGrid.addOne("TempGrid");
    //TempClassGrid.unLock();
    TempClassGrid.addOne("TempClassGrid");
   }

   //定额单交费，录入单证编码，单证号码
   if(fm.all('TempFeeType').value=="6")
   {
    initTempGridReadOnly();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    //if (verifyElement("单证编码|NOTNULL&code:CardCode", fm.all('InputNo11').value) != true) {
    //  return false;
    //}
    if (verifyElement("单证号码|NOTNULL", fm.all('InputNo12').value) != true) {
      return false;
    }

 //   if(needVerifyCertifyCode(fm.all('InputNo11').value)==true)
 //   {
 // 
 // 
	//  //校验单证发放
	//  //去单证状态表里查询该号码是否有效,暂交费收据号
	//  var strSql = "select CertifyCode from LZCardTrack where Startno<='"+fm.all('InputNo12').value+"' and Endno>='"+fm.all('InputNo12').value+"' and Receivecom = 'D"+fm.all('AgentCode').value+"' and StateFlag='0' and CertifyCode='" + fm.all('InputNo11').value + "'";
	//  strSql = strSql +" and length(trim(Startno))=length(trim('"+fm.all('InputNo12').value+"')) and length(trim(Endno))=length(trim('"+fm.all('InputNo12').value+"'))";
 // 
	//  var strResult=easyQueryVer3(strSql, 1, 1, 1);
 // 
	//  if(!strResult)
	//  {
	//    alert("该单证（单证编码为："+fm.all('InputNo12').value+" ）没有发放给该代理人（"+fm.all('AgentCode').value+"）!");
	//    return false;
	//  }
 // 
 // }
  
  //显示该定额单的险种信息
  //  strSql = "select a.RiskCode, b.RiskName, (select sum(actpaymoney) from lzcardpay where payno='"+fm.all('InputNo12').value+"') , a.PremProp, a.Premlot from LMCardRisk a, LMRisk b where a.riskcode=b.riskcode and CertifyCode = '" + fm.all('InputNo11').value + "'";
  //  arrCardRisk = easyExecSql(strSql);                                                                                                                                                                                                                                   
  
  //if (strResult==null || strResult!="D") {
  //  alert("该单证编码不是定额单");
  //  return false;
  //}

    strSql = "select a.RiskCode, b.RiskName, (select sum(actpaymoney) from lzcardpay where payno='"+fm.all('InputNo12').value+"') , a.PremProp, a.Premlot from LMCardRisk a, LMRisk b where a.riskcode=b.riskcode and CertifyCode in ( select a.CertifyCode from lzcard a,lzcardpay b where a.Startno=b.startno and a.Endno=b.Endno and a.subcode=b.cardtype and b.payno='" + fm.all('InputNo12').value + "')";
    arrCardRisk = easyExecSql(strSql);

    fm.all('TempFeeNo').value = fm.all('InputNo18').value;
    typeFlag = true;

    if (arrCardRisk==null) return;

    if (arrCardRisk[0][3] == "1") {
      initTempGridReadOnly();
    }
    else {
      initTempGrid();
    }
		var sumPay = 0;
    for (k=0; k<arrCardRisk.length; k++) {
      TempGrid.addOne("TempGrid");

      TempGrid.setRowColData(k,1,arrCardRisk[k][0]);
      TempGrid.setRowColData(k,2,arrCardRisk[k][1]);
      TempGrid.setRowColData(k,3,arrCardRisk[k][2]);          
      sumPay = sumPay = arrCardRisk[k][2];
      //TempGrid.setRowColData(k,4,fm.all('InputNo11').value);
    }

    //TempClassGrid.unLock();
    TempClassGrid.addOne("TempClassGrid");
    TempClassGrid.setRowColData(0,1,'1');
    TempClassGrid.setRowColData(0,2,'现金');
    TempClassGrid.setRowColData(0,3,sumPay);
   }
    //保全交费，录入交费收据号
  if(fm.all('TempFeeType').value=="7")
  {
    //校验录入
    if (verifyElement("保全受理号|NOTNULL", fm.all('InputNo13').value) != true) {
      return false;
    }
    if (verifyElement("交费收据号码|NOTNULL&len>=11", fm.all('InputNo14').value) != true) {
      return false;
    }

    //类型4：保全交费，录入交费收据号（31）验证格式
    //验证格式：提交查询时
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    initTempGridReadOnly();
    initTempClassGrid();
    fm.submit();//后台可以根据暂交费类型判断

    typeFlag=true;
  }

  //预打保单交费，录入暂交费号
  if (fm.all('TempFeeType').value=="8")
  {
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("投保单印刷号码|NOTNULL", fm.all('InputNo15').value) != true) {
      return false;
    }
    if (verifyElement("暂交费收据号|NOTNULL&len>=11", fm.all('InputNo16').value) != true) {
      return false;
    }

    //repair:类型1：新单交费输入的是印刷号，不验证格式
    //验证格式处理，需要按照中英模式修改
    //校验单证发放
    if (!verifyTempfeeNo(fm.all('InputNo16').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo15').value)) return false;

    fm.all('TempFeeNo').value = fm.all('InputNo16').value;
    typeFlag=true;

    TempGrid.unLock();
    TempGrid.addOne("TempGrid");
    TempClassGrid.unLock();
    TempClassGrid.addOne("TempClassGrid");

  }
   //理赔预付赔款回收
  if (fm.all('TempFeeType').value=="9")
  {   
    //校验录入
    if (verifyElement("预付赔款号|NOTNULL", fm.all('InputNo24').value) != true) {
      return false;
    }
    if (verifyElement("交费收据号|NOTNULL&len>=11", fm.all('InputNo25').value) != true) {
      return false;
    }
     
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	initTempGridReadOnly();
    initTempClassGrid();
    fm.submit();
    
    var tSql = "select bankcode from llprepaidclaim where prepaidno ='"+fm.all('InputNo24').value+"' ";
    var tResult = easyExecSql(tSql);
    if(tResult){ 
    if(TempClassGrid.mulLineCount>0){
	    TempClassGrid.setRowColData(0,7,tResult[0][0]);
    	}   	    	
   	}

    typeFlag=true;

  }
    //个人新单交费，录入暂交费号
  if (fm.all('TempFeeType').value=="11")
  {
    var sql = "select '1' from lccont where prtno='" + fm.all('InputNo22').value + "' and signdate is not null with ur"
    var arrResult = easyExecSql(sql);
   if(arrResult)
    {
        alert('已经签单，不能进行收费操作'); 
        return false ; //存在
   }
  	
 
    sql="select '1' from ljtempfee where tempfeeno='" + fm.all('InputNo23').value + "' with ur"
    var tarrResult = easyExecSql(sql);
    if(tarrResult){
    	alert('该暂收号码已经进行收费！'); 
    }
    
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('InputNo22').value + "' "
              + " and confdate is  null and enteraccdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode ='4')";

    var arrResult2 = easyExecSql(sql);
    if(arrResult2)
    {
        alert('该保单缴费方式是银行转帐，缴费凭证号是：'+arrResult2[0][0]); 
        return false ; //存在
    }
     flag=2;
     initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("投保单印刷号码|NOTNULL", fm.all('InputNo22').value) != true) {
      return false;
    }
    if (verifyElement("暂交费收据号|NOTNULL&len>=11", fm.all('InputNo23').value) != true) {
      return false;
    }

    //repair:类型1：新单交费输入的是印刷号，不验证格式
    //验证格式处理，需要按照中英模式修改
    //校验单证发放
    if (!verifyTempfeeNo(fm.all('InputNo23').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo22').value)) return false;

    fm.all('TempFeeNo').value = fm.all('InputNo23').value;
    typeFlag=true;
    
    sql="select '1' from lccont where prtno='" + fm.all('InputNo22').value + "' with ur"
    var tarrResult = easyExecSql(sql);
    if(!tarrResult){
    	alert('个人保单还未进行录单,该次收费为先收费！'); 
    }

    if(fm.all('InputNoName2').value=="1"&&tarrResult)
    { 	    
   fillgrid(fm.all('InputNo22').value);
   TempGrid.unLock();
   TempClassGrid.unLock();
    }
    else {
   TempGrid.unLock();
    flag=0;
   TempGrid.addOne("TempGrid");
   TempClassGrid.unLock();
   TempClassGrid.addOne("TempClassGrid");
  }
  }
  	   //健管交费，录入暂交费号
  if (fm.all('TempFeeType').value=="20")
  {    
      flag=0;
     initTempGrid();
     if(tempfeeType20()){
     typeFlag=true;
      }
   }  
	if (fm.all('TempFeeType').value=="10")
  {
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("批次号|NOTNULL", fm.all('InputNo17').value) != true) {
      return false;
    }
    //repair:类型1：新单交费输入的是印刷号，不验证格式
    //验证格式处理，需要按照中英模式修改
    //校验单证发放
    if (!verifyTempfeeNo(fm.all('InputNo2').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo1').value)) return false;

    fm.all('TempFeeNo').value = fm.all('InputNo17').value;
    typeFlag=true;

    //if(verifyAgent1(fm.all('InputNo1').value)==false)
    //{
    //	return false;
    //}
		if(!fillDuePay(fm.all('InputNo17').value)){
			return false;
		}
	   TempGrid.unLock();
	   TempClassGrid.unLock();
  }
  
  
  
  	   //社保报销交费，录入暂交费号
  if (fm.all('TempFeeType').value=="25")
  {    
      flag=0;
     initTempGrid();
     if(tempfeeType24()){
     typeFlag=true;
      }
   }  
//健康管理服务收费
if (fm.all('TempFeeType').value=="30")
  {
    sql="select '1' from ljtempfee where tempfeeno='" + fm.all('InputNo32').value + "' with ur"
    var tarrResult = easyExecSql(sql);
    if(tarrResult){
        alert('该暂收号码已经进行收费！'); 
    }
    
    
    
    flag=2;
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");
    
    if (!verifyTempfeeNo(fm.all('InputNo32').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo31').value)) return false;
    
    sql="select sumduepaymoney from ljspay where getnoticeno='" + fm.all('InputNo32').value + "' with ur"
    var tResult = easyExecSql(sql);
    
    //TempGrid.unLock();
    TempGrid.addOne("TempGrid");
    TempGrid.setRowColData(0,3,tResult[0][0]);
    TempClassGrid.unLock();
    TempClassGrid.addOne("TempClassGrid");
    flag=0;
   
    fm.all('TempFeeNo').value = fm.all('InputNo32').value;
    typeFlag=true;
  }
	if (fm.all('TempFeeType').value=="10")
  {
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("批次号|NOTNULL", fm.all('InputNo17').value) != true) {
      return false;
    }
    //repair:类型1：新单交费输入的是印刷号，不验证格式
    //验证格式处理，需要按照中英模式修改
    //校验单证发放
    if (!verifyTempfeeNo(fm.all('InputNo2').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo1').value)) return false;

    fm.all('TempFeeNo').value = fm.all('InputNo17').value;
    typeFlag=true;

    //if(verifyAgent1(fm.all('InputNo1').value)==false)
    //{
    //	return false;
    //}
		if(!fillDuePay(fm.all('InputNo17').value)){
			return false;
		}
	   TempGrid.unLock();
	   TempClassGrid.unLock();
  }
  
  
  
   if(typeFlag==true)
     confirmFlag=true;
   else
     confirmFlag=false;
}


//添加一笔纪录
function addRecord()
{//添加管理机构校验
    if((fm.all('ManageCom').value).length<8)
   {
  	alert("管理机构不能为空,且管理机构必须是8位");
    return false;
   }
   if(confirmFlag==false){alert("请单击确定");return;}//如果没有单击确定，提示

   if(fm.all('TempFeeType').value=="1" && fm.all('InputNo1').value == "")
   {
   	 alert("您没有输入投保单印刷号码，请输入后再次击确认按钮！");
   	 return false;
   }
   
   if(fm.all('TempFeeType').value=="1" && fm.all('TempFeeNo').value!=fm.all('InputNo2').value)
   {
     alert("您更改了录入号码，请再次点击确认按钮！");
     return false;
   }
   
   if(fm.all('TempFeeType').value=="2" && fm.all('TempFeeNo').value!=fm.all('InputNo4').value)
   {
   	 alert("您更改了录入号码，请再次点击确认按钮！");
   	 return false;
   }
    if(fm.all('TempFeeType').value=="6" && fm.all('TempFeeNo').value!=fm.all('InputNo18').value)
   {
   	 alert("您更改了录入号码，请再次点击确认按钮！");
   	 return false;
   }   
    
    //由于北分银行太多 把校验挪到了js里
    var bankcount = TempClassGrid.mulLineCount;
    var manage = fm.all('ManageCom').value;
    manage = manage.substring(0,4);
	for(var bankRow = 0; bankRow < bankcount; bankRow++){
		var bank = TempClassGrid.getRowColData(bankRow,7);
		var mode = TempClassGrid.getRowColData(bankRow,1);
		if(mode != 1 && bank != ""){
			var bankSql = "select 1 from LDBank"
                + " where (BankUniteFlag is null or BankUniteFlag<>'1') and "
                + " Comcode like '" + manage + "%' and bankcode='" + bank
                + "' order by BankCode";
			var bankRs = easyExecSql(bankSql);
			if(bankRs == null) {
				alert("保单信息中对方开户银行编码：" + bank + "不存在！请核查");
				return false;
			}
		}
	}

  if(!checkGridValue()) return; //检查MulLine的信息域的数据格式是否合格,TempClass是否有重复险种

	
  if(!checkTempRecord()) return; //检查添加的暂交费信息数据是否已经存在于保存数据的Grid中，并计算交费金额累计
   if(!checkTempClassRecord()) return; //计算添加的暂交费分类信息数据交费金额累计s
	 if(!checkDate()) return false;
	 
   tempClassFee=floatRound((tempClassFee/10000),5);
   tempFee=floatRound((tempFee/10000),5);
   
    if(fm.all('TempFeeType').value=="30")
       {
          strSql = "select 1 from ljspay where otherno='" + fm.all('InputNo31').value + "' and sumduepaymoney=" + tempFee;
          var tarrResult = easyExecSql(strSql);
          if(tarrResult == null){
               alert("录入的金额与应收金额不等");
               return false;
          }     
       }      
   if(tempClassFee==tempFee && tempFee!='')
   {
      addAction = addAction+1; //存放添加动作执行的次数
      sumTempFee = sumTempFee+tempFee;
      fm.all('TempFeeCount').value = addAction;
      fm.all('SumTempFee').value = sumTempFee;
      tempClassFee=0.0; //出现错误时,将上下Fee清0
      tempFee=0.0;
   }
   else
   {
      tempClassFee=0.0;
      tempFee=0.0;
      alert("金额不等或缺少数据！");
      return ;
   }
    //个单新单核保结论为“撤件”“延期”“拒保”，不能进行收费
	if(fm.all('TempFeeType').value == "11")
	{	  
		var sql=" select '1' from lccont where prtno='" + fm.all('InputNo22').value + "'  with ur";
		var tarrResult = easyExecSql(sql);
		if(tarrResult)
		{	
			var tSql = "select codename from ldcode where codetype='uwflag' and code in (select uwflag  from lccont where prtno ='"+ fm.all('InputNo22').value+"' and uwflag in ('1','8','a')) with ur ";
			var rResult = easyExecSql(tSql);
			if(rResult)
			{
				alert("该保单核保状态为：" + rResult[0][0]+ ",无法进行收费操作！"); 
				return false ;
			}
       		  
		}
	}
    
  divTempFeeSave.style.display="";

  var i = 0;
  var TempRecordCount = 0;
  var TempClassRecordCount = 0;
  var EnterAccDate=""; //到帐日期
  var EnterAccDateFlag=1; //是否录入到帐日期的标记(如果交费项有多条，并且都填写了到帐日期，那么有效)
  TempRecordCount = TempGrid.mulLineCount; //TempGrid的行数
  TempClassRecordCount = TempClassGrid.mulLineCount; //页面上纪录的行数


 //暂交费分类信息赋给用于提交数据的Grid
 for(i=0;i<TempClassRecordCount;i++)
 {
    if (fm.all('TempFeeType').value=="2" || fm.all('TempFeeType').value=="3" || fm.all('TempFeeType').value=="4") {
      if(TempClassGrid.getRowColData(i,1)=="4") {
        alert("该种暂交费类型（" + fm.all('TempFeeType').value + "）不允许使用银行转账的交费方式！");
        return false;
      }
    }

    if(TempClassGrid.getRowColData(i,1)!='')  //如果交费方式为空，则跳过
    {
     if(i==0)
       EnterAccDate=TempClassGrid.getRowColData(i,5);//将到帐日期赋第一行的到帐日期

     TempClassToGrid.addOne("TempClassToGrid");
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,1,fm.all('TempFeeNo').value);
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,2,TempClassGrid.getRowColData(i,1));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,3,fm.all('PayDate').value);
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,4,TempClassGrid.getRowColData(i,3));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,5,TempClassGrid.getRowColData(i,6));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,6,fm.all('ManageCom').value);
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,7,TempClassGrid.getRowColData(i,4));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,8,TempClassGrid.getRowColData(i,5));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,9,TempClassGrid.getRowColData(i,7));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,10,TempClassGrid.getRowColData(i,8));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,11,TempClassGrid.getRowColData(i,9));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,12,TempClassGrid.getRowColData(i,10));
     TempClassToGrid.setRowColData(TempClassToGrid.mulLineCount-1,13,TempClassGrid.getRowColData(i,11));
     
     if(trim(TempClassGrid.getRowColData(i,6))=="") //如果有交费项的到帐日期为空，那么标志置0
        EnterAccDateFlag=0;

     //if(EnterAccDate<TempClassGrid.getRowColData(i,5))  //如果下一行的到帐日期>当前到帐日期，赋值
     //  EnterAccDate=TempClassGrid.getRowColData(i,5);
     if(compareDate(EnterAccDate,TempClassGrid.getRowColData(i,6))==2)
       EnterAccDate=TempClassGrid.getRowColData(i,6);
    }
 }

  if(EnterAccDateFlag==0)
    EnterAccDate="";

  //alert(EnterAccDate);

  //暂交费信息赋给用于提交数据的Grid
  for(i=0;i<TempRecordCount;i++)
  {
    if(TempGrid.getRowColData(i,1)!=''||fm.all('TempFeeType').value == "11"||fm.all('TempFeeType').value == "25"||fm.all('TempFeeType').value == "30")  //如果险种编码为空，则跳过
     {
       TempToGrid.addOne("TempToGrid");//当前文件中的对象，不用加前缀
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,1,fm.all('TempFeeNo').value);
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,2,fm.all('TempFeeType').value);
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,3,fm.all('PayDate').value);
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,4,TempGrid.getRowColData(i,3));
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,5,EnterAccDate);
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,6,fm.all('ManageCom').value);
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,7,TempGrid.getRowColData(i,1));
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,8,fm.all('AgentGroup').value);
       TempToGrid.setRowColData(TempToGrid.mulLineCount-1,9,fm.all('AgentCode').value);
       if (fm.all('TempFeeType').value=="1") {
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo1').value);
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"4");
       }
       else if (fm.all('TempFeeType').value=="11"){
       		TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo22').value);
            TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"4");
       }
       else if (fm.all('TempFeeType').value=="6"){
       		TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo12').value);
       }
       //* ↓ *** liuhao *** 2005-05-16 *** add 预打保单添加 ******
       else if (fm.all('TempFeeType').value=="8") {
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo15').value);
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"4");
       }
       //* ↑ *** zhangjun *** 2006-05-31 *** add 续期续保 ******
       //* ↑ *** zhangjun *** 2008-05-28 *** 续期续保OtherNotype变更 ******
       else if (fm.all('TempFeeType').value=="2"){
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,TempGrid.getRowColData(i,4));
         var ReturnNoType=getCodeType(TempGrid.getRowColData(i,4));
         var OtherNo = TempGrid.getRowColData(i,4);
         var OtherNoType="";
         //alert(fm.all('OtherNoType').value);
         OtherNoType =  fm.all('OtherNoType').value;


         	TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,OtherNoType); //othernoType
         }
       else if (fm.all('TempFeeType').value=="10") {
       	 TempToGrid.setRowColData(TempToGrid.mulLineCount-1,1,fm.all('InputNo17').value);
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo17').value);
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"11");
       }
       else if (fm.all('TempFeeType').value=="20") {
       	 TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo20').value);
       	 if((fm.all('InputNo20').value.substring(0,2))==("12")){
       	 TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"21");
       	 }else{ 
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"20");
         }
       }   
       else if (fm.all('TempFeeType').value=="25") {
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo26').value);
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"25");
       }
       else if (fm.all('TempFeeType').value=="30") {
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,fm.all('InputNo31').value);
         TempToGrid.setRowColData(TempToGrid.mulLineCount-1,11,"30");
       }
       else{
       	 TempToGrid.setRowColData(TempToGrid.mulLineCount-1,10,TempGrid.getRowColData(i,4)); //otherno->polno
       }
     }
  }

  initTempGrid();
  initTempClassGrid();
  confirmFlag=false; //复位，需重新单击确定

}

var mainRiskNum = 0;
//检查MulLine的信息域的数据格式是否合格,是否有重复险种数据
function checkGridValue()
{
  mainRiskNum = 0;
  var arrCardMoney = new Array();
  var allMoney = 0;

  if(TempGrid.checkValue("TempGrid"))//&&TempClassGrid.checkValue("TempClassGrid"))
  {
  	  	for(var n=0; n<TempClassGrid.mulLineCount;n++)
  	{
  		if(TempClassGrid.getRowColData(0,1)!=1&&TempClassGrid.getRowColData(0,1)!=14)
  			return TempClassGrid.checkValue("TempClassGrid");
  	}
   
   for(var n=0;n<TempGrid.mulLineCount;n++)
     {
       // if (fm.all('TempFeeType').value=="1") {
          //strSql = "select SubRiskFlag from LMRiskApp where RiskCode = '"
             //    + TempGrid.getRowColData(n,1) + "' and RiskVer = '2002'";
          //var riskDescribe = easyExecSql(strSql);

         // if (riskDescribe == "M") {
            //mainRiskNum = mainRiskNum + 1;
            //if (mainRiskNum >= 2) {
              //alert("不能录入两个或以上的主险险种");
             // return false;
            //}
          //}
       // }
        //alert('ddd');
       for(var m=n+1;m<TempGrid.mulLineCount;m++)
       {
         if(TempGrid.getRowColData(n,1)==TempGrid.getRowColData(m,1))
            {  if(fm.all('TempFeeType').value!="20"){
             alert("不能录入重复的险种编码");
             return false;
              }
          }
          if(fm.all('TempFeeType').value=="1"){
            if(TempGrid.getRowColData(n,1)=="330501"||TempGrid.getRowColData(n,1)=="530301")
             {
             alert("常无忧B险种已经停售，不能进行缴费操作");
             return false;
             }
            if(TempGrid.getRowColData(n,1)=="330701")
             {
             alert("健康人生个人护理保险（万能型，A款）已经停售，不能进行缴费操作");
             return false;
             }
             var tCom=fm.GlobalManageCom.value;
            if(tCom.substring(0,4)=='8621' || tCom.substring(0,4)=='8695' || tCom.substring(0,4)=='8611' || tCom.substring(0,4)=='8631' || tCom.substring(0,4)=='8632' ){
            	var sql = "select 1 from lmriskapp where kindcode='U' and riskcode='"+TempGrid.getRowColData(n,1)+"' with ur";
    					var rs = easyExecSql(sql);
    					if(rs)
    					{
      					alert("该公司万能险已经停售，不能进行缴费操作");
      					return false;
    					}
            }
           }
       }
        //alert(fm.all('TempFeeType').value);
       if (fm.all('TempFeeType').value=="6") {
         //校验险种
         var isCardRisk = false;
         for (var k=0; k<arrCardRisk.length; k++) {
          if (TempGrid.getRowColData(n,1) == arrCardRisk[k][0]) {
            isCardRisk = true;
          }
         }

         if (!isCardRisk) {
          alert("该险种" + TempGrid.getRowColData(n,1) + "不是该类单证包含的险种！");
          return false;
         }

         //保存险种和金额
         arrCardMoney[n] = new Array(TempGrid.getRowColData(n,1), TempGrid.getRowColData(n,3));
         allMoney = allMoney + parseInt(TempGrid.getRowColData(n,3));
       }
     }

     if (fm.all('TempFeeType').value=="6") {
       //校验金额比例
       if (arrCardRisk[0][3] != "1") {
         for (var i=0; i<arrCardRisk.length; i++) {
           for (var j=0; j<arrCardMoney.length; j++) {
             //找到相同险种
             if (arrCardRisk[i][0] == arrCardMoney[j][0]) {
               //校验倍数
               if (arrCardRisk[0][3]=="2" && (arrCardMoney[j][1]%arrCardRisk[i][2] != 0)) {
                 alert("该险种（" + arrCardMoney[j][0] + "）的交费金额必须是设定金额（" + arrCardRisk[i][2] + "）的倍数！");
                 return false;
               }

               if (arrCardRisk[0][3]=="3" && (arrCardMoney[j][1]/allMoney != arrCardRisk[i][4])) {
                 alert("该险种（" + arrCardMoney[j][0] + "）的交费金额比例（" + arrCardRisk[i][4] + "）错误！");
                 return false;
               }
             }
           }
         }
       }
     }
     return true;
   }

  return false;
}

//添加一笔纪录前，检查添加的暂交费信息数据是否已经存在于保存数据的Grid中，并计算交费金额累计
function checkTempRecord()
{
  var TempRecordCount = 0;
  var TempToGridCount = 0;
  tempFee=0.0;
  var i = 0;
  TempRecordCount=TempGrid.mulLineCount; //TempGrid的行数
  TempToGridCount=TempToGrid.mulLineCount;//TempToGrid的行数
  for(i=0;i<TempRecordCount;i++)
  {
    if(TempGrid.getRowColData(i,1)!='')  //如果险种编码为空，则跳过
     {
      for(var n=0;n<TempToGridCount;n++)
       {         //检查是否有相同的暂交费号。0表示后台自动产生
        if(TempToGrid.getRowColData(n,1)==fm.all('TempFeeNo').value)
          {
            alert("发现重复数据:暂交费号相同");
            return false;
           }
       }
       }

       tempFee = tempFee+10000*parseFloat(TempGrid.getRowColData(i,3));//交费金额累计
       
     
  }
     return true;
}

//添加一笔纪录前,计算添加的暂交费分类信息数据交费金额累计
function checkTempClassRecord()
{
  var TempClassRecordCount = 0;
  tempClassFee=0.0;
  var i = 0;
  TempClassRecordCount=TempClassGrid.mulLineCount; //页面上纪录的行数

  for(var n=0;n<TempClassGrid.mulLineCount;n++)
  {
    for(var m=n+1;m<TempClassGrid.mulLineCount;m++)
    {
      if(TempClassGrid.getRowColData(n,1)==TempClassGrid.getRowColData(m,1))
       {
          alert("不能录入重复的交费方式");
          return false;
       }
    //判断缴费方式为营销员代收时金额是否超过1000元
    if(TempClassGrid.getRowColData(n,1)=='14'||TempClassGrid.getRowColData(n,1)=='15'){
        if(TempClassGrid.getRowColData(n,3)>=1000){
         alert("营销员代收金额不能超过1000元！！");
         return false;
        }
    }
    }
  }
  for(i=0;i<TempClassRecordCount;i++)
  {
   if(TempClassGrid.getRowColData(i,1)!='')  //如果交费方式为空，则跳过
   {
      tempClassFee = tempClassFee+10000*parseFloat(TempClassGrid.getRowColData(i,3));//交费金额累计

      if(trim(TempClassGrid.getRowColData(i,6))!='')//如果到帐日期不为空，那么判断是否大于等于当前日期
      {
      	if(isDate(trim(TempClassGrid.getRowColData(i,6))))//如果日期格式正确
      	{
      		if(compareDate(trim(TempClassGrid.getRowColData(i,6)),getCurrentDate())==1)//如果大于当前日期
      		{

      		  alert("到帐日期不能超过今天！");
      		  return false;
      		}
      	}
      	else
      	{
      	  alert("到帐日期格式不对！");
      	  return false;
      	}
      }
    if(TempClassGrid.getRowColData(i,1)=='11' || TempClassGrid.getRowColData(i,1)=='12')  //如果交费方式为11||12，则交费日期不能大于当天，否则会对契约签单产生影响
      {         
         //如果交费方式为11||12，则交费日期不能大于当天，否则会对契约签单产生影响
        
         if(isDate(trim(fm.all('PayDate').value)))//如果日期格式正确
        {
            if(compareDate(trim(fm.all('PayDate').value),getCurrentDate())==1)//如果大于当前日期
            {

              alert("银行汇款、其他银行代收收费方式，交费日期不能超过今天！");
              return false;
            }
        }
        else
        {
          alert("交费日期格式错误！");
          return false;
        }
      }
      
   if(TempClassGrid.getRowColData(i,1)=='11')  //如果交费方式为11||3-支票类，则票据号码和银行不能为空
      {
        if(trim(TempClassGrid.getRowColData(i,11))==''|| trim(TempClassGrid.getRowColData(i,10))=='')
          {
           alert("交费方式为银行收费时，本方开户银行和银行帐号不能为空");
           return false;
          }
          var accsql = "select 1 from ldfinbank where bankcode ='" + trim(TempClassGrid.getRowColData(i,10)) + "' and bankaccno='" + trim(TempClassGrid.getRowColData(i,11)) +"'";
          var arrBankResult = easyExecSql(accsql);
          if(arrBankResult==null){
             alert("归集账户不存在，请确认录入的归集账户");
             return false ;
          }
      }
   if(TempClassGrid.getRowColData(i,1)=='12'||TempClassGrid.getRowColData(i,1)=='15'||TempClassGrid.getRowColData(i,1)=='16'||TempClassGrid.getRowColData(i,1)=='17')  //如果交费方式为12，则票据号码和银行不能为空
      {
        if(trim(TempClassGrid.getRowColData(i,11))==''|| trim(TempClassGrid.getRowColData(i,10))=='')
          {
           alert("交费方式为银行收费时，本方开户银行和银行帐号不能为空");
           return false;
          }
          var accsql = "select 1 from ldfinbank where bankcode ='" + trim(TempClassGrid.getRowColData(i,10)) + "' and bankaccno='" + trim(TempClassGrid.getRowColData(i,11)) +"'";
          var arrBankResult = easyExecSql(accsql);
          if(arrBankResult==null){
             alert("归集账户不存在，请确认录入的归集账户");
             return false ;
          }
      }
    
   if(TempClassGrid.getRowColData(i,1)=='3')  //如果交费方式为11||3-支票类，则票据号码和银行不能为空
     {
       if(trim(TempClassGrid.getRowColData(i,4))==''|| trim(TempClassGrid.getRowColData(i,10))=='' || trim(TempClassGrid.getRowColData(i,11))=='')
         {
          alert("交费方式为支票收费时，票据号码和本方开户银行、银行帐号不能为空");
          return false;
         }
         var accsql = "select 1 from ldfinbank where bankcode ='" + trim(TempClassGrid.getRowColData(i,10)) + "' and bankaccno='" + trim(TempClassGrid.getRowColData(i,11)) +"'";
          var arrBankResult = easyExecSql(accsql);
          if(arrBankResult==null){
             alert("归集账户不存在，请确认录入的归集账户");
             return false ;
          }
     }
     
     if(TempClassGrid.getRowColData(i,1)=='2')  //如果交费方式为11||3-支票类，则票据号码和银行不能为空
     {
       if( trim(TempClassGrid.getRowColData(i,10))=='' || trim(TempClassGrid.getRowColData(i,11))=='')
         {
          alert("交费方式为支票收费时，本方开户银行、银行帐号不能为空");
          return false;
         }
         var accsql = "select 1 from ldfinbank where bankcode ='" + trim(TempClassGrid.getRowColData(i,10)) + "' and bankaccno='" + trim(TempClassGrid.getRowColData(i,11)) +"'";
          var arrBankResult = easyExecSql(accsql);
          if(arrBankResult==null){
             alert("归集账户不存在，请确认录入的归集账户");
             return false ;
          }
     }
      if(TempClassGrid.getRowColData(i,1)=='5')  //如果交费方式为5-内部转账，则票据号码不能为空
      {
      	TempClassGrid.setRowColData(i,6,getCurrentDate());
        if(trim(TempClassGrid.getRowColData(i,4))==''||trim(TempClassGrid.getRowColData(i,6))=='')
          {
           alert("内部转账时，票据号码和到帐日期不能为空");
           return false;
          }
      }

      if(TempClassGrid.getRowColData(i,1)=='4')  //如果交费方式为4-银行转账，必须填写 开户银行,银行账号,户名并且到帐日期必须为空
      {
        if(trim(TempClassGrid.getRowColData(i,7))==''||trim(TempClassGrid.getRowColData(i,8))==''||trim(TempClassGrid.getRowColData(i,9))=='')
          {
           alert("银行转账时，必须填写 开户银行,银行账号,户名");
           return false;
          }
        TempClassGrid.setRowColData(i,6,'');//  将到帐日期置空

    if (trim(TempClassGrid.getRowColData(i,7))=="0101") {
          if (trim(TempClassGrid.getRowColData(i,8)).length!=19 || !isInteger(trim(TempClassGrid.getRowColData(i,8)))) {
            alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！");
            return false;
          }
        }
      }
    if(TempClassGrid.getRowColData(i,1)=='11'||TempClassGrid.getRowColData(i,1)=='3'||TempClassGrid.getRowColData(i,1)=='12')  //如果交费方式为11||3-支票类，则票据号码和银行不能为空
      {
        if(trim(TempClassGrid.getRowColData(i,4))==''||trim(TempClassGrid.getRowColData(i,10))=='')
          {
           alert("交费方式为支票或银行收费时，票据号码和本方开户银行不能为空");
           return false;
          }
          var accsql = "select 1 from ldfinbank where bankcode ='" + trim(TempClassGrid.getRowColData(i,10)) + "' and bankaccno='" + trim(TempClassGrid.getRowColData(i,11)) +"'";
          var arrBankResult = easyExecSql(accsql);
          if(arrBankResult==null){
             alert("归集账户不存在，请确认录入的归集账户");
             return false ;
          }
      }
    }
   if	(isDate(TempClassGrid.getRowColData(i,5))||TempClassGrid.getRowColData(i,5)==""||TempClassGrid.getRowColData(i,5)==null)
   {
   }
   else
   {
   	alert("支票日期格式不对!");
   	return false;
   }
  }
     return true;
}

//点击全部提交或者全部清除时--清空数据并初始化
function clearFormData()
{
  initGlobalData();
  initTempToGrid();
  initTempClassToGrid();
  initTempGrid();
  initTempClassGrid();
  fm.all('TempFeeNo').valur="";
  fm.all('TempFeeCount').value=0;
  fm.all('SumTempFee').value=0.0 ;
  divTempFeeSave.style.display="none";
  divTempFeeInput.style.display="";
  TempGrid.lock();
}


//初始化全局变量
function initGlobalData()
{
//存放添加动作执行的次数
 addAction = 0;
//暂交费总金额
 sumTempFee = 0.0;
//暂交费信息中交费金额累计
 tempFee = 0.0;
//暂交费分类信息中交费金额累计
 tempClassFee = 0.0;
 confirmFlag=false; //复位，需重新单击确定
}


/*********************************************************************
 *  选择暂交费类型后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect(cCodeName, Field) {
	if(cCodeName == "TempFeeType1") {
	  showTempFeeTypeInput(Field.value);
	}
	if(cCodeName == "paymode1"){
		var sPayMode="";
		for(var i=0;i<TempClassGrid.mulLineCount;i++){
			sPayMode = TempClassGrid.getRowColData(i,1);
			//alert(sToDate);
			if (sPayMode == "1"||sPayMode == "8"){
				TempClassGrid.setRowColData(i,6,sToDate);
			}
			else{
				TempClassGrid.setRowColData(i,6,"");
			}
		}
	}
}

function showTempFeeTypeInput(type) {
  fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('AgentCode').value='';
		fm.all('InputNoType').value='';
		fm.all('InputNoName').value='';
		fm.all('InputNo1').value='';
		fm.all('InputNo2').value='';
		fm.all('InputNo3').value='';
		fm.all('InputNo4').value='';
		fm.all('InputNo7').value='';
		fm.all('InputNo24').value='';
		fm.all('InputNo25').value='';
		fm.all('InputNo18').value='';
		fm.all('InputNo20').value='';
		fm.all('InputNo21').value='';
		fm.all('InputNo26').value='';
		fm.all('InputNo27').value='';
  for (i=0; i<30; i++) {
    if ((i+1) == type) {
    	try{
      	fm.all("TempFeeType" + (i+1)).style.display = '';
      }catch(ex){}
    }
    else {
    	try{
	      fm.all("TempFeeType" + (i+1)).style.display = 'none';
	    }catch(ex){}
    }
  }
}

function queryLJSPay()
{
   if(fm.all('InputNo4').value!="")
   {
   }
   else
   {
    window.open("./FinFeeGetQueryLJSPay.html");
   }
}


//校验录入的单证是否不需要校验是否发放给业务员，如果需要返回true,否则返回false
function needVerifyCertifyCode(CertifyCode)
{

  try {

       var tSql = "select Sysvarvalue from LDSysVar where Sysvar='NotVerifyCertifyCode'";
       var tResult = easyExecSql(tSql, 1, 1, 1);
       var strRiskcode = tResult[0][0];
       var strValue=strRiskcode.split("/");
       var i=0;
	   while(i<strValue.length)
	   {
	   	if(CertifyCode==strValue[i])
	   	{
           return false;
	   	}
	   	i++;
	   }
  	 }
  	catch(e)
  	 {}

  	return true;
}

function queryAgent()
{
    if(fm.all('ManageCom').value==""){
         alert("请先录入管理机构信息");
         return;
    }
    if(fm.all('AgentCode').value == "")
    {
		  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  }
	if(fm.all('AgentCode').value != "")
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码
		var strSql = "select AgentCode,Name from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
      }
	}
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrReturn)
{
    fm.AgentCode.value = arrReturn[0][0];
    fm.GroupAgentCode.value = arrReturn[0][95];
    
}

//校验录入代理人和保单代理人是否一致
function verifyAgent(PolNo)
{

	var otherNoType=fm.OtherNoType.value;
	var strSql="";

	if(otherNoType=="1")
	{
	   strSql = "select agentcode from lcgrpcont where grpcontno='" + PolNo +"'";
	}
	else if(otherNoType=="2")
	{
		strSql = "select agentcode from lccont where contno='"+PolNo+"'"; //虽然最初开发者将参数写成PolNo,其实是ContNo
	}

    var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      if(arrResult[0][0]!=trim(fm.all('AgentCode').value))
      {
      	alert("录入代理人和保单代理人不一致!");
      	return false;
      }
    }
    else
    {
      alert("未查到该保单信息！");
      return false;
    }

	return true;
}

//校验录入代理人和保单代理人是否一致
function verifyAgent1(PrtNo)
{
	var strSql="";
	   strSql = "select agentcode from lccont where prtno='" + PrtNo +"'";

    var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      if(arrResult[0][0]!=trim(fm.all('AgentCode').value))
      {
      	alert("录入代理人和保单代理人不一致!");
      	return false;
      }
    }
    else
    {
      alert("未查到该保单信息！");
    }

	return true;
}
function verifyAgent20(PrtNo)
{
	var strSql="";
	   strSql = "select agentcode from HCCont where prtno='" + PrtNo +"' union all select agentcode from HCGrpCont where grpprtno='" + PrtNo +"'";

    var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      if(arrResult[0][0]!=trim(fm.all('AgentCode').value))
      {
      	alert("录入代理人和保单代理人不一致!");
      	return false;
      }
    }
    else
    {
      alert("未查到该保单信息！");
    }

	return true;
}

function checkchequenoanddate()
{
     var sPayMode="";
		 var checkno="";
		 var checkdate="";
		 var checkname="";
		 var count;
		 for(var i=0;i<TempClassGrid.mulLineCount;i++){
			sPayMode = TempClassGrid.getRowColData(i,1);

			if (sPayMode == "2"||sPayMode == "3"){

				checkno = TempClassGrid.getRowColData(i,4);
				checkdate = TempClassGrid.getRowColData(i,5);
			  checkname = TempClassGrid.getRowColData(i,9);

			  if(checkno.length==0||checkdate.length==0||checkname.length==0)
			  {  count = i+1;
			     alert("交费录入第"+count+"行需要录入支票日期、支票号、户名");
			     return  false ;
			  }


			}
			else
			{
				continue;
      }
    }
     return true;
}

function fillgrid(PrtNo)
{
   var strSql="select distinct conttype from lccont where prtno='"+PrtNo+"'";
   var strSql1="";
   var strSql2="";
   var strSql3="select a.riskcode,b.riskname,sum(a.prem)-(Select nvl(Sum(Mo.Paymoney),0) From Ljtempfee Mo Where Mo.Riskcode = a.Riskcode And Mo.Otherno = '"+PrtNo+"' And Exists (Select 1 From Ljtempfeeclass Where Tempfeeno = Mo.Tempfeeno And Paymode = 'YS')) from lcgrppol a,lmrisk b where prtno='"+PrtNo+"' and a.riskcode=b.riskcode group by a.riskcode,b.riskname";
   var strSql4="select paymode,codename,prem - (select nvl(Sum(Mo.Paymoney),0) from ljtempfeeclass mo where exists (select 1 from ljtempfee where tempfeeno=mo.tempfeeno and otherno='"+PrtNo+"')),'','','',bankcode,bankaccno,accname from lcgrpcont,ldcode where prtno='"+PrtNo+"' and  codetype='paymode' and paymode=code";
   var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      if (arrResult[0][0]=='1') { //个单
      	strSql1 = "select '000000','null',-LF_PayMoneyNo('"+PrtNo+"') from lcpol a where a.prtno='"+PrtNo+"' fetch first 1 rows only";
	      turnPage.queryModal(strSql1, TempGrid);
	      strSql2="select distinct a.paymode,ldcode.codename,-LF_PayMoneyNo('"+PrtNo+"'),'','','',"
	      +"a.bankcode,a.bankaccno,a.accname from lccont a,ldcode,lcinsured c ,lcpol d "
	      +"where a.prtno='"+PrtNo+"' and  ldcode.codetype='paymode' and a.paymode=code "
	      +"and a.contno=c.contno "
	      +" and a.contno=d.contno"
	      +" and c.insuredno=d.insuredno"
	      +" and d.uwflag in ('4','9')"
	      +" group by a.paymode,ldcode.codename,a.bankcode,a.bankaccno,a.accname";
	      turnPage.queryModal(strSql2, TempClassGrid);
      }
      else if (arrResult[0][0]=='2'){ //团单
      turnPage.queryModal(strSql3, TempGrid);	
      turnPage.queryModal(strSql4, TempClassGrid);
    }
  }
}

//create by yanjing
function fillgrid1(PrtNo)
{  
   var strSql1="";
   var strSql2="";
   var strSql3="select substr(b.ProductNo,1,6) ProductNo,'健管产品',decimal(sum(b.Prem),12,2) from HCGrpCont a,HCGrpProduct b where a.GrpContNo=b.GrpContNo and a.ContState='2' and a.ContKind='01' and a.GrpPrtNo='"+PrtNo+"' group by substr(b.ProductNo,1,6) with ur";
   var strSql4="select distinct a.PayMode,c.codename, decimal(sum(b.Prem),12,2),'','','',a.BankCode,a.BankAccNo,a.AccName from HCGrpCont a,HCGrpProduct b,ldcode c where a.GrpContNo=b.GrpContNo and a.ContState='2' and a.ContKind='01' and c.codetype='paymode' and a.paymode=c.code and a.GrpPrtNo='"+PrtNo+"' group by a.PayMode,c.codename,a.BankCode,a.BankAccNo,a.AccName with ur";
    if (PrtNo!= null)
    {
      if (PrtNo.substring(0,2)== '11') { //个单
      	strSql1 ="select substr(b.ProductNo,1,6),'健管产品',decimal(sum(b.Prem),12,2) from "
				+"HCCont a,HCProduct b where a.ContNo=b.ContNo and a.ContState='2' and a.ContKind='01' and a.prtno='"+PrtNo+"' "
				+"group by substr(b.ProductNo,1,6) with ur";
	      turnPage.queryModal(strSql1, TempGrid);	     
	      strSql2 ="select distinct a.PayMode,c.codename, decimal(sum(b.Prem),12,2),'','','',a.BankCode,a.BankAccNo,a.AccName from "
				+"HCCont a,HCProduct b,ldcode c where a.ContNo=b.ContNo and a.ContState='2' and a.ContKind='01' and c.codetype='paymode' and a.paymode=c.code and a.prtno='"+PrtNo+"' "
				+"group by a.PayMode,c.codename,a.BankCode,a.BankAccNo,a.AccName with ur";
	      turnPage.queryModal(strSql2, TempClassGrid);
      }
      else if (PrtNo.substring(0,2)== '12'){ //团单
      turnPage.queryModal(strSql3, TempGrid);	
      turnPage.queryModal(strSql4, TempClassGrid);
    }
  }
} 
//create by yanjing
function fillgrid2(PrtNo)
{  
   var strSql1="";
   var strSql2="";
   var strSql3="select '000000' ProductNo,'社保报销款',decimal(a.sumduepaymoney,12,2) from ljspay a where  a.getnoticeno='"+PrtNo+"' and a.othernotype='25' and exists (select 1 from LLCasePayAdvancedTrace b where b.getnoticeno=a.getnoticeno and b.dealstate='03') with ur";
   var strSql4="select  '12','社保报销款', decimal(a.sumduepaymoney,12,2),'','','','','','' from ljspay a where  a.getnoticeno='"+PrtNo+"' and a.othernotype='25' and exists (select 1 from LLCasePayAdvancedTrace b where b.getnoticeno=a.getnoticeno and b.dealstate='03') with ur";

      turnPage.queryModal(strSql3, TempGrid);	
      turnPage.queryModal(strSql4, TempClassGrid);
    
} 
//校验重复收费
function checkDate()
{
	  if(fm.all('TempFeeType').value=="1")
  {  	
				 for(var i=0;i<TempGrid.mulLineCount;i++){
				 	//alert(TempGrid.getRowColData(i,1));
				 	//alert(fm.InputNo2.value);
					var Sql="select 1 from ljtempfee where tempfeeno='"+fm.InputNo2.value+"' and riskcode ='"+TempGrid.getRowColData(i,1)+"'"
					var arrResult = easyExecSql(Sql);
				    if (arrResult != null) 
    				{
									alert("该保单的险种"+TempGrid.getRowColData(i,1)+"已经进行过交费!");
									return false;									
						}
			}
		}
			return true;
}
function fillDuePay(SerialNo){

	var strSql = "select * from ("
	+" select b.riskcode,c.riskname,sum(a.SumDuePayMoney) "
	+" from LJSPayGrp a,lcgrppol b,lmrisk c  "
	+" where a.grppolno=b.grppolno and b.riskcode=c.riskcode and a.SerialNo='"+SerialNo+"'  group by b.riskcode,c.riskname"
	+"  union "
	+" select b.riskcode,c.riskname,sum(a.SumDuePayMoney) "
	+" from LJSPayGrp a,lbgrppol b,lmrisk c  "
	+" where a.grppolno=b.grppolno and b.riskcode=c.riskcode and a.SerialNo='"+SerialNo+"'  group by b.riskcode,c.riskname"
	+") as x with ur";
	turnPage.queryModal(strSql, TempGrid);
	var strSql_sum = "select '','',sum(SumDuePayMoney) from LJSPay where SerialNo='"+SerialNo+"'";
	turnPage.queryModal(strSql_sum, TempClassGrid);
	return true;
}

function floatRound(myFloat,mfNumber)
{
  var cutNumber = Math.pow(10,mfNumber-1);
  return Math.round(myFloat * cutNumber)/cutNumber;
}

function checkNotice(getnoticeno)
{
	var checkSQL = " select distinct b.dealstate, a.getnoticeno,(select codename from ldcode where codetype ='feecancelreason' and code = a.cancelreason),b.getnoticeno from ljspaypersonb a,ljspayb b where "
							+" a.getnoticeno ='"+getnoticeno
							+"'and a.contno = b.otherno and a.getnoticeno = b.getnoticeno "
							+" and b.dealstate not in('2','3')"
							;
	var arrResult = easyExecSql(checkSQL);
	
	return arrResult;			
}


//团体保单信息
function getInfo1(control){
	var prtno=control.value;
    
    var numSQL = "select 801 + count(*) from ljtempfeeclass a where exists ( select 1 from ljtempfee where otherno='"+prtno+"' and tempfeeno=a.tempfeeno) and a.paymode='YS' with ur"
    var numResult = easyExecSql(numSQL);
    var num;
    if(numResult == null){
        num = 801;
    } else {
        num = numResult[0][0];
    }        
  var findSQL="select distinct a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
		+"a.AgentCode,trim(a.prtno)||'" + num + "',a.prtno,getUniteCode(a.AgentCode) from lcgrpcont a where a.paymode<>'4' "
		+"and a.signdate is null and appflag in ('0','9') "
		+"and  (select count(1) from lcrnewstatelog where prtno=a.prtno)=0 "
		+"and  (select count(1) from ljtempfee where enteraccdate is null and otherno=a.prtno)=0 and a.prtno='"+prtno+"' "
		+"and a.managecom='"+fm.all('GlobalManageCom').value+"' ";
	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('InputNoType').value='缴费通知书号码';
		fm.all('InputNoName').value='1';
		fm.all('InputNo2').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][5];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('GroupAgentCode').value='';
		fm.all('AgentCode').value='';
		fm.all('InputNoType').value='';
		fm.all('InputNoName').value='';
		fm.all('InputNo2').value='';
	}
}
//个人保单信息
function getInfo2(control){
	var prtno=control.value;
  var findSQL="select  distinct a.managecom,(select distinct name from ldcom where comcode=a.managecom) name,"
		+"a.AgentCode,trim(a.prtno)||'801',a.prtno,getUniteCode(a.AgentCode) from lccont a where a.paymode<>'4' "
		+"and a.signdate is null  "
		+"and not exists(select 1 from lcrnewstatelog where grpcontno = '00000000000000000000' and prtno=a.prtno) "
		+"and  (select count(1) from ljtempfee where enteraccdate is null and otherno=a.prtno)=0 "
		+"and conttype = '1'  and a.prtno='"+prtno+"' "
		+"and a.managecom='"+fm.all('GlobalManageCom').value+"' ";
	var arrResult = easyExecSql(findSQL);
	
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('InputNoType').value='缴费通知书号码';
		fm.all('InputNoName').value='1';
		fm.all('InputNo2').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][5];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('AgentCode').value='';
		fm.all('GroupAgentCode').value='';
		fm.all('InputNoType').value='';
		fm.all('InputNoName').value='';
		fm.all('InputNo2').value='';
	}
    flag=2;
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");
}
function getInfo3(control){
	var contno=control.value;		
	var findSQL ="select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
				+"a.AgentCode,getnoticeno,getUniteCode(a.AgentCode) from lccont a ,ljspay b where a.contno=b.otherno "
				+"and conttype='1' and b.othernotype='2' "
				+"and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
				+"and a.paymode<>'4' and b.sumduepaymoney<>0 and b.otherno='"+contno+"' "
				+"and a.managecom='"+fm.all('GlobalManageCom').value+"' "
				+" union "
				+"select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
				+"a.AgentCode,b.getnoticeno,getUniteCode(a.AgentCode) from lcgrpcont a ,ljspaygrp b where a.grpcontno=b.grpcontno "
				+" and a.paymode<>'4' and a.grpcontno<>'00000000000000000000' "
				+"and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
				+" and b.sumduepaymoney<>0 and b.grpcontno='"+contno+"' "
				+"and a.managecom='"+fm.all('GlobalManageCom').value+"' ";
	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('InputNo4').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][4];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('GroupAgentCode').value='';
		fm.all('AgentCode').value='';
		fm.all('InputNo4').value='';
	}
}
function getInfo5(control){
	var otherno=control.value;
	var findSQL ="select managecom,(select distinct name from ldcom where comcode=ljspay.managecom) name, "
				+"AgentCode,getnoticeno,getUniteCode(AgentCode) from ljspay where othernotype in ('3','10','13') "
				+"and not exists (select 1 from ljtempfee where tempfeeno=ljspay.getnoticeno) and bankcode is null "
				+"and sumduepaymoney<>0 and otherno='"+otherno+"' "
				+"and managecom='"+fm.all('GlobalManageCom').value+"' ";
	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('InputNo8').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][4];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('GroupAgentCode').value='';
		fm.all('AgentCode').value='';
		fm.all('InputNo8').value='';
	}
}
function getInfo6(control){
	fm.all('InputNo18').value = fm.all('InputNo12').value;
	var payno=control.value;
	var findSQL ="select b.managecom,(select distinct name from ldcom where comcode=b.managecom) name, "
				+"case substr(a.receivecom,1,1) when 'D' then substr(a.receivecom,2,10) else '' end,getUniteCode(case substr(a.receivecom,1,1) when 'D' then substr(a.receivecom,2,10) else '' end)  "
				+"from lzcard a ,LZCardPay b, lzcardnumber c where a.state in ('2', '12') "
				+"and a.subcode=b.cardtype and a.startno=b.startno and c.cardtype = b.cardtype "
				+"and b.startno = c.cardserno "
				+"and b.payno='"+payno+"' "
				+"and b.payno  not  in (select tempfeeno from ljtempfeeclass) "
				+"and b.managecom='"+fm.all('GlobalManageCom').value+"' ";

	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('GroupAgentCode').value=arrResult[0][3];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('AgentCode').value='';
		fm.all('GroupAgentCode').value='';
	}
}
function getInfo7(control){//代理点结算
	var otherno=control.value;
	
	var findSQL = "select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name,"+
	"AgentCode,getUniteCode(AgentCode) from LJSPayGrp a "
	+" where a.SerialNo='"+otherno+"' and managecom='"+fm.all('GlobalManageCom').value+"' with ur";
	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('GroupAgentCode').value=arrResult[0][3];		
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('AgentCode').value='';
		fm.all('GroupAgentCode').value='';
	}

}
function getInfo9(control){//理赔预付赔款
	var otherno=control.value;
	
	var findSQL = "select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name,"
		+" AgentCode,getnoticeno,getUniteCode(AgentCode) from LJSPay a "
		+" where a.otherno='"+otherno+"' and managecom='"+fm.all('GlobalManageCom').value+"' with ur";
	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('InputNo25').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][4];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('AgentCode').value='';
		fm.all('GroupAgentCode').value='';
        fm.all('InputNo25').value='';
        }
	}
 function getInfo20(control){
	var PrtNo=control.value;	
	var findSQL = "select  distinct managecom,(select distinct name from ldcom where comcode=HCCont.managecom) name,"
	    +"agentcode,trim(PrtNo)||'801',PrtNo,getUniteCode(AgentCode) from HCCont where PrtNo='" + PrtNo +"' and managecom='"+fm.all('GlobalManageCom').value+"' union all select distinct managecom,(select distinct name from ldcom where comcode=HCGrpCont.managecom) name,"
	    +"agentcode,trim(GrpPrtNo)||'801',GrpPrtNo,getUniteCode(AgentCode) from HCGrpCont where grpprtno='" + PrtNo +"' and managecom='"+fm.all('GlobalManageCom').value+"' with ur";
	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('InputNo21').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][5];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('AgentCode').value='';
		fm.all('GroupAgentCode').value='';
		fm.all('InputNo21').value='';
	}
}

 function getInfo24(control){
	var contno=control.value;	
	var findSQL = "select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
				+"a.AgentCode,b.getnoticeno,getUniteCode(a.AgentCode) from lcgrpcont a ,ljspay b where a.grpcontno=b.otherno and b.othernotype='25'"
				+"and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
				+" and b.sumduepaymoney<>0 and b.otherno='"+contno+"' "
				+"and exists (select 1 from LLCasePayAdvancedTrace c where c.getnoticeno=b.getnoticeno and c.dealstate='03') "
				+"and b.managecom like '"+fm.all('GlobalManageCom').value+"%' "
				+" union "
				+"select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
				+"a.AgentCode,b.getnoticeno,getUniteCode(a.AgentCode) from lbgrpcont a ,ljspay b where a.grpcontno=b.otherno and b.othernotype='25'"
				+"and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
				+"and exists (select 1 from LLCasePayAdvancedTrace c where c.getnoticeno=b.getnoticeno and c.dealstate='03') "
				+" and b.sumduepaymoney<>0 and b.otherno='"+contno+"' "
				+"and b.managecom like '"+fm.all('GlobalManageCom').value+"%' with ur";
	var arrResult = easyExecSql(findSQL);
	if(arrResult!=null){
		fm.all('ManageCom').value=arrResult[0][0];
		fm.all('ManageComName').value=arrResult[0][1];
		fm.all('AgentCode').value=arrResult[0][2];
		fm.all('InputNo27').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][4];
	}else{
		fm.all('ManageCom').value='';
		fm.all('ManageComName').value='';
		fm.all('AgentCode').value='';
		fm.all('GroupAgentCode').value='';
		fm.all('InputNo27').value='';
	}
}
 function getInfo30(otherno){
    var otherno=otherno.value;   
    var findSQL = "select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
                +"a.AgentCode,a.getnoticeno,getUniteCode(a.AgentCode) from ljspay a where a.othernotype='30'"
                +"and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=a.getnoticeno) "
                +" and a.sumduepaymoney<>0 and a.otherno='"+otherno+"' "
                +"and a.managecom like '"+fm.all('GlobalManageCom').value+"%' with ur";
    var arrResult = easyExecSql(findSQL);
    if(arrResult!=null){
        fm.all('ManageCom').value=arrResult[0][0];
        fm.all('ManageComName').value=arrResult[0][1];
        fm.all('AgentCode').value=arrResult[0][2];
        fm.all('InputNo32').value=arrResult[0][3];
		fm.all('GroupAgentCode').value=arrResult[0][4];
    }else{
        fm.all('ManageCom').value='';
        fm.all('ManageComName').value='';
        fm.all('AgentCode').value='';
		fm.all('GroupAgentCode').value='';
        fm.all('InputNo32').value='';
    }
}
function payModeHelp(){
	window.open("./PayModeHelp.jsp");
}



//function queryInsbank()
//{
//	showInfo = window.open("../bq/LDBankQueryMain.jsp");
//}

//function afterQuery(var i, var arrReturn )
//{
//
//	TempClassGrid.setRowColData(i,10,arrReturn[0]);
////获取正确的行号
//tRow = BankGrid.getSelNo() - 1;
//
//arrSelected = new Array();
////设置需要返回的数组
//arrSelected[0] = BankGrid.getRowColData(tRow, 1); 
//arrSelected[1] = BankGrid.getRowColData(tRow, 2);
//return arrSelected;
//	
//}

//卡折业务收费签单后的动作
function afterSignCont(FlagStr, content)
{
    showInfo.close();
    window.focus();
    if(FlagStr == "Fail")
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    clearFormData();
}

 //add by yanjing begin
function tempfeeType20(){
    var sql="select '1' from HCGrpCont a,HCGrpProduct b where a.GrpPrtNo='" + fm.all('InputNo20').value + "' and a.GrpContNo=b.GrpContNo and a.ContState='2' and a.ContKind='01' and a.signdate is null "
            +" union all "
            +"select '1' from HCCont a,HCProduct b where a.PrtNo='" + fm.all('InputNo20').value + "' and a.ContNo=b.ContNo and a.ContState='2' and a.ContKind='01'  with ur"
    var tarrResult = easyExecSql(sql);
    if(!tarrResult){
    	alert('保单不存在或者没有核保通过'); 
      return false ; 
    }
 
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('InputNo20').value + "' "
              + " and confdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode in ('1','11','10','13','3','2','12','6','5'))";
    var arrResult = easyExecSql(sql);
    if(arrResult)
    {
        alert('已经收过费，缴费凭证号是：'+arrResult[0][0]); 
        return false ; //存在
    }
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('InputNo20').value + "' "
              + " and confdate is  null and enteraccdate is not null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode ='4')";

    var arrResult1 = easyExecSql(sql);
    if(arrResult1)
    {
        alert('已经收过费，缴费凭证号是：'+arrResult1[0][0]); 
        return false ; //存在
    }
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('InputNo20').value + "' "
              + " and confdate is  null and enteraccdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode ='4')";

    var arrResult2 = easyExecSql(sql);
    if(arrResult2)
    {
        alert('该保单缴费方式是银行转帐，缴费凭证号是：'+arrResult2[0][0]); 
        return false ; //存在
    }
     //end   modify  yanjing   again date 2009-03-19
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("投保单印刷号码|NOTNULL", fm.all('InputNo20').value) != true) {
      return false;
    }
    if (verifyElement("暂交费收据号|NOTNULL&len>=11", fm.all('InputNo21').value) != true) {
      return false;
    }

    //repair:类型1：新单交费输入的是印刷号，不验证格式
    //验证格式处理，需要按照中英模式修改
    //校验单证发放
    if (!verifyTempfeeNo(fm.all('InputNo21').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo20').value)) return false;

    fm.all('TempFeeNo').value = fm.all('InputNo21').value;
    typeFlag=true;

    if(verifyAgent20(fm.all('InputNo20').value)==false)
    {
    	return false;
    }
   // if(fm.all('InputNoName').value=="1")
   //{
  //  	    alert(fm.all('InputNo1').value);
   //fillgrid(fm.all('InputNo20').value);
   //TempGrid.unLock();
   //TempClassGrid.unLock();
    //}
    //else {
   //TempGrid.unLock();
   //TempGrid.addOne("TempGrid");
   //TempClassGrid.unLock();
   //TempClassGrid.addOne("TempClassGrid");
  //}
  //modify by yanjing
  fillgrid1(fm.all('InputNo20').value);
   TempGrid.unLock();
   TempClassGrid.unLock();
  return true;
}

//add by yanjing begin 社保报销比例
function tempfeeType24(){
    fm.all('OtherNoType').value="1";
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where tempfeeno='" + fm.all('InputNo27').value + "' "
              + " and exists (select 1 from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno )";
    var arrResult = easyExecSql(sql);
    if(arrResult)
    {
        alert('已经收过费，缴费凭证号是：'+arrResult[0][0]); 
        return false ; //存在
    }
  
    initTempGrid();
    initTempClassGrid();
    TempGrid.clearData("TempGrid");
    TempClassGrid.clearData("TempClassGrid");

    //校验录入
    if (verifyElement("投保单印刷号码|NOTNULL", fm.all('InputNo26').value) != true) {
      return false;
    }
    if (verifyElement("暂交费收据号|NOTNULL&len>=11", fm.all('InputNo27').value) != true) {
      return false;
    }

    //repair:类型1：新单交费输入的是印刷号，不验证格式
    //验证格式处理，需要按照中英模式修改
    //校验单证发放
    if (!verifyTempfeeNo(fm.all('InputNo27').value)) return false;
    if (!verifyPrtNo(fm.all('InputNo26').value)) return false;

    fm.all('TempFeeNo').value = fm.all('InputNo27').value;
    typeFlag=true;

    if(verifyAgent(fm.all('InputNo26').value)==false)
    {
    	return false;
    }
 
  fillgrid2(fm.all('InputNo27').value);
   TempGrid.unLock();
   TempClassGrid.unLock();
  return true;
}

function batchGet() {
   window.open("./TempFeeBatchMain.jsp");
}
