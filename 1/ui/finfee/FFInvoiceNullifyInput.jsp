<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2007-05-18
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GB2312" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="FFInvoiceNullifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FFInvoiceNullifyInit.jsp"%>
</head>

<body onload="initForm();" >
    <form action="FFInvoiceNullifySave.jsp" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIBaseInfo);">
                </td>
                <td class="titleImg">发票基本查询</td>
            </tr>
        </table>
        <div id="divFFIBaseInfo" style= "display:''">
            <table class="common" align='center' >
                <tr class="common">
                    <td class="title">机构编号</td>
                    <td class="input"><input class="readonly" name="ComCode" readonly="readonly" /></td>
                    <td class="title"></td>
                    <td class="input"></td>
                </tr>
                <tr class="common">
                    <td class="title">纳税人标识号</td>
                    <td class="input"><input class="common" name="TaxpayerNo"/></td>
                    <td class="title">纳税人名称</td>
                    <td class="input"><input class="common" name="TaxpayerName"/></td>
                </tr>
                <tr>
                    <td class="title">发票代码</td>
                    <td class="input"><input class="common" name="InvoiceCode"/></td>
                    <td class="title">发票扩展代码</td>
                    <td class="input"><input class="common" name="InvoiceCodeExp"/></td>
                </tr>
            </table>
        </div>
        <input value="查  询" class="cssButton" type="button" onclick="easyQueryClick();">
        <p />
        <!-- 查询结果部分 -->
        <!-- 信息（列表） -->
        <div id="divInvoiceGrid" style="display:''">
            <span id="spanInvoiceGrid"></span>
        </div>
        <div id= "divPage" align="center" style= "display:''">
            <input class="cssButton" value="首  页" type="button" onclick="turnPage.firstPage();" /> 
            <input class="cssButton" value="上一页" type="button" onclick="turnPage.previousPage();" />                    
            <input class="cssButton" value="下一页" type="button" onclick="turnPage.nextPage();" /> 
            <input class="cssButton" value="尾  页" type="button" onclick="turnPage.lastPage();" />
        </div>
        <input type="hidden" id="fmtransact" name="fmtransact">
        <table>
            <tr>
                <td class="common"><IMG  src= "../common/images/butExpand.gif"></td>
                <td class="titleImg">作废信息录入</td>
            </tr>
        </table>
        <table class="common" align='center' >
                <tr class="common">
                    <td class="title">发票号码</td>
                    <td class="input"><input class="common" name="InvoiceNo" elementtype="nacessary" verify="发票号码间|notnull&len=8"/></td>
                    <td class="title">金额</td>
                    <td class="input"><input class="common" name="Money" elementtype="nacessary" verify="金额|num&notnull"/></td>
                </tr>
            </table>
        <div id="divButton"><input value="保  存" class="cssButton" type="button" onclick="addIBaseInfo();"></div>
    </form>
    <span id="spanCode"  style="display:none; position:absolute; slategray"></span>
</body>
</html>