<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
    var tType = "<%=request.getParameter("type")%>";
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="SendToBankConfirm.js"></SCRIPT>
    <%@include file="SendToBankConfirmInit.jsp"%>
    <title>签发保单 </title>
</head>

<body  onload="initForm();" >
    <form action="./SendToBankConfirmSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr>
                <td class= titleImg align= center>请输入查询条件：</td>
            </tr>
        </table>
        <table  class= common align=center>
            <TR  class= common>
                <TD  class= title>
                    管理机构
                </TD>
                <TD  class= input>
                    <Input class=codeNo readonly=true name=ManageCom value=manageCom verify="管理机构|notnull" 
                        ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
                        onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
                </TD>
                <TD  class= title>
                    印刷号
                </TD>
                <TD  class= input>
                    <Input class= common  name=OtherNo verify="印刷号码|int">
                </TD>
                <TD  class= title>
                    缴费方式
                </TD>
                <TD  class= input>
                    <Input NAME=PayMode VALUE=""  CLASS=codeNo MAXLENGTH=20  ondblclick="return showCodeList('PayModeInd',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('PayModeInd',[this,PayModeName],[0,1],null,null,null,1);"><input class=codename name=PayModeName readonly=true elementtype=nacessary>
                </TD>
            </TR>
            <TR  class= common>
                <TD  class= title>
                    投保人
                </TD>
                <TD  class= input>
                    <Input class= common name=AppntName>
                </TD>
                <TD  class= title>
                    业务员代码
                </TD>
                <TD  class= input>
                    <Input class= common  name=AgentCode>
                </TD>
                <td class="title8">渠道</td>
                <td class="input8">
                    <input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" />
                </td>
            </TR>
            <TR  class= common>
                <TD  class= title>
                    生效起期
                </TD>
                <TD  class= input>
                    <Input class="coolDatePicker" dateFormat="short" name=StartCValiDate verify="生效起期|notnull&Date" >
                </TD>
                <TD  class= title>
                    生效止期
                </TD>
                <TD  class= input>
                    <Input class="coolDatePicker" dateFormat="short" name=EndCValiDate verify="生效止期|notnull&Date" >
                </TD>
                <td class="title8">网点</td>
                <td class="input8">
                    <input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" />
                </td>
            </TR>
            <TR  class= common>
                <TD  class= title>
                    催缴发送
                </TD>
                <TD  class= input>
                    <Input NAME=HastenPrtNo VALUE=""  CLASS=codeNo CodeData="0|^1|已发送^2|未发送" MAXLENGTH=20  ondblclick="return showCodeListEx('HastenPrtNo',[this,HastenPrtName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('HastenPrtNo',[this,HastenPrtName],[0,1],null,null,null,1);"><input class=codename name=HastenPrtName readonly=true>
                </TD>
            </TR>
        </table>
        <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();">
        <INPUT VALUE="重  置" class= CssButton TYPE=button onclick="resetFm();">
        <INPUT class=cssbutton VALUE="清单下载" TYPE=button onclick="downloadClick();">
        <table>
            <tr>
                <td class=common>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
                </td>
                <td class= titleImg>
                    核保通过尚未收费成功的投保单信息
                </td>
            </tr>
        </table>
        <Div  id= "divLCPol1" style= "display: ''" align = center>
            <table  class= common>
                <tr  class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanPolGrid" >
                        </span>
                    </td>
                </tr>
            </table>

            <Div  id= "divPage" align=center style= "display: '' ">
                <INPUT CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage1.firstPage();">
                <INPUT CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage1.previousPage();">
                <INPUT CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage1.nextPage();">
                <INPUT CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage1.lastPage();">
            </Div>
        </div>
        <Div  id= "divLCPol2" style= "display: ''">
        <hr>
        <table>
            <tr>
                <td class=common>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
                </td>
                <td class= titleImg>
                    修改缴费信息
                </td>
            </tr>
        </table>
            <table  class= common align=center>
                <TR CLASS=common>
                    <TD CLASS=title>
                        缴费方式
                    </TD>
                    <TD CLASS=input COLSPAN=1>
                        <Input NAME=PayMode2 VALUE=""  CLASS=codeNo MAXLENGTH=20  ondblclick="return showCodeList('PayModeInd',[this,PayModeName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('PayModeInd',[this,PayModeName2],[0,1],null,null,null,1);"><input class=codename name=PayModeName2 readonly=true>
                    </TD>
                    <TD CLASS=title>
                    </TD>
                    <TD CLASS=input COLSPAN=1>
                    </TD>
                    <TD CLASS=title>
                    </TD>
                    <TD CLASS=input COLSPAN=1>
                    </TD>
                </TR>
                <TR CLASS=common>
                    <TD CLASS=title>
                        银行代码
                    </TD>
                    <TD CLASS=input COLSPAN=1>
                        <Input NAME=BankCode VALUE="" MAXLENGTH=10 CLASS=codeNo ondblclick="return showCodeList('bankcode',[this,BankCodeName],[0,1],null,fm.PayMode2.value,fm.ManageCom.value,1);" onkeyup="return showCodeList('bankcode',[this,BankCodeName],[0,1],null,fm.PayMode2.value,fm.ManageCom.value,1);" verify="code:bank" ><input class=codename name=BankCodeName readonly=true >
                    </TD>
                    <TD CLASS=title>
                        账户名称
                    </TD>
                    <TD CLASS=input COLSPAN=1>
                        <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20>
                    </TD>
                    <TD CLASS=title>
                        银行账号
                    </TD>
                    <TD CLASS=input COLSPAN=1>
                        <Input NAME=AccNo VALUE="" CLASS=common MAXLENGTH=40>
                    </TD>
                </TR>
            </table>
        </div>
        <br>
        <Div  id= "divLCPol3" style= "display: 'none'">
            <TR  class= common>
                <TD width="100%" height="15%"  class= title>转帐失败原因</TD>
            </TR>
            <TR  class= common>
                <TD height="85%"  class= title><textarea name="Content" cols="125" rows="5" class="common" ></textarea></TD>
            </TR>
            <p>
                <INPUT VALUE="保  存" class= cssButton TYPE=button onclick="SaveUnSuccreason()">
            </p>
        </div>
        <p>
            <Div  id= "divLCPol4" style= "display: ''">
            <INPUT VALUE="缴费方式修改" class= cssButton TYPE=button onclick="changePaymode()">
            <INPUT VALUE="账户信息修改" class= cssButton TYPE=button onclick="changeAcc()">
            <INPUT VALUE="缴费锁定" class= CssButton TYPE=button onclick="lock()">
            <INPUT VALUE="解除锁定" class= CssButton TYPE=button onclick="unlock()">
            <INPUT VALUE="发送催缴通知书" class= CssButton TYPE=button onclick="firstHasten()">
            </Div>
            <Div  id= "divLCPol5" style= "display: 'none'">
            <INPUT VALUE="缴费锁定" class= CssButton TYPE=button onclick="batchlock()">
            <INPUT VALUE="解除锁定" class= CssButton TYPE=button onclick="batchunlock()">
            <INPUT VALUE="发送催缴通知书" class= CssButton TYPE=button onclick="batchfirstHasten()">
            </Div>
        </p>
        <INPUT TYPE="hidden" NAME="workType">
        <INPUT VALUE="" TYPE=hidden name=gnNo>
        <INPUT VALUE="" TYPE=hidden name=pNo>

        <INPUT VALUE="" TYPE="hidden" name='PrtNo'>
        <INPUT VALUE="" TYPE="hidden" name='CustAgent'>
        <INPUT VALUE="" TYPE="hidden" name='CustCom'>
        <INPUT  type= "hidden" class= Common name= querySql >

    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
