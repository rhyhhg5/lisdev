  //               该文件中包含客户端需要处理的函数和事件
//控制界面上的mulLine的显示条数
var mulLineShowCount = 15;
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
var bankflag = "";
var userCurPage = 0;
//提交，保存按钮对应操作
//排盘导入
function submitForm() {
	var i = 0;
	getImportPath();
	ImportFile = fm.all("FileName").value;
	if (ImportFile == null || ImportFile == "") {
		alert("请选择要导入的磁盘文件");
		return;
	} else {
		fm.action = "./FFInvoiceDataSave.jsp?ImportPath=" + ImportPath;
		var a = ImportFile.lastIndexOf("\\");
		var b = ImportFile.lastIndexOf(".");
		var BatchNo = ImportFile.substring(a + 1, b);
		var showStr = "请选择要导入的磁盘文件";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo = window.open(urlStr, "", "height=250,width=500,top=250,left=270,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no");
		fm.submit();
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, InvoiceCode, InvoiceNo, ComCode) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		queryClick(InvoiceCode, InvoiceNo, ComCode);
	}
}
function queryClick(InvoiceCode, InvoiceNo, ComCode) {
	QueryGrid.clearData();
	var invCode = InvoiceCode.split(",");
	var invNo = InvoiceNo.split(",");
	var comCode = ComCode.split(",");
	for (var i = 0; i < invCode.length; i++) {
		var strSql = "select invoicecode, invoiceno,contno,xsummoney, case  when stateflag='0'then '正常' when stateflag ='2' then '已作废' end,payername,operator,opdate,comcode,memo from LOPRTInvoiceManager where invoicecode=" + invCode[i] + " and invoiceno= " + invNo[i] + " and comcode= " + comCode[i];
		var strSqlTemp = easyExecSql(strSql);
		turnPage.strQueryResult = strSqlTemp;
		if (!turnPage.strQueryResult) {
			alert("\u6ca1\u6709\u5bfc\u5165\u53d1\u7968\u4fe1\u606f");
			QueryGrid.clearData();
			return false;
		} else {
			QueryGrid.addOne();
			QueryGrid.setRowColData(i, 1, strSqlTemp[0][0]);
			QueryGrid.setRowColData(i, 2, strSqlTemp[0][1]);
			QueryGrid.setRowColData(i, 3, strSqlTemp[0][2]);
			QueryGrid.setRowColData(i, 4, strSqlTemp[0][3]);
			QueryGrid.setRowColData(i, 5, strSqlTemp[0][4]);
			QueryGrid.setRowColData(i, 6, strSqlTemp[0][5]);
			QueryGrid.setRowColData(i, 7, strSqlTemp[0][6]);
			QueryGrid.setRowColData(i, 8, strSqlTemp[0][7]);
			QueryGrid.setRowColData(i, 9, strSqlTemp[0][8]);
			QueryGrid.setRowColData(i, 10, strSqlTemp[0][9]);
		}
	}
}
function getImportPath() {
  // 书写SQL语句
	var strSQL = "";
	strSQL = "select SysvarValue from ldsysvar where sysvar = 'XmlPath' ";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("\u672a\u627e\u5230\u4e0a\u4f20\u8def\u5f84");
		return;
	}

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	ImportPath = turnPage.arrDataCacheSet[0][0];
}

