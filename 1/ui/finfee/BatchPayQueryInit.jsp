 <%
//程序名称：BatchPayQuereyInit.jsp
//程序功能：
//创建日期：2009-06-15 
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                          
 <%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String ManageCom = "";
  if(tGI==null)
  {
   ManageCom="unknown";
  }
  else //页面有效
  {
   ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  }
%>   
<script language="JavaScript">
                                
function initForm()
{
  try
  {
    fm.ComCode.value=<%=ManageCom%>;
    initCheckTempGrid();  
  }
  catch(re)
  {
    alert("BatchPayQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//待收费查询信息列表的初始化
function initCheckTempGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="暂收费号";          		//列名
      iArray[1][1]="110px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[1][4]="TempFeeNO";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
      iArray[1][9]="暂收费号|code:TempFeeNO&NOTNULL";
      iArray[1][18]=300;
      iArray[1][19]=1;                          //强制刷新

      iArray[2]=new Array();
      iArray[2][0]="关联号码";          		//列名
      iArray[2][1]="110px";      	      		//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[2][4]="OtherNO";              	        //是否引用代码:null||""为不引用
      iArray[2][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      iArray[2][6]="0|1";              	        //上面的列中放置引用代码中第几位值
      iArray[2][9]="关联号码|code:OtherNO&NOTNULL";
      iArray[2][18]=300;
      iArray[2][19]=1;  

      iArray[3]=new Array();
      iArray[3][0]="号码类型";      	   		//列名
      iArray[3][1]="60px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="号码类型|OtherNOType&NOTNULL";

      iArray[4]=new Array();
      iArray[4][0]="收费方式";      	   		//列名
      iArray[4][1]="60px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="交费金额";      	   		//列名
      iArray[5][1]="60px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="交费金额|NUM&NOTNULL";
            
      iArray[6]=new Array();
      iArray[6][0]="银行编码";      	   		//列名
      iArray[6][1]="60px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
            
      iArray[7]=new Array();
      iArray[7][0]="账号";      	   		//列名
      iArray[7][1]="100px";            			//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
            
      iArray[8]=new Array();
      iArray[8][0]="账户名";      	   		//列名
      iArray[8][1]="100px";            			//列宽
      iArray[8][2]=20;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="投保人";      	   		//列名
      iArray[9][1]="100px";            			//列宽
      iArray[9][2]=20;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[10]=new Array();
      iArray[10][0]="投保人编号";      	   		//列名
      iArray[10][1]="60px";            			//列宽
      iArray[10][2]=20;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
      CheckTempGrid = new MulLineEnter( "fm" , "CheckTempGrid" ); 
      //这些属性必须在loadMulLine前
      CheckTempGrid.mulLineCount = 0;   
      CheckTempGrid.displayTitle = 1;
      CheckTempGrid.canSel = 0;
      CheckTempGrid.locked=1;      
      CheckTempGrid.loadMulLine(iArray); 
      CheckTempGrid.selBoxEventFuncName= "initTempfeeNo"; 
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
