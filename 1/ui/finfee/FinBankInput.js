var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//添加支行
function ChildBankButt()
{
	
	if (fm.BankCode.value == null ||fm.BankCode.value == "")
	{
		alert('请选择总行代码');
		return;
	}
	if (fm.ManageCom.value == null || fm.ManageCom.value == "")
	{
		alert('请选择支行所属地区');
		return;
	}
	if (fm.ChildBankName.value == null ||fm.ChildBankName.value == "")
	{
		alert('请录入支行或分行名称');
		return;
	}
	var ManageCom = fm.ManageCom.value;
	var bankcode = fm.BankCode.value;
	if(!valProFlag(ManageCom,bankcode)){
		return false;
	}
	if(ManageCom.length == 4){
		fm.mProvincialBanch.value = '1';
	}else{
		fm.mProvincialBanch.value = '0';
	}
	fm.mOperate.value = "CHILD";
	submitForm();
	
}
//添加总行
function HeadBankButt()
{
	if (fm.HeadBankName.value == null ||fm.HeadBankName.value == "")
	{
		alert('请录入总行名称');
		return;
	}
	fm.mOperate.value = "HEAD";
	submitForm();
}
//批量导入
function BatchInput()
{
  location.href="./BatchInputList.jsp";
}

//添加银行转账参数
function DealBankButt()
{
	alert('3');
}
function afterSubmit(FlagStr,code,operate)
{
	  showInfo.close();
    window.focus;
	if (operate =="HEAD")
	{
//		alert();
	   fm.HeadBankCode.value = code;
	}
	else if(operate="CHILD")
	{
//		 alert(code);
	   fm.ChildBankCode.value = code; 
	}   
	if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "操作失败" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" +  "操作成功" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }     
	
}
//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput())
	{
  var i = 0;
  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
	}
}
//设置打印银行种类参数
function SetFlag()
{ 
  for(i = 0; i <fm.RgtState.length; i++){
    if(fm.RgtState[i].checked){
      mPrintFlag = "";
      break;
    }
    else
    {
    	mPrintFlag = "1";
    	break;
    }
}
}
//打印
function PrintList()
{
	var i = 0;
  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.target = "../f1print";
  fm.action = "./PrintBankListSave.jsp?Flag="+mPrintFlag;
  fm.submit();
	showInfo.close();	
}
//查询银行信息
function QueryBankButt()
{
	initQueryLDBankGrid();
	
	var sql="";
	sql = "select bankcode,bankname,comcode,accno,payaccno,case cansendflag when '1' then '是' else '否' end from ldbank where 1=1 "
		  + getWherePart( 'BankCode','mBankCode' )
	    + getWherePart( 'BankName','mBankName',"like"  );
	if(fm.mComCode.value!=""){
		sql+=" and ComCode like '"+fm.mComCode.value.substring(0,4)+"%'";
	}
	           
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(sql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    QueryLDBankGrid.clearData('QueryLDBankGrid');  
    alert("没有查询到数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QueryLDBankGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = sql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
}
//显示修改明细
function ShowDetail(){
	
	 var tSel = 0;
	 tSel = QueryLDBankGrid.getSelNo();
		try
		{
			if (tSel !=0 )
			   divLLMainAskInput5.style.display = "";
			else
				 divLLMainAskInput5.style.display = "none";
				 
			fm.all('repBankCode').value=QueryLDBankGrid.getRowColData(tSel-1,1);
		  fm.all('repBankName').value=QueryLDBankGrid.getRowColData(tSel-1,2);
		  fm.all('repComCode').value=QueryLDBankGrid.getRowColData(tSel-1,3);
		  fm.all('repGetAccNo').value=QueryLDBankGrid.getRowColData(tSel-1,4);
		  fm.all('repPayAccNo').value=QueryLDBankGrid.getRowColData(tSel-1,5);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
}
//修改数据
function RepBankButt()
{
	if (fm.repBankName.value == null)
	{
		alert('银行名称不能为空！');
		return;
	}
	if (fm.repComCode.value == null)
	{
		alert('银行所属机构不能为空！');
		return;
	}	
	
	var repComCode = fm.repComCode.value;
	var repBankCode = fm.repBankCode.value;
	repBankCode = repBankCode.substring(0,2);
	if(!valProFlag(repComCode,repBankCode)){
		return false;
	}
	if(repComCode.length == 4){
		fm.mProvincialBanch.value = '1';
	}else{
		fm.mProvincialBanch.value = '0';
	}
	
	fm.mOperate.value = "REPAIR";
	//fm.ChildBankCode.value = fm.all('repBankCode').value;
	//fm.ManageCom.value = fm.all('repComCode').value;
	//fm.ChildBankName.value = fm.all('repBankName').value;
	//fm.GetBankAccNo.value = fm.all('repGetAccNo').value;
	//fm.PayBankAccNo.value =fm.all('repPayAccNo').value;
	
	submitForm();	
}
//删除数据
function DelBankButt()
{
	fm.mOperate.value = "DELETE";
	//fm.ChildBankCode.value = fm.all('repBankCode').value;
	//fm.ManageCom.value = fm.all('repComCode').value;
	//fm.ChildBankName.value = fm.all('repBankName').value;
	//fm.GetBankAccNo.value = fm.all('repGetAccNo').value;
	//fm.PayBankAccNo.value =fm.all('repPayAccNo').value;
	//submitForm();
	//已存在银行接口代扣格式的银行不能进行删除
	var bankcodej=fm.all('repBankCode').value;
	var jkSQL  = " select 1 From ldbank where   bankcode = '"+ bankcodej +"' "
	           + " and ((agentpaysendf is not null and trim(agentpaysendf)<>'') "
	           + " or (agentgetsendf is not null and trim(agentgetsendf)<>'') "
	           + " or (agentpayreceivef is not null and trim(agentpayreceivef)<>'') "
	           + " or (agentgetreceivef is not null and trim(agentgetreceivef)<>'') ) "
	           + " with ur";
	var jkResult = easyExecSql(jkSQL);
	if(jkResult){
	   alert("该银行为银行接口银行，不能进行删除！");
	   return false;
	} 	
	 if (confirm("您确实想删除该记录吗?"))
   {
      var i = 0;
      var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      //fm.fmtransact.value = "DELETE||MAIN";
      fm.submit(); //提交
      fm.repBankName.value = '';
      fm.repComCode.value = '' ;
      fm.repGetAccNo.value = '' ;
      fm.repPayAccNo.value = '' ;
      fm.mBankCode.value = '' ;
      fm.mBankName.value = '';
      QueryBankButt();
    }
    else
    {
       alert("您取消了删除操作！");
    } 
}
//校验省级银行标志
function valProFlag(managecom,bankcode){
	if(managecom.length == 4){
		var valSQL  = "select 1 From ldbank where comcode = '"+ managecom +"' and  bankcode like '"+ bankcode +"%'  and provincialbanch = '1' with ur";
		var result = easyExecSql(valSQL);
		if(result != null){
			alert("该分公司在该银行下已经配置了省级分行，不能再次添加该银行的省级分行!");
			return false;
		}
	}
	return true;
}