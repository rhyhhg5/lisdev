<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TempFeeWithdrawSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  
<%
  System.out.println("\n\n---GetAdvanceDrawSave Start---");

  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String strResult = "";

  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");  
  
  LJTempFeeSet inLJTempFeeSet = new LJTempFeeSet();
  String inTempFeeNo = request.getParameter("TempFeeNo");
  String inRiskCode[] = request.getParameterValues("TempGrid1");
  String inConfMoney[] = request.getParameterValues("TempGrid3");
  String inAPPntName[] = request.getParameterValues("TempGrid4");
  String inAgentCode[] = request.getParameterValues("TempGrid6");
  String inPrtNo = request.getParameter("PrtNo"); 
  System.out.println("===" + inRiskCode.length);
  for(int i = 0 ; i<inRiskCode.length ; i++){
    LJTempFeeSchema inLJTempFeeSchema = new LJTempFeeSchema();
    inLJTempFeeSchema.setTempFeeNo(inTempFeeNo);
    inLJTempFeeSchema.setTempFeeType("1");
    inLJTempFeeSchema.setRiskCode(inRiskCode[i]);
    inLJTempFeeSchema.setPayMoney(inConfMoney[i]);
    System.out.println("金额："+ inLJTempFeeSchema.getPayMoney());
    inLJTempFeeSchema.setOtherNo(inPrtNo);
    inLJTempFeeSchema.setAgentCode(inAgentCode[i]);
    inLJTempFeeSchema.setAPPntName(inAPPntName[i]);
    inLJTempFeeSet.add(inLJTempFeeSchema);
  }

  
  LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
  
  String tTempFeeNo[] = request.getParameterValues("FeeGrid2");
  String tTempFeeType[] = request.getParameterValues("FeeGrid3");
  String tSel[] = request.getParameterValues("InpFeeGridSel"); 
  String tConfMoney[] = request.getParameterValues("FeeGrid7"); 
  String tPrtNo = request.getParameter("PrtNo"); 
  
  LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
  for(int i = 0 ; i<tTempFeeNo.length ; i++){
      if(tSel[i].equals("1")) {
         tLJTempFeeSchema.setOtherNo(tTempFeeNo[i]);
         tLJTempFeeSchema.setTempFeeType(tTempFeeType[i]);
         tLJTempFeeSchema.setPayMoney(tConfMoney[i]);
         tLJTempFeeSet.add(tLJTempFeeSchema);
      }
  }
  
  // 准备传输数据 VData
	VData tVData = new VData();
	tVData.add(tLJTempFeeSet);
	tVData.add(tG);
    tVData.add(inLJTempFeeSet);

  // 数据传输
	GetAdvanceConfUI tGetAdvanceConfUI = new GetAdvanceConfUI();
	tGetAdvanceConfUI.submitData(tVData, "INSERT");
  
  tError = tGetAdvanceConfUI.mErrors;
  System.out.println("needDeal:"+tError.needDealError());
  if (!tError.needDealError()) {                          
  	Content = " 转实收保费成功! ";
  	FlagStr = "Succ";
  	System.out.println("----------after submitData");
  }
  else {
  	Content = " 转实收保费失败，原因是:" + tError.getFirstError();
  	FlagStr = "Fail";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---GetAdvanceConfSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	//parent.fraInterface.tempFeeNoQuery(1);
</script>
</html>
