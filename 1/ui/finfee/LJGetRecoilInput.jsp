<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LJGetRecoilInput.jsp
//程序功能：反冲
//创建日期：2008-12-15
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LJGetRecoilInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LJGetRecoilInit.jsp"%>
  <title>反冲</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "./LJGetRecoilSave.jsp" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
          <td class="titleImg">请输入查询条件</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	<tr class=common>
      	<TD class= title>
   		 		收费凭证号
   			</TD>
   			<TD class= input >
   					<Input type="common" name="CertificateNo" >
   			</TD>
   			<TD class= title>
   				业务号
        </TD>
        <TD class= input>
        	<Input type="common" name="OtherNo" >
        </TD>
        <TD  class= title>
            收费方式
          </TD>          
        <TD  class= input>
            <Input class=codeNo name=ByPayMode ondblclick="return showCodeList('PayMode',[this,ByPayModeName],[0,1]);" onkeyup="return showCodeListKey('PayMode',[this,ByPayModeName],[0,1]);"><input class=codename name=ByPayModeName readonly=true >
        </TD> 
      </tr>
      <TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title></TD>
	          <TD  class= input></TD>
	    </TR>
  	</table>   
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
	<Div  id= "divResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanRecoilGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div>
	<Div  id= "divUpdate" style= "display: ''" align = left>
   <table class=common>
  		<tr class=common>
  			<TD class= title>
   				收费方式
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "paymode" 
          ondblClick="showCodeList('paymode1',[this,paymodeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKey('paymode1',[this,paymodeName],[0,1],null,null,null,1);" ><Input class="codename" name= 'paymodeName'  elementtype=nacessary readonly> 
        </TD>
        <TD  class= title>
        	银行代码
      	</TD>
      	<TD  class= input>
        	<Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('bank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName],[0,1]);" ><input class=codename name=BankCodeName readonly=true >
      	</TD>
        <TD  class= title>
        	银行帐号
        </TD>
        <TD  class= input>
        	<Input type="common" name="AccNo" >
        </TD>
  		</tr>
  		<TR class=common>  
          <TD  class= title>
              本方银行
          </TD>
          <TD  class= input>
          <Input NAME=getbankcode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('paybankcode',[this,getbankcodeName],[0,1]);" onkeyup="return showCodeListKey('paybankcode',[this,getbankcodeName],[0,1]);"  ><input class=codename name=getbankcodeName readonly=true >
          </TD>
          <TD  class= title>
             银行账号
          </TD>
          <TD  class= input>
            <Input NAME=getbankaccno CodeData="" MAXLENGTH=20 CLASS=code ondblclick="return showCodeList('paybankaccno',[this,BankCodeName],[0,1],null,fm.getbankcode.value,'BankCode');" onkeyup="return showCodeListKey('paybankaccno',[this,BankCodeName],[0,1],null,fm.getbankcode.value,'BankCode');"  >
          </TD>
          <TD  class= title>
             
          </TD>
          <TD  class= input>
          </TD>
        </TR> 
  	</table>
  	</Div>
  	<INPUT class=cssButton VALUE="保存" TYPE=button onClick="save()">
  	<input type="hidden" name="OperateType" >
  	<input type="hidden" name="TempfeeNo" >
  	<input type=hidden name=ComCode>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>