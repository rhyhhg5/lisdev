<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2007-05-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*" %>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.lis.finfee.*" %>
<%@page import="java.util.List" %>
<%
    System.out.println("start FFInvoiceExportHnSave ----");

    GlobalInput tGI = (GlobalInput) session.getValue("GI");

    String FlagStr = "";
    String Content = "";
    String transact = "";

    String sHostBasePath = application.getRealPath("/");
    String sPrtTemplate = "prtXMLTemplate/" + "liaoning_prtxml.properties";
    if (tGI.ManageCom.length()>2 && "8641".equals(tGI.ManageCom.substring(0,4))) {
         if ("0".equals(request.getParameter("Type"))){
             sPrtTemplate = "prtXMLTemplate/" + "henan_prtxml.properties";
         } else {
             sPrtTemplate = "prtXMLTemplate/" + "henanmx_prtxml.properties";
         }
    }
    String sOutPath = "printdata";

    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("invoiceStartDate", request.getParameter("InvoiceStartDate"));
    tTransferData.setNameAndValue("invoiceEndDate", request.getParameter("InvoiceEndDate"));
    tTransferData.setNameAndValue("manageCom", tGI.ManageCom);

    tTransferData.setNameAndValue("hostBasePath", sHostBasePath);
    tTransferData.setNameAndValue("prtTemplate", sPrtTemplate);
    tTransferData.setNameAndValue("outPath", sOutPath);
    tTransferData.setNameAndValue("SFState", request.getParameter("SFState"));
    tTransferData.setNameAndValue("Type",request.getParameter("Type"));
    
    System.out.println("invoiceStartDate : " + request.getParameter("InvoiceStartDate"));
    System.out.println("invoiceEndDate : " + request.getParameter("InvoiceEndDate"));
    System.out.println("manageCom : " + tGI.ManageCom);

    System.out.println("hostBasePath : " + sHostBasePath);
    System.out.println("prtTemplate : " + sPrtTemplate);
    System.out.println("outPath : " + sOutPath);
    System.out.println("SFState : " + request.getParameter("SFState"));
    System.out.println("Type : " + request.getParameter("Type"));
    VData tVData = new VData();
    tVData.add(tTransferData);
    tVData.add(tGI);

    FFIvoiceExportHnUI tFFIvoiceExportHnUI = new FFIvoiceExportHnUI();

    if(!tFFIvoiceExportHnUI.submitData(tVData,"EXPORT"))
    {
        FlagStr = "Fail";
        Content = tFFIvoiceExportHnUI.mErrors.getFirstError().toString();
    }

    if (FlagStr=="")
    {
        CErrors tError = tError = tFFIvoiceExportHnUI.mErrors;
        if (!tError.needDealError())
        {
            Content = " 数据导出成功! ";
            FlagStr = "Success";
            transact = "";
            //request.setAttribute("result", tFFIvoiceExportHnUI.getResult());
            List result = tFFIvoiceExportHnUI.getResult();
            if(result.size() > 0)
            {
                for(int i=0; i<result.size(); i++)
                {
                    transact += result.get(i) + "|";
                }
            }
        }
        else
        {
            Content = " 数据导出失败，原因是: " + tError.getFirstError();
            FlagStr = "Fail";
        }
    }

    System.out.println("end FFInvoiceExportSave ----");
%>

<html>
<script language="javascript">
    try
    {
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
    }
    catch(e)
    {
    }
</script>
</html>