//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var arrDataSet;
// 查询按钮
function easyQueryClick()
{	
	// 初始化表格
	initPolGrid();
   var ContNo=fm.all("ContNo").value;
   var ManageCom=fm.all("ManageCom").value;
	// 书写SQL语句
	var strSQL = "";
     strSQL = "select a.grpcontno,a.grpname,b.othersign from lcgrpcont a,ldcode b where a.grpcontno=b.codealias and b.codetype='TJAccident' and b.code=(select trim(CHAR(YEAR(cinvalidate))) from lcgrpcont where grpcontno=a.grpcontno fetch first 1 rows only)||'GrpContNo' "
        + getWherePart('grpcontno','ContNo');
     strSQL = strSQL + getWherePart( 'a.ManageCom','ManageCom','like' ) ;
     strSQL =  strSQL + " union select c.grpcontno,c.grpname,d.othersign from lbgrpcont c,ldcode d where c.grpcontno=d.codealias and d.codetype='TJAccident' and d.code=(select trim(CHAR(YEAR(cinvalidate))) from lcgrpcont where grpcontno=c.grpcontno fetch first 1 rows only)||'GrpContNo'  "
        + getWherePart('grpcontno','ContNo');
     strSQL = strSQL + getWherePart( 'c.ManageCom','ManageCom','like' ) ;
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 

     
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
   // alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
    
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  PolGrid.SortPage=turnPage;  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);   
}

function showdivLCPol2() {
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1)=='')
  {
    	 fm.all("divLCPol2").style.display = "none";
  }
else
{    var fee=PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3);
	  fm.all("divLCPol2").style.display = "";
     fm.all("tContNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
     fm.all("Feevalue").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3);
	 
}	  
}

function save()
{  
fm.all('WorkType').value="save";
    //首先检验录入框

  if(!checkValue()) 
  {
  fm.all('tContNo').value="";
  fm.all('Feevalue').value="";
  return false;
  } //附加检验
  if(fm.all('tContNo').value!=""&&fm.all('Feevalue').value!="")
  {  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交
  showInfo.close();
  }
  else
   alert("请先查询！");
}

function change()
{ fm.all('WorkType').value="change";
    //首先检验录入框
  if(!checkValue()) 
  {
  fm.all('tContNo').value="";
  fm.all('Feevalue').value="";
  return false;
  } //附加检验
  if(fm.all('tContNo').value!=""&&fm.all('Feevalue').value!="")
  {  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交
  showInfo.close();
  }
  else
   alert("请先查询！");
}



function del()
{
fm.all('WorkType').value="delete";
    //首先检验录入框
  if(!checkValue()) 
  {
  fm.all('tContNo').value="";
  fm.all('Feevalue').value="";
  return false;
  } //附加检验
  if(fm.all('tContNo').value!="")
  {  
  fm.all('Feevalue').value="";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交
  showInfo.close();
  }
  else
   alert("请先查询！");
}

function ispercent(source) {      
return /^((-|\+)?\d{1,2}(\.\d+)?|100)%$/.test(source);   
}



function checkValue()
{ 
var percent=fm.all('Feevalue').value;
  //页面没有录入
 
  if(fm.all('tContNo').value==""||fm.all('Feevalue').value=="")
  {
      	alert("保单号或者社保报销比例不能为空！");
      	return false;
  }else if(fm.all('Feevalue').value.substring(0,1) == "-") { 
        alert("报销比例不能为负")
        return false;
  }else{
  if(ispercent(percent)){
     //fm.all('Feevalue').value=parseFloat(percent)/100;
     	var strSql = "";
    strSql = "select codealias,othersign from ldcode  where codetype='TJAccident' and code=(select trim(CHAR(YEAR(cinvalidate))) from lcgrpcont where grpcontno=ldcode.codealias fetch first 1 rows only)||'GrpContNo'" 
            + getWherePart('codealias','tContNo');              
                                                                                                           
      var arrResult = easyExecSql(strSql);  
      if(fm.all('WorkType').value=="save"){                                                                                
          if(arrResult[0][1]==''){
             return true;
           }else{
            alert("该保单已经存在社保报销比例，如需修改，请点击修改按钮！");
            return false;
            } 
      }else if(fm.all('WorkType').value=="change"){
           if(arrResult[0][1]==''){
            alert("该保单不存在社保报销比例，如需新加，请点击保存按钮！");
            return false;   
           }else{
             return true; 
            } 
     }
     else if(fm.all('WorkType').value=="delete"){
           if(arrResult[0][1]==''){
            alert("该保单不存在社保报销比例，不能删除！");
            return false;   
           }else{
             return true; 
            } 
     }
      }

    else{
    alert("请填写正确的百分比！");
    return false;

      }
                                                                                                                   
  }	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content  )
{    
      if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     
    //执行下一步操作
  }
}
function resetFm()
{
  fm.all('WorkType').value="";
  fm.all('tContNo').value="";
  fm.all('Feevalue').value="";
  fm.all('ContNo').value = '';
  initForm();
}
