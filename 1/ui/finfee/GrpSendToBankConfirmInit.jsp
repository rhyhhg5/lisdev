<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
    GlobalInput globalInput = (GlobalInput)session.getValue("GI");
    String strManageCom = globalInput.ComCode;
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{
    try
    {
        // 查询条件
        fm.all('AppntName').value = '';
        fm.all('ManageCom').value = '';
        fm.all('OtherNo').value = '';
        fm.all('AgentCode').value = '';
        fm.all('workType').value = '';
        fm.all('PayMode').value = '4';
        fm.all('PayModeName').value = '银行转账';
        divLCPol3.style.display="none";

        //初始化生效日期
        var dateSql = "select (Current Date - 1 month), (Current Date + 1 month) from dual";
        var arr = easyExecSql(dateSql);
        if(arr){
            fm.all('StartCValiDate').value = arr[0][0];
            fm.all('EndCValiDate').value = arr[0][1];
        }
    }
    catch(ex)
    {
        alert("在GrpSendToBankConfirmInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initSelBox()
{
    try
    {
    }
    catch(ex)
    {
        alert("在FeeInvoiceInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
}
function initForm()
{
    try
    {
        initInpBox();
        initSelBox();
        initPolGrid();
        fm.all('ManageCom').value = <%=strManageCom%>;
        if(fm.all('ManageCom').value==86){
            fm.all('ManageCom').readOnly=false;
        }
        else
        {
            fm.all('ManageCom').readOnly=true;
        }
        if(fm.all('ManageCom').value!=null)
        {
            var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('ManageComName').value=arrResult[0][0];
            }
        }
        if(tType=="batch"){
           divLCPol2.style.display="none";
           divLCPol4.style.display="none";
           divLCPol5.style.display="";
        }else{
           divLCPol2.style.display="";
           divLCPol4.style.display="";
           divLCPol5.style.display="none";
        }

    }
    catch(re)
    {
        alert("GrpSendToBankConfirmInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

// 保单信息列表的初始化
function initPolGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="缴费通知书号";         		//列名
        iArray[1][1]="100px";            		//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

        iArray[2]=new Array();
        iArray[2][0]="印刷号";         		//列名
        iArray[2][1]="100px";            		//列宽
        iArray[2][2]=100;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[3]=new Array();
        iArray[3][0]="投保人";         		//列名
        iArray[3][1]="60px";            		//列宽
        iArray[3][2]=100;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[4]=new Array();
        iArray[4][0]="生效日";         		//列名
        iArray[4][1]="80px";            		//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[5]=new Array();
        iArray[5][0]="保费";         		//列名
        iArray[5][1]="60px";            		//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[6]=new Array();
        iArray[6][0]="缴费方式";         		//列名
        iArray[6][1]="60px";            		//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[7]=new Array();
        iArray[7][0]="状态";         		//列名
        iArray[7][1]="60px";            		//列宽
        iArray[7][2]=100;            			//列最大值
        iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[8]=new Array();
        iArray[8][0]="失败原因";         		//列名
        iArray[8][1]="100px";            		//列宽
        iArray[8][2]=100;            			//列最大值
        iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许

        iArray[9]=new Array();
        iArray[9][0]="是否锁定";         		//列名
        iArray[9][1]="60px";            		//列宽
        iArray[9][2]=100;            			//列最大值
        iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[10]=new Array();
        iArray[10][0]="业务员";         		//列名
        iArray[10][1]="80px";            		//列宽
        iArray[10][2]=100;            			//列最大值
        iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[11]=new Array();
        iArray[11][0]="管理机构";         		//列名
        iArray[11][1]="80px";            		//列宽
        iArray[11][2]=100;            			//列最大值
        iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[12]=new Array();
        iArray[12][0]="转帐失败原因";         		//列名
        iArray[12][1]="80px";            		//列宽
        iArray[12][2]=100;            			//列最大值
        iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[13]=new Array();
        iArray[13][0]="手工录入转帐失败原因";         		//列名
        iArray[13][1]="125px";            		//列宽
        iArray[13][2]=100;            			//列最大值
        iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[14]=new Array();
        iArray[14][0]="销售渠道";         		//列名
        iArray[14][1]="60px";            		//列宽
        iArray[14][2]=100;            			//列最大值
        iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[15]=new Array();
        iArray[15][0]="最早缴费日期";         		//列名
        iArray[15][1]="80px";            		//列宽
        iArray[15][2]=100;            			//列最大值
        iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        PolGrid = new MulLineEnter( "fm" , "PolGrid" );
        PolGrid.mulLineCount = 0;
        PolGrid.displayTitle = 1;
        PolGrid.hiddenPlus = 1;
        PolGrid.hiddenSubtraction = 1;
        if(tType=="batch"){
           PolGrid.canSel = 0;
           PolGrid.canChk = 1;
        }else{
           PolGrid.canSel = 1;
           PolGrid.canChk = 0;
           PolGrid.selBoxEventFuncName = "showBankInfo";
        }
        

        PolGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>