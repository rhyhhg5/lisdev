
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body >
<Form method=post name=fm target="fraSubmit">
	<br>
	<br> 
	<font size=3>
	<b>提盘规则说明</b> 
		<p></p>
		&nbsp;&nbsp;1、收付费方式不为银行转账，则银行接口和集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;2、若客户开户银行、开户账号、开户名任意一个为空，则银行接口和集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;3、若未到应收/应付费日期，银行接口、集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;4、若已过最晚缴费日期，集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;5、若处于加锁状态，则银行接口和集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;6、若处于银行在途状态，则该笔数据已提取。
		<p></p>
		&nbsp;&nbsp;7、若客户开户银行不为银联、通联子银行，则集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;8、若客户开户账号含有特殊符号，不符合集中代收付账号规则，则集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;9、若为对公业务，则集中代收付不会进行提取。
		<p></p>
		&nbsp;&nbsp;10、对于付费数据，若付费金额为0，则银行接口和集中代收付不会进行提取。

</font>
	<table class= common>
		<tr><td align=center><input type =button class=cssButton value="关 闭" onclick="window.close();"></td></tr>
</table>

</body>
</html>
