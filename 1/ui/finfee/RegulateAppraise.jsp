<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2003-03-26 15:39:06
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="RegulateAppraise.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel="stylesheet"
			type="text/css">
		<%@include file="RegulateAppraiseInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">
			<Div id="divOperator" style="display: ''">
				<strong>请输入查询的时间范围（旧财务规则）</strong>
				<table class="common">
					<TR class=common>
						<TD class=title>
							起始时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" id="StartDay"
								name="StartDay" verify="起始时间|NOTNULL">
						</TD>
						<TD class=title>
							结束时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" id="EndDay"
								name="EndDay" verify="结束时间|NOTNULL">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							同比起始时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short"
								id="ContrastStartDay" name="ContrastStartDay"
								verify="起始时间|NOTNULL">
						</TD>
						<TD class=title>
							同比结束时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short"
								id="ContrastEndDay" name="ContrastEndDay" verify="结束时间|NOTNULL">
						</TD>
					</TR>
				</table>
				<br>
				<input class="cssButton" style="width: 70px" type="Button"
					name="SSPrint" value="查  询" onclick="Print()" />

				<input type="hidden" id="curManageCom" name="curManageCom" />
				<input type="hidden" id="fmtransact" name="fmtransact" />
				<br>
				<br>
			</Div>
		</form>
        <form method=post name=fm1 target="fraSubmit">
			<Div id="divOperator1" style="display: ''">
				<strong>请输入查询的时间范围（新财务规则，适用于2011年1月之后的数据）</strong>
				<table class="common">
					<TR class=common>
						<TD class=title>
							起始时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" id="StartDay1"
								name="StartDay1" verify="起始时间|NOTNULL">
						</TD>
						<TD class=title>
							结束时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" id="EndDay1"
								name="EndDay1" verify="结束时间|NOTNULL">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							同比起始时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short"
								id="ContrastStartDay1" name="ContrastStartDay1"
								verify="起始时间|NOTNULL">
						</TD>
						<TD class=title>
							同比结束时间
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short"
								id="ContrastEndDay1" name="ContrastEndDay1" verify="结束时间|NOTNULL">
						</TD>
					</TR>
				</table>
				<br>
				<input class="cssButton" style="width: 70px" type="Button"
					name="SSPrint1" value="查  询" onclick="Print1()" />

				<input type="hidden" id="curManageCom1" name="curManageCom1" />
				<input type="hidden" id="fmtransact1" name="fmtransact1" />
				<br>
				<br>
			</Div>
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
