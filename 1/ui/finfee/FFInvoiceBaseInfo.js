var showInfo;
var turnPage = new turnPageClass();

function easyQueryClick()
{
    var strSql = "select ComCode, TaxpayerNo, TaxpayerName, InvoiceCode, InvoiceCodeExp, InvoiceStartNo, InvoiceEndNo, Operator " 
        + " from LJInvoiceInfo "
        + " where ComCode like '" + ManageCom + "%' "
        + getWherePart('TaxpayerNo', 'TaxpayerNo')
        + getWherePart('TaxpayerName', 'TaxpayerName','like')
        + getWherePart('InvoiceCode', 'InvoiceCode','like')
        + getWherePart('InvoiceCodeExp', 'InvoiceCodeExp','like')
        + getWherePart('InvoiceStartNo', 'InvoiceStartNo','>=')
        + getWherePart('InvoiceEndNo', 'InvoiceEndNo','<=');
    turnPage.queryModal(strSql, FFInvoiceBaseInfoGrid);
    divUpdate.style.display = "none";
}

function easyQueryCom()
{
    var strSql = "select OutComCode, LetterServiceName, ShortName from ldcom where ComCode = '" + ManageCom + "'";
    var arr = easyExecSql(strSql);
    if(arr != null)
    {
        fm.all('OutComCode').value = arr[0][0];
        fm.all('LetterServiceName').value = arr[0][1];
        fm.all('ShortName').value = arr[0][2];
        fm.all('Nsrsbh').value = "";
        fm.all('outputTemplate').value = "";
    }
}

function addIBaseInfo()
{
    if (!verifyInput2()) 
    {
        return false;
    }
    if(fm.InvoiceEndNo.value - fm.InvoiceStartNo.value < 0)
    {
        alert("发票终止号应大于发票起始号");
        return false;
    }
    fm.fmtransact.value = "INSERT" ;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
    return true;
}

function afterSubmit(FlagStr, content, transact)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        if(transact == "UPDATE"){
            easyQueryClick();
        }
		//showDiv(operateButton,"true"); 
		//showDiv(inputButton,"false"); 
		//执行下一步操作
	}
}

function onclkSelBox(){
    var tSel = FFInvoiceBaseInfoGrid.getSelNo();
    fmUpdate.upComCode.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,1);
    fmUpdate.upInvoiceCode.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,4);
    fmUpdate.upInvoiceStartNo.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,6);
    fmUpdate.upInvoiceEndNo.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,7);
    fmUpdate.upTaxpayerNo.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,2);
    divUpdate.style.display = "";
}

function updateInvoice(){
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fmUpdate.submit(); 
    return true;
}