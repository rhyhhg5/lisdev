  //               该文件中包含客户端需要处理的函数和事件
//控制界面上的mulLine的显示条数
var mulLineShowCount = 15;
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var bankflag="";
var userCurPage = 0;
//提交，保存按钮对应操作

//排盘导入
function submitForm()
{
  var i = 0;
  getImportPath();
  ImportFile = fm.all('FileName').value;
  if ( ImportFile == null ||
       ImportFile == "")
  {
    alert("请浏览要导入的磁盘文件");
    return;
  }
  else
  {
    fm.action = "./TempFeeBatchSave.jsp?ImportPath=" + ImportPath;
    var a= ImportFile.lastIndexOf("\\");
    var b= ImportFile.lastIndexOf(".");
    var BatchNo = ImportFile.substring(a+1,b);
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.open(urlStr,"",'height=250,width=500,top=250,left=270,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
    fm.submit();
    
  }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result )
{    
      showInfo.close();
      if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    queryClick(Result);
  }
  
 
}

function queryClick(Result)
{
    var strSql = "select distinct tf.tempfeeno,tf.tempfeetype,tf.otherno,tfc.paymode,tfc.paymoney,tfc.insbankcode,tfc.insbankaccno from ljtempfeeclass tfc,ljtempfee tf where tfc.tempfeeno=tf.tempfeeno and tfc.tempfeeno in " + Result;
    var strSqlTemp=easyQueryVer3(strSql, 1, 0, 1); 
    turnPage.strQueryResult=strSqlTemp;
     
     if(!turnPage.strQueryResult)
     {
        window.alert("没有导入收费信息");
        QueryGrid.clearData();
        return false;
     }
     else
     {    
        turnPage.queryModal(strSql, QueryGrid);
     }
}

function getImportPath ()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar = 'XmlPath' ";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("未找到上传路径");
    return;
  }

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  ImportPath = turnPage.arrDataCacheSet[0][0];
}
