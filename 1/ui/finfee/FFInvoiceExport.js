var showInfo;

function Export()
{
    if (!verifyInput2()) 
	{
		return false;
	}
    var bDate = toDate(fm.InvoiceStartDate.value);
    var eDate = toDate(fm.InvoiceEndDate.value);
    if(bDate == null || eDate == null)
    {
        alert("日期格式不正确");
        return false;
    }
    if((eDate - bDate) < 0)
    {
        alert("起始日期不能大于终止日期");
        return false;
    }
    if(fm.SFState.value == null || fm.SFState.value == "") {
       alert("收付费发票标记为空");
       return false;
    }
    if(fm.Type.value == null || fm.Type.value == "") {
       alert("导出类型为空");
       return false;
    }
    //var showStr = "正在进行数据导出，请您稍候并且不要修改屏幕上的值或链接其他页面";
    //var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    //showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    document.getElementById("filesList").innerHTML = "<font color='red'>正在生成数据文件......</font>";
    fm.submit();
    return true;
}

function toDate(str)
{
    var pattern = /^(\d{4})(-)(0?[1-9]|1[0-2])(-)(0?[1-9]|[12][0-9]|3[01])$/g; 
    var arr = pattern.exec(str);
    if (arr == null)
        return null;
    var date = new Date(arr[1], arr[3]-1, arr[5]);
    return date;
}

function afterSubmit(FlagStr, content, transact)
{
    if(showInfo != null && showInfo != "undefined")
	    showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//showDiv(operateButton,"true"); 
		//showDiv(inputButton,"false"); 
		//执行下一步操作
        var tfilesList = document.getElementById("filesList");
        tfilesList.innerHTML = "";
        if(transact != null && transact != "undefined")
        {
            var filename = transact.split("|");
            for(index in filename)
            {
                if(filename[index] != "")
                {
                    var tSkip = filename[index].lastIndexOf("/")==-1?0:filename[index].lastIndexOf("/")+1;
                    tfilesList.innerHTML +=  "<br /> 下载 [<a href='../" + filename[index] + "' >" + filename[index].substring(tSkip) + "</a>]";
                }
            }
        }
	}
}