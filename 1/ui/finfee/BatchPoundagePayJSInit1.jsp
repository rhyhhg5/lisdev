 <%
//程序名称：BatchPoundagePayInit.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                          
 <%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String ManageCom = "";
  if(tGI==null)
  {
   ManageCom="unknown";
  }
  else //页面有效
  {
   ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  }
%>   
<script language="JavaScript">
                                
function initForm()
{
  try
  {
	fm.ComCode.value=<%=ManageCom%>;
    initLJAGetGrid();  
  //  showDiv(divShowTotal,"false");
  }
  catch(re)
  {
    alert("BatchPayInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initLJAGetGrid()
{           
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";   		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=70;            		//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许   

      iArray[2]=new Array();
      iArray[2][0]="凭证登记码";		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="实付结果";		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=60;            		//列最大值
      iArray[3][3]=0;  
   
      LJAGetGrid = new MulLineEnter( "fm" , "LJAGetGrid" );   
      LJAGetGrid.displayTitle = 1;
      LJAGetGrid.canChk = 1;
      LJAGetGrid.hiddenPlus = 1;
      LJAGetGrid.hiddenSubtraction = 1;
      
      LJAGetGrid.loadMulLine(iArray);
	  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
