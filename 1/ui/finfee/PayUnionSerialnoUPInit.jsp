
<%
	//程序名称：BatchPoundagePayInit.jsp
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">
	function initForm() {
		try {
			initQueryLCPaySerialnoHZGrid();
		} catch (re) {
			alert("PayUnionSerialnoUPInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	function initQueryLCPaySerialnoHZGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "实付号"; //列名
			iArray[1][1] = "70px"; //列宽
			iArray[1][2] = 70; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许   

			iArray[2] = new Array();
			iArray[2][0] = "业务号"; //列名
			iArray[2][1] = "70px"; //列宽
			iArray[2][2] = 60; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "给付机构"; //列名
			iArray[3][1] = "45px"; //列宽
			iArray[3][2] = 60; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array();
			iArray[4][0] = "给付含税金额"; //列名
			iArray[4][1] = "45px"; //列宽
			iArray[4][2] = 60; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "收款方名称"; //列名
			iArray[5][1] = "45px"; //列宽
			iArray[5][2] = 60; //列最大值
			iArray[5][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[6] = new Array();
			iArray[6][0] = "险种编码"; //列名
			iArray[6][1] = "50px"; //列宽
			iArray[6][2] = 60; //列最大值
			iArray[6][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[7] = new Array();
			iArray[7][0] = "平台是否读取"; //列名
			iArray[7][1] = "50px"; //列宽
			iArray[7][2] = 60; //列最大值
			iArray[7][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[7][4] = 1;
			
			LCPaySerialnoHZGrid = new MulLineEnter("fm", "LCPaySerialnoHZGrid");
			LCPaySerialnoHZGrid.displayTitle = 1;

			LCPaySerialnoHZGrid.hiddenPlus = 1;
			LCPaySerialnoHZGrid.hiddenSubtraction = 1;
			LCPaySerialnoHZGrid.canChk = 1;

			LCPaySerialnoHZGrid.chkBoxEventFuncName = "EventOnMulLine";
			LCPaySerialnoHZGrid.chkBoxAllEventFuncName = "EventOnMulLineTotal";
			LCPaySerialnoHZGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}
</script>
