var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

/**
 * 后台数据库查询
 */
function easyQueryClick() {
  // 拼SQL语句，从页面采集信息
  var strSql = "select a.ActuGetNo, a.OtherNo, a.OtherNoType,a.PayMode,a.GetMoney,a.EnterAccDate,"
             + "a.ConfDate,a.Drawer,a.DrawerID,a.BankCode,a.ChequeNo,b.BankAccNo,a.AccName,b.insbankcode,b.insbankaccno, "
             +" (case b.othernotype when '3' then (select grpcontno from ljagetendorse where endorsementno=b.otherno fetch first 1 rows only)  " 
			       +" when '4' then b.otherno " 
			       +" when '5' then (select case grpcontno when '00000000000000000000' then contno else grpcontno end from ljagetclaim where otherno=b.otherno fetch first 1 rows only) " 
		         +" when 'C' then (select case grpcontno when '00000000000000000000' then contno else grpcontno end from ljagetclaim where otherno=b.otherno fetch first 1 rows only) " 
		      	 +" when 'F' then (select case grpcontno when '00000000000000000000' then contno else grpcontno end from ljagetclaim where otherno=b.otherno fetch first 1 rows only) " 
		      	 +" when '10' then (select contno from ljagetendorse where endorsementno=b.otherno fetch first 1 rows only)  " 
		      	 +" when '12' then b.otherno " 
	      		 +" when '13' then (select contno from ljagetendorse where endorsementno=b.otherno fetch first 1 rows only)  end),b.appntno, " 
	      		 +" (case b.othernotype when '3' then '团体保全付费'  " 
	      		 +" when '4' then '暂收退费' " 
		      	 +" when '5' then '理赔赔款' " 
		      	 +" when 'C' then '理赔查勘费赔款' " 
	      		 +" when 'F' then '理赔查勘费费用' " 
	      		 +" when '10' then '个单保全付费' " 
	      		 +" when '12' then '余额领取' " 
		      	 +" when '13' then '定期结算' end),b.Drawer  " 
             + "from LJFIGet a,ljaget b where a.ActuGetNo = b.ActuGetNo "
	           + getWherePart( 'a.ActuGetNo','ActuGetNo','like' )
	           + getWherePart( 'a.OtherNo','OtherNo','like' )
	           + getWherePart( 'a.PayMode','PayMode' )
	           + getWherePart( 'a.GetMoney','GetMoney','=','1' )
	           + getWherePart( 'a.ConfDate','ConfDate' )   
	           +"with ur" ;       

	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    QueryLJFIGetGrid.clearData('QueryLJFIGetGrid');  
    alert("没有查询到数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QueryLJFIGetGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
}

function returnParent()
{
	var arrReturn = new Array();
	var tSel = QueryLJFIGetGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
		top.opener.fm.all('ActuGetNo').value=QueryLJFIGetGrid.getRowColData(tSel-1,1);
		top.opener.fm.all('PolNo').value=QueryLJFIGetGrid.getRowColData(tSel-1,2);
		top.opener.fm.all('PayMode').value=QueryLJFIGetGrid.getRowColData(tSel-1,4);
		top.opener.fm.all('GetMoney').value=QueryLJFIGetGrid.getRowColData(tSel-1,5);
		top.opener.fm.all('EnterAccDate').value=QueryLJFIGetGrid.getRowColData(tSel-1,6);
	//	top.opener.fm.all('ConfDate').value=QueryLJAGetGrid.getRowColData(tSel-1,7);
	  top.opener.fm.all('Drawer').value=QueryLJFIGetGrid.getRowColData(tSel-1,8);
	  top.opener.fm.all('DrawerID').value=QueryLJFIGetGrid.getRowColData(tSel-1,9);
	  top.opener.fm.all('BankCode').value=QueryLJFIGetGrid.getRowColData(tSel-1,10);
	  top.opener.fm.all('ChequeNo').value=QueryLJFIGetGrid.getRowColData(tSel-1,11);
	  top.opener.fm.all('BankAccNo').value=QueryLJFIGetGrid.getRowColData(tSel-1,12);
	  top.opener.fm.all('AccName').value=QueryLJFIGetGrid.getRowColData(tSel-1,13);
	  top.opener.fm.all('getbankcode').value=QueryLJFIGetGrid.getRowColData(tSel-1,14);
	  top.opener.fm.all('getbankaccno').value=QueryLJFIGetGrid.getRowColData(tSel-1,15);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
		top.close();
	}
}