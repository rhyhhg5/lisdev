<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="APbalanceInput.js"></SCRIPT>
  <%@include file="APbalanceInputInit.jsp"%>
  <title>付费管理 </title>
</head>

<body  onload="initForm();" >
  <form action="./APbalanceInputSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
            <TD  class= title>
           保单号码
          </TD>
          <TD  class= input>
            <Input class= common  name=ContNo >
          </TD>
         </TR>
         <TR>
        	<TD  class= title> 起始日期</TD>
	        <TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=StartDate >  </TD>        
	        <TD  class= title> 截至日期</TD> 
	        <TD  class= input> <Input class= "coolDatePicker" dateFormat="short" name=EndDate > </TD>

      	</TR>
        
    </table>
          <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="重  置" class= CssButton TYPE=button onclick="resetFm();"> 


  <table>                                                                                                           
    	<tr>                                                                                                            
        	<td class=common>                                                                                           
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">     
    		</td>                                                                                                         
    		<td class= titleImg>                                                                                          
    			 待结算案件                                                                                                   
    		</td>                                                                                                         
    	</tr>                                                                                                           
    </table>                                                                                                          
  	                                                                                                                  
  	<Div  id= "divLCPol1" style= "display: ''" align = center>                                                        
      	<table  class= common>                                                                                        
       		<tr  class= common>                                                                                         
      	  		<td text-align: left colSpan=1>                                                                         
  					<span id="spanPolGrid" >                                                                                  
  					</span>                                                                                                   
  			  	</td>                                                                                                     
  			</tr>                                                                                                         
    	</table>                                                                                                        
    	                                                                                                                
      <Div  id= "divPage" align=center style= "display: '' ">                                                         
      <INPUT CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();">             
      <INPUT CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();">              
      <INPUT CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();">              
      </Div> 			                                                                                                        
  	</div>     
  		                                                                                                        
      <INPUT VALUE="选择完毕" class= cssButton TYPE=button onclick="ok()">                                                                                                           
    <br>   
    	<Div  id= "divLCPol2"  name=BankInfo style= "display: 'none'">                                                    
    <table>                                                                                                           
    	<tr>                                                                                                            
    		<td class= titleImg>                                                                                          
    			 社保报销比例                                                                                               
    		</td>                                                                                                         
    	</tr>                                                                                                           
    </table>          	                                                                                              
    <table  class= common align=center>                                                                               
                                                                                                                      
    	<TR CLASS=common>	                                                                                              
        <TD  class= title>                                                                                             
           保单号码                                                                                                   
        </TD>                                                                                                         
        <TD CLASS=input COLSPAN=1>                                                                                    
          <Input NAME=tContNo VALUE="" CLASS=common MAXLENGTH=20 READONLY=ture>                                                      
        </TD>                                                                                                         
        <TD CLASS=title>                                                                                              
          社保报销比例                                                                                                
        </TD>  	                                                                                                       
        <TD CLASS=input >                                                                                    
          <Input NAME=Feevalue VALUE="" CLASS=common MAXLENGTH=20 READONLY=ture>                                                     
        </TD>  
        </TR> 
        <TR CLASS=common>
         <TD  class= title>                                                                                             
           垫付金额汇总                                                                                                  
        </TD>                                                                                                         
        <TD CLASS=input COLSPAN=1>                                                                                    
          <Input NAME=tPayNo VALUE="" CLASS=common MAXLENGTH=20 READONLY=ture>                                                      
        </TD> 
         <TD CLASS=title>                                                                                              
          社保报销金额汇总                                                                                               
        </TD>  	                                                                                                       
        <TD CLASS=input >                                                                                    
          <Input NAME=tGetNo VALUE="" CLASS=common MAXLENGTH=20 READONLY=ture>                                                      
        </TD>  
                                                                                                       
      </TR>                                                                                                           
    </table>                                                                                                          
    <br>                                                                                                              
                                                                                                                      
  	                                                                                                         
  	<p>                                                                                                               
    <INPUT VALUE="保存" class= cssButton TYPE=button onclick="save()">                                                		                                        
                                                                                                                      
  	</p>   
  	</div>                                    
  	<INPUT VALUE="" TYPE=hidden name=ManageCom>	
  	<INPUT VALUE="" TYPE=hidden name=WorkType>	
 
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
