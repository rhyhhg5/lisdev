<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BatchSendQueryInput.jsp
//程序功能：清单下载
//创建日期：2012-2
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String YEAR=String.valueOf(cal.get(Calendar.YEAR));
  String MONTH=String.valueOf(cal.get(Calendar.MONTH));
  String DATE=String.valueOf(cal.get(Calendar.DATE));
  String manageCom = request.getParameter("ManageCom");
  String sql = "select name from ldcom where comcode='" + manageCom + "' ";
  ExeSQL tExeSQL = new ExeSQL();
  SSRS mSSRS = tExeSQL.execSQL(sql);
  String ConfrimFlag = request.getParameter("ConfrimFlag");
  System.out.println("ConfrimFlag"+ConfrimFlag);
  String downLoadFileName = "";
  if(ConfrimFlag.equals("1")){
	   downLoadFileName = "未结算清单_"+mSSRS.GetText(1, 1)+ YEAR + MONTH+DATE + ".xls";
  }else {
	   downLoadFileName = "已结算清单_"+mSSRS.GetText(1, 1)+ YEAR + MONTH+DATE + ".xls";
  }
  
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  String querySql = request.getParameter("querySql");
 
  SSRS tSSRS = tExeSQL.execSQL(querySql.replaceAll("%25","%"));
  System.out.println("-----sql------" + querySql.replaceAll("%25","%"));
  System.out.println("----" + tSSRS.getMaxRow());
  String[][] mToExcel = new String[4000][30];
        mToExcel[0][0] = "序号";
        mToExcel[0][1] = "暂收费号";
        mToExcel[0][2] = "保单号";
        mToExcel[0][3] = "结算金额";
        mToExcel[0][4] = "结算时间（YYYY-MM-DD）";
        mToExcel[0][5] = "本方开户银行";
        mToExcel[0][6] = "本方银行账户";
        
        int row;
        for( row = 1; row <= tSSRS.getMaxRow(); row++)
        {
        	
            for(int col = 1; col <= tSSRS.getMaxCol() - 2; col++)
            {
            	mToExcel[row][0]= Integer.toString(row);
                mToExcel[row][col] = tSSRS.GetText(row, col);
            }
            
        }        
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
  //返回客户端
  if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
  out.clear();
    out = pageContext.pushBody();
%>