<%
//程序名称：FinFeePaySave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>    
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  
<%@page contentType="text/html;charset=GBK" %>
<%
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {   
	  String ActuGetNo 		= request.getParameter("ActuGetNo");;
	  String PolNo 				= request.getParameter("PolNo");
	  String PayMode 			= request.getParameter("PayMode");
	  String GetMoney 		= request.getParameter("GetMoney");
	  String AgentCode 		= request.getParameter("AgentCode");
	  String Drawer 			= request.getParameter("Drawer");
	  String AgentGroup 	= request.getParameter("AgentGroup");   
	  String DrawerID 		= request.getParameter("DrawerID");   
	  String Operator 		= request.getParameter("Operator");   
	  String ModifyDate 	= request.getParameter("ModifyDate");  
	  String ModifyTime 	= request.getParameter("ModifyTime");  
	   
	  String BankCode 		= request.getParameter("BankCode");        
	  String ChequeNo 		= request.getParameter("ChequeNo");
	  String BankAccNo 		= request.getParameter("BankAccNo");
	  String AccName 			= request.getParameter("AccName");   
	  String InsBankAccNo = request.getParameter("InsBankAccNo");    
	  
System.out.println("in save : "+ModifyDate+"      "+ModifyTime);  	   
	
		LJAGetSchema  tLJAGetSchema  ; //实付总表
		LJAGetSet     tLJAGetSet     ;
		
		VData tVData = new VData();
		tLJAGetSchema = new LJAGetSchema();
		tLJAGetSchema.setActuGetNo(ActuGetNo);
		tVData.clear();
		tVData.add(tLJAGetSchema);
		
		LJAGetQueryUI tLJAGetQueryUI = new LJAGetQueryUI();
	  if(!tLJAGetQueryUI.submitData(tVData,"QUERY"))
	  {
	      Content = " 查询实付总表失败，原因是: " + tLJAGetQueryUI.mErrors.getError(0).errorMessage;
	      FlagStr = "Fail";	
	  }
		else
		{		
			TransferData updateData = new TransferData();
			
			updateData.setNameAndValue("ActuGetNo",		ActuGetNo 		);
			updateData.setNameAndValue("PolNo",				PolNo 				);
			updateData.setNameAndValue("PayMode",			PayMode 			);
			updateData.setNameAndValue("GetMoney",		GetMoney 		  );
			updateData.setNameAndValue("AgentCode",		AgentCode 		);
			updateData.setNameAndValue("Drawer",			Drawer 			  );
			updateData.setNameAndValue("AgentGroup",	AgentGroup 	  );
			updateData.setNameAndValue("DrawerID",		DrawerID 		  );
			updateData.setNameAndValue("Operator",		Operator 		  );
			updateData.setNameAndValue("ModifyDate",	ModifyDate 	  );
			updateData.setNameAndValue("ModifyTime",	ModifyTime 	  );
			
			updateData.setNameAndValue("BankCode",		BankCode 		  );
			updateData.setNameAndValue("ChequeNo",		ChequeNo 		  );
			updateData.setNameAndValue("BankAccNo",		BankAccNo 		);
			updateData.setNameAndValue("AccName",			AccName 			);
			
			updateData.setNameAndValue("InsBankAccNo",InsBankAccNo  );
			
			tVData.clear();
			tVData.add(tLJAGetSchema);
			tVData.add(updateData);
			
			FinFeeUpdateBL finFeeUpdate = new FinFeeUpdateBL();		
			if(!finFeeUpdate.submitData(tVData,"UPDATE"))
			{
				Content = " 更新实付总表失败，原因是: " + finFeeUpdate.mErrors.getError(0).errorMessage;
	      FlagStr = "Fail";	
	    }
	  	else
	  	{
	  		Content = "修改数据成功! ";
		    FlagStr = "Succ";
	  	}
%>  

<script language="javascript">
	parent.fraInterface.initForm();
</script>
<%    
		}
 	}//页面有效区    
%> 
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML>    