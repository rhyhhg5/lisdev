<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：NewGrpPolFeeWithDrowInput.jsp
//程序功能：个人保费催收，实现数据从保费项表到应收个人表和应收总表的流转
//创建日期：2002-07-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="NewGrpPolFeeWithDrowInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body >
	<form name=fm action=./NewGrpPolFeeWithDrow.jsp target=fraSubmit method=post>
		<table>
			<tr>
				<td>
					<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divPersonSingle);">
				</td>
				<td class= titleImg>团体新单溢交保费退费</td>
			</tr>
		</table>
		<Div id="divPersonSingle" style= "display: ''">
			<Table  class= common width="100%">
				<TR  class= common>
					<TD  class= title>合同单号</TD>
					<TD  class= input>
						<Input class= common name=GrpContNo >
					</TD>
					<td class=title>
						<input class="cssButton" value="查  询" type=button onclick="queryClick()">
					</td>
					<TD  class= input></td>
				</TR>
			</table>
		</Div>
		<div id="divDifShow" style="display:">
			<Table  class= common width="100%">
				<TR  class= common>
					<TD  class= title>退费金额</TD>
					<TD  class= input>
						<Input class="readonly" name=Dif readonly=true >
					</TD>
					<td class=title>
						<INPUT class="cssButton" VALUE="退  费" TYPE=button onclick="submitForm()">
					</TD>
					<td class=input>
					<INPUT class="cssButton" VALUE="打  印" TYPE=button onclick="print()">
					</TD>
				</TR>
			</Table>
		</div>
	<input type=hidden id="ActuGetNo" name="ActuGetNo">
	</form>
</body>
</html>