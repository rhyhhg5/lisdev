//TempFeeWithdrawInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var tFees;
//提交，保存按钮对应操作
function submitForm()
{ 
  if(!checkDate())
  {
     return false;
  }
  var i = 0;
  var checkFlag = 0;
  for (i=0; i<FeeGrid.mulLineCount; i++) {
    if (FeeGrid.getSelNo(i)) { 
      checkFlag = 1;
      break;
    }
  }
    if (checkFlag==1) { 
	   var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
       showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
       fm.submit(); //提交
  }
  else {
    alert("请先选择一条暂交费信息！"); 
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  initInpBox();
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

var queryBug = 0;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//暂交费号码查询按钮
function tempFeeNoQuery() {
    var moneySql = "sum(PayMoney)<>0";
    if(fm.PayMoney.value != null && fm.PayMoney.value != ""){
       moneySql = " sum(PayMoney)=" + fm.PayMoney.value;
    }
    var strSql = "select '', OtherNo, TempFeeType,getUniteCode(agentCode), APPntName, OtherNo, sum(PayMoney), PayDate, EnterAccDate, sum(PayMoney) from LJTempFee where ConfMakedate is not null "
               + " and Confdate is null and TempFeeType='YS' "
               + getWherePart('EnterAccDate')
               + getWherePart('ManageCom','ManageCom','like')
               + getWherePart('APPntName','APPntName','like')
               + getWherePart('AgentCode')
               + " group by TempFeeType, RiskCode, APPntName, OtherNo, PayDate, EnterAccDate, AgentCode having " + moneySql;
        
  var strSqlTemp=easyQueryVer3(strSql, 1, 0, 1); 
     turnPage.strQueryResult=strSqlTemp;
     
     if(!turnPage.strQueryResult)
     {
        window.alert("没有查询记录!");
        FeeGrid.clearData();
        return false;
     }
     else
     {
          turnPage.queryModal(strSql, FeeGrid);
     }
}

function getMoney(){ 
  var sql=" select '1' from lcgrpcont where prtno='" + fm.all('PrtNo').value + "' and uwflag in ('9', '4') with ur"
    var tarrResult = easyExecSql(sql);
    if(!tarrResult){
        alert('保单不存在或者没有核保通过'); 
      return false ; 
    }
     //add by yingxl begin
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('PrtNo').value + "' "
              + " and confdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode in ('1','11','10','13','3','2','12','6','5'))";
    var arrResult = easyExecSql(sql);
    if(arrResult)
    {
        alert('已经收过费，缴费凭证号是：'+arrResult[0][0]); 
        return false ; //存在
    }
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('PrtNo').value + "' "
              + " and confdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode ='YS')";

    var arrResult1 = easyExecSql(sql);
    if(arrResult1)
    {
        alert('已经收过费，缴费凭证号是：'+arrResult1[0][0]); 
        return false ; //存在
    }
    sql = " select tempfeeno "
              + " from ljtempfee "
              + " where otherno='" + fm.all('PrtNo').value + "' "
              + " and confdate is  null  and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode ='4')";

    var arrResult2 = easyExecSql(sql);
    if(arrResult2)
    {
        alert('该保单缴费方式是银行转帐，缴费凭证号是：'+arrResult2[0][0]); 
        return false ; //存在
    }
    
  var findSQL="select distinct a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
        +"a.AgentCode,trim(a.prtno)||'801',a.prtno from lcgrpcont a where a.paymode<>'4' "
        +"and a.signdate is null and appflag in ('0','9') "
        +"and  (select count(1) from lcrnewstatelog where prtno=a.prtno)=0 "
        +"and  (select count(1) from ljtempfee where enteraccdate is null and otherno=a.prtno)=0 and a.prtno='"+ fm.all('PrtNo').value +"' "
        +"and a.managecom='"+ fm.all('ManageCom').value +"' ";
    var arrResult = easyExecSql(findSQL);
    if(arrResult!=null){
        fm.all('TempFeeNo').value=arrResult[0][3];
    }else{
       alert("未查到保单相关信息！");
       return false ;
    }
    initTempGrid();
    TempGrid.clearData("TempGrid");
    fillgrid(fm.all('PrtNo').value)   
}

function fillgrid(PrtNo)
{
   var strSql="select distinct conttype from lccont where prtno='"+PrtNo+"'";
   var strSql1="";
   var strSql2="";
   var strSql3="select a.riskcode,b.riskname,sum(a.prem),a.grpName,getUniteCode(a.agentCode),a.agentCode from lcgrppol a,lmrisk b where prtno='"+PrtNo+"' and a.riskcode=b.riskcode group by a.riskcode,b.riskname,a.grpName,a.agentcode ";
   var strSql4="select paymode,codename,prem,'','','',bankcode,bankaccno,accname from lcgrpcont,ldcode where prtno='"+PrtNo+"' and  codetype='paymode' and paymode=code";
   var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      if (arrResult[0][0]=='1') { //个单
        strSql1 = "select '000000','null',-LF_PayMoneyNo('"+PrtNo+"') from lcpol a where a.prtno='"+PrtNo+"' fetch first 1 rows only";
          turnPage.queryModal(strSql1, TempGrid);
      }
      else if (arrResult[0][0]=='2'){ //团单
      turnPage.queryModal(strSql3, TempGrid);   
    }
  }
  turnPage.queryModal(strSql3, TempGrid);
}

function checkDate() {
    if(fm.PrtNo.value ==null || fm.PrtNo.value == ""){
        alert("请输入缴费通知书号！");
        return false;
    }
    if(fm.TempFeeNo.value ==null || fm.TempFeeNo.value == ""){
        alert("请输入暂收费号！");
        return false;
    }
    var tSql = " select tempfeeno "
              + " from ljtempfee "
              + " where tempfeeno='" + fm.all('TempFeeNo').value + "' ";
    var arrResult = easyExecSql(tSql);
    if(arrResult)
    {
        alert("该暂收费号已经使用，请更换暂收费号"); 
        return false ; //存在
    }
    return true;
}

function queryAgent()
{
    if(fm.all('ManageCom').value==""){
         alert("请先录入管理机构信息！");
         return;
    }
    if(fm.all('AgentCode').value == "")
    {
          var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
      }
    if(fm.all('AgentCode').value != "")
    {
      var cAgentCode = fm.AgentCode.value;  //保单号码
      var strSql = "select (select getUniteCode('" + cAgentCode +"') from dual),Name from LAAgent where AgentCode='" + cAgentCode +"'";
      var arrResult = easyExecSql(strSql);
      if (arrResult != null) {
        alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
      }
    }
}

function afterQuery2(arrReturn)
{
   fm.all('AgentCode').value = arrReturn[0][0];
   fm.GroupAgentCode.value = arrReturn[0][95];
}

