
<%
	//程序名称：BatchPoundagePayInit.jsp
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">
	function initForm() {
		try {
			initQueryLJFIGetGrid();
		} catch (re) {
			alert("PayUnionSerialnoTJInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	function initQueryLJFIGetGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "实付号"; //列名
			iArray[1][1] = "70px"; //列宽
			iArray[1][2] = 70; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许   

			iArray[2] = new Array();
			iArray[2][0] = "其它号码"; //列名
			iArray[2][1] = "70px"; //列宽
			iArray[2][2] = 60; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "号码类型"; //列名
			iArray[3][1] = "45px"; //列宽
			iArray[3][2] = 60; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array();
			iArray[4][0] = "给付金额"; //列名
			iArray[4][1] = "45px"; //列宽
			iArray[4][2] = 60; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "付费方式"; //列名
			iArray[5][1] = "45px"; //列宽
			iArray[5][2] = 60; //列最大值
			iArray[5][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[6] = new Array();
			iArray[6][0] = "对方帐户名"; //列名
			iArray[6][1] = "50px"; //列宽
			iArray[6][2] = 60; //列最大值
			iArray[6][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[7] = new Array();
			iArray[7][0] = "领取人"; //列名
			iArray[7][1] = "45px"; //列宽
			iArray[7][2] = 60; //列最大值
			iArray[7][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[8] = new Array();
			iArray[8][0] = "到帐日期"; //列名
			iArray[8][1] = "65px"; //列宽
			iArray[8][2] = 60; //列最大值
			iArray[8][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[9] = new Array();
			iArray[9][0] = "给付机构编码"; //列名
			iArray[9][1] = "80px"; //列宽
			iArray[9][2] = 0; //列最大值
			iArray[9][3] = 1; //是否允许输入,1表示允许，0表示不允许
			
			iArray[10] = new Array();
			iArray[10][0] = "管理机构编码"; //列名
			iArray[10][1] = "80px"; //列宽
			iArray[10][2] = 0; //列最大值
			iArray[10][3] = 1; //是否允许输入,1表示允许，0表示不允许
			
			iArray[11] = new Array();
			iArray[11][0] = "结算日期"; //列名
			iArray[11][1] = "65px"; //列宽
			iArray[11][2] = 0; //列最大值
			iArray[11][3] = 1; //是否允许输入,1表示允许，0表示不允许
			iArray[11][4] = 1;
			
			iArray[12] = new Array();
			iArray[12][0] = "险种编码"; //列名
			iArray[12][1] = "50px"; //列宽
			iArray[12][2] = 0; //列最大值
			iArray[12][3] = 1; //是否允许输入,1表示允许，0表示不允许
			iArray[12][4] = 1;

			LJFIGetGrid = new MulLineEnter("fm", "LJFIGetGrid");
			LJFIGetGrid.displayTitle = 1;

			LJFIGetGrid.hiddenPlus = 1;
			LJFIGetGrid.hiddenSubtraction = 1;
			LJFIGetGrid.canChk = 1;
			LJFIGetGrid.chkBoxEventFuncName = "EventOnMulLine";
			LJFIGetGrid.chkBoxAllEventFuncName = "EventOnMulLineTotal";
			LJFIGetGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}
</script>
