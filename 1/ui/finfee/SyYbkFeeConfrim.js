//程序名称：FinFeeSure.js
//程序功能：到帐确认
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var k = 0;
var turnPage = new turnPageClass();

/**
 * 查詢按鈕
 * @returns
 */
function easyQueryClick()
{   
	if(fm.ConfrimFlag.value==null || fm.ConfrimFlag.value==""){
		alert("是否结算必选");
		return false;
	}
	if(fm.ManageCom.value==null || fm.ManageCom.value==""){
		alert("管理机构不能为空");
		return false;
	}
	var strSQL = "";
	if(fm.ConfrimFlag.value=="1"){//未結算
		strSQL = "select ljt.tempfeeno,ljt.otherno,ljtc.paymode,ljt.paymoney,ljt.makedate,ljt.enteraccdate,'' from " 
			   + "ljtempfee ljt,ljtempfeeclass ljtc where exists " 
		       + "(select 1 from lccont lcc,lcpol lcp where  lcc.prtno=lcp.prtno  and   ljt.otherno=lcc.contno and   lcp.riskcode in (select code from ldcode where codetype='ybkriskcode') and lcc.customgetpoldate < current date - 15 days and lcc.StateFlag='1' and lcc.appflag='1')"
		       + "and  ljtc.tempfeeno=ljt.tempfeeno and ljtc.paymode='8' "
		       + " and not exists (select 1 from db2inst1.lysendtosettle where paycode=ljt.tempfeeno "
		       + getWherePart( 'Confdate','ValidateDate')
		       + " ) " 
			   + getWherePart( 'ljt.ManageCom','ManageCom')
			   + getWherePart( 'ljt.TempFeeNo','TempFeeNo');
	}else if (fm.ConfrimFlag.value=="2"){//已結算
		strSQL = "select ljt.tempfeeno,ljt.otherno,ljtc.paymode,ljt.paymoney,ljt.makedate,ljt.enteraccdate,a.confdate " 
			   + " from ljtempfee ljt,ljtempfeeclass ljtc,db2inst1.lysendtosettle a where exists " 
		       + "(select 1 from lccont lcc,lcpol lcp where  lcc.prtno=lcp.prtno  and   ljt.otherno=lcc.contno and   lcp.riskcode in (select code from ldcode where codetype='ybkriskcode') )"
		       + "and  ljtc.tempfeeno=ljt.tempfeeno and ljt.tempfeeno=a.paycode and ljtc.paymode='8' "
		       + getWherePart( 'a.Confdate','ValidateDate')
			   + getWherePart( 'ljt.ManageCom','ManageCom')
			   + getWherePart( 'ljt.TempFeeNo','TempFeeNo');
	}
	// 书写SQL语句
	
//查询SQL，返回结果字符串
turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

//判断是否查询成功
if (!turnPage.strQueryResult) {
 //alert(turnPage.strQueryResult);
 alert("没有查询到满足条件的数据！");
 return "";
}

//查询成功则拆分字符串，返回二维数组
turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);	 
				 
//设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
turnPage.pageDisplayGrid = PolGrid;    
       
//保存SQL语句
turnPage.strQuerySql     = strSQL; 

//设置查询起始位置
turnPage.pageIndex       = 0;  

//在查询结果数组中取出符合页面显示大小设置的数组
var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);

//调用MULTILINE对象显示查询结果
displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

//控制是否显示翻页按钮
if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
 try { window.divPage.style.display = ""; } catch(ex) { }
} else {
 try { window.divPage.style.display = "none"; } catch(ex) { }
}

return true;

}


function downLoad(){
	if(fm.ConfrimFlag.value==null || fm.ConfrimFlag.value==""){
		alert("是否结算必选");
		return false;
	}
	if(fm.ManageCom.value==null || fm.ManageCom.value==""){
		alert("管理机构不能为空");
		return false;
	}
	var strSQL = "";
	if(fm.ConfrimFlag.value=="1"){//未結算
		strSQL = "select ljt.tempfeeno as 暂收费号,ljt.otherno as 保单号,ljt.paymoney as 结算金额,'' as 结算时间,ljtc.insbankcode as 本方开户银行,ljtc.insbankaccno as 本方银行账户   from " 
			   + "ljtempfee ljt,ljtempfeeclass ljtc where exists " 
		       + "(select 1 from lccont lcc,lcpol lcp where  lcc.prtno=lcp.prtno  and   ljt.otherno=lcc.contno and   lcp.riskcode in (select code from ldcode where codetype='ybkriskcode') and lcc.customgetpoldate < current date - 15 days and lcc.StateFlag='1' and lcc.appflag='1' )"
		       + "and  ljtc.tempfeeno=ljt.tempfeeno and ljtc.paymode='8' "
		       + " and not exists (select 1 from db2inst1.lysendtosettle where paycode=ljt.tempfeeno "
		       + getWherePart( 'Confdate','ValidateDate')
		       + " ) " 
			   + getWherePart( 'ljt.ManageCom','ManageCom')
			   + getWherePart( 'ljt.TempFeeNo','TempFeeNo');
		fm.querySql.value=strSQL;
		fm.action="./SyYbkFeeConfrimSava.jsp?querySql="+strSQL+"&ConfrimFlag=1&ManageCom="+fm.ManageCom.value;
		fm.submit();
	}else if (fm.ConfrimFlag.value=="2"){//已結算
		strSQL = "select ljt.tempfeeno as 暂收费号,ljt.otherno as 保单号,ljt.paymoney as 结算金额,a.confdate as 结算时间,ljtc.insbankcode as 本方开户银行,ljtc.insbankaccno as 本方银行账户  " 
			   + " from ljtempfee ljt,ljtempfeeclass ljtc,db2inst1.lysendtosettle a where exists " 
		       + "(select 1 from lccont lcc,lcpol lcp where  lcc.prtno=lcp.prtno  and   ljt.otherno=lcc.contno and   lcp.riskcode in (select code from ldcode where codetype='ybkriskcode') )"
		       + "and  ljtc.tempfeeno=ljt.tempfeeno and ljt.tempfeeno=a.paycode and ljtc.paymode='8' "
		       + getWherePart( 'a.Confdate','ValidateDate')
			   + getWherePart( 'ljt.ManageCom','ManageCom')
			   + getWherePart( 'ljt.TempFeeNo','TempFeeNo');
		fm.querySql.value=strSQL;
		fm.action="./SyYbkFeeConfrimSava.jsp?querySql="+strSQL+"&ConfrimFlag=2&ManageCom="+fm.ManageCom.value;
		fm.submit();
	}
	
}
//提交，保存按钮对应操作
function submitForm()
{
	getImportPath();
	var ImportPath = fm.ImportPath.value;
	var ImportFileName = fm.FileName.value;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action="./SyYbkFeeConfrimInputSava.jsp?FileName="+ImportFileName+"&ImportPath="+ImportPath;
	fm.submit(); //提交
}

function getImportPath ()
{
  // 书写SQL语句

  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar = 'XmlPath' ";
  var tarrResult = easyExecSql(strSQL);
  //判断是否查询成功
  if (tarrResult[0][0]=="")
  {
    alert("未找到上传路径");
    return;
  }
  fm.ImportPath.value = tarrResult[0][0];
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);
    fm.FileName.value = "";
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    alert(content);
    fm.FileName.value = "";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
