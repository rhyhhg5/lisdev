 <%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>财务给付表信息查询 </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>   
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <script src="./FinFeePayQueryLJFIGet.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FinFeePayQueryLJFIGetInit.jsp"%>

</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm action=./FinFeePayQueryLJFIGetResult.jsp target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
          实付号码:
          </TD>
          <TD  class= input>
            <Input class= common name=ActuGetNo >
          </TD>
          <TD  class= title>
          付费方式:
          </TD>
          <TD  class= input>
          <Input class=codeNo name=PayMode  ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName],[0,1]);"><input class=codename name=PayModeName readonly=true >
          </TD> 
          <TD  class= title>
          业务序号:
          </TD>
          <TD  class= input>
         <Input class="common" name=OtherNo >
          </TD> 
               </TR>
      	<TR  class= common>
          <TD  class= title>
          给付金额:
          </TD>
          <TD  class= input>
         <Input class="common" name=GetMoney >
          </TD>  
         	<TD  class= title>  
         		给付时间: 
         		</TD>
          <TD  class= input>  <Input class="coolDatePicker" name=ConfDate verify="给付时间|date"> </TD>
   		     	      
       </TR>                     
   </Table>  
      <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="返  回" Class=cssButton TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			 实付总表信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanQueryLJFIGetGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>
