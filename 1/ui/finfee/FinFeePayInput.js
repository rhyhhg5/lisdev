  //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var bankflag="";
var OtherNoType="";
//提交，保存按钮对应操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!checkValue()) return false; //附加检验
  fm.all('ActuGetNo').value=fm.all('ActuGetNo').value;
  
  if(fm.all('ActuGetNo').value!=""&&fm.all('ActuGetNo').value==fm.all('ActuGetNo').value)
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交
  }
  else
   alert("请先查询！");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
//  alert(FlagStr);
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	try
  	{ 
	    fm.all('ActuGetNo').value = '';
	    fm.all('PolNo').value = '';
	    fm.all('GetMoney').value = '';
	    fm.all('PayMode').value = '';
	    fm.all('EnterAccDate'	).value = '';    
	    fm.all('BankCodeName'	).value = '';
	    fm.all('ChequeNo'			).value = '';
	    fm.all('BankAccNo'		).value = '';
	    fm.all('AccName'			).value = '';
	    fm.all('InsBankAccNo'	).value = '';
	    fm.all('PayModeName'	).value = '';
	    fm.all('BankCode'			).value = '';
	    
	    fm.all('Operator'			).value	=ope;    
  		fm.all('ModifyDate'		).value	=mDate;                       
  		fm.all('ModifyTime'		).value	=mTime;                
	    
	//  fm.all('AgentCode').value = '';
	//  fm.all('AgentGroup').value = '';                                          
	    fm.all('Drawer').value 			= '';
	    fm.all('DrawerID').value 		= '';    
	    
	    fm.all('getbankcode').value 	= '';
	    fm.all('getbankaccno').value 	= '';    
    
    if (fm.all('UpFlag').value=="1")
    {
    	divEnterAccDate.style.display = "none";
    	divEnterAccTitle.style.display = "none";
    	
    	document.all.ActuG.readOnly="true";
    	document.all.PolNoId.readOnly="true";
    	document.all.Drawer_Id.readOnly="true";
    	document.all.DrawerID_Id.readOnly="true";
        document.all.AccName_Id.readOnly="true";
    }
	  }
	  catch(ex)
	  {
	    alert("在FinFeePayInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	  }      
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  //  showDiv(operateButton,"true"); 
  //  showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterQuery( FlagStr, content )
{
  alert(FlagStr);
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  //  showDiv(operateButton,"true"); 
  //  showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
 // showDiv(operateButton,"false"); 
 // showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //首先检验录入框
  if(!verifyInput()) return false;
  if(!checkValue()) return false; //附加检验
  
  fm.all('Operator'		).value	=ope;    
  fm.all('ModifyDate'	).value	=mDate;                       
  fm.all('ModifyTime'	).value	=mTime;                       
  fm.all('ActuGetNo').value=fm.all('ActuGetNo').value;

  if(fm.all('ActuGetNo').value!=""&&fm.all('ActuGetNo').value==fm.all('ActuGetNo').value)
  {
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
	  fm.action="./FinFeeUpdateSave.jsp";
	  fm.submit(); //提交
  }
  else
   alert("请先查询！");
  
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
//   if(fm.all('ActuGetNo').value!="")
//   {
//    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      
//    fm.submit();
//   }
//   else
//   {
    //alert("实付号码不能为空！"); return ;	
    //alert(fm.all("UpFlag").value);
   window.open("./FinFeePayQueryLJAGet.html?UpFlag="+fm.all("UpFlag").value);
//   }	
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("没有该功能！");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function queryLJAGet()
{
  window.open("./FinFeePayQueryLJFIGet.html");
}

function checkValue()
{
	//不允许为9的付费方式
	if(fm.all('PayMode').value == '9'){
		alert("请重新选择付费方式！");
		return false ;
	}
    
  //如果付费方式是支票类
  if(fm.all('PayMode').value=='2'||fm.all('PayMode').value=='3')
  {
    if(fm.all('BankCode').value==''||fm.all('ChequeNo').value=='') 	
      {
      	alert("付费方式是支票:开户银行和票据号码不能为空！");
      	return false;
      }
  }
  //如果付费方式是银行转账
  if(fm.all('PayMode').value=='4')
  {
    if(fm.all('BankCode').value=='') 	
      {
      	alert("付费方式是银行转账:开户银行不能为空！");
      	return false;
      }      
  }
  
	if(fm.all('PayMode').value!='1' && fm.all('PayMode').value!='5')
  {
    if(fm.all('getbankcode').value==''||fm.all('getbankaccno').value=='') 	
      {
      	alert("由于付费方式不是现金:本方银行编码和本方银行帐号不能为空！");
      	return false;
      }
     var accsql = "select 1 from ldfinbank where bankcode ='" + fm.getbankcode.value + "' and bankaccno='" + fm.getbankaccno.value +"'";
     var arrBankResult = easyExecSql(accsql);
     if(arrBankResult==null){
        alert("归集账户不存在，请确认录入的归集账户");
        return false ;
     }
  }
  
  if(fm.all('PayMode').value!='4'){
  	if(fm.all('PayMode').value=='5'){
  	}else{
	  if(fm.all('Drawer').value == null || fm.all('Drawer').value == "")
	  {
	  	 alert("领取人不能为空！");
	  	 return false;
	  }
	}	
  }
  if(OtherNoType=='23'&& (fm.all('PayMode').value!='1' && fm.all('PayMode').value!='11'))
    {
        alert("社保通调查费，付费方式请选择现金或银行汇款进行给付！")
         return false;
    }
  return true;
	
}

//如果付费方式选择“转帐支票”，则显示银行帐号
function showBankAccNo()
{
  if(fm.all('PayMode').value=='2'||fm.all('PayMode').value=='3'||fm.all('PayMode').value=='4'||fm.all('PayMode').value=='11' || fm.all('PayMode').value=='12')
  {
    divBankAccNo.style.display="";	
  }
  else  
  {
  	alert("请先选择付费方式，付费方式为现金，内部转账，赞助无需录入本方银行");
    divBankAccNo.style.display="none";	
  }	
}

function afterCodeSelect(){
  showBankAccNo();	
}
function afterCodeSelect(cCodeName, Field) {
	showBankAccNo();
	if(cCodeName == "getbankcode") {
	  fm.getbankaccno.value='';
	}
	
}

function easyQueryClick() {
  // 拼SQL语句，从页面采集信息
  initQueryLJAGetGrid();
  var strSql = "select ActuGetNo, OtherNo, OtherNoType,PayMode,SumGetMoney,EnterAccDate,BankCode,AccName,BankAccNo, "
   + " drawer, drawerid,chequeno,InsBankAccNo "
   + " from LJAGet where 1=1 and ConfDate is null and EnterAccDate is null and (bankonthewayflag='0' or bankonthewayflag is null) "
 //  + " and sumgetmoney<>0 "
	 + getWherePart( 'ActuGetNo','qActuGetNo' )
	 + getWherePart( 'OtherNo'  ,'qOtherNo'  )
	 + getWherePart( 'PayMode'  ,'qPayMode'  );
	 if(!fm.OtherCom.checked){
	    strSql+= getWherePart('ManageCom','ComCode',"like");
	  }
	  else{
	    if(fm.qActuGetNo.value==''&&fm.qOtherNo.value==''){
	      alert("做异地代付请先录入给付号码或业务序号！");
	      return false;
	    }
	    else{
	      strSql+=getWherePart('ActuGetNo','qActuGetNo')+getWherePart('OtherNo','qOtherNo');
	    }
	  }
	           
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    QueryLJAGetGrid.clearData('QueryLJAGetGrid');  
    alert("没有查询到数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QueryLJAGetGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
}

function ShowDetail(){
	var tSel = QueryLJAGetGrid.getSelNo();
		try
		{
		  fm.all('ActuGetNo').value=QueryLJAGetGrid.getRowColData(tSel-1,1);
		  fm.all('PolNo').value=QueryLJAGetGrid.getRowColData(tSel-1,2);
		  OtherNoType =QueryLJAGetGrid.getRowColData(tSel-1,3);
		  fm.all('PayMode').value=QueryLJAGetGrid.getRowColData(tSel-1,4);
		  fm.all('GetMoney').value=QueryLJAGetGrid.getRowColData(tSel-1,5);
		  fm.all('EnterAccDate').value=QueryLJAGetGrid.getRowColData(tSel-1,6);
		  fm.all('BankCode').value=QueryLJAGetGrid.getRowColData(tSel-1,7);      
		
		Sql = "Select codename from ldcode where codetype='bank' and code='"+QueryLJAGetGrid.getRowColData(tSel-1,7)+"'";
		arr =easyExecSql(Sql);
		if(arr)
		  fm.all('BankCodeName').value=arr[0][0];
		fm.all('AccName').value=QueryLJAGetGrid.getRowColData(tSel-1,8);
		fm.all('BankAccNo').value=QueryLJAGetGrid.getRowColData(tSel-1,9);
	//	top.opener.fmSave.all('ConfDate').value=QueryLJAGetGrid.getRowColData(tSel-1,7);
	
		fm.all('Drawer'			).value=QueryLJAGetGrid.getRowColData(tSel-1,10);
		fm.all('DrawerID'		).value=QueryLJAGetGrid.getRowColData(tSel-1,11);
		fm.all('ChequeNo'		).value=QueryLJAGetGrid.getRowColData(tSel-1,12);
		fm.all('InsBankAccNo').value=QueryLJAGetGrid.getRowColData(tSel-1,13);
		
		fm.all('getbankcode').value = '';
		fm.all('getbankaccno').value = '';

		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
}
function afterQuery( arrReturn )
{
	if(bankflag=="0"){
	fm.all('BankCode').value = arrReturn[0];
	fm.all('BankCodeName').value = arrReturn[1];
	}else if(bankflag=="1"){
	fm.all('InsBankAccNo').value = arrReturn[0];
	fm.all('InsBankAccNoName').value = arrReturn[1];
	}
}
function querybank()
{
		bankflag="0";
	showInfo = window.open("../bq/LDBankQueryMain.jsp");
}
function queryInsbank()
{
	bankflag="1";
	showInfo = window.open("../bq/LDBankQueryMain.jsp");
}
function printfCurrentData()
{
    var d = fm.SDate.value;
    if(d==""){
    alert("请选择起始时间!");
    return false;
    }
    var d1 = fm.EDate.value;
    if(d1==""){
    alert("请选择结束时间!");
    return false;
    }
	
     var i = 0;
     var showStr="";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.target = "f1print";   
 	fm.action="../finfee/FinFeePayPrintCurrentSave.jsp";
	fm.submit();
	showInfo.close();
}

function BatchPay(){   

 	window.open("./BatchPayMain.jsp");

}
function PayQueryClick(){   

 	window.open("./BatchPayMainDraw.jsp");

}
function payModeHelp(){
	window.open("./PayModeHelp.jsp");
}


//添加时间2010年4月20日
function BatchPoundagePay(){
	//alert(fm.all('ComCode').value);
	
	//var reg = /8637[0-9]*$/ ;
	
	//var reg = /86[0-9]*$/ ;
	//if(reg.test(fm.all('ComCode').value)){
	//if(true){
		window.open("./BatchPoundagePayMain.jsp?type=PT");
	//}else{
		//alert("对不起，本功能只对山东分公司开放！");
		//return false;
	//}
}
/**
 * 打开付费批量导入窗口
 */
function batchPay(){
	window.open("./BatchExcelPayMain.jsp");
}
//添加时间2015年7月7日
function JSBatchPoundagePay(){
	//alert(fm.all('ComCode').value);
	
	//var reg = /8637[0-9]*$/ ;
	
	//var reg = /86[0-9]*$/ ;
	//if(reg.test(fm.all('ComCode').value)){
	//if(true){
		window.open("./BatchPoundagePayMain.jsp?type=JS");
	//}else{
		//alert("对不起，本功能只对山东分公司开放！");
		//return false;
	//}
}