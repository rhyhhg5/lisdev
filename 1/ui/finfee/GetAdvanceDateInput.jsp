<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：GetAdvanceDateInput.jsp
//程序功能：预收保费查询
//创建日期：2011-11-19 11:10:36
//创建人  ：ZCX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="GetAdvanceDateInput.js"></SCRIPT>
  <%@include file="GetAdvanceDateInit.jsp"%>
  <title>预收保费查询</title>
</head>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./GetAdvanceDown.jsp">
    <table class= common border=0 width=100%>
        <tr>
            <td class= titleImg align= center>请输入查询条件：</td>
        </tr>
    </table>
    <table  class= common align=center>  
        <TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate verify="起始日期|date&NOTNULL" >
            <font color="#FF0000" align="Left">*</font>
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate verify="终止日期|date&NOTNULL" >
            <font color="#FF0000" align="Left">*</font>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            投保人单位
          </TD>
         <TD  class= input>
            <Input class= common name=AppntName >
          </TD>
       
        <TD  class= title>
            管理机构
        </TD>
        <TD  class= input>
          <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
         </TD>
         </TR>
         <TR  class= common>
          <TD  class= title>
            业务员
          </TD>
         <TD class="input" width="25%">
           <Input class=code name=GroupAgentCode onclick="queryAgent()" elementtype="nacessary">  
           <Input class=common name=AgentCode VALUE="" MAXLENGTH=0 type='hidden'>
        </TD>
        <TD  class= title>
            业务类型
        </TD>
        <TD  class= input>
          <Input NAME=Type  CLASS=codeNo CodeData="0|^0|收费^1|退费^2|转实收保费^3|全部" MAXLENGTH=20  ondblclick="return showCodeListEx('Type',[this,TypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Type',[this,TypeName],[0,1],null,null,null,1);"><input class=codename name=TypeName readonly=true> 
         </TD>
         </TR>
    </table> 
    <table align=right>
      <tr>
        <td> 
          <INPUT VALUE="查 询" class=cssButton TYPE=button onclick="easyQueryClick();">
        </td>
        <td>
          <INPUT VALUE="下 载" class=cssButton TYPE=button onclick="download();"> 
        </td>
      </tr>
    </table>
    <br>
    <br>
    <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <td text-align: left colSpan=1>
            <span id="spanLJAGetGrid" ></span> 
        </td>
      </TR>
    </Table>                    
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">                  
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div> 
 <INPUT type=hidden name=sql>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
