 <%
//程序名称：BatchPayExInit.jsp
//程序功能：
//创建日期：2012-06-15 
//创建人  ：zhangchengxuan
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<SCRIPT src="Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
    
%>                            

<script language="JavaScript">
                                     

function initForm()
{
  try
  {  
    initQueryGrid();  
  }
  catch(re)
  {
    alert("menuGrpInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  } 
}

 

function initQueryGrid()
{

    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="实付号";   		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=0;            		//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许   

      iArray[2]=new Array();
      iArray[2][0]="其它号码";		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=0;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="号码类型";         		//列名
      iArray[3][1]="0px";            		//列宽
      iArray[3][2]=0;            	        //列最大值
      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="给付金额";         		//列名
      iArray[4][1]="45px";            		//列宽
      iArray[4][2]=0;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="付费方式";         		//列名
      iArray[5][1]="45px";            		//列宽
      iArray[5][2]=0;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="本方银行";  	//列名
      iArray[6][1]="0px";          //列宽
      iArray[6][2]=0;            	//列最大值
      iArray[6][3]=0;                //是否允许输入,1表示允许，0表示不允许
      
      
      iArray[7]=new Array();
      iArray[7][0]="本方帐号";  	//列名
      iArray[7][1]="70px";          //列宽
      iArray[7][2]=0;            	//列最大值
      iArray[7][3]=0;                //是否允许输入,1表示允许，0表示不允许
         
      iArray[8]=new Array();
      iArray[8][0]="对方银行";         		//列名
      iArray[8][1]="50px";          	//列宽
      iArray[8][2]=0;            	  //列最大值
      iArray[8][3]=3;                 //是否允许输入,1表示允许，0表示不允许
           
      iArray[9]=new Array();
      iArray[9][0]="对方帐号";        	//列名
      iArray[9][1]="50px";          //列宽
      iArray[9][2]=0;            	//列最大值
      iArray[9][3]=0;                //是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="对方帐户名";	//列名
      iArray[10][1]="50px";          //列宽
      iArray[10][2]=0;            	//列最大值
      iArray[10][3]=0;                //是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="领取人";        //列名
      iArray[11][1]="45px";            //列宽
      iArray[11][2]=0;            	  //列最大值
      iArray[11][3]=0;                 //是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="票据号";        	//列名
      iArray[12][1]="40px";          //列宽
      iArray[12][2]=0;            	//列最大值
      iArray[12][3]=0;                //是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="到帐日期";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=0;            	        //列最大值
      iArray[13][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="领取人id";         		//列名
      iArray[14][1]="0px";            		//列宽
      iArray[14][2]=0;            	        //列最大值
      iArray[14][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[15]=new Array();
      iArray[15][0]="给付机构编码";         		//列名
      iArray[15][1]="60px";            		//列宽
      iArray[15][2]=0;            	        //列最大值
      iArray[15][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
       
      QueryGrid = new MulLineEnter( "fm" , "QueryGrid" ); 
         
      //这些属性必须在loadMulLine前
      QueryGrid.mulLineCount = 0;     
      QueryGrid.displayTitle = 1;
      QueryGrid.canChk =0;
      QueryGrid.canSel =0;
      QueryGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
      QueryGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      QueryGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      QueryGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      QueryGrid.loadMulLine(iArray);     
      }
      catch(ex)
      { 
        alert(ex);
      }
  }     
</script>
 