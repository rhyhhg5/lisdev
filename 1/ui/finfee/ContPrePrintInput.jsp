<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2013-11-12
		//创建人  ：yan
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="ContPrePrintInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<%@include file="ContPrePrintInit.jsp"%>
	</head>
	<body onload="initElementtype();initForm();">
		<form method=post name=fm target="fraSubmit">

				<strong><IMG id="a1" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divOperator);" />保监会报表打印
				</strong>

			<Div id="divOperator" style="display: ''">
			   <font color="#FF0000" size="3">
    		   注意：因数据量较大原因，可能导致报表打印时间较长，请耐心等待，不要重复点击打印按钮！
    		   </font>
    		    <p>
			</Div>

			<p>
				<strong><IMG id="a2" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divMonthPrint);" />请输入查询范围
				</strong>
			</p>

			<Div id="divMonthPrint" style="display: ''">
				<table class="common">
						<TR class=common>
							<TD class=title>
								起始日期
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="StartDay" name="StartDay" verify="起始时间|NOTNULL" elementtype=nacessary>
							</TD>
							<TD class=title>
								结束日期
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="EndDay" name="EndDay" verify="结束时间|NOTNULL" elementtype=nacessary>
							</TD>
						</TR>
				</table>
				<p>
				</p>

<!--				<input class="cssButton" style="margin-left:100px;width:200px"-->
<!--					type="Button" name="PreReceivePrint" value="大病保险统计简表"-->
<!--					onclick="ContPre_Print()" />-->
<!--				<br />-->
<!--				<br />-->
<!--				<input class="cssButton" style="margin-left:100px;width:200px"-->
<!--					type="Button" name="PreBudgetPrint" value="大病保险利润表"-->
<!--					onclick="ContBen_Print()" />-->
<!--				<br />-->
<!--				<br />	-->
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="PreReceivePrint" value="大病保险统计简表"
					onclick="TContPre_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="PreBudgetPrint" value="大病保险利润表"
					onclick="TContBen_Print()" />
				<br />
				<br />
			</Div>

			<input type="hidden" id="fmtransact" name="fmtransact" />
			<input type="hidden" name="Opt" />
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
