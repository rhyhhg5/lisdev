
  <%
//程序名称：TempFeeQurey.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.text.DecimalFormat"%>
  
<%@page contentType="text/html;charset=GBK" %>

<!--Step 1--查询是否已经存在数据 -->
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";

  String TempFeeType=request.getParameter("TempFeeType");
  String InputNo   = "";
  String ManageCom = request.getParameter("ManageCom");

  String TempFeeNo="";
  String RiskCode="";
  String PolNo="";
  String OtherNo ="";
  String OtherNoType="";
  String InputNo1="";
  double SumDuePayMoney=0;
  SSRS tSSRS = new SSRS();
  String BankOnTheWayFlag="";

  //产生的通知书号
  String tLimit="";
  String tNo="";

  //应收总表
  LJSPaySchema tLJSPaySchema = new LJSPaySchema();
  LJSPaySet    mLJSPaySet = new LJSPaySet();
//-------------上面是共有信息---------------------
//-------------如果暂交费类型是2和4或7------------------------
if(TempFeeType.equals("2")) {
  InputNo   = request.getParameter("InputNo4");
  System.out.println("InputNo:"+InputNo);
  InputNo1=request.getParameter("InputNo3");
  if (InputNo1.length()!=0)
   tLJSPaySchema.setOtherNo(InputNo1);
}

if(TempFeeType.equals("4")) { 
  InputNo   = request.getParameter("InputNo8");
}
//理赔预付回收
if(TempFeeType.equals("9")) { 
	  InputNo   = request.getParameter("InputNo25");
	}
if(TempFeeType.equals("7")) {
  InputNo   = request.getParameter("InputNo14");
}

if(TempFeeType.equals("2")||TempFeeType.equals("4")||TempFeeType.equals("7")||TempFeeType.equals("9"))
{
//---------------------------------------------------
//判断输入的号码是保单号还是通知书号
//如果是保单号，最好判断出是个人，集体，合同
//查询应收总表，查询条件为OtherNo=输入的号码 OtherNoType=保单类型（可以不要）
//如果是通知书号，直接查询应收总表，查询条件为 GetNoticeNo=输入的号码
//因此只是查询条件不一样，其他操作相同
//---------------------------------------------------
//验证号码格式
  String codeType=CodeJudge.getCodeType(InputNo);
  if(codeType.equals("31")) //交费收据号
  {
    tLJSPaySchema.setGetNoticeNo(InputNo);
  }
  else    //repair:合同号待补充
    if(codeType.equals("21")||codeType.equals("22"))//个人保单号或集体保单号
    {
      tLJSPaySchema.setOtherNo(InputNo);
      if(codeType.equals("21")) //如果是个人保单号
        tLJSPaySchema.setOtherNoType("2");
      if(codeType.equals("22")) //如果是集体保单号
        tLJSPaySchema.setOtherNoType("1");
    }
    else if(!codeType.equals("00")) {
      System.out.println("codeType="+codeType);
      FlagStr="Fail";
      Content="号码输入类型不匹配";
    }

    if(FlagStr=="")
    {
      VData tVData = new VData();
      tVData.add(tLJSPaySchema);
      if(TempFeeType.equals("2")) //续期收费按照险种明细查询（查询个人应收明细或集体应收明细）
      {
        VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
        if(!tVerDuePayFeeQueryUI.submitData(tVData,"QUERYDETAIL"))
        {
          Content = " 查询失败，原因是: " + tVerDuePayFeeQueryUI.mErrors.getError(0).errorMessage;
          FlagStr = "Fail";
        }
        else
        {
          tVData.clear();
          mLJSPaySet = new LJSPaySet();
          tVData = tVerDuePayFeeQueryUI.getResult();
          mLJSPaySet.set((LJSPaySet)tVData.getObjectByObjectName("LJSPaySet",0));
          tLJSPaySchema = (LJSPaySchema)mLJSPaySet.get(1);
          tSSRS=(SSRS)tVData.getObjectByObjectName("SSRS",0);

          TempFeeNo = tLJSPaySchema.getGetNoticeNo();
          SumDuePayMoney=tLJSPaySchema.getSumDuePayMoney();
          OtherNo=tLJSPaySchema.getOtherNo();  //得到保单号（集体或个人）
          OtherNoType=tLJSPaySchema.getOtherNoType(); //得到号码类型（集体或个人或合同）
          RiskCode=tLJSPaySchema.getRiskCode();
          //银行在途标记
          BankOnTheWayFlag=tLJSPaySchema.getBankOnTheWayFlag();          
          if(BankOnTheWayFlag!=null)
          {
          	//如果在途，则不能柜台交费
          	if(BankOnTheWayFlag.equals("1"))
          	{
          		Content = "已经存在银行划帐数据，不能柜台交费 !";
          		FlagStr = "Fail";
          	}
          }

        }
      }
      else
      {
        VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
        if(!tVerDuePayFeeQueryUI.submitData(tVData,"QUERY"))
        {
          Content = " 查询失败，原因是: " + tVerDuePayFeeQueryUI.mErrors.getError(0).errorMessage;
          FlagStr = "Fail";
        }
        else{
          tVData.clear();
          mLJSPaySet = new LJSPaySet();
          tVData = tVerDuePayFeeQueryUI.getResult();
          mLJSPaySet.set((LJSPaySet)tVData.getObjectByObjectName("LJSPaySet",0));
          tLJSPaySchema = (LJSPaySchema)mLJSPaySet.get(1);
          TempFeeNo = tLJSPaySchema.getGetNoticeNo();
          
          SumDuePayMoney=tLJSPaySchema.getSumDuePayMoney();
          OtherNo=tLJSPaySchema.getOtherNo();  //得到保单号（集体或个人）
          OtherNoType=tLJSPaySchema.getOtherNoType(); //得到号码类型（集体或个人或合同）
          RiskCode=tLJSPaySchema.getRiskCode();
          
        }
      }
    }
    if (FlagStr=="")
    {
      Content = " 查询成功";
      FlagStr = "Succ";
    }
}

System.out.println("TempFeeType:"+TempFeeType);
%>
<html>
<script language="javascript">
//   if("<%=TempFeeType%>"=="2"||"<%=TempFeeType%>"=="4")
//   {
	parent.fraInterface.afterQuery("<%=FlagStr%>","<%=Content%>");
//   }
</script>
<body>
<script language="javascript">
   if("<%=TempFeeType%>"=="4"||"<%=TempFeeType%>"=="7"||"<%=TempFeeType%>"=="9")
     {
    if("<%=FlagStr%>"=="Succ")
      {
      var row= parent.fraInterface.TempGrid.mulLineCount;
      if(row==0)
        parent.fraInterface.TempGrid.addOne("TempGrid");

      var RiskCode="";
      var RiskName = new Array();
      if("<%=RiskCode%>"==null)
        {
        RiskName[0]= new Array();
        RiskName[0][0]= "";
      }
      else
      {
        RiskCode="<%=RiskCode%>";
        if(RiskCode=="000000")
        {
          RiskName[0]= new Array();
          RiskName[0][0]=""; 
          parent.fraInterface.initTempGridReadOnlyCont();   //合同交费时不显示险种信息
          parent.fraInterface.TempGrid.addOne("TempGrid");
          
        }
        else
        {
        var strSql = "select RiskName from LMRisk where RiskCode='"+"<%=RiskCode%>"+"'";
          var strResult=easyQueryVer3(strSql, 1, 1, 1);
          if(!strResult)
          {
            alert("险种编码表中没有查询到险种编码对应的险种名称！");
            RiskName[0]= new Array();
            RiskName[0][0]= "";
          }
          else
          {
            RiskName=decodeEasyQueryResult(strResult);
          }
         }
      }
      parent.fraInterface.TempGrid.setRowColData(row,1,RiskCode);
      parent.fraInterface.TempGrid.setRowColData(row,2,RiskName[0][0]);
      parent.fraInterface.TempGrid.setRowColData(row,3,"<%=new DecimalFormat("0.00").format(new Double(SumDuePayMoney))%>");
      parent.fraInterface.TempGrid.setRowColData(row,4,"<%=OtherNo%>");
      parent.fraInterface.fm.all('TempFeeNo').value = "<%=TempFeeNo%>";

      if (parent.fraInterface.fm.all('TempFeeType').value=="2" && parent.fraInterface.fm.all('InputNo3').value!="<%=OtherNo%>") {
        parent.fraInterface.alert("录入的保单号码与交费通知书号码不匹配！");
        parent.fraInterface.TempGrid.clearData();
      }
      else if (parent.fraInterface.fm.all('TempFeeType').value=="4" && parent.fraInterface.fm.all('InputNo7').value!="<%=OtherNo%>") {
        parent.fraInterface.alert("录入的批单号码与交费收据号码不匹配！");
        parent.fraInterface.TempGrid.clearData();
      }
      else if (parent.fraInterface.fm.all('TempFeeType').value=="7" && parent.fraInterface.fm.all('InputNo13').value!="<%=OtherNo%>") {
        parent.fraInterface.alert("录入的保全受理号与交费收据号码不匹配！");
        parent.fraInterface.TempGrid.clearData();
      }
      else {
        parent.fraInterface.TempClassGrid.addOne("TempClassGrid");
      }
    }
   }
   else if("<%=TempFeeType%>"=="2")
   {
      if("<%=FlagStr%>"=="Succ")
      {
<%        for (int i =1;i<=tSSRS.MaxRow;i++)
          {
          SumDuePayMoney=Double.valueOf(tSSRS.GetText(i,2)).doubleValue();
          RiskCode=tSSRS.GetText(i,1);
          PolNo=tSSRS.GetText(i,3);
  %>
          var row= parent.fraInterface.TempGrid.mulLineCount;

            parent.fraInterface.TempGrid.addOne("TempGrid");

          var RiskCode="";
          var RiskName = new Array();
      if("<%=RiskCode%>"==null) //如果从java得到的险种号为null
        {
        RiskName[0]= new Array();
        RiskName[0][0]= "";
      }
      else
      {
        RiskCode="<%=RiskCode%>";
        var strSql = "select RiskName from LMRisk where RiskCode='"+"<%=RiskCode%>"+"'";
          var strResult=easyQueryVer3(strSql, 1, 1, 1);
          if(!strResult)
          {
            alert("险种编码表中没有查询到险种编码对应的险种名称！");
            RiskName[0]= new Array();
            RiskName[0][0]= "";
          }
          else
          {
            RiskName=decodeEasyQueryResult(strResult);
          }
      }
      //将得到的LJSPayPerson或LJSPayGrp中得到的数据添加进TempGrid
      parent.fraInterface.TempGrid.setRowColData(row,1,RiskCode);
      parent.fraInterface.TempGrid.setRowColData(row,2,RiskName[0][0]);
      parent.fraInterface.TempGrid.setRowColData(row,3,"<%=SumDuePayMoney%>");
      parent.fraInterface.TempGrid.setRowColData(row,4,"<%=PolNo%>");
      parent.fraInterface.fm.all('TempFeeNo').value = "<%=TempFeeNo%>";
      row++;
<%
      }
%>
       // parent.fraInterface.TempClassGrid.addOne("TempClassGrid");
    }
   }
</script>
</body>
</html>

