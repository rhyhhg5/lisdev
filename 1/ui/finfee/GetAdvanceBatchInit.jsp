 <%
//程序名称：BatchPayQuereyInit.jsp
//程序功能：
//创建日期：2009-06-15 
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<SCRIPT src="Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
	
%>                            

<script language="JavaScript">
                                     

function initForm()
{
  try
  {  
    initQueryGrpGrid();  
  }
  catch(re)
  {
    alert("menuGrpInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  } 
}

 

function initQueryGrpGrid()
{

    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              	
      
      iArray[1]=new Array();
      iArray[1][0]="预收保费号码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="110px";         			//列宽
      iArray[1][2]=100;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="收费金额";    	                //列名
      iArray[2][1]="100px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
        	
      iArray[3]=new Array();
      iArray[3][0]="收费方式";    	                //列名
      iArray[3][1]="50px";            		        //列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

 
      iArray[4]=new Array();
      iArray[4][0]="交费单位名称";    	                //列名
      iArray[4][1]="230px";            		        //列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
 
      iArray[5]=new Array();
      iArray[5][0]="交费日期";    	                //列名
      iArray[5][1]="100px";            		        //列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[6]=new Array();
      iArray[6][0]="本方银行";                      //列名
      iArray[6][1]="50px";                         //列宽
      iArray[6][2]=100;                     //列最大值
      iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[7]=new Array();
      iArray[7][0]="本方银行帐号";                      //列名
      iArray[7][1]="120px";                         //列宽
      iArray[7][2]=100;                     //列最大值
      iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
       
      QueryGrpGrid = new MulLineEnter( "fm" , "QueryGrpGrid" ); 
         
      //这些属性必须在loadMulLine前
      QueryGrpGrid.mulLineCount = 0;     
      QueryGrpGrid.displayTitle = 1;
      QueryGrpGrid.canChk =0;
      QueryGrpGrid.canSel =0;
      QueryGrpGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
      QueryGrpGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      QueryGrpGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      QueryGrpGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      QueryGrpGrid.loadMulLine(iArray);     
      }
      catch(ex)
      { 
        alert(ex);
      }
  }     
</script>
 