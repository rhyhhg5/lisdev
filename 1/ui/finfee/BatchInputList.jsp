<html> 
<%
//程序名称：BatchInputListSave.jsp
//程序功能：财务收费查询
//创建日期：2009-06-19 
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="./com/sinosoft/lis/schema"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
	<SCRIPT src="BatchInputList.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BatchInputListInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action="" method=post 
  	name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
<table class = common>
    <TR  class= common>
      <TD class = title >
        选择导入的文件：
      </TD>
      <TD class = common width=30%>
        <Input  type="file" name=FileName size=30>
      </TD>
    </TR>
    
    <TR class = common >
      <td class = common colspan=2>
        <INPUT class=cssButton VALUE="导入" TYPE=button onclick = "submitForm();" > 
               
      </td>
    </TR>
    
    <input type=hidden name=ImportFile>
     <Div  id= "divQueryGrp" style= "display: ''">
    <table>
      <td class= titleImg>
    	 菜单组表
      </td>
    </table>
     <table  class= common>
        <tr>
    	  	<td text-align: left colSpan=1>
	         <span id="spanQueryGrpGrid" ></span>
		</td>
	</tr>
     </table>
        <INPUT VALUE="首  页" TYPE=button  onclick="userFirstPage()" class="cssButton">
        <INPUT VALUE="上一页" TYPE=button  onclick="userPageUp()" class="cssButton">
        <INPUT VALUE="下一页" TYPE=button  onclick="userPageDown()" class="cssButton">
        <INPUT VALUE="尾  页" TYPE=button  onclick="userLastPage()" class="cssButton">
</div>
</table>

  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 

 