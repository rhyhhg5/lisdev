<%
//程序名称：FinVerifyCollOperation.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>    
  
<%@page contentType="text/html;charset=GBK" %>

<%
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
   String GrpPolNo = "";
   int recordCount = 0;
   double tempMoney   = 0;
   double sumDueMoney = 0;
  
//对应暂交费表和暂交费分类表，应收总表中的暂交费收据号，通知书号  
   String TempFeeNo = "";
   TempFeeNo = request.getParameter("TempFeeNo");
//对应应收集体交费表中的集体保单号,对应于应收总表中的其它号码
   String GrpGrpPolNo = "";
   
//暂交费表
   LJTempFeeSchema tLJTempFeeSchema;
   LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
//暂交费分类表
   LJTempFeeClassSchema tLJTempFeeClassSchema;
   LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
//应收总表
    LJSPaySchema tLJSPaySchema;
    LJSPaySet tLJSPaySet = new LJSPaySet();
//应收集体交费表
    LJSPayGrpSchema tLJSPayGrpSchema;
    LJSPayGrpSet     tLJSPayGrpSet = new LJSPayGrpSet();     
//应收个人交费表
    LJSPayPersonSchema tLJSPayPersonSchema;
    LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();     
    
// 准备传输数据 VData
   VData tVData = new VData();    

//1-查询暂交费表，将TempFeeNo输入Schema中传入，查询得到Set集
//2-将Set的各个项的交费金额累计
    tVData.clear();     
    tLJTempFeeSchema = new LJTempFeeSchema();
    tLJTempFeeSet = new LJTempFeeSet();    
    TempFeeQueryUI tTempFeeQueryUI = new TempFeeQueryUI();    
    tLJTempFeeSchema.setTempFeeNo(TempFeeNo);
//    tLJTempFeeSchema.setOtherNoType("1");//其它号码类型为1(集体保单号类型)
    tVData.add(tLJTempFeeSchema);
System.out.println("Start first");    
    if(!tTempFeeQueryUI.submitData(tVData,"QUERY"))
    {
        Content = " 查询暂交费表失败，原因是: " + tTempFeeQueryUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
    }
    else
    { //第一个条件判断
        tVData.clear();    
	tVData = tTempFeeQueryUI.getResult();
        tLJTempFeeSet.set((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));
        recordCount=tLJTempFeeSet.size();
System.out.println("recordCount="+recordCount);         
        for(int i=1;i<=recordCount;i++)
        {
         tLJTempFeeSchema=(LJTempFeeSchema)tLJTempFeeSet.get(i);
         tempMoney=tempMoney+tLJTempFeeSchema.getPayMoney(); 
        }

System.out.println("查询暂交费表结束");     
System.out.println("tempMoney="+tempMoney);

//3-查询应收总表，将将TempFeeNo输入Schema中传入，查询得到单个Schema
//4-将Schema中的总应收金额取出

    tVData.clear();  
    tLJSPaySchema = new LJSPaySchema(); 
    tLJSPaySet = new LJSPaySet();       
    VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();    
    tLJSPaySchema.setGetNoticeNo(TempFeeNo);
    tVData.add(tLJSPaySchema);    
    if(!tVerDuePayFeeQueryUI.submitData(tVData,"QUERY"))
    {
        Content = " 查询应收总表失败，原因是: " + tVerDuePayFeeQueryUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
    }
    else
    { //第二个条件判断
        tVData.clear();   
	tVData = tVerDuePayFeeQueryUI.getResult();	 
        tLJSPaySet.set((LJSPaySet)tVData.getObjectByObjectName("LJSPaySet",0)); 
        tLJSPaySchema=(LJSPaySchema)tLJSPaySet.get(1);  
        sumDueMoney = sumDueMoney+tLJSPaySchema.getSumDuePayMoney();
        GrpPolNo =  tLJSPaySchema.getOtherNo(); //得到集体保单号
    
System.out.println("查询应收总表结束");   
System.out.println("SumDueMoney="+sumDueMoney);
   
//5-比较两个金额值，相等则核销
   if(sumDueMoney!=tempMoney)
   {
        Content = " 查询失败，原因是:应收总额不等于暂交金额之和！ " ;
        FlagStr = "Fail";   
   }
 else
  {  
  //第三个条件判断
//6.1-核销：应收总表和暂交费表数据填充实收总表，应收集体交费表填充实收集体交费表,应收个人表填充实收个人表
//6.2-核销：暂交费表，暂交费分类表，核销标志置为1
//6.3-核销：应收总表，应收集体交费表,应收个人表删除

//对应字段如下：
// 应收总表的    通知书号码,   其它号码，     其它号码类型，      投保人客户号码，总应收金额，
//对应实收总表的 交费收据号码，应收/实收编号，应收/实收编号类型， 投保人客户号码，总实交金额，交费日期


//上面已经查询出 暂交费表和应收总表，再查询出暂交费分类表和应收个人表
//置入VData中，传到后台，进行事务操作

//查询暂交费分类表
   tVData.clear(); 
   tLJTempFeeClassSchema = new LJTempFeeClassSchema();
   tLJTempFeeClassSet    = new LJTempFeeClassSet();
   tLJTempFeeClassSchema.setTempFeeNo(TempFeeNo);
   TempFeeClassQueryUI tTempFeeClassQueryUI = new TempFeeClassQueryUI();    
   tVData.add(tLJTempFeeClassSchema);
   if(!tTempFeeClassQueryUI.submitData(tVData,"QUERY"))
    {
        Content = " 查询暂交费分类表失败，原因是: " + tTempFeeClassQueryUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
    }      
   else
   { //第四个判断
        tVData.clear();    
	tVData = tTempFeeClassQueryUI.getResult();
        tLJTempFeeClassSet.set((LJTempFeeClassSet)tVData.getObjectByObjectName("LJTempFeeClassSet",0));  
 
System.out.println("查询暂交费分类表结束");   

//查询应收集体交费表 
    tVData.clear(); 
    tLJSPayGrpSchema  = new LJSPayGrpSchema();
    tLJSPayGrpSet     = new LJSPayGrpSet();
    tLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
    DuePayFeeCollQueryUI tDuePayFeeCollQueryUI = new DuePayFeeCollQueryUI();    
    tVData.add(tLJSPayGrpSchema);    
    if(!tDuePayFeeCollQueryUI.submitData(tVData,"QUERY"))
    {
        Content = " 查询应收集体交费表 失败，原因是: " + tDuePayFeeCollQueryUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
    }     
    else
   {  //第五个判断   
   
        tVData.clear();    
	tVData = tDuePayFeeCollQueryUI.getResult();
        tLJSPayGrpSet.set((LJSPayGrpSet)tVData.getObjectByObjectName("LJSPayGrpSet",0));  
        tLJSPayGrpSchema =(LJSPayGrpSchema)tLJSPayGrpSet.get(1); 
System.out.println("查询应收集体交费表 结束");   

//查询应收个人交费表
//查询表中记录集体保单号字段=GrpPolNo的个人纪录，循环处理

    tVData.clear(); 
    tLJSPayPersonSchema  = new LJSPayPersonSchema();
    tLJSPayPersonSet     = new LJSPayPersonSet();
    tLJSPayPersonSchema.setGrpPolNo(GrpPolNo);
    DuePayPersonFeeQueryUI tDuePayPersonFeeQueryUI = new DuePayPersonFeeQueryUI();    
    tVData.add(tLJSPayPersonSchema);    
    if(!tDuePayPersonFeeQueryUI.submitData(tVData,"QUERY"))
    {
        Content = " 查询应收个人交费表失败，原因是: " + tDuePayPersonFeeQueryUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
    }     
    else
   {  //第六个判断
        tVData.clear();    
	tVData = tDuePayPersonFeeQueryUI.getResult();
        tLJSPayPersonSet.set((LJSPayPersonSet)tVData.getObjectByObjectName("LJSPayPersonSet",0));  
System.out.println("查询应收个人交费表结束");   

  tVData.clear();
  tVData.add(tLJTempFeeSet);    
  tVData.add(tLJSPaySchema);
  tVData.add(tLJSPayGrpSchema);
  tVData.add(tLJTempFeeClassSet);
  tVData.add(tLJSPayPersonSet); 
     
//执行事务操作
//6.1-核销：应收总表和暂交费表数据填充实收总表，应收集体交费表填充实收集体交费表,应收个人表填充实收个人表
//6.2-核销：暂交费表，暂交费分类表，核销标志置为1
//6.3-核销：应收总表，应收集体交费表,应收个人表删除
/////////////////////////////////////////////////////////////  
  FinFeeVerifyCollUI tFinFeeVerifyCollUI = new FinFeeVerifyCollUI();
  tFinFeeVerifyCollUI.submitData(tVData,"VERIFY");
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tFinFeeVerifyCollUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 核销事务成功";
      FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 核销事务失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
}//对应第六个判断 
}//对应第五个
}//对应第四个
}//对应第三个
}//对应第二个
}//对应第一个     

%>
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML> 