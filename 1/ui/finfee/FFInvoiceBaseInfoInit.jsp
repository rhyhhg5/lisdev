<%
//程序名称：
//程序功能：
//创建日期：2007-05-18
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
var ManageCom = "<%=tGI.ManageCom%>";  //操作员编号

function initForm()
{
    try
    {
        initInputBox();
        initFFInvoiceBaseInfoGrid();
        initElementtype();
    }
    catch(e)
    {
        alert("初始化界面错误!");
    }
}

function initInputBox()
{
    fm.all('ComCode').value = ManageCom;
    fm.all('TaxpayerNo').value = "";
    fm.all('TaxpayerName').value = "";
    fm.all('InvoiceCode').value = "";
    fm.all('InvoiceCodeExp').value = "";
    fm.all('InvoiceStartNo').value = "";
    fm.all('InvoiceEndNo').value = "";
}

var FFInvoiceBaseInfoGrid;
function initFFInvoiceBaseInfoGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="机构编号";         	  //列名
    iArray[1][1]="60px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="纳税人标识号";         	
    iArray[2][1]="100px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="纳税人名称";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="发票代码";      //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[5]=new Array();
    iArray[5][0]="发票扩展代码";              //列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="发票起始号";              //列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="发票终止号";              //列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="操作员";              //列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    FFInvoiceBaseInfoGrid = new MulLineEnter("fm", "FFInvoiceBaseInfoGrid"); 
    //设置Grid属性
    FFInvoiceBaseInfoGrid.mulLineCount = 0;
    FFInvoiceBaseInfoGrid.displayTitle = 1;
    FFInvoiceBaseInfoGrid.locked = 1;
    FFInvoiceBaseInfoGrid.canSel = 1;
    FFInvoiceBaseInfoGrid.canChk = 0;
    FFInvoiceBaseInfoGrid.hiddenSubtraction = 1;
    FFInvoiceBaseInfoGrid.hiddenPlus = 1;
    FFInvoiceBaseInfoGrid.loadMulLine(iArray);
    FFInvoiceBaseInfoGrid.selBoxEventFuncName = "onclkSelBox";
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>