<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：2007-05-22
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	VData mInputData = new VData();

	String feeState = (String) request.getParameter("feeState");

	if ("Should".equals(feeState)) {
	
		String tRadio[] = request.getParameterValues("InpYingGridSel");
		String[] feeNo = request.getParameterValues("YingGrid1"); 
		String[] otherNo = request.getParameterValues("YingGrid3");
		String[] bankCode = request.getParameterValues("YingGrid6");
		String[] bankAccNo = request.getParameterValues("YingGrid7");
		String[] accName = request.getParameterValues("YingGrid8");
		String[] serialNo = request.getParameterValues("YingGrid13");
		String[] shouldDate = request.getParameterValues("YingGrid11");
		String[] payToDate = request.getParameterValues("YingGrid12");
		
		for (int i = 0; i < tRadio.length; i++) {
			
			if ("1".equals(tRadio[i])) {
			
				System.out.println("feeNo:" + feeNo[i]);
				System.out.println("otherNo:" + otherNo[i]);
				System.out.println("bankCode:" + bankCode[i]);
				System.out.println("bankAccNo:" + bankAccNo[i]);
				System.out.println("accName:" + accName[i]);
				System.out.println("serialNo:" + serialNo[i]);
				
				TransferData tTransferData = new TransferData();
				
				tTransferData.setNameAndValue("BankCode", bankCode[i]);
				tTransferData.setNameAndValue("BankAccNo", bankAccNo[i]);
				tTransferData.setNameAndValue("AccName", accName[i]);
				tTransferData.setNameAndValue("FeeNo", feeNo[i]);
				tTransferData.setNameAndValue("OtherNo", otherNo[i]);
				tTransferData.setNameAndValue("FeeFlag", (String) request.getParameter("feeFlag"));
				tTransferData.setNameAndValue("FeeState", feeState);
				tTransferData.setNameAndValue("SerialNo", serialNo[i]);
				tTransferData.setNameAndValue("ShouldDate", shouldDate[i]);
				tTransferData.setNameAndValue("PayToDate", payToDate[i]);
				tTransferData.setNameAndValue("PrintFlag", null);
				tTransferData.setNameAndValue("ConfDate", null);
				tTransferData.setNameAndValue("FeeDate", null);

				mInputData.add(tTransferData);
				break;
			}
		}
		
	} else if ("Conf".equals(feeState)) {
	
		String tRadio[] = request.getParameterValues("InpZanGridSel");
		String[] feeNo = request.getParameterValues("ZanGrid1"); 
		String[] otherNo = request.getParameterValues("ZanGrid3");
		String[] feeDate = request.getParameterValues("ZanGrid6");
		String[] confDate = request.getParameterValues("ZanGrid8");
		String[] printFlag = request.getParameterValues("ZanGrid10");
		
		for (int i = 0; i < tRadio.length; i++) {
			
			if ("1".equals(tRadio[i])) {
			
				System.out.println("feeNo:" + feeNo[i]);
				System.out.println("otherNo:" + otherNo[i]);
				
				TransferData tTransferData = new TransferData();
				
				tTransferData.setNameAndValue("BankCode", null);
				tTransferData.setNameAndValue("BankAccNo", null);
				tTransferData.setNameAndValue("AccName", null);
				tTransferData.setNameAndValue("FeeNo", feeNo[i]);
				tTransferData.setNameAndValue("OtherNo", otherNo[i]);
				tTransferData.setNameAndValue("FeeFlag", (String) request.getParameter("feeFlag"));
				tTransferData.setNameAndValue("FeeState", feeState);
				tTransferData.setNameAndValue("SerialNo", null);
				tTransferData.setNameAndValue("PayToDate", null);
				tTransferData.setNameAndValue("ShouldDate", null);
				if(printFlag[i] != null && !printFlag[i].trim().equals("") && !printFlag[i].equals("null")){
					tTransferData.setNameAndValue("PrintFlag", "true");
				} else {
					tTransferData.setNameAndValue("PrintFlag", "false");
				}
				tTransferData.setNameAndValue("ConfDate", confDate[i]);
				tTransferData.setNameAndValue("FeeDate", feeDate[i]);

				mInputData.add(tTransferData);
				break;
			}
		}
	}
	
	FeeStateQueryBL tFeeStateQueryBL = new FeeStateQueryBL();

	if ("it001".equals(tGI.Operator)) {
		GetFeeStateInfoBL.reload();
	}

	tFeeStateQueryBL.submitData(mInputData, "");

	Set tSet = tFeeStateQueryBL.getResult();

	Iterator tI = tSet.iterator();
%>

<html>
	<script language="javascript">
    try
    {
		<%	
		while (tI.hasNext()) {
			%>
			parent.fraInterface.afterSubmit("<%=tI.next()%>");
			<%
		}
		%>
    }
    catch(e)
    {
    }
</script>
</html>
