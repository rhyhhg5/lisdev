<html> 
<%
//程序名称：FinFeePayInput.jsp
//程序功能：财务付费表的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="GetAdvanceInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GetAdvanceInit.jsp"%>
</head>
<body  onload="initForm();initElementtype()" >
<Form action="./GetAdvanceSave.jsp" method=post name=fm target="fraSubmit">
    <Table>
        <TR>
            <TD class=common>
               <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
            </TD>
            <TD class= titleImg>
                 预收保费收费录入
            </TD>
        </TR>
    </Table>        
     
     <Table class= common>    
     <TR  class= common> 
          <TD  class= title>
          </TD>           
          <TD  class= input>
          </TD>    
          <TD  class= title>
            业务员
          </TD>
         <TD class="input" width="25%">
           <Input class=common name=GroupAgentCode  elementtype="nacessary" VALUE="" MAXLENGTH=0 >
           <Input class=common name=AgentCode VALUE="" MAXLENGTH=0 type='hidden'>
           <input class=cssButton type="button" value="查  询" style="width:60" onclick="queryAgent()" verify="业务员|notnull" > 
        </TD>                                  
       </TR>             
       <TR  class= common> 
          <TD  class= title>
            收费金额
          </TD>           
          <TD  class= input>
            <Input class= common name=PayMoney verify="收费金额|notnull&num" elementtype="nacessary">
          </TD>                   
          <TD  class= title>
            收费方式
          </TD>          
          <TD  class= input width="25%">
            <Input class=codeNo name=PayMode verify="收费方式|notnull" onchange="showBankAccNo();" ondblclick="return showCodeList('PayMode2',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKey('PayMode2',[this,PayModeName],[0,1]);" ><input class=codename name=PayModeName readonly=true elementtype="nacessary">
            <input type =button class=cssButton value="方式说明" onclick="payModeHelp();" style="width:70"> 
          </TD>                      
       </TR>
       <TR  class= common>                    
          <TD  class= title>
            交费单位名称
          </TD>           
          <TD  class= input>
            <Input class= "common" name=AppntName verify="交费单位名称|notnull" elementtype="nacessary">
          </TD>          
          <TD  class= title>
            <div id="divEnterAccTitle" style="display:">
                交费日期
            </div>
          </TD>
          <TD  class= input>
            <div id="divEnterAccDate" style="display:">
                <Input class="coolDatePicker"   name=EnterAccDate           
                verify="财务到帐日期|notnull&date">
                <font color="#FF0000" align="Left">*</font>
            </div>
          </TD>  
        </TR>                                                                   
     </TABLE> 
                                      
     <Div id='divBankAccNo' style='display:' >
       <table class=common>
      <TR  class= common>
         <TD  class= title>
            对方开户银行
         </TD>  
         <TD  class= input>
           <Input class=codeNo name=AccBank ondblclick="querybank();" onkeyup="querybank();"><input class=codename name=AccBankName readonly=true >                        
         </TD> 
         <TD class= title>
           票据号
         </TD>
         <TD class= input>
           <Input class = common name=ChequeNo>
         </TD>
       </TR>   
       <TR class=common>                                  
         <TD  class= title>
            对方银行帐号
         </TD>
          <TD  class= input>
            <Input class= common  name=BankAccNo >
          </TD>
          <TD class=title>
            对方银行帐户名
          </TD>
          <TD class=input>
            <Input class= common  name=AccName >
          </TD>
        </TR> 
        <TR class=common>  
          <TD  class= title>
              本方银行
          </TD>
          <TD  class= input>
        	<Input NAME=getbankcode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('getbankcode',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('getbankcode',[this,BankCodeName],[0,1]);" ><input class=codename name=BankCodeName readonly=true >
          </TD>
          <TD  class= title>
             银行账号
          </TD>
          <TD  class= input>
            <Input NAME=getbankaccno CodeData="" MAXLENGTH=20 CLASS=code ondblclick="return showCodeList('getbankaccno',[this],[0],null,fm.getbankcode.value,'BankCode');" onkeyup="return showCodeListKey('paybankaccno',[this],[0],null,fm.getbankcode.value,'BankCode');"  >
          </TD>
        </TR> 
        </table> 
        </Div>
        <br>
        <font color="#FF0000" align="Left">
         使用转账支票方式收费需要在“到账确认”菜单进行确认
         </font> 
        <br>
         <br>
        <INPUT VALUE="保  存" Class=cssButton TYPE=button onclick="submitForm();">
        <INPUT VALUE="批量导入" Class=cssButton TYPE=button onclick="BatchGet();">
        <input type="hidden" name="ManageCom">
</Form>    

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 