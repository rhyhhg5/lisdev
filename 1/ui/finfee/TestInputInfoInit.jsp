<%
//程序名称：
//程序功能：
//创建日期：2007-05-18
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
var ManageCom = "<%=tGI.ManageCom%>";  //操作员编号

function initForm()
{
    try
    {
        initInputBox();
        initFFInvoiceBaseInfoGrid();
        initElementtype();
    }
    catch(e)
    {
        alert("初始化界面错误!");
    }
}

function initInputBox()
{
/*fm.all('ComCode').value = ManageCom;
   // alert(ManageCom);
   fm.all('TaxpayerNo').value = "";
    fm.all('TaxpayerName').value = "";
    fm.all('InvoiceCode').value = "";
    fm.all('InvoiceCodeExp').value = "";
    fm.all('InvoiceStartNo').value = "";
    fm.all('InvoiceEndNo').value = ""; */
    fm.all('TempFeeNo').value = "";
    fm.all('TempFeeType').value = "";
    fm.all('RiskCode').value = "";
    fm.all('BackUpSerialNo').value = "";  
    fm.all('MakeTime').value = "";
    fm.all('MakeDate').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";  
    fm.all('Operator').value = ""; 
    
}

var FFInvoiceBaseInfoGrid;
function initFFInvoiceBaseInfoGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="TempFeeNo";         	  //列名
    iArray[1][1]="100px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="TempFeeType";         	
    iArray[2][1]="20px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="RiskCode";         	  //列名
    iArray[3][1]="50px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="BackUpSerialNo";              //列名
    iArray[4][1]="100px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
/***************************************************************************/
    
    iArray[5]=new Array();
    iArray[5][0]="MakeTime";         	  //列名
    iArray[5][1]="60px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0; 
    
    iArray[6]=new Array();
    iArray[6][0]="MakeDate";         	  //列名
    iArray[6][1]="70px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0; 
    
    iArray[7]=new Array();
    iArray[7][0]="ModifyDate";         	  //列名
    iArray[7][1]="70px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0; 
    
    iArray[8]=new Array();
    iArray[8][0]="ModifyTime";         	  //列名
    iArray[8][1]="60px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0; 
    
    iArray[9]=new Array();
    iArray[9][0]="Operator";         	  //列名
    iArray[9][1]="60px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0; 
    
    
    
    FFInvoiceBaseInfoGrid = new MulLineEnter("fm", "FFInvoiceBaseInfoGrid"); 
    //设置Grid属性
    FFInvoiceBaseInfoGrid.mulLineCount = 0;
    FFInvoiceBaseInfoGrid.displayTitle = 1;
    FFInvoiceBaseInfoGrid.locked = 1;
    FFInvoiceBaseInfoGrid.canSel = 1;
    FFInvoiceBaseInfoGrid.canChk = 0;
    FFInvoiceBaseInfoGrid.hiddenSubtraction = 1;
    FFInvoiceBaseInfoGrid.hiddenPlus = 1;
    FFInvoiceBaseInfoGrid.loadMulLine(iArray);
    FFInvoiceBaseInfoGrid.selBoxEventFuncName = "onclkSelBox";
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>