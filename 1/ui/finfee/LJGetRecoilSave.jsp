<%
//程序名称：LJGetRecoilSave.jsp
//程序功能：
//创建日期：2008-12-17
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  CErrors tError = null;

  String FlagStr = "";
  String Content = "";
  String mTempfeeNo = request.getParameter("TempfeeNo");
  String mPayMode = request.getParameter("paymode");
  String mBankCode = request.getParameter("BankCode");
  String mAccNo = request.getParameter("AccNo");
  String mGetBankCode = request.getParameter("getbankcode");
  String mGetBankAccNo = request.getParameter("getbankaccno");
  String mOperateType= request.getParameter("OperateType");

  TransferData tTransferData = new TransferData();
  if(!StrTool.cTrim(mTempfeeNo).equals("")){
			tTransferData.setNameAndValue("TempfeeNo",mTempfeeNo);
 			tTransferData.setNameAndValue("PayMode",mPayMode);
 			tTransferData.setNameAndValue("BankCode",mBankCode);
 			tTransferData.setNameAndValue("AccNo",mAccNo);
 			tTransferData.setNameAndValue("GetBankCode",mGetBankCode);
 			tTransferData.setNameAndValue("GetBankAccno",mGetBankAccNo);
	}

  VData tVData = new VData();
  LJRecoilUI mLJRecoilUI=new LJRecoilUI();
  try
  {
  	tVData.addElement(tTransferData);
  	tVData.addElement(globalInput);
    mLJRecoilUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mLJRecoilUI.mErrors;
    if (!tError.needDealError())
    {
      Content = "保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


