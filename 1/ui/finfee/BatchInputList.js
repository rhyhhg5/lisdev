  //               该文件中包含客户端需要处理的函数和事件
//控制界面上的mulLine的显示条数
var mulLineShowCount = 15;
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var bankflag="";
//提交，保存按钮对应操作

//排盘导入
function submitForm()
{
  var i = 0;
  getImportPath();
  ImportFile = fm.all('FileName').value;
  if ( ImportFile == null ||
       ImportFile == "")
  {
    alert("请浏览要导入的磁盘文件");
    return;
  }
  else
  {
    fm.action = "./BatchInputListSave.jsp?ImportPath=" + ImportPath;
    var a= ImportFile.lastIndexOf("\\");
    var b= ImportFile.lastIndexOf(".");
    var BatchNo = ImportFile.substring(a+1,b);
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.open(urlStr,"",'height=250,width=500,top=250,left=270,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
    fm.submit();
    showInfo.close();
    
     
  }

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result )
{    
      if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     
    //执行下一步操作
  }
  queryClick(Result);
 
}

function queryClick(Result)
{
    QueryGrpGrid.clearData();
    userCurPage = 0;
		
    userArray = decodeEasyQueryResult(Result);
 
    if (userArray == null) {
    	alert("没有银行导入信息。");
    	return;
    }

    userArrayLen = userArray.length;
    fillUserGrid();
}


function fillUserGrid()
{
   QueryGrpGrid.clearData("QueryGrpGrid");

   for (var i = 0; i < mulLineShowCount; i++) {
  	var offset = i  + userCurPage*mulLineShowCount;
    if (userArray[offset][1]==null||userArray[offset][1]==""){
       alert("存在相同的银行名称！！");
      }else{ 
   	if (offset < userArrayLen) {
   	    QueryGrpGrid.addOne("QueryGrpGrid");
   	    QueryGrpGrid.setRowColData(i,1,userArray[offset][0]);
   	    QueryGrpGrid.setRowColData(i,2,userArray[offset][1]);
   	    QueryGrpGrid.setRowColData(i,3,userArray[offset][3]);
   	    QueryGrpGrid.setRowColData(i,4,"否");
   	  //  QueryGrpGrid.setRowColData(i,5,userArray[offset][4]);
     //alert(userArray[offset][1]);
   	} 
   	else {
   	    QueryGrpGrid.setRowColData(i,1,"");
   	    QueryGrpGrid.setRowColData(i,2,"");
        QueryGrpGrid.setRowColData(i,3,"");
   	    QueryGrpGrid.setRowColData(i,4,"");
   	  //  QueryGrpGrid.setRowColData(i,5,"");
   	}
   	}
   }

   //下面的代码是为了使翻页时序号能正确显示
   for (var i = 0; i < mulLineShowCount; i++) {
		var offset = i  + userCurPage*mulLineShowCount;
        fm.all("QueryGrpGridNo")[i].value = offset + 1;
   }

}

function userFirstPage()
{
	if (userArrayLen == 0)
	    return;

	userCurPage = 0;
	fillUserGrid();
}

function userLastPage()
{
	if (userArrayLen == 0)
	    return;
	
	while ((userCurPage + 1)*mulLineShowCount <= userArrayLen)
	    userCurPage++;
 
	fillUserGrid();
}


function userPageDown()
{
	if (userArrayLen == 0)
	    return;

    if (userArrayLen <= (userCurPage + 1) * mulLineShowCount) {
    	alert("已达尾页");
    } else {
        userCurPage++;
        fillUserGrid();
    }
}

function userPageUp()
{
	if (userArrayLen == 0)
	    return;

    if (userCurPage == 0) {
    	alert("已到首页");
    } else {
        userCurPage--;
        fillUserGrid();
    }
}



function getImportPath ()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar = 'XmlPath' ";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("未找到上传路径");
    return;
  }

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  ImportPath = turnPage.arrDataCacheSet[0][0];
}
