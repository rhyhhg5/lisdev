<html> 
<%
//程序名称：BatchPoundagePayInput.jsp
//程序功能：手续费批量付费
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
	<SCRIPT src="BatchPoundagePayInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BatchPoundagePayInit.jsp"%>
</head>
<body  onload="initForm();" >
<Form action="" method=post name=fm target="fraSubmit">
	<Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,queryCondition);">
    		</TD>
    		<TD class= titleImg>
    			 查询条件
    		</TD>
    	</TR>
    </Table> 
    <div id= "queryCondition" align=center style= "display: ''">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input>
          	<Input class=codeNo name=paytype  value="7" ><input class=codename name=paytypeName value="手续费付费" readonly=true>       
          </TD>
          <TD  class= title>付费方式</TD>
          <TD  class= input>
          	<Input class=codeNo name=PayMode  ondblclick="return showCodeList('paymode2',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKey('paymode2',[this,PayModeName],[0,1]);"><input class=codename name=PayModeName readonly=true > 
          </TD>
          <TD  class= title>支付对象</TD>
          <TD  class= input>
          	<Input class=codeNo name=bankcode  ondblclick="return showCodeList('shpmanagecom',[this,bankname],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('shpmanagecom',[this,bankname],[0,1]);"><input class=codename name=bankname readonly=true >
          </TD> 
          <% 
          	//<TD  class= title>中介机构</TD>
          	//<TD  class= input>
          	//<Input class=codeNo name=bankcode1  ondblclick="return showCodeList('bank',[this,bankname],[0,1]);" onkeyup="return showCodeListKey('bank',[this,bankname],[0,1]);"><input class=codename name=bankname readonly=true >
          	//</TD>
          %>  
        </TR>
        <TR class= common>
        	<TD  class= title> 起始日期</TD>
	        <TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=StartDate >  </TD>        
	        <TD  class= title> 截至日期</TD> 
	        <TD  class= input> <Input class= "coolDatePicker" dateFormat="short" name=EndDate > </TD>
	        <TD  class= title>给付批次号</TD>
          	<TD  class= input><Input class=common name="HCNo" ></TD> 
      	</TR>
    </Table> 
    </div> 
     <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();">  
     <INPUT VALUE="下  载" Class=cssButton TYPE=button onclick="queryDown();"> 
     <INPUT VALUE="批量导入" Class=cssButton TYPE=button onclick="batchPay();"> 
     <INPUT type=hidden name=sql>
    <Table>
    	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			 手续费应付总表信息
    		</TD>
    </Table>  
  <input type=hidden name=ComCode>  
  <input type=hidden name=Operate>	
 <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanLJAGetGrid" ></span> 
  	    </TD>
      </TR>
    </Table>					
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>	
 <br>
 <div id="divShowTotal" style="display:''">
 	<table class="common">
 	   <tr class="common">
          <TD class= title>本方银行</TD>
          <TD class= input>
				<Input class=codeNo name=tBankCode  
				ondblclick="return showCodeList('paybankcode',[this,tBankName],[0,1]);" 
				onkeyup="return showCodeListKey('paybankcode',[this,tBankName],[0,1]);"><input class=codename name=tBankName readonly=true >
          </TD>
          <TD class= title>本方账号</TD>
          <TD class= input>
              <Input class=code name=tBankAccNo  length=20
              	ondblclick="return showCodeList('paybankaccno',[this],[0,1],null,fm.tBankCode.value,'bankcode');" 
              	onkeyup="return showCodeListKey('paybankaccno',[this],[0,1],null,fm.tBankCode.value,'bankcode');" readonly=true>
		  </TD>
		  <TD class= title>票据号</TD>
		<TD class= input><Input class=common name="ChequeNo"></TD>  
		</tr>
		<tr class="common">
			<TD class= title>对方银行</TD>
			<TD class= input>
				<Input class=codeNo name=otherBankCode
					ondblclick="return showCodeList('bank',[this,otherBankName],[0,1]);" 
					onkeyup="return showCodeListKey('bank',[this,otherBankName],[0,1]);"><input class=codename name=otherBankName readonly=true >
			</TD>
			<TD class= title>对方账号</TD>
			<TD class= input><Input class=common name="otherBankAccNo"></TD>   
			<TD  class= title>给付总金额：</TD>
			<TD  class= input>
				<Input class=common name="payTotal" readonly="true" >
			</TD> 
		</tr>
 	</table>
 </div>	
  <br>			
  <Div id='Savebutton' style='display:' >   
      <INPUT VALUE="保  存" Class=cssButton TYPE=button onclick="submitForm();">
 </Div>
</Form>    
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>