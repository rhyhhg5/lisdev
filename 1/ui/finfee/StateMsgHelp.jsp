
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body >
<Form method=post name=fm target="fraSubmit">
	<br>
	<br> 
	<font size=3>
	<b>状态明细说明</b> 
		<p></p>
		对于应收不在途信息： 
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;1、若处于加锁状态，则状态明细为该笔数据处于加锁状态，需要对该笔数据进行解锁操作，在解锁后才能进行银行转账处理。
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;2、若处于未加锁状态，且收费方式为银行转账，则状态明细为： 
		<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据会由集中代收付进行提取。
    	<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2） 该笔数据会由银行接口进行提取。
    	<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;3） 该笔数据会由总公司邮储进行提取。
    	<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;3、若处于未加锁状态，且收费方式不为为银行转账，则状态明细为： 
    	<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据可由界面进行收费。
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;4、若处于未加锁状态，且业务类型为续期，则状态明细为： 
		<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据尚未进行收费，可进行续期应收作废操作。	
    	<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;5、若加锁标志为其他，则状态明细为： 
		<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据已由总公司邮储提取，尚未进行提盘审核。		
    	<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2） 该笔数据已由总公司集中代收付提取。
    	<p></p>
		对于应收在途信息：	 	    
		<p></p>	  
		&nbsp;&nbsp;&nbsp;&nbsp;1、若批次状态为待发盘状态
		<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据由总公司集中代收付提取.  则状态明细为：该笔数据已由集中代收付提取，现尚未发盘。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2） 该笔数据由银行接口提取，则状态明细为：该笔数据由银行接口提取,现在尚未进行生成文件操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;2、若批次状态为待回盘状态
		<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据由总公司集中代收付提取.  则状态明细为：该笔数据已由集中代收付发盘，现尚未回盘。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2） 该笔数据由总公司邮储提取.  则状态明细为：该笔数据已由邮储发盘，现尚未回盘。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3） 该笔数据由银行接口提取.  则状态明细为：该笔数据已由银行接口发盘，现尚未进行文件返回操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;3、若批次状态为待返回处理状态
		<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据由银行接口提取.  则状态明细为：该笔数据已由银行接口回盘，现尚未进行返回处理操作。
        <p></p>
		对于实收信息：	 	    
		<p></p>	  
		&nbsp;&nbsp;&nbsp;&nbsp;1、若收费方式为支票类，且尚未核销，则状态明细为：该笔数据收费方式为支票类，尚未进行到账确认操作。
		<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;2、若数据尚未核销，且业务类型为首期收费，则状态明细为：
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1）该笔数据可进行暂收退费操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2）该笔数据尚未核销，可对该张保单进行签单操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3）该笔数据尚未核销，不可打印发票。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;3、若数据尚未核销，且业务类型为续期收费，则状态明细为：
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1）该笔数据尚未核销，不可打印发票。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;4、若数据尚未核销，且业务类型为保全收费，则状态明细为：
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1）该笔数据尚未核销，不可打印发票。
        <p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;5、若数据已核销，且业务类型为续期收费，则状态明细为：该笔数据可进行续期实收保费转出操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;6、若数据已核销，尚未打印发票，则状态明细为：该笔数据还未打印发票，可进行发票打印操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;7、若数据已核销，且已打印发票，则状态明细为：该笔数据已处理完毕。
    	<p></p>
		对于应付不在途信息：
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;1、若处于加锁状态，则状态明细为该笔数据处于加锁状态，需要对该笔数据进行解锁操作，在解锁后才能进行银行转账处理。
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;2、若处于未加锁状态，且收费方式为银行转账，则状态明细为： 
		<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据会由集中代收付进行提取。
    	<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2） 该笔数据会由银行接口进行提取。
    	<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;3） 该笔数据会由总公司邮储进行提取。
    	<p></p>
        &nbsp;&nbsp;&nbsp;&nbsp;3、若处于未加锁状态，且付费方式不为为银行转账，则状态明细为： 
    	<p></p>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据可由界面进行付费。
    	<p></p>
		对于应付在途信息：
    	<p></p>	  
		&nbsp;&nbsp;&nbsp;&nbsp;1、若批次状态为待发盘状态
		<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据由总公司集中代收付提取.  则状态明细为：该笔数据已由集中代收付提取，现尚未发盘。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2） 该笔数据由银行接口提取，现在尚未进行生成文件操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;2、若批次状态为待回盘状态
		<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据由总公司集中代收付提取.  则状态明细为：该笔数据已由集中代收付发盘，现尚未回盘。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2） 该笔数据由总公司邮储提取.  则状态明细为：该笔数据已由邮储发盘，现尚未回盘。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3） 该笔数据由银行接口提取.  则状态明细为：该笔数据已由银行接口发盘，现尚未进行文件返回操作。
    	<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;3、若批次状态为待返回处理状态
		<p></p> 
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1） 该笔数据由银行接口提取.  则状态明细为：该笔数据已由银行接口回盘，现尚未进行返回处理操作。
        <p></p>
		对于实付信息：
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;1、数据已核销，则状态明细为：该笔数据已处理完毕。
		 <p></p>
		其他信息：
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;1、客户开户账号含有特殊字符，则状态明细为：客户开户账号含有特殊符号，不符合集中代收付账号规则，集中代收付无法对该笔数据进行提取。
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;2、该笔数据未到应收/应付日期，则状态明细为：该笔数据未到应收/应付费日期，银行接口、集中代收付不会对该笔数据进行提取。
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;3、该笔数据已过最晚缴费日期，则状态明细为：该笔数据已过最晚缴费日期，集中代收付不会对该笔数据进行提取。
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;4、该笔数据应收/应付日期为空，则状态明细为：该笔数据应收/应付费日期为空，银行接口、集中代收付无法提取该笔数据。
		<p></p>
		&nbsp;&nbsp;&nbsp;&nbsp;5、该笔数据为对公业务，则状态明细为：该笔数据为对公业务，集中代收付不会进行提取。
		<p></p>    	
</font>
	<table class= common>
		<tr><td align=center><input type =button class=cssButton value="关 闭" onclick="window.close();"></td></tr>
</table>

</body>
</html>
