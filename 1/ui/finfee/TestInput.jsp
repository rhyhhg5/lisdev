<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2013-4-16
//创建人  ：WS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="TestInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="TestInit.jsp"%> 
<body onload="initForm();">
	<form action="TestSave.jsp" method="post" name="fm" target="fraSubmit">
		<table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIBaseInfo);">
                </td>
                <td class="titleImg">添加机构IP信息</td>
            </tr>
            <tr class=common >
            	<td class=title>机构编码</td><td class= input><input class="common" name="comCode"></td>
            	<td class=title>IP地址</td><td class= input><input class="common" name="ipAddr"></td>
            </tr>
        </table>
        <br>
    	<INPUT VALUE="添  加" class= cssButton TYPE=button onclick="insertCodeAndIP();">
        <br><br><hr>
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIBaseInfo);">
                </td>
                <td class="titleImg">查询机构IP信息</td>
            </tr>
        </table>
        <div id="divComCode" style= "display:''">
            <table class="common" align='center' >
                <tr class="common">
                    <TD  class= title>
					        管理机构
					</TD>
					<td>
						<Input name=ManageCom class='codeno' id="ManageCom"  verify="管理机构|NOTNULL"
				           ondblclick="return showCodeList('comcodetest',[this,ManageComName],[0,1]);"  
				           onkeyup="return showCodeListKey('comcodetest',[this,ManageComName],[0,1]);"
				          ><Input name=ManageComName class="codename" >
					</td>
                </tr>
            </table>
        </div>
        <input type="hidden" name="methodName" value="" />
        <input value="查  询" class="cssButton" type="button" onclick="easyQueryClick();">
        <p />

        <div id="divComCodeAndIpGrid" style="display:''">
            <span id="spanComCodeAndIpGrid"></span>
        </div>
    </form>
    <br><br><hr>
    <form action="TestSave.jsp" method="post" name="fmUpdate" target="fraSubmit">
        <div id="divUpdate" style="display:'none'">
                <table class="common" align='center' >
                <tr class="common">
                    <td class="title">机构编码</td>
                    <td class="input"><input class="common" name="upComCode" /></td>
                    <td class="title">IP地址</td>
                    <td class="input"><input class="common" name="upIPAddr" /></td>
                </tr>
            </table>
            <input type="hidden" name="methodName" value="" />
            <input value="修  改" class="cssButton" type="button" onclick="updateCodeAndIP();">
            <input value="删  除" class="cssButton" type="button" onclick="deleteCodeAndIP();">
        </div>
    </form>
    <span id="spanCode"  style="display:none; position:absolute; slategray"></span>
</body>