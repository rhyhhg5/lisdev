<html> 
<%
//程序名称：BatchPayInput.jsp
//程序功能：财务批量付费
//创建日期：2008-07-23 
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  
  <SCRIPT src="MyBatchPayInputDraw.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="MyBatchPayInitDraw.jsp"%>
</head>
<body  onload="initForm();initElementtype()" >
<Form action="" method=post name=fm target="fraSubmit">
		<Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</TD>
    		<TD class= titleImg>
    			 查询条件
    		</TD>
    	</TR>
    </Table> 
    <table  class= common align=center>

       <TR  class= common>
          <TD  class= title>
        管理机构
      </TD>
      <TD  class= input>
      	<Input class="codeNo"  name=AgentCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary >
      </TD>
          <td  class= title>付费方式</TD>
         <td  class= "input">
          <input name="PayMode" value=""  class="codeNo" ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName],[0,1],null,null,null,1);"><input class="codename" name="PayModeName" readonly=true> 
          </td>
       </TR>
       <TR>
        	<td  class= title> 起始日期</td>
	        <td  class= input>	<Input class= "coolDatePicker" dateFormat="short" name="StartDate" elementtype="nacessary" >  </td>        
	        <td  class= title> 终止日期</td> 
	        <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name="EndDate" elementtype="nacessary"> </td>
      	</TR>
    </Table>  
      <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();">  
    <Table>
    	<TR>
        	<td class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</td>
    		<td class= titleImg>
    			 应付总表信息
    		</td>
    	</TR>
    </Table>  
  <input type=hidden name=ComCode>  	
 <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <td text-align: left colSpan=1>
            <span id="spanLJAGetGrid" ></span> 
  	    </td>
      </TR>
    </Table>					
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					
  <div id='Savebutton' style='display:' > 
      <input type=hidden name="arrpSql">  
      <!--input type=button value="清单下载" class=cssButton onclick="download();"-->
  </div>
</Form>    
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 