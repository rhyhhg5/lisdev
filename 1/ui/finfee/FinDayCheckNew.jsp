<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2003-03-26 15:39:06
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="FinDayCheckNew.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel="stylesheet"
			type="text/css">
		<%@include file="FinDayCheckInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">

			<p>
				<strong><IMG id="a1" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divOperator);" />操作员日结打印</strong>
			</p>


			<Div id="divOperator" style="display: ''">
				<p>
					<strong>输入查询的时间范围</strong>
				<Div id="divFCDay" style="display: ''">
					<table class="common">
						<TR class=common>
							<TD class=title>
								起始时间
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="StartDay"
									name="StartDay" verify="起始时间|NOTNULL">
							</TD>
							<TD class=title>
								结束时间
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="EndDay"
									name="EndDay" verify="结束时间|NOTNULL">
							</TD>
						</TR>
					</table>
				</Div>
				</p>

				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="SSPrint" value="X1-实收日结单" onclick="FMPrint()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="SFPrint" value="X2-实付日结单" onclick="printPay()" />
				<br />
				<table>
				<tr>
				   <td>
						<br />
						
						<input class="cssButton" style="margin-left:100px;width:200px"
							type="Button" name="SQXQPrint" value="Z3-首期续期收入日结单"
							onclick="FenHeBao_Print()" />
					</td>
					 <td>
					 <br />
						<input class="cssButton" style="margin-left:100px;width:200px"
							type="Button" name="SQXQPrint" value="X3-首期续期收入日结单"
							onclick="HeBao_Print()" />
					</td>
				</tr>
				<tr>
				 <td>
				 <br />
				 	<input class="cssButton" style="margin-left:100px;width:200px"
						type="Button" name="BQPremPrint" value="Z4-保全保费日结单"
						onclick="FenBQPrem_Print()" />
					
				</td>
				<td>
				<br />
					<input class="cssButton" style="margin-left:100px;width:200px"
						type="Button" name="BQPremPrint" value="X4-保全保费日结单"
						onclick="BQPrem_Print()" />
				</td>
				</tr>
				<tr>
				   <td>
				   <br />
				    <input class="cssButton" style="margin-left:100px;width:200px"
						type="Button" name="YingFuPrint" value=" Z5-理赔应付日结单 "
						onclick="FenClaim_Print()" />
					
					</td>
					<td>
					<br />
					<input class="cssButton" style="margin-left:100px;width:200px"
						type="Button" name="YingFuPrint" value=" X5-理赔应付日结单 "
						onclick="Claim_Print()" />
					</td>
				</tr>
				</table>
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="CardPrint" value="X6-异地理赔日结单"
					onclick="YDClaimSF_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="BQPrint" value="X7-银保通异地代收日结单"
					onclick="YBTYDDS_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="SXPrint" value="X9-手续费日结单"
					onclick="SXDDS_Print()" />
			</Div>

			<p>
				<strong><IMG id="a2" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divMonthPrint);" />操作员月结打印</strong>
			</p>

			<Div id="divMonthPrint" style="display: ''">

				<p>
					<label style="width:128px;">
						查询年月 :
					</label>
					<Input class="coolDatePicker" dateFormat="short" id="StartMonth"
						name="StartMonth" />
				</p>

				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="CommisionPrint" value="X8-佣金月结单"
					onclick="Commision_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="ZBFPrint" value="X9-再保月结单（分公司）"
					onclick="ZBF_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="ZBZPrint" value="X10再保月结单（总公司）"
					onclick="ZBZ_Print()" />
				<br />
				<br />
			</Div>

			<input type="hidden" id="fmtransact" name="fmtransact" />
			<input type="hidden" name="Opt" />
			<input type="hidden" id="curManageCom" name="curManageCom" />
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
