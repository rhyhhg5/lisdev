var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

/**
 * 后台数据库查询
 */
function easyQueryClick() {
  // 拼SQL语句，从页面采集信息
  var strSql = "select ActuGetNo, OtherNo, OtherNoType,PayMode,SumGetMoney,EnterAccDate,BankCode,AccName,BankAccNo, "
   + " drawer, drawerid,chequeno,InsBankAccNo "
   + " from LJAGet where 1=1 and ConfDate is null and EnterAccDate is null and (bankonthewayflag='0' or bankonthewayflag is null) "
	 + getWherePart( 'ActuGetNo' )
	 + getWherePart( 'OtherNo' )
	 + getWherePart( 'PayMode' );
	           
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    QueryLJAGetGrid.clearData('QueryLJAGetGrid');  
    alert("没有查询到数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QueryLJAGetGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = QueryLJAGetGrid.getSelNo();
		//alert(UpFlag);	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );

	else if(QueryLJAGetGrid.getRowColData(tSel-1,4)==4&UpFlag!=1){
		alert( "付费方式为银行转帐,请使用银行代付模块进行付费操作!" );
	}
	else
	{
		try
		{
		top.opener.fm.all('ActuGetNo').value=QueryLJAGetGrid.getRowColData(tSel-1,1);
		top.opener.fmSave.all('PolNo').value=QueryLJAGetGrid.getRowColData(tSel-1,2);
		top.opener.fmSave.all('PayMode').value=QueryLJAGetGrid.getRowColData(tSel-1,4);
		top.opener.fmSave.all('GetMoney').value=QueryLJAGetGrid.getRowColData(tSel-1,5);
		top.opener.fmSave.all('EnterAccDate').value=QueryLJAGetGrid.getRowColData(tSel-1,6);
		top.opener.fmSave.all('BankCode').value=QueryLJAGetGrid.getRowColData(tSel-1,7);      
		
		Sql = "Select codename from ldcode where codetype='bank' and code='"+QueryLJAGetGrid.getRowColData(tSel-1,7)+"'";
		arr =easyExecSql(Sql);
		if(arr)
		  top.opener.fmSave.all('BankCodeName').value=arr[0][0];
		top.opener.fmSave.all('AccName').value=QueryLJAGetGrid.getRowColData(tSel-1,8);
		top.opener.fmSave.all('BankAccNo').value=QueryLJAGetGrid.getRowColData(tSel-1,9);
	//	top.opener.fmSave.all('ConfDate').value=QueryLJAGetGrid.getRowColData(tSel-1,7);
	
		top.opener.fmSave.all('Drawer'			).value=QueryLJAGetGrid.getRowColData(tSel-1,10);
		top.opener.fmSave.all('DrawerID'		).value=QueryLJAGetGrid.getRowColData(tSel-1,11);
		top.opener.fmSave.all('ChequeNo'		).value=QueryLJAGetGrid.getRowColData(tSel-1,12);
		top.opener.fmSave.all('InsBankAccNo').value=QueryLJAGetGrid.getRowColData(tSel-1,13);

		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
		top.close();
	}
}