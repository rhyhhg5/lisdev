//该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  var rowNum=ContInfoGrid.mulLineCount ; 
	if(rowNum==null || rowNum<1){
		alert('请先查询！');
		return false;
	}
	var ChkFlag=0;
	for(var i=0;i<rowNum;i++){
		if(ContInfoGrid.getChkNo(i)){
			ChkFlag=1;
		}
	}
	if(ChkFlag==0){
		alert('至少选择一项');
		return false;
	}
  
  if(beforeSubmit()){
  	fm.target = "f1print";  
  	fm.submit(); //提交
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {     
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  { 
	  initForm();
  }
  catch(re)
  {
  	alert("在resetForm函数中发生异常:初始化界面错误!");
  }
} 

 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  return true;
}                   

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function checkValue()
{
	var arrResult = easyExecSql("select case when (days(date('"+fm.StartDate.value+"') + 3 MONTHS)-days(date('"+fm.EndDate.value+"')))<0 then '1' else '0' end from dual");
	if(arrResult!=null){
		if(arrResult[0][0]=='1'){
			alert('财务收费起期和财务收费止期的间隔不能大于三个月');
			return false;
		}
	}
  return true;
}

function afterCodeSelect(cCodeName, Field) {
	//if(cCodeName == "SelectName") {
	  //fm.SelectName.value='11';
	//}
}

function easyQueryClick() {
	if(!verifyInput()) return false;
	if(!checkValue()) return false;
	var strSql ="select distinct managecom,otherno,confmakedate,"
						+"(select appntno from lccont where prtno=ljtempfee.otherno "
						+"union all select appntno from lcgrpcont where prtno=ljtempfee.otherno fetch first 1 rows only),"
						+"appntname,getUniteCode(agentCode),tempfeeno from ljtempfee "
						+"where tempfeetype in ('1','11') and confmakedate is not null "
						+"and confdate is null "
						+"and exists (select 1 from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno "
						+"and confmakedate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 						+"order by confmakedate fetch first 1 rows only) ";     
 	strSql+=getWherePart('ManageCom','ManageCom',"like");	
 	strSql+=" with ur";      
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
               
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    TempletGrid.clearData('ContInfoGrid');  
    alert("没有查询到数据！");
    return false;
  }            
               
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContInfoGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
}



