<%
//Name:FeeInvoiceAddInputInit.jsp
//function：
//author:yanjing
//Date:2009-08-11
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode; 
System.out.println(strManageCom);
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('mComCode').value = '<%=strManageCom%>';
  }
  catch(ex)
  {
    alter("在FeeInvoiceAddInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在FeeInvoiceAddInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initQueryLDBankGrid();

  }
  catch(re)
  {
    alter("在FeeInvoiceAddInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initQueryLDBankGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="套餐编码";   		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            		//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="套餐名称";		//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=60;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="发票打印显示名称";         		//列名
      iArray[3][1]="200px";            		//列宽
      iArray[3][2]=200;            	        //列最大值
      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许


      QueryLDBankGrid = new MulLineEnter( "fm" , "QueryLDBankGrid" ); 
      //这些属性必须在loadMulLine前
      QueryLDBankGrid.mulLineCount = 3;   
      QueryLDBankGrid.displayTitle = 1;
      QueryLDBankGrid.hiddenPlus = 1;
      QueryLDBankGrid.hiddenSubtraction = 1;
      QueryLDBankGrid.canSel = 1;
      QueryLDBankGrid.loadMulLine(iArray);  
      QueryLDBankGrid.selBoxEventFuncName = "ShowDetail";

      }
      catch(ex)
      {
        alert(ex);
      }
}

 </script>