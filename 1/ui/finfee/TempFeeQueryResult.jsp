 
  <%
//程序名称：TempFeeQureyResult.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  String mAction = request.getParameter("fmAction");
  System.out.println("1111111");
  CErrors tError = null;  
  String Content = "";
  String FlagStr = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
 
  String StartDate  = request.getParameter("StartDate"); 
  String EndDate   = request.getParameter("EndDate"); 
  String Operator   = request.getParameter("Operator"); 
  String AgentCode = request.getParameter("AgentCode"); 
  String TempFeeNo = request.getParameter("TempFeeNo");
  String PrtNo = request.getParameter("PrtNo");
  //System.out.println("chequeno:"+ChequeNo);
  System.out.println("----------mAction:"+mAction);
  if(mAction.equals("QUERY")){
    String TempFeeStatus   = request.getParameter("TempFeeStatus");
    String RiskCode = request.getParameter("RiskCode");
       
    int recordCount = 0;
    double  PayMoney = 0;;   //交费金额
    
    
    //暂交费表
    LJTempFeeSet     mLJTempFeeSet     ; 
    LJTempFeeSchema  tLJTempFeeSchema  ;  
    VData tVData = new VData();
    
    tVData.addElement(tG);
    tLJTempFeeSchema = new LJTempFeeSchema(); 
    tLJTempFeeSchema.setRiskCode(RiskCode); 
    tLJTempFeeSchema.setOperator(Operator); 
    tLJTempFeeSchema.setAgentCode(AgentCode); 
    tLJTempFeeSchema.setTempFeeNo(TempFeeNo); 
    if(PrtNo!=null&&!PrtNo.trim().equals("")){
      tLJTempFeeSchema.setOtherNo(PrtNo);
      tLJTempFeeSchema.setOtherNoType("4");
    }
    tVData.add(tLJTempFeeSchema);
    tVData.addElement(StartDate);
    tVData.addElement(EndDate);
    tVData.addElement(TempFeeStatus);
  
  
    TempFeeQueryUI tTempFeeQueryUI = new TempFeeQueryUI();

    if (!tTempFeeQueryUI.submitData(tVData,mAction))
    {
      System.out.println("查询暂交费表时出错！没有相关数据存在");
      Content = " 查询失败，原因是: " + tTempFeeQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    }   
    else
    {   
	tVData.clear();
	mLJTempFeeSet = new LJTempFeeSet();
	tVData = tTempFeeQueryUI.getResult();
        mLJTempFeeSet.set((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));             
       
       //暂交费表得到纪录数据：
       //得到符合查询条件的纪录的数目，后面循环时用到
       recordCount=mLJTempFeeSet.size();
       if (recordCount>0)
       {
       String strRecord="0|"+recordCount+"^";
       strRecord=strRecord+mLJTempFeeSet.encode();       
%>
        <script language="javascript">
        //调用js文件中显示数据的函数
        parent.fraInterface.showRecord("<%=strRecord%>"); 
        </script>                
<%

    	}
    }
    tError = tTempFeeQueryUI.mErrors;
   }
   
  if(mAction.equals("CHEQUE||QUERY")){
    String TempFeeStatus   = request.getParameter("TempFeeStatus1");
    String PayMode = request.getParameter("PayMode");
    String ChequeNo = request.getParameter("ChequeNo"); 
    String ChequeDate = request.getParameter("ChequeDate");
    
    int recordCount = 0;
    double  PayMoney = 0;;   //交费金额
    
    
    //暂交费表
    LJTempFeeClassSet     mLJTempFeeClassSet     ; 
    LJTempFeeClassSchema  tLJTempFeeClassSchema  ;  
    VData tVData = new VData();
    
    tVData.addElement(tG);
    tLJTempFeeClassSchema = new LJTempFeeClassSchema(); 
    tLJTempFeeClassSchema.setPayMode(PayMode);
    tLJTempFeeClassSchema.setChequeNo(ChequeNo); 
    tLJTempFeeClassSchema.setChequeDate(ChequeDate); 
    tLJTempFeeClassSchema.setOperator(Operator); 
    tLJTempFeeClassSchema.setTempFeeNo(TempFeeNo); 
    tVData.add(tLJTempFeeClassSchema);
    tVData.addElement(StartDate);
    tVData.addElement(EndDate);
    tVData.addElement(TempFeeStatus);
    tVData.addElement(PrtNo);
    tVData.addElement(AgentCode);
  
  
    TempFeeQueryUI tTempFeeQueryUI = new TempFeeQueryUI();

    if (!tTempFeeQueryUI.submitData(tVData,mAction))
    {
      System.out.println("查询暂交费表时出错！没有相关数据存在");
      Content = " 查询失败，原因是: " + tTempFeeQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    }   
    else
    {   
	tVData.clear();
	mLJTempFeeClassSet = new LJTempFeeClassSet();
	tVData = tTempFeeQueryUI.getResult();
        mLJTempFeeClassSet.set((LJTempFeeClassSet)tVData.getObjectByObjectName("LJTempFeeClassSet",0));             
       
       //暂交费表得到纪录数据：
       //得到符合查询条件的纪录的数目，后面循环时用到
       recordCount=mLJTempFeeClassSet.size();
       if (recordCount>0)
       {
       String strRecord="0|"+recordCount+"^";
       strRecord=strRecord+mLJTempFeeClassSet.encode();       
%>
        <script language="javascript">
        //调用js文件中显示数据的函数
        parent.fraInterface.showClassRecord("<%=strRecord%>"); 
        </script>                
<%

    	}
    }
    tError = tTempFeeQueryUI.mErrors;
   }      
  
   //如果在Catch中发现异常，则不从错误类中提取错误信息
   if (FlagStr=="")
   {    
     if (!tError.needDealError())
     {                          
       Content = " 查询成功";
       FlagStr = "Succ";
     }
     else                                                                           
     {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
     }
    }
  
%>                                        
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>




