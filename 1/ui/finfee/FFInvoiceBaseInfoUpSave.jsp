<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2007-05-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GB2312" %>
<%@page import="com.sinosoft.utility.*" %>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.lis.finfee.*" %>
<%@page import="java.util.List" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%
    System.out.println("start FFInvoiceBaseInfoSave ----");

    GlobalInput tGI = (GlobalInput) session.getValue("GI");

    String FlagStr = "";
    String Content = "";
    String transact = "";

    String tComCode = (String) request.getParameter("upComCode");
    String tInvoiceCode = (String) request.getParameter("upInvoiceCode");
    String tTaxpayerNo = (String) request.getParameter("upTaxpayerNo");
    String tInvoiceStartNo = (String) request.getParameter("upInvoiceStartNo");
    String tInvoiceEndNo = (String) request.getParameter("upInvoiceEndNo");

    LJInvoiceInfoSchema tLJInvoiceInfoSchema = new LJInvoiceInfoSchema();
    tLJInvoiceInfoSchema.setComCode(tComCode);
    tLJInvoiceInfoSchema.setInvoiceCode(tInvoiceCode);
    tLJInvoiceInfoSchema.setInvoiceEndNo(tInvoiceEndNo);
    tLJInvoiceInfoSchema.setInvoiceStartNo(tInvoiceStartNo);
    tLJInvoiceInfoSchema.setTaxpayerNo(tTaxpayerNo);

    VData tVData = new VData();
    tVData.add(tLJInvoiceInfoSchema);
    tVData.add(tGI);

    FFInvoiceBaseInfoUpUI tFFInvoiceBaseInfoUpUI = new FFInvoiceBaseInfoUpUI();

    try
    {
        if(!tFFInvoiceBaseInfoUpUI.submitData(tVData,"UPDATE"))
        {
            FlagStr = "Fail";
            Content = tFFInvoiceBaseInfoUpUI.mErrors.getFirstError().toString();
        }
    }
    catch(Exception ex)
    {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
    }

    if (FlagStr=="")
    {
        CErrors tError = tError = tFFInvoiceBaseInfoUpUI.mErrors;
        if (!tError.needDealError())
        {
            Content = " 保存成功! ";
            FlagStr = "Success";
            transact = "";
            List result = tFFInvoiceBaseInfoUpUI.getResult();
            if(result.size() > 0)
            {
                for(int i=0; i<result.size(); i++)
                {
                    transact += result.get(i) + "|";
                }
            }
        }
        else
        {
            Content = " 保存失败，原因是: " + tError.getFirstError();
            FlagStr = "Fail";
        }
    }

    System.out.println("end FFInvoiceBaseInfoSave ----");
%>

<html>
<script language="javascript">
    try
    {
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "UPDATE");
    }
    catch(e)
    {
    }
</script>
</html>