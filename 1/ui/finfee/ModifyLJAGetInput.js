//ModifyLJAGetInput.js该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;

//提交，保存按钮对应操作
function submitForm() {
  if (verifyInput() == false) return false;  

  if (fm.BankCode.value == "0101") {
    if (trim(fm.AccNo.value).length!=19 || !isInteger(trim(fm.AccNo.value))) {
      alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！");
      return false;
    }
  }
     
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句			     
	var strSql = "select a.actugetno, a.otherno, b.name, a.paymode, a.sumgetmoney, a.bankcode, a.bankaccno, a.accname "
	           + " from ljaget a, ldperson b where a.appntno=b.customerno and EnterAccDate is null "
          	 + getWherePart("ActuGetNo", "ActuGetNo")
          	 + getWherePart("OtherNo", "OtherNo");
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
	fm.ActuGetNo2.value = fm.ActuGetNo.value;
}

function afterCodeSelect(cCodeName, Field) {
  if (cCodeName == "PayMode") {
    if (Field.value == "1") {
      fm.BankCode.value = "";
      fm.AccNo.value = "";
      fm.AccName.value = "";
      
      fm.BankCode.verify = "";
      fm.AccNo.verify = "";
      fm.AccName.verify = "";
    }
    else if (Field.value == "4") {
      fm.BankCode.verify = "银行代码|notnull&code:bank";
      fm.AccNo.verify = "银行账号|notnull";
      fm.AccName.verify = "账户名称|notnull";
    }
  }
}


function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
	  fm.ActuGetNo2.value = turnPage.arrDataCacheSet[index][0];
	  fm.PayMode.value = turnPage.arrDataCacheSet[index][3];
	  fm.BankCode.value = turnPage.arrDataCacheSet[index][5];
	  fm.AccNo.value = turnPage.arrDataCacheSet[index][6];
	  fm.AccName.value = turnPage.arrDataCacheSet[index][7];
  }
}

function readBankInfo() {
  var strSql = "select OtherNo, OtherNoType from ljaget where ActuGetNo='" + fm.ActuGetNo2.value + "'";
  var arrResult = easyExecSql(strSql, 1, 0, 1, 0, 1);
  
  if (arrResult[0][1] != "3") {
    alert("不是保全数据！现在只支持保全类型数据的账户信息查询！");
    return;
  }
  
  strSql = "select bankcode, bankaccno, accname from lcpol where polno in (select polno from lpedormain where edorno='" + arrResult[0][0] + "')";
  var arrResult2 = easyExecSql(strSql, 1, 0, 1, 0, 1);
  
  if (arrResult2 == null) {
    strSql = "select bankcode, bankaccno, accname from lbpol where polno in (select polno from lpedormain where edorno='" + arrResult[0][0] + "')";
    arrResult2 = easyExecSql(strSql, 1, 0, 1, 0, 1);
  }
  
  if (arrResult2 == null) {
    alert("没有查询到保单数据！");
    return;
  }  
  
  if (arrResult2[0][0] != "") {
    fm.PayMode.value = "4";
    fm.BankCode.value = arrResult2[0][0];
    fm.AccNo.value = arrResult2[0][1];
    fm.AccName.value = arrResult2[0][2];
  }
}


