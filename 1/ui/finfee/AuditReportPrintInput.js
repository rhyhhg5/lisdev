//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() && CheckDate(fm.StartDate.value) && CheckDate(fm.EndDate.value))
	{
	   if(fm.EndDate.value<fm.StartDate.value){
					alert("起始时间大于终止时间,请重新输入!");
					fm.EndDate.focus();
					return false;
	　    }	
	if(fm.Opt.value =="TPremIncome" || fm.Opt.value=="TClaim")
	{
		if(fm.EndDate.value>fm.StartDate.value){
	 		var days = parseInt((new Date(fm.EndDate.value.replace(/-/g, "/")).getTime() - new Date(fm.StartDate.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
	 			//	alert(days);
	 		if(days>31) {
				alert("因数据量较大，所以起始时间与结束时间相差不能大于一个月,请重新输入按月统计，非常感谢!");
				fm.EndDate.focus();
				return false;
			}
	　  }
	}
	
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

//非现场业务审计 保费收入报表
function PremIncome_Print()
{
  fm.action = "../f1print/TPremIncomeExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="TPremIncome";//操作的标志
  submitForm();
  showInfo.close();
}
//非现场业务审计 赔款支出报表
function Claim_Print()
{
  fm.action = "../f1print/TClaimExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="TClaim";//操作的标志
  submitForm();
  showInfo.close();
}

//非现场业务审计 满期给付报表
function MaturePay_Print()
{
  fm.action = "../f1print/TMaturePayExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="TMaturePay";//操作的标志
  submitForm();
  showInfo.close();
}
//非现场业务审计 死伤医疗给付报表
function CasualtyPay_Print()
{
  fm.action = "../f1print/TCasualtyPayExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="TCasualtyPay";//操作的标志
  submitForm();
  showInfo.close();
}
//非现场业务审计 退保金报表
function SurrenderCharge_Print()
{
  fm.action = "../f1print/TSurrenderChargeExcelSave.jsp";
  fm.target = "f1print";
  fm.fmtransact.value = "PRINT";
  fm.Opt.value="TSurrenderCharge";//操作的标志
  submitForm();
  showInfo.close();
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
// ----日期格式的验证------------------
function CheckDate(strDate)
{
    var reg=/^(\d{4})([-])(\d{2})([-])(\d{2})/;
    if(!reg.test(strDate))
    {
        alert("日期格式不正确!\n 正确格式为:yyyy-mm-dd");
        return false;
    }
    return true;
}
 