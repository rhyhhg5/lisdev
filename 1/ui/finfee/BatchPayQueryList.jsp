<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：BatchPayQueryList.jsp
	//程序功能：清单下载
	//创建日期：2009-06-18
	//创建人  ：yanjing
	//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");

	//生成文件名
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));
	String downLoadFileName = "待收费清单_" + tG.Operator + "_" + min + sec
			+ ".xls";
	String filePath = application.getRealPath("temp");
	String tOutXmlPath = filePath + File.separator + downLoadFileName;
	System.out.println("OutXmlPath:" + tOutXmlPath);

	String tempFeeType = request.getParameter("type");
	String querySql = request.getParameter("arrpSql");
	querySql = querySql.replaceAll("%25", "%");
	//设置表头
	String[] tTitle = { "暂收费号", "关联号码", "号码类型", "收费方式", "交费金额", "银行编码",
			"帐户名", "帐号" };

	//数据的显示属性
	int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8 };

	if (tempFeeType != null) {
		if (tempFeeType.equals("1") || tempFeeType.equals("2")
				|| tempFeeType.equals("4") || tempFeeType.equals("10")
				|| tempFeeType.equals("20") || tempFeeType.equals("9")) {
			tTitle = new String[] { "暂收费号", "关联号码", "号码类型", "收费方式",
					"交费金额", "银行编码", "帐号", "账户名", "投保人", "投保人编号" };
			displayData = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		}
	}
	//生成文件
	CreateExcelListEx createexcellist = new CreateExcelListEx("");//指定文件名
	createexcellist.createExcelFile();
	String[] sheetName = { "list" };
	createexcellist.addSheet(sheetName);
	if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
		errorFlag = true;
	}
	if (!errorFlag) {
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}

	out.clear();
	out = pageContext.pushBody();
%>

