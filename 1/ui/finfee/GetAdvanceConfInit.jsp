<%
//程序名称：TempFeeWithdrawInit.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>  
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox() { 
}                                     

function initForm() {
  try {
  	initInpBox();
  	initFeeGrid();
    initTempGrid();
  	fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
  }
  catch(re) {
    alert("TempFeeWithdrawInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 领取项信息列表的初始化
var FeeGrid;
function initFeeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="退费原因";         		//列名
    iArray[1][1]="40px";            		//列宽
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="bz_cancel";            

    iArray[2]=new Array();
    iArray[2][0]="预收保费号码";      //列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="收费类型";                //列名
    iArray[3][1]="40px";            		//列宽
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="业务员";         		//列名
    iArray[4][1]="100px";            		//列宽
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="交费单位名称";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="投保单印刷号";         		//列名
    iArray[6][1]="0px";            		//列宽
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="交费金额";         			//列名
    iArray[7][1]="60px";            		//列宽
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="交费日期";         		//列名
    iArray[8][1]="60px";            		//列宽
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="到帐日期";         		//列名
    iArray[9][1]="0px";            		//列宽
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[10]=new Array();
    iArray[10][0]="预收保费剩余金额";         		//列名
    iArray[10][1]="100px";            		//列宽
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    FeeGrid = new MulLineEnter( "fm" , "FeeGrid" ); 
    //这些属性必须在loadMulLine前
    FeeGrid.mulLineCount = 0;   
    FeeGrid.displayTitle = 1;
    FeeGrid.hiddenPlus = 1;
    FeeGrid.hiddenSubtraction = 1;
    FeeGrid.canSel = 1;
    FeeGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}

function initTempGrid()
  {                              
    var iArray = new Array();      
      iArray[0]=new Array();
      iArray[0][0]="序号";                    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";                  //列宽
      iArray[0][2]=1;                       //列最大值
      iArray[0][3]=0;                       //是否允许输入,1表示允许，0表示不允许
     
      iArray[1]=new Array();
      iArray[1][0]="险种编码";                  //列名
      iArray[1][1]="60px";                      //列宽
      iArray[1][2]=20;                      //列最大值
            
      iArray[2]=new Array();
      iArray[2][0]="险种名称";                  //列名
      iArray[2][1]="140px";                     //列宽
      iArray[2][2]=20;                      //列最大值
      iArray[2][3]=0;                       //是否允许输入,1表示允许，0表示不允许    
     

      iArray[3]=new Array();
      iArray[3][0]="交费金额";                  //列名
      iArray[3][1]="100px";                     //列宽
      iArray[3][2]=20;                      //列最大值
      iArray[3][3]=1;                       //是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="交费金额|NUM&NOTNULL";

      iArray[4]=new Array();
      iArray[4][0]="交费单位名称";                  //列名
      iArray[4][1]="160px";                     //列宽
      iArray[4][2]=20;                      //列最大值
      iArray[4][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="代理人编码";                  //列名
      iArray[5][1]="100px";                     //列宽
      iArray[5][2]=20;                      //列最大值
      iArray[5][3]=0; 
      
      iArray[6]=new Array();
      iArray[6][0]="代理人";                  //列名
      iArray[6][1]="0px";                     //列宽
      iArray[6][2]=20;                      //列最大值
      iArray[6][3]=0; 

      TempGrid = new MulLineEnter( "fm" , "TempGrid" ); 
      TempGrid.mulLineCount = 0;   
      TempGrid.displayTitle = 1;
      TempGrid.locked=1;      
      TempGrid.loadMulLine(iArray);  
}
</script>

	
