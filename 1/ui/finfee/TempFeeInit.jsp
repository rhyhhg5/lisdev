 <%
//程序名称：TempFeeInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>  
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
    Frame1.style.display='';
    Frame2.style.display=''; 
    fm.all('GlobalManageCom').value='<%=strManageCom%>';
    fm.all('TempFeeNo').value='';            
    fm.all('ManageCom').value = '';
    fm.all('PayDate').value = '<%=CurrentDate%>';
    fm.all('AgentGroup').value = '';
    fm.all('AgentCode').value = '';
    fm.all('TempFeeType').value = '';     
    //fm.all('InputNo').value = '';     
    fm.all('TempFeeCount').value = 0;
    fm.all('SumTempFee').value = 0.0 ;    
  }
  catch(ex)
  {
    alert("在TempFeeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在TempFeeInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initTempGrid();  
    initTempClassGrid(); 
    initTempToGrid();  
    initTempClassToGrid(); 
    
  }
  catch(re)
  {
    alert("TempFeeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 暂收费信息列表的初始化
var flag=1;
function initTempGrid()
  {                              
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[1]=new Array();
      iArray[1][0]="险种编码";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
        
      if(flag==1)
      {
      iArray[1][3]=2;
      iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
      iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[1][18]=300;
      iArray[1][19]=1;                          //强制刷新
      
      iArray[2]=new Array();
      iArray[2][0]="险种名称";         			//列名
      iArray[2][1]="140px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//    iArray[2][9]="险种名称|NOTNULL";
      } else if(flag==0)
      {
       iArray[1][3]=0; //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
      
      iArray[2]=new Array();
      iArray[2][0]="险种名称";         			//列名
      iArray[2][1]="140px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//    iArray[2][9]="险种名称|NOTNULL";
      }
	    else
	    {
	    iArray[1][3]=3; //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	   
	    iArray[2]=new Array();
        iArray[2][0]="险种名称";         			//列名
        iArray[2][1]="140px";            			//列宽
        iArray[2][2]=20;            			//列最大值
        iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[2][9]="险种名称|NOTNULL";
	    }
     

      iArray[3]=new Array();
      iArray[3][0]="交费金额";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="交费金额|NUM&NOTNULL";

      iArray[4]=new Array();
      iArray[4][0]="其它号码";      	   		//列名
      iArray[4][1]="160px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      TempGrid = new MulLineEnter( "fm" , "TempGrid" ); 
      //这些属性必须在loadMulLine前
      TempGrid.mulLineCount = 0;   
      TempGrid.displayTitle = 1;
      TempGrid.locked=1;      
      TempGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 暂收费分类列表的初始化
function initTempClassGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="交费方式";          		//列名
      iArray[1][1]="50px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[1][4]="paymode1";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
      iArray[1][9]="交费方式|code:paymode1&NOTNULL";


      iArray[2]=new Array();
      iArray[2][0]="交费名称";         		        //列名
      iArray[2][1]="70px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[2][9]="交费方式名称|NOTNULL";

      iArray[3]=new Array();
      iArray[3][0]="交费金额";      	   		//列名
      iArray[3][1]="90px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="交费金额|NUM&NOTNULL";
      
      iArray[4]=new Array();
      iArray[4][0]="票据号码";      	   		//列名
      iArray[4][1]="100px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[4][9]="票据号码|NULL|LEN>10";              //测试或运算

      iArray[5]=new Array();
      iArray[5][0]="支票日期";      	   		//列名
      iArray[5][1]="80px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="到帐日期";      	   		//列名
      iArray[6][1]="0px";            			//列宽
      iArray[6][2]=10;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][9]="到帐日期|DATE|NULL";
      iArray[6][14]="<%=CurrentDate%>";

  
      iArray[7]=new Array();
      iArray[7][0]="对方开户银行";      	   		//列名
      iArray[7][1]="0px";            			//列宽
      iArray[7][2]=40;            			//列最大值
      iArray[7][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][4]="bank";              	 //是否引用代码:null||""为不引用
      //iArray[7][9]="开户银行|code:bank"; 
      iArray[7][18]=250;
      
      
      iArray[8]=new Array();
      iArray[8][0]="对方银行账号";      	   		//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=40;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="对方户名";      	   		//列名
      iArray[9][1]="0px";            			//列宽
      iArray[9][2]=60;            			//列最大值
      iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许
           
      iArray[10]=new Array();
      iArray[10][0]="本方开户银行";      	   	//列名
      iArray[10][1]="75px";            			//列宽
      iArray[10][2]=40;            			//列最大值
      iArray[10][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][4]="getbankcode";
      iArray[10][9]="银行编码|code:getbankcode";              	 //是否引用代码:null||""为不引用
      iArray[10][18]=250;             			//是否允许输入,1表示允许，0表示不允许
      
       iArray[11]=new Array();
      iArray[11][0]="本方银行帐号";      	   	//列名
      iArray[11][1]="100px";            			//列宽
      iArray[11][2]=40;            			//列最大值
      iArray[11][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[11][7]="getbankaccno3";          			//是否允许输入,1表示允许，0表示不允许        
			iArray[11][8]="[this]";                                 
			iArray[11][21]="banknames";                                 
      TempClassGrid = new MulLineEnter( "fm" , "TempClassGrid" );                                
      //这些属性必须在loadMulLine前
      TempClassGrid.mulLineCount = 0;   
      TempClassGrid.displayTitle = 1;     
      TempClassGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
  }

////初始化保存暂交费分类数据的Grid
function initTempToGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="交费收据号";          		//列名
      iArray[1][1]="100px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;
      
      iArray[2]=new Array();
      iArray[2][0]="收据类型";          		//列名
      iArray[2][1]="80px";      	      		//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;
            
      iArray[3]=new Array();
      iArray[3][0]="交费日期";      	   		//列名
      iArray[3][1]="80px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="交费金额";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="到帐日期";          		//列名
      iArray[5][1]="0px";      	      		//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="管理机构";      	   		//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="险种编码";          		//列名
      iArray[7][1]="60px";      	      		//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="代理人组别";          		//列名
      iArray[8][1]="80px";      	      		//列宽
      iArray[8][2]=20;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="代理人编码";          		//列名
      iArray[9][1]="80px";      	      		//列宽
      iArray[9][2]=20;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[10]=new Array();
      iArray[10][0]="其它号码";      	   	        //列名
      iArray[10][1]="100px";            			//列宽
      iArray[10][2]=20;            			//列最大值
      iArray[10][3]=0;              			//隐藏

      iArray[11]=new Array();
      iArray[11][0]="其它号码类型";      	   	        //列名
      iArray[11][1]="100px";            			//列宽
      iArray[11][2]=20;            			//列最大值
      iArray[11][3]=0;              			//隐藏


      TempToGrid = new MulLineEnter( "fmSave" , "TempToGrid" ); 
      //这些属性必须在loadMulLine前
      TempToGrid.mulLineCount = 0;   
      TempToGrid.displayTitle = 1;
      TempToGrid.locked=1;
      TempToGrid.hiddenPlus=1;
      TempToGrid.hiddenSubtraction=1;      
      TempToGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }	
}



//初始化保存暂交费分类数据的Grid
function initTempClassToGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="交费收据号";          		//列名
      iArray[1][1]="100px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;

      iArray[2]=new Array();
      iArray[2][0]="交费方式";          		//列名
      iArray[2][1]="80px";      	      		//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="交费日期";          		//列名
      iArray[3][1]="80px";      	      		//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="交费金额";          		//列名
      iArray[4][1]="80px";      	      		//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="到帐日期";          		//列名
      iArray[5][1]="0px";      	      		//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="管理机构";          		//列名
      iArray[6][1]="80px";      	      		//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="票据号码";          		//列名
      iArray[7][1]="100px";      	      		//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="支票日期";          		//列名
      iArray[8][1]="80px";      	      		//列宽
      iArray[8][2]=20;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="对方开户银行";      	   		//列名
      iArray[9][1]="100px";            			//列宽
      iArray[9][2]=40;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="对方银行账号";      	   		//列名
      iArray[10][1]="100px";            			//列宽
      iArray[10][2]=40;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[11]=new Array();
      iArray[11][0]="对方户名";      	   		//列名
      iArray[11][1]="100px";            			//列宽
      iArray[11][2]=10;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="本方开户银行";      	   		//列名
      iArray[12][1]="100px";            			//列宽
      iArray[12][2]=40;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
      
      iArray[13]=new Array();
      iArray[13][0]="本方开户银行帐号";      	   		//列名
      iArray[13][1]="100px";            			//列宽
      iArray[13][2]=40;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许     

      TempClassToGrid = new MulLineEnter( "fmSave" , "TempClassToGrid" ); 
      //这些属性必须在loadMulLine前
      TempClassToGrid.mulLineCount = 0;   
      TempClassToGrid.displayTitle = 1;
      TempClassToGrid.locked=1;
      TempClassToGrid.hiddenPlus=1;
      TempClassToGrid.hiddenSubtraction=1;
      TempClassToGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }	
	
}

// 暂收费信息列表的初始化
function initTempGridReadOnly() 
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="险种名称";         			//列名
      iArray[2][1]="140px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="交费金额";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保单号码";      	   		//列名
      iArray[4][1]="160px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      TempGrid = new MulLineEnter( "fm" , "TempGrid" ); 
      //这些属性必须在loadMulLine前
      TempGrid.mulLineCount = 0;   
      TempGrid.displayTitle = 1;
      TempGrid.locked=1;      
      TempGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }      
  }

// 暂收费信息列表的初始化
//合同交费时不显示险种信息
function initTempGridReadOnlyCont()
  {   
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="险种名称";         			//列名
      iArray[2][1]="140px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="交费金额";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保单号码";      	   		//列名
      iArray[4][1]="160px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      TempGrid = new MulLineEnter( "fm" , "TempGrid" ); 
      //这些属性必须在loadMulLine前
      TempGrid.mulLineCount = 0;   
      TempGrid.displayTitle = 1;
      TempGrid.locked=1;      
      TempGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }      
  }


// 暂收费信息列表的初始化(针对暂交费类型为3-不定期交费)
function initTempGridReadOnlyType()
{                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="险种名称";         			//列名
      iArray[2][1]="140px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="交费金额";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保单号码";      	   		//列名
      iArray[4][1]="160px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      TempGrid = new MulLineEnter( "fm" , "TempGrid" ); 
      //这些属性必须在loadMulLine前
      TempGrid.mulLineCount = 0;   
      TempGrid.displayTitle = 1;
      TempGrid.locked=1;      
      TempGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }  
 }    

// 暂收费分类信息列表的初始化(针对暂交费类型为5-银行扣款)
// 暂收费分类列表的初始化
function initTempClassGridType5()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="交费方式";          		//列名
      iArray[1][1]="50px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[1][14]="4";              	        //是否引用代码:null||""为不引用

      iArray[2]=new Array();
      iArray[2][0]="交费名称";         		        //列名
      iArray[2][1]="70px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][14]="银行转账";

      iArray[3]=new Array();
      iArray[3][0]="交费金额";      	   		//列名
      iArray[3][1]="90px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="交费金额|NUM&NOTNULL";
      
      iArray[4]=new Array();
      iArray[4][0]="票据号码";      	   		//列名
      iArray[4][1]="0px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="到帐日期";      	   		//列名
      iArray[5][1]="0px";            			//列宽
      iArray[5][2]=10;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="开户银行";      	   		//列名
      iArray[6][1]="100px";            			//列宽
      iArray[6][2]=40;            			//列最大值
      iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][4]="bank";              	        //是否引用代码:null||""为不引用
      iArray[6][9]="开户银行|code:bank&NOTNULL";
      
      iArray[7]=new Array();
      iArray[7][0]="银行账号";       	   		//列名
      iArray[7][1]="140px";             			//列宽
      iArray[7][2]=40;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][9]="银行账号|NOTNULL";              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="户名";      	   		//列名
      iArray[8][1]="100px";            			//列宽
      iArray[8][2]=20;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][9]="户名|NOTNULL";              			//是否允许输入,1表示允许，0表示不允许

      TempClassGrid = new MulLineEnter( "fm" , "TempClassGrid" ); 
      //这些属性必须在loadMulLine前
      TempClassGrid.mulLineCount = 0;   
      TempClassGrid.displayTitle = 1;
      TempClassGrid.locked = 1;     
      TempClassGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
  }

  
</script>
