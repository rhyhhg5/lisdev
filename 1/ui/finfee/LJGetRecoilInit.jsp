<html>
<%
//name :LJGetRecoilInit.jsp
//function :Manage CessInfo
//Creator :
//date :2008-10-11
%>
<%
  GlobalInput tGI = new GlobalInput(); 
  String ManageCom = "";
  String Operator = "";
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
   Operator="unknown";
   ManageCom="unknown";
  }
  else //页面有效
  {
   Operator  = tGI.Operator ;  //保存登陆管理员账号
   ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  }
%>

<!--用户校验类-->

<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>

<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('ComCode').value = '<%=ManageCom%>';
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在LRNewContDetailInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initPolGrid();
  }
  catch(re)
  {
    alert("RecoilGrid.jsp->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="缴费凭证号";         		//列名
      iArray[1][1]="70px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
      iArray[2]=new Array();
      iArray[2][0]="业务号";         		//列名
      iArray[2][1]="70px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="交费方式";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许            

      iArray[4]=new Array();                                                       
      iArray[4][0]="银行编码";         		//列名                                     
      iArray[4][1]="60px";            		//列宽                                   
      iArray[4][2]=100;            			//列最大值                                 
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
            
      iArray[5]=new Array();
      iArray[5][0]="银行帐号";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[6]=new Array();
      iArray[6][0]="本方银行编码";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      
      iArray[7]=new Array();
      iArray[7][0]="本方银行帐号";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="到帐确认日期";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      


      RecoilGrid = new MulLineEnter( "fm" , "RecoilGrid" ); 
      //这些属性必须在loadMulLine前
      RecoilGrid.mulLineCount = 0;   
      RecoilGrid.displayTitle = 1;
      RecoilGrid.locked = 1;
      RecoilGrid.canSel = 1;
      RecoilGrid.canChk =0; 
      RecoilGrid.hiddenPlus = 1;
      RecoilGrid.hiddenSubtraction = 1;
      RecoilGrid.loadMulLine(iArray);     
      RecoilGrid.selBoxEventFuncName ="getInfo";

      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>
