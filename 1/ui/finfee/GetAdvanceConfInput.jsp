<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：TempFeeWithdrawInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="GetAdvanceConfInput.js"></SCRIPT>
  <%@include file="GetAdvanceConfInit.jsp"%>
  
  <title>暂交费信息 </title>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./GetAdvanceConfSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 暂交费信息部分 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
      
      <TD  class= title>
        交费金额
      </TD>
      <TD  class= input>
        <Input class= common name=PayMoney >
      </TD>
      <TD  class= title>
        交费单位
      </TD>
      <TD  class= input>
        <Input class= common name=APPntName >
      </TD>
    </TR>
    <TR  class= common>
      <TD  class= title>
            <div id="divEnterAccTitle" style="display:">
                交费日期
            </div>
          </TD>
          <TD  class= input>
            <div id="divEnterAccDate" style="display:">
                <Input class="coolDatePicker"   name=EnterAccDate           
                verify="财务到帐日期|notnull&date" >
            </div>
          </TD>
      
      <TD  class= title>
        管理机构
      </TD>
      <TD  class= input>
      	<Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
        
      </TD>
    </TR>
    <TR  class= common>
      
      
      <TD  class= title>
        业务员
      </TD>
      <TD  class= input>
        <Input class= code name=GroupAgentCode  VALUE="" MAXLENGTH=0 ondblclick="queryAgent()">
        <Input class=common name=AgentCode VALUE="" MAXLENGTH=0 type='hidden'>
      </TD>
    </TR>

    </table>
    <table align=right>
      <TR>
        <TD>
          <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="tempFeeNoQuery();">
        </TD>
      </TR>
    </table>
    <br><br>
    
    <!-- 预收保费信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJTempFee1);">
    		</td>
    		<td class= titleImg>
    			 预收保费信息 
    		</td>
    	</tr>
    </table>
	<Div  id= "divLJTempFee1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanFeeGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
	
  <Div id= "divPage" align=center style= "display: 'none' ">
  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  <br>
  <br>
  <table align=left class= common>
  <TR  class= common>
      <TD  class= title>
        投保单印刷号码 
      </TD>
      <TD  class= input>
        <Input class= common name=PrtNo onblur="getMoney();" elementtype="nacessary">
      </TD>
      
      <TD  class= title>
        暂收费号
      </TD>
      <TD  class= input>
        <Input class= common name=TempFeeNo elementtype="nacessary">
      </TD>
    </TR>
  </table>
  <br><br><br>
<Div  id= "divLJTempFee2" style= "display: ''">
        <table  class= common>
            <tr  class= common>
                <td text-align: left colSpan=1>
                    <span id="spanTempGrid" >
                    </span> 
                </td>
            </tr>
        </table>  
    </div><br>
  <INPUT VALUE="转实收保费" class= cssButton TYPE=button onclick="submitForm();">		
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
