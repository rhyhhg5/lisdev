<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
	<!-- 农行重打返回报文转换 -->
		<Ret>
			<RetData>
				<!-- 返回码 -->
				<Flag><xsl:value-of select="TranData/RetData/Flag" /></Flag>
				<!-- 返回信息 -->
				<Mesg><xsl:value-of select="TranData/RetData/Desc" /></Mesg>
			</RetData>
			<!-- 保单信息 -->
			<Base>
				<!-- 保险单号码 -->
				<ContNo><xsl:value-of select="TranData/LCCont/ContNo" /></ContNo>
				<!-- 投保书号 -->
				<ProposalContNo><xsl:value-of select="TranData/LCCont/ProposalContNo" /></ProposalContNo>
				<!-- 签约日期 -->
				<SignDate><xsl:value-of select="TranData/LCCont/SignDate" /></SignDate>
				<!-- 保险生效日期 -->
				<ContBgnDate><xsl:value-of select="TranData/LCCont/CValiDate" /></ContBgnDate>
				<!-- 保险终止日期 -->
				<ContEndDate><xsl:value-of select="TranData/LCCont/ContEndDate" /></ContEndDate>
				<!-- 交费期限 -->
				<ExpDate><xsl:value-of select="TranData/LCCont/PayEndDate" /></ExpDate>
				<!-- 业务员代码 -->
				<AgentCode><xsl:value-of select="TranData/LCCont/AgentCode" /></AgentCode>
				<!-- 保险公司服务热线 -->
				<ComPhone><xsl:value-of select="TranData/LCCont/ComPhone" /></ComPhone>
				<!-- 险种名称 -->
				<RiskName><xsl:value-of select="TranData/LCCont/LCInsureds/LCInsured/Risks/Risk[1]/RiskName" /></RiskName>
				<!-- 账户姓名 -->
				<BankAccName><xsl:value-of select="TranData/LCCont/BankAccName" /></BankAccName>
				<!-- 总保费 -->
				<Prem><xsl:value-of select="TranData/LCCont/Prem" /></Prem>
			</Base>
			<Risks>
				<!-- 险种个数 -->
				<Count><xsl:value-of select="TranData/LCCont/LCInsureds/LCInsured/Risks/RiskCount"/></Count>
				<xsl:for-each select="TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
					<Risk>
						<!-- 险种名称 -->
						<Name><xsl:value-of select="RiskName" /></Name>
						<!-- 投保份数 -->
						<Mult><xsl:value-of select="Mult" /></Mult>
						<!-- 保费 -->
						<Prem><xsl:value-of select="Prem" /></Prem>
						<!-- 交费年期 -->
						<PayEndYear><xsl:value-of select="PayEndYear" /></PayEndYear>
						<!-- 交费方式 -->
						<PayIntv><xsl:value-of select="PayIntv" /></PayIntv>
					</Risk>
				</xsl:for-each>
			</Risks>
			<Prnts>
				<Count><xsl:value-of select="count(TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValues/RemarkDetails)" /></Count>
				<xsl:for-each select="TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
					<xsl:for-each select="CashValues/RemarkDetails">
						<Prnt>
							<!-- 打印信息 -->
							<Value><xsl:value-of select="RemarkDetail" /></Value>
						</Prnt>
					</xsl:for-each>
				</xsl:for-each>
			</Prnts>
			<Messages>
				<Count><xsl:value-of select="TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValues/CashValueCount" /></Count>
				<xsl:for-each select="TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
					<xsl:for-each select="CashValues/CashValue">
						<Message>
							<!-- 现金价值表信息 -->
							<Value><xsl:value-of select="Cash" /></Value>
						</Message>
					</xsl:for-each>
				</xsl:for-each>
			</Messages>
		</Ret>
	
	</xsl:template>
</xsl:stylesheet>