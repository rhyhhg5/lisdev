<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java">
	<xsl:output indent="yes" />
	<xsl:template match="/">
		<TranData>
			<xsl:apply-templates
				select="Transaction/Transaction_Header" />
			<LCCont>
				<ProposalContNo>
					<xsl:value-of
						select="Transaction/Transaction_Body/BkVchNo" />
				</ProposalContNo>
				<PolApplyDate>
					<xsl:value-of
						select="Transaction/Transaction_Header/BkPlatDate" />
				</PolApplyDate>
				<AccName>
					<xsl:value-of
						select="Transaction/Transaction_Body/PbHoldName" />
				</AccName>
				<AccBankCode>03</AccBankCode>
				<BankAccNo>
					<xsl:value-of
						select="Transaction/Transaction_Body/BkAcctNo3" />
				</BankAccNo>
				<PayIntv>
					<xsl:call-template name="tran_Contpayintv">
						<xsl:with-param name="payintv">
							<xsl:value-of
								select="Transaction/Transaction_Body/PbPayPeriod" />
						</xsl:with-param>
					</xsl:call-template>
				</PayIntv>
				<Password>
					<xsl:value-of
						select="Transaction/Transaction_Body/LiInsuSlipPWD" />
				</Password>
				<SpecContent>
					<xsl:value-of
						select="Transaction/Transaction_Body/LiSpec" />
				</SpecContent>
				<PrtNo>
					<xsl:value-of
						select="Transaction/Transaction_Body/PbApplNo" />
				</PrtNo>
				<!-- 投保人信息 -->
				<LCAppnt>
					<AppntName>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldName" />
					</AppntName>
					<AppntSex>
						<xsl:call-template name="tran_sex">
							<xsl:with-param name="sex">
								<xsl:value-of
									select="Transaction/Transaction_Body/PbHoldSex" />
							</xsl:with-param>
						</xsl:call-template>
					</AppntSex>
					<AppntBirthday>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldBirdy" />
					</AppntBirthday>
					<AppntIDType>
						<xsl:call-template name="tran_idtype">
							<xsl:with-param name="idtype">
								<xsl:value-of
									select="Transaction/Transaction_Body/PbHoldIdType" />
							</xsl:with-param>
						</xsl:call-template>
					</AppntIDType>
					<AppntIDNo>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldId" />
					</AppntIDNo>
					<AppntPhone>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldHomeTele" />
					</AppntPhone>
					<AppntOfficePhone>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldOfficTele" />
					</AppntOfficePhone>
					<AppntMobile>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldMobl" />
					</AppntMobile>
					<HomeAddress>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldHomeAddr" />
					</HomeAddress>
					<HomeZipCode>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldHomePost" />
					</HomeZipCode>
					<MailAddress>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldHomeAddr" />
						<!-- <xsl:value-of select="Transaction/Transaction_Body/PbHoldAddr"/>  -->
					</MailAddress>
					<MailZipCode>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldHomePost" />
					</MailZipCode>
					<Email>
						<xsl:value-of
							select="Transaction/Transaction_Body/PbHoldEmail" />
					</Email>
					<!--  <JobCode>
						<xsl:call-template name="tran_jobcode">
						<xsl:with-param name="jobcode">
						<xsl:value-of select="Transaction/Transaction_Body/PbHoldOccupCode"/>
						</xsl:with-param>
						</xsl:call-template>
						</JobCode> -->
					<JobCode>04704</JobCode>
					<RelaToInsured>
						<xsl:call-template name="tran_relation">
							<xsl:with-param name="relation">
								<xsl:value-of
									select="Transaction/Transaction_Body/PbHoldRcgnRela" />
							</xsl:with-param>
						</xsl:call-template>
					</RelaToInsured>
					<TellInfos>
						<TellInfoCount>1</TellInfoCount>
						<TellInfo>
							<TellVersion />
							<TellCode>否</TellCode>
							<TellContent />
							<TellRemark />
						</TellInfo>
					</TellInfos>
				</LCAppnt>
				<!-- 调用被保人信息模板 -->
				<!-- 在被保人信息模板中，会去调用险种模板和受益人模板 -->
				<LCInsureds>
					<xsl:apply-templates
						select="Transaction/Transaction_Body" />
				</LCInsureds>
			</LCCont>
		</TranData>
	</xsl:template>
	<!-- 代码转换 没有用到-->
	<xsl:template name="tran_pay_intv">
		<xsl:param name="pay_intv">0</xsl:param>
		<xsl:if test="$pay_intv = 1">0</xsl:if>
		<xsl:if test="$pay_intv = 10">ab</xsl:if>
		<xsl:if test="$pay_intv = 2">3</xsl:if>
		<xsl:if test="$pay_intv = 4">6</xsl:if>
		<xsl:if test="$pay_intv = 15">dd</xsl:if>
	</xsl:template>
	<!--代码转换 没有用到-->
	<xsl:template name="tran_funcflag">
		<xsl:param name="funcflag">0</xsl:param>
		<xsl:if test="$funcflag = 1">01</xsl:if>
		<xsl:if test="$funcflag = 2">02</xsl:if>
		<xsl:if test="$funcflag = 3">04</xsl:if>
		<xsl:if test="$funcflag = 4">12</xsl:if>
		<xsl:if test="$funcflag = 5">05</xsl:if>
		<xsl:if test="$funcflag = 6">06</xsl:if>
		<xsl:if test="$funcflag = 7">08</xsl:if>
		<xsl:if test="$funcflag = 10">14</xsl:if>
		<xsl:if test="$funcflag = 12">11</xsl:if>
		<xsl:if test="$funcflag = 20">16</xsl:if>
	</xsl:template>
	<!--职业代码代码转换-->
	<xsl:template name="tran_jobcode">
		<xsl:param name="jobcode">00</xsl:param>
		<xsl:if test="$jobcode = 'A'">00102</xsl:if>
		<xsl:if test="$jobcode = 'B'">02</xsl:if>
		<xsl:if test="$jobcode = 'C'">03</xsl:if>
		<xsl:if test="$jobcode = 'D'">04</xsl:if>
		<xsl:if test="$jobcode = 'E'">05</xsl:if>
		<xsl:if test="$jobcode = 'F'">06</xsl:if>
		<xsl:if test="$jobcode = 'G'">07</xsl:if>
		<xsl:if test="$jobcode = 'H'">08</xsl:if>
		<xsl:if test="$jobcode = 'I'">09</xsl:if>
		<xsl:if test="$jobcode = 'J'">10</xsl:if>
		<xsl:if test="$jobcode = 'K'">11</xsl:if>
		<xsl:if test="$jobcode = 'L'">12</xsl:if>
		<xsl:if test="$jobcode = 'M'">13</xsl:if>
		<xsl:if test="$jobcode = 'N'">14</xsl:if>
		<xsl:if test="$jobcode = 'O'">15</xsl:if>
		<xsl:if test="$jobcode = 'P'">16</xsl:if>
	</xsl:template>
	<!--代码转换 关系方之间的关系-->
	<!--因为有对应不上的关系代码，所以需要设置一个缺省值。-->
	<xsl:template name="tran_relation">
		<xsl:param name="relation">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$relation = 1">00</xsl:when>
			<xsl:when test="$relation = 2">02</xsl:when>
			<xsl:when test="$relation = 3">01</xsl:when>
			<xsl:when test="$relation = 4">04</xsl:when>
			<xsl:when test="$relation = 5">05</xsl:when>
			<xsl:when test="$relation = 6">03</xsl:when>
			<xsl:when test="$relation = 7">03</xsl:when>
			<xsl:when test="$relation = 8">19</xsl:when>
			<xsl:when test="$relation = 9">20</xsl:when>
			<xsl:when test="$relation = 10">21</xsl:when>
			<xsl:when test="$relation = 11">21</xsl:when>
			<xsl:when test="$relation = 12">22</xsl:when>
			<xsl:when test="$relation = 13">22</xsl:when>
			<xsl:when test="$relation = 14">24</xsl:when>
			<xsl:when test="$relation = 15">24</xsl:when>
			<xsl:when test="$relation = 16">12</xsl:when>
			<xsl:when test="$relation = 17">12</xsl:when>
			<xsl:when test="$relation = 18">12</xsl:when>
			<xsl:when test="$relation = 19">12</xsl:when>
			<xsl:when test="$relation = 20">09</xsl:when>
			<xsl:when test="$relation = 21">30</xsl:when>
			<xsl:when test="$relation = 22">30</xsl:when>
			<xsl:when test="$relation = 23">30</xsl:when>
			<xsl:when test="$relation = 24">30</xsl:when>
			<xsl:when test="$relation = 25">30</xsl:when>
			<xsl:when test="$relation = 26">30</xsl:when>
			<xsl:when test="$relation = 27">30</xsl:when>
			<xsl:when test="$relation = 28">30</xsl:when>
			<xsl:when test="$relation = 29">30</xsl:when>
			<xsl:when test="$relation = 30">30</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="tran_sex">
		<xsl:param name="sex">0</xsl:param>
		<xsl:if test="$sex = 1">0</xsl:if>
		<xsl:if test="$sex = 2">1</xsl:if>
	</xsl:template>
	<xsl:template name="tran_idtype">
		<xsl:param name="idtype">0</xsl:param>
		<xsl:if test="$idtype = 'A'">0</xsl:if>
		<xsl:if test="$idtype = 'B'">2</xsl:if>
		<xsl:if test="$idtype = 'C'">2</xsl:if>
		<xsl:if test="$idtype = 'D'">2</xsl:if>
		<xsl:if test="$idtype = 'E'">2</xsl:if>
		<xsl:if test="$idtype = 'F'">5</xsl:if>
		<xsl:if test="$idtype = 'G'">1</xsl:if>
		<xsl:if test="$idtype = 'H'">1</xsl:if>
		<xsl:if test="$idtype = 'I'">1</xsl:if>
		<xsl:if test="$idtype = 'J'">1</xsl:if>
		<xsl:if test="$idtype = 'K'">2</xsl:if>
		<xsl:if test="$idtype = 'L'">2</xsl:if>
		<xsl:if test="$idtype = 'M'">4</xsl:if>
		<xsl:if test="$idtype = 'Z'">4</xsl:if>
	</xsl:template>
	<xsl:template name="tran_coverage">
		<xsl:param name="coverage">0</xsl:param>
		<xsl:if test="$coverage = 0">Y</xsl:if>
		<xsl:if test="$coverage = 1">A</xsl:if>
		<xsl:if test="$coverage = 2">Y</xsl:if>
		<xsl:if test="$coverage = 3">A</xsl:if>
		<xsl:if test="$coverage = 4">M</xsl:if>
		<xsl:if test="$coverage = 5">A</xsl:if>
	</xsl:template>
	<xsl:template name="tran_get">
		<!--没有用到-->
		<xsl:param name="get">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$get = 1">1</xsl:when>
			<xsl:when test="$get = 2">12</xsl:when>
			<xsl:when test="$get = 3">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="REVMETHOD" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="tran_bonusget">
		<!--没有用到-->
		<xsl:param name="bonusget">0</xsl:param>
		<xsl:if test="$bonusget = 0">2</xsl:if>
		<xsl:if test="$bonusget = 1">3</xsl:if>
		<xsl:if test="$bonusget = 2">1</xsl:if>
	</xsl:template>
	<xsl:template name="tran_paymode">
		<xsl:param name="paymode">0</xsl:param>
		<xsl:if test="$paymode = 1">1</xsl:if>
		<xsl:if test="$paymode = 2">2</xsl:if>
		<xsl:if test="$paymode = 3">3</xsl:if>
		<xsl:if test="$paymode = 4">4</xsl:if>
		<xsl:if test="$paymode = 6">6</xsl:if>
		<xsl:if test="$paymode = 7">7</xsl:if>
		<xsl:if test="$paymode = 8">8</xsl:if>
		<xsl:if test="$paymode = 9">9</xsl:if>
	</xsl:template>
	<!-- 缴费间隔的转换 没有用到-->
	<xsl:template name="tran_payintv">
		<xsl:param name="payintv">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$payintv = 1">-1</xsl:when>
			<xsl:when test="$payintv = 2">12</xsl:when>
			<xsl:when test="$payintv = 3">1</xsl:when>
			<xsl:when test="$payintv = 4">-1</xsl:when>
			<xsl:when test="$payintv = 5">0</xsl:when>
			<xsl:when test="$payintv = 6">-1</xsl:when>
			<xsl:when test="$payintv = 7">-1</xsl:when>
			<xsl:when test="$payintv = 9">-1</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$payintv" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 保单的缴费间隔的转换 -->
	<xsl:template name="tran_Contpayintv">
		<xsl:param name="payintv">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$payintv = 0">0</xsl:when>
			<xsl:when test="$payintv = 12">12</xsl:when>
			<xsl:when test="$payintv = 1">1</xsl:when>
			<xsl:when test="$payintv = 6">6</xsl:when>
			<xsl:when test="$payintv = 3">3</xsl:when>
			<xsl:when test="$payintv = -1">-1</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="0" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 终交年期标志的转换 -->
	<xsl:template name="tran_payendyearflag">
		<xsl:param name="payendyearflag">Y</xsl:param>
		<xsl:choose>
			<xsl:when test="$payendyearflag = 1">A</xsl:when>
			<xsl:when test="$payendyearflag = 2">Y</xsl:when>
			<xsl:when test="$payendyearflag = 3">M</xsl:when>
			<xsl:when test="$payendyearflag = 4">D</xsl:when>
			<xsl:when test="$payendyearflag = 5">Y</xsl:when>
			<xsl:when test="$payendyearflag = 6">Y</xsl:when>
			<xsl:when test="$payendyearflag = 7">Y</xsl:when>
			<xsl:when test="$payendyearflag = 8">Y</xsl:when>
			<xsl:when test="$payendyearflag = 9">Y</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$payendyearflag" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="tran_sendmethod">
		<xsl:param name="sendmethod">0</xsl:param>
		<xsl:if test="$sendmethod = 1">1</xsl:if>
		<xsl:if test="$sendmethod = 2">1</xsl:if>
		<xsl:if test="$sendmethod = 3">1</xsl:if>
		<xsl:if test="$sendmethod = 4">0</xsl:if>
	</xsl:template>
	<xsl:template name="tran_bnftype">
		<xsl:param name="bnftype">0</xsl:param>
		<xsl:if test="$bnftype = 0">0</xsl:if>
		<xsl:if test="$bnftype = 1">1</xsl:if>
		<xsl:if test="$bnftype = 2">0</xsl:if><!-- 现在默认红利受益人为生存受益人 -->
		<xsl:if test="$bnftype = ''">1</xsl:if>
	</xsl:template>
	<xsl:template name="tran_bnfgrade">
		<!--这个不知道有什么作用-->
		<xsl:param name="bnfgrade">0</xsl:param>
		<xsl:if test="$bnfgrade = 1">1</xsl:if>
		<xsl:if test="$bnfgrade = 2">2</xsl:if>
		<xsl:if test="$bnfgrade = 3">3</xsl:if>
		<xsl:if test="$bnfgrade = 4">4</xsl:if>
	</xsl:template>
	<xsl:template name="tran_rela">
		<xsl:param name="rela">00</xsl:param>
		<xsl:if test="$rela = 6">07</xsl:if>
		<xsl:if test="$rela = 7">12</xsl:if>
		<xsl:if test="$rela = 8">00</xsl:if>
		<xsl:if test="$rela = 9">22</xsl:if>
		<xsl:if test="$rela = 0">23</xsl:if>
		<xsl:if test="$rela = 1">01</xsl:if>
		<xsl:if test="$rela = 2">02</xsl:if>
		<xsl:if test="$rela = 3">03</xsl:if>
		<xsl:if test="$rela = 4">04</xsl:if>
		<xsl:if test="$rela = 5">05</xsl:if>
	</xsl:template>
	<xsl:template name="tran_RiskCode">
		<xsl:param name="RiskCode">0</xsl:param>
		<xsl:if test="$RiskCode = 0002">331201</xsl:if>
		<xsl:if test="$RiskCode = 0001">230701</xsl:if>
		<xsl:if test="$RiskCode = 0003">331301</xsl:if>
		<xsl:if test="$RiskCode = 0004">331601</xsl:if>
		<xsl:if test="$RiskCode = 0005">240501</xsl:if>
		<xsl:if test="$RiskCode = 0006">331701</xsl:if>
		<xsl:if test="$RiskCode = 2601">30024000</xsl:if>
		<!-- xsl:if test="$RiskCode = 0007">331901</xsl:if-->
		<xsl:if test="$RiskCode = 0007">730101</xsl:if>
		<xsl:if test="$RiskCode = 0008">332401</xsl:if>
		<xsl:if test="$RiskCode = 332501">332501</xsl:if>
		<xsl:if test="$RiskCode = 531701">531701</xsl:if>
	</xsl:template>
	<xsl:template name="tran_bonusmode">
		<xsl:param name="bonusmode">0</xsl:param>
		<xsl:if test="$bonusmode = 0">1</xsl:if>
		<xsl:if test="$bonusmode = 2">2</xsl:if>
	</xsl:template>
	<xsl:template name="BaseInfo"
		match="Transaction/Transaction_Header">
		<BaseInfo>
			<BankDate>
				<xsl:value-of select="BkPlatDate" />
			</BankDate>
			<!--建行新接口报文，直接将银行编码写死成03 -->
			<BankCode>03</BankCode>
			<!--建行号称前三位是省分行代码-->
			<ZoneNo>
				<xsl:value-of select="substring(BkBrchNo, 1, 3)" />
				<!--<xsl:value-of select="BkBrchNo"/>-->
			</ZoneNo>
			<BrNo>
				<xsl:value-of select="substring(BkBrchNo, 4, 9)" />
				<!--<xsl:value-of select="BkBrchNo"/>-->
			</BrNo>
			<!--网点代码-->
			<TellerNo><!--建行银行柜员代码为12位，我方数据库(LKTRANSSTATUS)中对应字段(BANKOPERATOR)为10位，自动截取后10位。-->
				<xsl:value-of select="substring(BkTellerNo, 3)" />
			</TellerNo>
			<TransrNo>
				<xsl:value-of select="BkPlatSeqNo" />
			</TransrNo>
			<FunctionFlag>01</FunctionFlag>
			<!--change by DP at 20080519<xsl:value-of select="BkTxCode"/>-->
			<InsuID>
				<xsl:value-of select="PbInsuId" />
			</InsuID>
		</BaseInfo>
	</xsl:template>
	<!-- 投保人信息模板 -->

	<xsl:template name="tran_InsuYearFlag">
		<xsl:param name="RiskCode">0</xsl:param>
		<xsl:if test="$RiskCode = 0002">A</xsl:if>
		<xsl:if test="$RiskCode = 0003">A</xsl:if>
		<xsl:if test="$RiskCode = 0001">Y</xsl:if>
		<xsl:if test="$RiskCode = 0005">Y</xsl:if>
		<xsl:if test="$RiskCode = 0006">Y</xsl:if>
		<xsl:if test="$RiskCode = 0007">Y</xsl:if>
		<xsl:if test="$RiskCode = 0008">Y</xsl:if>
		<xsl:if test="$RiskCode = 332501">A</xsl:if>
		<xsl:if test="$RiskCode = 531701">A</xsl:if>
	</xsl:template>
	<xsl:template name="LCAppnt" match="Transaction/Transaction_Body">


	</xsl:template>
	<!-- 被保人信息模板 -->
	<xsl:template name="LCInsured"
		match="Transaction/Transaction_Body">
		<LCInsured>
			<Name>
				<xsl:value-of select="LiRcgnName" />
			</Name>
			<Sex>
				<xsl:call-template name="tran_sex">
					<xsl:with-param name="sex">
						<xsl:value-of select="LiRcgnSex" />
					</xsl:with-param>
				</xsl:call-template>
			</Sex>
			<Birthday>
				<xsl:value-of select="LiRcgnBirdy" />
			</Birthday>
			<IDType>
				<xsl:call-template name="tran_idtype">
					<xsl:with-param name="idtype">
						<xsl:value-of select="LiRcgnIdType" />
					</xsl:with-param>
				</xsl:call-template>
			</IDType>
			<IDNo>
				<xsl:value-of select="LiRcgnId" />
			</IDNo>
			<!-- <JobCode>
				<xsl:call-template name="tran_jobcode">
				<xsl:with-param name="jobcode">
				<xsl:value-of select="LiRcgnOccupCode"/>
				</xsl:with-param>
				</xsl:call-template>
				</JobCode>  -->
			<JobCode>04704</JobCode>
			<HomePhone>
				<xsl:value-of select="LiRcgnTele" />
			</HomePhone>
			<InsuredMobile>
				<xsl:value-of select="LiRcgnMobl" />
			</InsuredMobile>
			<HomeAddress>
				<xsl:value-of select="LiRcgnAddr" />
			</HomeAddress>
			<HomeZipCode>
				<xsl:value-of select="LiRcgnPost" />
			</HomeZipCode>
			<MailZipCode>
				<xsl:value-of select="LiRcgnPost" />
			</MailZipCode>
			<MailAddress>
				<xsl:value-of select="LiRcgnAddr" />
				<!-- <xsl:value-of select="LiRcgnAddr"/>  -->
			</MailAddress>
			<Email>
				<xsl:value-of select="LiRcgnEmail" />
			</Email>
			<RelaToMain>00</RelaToMain>
			<RelaToAppnt>
				<xsl:call-template name="tran_relation">
					<xsl:with-param name="relation">
						<xsl:value-of select="PbHoldRcgnRela" />
					</xsl:with-param>
				</xsl:call-template>
			</RelaToAppnt>
			<TellInfos>
				<TellInfoCount>
					<xsl:value-of select="count(LiHealthTag)" />
				</TellInfoCount>
				<TellInfo>
					<TellVersion />
					<TellCode>
						<xsl:value-of select="LiHealthTag" />
					</TellCode>
					<TellContent>
						<!--<xsl:value-of select="LiHealthTag"/>  徐斯伦写的-->
					</TellContent>
					<TellRemark />
				</TellInfo>
			</TellInfos>
			<Risks>
				<xsl:variable name="AdditionalRiskCount"
					select="count(Appd_List/Appd_Detail)" />
				<RiskCount>
					<xsl:value-of select="$AdditionalRiskCount + 1" />
				</RiskCount>
				<xsl:variable name="MainProductCode"
					select="PbInsuType" />
				<Risk>
					<RiskCode>
						<xsl:call-template name="tran_RiskCode">
							<xsl:with-param name="RiskCode">
								<xsl:value-of select="PbInsuType" />
							</xsl:with-param>
						</xsl:call-template>
					</RiskCode>
					<MainRiskCode>
						<xsl:call-template name="tran_RiskCode">
							<xsl:with-param name="RiskCode">
								<xsl:value-of select="$MainProductCode" />
							</xsl:with-param>
						</xsl:call-template>
					</MainRiskCode>
					<RiskType />
					<!-- 保额 -->
					<Amnt>
						<xsl:value-of select="PbMainInsuExp" />
					</Amnt>
					<Rate />
					<!-- 费率 -->
					<!-- 保险费 -->
					<Prem>
						<xsl:value-of select="PbInsuExp" />
					</Prem>
					<!-- 追加保费 PbInsuExpAppd-->
					<SupplementaryPrem>
						<xsl:value-of select="PbRemark1" />
					</SupplementaryPrem>
					<!-- 投保份数 -->
					<Mult>
						<xsl:value-of select="PbSlipNumb" />
					</Mult>
					<PayIntv>
						<xsl:call-template name="tran_Contpayintv">
							<xsl:with-param name="payintv">
								<xsl:value-of select="PbPayPeriod" />
							</xsl:with-param>
						</xsl:call-template>
					</PayIntv>
					<!-- 缴费间隔 -->
					<Years>
						<xsl:value-of
							select="/Transaction/Transaction_Body/PbInsuYear" />
					</Years>
					<!-- 保险期间 -->
					<SpecContent />
					<!-- 特别约定 -->
					<!-- 缴费年期年龄 -->
					<!--<xsl:if test="string-length(PbPayAgeTag)!=0">
						<PayEndYearFlag>Y</PayEndYearFlag>
						<PayEndYear>
						<xsl:value-of select="PbPayAgeTag"/>
						</PayEndYear>
						</xsl:if>-->
					<xsl:if test="string-length(PbPayAge)!=0">
						<!-- <PayEndYearFlag>A</PayEndYearFlag> -->
						<PayEndYearFlag>
							<xsl:call-template
								name="tran_InsuYearFlag">
								<xsl:with-param name="RiskCode">
									<xsl:value-of
										select="/Transaction/Transaction_Body/PbInsuType" />
								</xsl:with-param>
							</xsl:call-template>
						</PayEndYearFlag>
						<PayEndYear>
							<xsl:value-of select="PbPayAgeTag" />
						</PayEndYear>
					</xsl:if>
					<!-- 领取年龄年期 -->
					<!--<xsl:if test="string-length(PbDrawAgeTag)!=0">
						<GetYearFlag>Y</GetYearFlag>
						<GetYear>
						<xsl:value-of select="PbDrawAgeTag"/>
						</GetYear>
						</xsl:if>-->
					<xsl:if test="string-length(PbDrawAge)!=0">
						<GetYearFlag>A</GetYearFlag>
						<GetYear>
							<xsl:value-of select="PbDrawAge" />
						</GetYear>
					</xsl:if>
					<!-- 保险年期年龄标志 由于建行前台录入页面只有一个域，为了测试先把这一段注释掉 DP at 20080618 -->
					<!--<xsl:if test="string-length(LiInsuPeriod)!=0">
						<InsuYearFlag>Y</InsuYearFlag>
						<InsuYear>
						<xsl:value-of select="LiInsuPeriod"/>
						</InsuYear>
						</xsl:if>-->
					<xsl:if test="string-length(LiInsuPeriod)!=0">
						<InsuYearFlag>
							<xsl:call-template
								name="tran_InsuYearFlag">
								<xsl:with-param name="RiskCode">
									<xsl:value-of
										select="/Transaction/Transaction_Body/PbInsuType" />
								</xsl:with-param>
							</xsl:call-template>
						</InsuYearFlag>
						<InsuYear>
							<xsl:value-of select="LiInsuPeriod" />
						</InsuYear>
					</xsl:if>
					<AutoPayFlag>
						<xsl:value-of select="PbAutoPayTag" />
					</AutoPayFlag>
					<!-- 垫交标志 -->
					<BonusPayMode>
						<xsl:value-of select="LiBonusDistbTag" />
					</BonusPayMode>
					<!-- 红利分配标记 -->
					<SubFlag>
						<xsl:value-of select="LiPayOffTag" />
					</SubFlag>
					<!-- 减额交清标志 -->
					<BonusGetMode>
						<xsl:call-template name="tran_bonusmode">
							<xsl:with-param name="bonusmode">
								<xsl:value-of select="LiBonusGetMode" />
							</xsl:with-param>
						</xsl:call-template>
					</BonusGetMode>
					<Accounts>
						<AccountCount />
						<Account>
							<AccNo />
							<AccMoney />
							<AccRate />
						</Account>
					</Accounts>
					<HealthFlag />
					<FullBonusGetMode />
					<FirstRate />
					<SureRate />
					<!-- 调用受益人信息模板 -->
					<LCBnfs>
						<LCBnfCount>
							<xsl:value-of
								select="count(/Transaction/Transaction_Body/Benf_List/Benf_Detail)" />
						</LCBnfCount>
						<xsl:apply-templates
							select="/Transaction/Transaction_Body/Benf_List/Benf_Detail" />
					</LCBnfs>
				</Risk>
				<!-- 调用附加险信息模板 -->
				<!-- Risk>
					<xsl:apply-templates
						select="/Transaction/Transaction_Body/Appd_List/Appd_Detail" />
				</Risk-->
				<xsl:if test="/Transaction/Transaction_Body/BkNum1 != 0">
				<Risk>
			<RiskCode>
			<xsl:call-template name="tran_RiskCode">
							<xsl:with-param name="RiskCode">
								<xsl:value-of select="/Transaction/Transaction_Body/Appd_List/Appd_Detail/LiAppdInsuType" />
							</xsl:with-param>
						</xsl:call-template>
			</RiskCode>
			<MainRiskCode>
			<xsl:call-template name="tran_RiskCode">
							<xsl:with-param name="RiskCode">
								<xsl:value-of select="$MainProductCode" />
							</xsl:with-param>
						</xsl:call-template></MainRiskCode>
			<RiskType />
			<!-- 保额 -->
			<Amnt> 
				<xsl:value-of select="/Transaction/Transaction_Body/Appd_List/Appd_Detail/LiAppdInsuAmot" />
			</Amnt>
			<Rate />
			<!-- 费率 -->

			<!-- 保险费 -->
			<Prem>
				<xsl:value-of select="/Transaction/Transaction_Body/Appd_List/Appd_Detail/LiAppdInsuExp" />
			</Prem>
			<!-- 追加保费 -->
			<!-- 投保份数 -->
			<Mult>
				<xsl:value-of select="/Transaction/Transaction_Body/Appd_List/Appd_Detail/LiAppdInsuNumb" />
			</Mult>
			<PayIntv>
				<xsl:call-template name="tran_Contpayintv">
					<xsl:with-param name="payintv">
						<xsl:value-of
							select="/Transaction/Transaction_Body/PbPayPeriod" />
					</xsl:with-param>
				</xsl:call-template>
			</PayIntv>
			<!-- 缴费间隔 -->
			<Years>
				<xsl:value-of select="/Transaction/Transaction_Body/PbInsuYear" />
			</Years>
			<!-- 保险期间 -->
			<SpecContent />
			<!-- 特别约定 -->
			<!-- 缴费年期年龄 -->
				<PayEndYearFlag>
					<xsl:call-template name="tran_InsuYearFlag">
						<xsl:with-param name="RiskCode">
							<xsl:value-of
								select="/Transaction/Transaction_Body/PbInsuType" />
						</xsl:with-param>
					</xsl:call-template>
				</PayEndYearFlag>
				<PayEndYear>
					<xsl:value-of
						select="/Transaction/Transaction_Body/Appd_List/Appd_Detail/LiAppdInsuPayTerm" />
				</PayEndYear>
			<xsl:if
				test="string-length(/Transaction/Transaction_Body/PbDrawAge)!=0">
				<GetYearFlag>A</GetYearFlag>
				<GetYear>
					<xsl:value-of
						select="/Transaction/Transaction_Body/PbDrawAge" />
				</GetYear>
			</xsl:if>

			<xsl:variable name="RiskCode"
				select="/Transaction/Transaction_Body/PbInsuType" />

				<InsuYearFlag>
					<xsl:call-template name="tran_InsuYearFlag">
						<xsl:with-param name="RiskCode">
							<xsl:value-of
								select="/Transaction/Transaction_Body/PbInsuType" />
						</xsl:with-param>
					</xsl:call-template>
				</InsuYearFlag>

				<!-- <InsuYearFlag>A</InsuYearFlag>   -->
				<InsuYear>
					<xsl:value-of select="/Transaction/Transaction_Body/Appd_List/Appd_Detail/LiAppdInsuTerm" />
				</InsuYear>
			<AutoPayFlag>
				<xsl:value-of
					select="/Transaction/Transaction_Body/PbAutoPayTag" />
			</AutoPayFlag>
			<!-- 垫交标志 -->
			<BonusPayMode>
				<xsl:value-of
					select="/Transaction/Transaction_Body/LiBonusDistbTag" />
			</BonusPayMode>
			<!-- 红利分配标记 -->
			<SubFlag>
				<xsl:value-of
					select="/Transaction/Transaction_Body/LiPayOffTag" />
			</SubFlag>
			<!-- 减额交清标志 -->
			<BonusGetMode>
				<xsl:call-template name="tran_bonusmode">
					<xsl:with-param name="bonusmode">
						<xsl:value-of
							select="/Transaction/Transaction_Body/LiBonusGetMode" />
					</xsl:with-param>
				</xsl:call-template>
			</BonusGetMode>
			<Accounts>
				<AccountCount />
				<Account>
					<AccNo />
					<AccMoney />
					<AccRate />
				</Account>
			</Accounts>
			<HealthFlag />
			<FullBonusGetMode />
			<FirstRate />
			<SureRate />
			<!-- 调用受益人信息模板 -->
			<LCBnfs>
				<LCBnfCount>
					<xsl:value-of
						select="count(/Transaction/Transaction_Body/Benf_List/Benf_Detail)" />
				</LCBnfCount>
				<xsl:apply-templates
					select="/Transaction/Transaction_Body/Benf_List/Benf_Detail" />
			</LCBnfs>
		</Risk>
		</xsl:if> 
			</Risks>
		</LCInsured>
	</xsl:template>
	<!-- 受益人信息模板 -->
	<xsl:template name="LCBnf"
		match="/Transaction/Transaction_Body/Benf_List/Benf_Detail">
		<LCBnf>
			<BnfType>
				<xsl:call-template name="tran_bnftype">
					<xsl:with-param name="bnftype">
						<xsl:value-of select="PbBenfType" />
					</xsl:with-param>
				</xsl:call-template>
			</BnfType>
			<BnfNo>
				<xsl:value-of select="PbBenfSer" />
			</BnfNo>
			<BnfGrade>
				<xsl:call-template name="tran_bnfgrade">
					<xsl:with-param name="bnfgrade">
						<xsl:value-of select="PbBenfSequ" />
					</xsl:with-param>
				</xsl:call-template>
			</BnfGrade>
			<Name>
				<xsl:value-of select="PbBenfName" />
			</Name>
			<Sex>
				<xsl:call-template name="tran_sex">
					<xsl:with-param name="sex">
						<xsl:value-of select="PbBenfSex" />
					</xsl:with-param>
				</xsl:call-template>
			</Sex>
			<Birthday>
				<xsl:value-of select="PbBenfBirdy" />
			</Birthday>
			<IDType>
				<xsl:call-template name="tran_idtype">
					<xsl:with-param name="idtype">
						<xsl:value-of select="PbBenfIdType" />
					</xsl:with-param>
				</xsl:call-template>
			</IDType>
			<IDNo>
				<xsl:value-of select="PbBenfId" />
			</IDNo>
			<BnfLot>
				<xsl:value-of select="PbBenfProp" />
			</BnfLot>
			<RelaToAppnt />
			<RelationToInsured>
				<xsl:call-template name="tran_relation">
					<xsl:with-param name="relation">
						<xsl:value-of select="PbBenfHoldRela" />
					</xsl:with-param>
				</xsl:call-template>
			</RelationToInsured>
			<!-- 与主被保人关系 -->
		</LCBnf>
	</xsl:template>
	<!-- 险种信息模板 -->
	<xsl:template name="Risks"
		match="/TXLife/TXLifeRequest/OLifE/Holding/Policy/Life">
	</xsl:template>
</xsl:stylesheet>
