<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<Transaction>
			<!--报文体-->
			<Transaction_Body>
				<PbSlipNumb>
					<xsl:value-of select="/TranData/LCCont/Mult"/>
				</PbSlipNumb>
				<!--投保份数-->
				<PiStartDate>
					<xsl:value-of select="/TranData/LCCont/CValiDate"/>
				</PiStartDate>
				<!--起始日期-->
				<PiEndDate>
					<xsl:value-of select="/TranData/LCCont/ContEndDate"/>
				</PiEndDate>
				<!--终止日期-->
				<BkTotAmt>
					<xsl:value-of select="/TranData/LCCont/Prem"/>
				</BkTotAmt>
				<!--总保费-->
				<BkTxAmt>
					<xsl:value-of select="/TranData/LCCont/Prem"/>
				</BkTxAmt>
				<!--首期保费-->
				<PbMainExp>
					<xsl:value-of select="/TranData/LCCont/Prem"/>
				</PbMainExp>
				<!-- 首期主险保费 -->
				<BkNum1>0</BkNum1><!-- 附加险个数 现阶段情况下为0-->
		 	</Transaction_Body>
		</Transaction>
	</xsl:template>
</xsl:stylesheet>
