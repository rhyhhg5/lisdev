<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<TranData>
			<xsl:apply-templates select="Transaction/Transaction_Header"/>
			<LMCalMode>
				<CalSQL><xsl:value-of select="Transaction/Transaction_Body/BkDetail1"/></CalSQL>
				<Remark><xsl:value-of select="Transaction/Transaction_Body/BkDetail2"/></Remark>
			</LMCalMode>
		</TranData>
	</xsl:template>

	<xsl:template name="BaseInfo" match="Transaction/Transaction_Header">
		<BaseInfo>
			<BankDate><xsl:value-of select="BkPlatDate"/></BankDate>
			<BankTime><xsl:value-of select="BkPlatTime"/></BankTime>
			<BankCode>03</BankCode>
			<ZoneNo><xsl:value-of select="BkBrchNo"/></ZoneNo>
			<BrNo><xsl:value-of select="BkBrchNo"/></BrNo>
			<TellerNo><xsl:value-of select="BkTellerNo"/></TellerNo>
			<TransrNo><xsl:value-of select="BkPlatSeqNo"/></TransrNo>
			<SaleChannel><xsl:value-of select="BkChnlNo"/></SaleChannel>
			<FunctionFlag>99</FunctionFlag>
			<InsuID><xsl:value-of select="PbInsuId"/></InsuID>
		</BaseInfo>
	</xsl:template>
</xsl:stylesheet>
