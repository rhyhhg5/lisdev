<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<Transaction>
			<Transaction_Body>
				<!--# # # # 险种信息 # # # # -->
				<!--主险险种信息-->
				<PbInsuType>
					<xsl:call-template name="tran_RiskCode">
						<xsl:with-param name="RiskCode">
							<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/MainRiskCode"/>
						</xsl:with-param>
					</xsl:call-template>
				</PbInsuType>
				<PiEndDate>
					<xsl:value-of select="/TranData/LCCont/ContEndDate"/>
				</PiEndDate>
				<PbFinishDate>
					<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/PayToDate"/>
				</PbFinishDate>
				<LiDrawstring>
					<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/GetStartDate"/>
				</LiDrawstring>
				<!--# # # # 现金价值表# # # #因为建行不要一定传，所以直接置零先-->
				<LiCashValueCount>0</LiCashValueCount>
				<!--<xsl:value-of select="/TranData/LCCont/CashValues/CashValueCount"/>暂时置零-->
				<!--现金价值表循环，循环标签用Cash_List，每条现金价值的标签用Cash_Detail-->
				<!--<Cash_List>
					<xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
						<xsl:variable name="RiskCD" select="RiskCode"/>
						<xsl:variable name="MainRiskCD" select="MainRiskCode"/>
						<xsl:if test="$RiskCD=$MainRiskCD">
							<xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValues/CashValue">
								<Cash_Detail>
									<LiCashYearEnd>
										<xsl:value-of select="End"/>
									</LiCashYearEnd>
									<LiCashYearCash>
										<xsl:value-of select="Cash"/>
									</LiCashYearCash>
								</Cash_Detail>
							</xsl:for-each>
						</xsl:if>
					</xsl:for-each>
				</Cash_List>-->
				<!--循环结束-->
				<!--# # # # 红利保额保单年度末现金价值表 # # # #-->
				<LiBonusValueCount>0</LiBonusValueCount>
				<!--Bonus  红利保额保单年度末现金价值表循环-->
				<!--循环标签用Bonus_List，每条现金价值的标签用Bonus_Detail-->
				<!--<Bonus_List>
					<Bonus_Detail>
						<LiBonusEnd/>
						<LiBonusCash/>
					</Bonus_Detail>
				</Bonus_List>-->
				<!--循环结束-->
				<PbInsuSlipNo>
					<xsl:value-of select="/TranData/LCCont/ContNo"/>
				</PbInsuSlipNo>
				<!--保单号-->
				<BkTotAmt>
					<xsl:value-of select="/TranData/LCCont/Prem"/>
				</BkTotAmt>
				<!--总保费-->
				<LiSureRate>
					<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/SureRate"/>
				</LiSureRate>
				<!--保证利率-->
				<PbBrokId>
					<xsl:value-of select="/TranData/LCCont/AgentCode"/>
				</PbBrokId>
				<!--业务员代码-->
				<LiBrokName>
					<xsl:value-of select="/TranData/LCCont/AgentName"/>
				</LiBrokName>
				<!--业务员姓名-->
				<LiBrokGroupNo>
					<xsl:value-of select="/TranData/LCCont/AgentGroupCode"/>
				</LiBrokGroupNo>
				<!--业务员组号-->
				<BkOthName>
					<xsl:value-of select="/TranData/LCCont/LetterService"/>
				</BkOthName>
				<!--保险公司名称-->
				<BkOthAddr>
					<xsl:value-of select="/TranData/LCCont/ComLocation"/>
				</BkOthAddr>
				<!--保险公司地址-->
				<PiCpicZipcode/>
				<!--保险公司邮编-->
				<PiCpicTelno>
					<xsl:value-of select="/TranData/LCCont/ComPhone"/>
				</PiCpicTelno>
				<!--保险公司电话-->
				<xsl:variable name="SpecContent" select="/TranData/LCCont/SpecContent"/><!-- 新加入特别约定变量 -->
				<xsl:variable name="AppntName" select="/TranData/LCCont/LCAppnt/AppntName" />
				<xsl:variable name="AppntSex" select="/TranData/LCCont/LCAppnt/AppntSex" />
				<xsl:variable name="AppntBirthday" select="/TranData/LCCont/LCAppnt/AppntBirthday" />
				<xsl:variable name="AppntIDNo" select="/TranData/LCCont/LCAppnt/AppntIDNo" />
				
				<xsl:variable name="Name" select="/TranData/LCCont/LCInsureds/LCInsured/Name" />
				<xsl:variable name="Sex" select="/TranData/LCCont/LCInsureds/LCInsured/Sex" />
				<xsl:variable name="Birthday" select="/TranData/LCCont/LCInsureds/LCInsured/Birthday" />
				<xsl:variable name="IDNo" select="/TranData/LCCont/LCInsureds/LCInsured/IDNo" />
				
				<xsl:variable name="RiskName" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/RiskName" />
				<xsl:variable name="InsuYear" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/InsuYear" />
				<xsl:variable name="PayIntv" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/PayIntv" />
				<xsl:variable name="PayEndYear" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/PayEndYear" />
				<!-- xsl:variable name="Prem" select="/TranData/LCCont/Prem" /-->
				<xsl:variable name="Mult" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/Mult" />
				<!-- xsl:variable name="Amnt" select="/TranData/LCCont/Amnt" /-->
				<xsl:variable name="CashValueCount" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValueCount" />
				<xsl:variable name="End" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValue/End" />
				<xsl:variable name="Cash" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValue/Cash" />
				<xsl:variable name="CashDate" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValue/CashDate" />
												
				<xsl:variable name="ComLocation" select="/TranData/LCCont/ComLocation" />
				<xsl:variable name="AgentGroup" select="/TranData/LCCont/AgentGroup" />
				<xsl:variable name="AgentName" select="/TranData/LCCont/AgentName" />
				<xsl:variable name="ComPhone" select="/TranData/LCCont/ComPhone" />
				<xsl:variable name="PrintDate" select="/TranData/LCCont/PrintDate" />
				<xsl:variable name="BrNo" select="/TranData/LCCont/BrNo" />
				<xsl:variable name="SumPrem" select="/TranData/LCCont/SumPrem" />
				<xsl:variable name="ManageCom4" select="substring(/TranData/LCCont/ManageCom, 1, 4)" />
				
				<!-- BkFileNum>2</BkFileNum-->
				<BkFileNum>2</BkFileNum>			
				<Detail_List>
					<BkFileDesc>保单第一页</BkFileDesc>
					<BkType1>
						<xsl:if test="$ManageCom4=8637">370020000001</xsl:if>	<!-- 山东建行 -->
						<xsl:if test="$ManageCom4=8653">530020000001</xsl:if>	<!-- 云南建行 -->
						<xsl:if test="$ManageCom4=8641">410020000001</xsl:if>	<!-- 河南建行 -->
						<xsl:if test="$ManageCom4=8613">010020000001</xsl:if>	<!-- 河北建行 -->
						<xsl:if test="$ManageCom4=8632">320020000001</xsl:if>	<!-- 江苏建行 -->
						<xsl:if test="$ManageCom4=8642">420020000001</xsl:if>	<!-- 湖北建行 -->
						<xsl:if test="$ManageCom4=8651">510020000001</xsl:if>	<!-- 四川建行 -->
						<xsl:if test="$ManageCom4=8633">330020000001</xsl:if>	<!-- 浙江建行 -->
						<xsl:if test="$ManageCom4=8621">210020000001</xsl:if>	<!-- 辽宁建行 -->
						<xsl:if test="$ManageCom4=8636">360020000001</xsl:if>	<!-- 江西建行 -->
						<xsl:if test="$ManageCom4=8615">150020000001</xsl:if>	<!-- 内蒙古建行 -->
						<xsl:if test="$ManageCom4=8622">220020000001</xsl:if>	<!-- 吉林建行 -->
						<xsl:if test="$ManageCom4=8644">440020000001</xsl:if>	<!-- 广东建行 -->
						<xsl:if test="$ManageCom4=8635">350020000001</xsl:if>	<!-- 福建建行 -->
						<xsl:if test="$ManageCom4=8665">650020000001</xsl:if>	<!-- 新疆建行 -->
					</BkType1>
					<BkVchNo>
						<xsl:value-of select="/TranData/LCCont/ProposalContNo"/>
					</BkVchNo>
					<BkRecNum>52</BkRecNum>
					<Detail>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        保单合同号(保单号)：<xsl:value-of select="/TranData/LCCont/ContNo"/></BkDetail1>
						<BkDetail1>        保险合同生效日期：<xsl:value-of select="/TranData/LCCont/CValiDate"/>缴费方式：<xsl:value-of select="$PayIntv"/></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        投保人：<xsl:value-of select="$AppntName"/>性别：<xsl:value-of select="$AppntSex"/>生日：<xsl:value-of select="$AppntBirthday"/>   证件号码：<xsl:value-of select="$AppntIDNo"/> </BkDetail1>
						<BkDetail1>        被保险人：<xsl:value-of select="$Name"/>性别：<xsl:value-of select="$Sex"/>生日：<xsl:value-of select="$Birthday"/>   证件号码：<xsl:value-of select="$IDNo"/> </BkDetail1>
						<BkDetail1></BkDetail1>
						
						<!--  xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf"-->
						
						<BkDetail1>        身故受益人：<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/Name"/>受益比例：<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/BnfLot"/>受益顺序：<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/BnfNo"/></BkDetail1>
						
						<!-- /xsl:for-each-->
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>　　　　　        险种名称                               保险期间 缴费年期     保额/份数      保险费/基本保险费</BkDetail1>
						<xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
						<xsl:variable name="RiskName" select="RiskName" />
				        <xsl:variable name="InsuYear" select="InsuYear" />
				        <xsl:variable name="PayIntv" select="PayIntv" />
				        <xsl:variable name="PayEndYear" select="PayEndYear" />
				        <xsl:variable name="Prem" select="Prem" />
				        <xsl:variable name="Amnt" select="Amnt" />
						<BkDetail1><xsl:value-of select="$RiskName"/><xsl:value-of select="$InsuYear"/><xsl:value-of select="$PayEndYear"/><xsl:value-of select="$Amnt"/><xsl:value-of select="$Prem"/>元</BkDetail1>
						</xsl:for-each>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        本期保险费合计：<xsl:value-of select="$SumPrem"/></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        特别约定:<xsl:value-of select="$SpecContent"/></BkDetail1>
						<!-- BkDetail1></BkDetail1-->
						<!-- BkDetail1></BkDetail1-->
						<!-- BkDetail1></BkDetail1-->
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        公司地址：<xsl:value-of select="$ComLocation"/> 全国咨询电话：<xsl:value-of select="$ComPhone"/></BkDetail1>
						<BkDetail1>        营业网点：<xsl:value-of select="$BrNo"/>所属团队：<xsl:value-of select="$AgentGroup"/></BkDetail1>
						<BkDetail1>        保单打印时间：<xsl:value-of select="$PrintDate"/>业务员：<xsl:value-of select="$AgentName"/></BkDetail1>
					</Detail>	
				</Detail_List>
				<Detail_List>
					<BkFileDesc>保单第二页</BkFileDesc>
					<BkType1>
						<xsl:if test="$ManageCom4=8637">370020000001</xsl:if>	<!-- 山东建行 -->
						<xsl:if test="$ManageCom4=8653">530020000001</xsl:if>	<!-- 云南建行 -->
						<xsl:if test="$ManageCom4=8641">410020000001</xsl:if>	<!-- 河南建行 -->
						<xsl:if test="$ManageCom4=8613">010020000001</xsl:if>	<!-- 河北建行 -->
						<xsl:if test="$ManageCom4=8632">320020000001</xsl:if>	<!-- 江苏建行 -->
						<xsl:if test="$ManageCom4=8642">420020000001</xsl:if>	<!-- 湖北建行 -->
						<xsl:if test="$ManageCom4=8651">510020000001</xsl:if>	<!-- 四川建行 -->
						<xsl:if test="$ManageCom4=8633">330020000001</xsl:if>	<!-- 浙江建行 -->
						<xsl:if test="$ManageCom4=8621">210020000001</xsl:if>	<!-- 辽宁建行 -->
						<xsl:if test="$ManageCom4=8636">360020000001</xsl:if>	<!-- 江西建行 -->
						<xsl:if test="$ManageCom4=8615">150020000001</xsl:if>	<!-- 内蒙古建行 -->
						<xsl:if test="$ManageCom4=8622">220020000001</xsl:if>	<!-- 吉林建行 -->
						<xsl:if test="$ManageCom4=8644">440020000001</xsl:if>	<!-- 广东建行 -->
					</BkType1>
					<BkVchNo>
						<xsl:value-of select="/TranData/LCCont/ProposalContNo"/>
					</BkVchNo>
					<BkRecNum>52</BkRecNum>
					<Detail>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
						<BkDetail1>        保单合同号：<xsl:value-of select="/TranData/LCCont/ContNo"/></BkDetail1>
						<!-- xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValues/CashValue"-->						
						<xsl:variable name="RiskName" select="RiskName" />
						<BkDetail1>        被保险人：<xsl:value-of select="$Name"/>险种名称：<xsl:value-of   select="translate($RiskName,'   ','')"   /></BkDetail1>
						<BkDetail1>                                                    　　　货币单位：人民币元</BkDetail1>
						<BkDetail1>        保单年度      保单周年日      现金价值</BkDetail1>					
						<xsl:for-each select="CashValues/CashValue">
						<!-- BkDetail1><xsl:value-of select="End"/><xsl:value-of select="CashDate"/><xsl:value-of select="Cash"/></BkDetail1-->
						<BkDetail1><xsl:value-of select="End"/><xsl:value-of select="CashDate"/><xsl:value-of select="Cash"/></BkDetail1>
						</xsl:for-each>
						</xsl:for-each>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>       备注：</BkDetail1>
						<BkDetail1>       1.此现金价值表是本公司按照中国保险监督管理委员会的相关规定计算确定的，其他未列明的保单</BkDetail1>
						<BkDetail1>       年度末的现金价值可向我公司客户服务热线95591或4006695518咨询。</BkDetail1>
						<BkDetail1>       2.保单年度末解除合同时，现金价值根据表中对应保单年度末的现金价值计算；其他时间解除合同</BkDetail1>
						<BkDetail1>       时，以本现金价值表为基础，按照我公司规定的计算方法确定。</BkDetail1>
					<!--<BkDetail1>        附注：</BkDetail1>
						<BkDetail1>        1、客户在未交足2年保险费的情况下解除合同，按照合同约定，在扣除手续费后退还保险费。</BkDetail1>
						<BkDetail1>        上表中第1、2保单年度的值即为对应年度扣除手续费后退还保险费的金额。</BkDetail1>
						<BkDetail1>        2、上表中所列现金价值为对应保单年度末的现金价值。</BkDetail1>
						<BkDetail1>        3、解除合同时，本公司将退还解除合同当时的现金价值。</BkDetail1> -->
					</Detail>
				</Detail_List>
			</Transaction_Body>
		</Transaction>
	</xsl:template>
	<xsl:template name="tran_RiskCode">
		<xsl:param name="RiskCode">0</xsl:param>
		<xsl:if test="$RiskCode = 230701">0001</xsl:if>	<!--健康专家个人防癌疾病保险-->
		<xsl:if test="$RiskCode = 240501">0005</xsl:if>	<!--安心宝个人终身重大疾病保险-->
		<xsl:if test="$RiskCode = 730101">0007</xsl:if>
		<xsl:if test="$RiskCode = 332401">0008</xsl:if>
	</xsl:template>
</xsl:stylesheet>
