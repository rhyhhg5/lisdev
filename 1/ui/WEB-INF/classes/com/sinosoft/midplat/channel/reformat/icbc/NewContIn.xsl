<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java">

	<xsl:output indent='yes' />
	<xsl:template match="/">
		<TranData>
			<xsl:apply-templates select="TXLife/TXLifeRequest" />
			<LCCont>
				<!--  -->
				<!-- <xsl:variable name="Applyno" select="INSU/MAIN/APPLYNO" /> -->
				<PrtNo>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/Holding/Policy/ApplicationInfo/HOAppFormNumber" />
				</PrtNo>

				<PolApplyDate>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/Holding/Policy/ApplicationInfo/SubmissionDate" />
				</PolApplyDate>

				<AccName>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/Holding/Policy/AcctHolderName" />
				</AccName>
				<AccBankCode></AccBankCode>
				<BankAccNo>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/Holding/Banking/AccountNumber" />
				</BankAccNo>
				<PayIntv>
					<xsl:call-template name="tran_Contpayintv">
						<xsl:with-param name="payintv">
							<xsl:value-of
								select="TXLife/TXLifeRequest/OLifE/Holding/Policy/PaymentMode/@tc" />
						</xsl:with-param>
					</xsl:call-template>
				</PayIntv>
				<PayMode>
					<xsl:call-template name="tran_paymode">
						<xsl:with-param name="paymode">
							<xsl:value-of
								select="TXLife/TXLifeRequest/OLifE/Holding/Policy/PaymentMethod/@tc" />
						</xsl:with-param>
					</xsl:call-template>
				</PayMode>

				<!-- 如果有续期帐号，则续期交费方式设为转账，否则，则续期交费方式设为现金。 -->
				<xsl:if
					test="string-length(TXLife/TXLifeRequest/OLifE/Holding/Banking/AccountNumber) = 0">
					<ExPayMode>1</ExPayMode>
				</xsl:if>
				<xsl:if
					test="string-length(TXLife/TXLifeRequest/OLifE/Holding/Banking/AccountNumber) != 0">
					<ExPayMode>4</ExPayMode>
				</xsl:if>

				<GetPolMode></GetPolMode>
				<Password>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/Password" />
				</Password>
				<SpecContent>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/SpecialClause" />
				</SpecContent>
				<ProposalContNo>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/FormInstance[FormName='2']/ProviderFormNumber" />
				</ProposalContNo>
				<TempFeeNo>
					<xsl:value-of
						select="TXLife/TXLifeRequest/OLifE/FormInstance[FormName='1']/ProviderFormNumber" />
				</TempFeeNo>
				<!-- 工行不打印发票，所以在这里直接写成0000000000 -->
				<!--<TempFeeNo>0000000000</TempFeeNo>-->
				<AgentCode></AgentCode>
				<AgentGroup></AgentGroup>
				<AgentName></AgentName>

				<!-- 调用投保人信息模板 -->
				<xsl:apply-templates
					select="TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='80']" />

				<!-- 调用被保人信息模板 -->
				<!-- 在被保人信息模板中，会去调用险种模板和受益人模板 -->
				<LCInsureds>
					<LCInsuredCount>
						<xsl:value-of
							select="count(TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='81'])" />
					</LCInsuredCount>
					<xsl:for-each
						select="TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='81']">
						<xsl:apply-templates select="." />
					</xsl:for-each>
				</LCInsureds>

			</LCCont>
		</TranData>
	</xsl:template>

	<!-- 代码转换 -->
	<xsl:template name="tran_pay_intv">
		<xsl:param name="pay_intv">0</xsl:param>
		<xsl:if test="$pay_intv = 1">0</xsl:if>
		<xsl:if test="$pay_intv = 10">ab</xsl:if>
		<xsl:if test="$pay_intv = 2">3</xsl:if>
		<xsl:if test="$pay_intv = 4">6</xsl:if>
		<xsl:if test="$pay_intv = 15">dd</xsl:if>
	</xsl:template>

	<!--代码转换-->
	<xsl:template name="tran_funcflag">
		<xsl:param name="funcflag">0</xsl:param>
		<xsl:if test="$funcflag = 1">01</xsl:if>
		<xsl:if test="$funcflag = 2">02</xsl:if>
		<xsl:if test="$funcflag = 3">04</xsl:if>
		<xsl:if test="$funcflag = 4">12</xsl:if>
		<xsl:if test="$funcflag = 5">05</xsl:if>
		<xsl:if test="$funcflag = 6">06</xsl:if>
		<xsl:if test="$funcflag = 7">08</xsl:if>
		<xsl:if test="$funcflag = 10">14</xsl:if>
		<xsl:if test="$funcflag = 12">11</xsl:if>
		<xsl:if test="$funcflag = 20">16</xsl:if>
	</xsl:template>

	<!--代码转换 关系方之间的关系-->
	<!--复合关系的映射改在newconttransfer.java里处理，此处不做处理
		<xsl:template name="tran_relation">
		<xsl:param name="relation">0</xsl:param>
		<xsl:if test="$relation = 8">00</xsl:if>
		<xsl:if test="$relation = 1">01</xsl:if>
		<xsl:if test="$relation = 3">03</xsl:if>
		<xsl:if test="$relation = 2">04</xsl:if>
		<xsl:if test="$relation = 7">25</xsl:if>
		<xsl:if test="$relation = 99">05</xsl:if>
		<xsl:if test="$relation = 43">06</xsl:if>
		<xsl:if test="$relation = 44">07</xsl:if>
		<xsl:if test="$relation = 45">08</xsl:if>
		<xsl:if test="$relation = 40">09</xsl:if>
		<xsl:if test="$relation = 41">10</xsl:if>
		<xsl:if test="$relation = 42">11</xsl:if>
		<xsl:if test="$relation = 6">12</xsl:if>
		<xsl:if test="$relation = 19">14</xsl:if>  
		<xsl:if test="$relation = 10">03</xsl:if> 
		<xsl:if test="$relation = 11">03</xsl:if> 
		</xsl:template>
	-->

	<xsl:template name="tran_RiskCode">
		<xsl:param name="RiskCode">0</xsl:param>
		<xsl:if test="$RiskCode='201'">330501</xsl:if>
		<xsl:if test="$RiskCode='202'">530301</xsl:if>
		<xsl:if test="$RiskCode='203'">330701</xsl:if>
		<xsl:if test="$RiskCode='204'">331201</xsl:if>
		<xsl:if test="$RiskCode='205'">331301</xsl:if>
		<xsl:if test="$RiskCode='001'">331601</xsl:if>
		<xsl:if test="$RiskCode='206'">230701</xsl:if>
		<xsl:if test="$RiskCode='003'">240501</xsl:if><!-- 安心宝 -->
		<xsl:if test="$RiskCode='002'">331701</xsl:if><!-- 金利宝 -->
		<xsl:if test="$RiskCode='005'">331901</xsl:if><!-- 康利相伴 -->
		<xsl:if test="$RiskCode='004'">730101</xsl:if>
		<xsl:if test="$RiskCode='104'">332401</xsl:if>
		<xsl:if test="$RiskCode = 332501">332501</xsl:if>
		<xsl:if test="$RiskCode = 531701">531701</xsl:if>
	</xsl:template>

	<xsl:template name="tran_sex">
		<xsl:param name="sex">0</xsl:param>
		<xsl:if test="$sex = 1">0</xsl:if>
		<xsl:if test="$sex = 2">1</xsl:if>
		<xsl:if test="$sex = 3">2</xsl:if>
	</xsl:template>

	<xsl:template name="tran_idtype">
		<!--3,4,6,9类型不做转换，在处理类中判断这四种类型拒保-->
		<xsl:param name="idtype">0</xsl:param>
		<xsl:if test="$idtype = 0">0</xsl:if>
		<xsl:if test="$idtype = 1">1</xsl:if>
		<xsl:if test="$idtype = 2">2</xsl:if>
		<xsl:if test="$idtype = 3">3</xsl:if>
		<xsl:if test="$idtype = 4">4</xsl:if>
		<xsl:if test="$idtype = 5">5</xsl:if>
		<xsl:if test="$idtype = 6">6</xsl:if>
		<xsl:if test="$idtype = 7">4</xsl:if>
		<!--<xsl:if test="$idtype = 8">2</xsl:if>-->
		<xsl:if test="$idtype = 9">9</xsl:if>
		<!--工行新的数据字典里没有这些类型
			<xsl:if test="$idtype = 10">8</xsl:if>
			<xsl:if test="$idtype = 11">7</xsl:if>
			<xsl:if test="$idtype = 12">8</xsl:if>
			<xsl:if test="$idtype = 99">8</xsl:if>
		-->
	</xsl:template>

	<xsl:template name="tran_coverage">
		<xsl:param name="coverage">0</xsl:param>
		<xsl:if test="$coverage = 0">Y</xsl:if>
		<xsl:if test="$coverage = 1">A</xsl:if>
		<xsl:if test="$coverage = 2">Y</xsl:if>
		<xsl:if test="$coverage = 3">M</xsl:if>
		<xsl:if test="$coverage = 4">D</xsl:if>
		<xsl:if test="$coverage = 5">A</xsl:if>
	</xsl:template>

	<xsl:template name="tran_get">
		<xsl:param name="get">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$get = 1">1</xsl:when>
			<xsl:when test="$get = 2">12</xsl:when>
			<xsl:when test="$get = 3">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="REVMETHOD" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="tran_bonusget">
		<xsl:param name="bonusget">0</xsl:param>
		<xsl:if test="$bonusget = 0">2</xsl:if>
		<xsl:if test="$bonusget = 1">3</xsl:if>
		<xsl:if test="$bonusget = 2">1</xsl:if>
	</xsl:template>

	<xsl:template name="tran_paymode">
		<xsl:param name="paymode">0</xsl:param>
		<xsl:if test="$paymode = 0">1</xsl:if>
		<xsl:if test="$paymode = 1">4</xsl:if>
	</xsl:template>

	<!-- 缴费间隔的转换 -->
	<xsl:template name="tran_payintv">
		<xsl:param name="payintv">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$payintv = 1">-1</xsl:when>
			<xsl:when test="$payintv = 2">12</xsl:when>
			<xsl:when test="$payintv = 3">1</xsl:when>
			<xsl:when test="$payintv = 4">-1</xsl:when>
			<xsl:when test="$payintv = 5">0</xsl:when>
			<xsl:when test="$payintv = 6">-1</xsl:when>
			<xsl:when test="$payintv = 7">-1</xsl:when>
			<xsl:when test="$payintv = 9">-1</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$payintv" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- 保单的缴费间隔的转换 -->
	<xsl:template name="tran_Contpayintv">
		<xsl:param name="payintv">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$payintv = 1">12</xsl:when>
			<xsl:when test="$payintv = 2">1</xsl:when>
			<xsl:when test="$payintv = 3">6</xsl:when>
			<xsl:when test="$payintv = 4">3</xsl:when>
			<xsl:when test="$payintv = 5">0</xsl:when>
			<xsl:when test="$payintv = 6">-1</xsl:when>
			<xsl:when test="$payintv = 7">-1</xsl:when>
			<xsl:when test="$payintv = 9">-1</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$payintv" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- 终交年期标志的转换 -->
	<xsl:template name="tran_payendyearflag">
		<xsl:param name="payendyearflag">Y</xsl:param>
		<xsl:choose>
			<xsl:when test="$payendyearflag = 1">A</xsl:when>
			<xsl:when test="$payendyearflag = 2">Y</xsl:when>
			<xsl:when test="$payendyearflag = 3">M</xsl:when>
			<xsl:when test="$payendyearflag = 4">D</xsl:when>
			<xsl:when test="$payendyearflag = 5">0</xsl:when>
			<xsl:when test="$payendyearflag = 6">O</xsl:when>
			<xsl:when test="$payendyearflag = 7">O</xsl:when>
			<xsl:when test="$payendyearflag = 8">O</xsl:when>
			<xsl:when test="$payendyearflag = 9">O</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$payendyearflag" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="tran_sendmethod">
		<xsl:param name="sendmethod">0</xsl:param>
		<xsl:if test="$sendmethod = 1">1</xsl:if>
		<xsl:if test="$sendmethod = 2">1</xsl:if>
		<xsl:if test="$sendmethod = 3">1</xsl:if>
		<xsl:if test="$sendmethod = 4">0</xsl:if>
	</xsl:template>
	<xsl:template name="tran_bonusgetmode">
		<xsl:param name="bonusgetmode">2</xsl:param>
		<xsl:if test="$bonusgetmode = 1">2</xsl:if>
		<xsl:if test="$bonusgetmode = 2">1</xsl:if>
		<xsl:if test="$bonusgetmode = 3">2</xsl:if>
		<xsl:if test="$bonusgetmode = 4">2</xsl:if>
		<xsl:if test="$bonusgetmode = 5">2</xsl:if>
		<xsl:if test="$bonusgetmode = 9">2</xsl:if>
	</xsl:template>
	
	<!--
		<xsl:template name="tran_rela">
		<xsl:param name="rela">00</xsl:param>
		<xsl:if test="$rela = 6">07</xsl:if>
		<xsl:if test="$rela = 7">12</xsl:if>
		<xsl:if test="$rela = 8">00</xsl:if>
		<xsl:if test="$rela = 9">22</xsl:if>
		<xsl:if test="$rela = 0">23</xsl:if>
		<xsl:if test="$rela = 1">01</xsl:if>
		<xsl:if test="$rela = 2">02</xsl:if>
		<xsl:if test="$rela = 3">03</xsl:if>
		<xsl:if test="$rela = 4">04</xsl:if>
		<xsl:if test="$rela = 5">05</xsl:if>
		</xsl:template>
	-->

	<xsl:template name="BaseInfo" match="TXLife/TXLifeRequest">
		<BaseInfo>
			<xsl:variable name="sysDate" select="TransExeDate" />
			<BankDate>
				<xsl:value-of select="TransExeDate" />
			</BankDate>
			<!-- 因为是工行的报文，直接将银行编码写死成001 -->
			<BankCode>01</BankCode>
			<ZoneNo>
				<xsl:value-of select="OLifEExtension/RegionCode" />
			</ZoneNo>
			<BrNo>
				<xsl:value-of select="OLifEExtension/Branch" />
			</BrNo>
			<TellerNo>
				<xsl:value-of select="OLifEExtension/Teller" />
			</TellerNo>
			<TransrNo>
				<xsl:value-of select="TransRefGUID" />
			</TransrNo>
			<FunctionFlag>01</FunctionFlag>
			<InsuID>
				<xsl:value-of select="OLifEExtension/CarrierCode" />
			</InsuID>
		</BaseInfo>
	</xsl:template>

	<!-- 投保人信息模板 -->
	<xsl:template name="LCAppnt"
		match="TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='80']">
		<LCAppnt>
			<!-- 得到投保人的参与方标识 -->
			<xsl:variable name="PartyID" select="@RelatedObjectID" />
			<!-- 得到投保人信息节点 -->
			<xsl:variable name="AppntNode"
				select="/TXLife/TXLifeRequest/OLifE/Party[@id=$PartyID]" />
			<AppntName>
				<xsl:value-of select="$AppntNode/FullName" />
			</AppntName>
			<AppntSex>
				<xsl:call-template name="tran_sex">
					<xsl:with-param name="sex">
						<xsl:value-of select="$AppntNode/Person/Gender" />
					</xsl:with-param>
				</xsl:call-template>
			</AppntSex>

			<AppntBirthday>
				<xsl:value-of select="$AppntNode/Person/BirthDate" />
			</AppntBirthday>
			<AppntIDType>
				<xsl:call-template name="tran_idtype">
					<xsl:with-param name="idtype">
						<xsl:value-of select="$AppntNode/GovtIDTC" />
					</xsl:with-param>
				</xsl:call-template>
			</AppntIDType>
			<AppntIDNo>
				<xsl:value-of select="$AppntNode/GovtID" />
			</AppntIDNo>
			<AppntPhone>
				<xsl:value-of
					select="$AppntNode/Phone[PhoneTypeCode='1']/DialNumber" />
			</AppntPhone>
			<AppntOfficePhone>
				<xsl:value-of
					select="$AppntNode/Phone[PhoneTypeCode='2']/DialNumber" />
			</AppntOfficePhone>
			<AppntMobile>
				<xsl:value-of
					select="$AppntNode/Phone[PhoneTypeCode='3']/DialNumber" />
			</AppntMobile>
			<HomeAddress>
				<xsl:value-of
					select="$AppntNode/Address[AddressTypeCode='1']/Line1" />
			</HomeAddress>
			<HomeZipCode>
				<xsl:value-of
					select="$AppntNode/Address[AddressTypeCode='1']/Zip" />
			</HomeZipCode>
			<MailAddress>
				<xsl:value-of
					select="$AppntNode/Address[AddressTypeCode='2']/Line1" />
			</MailAddress>
			<MailZipCode>
				<xsl:value-of
					select="$AppntNode/Address[AddressTypeCode='2']/Zip" />
			</MailZipCode>
			<Email>
				<xsl:value-of select="$AppntNode/EMailAddress/AddrLine" />
			</Email>
			<!--
				<JobCode><xsl:value-of select="$AppntNode/Person/OccupationType"/></JobCode>
			-->
			<JobCode>04704</JobCode>
			<!-- PICC职业代码的录入和工行还没有确定先写死 -->
			<RelaToInsured>
				<!-- 得到被保人的参与方标识 -->
				<xsl:variable name="InsuredPartyID"
					select="/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='81']/@RelatedObjectID" />
				<!-- 得到与被保人关系并做转换 -->
				<xsl:value-of
					select="/TXLife/TXLifeRequest/OLifE/Relation[@OriginatingObjectID=$PartyID and @RelatedObjectID=$InsuredPartyID]/RelationRoleCode" />
				<!--
					<xsl:call-template name="tran_rela">
					<xsl:with-param name="rela">
					<xsl:value-of select="/TXLife/TXLifeRequest/OLifE/Relation[@OriginatingObjectID=$PartyID and @RelatedObjectID=$InsuredPartyID]/RelationRoleCode"/>
					</xsl:with-param>
					</xsl:call-template>
				-->
			</RelaToInsured>
			<!--
				<TellInfos>
				<TellInfoCount></TellInfoCount>
				<TellInfo>
				<TellVersion></TellVersion>
				<TellCode></TellCode>
				<TellContent></TellContent>
				<TellRemark></TellRemark>
				</TellInfo>
				</TellInfos>
			-->
		</LCAppnt>
	</xsl:template>

	<!-- 被保人信息模板 -->
	<xsl:template name="LCInsured"
		match="TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='81']">
		<LCInsured>
			<!-- 得到被保人的参与方标识 -->
			<xsl:variable name="PartyID" select="@RelatedObjectID" />
			<!-- 得到被保人信息节点 -->
			<xsl:variable name="LCInsuredNode"
				select="/TXLife/TXLifeRequest/OLifE/Party[@id=$PartyID]" />
			<Name>
				<xsl:value-of select="$LCInsuredNode/FullName" />
			</Name>
			<Sex>
				<xsl:call-template name="tran_sex">
					<xsl:with-param name="sex">
						<xsl:value-of
							select="$LCInsuredNode/Person/Gender" />
					</xsl:with-param>
				</xsl:call-template>
			</Sex>
			<Birthday>
				<xsl:value-of select="$LCInsuredNode/Person/BirthDate" />
			</Birthday>
			<IDType>
				<xsl:call-template name="tran_idtype">
					<xsl:with-param name="idtype">
						<xsl:value-of select="$LCInsuredNode/GovtIDTC" />
					</xsl:with-param>
				</xsl:call-template>
			</IDType>
			<IDNo>
				<xsl:value-of select="$LCInsuredNode/GovtID" />
			</IDNo>
			<!--<JobCode><xsl:value-of select="$LCInsuredNode/Person/OccupationType"/></JobCode>-->
			<JobCode>04704</JobCode>
			<Nationality></Nationality>
			<HomePhone>
				<xsl:value-of
					select="$LCInsuredNode/Phone[PhoneTypeCode='1']/DialNumber" />
			</HomePhone>
			<InsuredMobile>
				<xsl:value-of
					select="$LCInsuredNode/Phone[PhoneTypeCode='3']/DialNumber" />
			</InsuredMobile>
			<HomeAddress>
				<xsl:value-of
					select="$LCInsuredNode/Address[AddressTypeCode='1']/Line1" />
			</HomeAddress>
			<HomeZipCode>
				<xsl:value-of
					select="$LCInsuredNode/Address[AddressTypeCode='1']/Zip" />
			</HomeZipCode>
			<MailAddress>
				<xsl:value-of
					select="$LCInsuredNode/Address[AddressTypeCode='2']/Line1" />
			</MailAddress>
			<MailZipCode>
				<xsl:value-of
					select="$LCInsuredNode/Address[AddressTypeCode='2']/Zip" />
			</MailZipCode>
			<Email>
				<xsl:value-of
					select="$LCInsuredNode/EMailAddress/AddrLine" />
			</Email>
			<RelaToMain>00</RelaToMain><!-- 目前银保通系统仅支持单被保人，此处默认置为“00-本人” -->
			<!-- 与投保人关系 -->
			<xsl:variable name="InsuredParty"
				select="/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='81']/@RelatedObjectID" />
			<xsl:variable name="AppntParty"
				select="/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='80']/@RelatedObjectID" />
			<xsl:variable name="RelaAppntToInsured"
				select="/TXLife/TXLifeRequest/OLifE/Relation[@OriginatingObjectID=$InsuredParty and @RelatedObjectID=$AppntParty]/RelationRoleCode/@tc" />
			<xsl:if test="string-length($RelaAppntToInsured)=0">
				<RelaToAppnt>
					<xsl:value-of
						select="/TXLife/TXLifeRequest/OLifE/Relation[@OriginatingObjectID=$AppntParty and @RelatedObjectID=$InsuredParty]/RelationRoleCode/@tc" />
					<!--
						<xsl:variable name="RelaAppntToInsured1" select="/TXLife/TXLifeRequest/OLifE/Relation[@OriginatingObjectID=$AppntParty and @RelatedObjectID=$InsuredParty]/RelationRoleCode/@tc"/>
						<xsl:call-template name="tran_relation">
						<xsl:with-param name="relation">
						<xsl:value-of select="$RelaAppntToInsured1"/>
						</xsl:with-param>
						</xsl:call-template>
					-->
				</RelaToAppnt>
			</xsl:if>
			<xsl:if test="string-length($RelaAppntToInsured)!=0">
				<RelaToAppnt>
					<xsl:value-of select="$RelaAppntToInsured" />
					<!--
						<xsl:call-template name="tran_relation">
						<xsl:with-param name="relation">
						<xsl:value-of select="$RelaAppntToInsured"/>
						</xsl:with-param>
						</xsl:call-template>
					-->
				</RelaToAppnt>
			</xsl:if>

			<!-- 处理健康告知，财务告知信息 001代表健康告知;002代表职业告知-->
			<TellInfos>
				<TellInfoCount>2</TellInfoCount>
				<TellInfo>
					<TellVersion></TellVersion>
					<TellCode>001</TellCode>
					<TellContent>
						<xsl:value-of
							select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/HealthIndicator" />
					</TellContent>
					<TellRemark></TellRemark>
				</TellInfo>
				<TellInfo>
					<TellVersion></TellVersion>
					<TellCode>002</TellCode>
					<TellContent>
						<xsl:value-of
							select="TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/FinanceIndicator" />
					</TellContent>
					<TellRemark></TellRemark>
				</TellInfo>
			</TellInfos>

			<!-- 获取险种明细信息 -->
			<xsl:apply-templates
				select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/Life" />
		</LCInsured>
	</xsl:template>

	<!-- 受益人信息模板 -->
	<xsl:template name="LCBnf"
		match="/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='82']">
		<LCBnf>
			<!-- 得到参与方标识 -->
			<xsl:variable name="PartyID" select="@RelatedObjectID" />
			<!-- 得到信息节点 -->
			<xsl:variable name="PaytyNode"
				select="/TXLife/TXLifeRequest/OLifE/Party[@id=$PartyID]" />
			<BnfType>1</BnfType><!-- 受益人类别(0-生存受益人; 1-死亡受益人)，这里默认为“1-死亡受益人” -->
			<BnfGrade>
				<xsl:value-of select="Sequence" />
			</BnfGrade>
			<!-- 增加指定受益人字段，工行没有传  -->
			<CustomerNo>1</CustomerNo>


			<Name>
				<xsl:value-of select="$PaytyNode/FullName" />
			</Name>
			<Sex>
				<xsl:call-template name="tran_sex">
					<xsl:with-param name="sex">
						<xsl:value-of select="$PaytyNode/Person/Gender" />
					</xsl:with-param>
				</xsl:call-template>
			</Sex>

			<xsl:variable name="LCBnfBirthDate"
				select="$PaytyNode/Person/BirthDate" />
			<Birthday>
				<xsl:value-of select="$PaytyNode/Person/BirthDate" />
			</Birthday>
			<IDType>
				<xsl:call-template name="tran_idtype">
					<xsl:with-param name="idtype">
						<xsl:value-of select="$PaytyNode/GovtIDTC" />
					</xsl:with-param>
				</xsl:call-template>
			</IDType>
			<IDNo>
				<xsl:value-of select="$PaytyNode/GovtID" />
			</IDNo>
			<BnfLot>
				<xsl:value-of select="InterestPercent" />
			</BnfLot>


			<RelaToAppnt></RelaToAppnt>
			<xsl:variable name="BnfParty" select="@RelatedObjectID" />
			<xsl:variable name="InsuredParty"
				select="/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='81']/@RelatedObjectID" />
			<xsl:variable name="RelaInsuredToBnf"
				select="/TXLife/TXLifeRequest/OLifE/Relation[@OriginatingObjectID=$BnfParty and @RelatedObjectID=$InsuredParty]/RelationRoleCode/@tc" />
			<xsl:if test="string-length($RelaInsuredToBnf)=0">
				<xsl:variable name="RelaInsuredToBnf1"
					select="/TXLife/TXLifeRequest/OLifE/Relation[@OriginatingObjectID=$InsuredParty and @RelatedObjectID=$BnfParty]/RelationRoleCode/@tc" />
				<RelationToInsured>
					<xsl:value-of select="$RelaInsuredToBnf1" />
					<!--
						<xsl:call-template name="tran_relation">
						<xsl:with-param name="relation">
						<xsl:value-of select="$RelaInsuredToBnf1"/>
						</xsl:with-param>
						</xsl:call-template>
					-->
				</RelationToInsured><!-- 与主被保人关系 -->
			</xsl:if>
			<xsl:if test="string-length($RelaInsuredToBnf)!=0">
				<RelationToInsured>
					<xsl:value-of select="$RelaInsuredToBnf" />
					<!--
						<xsl:call-template name="tran_relation">
						<xsl:with-param name="relation">
						<xsl:value-of select="$RelaInsuredToBnf"/>
						</xsl:with-param>
						</xsl:call-template>
					-->
				</RelationToInsured><!-- 与主被保人关系 -->
			</xsl:if>

			<BelongToInsured /><!-- 受益人所属被保人 -->
			<Address /><!-- 受益人地址 -->
		</LCBnf>
	</xsl:template>

	<!-- 险种信息模板 -->
	<xsl:template name="Risks"
		match="/TXLife/TXLifeRequest/OLifE/Holding/Policy/Life">
		<Risks>
			<RiskCount>
				<xsl:value-of select="count(Coverage)" />
			</RiskCount>
			<xsl:variable name="MainProductCode"
				select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/Life/Coverage[IndicatorCode = '1']/ProductCode" />
			<xsl:for-each select="Coverage">
				<Risk>
					<RiskCode>
						<xsl:call-template name="tran_RiskCode">
							<xsl:with-param name="RiskCode">
								<xsl:value-of select="ProductCode" />
							</xsl:with-param>
						</xsl:call-template>
					</RiskCode>
					<!-- 工行不会传险种计划
						<PolPlanName><xsl:value-of select="substring(ProductCode, 6, 1)"/></PolPlanName>
					-->
					<MainRiskCode>
						<xsl:call-template name="tran_RiskCode">
							<xsl:with-param name="RiskCode">
								<xsl:value-of select="$MainProductCode" />
							</xsl:with-param>
						</xsl:call-template>
					</MainRiskCode>
					<RiskType></RiskType>

					<!-- 保额 -->
					<Amnt>
						<xsl:value-of select="InitCovAmt" />
					</Amnt>
					<Rate></Rate><!-- 费率 -->
					<CValiDate></CValiDate><!-- 起保日期 -->

					<!-- <CValiDate>20070810</CValiDate> -->
					<Rank></Rank><!-- 档次 -->
					<!-- 保险费 -->
					<Prem>
						<xsl:value-of select="ModalPremAmt" />
					</Prem>
					<!-- 投保份数 -->
					<Mult>
						<xsl:value-of select="IntialNumberOfUnits" />
					</Mult>
					<!-- 追加保费(万能C) -->
					<SupplementaryPrem>
						<xsl:value-of select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/FirstSuperaddAmt" />
					</SupplementaryPrem>
					<PayIntv>
						<xsl:call-template name="tran_Contpayintv">
							<xsl:with-param name="payintv">
								<xsl:value-of
									select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/PaymentMode/@tc" />
							</xsl:with-param>
						</xsl:call-template>
					</PayIntv><!-- 缴费间隔 -->
					<CostIntv></CostIntv><!-- 扣款间隔 -->
					<CostDate></CostDate><!-- 扣款时间 -->
					<Years>
						<xsl:value-of select="OLifEExtension/Duration" />
					</Years><!-- 保险期间 -->
					<SpecContent></SpecContent><!-- 特别约定 -->
					<!-- 缴费年期年龄标志 -->
					<PayEndYearFlag>
						<xsl:call-template name="tran_payendyearflag">
							<xsl:with-param name="payendyearflag">
								<xsl:value-of
									select="OLifEExtension/PaymentDurationMode" />
							</xsl:with-param>
						</xsl:call-template>
					</PayEndYearFlag>
					<!-- 缴费年期年龄 工行趸交时年期年龄字段置1-->
					<xsl:if
						test="OLifEExtension/PaymentDurationMode!='5'">
						<PayEndYear>
							<xsl:value-of
								select="OLifEExtension/PaymentDuration" />
						</PayEndYear>
					</xsl:if>
					<xsl:if
						test="OLifEExtension/PaymentDurationMode='5'">
						<PayEndYear>1</PayEndYear>
					</xsl:if>
					<!-- 长城保险公司不需要录入领取信息，而是直接在后台处理。 -->
					<!-- 领取年龄年期标志 -->
					<GetYearFlag></GetYearFlag>
					<!-- 领取年龄 -->
					<GetYear></GetYear>
					<!-- 保险年期年龄标志 -->
					<InsuYearFlag>
						<xsl:call-template name="tran_coverage">
							<xsl:with-param name="coverage">
								<xsl:value-of
									select="OLifEExtension/DurationMode" />
							</xsl:with-param>
						</xsl:call-template>
					</InsuYearFlag>
					<!-- 保险年期年龄 -->
					<xsl:if test="OLifEExtension/DurationMode!='5'">
						<InsuYear>
							<xsl:value-of
								select="OLifEExtension/Duration" />
						</InsuYear>
					</xsl:if>
					<xsl:if test="OLifEExtension/DurationMode='5'">
						<InsuYear>1000</InsuYear>
					</xsl:if>
					<GetIntv></GetIntv><!-- 领取方式 -->
					<!-- 领取银行编码 -->
					<GetBankCode></GetBankCode>
					<!-- 领取银行账户 -->
					<GetBankAccNo></GetBankAccNo>
					<GetAccName></GetAccName><!-- 领取银行户名 -->
					<AutoPayFlag></AutoPayFlag><!-- 垫交标志 -->
					<BonusPayMode></BonusPayMode><!-- 红利分配标记 -->
					<SubFlag></SubFlag><!-- 减额交清标志 -->
					<BonusGetMode>
					  <!-- xsl:value-of select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/Life/DivType/@tc" /-->
					  <xsl:call-template name="tran_bonusgetmode">
							<xsl:with-param name="bonusgetmode">
								<xsl:value-of
									select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/Life/DivType/@tc" />
							</xsl:with-param>
						</xsl:call-template>
					</BonusGetMode>
					<Accounts>
						<AccountCount></AccountCount>
						<Account>
							<AccNo></AccNo>
							<AccMoney></AccMoney>
							<AccRate></AccRate>
						</Account>
					</Accounts>
					<HealthFlag></HealthFlag>
					<FullBonusGetMode></FullBonusGetMode>
					<FirstRate></FirstRate>
					<SureRate></SureRate>

					<!-- 调用受益人信息模板 -->
					<LCBnfs>
						<xsl:if test="/TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/BeneficiaryIndicator = 'Y'">
							<LCBnfCount>0</LCBnfCount>
						</xsl:if>
						<xsl:if test="/TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/BeneficiaryIndicator != 'Y'">
							<LCBnfCount>
								<xsl:value-of
									select="count(/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='82'])" />
							</LCBnfCount>
							<!--<xsl:for-each select="/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='82']">-->
							<xsl:apply-templates
								select="/TXLife/TXLifeRequest/OLifE/Relation[RelationRoleCode/@tc='82']" />
							<!--</xsl:for-each>-->
						</xsl:if>
					</LCBnfs>
				</Risk>
			</xsl:for-each>
		</Risks>
	</xsl:template>

</xsl:stylesheet>
