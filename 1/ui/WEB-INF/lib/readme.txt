log4j日志：
   log4j-1.2.15.jar  -log4j日志系统主模块
   mail.jar          -支持log4j的邮件功能。可以不要，但缺少此包，无法使用log4j的自动发送邮件日志功能

xml解析(jdom)
   jdom-1.1.jar   -jdom主类
   jaxen-jdom.jar;jaxen-core.jar;saxpath.jar    -XPath支持类。缺少这个三个类，不能使用jdom的XPath

WebService(axis)
	axis.jar			-axis主类
	wsdl4j-1.5.1.jar;saaj.jar;jaxrpc.jar;activation.jar	-WebService协议实现类
	commons-logging-1.0.4.jar;commons-discovery-0.2.jar	-日志处理(log4j、java.util.logging等的上层通用框架)