<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
var turnPage = new turnPageClass();
function initInpBox()
{
  try{
    fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
    fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
    fm.MainFeeNo.value = '<%= request.getParameter("MainFeeNo") %>';
    fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
    
    var LoadFlag = '<%= request.getParameter("LoadFlag") %>';
    var strSQL3="select customername from llcase where caseno='"+fm.CaseNo.value+"'";
	var arrResult3 = easyExecSql(strSQL3);
	if(arrResult3 != null && arrResult3 !="" ){ 
		 fm.CustomerName.value = arrResult3[0][0];
     }
    if(LoadFlag=='2'){
      fm.confirmButton.style.display = 'none';
    }
  }
  catch(ex)
  {
    alert(ex.message);
    alert("在CaseChargeDetailsInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在CaseChargeDetailsInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCaseDrugGrid();
    easyQuery();
    //sumFee();
    //getCaseRemark();
    calVales();
       
  }
  catch(ex)
  {
    alert("CaseChargeDetailsInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//移动光标mulline计算，都得靠自己啊
function calVales(){
	var num = CaseDrugGrid.mulLineCount;
	//alert(num);
	if(num != 0){
		// 通过 getElementsByName 获得都有 mulline 控件 
		var inputs1 =document.getElementsByName("CaseDrugGrid5");//单价 
		// 为每一个button绑定onclick事件，alert一下 
		for(var i=0;i<inputs1.length;i++){ 
			//alert(inputs1.length);
			inputs1[i].onchange = function(){ 
				calPrice(); 
			} 
		} 
		var inputs2 =document.getElementsByName("CaseDrugGrid6");//数量 
		// 为每一个button绑定onclick事件，alert一下 
		for(var i=0;i<inputs2.length;i++){ 
			//alert(inputs2.length);
			inputs2[i].onchange = function(){ 
				calPrice(); 
			} 
		} 
	}
}


//收据费用明细
function initCaseDrugGrid()
{
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]= new Array("费用类型","60px","100","2","detailstype","1|11","1|0");
      iArray[1][19]= 1; 
      
      //iArray[1][10]="chargedetailstype";  //引用代码："CodeName"为传入数据的名称
      //iArray[1][11]="0|^1|药品^2|诊疗项目^3|服务设施"; //"CodeContent" 是传入要下拉显示的代码
      //iArray[1][12]="1|11";     //引用代码对应第几列，'|'为分割符
      //iArray[1][13]="1|0";    //上面的列中放置引用代码中第几位值
      
           
      iArray[2]=new Array();
      iArray[2][0]="费用代码";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="费用名称";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="规格";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[3][21]="ChargeDetail";

      iArray[5]=new Array();
      iArray[5][0]="单价";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[5][21]="UnitPrice";
      iArray[5][9]="UnitPrice|NOTNULL";

      iArray[6]=new Array();
      iArray[6][0]="数量";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[6][21]="Amount";

      iArray[7]=new Array();
      iArray[7][0]="金额";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[7][21]="ChargeFee";

      iArray[8]= new Array("给付类别","60px","100","2","detailsgivetype","8|10","1|0");
      //iArray[8][10]="detailsgivetype";  //引用代码："CodeName"为传入数据的名称
      //iArray[8][11]="0|^1|甲类^2|乙类^3|丙类"; //"CodeContent" 是传入要下拉显示的代码
      //iArray[8][12]="8|10";     //引用代码对应第几列，'|'为分割符
      //iArray[8][13]="1|0";    //上面的列中放置引用代码中第几位值
      

      iArray[9]=new Array();
      iArray[9][0]="帐单号";         			//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=80;            			//列最大值
      iArray[9][3]=3;      

	  iArray[10]= new Array("给付类别代码","80px","100","3");
	  iArray[11]= new Array("费用类型代码","80px","100","3");
	  
      CaseDrugGrid = new MulLineEnter( "fm" , "CaseDrugGrid" ); 
      CaseDrugGrid.mulLineCount = 1;   
      CaseDrugGrid.displayTitle = 1;
      CaseDrugGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      CaseDrugGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)     
      CaseDrugGrid.canSel=0;
      CaseDrugGrid.canChk=0;
      CaseDrugGrid.addEventFuncName="calVales";//“+”你写的JS函数名，不加扩号
      
      //CaseDrugGrid.delEventFuncName="calVales";//“-”你写的JS函数名，不加扩号

      
      CaseDrugGrid.loadMulLine(iArray);      
      
      }
      catch(ex)
      {
        //alert(ex);
      }
  }

</script>