/*******************************************************************************
* Name: LLRegisterInput.js
* Function:立案主界面的函数处理程序
* Author:LiuYansong
*/
var showInfo;
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();   

//#1769 案件备注信息的录入和查看功能 add by Houyd
function showCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}
	//#1769 案件备注信息的录入和查看功能 自动弹出页面只弹一次
	//获取session中的ShowCaseRemarkFlag
	var tShowCaseRemarkFlag = fm.ShowCaseRemarkFlag.value;
	if(tShowCaseRemarkFlag == "" || tShowCaseRemarkFlag == null || tShowCaseRemarkFlag == "null"){
		//tShowCaseRemarkFlag = "1";//准备存入session的标记:0-未弹出；1-已弹出		
	}else{
		return ;
	}	
	var tSql = "select 1 from llcaseremark where caseno='"+aCaseNo+"' with ur";
	var tResult = easyExecSql(tSql);
	if(tResult != null){
		pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  		showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
  	}	
}
function openCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}

	pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
}

/*******************************************************************************
* Name        :main_init
* Author      :LiuYansong
* CreateDate  :2003-12-16
* ModifyDate  :
* Function    :初始化主程序
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info){
  var i = 0;
  var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action ="./"+A_jsp;
  //alert("target标志是"+Target_Flag);
  if(Target_Flag=="1"){
    fm.target = "./"+T_jsp;
  }
  else{
    fm.target = "fraSubmit";
  }
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
fm.IdAppConf.disabled=false;
//fm.payreceipt.disabled=false;
  showInfo.close();
  
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var strsql = "select a.RgtState,b.codename,a.Operator,a.MakeDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
    +" and b.codetype='llrgtstate' and b.code=a.rgtstate";
    arrResult = easyExecSql(strsql);

    if ( arrResult ){
      fm.RgtState.value= arrResult[0][1];
      fm.Handler.value= arrResult[0][2];
      //fm.ModifyDate.value= arrResult[0][4];
      fm.ModifyDate.value = getCurrentDate();

      initTitle();
			fm.Sex.value = fm.Sex.value=='女'?'1':'0';
      ClientafterQuery( fm.RgtNo.value,fm.CaseNo.value );
    }

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    if(fm.appReasonCode[04].checked == true){
      var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerName.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
      showInfo=OpenWindowNew(pathStr,"EventInput","middle");
    }
  //alert(fm.OpType.value);
  if(fm.OpType.value=="OK")
   {
    //执行下一步操作(发送email、短信)
  	var ExeSql = "select returnmode,rgtantmobile,email,rgtantname from llregister where rgtno='"+fm.RgtNo.value+"'";
  	arr=easyExecSql(ExeSql);
  	if(arr)
  	{ //alert(arr[0][1]);
  		//alert(arr[0][2]);
  	  //fm.AnswerMode.value=arr[0][0];
  	  fm.Mobile.value=arr[0][1];
  	  fm.Email.value=arr[0][2];
  	  
  	  fm.SMSContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	                      +"发信人：中国人保健康"
  	  fm.EmailContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	                      +"发信人：中国人保健康"
  	  fm.action="./GrpSendMail.jsp";
  	  fm.submit();
  	}
   } 	  
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try{
    initForm();
  }
  catch(re){
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";
  }
}

//打印理赔申请书的函数
function PLPsqs(){
  main_init("PLPsqs.jsp","1","f1print","打印");
  showInfo.close();
}

function dealCaseGetMode(){
  mode_flag="1";
  if(fm.CaseGetMode.value=="4"||fm.CaseGetMode.value=="7")
  {
    if(fm.BankCode.value=="")
    {
      alert("请您录入开户银行的信息！！！");
      mode_flag="0";
      return;
    }
    if(fm.AccName.value=="")
    {
      alert("请您录入户名信息！！！！");
      mode_flag="0";
      return;
    }
    if(fm.BankAccNo.value=="")
    {
      alert("请您录入账户信息！！！");
      mode_flag="0";
      return;
    }
  }
}

function ClientQuery(){
  var  wherePart = getWherePart("a.InsuredNo", 'CustomerNo' )
  + getWherePart("a.name", 'CustomerName' )
  + getWherePart( 'a.ContNo','ContNo' )
  + getWherePart('a.OthIDNo','OtherIDNo')
  + getWherePart('a.IDNo','tIDNo');
  if ( wherePart == ""){
    alert("请输入查询条件");
    return false;
  }

  var strSQL0 ="select distinct a.insuredno,a.name,a.sex,a.birthday,"
  +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=a.IDType),"
  +"a.idno,'' ,'',a.IDType,a.OthIDNo,a.OthIDType ";
  var sqlpart1="from lcinsured a,lccont b where 1=1 and a.contno=b.contno and b.appflag='1' ";
  var sqlpart2="from lbinsured a,lbcont b where 1=1 and a.contno=b.contno and b.appflag='1' ";
  var sqlpart3="from lcinsured a,lccont b,llregister c where 1=1 and a.contno=b.contno and b.appflag='1' and b.GrpContNo=c.RgtObjNo and c.RgtNo='"+fm.RgtNo.value+"'";
  var sqlpart4="from lbinsured a,lbcont b,llregister c where 1=1 and a.contno=b.contno and b.appflag='1' and b.GrpContNo=c.RgtObjNo and c.RgtNo='"+fm.RgtNo.value+"'";
  var strSQL;
  if (fm.RgtNo.value==null||fm.RgtNo.value==""){
    strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
  }else{
    strSQL=strSQL0+sqlpart3+ wherePart+" union "+strSQL0+sqlpart4+wherePart;    
  }
  //+ " and b.AddressNo= a.AddressNo ";

  var arr= easyExecSql( strSQL,1,0,1 );


  if ( arr==null ){
    alert("没有符合条件的数据");
    return false;
  }
//  try{
    //如果查询出的数据是一条，显示在页面，如果是多条数据填
    //出一个页面，显示在mulline中
    if(arr.length==1){
      afterLLRegister(arr);
    }
    else{
      var varSrc = "&CustomerNo=" + fm.CustomerNo.value;
      varSrc += "&CustomerName=" + fm.CustomerName.value;
      varSrc += "&ContNo=" + fm.ContNo.value;
      varSrc += "&IDType=" + fm.tIDType.value;
      varSrc += "&IDNo=" + fm.tIDNo.value;
      varSrc += "&OtherIDNo=" + fm.OtherIDNo.value;
      if (fm.RgtNo.value!=null){
          varSrc += "&RgtNo=" + fm.RgtNo.value;
      }
      showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
    }
    var RgtNoPart="";
    var ModifyDatePart= " ORDER BY ModifyDate DESC,ModifyTime DESC";
    if (RgtNo==null||RgtNo==''){
    }else{
      RgtNoPart=" and grpcontno in (select rgtobjno from llregister where rgtno='"+RgtNo+"')";
    }
    var strSQL ="select a.Bankcode,a.Bankaccno,a.Accname "
    +"from lcinsured a where insuredno='"+fm.CustomerNo.value+"'"
    + RgtNoPart
    + ModifyDatePart;
    var drr= easyExecSql( strSQL,1,0,1 );
    if (drr&&drr[0][1]!=''){
      fm.BankCode.value=drr[0][0];
      fm.BankAccNo.value=drr[0][1];
      fm.AccName.value=drr[0][2];
    }else{ 
    var strSQLB = "select a.Bankcode,a.Bankaccno,a.Accname "
    +"from lbinsured a where insuredno='"+fm.CustomerNo.value+"'"
    + RgtNoPart
    + ModifyDatePart;
    var drrB= easyExecSql( strSQLB,1,0,1 );
    if (drrB&&drrB[0][1]!=''){
      fm.BankCode.value=drrB[0][0];
      fm.BankAccNo.value=drrB[0][1];
      fm.AccName.value=drrB[0][2];
    }
  }
//}catch(ex){
//    alert("错误:"+ ex.message);
//  }
}

function openAffix(){
  var varSrc ="";
  pathStr="./CaseAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value+"&LoadFlag=1&LoadC="+fm.LoadC.value;
  showInfo = OpenWindowNew(pathStr,"","middle");
}

function submitFormSurvery(){
  strSQL="select otherno from LLSurvey where otherno='"+fm.CaseNo.value+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult){
    if(!confirm("该案件提起过调查，您确定要继续吗？")){
      return false;
    }
  }
  else{

  }
  
    //TODO:临时校验，社保案件不可提起调查,需要通过案件号判断受理的案件
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
  
  //TODO:新增效验，理赔二核时不可提起调查
	var sql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstate=easyExecSql(sql);
	if (rgtstate!=null){
	     if(rgtstate[0][0]=='16'){
	       alert("理赔二核中不可提起调查，必须等待二核结束！");
	       return false;
	     }
	}
  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&StartPhase=0";
  varSrc += "&Type=1";

  pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,500);
}

function submitForm(){
var customerno=fm.CustomerNo.value;
var sql="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='"+customerno+"') and a.edorstate !='0'  and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
var sql2="select contno From lpedoritem a,lpedorapp b where  contno in(select contno from lccont where insuredno='"+customerno+"') and a.edoracceptno = b.edoracceptno and b.edorstate !='0'";
      
if(easyExecSql(sql)){
alert("该客户正在进行保全减人操作！");
return false;
}

if(easyExecSql(sql2)){
 if(!confirm("该客户正在进行保全操作！")){
  return false;
  }
}
var tSexSQL="select customersex from llcase where customerno='"+ fm.CustomerNo.value +"' with ur";
var aSex =easyExecSql(tSexSQL);
if(fm.tIDType.value=="0" || fm.tIDType.value=="5"){
//	alert(aSex);

	var strChkIdNo=checkIdNo(fm.tIDType.value,fm.tIDNo.value,fm.CBirthday.value,aSex[0][0]);
	if(strChkIdNo!=""){
		if(!confirm("被保人身份证号不符合规则，请核实")){
			return false;
		}
	}
	
}
if(fm.Relation.value=="00"){
	if(fm.IDType.value=="0" || fm.IDType.value=="5"){
		var strChkIdNo=checkIdNo(fm.IDType.value,fm.IDNo.value,fm.CBirthday.value,aSex[0][0]);
  		if(strChkIdNo!=""){
  			if(!confirm("申请人身份证号不符合规则，请核实")){
  				return false;
  			}
  		}
	}
}else{
	if(fm.IDType.value=="0" || fm.IDType.value=="5"){
		var strChkIdNo=checkIdNo(fm.IDType.value,fm.IDNo.value,"","");
  		if(strChkIdNo!=""){
  			alert("申请人身份证号不符合规则，请核实");
  			return false;
  		}
	} 
}

//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
if(fm.paymode.value!="1" && fm.paymode.value!="2"){
	if(fm.RelaDrawerInsured.value=="00"){
		if(fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5"){
			var strChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,fm.CBirthday.value,aSex[0][0]);
	  		if(strChkIdNo!=""){
	  			if(!confirm("领款人证件号不符合规则，请核实")){
	  				return false;
	  			}
	  		}
		}
	}else{
		if(fm.RelaDrawerInsured.value!="28"){
			if(fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5"){
				var strChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,"","");
		  		if(strChkIdNo!=""){
		  			alert("领款人证件号不符合规则，请核实");
		  			return false;
		  		}
			}
		}
		 
	} 
}//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求

  if(fm.RgtNo.value.substring(0,1)!='P'||fm.AppealFlag.value=='1'){
    if( verifyInput2() == false ) return false;
    
  }
  else{
    if(fm.paymode.value==''){
      alert("请录入受益金领取方式！");
      return false;
    }
  }
  if(fm.paymode.value!='1'&&fm.paymode.value!='2'){
    if(fm.BankCode.value==''){
      alert("请录入银行编码！");
      return false;
    }
    if(fm.BankAccNo.value==''){
      alert("请录入银行账号！");
      return false;
    }
    if(fm.AccName.value==''){
      alert("请录入银行账户名！");
      return false;
    }
    if(fm.paymode.value=='4'){
    	var tBankSQL="SELECT * FROM ldbank WHERE bankcode='"+fm.BankCode.value+"' AND cansendflag='1'";
    	if (!easyExecSql(tBankSQL)>0){
    		if(confirm("该银行不支持银行转账")){
    			fm.paymode.value='11';
    			alert("该银行不支持银行转账，给付方式修改为银行汇款");
    		} else {
    		  return false;
    	  }
    	}
    }
  }
//   if(fm.tIDType.value=="0"){
    if(!checkID1(fm.tIDNo.value)){
    	return false;
    }
//  }
    
    
  //if(fm.IDType.value=="0" ){
  //  if(!checkIdCard(fm.IDNo.value))
  // return false;
  //}
  if(fm.ReturnMode.value=='3'&&fm.Email.value==''){
    alert("您选择的回复方式为电子邮件，请录入电子邮箱地址！")
    return false;
  }
  if(fm.ReturnMode.value=='4'&&fm.Mobile.value==''){
    alert("您选择的回复方式为短信，请录入手机号码！")
    return false;
  }
  //3633 发生日期
  var appDate = fm.AppDate.value;
  if(!llcheckInputDate(appDate)){
	  alert("请核实申请日期录入是否正确");
	  return false;
  }
  //3633 死亡日期
  var DeathDate = fm.DeathDate.value;
  if(DeathDate!=""&&DeathDate!=null){
	  if(!llcheckInputDate(DeathDate)){
		  alert("请核实死亡日期录入是否正确");
		  return false; 
	  }
  }
  var EventCount=EventGrid.mulLineCount;
  var chkFlag=false;
  for (i=0;i<EventCount;i++){
    if(EventGrid.getChkNo(i)==true){
    	//# 3500 发生地点信息校验**start**
    	if((EventGrid.getRowColData(i,3)== null) || (EventGrid.getRowColData(i,3)=="")){
      	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(省)信息");
      	  EventGrid.setFocus(i,1,EventGrid);
            return false;
        }
        if((EventGrid.getRowColData(i,4)== null) || (EventGrid.getRowColData(i,4)=="")){
      	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(市)信息");
      	  EventGrid.setFocus(i,1,EventGrid);
            return false;
        }
        if((EventGrid.getRowColData(i,5)== null) || (EventGrid.getRowColData(i,5)=="")){
      	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(县)信息");
      	  EventGrid.setFocus(i,1,EventGrid);
            return false;
        }
     // 校验发生地点省市县编码
        var checkAccCode=checkAccPlace(EventGrid.getRowColData(i,15),EventGrid.getRowColData(i,16),EventGrid.getRowColData(i,17))
        if(checkAccCode !=""){
     	   alert(checkAccCode);
     	   return false;
        }
      //# 3500 发生地点信息校验**end**
        //3633 发生日期
        var happenDate = EventGrid.getRowColData(i,2);
        var HospstarDate = EventGrid.getRowColData(i,10);
        var HospendDate = EventGrid.getRowColData(i,11);
        if(!isDate(happenDate)){
        	alert("第"+(i+1)+"行发生日期有误，请核实发生日期");
        	return false;
        }
        if(!llcheckInputDate(happenDate)){
        	alert("请核实第"+(i+1)+"行发生日期录入是否正确");
        	return false;
        }
        if(HospstarDate!=""&&HospstarDate!=null){
	   
	        if(!llcheckInputDate(HospstarDate)){
	        	alert("请核实第"+(i+1)+"行入院日期录入是否正确");
	        	return false;
	        }

        }
        if(HospendDate!=""&&HospendDate!=null){
	        if(!llcheckInputDate(HospendDate)){
	        	alert("请核实第"+(i+1)+"行出院日期录入是否正确");
	        	return false;
	        }
        }
      chkFlag=true;
    }
  }
  if (chkFlag==false){
    alert("请选中关联事件");
    return false;
  }
  for(var rowNum=0;rowNum<EventGrid.mulLineCount;rowNum++){
   if(EventGrid.getChkNo(rowNum)){
      var happenDate=EventGrid.getRowColData(rowNum,2);
      var sql3="select count(1) From lpedoritem a,lpedorapp b where  contno in(select contno from lccont where insuredno='"+customerno+"') and a.edoracceptno = b.edoracceptno and a.edorappdate>'"+happenDate+"' with ur";
      
      if(easyExecSql(sql3)>0){
      if(!confirm("第"+rowNum+1+"行的事件在出险后申请过保全项目，是否继续下面的操作？")){return false;}
      }

   
      }
    }
  //3558
  var nowDate=getCurrentDate();
  for(var rNum=0;rNum<EventCount;rNum++){
    var happenDate=EventGrid.getRowColData(rNum,2);
    if(!isDate(happenDate)){
  	  alert("第"+(rNum+1)+"行发生日期有误，请核实事件发生日期")
  	  return false;
    }
    //3558
    
    var happenDate1=modifydate(happenDate);
    var day=dateDiff(happenDate1,nowDate,"D");
    if(day<0){
      alert("您在第"+(rNum+1)+"行输入的发生日期不应晚于当前时间");
      return false;
    }
    
    var inDate=EventGrid.getRowColData(rNum,10);

    var outDate=EventGrid.getRowColData(rNum,11);
    if (outDate.length!=0&&inDate.length!=0){ //3558
  	if(isDate(outDate)&&isDate(inDate)){
      if(dateDiff(inDate,outDate,"D")<0){
        alert("第"+(rNum+1)+"行入出院日期有误，请核实被保险人入出院日期");
        return false;
      }
    }else{
  	  alert("第"+(rNum+1)+"行入出院日期有误，请核实被保险人入出院日期");
        return false;
    }
    }
  } //3558
  if(fm.AppealFlag.value=="0"){
    //处理申请确认
    var nowDate=getCurrentDate();
    if(EventGrid.checkValue2(EventGrid.name,EventGrid)== false) return false;

    for(var rowNum=0;rowNum<EventGrid. mulLineCount;rowNum++){
      var happenDate=EventGrid.getRowColData(rowNum,2);
      var happenDate1=modifydate(happenDate);
      var day=dateDiff(happenDate1,nowDate,"D");
      if(day<0){
        alert("您在第"+(rowNum+1)+"行输入的发生日期不应晚于当前时间");
        return false;
      }
      var inDate=EventGrid.getRowColData(rowNum,10);
      inDate=modifydate(inDate);
      var outDate=EventGrid.getRowColData(rowNum,11);
      if (outDate.length!=0){
        outDate=modifydate(outDate);
        day1=dateDiff(inDate,outDate,"D");
        if(dateDiff(inDate,outDate,"D")<0){
          alert("您在第"+(rowNum+1)+"行输入的出院日期不应早于住院时间");
          return false;
        }
      }
    }
    //客户尚有未结案校
    strSQL = "select caseno from llcase where rgtstate not in ('14','09','12','11') and customerno='"
    +fm.all('CustomerNo').value+"' and caseno <> '"+fm.CaseNo.value+"'" ;
    crrResultTemp = easyExecSql(strSQL);

    if(crrResultTemp){
      if(!confirm("该客户有案件"+crrResultTemp+"未结案，您确定要继续吗？"))
      return false;
    }
    else{

    }



    if ( fm.CaseNo.value !='' ){
      //			alert( "案件已保存，您不能执行此操作");
      //			return false;
    }
    if(fm.CustomerNo.value==null||fm.CustomerNo.value==""){
      alert("请输入客户数据");
    }
    else{
      if (fm.all('rgtflag').value == "1"){
        fm.all('operate').value = "UPDATE||MAIN";

        strSQL = " select AppPeoples,customerno,grpname from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
        arrResult = easyExecSql(strSQL);
        if(arrResult != null){
          var AppPeoples;
          try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
          var appntno= arrResult[0][1];
          strSQL = "select InsuredNo from lcpol where appntno='"+appntno+"' and InsuredNo='"
          +fm.all('CustomerNo').value
          +"' union select InsuredNo from lbpol where appntno='"+appntno+"' and InsuredNo=' "
          +fm.all('CustomerNo').value +"'";
          crrResult = easyExecSql(strSQL);
          if(crrResult){
          }
          else{
            if(!confirm("团体客户"+arrResult[0][2]+"未给个人客户"+fm.CustomerName.value+"投保，您确定要继续吗？"))
            return false;
          }
        }
        else{
          alert("团体案件信息查询失败！");
          return;
        }

        strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
        arrResult = easyExecSql(strSQL);
        if(arrResult != null){
          var RealPeoples;
          try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
        }
        if ( (eval(AppPeoples) - eval(RealPeoples)) < 5 ){
          alert("团体申请人数：" + AppPeoples + "实际已录入人数：" + RealPeoples);
        }
        if ( eval(RealPeoples) >= eval(AppPeoples) ){
          //					if (!confirm("团体申请人数：" + AppPeoples + ", 实际已录入人数：" + RealPeoples + "你确认要补加个人客户吗?"));
          //					{
          //						return;
          //					}
        }
      }
      else{
        fm.all('operate').value = "INSERT||MAIN";
      }
	
        fm.IdAppConf.disabled=true;
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
       
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.action ="./ClientReasonSave.jsp";
      fm.submit();
     
    }
  }
  else{
    if(fm.AppeanRCode.value==null||fm.AppeanRCode.value==""){
      alert("请选择申诉原因！");
      return false;
    }
    
    //TODO:新增效验，理赔二核时不可申诉纠错
	var sqlUW = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstateUW=easyExecSql(sqlUW);
	if (rgtstateUW!=null){
	     if(rgtstateUW[0][0]=='16'){
	       alert("理赔二核中不可申诉纠错，必须等待二核结束！");
	       return false;
	     }
	}
    
    fm.all('operate').value = "APPEALINSERT||MAIN";
     fm.IdAppConf.disabled=true;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action ="./AppealSave.jsp";
   
    fm.submit();
    
  

  }
 
}


function afterAffix(ShortCountFlag,SupplyDateResult){
  var strsql = "select a.RgtState,b.codename,AffixGetDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
  +" and b.codetype='llrgtstate' and b.code=a.rgtstate"
  arrResult = easyExecSql(strsql);
  if ( arrResult ){
    fm.RgtState.value= arrResult[0][1];
    fm.AffixGetDate.value= arrResult[0][2];
  }

  fm.ModifyDate.value = getCurrentDate();
}

function SaveAffixDate(){
  if(fm.AffixGetDate.value!=""&&fm.AffixGetDate.value!=null){
    var strSQL="update llcase set affixgetdate='"+fm.AffixGetDate.value+"' where caseno='"+fm.CaseNo.value+"'";
    easyExecSql(strSQL);
  }
}

//ClientCaseQueryInput.js查询返回接口2005-2-21 13:09
function ClientafterQuery(RgtNo,CaseNo){
  //团体批次
  if ( RgtNo!=null && RgtNo.length>0){
    var arrResult = new Array();
    var strSQL="select "
    +"(select codename from ldcode where ldcode.codetype='llaskmode' and ldcode.code=llregister.RgtType),"
    +"AppAmnt,"
    +"(select codename from ldcode where ldcode.codetype='llreturnmode' and ldcode.code=llregister.ReturnMode),"
    +"RgtantName,"
    +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llregister.IDType),"
    +"IDNo,"
    +"(select codename from ldcode where ldcode.codetype='llrelation' and ldcode.code=llregister.relation),"
    +"RgtantPhone,RgtantAddress,PostCode,"
    +"(select codename from ldcode where ldcode.codetype='llgetmode' and ldcode.code=llregister.getmode), "
    +"IDType,bankcode,bankaccno,accname,RgtType,ReturnMode,relation,getmode, "
    +"(select bankname from ldbank where ldbank.bankcode=llregister.bankcode), "
    +"RgtantMobile,email "
    +"from LLRegister where rgtno='"+RgtNo+"'";
    arrResult = easyExecSql(strSQL);
    if( arrResult!=null){
      fm.all('RgtTypeName').value= arrResult[0][0];
      fm.all('AppAmnt').value= arrResult[0][1];
      fm.all('ReturnModeName').value= arrResult[0][2];
      fm.all('RgtantName').value= arrResult[0][3];
      fm.all('IDTypeName').value= arrResult[0][4];
      fm.all('IDNo').value= arrResult[0][5];
      fm.all('RelationName').value= arrResult[0][6];
      fm.all('RgtantPhone').value= arrResult[0][7];
      fm.all('RgtantAddress').value= arrResult[0][8];
      fm.all('PostCode').value= arrResult[0][9];
      fm.all('paymodeName').value= arrResult[0][10];
      fm.all('IDType').value=arrResult[0][11];
      fm.all('BankCode').value= arrResult[0][12];
      fm.all('BankAccNo').value= arrResult[0][13];
      fm.all('AccName').value=arrResult[0][14];
      fm.all('RgtType').value=arrResult[0][15];
      fm.all('ReturnMode').value=arrResult[0][16];
      fm.all('Relation').value=arrResult[0][17];
      fm.all('paymode').value = arrResult[0][18];
      fm.all('BankName').value = arrResult[0][19];
      fm.all('Mobile').value = arrResult[0][20];
      fm.all('Email').value = arrResult[0][21];
    }
    //个人案件
    if (CaseNo!=null && CaseNo.length>0){
      var strSQL="select customerno,customername,customersex,custbirthday,"
      +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llcase.IDType),"
      +"idno,idtype,otheridno,otheridtype,prepaidflag from llcase where caseno='"+CaseNo+"'";
      arrResult = easyExecSql(strSQL);
      if( arrResult != null ){
      	
        fm.CustomerNo.value   =arrResult[0][0];
        fm.CustomerName.value =arrResult[0][1];
        fm.Sex.value          =arrResult[0][2];
        fm.CBirthday.value    =arrResult[0][3];
        fm.tIDTypeName.value  =arrResult[0][4];
        fm.tIDNo.value        =arrResult[0][5];
        fm.tIDType.value = arrResult[0][6];
        fm.OtherIDNo.value = arrResult[0][7];
        fm.OtherIDType.value = arrResult[0][8];
        if(arrResult[0][9]=="1"){
          fm.PrePaidFlag.checked = "true";
        }
//        fm.CaseOrder.value = arrResult[0][9];
//        fm.CaseOrderName.value = arrResult[0][10];
     }
     
     strSQL="select a.caseno,"
           +"(select codename from ldcode where codetype='llrgtstate' and code=a.rgtstate),"
           +"coalesce((select realpay from llclaim where caseno=a.caseno),0),"
           +"(select codename from ldcode where codetype='llcasetype' and code=a.rgttype),"
           +"b.appealreason,b.appealrdesc "
           +"from llcase a,llappeal b where a.caseno=b.appealno "
           +"and b.caseno='"+CaseNo+"'";
      arrResult = easyExecSql(strSQL);
      if (arrResult){
      	displayMultiline(arrResult,AppealGrid);
      }
     

      strSQL="select AccStartDate,AccidentDate,DeathDate,AccidentSite,"
      +"AccdentDesc,AffixGetDate,"
      +"(select codename from ldcode where ldcode.codetype='llcuststatus' and ldcode.code=llcase.custstate),custstate"
      +" from llcase where caseno='"+CaseNo+"'";
      //	alert(strSQL);
      arrResult = easyExecSql(strSQL);
      if( arrResult != null ){
        fm.all('DeathDate').value= arrResult[0][2];
        fm.all('CustStatusName').value= arrResult[0][6];
        fm.all('CustStatus').value= arrResult[0][7];
        if ( arrResult[0][5]!=null || arrResult[0][5]!='null' ){
          fm.all('AffixGetDate').value =arrResult[0][5] ;
        }
      }
      strSQL = "select casegetmode,BankCode,BankAccNo,AccName, "+
      "(select b.bankname from ldbank b where b.bankcode=a.bankcode), "+
      "(select c.codename from ldcode c where c.codetype='llgetmode' and c.code=a.casegetmode), "+
      " (select RelaDrawerInsured from llcaseext ext where 1=1 and ext.caseno=a.caseno ), "+
      " (select codeName from LDcode where codetype='relation'  and code in (select RelaDrawerInsured from llcaseext ext where 1=1 and ext.caseno=a.caseno ) fetch first 1 rows only)  , "+
      " (select DrawerIDType from llcaseext ext where 1=1 and ext.caseno=a.caseno ), "+
      " (select codeName from LDcode where codetype='idtype'  and code in (select DrawerIDType from llcaseext ext where 1=1 and ext.caseno=a.caseno ) fetch first 1 rows only  ), "+
      " (select DrawerID from llcaseext ext where 1=1 and ext.caseno=a.caseno ) "+
      " from llcase a where a.caseno='"+fm.CaseNo.value+"'"
      brrResult = easyExecSql(strSQL);
      if(brrResult){
        fm.all('paymode').value=brrResult[0][0];
        fm.all('BankCode').value= brrResult[0][1];
        fm.all('BankAccNo').value= brrResult[0][2];
        fm.all('AccName').value=brrResult[0][3];
        fm.all('BankName').value=brrResult[0][4];
        fm.all('paymodeName').value = brrResult[0][5];
        fm.all('RelaDrawerInsured').value = brrResult[0][6];
        fm.all('RelaDrawerInsuredName').value = brrResult[0][7];
        fm.all('DrawerIDType').value = brrResult[0][8];
        fm.all('DrawerIDTypeName').value = brrResult[0][9];
        fm.all('DrawerID').value = brrResult[0][10];
      }
      showBank();
      strSQL="select ReasonCode,Reason from LLAppClaimReason where 1=1 "
      +"and rgtno='"+RgtNo+"' "
      +" and caseno='"+CaseNo+"'";
      arrResult = easyExecSql(strSQL);
      if ( arrResult!=null ){

        for (i=0;i< arrResult.length ;i++){

          //fm.all("appReasonCode"+ arrResult[i][0])
          var ind= eval(arrResult[i][0])-1;


          fm.appReasonCode[ind].checked = true;

        }
      }
      //申诉纠错页面添加申请日期
	  	strSQLz="select AppDate from llregister where RgtNo= '"+fm.CaseNo.value+"'";
		arrResultz=easyExecSql(strSQLz);
		if(arrResultz !=null){
			try{fm.all('AppDate').value=arrResultz[0][0]}catch(ex){alert(ex.message+"AppDate")}
		}
      //事件查询
      QueryEvent();
      fm.all("CaseNo").value = CaseNo;
      fm.all("RgtNo").value =  RgtNo;
      strSQL="select RgtState,Operator,ModifyDate,caseprop,codename from llcase,ldcode where caseno='"+CaseNo+"' and code=RgtState and codetype='llrgtstate'";
      arrResult = easyExecSql(strSQL);
      if(arrResult!=null){
        fm.all("Handler").value = arrResult[0][1];
        fm.all("ModifyDate").value = arrResult[0][2];
        fm.RgtState.value=arrResult[0][4];
        if(arrResult[0][3]=="06"){
          fm.SimpleCase.checked = "true";
        }
      }
      //strSQL = "select appealtype,case appealtype  when '1' then '纠错类' when '0' then '申诉类' end ,appeanrcode,appealreason,appealrdesc from LLAppeal where appealno ='"+fm.CaseNo.value+"'";
     // brrResult = easyExecSql(strSQL);
      //if(brrResult){
      //  fm.all('AppealType').value=brrResult[0][0];
     //   fm.all('AppealTypeName').value=brrResult[0][1];
       // fm.all('AppeanRCode').value= brrResult[0][2];
       // fm.all('AppealReason').value= brrResult[0][3];
       // fm.all('AppealRDesc').value= brrResult[0][4];

      //}
      showPRemark();
      showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出
    }
  }
}

function afterLLRegister(arr){
  if(arr){
    fm.CustomerNo.value  =arr[0][0];
    fm.CustomerName.value=arr[0][1];
    fm.Sex.value         =arr[0][2];
    fm.CBirthday.value   =arr[0][3];
    fm.tIDTypeName.value =arr[0][4];
    fm.tIDType.value     =arr[0][8];
    fm.OtherIDNo.value   =arr[0][9];
    fm.OtherIDType.value =arr[0][10];
    fm.tIDNo.value    =arr[0][5];
    fm.ReContNo.value =arr[0][6];
    fm.Sex.value      =arr[0][2];
    fm.CBirthday.value=arr[0][3];
    fm.ReContNo.value =arr[0][7];
    var addresscode   = arr[0][6];
    strSQL = "select b.PostalAddress,b.Phone from lcaddress b where b.AddressNo='" + addresscode +"'";
    arr = easyExecSql( strSQL, 1,0,1);
    if (arr){
      fm.PostalAddress.value=arr[0][0];
      fm.Phone.value        =arr[0][1];
    }
  }
  var  wherePart = getWherePart("a.InsuredNo", 'CustomerNo' )
  + getWherePart("a.name", 'CustomerName' )
  + getWherePart( 'a.ContNo','ContNo' );
  var strSQL ="select distinct a.Bankcode,a.Bankaccno,a.Accname "
  +"from lcinsured a where insuredno='"+fm.CustomerNo.value+"'"
  + wherePart+ " union select distinct a.Bankcode,a.Bankaccno,a.Accname "
  +"from lbinsured a where insuredno='"+fm.CustomerNo.value+"'"
  + wherePart;

  var drr= easyExecSql( strSQL,1,0,1 );
  if (drr&&drr[0][1]!=''){
    fm.BankCode.value=drr[0][0];
    fm.BankAccNo.value=drr[0][1];
    fm.AccName.value=drr[0][2];

  }
  QueryEvent();
  showPRemark();
}

//判断查询输入窗口的案件类型是否是回车，
//如果是回车调用查询客户函数
function QueryOnKeyDown(){
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13"){
    ClientQuery();
  }
}

//弹出窗口显示复选框选择受益人
function OpenBnf(){
  var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerName.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
  showInfo=OpenWindowNew(pathStr,"EventInput","middle");
}

//打印接口
function PrintPage() {
	var caseno = fm.CaseNo.value;
	if (fm.CaseNo.value == "") {
		alert("请先保存数据");
		return false;
	}
	// var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where
	// code = 'lp005' and otherno='"+caseno+"'");
	fm.payreceipt.disabled = true;
	// if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	// {

	// fm.action="./PayAppPrt.jsp?CaseNo="+caseno;
	// fm.submit();
	// }
	// else
	// {
	// window.open("../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq);
	// fm.action =
	// "../uw/PrintPDFSave.jsp?Code=0lp005&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+
	// caseno +"&PrtSeq="+PrtSeq;
	var showStr = "正在准备打印数据，请稍后...";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action = "../uw/PDFPrintSave.jsp?Code=lp005&OtherNo=" + caseno;
	fm.submit();
	// }
	// showInfo =
	// window.open("./PayAppPrt.jsp?CaseNo="+fm.CaseNo.value,"PrintPage",'width=600,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}
//function printPDF()
//{ 	
//    var caseno=fm.CaseNo.value;  //记录案件号
//	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp005' and otherno='"+caseno+"'");
//	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
//	{
//		alert("打印管理表中数据不存在");
//		return false;
//	}
//	else
//	{
//		//window.open("../uw/PrintPDFSave.jsp?Code=0lp005&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+caseno+"&PrtSeq="+PrtSeq);
//		fm.action = "../uw/PrintPDFSave.jsp?Code=0lp005&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ caseno +"&PrtSeq="+PrtSeq;
//		fm.submit();
//	}
//}

function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "llRelation" && Field.value=="00"){
    fm.RgtantName.value = fm.CustomerName.value;
    fm.IDTypeName.value=fm.tIDTypeName.value;
    fm.IDType.value = fm.tIDType.value;
    fm.IDNo  .value=fm.tIDNo.value;

    addSQL = "select b.PostalAddress,b.zipcode,b.phone,b.mobile,b.Email from lcinsured a,lcaddress b where b.AddressNo=a.AddressNo and b.CustomerNo=a.InsuredNo and a.InsuredNo='"+fm.CustomerNo.value+"'";
    crr = easyExecSql( addSQL , 1,0,1);
    if (crr){
      fm.RgtantAddress.value=crr[0][0];
      fm.PostCode.value =crr[0][1];
      fm.RgtantPhone.value = crr[0][2];
      fm.Mobile.value=crr[0][3];
      fm.Email.value = crr[0][4];
    }
  }
  if( cCodeName == "paymode"){
    if(Field.value=="1"||Field.value=="2"){
      divBankAcc.style.display='none';
      fm.BankAccM.disabled=true;
    }
    else{
      divBankAcc.style.display='';
      fm.BankAccM.disabled=false;
      var strSQL ="select distinct a.Bankcode,a.Bankaccno,a.Accname "
      +"from lcinsured a where InsuredNo='"+fm.CustomerNo.value+"'";
      +"union select distinct a.Bankcode,a.Bankaccno,a.Accname "
      +"from lbinsured a where insuredno='"+fm.CustomerNo.value+"'";
      var arr= easyExecSql( strSQL,1,0,1 );
      if (arr){
        fm.BankCode.value=arr[0][0];
        fm.BankAccNo.value=arr[0][1];
        fm.AccName.value=arr[0][2];
      }
    }
  }
  if(fm.paymode.value!="1" && fm.paymode.value!="2"){
	  if( cCodeName == "relation" ){ //领款人和被保险人关系
		  if(Field.value=="00"){//本人
			  fm.DrawerIDType.value = fm.tIDType.value;
			  fm.DrawerIDTypeName.value=fm.tIDTypeName.value;
			  fm.DrawerID.value = fm.tIDNo.value;
		  }
		}
	}
  
}

function RgtFinish(){
	fm.all('OpType').value="OK";
  strSQL = " select AppPeoples,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
  arrResult = easyExecSql(strSQL);
  if(arrResult){
    if ( arrResult[0][1]!='01'){
      alert("团体案件状态不允许该操作！");
      return;
    }
    var AppPeoples;
    AppPeoples = arrResult[0][0];
  }
  else{
    alert("团体案件信息查询失败！");
    return;
  }

  strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
  arrResult = easyExecSql(strSQL);
  if(arrResult){
    var RealPeoples;
    RealPeoples = arrResult[0][0];
  }
  strSQL = " select customername from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
  brr = easyExecSql(strSQL)
  if(brr){
    count = brr.length;
  }
  if ( RealPeoples < AppPeoples ){
    if(confirm("团体申请下个人申请尚未全部申请完毕！" + "团体申请人数：" + AppPeoples + "实际申请人数：" + RealPeoples+ "\n您确认要结束本团体批次吗?")){
      fm.realpeoples.value=RealPeoples;
      alert("本批次申请人数为："+RealPeoples+"人")
    }
    else{
      return;
    }
  }
  fm.realpeoples.value=RealPeoples;
  fm.all('operate').value = "UPDATE||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action = "./LLGrpRegisterSave.jsp";
  fm.submit();
}

function CaseChange(){
  initTitle();
	fm.Sex.value = fm.Sex.value=='女'?'1':'0';
  ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}


function shouCheck()
{	
	var rowNum=EventGrid.mulLineCount;
	var sqlsCheckSubrptNo="select subrptno  from llcaserela where CaseNo='"+fm.CaseNo.value+"' ";
	var CheckSubrptNo= easyExecSql(sqlsCheckSubrptNo);

    for(var i=0;i<rowNum;i++)
    {
    var value2=EventGrid.getRowColDataByName (i,"Subno");
  	 for(var j=0;j<CheckSubrptNo.length;j++){
  	   if (CheckSubrptNo[j][0]==value2){
  	      EventGrid.checkBoxSel(i+1);
  	    }
      }
    }
}

function QueryEvent(){
  initEventGrid();
  // 3500
  var strSQL="select SubRptNo,AccDate,(select codename from ldcode1 where codetype='province1' and code=accprovincecode), " +
	"(select codename from ldcode1 where codetype='city1' and code=acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=acccountycode fetch first 1 row only), " +
	"AccPlace,AccidentType,AccSubject,HospitalName,InHospitalDate,OutHospitalDate,AccDesc,"+
	"case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," +"AccidentType,accprovincecode,acccitycode,acccountycode from LLSubReport where CustomerNo='"
	+ fm.CustomerNo.value +"' order by AccDate desc";
  
    turnPage1.pageLineNum = 20;
	turnPage1.queryModal(strSQL,EventGrid);
	shouCheck();
}

/**
*申诉信息
**/
function OnAppeal(){
  IdAppeal.style.display='';
  fm.IdAppConf.value ="申诉保存";
  tAppealFlag="1";//申诉Flag状态改变
}

function showBank()
{
  if(fm.paymode.value=="1"||fm.paymode.value=="2"){
    divBankAcc.style.display='none';
    fm.BankAccM.disabled=true;
  }
  else{
    divBankAcc.style.display='';
    fm.BankAccM.disabled=false;
  }
}

function DealCancel(){
  var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value;
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function showPRemark(){
  var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
  +fm.CustomerNo.value+"'";
  var	crrResult1 = easyExecSql(strSQLx);
  if(crrResult1){
    alert("客户："+crrResult1[0][1]+"为黑名单客户，您确定要继续吗？");

  }
  var grpResult = new Array();
  var perResult = new Array();
  var arrResult = new Array();
  var polResult = new Array();
  var temp="";
  var str1SQL="select grpcontno from lccont where "+
  " insuredno='"+fm.CustomerNo.value+"' "+
  " and grpcontno in (select grpcontno from lcgrpcont where appflag='1')"+
  " and appflag='1'";
  grpResult=easyExecSql(str1SQL);
  if(grpResult != null&&grpResult != ''){
    for ( i = 0; i <= grpResult.length ;i++){
      var strSQL="select '保单'||grpcontno||'约定:'||remark from lcgrpcont "+
      "where grpcontno='"+grpResult[i]+"'";
      arrResult=easyExecSql(strSQL);

      var polSQL="Select '保障计划'||CONTPLANCODE||'下险种'||RISKCODE||coalesce((select codename from ldcode where codetype='CalTitle' and code=c.calfactor),'')||'约定:'||remark FROM LCCONTPLANDUTYPARAM c "+
      " where grpcontno='"+grpResult[i]+"' AND CONTPLANCODE!='11' and remark !='' "+
      "GROUP BY CONTPLANCODE,RISKCODE,remark,calfactor";
      polResult=easyExecSql(polSQL);
      if(arrResult != null&&arrResult != ''){
        try{
          temp += arrResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
      if(polResult !=null&&polResult != ''){
        try{
          temp += polResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
    }
  }
  var str2SQL="select contno from lccont where "+
  " insuredno='"+fm.CustomerNo.value+"' "+
  " and appflag='1'" ;
  perResult=easyExecSql(str2SQL);
  if(perResult != null&&perResult != ''){
    for ( i = 0; i <= perResult.length ;i++){
      var str3SQL="select '保单'||contno||'下险种'||(select riskcode from lcpol p where p.polno=c.polno)||'约定:'||speccontent from lcpolspecrela c "+
      " where contno ='"+perResult[i]+"'";
      arrResult=easyExecSql(str3SQL);
       var lptbInfoSql="select Impart from lptbinfo  where contno='"+perResult[i]+"'";
       var lptbInfoResult=easyExecSql(lptbInfoSql);
      if(arrResult != null&&arrResult != ''){
        try{
          temp +=	arrResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
      else{
      }
        if(lptbInfoResult != null&&lptbInfoResult != ''){
        try{
         temp +=	lptbInfoResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
      else{
      }
      
      
      
    }
  }
  fm.all('ContRemark').value=temp.replace(/\,/g,"\n");
  if(temp==""){
    fm.all('ContRemark').value="无特别约定，请依照条款处理";
  }
}

function ContQuery(){
  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  pathStr="./FrameMainContQuery.jsp?Interface=LLContQueryInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"LLContQueryInput","middle",800,330);
}

function SendMail(){
	showInfo.close();
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "邮件发送成功！" ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
}
function SendMsg()
{
	showInfo.close();
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送成功！" ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
}


function xxx()
{
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	//执行下一步操作(发送email、短信)
  	var ExeSql = "select returnmode,rgtantmobile,email,rgtantname from llregister where rgtno='"+fm.RgtNo.value+"'";
  	arr=easyExecSql(ExeSql);
  	if(arr)
  	{
  	  //fm.AnswerMode.value=arr[0][0];
  	  fm.Mobile.value=arr[0][1];
  	  fm.Email.value=arr[0][2];
  	  
  	  fm.SMSContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	                      +"发信人：中国人保健康"
  	  fm.EmailContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	                      +"发信人：中国人保健康"
  	  //alert(fm.Mobile.value);
  	  //
  	  //alert(fm.Email.value);
  	  fm.action="./GrpSendMail.jsp";
  	  //alert();
  	  fm.submit();
  	  //alert();
  	}  
	}
	
function openIssue()
{

	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&CustomerNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	varSrc += "&Operator=" + fm.Handler.value;
	//alert(varSrc);
	pathStr="./FrameIssue.jsp?Interface=LLIssueInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLIssueInput","middle",850,500);

}

function checkID1 (vIDNO) {
    if(vIDNO==""||vIDNO==null||fm.tIDType.value==""||fm.tIDType.value==null){
    alert("证件号码和证件类型不能为空，请确认是否录入,申请失败！");
    return false;
    }
    if(fm.CustomerNo.value!=""&&fm.CustomerNo.value!=null)
	{  
		var strSql = "select idtype,idno from lcinsured where insuredno='"+fm.CustomerNo.value+"' and idno = '"+vIDNO + "' and idtype = '"+fm.tIDType.value +"' union all select idtype,idno from lbinsured where insuredno='"+fm.CustomerNo.value +"' and idno = '" +vIDNO +  "' and idtype = '"+fm.tIDType.value +"' with ur";
        var arrResult = easyExecSql(strSql);
    if (arrResult){
    
    } else {
    	 alert("录入的证件号码【"+vIDNO+"】或者证件类型【"+fm.tIDType.value+"】有误,申请失败！");
         return false;
    }
  }
  return true;
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	fm.payreceipt.disabled=false;
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

