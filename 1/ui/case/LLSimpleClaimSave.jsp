<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLSimpleClaimSave.jsp
//程序功能：
//创建日期：2006-05-23 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%

String strOperate = request.getParameter("operate");
System.out.println("==== strOperate == " + strOperate);

//	LLAppClaimReasonSet tLLAppClaimReasonSet   = new LLAppClaimReasonSet();
LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
LLCaseSchema tLLCaseSchema = new LLCaseSchema();
LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
//声明VData
VData tVData = new VData();
//声明后台传送对象
SimpleClaimUI tSimpleClaimUI   = new SimpleClaimUI();
//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

tLLCaseSchema.setRgtType("1");
tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));

LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
tLLAppClaimReasonSchema.setReasonCode("02");        //原因代码
tLLAppClaimReasonSchema.setReason("住院");                //申请原因
tLLAppClaimReasonSchema.setCustomerNo(request.getParameter("CustomerNo"));
tLLAppClaimReasonSchema.setReasonType("0");

tLLCaseSchema.setRgtState("03");//案件状态
tLLCaseSchema.setCustomerName(request.getParameter("CustomerName"));
tLLCaseSchema.setCustomerNo(request.getParameter("CustomerNo"));
tLLCaseSchema.setDeathDate(request.getParameter("DeathDate"));
tLLCaseSchema.setCustBirthday(request.getParameter("CBirthday"));
tLLCaseSchema.setCustomerSex(request.getParameter("Sex"));
tLLCaseSchema.setIDType("0");
tLLCaseSchema.setIDNo(request.getParameter("IDNo"));
tLLCaseSchema.setOtherIDType("5");
tLLCaseSchema.setOtherIDNo(request.getParameter("SecurityNo"));
tLLCaseSchema.setCustState(request.getParameter("CustStatus"));
tLLCaseSchema.setGrpName(request.getParameter("GrpName"));
tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
tLLCaseSchema.setSurveyFlag("0");
tLLCaseSchema.setAccdentDesc(request.getParameter("PerRemark"));

tLLFeeMainSchema.setRgtNo(request.getParameter("RgtNo"));
tLLFeeMainSchema.setCaseNo(request.getParameter("CaseNo"));
tLLFeeMainSchema.setCustomerNo(request.getParameter("CustomerNo"));
tLLFeeMainSchema.setCustomerSex(request.getParameter("Sex"));
tLLFeeMainSchema.setHospitalCode(request.getParameter("HospitalCode"));
tLLFeeMainSchema.setHospitalName(request.getParameter("HospitalName"));
tLLFeeMainSchema.setHosAtti(request.getParameter("HosSecNo"));
tLLFeeMainSchema.setReceiptNo("53000");
tLLFeeMainSchema.setFeeType(request.getParameter("FeeType"));
tLLFeeMainSchema.setFeeAtti("1");
tLLFeeMainSchema.setFeeDate(request.getParameter("FeeDate"));
tLLFeeMainSchema.setHospStartDate(request.getParameter("HospStartDate"));
tLLFeeMainSchema.setHospEndDate(request.getParameter("HospEndDate"));
tLLFeeMainSchema.setRealHospDate(request.getParameter("RealHospDate"));
tLLFeeMainSchema.setInHosNo(request.getParameter("inpatientNo"));
tLLFeeMainSchema.setSecurityNo(request.getParameter("SecurityNo"));
tLLFeeMainSchema.setInsuredStat(request.getParameter("InsuredStat"));

String tsFee[] = request.getParameterValues("FeeGrid1");	        //费用金额
String tsSelfAmnt[]= request.getParameterValues("FeeGrid2");			//自费金额
String tsSelfPay2[]= request.getParameterValues("FeeGrid3");
String tsGetLimit[]= request.getParameterValues("FeeGrid4");
String tsMidSpan[] = request.getParameterValues("FeeGrid5");
String tsPlanFee[] = request.getParameterValues("FeeGrid6");			  //先行给付金额
String tsPaySpan[] = request.getParameterValues("FeeGrid7");		//不合理费用
String tsSupInHosFee[] = request.getParameterValues("FeeGrid8");
String tsHighAmnt[] = request.getParameterValues("FeeGrid9");
String tsOfficialSubsidy[] = request.getParameterValues("FeeGrid10");
  tLLSecurityReceiptSchema.setApplyAmnt(tsFee[0]);
  tLLSecurityReceiptSchema.setSelfAmnt(tsSelfAmnt[0]);
  tLLSecurityReceiptSchema.setSelfPay2(tsSelfPay2[0]);
  tLLSecurityReceiptSchema.setGetLimit(tsGetLimit[0]);
  tLLSecurityReceiptSchema.setMidAmnt(tsMidSpan[0]);
  tLLSecurityReceiptSchema.setPlanFee(tsPlanFee[0]);
  tLLSecurityReceiptSchema.setStandbyAmnt(tsPaySpan[0]);
  tLLSecurityReceiptSchema.setSupInHosFee(tsSupInHosFee[0]);
  tLLSecurityReceiptSchema.setHighAmnt1(tsHighAmnt[0]);
  tLLSecurityReceiptSchema.setHighAmnt2(tsSupInHosFee[0]);
  tLLSecurityReceiptSchema.setOfficialSubsidy(tsOfficialSubsidy[0]);
double tsumMoney = tLLSecurityReceiptSchema.getSelfAmnt()+tLLSecurityReceiptSchema.getSelfPay2()
                 +tLLSecurityReceiptSchema.getGetLimit()+tLLSecurityReceiptSchema.getMidAmnt()
                 +tLLSecurityReceiptSchema.getPlanFee()+tLLSecurityReceiptSchema.getStandbyAmnt()
                 +tLLSecurityReceiptSchema.getSupInHosFee()+tLLSecurityReceiptSchema.getHighAmnt1()
                 +tLLSecurityReceiptSchema.getOfficialSubsidy();
System.out.println(tLLSecurityReceiptSchema.getApplyAmnt());
System.out.println(tsumMoney);
System.out.println(Math.abs(tLLSecurityReceiptSchema.getApplyAmnt()-tsumMoney));
if(Math.abs(tLLSecurityReceiptSchema.getApplyAmnt()-tsumMoney)>0.01){
  Content = " 保存失败，原因是:各费用分项之和不等于费用总额！";
  FlagStr = "Fail";
}
System.out.println(FlagStr);
if(!FlagStr.equals("Fail")){
  String tDiseaseName = ""+request.getParameter("ICDName");
  String tDiseaseCode = ""+request.getParameter("ICDCode");
  if((!tDiseaseCode.equals("")&&!tDiseaseCode.equals("null"))
  ||(!tDiseaseName.equals("")&&!tDiseaseName.equals("null"))){
    LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
    tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
    tLLCaseCureSchema.setDiseaseName(tDiseaseName);
    tLLCaseCureSet.add(tLLCaseCureSchema);
  }
  
  //保存案件时效
  tLLCaseOpTimeSchema.setRgtState("01");
  tLLCaseOpTimeSchema.setStartDate(request.getParameter("OpStartDate"));
  tLLCaseOpTimeSchema.setStartTime(request.getParameter("OpStartTime"));

  try
  {
    // 准备传输数据 VData
    System.out.println("<--Star Submit VData-->");
    tVData.add(tLLAppClaimReasonSchema);
    tVData.add(tLLCaseSchema);
    tVData.add(tLLRegisterSchema);
    tVData.add(tLLFeeMainSchema);
    tVData.add(tLLSecurityReceiptSchema);
    tVData.add(tLLCaseCureSet);
    tVData.add(tLLCaseOpTimeSchema);

    tVData.add(tG);
    System.out.println("<--Into tSimpleClaimUI-->");
    tSimpleClaimUI.submitData(tVData,strOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是: " + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if ("".equals(FlagStr))
  {
    tError = tSimpleClaimUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Success";
      tVData.clear();
      tVData=tSimpleClaimUI.getResult();
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
//添加各种预处理
LLCaseSchema mLLCaseSchema = new LLCaseSchema();

mLLCaseSchema.setSchema(( LLCaseSchema )tVData.getObjectByObjectName( "LLCaseSchema", 0 ));
System.out.println("结束了");
%>
<script language="javascript">
parent.fraInterface.fm.all("CaseNo").value = "<%=mLLCaseSchema.getCaseNo()%>";
parent.fraInterface.fm.all("RgtNo").value = "<%=mLLCaseSchema.getRgtNo()%>";
parent.fraInterface.fm.all("Handler").value = "<%=mLLCaseSchema.getOperator()%>";
parent.fraInterface.fm.all("ModifyDate").value = "<%=mLLCaseSchema.getModifyDate()%>";
</script>
<%
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
