<%
//程序名称：LLReportInput.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
	LLSurveySchema mLLSurveySchema = new LLSurveySchema();
  LLSurveySchema rLLSurveySchema = new LLSurveySchema();//接受传递的数据24//
	SurveyOperateUI mSurveyOperateUI = new SurveyOperateUI();

	GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");


  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = request.getParameter("fmtransact");
	System.out.println("您现在的操作是"+transact);

  if (transact.equals("ACCEPT"))
  {

  	mLLSurveySchema.setClmCaseNo(request.getParameter("ClmCaseNo"));
		mLLSurveySchema.setType(request.getParameter("Type"));
		mLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));
	}
	else
	{
		mLLSurveySchema.setRgtNo(request.getParameter("RgtNo"));
		mLLSurveySchema.setCustomerNo(request.getParameter("CustomerNo"));
    mLLSurveySchema.setCustomerName(request.getParameter("CustomerName"));
    mLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));
    mLLSurveySchema.setClmCaseNo(request.getParameter("ClmCaseNo"));
    mLLSurveySchema.setType(request.getParameter("Type"));
    mLLSurveySchema.setSurveyor(request.getParameter("Surveyor"));
    mLLSurveySchema.setSurveySite(request.getParameter("SurveySite"));
    mLLSurveySchema.setSurveyStartDate(request.getParameter("SurveyStartDate"));
    mLLSurveySchema.setSurveyEndDate(request.getParameter("SurveyEndDate"));
		mLLSurveySchema.setContent(request.getParameter("Content"));
		mLLSurveySchema.setresult(request.getParameter("Result"));
	}

	VData tVData = new VData();
  try
  {
   tVData.addElement(mLLSurveySchema);
   tVData.addElement(tG);
   mSurveyOperateUI.submitData(tVData,transact);

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="")
  {
    tError = mSurveyOperateUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 操作成功";
    	FlagStr = "Succ";
    	tVData.clear();
    	tVData=mSurveyOperateUI.getResult();
    	rLLSurveySchema=(LLSurveySchema)tVData.getObjectByObjectName("LLSurveySchema",0);
    }

    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.fm.all("Surveyor").value = "<%=rLLSurveySchema.getSurveyor()%>";
  parent.fraInterface.fm.all("SurveyEndDate").value = "<%=rLLSurveySchema.getSurveyEndDate()%>";
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>