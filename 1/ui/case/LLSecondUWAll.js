//程序名称：ManuUWAll.js
//程序功能：个人人工核保
//创建日期：2005-01-24 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约人工核保的EasyQuery
 *  描述:查询显示对象是合同保单.显示条件:合同未进行人工核保，或状态为待核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL ="";
	
	
	strSQL = "select distinct LLCUWBatch.caseno,LLCUWBatch.BatNo,LLCUWBatch.InsuredNo,LDPerson.Name,LDPerson.sex, LDPerson.birthday from LLCUWBatch,ldperson "
	        + " where LDPerson.customerno = LLCUWBatch.insuredno and LLCUWBatch.state='0'"
	      
			
  turnPage.queryModal(strSQL, PolGrid);

}


/*********************************************************************
 *  执行新契约人工核保的EasyQueryAddClick
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryAddClick()
{
	var tSel = PolGrid.getSelNo();
	var tCaseNo = PolGrid.getRowColData(tSel - 1,1); 
	var tBatNo = PolGrid.getRowColData(tSel - 1,2); 	
	var tInsuredNo = PolGrid.getRowColData(tSel - 1,2)  
	
		window.location="./LLSecondUWALLInputMain.jsp?CaseNo="+tCaseNo+"&BatNo="+tBatNo+"&InsuredNo="+tInsuredNo; 
	
}


/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  

  if (FlagStr == "Fail" )
  {                 
    alert(content);
  }
  else
  { 
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");     
    //执行下一步操作
  }
  easyQueryClick();
 

}

