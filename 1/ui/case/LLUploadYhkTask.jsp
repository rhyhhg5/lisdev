<%
//Name：LLClaimInput.jsp
//Function：上海医保卡上报页面	
//Date：2016-11-25
//Author：李云凤
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<head >
		<SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="./CaseCommon.js"></SCRIPT>
		<SCRIPT src="LLUploadYhkTask.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	</head>

<body  onload="initForm();" >
<!--立案分案信息部分（列表） -->
<form action="./LLUploadYhkTaskSave.jsp" method=post name=fm target="fraSubmit">
	
	<div id=divconfirm align='left'>
		<TR>
		 <TD  class= title>业务号：</TD>
		 <TD  class= input>
		 	<input type="text" class = "common" name="pNumber" elementtype=nacessary>
		 </TD>
		 <br/><br/>
		 <TD  class= title>终止日期:</TD>
         <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name="EndDate" >
         </TD>
         <TD  class= title >（医保卡个单个案抽档时必填，其他情况为空）</TD>
		</TR>

		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="银行卡缴费" name="SubCalculate1" onclick="submitForm()" id = "sub1" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="核保信息上传" name="SubCalculate2" onclick="submitForm2()" id = "sub2" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="承保信息上传" name="SubCalculate3" onclick="submitForm3()" id = "sub3" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="保费信息上传" name="SubCalculate4" onclick="submitForm4()" id = "sub4" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="平台自核上传" name="SubCalculate5" onclick="submitForm5()" id = "sub5" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="保全信息上传" name="SubCalculate6" onclick="submitForm6()" id = "sub6" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="续保退保信息上传" name="SubCalculate7" onclick="submitForm7()" id = "sub7" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="理赔信息上传" name="SubCalculate11" onclick="submitForm11()" id = "sub11" />
		<br/><br/>
		<input class=CssButton style='width: 110px;' type=button value="理赔注销上传" name="SubCalculate13" onclick="submitForm13()" id = "sub13" />
		<br/><br/>
		<input class=CssButton style='width: 130px;' type=button value="医保卡个单个案抽档" name="SubCalculate15" onclick="submitForm15()" id = "sub15" />
		<br/><br/><br/>
		<input class=CssButton style='width: 130px;' type=button value="保全批处理测试" name="SubCalculate8" onclick="submitForm8()" id = "sub8" />
		<br/><br/>
		<input class=CssButton style='width: 130px;' type=button value="保单解锁批处理测试" name="SubCalculate9" onclick="submitForm9()" id = "sub9" />
		<br/><br/>
		<input class=CssButton style='width: 130px;' type=button value="保单再发送批处理测试" name="SubCalculate10" onclick="submitForm10()" id = "sub10" />
		<br/><br/>
		<input class=CssButton style='width: 130px;' type=button value="邮件提醒功能测试" name="SubCalculate12" onclick="submitForm12()" id = "sub12" />
		<br/><br/>
		<input class=CssButton style='width: 130px;' type=button value="人工核保系统强撤功能" name="SubCalculate14" onclick="submitForm14()" id = "sub14" />
	</div>   
	
	<Input type=hidden name="Opt">
	<Input type=hidden name="LoadFlag" id="LoadFlag" value="2">
	<input name ="LoadC" type="hidden">
	<input name ="LoadD" type="hidden">
	<input type=hidden name="ShowCaseRemarkFlag">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
