var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var tSaveType="";

//结案未结案查询sql
function classify(){
	
  var startDate = fm.RgtDateS.value;
  var endDate = fm.RgtDateE.value;
  if (startDate==""||startDate==null||endDate==""||endDate==null){
  	alert("请输入受理日期");
  	return false;
  }
  if(dateDiff(startDate,endDate,"M")>3){
      alert("统计期最多为三个月！");
      return false;
  }
	var StateRadio="";
	//第一组的单选框
	for(i = 0; i <fm.StateRadio.length; i++){
		if(fm.StateRadio[i].checked){
			StateRadio=fm.StateRadio[i].value;
			break;
		}
	}

	//给付结论类型
	var decision = "";
	for(i=1;i<=fm.llclaimdecision.length;i++){
    if(fm.llclaimdecision[i-1].checked){
      if (decision =="")
        decision="'" + fm.llclaimdecision[i-1].value +"'" ;
     	else
        decision +=",'" +fm.llclaimdecision[i-1].value +"'";
    }
  }

   	//案件类型
  var caseTypeRadio="";
	for(i = 0; i <fm.typeRadio.length; i++){
		if(fm.typeRadio[i].checked){
			caseTypeRadio=fm.typeRadio[i].value;
			break;
		}
	}
	var appCase = "";
	var consultCase = "";
	if ( caseTypeRadio=="1" || caseTypeRadio=="4" || caseTypeRadio=="5" )
	{
	//申请,申诉,错误处理
	  appCase = caseTypeRadio ;
	}else
	{
		//咨询通知类案件
		consultCase = caseTypeRadio;
	}
  var OrganCodeLength = fm.OrganCode.value.length;
  
  //集团统一工号
  var tGroupAgentCode=fm.GroupAgentCode.value;
  if(tGroupAgentCode != ""){
  	var strSQL="select agentcode from laagent where groupagentcode='"+tGroupAgentCode+"'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
    	fm.AgentCode.value = arrResult[0][0];
	}
  	
  }
	//业务员, 管理机构
	var AgentPart ="";
	if ( fm.AgentCode.value!="")
	{
		AgentPart =" and g.agentcode=f.agentcode and f.insuredno=a.CustomerNo"
						  + getWherePart("g.managecom","OrganCode","like") 
	    	 			 + getWherePart("g.agentcode","AgentCode");
	}
	else{
		AgentPart =" and g.agentcode=f.agentcode and f.insuredno=a.CustomerNo"
						  + getWherePart("g.managecom","OrganCode","like") ;
	}

	//起止时间
  var dpart=getWherePart("a.rgtdate","RgtDateS",">=") + getWherePart("a.rgtdate","RgtDateE","<=");

/////////查询sql////////////


	//未结案sql
	var sqlNotEndCase="";
	var anow = fm.RgtDateE.value;
	sqlNotEndCase="select distinct g.name,getUniteCode(g.agentcode),a.CaseNo,a.CustomerName,a.rgtstate,a.CustomerNo,"
	   		 +" a.rgtdate,a.endcasedate,"+
	   		 "(select e.codename from ldcode e where e.codetype='llcasetype' and e.code=a.rgttype),"
	   		 +"a.operator,"+
	   		 "(select b.codename from ldcode b where b.codetype='llrgtstate' and b.code=a.rgtstate),"
	   		 +"'' ";
	sqlNotEndCase+=",to_char(to_date('" + anow +"')-to_date(a.rgtdate)+1) diffdate ";
	sqlNotEndCase+=" from llcase a,laagent g,lcpol f ";
	sqlNotEndCase+="where EndCaseDate is null";
	if ( appCase!='') sqlNotEndCase =sqlNotEndCase+" and rgttype='" + appCase +"'" ; //案件类型
	//sqlNotEndCase+= AgentPart ; //代理人的客户
	sqlNotEndCase += AgentPart +dpart;

   //结案sql
	var	sqlEndCase ="select distinct g.name,getUniteCode(g.agentcode),a.CaseNo,a.CustomerName,a.rgtstate,a.CustomerNo,"
	   		 +" a.rgtdate,a.endcasedate,"+
	   		 "(select e.codename from ldcode e where e.codetype='llcasetype' and e.code=a.rgttype),"
	   		 +"a.operator,"+
	   		 "(select b.codename from ldcode b where b.codetype='llrgtstate' and b.code=a.rgtstate),"
         +"c.givetypedesc ";
	  	sqlEndCase+=",to_char(a.endcasedate-a.rgtdate+1) diffdate ";
	sqlEndCase+=" from llcase a, llclaim c, laagent g,lcpol f ";
	sqlEndCase+=" where a.EndCaseDate is not null";
	if ( appCase!='') sqlEndCase =sqlEndCase+" and rgttype='" + appCase +"'" ; //案件类型
	//sqlEndCase+= AgentPart ; //代理人的客户
  if ( decision!="") sqlEndCase=sqlEndCase+" and c.givetype in (" + decision +")"; //案件状态
  sqlEndCase+="and c.caseno=a.caseno ";
  sqlEndCase += AgentPart+dpart;

  //咨询通知类
  var sqlConsult = "";
  if ( consultCase!="" )
  {
  	if ( consultCase =="0" )
  	{
  	  sqlConsult ="select distinct g.name,getUniteCode(g.agentcode),a.ConsultNo,a.CustomerName,'',a.CustomerNo,b.makedate,f.AnswerDate,'咨询类',b.operator,'已回复','','1'"
	            +" from LLConsult a, LLMainAsk b,LLAnswerinfo f,laagent g,lcpol f where 1=1"
	            +" and b.AskType='0' "
	            + AgentPart  //代理人的客户
	            +" and b.logno=a.logno and a.logno = f.logno"
//	            +" and d.codetype='llreplystate' and d.code=a.ReplyState";
	   }
	   else{
		   sqlConsult ="select distinct g.name,getUniteCode(g.agentcode),a.ConsultNo,a.CustomerName,'',a.CustomerNo,b.makedate,b.makedate,'通知类','','','','1'"
	    		  +" from LLConsult a, LLMainAsk b, laagent g,lcpol f where 1=1"
	      		+" and b.AskType='1' "
	 	    	  + AgentPart  //代理人的客户
	       		+" and b.logno=a.logno "
  	}
  }

  /////////////要执行的sql////////////////
   var strSQL ="";
   if ( consultCase!="")
   {
      		strSQL = sqlConsult;
   }else{
     switch(StateRadio){
	   	case "0":  //所有'案件
	   	    strSQL = sqlNotEndCase +" union "+ sqlEndCase +"  AND g.agentcode = COALESCE(g.agentcode, g.agentcode) order by caseno";
	   	    break;
	   	case "1":  //未结案
           	strSQL = sqlNotEndCase +"  AND g.agentcode = COALESCE(g.agentcode, g.agentcode) order by caseno";
           	break;
	   	case "2":	//已结案
           	strSQL = sqlEndCase +"  AND g.agentcode = COALESCE(g.agentcode, g.agentcode) order by caseno";
           	break;
     
     }
   }


   if ( strSQL =="" )
     strSQL = sqlNotEndCase +" union "+ sqlEndCase +"  AND g.agentcode = COALESCE(g.agentcode, g.agentcode)";

   //处理时效---多加一天
 	 var idate = 0;
 	 var iidate = '';
    	fm.SQL.value=strSQL;
 	 turnPage.queryModal(strSQL, CheckGrid);

}

function queryMainAsk(){
  var caseTypeRadio="";
	for(i = 0; i <fm.typeRadio.length; i++){
		if(fm.typeRadio[i].checked){
			caseTypeRadio=fm.typeRadio[i].value;
			break;
		}
	}
  
}

//拼codeSelect中使用的字符串，作为查询条件
function getstr()
{
  str="1 and code like #"+fm.OrganCode.value+"%#";
}

//判断理赔人权限，下拉选项为自己或下级
function getRights(){
	var strSQL="select claimpopedom from llclaimuser where usercode='"+fm.Operator.value+"'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
    var Rights = arrResult[0][0];
	}
 	Str="1 and claimpopedom < #"+Rights+"#";
}

//回车相应函数
function QueryOnKeyDown()
{
 	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
		classify();
}

