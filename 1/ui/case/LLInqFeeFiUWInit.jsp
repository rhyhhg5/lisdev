<%
//Name:LLInqFeeFiUWInit.jsp
//function：
//author:Xx
//Date:2006-09-13
%>
<!--用户校验类-->
<script language="JavaScript">
  function initInpBox(){
    try{
      fm.FeeType.value='1';
      fm.FeeTypeName.value='直接';
      fm.UWType.value='1';
      fm.UWTypeName.value='未审核';
    }
    catch(ex){
      alter("LLInqFeeFiUWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("LLInqFeeFiUWInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm(){
    try{
      initInpBox();
      initSurveyGrid();
      initInqFeeDetailGrid();
      initAddFeeGrid();
      initSurveyCaseGrid();
    }
    catch(re){
      alter("LLInqFeeFiUWInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }

  function initSurveyGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("理赔号","50px","30","0");
      iArray[2]=new Array("调查序号","25px","30","0");
      iArray[3]=new Array("调查员代码","25px","10","0");
      iArray[4]=new Array("调查员姓名","30px","10","0");
      iArray[5]=new Array("主管代码","25px","10","0");
      iArray[6]=new Array("主管姓名","30px","10","0");
      iArray[7]=new Array("机构编码","30px","0","0");
      iArray[8]=new Array("审核状态","30px","10","0");
      iArray[9]=new Array("调查号","0px","12","3");

      SurveyGrid = new MulLineEnter("fm","SurveyGrid");
      SurveyGrid.mulLineCount = 2;
      SurveyGrid.displayTitle = 1;
      SurveyGrid.locked = 1;
      // SurveyerGrid.canChk =1	;
      SurveyGrid.canSel =1	;
      SurveyGrid.hiddenPlus=1;
      SurveyGrid.hiddenSubtraction=1;
      SurveyGrid.loadMulLine(iArray);
      SurveyGrid.selBoxEventFuncName = "ShowFeeDetail";
    }
    catch(ex){
      alter(ex);
    }
  }

  // 直接调查费用信息列表的初始化
  function initInqFeeDetailGrid()
  {
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");

      iArray[1]=new Array("费用项目","160px","10","0");
      iArray[2]=new Array("费用项目代码","30px","10","3");
      iArray[3]=new Array("金额","70px","10","0");
      iArray[4]=new Array("调查机构","70px","10","0");
      iArray[5]=new Array("审核状态","80px","10","0");
      iArray[6] = new Array("调查号","20px","50","3");

      InqFeeDetailGrid = new MulLineEnter( "fm" , "InqFeeDetailGrid" );
      InqFeeDetailGrid.mulLineCount = 3;
      InqFeeDetailGrid.displayTitle = 1;
      InqFeeDetailGrid.locked = 0;
      InqFeeDetailGrid.canSel = 0;
      InqFeeDetailGrid.canChk = 1;
      InqFeeDetailGrid.hiddenPlus=1;
      InqFeeDetailGrid.hiddenSubtraction=1;
      InqFeeDetailGrid.addEventFuncName="initAttr";
      InqFeeDetailGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert(ex);
    }
  }
  
  function initAddFeeGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");

      iArray[1]=new Array("费用项目","40px","40","0");
      iArray[2]=new Array("费用项目代码","0px","40","3");
      iArray[3]=new Array("金额","20px","100","0");
      iArray[4]=new Array("审核状态","22px","40","0");

      AddFeeGrid = new MulLineEnter( "fm" , "AddFeeGrid" );
      //这些属性必须在loadMulLine前
      AddFeeGrid.mulLineCount = 3;
      AddFeeGrid.displayTitle = 1;
      AddFeeGrid.locked = 0;
      AddFeeGrid.canSel = 0;
      AddFeeGrid.canChk = 1;
      AddFeeGrid.hiddenPlus=1;
      AddFeeGrid.hiddenSubtraction=1;
      AddFeeGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert(ex);
    }
  }

  function initSurveyCaseGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("案件号","30px","100","0");
      iArray[2]=new Array("提调次数","20px","0","0");
      iArray[3]=new Array("均摊费用","20px","0","0");
      SurveyCaseGrid = new MulLineEnter("fm","SurveyCaseGrid");
      SurveyCaseGrid.mulLineCount =3;
      SurveyCaseGrid.displayTitle = 1;
      SurveyCaseGrid.locked = 1;
      SurveyCaseGrid.canSel =0;
      SurveyCaseGrid.canChk =0;
      SurveyCaseGrid.hiddenPlus=1;
      SurveyCaseGrid.hiddenSubtraction=1;
      SurveyCaseGrid.loadMulLine(iArray);
      // SurveyCaseGrid.selBoxEventFuncName = "onSelSelected";
    }
    catch(ex){
      alter(ex);
    }
  }
</script>