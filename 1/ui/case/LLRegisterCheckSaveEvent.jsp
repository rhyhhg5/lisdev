<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLRegisterCheckSaveEvent.jsp
//程序功能：检录时保存事件，从前台传数
//创建日期：2005-02-18 08:49:52
//创建人  ：Xx
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数
//声明Schema Set

String strOperate = request.getParameter("operate");

LLSubReportSet tLLSubReporSet = new LLSubReportSet();
//声明VData
VData tVData = new VData();
//声明后台传送对象
EventSaveBL tEventSaveBL   = new EventSaveBL();
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

String CaseNo = request.getParameter("CaseNo");
String CustomerNo = request.getParameter("CustomerNo");
String CustomerName = request.getParameter("CustomerName");
//事件信息
String eventno[] = request.getParameterValues("InsuredEventGrid1");
String accdate[] = request.getParameterValues("InsuredEventGrid2");
String accProvinceCode[]=request.getParameterValues("InsuredEventGrid13");  //#3500 发生地点省
String accCityCode[] =request.getParameterValues("InsuredEventGrid14");     //发生地点市
String accCountyCode[] =request.getParameterValues("InsuredEventGrid15");   //发生地点县
String accplace[] = request.getParameterValues("InsuredEventGrid6");  //发生地点
String accindate[] = request.getParameterValues("InsuredEventGrid7"); //入院日期
String accoutdate[] = request.getParameterValues("InsuredEventGrid8");  //出院日期
String accdesc[] = request.getParameterValues("InsuredEventGrid9");     //事件信息
String acctype[] = request.getParameterValues("InsuredEventGrid11");     //事件类型

String AllEvent = ""+request.getParameter("AllEvent");
String tRNum[] = null;
if(AllEvent.equals("on"))
	tRNum=request.getParameterValues("InpInsuredEventGridChk");
if (eventno != null){
	for (int i = 0; i < eventno.length; i++)	{
			System.out.println("<-eventno[i]-->"+ eventno[i]);
		if(AllEvent.equals("on")){
			if ( !"1".equals( tRNum[i]) && eventno[i]!=null&& !eventno[i].equals("")){
				continue;
			}
			System.out.println("tRNum[i]:::::::::"+tRNum[i]);
		}
		LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
		tLLSubReportSchema.setSubRptNo(eventno[i]);
		tLLSubReportSchema.setCustomerNo(CustomerNo);
		tLLSubReportSchema.setCustomerName(CustomerName);
		tLLSubReportSchema.setAccProvinceCode(accProvinceCode[i]); // #3500 
		tLLSubReportSchema.setAccCityCode(accCityCode[i]);			 
		tLLSubReportSchema.setAccCountyCode(accCountyCode[i]);
		System.out.println("发生地点省:"+accProvinceCode[i]+","+"发生地点市："+accCityCode[i]+"发生地点县："+accCountyCode[i]+"。");   
		tLLSubReportSchema.setAccDate(accdate[i]);
		tLLSubReportSchema.setAccDesc(accdesc[i]);
		tLLSubReportSchema.setAccPlace( accplace[i]);
		tLLSubReportSchema.setInHospitalDate( accindate[i]);
		tLLSubReportSchema.setOutHospitalDate( accoutdate[i]);
		tLLSubReportSchema.setAccidentType(acctype[i]);
		System.out.println("事件类型:"+acctype[i]);

		tLLSubReporSet.add(tLLSubReportSchema);
	}
}
/**************************************************/
TransferData CN = new TransferData();
CN.setNameAndValue("CaseNo",CaseNo);

try
{
	// 准备传输数据 VData
	//VData传送
	System.out.println("<--Star Submit VData-->");
	tVData.add( tLLSubReporSet );
	tVData.add(CN);
	tVData.add(tG);
	System.out.println("<--Into tEventSaveBL-->");
	tEventSaveBL.submitData(tVData,"INSERT");
}
catch(Exception ex)
{
	Content = "保存失败，原因是: " + ex.toString();
	FlagStr = "Fail";
}

//如果在Catch中发现异常，则不从错误类中提取错误信息
if ("".equals(FlagStr))
{
	tError = tEventSaveBL.mErrors;
	if (!tError.needDealError())
	{
		Content = " 保存成功! ";
		FlagStr = "Success";
		tVData.clear();
		tVData=tEventSaveBL.getResult();
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}

%>
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
parent.fraInterface.queryEventInfo();
</script>
</html>


