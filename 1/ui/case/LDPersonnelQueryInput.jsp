<%
//程序名称：LDPersonnelInput.jsp
//程序功能：功能描述
//创建日期：2005-04-13 23:19:03
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LDPersonnelQueryInput.js"></SCRIPT> 
  <%@include file="LDPersonnelQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLDPersonnelGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      用户编码
    </TD>
    <TD  class= input>
			<Input class= "codeno"  name=UserCode ondblclick="return showCodeList('UserCode',[this, Name], [0, 1]);" onkeyup="return showCodeListKey('UserCode', [this, Name], [0, 1]);" ><Input class= 'codename' name=Name >
    </TD>
    <TD  class= title>
      员工号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerNo >
    </TD>
    <TD  class= title>
      所属机构
    </TD>
    <TD  class= input>
			<Input class="codeno" name=ManageCom verify="机构代码|notnull&code:ComCode" onclick="return showCodeList('ComCode',[this, ManageComName], [0, 1]);" onkeyup="return showCodeListKey('ComCode', [this, ManageComName], [0, 1]);"><Input class=codename name= ManageComName elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      员工性别
    </TD>
    <TD  class= input><Input class= 'codeno' name=Sex CodeData="0|2^0|男^1|女" onclick="return showCodeListEx('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKeyEx('Sex',[this,SexName],[0,1]);"><input class=codename  name=SexName>
    <TD  class= title>
      业务职级
    </TD>
    <TD  class= input>
			<Input class="codeno" name=OccupationGrade onClick="showCodeList('llclaimpopedom',[this,OccupationGradeName],[0,1]);"  onkeyup="showCodeListKey('llclaimpopedom',[this,OccupationGradeName],[0,1]);" ><Input class="codename" name=OccupationGradeName readonly>
    </TD>
    <TD  class= title>
      证件号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDNo >
    </TD>
  </TR>
</table>
  </Div>
  <Div id=invisible style="display:'none'">
  <TR  class= common>
    <TD  class= title>
      单位名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpName >
    </TD>
    <TD  class= title>
      单位编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpNo >
    </TD>
    <TD  class= title>
      入司日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=JoinCompanyDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
    <TD  class= title>
      出生日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Birthday >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      参加工作日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StartWorkDate >
    </TD>
    <TD  class= title>
      职位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Position >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      工资
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Salary >
    </TD>
    <TD  class= title>
      职业类别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      职业代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationCode >
    </TD>
    <TD  class= title>
      职业（工种）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WorkType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      兼职（工种）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PluralityType >
    </TD>
    <TD  class= title>
      死亡日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DeathDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否吸烟标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SmokeFlag >
    </TD>
    <TD  class= title>
      黑名单标记
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BlacklistFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDType >
    </TD>
    <TD  class= title>
      学历专业
    </TD>
    <TD  class= input>
      <Input class= 'common' name=speciality >
    </TD>
    <TD  class= title>
      属性
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Proterty >
    </TD>
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=State >
    </TD>
    <TD  class= title>
      VIP值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=VIPValue >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      密码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Password >
    </TD>
    <TD  class= title>
      国籍
    </TD>
    <TD  class= input>
      <Input class= 'common' name=NativePlace >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      民族
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Nationality >
    </TD>
    <TD  class= title>
      户口所在地
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RgtAddress >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      婚姻状况
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Marriage >
    </TD>
    <TD  class= title>
      结婚日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MarriageDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      健康状况
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Health >
    </TD>
    <TD  class= title>
      身高
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Stature >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      体重
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Avoirdupois >
    </TD>
    <TD  class= title>
      学历
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Degree >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      信用等级
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CreditGrade >
    </TD>
    <TD  class= title>
      其它证件类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OthIDType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      其它证件号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OthIDNo >
    </TD>
    <TD  class= title>
      ic卡号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICNo >
    </TD>
  </TR>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   class=cssButton TYPE=button   class=common onclick="easyQueryClick();">
          <INPUT VALUE="返回" class=cssButton TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonnel1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLDPersonnel1" align=center style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLDPersonnelGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT class=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT class=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT class=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT class=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
