<%
//Name:ReportInit.jsp
//function：
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">

function initForm()
{

  try
  {
   
    
    initLLLockGrid(1);
    initLockRecordGrid();
    fm.ManageCom.value = "<%=tGI.ManageCom%>";
    SearchLLAppClaimReason();
    
  }
  catch(re)
  {
    alter("在GrpContInvaliedate.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initLLLockGrid(allevent)
{

   var iArray = new Array();
   try
   {
	iArray[0]=new Array("序号","30px","10","0");
	iArray[1]=new Array("项目名称","80px","0",0);
	
    iArray[2]=new Array("团体保单号","50px","0",0);
    iArray[3]=new Array("批次号","50px","0",0);
    iArray[4]=new Array("本次赔款","30px","0",0);
    iArray[5]=new Array("当前状态","30px","0",0);
    iArray[6]=new Array("锁定/解锁日期","30px","0",0);
    
    LLLockGrid = new MulLineEnter("fm","LLLockGrid");
    LLLockGrid.mulLineCount = 1;
    LLLockGrid.displayTitle = 1;
    LLLockGrid.locked = 0;
    LLLockGrid.canChk =allevent;
    LLLockGrid.canSel =1;
    LLLockGrid.hiddenPlus=0;
    LLLockGrid.hiddenSubtraction=1;
    LLLockGrid.selBoxEventFuncName = "SelectLLLockGrid";
    LLLockGrid.loadMulLine(iArray);
      
  } 
  catch(ex)
  {
    alert(ex);
  }    
}
function initLockRecordGrid()
{

   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("团体保单号","30px","0",0);
    iArray[2]=new Array("批次号","30px","0",0);
    iArray[3]=new Array("执行操作","30px","0",0);
    iArray[4]=new Array("加锁/解锁日期","30px","0",0);
    iArray[5]=new Array("操作人员","10px","0",0);
    
    LockRecordGrid = new MulLineEnter( "fm" , "LockRecordGrid" );
    LockRecordGrid.mulLineCount =7;
    LockRecordGrid.displayTitle = 1;
    LockRecordGrid.locked = 1;
    LockRecordGrid.canChk =0;
    LockRecordGrid.hiddenPlus=1;  
    LockRecordGrid.hiddenSubtraction=1; 
    LockRecordGrid.loadMulLine(iArray);
  } 
  catch(ex)
  {
    alert(ex);
  }    
}

 </script>