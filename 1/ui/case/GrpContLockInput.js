
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var tSaveFlag = "0";
var mDebug="1";
var mAction = "";
var tSaveType="";

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function SelectLLLockGrid() {
	var selNo = LLLockGrid.getSelNo();
	if(selNo<0) {
		return;
	}
	var grpContNo = LLLockGrid.getRowColData(selNo - 1, 2);
	//alert("grpContNo=" + grpContNo);
	var rgtNo = LLLockGrid.getRowColData(selNo - 1, 3);
	//alert("rgtno = " + rgtNo);
	var strSQL = "select grpcontno,rgtno," +
				 " (case state when '1' then '锁定' when '2' then '解锁' end),lockdate,operator " +
				 " from lllockrecord where 1=1 " + 
				 " and grpcontno = '" + grpContNo +"'" + 
				 " and rgtno = '" + rgtNo + "'" + 
				 " with ur";
	turnPage.queryModal(strSQL,LockRecordGrid);
}
function SearchLLLock(){
  var grpContNo=fm.grpContNo.value;
  //alert(grpContNo);
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句 
  var strSql = "select projectname,Grpcontno,rgtno,onepayfee," +
  		       " (case state when '1' then '已锁定' when '2' then '已解锁' end),lockdate " +
  		       " from lllock where 1=1 " + 
               getWherePart("grpcontno","grpContNo") + 
               getWherePart("projectno","projectNo") + 
               getWherePart("state","stateno") + 
               " with ur";
   turnPage.queryModal(strSql,LLLockGrid);
  showInfo.close();

}
function updateLLLock()
{
   var rowsNo = LLLockGrid.mulLineCount;
   if ( rowsNo>0){
   	var checkRc = false;
   	var kk=0;
   	for(kk=0;kk<rowsNo;kk++){
   	 if(LLLockGrid.getChkNo(kk)){
   	   //alert("保单状态=" + LLLockGrid.getRowColData(kk,5));
   	   if(LLLockGrid.getRowColData(kk,5)=="已解锁") {
   		   alert("保单已解锁，不能重复解锁");
   		   return;
   	   }
   	   if(LLLockGrid.getRowColData(kk,5)==""||LLLockGrid.getRowColData(kk,5)==null || LLLockGrid.getRowColData(kk,5)=="null") {
		   alert("保单数据不能为空");
		   return;
	   }
   	   checkRc=true;
   	 }
	}
	  	
    if(!checkRc){
      alert("请选择一个保单！");
      return false;
    }
  } else {
     alert("没有保单数据");
     return false;
  }
    
    fm.all('operate').value = 'update';
    var showStr="正在修改数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 	fm.action="./LockUpdateSave.jsp";
	fm.submit();
	


}

function afterSubmit(FlagStr, content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
  
}

