<%
/*******************************************************************************
 * Name     :LLRegisterCheckSaveAll.jsp
 * Function :事故/伤残/医疗信息的保存程序
 * Authorm  :wujs
 * Date     :2005-2-21 20:40
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
 	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page contentType="text/html;charset=GBK" %>
<%
  
  /**
  transact
    账单关联保存
  CHECKFINISH 审核完成
  **/
  //CaseCheckUI tCaseCheckUI   = new CaseCheckUI();
  AllCaseInfoUI tAllCaseInfoUI = new AllCaseInfoUI();
  
  CErrors tError = null;
  //String transact = request.getParameter("fmtransact");
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "INSERT";
  String strCaseNo=request.getParameter("CaseNo");
  String CustomerNo = request.getParameter("CustomerNo");
  String CaseRelaNo = request.getParameter("CaseRelaNo");
  String RgtNo = request.getParameter("RgtNo");
  System.out.println(strCaseNo +"aaa"+CaseRelaNo+"bbbb"+RgtNo);
  LLCaseSchema tLLCaseSchema  = new LLCaseSchema();
  tLLCaseSchema.setCaseNo(strCaseNo);
  tLLCaseSchema.setRgtNo(RgtNo);
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI"); 
  
  VData tVData = new VData();
	tVData.addElement(tG);
  
 /////////////////////////////////////////////////////////////////////////////////////
 	LLCaseInfoSet tLLCaseInfoSet=new LLCaseInfoSet();
  LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
  LLOperationSet tLLOperationSet = new LLOperationSet();
  LLOtherFactorSet tLLOtherFactorSet = new LLOtherFactorSet();
  LLAccidentSet tLLAccidentSet = new LLAccidentSet();
  LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
  LLSubReportSet tLLSubReportSet = new LLSubReportSet();
  LLDisabilitySet tLLDisabilitySet = new LLDisabilitySet();
  mLLCaseInfoSchema.setCaseNo(request.getParameter("CaseNo"));
  
  //事件信息
	String tRNum[] = request.getParameterValues("InpInsuredEventGridSel");
	String eventno[] = request.getParameterValues("InsuredEventGrid1");
	String accdate[] = request.getParameterValues("InsuredEventGrid2");
	String accProvinceCode[]=request.getParameterValues("InsuredEventGrid13");  //#3500 发生地点省
	String accCityCode[]=request.getParameterValues("InsuredEventGrid14");       // 发生地点市
	String accCountyCode[]=request.getParameterValues("InsuredEventGrid15");     //发生地点县
	String accplace[] = request.getParameterValues("InsuredEventGrid6");  //发生地点
	String accindate[] = request.getParameterValues("InsuredEventGrid7");  //入院日期
	String accoutdate[] = request.getParameterValues("InsuredEventGrid8");  //出院日期
	String accdesc[] = request.getParameterValues("InsuredEventGrid9");    //事件类型
	
	System.out.println("<--submit mulline-->");
	
	if (tRNum != null) 
	{
		System.out.println("<-tRNum-->"+ tRNum.length);
		for (int i = 0; i < tRNum.length; i++)	
		{   
			if ( "1".equals( tRNum[i]) || eventno[i]==null || eventno[i].equals(""))
			{
				System.out.println("<-eventno[i]-->"+ eventno[i]+"pppp");
				LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
				 tLLSubReportSchema.setSubRptNo(eventno[i]);
			    tLLSubReportSchema.setCustomerNo(request.getParameter("CustomerNo"));
			    tLLSubReportSchema.setCustomerName(request.getParameter("CustomerName"));      
			    tLLSubReportSchema.setAccDate(accdate[i]);
			    tLLSubReportSchema.setAccDesc(accdesc[i]);   
			    tLLSubReportSchema.setAccPlace( accplace[i]);  
			    tLLSubReportSchema.setInHospitalDate( accindate[i]);
			    tLLSubReportSchema.setOutHospitalDate( accoutdate[i]); 
			    //tLLSubReportSchema.setCustSituation(request.getParameter("CustSituation1"));
			    //tLLSubReportSchema.setAccSubject(request.getParameter("AccSubject1"));
			   	// tLLSubReportSchema.setAccidentType(request.getParameter("AccidentType1"));
			   	// tLLSubReportSchema.setAccEndDate(request.getParameter("AccEndDate1"));
			   	// tLLSubReportSchema.setHospitalCode(request.getParameter("HospitalCode1"));
			    //tLLSubReportSchema.setHospitalName(request.getParameter("HospitalName1"));
			    //tLLSubReportSchema.setOutHospitalDate( accoutdate[0]); 
			   	// tLLSubReportSchema.setSeriousGrade(request.getParameter("SeriousGrade1"));
			   	// tLLCaseRelaSchema.setCaseNo(request.getParameter("CaseNo"));
			   	tLLSubReportSchema.setAccProvinceCode(accProvinceCode[i]);  // 3500 发生地点省
			   	tLLSubReportSchema.setAccCityCode(accCityCode[i]); // 发生地点市
			   	tLLSubReportSchema.setAccCountyCode(accCountyCode[i]);//发生地点县
				System.out.println("发生地点省:"+accProvinceCode[i]+","+"发生地点市："+accCityCode[i]+"发生地点县："+accCountyCode[i]+"。"); 
				System.out.println("事件信息："+accdesc[i]);
			   	tLLSubReportSet.add(tLLSubReportSchema);
			}
		}
	}
 
    //疾病信息
  { 
  	 
	  String[] Dianose=request.getParameterValues("DiseaseGrid1");
	  String[] DiseaseName=request.getParameterValues("DiseaseGrid3");
	  String[] DiseaseCode=request.getParameterValues("DiseaseGrid4");
	  String[] LDCode= request.getParameterValues("DiseaseGrid7");
	  String[] LDName= request.getParameterValues("DiseaseGrid6");
	  String[] RiskLevel=request.getParameterValues("DiseaseGrid8");
	  String[] RiskContinueFlag=request.getParameterValues("DiseaseGrid9");
	  String[] HospitalCode=request.getParameterValues("DiseaseGrid10");
	  String[] HospitalName=request.getParameterValues("DiseaseGrid11");
	  String[] DoctorName= request.getParameterValues("DiseaseGrid12");
	  String[] DoctorNo= request.getParameterValues("DiseaseGrid14");
	  String[] IsOperation = request.getParameterValues("DiseaseGrid16");
	 
	 	int intLength =0;
    if (DiseaseName!=null )
	 	{ 
	   	 intLength = DiseaseName.length ;		
			 for(int i=0;i<intLength;i++) {
      	 if ( DiseaseName[i].length()<=0)
         {
           System.out.println(">>>>>>>>>>>>");
           continue ;
         }
		 			LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
		 			
		 			tLLCaseCureSchema.setCaseNo(strCaseNo);
		 			tLLCaseCureSchema.setCaseRelaNo(CaseRelaNo);
		 			tLLCaseCureSchema.setDiseaseName(DiseaseName[i]);
		 			tLLCaseCureSchema.setDiseaseCode(DiseaseCode[i]);
		 			tLLCaseCureSchema.setLDNAME(LDName[i]);//tmm
		 			tLLCaseCureSchema.setLDCODE(LDCode[i]);//tmm	 			
		 			tLLCaseCureSchema.setRiskLevel(RiskLevel[i]);
		 			tLLCaseCureSchema.setRiskContinueFlag(RiskContinueFlag[i]);
		 			tLLCaseCureSchema.setDiagnose(Dianose[i]);
		 			tLLCaseCureSchema.setHospitalCode(HospitalCode[i]);
		 			tLLCaseCureSchema.setHospitalName(HospitalName[i]);
		 			tLLCaseCureSchema.setSeriousFlag("0"); //一般疾病
		 			tLLCaseCureSchema.setDoctorName( DoctorName[i] );
		 			tLLCaseCureSchema.setDoctorNo( DoctorNo[i] );
		 			tLLCaseCureSchema.setOperationFlag(IsOperation[i]);
		 			tLLCaseCureSet.add(tLLCaseCureSchema);
		  }
	  }
	}
	 
	  //重疾疾病信息
	 {
	
 		  String[] DiseaseName=request.getParameterValues("SeriousDiseaseGrid1");
 		  String[] DiseaseCode=request.getParameterValues("SeriousDiseaseGrid2");
 		  String[] HospitalCode=request.getParameterValues("SeriousDiseaseGrid4");
 		  String[] HospitalName=request.getParameterValues("SeriousDiseaseGrid3");
 		  String[] DoctorName= request.getParameterValues("SeriousDiseaseGrid5");
 		  String[] DiagnoseDesc = request.getParameterValues("SeriousDiseaseGrid6");
 		  String[] SeriousWard= request.getParameterValues("SeriousDiseaseGrid7");
 		  String[] JudgeDepend = request.getParameterValues("SeriousDiseaseGrid9");
 		  String[] SeriousDutyFlag = request.getParameterValues("SeriousDiseaseGrid8");
 		  String[] DiagnoseDate = request.getParameterValues("SeriousDiseaseGrid11");
 		  
 		  int intLength =0;
      if (DiseaseName!=null )
	 		{ 
	   		 intLength = DiseaseName.length ;		
		 		 for(int i=0;i<intLength;i++) {
         
          	if ( DiseaseName[i].length()<=0)
          	{
          	  System.out.println("<<<<<<<<1:"+CaseRelaNo);
          	 continue ;
          	} 
		 				LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
		 				tLLCaseCureSchema.setCaseNo(strCaseNo);
		 				tLLCaseCureSchema.setCaseRelaNo(CaseRelaNo);
		 				tLLCaseCureSchema.setDiseaseName(DiseaseName[i]);
		 				tLLCaseCureSchema.setDiseaseCode(DiseaseCode[i]);
		 				tLLCaseCureSchema.setHospitalCode(HospitalCode[i]);
		 				tLLCaseCureSchema.setHospitalName(HospitalName[i]);
		 				tLLCaseCureSchema.setDoctorName(DoctorName[i]);
		 				tLLCaseCureSchema.setSeriousFlag("1"); //重疾疾病
		 				tLLCaseCureSchema.setJudgeDepend(JudgeDepend[i]);
		 				tLLCaseCureSchema.setDiagnoseDesc(DiagnoseDesc[i]);
		 				tLLCaseCureSchema.setSeriousDutyFlag(SeriousDutyFlag[i]);
		 				tLLCaseCureSchema.setDiagnoseDate(DiagnoseDate[i]);
		 				//tLLCaseCureSchema.setDianose(Dianose[i]);
		 				//tLLCaseCureSchema.setSeriousWard(SeriousWard[i]); //一般疾病
		 				
		 				if ( i==0 ) 
		 					tLLCaseCureSchema.setMainDiseaseFlag("1");//主要疾病
 	 				  else
		 				  	tLLCaseCureSchema.setMainDiseaseFlag("0");//主要疾病
		 				  	
		 				tLLCaseCureSet.add(tLLCaseCureSchema);
		  		}
	 		}   
  	}
	  //轻症疾病信息
	 {
	
		  String[] DiseaseName=request.getParameterValues("MildGrid1");
		  String[] DiseaseCode=request.getParameterValues("MildGrid2");
		  String[] HospitalCode=request.getParameterValues("MildGrid4");
		  String[] HospitalName=request.getParameterValues("MildGrid3");
		  String[] DoctorName= request.getParameterValues("MildGrid5");
		  String[] DiagnoseDesc = request.getParameterValues("MildGrid6");
		  String[] SeriousWard= request.getParameterValues("MildGrid7");
		  String[] JudgeDepend = request.getParameterValues("MildGrid9");
		  String[] SeriousDutyFlag = request.getParameterValues("MildGrid8");
		  String[] DiagnoseDate = request.getParameterValues("MildGrid11");
		  
		  int intLength =0;
     if (DiseaseName!=null )
	 		{ 
	   		 intLength = DiseaseName.length ;		
		 		 for(int i=0;i<intLength;i++) {
        
         	if ( DiseaseName[i].length()<=0)
         	{
         	  System.out.println("<<<<<<<<1:"+CaseRelaNo);
         	 continue ;
         	} 
		 				LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
		 				tLLCaseCureSchema.setCaseNo(strCaseNo);
		 				tLLCaseCureSchema.setCaseRelaNo(CaseRelaNo);
		 				tLLCaseCureSchema.setDiseaseName(DiseaseName[i]);
		 				tLLCaseCureSchema.setDiseaseCode(DiseaseCode[i]);
		 				tLLCaseCureSchema.setHospitalCode(HospitalCode[i]);
		 				tLLCaseCureSchema.setHospitalName(HospitalName[i]);
		 				tLLCaseCureSchema.setDoctorName(DoctorName[i]);
		 				tLLCaseCureSchema.setSeriousFlag("6"); //轻症疾病
		 				tLLCaseCureSchema.setJudgeDepend(JudgeDepend[i]);
		 				tLLCaseCureSchema.setDiagnoseDesc(DiagnoseDesc[i]);
		 				tLLCaseCureSchema.setSeriousDutyFlag(SeriousDutyFlag[i]);
		 				tLLCaseCureSchema.setDiagnoseDate(DiagnoseDate[i]);
		 				//tLLCaseCureSchema.setDianose(Dianose[i]);
		 				//tLLCaseCureSchema.setSeriousWard(SeriousWard[i]); //一般疾病
		 				
		 				if ( i==0 ) 
		 					tLLCaseCureSchema.setMainDiseaseFlag("1");//主要疾病
	 				  else
		 				  	tLLCaseCureSchema.setMainDiseaseFlag("0");//主要疾病
		 				  	
		 				tLLCaseCureSet.add(tLLCaseCureSchema);
		  		}
	 		}   
 	}	  
	  
  
   //残疾信息
{
	  String[] Name=request.getParameterValues("DeformityGrid1");
	  String[] Code=request.getParameterValues("DeformityGrid2");
	  String[] DeformityGrade=request.getParameterValues("DeformityGrid3");
	  String[] Payrate=request.getParameterValues("DeformityGrid4");
	  String[] JudgeDate=request.getParameterValues("DeformityGrid7");
	  String[] JudgeOrganName=request.getParameterValues("DeformityGrid9");
	  String[] JudgeOrgan=request.getParameterValues("DeformityGrid8");

	int intLength =0;
	if (Code!=null )
	{
		intLength = Code.length ;
		for(int i=0;i<intLength;i++) {
			LLCaseInfoSchema tLLCaseInfoSchema = new LLCaseInfoSchema();

			if ( Code[i].length()<=0)
			{
				System.out.println("<<<<<<<<2");
				continue ;
			}

		 tLLCaseInfoSchema.setCaseNo(strCaseNo);
		 tLLCaseInfoSchema.setCaseRelaNo(CaseRelaNo);
		 tLLCaseInfoSchema.setCode(Code[i]);
		 tLLCaseInfoSchema.setType(Code[i].substring(0,1));
		 tLLCaseInfoSchema.setName(Name[i]);
		 //tLLCaseInfoSchema.setDianose(Dianose[i]);
		 tLLCaseInfoSchema.setDeformityGrade(DeformityGrade[i]);
		 tLLCaseInfoSchema.setDeformityRate(Payrate[i]);
		 tLLCaseInfoSchema.setJudgeDate(JudgeDate[i]);
		 tLLCaseInfoSchema.setJudgeOrganName(JudgeOrganName[i]);
		 tLLCaseInfoSchema.setJudgeOrgan(JudgeOrgan[i]);
		 tLLCaseInfoSet.add(tLLCaseInfoSchema);
		System.out.println(strCaseNo+"&&&"+CaseRelaNo+"&&&"+Code[i]+"&&&"+Name[i]+"&&&"+DeformityGrade[i]+"&&&"+Payrate[i]+"&&&"+JudgeDate[i]+"&&&"+JudgeOrganName[i]+"&&&"+JudgeOrgan[i]+"&&&");
		}
	}
}

//伤残	add by Houyd #1777 新伤残评定标准及代码系统定义
{
	  String[] Name=request.getParameterValues("InvalidismGrid1");
	  String[] Code=request.getParameterValues("InvalidismGrid3");
	  String[] InvalidismGrade=request.getParameterValues("InvalidismGrid4");
	  String[] Payrate=request.getParameterValues("InvalidismGrid5");
	  String[] JudgeDate=request.getParameterValues("InvalidismGrid7");
	  String[] JudgeOrganName=request.getParameterValues("InvalidismGrid9");
	  String[] JudgeOrgan=request.getParameterValues("InvalidismGrid8");

	int intLength =0;
	if (Code!=null )
	{
		intLength = Code.length ;
		for(int i=0;i<intLength;i++) {
			LLCaseInfoSchema tLLCaseInfoSchema = new LLCaseInfoSchema();

			if ( Code[i].length()<=0)
			{
				System.out.println("伤残代码错误");
				continue ;
			}

		 tLLCaseInfoSchema.setCaseNo(strCaseNo);
		 tLLCaseInfoSchema.setCaseRelaNo(CaseRelaNo);
		 tLLCaseInfoSchema.setCode(Code[i]);
		 System.out.println("Code[i]:"+Code[i]);
		 tLLCaseInfoSchema.setType("3");//伤残信息，暂时存储在llcaseinfo中，以type 3来区分
		 tLLCaseInfoSchema.setName(Name[i]);
		 //tLLCaseInfoSchema.setDianose(Dianose[i]);
		 tLLCaseInfoSchema.setDeformityGrade(InvalidismGrade[i]);
		 tLLCaseInfoSchema.setDeformityRate(Payrate[i]);
		 tLLCaseInfoSchema.setJudgeDate(JudgeDate[i]);
		 tLLCaseInfoSchema.setJudgeOrganName(JudgeOrganName[i]);
		 tLLCaseInfoSchema.setJudgeOrgan(JudgeOrgan[i]);
		 tLLCaseInfoSet.add(tLLCaseInfoSchema);
		 System.out.println(strCaseNo+"&&"+CaseRelaNo+"&&"+Code[i]+"&&"+Name[i]+"&&"+InvalidismGrade[i]+"&&"+Payrate[i]+"&&"+JudgeDate[i]+"&&"+JudgeOrganName[i]+"&&"+JudgeOrgan[i]+"&&");
		}
	}
}

  //手术信息
	 {
		  String[] OperationCode=request.getParameterValues("DegreeOperationGrid2");
		  String[] OperationName=request.getParameterValues("DegreeOperationGrid1");
		  String[] OpLevel=request.getParameterValues("DegreeOperationGrid4");

		  int intLength =0;
      if (OperationCode!=null )
		  { 
	   		 intLength = OperationCode.length ;		
		 		 for(int i=0;i<intLength;i++) 
		 		 {
		 				if ( OperationCode[i].length()<=0)
        		{
         			 System.out.println("<<<<<<<<3");
         		   continue ;
         		}
         		LLOperationSchema tLLOperationSchema = new LLOperationSchema();
		 
			      tLLOperationSchema.setCaseNo(strCaseNo);
			      tLLOperationSchema.setCaseRelaNo(CaseRelaNo);
			      tLLOperationSchema.setOperationCode(OperationCode[i]);
			      tLLOperationSchema.setOperationName(OperationName[i]);
			      tLLOperationSchema.setOpLevel(OpLevel[i]);
		
		 				tLLOperationSet.add(tLLOperationSchema);
				}
	 		}   
  }
 
  //其他录入要素
	{
	  String[] num=request.getParameterValues("OtherFactorGridNo");
	  String[] FactorType=request.getParameterValues("OtherFactorGrid6");
	  String[] FactorCode=request.getParameterValues("OtherFactorGrid2");
	  String[] FactorName=request.getParameterValues("OtherFactorGrid3");
	  String[] Value=request.getParameterValues("OtherFactorGrid4");
	  String[] Remark=request.getParameterValues("OtherFactorGrid5");
	 
	  int intLength =0;
    if (FactorType!=null )
	  { 
	   	 intLength = FactorType.length ;		
		 	 for(int i=0;i<intLength;i++) 
		 	 {
		 			if ( FactorType[i].length()<=0)
        	{
         		 System.out.println("<<<<<<<<4");
         		 continue ;
         	}
         	LLOtherFactorSchema tLLOtherFactorSchema = new LLOtherFactorSchema();
		 
		 			tLLOtherFactorSchema.setFactorType(FactorType[i]);
		 			tLLOtherFactorSchema.setFactorCode(FactorCode[i]);
		 			tLLOtherFactorSchema.setCaseNo(request.getParameter("CaseNo"));
		 			tLLOtherFactorSchema.setCaseRelaNo(CaseRelaNo);
		 			tLLOtherFactorSchema.setSubRptNo("");
		 			tLLOtherFactorSchema.setValue(Value[i]);
		 			tLLOtherFactorSchema.setFactorName(FactorName[i]);
		 			tLLOtherFactorSchema.setRemark(Remark[i]);
	   			
		 			tLLOtherFactorSet.add(tLLOtherFactorSchema);
			 }
	 	}   
  }
  
  //意外信息
	{
	  String[] num=request.getParameterValues("AccidentGridNo");
	  String[] AccidentNo=request.getParameterValues("AccidentGrid1");
	  String[] Type=request.getParameterValues("AccidentGrid2");
	  String[] TypeName=request.getParameterValues("AccidentGrid3");
	  String[] Code=request.getParameterValues("AccidentGrid6");
	  String[] Name=request.getParameterValues("AccidentGrid5");
	  String[] ReasonCode=request.getParameterValues("AccidentGrid7");
	  String[] Reason=request.getParameterValues("AccidentGrid8");
	  String[] Remark=request.getParameterValues("AccidentGrid9");
	
	  int intLength =0;
    if (AccidentNo!=null )
	  { 
	  	 intLength = AccidentNo.length ;		
		 	 for(int i=0;i<intLength;i++) 
		 	 {
		 	 		if ( Name[i].length()<=0)
        	{
         		 System.out.println("<<<<<<<<5");
         		 continue ;
         	}
         	LLAccidentSchema tLLAccidentSchema = new LLAccidentSchema();
		 
					//tLLAccidentSchema.setAccidentNo("3"); 	//以后添加
					tLLAccidentSchema.setType(Type[i]);
					tLLAccidentSchema.setCaseNo(request.getParameter("CaseNo"));
					tLLAccidentSchema.setCaseRelaNo(CaseRelaNo);
					tLLAccidentSchema.setTypeName(TypeName[i]);
					tLLAccidentSchema.setCode(Code[i]);
					tLLAccidentSchema.setName(Name[i]);
					tLLAccidentSchema.setReasonCode(ReasonCode[i]);
					tLLAccidentSchema.setReason(Reason[i]);
					tLLAccidentSchema.setRemark(Remark[i]);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
					tLLAccidentSet.add(tLLAccidentSchema);
			 }
	 	}   
  }
  
  //失能信息
	{
	  String[] DisabilityNum=request.getParameterValues("DisabilityGridNo");
	  String[] DisabilityFlag=request.getParameterValues("DisabilityGrid2");
	  String[] DisabilityDate=request.getParameterValues("DisabilityGrid3");
	  String[] ObservationDate=request.getParameterValues("DisabilityGrid4");
	  
	
	  int intLength =0;
    if (DisabilityFlag!=null )
	  { 
	     intLength = DisabilityNum.length ;		
		 	 for(int i=0;i<intLength;i++) 
		 	 {
		 	 		if ( DisabilityFlag[i].length()<=0)
        	{
         		 System.out.println("<<<<<<<<6");
         		 continue ;
         	}
         	LLDisabilitySchema tLLDisabilitySchema = new LLDisabilitySchema();
		 
					tLLDisabilitySchema.setCaseNo(request.getParameter("CaseNo"));
					tLLDisabilitySchema.setCaseRelaNo(CaseRelaNo);
					tLLDisabilitySchema.setDisabilityFlag(DisabilityFlag[i]);
					tLLDisabilitySchema.setDisabilityDate(DisabilityDate[i]);
					tLLDisabilitySchema.setObservationDate(ObservationDate[i]);
					tLLDisabilitySet.add(tLLDisabilitySchema);
			 }
	 	}   
  }
  
  try
  {
    //VData tVData = new VData();
    tVData.addElement(tLLSubReportSet);
    tVData.addElement(CaseRelaNo);
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tLLCaseCureSet);
    tVData.addElement(tLLCaseInfoSet);
    tVData.addElement(tLLOtherFactorSet);
    tVData.addElement(tLLOperationSet);
    tVData.addElement(tLLAccidentSet);
    tVData.addElement(tLLDisabilitySet);
   // tCaseInfoUI.submitData(tVData,transact);
  
 /////////////////////////////////////////////////////////////////////////////////////

  //账单关联保存
  //if ( "FEERELASAVE".equals(transact))
  //{
  //String CaseRelaNo = request.getParameter("CaseRelaNo");
    String[] tChk = request.getParameterValues("InpICaseCureGridChk");
    String[] strFeeNo=request.getParameterValues("ICaseCureGrid5");
    LLFeeMainSet tLLFeeMainSet= new LLFeeMainSet();
   
	  if ( tChk!=null )
	  {
		  int intLength=tChk.length;
		  for(int i=0;i<intLength;i++)
		  {
		    if(tChk[i].equals("0")) //未选
		      continue;
		 
		    LLFeeMainSchema tLLFeeMainSchema=new LLFeeMainSchema();
		    tLLFeeMainSchema.setCaseNo(strCaseNo);
		    tLLFeeMainSchema.setMainFeeNo( strFeeNo[i]);
		    tLLFeeMainSchema.setCaseRelaNo(CaseRelaNo);
		    tLLFeeMainSet.add(tLLFeeMainSchema);
		  }
	  }

    tVData.addElement(tLLFeeMainSet);
  // }
    tAllCaseInfoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tAllCaseInfoUI.mErrors;
    if (!tError.needDealError())
    {
      Content = "保存成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = "保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

  
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>