/*******************************************************************************
 * Name: RegisterInput.js
 * Function:立案主界面的函数处理程序
 * Author:LiuYansong
 */
var showInfo;
var mDebug="0";
var tSaveFlag = "0";
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
/*******************************************************************************
 * Name        :main_init
 * Author      :LiuYansong
 * CreateDate  :2003-12-16
 * ModifyDate  :
 * Function    :初始化主程序
 * Parameter   :A_jsp---fm.action;Target_Flag (0--无target，1有target),T_jsp--target 对应的页面 ;Info---提示信息;Operate---操作标识
 *
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info,Operate)
{
	
  var i = 0;
  var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action ="./"+A_jsp;
  if(Operate=="INSERT")
  {
    fm.fmtransact.value="INSERT";
    
  }
  if(Operate=="QUERY")
  {
    fm.fmtransact.value="QUERY";
  }
  if(Operate=="UPDATE")
  {
    fm.fmtransact.value="UPDATE";
  }
  if(Operate=="DELETE")
  {
    fm.fmtransact.value="DELETE";
  }
  if(Target_Flag=="1")
  {
    fm.target = "./"+T_jsp;
  }
  else
  {
    fm.target = "fraSubmit";
  }
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//提交，保存按钮对应操作
//添加控制，不能对其点击两次的控制
function submitForm()
{
  if( verifyInput2() == false ) return false;
  if(tSaveFlag=="1")
  {
    alert("您不能执行保存操作！");
    return;
  }
  else
  {
    if (confirm("您确实想保存该记录吗?"))
    {
  
      tSaveFlag = "1";
      if(!checkdata())
        return ;
      else
      {
      //if(!checkRight()){ return false;}
      if(!checkMngCom()) {return false;}
      if(!checkCaseGrid()){
      return false;
      }
        main_init("ClaimUserSave.jsp","0","_blank","保存","INSERT");
        tSaveFlag = "0";
      }
    }
    else
    {
      alert("您取消了保存操作！");
    }
  }
}
//修改按钮对应的操作
function updateClick()
{
  if( verifyInput2() == false ) return false;
  if(tSaveFlag=="1")
  {
    alert("您不能执行修改操作！");
    return;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
      if( verifyInput2() == false ){
       return false;
      }
      if(!checkdata())
        return;
      else
      {
       //if(!checkRight()) return false;
       if(!checkMngCom()) return false;
       if(!checkCaseGrid()){
       return false;
       }

        main_init("ClaimUserSave.jsp","0","_blank","修改","UPDATE");
        tSaveFlag = "0";
      }
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}



function deleteClick()
{
  if(tSaveFlag=="1")
  {
    alert("您不能执行删除操作！");
    return;
  }
  else
  {
    if (confirm("您确实想删除该记录吗?"))
    {
      tSaveFlag = "1";
      main_init("ClaimUserSave.jsp","0","_blank","删除","DELETE");
      tSaveFlag = "0";
    }
    else
    {
      alert("您取消了删除操作！");
    }
  }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  var pathStr="./FrameMain.jsp?Interface=ClaimUserQuery.jsp";
  var newWindow=OpenWindowNew(pathStr,"理赔岗位权限查询","left");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
/*****************************************************************************************
 * Name        :checkdata
 * Author      :LiuYansong
 * CreateDate  :2004-10-26
 * ModifyDate  :
 * Function    :页面数据录入及修改的校验程序
 * Parameter   :
 * Notice      :
 */
function checkdata()
{
	
  if ((fm.UserCode.value=="")||(fm.UserCode.value=="null"))
  {
    alert ("“用户代码”不能为空，请录入相应的用户代码！");
    tSaveFlag = "0";
    return false;
  }
  if ((fm.UserName.value=="")||(fm.UserName.value=="null"))
  {
    alert ("“用户名称”不能为空，请录入相应的用户名称！");
    tSaveFlag = "0";
    return false;
  }
  if ((fm.Station.value=="")||(fm.Station.value=="null"))
  {
    alert ("“用户机构代码”不能为空，请录入相应的用户机构代码！");
    tSaveFlag = "0";
    return false;
  }

  if ((fm.ClaimPopedom.value=="")||(fm.ClaimPopedom.value=="null"))
  {
    alert ("“用户核赔权限”不能为空，请录入相应的用户核赔权限！");
    tSaveFlag = "0";
    return false;
  }

  if ((fm.StateFlag.value=="")||(fm.StateFlag.value=="null"))
  {
    alert ("“用户有效状态”不能为空，请录入相应的用户有效状态！");
    tSaveFlag = "0";
    return false;
  }
  if ((fm.CheckFlag.value=="")||(fm.CheckFlag.value=="null"))
  {
    alert ("“理算审核权限”不能为空，请录入相应的理算审核权限！");
    tSaveFlag = "0";
    return false;
  }
  if ((fm.HandleFlag.value=="")||(fm.HandleFlag.value=="null"))
  {
    fm.HandleFlag.value='0';
  }
  if ((fm.StateFlag.value!="1")&&((fm.Remark.value=="")||(fm.Remark.value=="null")))
  {
    alert ("当用户状态为非有效时，请录入备注信息！");
    tSaveFlag = "0";
    return false;
  }
  if(fm.ClaimSpotRate.value/1<0||fm.ClaimSpotRate.value/1>100)
  {
    alert("抽检比例应为介于0和100之间的数字");
    tSaveFlag = "0";
    return false;
  }
  if(fm.DispatchRate.value/1<0||fm.DispatchRate.value/1>60000)
  {
    alert("案件分配上限应为介于0和60000之间的数字");
    tSaveFlag = "0";
    return false;
  }
  // #3342 用户核赔权限和调查权限**start**
  if((fm.ClaimPopedom.value!="17" && fm.ClaimPopedom.value!="A") && fm.ServeyFlag.value!="0"){
	  alert("有理赔结案权限的理赔人员不能同时有调查权限。");
	  tSaveFlag = "0";
	  return false;
  }
  /*if(fm.ClaimPopedom.value!="17" && fm.ClaimPopedom.value!="A"){
  var tsql="select distinct 1 from LDUserTOMenuGrp where usercode='"+fm.UserCode.value+"' and menugrpcode in ('cm01','cm18','cm26') ";
  var aMenugrp= easyExecSql(tsql);
//   alert(aMenugrp);
   if(aMenugrp=="1"){
	   alert("有理赔结案权限的理赔人员不能同时有受理权限。");
		  tSaveFlag = "0";
		  return false;
   }
  }*/
  
  // #3342 用户核赔权限和调查权限**end**
  
  //----------zhangjl新增校验（理赔人变更机构如果没处理完的案件 调查案件 提示不能变更 ）
  //1.查询是否需要变更机构
  var JGSQL = "select ComCode from LLClaimUser where UserCode = '"+fm.UserCode.value+"' with ur";   //查询用户现在的机构
  var tComCode = easyExecSql(JGSQL);
  if(tComCode != fm.Station.value){
  //2.如果变更机构，查出没有处理完的案件
  var CaseSQL = "select CaseNo "
	 +"from llcase "
	 +"where Handler = '"+fm.UserCode.value+"' "
	 +"and rgtstate in ('02','03','04','06')  "    //"02"扫描状态,"03"检录状态,"04"理算状态,"06"审定状态
	 +"union "
	 +"select CaseNo "
	 +"from llcase "
	 +"where Rigister = '"+fm.UserCode.value+"' "
	 +"and rgtstate in ('01','09','13')  "         //"01"受理状态,"09"结案状态,"13"延迟状态
	 +"union "
	 +"select b.caseno "
	 +"from LLInqApply a,llcase b "
	 +"where a.otherno = b.caseno "
	 +"and b.rgtstate = '07' "
	 +"and a.inqper = '"+fm.UserCode.value+"'  "   //"07"调查状态,inqper不为空
	 +"union "
	 +"select b.caseno "
	 +"from LLInqApply a,llcase b "
	 +"where a.otherno = b.caseno "
	 +"and b.rgtstate = '07' "
	 +"and a.inqper is null "
	 +"and a.Dipatcher = '"+fm.UserCode.value+"' " //"07"调查状态,inqper不空,查询Dipatcher
	 +"union "
	 +"select b.caseno "
	 +"from llsurvey a,llcase b "
	 +"where a.otherno = b.caseno "
	 +"and b.rgtstate = '08' "
	 +"and a.StartMan = '"+fm.UserCode.value+"' "   //"08"查讫状态
	 +"union "
	 +"select b.caseno "
	 +"from LLClaimUWMain a,llcase b "
	 +"where a.caseno = b.caseno "
	 +"and b.rgtstate in ('05','10') "
	 +"and a.AppClmUWer = '"+fm.UserCode.value+"' "   //"05"审批状态,"10"抽检状态
	 +"with ur";
  var tCaseNo = easyExecSql(CaseSQL);
  //3.如果有未处理完的案件，弹出提示
  if(tCaseNo!=null&&tCaseNo!='NULL'&&tCaseNo!=""){
  	alert("用户:"+fm.UserCode.value+",还有案件:"+tCaseNo+"没处理完");
  	tSaveFlag = "0";
  	return false;
  }
  }
  //----------END
  

 var surveyStat = fm.ServeyFlag.value;
 var mngcomcode = fm.Station.value;
 var get_surveyFlag =  "select surveyflag from llclaimuser where comcode='"+mngcomcode+"' and usercode='"+fm.UserCode.value+"' with ur";
 var surveyFlayR = easyExecSql(get_surveyFlag);
 if(surveyFlayR!=null){
 if(surveyStat=="0")
  {
   if(surveyFlayR=='1'){
    var strSQL_M="select surveyno from llInqapply s where Dipatcher='"+fm.UserCode.value+"' and surveyno in "
							  +"(select surveyno from LLSurvey  where surveyflag <>'3' ) and exists (select 1 from llcase where caseno=s.otherno and rgtstate not in ('14'))"
							  +" union select surveyno from llsurvey s where Confer='"+fm.UserCode.value
							  +"'and surveyno in  (select surveyno from LLSurvey  where surveyflag <>'3' ) and exists (select 1 from llcase where caseno=s.otherno and rgtstate not in ('14')) with ur";
	var arrR1 = easyExecSql(strSQL_M);
	if (!(arrR1 == null||arrR1==""))
	 {
	 	alert("该用户存在未分配或未审核的调查！");
	 	tSaveFlag = "0";
     return false;
	}	
	}					  
  	var strSQLx = "select * from LLInqApply s where inqper='"+fm.UserCode.value+"' and surveyno in "
							  +"(select surveyno from LLSurvey  where surveyflag <>'3' ) and exists (select 1 from llcase where caseno=s.otherno and rgtstate not in ('14'))";
	 var arrResult = easyExecSql(strSQLx);
	 
	 if (!(arrResult == null||arrResult==""))
	 {
	 	alert("系统中尚有未结调查，不能修改为无调查权限！");
	 	tSaveFlag = "0";
    return false;
	}
	
	
  }else if (surveyStat=="1"){

   var sql_M="select 1 from llclaimuser where SurveyFlag='1' and stateflag='1' and comcode='"+mngcomcode+"' and usercode<>'"+fm.UserCode.value+"'with ur";
   var arrR2 = easyExecSql(sql_M);
   if (!(arrR2 == null||arrR2=="") && surveyFlayR!='1')
	 {
	 	alert("该机构已经存在调查主管！");
	 	tSaveFlag = "0";
    return false;
	}						  
  }else{
   
   if(surveyFlayR=='1'){
   var strSQL_M1="select surveyno from llInqapply s where Dipatcher='"+fm.UserCode.value+"' and surveyno in "
							  +"(select surveyno from LLSurvey  where surveyflag <>'3' ) and exists (select 1 from llcase where caseno=s.otherno and rgtstate not in ('14'))"
							  +" union select surveyno from llsurvey  s where Confer='"+fm.UserCode.value
							  +"'and surveyno in  (select surveyno from LLSurvey  where surveyflag <>'3' ) and exists (select 1 from llcase where caseno=s.otherno and rgtstate not in ('14')) with ur";
	var arrR2 = easyExecSql(strSQL_M1);
	if (!(arrR2 == null||arrR2==""))
	 {
	 	alert("该用户存在未分配或未审核的调查！");
	 	tSaveFlag = "0";
    return false;
	}	
   }
  }
  }
  
  return true;
}

function afterQuery(userCode )
{
	 var strSQL1= "select uwrate from ldspotuwrate"
	 					+" where UserCode='" + userCode +"'"; 
	 var brrResult = easyExecSql(strSQL1);
	 if(brrResult)
	 var rate = brrResult[0][0];
	else 
		var rate = "";
	var strSQL = "select UserCode,UserName,ComCode,UpUserCode,ClaimDeal,"
	 + "ReportFlag,RegisterFlag,ClaimFlag,ClaimPopedom,CheckFlag,UWFlag,"
	 + "SurveyFlag,PayFlag,StateFlag,Remark, "
	 + "(select distinct popedomname from  llclaimpopedom where claimpopedom = a.ClaimPopedom),"
	 + "(select UserName from  lduser where UserCode = a.UpUserCode),HandleFlag,DispatchRate,"
	 +" PrepaidFlag,PrepaidUpUserCode,(case when PrepaidLimit is null then 0 else PrepaidLimit end) ,(select UserName from  lduser where UserCode = a.PrepaidUpUserCode), "
	 +" (case PrepaidFlag when '0' then '否' when '1' then '是' end ),"
	 +" SpecialNeedFlag,SpecialNeedUpUserCode,(case when SpecialNeedLimit is null then 0 else SpecialNeedLimit end),(select UserName from  lduser where UserCode = a.SpecialNeedUpUserCode), "
	 +" (case SpecialNeedFlag when '0' then '否' when '1' then '是' end ) "
	 +" from llclaimuser a"
	 +" where UserCode='" + userCode +"'"; 
	 var arrResult = easyExecSql(strSQL);
	 if (arrResult != null)
	 {
	    	try{
		    	fm.Station.value = arrResult[0][2];
		    	fm.UserCode.value = arrResult[0][0];
		    	fm.ClaimPopedom.value = arrResult[0][8]; 
		        fm.UpUserCode.value = arrResult[0][3];
		        fm.ClaimDeal.value = arrResult[0][4];
		        if(fm.ClaimDeal.value=='0')
		          fm.ClaimDealName.value='无权限';
		        if(fm.ClaimDeal.value=='1')
		          fm.ClaimDealName.value='有理赔权限';
		        fm.ServeyFlag.value = arrResult[0][11];  
		        fm.StateFlag.value = arrResult[0][13];  
		        fm.Remark.value = arrResult[0][14];
		        fm.UserName.value = arrResult[0][1];
		        fm.ClaimPopedomName.value = arrResult[0][15];
		        fm.UpUserName.value = arrResult[0][16];
		        fm.HandleFlag.value = arrResult[0][17];
		        fm.DispatchRate.value = arrResult[0][18];
		        fm.PrepaidFlag.value = arrResult[0][19];
		        fm.PrepaidUpUserCode.value = arrResult[0][20];
		        fm.PrepaidLimit.value = arrResult[0][21];
		        fm.PrepaidUpUserCodeName.value = arrResult[0][22];
		        fm.PrepaidFlagName.value = arrResult[0][23];
		        
		        fm.SpecialNeedFlag.value = arrResult[0][24];
		        fm.SpecialNeedUpUserCode.value = arrResult[0][25];
		        fm.SpecialNeedLimit.value = arrResult[0][26];
		        fm.SpecialNeedUpUserCodeName.value = arrResult[0][27];
		        fm.SpecialNeedName.value = arrResult[0][28];
		        
		        fm.ClaimSpotRate.value = rate;
		        if(fm.HandleFlag.value=='0')
		          fm.HandleFlagName.value='否';
		        if(fm.HandleFlag.value=='1')
		          fm.HandleFlagName.value='是';
		        if(fm.ServeyFlag.value=='0')
		          fm.ServeyFlagName.value='无调查权限';
		        if(fm.ServeyFlag.value=='1')
		          fm.ServeyFlagName.value='调查主管';
		        if(fm.ServeyFlag.value=='2')
		          fm.ServeyFlagName.value='普通调查员';
		        if(fm.StateFlag.value=='1')
		          fm.StateFlagName.value='有效';
		        if(fm.StateFlag.value=='2')
		          fm.StateFlagName.value='休假';
		        if(fm.StateFlag.value=='3')
		          fm.StateFlagName.value='离职';
		        if(fm.StateFlag.value=='4')
		          fm.StateFlagName.value='离岗';
		        if(fm.StateFlag.value=='9')
		          fm.StateFlagName.value='其他无效情况';

	        }catch(ex)
		{
			alert(ex.message );
		}
		var strSQL1="select codename from ldcode where codetype='station' and code='" + fm.Station.value +"'";
		var arrResult1 = easyExecSql(strSQL1);
		if (arrResult1 != null){
			fm.StationName.value=arrResult1[0][0];    
	         }
	        else{
			return;
		}

		
		initCaseDisGrid();
		var strSQL1 = "select ComCode,CodeName,OtherSign from ldcode1 where codetype='LLCaseAssign' and code='"+arrResult[0][0]+"' with ur";
		turnPage.queryModal(strSQL1, CaseDisGrid);
	    
		
	  }
	 
	 else
	 {
	  return;
	 }
}

function checkCaseGrid() {
	var Count=CaseDisGrid.mulLineCount;
	for (i=0;i<Count;i++) {
		if(CaseDisGrid.getRowColData(i,1)==""||CaseDisGrid.getRowColData(i,1)==null) {
			alert("第"+(i+1)+"行机构代码不能为空!");	
			tSaveFlag = "0";
			return false;
		}
	}
	return true;
}	 

function checkRight() {
	var Count=CaseDisGrid.mulLineCount;
	if(Count>0 && (fm.ClaimDeal.value!='1' || fm.HandleFlag.value!='1')){
		alert("该客户无案件配置权限！");
		return false;
	}
	return true;
}

function checkMngCom() {
	var comcode = fm.Station.value;
	var Count=CaseDisGrid.mulLineCount;
	if(comcode.length==2){
		return true;
	}
	if(comcode.length==4){
		for (i=0;i<Count;i++) {
			if(comcode!=CaseDisGrid.getRowColData(i,1).substring(0,4)) {
				alert("第"+(i+1)+"行机构错误,只能为本机构及其下属机构进行案件分配!");
				tSaveFlag = "0";
				return false;	
			}
		}
	} else {
		for (i=0;i<Count;i++) {
			if(comcode!=CaseDisGrid.getRowColData(i,1)) {
				alert("第"+(i+1)+"行机构错误,只能为本机构及其下属机构进行案件分配!");	
				return false;	
				tSaveFlag = "0";
			}
		}
	}
	return true;
}


