<%
//程序名称：RgtSurveyInit.jsp
//程序功能：理赔提调的初始化
//创建日期：
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	GlobalInput mG = new GlobalInput();
	mG=(GlobalInput)session.getValue("GI");
	String CaseNo = request.getParameter("CaseNo");
	String InsuredNo = request.getParameter("InsuredNo");
	String SurveyType = request.getParameter("Type");
	String StartPhase = request.getParameter("StartPhase");
	String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
%>

<script language="JavaScript">
function initInpBox(){
	try{
		fm.CaseNo.value = '<%= CaseNo%>';
		fm.InsuredNo.value = '<%= InsuredNo%>';
		var tsql = "select substr(Name,1,20) from ldperson where CustomerNo='"+fm.InsuredNo.value+"'";
		var xrr = easyExecSql(tsql);
		if(xrr!= null){
		   
			fm.CustomerName.value = xrr[0][0];
		}

		fm.SurveyType.value = '<%= SurveyType%>';
		fm.all('StartPhase').value = "<%=StartPhase %>";
		if(fm.all('StartPhase').value=='2'){
			fm.SurvType.value = '0';
			fm.SurvTypeName.value = '即时调查';
		}
		else{
			fm.SurvType.value = '1';
			fm.SurvTypeName.value = '一般调查';
		}
		if(fm.all('SurveyType').value=='2'){
			document.all("div2").style.display = "none";
			document.all("div3").style.display = "";
		}
		else{
			document.all("div2").style.display = "";
			document.all("div3").style.display = "none";
		}

		strSQL="select count(*) from llsurvey where otherno='"+fm.CaseNo.value+"'";
		var arr=easyExecSql(strSQL);
		if(arr!= null){
			fm.all('SerialNo').value=arr[0][0];
		}

		fm.all('Surveyer').value = "<%=mG.Operator%>";
	}
	catch(ex){
		alter("在RgtSurveyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm(){
	try{
		initInpBox();
		initPastGrid();
		EasyQuery();
	}
	catch(re){
		alter("在RgtSurveyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initPastGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array("序号","30px","10","0");
		iArray[1]=new Array("案件号","120px","30","0");
		iArray[2]=new Array("调查序号","70px","30","0");
		iArray[3]=new Array("提调机构","70px","50","0");
		iArray[4]=new Array("提调人","70px","100","0");
		iArray[5]=new Array("提调日期","70px","100","0");
		iArray[6]=new Array("查讫日期","70px","100","0");
		iArray[7]=new Array("案件状态","70px","100","0");
		iArray[8]=new Array("调查项目号","0px","100","3");
		iArray[9]=new Array("调查人","70px","100","0");
		iArray[10]=new Array("调查类型","70px","100","0");
		iArray[11]=new Array("调查号类型","0px","100","3");
		iArray[12]=new Array("涉案金额","70px","100","0");

		PastGrid = new MulLineEnter( "fm" , "PastGrid" );
		//这些属性必须在loadMulLine前
		PastGrid.mulLineCount = 3;
		PastGrid.displayTitle = 1;
		PastGrid.locked = 1;
		PastGrid.canSel = 1;
		PastGrid.hiddenPlus = 1;
		PastGrid.hiddenSubtraction = 1;
		PastGrid.loadMulLine(iArray);
		PastGrid.selBoxEventFuncName = "ShowContent";
	}
	catch(ex){
		alert(ex);
	}
}


</script>