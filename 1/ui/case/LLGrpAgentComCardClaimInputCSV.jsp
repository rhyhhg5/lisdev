<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLAgentComCardClaimInputCSV.jsp
//程序功能：F1报表生成
//创建日期：2012-09-25
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
   String sql = "select case when current time>='08:00:00' and current time<'17:30:00' then 'Y' else 'N' end from dual"; 
   String sql1 = "select case when dayofweek(current date)=7 or dayofweek(current date)=1 then 'Y' else 'N' end from dual with ur";
   ExeSQL tExeSQL = new ExeSQL();
   String rel = tExeSQL.getOneValue(sql);
   String rel1 = tExeSQL.getOneValue(sql1);
%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLGrpAgentComCardClaimInputCSV.js"></SCRIPT>    
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
</head>
<body>    
  <form action="./LLGrpAgentComCardClaimRptCSV.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title>中介机构</TD>
          <TD  class= input> <input class="codeno" verify="中介机构|code:agentcom" elementtype=nacessary name=AgentCom ondblclick="return showCodeList('agentcom',[this,AgentComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('agentcom',[this,AgentComName],[0,1],null,null,null,1);"><input class=codename name=AgentComName></TD>            
         </TR>
      	<TR  class= common>
      	  <td  class= title> 中介机构类型 </td>  
        <td  class= code> <input class="codeno" name="ACType" verify="中介机构类型|code:ACType"
        	   ondblclick="return showCodeList('ACType',[this,ACTypeName],[0,1],null,null,null,1);"
        	   onkeyup="return showCodeListKey('ACType',[this,ACTypeName],[0,1],null,null,null,1);"
        	   ><Input class=codename name=ACTypeName readOnly elementtype=nacessary>
        </td>
		  <TD  class= title>中介机构状态</TD>
          <TD  class= input> <input class="codeno" CodeData="0|2^1|有效^2|无效"  verify="中介机构状态" elementtype=nacessary name=AgentComState ondblclick="return showCodeListEx('AgentComState',[this,AgentComStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('AgentComState',[this,AgentComStateName],[0,1]);"><input class=codename name=AgentComStateName></TD> 
	    </TR>
      	<TR  class= common>
		  <TD  class= title>财务结算起期</TD>
          <TD  class= input> <Input name=SignDate class='coolDatePicker' dateFormat='short' verify="财务结算起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>财务结算止期</TD>
          <TD  class= input> <Input name=EndSignDate class='coolDatePicker' dateFormat='short' verify="财务结算止期|notnull&Date" elementtype=nacessary> </TD> 
		</TR>
		<TR  class= common>
		  <TD  class= title>结案起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="结案起期|Date" elementtype=nacessary> </TD> 
          <TD  class= title>结案止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="结案止期|Date" elementtype=nacessary> </TD> 
        </TR>
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="BranchEfficient()"></TD>
			</tr>
		</table>
		<table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
			<tr class="common"><td class="title">保费收入：保单实收保费</td></tr><br>
			<tr class="common"><td class="title">结案赔款：保单承保险种下结案日期在统计期内的已结案赔款</td></tr><br>
		</Div>
		<table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		  查询条件说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
			<tr class="common"><td class="title">中介机构状态：可根据数据统计需要选择全部中介机构或当前合作状态为有效/无效的中介机构</td></tr><br>
			<tr class="common"><td class="title">财务结算起止期：指保费收入数据统计的起止期</td></tr><br>
			<tr class="common"><td class="title">结案起止期：如不对该日期进行限定，则统计出的结案赔款为业务对应产生的全部结案赔款；如对该日期进行限定，则统计出的结案赔款为业务在选定结案起止期内对应产生的结案赔款</td></tr><br>
		</Div>
<INPUT  type= hidden name="result" value=<%=rel %>>
<INPUT  type= hidden name="isweek" value=<%=rel1 %>>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 