<html>
	<%
	//Name：LLMainAskInput.jsp
	//Function：登记界面的初始化
	//Date：2004-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.llcase.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLContQueryInput.js"></SCRIPT>
		<%@include file="LLContQueryInit.jsp"%>
	</head>
	<body  onload="initForm('<%=tCaseNo%>','<%=tInsuredNo%>');" >

		<form action="./LLSecondUWSave.jsp" method=post name=fm target="fraSubmit">
			<Div align="left">
				<Table>
					<TR>
						<TD class=common>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
						</TD>
						<TD class= titleImg >
							客户信息
						</TD>
					</TR>
				</Table>
			</div>
			<div id='divLDPerson1'>
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>理赔号</TD><TD  class= input8><input class= common name="CaseNo" onkeydown="QueryOnKeyDown();" ></TD>
						<TD  class= title8>客户号</TD><TD  class= input8><input class= common name="InsuredNo" onkeydown="QueryOnEnter();"></TD>
						<TD  class= title8>客户姓名</TD><TD  class= input8><input class= common name="CustomerName" onkeydown="QueryOnEnter();"></TD>
					</TR>
				</table>
			</div>

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						客户保单信息
					</TD>
				</TR>
			</table>

			<Div  id= "divCont" align= center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanPolGrid" >
							</span>
						</TD>
					</TR>
				</table>
				<!-- wdx增加了提交方法，使变更页数后也会去后台查询保额 111-->
				<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();fm.submit();Supdiscountfactor('page');">
				<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();fm.submit();Supdiscountfactor('page');">
				<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();fm.submit();Supdiscountfactor('page');">
				<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();fm.submit();Supdiscountfactor('page');">
			</Div>
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						特别约定
					</TD>
				</TR>
			</table>
				<table class=common>
          <TR  class= common>
          	<TD  class= input>
							<textarea name="Remark" cols="100%" rows="4" witdh=25% class="common"></textarea>
	  				</TD>
        </tr>  
      </table>
      			<!-- 
      			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						理赔二核核保意见
					</TD>
				</TR>
			</table>
				<table class=common>
          <TR  class= common>
          	<TD  class= input>
							<textarea name="UWRemark" cols="100%" rows="4" witdh=25% class="common"></textarea>
	  				</TD>
        </tr>  
      </table>
       -->
      <table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						补充告知
					</TD>
				</TR>
			</table>
				<table class=common>
          <TR  class= common>
          	<TD  class= input>
				<iframe id="EdorInfo" width="100%" height="200" frameborder="0" scrolling="auto" src="about:blank"></iframe>
	  	   </TD>
        </tr>  
      </table>      
				<input name="Suggest" style="display:''"  class=cssButton type=button value="承保协议图像查询" onclick="ScanQuery()">
			<input type=hidden name="ContNo" >
			<input type=hidden name="ContNoA" >
			<input type=hidden name="sql" >
			<input type="hidden" name="EdorNo">
		</form>
				<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
			</body>
		</html>
