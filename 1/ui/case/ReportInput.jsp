<html>
<%
//Name：ReportInput.jsp
//Function：报案界面的初始化
//Date：2004-12-23 16:49:22
//Author：LiuYansong，陈海强
%>
 <%@page contentType="text/html;charset=GBK" %>
 <!--用户校验类-->
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="ReportInput.js"></SCRIPT>
   <%@include file="ReportInit.jsp"%>
 </head>

 <body  onload="initForm();" >
   <form action="./ReportQueryOut.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
         </TD>
         <TD class= titleImg>
         报案信息
         </TD>
       </TR>
      </table>

     <Div  id= "divLLReport1" style= "display: ''">
       <table  class= common>
         <TR  class= common8>
           <TD  class=title8>
                报案号
           </TD>
           <TD  class= input8>
               <Input class="readonly" readonly  name=RptNo >
           </TD>
           <!--
           <TD  class= title8>
                理赔状态
		  </TD>
		  <TD class= input8>
		        <Input class= "readonly" readonly name=CaseState>
		  </TD>
		  <TD class=title8>
			    事故者类型
		  </TD>
		  <TD class=input8> 
			    <Input class="code" name=PeopleType verify="事故者类别|NOTNULL" CodeData="0|^0|被保险人^1|投保人^2|被保险人的配偶^3|被保险人的子女" ondblClick="showCodeListEx('PeopleType_1',[this],[0,1,2,3]);" onkeyup="showCodeListKeyEx('PeopleType_1',[this],[0,1,2,3]);">
		  </TD>
		</TR>
		<TR class=common>
           <TD  class= title8>
                号码类型
           </TD>
           <TD  class= input8>
                <Input class="code" name=RptObj verify="号码类型|NOTNULL" CodeData="0|^0|团单^1|个单^2|客户号" ondblClick="showCodeListEx('RptObjReport',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('RptObjReport',[this],[0,1,2]);">
           </TD>
         
           <TD  class= title8>
               号码
           </TD>
           
            <TD  class= input8>
            	<Input class= common name=RptObjNo>
          	</TD>
    		<TD class=title>
          		<input class=cssButton type=button value="查  询" onclick="submitForm1()">
			</TD>
			<TD  class= input8>
           </TD>
		</TR>
        <TR  class= common8>
        -->
          	<TD  class= title8>
            	报案人姓名
          	</TD>
          	<TD  class= input8>
            	<Input class= common name=RptorName >
          	</TD>
          	<TD  class= title8>
            	报案人电话
          	</TD>
       
          	<TD  class= input8>
            	<Input class= common name=RptorPhone >
          	</TD>
           </TR>
        <TR  class= common8>
        	<TD  class= title8>
            	报案人通讯地址
          	</TD>
          	<TD  class= input8>
            	<Input class= common name=RptorAddress >
          	</TD>
        	
        	<TD  class= title8>
            	报案人与事故人关系
          	</TD>
          	<TD  class= input8>
            	<Input class="code8" name=Relation ondblclick="return showCodeList('Relation',[this]);" onkeyup="return showCodeListKey('Relation',[this]);">
          	</TD>
          	<TD  class= title8>
            	报案方式
          	</TD>
          	<TD  class= input8>
           		<Input class="code8" name="RptMode" ondblclick="return showCodeList('RptMode',[this]);" onkeyup="return showCodeListKey('RptMode',[this]);">
          	</TD>
          	 </TR>

        <TR  class= common8>    
        <TD  class= title8>
            	出险日期
          	</TD>
          	<TD  class= input8>
            	<input class="coolDatePicker" dateFormat="short" name="AccidentDate" >
          	</TD>        
       
		  	<TD  class= title8>
            	出险地点
          	</TD>
      
          	<TD  class= input8>
            	<Input class= common8 name=AccidentSite >
          	</TD>        
       			<TD  class= title8>
            	报案受理日期
          	</TD>
          	<TD  class= input8>
            	<input class="readonly" readonly name="RptDate" >
            </TD>
			</TR>

        <TR  class= common8>
		
			<TD  class= title8>
            	管辖机构
          	</TD>
          	<TD  class= input8>
            	<Input class= "readonly" readonly name= MngCom>
			</TD>
         
        	<TD  class= title8>
            	报案受理人
          	</TD>
          	<TD  class= input8>
            	<Input class="readonly" readonly name=Operator >
          	</TD>        
      		<TD  class= title8>
                理赔状态
		  </TD>
		  <TD class= input8>
		        <Input class= "readonly" readonly name=CaseState>
		  </TD>
         </TR>
     </table>  
 <table  class= common>
		<TR class= common>
			<TD class=title8>
 				事故起因经过描述
  			</TD>
		</TR>

		<TR  class= common>
  			<TD  class= input8>
    				<textarea name="AccidentReason" cols="100%" rows="3" witdh=25% class="common">
            </textarea>
    			</TD>
  		</TR>

					<TR  class= common>
						<TD  class= title8>
							出现过程和结果
						</TD>
					</TR>

					<TR  class= common>
						<TD  class= input8>
							<textarea name="AccidentCourse" cols="100%" rows="3" witdh=25% class="common">
              </textarea>
						</TD>
					</TR>
<!--
					<TR  class= common>
	    			<TD  class= title8>
      				备注
      			</TD>
    			</TR>

    			<TR class= common>
          	<TD  class= title8>
         			<textarea name="Remark" cols="100%" rows="3" witdh=25% class="common"></textarea>
         		</TD>
        	</TR>
        	-->
				</table>

    	  <table  class= common>
        	<TR>
    	  		<TD text-align: left colSpan=1>
	         		<span id="spanNormPayCollChooseGrid" ></span>
						</TD>
					</TR>
      	</table>
  		</Div>

  		<span id="spanLJSPayPersonCode"  style="display:''; position:absolute; slategray"></span>

    		<table>
    			<tr>
        		<td class=common>
			    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLSubReport);">
    				</td>
    				<td class= titleImg>
    			 		客户信息
    				</td>
    			</tr>
    		</table>

			<Div  id= "divLLSubReport" style= "display: ''">
    		<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
							<span id="spanSubReportGrid" >
							</span>
						</td>
					</tr>
				</table>
			</div>
			
			  <div>
		  <input  style="display:''" class=cssButton type=button value="关联客户查询" onclick="ClientQuery()">
		  </div>
		  
			<input type=hidden id="fmtransact" name="fmtransact">
			<br>
       <TD class=input width="26%">
            <input class=cssButton type=button value="保单查询" onclick="showInsuredLCPol()">
      </td>
      <TD class=input width="26%">
            <input class=cssButton type=button value="客户资料查询" onclick="showCustomerInfo()">
      </td>
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>