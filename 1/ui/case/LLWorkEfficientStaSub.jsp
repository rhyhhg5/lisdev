<%
//程序名称：LLWorkEfficientStaSub.jsp
//程序功能：工作状况统计表：2-工作效率统计
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gbk" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gbk">
ReportUI
</title>
<head>
</head>
<body>
<%

//String FileName = "GroupProduct_Income";
//String StartDate = "2005-01-01";
//String EndDate = "2005-01-11";
//String ManageCom = "86";
String flag = "0";
String FlagStr = "";
String Content = "";

GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
String operator=tG.Operator;

//设置模板名称

String FileName ="LLWorkEfficientSta";
String ManageCom = request.getParameter("ManageCom");
System.out.println(ManageCom);
String StartDate = request.getParameter("StartDate");
System.out.println(StartDate);
String EndDate = request.getParameter("EndDate");
System.out.println(EndDate);

String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");
System.out.println("@工作状况统计表：2-工作效率统计@");
System.out.println("Operate:"+operator);
String comlength = "";
String StatsType = request.getParameter("StatsType");//统计类型
System.out.println("StatsType:"+StatsType);

//对不同的统计类型，选择不同的打印模板
if("1".equals(StatsType)){
	FileName = "LLWorkEfficientStaST";//商业保险
	System.out.println("FileName:"+FileName);
}else if("2".equals(StatsType)){
	FileName = "LLWorkEfficientStaSB";//社会保险
	System.out.println("FileName:"+FileName);
}else{
	FileName = "LLWorkEfficientSta";//全部
	System.out.println("FileName:"+FileName);
}

RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);

if(ManageCom.length()==2){
  comlength="4";
}
else {
  comlength="8";
}

if(sd.compareTo(ed) > 0)
{
flag = "1";
FlagStr = "Fail";
Content = "操作失败，原因是:统计止期比统计统计起期早";
}


JRptList t_Rpt = new JRptList();
String tOutFileName = "";
if(flag.equals("0"))
{
t_Rpt.m_NeedProcess = false;
t_Rpt.m_Need_Preview = false;
t_Rpt.mNeedExcel = true;

StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
String CurrentDate = PubFun.getCurrentDate();
CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");

t_Rpt.AddVar("StartDate", StartDate);
t_Rpt.AddVar("EndDate", EndDate);
t_Rpt.AddVar("ManageCom",ManageCom);
t_Rpt.AddVar("operator",operator);
t_Rpt.AddVar("ComLength",comlength);
t_Rpt.AddVar("ManageComName", ReportPubFun.getMngName(ManageCom));
String YYMMDD = "";
YYMMDD = StartDate.substring(0, StartDate.indexOf("-")) + "年"
       + StartDate.substring(StartDate.indexOf("-") + 1,StartDate.lastIndexOf("-")) + "月"
       + StartDate.substring(StartDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("StartDateN", YYMMDD);
YYMMDD = "";
YYMMDD = EndDate.substring(0, EndDate.indexOf("-")) + "年"
       + EndDate.substring(EndDate.indexOf("-") + 1,EndDate.lastIndexOf("-")) + "月"
       + EndDate.substring(EndDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("EndDateN", YYMMDD);
YYMMDD = "";
YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("MakeDate", YYMMDD);

t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName;
System.out.println("strVFPathName : "+ strVFPathName);
System.out.println("=======Finshed in JSP===========");
System.out.println(tOutFileName);

rpt.updateReportMetaData(strVFFileName);

response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >
