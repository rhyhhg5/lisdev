<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLSimpleClaimSave.jsp
//程序功能：
//创建日期：2006-05-23 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%

//操作符
String strOperate = request.getParameter("operate");
System.out.println("==== strOperate == " + strOperate);

//输入参数
//	LLAppClaimReasonSet tLLAppClaimReasonSet   = new LLAppClaimReasonSet();
LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
LLCaseSchema tLLCaseSchema = new LLCaseSchema();
LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
//声明VData
VData tVData = new VData();

//声明后台传送对象
SimpleClaimUI tSimpleClaimUI   = new SimpleClaimUI();

//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
LLCaseSchema mLLCaseSchema = new LLCaseSchema();

tLLCaseSchema.setRgtType("1");
tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));

LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
tLLAppClaimReasonSchema.setReasonCode("02");        //原因代码
tLLAppClaimReasonSchema.setReason("住院");                //申请原因
tLLAppClaimReasonSchema.setCustomerNo(request.getParameter("CustomerNo"));
tLLAppClaimReasonSchema.setReasonType("0");

tLLCaseSchema.setRgtState("03");//案件状态
tLLCaseSchema.setCustomerName(request.getParameter("CustomerName"));
tLLCaseSchema.setCustomerNo(request.getParameter("CustomerNo"));
tLLCaseSchema.setDeathDate(request.getParameter("DeathDate"));
tLLCaseSchema.setCustBirthday(request.getParameter("CBirthday"));
tLLCaseSchema.setCustomerSex(request.getParameter("Sex"));
tLLCaseSchema.setIDType("0");
tLLCaseSchema.setIDNo(request.getParameter("IDNo"));
tLLCaseSchema.setOtherIDType("5");
tLLCaseSchema.setOtherIDNo(request.getParameter("SecurityNo"));
tLLCaseSchema.setCustState(request.getParameter("CustStatus"));
tLLCaseSchema.setGrpName(request.getParameter("GrpName"));
tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
tLLCaseSchema.setSurveyFlag("0");
tLLCaseSchema.setAccdentDesc(request.getParameter("PerRemark"));

tLLFeeMainSchema.setRgtNo(request.getParameter("RgtNo"));
tLLFeeMainSchema.setCaseNo(request.getParameter("CaseNo"));
tLLFeeMainSchema.setCustomerNo(request.getParameter("CustomerNo"));
tLLFeeMainSchema.setCustomerSex(request.getParameter("Sex"));
tLLFeeMainSchema.setHospitalCode(request.getParameter("HospitalCode"));
tLLFeeMainSchema.setHospitalName(request.getParameter("HospitalName"));
tLLFeeMainSchema.setHosAtti(request.getParameter("HosSecNo"));
tLLFeeMainSchema.setReceiptNo("53000");
tLLFeeMainSchema.setFeeType(request.getParameter("FeeType"));
tLLFeeMainSchema.setFeeAtti("1");
tLLFeeMainSchema.setFeeDate(request.getParameter("FeeDate"));
tLLFeeMainSchema.setHospStartDate(request.getParameter("HospStartDate"));
tLLFeeMainSchema.setHospEndDate(request.getParameter("HospEndDate"));
tLLFeeMainSchema.setRealHospDate(request.getParameter("RealHospDate"));
tLLFeeMainSchema.setInHosNo(request.getParameter("inpatientNo"));
tLLFeeMainSchema.setSecurityNo(request.getParameter("SecurityNo"));
tLLFeeMainSchema.setInsuredStat(request.getParameter("InsuredStat"));
tLLFeeMainSchema.setSecuPayDate(request.getParameter("SFeeDate"));
String tsFee[] = request.getParameterValues("FeeGrid1");	        //费用金额
//String tsSelfAmnt[]= request.getParameterValues("FeeGrid2");			//自费金额
//String tsSelfPay2[]= request.getParameterValues("FeeGrid3");
String tsFeeInSecu[]= request.getParameterValues("FeeGrid4");
String tsPlanFee[] = request.getParameterValues("FeeGrid5");
String tsSupInHosFee[] = request.getParameterValues("FeeGrid6");			  //先行给付金额
String tsSecuRefuseFee[] = request.getParameterValues("FeeGrid7");		//不合理费用
tLLSecurityReceiptSchema.setApplyAmnt(tsFee[0]);
//tLLSecurityReceiptSchema.setSelfAmnt(tsSelfAmnt[0]);
//tLLSecurityReceiptSchema.setSelfPay2(tsSelfPay2[0]);
tLLSecurityReceiptSchema.setFeeInSecu(tsFeeInSecu[0]);
tLLSecurityReceiptSchema.setPlanFee(tsPlanFee[0]);
tLLSecurityReceiptSchema.setSupInHosFee(tsSupInHosFee[0]);
tLLSecurityReceiptSchema.setSecuRefuseFee(tsSecuRefuseFee[0]);
  tLLSecurityReceiptSchema.setHighAmnt2(tsSupInHosFee[0]);
double tsumMoney = tLLSecurityReceiptSchema.getSecuRefuseFee()
                 +tLLSecurityReceiptSchema.getPlanFee()
                 +tLLSecurityReceiptSchema.getSupInHosFee();
if(tLLSecurityReceiptSchema.getApplyAmnt()<0.01){
  tLLSecurityReceiptSchema.setApplyAmnt(tsumMoney);
}
if(Math.abs(tLLSecurityReceiptSchema.getApplyAmnt()-tsumMoney)>0.01){
  Content = " 保存失败，原因是:各费用分项之和不等于费用总额！";
  FlagStr = "Fail";
}

System.out.println(FlagStr);
if(!FlagStr.equals("Fail")){
  LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
  if(request.getParameter("MICDCode")!=null){
  tLLCaseCureSchema.setDiseaseCode(request.getParameter("MICDCode"));
  tLLCaseCureSchema.setDiseaseName(request.getParameter("MICDName"));
  tLLCaseCureSchema.setMainDiseaseFlag("1");
  tLLCaseCureSet.add(tLLCaseCureSchema);
  }
  if(request.getParameter("SICDCode")!=null){
  tLLCaseCureSchema = new LLCaseCureSchema();
  tLLCaseCureSchema.setDiseaseCode(request.getParameter("SICDCode"));
  tLLCaseCureSchema.setDiseaseName(request.getParameter("SICDName"));
  tLLCaseCureSchema.setMainDiseaseFlag("0");
  tLLCaseCureSet.add(tLLCaseCureSchema);
  }
  

  //保存案件时效
  tLLCaseOpTimeSchema.setRgtState("01");
  tLLCaseOpTimeSchema.setStartDate(request.getParameter("OpStartDate"));
  tLLCaseOpTimeSchema.setStartTime(request.getParameter("OpStartTime"));

  try
  {
    // 准备传输数据 VData
    System.out.println("<--Star Submit VData-->");
    tVData.add(tLLAppClaimReasonSchema);
    tVData.add(tLLCaseSchema);
    tVData.add(tLLRegisterSchema);
    tVData.add(tLLFeeMainSchema);
    tVData.add(tLLSecurityReceiptSchema);
    tVData.add(tLLCaseCureSet);
    tVData.add(tLLCaseOpTimeSchema);

    tVData.add(tG);
    System.out.println("<--Into tSimpleClaimUI-->");
    tSimpleClaimUI.submitData(tVData,strOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是: " + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if ("".equals(FlagStr))
  {
    tError = tSimpleClaimUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Success";
      tVData.clear();
      tVData=tSimpleClaimUI.getResult();
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
//添加各种预处理

mLLCaseSchema.setSchema(( LLCaseSchema )tVData.getObjectByObjectName( "LLCaseSchema", 0 ));
System.out.println("结束了");
%>
<script language="javascript">
parent.fraInterface.fm.all("CaseNo").value = "<%=mLLCaseSchema.getCaseNo()%>";
parent.fraInterface.fm.all("RgtNo").value = "<%=mLLCaseSchema.getRgtNo()%>";
parent.fraInterface.fm.all("Handler").value = "<%=mLLCaseSchema.getOperator()%>";
parent.fraInterface.fm.all("ModifyDate").value = "<%=mLLCaseSchema.getModifyDate()%>";
</script>
<%
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
