<%
//程序名称：LLCaseReturnInputInit.jsp
//程序功能：
//创建日期：2005-2-26 12:07
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
String InsuredNo = request.getParameter("InsuredNo");
String CaseNo = request.getParameter("CaseNo");
String RgtState = request.getParameter("RgtState");
GlobalInput tGI = new GlobalInput(); //repair:
String Operator="";
String ManageCom="";
tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI!=null)
  {
    Operator  = tGI.Operator ;  //保存登陆管理员账号   
    //modify by Houyd 由于特需业务需求，需要对特需上级同步查询，添加社保查询
    ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
       
  }
//String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
var turnPage=new turnPageClass();
function initInpBox()
{ 
  var tCaseNo = "";
  try
  {       
     var InsuredNo = "<%=InsuredNo%>"; 
 
     fm.CaseNo.value = "<%=CaseNo%>";
     fm.Operator.value = "<%=Operator%>";
	 fm.RgtState.value = "<%=RgtState%>"; 
	 tCaseNo = "<%=CaseNo%>";
	 //alert(fm.BackState.CodeData);
	 //对于社保案件的回退，此处只显示受理、理算、审批
	 //1.判断是否社保案件
	 //此处判断需要支持社保的申诉纠错案件
     if(fm.CaseNo.value.substring(0,1) == 'R' || fm.CaseNo.value.substring(0,1) == 'S')
	 {
	    var SQLCaseNo = "select caseno from llcase where caseno=(select distinct caseno from LLAppeal where appealno ='"+fm.CaseNo.value+"')";
  	    var JCCaseNo = easyExecSql(SQLCaseNo);
  	    tCaseNo = JCCaseNo[0][0];
	 }
	 var OperatorStr = "";
	 var Operator = "<%=Operator%>";
	 var ManageCom = "<%=ManageCom%>";
	 var sql1 = "Select CHECKGRPCONT(rgtobjno) from llregister where rgtno =(select rgtno from llcase where caseno = '"+tCaseNo+"') with ur";   
     var tarr1 = easyExecSql(sql1);   
     if(tarr1){
        if(tarr1[0][0]=='Y')
       	{
       		//2.修改下拉框
			fm.BackState.CodeData = "0|3^01|受理^04|理算^05|审批";
			OperatorStr = "1 and upusercode=#"+Operator+"# or specialneedupusercode=#"+Operator+"# "
   				+"union select usercode,username,claimpopedom from llsocialclaimuser where "
   				+"stateflag=#1# and (upusercode=#"+Operator+"# or (handleflag=#0# and comcode like #"+ManageCom+"%#))";		
   			fm.OperatorStr.value = OperatorStr;		
       	}
       	else{
       		fm.BackState.CodeData = "0|4^01|受理^03|检录^04|理算^05|审批";
       		OperatorStr = "1 and upusercode=#"+Operator+"# or specialneedupusercode=#"+Operator+"# or usercode=#"+Operator+"# "; 	
       		fm.OperatorStr.value = OperatorStr;			       		       			     		
       	}    
     }else{
     	fm.BackState.CodeData = "0|4^01|受理^03|检录^04|理算^05|审批";
     	OperatorStr = "1 and upusercode=#"+Operator+"# or specialneedupusercode=#"+Operator+"# or usercode=#"+Operator+"# "; 	
       	fm.OperatorStr.value = OperatorStr;	
     }
  }
  catch(ex)
  {
    alert("在LLCaseReturnInputInit.jsp-->InitInpBox1函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LLCaseReturnInputInit.jsp-->InitSelBox2函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
  
    initInpBox(); 
   //initAccountGrid();
   //afterQuery1();
   //initBnfGrid();
   //afterQuery();
 

    
  }
  catch(re)
  {
    alert("LLCaseReturnInputInit.jsp-->InitForm函数中发生异常3:初始化界面错误!"+re.message);
  }
}

// 保单信息列表的初始化
function initBnfGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="受益人姓名";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="性别";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="sex"; 

      iArray[3]=new Array();
      iArray[3][0]="出生日期";         		//列名
      iArray[3][1]="70px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="受益级别";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="收益份额";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();  
	  iArray[6][0]="CustomerNo";
	  iArray[6][1]="140px";   
	  iArray[6][2]=200;       
	  iArray[6][3]=1;
	  
	  iArray[7]=new Array();  
	  iArray[7][0]="IDType";
	  iArray[7][1]="140px";   
	  iArray[7][2]=200;       
	  iArray[7][3]=1;   
	  
	  iArray[8]=new Array();  
	  iArray[8][0]="IDNo";
	  iArray[8][1]="140px";   
	  iArray[8][2]=200;       
	  iArray[8][3]=1;   
	  
	  iArray[9]=new Array();  
	  iArray[9][0]="PolNo";  //保单险种号码
	  iArray[9][1]="140px";   
	  iArray[9][2]=200;       
	  iArray[9][3]=1;   
	  
	  iArray[10]=new Array();  
	  iArray[10][0]="BnfType";//受益人类别
	  iArray[10][1]="140px";   
	  iArray[10][2]=200;       
	  iArray[10][3]=1;   
	  
	  iArray[11]=new Array();  
	  iArray[11][0]="ContNo";//合同号码
	  iArray[11][1]="140px";   
	  iArray[11][2]=200;       
	  iArray[11][3]=1;        
	 
	  iArray[12]=new Array();  
	  iArray[12][0]="RelationToInsured";
	  iArray[12][1]="140px";   
	  iArray[12][2]=200;       
	  iArray[12][3]=1; 
	  
	  iArray[13]=new Array();  
	  iArray[13][0]="账户名";
	  iArray[13][1]="140px";   
	  iArray[13][2]=200;       
	  iArray[13][3]=1;
	  
	  iArray[14]=new Array();  
	  iArray[14][0]="金额";
	  iArray[14][1]="60px";   
	  iArray[14][2]=200;
	  if(fm.LoadFlag.value=="1")
	  {
	  	iArray[14][3]=1;
	  }
	  if(fm.LoadFlag.value=="2")
	  {
	  	iArray[14][3]=1;
	  }		
	  iArray[15]=new Array();
      iArray[15][0]="银行编码";         		//列名
      iArray[15][1]="90px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[15][4]="bank";

	  iArray[16]=new Array();  
	  iArray[16][0]="银行账号";
	  iArray[16][1]="100px";   
	  iArray[16][2]=200;       
	  iArray[16][3]=1;         
	  
	  iArray[17]=new Array();  
	  iArray[17][0]="给付金领取方式";
	  iArray[17][1]="100px";   
	  iArray[17][2]=200;       
	  iArray[17][3]=2;
	  iArray[17][3]=2;
	  iArray[17][4]="paymode";
	  iArray[17][5]="17||20";
	  iArray[17][6]="1|0" ;
	  if(fm.LoadFlag.value=="2")
	  {
	  iArray[18]=new Array();  
	  iArray[18][0]="账户修改原因";
	  iArray[18][1]="80px";   
	  iArray[18][2]=200;       
	  iArray[18][3]=1;
	  iArray[18][4]="reasoncode";
	  iArray[18][5]="18|19";              	                //引用代码对应第几列，'|'为分割符
      iArray[18][6]="1|0";
	  
	  iArray[19]=new Array();  
	  iArray[19][0]="原因代码";
	  iArray[19][1]="80px";   
	  iArray[19][2]=200;       
	  iArray[19][3]=1;
	  iArray[19][4]="code"
	  
	  iArray[20]=new Array();  
	  iArray[20][0]="给付金领取方式代码";
	  iArray[20][1]="80px";   
	  iArray[20][2]=200;       
	  iArray[20][3]=1;
	  iArray[20][4]="code"
	  
	  iArray[20]=new Array();  
	  iArray[20][0]="CaseNo";
	  iArray[20][1]="80px";   
	  iArray[20][2]=200;       
	  iArray[20][3]=1;
	  }

	  BnfGrid = new MulLineEnter( "fm" , "BnfGrid" ); 
      //这些属性必须在loadMulLine前
      BnfGrid.mulLineCount = 3;   
      BnfGrid.displayTitle = 1;
      BnfGrid.locked = 0;
      if(fm.LoadFlag.value=="1")
      {
      	BnfGrid.canSel = 0;
      	BnfGrid.canChk = 0;
      }

	  if(fm.LoadFlag.value=="2")
      {
      	BnfGrid.canSel = 0;
      	BnfGrid.canChk = 0;
      	//BnfGrid.selBoxEventFuncName = "showOne";
      }
      BnfGrid.hiddenPlus=0;
      BnfGrid.hiddenSubtraction=0;
      BnfGrid.loadMulLine(iArray);  
     
      }
      
      catch(ex)
      {
        alert(ex);
      }
}
function initAccountGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="客户号码";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
   
    iArray[2]=new Array();
    iArray[2][0]="账户名";
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="银行编码";
    iArray[3][1]="100px";
    iArray[3][2]=60;
    iArray[3][3]=0;
    

    iArray[4]=new Array("银行账号","100px","0","0");;
    iArray[5]=new Array("领取方式","100px","0","0");;
    AccountGrid = new MulLineEnter("fm","AccountGrid");
    AccountGrid.mulLineCount =3;
    AccountGrid.displayTitle = 1;
    AccountGrid.locked = 1;
    AccountGrid.canSel =0;
    AccountGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    AccountGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CaseGrid. selBoxEventFuncName = "onSelSelected";
    AccountGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

</script>