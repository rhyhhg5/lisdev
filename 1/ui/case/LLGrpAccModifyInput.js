//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var theFirstValue="";
var theSecondValue="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		EasyQuery();
	}
}
function checkData()
{
	if(fm.StartDate.value == null || fm.StartDate.value == "" 
		|| fm.EndDate.value == null||fm.EndDate.value=="")
	{
		alert("统计日期不能为空");
		
		return false;
	}
}

//查询函数
function EasyQuery()
{
		var MngCom=fm.MngCom.value;

        var sql="SELECT G.MANAGECOM,G.ACTUGETNO,l.rgtno,G.SUMGETMONEY,G.ACCNAME,(select bankname from ldbank where bankcode=g.bankcode),CASE WHEN G.confdate is not null " +
        		" THEN '是' ELSE '否' END FROM LJAGET G, LLRegister L WHERE G.PAYMODE = '4' AND G.OTHERNO = L.rgtno " +
				   " and G.OTHERNOTYPE = '5' AND l.TogetherFlag in('3','4') and l.rgtno LIKE 'P%'" 
				   + getWherePart('g.actugetno','ActuGetNo')
				   + getWherePart('l.rgtno','OtherNo')
				   + getWherePart('l.handler1','Handler')
				   + getWherePart('g.accname','SearchAccName')
				   + getWherePart('g.bankaccno','SearchAccNo')
				   + getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
				   +" and g.managecom like '"+MngCom+"%' union ";		
			sql = sql + "SELECT G.MANAGECOM,G.ACTUGETNO,l.rgtno,G.SUMGETMONEY,G.ACCNAME,(select bankname from ldbank where bankcode=g.bankcode),CASE WHEN G.confdate is not null " +
        		" THEN '是' ELSE '否' END FROM LJAGET G, LLRegister L, llcase lc WHERE G.PAYMODE = '4' AND G.OTHERNO = Lc.caseno " +
				   " and G.OTHERNOTYPE = '5' AND l.TogetherFlag in('3','4') and l.rgtno LIKE 'P%' and l.rgtno = lc.rgtno" 
				   + getWherePart('g.actugetno','ActuGetNo')
				   + getWherePart('l.rgtno','OtherNo')
				   + getWherePart('l.handler1','Handler')
				   + getWherePart('g.accname','SearchAccName')
				   + getWherePart('g.bankaccno','SearchAccNo')
				   + getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
				   +" and g.managecom like '"+MngCom+"%'";
		sql = sql+"order by managecom with ur";
						
		turnPage.pageLineNum = 10;
		turnPage.queryModal(sql,GetModeGrid);
		var arr = easyExecSql(sql );
		if(!arr)alert("没有数据");
		
}
function submitForm()
{ 
   var chkFlag=false;
   for (i=0;i<GetModeGrid.mulLineCount;i++){
       if(GetModeGrid.getSelNo(i)!='0'){
            chkFlag=true;
       }
  }
   if (chkFlag==false){
       alert("请选中待处理案件！");
       return false;
   }
  //页面提交时设置操作类型为'MODIFY'
  fm.operate.value="MODIFY"	
  
	if(fm.BankCode.value=="")
	{
		alert("对方开户银行不能为空！")
		return; 
	}
	 if (fm.BankAccNo.value=="")
	 {
	 	alert("对方银行帐号不能为空！");
        return false;
  	}
  	if (fm.AccName.value=="")
	 {
	 	alert("对方银行帐户名不能为空！");
        return false;
  	}
 
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "fraSubmit";
  fm.action="./LLGrpAccModifySave.jsp"
  fm.submit();
}
	
	
function query()
{
	  var tsel = GetModeGrid.getSelNo();
	  var otherno =GetModeGrid.getRowColData(tsel-1,2);
	  //alert(otherno);

		var sql ="select a.actugetno ,b.rgtno,a.paymode,(select codename from ldcode "
		          +"where codetype  = 'paymode' and code = a.paymode),a.sumgetmoney,a.Drawer,"
		          +"a.DrawerID,a.BankCode,(select bankname from ldbank "
		          +"where bankcode=a.bankcode),a.BankAccNo,a.AccName, "//11
		          +"paymode,bankonthewayflag,confdate,cansendbank"//15
		          +" from ljaget a ,llregister b where a.actugetno = '"+otherno+"' and a.otherno = b.rgtno union ";
			sql = sql + "select a.actugetno ,b.rgtno,a.paymode,(select codename from ldcode "
		          +"where codetype  = 'paymode' and code = a.paymode),a.sumgetmoney,a.Drawer,"
		          +"a.DrawerID,a.BankCode,(select bankname from ldbank "
		          +"where bankcode=a.bankcode),a.BankAccNo,a.AccName, "//11
		          +"paymode,bankonthewayflag,confdate,cansendbank"//15
		          +" from ljaget a ,llregister b ,llcase c where a.actugetno = '"+otherno+"' and a.otherno = c.caseno and c.rgtno = b.rgtno"; 
		var arr = easyExecSql(sql );
		if(arr)
		{ 
			fm.all('ActuGetNo2').value = arr[0][0];
			fm.all('OtherNo2').value = arr[0][1];
			fm.all('PayMode').value = arr[0][2];
			fm.all('PayModeName').value = arr[0][3];
			fm.all('GetMoney').value = arr[0][4];
			fm.all('Drawer').value = arr[0][5];
			fm.all('DrawerID').value = arr[0][6];
			fm.all('BankCode').value = arr[0][7];
			fm.all('BankCodeName').value = arr[0][8];
			fm.all('BankAccNo').value = arr[0][9];
			fm.all('AccName').value = arr[0][10];
			
			if(arr[0][13]=="" || arr[0][13]==null){
			    
			    if(arr[0][12]=='1'){
			    	alert("银行转账在途中，不可修改付费信息！");
			        fm.ModifyButton.disabled=true;    
			    }else{
			        if(arr[0][11]=='4'){
			        	if(arr[0][14]=='0' || arr[0][14]=='1' || arr[0][14]==''){
			        		fm.ModifyButton.disabled=false;
			        	}     
			        }else{
			        		alert("付费方式不为银行转账，不可修改");
			                fm.ModifyButton.disabled=true;
                    }   
			    }
			}else{
			alert("该批次已经成功转账，不可重复修改");
			fm.ModifyButton.disabled=true;
			}
		}	   
	}	


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
 // EasyQuery();
}

