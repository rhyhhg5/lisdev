<html>
  <%
  //Name:LLDetailInput.jsp
  //Function：
  //Date：2010-08-19 17:44:28
  //Author ：kedy
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLDetailInput.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LLDetailInit.jsp"%>
  </head>
  <body  onload="initForm();">
    <form method=post name=fm target="fraSubmit">
     <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisease);">
            </TD>
            <TD class=titleImg>
              案件基本信息
            </TD>
          </TR>
        </Table>
	<table  class= common>
    <TR  class= common8>
      <TD  class= title8>客户号</TD><TD  class= input8><Input class= readonly name="CustomerNo" readonly></TD>
      <TD  class= title8>客户姓名</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
      <TD  class= title8>客户性别</TD><TD  class= input8><Input class= readonly name="Sex" readonly></TD>
    </TR>
	</table>   
     
      <Div id = "divCheckInfo" style = "display: ''">
        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisease);">
            </TD>
            <TD class=titleImg>
              疾病信息
            </TD>
          </TR>
        </Table>
        <Div id = "divDisease" style = "display: ''">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanDiseaseGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>

        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divSeDisease);">
            </TD>
            <TD class=titleImg>
              重大疾病责任信息
            </TD>
          </TR>
        </Table>
        <Div id = "divSeDisease" style = "display: ''" >
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanSeriousDiseaseGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>

        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divOperation);">
            </td>
            <td class= titleImg>
              手术信息
            </td>
          </tr>
        </table>
        <Div  id= "divOperation" style= "display: ''">
          <table  class= common>
            <tr  class= common>
              <td text-align: left colSpan=1>
                <span id="spanDegreeOperationGrid" >
                </span>
              </td>
            </tr>
          </table>
        </div>

        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divAccident);">
            </td>
            <td class= titleImg>
              意外信息
            </td>
          </tr>
        </table>
        <Div  id= "divAccident" style= "display: ''">
          <table  class= common>
            <tr  class= common>
              <td text-align: left colSpan=1>
                <span id="spanAccidentGrid" >
                </span>
              </td>
            </tr>
          </table>
        </div>

        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDeformity);">
            </TD>
            <TD class=titleImg>
              残疾信息
            </TD>
          </TR>
        </Table>
        <Div id = "divDeformity" style = "display: ''">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanDeformityGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>
        
        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisability);">
            </TD>
            <TD class=titleImg>
              失能信息
            </TD>
          </TR>
        </Table>
        <Div id = "divDisability" style = "display: ''">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanDisabilityGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>           
      </Div>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>