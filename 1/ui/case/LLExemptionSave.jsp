<html>
<%
//程序名称：LLExemptionSave.jsp
//程序功能：豁免保费页面数据处理
//创建日期：2013-12-03 10：10
//创建人  ：Houyd
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  //豁免保费层
  LLExemptionSet tLLExemptionSet = new LLExemptionSet();
  LLExemptionSchema tLLExemptionSchema = new LLExemptionSchema();
    
  LLExemptionBL tLLExemptionBL = new LLExemptionBL();
  TransferData tTransferData = new TransferData();
  //输出参数
  CErrors tError = null;
  String tOperate = request.getParameter("fmtransact");
  System.out.println("tOperate:"+tOperate);
  tOperate = tOperate.trim();
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  String CaseNo = request.getParameter("CaseNo");
  String InsuredNo = request.getParameter("CustomerNo");
  String InsuredName = request.getParameter("CustomerName");
  String Remark = request.getParameter("Remark");
  
  tLLExemptionSchema.setCaseNo(CaseNo);
  tLLExemptionSchema.setInsuredNo(InsuredNo);
  tLLExemptionSchema.setInsuredName(InsuredName);
  tLLExemptionSchema.setRemark(Remark);
  
  if("SAVE".equals(tOperate)) {
    String tChk[] = request.getParameterValues("InpExemptionGridChk");
    //页面数据传入Schema
    String tRiskCode[] = request.getParameterValues("ExemptionGrid2"); 
    String tFreeStartDate[] = request.getParameterValues("ExemptionGrid4");
    String tFreeEndDate[] = request.getParameterValues("ExemptionGrid5");
    String tPayEndDate[] = request.getParameterValues("ExemptionGrid5");
    String tFreePried[] = request.getParameterValues("ExemptionGrid6");
    String tPolNo[] = request.getParameterValues("ExemptionGrid8");
    String tContNo[] = request.getParameterValues("ExemptionGrid9");
    String tPayIntv[] = request.getParameterValues("ExemptionGrid10");
    for (int index=0 ; index<tChk.length ; index++) {
        System.out.println(tChk[index]);
        if(tChk[index].equals("1")) {
          System.out.println(tPolNo[index]);
          LLExemptionSchema mLLExemptionSchema = new LLExemptionSchema();
          mLLExemptionSchema.setRiskCode(tRiskCode[index]);
          mLLExemptionSchema.setFreeStartDate(tFreeStartDate[index]);
          mLLExemptionSchema.setFreeEndDate(tFreeEndDate[index]);
          mLLExemptionSchema.setPayEndDate(tPayEndDate[index]);
          mLLExemptionSchema.setFreePried(tFreePried[index]);
          mLLExemptionSchema.setPolNo(tPolNo[index]);
          mLLExemptionSchema.setContNo(tContNo[index]);
          mLLExemptionSchema.setPayIntv(tPayIntv[index]);
          
          tLLExemptionSet.add(mLLExemptionSchema);
          
        }
    }
  }
  if("CANCEL".equals(tOperate)){
      String tChk[] = request.getParameterValues("InpExemptionGridChk");
      //页面数据传入Schema
      String tRiskCode[] = request.getParameterValues("ExemptionGrid2"); 
      String tFreeStartDate[] = request.getParameterValues("ExemptionGrid4");
      String tFreeEndDate[] = request.getParameterValues("ExemptionGrid5");
      String tPayEndDate[] = request.getParameterValues("ExemptionGrid5");
      String tFreePried[] = request.getParameterValues("ExemptionGrid6");
      String tPolNo[] = request.getParameterValues("ExemptionGrid8");
      String tContNo[] = request.getParameterValues("ExemptionGrid9");
      String tPayIntv[] = request.getParameterValues("ExemptionGrid10");
      for (int index=0 ; index<tChk.length ; index++) {
          System.out.println(tChk[index]);
          if(tChk[index].equals("1")) {
            System.out.println(tPolNo[index]);
            LLExemptionSchema mLLExemptionSchema = new LLExemptionSchema();
            mLLExemptionSchema.setRiskCode(tRiskCode[index]);
            mLLExemptionSchema.setFreeStartDate(tFreeStartDate[index]);
            mLLExemptionSchema.setFreeEndDate(tFreeEndDate[index]);
            mLLExemptionSchema.setPayEndDate(tPayEndDate[index]);
            mLLExemptionSchema.setFreePried(tFreePried[index]);
            mLLExemptionSchema.setPolNo(tPolNo[index]);
            mLLExemptionSchema.setContNo(tContNo[index]);
            mLLExemptionSchema.setPayIntv(tPayIntv[index]);
            
            tLLExemptionSet.add(mLLExemptionSchema);
            
          }
      }
   }
  
  
  //将封装完毕的页面数据放入容器
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLLExemptionSet);	
	tVData.addElement(tLLExemptionSchema);
	tVData.addElement(tTransferData);
	tVData.addElement(tG);
	
  //传入BL层进行处理
  try
  {
      tLLExemptionBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLLExemptionBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	//Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
%>     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>