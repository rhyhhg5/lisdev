<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLClaimerEfficiencyInput.jsp
//程序功能：F1报表生成
//创建日期：2008-07-24
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
   String sql = "select case when current time>='08:00:00' and current time<'17:30:00' then 'Y' else 'N' end from dual"; 
   String sql1 = "select case when dayofweek(current date)=7 or dayofweek(current date)=1 then 'Y' else 'N' end from dual with ur";
   ExeSQL tExeSQL = new ExeSQL();
   String rel = tExeSQL.getOneValue(sql);
   String rel1 = tExeSQL.getOneValue(sql1);
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLClaimerEfficiencyInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title></TD>
          <TD  class= input></TD> 
         </TR>
    </table>
        <table class= common width=100%>
      	<TR  class= common>
		  <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
       	</TR>
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="ClaimerEfficiency()"></TD>
			</tr>
		</table>
	<Input type="hidden" class= common name="RiskRate" >
	<INPUT  type= hidden name="result" value=<%=rel%>>
	<INPUT  type= hidden name="isweek" value=<%=rel1%>>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 