<html>
<%
//程序名称：LLRgtChangePayMode.jsp
//程序功能：团体给付方式修改
//创建日期：2008-4-11
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LLRgtChangePayMode.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLRgtChangePayModeInit.jsp"%>
  <script type="text/javascript">
   var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
  
  </script>
</head>
<body onload="initForm();initElementtype();">
  <form  action='./LLRgtChangePayModeSave.jsp' method=post name=fm target="fraSubmit">
    <table>
      <tr>
	      <td>
  	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
    	  </td>
      	<td class= titleImg>
      	  批次基本信息
      	</td>
    	</tr>
    </table>

  <Div  id= "divLLCase1" style= "display: ''">
		<table  class= common>
			<TR  class= common id= "divRgt" style= "display: ''">
				<TD  class= title8 width=25% >
					团体批次号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=RgtNo >
				</TD>
				<TD  class= title8 colspan=1>
					团体客户号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=GrpNo >
				</TD>
				<TD  class= title8>
					单位名称
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=GrpName  >
				</TD>
			</TR>
			<TR  class= common>
          <TD  class= title8>
            给付方式
          </TD>
          <TD class=input8 >
          	<Input class=codeno name=TogetherFlag  onClick="showCodeList('lltogetherflag',[this,TogetherFlagName],[0,1]);" onkeyup="showCodeListKey('lltogetherflag',[this,TogetherFlagName],[0,1]);" verify="给付方式|notnull&code:lltogetherflag" ><Input class= codename name=TogetherFlagName elementtype=nacessary>
          </TD>
          <TD class= title8 id='titleCaseGetMode1'>
          	领取方式
          </TD>
          <TD class= input8 id='titleCaseGetMode2'>
          	<Input class= codeno name=CaseGetMode onClick="showCodeList('paymode',[this,CaseGetModeName],[0,1],null,str,'1');" onkeyup="showCodeListKey('paymode',[this,CaseGetModeName],[0,1],null,str,'1');" verify="保险金领取方式|code:llgetmode" ><Input class= codename name=CaseGetModeName >
          </TD>
      </TR>
     <tr class= common id='titleBank'>
            <TD class= title8>银行编码</TD>
            <TD class= input8><Input class="codeno"  name=BankCode onclick="return showCodeList('lllbank',[this,BankName],[0,1],null,fm.BankName.value,'bankname',1);" onkeyup="return showCodeListKey('lllbank',[this,BankName],[0,1],null,fm.BankName.value,'bankname',1);"><Input class="codename"  name=BankName></TD>
            <TD class= title8>银行账户</TD>
            <TD class= input8><Input class= common  name=BankAccNo readOnly ></TD>
            <TD class= title8>账户名</TD>
            <TD class= input8><Input class= common  name=AccName readOnly ></TD>
     </tr>
     </table>
     <table>
     	<tr><td></td></tr>
    </table>
    <INPUT VALUE="确  认" TYPE=button name=submitButton class=cssbutton onclick="submitForm();">
    <INPUT VALUE="返  回" TYPE=button class=cssbutton onclick="top.close();"> 
    	<input type=hidden id="fmtransact" name="fmtransact">			
    	<input type=hidden id="LoadFlag" name="LoadFlag">
    	<input type=hidden id="GrpContNo" name="GrpContNo">
          </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
 
</script>
</html>
