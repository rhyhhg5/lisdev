<%
//程序名称：OutsourcingCostSave.jsp
//程序功能：导入提交页面
//创建日期：2013-12-23
//创建人  ：LiuJian
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.lang.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="jxl.*"%>
<%
	String flag = "";
	String content = "";
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	Calendar cal = new GregorianCalendar();
	String year = String.valueOf(cal.get(Calendar.YEAR));
	String month=String.valueOf(cal.get(Calendar.MONTH)+1);
	String date=String.valueOf(cal.get(Calendar.DATE));
	String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
	String min=String.valueOf(cal.get(Calendar.MINUTE));
	String sec=String.valueOf(cal.get(Calendar.SECOND));
	String now = year + month + date + hour + min + sec + "_" ;
	
	String  millis = String.valueOf(System.currentTimeMillis());  
	String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
	
	String tOutXmlPath = application.getRealPath("temp_lp") + "/" + fileName;
	
	System.out.println("OutXmlPath:" + tOutXmlPath);
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
	
  
	//定义列名
	String  strSQLTitle = "select '保单号','本保费分摊费用'  from dual where 1=1 ";
  
	//定义表名
	String Title ="select '本保费分摊费用表 ' from dual where 1=1  ";  
	tTransferData.setNameAndValue("querySql",request.getParameter("querySql"));
	tTransferData.setNameAndValue("querySqlTitle",strSQLTitle);
	tTransferData.setNameAndValue("Title",Title);
	
	
	try
	  {
	      VData vData = new VData();
	  	  vData.add(tG);
	  	  vData.add(tTransferData);
	  	 OutsourcingCostExportUI tOutsourcingCostExportUI=new OutsourcingCostExportUI();
	  	if(!tOutsourcingCostExportUI.submitData(vData,"")){
	  		content = "报表下载失败，原因是:" + tOutsourcingCostExportUI.mErrors.getFirstError();
	  		flag = "Fail";
	          
	  	}else{
	  		 String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
		 	  File file = new File(tOutXmlPath);
		 	  
		        response.reset();
	          response.setContentType("application/octet-stream"); 
	          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
	          response.setContentLength((int) file.length());
	      
	          byte[] buffer = new byte[10000];
	          BufferedOutputStream output = null;
	          BufferedInputStream input = null;    
	          //写缓冲区
	          try 
	          {
	              output = new BufferedOutputStream(response.getOutputStream());
	              input = new BufferedInputStream(new FileInputStream(file));
	        
	          int len = 0;
	          while((len = input.read(buffer)) >0)
	          {
	              output.write(buffer,0,len);
	          }
	          input.close();
	          output.close();
	          }
	          catch (Exception e) 
	          {
	            e.printStackTrace();
	           } // maybe user cancelled download
	          finally 
	          {
	              if (input != null) input.close();
	              if (output != null) output.close();
	              file.delete();
	          }
		   
	  		
	  	}
	  }catch(Exception ex)
		{
		  ex.printStackTrace();
		}
	  
	
%>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>