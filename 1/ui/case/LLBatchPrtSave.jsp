<%
  //程序名称：LLSurveyReplySave.jsp
  //程序功能：
  //创建日期：2005-07-23 11:53:36
  //创建人  ：Xx
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.lang.String"%>
<%@page contentType="text/html;charset=GBK"%>
<%
  //接收信息，并作校验处理。
  //LLBatchPrtBL tLLBatchPrtBL = new LLBatchPrtBL();
   NewLLBatchPrtBL tLLBatchPrtBL = new NewLLBatchPrtBL();
  //输出参数
  CErrors tError = null;
  String tRela = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tPrintServerPath = "";
  int tCount = 0;
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  System.out.println("Start Product XML:" + PubFun.getCurrentTime());
  //tG.ClientIP = request.getRemoteAddr();
  tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
  tG.ServerIP = tG.GetServerIP();
  System.out.println("IP's address :" + tG.ClientIP);
  System.out.println("IP's address :" + tG.ClientIP);
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("transact : " + transact);
  String tRgtNo = request.getParameter("GrpRgtNo");
  String tSCaseNo = request.getParameter("StartCaseNo");
  String tECaseNo = request.getParameter("EndCaseNo");
  String tRgtDateS = request.getParameter("RgtDateS");
  String tRgtDateE = request.getParameter("RgtDateE");
  String tEndDateS = request.getParameter("EndDateS");
  String tEndDateE = request.getParameter("EndDateE");
  String tCaseNoBatch = request.getParameter("SingleCaseNo");
  String tBatchType = request.getParameter("selno");
  String tDetailPrt = request.getParameter("Detail");
  String tGrpDetailPrt = request.getParameter("GrpDetail");
  String tNoticePrt = request.getParameter("Notice");
  String tGetDetailPrt = request.getParameter("GetDetail"); //给付凭证复选框
  String tOutXmlFile = application.getRealPath("");
  
  TransferData PrintElement = new TransferData();
  PrintElement.setNameAndValue("RgtNo", tRgtNo);
  PrintElement.setNameAndValue("SCaseNo", tSCaseNo);
  PrintElement.setNameAndValue("ECaseNo", tECaseNo);
  PrintElement.setNameAndValue("RgtDateS", tRgtDateS);
  PrintElement.setNameAndValue("RgtDateE", tRgtDateE);
  PrintElement.setNameAndValue("EndDateS", tEndDateS);
  PrintElement.setNameAndValue("EndDateE", tEndDateE);
  PrintElement.setNameAndValue("CaseNoBatch", tCaseNoBatch);
  PrintElement.setNameAndValue("BatchType", tBatchType);
  PrintElement.setNameAndValue("DetailPrt", tDetailPrt);
  PrintElement.setNameAndValue("GrpDetailPrt", tGrpDetailPrt);
  PrintElement.setNameAndValue("NoticePrt", tNoticePrt);
  PrintElement.setNameAndValue("GetDetailPrt", tGetDetailPrt); //给付凭证复选框
  
  System.out.println("BatchType" + tBatchType);
  System.out.println("DetailPrt" + tDetailPrt);
  System.out.println("GrpDetailPrt" + tGrpDetailPrt);
  System.out.println("tNoticePrt" + tNoticePrt);
  System.out.println("GetDetailPrt" + tGetDetailPrt);
  
  LDSysVarSchema tLDSysVarSechma = new LDSysVarSchema();
  tLDSysVarSechma.setSysVarValue(tOutXmlFile);
  try {
    VData tVData = new VData();
    tVData.add(tLDSysVarSechma);
    tVData.add(PrintElement);
    tVData.add(tG);
    System.out.println("transact" + transact);
    System.out.println("tVData" + tVData);
    
    if (tLLBatchPrtBL.submitData(tVData, transact)) {
      if (transact.equals("PRINT")) {
        tCount = tLLBatchPrtBL.getCount();
        System.out.println("~~~~~~~~~~~~~~~~~~~" + tCount);
        String tFileName[] = new String[tCount];
        VData tResult = new VData();
        tResult = tLLBatchPrtBL.getResult();
        tFileName = (String[]) tResult.getObject(0);
        String mFileNames = "";
        for (int i = 0; i <= (tCount - 1); i++) {
          System.out.println("~~~~~~~~~~~~~~~~~~~" + i);
          System.out.println(tFileName[i]);
          mFileNames = mFileNames + tFileName[i] + ":";
        }
        System.out.println("=============================================");
        System.out.println("~~~~~~~~~~~~~~~~~~~" + mFileNames);
        System.out.println("=========tFileName.length===========" + tFileName.length);
        String strRealPath = application.getRealPath("/").replace('\\', '/');
        System.out.println(strRealPath);
        String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
        ExeSQL mExeSQL = new ExeSQL();
        tPrintServerPath = mExeSQL.getOneValue(sql);
%>
<html>
	<script language="javascript">
	var printform = parent.fraInterface.document.getElementById("printform");
	printform.elements["filename"].value = "<%=mFileNames%>";
	printform.action = "<%=tPrintServerPath%>";
	printform.submit();
	</script>
</html>
<%
  }
  }
  } catch (Exception ex) {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "") {
    tError = tLLBatchPrtBL.mErrors;
    if (!tError.needDealError()) {
      Content = " 操作成功! ";
      FlagStr = "Success";
    }
    else {
      Content = " 操作失败!原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  //添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
