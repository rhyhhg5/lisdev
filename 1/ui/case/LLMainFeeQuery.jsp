<html>
<%
//Name：LLMainFeeQuery.jsp
//Function：登记界面的初始化
//Date：2005-7-23 16:49:22
//Author：St.GN
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLMainFeeQuery.js"></SCRIPT>
   <%@include file="LLMainFeeQueryInit.jsp"%>
   <script language="javascript">
   function initDate(){
   		fm.RgtDateS.value="<%=afterdate%>";
   		fm.RgtDateE.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
   		
   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
   </script>
 </head>
<body  onload="initDate();initForm();">
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

			<table>
        <TR>
	         <TD class= titleImg>
	         请录入查询条件
	         </TD>
        </TR>
      </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
       	<TR>
					<TD  class= title>客户姓名</TD>
					<TD  class= input><Input class=common8 name=CustomerName onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>客户号</TD>
					<TD  class= input><Input class=common name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>身份证号</TD>
					<TD  class= input><Input class=common name=IDNo onkeydown="QueryOnKeyDown()"></TD>
				</TR>
				<TR  class= common>	
					<TD  class= title>理赔号</TD>
					<TD  class= input> <Input class=common8 name=CaseNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>类型</TD> 
	        <TD  class= input8><Input  CodeData="0|2^0|未结案^1|已结案" onClick="return showCodeListEx('casestate',[this,CaseStateName],[0,1]);" onkeyup="return showCodeListKeyEx('casestate',[this,CaseStateName],[0,1]);" class=codeno name="CaseState" onkeydown="QueryOnKeyDown()"><Input class= codename name=CaseStateName onkeydown="QueryOnKeyDown()"></TD>
	        <TD  class= title8>账单类型</TD>
					<TD  class= input8><input class=codeno name=AccType ondblclick=" showCodeListEx('AccType',[this,AccType_ch],[0,1],null,null,null,1,150)" CodeData="0^1|门诊^2|普通住院^3|手工报销^4|社保住院^0|全部" onkeydown="QueryOnKeyDown();"><input class=codename name=AccType_ch onkeydown="QueryOnKeyDown();"></TD> 
				</TR>
				<TR style="display:'none'">
					<TD  class= title>团体批次号</TD>
					<TD  class= input> <Input class=common8 name=RgtNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>操作员姓名</TD>
					<TD  class= input><Input class= "code8"  name=optname onkeydown="QueryOnKeyDown();"onclick="return showCodeList('optname',[this, Operator], [0, 1],null,fm.optname.value,'username','1');">
					<TD  class= title>操作员代码</TD>
					<TD  class= input><Input class="common"  name=Operator></TD>
					<TD  class= title>管理机构</TD>
					<TD  class= input><Input class= "code8"  name=OrganCode onkeydown="QueryOnKeyDown();" onclick="return showCodeList('stati',[this], [0],null,null,null,'1');" onkeyup="getstr();return showCodeListKey('stati', [this], [0],null,null,null,'1');" >
				</TR>
				<TR>
					<TD  class= title8>受理日期从</TD>
					<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>到</TD>
					<TD  class= input8><input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title></TD>
	        <TD  class= input></TD>
				</TR>
       </table>
     </Div>
     <br><hr>
 <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1 align=center>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
    </Div>
	<table class=common>
	  	<tr class=common>
			<td class=title style="width:60px">		
				历史累计理赔次数
			</td>
			<td class=input >
				<input class=common name=ClaimNum>
			</td>
			<td class=title style="width:55px">		
				历史累计理赔金额
			</td>
			<td class=input >
				<input class=common name=ClaimAmt>
			</td>
			<td>		
				<Div  align=right style= "display: '' ">  
				   <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
				   <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
				   <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
				   <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
				</Div>   
			</td>
		 </tr>
	</table>
	<hr>
	  <table class=common>
      <tr class=common8>
	     	<td class= titleImg>
	        账单列表
	      </td>
	      <td align=right  class=title8><input type="radio" align=right name="typeRadio" checked  value="0" onclick="showDetail();"><b>单账单</b></td><td align=right  class=title8><input type="radio" align=right name="typeRadio"  value="1"  onclick="CalMulAcc();"><b>多账单</b></td>
    	</tr>
    </table>
    <!--Div  id= "divAccount" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1 align=center>
            <span id="spanAccountGrid" >
            </span>
          </TD>
        </TR>
      </table>
	 </Div-->
	 
      <TABLE style=" cellSpacing=0 cellPadding=0 width=300 border=0 SCROLLBAR-FACE-COLOR: #799AE1; SCROLLBAR-HIGHLIGHT-COLOR: #799AE1; SCROLLBAR-SHADOW-COLOR: #799AE1; SCROLLBAR-DARKSHADOW-COLOR: #799AE1; SCROLLBAR-3DLIGHT-COLOR: #799AE1; SCROLLBAR-ARROW-COLOR: #FFFFFF; SCROLLBAR-TRACK-COLOR: #AABFEC ">			
				<TR>
					<TD>
							<DIV id=divAccount name=slightbar style="OVERFLOW:auto; height:120 ">
						      <table  class= common>
						        <TR  class= common>
						          <TD text-align: left colSpan=1 align=center>
						            <span id="spanAccountGrid" >
						            </span>
						          </TD>
						        </TR>
						      </table>
							 </Div>
					</TD>
				</TR>
	</TABLE>
	 
	 
	
		<table  style= "display: 'none'">
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divICaseCure1);">
	      </td>
	      <td class= titleImg>
	        账单信息明细
	      </td>
    	</tr>
    </table>

    <Div  id= "divICaseCure1" style= "display:'none'">
      <table  class= common>
        <TR  class= common style="display:'none'">  
        	<TD  class= title8>客户姓名</TD>
	     		<TD  class= input8> <input class="common" readonly   name=CustomerName_R></TD>
	     		       
	     		<TD  class= title8>客户号码</TD>
	     		<TD  class= input8> <input class="common" readonly   name=CustomerNo_R></TD>
	     		<TD  class= title8>性别</TD>
	     		<TD  class= input8> <input class="common" name="CustomerSex"> </TD>
        </TR>
        <TR  class= common>
	        <TD  class= title8>医院名称</TD>
	        <TD  class= input8> <input readonly class=codeNo  name=HospitalCode ><input readonly class=codeName  name=HospitalName ></TD>
			<TD  class= title8>账单属性</TD>
			<TD  class= input8><input readonly class="codeno" name="FeeAtti" ><input readonly class=codename  name=FeeAttiName ></TD>
			<TD  class= title8>账单种类</TD>
			<TD  class= input8><input readonly class="codeno" name="FeeType" ><input readonly class=codename name=FeeTypeName  ></TD>
		</TR>
		<TR  class= common id='titleC1'>
			<TD  class= title8>账单号码</TD> 
			<TD  class= input8> <input readonly class=common  name=ReceiptNo ></TD>
			<TD  class= title8>帐单类型</TD>
			<TD  class= input8><input readonly class="codeno" name="FeeAffixType" ><input readonly class=codename  name=FeeAffixTypeName></TD>
			<TD  class= title8>结算日期</TD>
			<TD  class= input8> <input readonly class="coolDatePicker"  dateFormat="short"  name=FeeDate ></TD>
		</TR>
		<TR  class= common id='titleC2'>
			<TD  class= title8 id='titleInpatient' style="display:''">入院日期</TD>
			<TD  class= input8 > <input readonly class="coolDatePicker"  dateFormat="short"  name=HospStartDate></TD>
			<TD  class= title8 id='titleOutpatient' style="display:''">出院日期</TD>
			<TD  class= input8 > <input readonly class="coolDatePicker"  dateFormat="short"  name=HospEndDate></TD>
			<TD  class= title8></TD><TD  class = input8 ></TD>
		</TR>
	</table>
    </Div>
    
    <div id=commonZD >
    <table id=divAccDetail style= "display: 'none'">
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseReceipt);">
    		</td>
    		<td class= titleImg>
    			 账单费用项目明细
    		</td>
    	</tr>
    </table>

  <Div  id= "divCaseReceipt" style= "display: ''">

  <div id= DiagFee style="display:'none'">
		  <table class= common>
		  		<tr class = common>
		  			<TD class= title_Acc>西药费</TD>
		  			<TD class= input_Acc><input readonly class="common_A" name=101> <input readonly class="common_A1" name=101r >%</TD> 
		  			<TD class= title_Acc>中成药费</TD>                                                                        
		  			<TD class= input_Acc><input readonly class="common_A" name=102> <input readonly class="common_A1" name=102r >%</TD>
		  			<TD class= title_Acc>门观费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=103> <input readonly class="common_A1" name=103r >%</TD>
		  			<TD class= title_Acc>治疗费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=104> <input readonly class="common_A1" name=104r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>换药费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=105> <input readonly class="common_A1" name=105r >%</TD>
		  			<TD class= title_Acc>诊疗费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=106> <input readonly class="common_A1" name=106r >%</TD>
		  			<TD class= title_Acc>检查费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=107> <input readonly class="common_A1" name=107r >%</TD>
		  			<TD class= title_Acc>心电费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=108> <input readonly class="common_A1" name=108r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>急救费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=109> <input readonly class="common_A1" name=109r >%</TD>
		  			<TD class= title_Acc>B超费</TD>                                                                           
		  			<TD class= input_Acc><input readonly class="common_A" name=110> <input readonly class="common_A1" name=110r >%</TD>
		  			<TD class= title_Acc>放射费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=111> <input readonly class="common_A1" name=111r >%</TD>
		  			<TD class= title_Acc>化验费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=112> <input readonly class="common_A1" name=112r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>手术费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=113> <input readonly class="common_A1" name=113r >%</TD>
		  			<TD class= title_Acc>注射费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=114> <input readonly class="common_A1" name=114r >%</TD>
		  			<TD class= title_Acc>体检费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=115> <input readonly class="common_A1" name=115r >%</TD>
		  			<TD class= title_Acc>卫材费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=116> <input readonly class="common_A1" name=116r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>氧气费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=117> <input readonly class="common_A1" name=117r >%</TD>
		  			<TD class= title_Acc>其他费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=118> <input readonly class="common_A1" name=118r >%</TD>
		  			<TD class= title_Acc>自付</TD>                                                                            
		  			<TD class= input_Acc><input readonly class="common_A" name=119> <input readonly class="common_A1" name=119r >%</TD>
		  			<TD class= title_Acc>救护车费</TD>                                 
		  			<TD class= input_Acc><input readonly class="common_A" name=120> <input readonly class="common_A1" name=120r >%</TD>
		  		</tr>
				<tr class = common>                                                                                         
		  			<TD class= title_Acc>中草药费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=121> <input readonly class="common_A1" name=121r >%</TD>
		  			<TD class= title_Acc>挂号费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=122> <input readonly class="common_A1" name=122r >%</TD>
		  			<TD class= title_Acc>煎药费</TD>                                                                            
		  			<TD class= input_Acc><input readonly class="common_A" name=123> <input readonly class="common_A1" name=123r >%</TD>
		  			<TD class= title_Acc>材料费</TD>                                 
		  			<TD class= input_Acc><input readonly class="common_A" name=124> <input readonly class="common_A1" name=124r >%</TD>
		  		</tr>
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>出诊费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=125> <input readonly class="common_A1" name=125r >%</TD>
		  			<TD class= title_Acc>医疗费用</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=151> <input readonly class="common_A1" name=151r >%</TD>
		  			<TD class= title_Acc>自费购药费用</TD>                                                                            
		  			<TD class= input_Acc><input readonly class="common_A" name=152> <input readonly class="common_A1" name=152r >%</TD>
		  			<TD class= title_Acc>保健品费用</TD>                                 
		  			<TD class= input_Acc><input readonly class="common_A" name=153> <input readonly class="common_A1" name=153r >%</TD>
		  		</tr>
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>医疗器械费用</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=154> <input readonly class="common_A1" name=154r >%</TD>
		  			<TD class= title_Acc>康复费用</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=155> <input readonly class="common_A1" name=155r >%</TD>
		  			<TD class= title_Acc>疗养费用</TD>                                                                            
		  			<TD class= input_Acc><input readonly class="common_A" name=156> <input readonly class="common_A1" name=156r >%</TD>
		  			<TD class= title_Acc>整容费用</TD>                                 
		  			<TD class= input_Acc><input readonly class="common_A" name=157> <input readonly class="common_A1" name=157r >%</TD>
		  		</tr>
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>体检费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=158> <input readonly class="common_A1" name=158r >%</TD>
		  			<TD class= title_Acc>健身费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=159> <input readonly class="common_A1" name=159r >%</TD>
		  			<TD class= title_Acc>健康护理费</TD>                                                                            
		  			<TD class= input_Acc><input readonly class="common_A" name=160> <input readonly class="common_A1" name=160r >%</TD>
		  			<TD class= title_Acc>健康培训费</TD>                                 
		  			<TD class= input_Acc><input readonly class="common_A" name=161> <input readonly class="common_A1" name=161r >%</TD>
		  		</tr>	
		  		<tr class = common>           
		  			<TD class= title_Acc>合计</TD>                                 
		  			<TD class= input_Acc><input readonly class="common" name=DiagSum style="width:111px"></TD>	  		
		  		</tr>
		  </table>
	</Div>
	
	<div id= HospitalFee style="display:'none'">
		  <table class= common>
		  		<tr class = common>
		  			<TD class= title_Acc>床位费</TD>
		  			<TD class= input_Acc><input readonly class="common_A" name=201> <input readonly class="common_A1" name=201r >%</TD>
		  			<TD class= title_Acc>西药费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=202> <input readonly class="common_A1" name=202r >%</TD>
		  			<TD class= title_Acc>中成药</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=203> <input readonly class="common_A1" name=203r >%</TD>
		  			<TD class= title_Acc>中草药</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=204> <input readonly class="common_A1" name=204r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>检查费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=205> <input readonly class="common_A1" name=205r >%</TD>
		  			<TD class= title_Acc>治疗费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=206> <input readonly class="common_A1" name=206r >%</TD>
		  			<TD class= title_Acc>放射费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=207> <input readonly class="common_A1" name=207r >%</TD>
		  			<TD class= title_Acc>手术费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=208> <input readonly class="common_A1" name=208r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>化验费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=209> <input readonly class="common_A1" name=209r >%</TD>
		  			<TD class= title_Acc>输血费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=210> <input readonly class="common_A1" name=210r >%</TD>
		  			<TD class= title_Acc>输氧费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=211> <input readonly class="common_A1" name=211r >%</TD>
		  			<TD class= title_Acc>护理费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=212> <input readonly class="common_A1" name=212r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>诊疗费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=213> <input readonly class="common_A1" name=213r >%</TD>
		  			<TD class= title_Acc>B超费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=214> <input readonly class="common_A1" name=214r >%</TD>
		  			<TD class= title_Acc>接生费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=215> <input readonly class="common_A1" name=215r >%</TD>
		  			<TD class= title_Acc>婴儿费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=216> <input readonly class="common_A1" name=216r >%</TD>
		  		</tr>                                                                                                       
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>麻醉费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=217> <input readonly class="common_A1" name=217r >%</TD>
		  			<TD class= title_Acc>取暖费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=218> <input readonly class="common_A1" name=218r >%</TD>
		  			<TD class= title_Acc>陪床费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=219> <input readonly class="common_A1" name=219r >%</TD>
		  			<TD class= title_Acc>空调费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=220> <input readonly class="common_A1" name=220r >%</TD>
		  		</tr>
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>材料费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=221> <input readonly class="common_A1" name=221r >%</TD>
		  			<TD class= title_Acc>其他</TD>                                                                              
		  			<TD class= input_Acc><input readonly class="common_A" name=222> <input readonly class="common_A1" name=222r >%</TD>
		  			<TD class= title_Acc>病历费</TD>                                                                                     
		  			<TD class= input_Acc><input readonly class="common_A" name=223> <input readonly class="common_A1" name=223r >%</TD>
		  			<TD class= title_Acc>煎药费</TD>                                                                          
		  			<TD class= input_Acc><input readonly class="common_A" name=224> <input readonly class="common_A1" name=224r >%</TD>
		  		</tr>
		  		<tr class = common>                                                                                         
		  			<TD class= title_Acc>注射费</TD>                                                                             
		  			<TD class= input_Acc><input readonly class="common_A" name=225> <input readonly class="common_A1" name=225r >%</TD>
		  			<TD class= title_Acc>器官购置</TD>                                                                              
		  			<TD class= input_Acc><input readonly class="common_A" name=226> <input readonly class="common_A1" name=226r >%</TD>
		  			<TD class= title_Acc>合计</TD>
		  			<TD class= input_Acc><input readonly class="common" name=HospitalSum style="width:111px"></TD>
		  		</tr>
		  </table>
	</Div>
</div>
<div id= AfterCal style="display:'none'">
		  <table class= common>
		  		<tr class = common>
		  			<TD class= title_Acc1>医疗保险范围内金额</TD>
		  			<TD class= input_Acc1><input readonly class="common_A" name=301> <input readonly class="common_A1" name=301r >%</TD> 
		  			<TD class= title_Acc1>医疗保险范围外金额</TD>                                                                    
		  			<TD class= input_Acc1><input readonly class="common_A" name=302> <input readonly class="common_A1" name=302r >%</TD>
		  			<TD class= title_Acc1>账户支付金额</TD>                                                                      
		  			<TD class= input_Acc1><input readonly class="common_A" name=303> <input readonly class="common_A1" name=303r >%</TD>
		  			                                                                                                           
		  		</tr>                                                                                                        
		  		<tr class = common>                                                                                          
		  			<TD class= title_Acc1>大额医疗互助支付金额</TD>                                                                      
		  			<TD class= input_Acc1><input readonly class="common_A" name=304> <input readonly class="common_A1" name=304r >%</TD>
		  			<TD class= title_Acc1>统筹基金支付金额</TD>                                                                         
		  			<TD class= input_Acc1><input readonly class="common_A" name=305> <input readonly class="common_A1" name=305r >%</TD>
		  			<TD class= title_Acc1>现金支付自费金额</TD>                                                                         
		  			<TD class= input_Acc1><input readonly class="common_A" name=306> <input readonly class="common_A1" name=306r >%</TD>
		  			                                                                                                           
		  		</tr>                                                                                                        
		  		<tr class = common>                                                                                          
		  			<TD class= title_Acc1>自付一</TD>                                                                          
		  			<TD class= input_Acc1><input readonly class="common_A" name=307> <input readonly class="common_A1" name=307r >%</TD>
		  			<TD class= title_Acc1>自付二</TD>                                                                          
		  			<TD class= input_Acc1><input readonly class="common_A" name=308> <input readonly class="common_A1" name=308r >%</TD>
		  			<TD class= title_Acc1>公务员医疗补助支付金额</TD>                                                                         
		  			<TD class= input_Acc1><input readonly class="common_A" name=309> <input readonly class="common_A1" name=309r >%</TD>
		  			                                                                                                           
		  		</tr>                                                                                                       

		  </table>
	</Div>




<DIV id=divsecurity style='display:none'>
    <table>
    	<tr>
    		<td class= titleImg>
    			 社保账单申报内容
    		</td>
    	</tr>
    </table>
<table class= common cellspacing=0 cellpadding=0>
  <tr class=common height="20">
    <td id=titleMZ class= gridtitle1 rowspan="2">门诊</td>
    <td id=titleZY class= gridtitle1 rowspan="2" style="display:'none'">住院</td>
    <td class= gridtitle0>申报金额</td>
    <td id=titleFeeStart class= gridtitle0>发生费用时间</td>
    <td id=titleApply class= gridtitle0>申报时间</td>
    <td id=titleConDays class= gridtitle0>连续天数</td>
    <td id=titleInHos class= gridtitle0 style="display:'none'">入院时间</td>
    <td id=titleOutHos class= gridtitle0 style="display:'none'">出院时间</td>
    <td id=titleDays class= gridtitle0 style="display:'none'">住院天数</td>
    <td class= gridtitle0>医保支付金额</td>
    <td class= gridtitle0>医保拒付金额</td>
  </tr>
  <tr class=common >
    <td class= grid0 ><input class=common9 name=ApplyAmnt></td>
    <td class= grid0 ><input class=common9 name=FeeStart></td>
    <td class= grid0 ><input class=common9 name=FeeEnd></td>
    <td class= grid0 ><input class=common9 name=FeeDays></td>
    <td class= grid0 ><input class=common9 name=FeeInSecu></td>
    <td class= grid0 ><input class=common9 name=FeeOutSecu></td>
  </tr>        
  <tr class=common >
		<TD  class= gridtitle1 colspan=3>本次报销前，门诊大额年度累计支付金额</TD>
    <td class= gridtitle0 colspan="2">申报金额合计</td>
    <td class= gridtitle0>报销单据张数</td>
    <td class= gridtitle0>拒付单据张数</td>
  </tr>
	<TR class= common id='titleS3' style="display:''">
		<TD  class= gridtitle colspan=3 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=TotalSupDoorFee></TD>
    <td class= grid0 colspan=2 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=TotalApplyAmnt></td>
    <td class= grid0 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=PayReceipt></td>
    <td class= grid0 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=RefuseReceipt></td>
	</TR>
</table>
    <table>
    	<tr>
    		<td class= titleImg>
    			 医保支付金额
    		</td>
    	</tr>
    </table>
<table class= common cellspacing=0 cellpadding=0>
  <tr class=common >
    <td class= gridtitle1 colspan="2">门诊大额医疗费用</td>
    <td class= gridtitle0 colspan="2">统筹基金医疗费用</td>
    <td class= gridtitle0 colspan="2">住院大额医疗费用</td>
    <td class= gridtitle0 rowspan="2">医保支付合计</td>
  </tr>
  <tr class=common >
    <td class= gridtitle1 >本次支付金额</td>
    <td class= gridtitle0>年度内累计支付金额</td>
    <td class= gridtitle0>本次支付金额</td>
    <td class= gridtitle0>年度内累计支付金额</td>
    <td class= gridtitle0>本次支付金额</td>
    <td class= gridtitle0>年度内累计支付金额</td>
  </tr>        
  <tr class=common >
    <td class= gridtitle style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SupDoorFee></td>
    <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=YearSupDoorFee></td>
    <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=PlanFee></td>
    <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=YearPlayFee></td>
    <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SupInHosFee></td>
    <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=YearSupInHosFee></td>
    <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SecurityFee></td>
  </tr>
</table>     
    <table>
    	<tr>
    		<td class= titleImg>
    			 医保不予支付金额
    		</td>
    	</tr>
    </table>
    <table class= common cellspacing=0 cellpadding=0>
  <tr class=common >
    <td class= gridtitle1 colspan="2">自付一小计</td>
    <td class= gridtitle0 colspan="2">自付二小计</td>
    <td class= gridtitle0 colspan="2">自费小计</td>
    <td class= gridtitle0 >合计</td>
  </tr>
  <tr class=common >
    <td class= gridtitle colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SelfPay1></td>
    <td class= grid0 colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SelfPay2></td>
    <td class= grid0 colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SelfAmnt></td>
    <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SecuRefuseFee></td>
  </tr>
</table>
</Div>
	<hr>
		<input type=button class=cssButton value="理赔帐单查询" OnClick="ShowInfoPage();">
		<input type=button class=cssButton value="保单查询" OnClick="ContInfoPage();">
		<!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="ComCode"  value="<%=Comcode%>">
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
