<%
//程序名称：PayAffirmInit.jsp
//程序功能：给付确认的初始化
//创建日期：
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
%>                            

<script language="JavaScript">
function initForm()
{
  try
  {
		initClaimPolGrid();
		initBnfGrid();
  }
  catch(re)
  {
    alert("ProposalInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initClaimPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         			//列名
      iArray[1][1]="150px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[2]=new Array();
      iArray[2][0]="险种";         			//列名
      iArray[2][1]="60px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="分案号";         			//列名
      iArray[3][1]="0px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[4]=new Array();
      iArray[4][0]="事故者客户号";         			//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="事故者名称";         			//列名
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="保单生效日期";         			//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="核算赔付金额";         			//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="实际赔付金额";         			//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      ClaimPolGrid = new MulLineEnter( "fm" , "ClaimPolGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimPolGrid.mulLineCount = 0;   
      ClaimPolGrid.displayTitle = 1;
      //ClaimPolGrid.canSel=1;
      //ClaimPolGrid.canChk=0;
      ClaimPolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initBnfGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("客户名称","100px","0",1);
   iArray[2]=new Array("与被保险人关系","100px","0",2,"Relaction");
   iArray[3]=new Array("赔付方式","80px","0",2);
   iArray[4]=new Array("银行代码","80px","0",2);
   iArray[5]=new Array("账号","100px","0",1);
   iArray[6]=new Array("户名","100px","0",1);

    BnfGrid = new MulLineEnter("fm","BnfGrid");
    BnfGrid.mulLineCount = 1;
    BnfGrid.displayTitle = 1;
    BnfGrid.locked = 0;
    BnfGrid.canChk =1;
    BnfGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    BnfGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   BnfGrid. selBoxEventFuncName = "onSelSelected";
    BnfGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}


</script>