<%
//Name：LLRegisterCheckInit.jsp
//Function：检录界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：wujs
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
String LoadC = "";
if(request.getParameter("LoadC")!=null){
LoadC = request.getParameter("LoadC");
}
String LoadD="";
if(request.getParameter("LoadD")!=null){
	LoadD = request.getParameter("LoadD");
}

String tShowCaseRemarkFlag = "";//
tShowCaseRemarkFlag = (String)session.getAttribute("ShowCaseRemarkFlag");
System.out.println("tShowCaseRemarkFlag:"+tShowCaseRemarkFlag);
%>

<script language="JavaScript">
  function initInpBox(){
    try {
      fm.reset();
      <%GlobalInput mG = new GlobalInput();
      mG=(GlobalInput)session.getValue("GI");
      %>
      
      var tCaseNo ="<%=request.getParameter("CaseNo")%>"
      fm.CaseNo.value = tCaseNo=='null'?'':tCaseNo;
      fm.Handler.value = "<%=mG.Operator%>";
      fm.cOperator.value ="<%=mG.Operator%>"; // #3409 
      fm.ModifyDate.value = getCurrentDate();
      var strSQL3="select enddate,operator from llcaseoptime where rgtstate='04' and caseno='"
      +fm.CaseNo.value+"'order by sequance desc ";

      var arrResult3 = easyExecSql(strSQL3);
      if(arrResult3 != null && arrResult3 !="" ){
        fm.ModifyDate.value = arrResult3[0][0];
        fm.Handler.value = arrResult3[0][1];
      }
      if(fm.CaseNo.value!=''){
      	var tsql = "select riskcode from llcase where caseno ='"+fm.CaseNo.value+"'"
      	var riskinfo = easyExecSql(tsql);
      	if(riskinfo){
      		fm.RiskCode.value = riskinfo[0][0];
      	}
      }
    }
    catch(ex) {
      alert("在LLRegisterCheckInit.jsp-->initInpBox函数中发生异常:初始化界面错误!"+ex.message);
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在LLRegisterCheckInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm()
  {
    try{
      initInpBox();
      fm.ShowCaseRemarkFlag.value = "<%=tShowCaseRemarkFlag%>";
      fm.LoadC.value="<%=LoadC%>";
      if (fm.LoadC.value=='2'){
        divconfirm.style.display='none';
        divconfevent.style.display='none';
        fm.confirm.style.display='none';
        Contral.style.display='none';
        ConStep.style.display='none';
      }
      fm.LoadD.value="<%=LoadD%>";
       if(fm.LoadD.value=='1')
     {
       document.getElementById('sub1').disabled = true;
       document.getElementById('sub2').disabled = true;
       document.getElementById('sub3').disabled = true;
      //  document.getElementById('sub4').disabled = true;
       document.getElementById('sub5').disabled = true;
       document.getElementById('sub6').disabled = true;
       document.getElementById('sub7').disabled = true;
       document.getElementById('sub8').disabled = true;
     }
      initInsuredEventGrid(0);
      initICaseCureGrid();
      initTitle();
      queryEventInfo();
      showFeeRela();
      ////出险内容

      initDiseaseGrid();
      initDeformityGrid();
      initInvalidismGrid();//伤残信息初始化
      initSeriousDiseaseGrid();
      initMildGrid();
      initOtherFactorGrid();
      initDegreeOperationGrid();
      initAccidentGrid();
      initCasePolicyGrid();
      initSDiseaseProofGrid("重疾依据名称");
      initDisabilityGrid();
      easyQueryPolicy();
      initMDiseaseProofGrid("轻症依据名称");
      
      showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出
    }
    catch(re){
      alert("在LLRegisterCheckInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
    }
  }

//初始化事件列表
  function initInsuredEventGrid(allevent){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","0px","10","0");
      
        iArray[1]=new Array();
        iArray[1][0]="事件号";
        iArray[1][1]="100px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="Subno";
      
      iArray[2]=new Array("发生日期","80px","10","1");
      iArray[2][9]="发生日期|notnull&date";

      iArray[3]=new Array("发生地点(省)","90px","100","2");
      iArray[3][4]="province1";
      iArray[3][5]="3|13";
      iArray[3][6]="1|0";
      
      iArray[4] =new Array("发生地点(市)","90px","100","2");
      iArray[4][4]="city1";
      iArray[4][5]="4|14";
      iArray[4][6]="1|0";
      iArray[4][15]="code1";
      iArray[4][17]="13";
      
      iArray[5] = new Array("发生地点(县)","90px","100","2");
      iArray[5][4]="county1";
      iArray[5][5]="5|15";
      iArray[5][6]="1|0";
      iArray[5][15]="code1";
      iArray[5][17]="14";
		
      iArray[6] = new Array("发生地点","150px","100","1")
      iArray[7]=new Array("入院日期","80px","100","1");
      iArray[7][9]="入院日期|date";
      
      iArray[8]=new Array("出院日期","80px","100","1");
      iArray[8][9]="出院日期|date";
      
      iArray[9] = new Array("事件信息","200px","1000","1");
      iArray[10] = new Array("事件类型","60px","10","2")
      iArray[10][10]="AccType";
      iArray[10][11]="0|^1|疾病|^2|意外"
      iArray[10][12]="10|11"
      iArray[10][13]="1|0"

      iArray[11] = new Array("事件类型","60px","10","3")
      iArray[12] = new Array("CaseRelaNo","0px","30","3")
      iArray[13] = new Array("发送地点省编码","60px","10","3")
      iArray[14] = new Array("发生地点市编码","60px","10","3")
	  iArray[15] = new Array("发生地点县编码","60px","10","3")  
      
      
      InsuredEventGrid = new MulLineEnter("fm","InsuredEventGrid");
      InsuredEventGrid.mulLineCount = 1;
      InsuredEventGrid.displayTitle = 1;
      InsuredEventGrid.locked = 0;
      InsuredEventGrid.canChk =allevent;
      InsuredEventGrid.canSel =1;
      InsuredEventGrid.hiddenPlus=0;
      InsuredEventGrid.hiddenSubtraction=1;
      InsuredEventGrid.selBoxEventFuncName = "SelectICaseCureGrid";
      InsuredEventGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter("在InsuredEventGrid中出错"+ex.message);
    }
  }

  //初始化帐单费用列表信息
  function initICaseCureGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("账单收据号","100px","100","0");
      iArray[2]=new Array("医院名称","90px","100","0");
      iArray[2][21] = "HospitalName";
      iArray[3]=new Array("账单日期","120px","10","0");
      iArray[4] = new Array("受理事故号","150","100","3");
      iArray[5] = new Array("账单号","100","100","3");
      iArray[6] = new Array("事件号","100","100","3");
      iArray[7] = new Array("账单金额","100","100","1");
      iArray[8] = new Array("医院编码","100","100","3");
      iArray[8][21] = "HospitalCode";

      ICaseCureGrid = new MulLineEnter( "fm" , "ICaseCureGrid" );
      ICaseCureGrid.mulLineCount = 1;
      ICaseCureGrid.displayTitle = 1;
      ICaseCureGrid.canChk = 1;
      ICaseCureGrid.hiddenPlus=1;
      ICaseCureGrid.hiddenSubtraction=1;
      ICaseCureGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在DiseaseGrid中出错"+ex.message);
    }
  }

//初始化疾病信息列表
  function initDiseaseGrid(){
    var iArray = new Array();
    try{
        iArray[0]=new Array("序号","30px","10","3");
        iArray[1]=new Array("临床诊断","100px","100",1);
        iArray[2]=new Array("疾病","80px","100",3);
        iArray[3]=new Array("疾病名称","120px","100",1);

        iArray[4]=new Array("疾病代码","80px","100",2);
        iArray[4][4]="lldiseas"
        iArray[4][5]="3|4|5|8|9";
        iArray[4][6]="0|1|2|3|4";
        iArray[4][15]="ICDName"
        iArray[4][17]="3"
        iArray[4][18]="400";
        iArray[4][19]="1";
        iArray[5]=new Array("肿瘤标志","80px","100",3);

        iArray[6]=new Array("肿瘤形态学名称","120px","100",1);

        iArray[7]=new Array("肿瘤形态学代码","120px","100",2);
        iArray[7][4]="tumour"
        iArray[7][5]="6|7";
        iArray[7][6]="0|1";
        iArray[7][15]="LDName"
        iArray[7][17]="6"
        iArray[7][18]="400";
        iArray[7][19]="1";
        
           
        iArray[8]=new Array("风险等级","60px","100",0);
        iArray[9]=new Array("风险存续期","60px","100",0);

        iArray[10]=new Array("诊治医院","105px","100",2,"llhospiquery","10|11","0|1");
        iArray[10][15]="Hospitname";
        iArray[10][17]="10";
        iArray[10][19]=1;
        iArray[11]=new Array("诊治医院名称","140px","100",1);

        iArray[12]=new Array("治疗医生","105px","100",1);
        iArray[13]=new Array("序号","300px","0",3);
        iArray[14]=new Array("治疗医生代码","120px","100",3);

        iArray[15]=new Array("手术","50px","30",2);
        iArray[15][10]="IsOps";
        iArray[15][11]="0|^1|有|^0|无"
        iArray[15][12]="15|16"
        iArray[15][13]="1|0"
        iArray[16] = new Array("有无手术","60px","10","3")

        DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
        DiseaseGrid.mulLineCount = 0;
        DiseaseGrid.displayTitle = 1;
        DiseaseGrid.addEventFuncName="setHosInfo";
        DiseaseGrid.loadMulLine(iArray);

    }
    catch(ex){
      alert("在DiseaseGrid中出错"+ex.message);
    }
  }
  
  //初始化重疾列表
  function initSeriousDiseaseGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","3");
      iArray[1]=new Array("重疾名称","120px","100","1");
      iArray[2]=new Array("重疾代码","60px","160","2");
      iArray[2][4]="llseriousdiseas";
      iArray[2][5]="2|1";
      iArray[2][6]="0|1";
      iArray[2][9]="疾病代码|NOTNULL";
      iArray[2][15]="name"
      iArray[2][17]="1"
      iArray[2][19]="1";

      iArray[3]=new Array("诊治医院名称","120px","100",3,"hospital","3|4","1|0");
      iArray[4]=new Array("诊治医院代码","100px","100",3);
      iArray[5]=new Array("治疗医生","60px","100",1);
      iArray[6]=new Array("重疾责任依据编码","300px","0",3);
      iArray[7]=new Array("重症监护天数","80px","4",3);
      iArray[8]=new Array("重疾责任标志","20px","100",3);
      iArray[9]=new Array("重疾责任依据","320px","100",0);
      iArray[9][7]="OpenProof";
      iArray[10]=new Array("重疾责任","50","100",0);
      iArray[11]=new Array("确诊时间","100px","100",1);

      SeriousDiseaseGrid = new MulLineEnter( "fm" , "SeriousDiseaseGrid" );
      SeriousDiseaseGrid.mulLineCount = 1;
      SeriousDiseaseGrid.displayTitle = 1;
      SeriousDiseaseGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在SeriousDiseaseGrid中出错"+ex.message);
    }
  }
  
//初始化轻症列表
  function initMildGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","3");
      iArray[1]=new Array("轻症名称","120px","100","1");
      iArray[2]=new Array("轻症代码","60px","160","2");
      iArray[2][4]="llmdseriousdiseas";
      iArray[2][5]="2|1";
      iArray[2][6]="0|1";
      iArray[2][9]="疾病代码|NOTNULL";
      iArray[2][15]="name"
      iArray[2][17]="1"
      iArray[2][19]="1";

      iArray[3]=new Array("诊治医院名称","120px","100",3,"hospital","3|4","1|0");
      iArray[4]=new Array("诊治医院代码","100px","100",3);
      iArray[5]=new Array("治疗医生","60px","100",1);
      iArray[6]=new Array("轻症责任依据编码","300px","0",3);
      iArray[7]=new Array("轻症监护天数","80px","4",3);
      iArray[8]=new Array("轻症责任标志","20px","100",3);
      iArray[9]=new Array("轻症责任依据","320px","100",0);
      iArray[9][7]="OpenMDProof";
      iArray[10]=new Array("轻症责任","50","100",0);
      iArray[11]=new Array("确诊时间","100px","100",1);

      MildGrid = new MulLineEnter( "fm" , "MildGrid" );
      MildGrid.mulLineCount = 1;
      MildGrid.displayTitle = 1;
      MildGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在MildGrid中出错"+ex.message);
    }
  }
  
  //初始化残疾信息列表
  function initDeformityGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","3");
      iArray[1]=new Array("残疾名称","200px","100",1);

      iArray[2]=new Array("残疾代码","80px","100",2,"desc_wound","2|1|3|4","0|1|3|4");
      iArray[2][19]=1;

      iArray[3]=new Array("残疾级别","80px","100",0);
      iArray[4]=new Array("给付比例","80px","100",0);
      iArray[5]=new Array("确认依据","120px","100",3);
      iArray[6]=new Array("序号","300px","0",3);
      iArray[7]=new Array("鉴定日期","100px","100",1);
      iArray[7][9]="鉴定日期|notnull&date";
      iArray[8]=new Array("合法认证机构","110px","20",3);
      iArray[9]=new Array("合法认证机构代码","300px","0",3);

      DeformityGrid = new MulLineEnter( "fm" , "DeformityGrid" );
      DeformityGrid.mulLineCount = 1;
      DeformityGrid.displayTitle = 1;
      DeformityGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在DeformityGrid中出错"+ex.message);
    }
  }

  //初始化伤残信息列表
  function initInvalidismGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","3");
      iArray[1]=new Array("伤残名称","200px","100",1);

      iArray[2]=new Array("伤残代码","250px","100",2,"desc_invalidism","2|1|3|4|5","1|0|2|3|4");
      iArray[2][15]="codeName";
      iArray[2][17]="1";
      iArray[2][18]=500;
      iArray[2][19]=1;	//1是需要强制刷新
	  iArray[3]=new Array("伤残代码描述","200px","0",3);
      iArray[4]=new Array("伤残级别","80px","100",0);
      iArray[5]=new Array("给付比例","80px","100",0);
      iArray[6]=new Array("确认依据","120px","100",3);      
      iArray[7]=new Array("鉴定日期","100px","100",1);
      iArray[7][9]="鉴定日期|notnull&date";
      iArray[8]=new Array("合法认证机构","110px","20",3);
      iArray[9]=new Array("合法认证机构代码","300px","0",3);

      InvalidismGrid = new MulLineEnter( "fm" , "InvalidismGrid" );
      InvalidismGrid.mulLineCount = 1;
      InvalidismGrid.displayTitle = 1;
      InvalidismGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在InvalidismGrid中出错"+ex.message);
    }
  }
  
  //初始化手术列表信息
  function initDegreeOperationGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","10","3");
      iArray[1]=new Array("手术名称","200px","100","1");

      iArray[2]=new Array("手术代码","150px","100","2");
      iArray[2][4]="lloperation";
      iArray[2][5]="2|1|4";
      iArray[2][6]="1|0|2";
      iArray[2][15]="Icdopsname";
      iArray[2][17]="1";
      iArray[2][19]=1

      iArray[3]=new Array("手术费用","90px","100","3");
      iArray[4]=new Array("手术等级","180px","100","1");
      iArray[5]=new Array("序号","300px","0",3);

      DegreeOperationGrid = new MulLineEnter( "fm" , "DegreeOperationGrid" );
      DegreeOperationGrid.mulLineCount = 1;
      DegreeOperationGrid.displayTitle = 1;
      DegreeOperationGrid.canSel = 0;
      DegreeOperationGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在DegreeOperation中出错"+ex.message);
    }
  }

//初始化其他录入要素列表  
  function initOtherFactorGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","3");
      iArray[1]=new Array("要素类型","30px","10","2");
      iArray[1][4]="llotherfactor";
      iArray[1][5]="1|6";
      iArray[1][6]="1|0";

      iArray[2]=new Array("要素编号","10px","20","3");

      iArray[3]=new Array("要素","100px","10","2");
      iArray[3][4]="llfactor";
      iArray[3][5]="3|2";
      iArray[3][6]="0|1";
      iArray[3][15]="要素类型";
      iArray[3][17]="6";

      iArray[4]=new Array("要素值","20px","15","1");
      iArray[5]=new Array("备注","60px","20","1");
      iArray[6]=new Array("要素类型代码","20px","15","3");

      OtherFactorGrid = new MulLineEnter( "fm" , "OtherFactorGrid" );
      OtherFactorGrid.mulLineCount = 1;
      OtherFactorGrid.displayTitle = 1;
      OtherFactorGrid.canSel = 0;
      OtherFactorGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在OtherFactor中出错"+ex.message);
    }
  }

//初始化关联保单信息
  function initCasePolicyGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","0",0);
      iArray[1]=new Array("保单号码","100px","0",0);
      iArray[2]=new Array("被保险人姓名","100px","0",0);
      iArray[3]=new Array("险种代码","100px","0",0);
      iArray[4]=new Array("保险金额","100px","0",0);
      iArray[5]=new Array("生效日期","180px","0",0);
      iArray[6]=new Array("险种号","180px","0",3);

      CasePolicyGrid = new MulLineEnter( "fm" , "CasePolicyGrid" );
      CasePolicyGrid.displayTitle = 1;
      CasePolicyGrid.mulLineCount = 0;
      CasePolicyGrid.hiddenPlus=1;
      CasePolicyGrid.hiddenSubtraction=1;
      CasePolicyGrid.loadMulLine(iArray);
      CasePolicyGrid.lock ();
    }
    catch(ex){
      alert("在CasePolicy中出错"+ex.message);
    }
  }

//初始化意外信息列表
  function initAccidentGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10",3);
      iArray[1]=new Array("意外序号","100px","10",3);
      iArray[2]=new Array("意外类型","100px","100",3);
      iArray[3]=new Array("意外类型名称","150px","100",3);
      iArray[4]=new Array("意外查询","80px","100",3);
      iArray[5]=new Array("意外名称","300px","100",1);

      iArray[6]=new Array("意外代码","90px","100",2);
      iArray[6][4]="llacci"
      iArray[6][5]="6|5";
      iArray[6][6]="0|1";
      iArray[6][15]="accname";
      iArray[6][17]="5";
      iArray[6][19]=1;

      iArray[7]=new Array("意外原因代码","80px","100",3);
      iArray[8]=new Array("意外原因名称","180px","100",3);
      iArray[9]=new Array("备注","155px","100",1);

      AccidentGrid = new MulLineEnter( "fm" , "AccidentGrid" );
      AccidentGrid.mulLineCount = 1;
      AccidentGrid.displayTitle = 1;
      AccidentGrid.canSel = 0;
      AccidentGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在Accident中出错"+ex.message);
    }
  }

  //重疾依据表
  function initSDiseaseProofGrid(strpart){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","0px","0",0);
      iArray[1]=new Array("重疾依据代码","100px","0",3);
      iArray[2]=new Array(strpart,"700px","0",0);
      iArray[3]=new Array("依据属性","100px","0",3);
      iArray[4]=new Array("新依据属性判断","100px","0",3);
      SDiseaseProofGrid = new MulLineEnter( "fm" , "SDiseaseProofGrid" );
      SDiseaseProofGrid.displayTitle = 1;
      SDiseaseProofGrid.mulLineCount = 2;
      SDiseaseProofGrid.hiddenPlus=1;
      SDiseaseProofGrid.hiddenSubtraction=1;
      SDiseaseProofGrid.canChk =1;
      SDiseaseProofGrid.loadMulLine(iArray);
      SDiseaseProofGrid.lock ();
    }
    catch(ex){
      alert("在SDiseaseProof中出错"+ex.message);
    }
  }
  
//轻症依据表
  function initMDiseaseProofGrid(strpart){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","0px","0",0);
      iArray[1]=new Array("轻症依据代码","100px","0",3);
      iArray[2]=new Array(strpart,"700px","0",0);
      iArray[3]=new Array("依据属性","100px","0",3);
      iArray[4]=new Array("新依据属性判断","100px","0",3);
      MDiseaseProofGrid = new MulLineEnter( "fm" , "MDiseaseProofGrid" );
      MDiseaseProofGrid.displayTitle = 1;
      MDiseaseProofGrid.mulLineCount = 2;
      MDiseaseProofGrid.hiddenPlus=1;
      MDiseaseProofGrid.hiddenSubtraction=1;
      MDiseaseProofGrid.canChk =1;
      MDiseaseProofGrid.loadMulLine(iArray);
      MDiseaseProofGrid.lock ();
    }
    catch(ex){
      alert("在MDiseaseProof中出错"+ex.message);
    }
  }
    //失能表
  function initDisabilityGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px",10,0);
      iArray[1]=new Array("是否丧失日常生活能力","50px",10,2);
      iArray[1][10]="Type";
      iArray[1][11]="0|^0|否|^1|是"
      iArray[1][12]="1|2"
      iArray[1][13]="1|0"
      
      iArray[2]=new Array("code","30px",10,3);
      iArray[2][21]="DisabilityFlag";
      
      iArray[3]=new Array("失能日期","30px",10,1);
      iArray[3][9]="失能日期|notnull&date"
      iArray[3][21]="DisabilityDate";
      
      iArray[4]=new Array("观察期结束日期","30px",10,1);
      iArray[4][7]="getObservationDate";
      iArray[4][9]="观察期结束日期|notnull&date"
      iArray[4][21]="ObservationDate";
      
      
      DisabilityGrid = new MulLineEnter( "fm" , "DisabilityGrid" );
      DisabilityGrid.mulLineCount = 1;
      //DisabilityGrid.hiddenPlus=0;
      //DisabilityGrid.hiddenSubtraction=0;
      DisabilityGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert("在DisabilityGrid中出错"+ex.message);
    }
  }
</script>