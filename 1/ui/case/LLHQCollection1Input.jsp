<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLHQCollection1Input.jsp
//程序功能：F1报表生成
//创建日期：2005-08-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLHQCollection1Input.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="getCurrentDate()">    
  <form action="./LLHQCollection1.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>统计年份</TD>
          <TD  class= input> <Input name=Year class=common dateFormat='short' verify="统计年份|notnull&INT" elementtype=nacessary> </TD> 
          <TD  class= title>统计月份</TD>
					<TD  class= input> <input class=codeno CodeData="0|^1|一月|M^2|二月|M^3|三月|M^4|四月|M^5|五月|M^6|六月|M^7|七月|M^8|八月|M^9|九月|M^10|十月|M^11|十一月|M^12|十二月|M"  name=Month ondblclick="return showCodeListEx('Month',[this,MonthName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Month',[this,MonthName],[0,1],null,null,null,1);" verify="统计月份|INT"><input class=codename name=MonthName></TD>	   	 
          <TD  class= title>统计方式</TD>
					<TD  class= input> <input class=codeno CodeData="0|^1|按月统计^2|按年统计"  name=Choose ondblclick="return showCodeListEx('Choose',[this,ChooseName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Choose',[this,ChooseName],[0,1],null,null,null,1);" ><input class=codename name=ChooseName></TD>	   	 
		    </TR>
    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" name=MStartDate value="">
    <input type="hidden" name=MEndDate value="">
    <input type="hidden" name=YStartDate value="">
    <input type="hidden" name=YEndDate value="">
    <br>
		<INPUT VALUE="理赔结果统计表" class="cssButton" TYPE="button" onclick="result()">
		<INPUT VALUE="理赔质量效率统计表" class="cssButton" TYPE="button" onclick="efficient()"> 	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 