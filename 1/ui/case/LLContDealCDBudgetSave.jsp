<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<% 
//程序名称：LLContDealBudgetSave.jsp
//程序功能：退保试算
//创建日期：2013-02-21
//创建人  ：GY
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String flag = "Succ";
  String content = "合同终止退费试算成功";
  String remark = "无";
    String transact = "INSERT";
  long startTime = System.currentTimeMillis();
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  
  String CaseNo=request.getParameter("CaseNo");

  String contNo = request.getParameter("contNo");
  String edorType = request.getParameter("edorType");
  String edorNo = request.getParameter("edorNo");
  boolean needResultFlag = (edorNo != null && !edorNo.equals("") 
                            && edorType != null && !edorType.equals(""));
  System.out.println("\n\n\n\n\n\n" + needResultFlag);
  String edorValiDate = request.getParameter("edorValiDate");
  String payToDateLongPol = request.getParameter("payToDateLongPol");
  String[] tChecks = request.getParameterValues("InpLCPolGridChk");
  
    
  if(tChecks != null)
  {
    for(int i = 0; i < tChecks.length; i++)
    { 

      if (!tChecks[i].equals("1"))
      {
        continue;
      }
      double getMoney = 0;  //退保退费
      String[] tRiskCodes = request.getParameterValues("LCPolGrid5");
      String[] tPolNos = request.getParameterValues("LCPolGrid1");   
   
      
      try
		{
	  LLContDealCDBudgetBL tLLContDealCDBudgetBL = new LLContDealCDBudgetBL();

      VData tVData = new VData();
      tVData.addElement(contNo);
      tVData.addElement(tRiskCodes[i]);
      tVData.addElement(edorNo);
      tVData.addElement(edorType);
      tVData.addElement(edorValiDate);
      tVData.addElement(tPolNos[i]);
      tVData.addElement(CaseNo);
      tVData.addElement(tGI);
      tLLContDealCDBudgetBL.submitData(tVData,transact);
         System.out.println("5555555555555555"+tPolNos[i]);	  
        getMoney = tLLContDealCDBudgetBL.getRValue();
        getMoney = PubFun.setPrecision(getMoney, "0.00");
        System.out.println("9999999999999999:" + getMoney);
      
		}
		catch(Exception ex)
		{
			flag = "Fail";
		    content = "试算金额获取失败" + ex.toString();
		    System.out.println(content);	
		    	
		}
      
%>      
<script language="javascript">
      parent.fraInterface.LCPolGrid.setRowColDataByName("<%=i%>", "getMoney", "<%=getMoney%>");
      parent.fraInterface.LCPolGrid.setRowColDataByName("<%=i%>", "remark", "合同终止退费");      
</script>
<%      
      if(flag.equals("Fail"))
      {
        break;
      }
    }
  }
  System.out.println(System.currentTimeMillis() - startTime);
  content = PubFun.changForHTML(content);
  
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>

