<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：LLClaimCollectionRpt.jsp
	//程序功能：机构理赔案件处理状况统计表
	//创建日期：2005-07-16
	//创建人  ：St.GN
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
	System.out.println("start");
	CError cError = new CError();
	boolean operFlag = true;

	//直接获取页面的值
	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	String MngCom = request.getParameter("ManageCom");
	String ManageName = request.getParameter("ComName");
	String EndCaseDateS = request.getParameter("StartDate"); //起期
	String EndCaseDateE = request.getParameter("EndDate"); //止期
	String StatsType = request.getParameter("StatsType");//统计类型
	System.out.println("StatsType:"+StatsType);



	TransferData Para = new TransferData();

	Para.setNameAndValue("MngCom", MngCom);
	Para.setNameAndValue("ManageName", ManageName);
	Para.setNameAndValue("tStartDate", EndCaseDateS);
	Para.setNameAndValue("tEndDate", EndCaseDateE);
  

	//  ------------------------------
	RptMetaDataRecorder rpt = new RptMetaDataRecorder(request);
	//   	-------------------------------
	String date = PubFun.getCurrentDate().replaceAll("-", "");
	String time = PubFun.getCurrentTime3().replaceAll(":", "");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String fileNameB = tG.Operator + "_" + FileQueue.getFileName()
	+ ".vts";
	Para.setNameAndValue("tFileNameB", fileNameB);
	String operator = tG.Operator;
	Para.setNameAndValue("tOperator", operator);
	Para.setNameAndValue("StatsType", StatsType);
	System.out.println("@机构理赔案件处理状况报表@");
	System.out.println("Operator:" + operator);
	System.out.println("ManageCom:" + tG.ManageCom);
	System.out.println("Operator:" + tG.Operator);
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	String CSVFileName = "";

	tVData.addElement(tG);
	tVData.addElement(Para);

	//对不同的统计类型，选择不同的打印处理类
	if("2".equals(StatsType)){
		LLComCaseReportSBBL tLLComCaseReportSBBL = new LLComCaseReportSBBL();//商业保险
		System.out.println("社保业务清分-机构理赔案件处理状况打印类:LLComCaseReportSBBL");
		
		if (!tLLComCaseReportSBBL.submitData(tVData, "PRINT")) {
	operFlag = false;
	Content = tLLComCaseReportSBBL.mErrors.getFirstError().toString();
%>
			<%=Content%>
			<%
			return;
		} else {
			System.out.println("--------成功----------");
			CSVFileName = tLLComCaseReportSBBL.getMFileName() + ".csv";

			if ("".equals(CSVFileName)) {
				operFlag = false;
				Content = "没有得到要显示的数据文件";
			%>
			<%=Content%>
			<%
			return;
			}
		}
	}else if("1".equals(StatsType)){
		LLComCaseReportSTBL tLLComCaseReportSTBL = new LLComCaseReportSTBL();//全部
		System.out.println("商业业务清分-机构理赔案件处理状况打印类:LLComCaseReportSTBL");
		
		if (!tLLComCaseReportSTBL.submitData(tVData, "PRINT")) {
			operFlag = false;
			Content = tLLComCaseReportSTBL.mErrors.getFirstError().toString();
			%>
			<%=Content%>
			<%
			return;
		} else {
			System.out.println("--------成功----------");
			CSVFileName = tLLComCaseReportSTBL.getMFileName() + ".csv";

			if ("".equals(CSVFileName)) {
				operFlag = false;
				Content = "没有得到要显示的数据文件";
			%>
			<%=Content%>
			<%
			return;
			}
		}
	}else{
		LLComCaseReportBL tLLComCaseReportBL = new LLComCaseReportBL();//全部
		System.out.println("全部业务-机构理赔案件处理状况打印类:LLComCaseReportBL");
		
		if (!tLLComCaseReportBL.submitData(tVData, "PRINT")) {
			operFlag = false;
			Content = tLLComCaseReportBL.mErrors.getFirstError().toString();
			%>
			<%=Content%>
			<%
			return;
		} else {
			System.out.println("--------成功----------");
			CSVFileName = tLLComCaseReportBL.getMFileName() + ".csv";

			if ("".equals(CSVFileName)) {
				operFlag = false;
				Content = "没有得到要显示的数据文件";
			%>
			<%=Content%>
			<%
			return;
			}
		}
	}

	Readhtml rh = new Readhtml();

	String realpath = application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
	String temp = realpath.substring(realpath.length() - 1, realpath
			.length());
	if (!temp.equals("/")) {
		realpath = realpath + "/";
	}
	System.out.println(realpath);
	rpt.updateReportMetaData(StrTool.replace(CSVFileName, ".csv", ".zip"));
	String outpathname=realpath+"vtsfile/"+CSVFileName;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	try {
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	String[] InputEntry = new String[1];
	InputEntry[0] = outpathname;
	System.out.println(InputEntry[0]);
	String tZipFile = StrTool.replace(outpathname, ".csv", ".zip");
	System.out.println("tZipFile == " + tZipFile);
	rh.CreateZipFile(InputEntry, tZipFile);
%>

<html>
<%@page contentType="text/html;charset=GBK"%>
<a
	href="../f1print/download.jsp?filename=<%=StrTool.replace(CSVFileName,".csv",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>

