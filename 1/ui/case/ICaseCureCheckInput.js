//该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var hospRec = "<%@include file=\"HospReceipt.jsp\"%>";

//账单重置
function zdReset(){
  initInpBox();
  initIFeeCaseCureGrid(fm.FeeAtti.value);
  initQDSecuGrid();
  initSecuSpanGrid();
  initSecuAddiGrid();
    querySecuDetail();
}

//下一张账单
function zdNext()
{
  QueryZDInfo(2);
}

//上一张账单
function zdPrevious()
{
   QueryZDInfo(1);
}

//显示第一张帐单
function initQueryZD()
{
  QueryZDInfo(0);
}
                      
function initQuery(){
  var strSql = " SELECT C.RGTNO, C.CUSTOMERNO, C.CUSTOMERNAME, C.CUSTOMERSEX ,C.CUSTOMERAGE,C.IDNO " +
  ",c.AccdentDesc FROM LLCASE C WHERE C.CASENO = '" + fm.all('CaseNo').value + "'";
  var arrResult = easyExecSql(strSql );
	var Remark="";
  if ( arrResult!=null ){
    fm.RgtNo.value =arrResult[0][0];
    fm.CustomerNo.value = arrResult[0][1];
    fm.CustomerName.value = arrResult[0][2];
    fm.CustomerSex.value = arrResult[0][3];
    if(arrResult[0][3]==0)
    fm.CustomerSexName.value = "男";
    if(arrResult[0][3]==1)
    fm.CustomerSexName.value = "女";
    if(arrResult[0][3]==2)
    fm.CustomerSexName.value = "不详";
    fm.Age.value = arrResult[0][4];
    fm.IDNo.value = arrResult[0][5];   
    fm.ContRemark.value = arrResult[0][6];

    strSql="select rgtstate from llcase where caseno='"+fm.CaseNo.value+"'";
    var mrr = easyExecSql(strSql);
    if (mrr){
      mrr[0][0]==null||mrr[0][0]=='null'?'0':fm.Case_RgtState.value=mrr[0][0];
    }
    strSql = "select insuredstat, case insuredstat when '1' then '在职' when '2' then '退休' else '' end from lcinsured where insuredno = '"+fm.CustomerNo.value+"' order by makedate desc";
    var prr = easyExecSql(strSql);
    if (prr!=null){
      fm.InsuredStat.value = prr[0][0];
      fm.InsuredStatName.value = prr[0][1];
    }
  var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
							+fm.CustomerNo.value+"'";
	var	crrResult1 = easyExecSql(strSQLx);
	if(crrResult1){
	  alert("客户："+crrResult1[0][1]+"为黑名单客户!");
	}
    initDutyGrid();
  }
  else
    {
      fm.RgtNo.value ="";
      fm.CustomerNo.value = "";
      fm.CustomerName.value = "";
      fm.CustomerSex.value = "";
      fm.Case_RgtState.value= "";
      fm.cOperator.value= "";
      fm.MakeDate.value= "";
    }
  }

function QueryZDInfo(type)
{
  var strSql="select f.hospitalcode,f.hospitalname,f.receiptno,f.feetype,f.feeatti,"
  + "f.feeaffixtype,f.feedate,f.hospstartdate,f.hospenddate,f.realhospdate,f.mainfeeno ,"
  +"(select b.codename from ldcode b,ldhospital a where b.codetype='llhospiflag' and b.code=a.associateclass and a.hospitcode =f.hospitalcode),"
  +"f.inhosno,f.hosgrade,f.firstinhos,f.compsecuno,f.securityno,f.sumfee,f.hosdistrict,f.InsuredStat "
  + " from llfeemain f  where f.caseno='" + fm.all('CaseNo').value
  + "' ";
  p=0;
  if(type==0)
    strSql+="order by f.mainfeeno";
  if(type==1)
    strSql+=" and f.mainfeeno < '" +fm.all('MainFeeNo').value+"' order by f.mainfeeno ";
  if(type==2)
    strSql+=" and f.mainfeeno > '" +fm.all('MainFeeNo').value+"' order by f.mainfeeno";
  if(type==3)
    strSql+=" and f.mainfeeno = '" +fm.all('MainFeeNo').value+"' order by f.mainfeeno";
  var arr=easyExecSql(strSql);
  if(type==1){
    if(arr==null||arr.length==0)
    {
      alert("已经到达第一张");
      return;
    }
    else
    {
      p=arr.length-1;
    }
  }
  if(type==2){
    if(arr==null||arr.length==0)
    {
      alert("已经到达最后一张");
      return;
    }
  }
  if(arr)
  {
    fm.HospitalCode.value=arr[p][0];
    fm.HospitalName.value=arr[p][1];
    fm.ReceiptNo.value=arr[p][2];
    fm.FeeType.value=arr[p][3];
    fm.FeeAtti.value=arr[p][4];
    fm.FeeAffixType.value=arr[p][5];
    fm.FeeDate.value=arr[p][6];
    fm.HospStartDate.value=arr[p][7];
    fm.HospEndDate.value=arr[p][8];
    fm.RealHospDate.value=arr[p][9];
    fm.FeeStart.value = fm.HospStartDate.value
    fm.FeeEnd.value = fm.HospEndDate.value
    fm.FeeDays.value = fm.RealHospDate.value
    fm.MainFeeNo.value=arr[p][10];
    fm.FixFlag.value=arr[p][11];
    if(fm.FeeType.value=="1")
       fm.FeeTypeName.value="门诊";
    else
      fm.FeeTypeName.value="住院";
    if(fm.FeeAtti.value=="0")
      fm.FeeAttiName.value="医院";
    if(fm.FeeAtti.value=="1")
      fm.FeeAttiName.value="社保结算";
    if(fm.FeeAtti.value=="2")
      fm.FeeAttiName.value="手工报销";
    if(fm.FeeAtti.value=="3")
      fm.FeeAttiName.value="特需业务";
    if(fm.FeeAffixType.value=="0")
      fm.FeeAffixTypeName.value="原件";
    else
      fm.FeeAffixTypeName.value="复印件";
    fm.inpatientNo.value=arr[p][12];
    fm.HosGrade.value=arr[p][13];
    temsql="select codename from ldcode where codetype='hospitalclass' and code='"+fm.HosGrade.value+"'";
    var xrr = easyExecSql(temsql);
    if(xrr)
      fm.HosGradeName.value = xrr[0][0];
    fm.FirstInHos.value=arr[p][14];
    fm.CompSecuNo.value=arr[p][15];
    fm.SecurityNo.value=arr[p][16];
    fm.SumFee.value = arr[p][17];
    fm.InsuredStat.value = arr[p][19];
    if(fm.InsuredStat.value=='1'){
      fm.InsuredStatName.value='在职';
    }
    else{
      if(fm.InsuredStat.value=='2'||fm.InsuredStat.value=='3')
        fm.InsuredStatName.value='退休';
      else
        fm.InsuredStatName.value='';
    }
    fm.UrbanFlag.value = arr[p][18];
      if(fm.UrbanFlag.value=='0')
        fm.UrbanFlagName.value = '非城区';
      if(fm.UrbanFlag.value=='1')
        fm.UrbanFlagName.value = '城区';
    afterCodeSelect('FeeType',fm.FeeType.value);
    //填充账单项目明细
    if (fm.FeeAtti.value=="0")   //普通帐单
    {
      strSql="select FeeItemCode,FeeItemName,Fee, SelfAmnt,PreAmnt,RefuseAmnt,avaliflag from LLCaseReceipt where MainFeeNo='"
      +fm.all('MainFeeNo').value+"' order by FeeItemCode";
      turnPage.pageLineNum = 100;
      turnPage.queryModal(strSql,IFeeCaseCureGrid);
  
      if (IFeeCaseCureGrid.mulLineCount<=0)//防止没有对应数据,则显示默认的显示状态
        initIFeeCaseCureGrid(fm.FeeAtti.value);
    }
    if (fm.FeeAtti.value=="1")
    {
      if(fm.MngCom.value=='8694')
        queryQDZD();
      fillHos_SecuReceipt_BJ();
      querySecuDetail();
    }
    if (fm.FeeAtti.value=="2")
    {
      fillManualReceipt();
      querySecuDetail();
    }
    if (fm.FeeAtti.value=="3")//填充特需医疗帐单
    {
      strSql = "select SumFee,SocialPlanAmnt,OtherOrganAmnt, b.remnant "+
      " from llsecurityreceipt a,llfeemain b where a.mainfeeno=b.oldmainfeeno and "+
      " b.mainfeeno='"+fm.all('MainFeeNo').value+"' union "+
      "select SumFee,SocialPlanAmnt,OtherOrganAmnt, remnant from llfeemain where mainfeeno='"+
      fm.all('MainFeeNo').value+"'";
      turnPage.queryModal(strSql,SecuAddiGrid);
  
      if (SecuAddiGrid.mulLineCount<=0)//防止没有对应数据,则显示默认的显示状态
      {
        initSecuAddiGrid();
      }
    }
  }
  else
    initInpBox();
}

//填充社保医院结算单
function fillHos_SecuReceipt_BJ()
{
      strSql="select FeeItemCode,FeeItemName,Fee, SelfAmnt,PreAmnt,RefuseAmnt,avaliflag from LLCaseReceipt where MainFeeNo='"
      +fm.all('MainFeeNo').value+"' order by FeeItemCode";
      turnPage.pageLineNum = 100;
      turnPage.queryModal(strSql,IFeeCaseCureGrid);
  
      if (IFeeCaseCureGrid.mulLineCount<=0)//防止没有对应数据,则显示默认的显示状态
        initIFeeCaseCureGrid('1');
}

function verifyHospital()
{
	
}


//帐单保存前校验
function saveFee()
{
  var cont="";
  if(fm.FeeAtti.value=="2"){
    cont=" and FeeAtti ='2' and hospstartdate='"+fm.FeeStart.value+"' and CustomerNo='"+fm.CustomerNo.value+"'";
  }
  else{
		cont=" and ReceiptNo='"+fm.ReceiptNo.value+"'";
	}
			
	var sqlx="select distinct caseno from llfeemain where HospitalCode ='"+fm.HospitalCode.value+
					"' and caseno <> '"+fm.CaseNo.value+"'"+cont;
	var arrResult = easyExecSql(sqlx);	
	if(arrResult){
	  if(!confirm("该帐单在"+arrResult+"中有理赔记录，您确定要继续吗？"))
	    return false;
	}
		
	if( verifyInput2() == false ) return false;
	
	var rowNum=IFeeCaseCureGrid.mulLineCount ;
  var count=0; 
    for (count=0;count<rowNum;count++)
    {
  	  if(
  	    IFeeCaseCureGrid.getRowColData(count,3)<0||
  	    IFeeCaseCureGrid.getRowColData(count,4)<0||
  	    IFeeCaseCureGrid.getRowColData(count,5)<0||
  	    IFeeCaseCureGrid.getRowColData(count,6)<0 ){
  	    alert("金额必须大于0");
  	    return false;
      }
  	}
  
  if(fm.FeeAtti.value=='1'&&fm.MngCom.value=='8694'){
    if(checkQDZD()==false)
      return;
  }
  if(fm.FeeAtti.value=='2'){
    if(!checkSGZD()){
      return false;
    }
    fm.HospStartDate.value = fm.FeeStart.value;
    fm.HospEndDate.value = fm.FeeEnd.value;
    fm.RealHospDate.value = fm.FeeDays.value;
    if(fm.ReceiptNo.value=='')
      fm.ReceiptNo.value = "000000";
    fm.FeeAffixType.value = "0";
    fm.FeeDate.value = fm.FeeEnd.value;
  }

  if( verifyInput2() == false ) return false;
  if (fm.HospEndDate.value.length != 0){
    if(!checkDate(fm.HospStartDate.value,fm.HospEndDate.value)){
      alert("住院开始日期不能晚于住院结束日期");
      return false;
    }
  }
  if(!checkDate(fm.HospStartDate.value,fm.FeeDate.value)){
    alert("帐单日期必须晚于住院开始日期");
    return false;
  }

  fm.all('cOperate').value = "INSERT";
	fm.action ="./ICaseCureSave.jsp";
  submitForm();
}

//删除帐单，该功能目前不使用
  function deleteFee()
  {
    if(fm.all('MainFeeNo').value == null ||
    fm.all('MainFeeNo').value == '')
    {
      alert("账单信息尚未保存！");
      return;
    }
    fm.all('cOperate').value = "DELETE";
    submitForm();
  }

//数据提交
function submitForm()
{
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
}

//提交数据后操作
function afterSubmit(FlagStr, content, tMainFeeNo)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    fm.all('MainFeeNo').value = tMainFeeNo;
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    querySecuDetail();
    QueryZDInfo(3);
  }
  if (fm.all('cOperate').value == "DELETE")
  {
    initInpBox();
    initIFeeCaseCureGrid(fm.FeeAtti.value);
  }
}

//录入理赔号后回车查询相应的案件帐单信息
function QueryOnKeyDown()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    initQuery();
    QueryZDInfo(0);
  }
}

//跳转到检录处理
function nextDeal()
{
  var caseNo = fm.all('CaseNo').value;

  var varSrc="&CaseNo="+ caseNo;
  parent.window.location = "./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+varSrc
}

//返回理赔人信箱
function backDeal()
{
  top.close();
}

//选择下拉列表后响应函数
function afterCodeSelect( cCodeName, Field )
{
  if(cCodeName=="llhospiquery"){
    if(fm.UrbanFlag.value=='0')
      fm.UrbanFlagName.value = '非城区';
    if(fm.UrbanFlag.value=='1')
      fm.UrbanFlagName.value = '城区';
    var tssql0 = "select codename from ldcode where ";
    sqlpart1=" codetype = 'llhospiflag' and code='"+fm.FixFlag.value+"'";
    tssql = tssql0+sqlpart1;
    urr = easyExecSql(tssql);
    if(urr){
      fm.FixFlag.value=urr[0][0];
    }
    sqlpart2 = " codetype='hospitalclass' and code='"+fm.HosGrade.value+"'";
    tssql = tssql0+sqlpart2;
    vrr = easyExecSql(tssql);
    if(vrr)
      fm.HosGradeName.value = vrr[0][0];
  }
  if( cCodeName=="feeatti"){
    getPage();
  }
  if( cCodeName == "FeeType")
  {
    initZDStyle();
    caldate();
    strsqlinit="1 and code like #"+fm.all('FeeType').value+"%#";
    initIFeeCaseCureGrid(fm.FeeAtti.value);
  }
  if( cCodeName == "llfeeitemtype")
  {
    if (fm.FeeDutyFlag.value=="0")
       alert("该客户没有费用险责任");
  }
}

//计算实际住院天数
function caldate()
{
  if(fm.HospStartDate.value!=""&&fm.HospEndDate.value!="")
  {
    fm.HospStartDate.value=modifydate(fm.HospStartDate.value);
    fm.HospEndDate.value=modifydate(fm.HospEndDate.value);
    //	var a=dateDiff(fm.HospStartDate.value,fm.HospEndDate.value,"D")-1;
    var strSql= "select to_char(to_date('"+ fm.HospEndDate.value + "') - to_date('" + fm.HospStartDate.value +"')) from dual"
    //alert(strSql );
    var mrr = easyExecSql(strSql);
    if (mrr!=null)
    {
      a= mrr[0][0] ;
      if(fm.FeeType.value=="1")
        a=a/1+1;
      if ( a<0) 
        fm.RealHospDate.value =0 ;
      else
        fm.RealHospDate.value= a;
    }
  }
}

//筛选门诊的开始时间和结束时间
function fillDate()
{
  if (fm.FeeType.value=="1")
  {
    var StartDate=modifydate(fm.FeeDate.value);
    var EndDate=modifydate(fm.FeeDate.value);
    var strSql="select feedate from llfeemain where caseno='" + fm.all('CaseNo').value
    +"' and FeeType='1'";
    var arr=easyExecSql(strSql);
    if (arr!=null)
    {
      var count=arr.length;
      for(i=0;i<count;i++){
        if(compareDate(StartDate,arr[i][0])==1)
          StartDate=arr[i][0];
        if(compareDate(EndDate,arr[i][0])==2)
          EndDate=arr[i][0];
      }
      fm.HospStartDate.value=StartDate;
      fm.HospEndDate.value=EndDate;
    }
    else
    {
      fm.HospStartDate.value=fm.FeeDate.value;
      fm.HospEndDate.value=fm.FeeDate.value;
    }
   }
}
         
//校验录入日期         
function checkDate(begin,end)
{
  begin=modifydate(begin);
  end=modifydate(end);
  if(begin<=end)
    return true;
  else
    return false;
}

//选择帐单显示界面
function initZDStyle()
{
    tsql = "select code,codename,'','','','',codealias from ldcode where codetype='llfeeitemtype' and "
    +" (substr(code,1,1) ='"+fm.FeeType.value+"' ";
  if( fm.FeeType.value=="1"){
    titleInpatient.style.display = 'none';
    titleOutpatient.style.display = 'none';
    titleStart.style.display='';
    titleEnd.style.display='';
    titleZY.style.display='none';
    titleInHos.style.display='none';
    titleOutHos.style.display='none';
    titleDays.style.display='none';
    titleMZ.style.display='';
    titleFeeStart.style.display='';
    titleApply.style.display='';
    titleConDays.style.display='';
  }
  if( fm.FeeType.value=="2"){
    titleStart.style.display='none';
    titleEnd.style.display='none';
    titleInpatient.style.display='';
    titleOutpatient.style.display='';
    titleZY.style.display='';
    titleInHos.style.display='';
    titleOutHos.style.display='';
    titleDays.style.display='';
    titleMZ.style.display='none';
    titleFeeStart.style.display='none';
    titleApply.style.display='none';
    titleConDays.style.display='none';
  }
  if(fm.FeeAtti.value=="0"){
    titleC1.style.display='';
    fm.InsuredStat.verify="" ;
    titleC2.style.display='';
    commonZD.style.display='';
    titleS1.style.display='none';
    titleS2.style.display='none';
    divsecurity.style.display='none';
    secuspan.style.display='none'
    SecuAddiZD.style.display='none';
    QDZD.style.display='none';
    divSecuItem.style.display='none';
  }
  if(fm.FeeAtti.value=="1"){
    titleS1.style.display='';
    fm.InsuredStat.verify="参保人员类别|notnull" ;
    titleS2.style.display='';
    titleC1.style.display='';
    titleC2.style.display='';
    secuspan.style.display=''
    divsecurity.style.display='none';
    SecuAddiZD.style.display='none';
    if(fm.MngCom.value.substr(0,4)=='8694'){
      QDZD.style.display='';
      commonZD.style.display='none';
      divSecuItem.style.display='none';
    }
    else{
      QDZD.style.display='none';
      commonZD.style.display='';
      divSecuItem.style.display='';
    }
    querySecuDetail();
  }
  if( fm.FeeAtti.value=="2"){
    titleS1.style.display='';
    fm.InsuredStat.verify="参保人员类别|notnull" ;
    titleS2.style.display='';
    secuspan.style.display=''
    divsecurity.style.display='';
    titleC1.style.display='';
    titleC2.style.display='none';
    commonZD.style.display='none';
    SecuAddiZD.style.display='none';
    QDZD.style.display='none';
    querySecuDetail();
    divSecuItem.style.display='none';
    if(fm.MngCom.value.substr(0,4)=='8611'&&fm.InsuredStat.value=="2"){
      divRetireAddFee.style.display='';
    }
  }
  if(fm.FeeAtti.value=="3"){
    titleS1.style.display='';
    fm.InsuredStat.verify="" ;
    titleS2.style.display='';
    titleC1.style.display='';
    titleC2.style.display='';
    SecuAddiZD.style.display='';
    divSecuAddiReceipt.style.display='';
    divsecurity.style.display='none';
    commonZD.style.display='none';
    QDZD.style.display='none';
    secuspan.style.display='none'
    divSecuItem.style.display='none';
  }
}//填充社保分段信息
function querySecuDetail()
{  
  if(fm.MainFeeNo.value==null||fm.MainFeeNo.value=='')
    SDSql = "select '大额封顶线以上','','','','','','88' from dual union select '合计金额','','','','','','99' from dual";
  else{
    var SDSql = "(select to_char(downlimit),to_char(uplimit),char(decimal(planpayrate,3,2)),planfee,"+
    "selfpay,planfee+selfpay,serialno a from llsecudetail where MainFeeNo='"+fm.MainFeeNo.value+
    "' and uplimit>0 union select '大额封顶线以上','',char(decimal(planpayrate,3,2)),planfee,selfpay,"+
    "planfee+selfpay,'88' a from llsecudetail where uplimit=0 and MainFeeNo='"+fm.MainFeeNo.value+
 //   " union select '退休人员补充费用','',char(decimal(planpayrate,3,2)),planfee,selfpay, " +
  //  " planfeeselfpay,'8611' a from llsecudetail where uplimit=1 and MainFeeNo='"+fm.MainFeeNo.value+" '"+ 
    "' union select '合计金额','','',case when sum(planfee) is null then 0 else sum(planfee) "+
    "end,case when sum(selfpay) is null then 0 else sum(selfpay) end,case when (sum(planfee)is"+
    " null or sum(selfpay) is null)  then 0 else sum(planfee)+sum(selfpay) end, '99' a "
    +"from llsecudetail where MainFeeNo='"+fm.MainFeeNo.value+"') order by a";
  }
  turnPage.queryModal(SDSql,SecuSpanGrid);

}



//显示过滤出的保单责任
function initDutyGrid()
{
  fm.FeeDutyFlag.value='0';
  var strSql="select distinct a.getdutycode,b.getdutyname,a.contno,c.getstartdate,c.amnt,c.mult from lltoclaimduty a,lmdutygetclm b,lcduty c where c.polno=a.polno and b.getdutycode=a.getdutycode and a.caseno='"+fm.CaseNo.value+"' order by getdutycode";
  turnPage.queryModal(strSql,DutyRelaGrid);
  strSql="select '1' from lcpol where riskcode in ('1602','1603','1604','1202','1203','1204') and insuredno='"+fm.CustomerNo.value+"'"
  var brr=easyExecSql(strSql);
  if (brr){
    fm.FeeDutyFlag.value=brr[0][0];
  }
}

function getSGBXD()
{
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.action ="./ICaseSecuZD.jsp";
			fm.submit();

}

function openDrug()
{
    var varSrc = "&CaseNo=" + fm.CaseNo.value;
      varSrc += "&RgtNo=" + fm.RgtNo.value;
      varSrc += "&MainFeeNo="+fm.MainFeeNo.value;
      varSrc += "&LoadFlag=1" ;
      varSrc += "&CustomerNo=" + fm.CustomerNo.value;
      varSrc += "&CustomerName=" + fm.CustomerName.value;
      
      pathStr="./FrameMainDrug.jsp?Interface=CaseDrugInput.jsp"+varSrc;
      showInfo = OpenWindowNew(pathStr,"CaseDrugInput","middle",800,330);
}

function showBlack() 
{

	var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
							+fm.CustomerNo.value+"'";
	var	crrResult1 = easyExecSql(strSQLx);
	if(crrResult1)
	{
	alert("客户："+crrResult1[0][1]+"为黑名单客户!");
	
	}
}

function zdComplete()
{
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./ICaseCureSaveAll.jsp";
    fm.submit(); //提交
}

function getPage(){
  if(fm.FeeAtti.value='0'){
    alert(hospRec);
    fm.all("spanReceipt").innerHTML="./HospReceipt.jsp";
  }
  initPage();
}