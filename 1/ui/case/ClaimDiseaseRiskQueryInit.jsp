<%
//程序名称：DiseaseRiskQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//返回按钮初始化
var str = "";
function initDisplayButton()
{
	tDisplay = <%=tDisplay%>;
	//alert(tDisplay);
	if (tDisplay=="1"||tDisplay=="2")
	{
		fm.Return.style.display='';
	}
	else if (tDisplay=="0")
	{
		fm.Return.style.display='none';
	}
}
function initQuery()
{
    try
    {
        //alert("asdfsdaf"+top.opener.fm.all('MainDiseaseNo'));
        var tMainDiseaseNo = top.opener.fm.all('MainDiseaseNo').value;
	    //alert(tMainDiseaseNo);
	    if (tMainDiseaseNo!=""&&tMainDiseaseNo!=null)
	    {
	    	fm.all('MainDiseaseNo').value = tMainDiseaseNo; 
	    	easyQueryClick();
	    }
	 }
	 catch(ex)
	 {
	 }
}
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('MainDiseaseNo').value = '';    
    fm.all('MainDiseaseKind').value = '';    
//    fm.all('PrtNo').value = '';
//    fm.all('ContNo').value = '';
//    fm.all('ManageCom').value = '';
//    fm.all('AgentCode').value = '';
//    fm.all('AgentGroup').value = '';
//    fm.all('GrpContNo').value = '';
//    fm.all('DiseaseRiskNo').value = '';
//    fm.all('AppntName').value = '';
    
  }
  catch(ex)
  {
    alert("在AllDiseaseRiskQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在DiseaseRiskQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
		initPolGrid();
		initDisplayButton();
		initQuery();
	
  }
  catch(re)
  {
    alert("DiseaseRiskQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 疾病查询信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="主要疾病类目编码";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


			iArray[2]=new Array();                                                 
      iArray[2][0]="主要疾病类目";         		//列名                     
      iArray[2][1]="120px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[3]=new Array();
      iArray[3][0]="疾病严重程度";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      
      iArray[4]=new Array();
      iArray[4][0]="性别";         		//列名
      iArray[4][1]="30px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[5]=new Array();
      iArray[5][0]="年龄";         		//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="险种分类";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[7]=new Array();                                                 
      iArray[7][0]="是否做手术";         					//列名                             
      iArray[7][1]="80px";            	//列宽                             
      iArray[7][2]=100;            			//列最大值                           
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[8]=new Array();                                                 
			iArray[8][0]="核保结论";         					//列名                               
			iArray[8][1]="50px";            	//列宽                               
			iArray[8][2]=100;            			//列最大值                           
			iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许


			iArray[9]=new Array();                                                 
			iArray[9][0]="手术编码";         					//列名                               
			iArray[9][1]="50px";            	//列宽                               
			iArray[9][2]=100;            			//列最大值                           
			iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
			
			
			iArray[10]=new Array();                                                 
			iArray[10][0]="疾病大分类编码";         					//列名                               
			iArray[10][1]="100px";            	//列宽                               
			iArray[10][2]=100;            			//列最大值                           
			iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			
			iArray[11]=new Array();                                                 
			iArray[11][0]="疾病大分类名称";         					//列名                               
			iArray[11][1]="100px";            	//列宽                               
			iArray[11][2]=100;            			//列最大值                           
			iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			       
			
			iArray[12]=new Array();                                                 
			iArray[12][0]="疾病小分类编码";         					//列名                               
			iArray[12][1]="100px";            	//列宽                               
			iArray[12][2]=100;            			//列最大值                           
			iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			
			iArray[13]=new Array();                                                 
			iArray[13][0]="疾病小分类名称";         					//列名                               
			iArray[13][1]="100px";            	//列宽                               
			iArray[13][2]=100;            			//列最大值                           
			iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			
			iArray[14]=new Array();                                                 
			iArray[14][0]="风险等级";         					//列名                               
			iArray[14][1]="50px";            	//列宽                               
			iArray[14][2]=100;            			//列最大值                           
			iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			
			iArray[15]=new Array();                                                 
			iArray[15][0]="手术名称";         					//列名                               
			iArray[15][1]="50px";            	//列宽                               
			iArray[15][2]=100;            			//列最大值                           
			iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
			
			
			iArray[16]=new Array();                                                       
			iArray[16][0]="复发期";         					//列名                               
			iArray[16][1]="60px";            	//列宽                               
			iArray[16][2]=100;            			//列最大值                           
      iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[17]=new Array();                                                       
			iArray[17][0]="疾病文本";         					//列名                               
			iArray[17][1]="50px";            	//列宽                               
			iArray[17][2]=100;            			//列最大值                           
      iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[18]=new Array();                                                       
			iArray[18][0]="妊娠相关";         					//列名                               
			iArray[18][1]="50px";            	//列宽                               
			iArray[18][2]=100;            			//列最大值                           
      iArray[18][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[19]=new Array();                                                       
			iArray[19][0]="状态";         					//列名                               
			iArray[19][1]="50px";            	//列宽                               
			iArray[19][2]=100;            			//列最大值                           
      iArray[19][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[20]=new Array();                                                       
			iArray[20][0]="观察期";         					//列名                               
			iArray[20][1]="50px";            	//列宽                               
			iArray[20][2]=100;            			//列最大值                           
      iArray[20][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[21]=new Array();                                                       
			iArray[21][0]="可保性判断原则";         					//列名                               
			iArray[21][1]="100px";            	//列宽                               
			iArray[21][2]=100;            			//列最大值                           
      iArray[21][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[22]=new Array();                                                       
			iArray[22][0]="年份";         					//列名                               
			iArray[22][1]="50px";            	//列宽                               
			iArray[22][2]=100;            			//列最大值                           
      iArray[22][3]=3;              			//是否允许输入,1表示允许，0表示不允许
  
       
       
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray); 
      
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>