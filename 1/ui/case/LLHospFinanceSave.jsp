<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：医保通结算批次申请
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
  
<%
  CErrors tError = null;
	String FlagStr;
	String Content;
	String tManageCom = "";
	String tHospitCode = "";
	String tHCNo = "";
	String tOperate = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	//输入参数
	LLHospGetSchema tLLHospGetSchema = new LLHospGetSchema();
	tLLHospGetSchema.setHospitCode(request.getParameter("HospitalCode"));
	tLLHospGetSchema.setHCNo(request.getParameter("HCNoT"));
	
	tOperate = request.getParameter("Operate");
	
	VData tVData = new VData();
	tVData.add(tLLHospGetSchema);
	tVData.add(tGI);
	
	LLHospFinanceBL tLLHospFinanceBL = new LLHospFinanceBL();
	if (!tLLHospFinanceBL.submitData(tVData, tOperate))
	{
		FlagStr = "Fail";
		tError = tLLHospFinanceBL.mErrors;
		Content = tError.getFirstError();
	}
	else
	{
		//设置显示信息
		VData tRet = tLLHospFinanceBL.getResult();
		LLHospGetSchema mLLHospGetSchema = new LLHospGetSchema();
		mLLHospGetSchema.setSchema((LLHospGetSchema) tRet.getObjectByObjectName("LLHospGetSchema", 0));
		
		tHCNo = mLLHospGetSchema.getHCNo();
		
		FlagStr = "Succ";
		Content = tHCNo+"数据保存成功";
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=tHCNo%>");
</script>
</html>

