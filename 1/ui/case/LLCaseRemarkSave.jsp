<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLCaseRemarkSave.jsp
//程序功能：
//创建日期：2014-07-07 
//创建人  ：Houyd
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LLCaseRemarkSchema tLLCaseRemarkSchema  = new LLCaseRemarkSchema();
  LLCaseRemarkBL tLLCaseRemarkBL = new LLCaseRemarkBL();
  LLCaseDB tLLCaseDB = new LLCaseDB();
  String tRgtState = "";

  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  
  tLLCaseDB.setCaseNo(request.getParameter("CaseNo"));
  tLLCaseDB.getInfo();
  tRgtState = tLLCaseDB.getRgtState();
  
  tLLCaseRemarkSchema.setCaseNo(request.getParameter("CaseNo"));
  tLLCaseRemarkSchema.setRemark(request.getParameter("Remark"));
  tLLCaseRemarkSchema.setRgtState(tRgtState);

  try{
  	VData tVData = new VData();
	tVData.add(tLLCaseRemarkSchema);
  	tVData.add(tG);
    tLLCaseRemarkBL.submitData(tVData,transact);
  }
  catch(Exception ex){
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (FlagStr==""){
    tError = tLLCaseRemarkBL.mErrors;
    if (!tError.needDealError()){
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else{
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
