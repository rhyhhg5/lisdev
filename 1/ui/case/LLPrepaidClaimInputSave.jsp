<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK"%>

<%
	//程序名称：LLPrepaidClaimInputSave.jsp
	//程序功能：预付赔款录入
	//创建日期：2010-11-25
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>

<%
	//接收信息，并作校验处理。
	String tChk[] = request
			.getParameterValues("InpPrepaidClaimGridChk");
	String tRiskCode[] = request
			.getParameterValues("PrepaidClaimGrid1");
	String tMoney[] = request.getParameterValues("PrepaidClaimGrid6");
	String tGrpPolNo[] = request.getParameterValues("PrepaidClaimGrid7");
	
	String tPrepaidNo = request.getParameter("PrepaidNo");
	String tGrpContNo = request.getParameter("GrpContNo");
	String tRgtType = request.getParameter("RgtType");
	String tPayMode = request.getParameter("PayMode");
	String tBankCode = request.getParameter("BankCode");
	String tAccNo = request.getParameter("AccNo");
	String tAccName = request.getParameter("AccName");
	
	//输入参数
	LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
	LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
	LLPrepaidClaimInputUI tLLPrepaidClaimInputUI = new LLPrepaidClaimInputUI();

	//输出参数
	CErrors tError = null;
	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";
	VData tResult = new VData();

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");

	//从url中取出参数付给相应的schema
	tLLPrepaidClaimSchema.setPrepaidNo(tPrepaidNo);
	tLLPrepaidClaimSchema.setGrpContNo(tGrpContNo);
	tLLPrepaidClaimSchema.setRgtType(tRgtType);
	tLLPrepaidClaimSchema.setBankCode(tBankCode);
	tLLPrepaidClaimSchema.setBankAccNo(tAccNo);
	tLLPrepaidClaimSchema.setAccName(tAccName);
	tLLPrepaidClaimSchema.setPayMode(tPayMode);

	for (int index = 0; index < tChk.length; index++) {
		if (tChk[index].equals("1")) {
			LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
			tLLPrepaidClaimDetailSchema.setRiskCode(tRiskCode[index]);
			tLLPrepaidClaimDetailSchema.setMoney(tMoney[index]);
			tLLPrepaidClaimDetailSchema.setGrpPolNo(tGrpPolNo[index]);
			tLLPrepaidClaimDetailSet.add(tLLPrepaidClaimDetailSchema);
		}
	}

	try {
		//准备传输数据VData
		VData tVData = new VData();

		//传输schema
		tVData.addElement(tLLPrepaidClaimSchema);
		tVData.addElement(tLLPrepaidClaimDetailSet);

		tVData.add(tG);
		tLLPrepaidClaimInputUI.submitData(tVData, transact);
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "") {
		tError = tLLPrepaidClaimInputUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Success";
			tResult = tLLPrepaidClaimInputUI.getResult();
			tLLPrepaidClaimSchema = (LLPrepaidClaimSchema)tResult.getObjectByObjectName("LLPrepaidClaimSchema",0);
		} else {
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

	//添加各种预处理
	
	System.out.println("操作预付赔款号："+tLLPrepaidClaimSchema.getPrepaidNo());
%>
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.fm.all("PrepaidNo").value = "<%=tLLPrepaidClaimSchema.getPrepaidNo()%>";
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

