//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();

function sumFeeItem()
{
  fm.fmtransact.value = "INSERT";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLSurveyFeeSave.jsp";
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function submitForm()
{
  fm.fmtransact.value = "INSERT";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLSurveyFeeSave.jsp";
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function submitConf()
{
  fm.fmtransact.value = "CONFIRM";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLSurveyFeeSave.jsp";
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    SurveyQuery();
    top.opener.initTitle();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try{
    initForm();
  }
  catch(re){
    alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

function QueryOnKeyDown()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13"){
    ClientQuery();
    initInqFeeGrid();
    initInqFeeDetailGrid();
    SurveyQuery();
  }
}

function SurveyQuery()
{

  strSql= "select substr(a.surveyno,18),b.codename,a.FeeItem,a.FeeSum,a.inqdept,'直接调查费',"
  +" (case a.uwstate when '1' then '审核通过' when '4' then '异地审核通过' else '待审核' end),"
  +"a.surveyno from llinqfee a,ldcode b where b.codetype='llsurveyfee' and b.code=a.feeitem and a.surveyno='"+fm.CaseNo.value+fm.SurveyNo.value+"'"
  var brr = easyExecSql(strSql );

  if ( brr )displayMultiline( brr, InqFeeDetailGrid);
  strSql1= "select substr(surveyno,18),INQFEE,inputer,Confer from llinqfeesta where surveyno='"+fm.CaseNo.value+fm.SurveyNo.value+"'"
  var crr =easyExecSql(strSql1);
  if ( crr )displayMultiline( crr, InqFeeGrid);
}

function makesql()
{
  strsqlinit1="1 and otherno= #"+fm.all('CaseNo').value+"#";
}

function afterCodeSelect(ObjType,Obj)
{

}

function ClientQuery()
{
	if(fm.CaseNo.value==''&&fm.CustomerNo.value==''&&fm.CustomerName.value==''){
		return;
	}
  caseSQL="select customername,customerno,CaseNo from llcase where 1=1 "
  	+getWherePart("CaseNo","CaseNo")
  sqlpart=getWherePart("CustomerNo","CustomerNo")
  	+getWherePart("CustomerName","CustomerName");
  consSQL="select customername,customerno,consultno from llconsult where 1=1 "
  	+getWherePart("consultno","CaseNo")
  	
  strSQL = caseSQL+sqlpart+" union "+consSQL+sqlpart;
  arr = easyExecSql(strSQL);
  if(arr){
    fm.CustomerName.value=arr[0][0];
    fm.CustomerNo.value=arr[0][1];
    fm.CaseNo.value = arr[0][2];
  }
}
