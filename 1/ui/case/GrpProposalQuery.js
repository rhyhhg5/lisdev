//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var Row_1_2;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function getQueryDetail()
{  
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = PolGrid.getRowColData(tSel - 1,2);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		
		if (cPolNo == "")
		    return;
		    
		var GrpPolNo = PolGrid.getRowColData(tSel-1,1);
                var prtNo = PolGrid.getRowColData(tSel-1,3);
        //alert("dfdf");
        if( tIsCancelPolFlag == "0"){
	    	if (GrpPolNo =="00000000000000000000") {
	    	 	window.open("./AllProQueryMain.jsp?LoadFlag=6&prtNo="+prtNo,"window1");	
		    } else {
			window.open("./AllProQueryMain.jsp?LoadFlag=4");	
		    }
		} else {
		if( tIsCancelPolFlag == "1"){//销户保单查询
			if (GrpPolNo =="00000000000000000000")   {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} else {
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	    } else {
	    	alert("保单类型传输错误!");
	    	return ;
	    }
	 }
 }
}

//销户保单的查询函数
function getQueryDetail_B()
{
	
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	  var cPolNo = PolGrid.getRowColData(tSel - 1,1);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		if (cPolNo == "")
			return;
		var GrpPolNo = PolGrid.getRowColData(tSel-1,6);
	    if (GrpPolNo =="00000000000000000000") 
	    {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} 
			else 
			{
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	}
}



// 保单明细查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cContNo = PolGrid.getRowColData(tSel - 1,1);				
		if (cContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0");	
	}
}

// 基本信息查询
function FunderInfo()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cInsuredNo = PolGrid.getRowColData(tSel - 1,4);				
		if (cInsuredNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/CQPersonMain.jsp?CustomerNo="+cInsuredNo);	
	}
}
//个人业务
function PersonBiz()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var ContNo = PolGrid.getRowColData(tSel - 1,1);				
		if (ContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&IsCancelPolFlag=0");	
	}
}

//保单保障项目查询
function PolPolicy()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var ContNo = PolGrid.getRowColData(tSel - 1,1);				
		if (ContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../case/PolInfoQueryMain.jsp?ContNo="+ContNo+"&IsCancelPolFlag=0");	
	}
}

// 查询按钮
function easyQueryClick()
{
  var comCode = fm.ComCode.value;
  if (fm.GrpContNo.value==""&&fm.CustomerNo.value==""&&fm.GrpName.value==""){
    alert("请输入查询条件");
    return false; 
  } 

	// 初始化表格
	initContGrid();
		initClaimGrid();
	
	// 书写SQL语句
	var strSQL = "";
			strSQL = "select distinct b.appntno,b.grpname,"
							+"(select linkman1 from lcgrpaddress where customerno=b.appntno order by addressno desc fetch first 1 rows only),"
							+"(select phone1 from lcgrpaddress where customerno=b.appntno order by addressno desc fetch first 1 rows only),"
							+"(select grpaddress from lcgrpaddress where customerno=b.appntno order by addressno desc fetch first 1 rows only)"
							+" from lcgrpcont b "
							+" where b.appflag='1'"
							+ getWherePart("b.grpcontno","GrpContNo")	
							+ getWherePart("b.appntno","CustomerNo")	
							+ getWherePart("b.grpname", "GrpName", "like")
							+" and managecom like '"+comCode+"%' order by b.appntno desc with ur ";
			turnPage.pageLineNum = 6;
			turnPage.queryModal(strSQL,PolGrid);  
			if (PolGrid.mulLineCount==0){
			alert("该团单不属于"+comCode+",不能进行此操作!");
			return false;
			} 
   if (fm.GrpContNo.value!=null && fm.GrpContNo.value!="") {
      var sql_grpcont = " select GrpContNo, GrpName,'', Peoples2, Prem, HandlerDate, CValiDate, CInValiDate, "
										+ " case when appflag = '1' then '承保' else '投保' end"
										+ " from lcgrpcont where GrpContNo = '"+fm.GrpContNo.value+"' with ur " ;
	  turnPage1.pageLineNum = 5;
	  turnPage1.queryModal(sql_grpcont, ContGrid);
	  initClaimGrid();   
   }
   clearinput();
		    	
}

function getQueryResult()
{
	var arrSelected = null;
	var tRow = PolGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
		      return arrSelected;
	
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	
	return arrSelected;
}


//单击PolGrid单选框响应的函数--查询团单信息
function showGrid1()
{
		Row_1_2 = PolGrid.getSelNo();
		var sql_grpcont = " select GrpContNo, GrpName,'', Peoples2, Prem, HandlerDate, CValiDate, CInValiDate, "
										+ " case when appflag = '1' then '承保' else '投保' end"
										+ " from lcgrpcont where AppntNo = '"+PolGrid.getRowColData(Row_1_2-1,1)+"' with ur " ;
	  turnPage1.pageLineNum = 5;
	  turnPage1.queryModal(sql_grpcont, ContGrid);
	  initClaimGrid();
	  clearinput();
}   
    
//单击ContGrid单选框响应的函数--查询理赔信息
function showGrid2()    
{
		Row_2_3 = ContGrid.getSelNo();
  	var sql_grpclaim = " select B, A, CASE WHEN C IS NULL THEN 0 ELSE C END, "
					  +" CASE WHEN A=0 OR C IS NULL THEN 0 ELSE decimal(C/float(A), 12, 2) END,"
					  +" CASE WHEN A=0 OR C IS NULL or D is null THEN 0 ELSE decimal(C*100/float(D), 12, 2) END "                              
  	                  +" from ( select ( select count( distinct a.caseno) from llclaimdetail a, llcase b "                          
  	                  +" where  a.grpcontno = '"+ContGrid.getRowColData(Row_2_3-1,1)+"' and a.caseno = b.caseno and b.endcasedate is not null) A, "
  	                  +" ( select count( distinct b.customerno) from llclaimdetail a, llcase b "                                     
  	                  +" where  a.grpcontno = '"+ContGrid.getRowColData(Row_2_3-1,1)+"' and a.caseno = b.caseno and b.endcasedate is not null) B, "            
  	                  +" ( select sum(c.realpay) from (select distinct a.caseno from llclaimdetail a, llcase b "                    
  	                  +" where  a.grpcontno = '"+ContGrid.getRowColData(Row_2_3-1,1)+"' and a.caseno = b.caseno and b.endcasedate is not null) a, "            
  	                  +" llcase b, llclaimdetail c where a.caseno = b.caseno and b.endcasedate is not null and b.caseno = c.caseno and  c.grpcontno = '"
  	                  +ContGrid.getRowColData(Row_2_3-1,1)+"') C, "
  	                  +"(SELECT "
                             + "(SELECT coalesce(SUM(SumActuPayMoney),0) FROM ljapaygrp WHERE endorsementno IS NULL and paytype <> 'YF' and paytype <> 'YEL' "
                             + "AND grpcontno='"+ContGrid.getRowColData(Row_2_3-1,1)+"')+"
                             +
                             "(SELECT coalesce(SUM(getmoney),0) FROM ljagetendorse WHERE "
                             + "grpcontno='"+ContGrid.getRowColData(Row_2_3-1,1)+"')"+ " FROM dual) D "
  	                  +"from dual ) as X with ur " ;    
			
		turnPage2.pageLineNum = 5;
		turnPage2.queryModal(sql_grpclaim, ClaimGrid);
		var claimdetailSQL = "select a.givetype,count(distinct a.caseno),sum(a.realpay) "
		                   + " from llclaimdetail  a, llcase b "
		                   + " where a.caseno=b.caseno and a.grpcontno='"+ContGrid.getRowColData(Row_2_3-1,1)
		                   + "' and b.endcasedate is not null group by a.givetype with ur";
		var claimarr = easyExecSql(claimdetailSQL);
		if (claimarr!=null) {
		for (var i=0 ; i < claimarr.length ; i++){
			if (claimarr[i][0]== "1" ){
				 fm.all('get1').value = claimarr[i][1];
				 fm.all('rget1').value = claimarr[i][2];
			}
			if (claimarr[i][0]== "2" ){
				 fm.all('get2').value = claimarr[i][1];
				 fm.all('rget2').value = claimarr[i][2];
			}
			if (claimarr[i][0]== "3" ){
				 fm.all('get3').value = claimarr[i][1];
				 fm.all('rget3').value = claimarr[i][2];
			}
			if (claimarr[i][0]== "4" ){
				 fm.all('get4').value = claimarr[i][1];
				 fm.all('rget4').value = claimarr[i][2];
			}
			if (claimarr[i][0]== "5" ){
				 fm.all('get5').value = claimarr[i][1];
				 fm.all('rget5').value = claimarr[i][2];
			}
		}
	}
}       

function clearinput()
{
	fm.all('get1').value = "";  fm.all('rget1').value = "";
	fm.all('get2').value = "";  fm.all('rget2').value = ""; 
	fm.all('get3').value = "";  fm.all('rget3').value = ""; 
	fm.all('get4').value = "";  fm.all('rget4').value = ""; 
	fm.all('get5').value = "";  fm.all('rget5').value = ""; 	
}                
                        
function GRDPrint()     
{                       
	var selno1 = PolGrid.getSelNo();
	var selno2 = ContGrid.getSelNo();
	var GrpNo="";
	if (selno2 <= 0)
	{
	      alert("请选择要打印赔付报告的团体保单");
	      return ;
	}
	GrpNo = PolGrid.getRowColData( selno1-1, 1);
	GrpContNo = ContGrid.getRowColData( selno2-1, 1);
  fm.target="GRPrt2";
  fm.fmtransact.value="PRINT";
  	
	
	var newWindow = window.open("GrpReportPrtD.jsp?GrpNo=" + GrpNo+ "&GrpContNo="+GrpContNo+"&fmtransact=PRINT");		
}

function GRPrint()
{
	var selno1 = PolGrid.getSelNo();
	var selno2 = ContGrid.getSelNo();
	var GrpNo="";
	if (selno2 <= 0)
	{
	      alert("请选择要打印赔付报告的团体保单");
	      return ;
	}
	if (fm.StartDate.value==null||fm.StartDate.value==''){
	  alert("请输入统计起期");
	  return;
	}
	if (fm.EndDate.value==null||fm.EndDate.value==''){
	  alert("请输入统计止期");
	  return;
	}
	GrpNo = PolGrid.getRowColData( selno1-1, 1);
	GrpContNo = ContGrid.getRowColData( selno2-1, 1);
	StartDate = fm.StartDate.value;
	EndDate = fm.EndDate.value;
  fm.target="GRPrt1";
  fm.fmtransact.value="PRINT";
  var newWindow = window.open("GrpReportPrt.jsp?GrpNo=" + GrpNo+ "&GrpContNo="+GrpContNo+"&RgtDateStart="+StartDate+ "&RgtDateEnd="+EndDate+"&fmtransact=PRINT");		
}

	function afterCodeSelect( cCodeName, Field )
	{
		if( cCodeName == "grprint" && Field.value=="1")
		{
			GRPrint();
		}
		if( cCodeName == "grprint" && Field.value=="2")
		{
			GRDPrint();
		}
	}
