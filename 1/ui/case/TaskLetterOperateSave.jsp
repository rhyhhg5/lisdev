<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskLetterOperateSave.jsp
//程序功能：
//创建日期：2005-08-22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
   <%@page import="com.sinosoft.lis.llcase.*"%>
  
<%             
	String FlagStr = "";
	String Content = "";
	String transact = request.getParameter("loadFlag");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	Content = "在这里了";
	//取得函件信息
	LGLetterSchema tLGLetterSchema = new LGLetterSchema();
	tLGLetterSchema.setEdorAcceptNo(request.getParameter("edorAcceptNo"));
	tLGLetterSchema.setSerialNumber(request.getParameter("serialNumber"));
	tLGLetterSchema.setFeedBackInfo(request.getParameter("feedBackInfo"));
	tLGLetterSchema.setBackFlag(request.getParameter("backFlag"));
	tLGLetterSchema.setBackDate(request.getParameter("backDate"));
	
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGLetterSchema);
	tVData.add(tG);
	
	//调用后台提交数据
	LTaskLetterOperateUI tTaskLetterOperateUI = new LTaskLetterOperateUI();
  	if (!tTaskLetterOperateUI.submitData(tVData, transact))
	{
		//失败
		FlagStr = "Fail";
		
		//需要处理后台错误
		if(tTaskLetterOperateUI.mErrors.needDealError())
		{
			System.out.println(tTaskLetterOperateUI.mErrors.getErrContent());
			
			Content = tTaskLetterOperateUI.mErrors.getErrContent();
		}
		else
		{
			Content = "数据保存失败!";
		}
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}
	
	Content = PubFun.changForHTML(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>
</html>