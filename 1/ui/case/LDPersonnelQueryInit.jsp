<%
//程序名称：LDPersonnelQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-04-13 23:19:03
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('Name').value = "";
    fm.all('Sex').value = "";
    fm.all('Birthday').value = "";
    fm.all('IDType').value = "";
    fm.all('IDNo').value = "";
    fm.all('Password').value = "";
    fm.all('NativePlace').value = "";
    fm.all('Nationality').value = "";
    fm.all('RgtAddress').value = "";
    fm.all('Marriage').value = "";
    fm.all('MarriageDate').value = "";
    fm.all('Health').value = "";
    fm.all('Stature').value = "";
    fm.all('Avoirdupois').value = "";
    fm.all('Degree').value = "";
    fm.all('CreditGrade').value = "";
    fm.all('OthIDType').value = "";
    fm.all('OthIDNo').value = "";
    fm.all('ICNo').value = "";
    fm.all('GrpNo').value = "";
    fm.all('JoinCompanyDate').value = "";
    fm.all('StartWorkDate').value = "";
    fm.all('Position').value = "";
    fm.all('Salary').value = "";
    fm.all('OccupationType').value = "";
    fm.all('OccupationCode').value = "";
    fm.all('WorkType').value = "";
    fm.all('PluralityType').value = "";
    fm.all('DeathDate').value = "";
    fm.all('SmokeFlag').value = "";
    fm.all('BlacklistFlag').value = "";
    fm.all('Proterty').value = "";
    fm.all('Remark').value = "";
    fm.all('State').value = "";
    fm.all('VIPValue').value = "";
    fm.all('GrpName').value = "";
    fm.all('speciality').value = "";
    fm.all('ManageCom').value = "";
    fm.all('OccupationGrade').value = "";
    fm.all('UserCode').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LDPersonnelQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm() {
  try {
    initInpBox();
    initSelBox();
    initLDPersonnelGrid();  
  }
  catch(re) {
    alert("LDPersonnelQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDPersonnelGrid;
function initLDPersonnelGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名

    iArray[1]=new Array();
    iArray[1][0]="用户编码";    	//列名
    iArray[1][1]="40px";            		//列宽
    iArray[1][2]=40;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="员工号码";    	//列名
    iArray[2][1]="40px";            		//列宽
    iArray[2][2]=40;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="用户姓名";         			//列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="业务职级";         			//列名
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="机构编码";         			//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=60;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="员工性别";    	//列名
    iArray[6][1]="0px";            		//列宽
    iArray[6][2]=40;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="出生日期";         			//列名
    iArray[7][1]="0px";            		//列宽
    iArray[7][2]=60;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="证件类型";         			//列名
    iArray[8][1]="0px";            		//列宽
    iArray[8][2]=60;            			//列最大值
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="证件类型名称";         			//列名
    iArray[9][1]="0px";            		//列宽
    iArray[9][2]=60;            			//列最大值
    iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[10]=new Array();
    iArray[10][0]="证件号码";    	//列名
    iArray[10][1]="0px";            		//列宽
    iArray[10][2]=40;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[11]=new Array();
    iArray[11][0]="学历专业";         			//列名
    iArray[11][1]="0px";            		//列宽
    iArray[11][2]=60;            			//列最大值
    iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[12]=new Array();
    iArray[12][0]="学历专业名称";         			//列名
    iArray[12][1]="0px";            		//列宽
    iArray[12][2]=60;            			//列最大值
    iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[13]=new Array();
    iArray[13][0]="业务职级";         			//列名
    iArray[13][1]="0px";            		//列宽
    iArray[13][2]=60;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[14]=new Array();
    iArray[14][0]="业务职级名称";         			//列名
    iArray[14][1]="0px";            		//列宽
    iArray[14][2]=60;            			//列最大值
    iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[15]=new Array();
    iArray[15][0]="电子邮箱";         			//列名
    iArray[15][1]="0px";            		//列宽
    iArray[15][2]=60;            			//列最大值
    iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[16]=new Array();
    iArray[16][0]="手机号码";         			//列名
    iArray[16][1]="0px";            		//列宽
    iArray[16][2]=60;            			//列最大值
    iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[17]=new Array();
    iArray[17][0]="电话号码";         			//列名
    iArray[17][1]="0px";            		//列宽
    iArray[17][2]=60;            			//列最大值
    iArray[17][3]=3;             			//是否允许输入,1表示允许，0表示不允许
    
    iArray[18]=new Array();
    iArray[18][0]="性别名称";         			//列名
    iArray[18][1]="0px";            		//列宽
    iArray[18][2]=60;            			//列最大值
    iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
    // 3340人员信息档案管理模块**start**
    iArray[19] =new Array();
    iArray[19][0]="医学背景";         			//列名
    iArray[19][1]="0px";            		//列宽
    iArray[19][2]=60;            			//列最大值
    iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[20] =new Array();
    iArray[20][0]="理赔工作年限";				//列名
    iArray[20][1]="0px";					//列宽
    iArray[20][2]=60;					    //列最大值
    iArray[20][3]=3;						//是否允许输入,1表示允许，0表示不允许
   
    iArray[21] =new Array();
    iArray[21][0]="理赔员评聘";				    //列名
    iArray[21][1]="0px";					//列宽
    iArray[21][2]=60;					    //列最大值
    iArray[21][3]=3;						//是否允许输入,1表示允许，0表示不允许
	
    iArray[22] =new Array();
    iArray[22][0]="评聘级别";				     //列名
    iArray[22][1]="0px";					//列宽
    iArray[22][2]=60;					    //列最大值
    iArray[22][3]=3;						//是否允许输入,1表示允许，0表示不允许
    
    iArray[23] =new Array();
    iArray[23][0]="医学背景名称";				 //列名
    iArray[23][1]="0px";					//列宽
    iArray[23][2]=60;					    //列最大值
    iArray[23][3]=3;						//是否允许输入,1表示允许，0表示不允许
	
  
    
    LDPersonnelGrid = new MulLineEnter( "fm" , "LDPersonnelGrid" ); 
    LDPersonnelGrid.mulLineCount = 10;
    LDPersonnelGrid.displayTitle = 1;
    LDPersonnelGrid.canSel=1;
//    LDPersonnelGrid.locked = 0;
    LDPersonnelGrid.hiddenPlus=1;   
    LDPersonnelGrid.hiddenSubtraction=1; 
    LDPersonnelGrid.loadMulLine(iArray);
    //这些属性必须在loadMulLine前
/*
    LDPersonnelGrid.mulLineCount = 0;   
    LDPersonnelGrid.displayTitle = 1;
    LDPersonnelGrid.canChk = 0;
    LDPersonnelGrid.selBoxEventFuncName = "showOne";
*/
    LDPersonnelGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
   //LDPersonnelGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
