<%
//程序名称：ClaimUnderwriteInput.jsp
//程序功能：审批审定页面
//创建日期：2002-06-19 11:10:36
//创建人  ：Wujs
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<%
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LLStringUwInput.js"></SCRIPT>


  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LLStringUwInit.jsp"%>
  
</head>

<body  onload="initForm();" >
	<form action="./LLStringUwInputSave.jsp" method=post name=fm target="fraSubmit">
	<table  class= common>
    <table  class= common>
				<TR  class= common8>
					<TD  class= title8>批次号</TD><TD  class= input8><Input class=common name="RgtNo" ></TD>
					<TD  class= title8>单位名称</TD><TD  class= input8><Input class=common name="GrpName" ></TD>
		      <TD  class= title8>保单号码</TD><TD  class= input8><Input class=common name="GrpContNo"></TD>
				</tr>
				
			</table>
		</div>
	</table>
    
    </Div> 
    
    <table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimPol);">
        </td>
        <td class= titleImg> 拨付明细</td>
      </tr>
    </table>
    <Div  id= "divAccountList" align = center style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanAccountGrid" >
              </span>
            </TD>
          </TR>
        </table>
        <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
        <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();">

      </Div>
    


 <table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divShowDetail);">
        </td>
        <td class= titleImg> 审批审定</td>
      </tr>
    </table>
    
      <Div  id= "divShowDetail" style="display: '';">
    
    <TABLE class=common>
	<TR class=common8>
	    	<TD  class= title8>审定结论</TD>
            <TD  class= input8><Input class=codeno name="DecisionSD" CodeData="0|2^1|同意审批结论^2|不同意审批结论" ondblClick="return showCodeListEx('X',[this,DecisionSDName],[0,1]);" onkeyup="return showCodeListKeyEx('X',[this,DecisionSDName],[0,1]);"><Input class=codename name=DecisionSDName></TD>
	    	<TD  class= title8></TD><TD  class= input8></td>
	    	<TD  class= title8></TD><TD  class= input8></td>
	</TR>
    </table>
    <TABLE class=common>
    	<TR  class= common8>
		<TD  class= title colspan="6">审定意见</TD>
	</TR>	
	<TR  class= common8>
		<TD  class= input colspan="6">
		    <textarea name="RemarkSD" cols="100%" rows="3"  class="common">
		    </textarea>
		</TD>
	</TR>
    </table>
    
 </div>

<br>

 <div id=divconfirm align='left'>

   	
	  <input class=cssButton style='width:60px;' type=button value="给付确认" name="Btn_GiveEn" onclick="GiveEnsure()">
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	<input type=hidden id="Handler" name="Handler">
 </Div>

</div>
  </form>
  
</body>
</html>
