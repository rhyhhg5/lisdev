//程序名称：LLSecondUW.js
//程序功能：合同审查
//创建日期：2006-01-01 15:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPagePol = new turnPageClass();
var turnPageEdor = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content); 
  } else { 
    getEdorItem();
    if (fm.all('fmtransact').value == 'INSERT||EDORAPP')
    {
    	DealContSearch();
    }
    if (fm.all('fmtransact').value == 'INSERT||EDORAPPCONFIRM')
    {  
    	PrtEdor();
    }
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
    cDiv.style.display="";
  else
    cDiv.style.display="none";  
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	k++;
	var tInsuredNo = fm.InsuredNo.value;
  var strSQL = "select ContNo,(select AppntNo from lccont where contno=p.contno),ManageCom,grpcontno,prtno,cvalidate,date(enddate)-1 day,conttype,proposalcontno,(select riskshortname from lmrisk m where m.riskcode=p.riskcode ) from LCPol p where appflag='1' "
	           + " and InsuredNo = '"+tInsuredNo+"'";
  strSQL += " union select ContNo,(select AppntNo from lbcont where contno=p.contno union select appntname from lccont where contno=p.contno),ManageCom,grpcontno,prtno,cvalidate,(CASE WHEN (SELECT date(EDORVALIDATE)-1 day FROM LPEDORITEM WHERE EDORNO = P.EDORNO AND EDORTYPE !='XB' and contno = p.contno order by modifydate desc fetch first  row only) is null then p.enddate else (SELECT EDORVALIDATE FROM LPEDORITEM WHERE EDORNO = P.EDORNO AND EDORTYPE !='XB' and contno = p.contno order by modifydate desc fetch first  row only) end),conttype,proposalcontno,(select riskshortname from lmrisk m where m.riskcode=p.riskcode ) from LBPol p where appflag='1' "
	           + " and InsuredNo = '"+tInsuredNo+"' with ur";
	//strSQL += " union select ContNo,(select AppntName from lccont where contno=p.contno union select appntname from lbcont where contno=p.contno),ManageCom,grpcontno,prtno,cvalidate,(CASE WHEN (SELECT date(EDORVALIDATE)-1 day FROM LPEDORITEM WHERE EDORNO = P.EDORNO AND EDORTYPE !='XB' and contno = p.contno order by modifydate desc fetch first  row only) is null then p.enddate else (SELECT EDORVALIDATE FROM LPEDORITEM WHERE EDORNO = P.EDORNO AND EDORTYPE !='XB' and contno = p.contno order by modifydate desc fetch first  row only) end),conttype,proposalcontno,(select riskshortname from lmrisk m where m.riskcode=p.riskcode ) from LBPol p where appflag='1' "
	       //    + " and InsuredNo = '"+tInsuredNo+"'";
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult != null )
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
			CheckGrid.setRowColData( i, j+1, arrResult[i][j] );
			}
		}
		//alert("result:"+arrResult);
	}
}


/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function onSelSelected()
{
	var row=PolGrid.getSelNo()-1;
	var ContNo="";
	var proposalcontno="";
	if(row>=0)
	{
		fm.ContNo.value = PolGrid.getRowColData(row,4);;
		fm.ContNoA.value=PolGrid.getRowColData(row,1);
		proposalcontno = PolGrid.getRowColData(row,9);
		
		fm.Remark.value="";
		var tsql = "select remark from lcgrpcont where grpcontno='"+fm.ContNo.value+"'";
		brr = easyExecSql(tsql);
		if(brr){
		  fm.Remark.value = brr[0][0]+"\n";
		}
		tsql = "select speccontent from lcspec a where a.contno='"+proposalcontno+"'";
		crr = easyExecSql(tsql);
		if(crr){
		  for(i=0;i<crr.length;i++){
		    fm.Remark.value += crr[i][0]+"\n";
		  }
		}
	}
	
}



function RenewSuggest()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameMainRenewSuggest.jsp?Interface=LLRenewSuggestInput.jsp"+varSrc,"续保建议",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function DealExemption()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameMainExemption.jsp?Interface=LLExemptionInput.jsp"+varSrc,"续保建议",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}


//将加费的信息显示出来；
function ShowAddFeeInfo()
{
		var strSql = "select prem,round(rate,2),paystartdate,payenddate from lcprem where ContNo='"
		+fm.ContNoA.value+"' and substr(payplancode,1,6)='000000' ";
		turnPage.queryModal(strSql, AddFeeGrid);
}

//将免责的信息显示出来；
function ShowSpecInfo()
{
		var strSql = "select SpecCode,SpecContent,'','',SerialNo from LCSpec where ContNo='"
		+fm.ContNoA.value+"'";
		turnPage.queryModal(strSql, SpecGrid);
}

function QueryOnKeyDown()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    var tsql = "select CustomerName,CustomerNo from llcase where CaseNo='"+fm.CaseNo.value+"'";
    var arr = easyExecSql(tsql);
    if(arr){
      fm.CustomerName.value = arr[0][0];
      fm.InsuredNo.value = arr[0][1];
    }
    easyQueryClick();
  }
}

function QueryOnEnter()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    var tsql = "select CustomerNo,Name from ldperson where 1=1"
    +getWherePart("Name","CustomerName")
    +getWherePart("CustomerNo","InsuredNo");
    var arr = easyExecSql(tsql);
    if (arr)
    {
      try
      {
        if(arr.length==1)
        {
          afterLLRegister(arr);
        }
        else{
          var varSrc = "&CustomerNo=" + fm.InsuredNo.value;
          varSrc += "&CustomerName=" + fm.CustomerName.value;
          varSrc += "&ContNo=";
          varSrc += "&IDType=";
          varSrc += "&IDNo=" ;
          showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
        }
      }catch(ex)
      {
        alert("错误:"+ ex.message);
      }
    }
  }
}

function afterLLRegister(arrReturn)
{
  fm.InsuredNo.value=arrReturn[0][0];
  fm.CustomerName.value=arrReturn[0][1];
      easyQueryClick();
}

function DealContSearch()
{	
	var strSQL = "select a.contno,a.edorno,a.insuredno,a.insuredname,a.appoperator,a.caseno,b.edorappdate from llcontdeal a,lpedorapp b "
	           + " where  a.edorno=b.edorAcceptNo and a.insuredno='"+fm.InsuredNo.value+"' with ur";
	turnPage.queryModal(strSQL, ContDealGrid);
}

function ContSearch()
{
	var strSQL= "select contno,'1',AppntNo,InsuredName,CValiDate,PaytoDate,PayIntv,prem,getUniteCode(a.agentcode),grpcontno,"
            + " (select laagent.name from laagent where a.agentcode=laagent.agentcode),codename('stateflag',a.stateflag),"
            + " (case when (select  state from lcconthangupstate where a.contno = contno order by makedate desc fetch first 1 rows only)= '1' then '已挂起' else '未挂起' end) "
            + " from lccont a where"
            + " appflag='1' and conttype ='1' and a.contno in (select contno from lcinsured where insuredno = '" + fm.InsuredNo.value + "' union select contno from lbinsured where insuredno = '" + fm.InsuredNo.value + "')"
            + " and (a.StateFlag is null or a.StateFlag in ('1','3','2')) "//#2573 by lyc
            + " and exists (select 1 from lcinsured where contno=a.contno and insuredno='"+fm.InsuredNo.value+"'"
            +" union all select 1 from lbinsured lb,llcontdeal lc where lc.insuredno='"+fm.InsuredNo.value+"' and EdorType in ('CT','HZ','CD')"
			+" and lc.contno=a.contno and lb.contno=lc.contno and lb.insuredno=lc.insuredno) "
            + " union all"
            + " select contno,'1',AppntNo,InsuredName,CValiDate,PaytoDate,PayIntv,prem,getUniteCode(a.agentcode),grpcontno,(select laagent.name from laagent where a.agentcode=laagent.agentcode) "
            + " ,codename('stateflag',a.stateflag) "
            + " ,(case when (select  state from lcconthangupstate where a.contno = contno order by makedate desc fetch first 1 rows only)= '1' then '已挂起' else '未挂起' end) "
            + " from lbcont a where"
            + " a.appflag='1' and a.conttype ='1' and a.contno in (select contno from lcinsured where insuredno = '" + fm.InsuredNo.value + "' union select contno from lbinsured where insuredno = '" + fm.InsuredNo.value + "')"
            + " and a.StateFlag in ('3','4')"
            + " and exists (select 1 from lbinsured where contno=a.contno and insuredno='"+fm.InsuredNo.value+"') "           
            + " and exists (select 1 from llcontdeal where contno=a.contno and EdorType in ('CT','HZ','CD'))"
            + " with ur";	
  turnPage1.queryModal(strSQL, ContGrid);
}
//【新建】-申请合同处理
function saveDealCont() 
{
	var tSelNo = ContGrid.getSelNo();
	if (tSelNo == 0)
	{
		alert("请选择保单!");
		return false;
	}
	//校验申请人权限
	if (!checkDealPopedom()){
		return false;
	}

	var state = ContGrid.getRowColDataByName(tSelNo - 1, 'state');	
	if (state=='2') {
		alert("该保单已处理！");
		return false;
	}
	
	var ContNo = ContGrid.getRowColDataByName(tSelNo - 1, 'contno');
	//校验保单存在未给付确认案件
	if (!checkDealCase(ContNo)){
		return false;
	}
	//var sql = "select StateType,State from LCContState where contno='"+ContNo+"' and PolNo='000000'";
	//var result = easyExecSql(sql);
	//if(result){
	//  if(result[0][0]=="Available"&&result[0][1]=="1"){
	//  	alert("保单"+ContNo+"处于失效状态!");
	//  }
	//	if(result[0][0]=="Terminate"&&result[0][1]=="1"){
	//		alert("保单"+ContNo+"处于满期终止状态!");
	//	}
	//}

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('fmtransact').value="INSERT||ContDeal";
	fm.action = "./LLContDealSave.jsp?statusNo=3";
	fm.target="fraSubmit";
	fm.submit();

}

function onSelContDeal() {
	var selNo = ContDealGrid.getSelNo();
	fm.EdorAcceptNo.value= ContDealGrid.getRowColDataByName(selNo-1,"EdorAcceptNo");
	fm.Edorappdate.value= ContDealGrid.getRowColDataByName(selNo-1,"Edorappdate");
	fm.ContNo.value = ContDealGrid.getRowColDataByName(selNo-1,"contno");
	fm.Operator.value = ContDealGrid.getRowColDataByName(selNo-1,"operator");
	getEdorItem();
	getPayMode();
}

function onSelCont() {
	var selNo = ContGrid.getSelNo();
	fm.ContNo.value = ContGrid.getRowColDataByName(selNo-1,"contno");
	fm.AppntNo.value = ContGrid.getRowColDataByName(selNo-1,"AppntNo");
	if (fm.EdorAcceptNo.value!=null&&fm.EdorAcceptNo.value!="") {
		getInsuredPolInfo(fm.ContNo.value);
	}	
	getPayMode();
}
function getPayMode(){
	fm.PayMode.value = "";
    fm.PayModeName.value = "";
    fm.BankCode.value = "";
    fm.BankName.value = "";
    fm.SendFlag.value = "";
    fm.AccNo.value = "";
    fm.AccName.value = "";
 
    var getSQL="select a.paymode,codename('paymode',a.paymode),a.bankcode,b.bankname, "
    		+" (CASE WHEN b.cansendflag='1' then '是' else '否' END),a.bankaccno,a.accname"
    	 	+" from ljaget a LEFT JOIN ldbank b ON a.BankCode = b.bankcode where otherno='"+ fm.EdorAcceptNo.value +"'"
    	 	+" union"
    	 	+" select a.paymode,codename('paymode',a.paymode),a.bankcode,b.bankname, "    	 
    		+" (CASE WHEN b.cansendflag='1' then '是' else '否' END),a.bankaccno,a.accname"
    	 	+" from ljtempfee c,ljtempfeeclass a LEFT JOIN ldbank b ON a.BankCode = b.bankcode "
    	 	+" where a.tempfeeno=c.tempfeeno and c.otherno='"+ fm.EdorAcceptNo.value +"'";
    var GetArr = easyExecSql(getSQL);
    if(GetArr)
    {
       fm.PayMode.value = GetArr[0][0];
       fm.PayModeName.value = GetArr[0][1];
       fm.BankCode.value = GetArr[0][2];
       fm.BankName.value = GetArr[0][3];
       fm.SendFlag.value = GetArr[0][4];
       fm.AccNo.value = GetArr[0][5];
       fm.AccName.value = GetArr[0][6];
    }else{
		//初始化收付信息
		 var tPayModeSQL = " SELECT c.paymode,codename('paymode',c.paymode),a.bankcode, b.bankname, "
	                      + " (CASE WHEN b.cansendflag='1' then '是' else '否' END), "
	                      + " a.bankaccno, a.accname "
	                      + " FROM lccont c, lcappnt a LEFT JOIN ldbank b ON a.BankCode = b.bankcode "
	                      + " WHERE a.contno=c.contno and c.contno='"+ fm.ContNo.value +"'"
	                      + " union "
	                      + " SELECT c.paymode,codename('paymode',c.paymode),a.bankcode, b.bankname, "
	                      + " (CASE WHEN b.cansendflag='1' then '是' else '否' END), "
	                      + " a.bankaccno, a.accname " 
	                      + " FROM lbcont c, lbappnt a LEFT JOIN ldbank b ON a.BankCode = b.bankcode "
	                      + " WHERE a.contno=c.contno and c.contno='"+ fm.ContNo.value +"'";
	  	var PayModeArr = easyExecSql(tPayModeSQL);
	  
	 	 if (PayModeArr) {
	      	  fm.PayMode.value = PayModeArr[0][0];
	          fm.PayModeName.value = PayModeArr[0][1];
	          fm.BankCode.value = PayModeArr[0][2];
	          fm.BankName.value = PayModeArr[0][3];
	          fm.SendFlag.value = PayModeArr[0][4];
	          fm.AccNo.value = PayModeArr[0][5];
	          fm.AccName.value = PayModeArr[0][6];
	  	}else{
		  	  fm.PayMode.value = "1";
	          fm.PayModeName.value = "现金";
	  	}
  	}
}

function getInsuredPolInfo(contNo)
{
	var strSQL ="select distinct RiskSeqNo, InsuredNo, InsuredName, LCPol.RiskCode, LMRiskApp.RiskName, " +
	            " Amnt,LCPol.mult, prem, CValiDate, PayToDate, ContNo,  LCPol.GrpContNo ,codename('stateflag',stateflag),InsuredBirthday " +
	            "from LCPol INNER JOIN lMRiskApp ON  LCPol.RiskCode=LMRiskApp.RiskCode" +" "+
	            "left join LCContPlanRisk ON LCPol.riskcode=LCContPlanRisk.riskcode"+" "+
	            "where ContNo = '" + contNo + "' " +
	            "and appflag = '1'"
	            +" union "
	            +" select distinct RiskSeqNo, InsuredNo, InsuredName, LBPol.RiskCode, LMRiskApp.RiskName, " 
	            +" Amnt,LBPol.mult, prem, CValiDate, PayToDate, ContNo,  LBPol.GrpContNo ,codename('stateflag',stateflag),InsuredBirthday "
	            +" from LBPol INNER JOIN lMRiskApp ON  LBPol.RiskCode=LMRiskApp.RiskCode" +" "
	            +" left join LCContPlanRisk ON LBPol.riskcode=LCContPlanRisk.riskcode"+" "
	            +" where ContNo = '" + contNo + "' " 
	            +" and appflag = '1'";
	turnPagePol.pageLineNum = 100;
	turnPagePol.queryModal(strSQL, PolGrid);
	
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getRowColData(i, 7) == "0")
		{
			PolGrid.setRowColData(i, 7, "");
		}
	}
}
//添加合同处理项目
function addRecord()
{
  getEdorType();
  if(!checkState())
  {
    return false;
  }
  
  if (fm.all('EdorAcceptNo').value==null || fm.all('EdorAcceptNo').value=="")
  {
    alert("请选择合同处理号！");
    return false;
  }
  
  var tSelNo = ContGrid.getSelNo();
	if (tSelNo == 0)
	{
		alert("请选择保单!");
		return false;
	}
  
  if (fm.all('ContNo').value==null || fm.all('ContNo').value=="")
  {
    alert("请选择保单！");
    return false;
  }
  
  if (!checkRight()){
		return false;
	}
  //校验解约回退项目只能对通过理赔进行解约、合同终止的保单	
  if (fm.all('EdorType').value=='RB')
  {
  	if (!checkRBCont())
  	{
		return false;
  	}
  } else if (fm.all('EdorType').value=='CT'){
  	if (!checkCTCont())
  	{
		return false;
  	}
  } 
  
  if (fm.all('EdorValiDate').value==null || fm.all('EdorValiDate').value=="")
  {
    alert("合同处理生效日期不能为空!");
    return false;
  }
  
  if (fm.all('EdorType').value==null||fm.all('EdorType').value=="")
  {
  		//EdorType即是AD、BB等
      alert("请选择需要添加的合同处理项目");
      return false;    
  }
  
  //added by suyanlin at 2007-11-19 14:05 start
    //撤销犹豫期撤保处理增加限制提示
    var mySql = "select getpoldate from LCCont where ContNo = '" + fm.ContNo.value + "'" ;
    var result = easyExecSql(mySql);
    if(result != "null" && result != "" && result != null)
    {
    }
    else
    {
    	if(confirm("保单未进行回执回销，是否继续对保单继续做合同处理？")==false)
    	{
    		return false;
    	}
    }  
  // added by suyanlin at 2007-11-14 14:11 start
  //个险在保单失效及满期终止状态下继续添加保全项目需要提示确认
  var sql = "select * from lccont "
  			  + "where contno = '" + fm.ContNo.value + "' "
  			  + "and stateflag in('2','3') ";
  var result = easyExecSql(sql);
  if(result)
  {
  	if(fm.all('EdorType').value != "RV" )
  	{
			if(hasULIRisk(fm.ContNo.value))
			{
				alert("万能险保单已失效!");
  				return false;
			}	
  	}
  	
  	if(confirm("该保单已经失效，是否继续操作？")==false)
  	{
  		return false;
    }  	
  }
    
//校验万能险
  if(hasULIRisk(fm.ContNo.value))
  {
  	if(!checkULIRisk())
  	{
  		return false;
  	}
  }
  //续期待核销的不能继续操作，有续期应收的提示是否继续
  if(!checkDueFee())
  {
    return false;  
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value="INSERT||EDORITEM";
  fm.action = "LCContDealItemSave.jsp"
  fm.target="fraSubmit";
  fm.submit();
}
function checkRBCont()
{	
  var sql = "select 1 from LMRiskApp as R,LbPol as P where R.RiskCode=P.RiskCode and  ContNo='"+fm.ContNo.value+"' and RiskType4='4'";
  var result = easyExecSql(sql);
  if(result)
  {
  	alert("万能险保单不能做解约回退!");
  	return false;
  }
  var sql = "select b.confdate,b.modifytime from llcontdeal a,lpedorapp b "
  	+" where a.edorno=b.edoracceptno and a.Contno = '" + fm.ContNo.value + "'"
  	+" and a.edortype in ('CT','HZ','CD') and b.edorstate='0' with ur";
  var result = easyExecSql(sql);
  if(!result)
  {
    alert("保单"+ fm.ContNo.value + "没有做过解约,合同终止,合同终止退费，不能进行解约回退!");
    return false;
  }else{
  		var Casesql="select 1 from llcase a,llclaimdetail b where a.caseno=b.caseno and b.contno='"+ fm.ContNo.value +"'"
  			+" and (a.rgtdate>'"+ result[0][0] +"' or (a.rgtdate='"+ result[0][0] +"' and a.maketime>'"+ result[0][1] +"'))";
  		var caseResult = easyExecSql(Casesql);
	   if(caseResult)
	   {
	    	alert("保单"+ fm.ContNo.value + "解约后做过理赔不能进行解约回退!");
	    	return false;
	   }		
  }
  return true;
}
function checkCTCont()
{
  var sql = "select 1 from lccont where Contno = '" + fm.ContNo.value + "' and appflag='1' and (StateFlag is null or StateFlag !='0')";
  var result = easyExecSql(sql);
  if(!result)
  {
    alert("保单"+ fm.ContNo.value + "不是有效保单，不能做解约或合同终止!");
    return false;
  }
  return true;
}

function checkState()
{
  var sql = "select 1 from LCCont where Contno = '" + fm.ContNo.value + "' "
          + "and State = '1' with ur" ;
  var result = easyExecSql(sql);
  if(result)
  {
    alert("保单"+ fm.ContNo.value + "已经被冻结。");
    return false;
  }
  return true;
}

function hasULIRisk(ContNo)
{
//	var sql = "select * from LMRiskApp where RiskCode in (select RiskCode from LCPol where ContNo='"+ContNo+"') and RiskType4='4'";
	var sql = "select 1 from LMRiskApp as R,LCPol as P where R.RiskCode=P.RiskCode and  ContNo='"+ContNo+"' and RiskType4='4'";
	var result = easyExecSql(sql);
	if(result)
	{
		return true;
	}
	return false;	
}

function checkULIRisk()
{
  	var sql = "select 1 from LPEdorItem where EdorAcceptNo = '" + fm.all('EdorAcceptNo').value + "' ";
	var result = easyExecSql(sql);
	if(result)
	{
		alert("不能再添加合同处理项目!\n万能险保单处理，同时只能对一个项目操作!");
	 	return false;
	}
	return true;	
}

/**
校验续期续保应收状态
a)	若续保续期已收费（待核销状态），则不得再进行保全的正常操作，系统提示“该保单有续期续保应收记录待核销，请核销后操作或先作废原应收记录后再操作”。
b)	若续保续期已抽档但未收费，系统提示“该保单有续期续保应收记录，是否继续？”，若继续操作则暂停原续期续保应收记录的收费操作，在保全结案的同时执行续期续保应收作废、抽档操作（系统自动运行），使用原来的应收号，原应收不用记录历史信息；

*/
function checkDueFee()
{
  var contNo = fm.ContNo.value;
  
  var sql = "  select dealState "
            +" from LJSPayB "
            +" where otherNo = '" + contNo + "' "
            +" and otherNoType = '2' "  //个单
            +" and dealState not in ('1', '2', '3','5','6') ";　//未结案,未作废,未撤销
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != "" && rs[0][0] != "null")
  {
    if(rs[0][0] == "4")  //待核销
    {
      alert("该保单有续期续保应收记录待核销，请核销后操作或先作废原应收记录后再操作");
      return false;
    }
    else
    {
      return confirm("该保单有续期续保应收记录，是否继续？");
    }
  }
  
  return true;
}

function getEdorItem()
{
  var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
  var tContNo = fm.all('ContNo').value;
  var tedorType = fm.EdorType.value;
  //险种信息初始化
	  if (tEdorAcceptNo != null)
	  {
	      var strSQL = "select a.EdorAcceptNo, a.EdorNo,(select case edortype when 'CT' then '解约' when 'HZ' then '合同终止' when 'RB' then '解约回退' when 'CD' then '合同终止退费' end from llcontdeal where edorno=a.edoracceptno), a.ContNo, b.AppntNo, a.InsuredNo, " +
	                   " a.EdorValiDate, case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已完成' when '4' then '试算完成' when '2' then '理算完成' when '9' then '待收费' end, " +
	                   " a.PolNo, (select edortype from llcontdeal where edorno=a.edoracceptno)" +
	                   "from LPEdorItem a, LCAppnt b " +
	                   "where a.ContNo = b.ContNo " +
	                   "and a.EdorAcceptNo = '" + tEdorAcceptNo + "' "
	                   +" union "
			           +" select a.EdorAcceptNo, a.EdorNo,(select case edortype when 'CT' then '解约' when 'HZ' then '合同终止' when 'RB' then '解约回退' when 'CD' then '合同终止退费' end from llcontdeal where edorno=a.edoracceptno), a.ContNo, (select appntno  from lbappnt where contno=a.contno), a.InsuredNo " 
	  				   +" ,a.EdorValiDate, case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已完成' when '4' then '试算完成' when '2' then '理算完成' when '9' then '待收费' end,a.polno, "
	  				   +" (select edortype from llcontdeal where edorno=a.edoracceptno)"
	  				   +" from lpedoritem a, LBAppnt b where 1=1 and a.ContNo = b.ContNo and a.EdorAcceptNo ='"+tEdorAcceptNo+"'";
	               //   +" select a.EdorAcceptNo, a.EdorNo, a.EdorType, a.ContNo, b.AppntNo, a.InsuredNo, a.EdorValiDate "
				   //	+", case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已提交' when '4' then '试算完成' when '2' then '理算完成'  end,a.PolNo "
				   //	+" from LPEdorItem a, LBAppnt b where '1205380064000'='1205380064000' and  a.ContNo = b.ContNo "
				   //	+" and a.ContNo = '"+tContNo+"'  fetch first 3000 rows only ";
				turnPageEdor.queryModal(strSQL, EdorItemGrid);     
	  }
 return true;
}
//合同处理项目明细
function detailEdor()
{
  if (EdorItemGrid.getSelNo() == 0)
  {
    alert("请选择一个批改项目！");
    return false;
  }
  detailEdorType();
}

function detailEdorType()
{
	var newWin;
	var itemDetailUrl="";
	//添加“合同终止退费”选项，按解约方式处理
	if (fm.EdorTypeValue.value=="CD"){
	var itemDetailUrl = "PEdorTypeCDInput.jsp";
	}else{
  	var itemDetailUrl = "PEdorType" + trim(fm.EdorTypeValue.value) + "Input.jsp";
	}
	newWin = window.open("EdorItemDetailMain.jsp?edorAcceptNo=" 
		     + fm.EdorAcceptNo.value + "&itemDetailUrl=" + itemDetailUrl 
		     + "&GrpContNo=" + fm.GrpContNo.value + "&CaseNo=" + fm.CaseNo.value + "&ContNo=" + fm.ContNo.value + "&EdorType=" + fm.EdorType.value, 
		     itemDetailUrl.substring(0, itemDetailUrl.indexOf(".jsp")),
		     'width=' + screen.availWidth + ',height=' + screen.availHeight 
		     + ',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,'
		     + 'resizable=1,status=0');
	
	if (newWin != null)
	{
		newWin.focus();
	}
}

function getEdorItemDetail(parm1, parm2)
{
  fm.EdorTypeValue.value=fm.all(parm1).all('EdorItemGrid10').value
  getEdorType();
  fm.EdorTypeName.value = getEdorTypeName(fm.EdorTypeValue.value);
  fm.ContNo.value=fm.all(parm1).all('EdorItemGrid4').value
  fm.EdorValiDate.value=fm.all(parm1).all('EdorItemGrid7').value;
  getPayMode();
}

function getEdorTypeName(edorType)
{
	var edorTypeName = "";
	if(edorType=="CT") {
  	edorTypeName = "解约";
  } else if (edorType=="HZ") {
  	edorTypeName = "合同终止";
  } else if (edorType=="RB") {
  	edorTypeName = "解约回退";
  } else if (edorType=="CD") {//此处暂定为CD（取ClaimDeal意）
    edorTypeName = "合同终止退费"
  }
  	
  return edorTypeName;
}
//删除合同处理项目
function delRecord()
{
	if(EdorItemGrid.mulLineCount==0)
	{
		alert("没有批改项目！");
		return false;
	}

  	if (!checkRight()){
		return false;
	}
 
	var tSelNo = EdorItemGrid.getSelNo()-1;
	if (tSelNo < 0)
	{
		alert("请选择需要删除的项目！");
		return false;
	}
	
	var strWorkSQL = "select 1 from lgwork where workno='"+fm.all('EdorAcceptNo').value+"' and statusno='5' ";
	var arrWorkResult = easyExecSql(strWorkSQL);
	if (arrWorkResult) {
		alert("合同处理已经完成，不能进行项目删除");
		return false;
	}
	
  	var state= EdorItemGrid.getRowColData(tSelNo,8);
  	var strSQL=" select EdorState from lpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"' ";
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{	
	 	if(arrResult[0][0]!="1"&& arrResult[0][0]!="3" &&arrResult[0][0]!="4")
	 	{
	 		alert("请选择未提交和未理算的合同处理项目!");
	 		return false;
	 	}
	}		
  	if(confirm("确认删除吗？"))
  	{
	  	var EdorAcceptNo = EdorItemGrid.getRowColData(tSelNo,1);
	  	var EdorNo = EdorItemGrid.getRowColData(tSelNo,2);
	  	var ContNo = EdorItemGrid.getRowColData(tSelNo,4);
	  	var InsuredNo = EdorItemGrid.getRowColData(tSelNo,6);
	  	var PolNo = EdorItemGrid.getRowColData(tSelNo,9);
	  	fm.all('Transact').value="I&EDORITEM";
	  	//fm.all("EdorType").value = EdorItemGrid.getRowColData(tSelNo,3);
	  	fm.action="./LLContDealItemSave.jsp?hEdorAcceptNo="+EdorAcceptNo+"&EdorNo="+EdorNo+"&"
	  	          +"ContNo="+ContNo+"&InsuredNo="+InsuredNo+"&PolNo="+PolNo+"&DelFlag=1";
	  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  	fm.target="fraSubmit";
	  	fm.submit();
	  	fm.all('fmtransact').value="";
	  	fm.all('EdorType').value="";
  	}
}
//合同处理理算
function edorAppConfirm()
{
  if (fm.all('EdorAcceptNo').value==null || fm.all('EdorAcceptNo').value=="")
  {
    alert("请选择合同处理号！");
    return false;
  }
  
	//add by chenlei 2008-4-24 未添加保全项目不能进行理算操作
	var strSQL=" select * from lpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"'";
	var arrResult = easyExecSql(strSQL);
	if(!arrResult)
	{
	  alert("未添加合同处理项目，不能进行理算！");	
	  return false;
	}
	
	if (!checkRight()){
		return false;
	}
	//add by chenlei 2008-4-24 本次保全申请，若有保全项目未录入完毕，不能进行保全理算
	var strSQL=" select 'x' from lpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and EdorState<>'1'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{
	  alert("本次申请，有合同处理项目处于未录入状态或已经理算完成，不能进行理算！");	
	  return false;
	}
	fm.all("fmtransact").value = "INSERT||EDORAPPCONFIRM";
	if (window.confirm("是否理算本次申请?"))
	{
		var showStr="正在计算数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action = "./LLContDealCalSubmit.jsp";
		fm.target="fraSubmit";
		fm.submit();
	}
}  

function afterSubmit2(FlagStr, content)
{
	try {showInfo.close();} catch(re) {}
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
		var getMoney=HaveFee(fm.EdorAcceptNo.value);

		if(getMoney=="")//查询出错
		{
			return;
		}

		//if(Number(getMoney)==0)
		//{
			edorAppAccConfirm();
		//	return;
		//}else
		{
		//	var ret=AppAccExist(fm.OtherNo.value);
		//	if(ret=="")
		//	{
		//		return;
		//	}
		//	if(ret=="No")
		//	{
		//		edorAppAccConfirm();
		//		return;
		//	}
		//	if(ret=="Yes")
		//	{
		//		if(Number(getMoney)>0)
		//		{
		//			var accMoney=AppAccHaveGetFee(fm.OtherNo.value);
		//			if(accMoney=="")
		//			{
		//				return;
		//			}
		//			if(Number(accMoney)>0)
		//			{
		//				var urlStr="BqAppAccConfirmMain.jsp?ContType=0&AccType=0&EdorAcceptNo="+fm.EdorAcceptNo.value+"&CustomerNo="+fm.OtherNo.value+"&DestSource=01";
		//				showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:200px");	
		//			}
		//			edorAppAccConfirm();
		//			return;
		//		}
		//		if(Number(getMoney)<0)
		//		{
		//			var urlStr="BqAppAccConfirmMain.jsp?ContType=0&AccType=1&EdorAcceptNo="+fm.EdorAcceptNo.value+"&CustomerNo="+fm.OtherNo.value+"&DestSource=11";
		//			showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:200px");	
		//			edorAppAccConfirm();
		//			return;
		//		}	
		//	}
		}		
	}
}

function edorAppAccConfirm()
{
	if (fm.all('EdorAcceptNo').value==null || fm.all('EdorAcceptNo').value=="")
  {
    alert("请选择合同处理号！");
    return false;
  }
  
  if(EdorItemGrid.mulLineCount==0)
	{
		alert("没有合同处理项目！");
		return false;
	}
	
	if (!checkRight()){
		return false;
	}
	
	fm.all("fmtransact").value = "INSERT||EDORAPPCONFIRM";	
	fm.action = "../bq/BqAppAccAfterConfirmSubmit.jsp";
	fm.target="fraSubmit";
	fm.submit();
}

function afterSubmit3( FlagStr, content,AccpetNo )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content); 
  }
  else
  { 
    DealContSearch();
    fm.EdorAcceptNo.value = AccpetNo;
    getInsuredPolInfo(fm.ContNo.value);
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

function getEdorType(){
	if(fm.EdorTypeValue.value=="HZ") {
		fm.EdorType.value="CT";
	}else if (fm.EdorTypeValue.value=="CT") {
		fm.EdorType.value="XT";
	}else if (fm.EdorTypeValue.value=="CD"){
	    fm.EdorType.value="XT"
	}else {
		fm.EdorType.value=fm.EdorTypeValue.value;
  	}
}

function PrtEdor(printType)
{
  if (fm.all('EdorAcceptNo').value==null || fm.all('EdorAcceptNo').value=="")
  {
    alert("请选择合同处理号！");
    return false;
  }
  if(EdorItemGrid.mulLineCount==0)
	{
		alert("没有合同处理项目！");
		return false;
	}
	var strSQL=" select 'x' from lpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and EdorState not in ('0','2','9')";
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{
	  alert("本次申请，未理算，不能进行查看明细！");	
	  return false;
	}
	fm.action = "../f1print/AppEndorsementF1PJ1.jsp?type="+printType;
	fm.target="f1print";		 	
	fm.submit();
}

//理算回退
function reCal()
{
  if (fm.all('EdorAcceptNo').value==null || fm.all('EdorAcceptNo').value=="")
  {
    alert("请选择合同处理号！");
    return false;
  }
  if(EdorItemGrid.mulLineCount==0)
	{
		alert("没有合同处理项目！");
		return false;
	}
	if (!checkRight()){
		return false;
	}
  
	var strSQL=" select 'x' from lpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and EdorState!='2'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{
	  alert("本次申请，未理算，不能进行重新理算！");	
	  return false;
	}
	fm.action ="../bq/PEdorReCalSubmit.jsp";
	fm.target="fraSubmit";
	fm.submit();
	alert("回退理算完成");
	getEdorItem();
}

function checkRight(){
	//案件处理人用户失效时，支持其有效上级（包含非直接上级）做理赔合同处理
	//Add By Houyd	
	//首先查询该受理号对应的申请人
	var appoperatorSQL="select appoperator from llcontdeal where edorno='"+fm.EdorAcceptNo.value+"'";
	var appoperatorResult = easyExecSql(appoperatorSQL);
	var appoperator=appoperatorResult[0][0];//受理申请人
	//其次查询受理人是否有权限
	var checkRightSQL="select 1 from llclaimuser where usercode='"+appoperator+"' and stateflag='1'";
	var result = easyExecSql(checkRightSQL);
	if (result) {
  		if (fm.CurrentOperator.value==appoperator) {
  			return true;
   	 	}else{
   	 		alert("您没有权限进行处理，只能由："+appoperator+"操作！");
   	 		return false;
   	 	}
  	}else{
		var upUser = appoperator;	
		while (true) {
			var rightSQL = "select a.upusercode,a.stateFlag"
	                 + " From llclaimuser a where a.usercode='"+appoperator+"'";
	    	var rightResult = easyExecSql(rightSQL);

	    	if (!rightResult) {
	    		alert("没有权限进行操作");
	    		return false;
	    	}
	    
	    	if ("1"==rightResult[0][1]) {
	    		break;
	    	}
	            
	    	upUser = rightResult[0][0];
	    	appoperator = upUser;
	  }	  
	  if (fm.CurrentOperator.value==upUser) {
	  	return true;
	  }else{
	  	alert("您没有合同处理申请权限，请用"+upUser+"进行处理");
	  	return false;
	  }
  }
}

function edorCancle() {
	if (fm.all('EdorAcceptNo').value==null || fm.all('EdorAcceptNo').value=="") {
		alert("请选择合同处理工单！");
		return false;
	}
	
	var varSrc = "&EdorAcceptNo=" + fm.EdorAcceptNo.value;
	pathStr="./FrameMainPEdorCancel.jsp?Interface=PEdorAppCancel.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"PEdorAppCancel","left");
}
//【合同处理确认】
function edorConfirm() {
	if (fm.all('EdorAcceptNo').value==null || fm.all('EdorAcceptNo').value=="") {
		alert("请选择合同处理工单！");
		return false;
	}
	
	var strSQL = "select 1 from lpedorapp a where a.edoracceptno='"+fm.EdorAcceptNo.value
	           + "' and a.edorstate='2' ";
	var arrResult = easyExecSql(strSQL);
	if (!arrResult) {
		alert("合同处理项目未理算");
		return false;
	}
	
	if (!checkConfirmRight()){
		return false;
	}		
	//校验收付方式信息
	if(fm.PayMode.value==null || fm.PayMode.value=="")
	{
		alert("请录入收付方式");
		return false;
	}
	if (fm.PayMode.value!="1" && fm.PayMode.value!="2") {
      if (fm.BankCode.value==null || fm.BankCode.value=="") {
          alert("请选择银行");
          return false;
      }
  	}
  
  if (fm.PayMode.value=="4" || fm.PayMode.value=="11") {
      if (fm.AccNo.value==null || fm.AccNo.value=="") {
          alert("请录入银行账号");
          return false;
      }
      
      if (fm.AccName.value==null || fm.AccName.value=="") {
          alert("请录入账户名");
          return false;
      }
  	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorConfirmSubmit.jsp";
	fm.target="fraSubmit";
	fm.submit();
}
//校验合同处理确认审批人权限
function checkConfirmRight() {	

	var uwSQL="select count(1) from LLContDealUWDetail where EdorNo='"+ fm.EdorAcceptNo.value +"'";
	var uwResult = easyExecSql(uwSQL);
	if(uwResult[0][0]<=0)
	{	
		var handlerSQL = "select handler from llcase where caseno='"+fm.CaseNo.value+"'";
		var arrResult = easyExecSql(handlerSQL);
		if (!arrResult) {
			alert("案件处理人查询失败");
			return false;
		}
		//查询案件申请人是否失效
		var handler = arrResult[0][0];
		var strSQL = "select 1 From llclaimuser a where a.usercode='"+handler+"' and stateflag='1'";
    	var mrr = easyExecSql(strSQL);
	 	if (mrr) {
	  		if (fm.CurrentOperator.value==handler) {
	  			return true;
	   	 	}else{
	   	 		alert("合同处理审批人为"+handler+",当前操作员没有权限进行操作");
	   	 		return false;
	   	 	}
	  	}
		else{
			var upUser = handler;	
			while (true) {
				var rightSQL = "select a.upusercode,a.stateFlag"
			                 + " From llclaimuser a where a.usercode='"+handler+"'";
			    var rightResult = easyExecSql(rightSQL);	
			    if (!rightResult) {
			    	alert("没有权限进行操作");
			    	return false;
			    }		    
			    if ("1"==rightResult[0][1]) {
			    	break;
			    }		            
			    upUser = rightResult[0][0];
			    handler = upUser;
			}		  
			if (fm.CurrentOperator.value==upUser) {
		  		return true;
			}
	  		else{
	  	  		alert("合同申请人已失效，请使用其上级用户"+upUser+"操作");	  	  		
		  		return false;
			}
		}	
	}else{
		var upSQL="select Dealer from LLContDeal where EdorNo='"+ fm.EdorAcceptNo.value +"'";
		var upResult = easyExecSql(upSQL);
		//查询案件审批人是否失效
		var dealer = upResult[0][0];
		var strSQL = "select 1 From llclaimuser a where a.usercode='"+dealer+"' and stateflag='1'";
    	var mrr = easyExecSql(strSQL);
		if (mrr) {
	  		if (fm.CurrentOperator.value==dealer) {
	  			return true;
	   	 	}else{
	   	 		alert("合同处理审批人为"+dealer+",当前操作员没有权限进行操作");
	   	 		return false;
	   	 	}
	  	}
		else{
			var upUser = dealer;	
			while (true) {
				var rightSQL = "select a.upusercode,a.stateFlag"
			                 + " From llclaimuser a where a.usercode='"+dealer+"'";
			    var rightResult = easyExecSql(rightSQL);	
			    if (!rightResult) {
			    	alert("合同处理审批人查询失败");
			    	return false;
			    }		    
			    if ("1"==rightResult[0][1]) {
			    	break;
			    }		            
			    upUser = rightResult[0][0];
			    dealer = upUser;
			}		  
			if (fm.CurrentOperator.value==upUser) {
		  		return true;
			}
	  		else{ 
	  	  		alert("合同审批人已失效，请使用其上级用户"+upUser+"操作"); 	  		
		  		return false;
			}
		}
	}	
}
//校验存在未给付确认案件
function checkDealCase(tContNo) 
{
	var strSql= "select distinct caseno from llcase "
				+" where customerno in (select insuredno from lcinsured where contno ='"+ tContNo +"' "
				+" union all select insuredno from lbinsured where contno ='"+ tContNo +"')  "
				+" and rgtstate not in('11','12','14') with ur ";	
	var arrResult = easyExecSql(strSql);
	if(arrResult)
	{
		alert("保单"+tContNo+"存在未给付确认案件"+arrResult+"");
		return false;
	}
	return true;
}
//校验申请人权限
function checkDealPopedom() 
{
	var handlerSQL = "select handler from llcase where caseno='"+fm.CaseNo.value+"'";
	var arrResult = easyExecSql(handlerSQL);
	if (!arrResult) {
		alert("案件处理人查询失败");
		return false;
	}
	var handler = arrResult[0][0];	
	var strSQL = "select 1 From llclaimuser a where a.usercode='"+handler+"' and stateflag='1'";
    var mrr = easyExecSql(strSQL);
 	if (mrr) {
  		if (fm.CurrentOperator.value==handler) {
  			return true;
   	 	}else{
   	 		alert("案件处理人为"+handler+",当前操作员没有权限进行操作");
   	 		return false;
   	 	}
  	}
	else{
		var upUser = handler;	
		while (true) {
		var rightSQL = "select a.upusercode,a.stateFlag"
	                 + " From llclaimuser a where a.usercode='"+handler+"'";
	    var rightResult = easyExecSql(rightSQL);

	    if (!rightResult) {
	    	alert("没有权限进行操作");
	    	return false;
	    }
	    
	    if ("1"==rightResult[0][1]) {
	    	break;
	    }
	            
	    upUser = rightResult[0][0];
	    handler = upUser;
	  }
	  
	  if (fm.CurrentOperator.value==upUser) {
	  	return true;
	  }
  	  else{
	  alert("您没有合同处理申请权限，请用"+upUser+"进行申请");
	  return false;
	  }
  }
}