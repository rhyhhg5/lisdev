<%
//程序名称：ImportRiskStateSave.jsp
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="java.util.*"%>
	<%@page import="java.io.*"%>
	<%@page import="org.apache.commons.fileupload.*"%>
<%
	String FileName = "";
	String filePath = "";
	boolean res = false;
	int count = 0;
	
	 //得到excel文件的保存路径
	 	//String path = application.getRealPath(subPath);
	 	String ImportPath = request.getParameter("ImportPath");
	 	System.out.println(ImportPath);
	 	String path = application.getRealPath("").replace('\\','/')+'/';  //application.getRealPath("")取到的路径是用"\"分隔的
	 	System.out.println(path);
	 	
  LDCode1DB saveLDCodeDB = new LDCode1DB();
  String RiskCode=request.getParameter("RiskCode");
  System.out.println(RiskCode);
  String RiskName=request.getParameter("RiskName");
  String FileType=request.getParameter("FileType");
  
    DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
    fu.setSizeMax(10000000);

// 设置最多只允许在内存中存储的数据,单位:字节
    fu.setSizeThreshold(4096);
    
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
    fu.setRepositoryPath(path+"temp");
//开始读取上传信息
    List fileItems = null;
    try{
     fileItems = fu.parseRequest(request);
    }
    catch(Exception ex)
    {
    	ex.printStackTrace();
    }


// 依次处理每个上传的文件
    Iterator iter = fileItems.iterator();
    System.out.println("^_^_^_^_^_^_^_^");
    while (iter.hasNext()) {
      FileItem item = (FileItem) iter.next();
      //忽略其他不是文件域的所有表单信息
      if (!item.isFormField()) {
        String name = item.getName();
        System.out.println(name);
        long size = item.getSize();
        if((name==null||name.equals("")) && size==0)
          continue;
        ImportPath= path + ImportPath;
        FileName = name.substring(name.lastIndexOf("\\") + 1);
        System.out.println("-----------importpath."+ImportPath + FileName);
        //保存上传的文件到指定的目录
        try {
          item.write(new File(ImportPath + FileName));
          count = 1;
          res = true;
          System.out.println("Save Begin ...");
          saveLDCodeDB.setCodeType("llriskfile");
          saveLDCodeDB.setCode1("0");
          saveLDCodeDB.setCode(RiskCode);
          saveLDCodeDB.getInfo();
          saveLDCodeDB.setCodeName(RiskName);
          if(FileType.equals("TK")||FileType.equals("JB"))
            saveLDCodeDB.setCodeAlias("1");
          if(FileType.equals("FL"))
            saveLDCodeDB.setOtherSign("1");
          saveLDCodeDB.delete();
          saveLDCodeDB.insert();
          System.out.println("Save End ...");
        } catch(Exception e) {
          System.out.println("upload file error ...");
        }
      }
    }
      System.out.println("upload successfully");
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "Fail";
	String Content = "";
	String Result="";
	
    if (res)
    {                          
    	Content = " 提交成功! ";
    	FlagStr = "Succ";
    	  System.out.println("---aaa");
   	}
	  else                                                                           
	  {
	  	Content = " 保存失败" ;
	   	FlagStr = "Fail";
	  }

%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
