
<jsp:directive.page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"/><%
//程序名称：SocialSecurityFeeSave.jsp
//程序功能：社保调查费结算提交页面
//创建日期：2014-05-26
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="jxl.*"%>
<%@page import="java.sql.ResultSet"%>;
<%

	//错误处理类
	CErrors tError = null;
	String flag="Fail";
	String content="";
	//接收类（接收前台传入的关于社保查勘的结算信息）
	SocialSecurityFeeClaimSchema tSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
	//接收类（接收前台传入的模糊查询信息）
	TransferData tTransferData = new TransferData();
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	//操作符
	String tOperate =request.getParameter("operate");		
	System.out.println("=========操作符："+tOperate);
	if("QUERY||MAIN".equals(tOperate)){
		String RgtDateS = request.getParameter("RgtDateS");
		String RgtDateE = request.getParameter("RgtDateE");
		String cbdUnitIdT = request.getParameter("cbdUnitIdT");
		String ydUnitIdT = request.getParameter("ydUnitIdT");
		String nameT = request.getParameter("nameT");
		String medicareCodeT = request.getParameter("medicareCodeT");
		String idNoT = request.getParameter("idNoT");
		
		tTransferData.setNameAndValue("RgtDateS",RgtDateS);
		tTransferData.setNameAndValue("RgtDateE",RgtDateE);
		tTransferData.setNameAndValue("cbdUnitIdT",cbdUnitIdT);
		tTransferData.setNameAndValue("ydUnitIdT",ydUnitIdT);
		tTransferData.setNameAndValue("nameT",nameT);
		tTransferData.setNameAndValue("medicareCodeT",medicareCodeT);
		tTransferData.setNameAndValue("idNoT",idNoT);
				
	}else if("INSERT||MAIN".equals(tOperate)){
		String tRadio[] = request.getParameterValues("InpContGridSel"); 
 		for(int index =0 ; index<tRadio.length; index++)
 		{
 			if(tRadio[index].equals("1"))
	 		{	
	 			System.out.println("index:"+index);
				String SurveyCaseNo[] =request.getParameterValues("ContGrid1");
				String SettlementNo[] =request.getParameterValues("ContGrid2");
				String PaymentVoucherNo[] =request.getParameterValues("ContGrid3");
				String InsuranceComName[] =request.getParameterValues("ContGrid4");
				String RemoteSurveyComName[] =request.getParameterValues("ContGrid5");
				String TotalAmount[] =request.getParameterValues("ContGrid6");
				String SurveyStartDate[] =request.getParameterValues("ContGrid7");
				String SurveyEndDate[] =request.getParameterValues("ContGrid8");
				String CreateDate[] =request.getParameterValues("ContGrid9");
				String Name[] =request.getParameterValues("ContGrid10");
				String MedicareCode[] =request.getParameterValues("ContGrid11");
				String IdNo[] =request.getParameterValues("ContGrid12");
				String InsuranceComCode[] =request.getParameterValues("ContGrid13");
				String RemoteSurveyComCode[] =request.getParameterValues("ContGrid14");
				String ActuGetNo[] =request.getParameterValues("ContGrid15");
				String CostCenter[] =request.getParameterValues("ContGrid16");
		
				System.out.println("选中的记录为第"+index+"行结算号："+SettlementNo[index]);
				tSocialSecurityFeeClaimSchema.setSurveyCaseNo(SurveyCaseNo[index]);
				tSocialSecurityFeeClaimSchema.setSettlementNo(SettlementNo[index]);
				tSocialSecurityFeeClaimSchema.setPaymentVoucherNo(PaymentVoucherNo[index]);
				tSocialSecurityFeeClaimSchema.setInsuranceComName(InsuranceComName[index]);
				tSocialSecurityFeeClaimSchema.setRemoteSurveyComName(RemoteSurveyComName[index]);
				tSocialSecurityFeeClaimSchema.setTotalAmount(TotalAmount[index]);
				tSocialSecurityFeeClaimSchema.setSurveyStartDate(SurveyStartDate[index]);
				tSocialSecurityFeeClaimSchema.setSurveyEndDate(SurveyEndDate[index]);
				tSocialSecurityFeeClaimSchema.setCreateDate(CreateDate[index]);
				tSocialSecurityFeeClaimSchema.setName(Name[index]);
				tSocialSecurityFeeClaimSchema.setMedicareCode(MedicareCode[index]);
				tSocialSecurityFeeClaimSchema.setIdNo(IdNo[index]);
				tSocialSecurityFeeClaimSchema.setInsuranceComCode(InsuranceComCode[index]);
				tSocialSecurityFeeClaimSchema.setRemoteSurveyComCode(RemoteSurveyComCode[index]);	
				tSocialSecurityFeeClaimSchema.setActuGetNo(ActuGetNo[index]);
				tSocialSecurityFeeClaimSchema.setCostCenter(CostCenter[index]);	
	 		}
		}	
	}
		
	SocialSecurityFeeBL aSocialSecurityFeeBL = new SocialSecurityFeeBL();
	try{
		VData tVData = new VData();
		tVData.add(tG);
		tVData.add(tSocialSecurityFeeClaimSchema);	
		tVData.add(tTransferData);	
		aSocialSecurityFeeBL.submitData(tVData,tOperate);
	} catch(Exception ex)
	  {
		content = "保存失败，原因是:" + ex.toString();
	    flag = "Fail";
	  }
	if(!aSocialSecurityFeeBL.mErrors.needDealError()){
		if("INSERT||MAIN".equals(tOperate)){
			content = "核心生成结算数据成功！";
		    flag = "SuccessAdd";
		}else if("QUERY||MAIN".equals(tOperate)){
			content = "数据查询成功！";
		    flag = "SuccessQuery";
		}
		
	}else{
		content = "保存失败，原因是:" +aSocialSecurityFeeBL.mErrors.getFirstError();
	    flag = "Fail";
	}
	
	
%>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>