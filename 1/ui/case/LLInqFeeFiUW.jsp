<html>
	<%
	//  Name：LLInqFeeFiUW.jsp
	//  Function：查勘费用财务审核页面
	//  Date：2006-09-13 16:49:22
	//  Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import="java.util.*"%>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String Operator=tG.Operator;
	String Comcode=tG.ManageCom;

	String CurrentDate= PubFun.getCurrentDate();
	String AheadDays="-60";
	FDate tD=new FDate();
	Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
	FDate fdate = new FDate();
	String afterdate = fdate.getString( AfterDate );
	%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLInqFeeFiUW.js"></SCRIPT>
		<%@include file="LLInqFeeFiUWInit.jsp"%>
		<script language="javascript">
			function initDate(){
				fm.RgtDateS.value="<%=afterdate%>";
				fm.RgtDateE.value="<%=CurrentDate%>";
				fm.Year.value=fm.RgtDateE.value.substring(0,4);
				fm.Operator.value="<%=Operator%>";
				var comcode="<%=Comcode%>";
				fm.ComCode.value=comcode;
			}
		</script>
	</head>

	<body  onload="initDate();initForm();" >
		<form action="./LLInqFeeFiUWSave.jsp" method=post name=fm target="fraSubmit">

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFeeType);">
					</TD>
					<TD class= titleImg>调查费用类型</TD>
				</TR>
			</table>
			<Div  id= "divFeeType" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD class= title8>调查费用类型</TD>
						<TD class= input8><input class=codeno name=FeeType CodeData="0|2^1|直接^2|间接" onclick="return showCodeListEx('sfeetype',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('sfeetype',[this,FeeTypeName],[0,1]);"><input class=codename name=FeeTypeName></TD>
						<TD class= title8>管理机构</TD>
						<TD class= input8><input class=codeno name=ComCode onclick="getstr();return showCodeList('comcode',[this,ComName],[0,1],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode',[this,ComName],[0,1],null,str,'1');"><input class=codename name=ComName></TD>
						<TD class= title8>审核状态</TD>
						<TD class= input8><input class=codeno name=UWType CodeData="0|2^1|未审核^2|已审核^3|全部" onclick="return showCodeListEx('uwtype',[this,UWTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('uwtype',[this,UWTypeName],[0,1]);"><input class=codename name=UWTypeName></TD>
					</TR>
				</table>
			</DIV>

			<div id= "divZJ" style= "display:''">
				<table class= common>
					<TR class= common8>
						<TD class= title8>提调日期从</TD>
						<TD class= input8><input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
						<TD class= title8>到</TD>
						<TD class= input8><input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
					</TR>
					<TR class= common8>
						<TD class= title8>理赔号</TD>
						<TD class= input8><input class=common name="CaseNo" ></TD>
						<TD class= title8>调查员</TD>
						<TD  class= input8><input class=codeno name="Replyer" onclick="Dispatch(1);return showCodeList('optname',[this,ReplyerName], [0,1],null,Str,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,ReplyerName], [0,1],null,Str,'1');" ><input class=codename name= ReplyerName></TD>
						<TD class= title8>调查主管</TD>
						<TD  class= input8><input class=codeno name="Inspector" onclick="Dispatch(2);return showCodeList('optname',[this,InspectorName], [0,1],null,Str1,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,InspectorName], [0,1],null,Str1,'1');" ><input class=codename name= InspectorName></TD>
					</TR>
				</table>
				<input name="AskIn" style="display:''"  class=cssButton type=button value="查  询" onclick="easyQuery()">
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSurvey);">
						</TD>
						<TD class= titleImg>调查案件列表</TD>
					</TR>
				</table>
				<Div  id= "divSurvey" align=center style= "display: ''">
					<table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanSurveyGrid" >
								</span>
							</TD>
						</TR>
					</table>
					<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
					<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
					<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
					<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
				</div>

				<div id= "divInqFeeItem" style= "display:''">
					<table>
						<TR>
							<TD><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInqFeeItemDetail);"></TD>
							<TD class= titleImg>直接调查费用明细</TD>
						</TR>
					</table>
					<Div  id= "divInqFeeItemDetail" align=center style= "display: ''">
						<table  class= common>
							<TR  class= common>
								<TD text-align: left colSpan=4>
									<span id="spanInqFeeDetailGrid" ></span>
								</TD>
							</TR>
							<TR  class= common>
								<TD  class= title8>合  计</TD><TD  class= input8><input class= readonly name="SumInqFee" readonly></TD>
								<TD  class= title8>财务审核通过金额</TD><TD  class= input8><input class= readonly name="UWInqFee" readonly></TD>
							</TR>
						</table>
					</Div>
				</Div>
			</div>

			<Div  id= "divJJ" style= "display: 'none'">
				<table class= common border=0 width=100%>
					<TR  class= common>
						<TD class= title>统计年份</TD>
						<TD class= input> <Input class=common name= Year onkeydown="QueryOnKeyDown();"></TD>
						<TD class= title>统计月份</TD>
						<TD class= input> <Input name=Month class=codeno  ondblClick="showCodeListEx('Month',[this,MonthName],[0,1]);" onkeyup="showCodeListKeyEx('Month',[this,MonthName],[0,1]);" ><input class=codename name=MonthName> </TD>
						<TD class= input style="width=30%"></TD>
					</TR>
				</table>
				<input name="AskIn" style="display:''"  class=cssButton type=button value="查  询" onclick="easyQuery()">
				<Table>
					<TR>
						<TD class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssignCaseList);"></TD>
						<TD class= titleImg>当月调查案件</TD>
					</TR>
				</Table>
				<Div  id= "divAssignCaseList" align =center style= "display: ''">
					<Table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanSurveyCaseGrid" ></span>
							</TD>
						</TR>
					</Table>
				</div>
				<Div id="divIndirectFee" align="left">
					<table >
						<tr >
							<td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divIndFeeDetail);"></td >
							<td class= titleImg>间接调查费用明细</td>
						</tr>
					</table>
					<Div  id= "divIndFeeDetail" align = center style= "display: ''">
						<table  class= common >
							<TR  class= common>
								<TD text-align: left colSpan=1>
									<span id="spanAddFeeGrid" ></span>
								</TD>
							</TR>
						</table>
					</Div>
				</div>
				<br>
				<div id='divLDPerson1'>
					<table  class= common>
						<TR  class= common8>
							<TD  class= title8>合  计</TD><TD  class= input8><input class= readonly name="IndFeeSum" readonly></TD>
							<TD  class= title8>案件平均</TD><TD  class= input8><input class= readonly name="Average" readonly></TD>
							<TD  class= title8>录入人</TD><TD  class= input8><input class= readonly name="Handler" readonly></TD>
						</TR>
					</table>
				</div>
				<hr>
			</Div>
			<INPUT VALUE="确  认" class= cssButton TYPE=button onclick="submitForm();">

			<!--隐藏域-->
			<Input type="hidden" name="fmtransact" >
			<Input type="hidden" name="Operator" >
			<Input type="hidden" name="SurveyNo">
    	<input type="hidden" name="MEndDate">
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
