//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifyList()
{
    if(!verifyInput2())
    {
        return false;
    }
	if((fm.PrtNo.value==""||fm.PrtNo.value==null)&&
		(fm.tIDNo.value=="" || fm.tIDNo.value==null)&&
		(fm.CardNo.value=="" || fm.CardNo.value==null))
	{
		alert("请至少录入结算单号、保险卡号、证件号码其中的一项！");
		return false;
	}
	
	cleanCertInsuMul();
	var tInfoSQL = "";
	if ((fm.tIDNo.value!="" && fm.tIDNo.value!=null)
	    || (fm.CustomerName.value!="" && fm.CustomerName.value!=null)
	    || (fm.tIDType.value!="" && fm.tIDType.value!=null)){
	    tInfoSQL = " and exists (select 1 from licardactiveinfolist where cardno=lict.cardno "
	             + getWherePart("idno","tIDNo")
	             + getWherePart("name","CustomerName")
	             + getWherePart("idtype","tIDType")
	             + ") ";
	}
	
    var tStrSql = ""
        + " select lict.CardNo, lict.PrtNo, (select wrapname from ldwrap ld,lmcardrisk lm where ld.riskwrapcode=lm.riskcode and lm.certifycode=lict.certifycode),"
        + " licti.CValidate, licti.ActiveDate, "
        + " licti.Name, licti.IdNo, lict.InsuPeoples, "
        + " CodeName('certifycontstate', lict.State) State, "
        + " (case lict.WSState when '00' then '未确认' when '01' then '已确认' end) WSState"
        + " from LICertify lict "
        + " left join LICardActiveInfoList licti on licti.CardNo = lict.CardNo and licti.SequenceNo = '1' "
        + " where 1=1"
  //      + " lict.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        + getWherePart("lict.CardNo", "CardNo")
        + getWherePart("lict.PrtNo", "PrtNo")
        + tInfoSQL
        + " with ur";
	//alert(tStrSql);
    turnPage1.pageDivName = "divCertifyListGridPage";
    turnPage1.queryModal(tStrSql, CertifyListGrid);
    
    if (!turnPage1.strQueryResult)
    {    
        //针对撕单不存licardactiveinfolist的处理
        var strSQL = ""
        + " select lict.CardNo, lict.PrtNo,(select wrapname from ldwrap ld,lmcardrisk lm where ld.riskwrapcode=lm.riskcode and lm.certifycode=lict.certifycode), lict.CValidate, lict.ActiveDate, "
        + " licti.Name, licti.IdNo, lict.InsuPeoples, "
        + " CodeName('certifycontstate', lict.State) State, "
        + " (case lict.WSState when '00' then '未确认' when '01' then '已确认' end) WSState,licti.customerno "
        + " from LICertify lict "
        + " left join LICertifyInsured licti on licti.CardNo = lict.CardNo and licti.SequenceNo = '1' "
        + " where 1=1"
   //     + " lict.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        + getWherePart("lict.CardNo", "CardNo")
        + getWherePart("lict.PrtNo", "PrtNo")
        + tInfoSQL
        + " with ur "
        ;
        var arrResult1=easyExecSql(strSQL);
	    if(arrResult1 != null)
	    {
	      	turnPage1.pageDivName = "divCertifyListGridPage";
	    	turnPage1.queryModal(strSQL, CertifyListGrid);
	    }
	    if(arrResult1 == null)
	    {
	        alert("没有待处理的结算信息,请确认：该卡信息导入核心系统并且已经完成网站激活！");
        	return false;
	    }
    }
    queryCertInsuList();
    return true;
}

/**
 * 卡折实名化
 */
function dealWSCertify()
{
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    fm.BCardNo.value = CertifyListGrid.getRowColData(tRow,1);
    
    //var strSQL="select count(1) from licardactiveinfolist lict where batchno is null and not exists (select 1 from licertifyinsured "
    //			+" where cardno = lict.cardno) and cardno =  '"+fm.BCardNo.value+"'";
   var strSQL="select count(1) from licertify lict where activeflag='00' and state!='00'  and not exists (select 1 from licertifyinsured "
    	+" where cardno = lict.cardno) and cardno =  '"+fm.BCardNo.value+"' and exists (select 1 from licardactiveinfolist where cardno = lict.cardno) ";
    var arrResult=easyExecSql(strSQL);
    if(arrResult)
    {
    	if(arrResult[0][0]==1){
    		fm.all('Flag').value='1';
    	}
    	else
    	{
    		fm.all('Flag').value='0';
		}
    }

    showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.btnWSCertify.disabled = true;
    fm.action = "LLWSCertifySave.jsp";
    fm.submit();
    fm.action = "";
}

/**
 * 实名化提交后动作。
 */
function afterWSSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("确认失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //queryCertifyList();
        //fm.BatchNo.value = cBatchNo;
        //queryCertifyImportBatchList();
        for(var rowNum=0;rowNum<CertifyListGrid.mulLineCount;rowNum++){
        	if (fm.BCardNo.value==CertifyListGrid.getRowColData(rowNum,1)) {
        		var SQL = "select customerno from LICertifyInsured where cardno='"+fm.BCardNo.value+"'";
        		arrResult = easyExecSql(SQL);
        		
        		CertifyListGrid.setRowColData(rowNum,9,"已确认");
        		//CertifyListGrid.setRowColData(rowNum,9,arrResult[0][0]);
        		break;
        	}
        	
        }
        queryCertInsuList();
    }

    fm.btnWSCertify.disabled = false;
}

function returnParent()
{
	var tSel = CertInsuListGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			var CustomerNo = CertInsuListGrid.getRowColDataByName(tSel-1,"CustomerNo");
			if (CustomerNo==""||CustomerNo==null) {
				alert("该卡折未确认");
				return false;
			}
			top.opener.afterCertify(CustomerNo);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

/**
 * 清除被保人信息查询列表
 */
function cleanCertInsuMul()
{
    CertInsuListGrid.clearData();
    fm.all('divCertInsuListGrid').style.display = "none";
}

/**
 * 查询被保人信息
 */
function queryCertInsuList()
{
    /**
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = CertifyListGrid.getRowData(tRow);
    var tCardNo = tRowDatas[0];
    */
    var tStrSql = ""
        + " select licail.CardNo, "
        + " licail.Name, CodeName('sex', licail.Sex), licail.Birthday, CodeName('idtype', licail.IdType), licail.IdNo ,"
        +"CodeName('occupationtype',nvl((select occupationtype from lcinsured where insuredno = licail.customerno and contno = licail.cardno "
        +"union select occupationtype from lbinsured where insuredno = licail.customerno and contno = licail.cardno),licail.occupationtype)),"
        + " licail.customerno "
        + " from licertifyinsured licail "
        + " left join LICertify lic on lic.CardNo = licail.CardNo"
        + " where 1 = 1 "
        
//        + " and licti.CardNo = '" + tCardNo + "'"
        + getWherePart("licail.CardNo", "CardNo")
        + getWherePart("lic.PrtNo", "PrtNo")
        + getWherePart("licail.idno", "tIDNo")
        + getWherePart("lic.CValidate", "StartCValidate", ">=")
        + getWherePart("lic.CValidate", "EndCValidate", "<=")
        + getWherePart("licail.name","CustomerName")
        + getWherePart("licail.idtype","tIDType")
        
        ;
    
    turnPage2.pageDivName = "divCertInsuListGridPage";
    turnPage2.queryModal(tStrSql, CertInsuListGrid);
    
    if (!turnPage2.strQueryResult)
    {
        alert("该卡未找到被保人相关信息！");
        return false;
    }
    
    fm.all('divCertInsuListGrid').style.display = "";
}

