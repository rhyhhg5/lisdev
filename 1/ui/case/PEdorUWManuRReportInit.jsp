<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuRReportInit.jsp
//程序功能：保全生存调查报告录入
//创建日期：2006-03-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initLCInsuredGrid();
    initInvestigateGrid();
    queryInsured();
  }
  catch(ex) 
  {
    alert("页面初始化错误，请重新登陆！");
  } 
}

function initInpBox()
{
  fm.EdorNo.value = "<%=request.getParameter("EdorNo")%>";
  fm.AppntNo.value = "<%=request.getParameter("AppntNo")%>";
  fm.ContNo.value = "<%=request.getParameter("ContNo")%>";
}

function initLCInsuredGrid() 
{
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号";    				//列名1
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="客户号";         			//列名2
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="姓名";         			//列名8
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         			//列名5
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
    iArray[4][4]="Sex";

    iArray[5]=new Array();
    iArray[5][0]="出生日期";         		//列名6
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="证件类型";         		//列名6
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][4]="IDType";

    iArray[7]=new Array();
    iArray[7][0]="证件号码";         		//列名6
    iArray[7][1]="150px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="联系电话";         		//列名6
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="通知书号码";         		//列名6
    iArray[9][1]="80px";            		//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="体检状态";         		//列名6
    iArray[10][1]="100px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    LCInsuredGrid = new MulLineEnter( "fm" , "LCInsuredGrid" ); 
    //这些属性必须在loadMulLine前
    LCInsuredGrid.mulLineCount = 0;    
    LCInsuredGrid.displayTitle = 1;
    LCInsuredGrid.canSel = 1;
    LCInsuredGrid.hiddenPlus = 1; 
    LCInsuredGrid.hiddenSubtraction = 1;
    LCInsuredGrid.selBoxEventFuncName = "onClickedInsured";
    LCInsuredGrid.detailInfo = "单击显示详细信息";
    LCInsuredGrid.loadMulLine(iArray);  
  }
  catch(ex) 
  {
    alert(ex);
  }
}

function initInvestigateGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="契调项目编号";    	//列名
    iArray[1][1]="50px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4] = "RReportCode1";             			//是否允许输入,1表示允许，0表示不允许
    iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
    iArray[1][18] = 150;
    
    iArray[2]=new Array();
    iArray[2][0]="契调项目名称";         			//列名
    iArray[2][1]="90px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="补充说明";         			//列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    
    InvestigateGrid = new MulLineEnter( "fm" , "InvestigateGrid" ); 
    //这些属性必须在loadMulLine前                            
    InvestigateGrid.mulLineCount = 0;
    InvestigateGrid.displayTitle = 1;
    InvestigateGrid.canChk = 0;
    InvestigateGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //HealthGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>


