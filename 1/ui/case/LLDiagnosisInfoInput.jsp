<html> 
<%
  //程序名称：LLDiagnosisInfoInput.jsp
  //程序功能：诊断数据信息（含门诊特殊病）信息
  //创建日期：2015-02-09 
  //创建人  ：Liyunxia
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LLDiagnosisInfoInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLDiagnosisInfoInit.jsp"%>
  
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
 
<title>诊断数据信息（含门诊特殊病）</title>   
</head>
<body onload="initForm(); initElementtype();" >
<form action="./LLDiagnosisInfoSave.jsp" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<%@include file="../common/jsp/InputButton.jsp"%>
			<hr>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDHospital1);">
    		</td>
    		 <td class= titleImg>
        		 诊断数据信息（含门诊特殊病）
       	 </td>   		 
    	</tr>
    </table>
<Div  id= "divLDHospital1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      地区疾病编码
    </TD>
    <TD  class= input>
      <Input class=common name=DiseaseCode elementtype=nacessary  verify="地区疾病编码|NOTNULL&len<=30">
    </TD>
    <TD  class= title>
      ICD9编码
    </TD>
    <TD  class= input>
      <Input class=common name=ICD9 >
    </TD>    
  	<TD  class= title>
      ICD10编码
    </TD>
    <TD  class= input>
      <Input class=common name=ICD10 elementtype=nacessary  verify="ICD10编码|NOTNULL&len<=30">
    </TD>
  	
  </TR>
  <TR  class= common>
    <TD  class= title>
      疾病名称
    </TD>
    <TD  class= input>
      <Input class=common name=DiseaseName elementtype=nacessary verify="疾病名称|NOTNULL&len<=200">
    </TD> 
    <TD  class= title>
      英文名
    </TD>
    <TD  class= input>
      <Input class=common name=EnglishName >
    </TD>
	<TD  class= title>
      疾病分类
    </TD>
    <TD  class= input>
	  <Input class= 'common' name=DiseaseClassification >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      子类别
    </TD>
    <TD  class= input>
      <Input class=common name=SubCategory >
    </TD> 
    <TD  class= title>
      拼音简码
    </TD>
    <TD  class= input>
      <Input class=common name=Phonetic >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      备注
    </TD>
		<TD  class= input colspan="6">
		    <textarea  name="Remark" cols="100%" rows="3">
		    </textarea>
		</TD> 
  </TR>     
  <TR  class= common>
    <TD  class= title>管理机构</TD><TD  class= input>
		<Input class= "codeno"  name=MngCom onkeydown="QueryOnKeyDown();" ondblclick="getstr();return showCodeList('comcode',[this,MngCom_ch], [0,1],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode', [this,MngCom_ch], [0,1],null,str,'1');" ><Input class=codename  name=MngCom_ch elementtype=nacessary>

    <TD  class= title>地区名称</TD><TD  class= input>
		<Input class= "codeno"  name=AreaCode    ondblclick="return showCodeList('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);" onkeyup="return showCodeListKey('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);"   verify="地区名称|notnull&len<=20" ><Input class=codename  name=AreaName elementtype=nacessary></TD>
  </TR>
</table>
</Div>
<Div id=CurrentlyInfo style="display: ''">  
	 <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		 <td class= titleImg>
        		 诊断数据信息（含门诊特殊病）列表（请至少录入‘地区疾病编码’或‘ICD10编码’或‘疾病名称’进行查询）
       	 </td>   		 
    	</tr>
    </table> 
	<Div id="comHospital1" style="display:'' ">
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanEvaluateGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
	</div>
</div>

   
    <input type=hidden id="fmtransact" name="Transact">
    <input type=hidden id="fmAction" name="fmAction">

    <input type=hidden name="SpecialClass">
    <input type=hidden name="Operator">
    <input type=hidden name="MakeDate">
    <input type=hidden name="MakeTime">
		<input type=hidden name="ModifyDate">
		<input type=hidden name="ModifyTime">
		<input type=hidden name="LoadFlag">
		<input type = hidden name = HiddenBtn>
		<Input type=hidden name=FlowNo ondblclick="generateFlowNo();">
</form>
<hr>
  <form action="./LLDiagnosisInfoImport.jsp" method=post name=fmlode target="fraSubmit" ENCTYPE="multipart/form-data" >
  	<table class = common >    	
    <TR  >
      <TD  width='8%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <Input  type="file"　width="100%" name=FileName class= common>
        <INPUT  VALUE="诊断数据信息导入" class="cssbutton" TYPE=button onclick = "DiagnosticInfoUpload();" >
        <INPUT  VALUE="导入模板下载" class="cssbutton" TYPE=button onclick = "downLoad();" >
        <Input  type="hidden"　width="100%" name="insuredimport" value ="1">
      </TD>
    </TR>
    
	    <input type=hidden name=ImportFile>
	</table>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
