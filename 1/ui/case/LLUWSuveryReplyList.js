var turnPage = new turnPageClass();

//提交前的校验、计算
function beforeSubmit(){
}

function afterQuery(arr){
}

//如果是回车调用查询客户函数
function QueryOnKeyDown(){
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13"){
		SurveyQuery();
	}
}

function afterCodeSelect( cCodeName, Field ) {
	try{
		SurveyQuery();
	}
	catch(ex){
		alert(ex.message);
	}
}

function SurveyQuery(){
	var strSql = "select b.OtherNo,substr(b.SurveyNo,18),"
	+ "(select c.codename from ldcode c where codetype='llsurveykind' and code=a.SurveyType),"
	+"a.SurveyStartDate,a.Startman,a.RgtState,a.SurveyType,b.inqno,"
	+ "(select c.codename from ldcode c where codetype='llsurveyflag' and code=a.surveyflag),"
	+ "b.SurveyNo,a.surveysite from LLSurvey a,LLInqApply b where 1=1 and a.surveyno=b.surveyno and not exists (select 1 from llcase where rgtstate='14' and caseno=b.otherno and not exists (select 1 from llcaseoptime where caseno=llcase.caseno and rgtstate='08')) "
	+ getWherePart("b.OtherNo", "OtherNo")
	strSql=strSql+" with ur ";
	turnPage.queryModal(strSql,CheckGrid);
}


function SurveyReply(){
	var row=CheckGrid.getSelNo()-1;
	if(row>=0){
		var varSrc = "&tSurveyNo="+CheckGrid.getRowColData(row,10);
		varSrc += "&CaseNo=" + CheckGrid.getRowColData(row,1);
		varSrc += "&InqNo=" + CheckGrid.getRowColData(row,8);
	}
	var newWindow = window.open("./SurveyReplyMain.jsp?MenuFlag=0"+varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SurveyFee(){
	var row=CheckGrid.getSelNo()-1;
	var CaseNo="";
	var SurveyNo="";
	if(row>=0){
		CaseNo = CheckGrid.getRowColData(row,1);
		SurveyNo = CheckGrid.getRowColData(row,2);
	}
	var newWindow = window.open("./SurveyFeeMain.jsp?Interface=LLSurveyFeeInput.jsp&CaseNo="+CaseNo+"&SurveyNo="+SurveyNo+"&LoadFlag=0","",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SumFee(){
	var row=CheckGrid.getSelNo()-1;
	var CaseNo="";
	if(row>=0){
		CaseNo = CheckGrid.getRowColData(row,1);
	}
	var newWindow = window.open("./SurveyFeeMain.jsp?Interface=LLSurvFeeSum.jsp&CaseNo="+CaseNo,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function Dispatch(){
	Str="1 and (surveyflag =#1# or surveyflag= #2#) and stateflag= #1# and comcode like #"+fm.MngCom.value+"%#";
}

function SurveyPrint(){
	var selno = CheckGrid.getSelNo();
	if (selno <= 0){
		alert("请选择要打印调查报告的理赔案件");
		return false;
	}

	var SurveyNo = CheckGrid.getRowColData(selno - 1, 10);
	var newWindow = OpenWindowNew("SurveyReportPrt.jsp?SurveyNo=" + SurveyNo,"","left" );
}

function DealSurvey(){
  
}
