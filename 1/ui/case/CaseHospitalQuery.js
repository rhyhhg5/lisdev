//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = './ClaimDetailSave.jsp';
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

// 查询按回车
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
//		UpdateGrid();
	}
}

function easyQuery()
{
	var mngCom = fm.MngCom.value;
	var RgtDateS = fm.RgtDateS.value;
	var RgtDateE   = fm.RgtDateE.value;
	
	if(mngCom==""||RgtDateS==""||RgtDateE=="")
	{
		alert("录入全部查询条件");
		return;
	}
	
	var strSQL =" select  llfeemain.HospitalName,llfeemain.HospitalCode,ldcom.name," 
		+" llfeemain.CustomerNo,llfeemain.CustomerName,llfeemain.CaseNo,"
		+" llfeemain.feedate,HospStartDate,llclaim.GiveTypeDesc,"
		+" (select substr(c.codename,1,6) from ldcode c where c.codetype = 'llrgtstate' and c.code = llcase.RgtState)"
		+" from llfeemain,llclaim,llcase,ldcom"
		+" where 1=1"
		+" and to_date(llfeemain.HospstartDate)>=to_date('"+RgtDateS+"')"
		+" and to_date(llfeemain.HospendDate)<=to_date('"+RgtDateE+"')"
		+" and llclaim.caseno=llfeemain.caseno"
		+" and llcase.mngcom like '"+fm.all('MngCom').value+"%%'"
		+" and llcase.caseno=llfeemain.caseno"
		+"  and ldcom.comcode= llcase.mngcom  "
		+" order by llfeemain.feedate DESC with ur "
		;

	turnPage.queryModal(strSQL, ClaimDetailGrid);
	                
}

function hosInfo()
{
	var LEN;
	if (comCode.length == 8){LEN = comCode.substring(0,4);}
	else{LEN = comCode;}
	var SQL = "select A,B,'',C,decimal(ROUND(decimal(C)*100/decimal(E),2),8,2),D, "
		    + " decimal(ROUND(decimal(D)*100/decimal(F),2),8,2) from ( "
            + " select hospitalname A ,hospitalcode B ,count(distinct a.caseno) C,coalesce(sum(c.realpay),0) D, "
            + " (select count( distinct b.caseno) from llfeemain b) E, "
            + " (select coalesce(sum(c.realpay),0) from llclaim c) F "
            + " from llfeemain a , ldhospital h,llclaim c where  h.managecom like '"+fm.all('MngCom').value+"%%' "
            + " and  a.hospitalcode = h.hospitcode and c.caseno=a.caseno "
            + " and a.caseno in (select caseno from llcase where endcasedate is not null and rgtstate in ('11','12','09')) "
            + " group by hospitalcode,hospitalname order by C,B,A "
            + " ) AS X with ur ";
	turnPage2.queryModal(SQL, ClaimInfoGrid); 
}
