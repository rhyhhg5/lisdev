<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：LLPrepaidClaimInput.jsp
 //程序功能：预付赔款录入
 //创建日期：2010-11-25
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  String Operator = tG.Operator;
  String ManageCom = tG.ManageCom;
%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="LLPrepaidClaimInput.js"></SCRIPT>
  <%@include file="LLPrepaidClaimInputInit.jsp"%>
  <script language="javascript">
   var str = "1 and code in (select code from ldcode where codetype=#llbgetmode# )";
   function initData(){
      fm.ManageCom.value="<%=ManageCom%>";
      fm.Operator.value="<%=Operator%>";
   }
  </script>
</head>
<body  onload="initForm();initElementtype();initData();">
<form action="./LLPrepaidClaimInputSave.jsp" method=post name=fm target="fraSubmit">
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLPrepaid);">
    </td>
    <td class= "titleImg">保单查询</td>
  </tr>
</table>
<div id="divLLPrepaid">
<table class="common" >
  <tr class="common">
    <td class="title">管理机构</td>
    <td class="input"><Input class="codeno" name="ManageCom" ondblclick="return showCodeList('comcode4',[this,ManageName],[0,1],null,'1','sign',1);" onkeyup="return showCodeListKey('comcode4',[this,ManageName],[0,1],null,'1','sign',1);" readonly="true" ><input class="codename" name="ManageName" elementtype="nacessary"></TD>
    <td class="title">投保单位名称</td>
    <td class="input"><input class="common" name="SGrpName" ></td>  
    <td class="title">团体保单号</td>
    <td class="input"><input class="common" name="SGrpContNo" ></td>  
  </tr>
</table>

<input value="查  询"  onclick="searchGrpCont()" class="cssButton" type="button" >
<br>

<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLPrepaidGrpCont);">
    </td>
    <td class="titleImg" >保单信息</td>
  </tr>
</table>
<div id="divLLPrepaidGrpCont">
<table class="common">
  <tr class="common">
    <td text-align: left colSpan=1>
     <span id="spanGrpContGrid" >
     </span> 
    </td>
  </tr>
</table>

<input value="预付保单确认" name="GrpContConfirm" onclick="GrpContDeal('Confirm')" class="cssButton" type="button" >
<input value="预付保单撤销" name="GrpContCancel" onclick="GrpContDeal('Cancel')" class="cssButton" type="button" >
</div>
<br>
</div>
<div id="divLLPrepaidInput">
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLPrepaidInfo);">
    </td>
    <td class="titleImg" >预付金额录入</td>
  </tr>
</table>
<div id="divLLPrepaidInfo">
<table class="common" >
  <tr class="common" >
    <td class="title">预付赔款号</td>
    <td class="input"><Input class="common" name="PrepaidNo" onkeydown="searchPrepaid()"></td>
    <td class="title">预付赔款状态</td>
    <td class="input"><input class="readonly" name="PrepaidState" readonly="true" ></td> 
  </tr>
  <tr class="common">
    <td class="title">投保单位名称</td>
    <td class="input"><input class="readonly" name="GrpName" readonly="true" ></td>  
    <td class="title">团体保单号</td>
    <td class="input"><input class="readonly" name="GrpContNo" verify="团体保单号|NOTNULL" readonly="true" elementtype="nacessary" ></td>
    <td class="title">实收保费</td>
    <td class="input"><input class="readonly" name="SumActuPay" readonly="true" ></td> 
  </tr>
  <tr class="common">
    <td class="title">银行编码</td>
    <td class="input"><Input class="code" name="BankCode" verify="银行编码|NOTNULL" onclick="return showCodeList('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" onkeyup="return showCodeListKey('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" elementtype="nacessary"></td>  
    <td class="title">银行名称</td>
    <td class="input"><Input class="input" name="BankName" ></td> 
    <td class="title">签约银行</td>
    <td class="input"><input class="readonly" name="SendFlag" readonly="true"></td>
  </tr>
  <tr class="common" >
    <td class="title">银行账号</td>
    <td class="input"><Input class="input" name="AccNo" verify="银行账号|NOTNULL" elementtype="nacessary"></td>  
    <td class="title">账户名</td>
    <td class="input"><Input class="input" name="AccName" verify="账户名|NOTNULL" elementtype="nacessary"></td> 
  </tr>
  <tr class="common" >
    <td class="title">给付方式</td>
    <td class="input"><Input class="codeno" name="PayMode" verify="给付方式|code:llbgetmode&INT&notnull" onclick="return showCodeList('paymode',[this,PayModeName],[0,1],null,str,'1');" onkeyup="return showCodeListKey('paymode',[this,PayModeName],[0,1],null,str,'1');" ><input class=codename name=PayModeName readonly="true" elementtype="nacessary"></td>  
  </tr>
</table>
<table class="common">
  <tr class="common">
    <td text-align: left colSpan=1>
     <span id="spanPrepaidClaimGrid" >
     </span> 
    </td>
  </tr>
</table>
<div id= "divSubmit" style= "display: ''" align= right>
<input value="申  请"  name="Apply" onclick="submitForm()" class="cssButton" type="button" >
<input value="撤  销"  name="Cancel" onclick="cancel()" class="cssButton" type="button" >
</div>
</div>
</div>

<Input type="hidden" class="common" name="Operator">
<Input type="hidden" class="common" name="RgtType" value="1" >
<Input type="hidden" class="common" name="fmtransact">

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
