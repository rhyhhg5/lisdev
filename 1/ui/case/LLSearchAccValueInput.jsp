<html>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
		<SCRIPT src="LLSearchAccValueInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLSearchAccValueInit.jsp"%>
</head>
<body onload="initForm();">
  <form  action='./LLSearchAccValueSave.jsp' method=post name=fm target="fraSubmit">
    <table  class= common>
       <TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name="InsuredNo" >
          </TD>
          <TD  class= title>
            客户姓名
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name="CustomerName" >
          </TD>
          <TD  class= title>
          </TD>
          <TD  class= input>
          </TD>
        </TR>
       </table>

     <table>
				<tr>
					<td class=common><img src="../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divPolGrid);"></td>
					<td class= titleImg> 保单明细</td>
				</tr>
			</table>
			<Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanPolGrid" ></span>
						</td>
					</tr>
				</table>
			</Div>
			<table>
				<tr>
					<td class=common><img src="../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divAccGrid);"></td>
					<td class= titleImg>账户信息</td>
				</tr>
			</table>
			<Div  id= "divAccGrid" style= "display: ''">
			<table  class= common align=center>
      <tr  class= common>
        <td  class= title>账户建立时间</td>
        <td  class= input><Input class=common  name=AccFoundDate readonly=true></td>
        <td  class= title>账户结束时间</td>
        <td  class= input><input class=common name=AccEndDate readonly=true></td>
        <td  class= title>账户当前金额</td>
        <td  class= input><input class=common name=InsuAccBala  readonly=true></td>
      </tr>
      <tr  class= common>
        <td class= title>最近结算月份</td>
        <td  class= input><input class=common name=BalaMonth  readonly=true></td>
        <td class= title>最近结算时间</td>
        <td  class= input><input class=common name=BalaDate  readonly=true></td>
      </tr>
    </table>
    </div>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divOmnipotenceAccTrace);"></td>
	    <td class=titleImg>账户价值历史信息:</td>
	  </tr>
  </table>
  <!-- 信息（列表） -->
  <div id="divOmnipotenceAccTrace" style="display:''">
	  <table class=common>
      <tr class=common>
	      <td text-align:left colSpan=1><span id="spanOmnipotenceAccTraceGrid"></span></td>
	    </tr>
    </table>
  </div>

  <div id="divPage2" align=center style="display: 'none' ">
	  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSearchAccValue);"></td>
	    <td class=titleImg>账户价值查询:</td>
	  </tr>
	  <div id="divSearchAccValue">
  </table>
       <table class=common>
		  	<TR  class= common8>
		  		<TD  class= title8>查询日期</TD>
		  		<TD  class= input8><Input name="CalDate" class='coolDatePicker' dateFormat='short' verify="查询日期|notnull&Date" > </TD>
		  		<TD  class= title8></TD>
		  		<TD  class= input8></TD>
		  		<TD  class= title8></TD>
		  		<TD  class= input8></TD>
		  	</TR>
		</table>
    <INPUT VALUE="查  询" TYPE=button class=cssbutton onclick="submitForm();">
    <INPUT VALUE="返  回" TYPE=button class=cssbutton onclick="top.close();"> 
    <input type=hidden id="fmtransact" name="fmtransact">	
    <input type=hidden id="PolNo" name="PolNo">
    <input type=hidden id="InsuaccNo" name="InsuaccNo">
    <input type=hidden id="CurrentDate" name="CurrentDate">
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
