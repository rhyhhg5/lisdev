<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAMarketInput.jsp
//程序功能：F1报表生成
//创建日期：2007-11-13
//创建人  ：xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.utility.*" %>
<%
    GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-60";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
%>
 <script>
   var msql=" 1 and   char(length(trim(comcode)))<=#4# ";
 var msql1=" 1 and   branchtype=#2#  and branchtype2=#01# and endflag=#N#";

</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="GrpContLockInput.js"></SCRIPT>  
 <%@include file="GrpContLockInit.jsp"%> 
 
<script language="javascript">
   function initDate(){
		
  }
   </script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body  onload="initDate();initForm();">    
  <form action="" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
      	<TR  class= common>      
      	<TD  class= title>团体保单号</TD><TD  class= input>
      		<Input class= common type='text' name="grpContNo" >
      		      	</TD>
		<TD  class= title8>项目名称</TD>
			<TD  class= input8> <input class=codeno  name=projectNo onclick="return showCodeList('ProjectAskMode',[this,ProjectName],[0,1],null,fm.ProjectName.value,'Projectname',1);" onkeyup="return showCodeListKeyEx('ProjectAskMode',[this,ProjectName],[0,1],null,fm.ProjectName.value,'Projectname',1);" elementtype=nacessary verify="医院代码|notnull"><Input class=codename name= ProjectName elementtype=nacessary ></TD>
		<TD  class= title>状态</TD>
			<TD  class= input> <input class="codeno" CodeData="0|2^1|锁定^2|解锁"  verify="状态|notnull" elementtype=nacessary name=stateno 
			ondblclick="return showCodeListEx('StateType',[this,StateName],[0,1],null,null,null,1);"  
			onkeyup="return showCodeListKeyEx('StateType',[this,StateName],[0,1]);"><input class=codename name=StateName></TD>
        </tr>
        <tr>
        	<td><input class=cssButton style='width:80px;' type=button value="查 询" onclick="SearchLLLock()">
        	</td>
        </tr>
    </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,LockRecordGrid);">
    		</td>
    		<td class= titleImg>
    			批量审批--团体保单查询
    		</td>
    	</tr>
    </table>
      <Div  id= "divLLLockGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left >
            <span id="spanLLLockGrid" >
            </span>
          </TD>
        </TR>
      </table>
  	
         <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">      
    </Div> 
    <table>
        <tr>
		 <td>
		   <input class=cssButton style='width:80px;' type=button value="解锁" onclick="updateLLLock();">
		 </td>
        </TR>  
    </table>
    <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divReceipt);">
          </td>
          <td class= titleImg>
            操作记录
        </td>
        </tr>
     </table>
     <Div id='divLockRecordGrid' align=center style="display: ''" >
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanLockRecordGrid" >
              </span>
            </td>
          </tr>
        </table>
         <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">      
     </Div> 
       
    <input type=hidden name=operate>
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		<iframe id="printfrm" src="" width=10 height=10></iframe>
    
</body>
</html> 
