<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@ page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：OutsourcingCostInput.jsp
//程序功能：外包费用录入界面
//创建日期：2013-12-23
//创建人  ：LiuJian
//更新记录：更新人    更新日期     更新原因/内容
%>

<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="OutsourcingCostInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="OutsourcingCostInit.jsp"%>
  <title>外包错误数据</title>
</head>
<body  onload="initForm();initElementtype();"  >
  <form action="OutsourcingCostSave.jsp" method=post name=fm target="fraSubmit">
    <table class="common">
  		<tr CLASS="common">
  			<td CLASS="title">批次号</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="BatchNo" CLASS="common"  maxlength="19" >
      	</td>  	
  			<td CLASS="title">外包费用总额</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="OSTotal" CLASS="common" verify="外包费用总额|num" maxlength="10">
      	</td>
  			<td CLASS="title">险种编码</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="RiskCode" CLASS="common"  style="width:120" maxlength="19">
      	</td>
      	</tr>
      	<tr CLASS="common">
      	<td CLASS="title">结算开始日期</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="BalanceStartDate" class="coolDatePicker" verify="结算开始日期|date"  style="width:120">
      	</td> 
  			<td CLASS="title">结算结束日期</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="BalanceEndDate" class="coolDatePicker" verify="结算结束日期|date"  style="width:120">
      	</td>	
      	  	<td CLASS="title">保单年度</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="PayYear" CLASS="common" maxlength="19" verify="保单年度|num">
      	</td>
      	 </tr>
      	<tr CLASS="common">
  			<td CLASS="title">管理机构</td>
  			<td CLASS="input" COLSPAN="1">
  			  <Input class= "codeno" name="ManageCom" style="width:50" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
  			   onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"  >
  			  <Input class=codename style="width:100" name="ManageComName" >
      		</td> 
  			
  			<td CLASS="title">操作用户</td>
  			<td CLASS="input" COLSPAN="1">
  				<input NAME="Operator" CLASS="common" value="" />
      		</td>  	
      	      	
  			<td CLASS="title">外包单位账户名</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="OSAccount" CLASS="common"  >
      		</td>  		
  		</tr>
  		<tr CLASS="common">
  			  <td CLASS="title">外包单位账户号码</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="OSAccountNo" CLASS="common"  verify="保单年度|num">
      	</td>  
  			<td CLASS="title">外包单位开户行</td>
  			<td CLASS="input" COLSPAN="1">
  			  <Input class= "codeno"  name="OSBank" style="width:50" 
  			 ondblclick="return showCodeList('osbank',[this,OSBankName],[0,1],null,null,fm.ManageCom.value,1);" onkeyup="return showCodeListKey('bankcode',[this,OSBankName],[0,1],null,null,fm.ManageCom.value,1);"  ><Input class=codename readonly="readonly" style="width:100" name="OSBankName" >
      		</td> 
  			<td CLASS="title">转账方式</td>
  			<td CLASS="input" COLSPAN="1">
  			  <Input class= "codeno"  name="Transfertype" style="width:50"  CodeData="0|^1|现金^2|现金支票 ^3|转账支票 ^4|银行转账 ^11|银行汇款" ondblclick="return showCodeListEx('Transfer',[this,TransferTypeName],[0,1]);" onkeyup="return showCodeListEx('Transfer',[this,TransferTypeName],[0,1]);" ><Input class=codename  readonly="readonly" style="width:100" name=TransferTypeName >
      		</td> 
  		</tr>
  	
    </table>
    <INPUT VALUE="导入保单号" class =cssButton TYPE=button onclick="diskImport();" />
    <INPUT VALUE="查  询" class =cssButton TYPE=button onclick="queryOSInfo();">
    <INPUT VALUE="保  存" class =cssButton TYPE=button onclick="saveOSInfo();">
    <INPUT VALUE="删  除" class =cssButton TYPE=button onclick="deleteOSInfo();">
    <INPUT VALUE="导出保单号" class =cssButton TYPE=button onclick="exportCont();">
    <input type="hidden"  name="querySql" > 
    <input type="hidden"  name="querySqlTitle" > 
    <input type="hidden"  name="Title" >
    <input type="hidden" name="operate" />
    <input type="hidden" name="currentOperate" value=<%=tG.Operator %> />
    <input type="hidden" name="LogmanageCom" value=<%=tG.ManageCom%> />
  <br></br>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			 外包信息查询
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''" align=center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
    </Div>
  	<Div id= "divPage2" align=center style= "display: '' ">
      <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage.firstPage(); "> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage(); "> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage(); "> 
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage.lastPage();"> 
    </Div>
    <Div >
    <br><BR>
<input class="readonly" name="" readonly value="审核意见" >
<br><BR>
<textarea class="common" name="AuditOpinion" cols="100%" rows="2" ></textarea>
  </Div>
    <input type="hidden" name="auditState" />
    <INPUT VALUE="审核通过" class = cssButton TYPE=button onclick="audit(1);"> 
    <INPUT VALUE="审核不通过" class = cssButton TYPE=button onclick="audit(2);"> 
    <INPUT VALUE="打印给付凭证" class = cssButton TYPE=button onclick="osprint();"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
