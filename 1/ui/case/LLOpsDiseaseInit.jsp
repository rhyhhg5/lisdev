<%
//程序名称：LLOpsDiseaseInit.jsp
//程序功能：
//创建日期：2016年1月19日17:27:43
//创建人  ：  Rs
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<SCRIPT src="Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。

%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
      fm.all("ICDOPSName").value = "";
      fm.all("ICDOPSCode").value = "";
      fm.all("ICDCode").value = "";
      fm.all("ICDName").value = "";

  }
  catch(ex)
  {
    alert("在LLOpsDiseaseInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initLLOpsDiseaseGrid();
  }
  catch(re)
  {
    alert("LLOpsDiseaseInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initLLOpsDiseaseGrid()
{

    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;

      iArray[1]=new Array();
      iArray[1][0]=" 手术代码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="120px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="手术名称";    	                //列名
      iArray[2][1]="120px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[3]=new Array();
      iArray[3][0]="疾病代码";    	                //列名
      iArray[3][1]="120px";            		        //列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[4]=new Array();
      iArray[4][0]="疾病名称";    	                //列名
      iArray[4][1]="120px";            		        //列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[5]=new Array();
      iArray[5][0]="菜单ID";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[5][1]="0px";         			//列宽
      iArray[5][2]=10;          			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      LLOpsDiseaseGrid = new MulLineEnter( "fm" , "LLOpsDiseaseGrid" );

      //这些属性必须在loadMulLine前
      LLOpsDiseaseGrid.mulLineCount = 5;
      LLOpsDiseaseGrid.displayTitle = 1;
      LLOpsDiseaseGrid.canChk =0;
      LLOpsDiseaseGrid.canSel =1;
      LLOpsDiseaseGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
      LLOpsDiseaseGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏
      LLOpsDiseaseGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      LLOpsDiseaseGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
      LLOpsDiseaseGrid.selBoxEventFuncName = "onQuerySelected";
      LLOpsDiseaseGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
  }
</script>