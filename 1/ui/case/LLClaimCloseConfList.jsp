<html>
	<%
	//Name：LLClaimCloseConfList.jsp
	//Function：个单给付确认
	//Date：2004-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import="java.util.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   String Operator=tG.Operator;
   String tComcode=tG.ManageCom;
   String CurrentDate = PubFun.getCurrentDate();
	 CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
	 FDate tD=new FDate();
   String AfterDate = tD.getString(PubFun.calDate(tD.getDate(CurrentDate),-2,"M",null));
	 
%>   
	<head >
	<SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>
	
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLClaimCloseConfList.js"></SCRIPT>
		<%@include file="LLClaimCloseConfListInit.jsp"%>
		   <script language="javascript">
		   
		    var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
		   
   function initDate(){
		fm.RgtDateStart.value="<%=AfterDate%>";
		fm.RgtDateEnd.value="<%=CurrentDate%>";
		fm.Comcode.value="<%=tComcode%>";
		fm.Operator.value="<%=Operator%>";
   }
   </script>
	</head>

	<body  onload="initDate();initForm();" >
		<form action="./LLClaimCloseConfSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<TR>
					<TD><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCondition);"></TD>
					<TD class= titleImg>查询条件</TD>
				</TR>	
			</table>

			<Div  id= "divCondition" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>团体批次号</TD>
						<TD  class= input8><input class= common name="RgtNo" onkeydown="QueryOnKeyDown1()"></TD>
						<TD  class= title8>单位名称</TD>
						<TD  class= input8><input class= common name="RgtName" onkeydown="QueryOnKeyDown()"></TD>
						<TD  class= title8>申请人姓名</TD>
						<TD  class= input8><input class= common name="RgtantName" onkeydown="QueryOnKeyDown()"></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>理赔号码</TD>
						<TD  class= input8><input class= common name="CaseNo" onkeydown="QueryOnKeyDown()"></TD>
						<TD  class= title8>客户姓名</TD>
						<TD  class= input8><input class= common name="CustomerName" onkeydown="QueryOnKeyDown()"></TD>
						<TD  class= title8>客户号码</TD>
						<TD  class= input8><input class= common name="CustomerNo" onkeydown="QueryOnKeyDown()"></TD>
					</TR>
					<TR  class= common8 >
						<TD  class= title8>受理日期 起始</TD>
						<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateStart onkeydown="QueryOnKeyDown()" ></TD>
						<TD  class= title8>结束</TD>
						<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateEnd onkeydown="QueryOnKeyDown()"></TD>
						<TD class= title8>给付方式</TD>
					  <TD class= input8><Input class="codeno" name=paymode onclick="return showCodeList('paymode',[this,paymodename],[0,1],null,str,'1');" onkeyup="return showCodeListKey('paymode',[this,paymodename],[0,1],null,str,'1');" onkeydown="QueryOnKeyDown()" verify="给付方式|code:llgetmode&INT&notnull"><Input class="codename" name=paymodename  elementtype=nacessary ></TD>
				
					</TR>
					<TR  class= common8>
						<td colspan=3>
							<input type="radio" value="09" name="RgtState" checked onclick="easyQueryClick()">结案状态
							<input type="radio" value="11" name="RgtState" onclick="easyQueryClick()">通知状态
							
						</TD>
					</TR>
				</table>
			</DIV>
			<hr>
			<input name="AskIn" style="display:''"  class=cssButton type=hidden value="查  询" onclick="easyQueryClick()">
			
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseList);">
					</TD>
					<TD class= titleImg>
						给付确认工作列表
					</TD>
				</TR>
			</table>
			<Div  id= "divCaseList" align = center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanCaseGrid" >
							</span>
						</TD>
					</TR>
				</table>
				<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
				<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
			</Div>
			
			<br>
			<input name="AskIn" style="display:''"  class=cssButton type=button style='width: 70px;' value="查看扫描件" onclick="ScanQuery()">
			<input  style="display:''"  class=cssButton name='giveconfirm1' type=button value="给付确认" onclick="giveConfirm()">
			<input style="display:''"  class=cssButton name='givegrpconfire1' type=button value="团体给付确认" onclick="giveGrpConfirm()">
			<!--<input name="AskInPrint" style="display:''"  class=cssButton type=button value="批量打印" onclick="ClaimGetPrint()">-->
			<!--隐藏域-->
			<Input type="hidden" name="fmtransact" >
			<input type=hidden id="" name="Comcode">
			<input type=hidden id="" name="Operator">
			<input type=hidden id="" name="RgtNo1">	
			<input type=hidden id="" name="IfConfirm" value='NULL'>	
			
			
					
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		<iframe id="printfrm" src="" width=10 height=10></iframe>
	</body>
</html>
