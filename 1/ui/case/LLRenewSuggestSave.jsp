<%
//Name：LLRenewSuggestSave.jsp
//Function：续保建议
//Date：2005-9-21
//Author  ：Xx
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>
<%
  LLContUWBL tLLContUWBL   = new LLContUWBL();
  LLRenewOpinionSchema tLLRenewOpinionSchema = new LLRenewOpinionSchema();
  VData tVData = new VData();
  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String strOperate = request.getParameter("fmtransact");
  System.out.println("strOperate::"+strOperate);
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
  String CaseNo = request.getParameter("CaseNo");
  String CustomerNo = request.getParameter("CustomerNo");
  String CustomerName = request.getParameter("CustomerName");
  String RemarkType = request.getParameter("RemarkType");
  String Remark = request.getParameter("Remark");
  tLLRenewOpinionSchema.setCaseNo(CaseNo);
  tLLRenewOpinionSchema.setCustomerNo(CustomerNo);
  tLLRenewOpinionSchema.setCustomerName(CustomerName);
  tLLRenewOpinionSchema.setRemarkType(RemarkType);
  tLLRenewOpinionSchema.setRemark(Remark);
  
  try
  {
    tVData.addElement(tLLRenewOpinionSchema);
    tVData.addElement(tG);
    if(tLLContUWBL.submitData(tVData,strOperate))
    {
      Content = "保存成功";
      FlagStr = "Succ";
    }
    else
    {
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("返回的标志是"+FlagStr);
  if (FlagStr=="Fail")
  {
    tError = tLLContUWBL.mErrors;
    if (tError.needDealError())
    {
      Content = " 保存失败,原因是"+tError.getFirstError();
      FlagStr = "Fail";
      tVData.clear();
    }
  }
  else
  {
    Content = "保存成功！";
    FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>