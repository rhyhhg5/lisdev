//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  //var i = 0;
  initReportGrid();
  var strSQL = " select LLReport.RptNo,LLSubReport.CustomerName,LLSubReport.CustomerNo,LLSubReport.AccidentType,LLReport.MakeDate,LLReport.Operator,LLReport.MngCom "
  						+" from LLReport ,LLSubReport "
  						+" where LLReport.RptNo = LLSubReport.RptNo "
  						+" and LLReport.RgtFlag = '0' and LLReport.MngCom like '"+ComCode+"%%'"
  						+ getWherePart( 'LLReport.RptNo','RptNo' )
  						+ getWherePart( 'LLSubReport.CustomerName','People' )
  						+ getWherePart( 'LLReport.MakeDate','RptDate')
  						+" order by LLReport.MakeDate,LLReport.MakeTime ";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
    alert("在该管理机构下没有满足条件的待立案信息记录");
    return "";
  }

  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.pageDisplayGrid = ReportGrid;
  turnPage.strQuerySql     = strSQL;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
 //fm.submit(); //提交
}




//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
  }
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
  parent.fraMain.rows = "0,0,0,0,*";
}

function returnParent()
{
  tRow=ReportGrid.getSelNo();
  if (tRow==0)
  {
    alert("请您先进行选择");
    return;
  }
  else
  {
    clearData();
    tCol=1;
    tReportNo = ReportGrid.getRowColData(tRow-1,tCol);
    var i = 0;
    fm.action = "./RegisterQueryOut1.jsp?RptNo="+tReportNo;
    fm.submit();
  }
}
function clearData()
{
  top.opener.fm.RgtNo.value="";
  top.opener.fm.RptNo.value="";
  top.opener.fm.ClmState.value="";
  top.opener.fm.DisplayFlag.value="";
  top.opener.fm.CaseState.value="";
  top.opener.fm.CaseGetMode.value="";
  top.opener.fm.PeopleName.value="";
  top.opener.fm.CustomerNo.value="";
  top.opener.fm.Handler1.value="";
  top.opener.fm.Handler1Phone.value="";
  top.opener.fm.RgtDate.value="";
  top.opener.fm.AccidentDate.value="";
  top.opener.fm.RgtantName.value="";
  top.opener.fm.Sex.value="";
  top.opener.fm.IDType.value="";
  top.opener.fm.IDNo.value="";
  top.opener.fm.ApplyType.value="";
  top.opener.fm.Relation.value="";
  top.opener.fm.RgtantPhone.value="";
  top.opener.fm.RgtantAddress.value="";
  top.opener.fm.BankCode.value="";
  top.opener.fm.AccName.value="";
  top.opener.fm.BankAccNo.value="";
  top.opener.fm.RgtantMobile.value="";
  top.opener.fm.Operator.value="";
  top.opener.fm.Handler.value="";
  top.opener.fm.MngCom.value="";
  top.opener.fm.AgentCode.value="";
  top.opener.fm.AgentGroup.value="";
  top.opener.fm.AccidentReason.value="";
  top.opener.fm.AccidentSite.value="";
  top.opener.fm.AccidentCourse.value="";
  top.opener.fm.Remark.value="";
  top.opener.fm.RgtReason.value="";
}
