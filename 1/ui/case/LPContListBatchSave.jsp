<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
CErrors tError = null;
boolean operFlag=true;
String FlagStr = "Fail";
String Content = "";

String FileName = "";
String filePath = "";
int count = 0;

//得到excel文件保存的路径
String ImportPath = request.getParameter("ImportPath");
System.out.println("ImportPath="+ImportPath);
//application.getRealpath()取到的路径是用"\"分割的，所以需要替换
String path = application.getRealPath("").replace("\\", "/") + "/";
System.out.println("path=" + path);
DiskFileUpload fu = new DiskFileUpload();
//设置允许用户上传文件的大小，单位字节
fu.setSizeMax(10000000);
//设置最多只允许在内存中存储的数据，单位，字节
fu.setSizeThreshold(4096);
//设置文件大小一旦超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(path+"temp");
//开始读取上传信息
List fileItems = null;
try{
	fileItems = fu.parseRequest(request);
}catch(Exception ex) {
	ex.printStackTrace();
}
Iterator iter = fileItems.iterator();
while(iter.hasNext()) {
	FileItem item = (FileItem)iter.next();
	//判断FileItem中封装的数据是一个普通文本表单字段，还是一个文件表单字段，如果是普通表单字段返回true否则返回false
	if(!item.isFormField()) {
		//获取文件上传字段中的文件名
		String name = item.getName();
		//返回文件的大小
		long size = item.getSize();
		if((name==null||name=="")&size==0) {
			continue;
		}
		ImportPath =   path + ImportPath;
		FileName = name.substring(name.lastIndexOf("\\") + 1);
		System.out.println("ImportPath=" + ImportPath);
		System.out.println("fileName=" + FileName);
		File file = new File(ImportPath+FileName);
		if(file.exists()) {
			Content = "上传文件：" + FileName + "已存在";
			FlagStr = "Fail";
			break;
		}
		try{
			//将Fileitem对象中的内容保存到某个指定 的文件中
			item.write(file);
			count=1;
		}catch(Exception ex) {
			System.out.println("Upload File Error ...");
		}
	}
}
System.out.println("Upload File SuccessFully");
//上传完毕开始业务逻辑处理
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getAttribute("GI");
System.out.println("tG="+tG);
TransferData tTransferData = new TransferData();
boolean result = true;
LPContListBatchBL tLPContListBatchBL = new LPContListBatchBL(ImportPath,FileName,tG);
if(count>0) {
	//准备传输数据VData
	VData tVData = new VData();
	FlagStr = "";
	tTransferData.setNameAndValue("FileName", FileName);
	tTransferData.setNameAndValue("FilePath", ImportPath);
	System.out.println("FileName="+FileName);
	System.out.println("FilePath="+ImportPath);
	tVData.add(tTransferData);
	tVData.add(tG);
	try{
		//提取案件信息
		if(!tLPContListBatchBL.doAdd(ImportPath, FileName)) {
			Content = tLPContListBatchBL.mErrors.getFirstError();
			FlagStr = "Fail";
			result = false;
		}
	}catch(Exception ex) {
		System.out.println(ex.toString());
		Content = ex.toString();
		FlagStr = "Fail";
	}
	System.out.println("submitData Finished");
}else {
	Content += "上传文件失败！";
	FlagStr = "Fail";
}

if(FlagStr.equals("Fail")) {
	result = false;
}
if(result) {
	//输出导入成功了多少条数据
	int tSuccNum = tLPContListBatchBL.getSuccNum();
	System.out.println("tSuccNum=" + tSuccNum);
	//传向前台的批次号
	String tBatchNo = tLPContListBatchBL.getBatchNo();
	System.out.println("tBatchNo=" + tBatchNo);
	
	Content = "提交成功,导入数据成功：" + tSuccNum + "行";
	FlagStr = "Succ";
}else {
	Content = "保存失败，原因是：" + Content;
	FlagStr = "Fail";
}

%>
<html>
	<script language="javascript">
	parent.fraInterface.UploadiFrame.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>