/*********************************************************************
 *  程序名称：InsuredUWInfo.js
 *  程序功能：人工核保被保人信息页面函数
 *  创建日期：2005-01-06 11:10:36
 *  创建人  ：HYQ
 *  返回值：  无
 *  更新记录：  更新人    更新日期     更新原因/内容
 *********************************************************************
 */

var arrResult1 = new Array();
var arrResult2 = new Array();
var arrResult3 = new Array();
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var temp = new Array();



/*********************************************************************
 *  查询核保被保人险种信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryRiskInfo()
{
	
	var tSql = "select lcpol.polno,lcpol.MainPolNo,lcpol.riskcode,lmrisk.riskname,lcpol.Prem,lcpol.Amnt,lcpol.CValiDate,lcpol.EndDate,lcpol.PayIntv,lcpol.PayYears from LCPol,lmrisk where 1=1"
							+ " and contno='"+ContNo+"'"							
							+ " and lcpol.riskcode = lmrisk.riskcode"
							;

	turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("险种信息查询失败!");
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = RiskGrid ;    

  //保存SQL语句
  turnPage.strQuerySql     = tSql ; 

  //设置查询起始位置
  turnPage.pageIndex       = 0;  

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  if(arrDataSet.length!=0)
  {
	  for(i=0;i<arrDataSet.length;i++)
	  {
	  	 temp[i] = arrDataSet[i][2];
	  }
  }
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  return true;  					
}

/*********************************************************************
 *  体检资料查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealthQ()
{

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  
  var cContNo = fm.ContNo.value;

  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;
  
  
  if (cContNo!= ""  )
  {     
  	//var tSelNo = PolAddGrid.getSelNo()-1;
  	//var tNo = PolAddGrid.getRowColData(tSelNo,1);	
  	//showHealthFlag[tSelNo] = 1 ;
  	window.open("./UWManuHealthQMain.jsp?ContNo="+cContNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&PrtNo="+cPrtNo,"window1");
  	showInfo.close();
  	
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}       

/*********************************************************************
 *  生存调查报告查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function RReportQuery()
{
  cContNo = fm.ContNo.value;  //保单号码

  //var tSel = PolAddGrid.getSelNo();
  //if (tSel == 0 || tSel == null)
  //{
  // 	alert("请先选择保单!");  	
	//return;
  //}
  //var cNo = PolAddGrid.getRowColData(tSel - 1, 1);

  
  if (cContNo != "")
  {			
	window.open("./RReportQueryMain.jsp?ContNo="+cContNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

/*********************************************************************
 *  既往投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showApp(cindex)
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cInsureNo = fm.InsuredNo.value;
  window.open("../uw/UWAppMain.jsp?ContNo="+ContNo+"&CustomerNo="+cInsureNo+"&type=2");
  	showInfo.close();
}         

/*********************************************************************
 *  体检资料录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cContNo = fm.ContNo.value;
  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;
  
  if (cContNo != "")
  {
  	//var tSelNo = PolAddGrid.getSelNo()-1;
  	//var tNo = PolAddGrid.getRowColData(tSelNo,1);	
  	//showHealthFlag[tSelNo] = 1 ;
  	window.open("./UWManuHealthMain.jsp?ContNo1="+cContNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&PrtNo="+cPrtNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}           

/*********************************************************************
 *  生存调查报告
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showRReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ContNo.value;
  alert(cProposalNo);
  var cMissionID =fm.MissionID.value; 
  var cSubMissionID =fm.SubMissionID.value; 
  var tSelNo = PolAddGrid.getSelNo()-1;
  var cPrtNo = PolAddGrid.getRowColData(tSelNo,3);	
  if (cProposalNo != ""  && cMissionID != "")
  {
  	window.open("./UWManuRReportMain.jsp?ContNo="+cProposalNo+"&MissionID="+cMissionID+"&PrtNo="+cPrtNo+"&SubMissionID="+cSubMissionID+"&Flag="+pflag,"window1");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}         
   


/*********************************************************************
 *  理赔给付查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function ClaimGetQuery()
{
	  var cInsuredNo = fm.InsuredNo.value;				
		if (cInsuredNo == "")
		    return;
		window.open("../sys/AllClaimGetQueryMain.jsp?InsuredNo=" + cInsuredNo);										

}

/*********************************************************************
 *  加费承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function showAdd()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 
 
             window.open("../uw/UWManuAddMain.jsp?ContNo="+ContNo+"&InsuredNo="+InsuredNo,"window1"); 
  

}

/*********************************************************************
 *  特约承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function showSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  if (ContNo != "" )
  { 	
  	window.open("../uw/UWManuSpecMain.jsp?ContNo="+ContNo,"window1");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("数据传输错误!");
  }
}

/*********************************************************************
 *  返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function InitClick(){
	window.close();
}

/*********************************************************************
 *  提交，保存按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function submitForm(FlagStr)
{
	
	if(FlagStr == 1)
	{
		var tSelNo = RiskGrid.getSelNo();
 		if(tSelNo<=0)
 		{
 			alert("请选择险种保单！");
 			return;
 		}
  	fm.all('PolNo').value = RiskGrid.getRowColData(tSelNo - 1,1);	
  	fm.all('flag').value = "risk";
	}

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 	 
  fm.action = "./LLSecondUWRiskChk.jsp";
  fm.submit(); //提交
}

/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function afterSubmit( FlagStr, content )
{
	var flag = fm.all('flag').value;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      

    	showInfo.close();
    alert(content);
    //parent.close();
  }
  else
  { 
	var showStr="操作成功";
  	showInfo.close();
  	alert(showStr);
  	if(flag=="Insured")
    {
  		parent.close();
  	}
    else
  	{
  		showInsuredResult();
  	}
    //执行下一步操作
  }
}

/*********************************************************************
 *  取消
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelchk()
{
	fm.all('uwstate').value = "";
	fm.all('UWIdea').value = "";
}

/*********************************************************************
 *  返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function InitClick(){
	parent.close();
}

/*********************************************************************
 *  暂停被保人，向后台提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function StopInsured()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.flag.value = "StopInsured";
	
	fm.action = "./InsuredUWInfoChk.jsp";
  fm.submit(); //提交
}


/*********************************************************************
 *  初始化上报
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initSendTrace()
{
	if(SendFlag =="1")
	{
		divUWButton1.style.display = "none";
		divUWButton2.style.display = "none";
	}
}

/*********************************************************************
 *  计算核保结论
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function calRiskResult(arrSelected)
{
	var tResult="S";
	var arrSelected =new Array();
	var dFlag = 0 ;
	var pFlag = 0 ;
	var lFlag = 0 ;
	var eFlag = 0 ;
	var rFlag = 0 ;

	for(i=0;i<arrSelected.length;i++)
	{
		if(arrSelected[i]=="D")
		{
			dFlag = 1;
		}
		if(arrSelected[i]=="P")
		{
			pFlag = 1;
		}
		if(arrSelected[i]=="R")
		{
			rFlag = 1;
		}		
		if(arrSelected[i]=="E")
		{
			eFlag = 1;
		}			
		if(arrSelected[i]=="L")
		{
			lFlag = 1;
		}	
	}
	if(dFlag == 1)
	{
		tResult="D";
	}
  else
  {
  	if(pFlag == 1)
  	{
  		tResult="P";
  	}
  	else
  	{
  		if(rFlag == 1)
  		{
  			tResult="R";
  		}
  		else
  		{
  			  	if(eFlag == 1)
  					{
  						tResult="E";
  					}
  					else
  					{
  						  	if(lFlag == 1)
  								{
  									tResult="L";
  								}
  								else
  								{
  									tResult="S";
  								}
  					} 
  		}  		  
  	}         
  }
  	
  return tResult;     
}             
              
/*********************************************************************
 *  得出被保人建议结论
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */              
function showInsuredResult()
{
	var arrSelected = new Array();
	var arrResult = new Array();

	strSQL = "select sugpassflag from lcuwmaster where 1=1 "
						+" and contno ='"+ContNo+"'"
						;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	arrResult = decodeEasyQueryResult(turnPage.strQueryResult);

	//判断是否查询成功
  if (turnPage.strQueryResult) { 
		for(j=0;j<arrResult.length;j++)
		{
			arrSelected[j] = arrResult[j][0];
		}
		//fm.SugIndUWFlag.value = calRiskResult(arrSelected);
	}	
}        

