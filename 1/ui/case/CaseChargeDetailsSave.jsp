<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>



<%
  LLCaseChargeDetailSchema tLLCaseChargeDetailSchema   = new LLCaseChargeDetailSchema();
  LLCaseChargeDetailSet tLLCaseChargeDetailSet=new LLCaseChargeDetailSet();
  LLFeeMainSchema aLLFeeMainSchema = new LLFeeMainSchema();
  LLCaseChargeDetailBL tLLCaseChargeDetailBL   = new LLCaseChargeDetailBL();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "INSERT";

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
	//读取立案明细信息
  

  String strCaseNo=request.getParameter("CaseNo");
  String strRgtNo=request.getParameter("RgtNo");
  String strMainFeeNo=request.getParameter("MainFeeNo");
  System.out.println("分案号======="+strCaseNo);
  
  String[] strNumber=request.getParameterValues("CaseDrugGridNo");
  String[] strChargeType=request.getParameterValues("CaseDrugGrid11");
  String[] strChargeCode=request.getParameterValues("CaseDrugGrid2");
  String[] strChargeName=request.getParameterValues("CaseDrugGrid3");
  String[] strChargeDetail=request.getParameterValues("CaseDrugGrid4");
  String[] strUnitPrice=request.getParameterValues("CaseDrugGrid5");
  String[] strAmount=request.getParameterValues("CaseDrugGrid6");
  String[] strChargeFee=request.getParameterValues("CaseDrugGrid7");
  String[] strGiveType=request.getParameterValues("CaseDrugGrid10");
  //String[] strRemark=request.getParameterValues("CaseDrugGrid9");
  
  int intLength=0;
  if(strNumber!=null){
  	    intLength +=strNumber.length;
  		System.out.println("数据容量："+intLength);
  }

  for(int i=0;i<intLength;i++)
  {
    if(!strChargeName.equals("")){
  	  tLLCaseChargeDetailSchema=new LLCaseChargeDetailSchema();
  	  tLLCaseChargeDetailSchema.setCaseNo(strCaseNo);
  	  tLLCaseChargeDetailSchema.setRgtNo(strRgtNo);
  	  tLLCaseChargeDetailSchema.setMainFeeNo(strMainFeeNo);
  	  tLLCaseChargeDetailSchema.setChargeType(strChargeType[i]);
  	  tLLCaseChargeDetailSchema.setChargeCode(strChargeCode[i]);
  	  tLLCaseChargeDetailSchema.setChargeName(strChargeName[i]);
  	  tLLCaseChargeDetailSchema.setChargeDetail(strChargeDetail[i]);
  	  tLLCaseChargeDetailSchema.setUnitPrice(strUnitPrice[i]);
  	  tLLCaseChargeDetailSchema.setAmount(strAmount[i]);
  	  tLLCaseChargeDetailSchema.setChargeFee(strChargeFee[i]);
  	  tLLCaseChargeDetailSchema.setGiveType(strGiveType[i]);
  	  //tLLCaseChargeDetailSchema.setRemark(strRemark[i]);
  	  
      tLLCaseChargeDetailSet.add(tLLCaseChargeDetailSchema);
    }
  }
  aLLFeeMainSchema.setCaseNo(strCaseNo);
  aLLFeeMainSchema.setMainFeeNo(strMainFeeNo);
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLLCaseChargeDetailSet);
   tVData.addElement(tG);
   tVData.addElement(aLLFeeMainSchema);
   tLLCaseChargeDetailBL.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("3");
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLCaseChargeDetailBL.mErrors;
    if (!tError.needDealError())
    {
        Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

