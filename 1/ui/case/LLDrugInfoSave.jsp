
<%
	//程序名称：LLDrugInfoSave.jsp
	//程序功能：药品信息保存操作
	//创建日期：2015-02-11
	//创建人  ：Liyunxia
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

//接收信息，并作校验处理。
//输入参数
 
LLCaseDrugInfoSchema tLLCaseDrugInfoSchema = new LLCaseDrugInfoSchema();
LLCaseDrugInfoUI tLLCaseDrugInfoUI = new LLCaseDrugInfoUI();
//输出参数
String FlagStr = "";
String Content = "";
GlobalInput tGI = new GlobalInput(); //repair:
tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
//后面要执行的动作：添加，修改，删除
String transact=request.getParameter("Transact");

if(tGI==null)
{
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
}
else
{
	String Operator  = tGI.Operator ;  //保存登陆管理员账号
	String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
	CErrors tError = null;
    String tBmCert = "";
    
    System.out.println("transact:"+transact);
    if("INSERT".equals(transact))
    {
    	tLLCaseDrugInfoSchema.setSerialNo(PubFun1.CreateMaxNo("DRUG", 20));
    	tLLCaseDrugInfoSchema.setDrugCode(request.getParameter("DrugCode"));
    	tLLCaseDrugInfoSchema.setDrugCategory(request.getParameter("DrugCategoryValue"));
    	tLLCaseDrugInfoSchema.setDrugGenericName(request.getParameter("DrugGenericName"));
    	tLLCaseDrugInfoSchema.setDrugTradeName(request.getParameter("DrugTradeName"));
    	tLLCaseDrugInfoSchema.setDrugEnglishName(request.getParameter("DrugEnglishName"));
    	tLLCaseDrugInfoSchema.setDrugClassification(request.getParameter("DrugClassification"));
    	tLLCaseDrugInfoSchema.setSecondCategory(request.getParameter("SecondCategory"));
    	tLLCaseDrugInfoSchema.setFormulations(request.getParameter("Formulations"));
    	tLLCaseDrugInfoSchema.setSpecifications(request.getParameter("Specifications"));
    	tLLCaseDrugInfoSchema.setPhonetic(request.getParameter("Phonetic"));
    	tLLCaseDrugInfoSchema.setManufacturer(request.getParameter("Manufacturer"));
    	tLLCaseDrugInfoSchema.setPackageMessage(request.getParameter("PackageMessage"));
    	tLLCaseDrugInfoSchema.setAdministration(request.getParameter("Administration"));
    	tLLCaseDrugInfoSchema.setHealthNo(request.getParameter("HealthNo"));
    	tLLCaseDrugInfoSchema.setCostLevel(request.getParameter("CostLevelValue"));
    	tLLCaseDrugInfoSchema.setPaymentScope(request.getParameter("PaymentScope"));
    	tLLCaseDrugInfoSchema.setOutpatientLimit(request.getParameter("OutpatientLimitValue"));
    	tLLCaseDrugInfoSchema.setHospitalLimit(request.getParameter("HospitalLimitValue"));
    	tLLCaseDrugInfoSchema.setInjuriesMark(request.getParameter("InjuriesMarkValue"));
    	tLLCaseDrugInfoSchema.setFertilityMark(request.getParameter("FertilityMarkValue"));
    	tLLCaseDrugInfoSchema.setPriceCeiling(request.getParameter("PriceCeilingValue"));
    	tLLCaseDrugInfoSchema.setRemark(request.getParameter("Remark"));
    	tLLCaseDrugInfoSchema.setMngCom(request.getParameter("MngCom"));
    	tLLCaseDrugInfoSchema.setAreaCode(request.getParameter("AreaCode"));
    	tLLCaseDrugInfoSchema.setOperator(Operator);
    	tLLCaseDrugInfoSchema.setMakeDate(PubFun.getCurrentDate());
    	tLLCaseDrugInfoSchema.setMakeTime(PubFun.getCurrentTime());
    	tLLCaseDrugInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLLCaseDrugInfoSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else if("DELETE".equals(transact))
    {
    	String tSerialNo =request.getParameter("SerialNo");
    	tLLCaseDrugInfoSchema.setSerialNo(tSerialNo);
    }
    else if("UPDATE".equals(transact))
    {
    	String tSerialNo =request.getParameter("SerialNo");
    	
    	LLCaseDrugInfoDB tLLCaseDrugInfoDB = new LLCaseDrugInfoDB();
	    tLLCaseDrugInfoDB.setSerialNo(tSerialNo);
        tLLCaseDrugInfoDB.getInfo();
        LLCaseDrugInfoSchema cLLCaseDrugInfoSchema = tLLCaseDrugInfoDB.getSchema();
        
        
    	tLLCaseDrugInfoSchema.setSerialNo(tSerialNo);
    	tLLCaseDrugInfoSchema.setDrugCode(request.getParameter("DrugCode"));
    	tLLCaseDrugInfoSchema.setDrugCategory(request.getParameter("DrugCategoryValue"));
    	tLLCaseDrugInfoSchema.setDrugGenericName(request.getParameter("DrugGenericName"));
    	tLLCaseDrugInfoSchema.setDrugTradeName(request.getParameter("DrugTradeName"));
    	tLLCaseDrugInfoSchema.setDrugEnglishName(request.getParameter("DrugEnglishName"));
    	tLLCaseDrugInfoSchema.setDrugClassification(request.getParameter("DrugClassification"));
    	tLLCaseDrugInfoSchema.setSecondCategory(request.getParameter("SecondCategory"));
    	tLLCaseDrugInfoSchema.setFormulations(request.getParameter("Formulations"));
    	tLLCaseDrugInfoSchema.setSpecifications(request.getParameter("Specifications"));
    	tLLCaseDrugInfoSchema.setPhonetic(request.getParameter("Phonetic"));
    	tLLCaseDrugInfoSchema.setManufacturer(request.getParameter("Manufacturer"));
    	tLLCaseDrugInfoSchema.setPackageMessage(request.getParameter("PackageMessage"));
    	tLLCaseDrugInfoSchema.setAdministration(request.getParameter("Administration"));
    	tLLCaseDrugInfoSchema.setHealthNo(request.getParameter("HealthNo"));
    	tLLCaseDrugInfoSchema.setCostLevel(request.getParameter("CostLevelValue"));
    	tLLCaseDrugInfoSchema.setPaymentScope(request.getParameter("PaymentScope"));
    	tLLCaseDrugInfoSchema.setOutpatientLimit(request.getParameter("OutpatientLimitValue"));
    	tLLCaseDrugInfoSchema.setHospitalLimit(request.getParameter("HospitalLimitValue"));
    	tLLCaseDrugInfoSchema.setInjuriesMark(request.getParameter("InjuriesMarkValue"));
    	tLLCaseDrugInfoSchema.setFertilityMark(request.getParameter("FertilityMarkValue"));
    	tLLCaseDrugInfoSchema.setPriceCeiling(request.getParameter("PriceCeilingValue"));
    	tLLCaseDrugInfoSchema.setRemark(request.getParameter("Remark"));
    	tLLCaseDrugInfoSchema.setMngCom(request.getParameter("MngCom"));
    	tLLCaseDrugInfoSchema.setAreaCode(request.getParameter("AreaCode"));
    	tLLCaseDrugInfoSchema.setOperator(Operator);
    	tLLCaseDrugInfoSchema.setMakeDate(cLLCaseDrugInfoSchema.getMakeDate());
    	tLLCaseDrugInfoSchema.setMakeTime(cLLCaseDrugInfoSchema.getMakeTime());
    	tLLCaseDrugInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLLCaseDrugInfoSchema.setModifyTime(PubFun.getCurrentTime());
    }
    
	try{
	  // 准备传输数据 VData
	   VData tVData = new VData();
	   tVData.add(tLLCaseDrugInfoSchema);
	   tLLCaseDrugInfoUI.submitData(tVData,transact);
  	}
 	 catch(Exception ex)
  	{
	    Content = "保存失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
  	}
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
      tError = tLLCaseDrugInfoUI.mErrors;
      if (!tError.needDealError())
      {     
        if("INSERT".equals(transact))
  	    {
        	Content ="保存成功！";
	  	    FlagStr = "Success";
  	    }
  	    else if("DELETE".equals(transact))
  	    {
  	    	Content ="删除成功！";
	  	    FlagStr = "Success";
  	    }
  	    else if("UPDATE".equals(transact))
  	    {
  	    	Content ="修改成功！";
	  	    FlagStr = "Success";
  	    }
      }
      else                                                                           
     {
    	    if("INSERT".equals(transact))
	  	    {
    	    	Content = "保存失败，原因是:" + tError.getFirstError();
   	  	     	FlagStr = "Fail";
	  	    }
	  	    else if("DELETE".equals(transact))
	  	    {
	  	    	Content = "删除失败，原因是:" + tError.getFirstError();
		  	    FlagStr = "Fail";
	  	    }
	  	    else if("UPDATE".equals(transact))
	  	    {
	  	    	Content = "修改失败，原因是:" + tError.getFirstError();
		  	    FlagStr = "Fail";
	  	    }
     }
  }
}

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>