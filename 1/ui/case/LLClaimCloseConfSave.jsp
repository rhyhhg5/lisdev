<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLClaimCloseConfSave.jsp
//程序功能：批次给付确认
//创建日期：2006-11-21
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
	<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String transact = "";
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");	


		VData tVData = new VData();
		LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
	  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	  LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
	  LLCaseSet tLLCaseSet = new LLCaseSet();
	  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
	  LJAGetSet tLJAGetSet = new LJAGetSet();
	  transact = request.getParameter("Operator");
    String CaseNo[]=request.getParameterValues("CaseGrid1");
    String RgtNo[]=request.getParameterValues("CaseGrid12");    
    String PayMode[]=request.getParameterValues("CaseGrid11");
    String AccName[]=request.getParameterValues("CaseGrid10");
    String BankAccNo[]=request.getParameterValues("CaseGrid9");
    String BankCode[]=request.getParameterValues("CaseGrid8");    
    String Drawer[]=request.getParameterValues("CaseGrid6");
    String DrawerID[]=request.getParameterValues("CaseGrid7");
    String tRNum[] = request.getParameterValues("InpCaseGridChk");
    System.out.println("回销标记=="+request.getParameterValues("CaseGrid16"));
    String tPrePaidFlag[] = request.getParameterValues("CaseGrid16");
    String modeDrawerID[]=request.getParameterValues("CaseGrid5");
   	for(int i=0;i<tRNum.length;i++){  
   		if(tRNum[i].equals("1")){
   		tVData = new VData();
   		tLJAGetInsertBL = new LJAGetInsertBL();
   		tLLCaseSchema = new LLCaseSchema();
  		tLJAGetSchema = new LJAGetSchema();
  		tLLCaseSchema.setCaseNo(CaseNo[i]);
  		tLLCaseSchema.setRgtNo(RgtNo[i]);
  		System.out.println("tPrePaidFlag[i]======="+tPrePaidFlag[i]);
  		tLLCaseSchema.setPrePaidFlag(tPrePaidFlag[i]);
  		System.out.println("modeDrawerID[i]======="+modeDrawerID[i]);
  		if(modeDrawerID[i]!="现金" && modeDrawerID[i]!="现金支票"){
  			tLLCaseExtSchema.setDrawerID(DrawerID[i]);
  		}
  		
  		
			tLJAGetSchema.setOtherNo(CaseNo[i]);
			tLJAGetSchema.setOtherNoType("5");
			tLJAGetSchema.setAccName(AccName[i]);
			tLJAGetSchema.setBankAccNo(BankAccNo[i]);
			tLJAGetSchema.setPayMode(PayMode[i]);
			 System.out.println("2222222222222:"+PayMode[i]);
			tLJAGetSchema.setBankCode(BankCode[i]);
			if(Drawer[i].equals("")||Drawer[i]==null){
			Drawer[i]=AccName[i];
			}
			tLJAGetSchema.setDrawer(Drawer[i]);
			tLJAGetSchema.setDrawerID(DrawerID[i]);
		  tVData.add(tLLCaseSchema);
		  tVData.add(tLLCaseExtSchema);
		  tVData.add(tLJAGetSchema);  
		  tVData.add(tG);	
		  if(!tLJAGetInsertBL.submitData(tVData,transact)) {
			  tError=tLJAGetInsertBL.mErrors;
			  Content += "<br>案件"+CaseNo[i]+",操作失败 "+ tError.getFirstError();
			  FlagStr = "Fail";
		  } else {
			  Content += "<br>案件"+CaseNo[i]+"操作成功" + tLJAGetInsertBL.getBackMsg();
			  
			  System.out.println("LLContAutoDealBL============Begin==========:");
		  	  try{
		  		String tsql = "select 1 from llclaimdetail where caseno = '"+CaseNo[i]+"' and riskcode in (select code from ldcode where codetype='lxb' ) and exists(select 1 from lcpol where polno=llclaimdetail.polno and polstate!='03070001')  with ur ";
		    	SSRS tSSRS = new ExeSQL().execSQL(tsql);   	
		    	
		    	
		  	  	  if(tSSRS.MaxRow>0){
		  	  	  	LPSecondUWBL tLPSecondUWBL  = new LPSecondUWBL();
		  	  	  	tLPSecondUWBL.submitData(tVData,"");
		  	  	  }else{
					LLContAutoDealBL tLLContAutoDealBL = new LLContAutoDealBL();
					tLLContAutoDealBL.submitData(tVData,"");
					System.out.println(tLLContAutoDealBL.getBackMsg());
					Content += tLLContAutoDealBL.getBackMsg();
		  	  	  }
		  	  	  System.out.println("Content=="+Content);
		  		  
		  	  }catch(Exception tex) {
		  		  Content += "合同终止保存失败，原因是:" + tex.toString()+"，请到理赔合同处理中进行操作!";
		  	  }
		  }
		  System.out.println("循环次数"+i);
		 }
   }
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
  tError=tLJAGetInsertBL.mErrors;
    if (!tError.needDealError())
    {                          
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }


//添加各种预处理
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html> 