<%
//程序名称：LLRgtChangePayModeInit.jsp
//程序功能：团体给付方式修改
//创建日期：2008-4-11
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  
%>
                          
<script language="JavaScript">
function initInpBox()
{
 try
 {
 	var LoadFlag = '<%=request.getParameter("LoadFlag")%>';
 	fm.LoadFlag.value=LoadFlag;
 if(LoadFlag=='2')
 {
	 	divRgt.style.display='';
		try
		{
			fm.RgtNo.value = "";
			fm.GrpNo.value = "";
			fm.GrpName.value = "";
			fm.TogetherFlag.value = "";
			fm.CaseGetMode.value = "";
			fm.BankCode.value = "";
			fm.BankAccNo.value = "";
			fm.AccName.value = "";
		 	fm.RgtNo.value = '<%=request.getParameter("RgtNo")%>';
		  var tsql = "SELECT rgtobjno,customerno,grpname,togetherflag,"
		           + "(select codename from ldcode where ldcode.codetype='lltogetherflag' and ldcode.code=llregister.TOGETHERFLAG),"
		           + "casegetmode,"
		           + "(select codename from ldcode where ldcode.codetype='llgetmode' and ldcode.code=llregister.casegetmode),"
		           + " bankcode,"
		           + "(select bankname from ldbank where bankcode=llregister.bankcode),"
		           + "BankAccNo,AccName"
		           + " FROM llregister WHERE rgtno = '"+fm.RgtNo.value+"' with ur";
		  var xrr = easyExecSql(tsql);
		  if(xrr){
		  	fm.GrpContNo.value = xrr[0][0];
		  	fm.GrpNo.value = xrr[0][1];
		  	fm.GrpName.value = xrr[0][2];
		  	fm.TogetherFlag.value = xrr[0][3];
		  	fm.TogetherFlagName.value = xrr[0][4];
		  	fm.CaseGetMode.value = xrr[0][5];
		  	fm.CaseGetModeName.value = xrr[0][6] ;
		  	fm.BankCode.value = xrr[0][7];
			fm.BankName.value = xrr[0][8];
			fm.BankAccNo.value = xrr[0][9];
			fm.AccName.value = xrr[0][10];
		  }
		  if ((fm.BankCode.value==""||fm.BankCode.value==null)
		       &&(fm.BankAccNo.value==""||fm.BankAccNo.value==null)
		       &&(fm.AccName.value==""||fm.AccName.value==null)) {
			searchAcc();
		  }

			if (fm.CaseGetMode.value!="1"&&fm.CaseGetMode.value!="2"){
				showDiv(titleBank,"true");
			}else{
				showDiv(titleBank,"false");
			}
			
			if (fm.TogetherFlag.value=="1"){
				showDiv(titleCaseGetMode1,"true");
				showDiv(titleCaseGetMode2,"true");
				showDiv(titleBank,"false");
			} else if (fm.TogetherFlag.value=="4"){
				showDiv(titleCaseGetMode1,"true");
				showDiv(titleCaseGetMode2,"true");
				fm.BankCode.onclick=bankcodename;
				fm.BankName.readOnly=false;
				fm.BankAccNo.readOnly=false;
				fm.AccName.readOnly=false;
			} else {
			    showDiv(titleCaseGetMode1,"true");
				showDiv(titleCaseGetMode2,"true");
				fm.BankCode.onclick="";
				fm.BankName.readOnly=true;
				fm.BankAccNo.readOnly=true;
				fm.AccName.readOnly=true;
			}
		}
		catch(ex)
		{
		  alert(ex);
		}
 }
 	  <%GlobalInput mG = new GlobalInput();
  	mG=(GlobalInput)session.getValue("GI");
  	%>
 }
 catch(ex)
 {
 alter("在LLRgtChangePayModeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
 }
 }
 function initForm()
 {
 try
 {
 initInpBox();
 }
 catch(re)
 {
 alter("在LLRgtChangePayModeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
 }
 }

</script>