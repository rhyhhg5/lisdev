var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var tSaveType="";
//提交，保存按钮对应操作
function submitForm()
{

  if ( fm.LogNo.value!="")
  {
    alert("你不能执行改操作");
    return false;
  }
  if (confirm("您确实想保存该记录吗?"))
  {
    tSaveFlag = "1";

    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.fmtransact.value="INSERT||MAIN";
    fm.submit(); //提交
    tSaveFlag ="0";
  }
  else
    {
      alert("您取消了修改操作！");
    }

  }

  //提交后操作,服务器数据返回后执行的操作
  function afterSubmit( FlagStr, content )
  {
    showInfo.close();
    if (FlagStr == "Fail" )
    {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
      {
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

        if (fm.fmtransact.value=="DELETE||MAIN")
        {
          fm.reset();
        }
      }
    }

    //重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
    function resetForm()
    {
      try
      {
        initForm();
      }
      catch(re)
      {
        alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
      }
    }

    //提交前的校验、计算
    function beforeSubmit()
    {
    }

    //显示frmSubmit框架，用来调试
    function showSubmitFrame(cDebug)
    {
      if(cDebug=="1")
      {
        parent.fraMain.rows = "0,0,0,0,*";
      }
      else
        {
          parent.fraMain.rows = "0,0,0,0,*";
        }
      }

      function afterCodeSelect( cCodeName, Field ) {
      }

      function onSelSelected(parm1,parm2)
      {
        alert();
      }

      function afterQuery(arr)
      {
      }

      function UWClaim()
      {
        var selno = CheckGrid.getSelNo();
        if (selno <=0)
        {
          alert("请选择要理算的个人受理");
          return ;
        }

        var varSrc="";
        var newWindow = window.open("./FrameMain.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0","",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
      }

      //从数据库中查询处调查回复与调查退回
      function QueryClick()
      {
        var strSql = "select CaseNo,SpotMngCom,Spoter,"
        + "SpotDate from LLCaseSpotTrace where 1=1 "
        + getWherePart("CaseNo", "CaseNo")
        + getWherePart("SpotMngCom", "SpotMngCom")
        + getWherePart("Spoter", "Spoter");
        turnPage.queryModal(strSql,SpotTraceGrid);
      }