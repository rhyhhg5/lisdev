<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：LiuYansong，陈海强
%>
 <%@page contentType="text/html;charset=GBK" %>
 <!--用户校验类-->
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLNoticeInput.js"></SCRIPT>
   <%@include file="LLNoticeInputInit.jsp"%>
 </head>

 <body  onload="initForm();" >
   <form action="./LLMainAskInputQueryOut.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         咨询客户信息
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
     <Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerInfo);">
          </TD>
          <TD class= titleImg>
            客户列表
          </TD>
        </TR>
      </Table>

    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCustomerGrid" >
            </span>
          </TD>
        </TR>
		  </table>
    </Div>
    <hr  width="88%" align='left'>
 <table  class= common>
<TR  class= common8>
<TD  class= title8>客户号码</TD><TD  class= input8><Input class= common name="CustomerNo"></TD>
<TD  class= title8>客户名称</TD><TD  class= input8><Input class= common name="CustomerName"></TD>
<TD  class= title8>客户类型</TD><TD  class= input8><Input class= code8 name="CustomerType"></TD>
</TR><TR  class= common8>
<TD  class= title8>是否咨询专家</TD><TD  class= input8><Input class= common name="ExpertFlag"></TD>
<TD  class= title8>专家编号</TD><TD  class= input8><Input class= common name="ExpertNo"></TD>
<TD  class= title8>专家姓名</TD><TD  class= input8><Input class= common name="ExpertName"></TD>
</TR>
<TR  class= common8>
<TD  class= title8>疾病代码</TD><TD  class= input8><Input class= code8 name="DiseaseCode"></TD>
<TD  class= title8>	期待回复日期</TD><TD  class= input8><Input class= common name="	ExpRDate"></TD>
</TR>

<TR  class= common>
	<TD  class= input colspan="6">咨询主题</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">	
		<textarea name="CSubject" cols="100%" rows="2" witdh=25% class="common"></textarea>
	</TD>
</TR>
<TR  class= common>
	<TD  class= title colspan="6">咨询内容</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="CContent" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>

<TR  class= common>
	<TD  class= input colspan="6">客户现状</TD>
	
</TR>
<TR  class= common>
	<TD  class= input colspan="6">	
		<textarea name="CustStatus" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>
<TR  class= common8>
	<TD  class= title8>咨询级别</TD><TD  class= input8><Input class= common name="AskGrade"></TD>
	<TD  class= title8>	咨询有效标志</TD><TD  class= input8><Input class= common name="	AvaiFlag"></TD>
</TR>
<TR  class= common8>
	<Input type=hidden class= common name="PeplyState"></TD>
</TR>
</table>
</DIV>
  <Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
          </TD>
          <TD class= titleImg>
            关联事件
          </TD>
        </TR>
      </Table>

    <Div  id= "divSecondUWInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanSubReportGrid" >
            </span>
          </TD>
        </TR>
		  </table>
    </Div>
    
<hr  width="25%" align='left'>

            <input class=cssButton type=button value="事件关联查询" onclick="showInsuredLCPol()">
            <input class=cssButton type=button value="事件录入" onclick="showCustomerInfo()">
<hr  width="25%" align='left'>           
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>