<html>
<%
//程序名称：药品剂量明细
//程序功能：药品剂量明细表录入
//创建日期：2015-10-28 16:45:10
//创建人  ：zqs
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  
  <SCRIPT src="./LLCasePrescriptionInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LLCasePrescriptionInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LLCasePrescriptionSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏CaseReceipt1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseReceipt1);">
      </td>
      <td class= titleImg>
        案件信息
      </td>
    	</tr>
    </table>

    <Div  id= "divCaseReceipt1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>案件号</TD>
          <TD  class= input><Input class= "readonly" readonly name=CaseNo ></TD>
          <TD  class= title>客户号</TD>
          <TD  class= input><Input class= "readonly" readonly name=CustomerNo ></TD>
          <TD  class= title>客户姓名</TD>
          <TD  class= input><Input class= "readonly" readonly name=CustomerName ></TD>
        </TR>
        <TR  class= common>
        	<TD  class= title8>门诊诊断类型为</TD>
            
      		<TD  class= input8><input class="codeno" name="DiagnoseType" CodeData="0|2^0|急性病^1|慢性病^2|行动不便^3|特定疾病" onclick="return showCodeListEx('DiagnoseType',[this,DiagnoseTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('DiagnoseType',[this,DiagnoseTypeName],[0,1]);"><input class=codename  name=DiagnoseTypeName></TD>
      	</TR>
      </table>
    </Div>
    
    <table style= "display: 'none'">
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseReceipt3);">
      </td>
      <td class= titleImg>
        药品剂量明细信息
      </td>
    	</tr>
    </table>
    
    
    <!--药品剂量明细部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCasePrescription);">
    		</td>
    		<td class= titleImg>
    			 费用明细信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divCasePrescription" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanLLCasePrescriptionGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
    <TABLE class=common style= "display: 'none'">
    		
	<TR  class= common8>
		<TD  class= input colspan="6">
		    <textarea readonly name="Remark" cols="100%" rows="3"  class="readonly">
		    </textarea>
		</TD>
	</TR>
    </table>
         <INPUT VALUE="确  认" class=cssButton name=confirmButton TYPE=button onclick="submitForm();"> 					
         <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="top.close();"> 					
      <Input type="hidden" name="MainFeeNo">
      <Input type="hidden" name="RgtNo">
      <Input type="hidden" name="FeeType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
}
catch(ex)
{
  alert(ex);
}
 
</script>

</html>
