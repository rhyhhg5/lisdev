<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAMarketInput.jsp
//程序功能：F1报表生成
//创建日期：2007-11-13
//创建人  ：xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.utility.*" %>
<%
    GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-60";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
%>
 <script>
   var msql=" 1 and   char(length(trim(comcode)))<=#4# ";
 var msql1=" 1 and   branchtype=#2#  and branchtype2=#01# and endflag=#N#";

</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLOnlinClaimQuestion.js"></SCRIPT>  
 <%@include file="LLOnlinClaimQuestionInit.jsp"%> 
 
<script language="javascript">
   function initDate(){
		
  }
   </script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body  onload="initDate();initForm();">    
  <form action="" method=post name=fm target="fraSubmit">

    <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClientInfoTitle);">
          </td>
          <td class= titleImg>
            	案件问题件信息
          </td>
        </tr>
      </table>
      <div id= "div" style= "display: ''" >
        <table  class= common>
          <TR  class= common8>
          <TD  class= title8>理赔号</TD><TD  class= input8><Input class=readonly readonly name="Caseno" ></TD>
          <TD  class= title8>客户号</TD><TD  class= input8><Input class=readonly readonly name="Customerno" ></TD>
          <TD  class= title8>客户姓名</TD><TD  class= input8><Input class=readonly readonly name="Customername" ></TD>
          </TR>
          <TR  class= common8>
          <TD  class= title8>下发状态</TD><TD  class= input8><Input class=readonly readonly name="Lowerstate" ></TD>
          <TD  class= title8>下发人员</TD><TD  class= input8><Input class=readonly readonly name="Lowerpeople" ></TD>
          <TD  class= title8></TD><TD  class= input8><Input class=readonly readonly name="" ></TD>
          </TR>
          <TR  class= common>
          <TD  class= title>问题下发原因</TD>
          <TD  class= input8 colspan=5 > <input class=codeno  name=QuestionLowerNo onclick="return showCodeList('QuestionLower',[this,QuestionLowerName],[0,1],null,fm.QuestionLowerName.value,'QuestionLowerName',1);" onkeyup="return showCodeListKeyEx('QuestionLower',[this,QuestionLowerName],[0,1],null,fm.QuestionLowerName.value,'QuestionLowerName',1);" elementtype=nacessary ><Input class=codename name= QuestionLowerName elementtype=nacessary style="width:'50%'"></TD>                  
           </TR>
        </table>
        <table  class= common>
        	 <TR  class= common>
          <TD  class= title colspan=6>
            		问题备注信息
          </TD>
          </tr>
          <TR  class= common>
          <TD  class= input colspan=6>
		<textarea name="QuestionRemark" cols="90%" rows="4" width=25% class="common"></textarea></TD>
        </tr>  
        </table>
      </div>
      
      <input class=cssButton style='width:80px;' type=button value="确认" onclick="confirmData()">
      <input class=cssButton style='width:80px;' type=button value="返回" onclick="top.close();">
      
      <div id="div1" style="display:''">
			<table>
				<TR>
					<td>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegisterInfo);">
					</td>
					<td class= titleImg>
						既往问题件
					</TD>
				</TR>
			</table>
		</div>
		
	  <Div  id= "divPastQuestionGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left >
            <span id="spanPastQuestionGrid" >
            </span>
          </TD>
        </TR>
      </table>
  	
         <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">      
    </Div> 
     <input type=hidden name=operate>
</form>
 		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    
</body>
</html> 
