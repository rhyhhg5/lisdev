<%
//Name:ReportInit.jsp
//function：
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    var DealType = "<%=request.getParameter("DealType")%>";
    if(DealType=='QD'){
    	fm.ManageCom.value='8694';
    }
    if(DealType=='YN'){
    	fm.ManageCom.value='8653';
    }
    if(DealType=='NJ'){
    	fm.ManageCom.value='8632';
    }
  }
  catch(ex)
  {
    alter("在SimpleCaseListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在SimpleCaseListInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initGrpRegisterGrid();
    SearchGrpRegister();
    initGrpCaseGrid();
  }
  catch(re)
  {
    alter("在SimpleCaseListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpRegisterGrid()
{
   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("团体批次号","120px","0",0);
    iArray[2]=new Array("团体客户号","100px","0",0);
    iArray[3]=new Array("单位名称","120px","0",0);
    iArray[4]=new Array("团体合同号","100px","0",0);
    iArray[5]=new Array("申请人","70px","0",0);
    iArray[6]=new Array("申请日期","80px","0",0);
    iArray[7]=new Array("申请人数","60px","0",0);
    iArray[8]=new Array("案件状态","80px","0",0);
    iArray[9]=new Array("案件状态","80px","0",3);
    
    GrpRegisterGrid = new MulLineEnter( "fm" , "GrpRegisterGrid" );
    
    GrpRegisterGrid.mulLineCount = 10;
    GrpRegisterGrid.displayTitle = 1;
    GrpRegisterGrid.canChk =0;
    GrpRegisterGrid.canSel =1;
    GrpRegisterGrid.hiddenPlus=1;
    GrpRegisterGrid.hiddenSubtraction=1;
    GrpRegisterGrid.locked = 1;  
    GrpRegisterGrid.selBoxEventFuncName = "queryGrpCaseGrid"; 
    GrpRegisterGrid.loadMulLine(iArray);
   } 
  catch(ex)
  {
    alert(ex);
  }    
}

function initGrpCaseGrid(backFlag)
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("理赔号", "100px", "0", "0");
    iArray[2]=new Array("社保号", "60px", "0", "0");
    iArray[3]=new Array("客户姓名", "60px", "0", "0");
    iArray[4]=new Array("客户号", "60px", "0", "0");
    iArray[5]=new Array("申请日期", "60px", "0", "0");
    iArray[6]=new Array("申请金额", "40px", "0", "0");
    iArray[7]= new Array("回退原因","40px","10","2")
    iArray[7][4]="llcasebackreason"
    iArray[7][5]="7|8";       //引用代码对应第几列，'|'为分割符
    iArray[7][6]="0|1";
    iArray[7][9]="回退原因|NOTNULL";
    iArray[7][17]="3"     
    iArray[7][19]="1";      
    iArray[8]= new Array("备注","160px","10","1")
    if(backFlag!='2'){
      iArray[7][3]="3";
      iArray[8][3]="3";
    }

    GrpCaseGrid = new MulLineEnter("fm","GrpCaseGrid");
    GrpCaseGrid.mulLineCount =5;
    GrpCaseGrid.displayTitle = 1;
    GrpCaseGrid.locked = 1;
    GrpCaseGrid.canSel =1;
    GrpCaseGrid.hiddenPlus=1;  
    GrpCaseGrid.hiddenSubtraction=1;
    GrpCaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter("案件列表GrpCaseGrid加载失败");
  }
}

 </script>