var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var tSaveType="";

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function QueryOnKeyDown()
{
  	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}

//TODO：理赔二核页面跳转
function submitLLUnderWrit(){

  var selno = CheckGrid.getSelNo()-1;
  var CaseNo="";
  var RgtNo="";
  var CustomerNo="";
  var CustomerName="";
  if(selno < 0){
  	alert("请选中案件！");
  	return false;
  }
  if (selno >=0)
  {
    CaseNo = CheckGrid.getRowColData( selno, 2);
    CustomerNo = CheckGrid.getRowColData( selno, 5);
    CustomerName = CheckGrid.getRowColData( selno, 3)
  }
  strSQL="select rgtno from llcase where caseno='"+CaseNo+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }
  
  var varSrc = "&CaseNo=" + CaseNo;
  varSrc += "&InsuredNo=" + CustomerNo;
  varSrc += "&CustomerName=" + CustomerName;
  varSrc += "&RgtNo=" + RgtNo;
  pathStr="./FrameMainLLUnderWrit.jsp?Interface=LLUnderWritInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"LLUnderWritInput","middle",800,500);
}

function easyQuery()
{
	if (fm.RgtNo.value=="" && fm.CustomerName.value=="" && fm.CustomerNo.value=="" && fm.CaseNo.value=="") {
		var startDate = fm.RgtDateS.value;
		var endDate = fm.RgtDateE.value;
		if (startDate==""||startDate==null||endDate==""||endDate==null){
			alert("请输入受理日期");
			return false;
		}
	}
  var contType=fm.ContType.value;
  var contTypeSQL="";
   if(contType!="" && contType!="null"){
   if(contType=="1"){
     contTypeSQL=" and exists (select 1 from lcpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno union select 1 from lbpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno)";
    }
    if(contType=="2"){
   contTypeSQL=" and not exists (select 1 from lcpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno union select 1 from lbpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno ) and exists (select 1 from lcpol where conttype='2'  and appflag='1' and insuredno=a.customerno union select 1 from lbpol where  conttype='2' and appflag='1' and insuredno=a.customerno)";
   }
   
  }

	//咨询类
	var typeRadio="";
	for(i = 0; i <fm.typeRadio.length; i++)
	{
		if(fm.typeRadio[i].checked)
		{
			typeRadio=fm.typeRadio[i].value;
			break;
		}
	}
	var strSQL ="";
	var strSQL2="";
    var dpart="";
    //getWherePart("a.rgtdate","RgtDateS",">=") + getWherePart("a.rgtdate","RgtDateE","<=");
    if(fm.RgtDateS.value!=null && fm.RgtDateS.value!=''){
       dpart = dpart + " and a.rgtdate >= trim('" + fm.RgtDateS.value + "') ";
    }
    if(fm.RgtDateE.value !=null && fm.RgtDateE.value!=''){
       dpart = dpart + " and a.rgtdate <= trim('" + fm.RgtDateE.value + "') ";
    }
    
    var RgtType = fm.RgtType.value;
    var RgtTypeSQL = "";
    if (RgtType == 1) {
    	RgtTypeSQL = " and not exists (select 1 from llhospcase where caseno=a.caseno)"
    		       + " and not exists (select 1 from llregister where rgtno=a.rgtno and applyertype='5')";
    } else if (RgtType == 2) {
    	RgtTypeSQL = " and exists (select 1 from llregister where rgtno=a.rgtno and applyertype='5')";
    } else if (RgtType == 3) {
    	RgtTypeSQL = " and exists (select 1 from llhospcase where caseno=a.caseno and casetype is null) ";
    } else if (RgtType == 4) {
    	RgtTypeSQL = " and exists (select 1 from llhospcase where caseno=a.caseno and casetype='01') ";
    } else if (RgtType == 5) {
    	RgtTypeSQL = " and exists (select 1 from llhospcase where caseno=a.caseno and casetype='02') ";
    } else if(RgtType == 6){
    	RgtTypeSQL = " and exists (select 1 from lcpol where prtno in(select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo in(select PolicySequenceNo from YbkReport)) and riskcode in(select code from ldcode where codetype='ybkriskcode') and InsuredNo=a.CustomerNo) ";
    } else if (RgtType == 7) {
    	RgtTypeSQL = " and exists (select 1 from llhospcase where caseno=a.caseno and casetype='10')";
    }else if(RgtType == 8){
    	RgtTypeSQL =" and exists (select 1 from llhospcase where caseno=a.caseno and casetype in ('12','14','15'))";
    }else if(RgtType == 9){
		RgtTypeSQL =" and exists (select 1 from llhospcase where caseno=a.caseno and casetype in ('18'))";
    }else {
    
    	RgtTypeSQL = "";
    }

	switch(typeRadio)
	{
		//咨询类
		case "0":
		strSQL ="select '',c.ConsultNo,'咨询类','',c.CustomerNo,c.CustomerName,b.LogDate,a.AnswerDate,'','',d.codename"
		+" from LLConsult c, LLMainAsk b,LLAnswerinfo a,ldcode d  where 1=1"
		+ getWherePart("c.ConsultNo","CaseNo")
		+ getWherePart("c.CustomerNo","CustomerNo")
		+ getWherePart("c.CustomerName","CustomerName")
		//+ getWherePart("c.MngCom","OrganCode","like")
		+ getWherePart("c.MngCom","ManageCom","like")
		//#2330 （需求编号：2014-230）客户信箱增加“证件号码”查询条件
		+ getWherePart("b.otherno","IDNo")
		+ " and substr(c.ConsultNo,1,1)='Z' "
		+" and b.logno=c.logno and c.logno = a.logno"
		+" and d.codetype='llreplystate' and d.code=c.ReplyState"
		+ getWherePart("c.makedate","RgtDateS",">=") + getWherePart("c.makedate","RgtDateE","<=");
		//divLLcaseRgtstate.style.display='none';//咨询类时不显示案件状态 modified by zzh 101108
		break;

		//通知类
		case "1":
		strSQL ="select '',c.ConsultNo,'通知类','',c.CustomerNo,c.CustomerName,b.LogDate,(select AnswerDate from LLAnswerinfo where logno = c.logno),'','',d.codename"
		+" from LLConsult c,LLMainAsk b,ldcode d  where 1=1"
		+ getWherePart("c.ConsultNo","CaseNo")
		+ getWherePart("c.CustomerNo","CustomerNo")
		+ getWherePart("c.CustomerName","CustomerName")
		//+ getWherePart("c.MngCom","OrganCode","like")
		+ getWherePart("c.MngCom","ManageCom","like")
		//#2330 （需求编号：2014-230）客户信箱增加“证件号码”查询条件
		+ getWherePart("b.otherno","IDNo")
		+ " and substr(c.ConsultNo,1,1)='T' "
		+" and b.logno=c.logno "
		+" and d.codetype='llreplystate' and d.code=c.ReplyState"
		+ getWherePart("c.makedate","RgtDateS",">=") + getWherePart("c.makedate","RgtDateE","<=");
		//divLLcaseRgtstate.style.display='none';//通知类时不显示案件状态 modified by zzh 101108
		break;

		//咨询通知类（取消）
		case "2":
		strSQL ="select c.ConsultNo,'咨询通知类','',c.CustomerNo,c.CustomerName,b.LogDate,a.AnswerDate,'','',d.codename"
		+" from LLConsult c, LLMainAsk b,LLAnswerinfo a,ldcode d  where 1=1"
		+ getWherePart("c.CustomerNo","CustomerNo")
		//+ getWherePart("c.MngCom","OrganCode","like")
		+ getWherePart("c.MngCom","ManageCom","like")
		+" and b.logno=c.logno and c.logno = a.logno"
		+" and d.codetype='llreplystate' and d.code=c.ReplyState";
		break;

		//申请类
		case "3":
		var rgtstate="";
		for(i = 0; i <fm.RgtState.length; i++)
		{
			if(fm.RgtState[i].checked)
			{
				rgtstate=fm.RgtState[i].value;
			}
		}

		var statePart ="";
		switch(rgtstate){	   case "01":
			statePart =" and rgtstate not in ('09','11','12','14')";
			break;
			case "02":
			statePart =" and rgtstate ='09'";
			break;
			case "03":
			statePart =" and rgtstate='11'";
			break;
			case "04":
			statePart =" ";
			break;
		}


		if ( strSQL !="" ) strSQL += " union ";
		strSQL ="select case when a.rgtno=a.caseno then '' else a.rgtno end,a.CaseNo,'申请类',a.rgtstate,a.CustomerNo,a.CustomerName,a.rgtdate,a.endcasedate,a.handler,(select u.username from lduser u where u.usercode=a.handler),b.codename "
		+" from llcase a, ldcode b where 1=1 "
		//         + getWherePart("a.Operator","Operator")
		+ getWherePart("a.Rgtno","RgtNo")
		+ getWherePart("a.Caseno","CaseNo")
		+ getWherePart("a.CustomerNo","CustomerNo")
		//#2330 （需求编号：2014-230）客户信箱增加“证件号码”查询条件
		+ getWherePart("a.idno","IDNo")
		+ getWherePart("a.CustomerName","CustomerName");
		strSQL2=strSQL; //added by zzh 101108 新增显示异地调查案件，就不需要用getWherePart("a.MngCom","ManageCom","like")了
		//+ getWherePart("a.MngCom","OrganCode","like")
		strSQL=strSQL+ getWherePart("a.MngCom","ManageCom","like")
		+" and b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgttype='1' "
		strSQL = strSQL +dpart+ statePart;
		
		strSQL2=strSQL2+" and b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgttype='1' "//add by zzh 101108
		strSQL2 = strSQL2 +dpart+ statePart;
		
		if(contType!=""){
		strSQL=strSQL+contTypeSQL;
		strSQL2=strSQL2+contTypeSQL;//add by zzh 101108
		}
		
		if (RgtTypeSQL != "") {
			strSQL = strSQL + RgtTypeSQL;
			strSQL2 = strSQL2 + RgtTypeSQL;
		}
		
		strSQL2+=" and a.caseno in (select otherno from llinqapply where inqper in "+
		"(select usercode from llclaimuser where 1=1 "+getWherePart("comcode","ManageCom","like")+" and surveyflag='2' and stateflag='1' )"
		+" or dipatcher in (select usercode from llclaimuser where 1=1 "+
		getWherePart("comcode","ManageCom","like")+" and surveyflag='1' and stateflag='1' )) ";
		
		strSQL=strSQL+" union "+strSQL2;
		strSQL +=" order by rgtdate";
		//divLLcaseRgtstate.style.display='';//申请类时显示案件状态 modified by zzh 101108
    break;
		case "4":
		var rgtstate="";
		for(i = 0; i <fm.RgtState.length; i++)
		{
			if(fm.RgtState[i].checked)
				rgtstate=fm.RgtState[i].value;
		}

		var statePart ="";
		switch(rgtstate){	   case "01":
			statePart =" and rgtstate not in ('09','11','12','14')";
			break;
			case "02":
			statePart =" and rgtstate = '09'";
			break;
			case "03":
			statePart =" and rgtstate='11'";
			break;
			case "04":
			statePart =" ";
			break;
		}


		if ( strSQL !="" ) strSQL += " union ";
		strSQL ="select case when a.rgtno=a.caseno then '' else a.rgtno end,a.CaseNo,'申诉类',a.rgtstate,a.CustomerNo,a.CustomerName,a.rgtdate,a.endcasedate,a.handler,(select u.username from lduser u where u.usercode=a.handler),b.codename "
		+" from llcase a, ldcode b where 1=1 "
		//         + getWherePart("a.Operator","Operator")
		+ getWherePart("a.Rgtno","RgtNo")
		+ getWherePart("a.Caseno","CaseNo")
		+ getWherePart("a.CustomerNo","CustomerNo")
		//#2330 （需求编号：2014-230）客户信箱增加“证件号码”查询条件
		+ getWherePart("a.idno","IDNo")
		+ getWherePart("a.CustomerName","CustomerName")

		strSQL2=strSQL;
		
		//+ getWherePart("a.MngCom","OrganCode","like")
		strSQL=strSQL+ getWherePart("a.MngCom","ManageCom","like")
		+" and b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgttype='4' "
		strSQL = strSQL+dpart+ statePart;
		
		strSQL2=strSQL2+" and b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgttype='4' "
		strSQL2 = strSQL2+dpart+ statePart;
		strSQL2+=" and a.caseno in (select otherno from llinqapply where inqper in "+
		"(select usercode from llclaimuser where 1=1 "+getWherePart("comcode","ManageCom","like")+" and surveyflag='2' and stateflag='1' )"
		+" or dipatcher in (select usercode from llclaimuser where 1=1 "+
		getWherePart("comcode","ManageCom","like")+" and surveyflag='1' and stateflag='1' )) ";
		
		if (RgtTypeSQL != "") {
			strSQL = strSQL + RgtTypeSQL;
			strSQL2 = strSQL2 + RgtTypeSQL;
		}
		
		strSQL=strSQL+" union "+strSQL2+" order by CaseNo";
		//divLLcaseRgtstate.style.display='';//申诉类时显示案件状态 modified by zzh 101108
    break;
		case "5":
		var rgtstate="";
		for(i = 0; i <fm.RgtState.length; i++)
		{
			if(fm.RgtState[i].checked)
				rgtstate=fm.RgtState[i].value;
		}

		var statePart ="";
		switch(rgtstate){	   case "01":
			statePart =" and rgtstate not in ('09','11','12','14')";
			break;
			case "02":
			statePart =" and rgtstate = '09'";
			break;
			case "03":
			statePart =" and rgtstate='11'";
			break;
			case "04":
			statePart =" ";
			break;
		}


		if ( strSQL !="" ) strSQL += " union ";
		strSQL ="select case when a.rgtno=a.caseno then '' else a.rgtno end,a.CaseNo,'纠错类',a.rgtstate,a.CustomerNo,a.CustomerName,a.rgtdate,a.endcasedate,a.handler,(select u.username from lduser u where u.usercode=a.handler),b.codename "
		+" from llcase a, ldcode b where 1=1 "
		//         + getWherePart("a.Operator","Operator")
		+ getWherePart("a.Rgtno","RgtNo")
		+ getWherePart("a.Caseno","CaseNo")
		+ getWherePart("a.CustomerNo","CustomerNo")
		//#2330 （需求编号：2014-230）客户信箱增加“证件号码”查询条件
		+ getWherePart("a.idno","IDNo")
		+ getWherePart("a.CustomerName","CustomerName")
		//+ getWherePart("a.MngCom","OrganCode","like")
		
		strSQL2=strSQL;
		
		strSQL=strSQL+ getWherePart("a.MngCom","ManageCom","like")
		+" and b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgttype='5' "
		strSQL = strSQL+dpart+ statePart;
		
		strSQL2=strSQL2+" and b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgttype='5' "
		strSQL2 = strSQL2+dpart+ statePart;
		strSQL2+=" and a.caseno in (select otherno from llinqapply where inqper in "+
		"(select usercode from llclaimuser where 1=1 "+getWherePart("comcode","ManageCom","like")+" and surveyflag='2' and stateflag='1' )"
		+" or dipatcher in (select usercode from llclaimuser where 1=1 "+
		getWherePart("comcode","ManageCom","like")+" and surveyflag='1' and stateflag='1' )) ";
		
		if (RgtTypeSQL != "") {
			strSQL = strSQL + RgtTypeSQL;
			strSQL2 = strSQL2 + RgtTypeSQL;
		}
		
		strSQL=strSQL+" union "+strSQL2+" order by CaseNo";
		//divLLcaseRgtstate.style.display='';//纠错类时显示案件状态 modified by zzh 101108
	}

	if ( strSQL =="" )
	{
		alert("请输入查询条件");
		return false;
	}else{
		strSQL+=" with ur ";
		turnPage1.queryModal(strSQL, CheckGrid);
	}

}

//检录
function DealCheck()
{
	var caseNo ="";
	var selno = CheckGrid.getSelNo()-1;
	if (selno >=0)
	{
		caseNo = CheckGrid.getRowColData( selno, 2);
	}
	var varSrc="&CaseNo="+ caseNo;
	var newWindow = OpenWindowNew("./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
	varSrc,"检录","left");

}

//理算
function DealClaim()
{
	var caseno = '';
	var selno = CheckGrid.getSelNo()-1;
	if (selno >=0)
	{
		caseno = CheckGrid.getRowColData(selno,2);
	}
	var varSrc="&CaseNo=" + caseno ;
	var newWindow = OpenWindowNew("./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+
	varSrc,"理算","left");

}

//咨询通知
function DealConsult()
{
	var selno = CheckGrid.getSelNo()-1;
	var caseNo;
	if (selno >=0)
	{
		caseNo = CheckGrid.getRowColData( selno, 2);
	}

		var varSrc="&CaseNo="+ caseNo;
		var newWindow = window.open("./FrameMainCaseNote.jsp?Interface=LLMainAskInput.jsp"+varSrc,"咨询通知",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');

	}

//受理申请
function DealApp()
{

	var selno = CheckGrid.getSelNo()-1;
	var caseNo;
	var RgtNo;
	if (selno >=0)
	{
		caseNo = CheckGrid.getRowColData( selno, 2);
		strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
		arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
			RgtNo = arrResult[0][0];

		}
	}

		var varSrc= "&RgtNo="+ RgtNo + "&CaseNo="+ caseNo;
		var newWindow = window.open("./FrameMainCaseRequest.jsp?Interface=LLRegisterInput.jsp"+varSrc,"受理申请",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');

	}

//团体受理申请
function DealAppGrp()
{
	var selno = CheckGrid.getSelNo()-1;
	var RgtNo;
	if (selno >=0)
	{
		RgtNo = CheckGrid.getRowColData( selno, 1);
	}
		var varSrc="&RgtNo="+ RgtNo;
		var newWindow = window.open("./FrameMainGrpCheck.jsp?Interface=LLGrpRegisterInput.jsp"+varSrc,"团体受理申请",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

//帐单录入
function DealFeeInput()
{	var CaseNo="";
	var selno = CheckGrid.getSelNo()-1;
	if (selno >=0)
	{
		CaseNo = CheckGrid.getRowColData(selno, 2);
	}
	var varSrc = "&CaseNo=" + CaseNo;
	var newWindow =OpenWindowNew("./FrameMainZD.jsp?Interface=ICaseCureInput.jsp" + varSrc,"","left");

}

function DealApprove()
{
	var selno = CheckGrid.getSelNo()-1;
	var caseNo = "";
	if (selno <0)
	{
		//alert("请选择要审批审定的个人受理");
	}
else
	{
		caseNo = CheckGrid.getRowColData( selno, 2);
	}

	var varSrc = "&CaseNo=" + caseNo;
	var newWindow = OpenWindowNew("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + varSrc,"","left");

}
function DealCancel()
{
	var caseno = '';
	var selno = CheckGrid.getSelNo()-1;
	if (selno >=0)
	{
		caseno = CheckGrid.getRowColData(selno,2);
		CustomerName = CheckGrid.getRowColData(selno,6);
		CustomerNo = CheckGrid.getRowColData(selno,5);
	}
	var varSrc="&CaseNo=" + caseno +"&CustomerNo="+CustomerNo+"&CustomerName="+CustomerName;
showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

//给付通知书
function ClaimGetPrint()
{

	var selno = CheckGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选择要打印决定通知书的理赔案件");
		return ;
	}

	var CaseNo = CheckGrid.getRowColData(selno - 1, 2);


	//var newWindow = window.open("../f1print/ClaimGetPrintChk.jsp?hiddenCaseNo="+CaseNo );
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action = "../uw/PDFPrintSave.jsp?Code=lp000&OtherNo="+CaseNo;
	fm.submit();
}

//拒付通知书
function ClaimDeclinePrint()
{

	var selno = CheckGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选择要打印决定通知书的理赔案件");
		return ;
	}

	var CaseNo = CheckGrid.getRowColData(selno - 1, 2);


	var newWindow = window.open("../f1print/ClaimDeclinePrint.jsp?hiddenCaseNo="+CaseNo );
}

function ClaimDetailPrint()
{
	var selno = CheckGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选择要打印给付明细表的理赔案件");
		return ; 
	} 
	var CaseNo = CheckGrid.getRowColData(selno - 1, 2);
	var code;
	fm.SingleCaseNo.value=CaseNo;
	fm.fmtransact.value="only";
	fm.selno.value="4";
	fm.Detail.value="on";
	fm.GrpDetail.value="off";
	fm.Notice.value="off";  
	
	var tsql="select distinct b.caserelano from llcaserela a,llclaimdetail b where a.caseno=b.caseno and a.caseno='"+ CaseNo +"'";
	var arrResult = easyExecSql(tsql);
	for(i = 0; i <arrResult.length; i++)
	{
		fm.CaseRelaNo.value = arrResult[i][0];
		if(CaseNo.substr(1, 2)=="31"||CaseNo.substr(1, 2)=="32"){
  	   	var newWindow = window.open("./ClaimDetailPrintSave.jsp?CaseNo=" + CaseNo+ "&fmtransact=PRINT&CaseRelaNo="+fm.CaseRelaNo.value);		 
		  }else{
		 	 var showStr="正在打印，请您稍候并且不要修改屏幕上的值或链接其他页面";
		 	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  	  	fm.action="./LLBatchPrtSave.jsp";
	  	}	  	 
	  fm.submit();
	}	

}

//给付凭证打印
function GPrint()
{
	var selno = CheckGrid.getSelNo();
	var ActuGetNo="";
	var caseNo;
	if (selno <= 0)
	{
		alert("请选择要打印给付凭证的理赔案件");
		return ;
	}

//	fm.target="f1print";
//	fm.fmtransact.value="PRINT";

	caseNo = CheckGrid.getRowColData( selno-1, 2);

	strSQL="select ActuGetNo from LJAGet where OtherNo='"+caseNo+"'" +"and OtherNoType='5' and (finstate<>'FC' or finstate is null)";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		ActuGetNo = arrResult[0][0];

	}else{
            //转出去,不执行打印方法
           alert("实付号码为空，团体案件尚未给付确认！");
           return;
        }
        //var tGetNoticeNo=ContGrid.getRowColData(tChked[0],10);
	//var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp008' and standbyflag2='"+ActuGetNo+"'");

	//if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	//{

	//	fm.action="./LLClaimMainListPrintSave.jsp?ActuGetNo="+ActuGetNo+"&RgtClass=0";
	//	fm.submit();
	//}
	//else
	//{
		//window.open("../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq);
	//	fm.action = "../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ ActuGetNo +"&PrtSeq="+PrtSeq;
	//	fm.submit();
	//}
	//var newWindow =OpenWindowNew("./LLClaimMainListPrintSave.jsp?ActuGetNo=" + ActuGetNo + "&RgtClass=0" + "&fmtransact=PRINT" ,"","left");
    var showStr="正在准备打印数据，请稍后...";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.target = "fraSubmit";
		fm.action = "../uw/PDFPrintSave.jsp?Code=lp008&OtherNo="+caseNo+"&StandbyFlag2="+ActuGetNo+"&StandbyFlag3=0";
		fm.submit();
}
// 调用公共的PDF打印
function printPDF()
{
	var selno = CheckGrid.getSelNo();
	var ActuGetNo="";
	var caseNo;
	if (selno <= 0)
	{
		alert("请选择要打印给付凭证的理赔案件");
		return ;
	}
	var caseNo = CheckGrid.getRowColData( selno-1, 2);
        strSQL="select ActuGetNo from LJAGet where OtherNo='"+caseNo+"'" +"and OtherNoType='5'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		ActuGetNo = arrResult[0][0];

	}
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp008' and standbyflag2='"+ActuGetNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		alert("提取数据失败-->PrtSeq为空");
		return false;
	}
	//window.open("../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq );
	fm.action="../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq;
	fm.submit();
}


//调查报告打印
function SurveyPrint()
{
	var selno = CheckGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选择要打印调查报告的理赔案件");
		return ;
	}

	var CaseNo = CheckGrid.getRowColData(selno - 1, 2);
	var newWindow = OpenWindowNew("SurveyReportPrt.jsp?CaseNo=" + CaseNo,"SurveyReportPrt","left" );
}

//补充材料打印
function AffixPrint()
{
	var selno = CheckGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选择要打印补充材料通知书的理赔案件");
		return ;
	}

	var CaseNo = CheckGrid.getRowColData(selno - 1, 2);


	var newWindow =OpenWindowNew("ShortAffixPrt.jsp?CaseNo=" + CaseNo,"AffixPrint","left" );

}


function afterCodeSelect( cCodeName, Field )
{
	//选择了处理
	if( cCodeName == "DealWith")
	{
		switch(fm.all('DealWith').value){
			case "0":
			DealConsult();             //咨询通知
      window.focus();              //咨询通知
			break;
			case "1":
			DealApp();                 //受理申请
      window.focus();              //咨询通知
			break;
			case "2":
			DealAppGrp();              //团体受理申请
      window.focus();              //咨询通知
			break;
			case "3":
			DealFeeInput();            //帐单录入
      window.focus();              //咨询通知
			break;
			case "4":
			DealCheck();               //检录
      window.focus();              //咨询通知
			break;
			case "5":
			DealClaim();               //理算
      window.focus();              //咨询通知
			break;
			case "6":
			DealApprove();             //审批审定
      window.focus();              //咨询通知
			break;
			case "7":
			DealCancel();							 //撤件
      window.focus();
      break;
      case "8":
      openSubmittedAffix();					//材料退回登记
      window.focus();
      break;
      case "9":
      openCaseTransfer();					//材料退回登记
      window.focus();  
      break;
      case "10":
      submitLLUnderWrit();					//TODO:理赔二核
      window.focus(); 
		}
	}
	if( cCodeName == "Printf"){
		switch(fm.all('Printf').value){
			case "0":
			ClaimGetPrint();         //给付通知书打印
      window.focus();              //咨询通知
			break;
			case "1":
			ClaimDetailPrint();      //给付明细表打印
      window.focus();              //咨询通知
			break;
			case "2":
			GPrint();                //给付凭证打印
      window.focus();              //咨询通知
			break;
			case "3":
			SurveyPrint();           //调查报告打印
      window.focus();              //咨询通知
			break;
			case "4":
			AffixPrint();            //补充材料打印
      window.focus();              //咨询通知
			break;
			case "5":
			ClaimDeclinePrint();            //补充材料打印
      window.focus();              //咨询通知
      break;
			case "6":
			AccPrint();            //万能账户对账单
      window.focus();              
		}

	}
	if( cCodeName == "Query"){
		switch(fm.all('Query').value){
			case "1":
			showInfo = window.open("./LLMainAskQueryInput.jsp" ,"","left");
			window.focus();       //咨询通知
			break;
			case "2":
			showInfo = window.open("./RegisterQuery.jsp" ,"","left");
			window.focus();       			 //理赔申请
			break;
			case "3":
			showInfo = window.open("./LLMainFeeQuery.jsp" ,"","left");
			window.focus();        		 //理赔帐单
			break;
			case "4":
			showInfo = window.open("./QueryDisease.jsp" ,"","left");
			window.focus();       				 //疾病记录
			break;
			case "5":
			showInfo = window.open("./ClaimQuery.jsp" ,"","left");
			window.focus();       		 //案件理算
			break;
			case "6":
			showInfo = window.open("./ClaimPayQuery.jsp" ,"","left");
			window.focus();        			 //理赔给付
			break;
			case "7":
			showInfo = window.open("./ClaimRecordQuery.jsp" ,"","left");
			window.focus();        	 //历史赔付
			break;
			case "8":
			QueryDealer();
			break;
		}
	}
}
function QueryDealer()
{
	var selno = CheckGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选择要查询的理赔案件!");
		return ;
	}
	var CaseNo = CheckGrid.getRowColData(selno - 1, 2);		//案件号
	var RgtState = CheckGrid.getRowColData(selno - 1, 11);	//案件状态	
	var strSQL="";
	switch (RgtState)
	{		
		case "扫描状态":
		case "检录状态":
		case "理算状态":
		case "审定状态":
		strSQL="select Handler,(select username from lduser where usercode=Handler) from llcase where caseno='"+CaseNo+"' with ur" ;//案件处理人
		break;
		case "延迟状态":
		case "受理状态":
		case "结案状态":
		case "通知状态":
		case "给付状态":
		case "撤件状态":
		strSQL="select Rigister,(select username from lduser where usercode=Rigister) from llcase where caseno='"+CaseNo+"' with ur" ;//案件受理人
		break;
		case "调查状态":
		strSQL="select case when inqper is null then Dipatcher else inqper end,(select username from lduser where usercode=case when inqper is null then Dipatcher else inqper end) from LLInqApply where otherno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;
		break;
		case "查讫状态":
		strSQL="select StartMan,(select username from lduser where usercode=StartMan) from llsurvey where otherno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;//案件处理人
		break;
		case "审批状态":
		case "抽检状态":
		strSQL="select AppClmUWer,(select username from lduser where usercode=AppClmUWer) from LLClaimUWMain where caseno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;//案件处理人
		break;		
	}

	var arrResult = easyExecSql(strSQL);	
	if(arrResult)
	{
		alert("案件当前状态："+RgtState+";待处理人："+arrResult[0][0]+"("+arrResult[0][1]+")");		
	}	
}
function ShowInfoPage()
{
	var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = CheckGrid.getRowColData(selno-1,2);
		var typeRadio="";
	for(i = 0; i <fm.typeRadio.length; i++)
	{
		if(fm.typeRadio[i].checked)
		{
			typeRadio=fm.typeRadio[i].value;
			break;
		}
	}


	if(typeRadio=='0'||typeRadio=='1')
		{

		OpenWindowNew("./LLMainAskInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");

		}else
		{
	OpenWindowNew("./ClaimUnderwriteInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
		}



}

//保单查询
function ContInfoPage()
{
		var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = CheckGrid.getRowColData(selno-1,2);
		var ContNo = easyExecSql("select distinct max(grpcontno) from llclaimpolicy where caseno = '"+CaseNo+"'");
		if(ContNo=='00000000000000000000')
		{
			ContNo = easyExecSql("select distinct max(contno) from llclaimpolicy where caseno = '"+CaseNo+"'");
		  	window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );
			}else
				{
					window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );
	      }

}

function openSubmittedAffix()

{
	 var caseno = '';
   var selno = CheckGrid.getSelNo()-1;
   if (selno >=0)
  {
    caseno = CheckGrid.getRowColData(selno,2);
  }
  strSQL="select rgtno,rgtstate from llcase where caseno='"+caseno+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
    RgtState=arrResult[0][1];
  }
  if(RgtState=="09"||RgtState=="12")
  {
	  //showInfo = window.open("./SubmittedAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value);
	  pathStr="./SubmittedAffixMain.jsp?CaseNo="+caseno+"&RgtNo="+RgtNo;
	  showInfo = OpenWindowNew(pathStr,"","middle");
	}else
		{
			alert("未结案，不能回退");
		}
}
function openCaseTransfer(){
	if (fm.RgtNo.value=="" && fm.CustomerName.value=="" && fm.CustomerNo.value=="" && fm.CaseNo.value=="") {
		var startDate = fm.RgtDateS.value;
		var endDate = fm.RgtDateE.value;
		if (startDate==""||startDate==null||endDate==""||endDate==null){
			alert("请输入受理日期");
			return false;
		}
	}
	
var typeRadio="";
  for(i = 0; i <fm.typeRadio.length; i++){
    if(fm.typeRadio[i].checked){
      typeRadio=fm.typeRadio[i].value;
      break;
    }
  }
	var rgtstate="";
  for(i = 0; i <fm.RgtState.length; i++){
    if(fm.RgtState[i].checked){
      rgtstate=fm.RgtState[i].value;
      break;
    }
  }
    var StartDate=""
  var EndDate=""
  StartDate = fm.RgtDateS.value;
  EndDate = fm.RgtDateE.value;
	var handler = fm.Operator.value;
	var tManagecom = fm.ManageCom.value;
	
	var RgtNo=fm.RgtNo.value;
	var CustomerName=fm.CustomerName.value;
	var CustomerNo=fm.CustomerNo.value;
	var CaseNo=fm.CaseNo.value;
	var ContType=fm.ContType.value;
  var varSrc="&Handler="+ handler+"&ManageCom="+tManagecom;
		varSrc=varSrc+"&RgtState="+rgtstate;
		varSrc=varSrc+"&EndDate="+EndDate;
		varSrc=varSrc+"&StartDate="+StartDate;
		varSrc=varSrc+"&RgtNo="+RgtNo;
		varSrc=varSrc+"&CustomerName="+CustomerName;
		varSrc=varSrc+"&CustomerNo="+CustomerNo;
		varSrc=varSrc+"&CaseNo="+CaseNo;
		varSrc=varSrc+"&ContType="+ContType;
		
 showInfo = window.open("./LLCaseTransferMain.jsp?Interface=LLCaseTransferClaimer.jsp"+varSrc,"转交",'width=1000,height=650,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
   // showDiv(inputButton,"false");
  }
}

//账户对账单
function AccPrint()
{

	var selno = CheckGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选择要打印账户对账单的理赔案件");
		return ;
	}

	var CaseNo = CheckGrid.getRowColData(selno - 1, 2);


	var sql = "select a.polno,a.insuaccno,a.contno From lcinsureacc a,lcinsureacctrace b where "
          + " a.contno=b.contno and a.insuaccno=b.insuaccno "
          + " and a.grpcontno='00000000000000000000' and "
          + " otherno='"+CaseNo+"' with ur";
  var rs = easyExecSql(sql);
  if(rs == null || rs[0][0] == null || rs[0][0] == "")
  {
    alert("没有查询到账户信息");
    return false;
  }
  
  var tPolNo = rs[0][0];
  var tInsuAccNo = rs[0][1];
  var tContNo = rs[0][2];
  
  fm.action = "../bq/BalanceListPrint.jsp?ContNo=" + tContNo + "&PolNo=" + tPolNo + "&InsuAccNo=" + tInsuAccNo;
	fm.target = "_blank";
	fm.submit();
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}