 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
<%
//程序名称：OutsourcingCostInput.jsp
//程序功能：外包费用录入界面
//创建日期：2013-12-23
//创建人  ：LiuJian
//更新记录：更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="SocialSecurityFeeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="SocialSecurityFeeInit.jsp"%>
  <title>外包错误数据</title>  
</head>
<body  onload="initForm();initElementtype();"  >
  <form action="SocialSecurityFeeSave.jsp" method=post name=fm target="fraSubmit">
    <table class="common">
  		
      	<tr CLASS="common">
      	<td CLASS="title">调查结案时间起期</td>
  		<td CLASS="input" COLSPAN="1">
 			<input class="coolDatePicker" verify="结算开始日期|date&notnull" style="width:120" name=RgtDateS onkeydown="QueryOnKeyDown()" elementtype=nacessary>
      	</td> 
  		<td CLASS="title">调查结案时间止期</td>
  		<td CLASS="input" COLSPAN="1">
  			  <input class="coolDatePicker" verify="结算结束日期|date&notnull" style="width:120" name=RgtDateE onkeydown="QueryOnKeyDown()" elementtype=nacessary>
      	</td>
      	 </tr>
      	<tr CLASS="common">
  			<td CLASS="title">参保地机构</td>
  			<td CLASS="input" COLSPAN="1">
  			  <Input class= "codeno" name="cbdUnitIdT" style="width:50" verify="参保地机构|string&notnull" ondblclick="return showCodeList('ssfmng',[this,cbdUnitNameT],[0,1],null,null,null,1);"
  			   onkeyup="return showCodeListKey('ssfmng',[this,cbdUnitNameT],[0,1],null,null,null,1);"  ><Input class=codename style="width:100" name="cbdUnitNameT"  elementtype=nacessary>
      		</td> 
  			<td CLASS="title">(异地)调查机构</td>
  			<td CLASS="input" COLSPAN="1">
  			  <Input class= "codeno" name="ydUnitIdT" style="width:50" verify="(异地)调查机构|string&notnull" ondblclick="return showCodeList('ssfmng',[this,ydUnitNameT],[0,1],null,null,null,1);"
  			   onkeyup="return showCodeListKey('ssfmng',[this,ydUnitNameT],[0,1],null,null,null,1);"  ><Input class=codename style="width:100" name="ydUnitNameT" elementtype=nacessary>
      		</td> 		
  		</tr>
  		<tr CLASS="common">
  			  <td CLASS="title">患者</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="nameT" CLASS="common" maxlength="19" >
      	</td>  
  			<td CLASS="title">医保编号</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="medicareCodeT" CLASS="common" maxlength="19" >
      	</td>  
  		</tr>
  	  		<tr CLASS="common">
  			  <td CLASS="title">身份证号码</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="idNoT" CLASS="common" maxlength="19" >
      	</td>  
  		</tr>
  		<!--  <tr>
					 <TD  class= title8 colspan=6>
							<input type="radio" name="typeRadio"  value="0" onclick="queryResult(0);" checked>未结算 
							<input type="radio" name="typeRadio"  value="1" onclick="queryResult(1);">已结算
					</td>
          </tr>-->
  		
    </table>
    <INPUT VALUE="查  询" class =cssButton TYPE=button onclick="dealInfo(0);">
    <INPUT VALUE="结  算" class =cssButton TYPE=button onclick="dealInfo(1);">
    <input type="hidden" name="operate" >
    <input type="hidden" name="queryMode" value="0">
    
  <br></br>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			 社保通调查费查询
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''" align=center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
    </Div>
  	<Div id= "divPage2" align=center style= "display: '' ">
      <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage.firstPage(); "> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage(); "> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage(); "> 
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage.lastPage();"> 
    </Div>
   
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>