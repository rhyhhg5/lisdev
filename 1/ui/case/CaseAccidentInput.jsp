<html>
<%
/*******************************************************************************
 * Name     :CaseAccidentInput.jsp
 * Function :立案－事故/伤残信息的主界面
 * company  :sinosoft
 * Author   :LiuYansong
 * Date     :2003-7-15
 */
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="CaseAccidentInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CaseAccidentInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./CaseAccidentSave.jsp" method=post name=fm target="fraSubmit">


  

    <!--收据明细费用信息部分（列表） -->
 

  <Div id = "divDiseaseInfo" style = "display: ''">
    <Table>
      <TR>
        <TD>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisease);">
        </TD>
        <TD class=titleImg>
          疾病信息
        </TD>
      </TR>
    </Table>

    <Div id = "divDisease" style = "display: ''">

      <Table class= common>
        <TR class= common>
          <TD text-align: left colSpan=1>
            <span id="spanDiseaseGrid" >
            </span>
          </TD>
        </TR>
      </Table>
    </Div>
  </Div>

  <Div id = "divSeDiseaseInfo" style = "display: ''">
    <Table>
      <TR>
        <TD>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSeDisease);">
        </TD>
        <TD class=titleImg>
          重大疾病责任信息
        </TD>
      </TR>
    </Table>

    <Div id = "divSeDisease" style = "display: ''">

      <Table class= common>
        <TR class= common>
          <TD text-align: left colSpan=1>
            <span id="spanSeriousDiseaseGrid" >
            </span>
          </TD>
        </TR>
      </Table>
    </Div>
    
  </Div>
  
  
  
    
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseCure2);">
    		</td>
    		<td class= titleImg>
    			 手术信息
    		</td>
    	</tr>
    </table>

  <Div  id= "divCaseCure2" style= "display: ''">
    <table  class= common>
    	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanDegreeOperationGrid" >
					</span>
				</td>
			</tr>
		</table>
</div>

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,div2);">
    		</td>
    		<td class= titleImg>
    			 意外信息
    		</td>
    	</tr>
    </table>

  <Div  id= "div2" style= "display: ''">
    <table  class= common>
    	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanAccidentGrid" >
					</span>
				</td>
			</tr>
		</table>
</div>
<Div id = "divDeformityInfo" style = "display: ''">
    <Table>
      <TR>
        <TD>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDeformity);">
        </TD>
        <TD class=titleImg>
          残疾信息
        </TD>

      </TR>
    </Table>

    <Div id = "divDeformity" style = "display: ''">
      <Table class= common>
        <TR class= common>
          <TD text-align: left colSpan=1>
            <span id="spanDeformityGrid" >
            </span>
          </TD>
        </TR>
      </Table>

    </Div>
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,div1);">
    		</td>
    		<td class= titleImg>
    			 其他录入要素
    		</td>
    	</tr>
    </table>

  <Div  id= "div1" style= "display: ''">
    <table  class= common>
    	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanOtherFactorGrid" >
					</span>
				</td>
			</tr>
		</table>
</div>

  </Div>

         <INPUT VALUE="保存" class=cssButton TYPE=button onclick="submitForm();">
         <INPUT VALUE="返回" class=cssButton TYPE=button onclick="top.close();">
          <INPUT  type= hidden name="CaseNo">
          <INPUT  type= hidden name="CaseRelaNo">
          <INPUT  type= hidden name="RgtNo">
          <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
 
</body>


</html>
