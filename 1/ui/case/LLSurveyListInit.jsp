<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initCheckGrid();
    initSurveyerGrid();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="调查号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="调查类别";
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="调查类型";
    iArray[3][1]="100px";
    iArray[3][2]=60;
    iArray[3][3]=0;
    

    iArray[4]=new Array("提起日期","100px","0","0");;
    iArray[5]=new Array("调查地点","100px","0","0");
    iArray[6]=new Array("调查事件号","100px","0","0");;
    iArray[7]=new Array("调查类别代码","80px","0","3");;
    iArray[8]=new Array("调查类型代码","80px","0","3");;


    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =2;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canSel =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
function initSurveyerGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="调查员代码";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="调查员名称";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="在调查案件";
    iArray[3][1]="80px";
    iArray[3][2]=60;
    iArray[3][3]=0;
  



   iArray[4] = new Array("机构编码","80px","0","0");
  
   
    SurveyerGrid = new MulLineEnter("fm","SurveyerGrid");
    SurveyerGrid.mulLineCount = 2;
    SurveyerGrid.displayTitle = 1;
    SurveyerGrid.locked = 1;
  // SurveyerGrid.canChk =1	;
  //  SurveyerGrid.canSel =1	;
    SurveyerGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SurveyerGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    SurveyerGrid.loadMulLine(iArray);
   // SurveyerGrid.selBoxEventFuncName = "ShowRela";
  }
  catch(ex)
  {
    alter(ex);
  }
}
 </script>