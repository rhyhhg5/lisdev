<html>
	<%
	//Name：LLMainAskInput2.jsp
	//Function：登记界面的初始化
	//Date：2004-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.llcase.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLMainAskInput2.js"></SCRIPT>
		<%@include file="LLMainAskInputInit2.jsp"%>

	</head>

	<body  onload="initForm();initElementtype();" >
		<form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
			<%@include file="ConsultTitle2.jsp" %>
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						客户信息
					</TD>
				</TR>
			</table>

			<Div  id= "divLLLLMainAskInput1" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>客户姓名</TD><TD  class= input8><input readOnly class= common name="LogName" ></TD>
						<TD  class= title8>客户号码</TD><TD  class= input8><input readOnly class= common name="LogerNo"  ></TD>
						<TD  class= title8>证件号码</TD><TD  class= input8><input readOnly class= common name="IDNo"  ></TD>
						</TR><TR  class= common8 style="display:'none'">


							<TD  class= title8>电话号码</TD><TD  class= input8><input readOnly class= common name="Phone"  ></TD>
							<TD  class= title8>手机号码</TD><TD  class= input8><input readOnly class= common name="Mobile" ></TD>
							<TD  class= title8>电子邮箱</TD><TD  class= input8><input readOnly class= common name="Email" ></TD>
							</TR><TR  class= common8 style=" display:'none'">
								<TD  class= title8>单位名称</TD><TD  class= input8><input readOnly class= common name="LogComp" ></TD>
								<TD  class= title8>通讯地址</TD><TD  class= input8><input readOnly class= common name="AskAddress"  ></TD>
								<TD  class= title8>回复方式</TD><TD  class= input8><input readOnly class="codeno" name="AnswerMode" ><input readOnly class= codename name=AnswerModeName ></TD>
							
							</TR><TR  class= common8>
	 							<TD  class= title8>登记方式</TD><TD  class= input8><input readOnly class=codeno name="AskMode"  ><input readOnly class= codename name="AskModeName" ></TD>
								<TD  class= title8>客户现状</TD><TD  class= input8><input readOnly class="codeno" name="CustStatus" ><input readOnly class= codename name="CustStatusName"></TD>
								<TD  class= title8>出险类别</TD><TD  class= input8><input readOnly class= codeno  name="AccType" ><input readOnly class= codename name="AccTypeName"></TD>
								</TR>
							</table>
						</DIV>
						<div id="DivCusInfo" style="display:''">
							<table>
								<TR>
									<TD>
										<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,ConsultInfo);">
									</TD>
									<TD class= titleImg>
										信息
									</TD>
								</TR>
							</table>
							<div id="ConsultInfo1" style="display:''">
								<table  class= common>

									<TR  class= common>
										<TD  class= title colspan="6">信息内容</TD>
									</TR>
									<TR  class= common>
										<TD  class= input colspan="6">
											<textarea readOnly name="CContent" cols="100%" rows="3" witdh=25% class="common"></textarea>
										</TD>
									</TR>
								</table>
							</div>
						</div>


					</DIV>


					<div id="ConsultInfo2" style="display:''">
						<table class=common>
							<TR  class= common>
								<TD  class= title colspan="6">回复内容</TD>
							</TR>
							<TR  class= common>
								<TD  class= input colspan="6">
									<textarea readOnly name="Answer" cols="100%" rows="3" witdh=25% class="common"></textarea>
								</TD>
							</TR>
							<TR  class= common8>
							<Input type=hidden class= common name="ReplyState"></TD>
						</TR>
					</table>
				</div>

				<div id="divCommendHospital" style="display:'none'">
					<Table>
						<TR class=common>
							<TD class=common>
								<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospiInfo);">
							</TD>
							<TD class= titleImg>
								就诊医院
							</TD>
						</TR>
					</Table>
					<Div  id= "divHospiInfo" style= "display: ''">
						<table  class= common>
							<TR  class= common>
								<TD text-align: left colSpan=1>
									<span id="spanCommHospitalGrid">
									</span>
								</TD>
							</TR>
						</table>
					</Div>
				</div>


				<div id="divEventInfo" style="display:''">
					<Table>
						<TR class=common>
							<TD class=common>
								<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
							</TD>
							<TD class= titleImg>
								关联事件
							</TD>
						</TR>
					</Table>
					<Table class=common>
					</tr>
					<TD  class= title8>起始日期</TD>
					<TD  class= input8> <input readOnly class="coolDatePicker"  dateFormat="short"  name=CDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>终止日期</TD>
					<TD  class= input8 > <input readOnly class="coolDatePicker"  dateFormat="short"  name=CDateE onkeydown="QueryOnKeyDown()"></TD>
				</TR>
			</Table>
			<Div  id= "divSecondUWInfo" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanSubReportGrid">
							</span>
						</TD>
					</TR>
				</table>
			</div>
		</Div>
		<Div  id= "divLLLLEventInput1" style= "display: 'none'">
			
				<div align='right'>
					<input readOnly class=cssButton type=button value="添加事件" onclick="EventSave()">
				</div>



			</DIV>
			<div id="dvi1" style="display: 'none'">
				<TD  class= title8>登记类型</TD><TD  class= input8><input readOnly class= code8 name="AskType"  elementtype=nacessary verify="登陆类型|len<=1"></TD>
			</div>
			<hr>
			<div id="divconfirm" align='right'>
				<input  class=cssButton type=button value="确 认" onclick="submitForm()">
				<input  class=cssButton type=button value="提起调查" onclick="submitFormSurvery()">
				<input  class=cssButton type=button value="打印咨询回执" onclick="printPage()">
			</div>
			<!--隐藏域-->
			<input name ="fmtransact" type="hidden">
			<input name ="LogNo" type="hidden">
			<input name ="ConsultNo1" type="hidden">
			<input name ="CNNo" type="hidden">
			<input name = "PostCode" type="hidden">
			<input name ="NoticeNo" type="hidden">
			<input name ="SaveFlag" value="0" type="hidden">
			<input name ="LoadC" type="hidden">
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>