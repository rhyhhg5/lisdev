<%
//程序名称：CaseReceiptInput.jsp
//程序功能：
//创建日期：2002-07-21 20:09:20
//创建人  ：CrtHtml程序创建
//更新记录：
// 更新人:刘岩松    
//更新日期：2002-09-24
//更新原因/内容：将"费用项目名称"的列宽该成＂0＂
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
var turnPage = new turnPageClass();
function initInpBox()
{ 
  try
  {}
  catch(ex)
  {
    alert("在CaseReceiptInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在CaseReceiptInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initCaseReceiptGrid();
    showCaseReceiptInfo();
  }
  catch(re)
  {
    alert("CaseReceiptInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//收据费用明细
function initCaseReceiptGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[1]=new Array();
      iArray[1][0]="医院代码";         			//列名
      iArray[1][1]="0px";            		//列宽
      iArray[1][2]=0;            			//列最大值
      iArray[1][3]=0;   
      iArray[1][4]="HospitalCode";
 			iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
 			iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
 			iArray[1][9]="医院代码|NOTNULL";
   

      iArray[2]=new Array();
      iArray[2][0]="医院名称";         			//列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=160;            			//列最大值
      iArray[2][3]=0;      

      iArray[3]=new Array();
      iArray[3][0]="收据号";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;      
      
      iArray[4]=new Array();
      iArray[4][0]="费用项目类型";    	//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;
      iArray[4][4]="FeeItemCode";
      iArray[4][5]="4|5";
      iArray[4][6]="0|1";              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="费用项目代码";         			//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="申请赔付金额";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[7]=new Array();
      iArray[7][0]="实际赔付金额";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

			iArray[8]=new Array();
      iArray[8][0]="缴费日期";         			//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=60;            			//列最大值
      iArray[8][3]=0;      

      CaseReceiptGrid = new MulLineEnter( "fm" , "CaseReceiptGrid" ); 
      //这些属性必须在loadMulLine前
      CaseReceiptGrid.mulLineCount = 1;   
      CaseReceiptGrid.displayTitle = 1;
      CaseReceiptGrid.locked = 1;
      CaseReceiptGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //CaseReceiptGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        //alert(ex);
        alert("在此出错");
      }
  }

</script>