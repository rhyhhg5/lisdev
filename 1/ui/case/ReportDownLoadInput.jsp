<html>
	<%
	      /*=============================================
		  Name：LLRptDownLoadInput.jsp
		  Function： 打印报表下载
		  Date：2010-8-3
		  Author：ZhangXiaolei
		 =============================================*/
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="./ReportDownLoad.js"></SCRIPT>
		<%@include file="./ReportDownLoadInit.jsp"%>
	
	<script language="javascript">
	    
    </script>
	</head>	
	<body onload="initForm();">
		<form action="./" method=post name="fm" target="fraSubmit">
			 <table class=common>
      			<tr>
      				<TD class=titleImg>
						查询条件：
					</TD>
    			</tr>
    			<tr  class= common>
                	<td  class= title>统计机构</TD>
          			<td  class= input> <Input class="codeno" name=ManageCom  elementtype=nacessary   verify="统计机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class="codename" name=ManageComName elementtype=nacessary ><font size=1 color="#ff0000"><b>*</b></font> </td>          
                	<td  class= title>用户名</TD>
    				<td  class= input> <Input class="codeno" name=UserCode elementtype=nacessary  verify="用户名|NOTNULL" ondblclick="return showCodeList('cjusercode',[this,UserName],[0,1],null,fm.ManageCom.value,'1');" onkeyup="return showCodeListKey('cjusercode',[this,UserName],[0,1],null,fm.ManageCom.value,'1');" ><input class="codename" name=UserName elementtype=nacessary  ><font size=1 color="#ff0000"><b>*</b></font>  </td>          			
    			</tr>
    			<tr  class= common>
                	<td  class= title>报表名称</TD>
          			<td  class= input> <Input class="codeno" name=ReportNo  elementtype=nacessary   verify="报表名称|code:rptname" ondblclick="return showCodeList('cjreportname',[this,ReportName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('cjreportname',[this,ReportName],[0,1],null,null,null,1);"><input class="codename" name=ReportName elementtype=nacessary ></td>          
                	<td  class= title>打印起期</TD>
          			<TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起期|notnull&Date" elementtype=nacessary><font size=1 color="#ff0000"><b>*</b></font> </TD> 
    			</tr>
    			<tr  class= common>
    				<TD  class= title>打印止期</TD>
                    <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="结案止期|notnull&Date" elementtype=nacessary><font size=1 color="#ff0000"><b>*</b></font>  </TD> 
    			</tr>
    		</table>
    		<table class=common>
    			<tr class=common>
          			<TD  class= title><INPUT VALUE="查 询" class="cssButton" TYPE="button" onclick="query()"></TD>
				</tr>
			</table>
    		<table>
				<TR>
					<TD>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	onClick="showPage(this,divCustomerInfo);">
					</TD>
					<TD class=titleImg>
						报表信息列表
					</TD>
				</TR>
			</table>
			<Div id="divCustomerInfo" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD text-align: left colSpan=1>
							<span id="spanReportMetaData"> </span>
						</TD>
					</TR>
				</table>
				<Div id="divPage" align=center style="display: 'none' ">
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button
						onclick="turnPage.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button
						onclick="turnPage.lastPage();">
				</Div>
				
				<INPUT class="cssbutton" style='width:80' TYPE=button VALUE="报表下载" onclick="downLoadFile()">
			</Div>
			<Input type="hidden" class=common name="operator">
			
			<Input type="hidden" class=common name="remark2" value='CJ'>
			
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
