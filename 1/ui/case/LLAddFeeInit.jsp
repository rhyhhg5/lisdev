<%
//程序名称：LLAddFeeInit.jsp
//程序功能：保单加�
//创建日期：
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%

%>

<script language="JavaScript">
	function initInpBox()
	{
		try
		{
			var LoadFlag = '<%=request.getParameter("LoadFlag")%>';
			fm.LoadFlag.value=LoadFlag;
			try
			{
				divRgt.style.display='none';
				divCase.style.display='';
				fm.RgtNo.value = "";
				fm.GrpNo.value = "";
				fm.GrpName.value ="";
				fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
				fm.CaseNo.value = '<%= request.getParameter("ContNo")%>';
				if(fm.CaseNo.value=="null")
				fm.CaseNo.value="";
				fm.ContNo.value = '<%= request.getParameter("ContNo") %>';
				fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
				fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';

			}
			catch(ex)
			{
				alert(ex);
			}
			<%GlobalInput mG = new GlobalInput();
			mG=(GlobalInput)session.getValue("GI");
			%>
		}
		catch(ex)
		{
			alert("在LLRgtSurveyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initForm()
	{
		try
		{
			initInpBox();
			initRiskGrid();
			initAddFeeGrid();
			queryRiskInfo();
		}
		catch(re)
		{
			alert("在LLAddFeeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	// 保单信息列表的初始化
	function initRiskGrid() {
		var iArray = new Array();

		try {
			iArray[0]=new Array();
			iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1]="0px";            		//列宽
			iArray[0][2]=30;            			//列最大值
			iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

			iArray[1]=new Array();
			iArray[1][0]="保单险种号码";         	//列名
			iArray[1][1]="80px";            		//列宽
			iArray[1][2]=100;            			//列最大值
			iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();
			iArray[2][0]="主险保单号码";         		//列名
			iArray[2][1]="0px";            		//列宽
			iArray[2][2]=100;            			//列最大值
			iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

			iArray[3]=new Array();
			iArray[3][0]="险种编码";         		//列名
			iArray[3][1]="50px";            		//列宽
			iArray[3][2]=100;            			//列最大值
			iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[4]=new Array();
			iArray[4][0]="险种名称";         		//列名
			iArray[4][1]="210px";            		//列宽
			iArray[4][2]=100;            			//列最大值
			iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[5]=new Array();
			iArray[5][0]="保费";         		    //列名
			iArray[5][1]="60px";            		//列宽
			iArray[5][2]=100;            			//列最大值
			iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[6]=new Array();
			iArray[6][0]="风险保费";         		    //列名
			iArray[6][1]="50px";            		//列宽
			iArray[6][2]=100;            			//列最大值
			iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许

			iArray[7]=new Array();
			iArray[7][0]="保额";         		    //列名
			iArray[7][1]="70px";            		//列宽
			iArray[7][2]=100;            			//列最大值
			iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[8]=new Array();
			iArray[8][0]="保险起期";         		//列名
			iArray[8][1]="80px";            		//列宽
			iArray[8][2]=100;            			//列最大值
			iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[9]=new Array();
			iArray[9][0]="保险止期";         		//列名
			iArray[9][1]="80px";            		//列宽
			iArray[9][2]=100;            			//列最大值
			iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许


			iArray[10]=new Array();
			iArray[10][0]="交费间隔(月)";         	//列名
			iArray[10][1]="50px";            		//列宽
			iArray[10][2]=100;            			//列最大值
			iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许

			iArray[11]=new Array();
			iArray[11][0]="交费年期";         	//列名
			iArray[11][1]="60px";            	//列宽
			iArray[11][2]=100;            		//列最大值
			iArray[11][3]=0;              		//是否允许输入,1表示允许，0表示不允许

			RiskGrid = new MulLineEnter( "fm" , "RiskGrid" );
			//这些属性必须在loadMulLine前
			RiskGrid.mulLineCount = 3;
			RiskGrid.displayTitle = 1;
			RiskGrid.locked = 1;
			RiskGrid.canSel = 1;
			RiskGrid.hiddenPlus = 1;
			RiskGrid.hiddenSubtraction = 1;
			RiskGrid.loadMulLine(iArray);

			RiskGrid. selBoxEventFuncName = "showRiskResult";
			} catch(ex) {
				alert(ex);
			}
		}

		function initAddFeeGrid() {
			var iArray = new Array();

			try {
				iArray[0]=new Array();
				iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
				iArray[0][1]="30px";            		//列宽
				iArray[0][2]=30;            			//列最大值
				iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

				iArray[1]=new Array();
				iArray[1][0]="费用值";         	//列名
				iArray[1][1]="100px";            		//列宽
				iArray[1][2]=100;            			//列最大值
				iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

				iArray[2]=new Array();
				iArray[2][0]="费用比例";         	//列名
				iArray[2][1]="100px";            		//列宽
				iArray[2][2]=100;            			//列最大值
				iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

				iArray[3]=new Array();
				iArray[3][0]="加费起始日期";         		//列名
				iArray[3][1]="120px";            		//列宽
				iArray[3][2]=100;            			//列最大值
				iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

				iArray[4]=new Array();
				iArray[4][0]="加费终止日期";         		//列名
				iArray[4][1]="120px";            		//列宽
				iArray[4][2]=100;            			//列最大值
				iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许


				AddFeeGrid = new MulLineEnter( "fm" , "AddFeeGrid" );
				//这些属性必须在loadMulLine前
				AddFeeGrid.mulLineCount = 1;
				AddFeeGrid.displayTitle = 1;
				AddFeeGrid.locked = 0;
				AddFeeGrid.canSel = 0;
				AddFeeGrid.hiddenPlus = 1;
				AddFeeGrid.hiddenSubtraction = 1;
				AddFeeGrid.addEventFuncName="checkAddfee";
				AddFeeGrid.loadMulLine(iArray);

				//RiskGrid. selBoxEventFuncName = "showRiskResult";
				} catch(ex) {
					alert(ex);
				}
			}

		</script>