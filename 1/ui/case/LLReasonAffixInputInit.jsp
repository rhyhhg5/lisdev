<%
//Name:LLReasonAffixInputInit.jsp
//function：申请材料配置
//author:Applewood/Helga
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GB2312" %>
<%
%>
<script language="JavaScript">
	function initInpBox(){
		try{

		}
		catch(ex){
			alter("在LLReasonAffixInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initSelBox(){
		try{
		}
		catch(ex){
			alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
		}
	}
	function initForm(){
		try{
			// initInpBox();
			initAffixBaseGrid();
			initAffixGrid();
			fm.DelButton.style.display='none';		
			//easyQuery();
		}
		catch(re){
			alter("在LLReasonAffixInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	//立案附件信息
	function initAffixGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array("序号","30px","0",1);
			iArray[1]=new Array("材料代码","50px","10",2);
			iArray[1][4]="llmaffix"
			iArray[1][5]="1|2";
			iArray[1][6]="0|1";

			iArray[2]=new Array("材料名称","180px","20",0);
			//	      iArray[4]=new Array("提供日期","100px","20",1);

			AffixGrid = new MulLineEnter( "fm" , "AffixGrid" );
			AffixGrid.mulLineCount = 0;
			AffixGrid.displayTitle = 1;
			AffixGrid.canChk = 0;
			AffixGrid.hiddenPlus=0;
			AffixGrid.hiddenSubtraction=0;
			AffixGrid.loadMulLine(iArray);
		}
		catch(ex){
			alert(ex);
		}
	}

	//立案附件信息
	function initAffixBaseGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array("序号","30px","0",1);
			iArray[1]=new Array("材料类型代码","50px","10",1);
			iArray[1][9]="材料类型代码|num&&len<=6|notnull";
			iArray[2]=new Array("材料类型名称","180px","20",1);
			iArray[2][9]="材料类型名称|notnull";
			iArray[3]=new Array("材料代码","50px","10",1);
			iArray[3][9]="材料代码|num&&len<=6|notnull";
			iArray[4]=new Array("材料名称","180px","20",1);
			iArray[4][9]="材料名称|notnull";

			AffixBaseGrid = new MulLineEnter( "fm" , "AffixBaseGrid" );
			AffixBaseGrid.mulLineCount = 0;
			AffixBaseGrid.displayTitle = 1;
			AffixBaseGrid.canChk = 1;
			AffixBaseGrid.hiddenPlus=0;
			AffixBaseGrid.hiddenSubtraction=0;
			AffixBaseGrid.loadMulLine(iArray);
		}
		catch(ex){
			alert(ex);
		}
	}

	function getCon(){
		//return " and affixtypecode=#"+fm.all('codetype').value+"#";
		alert(fm.all("codetype").value);
		return "#"+fm.all('codetype').value+"#"
	}

</script>