//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//查询函数
function EasyQuery()
{
		var sql = " select a.RiskName, a.RiskCode, "
						+ " CASE WHEN a.RiskCode like '1%%' THEN '健康险' ELSE '意外险' END ,"
						+ " b.startdate,b.enddate ,"
						+ " (select count(c.RiskCode) from lcpol c where c.RiskCode = a.RiskCode )"
						+ " from lmrisk a, lmriskapp b where a.RiskCode = b.RiskCode  order by RiskCode"
						;
		turnPage.pageLineNum = 20;
		turnPage.queryModal(sql,PolGrid);
}
function ContQuery()
{
	var sql =  " select distinct grpcontno, grpname, prtno, cvalidate from lcgrpcont where grpcontno <> '00000000000000000000' "
						+getWherePart("grpcontno","ContNo")
						+getWherePart("prtno","PrtNo")
						+ " and grpname like '%%"+fm.GrpName.value+"%%'"
						+ "order by  cvalidate desc with ur"; 
	turnPage1.pageLineNum = 20;
	turnPage1.queryModal(sql,ContGrid);
}
function ShowScan()
{
	var selno = ContGrid.getSelNo();
	if(selno < 0 ){alert("请先选择一张保单");return;}
	var tPrtNo = ContGrid.getRowColData(selno-1,3);
	window.open("../sys/ProposalEasyScan.jsp?prtNo="+ tPrtNo + "&SubType=TB1002&BussType=TB&BussNoType=12" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");					
}


function showTKPDF(a)
{
	var RiskCode = fm.all(a).all('PolGrid2').value;
//	var flag = fm.all(a).all('RiskStatementGrid3').value;
//	if(flag=='有'){
  var PdfURL="../ppdf/t"+RiskCode+".pdf";
 		var newWindow = OpenWindowNew(PdfURL,"审核","left");
// 	}
// 	else{
// 	  alert("该产品的条款尚未导入");
// 	}
}