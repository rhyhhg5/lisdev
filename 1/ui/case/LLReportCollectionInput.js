//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作


function submitForm()
{
  if( verifyInput2() == false ) return false;
  
  if(!isInteger(fm.Year.value))
  {
  	alert("输入的年份不对!");
  	return false;
  }
  if(!isInteger(fm.Month.value))
  {
  	alert("输入的月份不对!");
  	return false;
  }
  if(parseInt(fm.Month.value)<1||parseInt(fm.Month.value)>12)
  {
  	alert("输入的月份不对!");
  	return false;
  }
  if(parseInt(fm.Year.value)>2999||parseInt(fm.Year.value)<2000){
    alert("请录入2000到2999间的数字！");
    return false;
  }
  year=fm.Year.value;
  if(fm.ReportType.value!="1"&&fm.SpanType.value=="2")
  {
    fm.MStartDate.value="";
    fm.MEndDate.value="";
  }
  else
    {
      month=fm.Month.value;
      days=getDays(month, year);
      Ndays=getDays(month, year-1);

      fm.MStartDate.value=year+"-"+month+"-"+"01";
      fm.MEndDate.value=year+"-"+month+"-"+days;
     fm.Days.value=Ndays;
    }
    fm.YStartDate.value=year+"-"+"01"+"-"+"01";
    fm.YEndDate.value=year+"-"+month+"-"+days;
    if (verifyInput() == false)
    return false;
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    //showSubmitFrame(mDebug);
    //fm.fmtransact.value = "PRINT";
    switch (fm.ReportType.value){
      case "1":
      fm.target = "CollectionPrt";
      break;
      case "4":
      fm.target = "RsuCompPrt";
      break;
      case "5":
      fm.target = "WorkCompPrt";
      break;
    }
      fm.all('op').value = '';
      fm.submit();
      showInfo.close();
    }

    function download()
    {
      if (verifyInput() == false)
      return false;
      var i = 0;
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //showSubmitFrame(mDebug);
      //fm.fmtransact.value = "PRINT";
      fm.target = "f1print";
      fm.all('op').value = 'download';
      fm.submit();
      showInfo.close();
    }

    function getDays(month, year)
    {
      var daysInMonth = new Array(0,31, 28, 31, 30, 31, 30, 31, 31,30, 31, 30, 31);
      if (2 == month)
      return ((0 == year % 4) && (0 != (year % 100))) ||(0 == year % 400) ? 29 : 28;
      else
        return daysInMonth[month];
      }

      function CollectionPrt()
      {
        if(fm.Month.value=="")
        {
          alert("统计月份不能为空");
          return;
        }
        fm.ReportType.value="1";
        submitForm();
      }

      function RsuCompPrt()
      {
        if(fm.SpanType.value=="1" && fm.Month.value=="")
        {
          alert("统计月份不能为空");
          return;
        }
        if(fm.SpanType.value=="")
        {
          alert("请选择统计类型!");
          return;
        }
        fm.ReportType.value="4";
        submitForm();
      }

      function WorkCompPrt()
      {
        if(fm.SpanType.value=="1" && fm.Month.value=="")
        {
          alert("统计月份不能为空");
          return;
        }
        if(fm.SpanType.value=="")
        {
          alert("请选择统计类型!");
          return;
        }
        fm.ReportType.value="5";
        submitForm();
      }
