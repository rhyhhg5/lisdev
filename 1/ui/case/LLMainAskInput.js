//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass(); 
//window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
//	alert(fm.AskMode.value); 
    
	if(fm.SaveFlag.value=="1")
	{
		alert("该咨/询通知已保存!");
	  return false;
	}
		
  var i = 0;
  if (fm.Answer.value=="")
  {
  	fm.AskType.value="1";
  }
else
	{
		fm.AskType.value="0";
	}
//  if(!checkDate(fm.AccDate1.value,fm.AccEndDate1.value)
//  ||!checkDate(fm.InHospitalDate.value,fm.OutHospitalDate.value)){
//  	alert("请确认日期先后顺序是否正确!");
//  	return false;
//  	}
   //在系统中匹配校验客户姓名和客户号码
   if(fm.LogerNo.value != "" && fm.LogName.value != "") {
     
       if(!checkUser()) 
       {
         return false;
       }
   }
 //3558
   var EventCount=SubReportGrid.mulLineCount;
   var chkFlag=false;
   for (i=0;i<EventCount;i++){
     if(SubReportGrid.getChkNo(i)==true){
   var happenDate=SubReportGrid.getRowColData(i,2);
   if(!isDate(happenDate)){
 	  alert("第"+(i+1)+"行发生日期有误，请核实事件发生日期")
 	  return false;
   }
   //3558
   
   var nowDate=getCurrentDate();
   var day=dateDiff(happenDate,nowDate,"D");
//   if(day<0){ //3558  5.7并没有提，所以不必写
//     alert("您在第"+(i+1)+"行输入的发生日期不应晚于当前时间");
//     return false;
//   }
   
   var inDate=SubReportGrid.getRowColData(i,7);

   var outDate=SubReportGrid.getRowColData(i,8);
   if (outDate.length!=0&&inDate.length!=0){ //3558
	     outDate=modifydate(outDate);
	     inDate=modifydate(inDate);
 	if(isDate(outDate)&&isDate(inDate)){

     if(dateDiff(inDate,outDate,"D")<0){
       alert("第"+(i+1)+"行入出院日期有误，请核实被保险人入出院日期");
       return false;
     }
   }else{
 	  alert("第"+(i+1)+"行入出院日期有误，请核实被保险人入出院日期");
       return false;
   }
   }
   //3633
	if(!llcheckInputDate(happenDate)){
		alert("请核实第"+(i+1)+"行发生日期录入是否正确")
		return false;
	}
	if(inDate.length!=0){
		if(!llcheckInputDate(inDate)){
     		alert("请核实第"+(i+1)+"行入院日期录入是否正确")
     		return false;
     	}
	}
	if(outDate.length!=0){
		if(!llcheckInputDate(outDate)){
     		alert("请核实第"+(i+1)+"行出院日期录入是否正确")
     		return false;
     	}
	}
   //3633
     }
   }//3558
   
  if(!testDate()) 
  {
     
  	return false;}
 
  if( verifyInput2() == false ) return false;
 

  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

// 客户校验函数
//函数名：checkUser
//功能：验证咨询客户输入的客户姓名和客户号码是否存在且是否一致
function checkUser() {
    var LogerNo = fm.LogerNo.value;
    var LogName = fm.LogName.value;
   var  wherePart = getWherePart("a.InsuredNo", 'LogerNo' )

	fm.CustomerNo.value=fm.LogerNo.value;
	fm.CustomerName.value=fm.LogName.value;
    var strSQL0 ="select distinct a.name ";
    var sqlpart1="from lcinsured a where 1=1 ";
    var sqlpart2="from lbinsured a where 1=1 ";
    var strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;

  	var arrs=easyExecSql(strSQL);
  	if(arrs!=null)
  	{   
  	    if(arrs == fm.LogName.value) {
  	         return true;
  	    } else {
  	        alert("该客户姓名与客户号码不匹配！");
  	        return false;
  	    }
	  	

  	} else {
  	    alert ("该客户号码在系统中不存在！");
  	    return false;
  	}
	
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.SaveFlag.value="1";
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
	initTitle();
	updateEventGrid();
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
       
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLMainAskInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
   if(!checkDate(fm.AccDate1.value,fm.AccEndDate1.value)||!checkDate(fm.InHospitalDate.value,fm.OutHospitalDate.value)){
  	alert("请确认日期先后顺序是否正确!");
  	return false;}
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDiseaseQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//判断查询输入窗口的案件类型是否是回车， 
//如果是回车调用查询客户函数
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClientQuery();
		UpdateGrid();
	}
}
//查询客户信息
function ClientQuery()
{
	  var  wherePart = getWherePart("a.InsuredNo", 'LogerNo' )
  					   + getWherePart("a.name", 'LogName' )
  					   + getWherePart("a.IDNo", 'IDNo' );
  					  
  if ( wherePart == ""){
    alert("查询条件不能为空!");
    return false;
  }
	fm.CustomerNo.value=fm.LogerNo.value;
	fm.CustomerName.value=fm.LogName.value;
	//openWindow("ClientQueryMain.html","客户查询");
	//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  var strSQL0 ="select distinct a.insuredno,a.name,a.sex,a.birthday,"
  +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=a.IDType),"
  +"a.idno,'' ,'',a.IDType,a.OthIDNo,a.OthIDType ";
  var sqlpart1="from lcinsured a,lccont b where 1=1 and a.contno=b.contno and b.appflag='1' ";
  var sqlpart2="from lbinsured a,lbcont b where 1=1 and a.contno=b.contno and b.appflag='1' ";
  var strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;

  	var arrs=easyExecSql(strSQL);
  	if(arrs!=null)
  	{
	  	if(arrs.length==1){
	
		afterLLRegister(arrs);
		
	  	}else{
	      var varSrc = "&CustomerNo=" + fm.LogerNo.value;
	          varSrc += "&CustomerName=" + fm.LogName.value;
			  varSrc += "&IDNo=" + fm.IDNo.value;
	          varSrc += "&RgtNo=" + "";
	     
	      showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
	    }

  	} else {
  	    alert ("不存在所查询客户相关信息");
  	}
}

function afterLLRegister(arr){

  if(arr){
  	    
		fm.LogerNo.value=arr[0][0];
		fm.LogName.value=arr[0][1];
		fm.IDNo.value   =arr[0][5];
		
  	
  	  var strSql1="select phone,mobile,email,Postaladdress,zipcode from lcaddress where CustomerNo='"+fm.LogerNo.value+"'";
          strSql1+=" and AddressNo='1'";
        
  	  var arr1=easyExecSql(strSql1);

  	  if(arr1!=null)
  	  {
  	  	
  	  	fm.Phone.value=arr1[0][0];   
  	  	fm.Mobile.value=arr1[0][1];   
  	  	fm.Email.value   =arr1[0][2];
  	  	fm.AskAddress.value=arr1[0][3];   
  	    fm.PostCode.value=arr1[0][4];   
      }

      var strSql2="select Grpname from ldperson where CustomerNo='"+fm.LogerNo.value+"' and Name='"+fm.LogName.value+"'";
	  var arr2=easyExecSql(strSql2);
	
	if(arr2!=null)
  	  {
  	  	fm.LogComp.value = arr2[0][0];
  	  }
	     
	}
}
//查询医院名称
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitalCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}
//回复咨询
 function ReplySave()
{
  if(fm.AskType.value=="0")
  {
      if( fm.ConsultNo.value =="")
      {
          alert("没有咨询信息");
          return false;
      }
  }
      if(fm.AskType.value=="2")
      {
      	if(fm.CNNo.value=="")
      	{
      	alert("没有咨询通知信息");
      	return false;
      	}
      }	
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.action= "./LLAnswerInfoSave.jsp";
      fm.submit();
}
//添加事件
function EventSave()
{
	if(fm.AskType.value=="0")
	{
	  if ( fm.ConsultNo.value =="")
	  {
		  alert("请先保存咨询信息");
		  return 
	  }
	}
	//if(fm.AskType.value=="1")
	//{
	//	if(fm.NoticeNo.value=="")
	//	{
	//		 alert("请先保存咨询信息");
	//		 return
	//	}
	//}	   
	//if(fm.AskType.value=="2")
	//{
	//	if ( fm.CNNo.value=="")
	//	{
	//		 alert("请先保存咨询通知信息");
	//		 return
	//	}
	//}	
	fm.fmtransact.value="INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action= "./LLSubReportSave.jsp";
    fm.submit();	
}
//保存事件后初始化事件列表
function updateEventGrid(EventNo)
{
	if (fm.fmtransact.value=="INSERT||MAIN")
	{
//		alert(fm.AskType.value);
    	if(fm.AskType.value==0)
		{
    		// #3500  关联事件查询
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,(select codename from ldcode1 where codetype='province1' and code=LLSubReport.accprovincecode), " +
		"(select codename from ldcode1 where codetype='city1' and code=LLSubReport.acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=LLSubReport.acccountycode fetch first 1 row only), " +
		"LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate,LLSubReport.AccDesc,LLSubReport.accprovincecode,LLSubReport.acccitycode,LLSubReport.acccountycode "
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		var arr=easyExecSql(strsql);
		}
		if(fm.AskType.value==1)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,(select codename from ldcode1 where codetype='province1' and code=LLSubReport.accprovincecode), " +
		"(select codename from ldcode1 where codetype='city1' and code=LLSubReport.acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=LLSubReport.acccountycode fetch first 1 row only), " +
		"LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate,LLSubReport.AccDesc,LLSubReport.accprovincecode,LLSubReport.acccitycode,LLSubReport.acccountycode "
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		}
		if(fm.AskType.value==2)
		{	
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,(select codename from ldcode1 where codetype='province1' and code=LLSubReport.accprovincecode), " +
		"(select codename from ldcode1 where codetype='city1' and code=LLSubReport.acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=LLSubReport.acccountycode fetch first 1 rows only), " +
		"LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate,LLSubReport.AccDesc,LLSubReport.accprovincecode,LLSubReport.acccitycode,LLSubReport.acccountycode "
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		fm.Answer.value=arr;
		}
	
		turnPage.queryModal(strsql, SubReportGrid);
	}
	if(fm.fmtransact.value=="DELETE||RELA")
	{

		if(fm.AskType.value==0)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,(select codename from ldcode1 where codetype='province1' and code=LLSubReport.accprovincecode), " +
		"(select codename from ldcode1 where codetype='city1' and code=LLSubReport.acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=LLSubReport.acccountycode fetch first 1 row only), " +
		"LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject,LLSubReport.accprovincecode,LLSubReport.acccitycode,LLSubReport.acccountycode "
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		}
		if(fm.AskType.value==1)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,(select codename from ldcode1 where codetype='province1' and code=LLSubReport.accprovincecode), " +
		"(select codename from ldcode1 where codetype='city1' and code=LLSubReport.acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=LLSubReport.acccountycode fetch first 1 row only), " +
		"LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject,LLSubReport.accprovincecode,LLSubReport.acccitycode,LLSubReport.acccountycode "
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.noticeno='" + fm.NoticeNo.value +"')";
		}
		if(fm.AskType.value==2)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,(select codename from ldcode1 where codetype='province1' and code=LLSubReport.accprovincecode), " +
		"(select codename from ldcode1 where codetype='city1' and code=LLSubReport.acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=LLSubReport.acccountycode fetch first 1 row only), " +
		"LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject,LLSubReport.accprovincecode,LLSubReport.acccitycode,LLSubReport.acccountycode "
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.CNNo.value +"')";
		}
		turnPage.queryModal(strsql, SubReportGrid);
	}

}
//选择咨询类型判断显示不显示回复内容
function afterCodeSelect(ObjType,Obj)
{
	if(ObjType=="AskType" && Obj.value!="")
	{
		if(Obj.value==0)
		{
			ConsultInfo2.style.display="";
		}
		if(Obj.value==1)
		{
			ConsultInfo2.style.display="none";
		}
		if(Obj.value==2)
		{
			ConsultInfo2.style.display="";
		}
	}
}

function checkDate(begin,end){
	if(modifydate(begin)<=modifydate(end))
		return true;
	else
		return false;
}

function CaseChange()
{
   initTitle();
   ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}
function ConsultChange()
{
	fm.fmtransact.value="INSERT||MAIN";
	updateEventGrid();
	var strSQL="select (select LLConsult.customername from LLConsult where LLConsult.Consultno='"+fm.ConsultNo.value+"'),(select LLConsult.customerno from LLConsult where LLConsult.Consultno='"+fm.ConsultNo.value+"'),llmainask.Phone,"
							+"llmainask.Email,llmainask.LogComp,llmainask.AskAddress,llmainask.PostCode,"
							+"(select ldcode.codename from "
												+"ldcode where ldcode.codetype='llaskmode'and ldcode.code=llmainask.AskMode),"
							+"(select ldcode.codename from "
												+"ldcode where ldcode.codetype='llreturnmode'and ldcode.code=llmainask.AnswerMode),"
							+"llmainask.Mobile,llmainask.AskMode,llmainask.AnswerMode"
							+" from llmainask where llmainask.logno=(select LLConsult.LogNo from LLConsult where LLConsult.Consultno='"+fm.ConsultNo.value+"')";
	var arr = easyExecSql(strSQL);
	if (arr)
	{
		fm.LogName.value=arr[0][0];
		fm.LogerNo.value=arr[0][1];
		fm.Phone.value=  arr[0][2];
		fm.Email.value=  arr[0][3];
		fm.LogComp.value=arr[0][4];
		fm.AskAddress.value=arr[0][5];
		fm.PostCode.value=  arr[0][6];
		fm.AskModeName.value=   arr[0][7];
		fm.AnswerModeName.value=   arr[0][8];
		fm.Mobile.value=arr[0][9];
		fm.AskMode.value=arr[0][10];
		fm.AnswerMode.value=arr[0][11];
		}
		var strSQL="select idno from ldperson where customerno='"+fm.LogerNo.value+"'";
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.IDNo.value=arr[0][0];
		}
		var strSQL="select a.CustStatus,(select b.codename from ldcode b where b.codetype='llcuststatus' and b.code=a.CustStatus),a.acccode,case when a.acccode='1' then '意外' else '疾病' end from LLConsult a where a.ConsultNo='"+fm.ConsultNo.value+"'";
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.CustStatus.value=arr[0][0];
			fm.CustStatusName.value=arr[0][1];
			fm.AccType.value=arr[0][2];
			fm.AccTypeName.value=arr[0][3];
		}
		var strSQL="select Answer from LLAnswerInfo where ConsultNo='"+fm.ConsultNo.value+"'";
		//fm.CContent.value=strSQL;
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.Answer.value=arr[0][0];
		}
		var strSQL="select CContent from LLConsult where ConsultNo='"+fm.ConsultNo.value+"'";
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.CContent.value=arr[0][0];
		}
		//QueryCommHop();
		QueryHospital();
}
function initMain()
{
	var a=fm.ConsultNo.value;
//	alert(a);
	if(a.length>0)
	{
		ConsultChange();
	}
}

//用于校验日期先后
//出险日期为避填
//出险日期>住院日期
//住院日期<出院日期
function testDate()
{

	for(i=0;i<SubReportGrid.mulLineCount;i++)
	{
		var accDate=SubReportGrid.getRowColData(i,2);
		if(accDate=="")
		{
			alert("请输入出险日期！");
			return false;
		}
		var inDate=SubReportGrid.getRowColData(i,7);
		var outDate=SubReportGrid.getRowColData(i,8);
		var accDate = modifydate(accDate);
		if(inDate.length!=0){
			inDate = modifydate(inDate);
			if(dateDiff(accDate,inDate,"D")<0)
			{
				alert("第"+(i+1)+"行，住院日期晚于出险日期！");
				return false;
			}
		}
		if (outDate.length!=0){
			outDate = modifydate(outDate);
			if(dateDiff(inDate,outDate,"D")<0)
			{
				alert("第"+(i+1)+"行，住院日期晚于出院日期！");
				return false;
			}
		}
		
		//#3500 增加发生地点校验**start**
		if((SubReportGrid.getRowColData(i,3)== null) || (SubReportGrid.getRowColData(i,3)=="")){
	      	  alert("第"+(i+1)+"行关联事件信息录入信息不全,请录入发生地点(省)信息");
	      	  	SubReportGrid.setFocus(i,1,SubReportGrid);
	            return false;
	        }
	        if((SubReportGrid.getRowColData(i,4)== null) || (SubReportGrid.getRowColData(i,4)=="")){
	      	  alert("第"+(i+1)+"行关联事件信息录入信息不全,请录入发生地点(市)信息");
	      	  	SubReportGrid.setFocus(i,1,SubReportGrid);
	            return false;
	        }
	        if((SubReportGrid.getRowColData(i,5)== null) || (SubReportGrid.getRowColData(i,5)=="")){
	      	  alert("第"+(i+1)+"行关联事件信息录入信息不全,请录入发生地点(县)信息");
	      	  	SubReportGrid.setFocus(i,1,SubReportGrid);
	            return false;
	        }
	     // 校验发生地点省市县编码
	        var checkAccCode=checkAccPlace(SubReportGrid.getRowColData(i,10),SubReportGrid.getRowColData(i,11),SubReportGrid.getRowColData(i,12))
	        if(checkAccCode !=""){
	     	   alert(checkAccCode);
	     	   return false;
	        }
		//#3500增加发生地点校验**end**
	}
		return true;
}
function UpdateGrid()
{
	var dpart=getWherePart("AccDate","CDateS",">=") + getWherePart("AccDate","CDateE","<=");
	var strSql="select SubRptNo,AccDate,(select codename from ldcode1 where codetype='province1' and code=accprovincecode), " +
	"(select codename from ldcode1 where codetype='city1' and code=acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=acccountycode fetch first 1 row only), " +
	"AccPlace,InHospitalDate,OutHospitalDate,AccDesc,accprovincecode,acccitycode,acccountycode "+
							"from LLSubReport where CustomerNo='"+fm.LogerNo.value+"'";
			strSql+=dpart;
			strSql+=" order by AccDate desc";
			turnPage.queryModal(strSql, SubReportGrid);
}

function QueryCommHop()
{
	if(fm.ConsultNo.value!="")
	{
	
	var strSQL="select a.HospitalCode,a.HospitalName,b.codename,a.HosAtti,a.Sessionroom,a.Bed from LLCommendHospital a,ldcode b where ConsultNo='"
					+ fm.ConsultNo.value +"' and b.codetype='llhospiflag' and b.code=a.HosAtti";
	//turnPage.queryModal(strSQL, EventGrid);
	arrResult = easyExecSql(strSQL);
	if ( arrResult )
	{
		 displayMultiline(arrResult,CommHospitalGrid);
	
   }
	 
	}
}

//打印接口
function printPage()
{
	if(fm.ConsultNo.value=="")
	{
		alert("请先保存数据");
		return false;
	}
	showInfo = window.open("./CsuRplPrt.jsp?ConsultNo="+fm.ConsultNo.value,"PrintPage",'width=600,height=400,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
if(fm.AnswerMode.value=='4'&&fm.Mobile.value==''){
	alert("您选的回复方式是短信，请填入手机号码！");
	return false;
}
if(fm.AnswerMode.value=='3'&&fm.Email.value==''){
	alert("您选的回复方式是电子邮件，请填入电子邮箱地址！");
	return false;
}
  fm.SMSContent.value="尊敬的"+fm.LogName.value+"先生\女士,您好,关于您对("
              +fm.CContent.value+")的咨询,本公司答复如下("+fm.Answer.value
              +").请您保存好本信息,以作为答复确认的凭证.如有任何疑问,请拨打95518(北京)或4006695518(全国其他地区)进行咨询.";
  fm.EmailContent.value="尊敬的"+fm.LogName.value+"先生\女士,您好,关于您对("
              +fm.CContent.value+")的咨询,本公司答复如下("+fm.Answer.value
              +").请您保存好本信息,以作为答复确认的凭证.如有任何疑问,请拨打95518(北京)或4006695518(全国其他地区)进行咨询.";
	fm.action="./SendMail.jsp";
	fm.submit();
}

function newDate(oldDate,months)   
{
	if(oldDate==""||months=="")
	{
		return false;
	}
	if (typeof(oldDate) == "string") {
		origDate = getDate(oldDate);
	}
	var oldD=origDate.getDate();
	var oldM=origDate.getMonth();
	var oldY=origDate.getFullYear();
	var tempM=oldM+months;
	var newM=tempM;
	var newY=oldY;
while (tempM>12||tempM<1)
	{
		if (tempM>12)
		{
			newM=tempM-12;
			newY=oldY+1;
		}
		if(tempM<1)
		{
			newM=tempM+12;
			newY=oldY-1;
		}
	}
	
	newD=oldD;
	splitOp='-';
	newDate=newY+splitOp+newM+splitOp+newD;
//	alert(newDate);
	return newDate;
}

function submitFormSurvery()
{    if( verifyInput2() == false ) return false;
    if(fm.LogerNo.value != "" && fm.LogName.value != "") {
       
       if(!checkUser()) 
       { 
         return false; 
       } else {
         var varSrc = "&CaseNo=" + fm.ConsultNo.value;
		varSrc += "&InsuredNo=" + fm.LogerNo.value;
		varSrc += "&CustomerName=" + fm.LogName.value;
		varSrc += "&StartPhase=2" 
		//alert(varSrc);
		//showInfo = window.open("./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
		pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
		showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
       }
   }
	
}
function SendMail(){
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "邮件发送成功！" ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	}
function SendMsg( ttext,tto,MsgFlag )
{
		var Flag=MsgFlag;
  		if (MsgFlag) {
				   var tsname= "service@picchealth.com";      
   				  var tlogin="理赔咨询回复";      
   				  var tmobile=tto;//fm.all('to').value; 
   				  var i=0;
   				  var tUrl = new Array();  
   		while(ttext.length>210)
   		{
   		  var tmpMSG = ttext.substring(0,210);		  
   		  alert(tmpMSG);       
				tUrl[i]="http://211.100.6.183/TSmsPortal/smssend?dt="+tmobile+"&feecode=000000&feetype=01&svid=picch&spno=5467&msg="+ tmpMSG +"&reserve=0000000000000000&type=0";
				var win=window.open(tUrl,"fraSubmit",'height=110,width=220,top=260,left=400,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
				ttext = ttext.substring(210);
				i++;
			}
				tUrl[i]="http://211.100.6.183/TSmsPortal/smssend?dt="+tmobile+"&feecode=000000&feetype=01&svid=picch&spno=5467&msg="+ ttext +"&reserve=0000000000000000&type=0";
				var win=window.open(tUrl[i],"fraSubmit",'height=110,width=220,top=260,left=400,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
   		  alert(ttext);       
			
				if (win.result==0){					
			      var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送" ;  
						showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
					 }
			 else{		
						var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送成功！" ;  
						showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");    	           
					  }	 
          }
 
}

function QueryHospital()
{ 
	var sql = "select a.hospitalcode,a.hospitalname,"
	         +"(select b.associateclass from ldhospital b where b.hospitcode = a.hospitalcode),"
	         +"(select c.levelcode from ldhospital c where c.hospitcode = a.hospitalcode),"
	         +"a.sessionroom,a.bed from LLCommendHospital a where a.consultno = '"
	         +fm.all('ConsultNo').value+"'";
	      
	turnPage.queryModal(sql,CommHospitalGrid); 
	
	}
