//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="1";
          //使用翻页功能，必须建立为全局变量

var turnPage = new turnPageClass();
var tRowNo=0;
//提交，查询按钮对应操作
function submitFormPA()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  window.open("./FramePayAffirmQuery.jsp?Flag=4");
}
function submitFormPB()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  window.open("./FramePayAffirmQuery.jsp?Flag=5");
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function submitFormLPPDDY()
{
  
  var i = 0;
  var showStr="正在打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action = "./ClaimPD.jsp";
  fm.target="f1print";
  showInfo.close();
  fm.submit();
}

function submitFormTZSDY()
{
	alert("二次通知书打印！");
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
}

//提交前的校验、计算
function beforeSubmit()
{
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
	alert("新增操作！");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

function Gf_ensure()
{
	if (fm.ClmNo.value=="")
	{
		alert("请作赔案查询");
		return;
	}
	fm.Opt.value="Gf_ensure";
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.action="./ClaimUnderwriteSave.jsp";
	fm.target = "fraSubmit";
	fm.submit(); //提交
}
