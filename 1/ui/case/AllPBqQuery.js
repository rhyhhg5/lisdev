//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//批单打印下拉框险种条件已经被写死
function initEdorType(cObj)
{
	var tRiskCode = fm.all('RiskCode').value;
	mEdorType = " 1 and RiskCode=#112103# and AppObj=#I#";
	//alert(mEdorType);
	showCodeList('EdorCode',[cObj], null, null, mEdorType, "1");
	//alert('bbb');
}

function actionKeyUp(cObj)
{
	var tRiskCode = fm.all('RiskCode').value;
	mEdorType = " 1 and RiskCode=#112103# and AppObj=#I#";
	//alert(mEdorType);
	showCodeListKey('EdorCode',[cObj], null, null, mEdorType, "1");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
var clickTimes = 0;
function easyQueryClick()
{

	var strSQL = "select b.edorno,b.caseno,b.insuredno,b.insuredname,-a.getmoney, "
		         + " b.appoperator,"
		         + " case a.edorState"
    		     + " when '0' then '确认' "
    		     + " when '9' then '待收费' "
    		     + " else '正在申请' "
    		     + " end ,"
    		     + " case b.edortype when 'CT' then '解约' when 'RB' then '解约回退' when 'HZ' then '合同终止' when 'XT' then '协议退保' end"
    		     + " from LPEdormain a, llcontdeal b "
    		     + " where a.edorAcceptNo = b.edorno and a.EdorState in ('0','9') "
    		     + " and exists (select 1 from llcase where caseno=b.caseno and mngcom like '"+fm.ManageCom.value+"%') "
                 + getWherePart('b.EdorNo','EdorNo')
             //    + getWherePart('b.Managecom','ManageCom','like')
                 + getWherePart('b.edortype','EdorType')
                 + getWherePart('b.CaseNo','CaseNo')
                 + getWherePart('b.AppDate','EdorAppDate')
                 + getWherePart('b.insuredno','InsuredNo')
                 + "order by edorAcceptNo desc ";
  turnPage.queryModal(strSQL,PolGrid);
  
}

function setEdorName()
{
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    var edorNo = PolGrid.getRowColDataByName(i, "EdorAcceptNo");
    var sql = "select EdorType from LPEdorItem where EdorNo = '" + edorNo + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      var edorCode = "";
      for(var j = 0; j < rs.length; j++)
      {
        edorCode = edorCode + rs[j][0] + " ";
      }
      PolGrid.setRowColDataByName(i, "EdorCode", edorCode);
    }
  }
}

//查询按钮
function QueryClick()
{
	initPolGrid();
	fm.all('Transact').value ="QUERY|EDOR";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}



// 项目明细查询
function ItemQuery()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击明细查询按钮。" );
	else
	{
	    var cEdorNo = PolGrid.getRowColData(tSel - 1,1);	
	    var cSumGetMoney = 	PolGrid.getRowColData(tSel - 1,5);			
        var cPolNo = PolGrid.getRowColData(tSel - 1,2);	
        
		if (cEdorNo == ""||cPolNo=="")
		    return;
		    
		window.open("../sys/AllPBqItemQueryMain.jsp?EdorNo=" + cEdorNo + "&SumGetMoney=" + cSumGetMoney+ "&PolNo=" + cPolNo);		
							
	}
}


	//打印批单
function PrtEdor()
{
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击查看按钮。" );
		else
		{
			var state =PolGrid. getRowColData(tSel-1,6) ;
	        
			if (state=="正在申请")
				alert ("所选批单处于正在申请状态，不能打印！");
			else{
			  var EdorAcceptNo=PolGrid. getRowColData(tSel-1,1) ;
				
				window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+EdorAcceptNo+"");
				//fm.target="f1print";	
			
			//	fm.submit();

			}
		}
}


function PBqQueryClick()
{
//	
//    arrReturn = getQueryResult();
//	fm.all('EdorNo').value = arrReturn[0][0];
//	fm.all('PolNo').value=arrReturn[0][1];		
	
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	//arrSelected[0] = arrGrid[tRow-1];
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//alert(arrSelected[0][0]);
	return arrSelected;
}