//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

 
//提交前的校验、计算  

/*
function beforeSubmit()
{	
}           
*/

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
 	parent.fraMain.rows = "0,0,0,0,*";
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function QuerySurvey()
{
    window.open("FrameSurveyQuery.jsp");
}    
function EndSurvey()
{
	window.open("FrameSurveyEnd.jsp");
}
       
//调查岗确认调查报告的操作
function EnsureSurvey()
{
	if(fm.Result.value==""||fm.Result.value==null)
	{
		alert("请您录入调查报告内容");
		return;
	}
	else 
	{
		if (fm.SurveyFlag.value==2)
		{
			alert("该调查报告已经确认！");
			return;
		}
		else
		{
			var i = 0;
  		var showStr="正在确认该调查报告，请您稍候并且不要修改屏幕上的值或链接其他页面";
 			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			showSubmitFrame(mDebug);
			fm.fmtransact.value = "ENSURE";
			fm.action = "./SurveyOperate.jsp";
			fm.submit();
			fm.SurveyFlag.value="2";
		}
	}
}

//调查岗修改调查报告的操作；
function UpdateSurvey()
{
	if (fm.SurveyFlag.value!=1)
	{
		alert ("您无法修改该调查报告！");
		return;
	}
	else
	{
		var i = 0;
  	var showStr="正在修改该调查报告，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		showSubmitFrame(mDebug);
		fm.fmtransact.value = "UPDATE";
		fm.action = "./SurveyOperate.jsp";
		fm.submit();
	}
}

//调查岗接收调查报告的操作；
function AcceptSurvey()
{
	if (fm.SurveyFlag.value!=0)
	{
		alert ("您无法接收该调查报告！");
		return;
	}
	
	else
	{
		var i = 0;
  	var showStr="正在接收该调查报告，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		showSubmitFrame(mDebug);
		fm.fmtransact.value = "ACCEPT";
		fm.action = "./SurveyOperate.jsp";
		fm.submit();
		fm.SurveyFlag.value="1";
	}
}