<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2005-4-23 16:49:22
//Author：Xx
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
    String AheadDays="-30";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLClaimMainNewList.js"></SCRIPT>
   <%@include file="LLClaimMainInit.jsp"%>
   <script language="javascript">
   function initDate(){
   fm.RgtDateS.value="<%=afterdate%>";
   fm.RgtDateE.value="<%=CurrentDate%>";
   var usercode="<%=Operator%>";
   var comcode="<%=Comcode%>";
   fm.Operator.value=usercode;
   fm.OrganCode.value=comcode;

   var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
   var arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
      fm.optname.value= arrResult[0][0];
 		}
   }
   </script>
 </head>
<body  onload="initDate();initForm();">
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         理赔员信箱
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
			<table  class= common>
				<TR  class= common8>
					<TD  class= title8>
							<input type="radio" name="StateRadio"  value="0" onclick="classify()">全部案件 
							<input type="radio" name="StateRadio"  value="3" checked onclick="classify()">待处理案件 
							<input type="radio" name="StateRadio"  value="1" onclick="classify()">未结案  
							<input type="radio" name="StateRadio"  value="2" onclick="classify()">已结案
							<input type="radio" name="StateRadio"  value="4" onclick="classify()">无影像
							<input type="radio" name="StateRadio"  value="4" onclick="classify()">问题件
							<input type="checkbox" name="AllTime" >全部时效</input>
				 	</TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>
							<input type="radio" name="typeRadio"  value="0">咨询类 
							<input type="radio" name="typeRadio"   value="1">通知类  
							<input type="radio" name="typeRadio" checked  value="3">申请类
							<input type="radio" name="typeRadio" value="4">申诉类
							<input type="radio" name="typeRadio" value="5">错误处理类
					 </td>
			  </tr>
				 <TR  class= common8>
						 <td>
								 <input type="radio" value="01" name="RgtState" onclick="easyQuery()">受理</input>
								 <input type="radio" value="02" name="RgtState" onclick="easyQuery()">扫描</input>
								 <input type="radio" value="03" name="RgtState" onclick="easyQuery()">检录</input>
								 <input type="radio" value="04" name="RgtState" onclick="easyQuery()">理算</input>
								 <input type="radio" value="05" name="RgtState" onclick="QueryUW()">审批</input>
								 <input type="radio" value="06" name="RgtState" onclick="easyQuery()">审定</input>
								 <input type="radio" value="07" name="RgtState" onclick="easyQuery()">调查</input>
								 <input type="radio" value="08" name="RgtState" onclick="easyQuery()">查讫</input>
								 <input type="radio" value="09" name="RgtState" onclick="easyQuery()">结案</input>
								 <input type="radio" value="10" name="RgtState" onclick="QueryUW()">抽检</input>
								 <input type="radio" value="11" name="RgtState" onclick="QueryReceipt()">通知</input>
								 <input type="radio" value="12" name="RgtState" onclick="easyQuery()">给付</input>
								 <input type="radio" value="13" name="RgtState" onclick="QueryReceipt()">延迟</input>
								 <input type="radio" value="14" name="RgtState" onclick="easyQuery()">撤件</input>
								 <input type="radio" value="15" name="RgtState" onclick="easyQuery()">稽查</input>
								 <input type="radio" value="16" name="RgtState" onclick="easyQuery()">理赔二核</input>
								 
						 </TD>                                             
			  </TR>
				<tr>
				   <td>
				   	<input type="checkbox" value="1" name="llclaimdecision" onclick="PayQuery()">正常给付</input>
				   	<input type="checkbox" value="2" name="llclaimdecision" onclick="PayQuery()">部分给付</input>
				   	<input type="checkbox" value="3" name="llclaimdecision" onclick="PayQuery()">全额拒付</input>
				   	<input type="checkbox" value="4" name="llclaimdecision" onclick="PayQuery()">协议给付</input>
				   	<input type="checkbox" value="5" name="llclaimdecision" onclick="PayQuery()">通融给付</input>
				   </td>
				</tr>
			</table>

</DIV>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
	<TR  class= common8>
	<TD  class= title>管理机构</TD><TD  class= input> 
	<Input class= "code8"  name=OrganCode onkeydown="QueryOnKeyDown();" ondblclick="getstr();return showCodeList('comcode',[this], [0],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode', [this], [0],null,str,'1');" >
	<TD  class= title>理赔员代码</TD><TD  class= input> 
	<Input class= "readonly"  name=Operator >
	<TD  class= title>理赔员姓名</TD><TD  class= input> 
	<Input class="readonly" readonly name=optname></TD>
	
<!--	<TD  class= title>理赔号码</TD><TD  class= input> 
	<Input class=common8 name=CaseNo  onkeydown="QueryOnKeyDown()"></TD>-->
	<TR  class= common>	
<!--	<TD  class= title>客户号</TD><TD  class= input> -->
	<Input type=hidden name=CustomerNo onkeydown="QueryOnKeyDown()">
	<!--<TD  class= title>客户名称</TD><TD  class= input> 
	<Input class=common8 name=CustomerName></TD>-->
	<!--
		<TR  class= common>	
	</tr>
		<TD  class= title>团体批次号</TD><TD  class= input> -->
		<Input type=hidden name=RgtNo onkeydown="QueryOnKeyDown()">
		<TD  class= title8>受理日期从</TD>
		<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title8>到</TD>
		<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title>险种类型</TD>
        <TD  class= input> <input class="codeno" CodeData="0|2^1|个险^2|团险"  verify="险种类型|notnull" elementtype=nacessary name=ContType ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeydown="QueryOnKeyDown();" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input class=codename name=ContTypeName></TD>
     
     
	</TR>
	<TR  class= common>	
	 <TD  class= title>受理类型</TD>
        <TD  class= input> <input class="codeno" CodeData="0|5^0|全部^1|常规^2|批次导入^3|医保通^4|意外险平台^5|社保补充平台^6|上海医保卡平台"  name=RgtType ondblclick="return showCodeListEx('RgtType',[this,RgtTypeName],[0,1],null,null,null,1);" onkeydown="QueryOnKeyDown();" onkeyup="return showCodeListKeyEx('RgtType',[this,RgtTypeName],[0,1]);" value="0"><input class=codename name=RgtTypeName value="全部"></TD>
	</TR>
       </table>
     </Div>
 <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
       
 <!--新增加的下拉框-->
 <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
        <TR  class= common>
	<TD  class= title8>处理</TD>
	<TD  class= input8> <!-- <input class=codeno CodeData="0|8^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|撤件^8|材料退回登记^9|理赔人变更^10|理赔二核 "  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"> --><input class=codeno CodeData="0|8^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|撤件^8|材料退回登记^9|理赔人变更 "  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input class=codename name=DealWithName></TD>
	<TD  class= title8>查询</TD>
	<TD  class= input8> <input class=codeno CodeData="0|^1|通知咨询^2|理赔申请^3|理赔帐单^4|疾病记录^5|案件理算^6|理赔给付^7|历史赔付"  name=Query ondblclick="return showCodeListEx('Query',[this,QueryName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Query',[this,QueryName],[0,1],null,null,null,1);"><input class=codename name=QueryName></TD>
	<TD  class= title8>打印</TD>
	<TD  class= input8> <input class=codeno CodeData="0|8^0|给付通知书打印^1|给付明细表打印^2|给付凭证打印^4|补充材料打印^5|拒付通知书打印^6|案件归档清单打印^7|账户对账单^8|理赔给付细目表(Excel)"  name=Printf ondblclick="return showCodeListEx('Printf',[this,PrintfName],[0,1]);" onkeyup="return showCodeListKeyEx('Printf',[this,PrintfName],[0,1]);"><input class=codename name=PrintfName></TD>
        </TR>	
        <TR  class= common>

        </TR>
       </table>
     </Div>
      
    </Div>
   <Div id="divPage1" align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
	</Div>    
  <Div style= "display: 'none' ">  
 <hr/>       
 <input  class=cssButton type=button value="咨询通知" onclick="DealConsult()">
<input  class=cssButton type=button value="受理申请" onclick="DealApp()">
<input  class=cssButton type=button value="受理申请(团体)" onclick="DealAppGrp()">
<input  class=cssButton type=button value="账单录入" onclick="DealFeeInput()">
<input  class=cssButton type=button value="检录" onclick="DealCheck()">
<input  class=cssButton type=button value="理算" onclick="DealClaim()">
<input  class=cssButton type=button value="审批审定" onclick="DealApprove()">
<input  class=cssButton type=button value="理赔二核" onclick="submitLLUnderWrit()">
 <hr/>
<input  class=cssButton type=button value="给付通知书打印" onclick="ClaimGetPrint()">
<input  class=cssButton type=button value="给付明细表打印" onclick="ClaimDetailPrint()">
<input  class=cssButton type=button value="给付凭证打印" onclick="GPrint()">
<input  class=cssButton type=button value="调查报告打印" onclick="SurveyPrint()">
<input  class=cssButton type=button value="补充材料打印" onclick="AffixPrint()">
<input  class=cssButton type=button value="理赔给付细目表(Excel)" onclick="ClaimDetailPrintExcel()">

</div>
<!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
     <!--隐藏域-->

     <Input type="hidden" class= common name="fmtransact" >
     <Input type=hidden name=AllOperator>
     <input type=hidden name=sql>
     <Input type="hidden" class= common name="GrpRgtNo">
			<Input type="hidden" class= common name="Notice" >
			<Input type="hidden" class= common name="Detail" >
			<Input type="hidden" class= common name="DetailExcel" >
			<Input type="hidden" class= common name="GrpDetail" >
			<Input type="hidden" class= common name="SingleCaseNo" >
			<Input type="hidden" class= common name="selno" >
			<Input type="hidden" class= common name="StartCaseNo">
			<Input type="hidden" class= common name="EndCaseNo">
			<Input type="hidden" class= common name="GetDetail">
			<Input type="hidden" class= common name="CaseRelaNo">
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
