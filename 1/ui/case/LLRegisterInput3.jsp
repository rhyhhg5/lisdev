<html>
	<%
	//Name:LLRegisterInput.jsp
	//Function：个人立案界面的初始化程序
	//Date：2005-02-16 17:44:28
	//Author ：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%
 	String CurrentDate= PubFun.getCurrentDate();   
    String CurrentTime= PubFun.getCurrentTime();
  %>
	<head >
		<SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>	
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    
    <!-- 新加 -->
	<script type="text/javascript" src="../common/IDjs/jeasy_vedio.js"></script>

    
		<SCRIPT src="LLRegisterInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLRegisterInit.jsp"%>
    <script language="javascript">
    var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
      function initDate(){
        fm.OpStartTime.value="<%=CurrentTime%>";
        fm.OpStartDate.value="<%=CurrentDate%>";
        fm.CaseRuleMng.value="<%=mG.ComCode%>";
      }
    </script>
	</head>
	<body  onload="initForm();initElementtype();initID();">
		<form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">

    <%@include file="LLRemark.jsp"%>
	  <%@include file="CaseTitle.jsp"%>
			
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerSearch);">
				</td>
				<td class= titleImg>
					客户查询
				</td>
			</tr>
		</table>
		<div id="divCustomerSearch" style="display:''">
			<table  class= common>
				<TR  class= common8>
					<TD  class= title8>客户姓名</TD><TD  class= input8><Input class=common name="CustomerName"  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>客户号码</TD><TD  class= input8><Input class=common name="CustomerNo" onkeydown="QueryOnKeyDown()"></TD>
		      		<TD  class= title8>保单号码</TD><TD  class= input8><Input class=common name="ContNo" onkeydown="QueryOnKeyDown()" ></TD>
				</tr>
				<TR  class= common8>
					<TD  class= title8>证件类型</TD><TD  class= input8><Input  onClick="showCodeList('idtype',[this,tIDTypeName],[0,1]);" onkeyup="showCodeListKeyEx('idtype',[this,tIDTypeName],[0,1]);" class=codeno name="tIDType" verify="证件类型|code:idtype&INT"><Input class= codename name=tIDTypeName onkeydown="QueryOnKeyDown()"></TD>				
					<TD  class= title8>证件号码</TD><TD  class= input8><Input class=common name="tIDNo" id="idcard"  onkeydown="QueryOnKeyDown()">
					<input class=cssButton style='width:80px;' type=button value="读取身份证" onclick="readidcard()">
					<!--<TD  class= title8>  <input class=cssButton id='btnreadid' value='读取身份证' onclick='readidcard()'/></TD>-->
					</TD>
					<TD  class= title8>出生日期</TD><TD class= input8><Input class=readonly readonly name="CBirthday"></TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>证件生效日期</TD><TD  class= input8><Input class=readonly name="IDStartDate" readonly></TD>
					<TD  class= title8>证件失效日期</TD><TD  class= input8><Input class=readonly name="IDEndDate" readonly></TD>				
					<TD  class= title8><input class=cssButton style='width:80px;' type=button value="卡折查询" onclick="CertifySearch()"></TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>社保号码</TD><TD  class= input8><Input class=common name="OtherIDNo"  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>性别</TD><TD  class= input8><Input class=readonly readonly name="ShowSexName" ></TD>
					<TD  class= title8>国籍</TD><TD class= input8><Input class=readonly readonly name="ShowNativePlace" ></TD>
				</TR>
				<TR>
					<TD  class= title8>职业类别</TD><TD class= input8><Input class=readonly readonly name="ShowOccupationType" ></TD>
					<TD  class= title8>固定电话</TD>
					<TD  class= input8><Input class= readonly name="ShowPhone" readonly">
					</TD>
					<TD  class= title8>手机号码</TD><TD class= input8><Input class=common name="MobilePhone"></TD>								
				</tr>
				<TR  class= common8>
					<TD  class= title8>联系地址</TD>
					<TD  class= input8 colspan=3><Input class= readonly readonly name="ShowPostalAddress" style=width:97% ></TD>				
				</TR>
			</table>
		</div>
		
		<div id="div1" style="display:''">
			<table>
				<TR>
					<td>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegisterInfo);">
					</td>
					<td class= titleImg>
						个人申请信息
					</TD>
				</TR>
			</table>
		</div>
		<Div id= "divRegisterInfo" style="display: ''">
			<table class=common>
				<TR class=common >
					<TD  class= title8>申请人与被保人关系</TD>
					<TD  class= input8><Input class="codeno" name=Relation onClick="return showCodeList('llRelation',[this,RelationName],[0,1]);" onkeyup="showCodeListKeyEx('llRelation',[this,RelationName],[0,1]);"  verify="申请人与被保人关系|code:llRelation&INT&notnull"><Input class="codename"  elementtype=nacessary name=RelationName></TD>
					<TD  class= title8>受理方式</TD>
					<TD  class= input8><Input class=codeno  name=RgtType onClick="showCodeList('LLAskMode',[this,RgtTypeName],[0,1]);" onkeyup="showCodeListKeyEx('LLAskMode',[this,RgtTypeName],[0,1]);" verify="受理方式|notnull&code:LLAskMode&INT"><Input class=codename name= RgtTypeName elementtype=nacessary ></TD>
					<TD  class= title8>回执发送方式</TD>
					<TD  class= input8><Input class= codeno name=ReturnMode onClick="showCodeList('LLreturnMode',[this,ReturnModeName],[0,1]);" onkeyup="showCodeListKeyEx('LLreturnMode',[this,ReturnModeName],[0,1]);" verify="回执发送方式|code:LLreturnMode&INT"><Input class= codename name=ReturnModeName ></TD>
				</TR>
				<TR  class= common>
					<TD  class= title8>申请人姓名</TD>
					<TD  class= input8><Input class= common name=RgtantName elementtype=nacessary verify="申请人姓名|notnull"></TD>
					<TD class= title8>申请人证件号码</TD>
					<TD class= input8><Input class= common name=IDNo elementtype=nacessary verify="申请人证件号码|notnull"></TD>
					<TD class= title8>申请人证件类型</TD>
					<TD class= input8> <Input class="codeno" name=IDType onclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this，IDTypeName],[0,1]);"  verify="证件类型|notnull&code:idtype&INT"><Input class="codename"  name=IDTypeName elementtype=nacessary></TD>
				</TR>
				<TR  class= common>
					<TD  class= title8>申请人电话</TD>
					<TD  class= input8><Input class= common name=RgtantPhone verify="电话号码|num&&len<=30"></TD>
					<TD  class= title8>申请人手机</TD>
					<TD  class= input8><Input class= common name=Mobile verify="手机号码|num&&len>=8"></TD>
					<TD  class= title8>申请人电子邮箱</TD>
					<TD  class= input8><Input class= common name=Email verify="电子邮箱|Email"></TD>
				</TR>
				<TR>
					<TD  class= title8>申请人地址</TD>
					<TD  class= input8 colspan=3><Input class= common name=RgtantAddress style=width:97% ></TD>
					<TD  class= title8>邮政编码</TD>
					<TD  class= input8><Input class= common name="PostCode" verify="邮政编码|zipcode&INT">
					</TD>
				</tr>
			</table>
		</Div>
		<Div id= "divGetMode" style="display: ''">
			<table class=common>
				<TR >
					<TD class= title8>受益金领取方式</TD>
					<TD class= input8><Input class="codeno" name=paymode onclick="return showCodeList('paymode',[this,paymodename],[0,1],null,str,'1');" onkeyup="return showCodeListKey('paymode',[this,paymodename],[0,1],null,str,'1');" verify="受益金领取方式|code:llgetmode&INT&notnull"><Input class="codename" name=paymodename  elementtype=nacessary ></TD>
					<TD class= title8 id=tiAppMoney style= "display: 'none'">本次理赔申报金额</TD>
					<TD class= input8 id=idAppMoney style= "display: 'none'"><Input class= common name="ApplyMoney"  ></TD>
					<TD class= title8 id=titemp0></TD>
					<TD id=idtemp0 class= input8><Input class= common name="sql" type=hidden></td>
					<TD class= title8></TD>
					<TD class= input8><Input class= common name="temp1" type=hidden></td>
				</TR>
				<table class=common id='divBankAcc'>
				<TR  class= common8 >
					<TD class= title8>银行编码</TD>
					<TD class= input8><Input class="codeno"  name=BankCode onclick="return showCodeList('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" onkeyup="return showCodeListKey('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" ><Input class="codename"  elementtype=nacessary name=BankName ></TD>
          <TD class= title8>签约银行</TD> 
          <TD class= input8><Input class="codename" name=SendFlag readonly="true" ></TD>
          <TD class= title8></TD>
          <TD class= input8></TD>				
				</TR>
				<TR>	
					<TD class= title8>银行账号</TD>
					<TD class= input8><input class= common name="BankAccNo" elementtype=nacessary id="bankcard">
						<input class=cssButton style='width:80px;' type=button value="读取银行卡" onclick="readbankcard()">
					</TD>
				
					<TD class= title8>银行账户名</TD>
					<TD class= input8><input class= common name="AccName" elementtype=nacessary></TD>
					 <TD class= title8>领款人和被保人关系</TD> 
          			<TD class= input8><Input class=codeno name=RelaDrawerInsured onClick="return showCodeList('relation',[this,RelaDrawerInsuredName],[0,1],null,1,'1');" onkeyup="showCodeListKeyEx('relation',[this,RelaDrawerInsuredName],[0,1],null,1,'1');"  ><Input class="codename" name=RelaDrawerInsuredName ></TD>
				</TR>
				</table>
			</table>
		</div>

		<table>
			<TR>
				<td>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEventInfo);">
				</td>
				<td class= titleImg>
					申请原因
				</TD>
			</TR>
		</table>
		<Div id= "divEventInfo" style="display: ''">
		  <table class=common>
		  	<TR  class= common8>
		  	    <TD  class= title8>申请日期</TD>
            	<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name="AppDate" onblur="fillDate()" elementtype=nacessary ></TD>
		  		<TD  class= title8>事故者现状</TD>
		  		<TD  class= input8><Input class=codeno name="CustStatus" onClick="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" onkeyup="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" verify="事故者现状|code:llcuststatus&INT"><Input class= codename name="CustStatusName"  ></TD>
		  		<TD  class= title8>死亡日期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="DeathDate" verify="死亡日期|date"></TD>
		  		<TD  class= title8></TD>
		  		<TD  class= input8></TD>
		  	</TR>
		  	<TR  class= common8>
		  	  <td colspan=6>
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="01">门诊费用
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="02">住院费用
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="03">医疗津贴
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="11">轻症疾病
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="04">重大疾病
	          <INPUT TYPE="checkBox" NAME="appReasonCode" value="05">身 故
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="06">护 理
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="07">失 能
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="08">伤 残
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="09">特 需
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="10">门诊大额
          </td>
		  	</TR>
		  </table>
    </div>

    <Table>
    	<TR>
    		<TD class=common>
    			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg>
    			客户事件信息
    		</TD>
    	</TR>
    </Table>
    <Div  id= "divLDPerson1" style= "display: ''" align = center>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanEventGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    	 <div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage(); shouCheck(); ">      
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage(); shouCheck(); "> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage(); shouCheck(); ">       
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage(); shouCheck(); ">       
     </div> 	
    </Div>

    <div>
    	<table class= common>
    		<tr class= common8> 
    		<TD  class= input>
    		<Div id= "divSimpleCase" style="display: ''">  			
    			<INPUT TYPE="checkBox" NAME="SimpleCase" value="01">机构处理
    		 </div>
    		  </TD>
    		 <TD  class= input>
    		 <Div id= "divEasyCase" style="display: ''"> 
    			  <INPUT TYPE="checkBox" NAME="EasyCase" value="01"> 简易案件
    		  </div>
            </TD>
            <TD  class= input>
    		 <Div id= "divFenCase" style="display: ''"> 
    			  <INPUT TYPE="checkBox" NAME="FenCase" value="01"> 上报分公司
    		  </div> 
    		</TD>
            <TD  class= input>
    		 <Div id= "divHeadCase" style="display: ''"> 
    			  <INPUT TYPE="checkBox" NAME="HeadCase" value="01"> 上报总公司
    		  </div> 
    		</TD>
    		</tr>
    		<tr class= common8>
    		  	<TD  class= input>
    			  <INPUT TYPE="checkBox" NAME="PrePaidFlag" value="01"> 回销预付赔款
    		 	 </TD>
    		  	<TD  class= title>材料齐备日期</TD>
    		    <TD  class= input><Input class=readonly readonly name=AffixGetDate ></TD>
    	  </tr>
      </table>
    </div>
    <br>
    <Div  id= "aa" style= "display: ''" align= right>
    	<input class=cssButton style='width:80px;' id="IdAppConf" type=button name='IdAppConf' value="申请确认" onclick="submitForm()" >
    </div>
    <hr>
    <Div  id= "divnormalquesbtn" style= "display: ''" align= right>
    	<input class=cssButton style='width:80px;' type=button value="撤  件" onclick="DealCancel()" id = "sub1">
    	<input class=cssButton style='width:80px;' type=button value="案件备注信息" onclick="openCaseRemark()" id = "sub7">
    	<input class=cssButton style='width:80px;' type=button value="申请材料选择" onclick="openAffix()" id = "sub2">
    	<input class=cssButton style='width:80px;' type=button value="账户修改" onclick="OpenBnf()" name=BankAccM id = "sub3">
    	<input class=cssButton style='width:80px;' type=button value="提起调查" onclick="submitFormSurvery()" id = "sub4">
    	 
    	<input class=cssButton style='width:80px;' type=button value="理赔二核" onclick="submitLLUnderWrit()" id = "sub6">
    	
    	<input class=cssButton style='width:80px;' type=button value="理赔受理回执"  name='payreceipt' onclick="PrintPage()"id = "sub5">
    	<input class=cssButton type=hidden value="账户更替" onclick="BnfModify()">
    	<input class=cssButton type=hidden value="事件信息" onclick="OpenEvent()">
    	<INPUT class=cssButton type=hidden value="查  询"  onclick="return queryClick();">
    	<input class=cssButton type=hidden value="申诉处理" id='AppealDeal'  onclick="OnAppeal()">
    </Div>
    <Div  id= "divRgtFinish" style= "display: 'none'" align= right>
      <hr>
    	<input class=cssButton style='width:80px;' type=button  value="团体申请完毕" onclick="RgtFinish()">
    	<input class=cssButton style='width:80px;' type=button  value="返  回" onclick="top.close()">
    </Div>

	    <input type=hidden name="fmtransact">
	    <input type=hidden name="Reason">
      <Input type=hidden name=AppAmnt >
      <Input type=hidden name=Sex>
	    <input type=hidden name="ReContNo">
	    <input type=hidden name="LoadFlag" value="1">
	    <input type=hidden name="AppealFlag" value="0">
	    <input type=hidden name="operate">
	    <input type=hidden name="rgtflag">
	    <input type=hidden name="realpeoples">
	    <input type=hidden name="AccFlag">
	    <input type=hidden name="OpStartDate">
	    <input type=hidden name="OpStartTime">
	    <input type=hidden name="OtherIDType">
	    <input type=hidden name="LoadC">
	    <input type=hidden name="LoadD">
	    <input type=hidden name="RiskCode">
	    <input type=hidden name="OpType">
	    
	    <input type=hidden name="SMSContent">
	    <input type=hidden name="EmailContent">
	    <input type=hidden name="CaseRule">
	    <input type=hidden name="CaseFlag">
	    <input type=hidden name="CaseRuleMng">
	    <input type=hidden name="ShowCaseRemarkFlag">
	  <span id="spanCode"  style="display: none; position:absolute; slategray">
	  </span>
  </form>


  </body>
</html>