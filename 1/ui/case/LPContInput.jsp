<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
	<%
		//程序名称：LDCodeInput.jsp
		//程序功能：外包业务保单配置
		//创建日期：2018-01-19 
		//创建人:xhw
		//更新记录:更新人  更新日期   更新原因/内容
	%>
	<%
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
 		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
 		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="./LPContInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LPContInputInit.jsp"%>
	</head>

	<body onload="initForm();initElementtype();">
		<form action="./LPContSave.jsp" method="post" name="fm" target="fraSubmit">
			<%-- <%@include file="../common/jsp/OperateButton.jsp"%> --%>
			<%@include file="../common/jsp/InputButton.jsp"%>
			
			<table class= common align='center'>
				<tr class="common">
					<td class="title">
						保单机构
					</td>
					<!-- <TD  class= input>
        <Input NAME=ThridCompany1 CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('ThridCompany1',[this,ThridCompanyName1],[0,1]);" onkeyup="return showCodeListKey('ThridCompany1',[this,ThridCompanyName1],[0,1]);" verify="外包第三方|code:ThridCompany1&null" ><input class=codename name=ThridCompanyName1 readonly=true  elementtype="">
      				</TD> -->
      				<TD class= input>
						<Input class="codeNo"  name=ManageCom verify="保单机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > </TD>
					<td class="title">
						团单号
					</td>
					<td class="input">
						<input class="common" name="Code" verify="团单号|len<21"/>
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						操作起期
					</td>
					<td class="input">
						<input class="coolDatePicker" name="startDate" verify="操作起期|len=10&DATE" />
						<span style='color:red'>(YYYY-MM-DD)</span>
					</td>
					<td class="title">
						操作止期
					</td>
					<td class="input">
						<input class="coolDatePicker" name="endDate" verify="操作止期|len=10&DATE" />
						<span style='color:red'>(YYYY-MM-DD)</span>
					</td>
				</tr>
				<tr>
					<td class="title"> 
						外包第三方
					</td>					
					<TD  class= input>
        <Input NAME=ThridCompany2 CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('ThridCompany2',[this,ThridCompanyName2],[0,1]);" onkeyup="return showCodeListKey('ThridCompany2',[this,ThridCompanyName2],[0,1]);" verify="外包第三方|code:ThridCompany2" ><input class=codename name=ThridCompanyName2   elementtype="">
      				</TD>
      
				</tr>
			</table>
			
			<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="queryClick();">
			
			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLDCode1)"/>
					</td>
					<td class="titleImg">
						查询信息
					</td>
				</tr>
			</table>
			
			<div id="divLDCode1" style="display:''">
    			<table class=common>
    				<tr class=common>
	  					<td text-align:left colSpan=1>
  							<span id="spanLDCodeGrid">
  							</span> 
		    			</td>
					</tr>
				</table>
			</div>
			
			<div id="divSetGrid" style="display:''">      
   				<table class=common>   
    				<tr class=common>
     					<td text-align:left colSpan=1>
      						<span id="spanSetGrid" ></span>
     					</td>
    				</tr>    
   				</table>  
  			</div>
  			<div id= "divPage" align=center style= "display: '' ">
				<input class=cssbutton value="首页" type=button onclick="turnPage.firstPage();">
				<input class=cssbutton value="上一页" type=button onclick="turnPage.previousPage();">
				<input class=cssbutton value="下一页" type=button onclick="turnPage.nextPage();">
				<input class=cssbutton value="尾页" type=button onclick="turnPage.lastPage();">
			</div>
  			
			<table>
				<tr>
					<td>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDCode1);">
					</td>
					<td class= titleImg>
						保单上报配置信息
					</td>
				</tr>			
			</table>
			
    		<iframe name="UploadiFrame" src="LPContUpLoadInput.jsp" frameborder="0" scrolling="no" height="40px" width="100%"></iframe>
    		<input type="hidden" id="fmcode" name="fmcode" value=<%=tG.ManageCom%>>
			<input type="hidden" id="fmtransact" name="fmtransact"/>
			<input type="hidden" id="fmAction" name="fmAction"/>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>