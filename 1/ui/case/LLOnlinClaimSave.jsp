<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GrpSimpleClaimConfSave.jsp
//程序功能：
//创建日期：2005-02-5 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数


CErrors tError = null;

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
VData tVData = new VData();
LLCASEPROBLEMSchema mLLCASEPROBLEMSchema = new LLCASEPROBLEMSchema();
LLCASEPROBLEMSet mLLCASEPROBLEMSet = new LLCASEPROBLEMSet();
LLOnlinCLaimDealBL mLLOnlinCLaimDealBL = new LLOnlinCLaimDealBL();

//输出参数
String FlagStr = "";
String Content = "";
String strOperate = request.getParameter("operate");
System.out.println("strOperate=" + strOperate);

if("INSERT".equals(strOperate)) {
	String caseNo = request.getParameter("Caseno");
	String customerNo = request.getParameter("Customerno");
	String customerName = request.getParameter("Customername");
	String lowerState = request.getParameter("Lowerstate");
	String lowerPeople = request.getParameter("Lowerpeople");
	String questionLowerNo = request.getParameter("QuestionLowerNo");
	//String remark = new String(request.getParameter("QuestionRemark").getBytes("ISO8859_1"),"GBK");
	String remark = request.getParameter("QuestionRemark");
	System.out.println("===================下发问题件备注：" + remark);
	mLLCASEPROBLEMSchema.setCASENO(caseNo);
	mLLCASEPROBLEMSchema.setREASONCODE(questionLowerNo);
	mLLCASEPROBLEMSchema.setUPREMARK(remark);
	mLLCASEPROBLEMSet.add(mLLCASEPROBLEMSchema);
}
try{
	tVData.add(mLLCASEPROBLEMSet);
    tVData.add(tG);
    mLLOnlinCLaimDealBL.submitData(tVData, strOperate);
}catch(Exception ex) {
	Content = "确认失败，原因是:" + ex.toString();
    System.out.println("aaaa"+ex.toString());
    FlagStr = "Fail";
}
//如果catch中发现异常，则不从错误类中提取错误信息
if(FlagStr == "") {
	tError = mLLOnlinCLaimDealBL.mErrors;
	if(!tError.needDealError()) {
		Content = "保存成功";
		FlagStr = "Success";
	}else {
		Content = "保存失败,原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>


<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>



