<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLConsultNoticeSave.jsp
//程序功能：
//创建日期：2005-01-12 16:10:51
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLConsultSchema tLLConsultSchema   = new LLConsultSchema();
  LLNoticeSchema  tLLNoticeSchema = new LLNoticeSchema();
  
  OLLConsultNoticeUI tOLLConsultNoticeUI   = new OLLConsultNoticeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    String Path = application.getRealPath("config//Conversion.config");	
    tLLConsultSchema.setConsultNo(request.getParameter("ConsultNo"));
    tLLConsultSchema.setLogNo(request.getParameter("LogNo"));
    tLLConsultSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLConsultSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLConsultSchema.setReplyState("0");
    tLLConsultSchema.setCustomerType(request.getParameter("CustomerType"));
    tLLConsultSchema.setCSubject(StrTool.Conversion(request.getParameter("CSubject"),Path));
    tLLConsultSchema.setCContent(StrTool.Conversion(request.getParameter("CContent"),Path));
    tLLConsultSchema.setAskGrade(request.getParameter("AskGrade"));
    tLLConsultSchema.setAccCode(request.getParameter("AccCode"));
    tLLConsultSchema.setDiseaseDesc(request.getParameter("DiseaseDesc")); 
    tLLConsultSchema.setExpRDate(request.getParameter("ExpRDate"));
    tLLConsultSchema.setDiseaseCode(request.getParameter("DiseaseCode"));
    tLLConsultSchema.setCustStatus(StrTool.Conversion(request.getParameter("CustStatus"),Path));
    tLLConsultSchema.setExpertFlag(request.getParameter("ExpertFlag"));
    tLLConsultSchema.setExpertNo(request.getParameter("ExpertNo"));
    tLLConsultSchema.setExpertName(request.getParameter("ExpertName"));   
    tLLConsultSchema.setAvaiFlag(request.getParameter("AvaiFlag"));
    tLLConsultSchema.setAvaliReason(request.getParameter("AvaliReason"));
    tLLConsultSchema.setAccDesc(request.getParameter("AccDesc"));
    tLLConsultSchema.setHospitalCode(request.getParameter("HospitalCode"));
    tLLConsultSchema.setHospitalName(request.getParameter("HospitalName"));    
    tLLConsultSchema.setInHospitalDate(request.getParameter("InHospitalDate"));
    tLLConsultSchema.setOutHospitalDate(request.getParameter("OutHospitalDate"));
    
    
    
    tLLNoticeSchema.setNoticeNo(request.getParameter("NoticeNo"));
    tLLNoticeSchema.setLogNo(request.getParameter("LogNo"));
    tLLNoticeSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLNoticeSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLNoticeSchema.setCustomerType(request.getParameter("CustomerType"));  
    tLLNoticeSchema.setNSubject(StrTool.Conversion(request.getParameter("NSubject"),Path));
    tLLNoticeSchema.setNContent(StrTool.Conversion(request.getParameter("NContent"),Path));    
    tLLNoticeSchema.setAccCode(request.getParameter("AccCode"));
    tLLNoticeSchema.setAccDesc(request.getParameter("AccDesc"));    
    //tLLNoticeSchema.setAvaiFlag("1");
    tLLNoticeSchema.setHospitalCode(request.getParameter("HospitalCode"));
    tLLNoticeSchema.setHospitalName(request.getParameter("HospitalName"));    
    tLLNoticeSchema.setInHospitalDate(request.getParameter("InHospitalDate"));
    tLLNoticeSchema.setOutHospitalDate(request.getParameter("OutHospitalDate"));
    tLLNoticeSchema.setDiseaseCode(request.getParameter("DiseaseCode"));
    tLLNoticeSchema.setDiseaseDesc(request.getParameter("DiseaseDesc"));  
    tLLNoticeSchema.setCustStatus(request.getParameter("CustStatus"));
    
    
    
    
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLLConsultSchema);
	  tVData.add(tLLNoticeSchema);
  	tVData.add(tG);
  	System.out.println(transact );
    tOLLConsultNoticeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLConsultNoticeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 咨询通知保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  String CNNo ="";
  LLConsultSchema rLLConsultSchema = null;
  if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact))
  {
  
    VData res = tOLLConsultNoticeUI.getResult();
    rLLConsultSchema=(LLConsultSchema)res.getObjectByObjectName("LLConsultSchema", 0);
    if ( rLLConsultSchema!=null )
    {
    	CNNo= rLLConsultSchema.getConsultNo();
    }
  }
  
  //添加各种预处理
%>                      

<html>
<script language="javascript">
  parent.fraInterface.fm.CNNo.value = "<%=CNNo%>" ;
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	//parent.fraInterface.updateCustomer("<%=CNNo%>");
</script>
</html>
