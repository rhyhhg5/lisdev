//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPersonGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

/*
function display(indexNum) 
{
//得到被选中的个人信息字符串
 str=fm.all("personInformation"+indexNum).value;
 arrRecord = str.split("|");  //拆分字符串，形成返回的数组
// alert(str);

 //前面只是得到部分字段的值，待完善，其他字段暂时赋空值
    top.opener.fm.all('CustomerNo').value=arrRecord[0];
    top.opener.fm.all('Password').value = arrRecord[1];    
    top.opener.fm.all('Name').value=arrRecord[2];
    top.opener.fm.all('Sex').value = arrRecord[3];
    top.opener.fm.all('Birthday').value = arrRecord[4];
    top.opener.fm.all('NativePlace').value = arrRecord[5];  
    top.opener.fm.all('Nationality').value = arrRecord[6];
    top.opener.fm.all('Marriage').value = arrRecord[7];
    top.opener.fm.all('MarriageDate').value = arrRecord[8];
    top.opener.fm.all('OccupationType').value = arrRecord[9];
    top.opener.fm.all('StartWorkDate').value = arrRecord[10];
    top.opener.fm.all('Salary').value = arrRecord[11];       //double    
    top.opener.fm.all('Health').value = arrRecord[12];
    top.opener.fm.all('Stature').value = arrRecord[13];      //double 
    top.opener.fm.all('Avoirdupois').value = arrRecord[14];   //double
    
    top.opener.fm.all('CreditGrade').value = arrRecord[15];
    top.opener.fm.all('IDType').value = arrRecord[16];
    top.opener.fm.all('Proterty').value = arrRecord[17];
    top.opener.fm.all('IDNo').value = arrRecord[18];
    top.opener.fm.all('OthIDType').value = arrRecord[19];
    top.opener.fm.all('OthIDNo').value = arrRecord[20];
    top.opener.fm.all('ICNo').value = arrRecord[21];
    top.opener.fm.all('HomeAddressCode').value = arrRecord[22];
    top.opener.fm.all('HomeAddress').value = arrRecord[23];
    top.opener.fm.all('PostalAddress').value = arrRecord[24];
    top.opener.fm.all('ZipCode').value = arrRecord[25];
    top.opener.fm.all('Phone').value = arrRecord[26];
    top.opener.fm.all('BP').value = arrRecord[27];
    top.opener.fm.all('Mobile').value = arrRecord[28];
    top.opener.fm.all('EMail').value = arrRecord[29];
    top.opener.fm.all('BankCode').value = arrRecord[30];
    top.opener.fm.all('BankAccNo').value = arrRecord[31];
    top.opener.fm.all('JoinCompanyDate').value = arrRecord[32];
    top.opener.fm.all('Position').value = arrRecord[33];
    top.opener.fm.all('GrpNo').value = arrRecord[34];
    top.opener.fm.all('GrpName').value = arrRecord[35];
    top.opener.fm.all('GrpPhone').value = arrRecord[36];
    top.opener.fm.all('GrpAddressCode').value = arrRecord[37];
    top.opener.fm.all('GrpAddress').value = arrRecord[38];
    top.opener.fm.all('DeathDate').value = arrRecord[39];
    top.opener.fm.all('Remark').value = arrRecord[40];
    top.opener.fm.all('State').value = arrRecord[41];
    top.opener.fm.all('BlacklistFlag').value = arrRecord[42];
    top.opener.fm.all('Operator').value = arrRecord[43];
 
}
*/


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPersonGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select * from LDPerson where 1=1 "
				 + getWherePart( 'CustomerNo' )
				 + getWherePart( 'Name' )
				 + getWherePart( 'Birthday' )
				 + getWherePart( 'IDType' )
				 + getWherePart( 'IDNo' );
//alert(strSQL);
	execEasyQuery( strSQL );
}

//选择页面上查询的字段对应于"select *"中的位置
function getSelArray()
{
	var arrSel = new Array();
	
	arrSel[0] = 0;
	arrSel[1] = 2;
	arrSel[2] = 3;
	arrSel[3] = 4;
	arrSel[4] = 16;
	arrSel[5] = 18;

	return arrSel;
}

function displayEasyResult( arrQueryResult )
{
	var i, j, m, n;
	var arrSelected = new Array();
	var arrResult = new Array();

	if( arrQueryResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPersonGrid();
		PersonGrid.recordNo = (currPageIndex - 1) * MAXSCREENLINES;
		PersonGrid.loadMulLine(PersonGrid.arraySave);

		arrGrid = arrQueryResult;
		// 转换选出的数组
		arrSelected = getSelArray();
		arrResult = chooseArray( arrQueryResult, arrSelected );
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PersonGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//PersonGrid.delBlankLine();
	} // end of if
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterLLRegister( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PersonGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = PersonGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}


function afterQuery()
{
	var strSQL0="select distinct a.InsuredNo,a.Name,a.Sex,a.Birthday,b.codename,"
	+"a.IDNo,'','',a.IDType,a.othidno,a.othidtype,a.addressno,"
	//#2329 （需求编号：2014-229）客户基本信息要素录入需求
  	+"codename('sex',a.sex),codename('nativeplace',a.nativeplace),codename('occupationtype',a.occupationtype),a.IDStartDate,a.IDEndDate "	
	+" from ";
	
	var wherePart = getWherePart("a.InsuredNo", "CustomerNo" )
								+ getWherePart("a.Name", "CustomerName" )
								+ getWherePart("a.ContNo","ContNo" )
								+ getWherePart("a.GrpContNo","GrpContNo" )
								+ getWherePart("a.IDType","IDType" )
								+ getWherePart("a.IDNo","IDNo" )
								+ getWherePart("a.Othidno","OtherIDNo");
	if (trim(wherePart)==""){
		alert("请录入查询条件！");
		return;
	}
	if (fm.RgtNo.value==null||fm.RgtNo.value==''){
	  wherePart = ",ldcode b where a.contno=c.contno and b.codetype='idtype' and b.code=a.idtype " +wherePart;
	}else{
	  wherePart = ",llregister d,ldcode b where a.contno=c.contno and b.codetype='idtype' and b.code=a.idtype and c.GrpContNo=d.RgtObjNo and d.RgtNo='"+fm.RgtNo.value+"'" +wherePart;
	}
	strSQL = strSQL0+" LCInsured a,lccont c "+wherePart+" and c.appflag='1' and c.stateflag<>'4' union "+strSQL0+" LBInsured a,lbcont c "+wherePart+" and c.appflag='1' and c.stateflag<>'4'";
	turnPage.queryModal(strSQL, PersonGrid);
}
