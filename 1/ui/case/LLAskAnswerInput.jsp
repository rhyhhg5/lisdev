<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLAskAnswerInput.js"></SCRIPT>
   <%@include file="LLAskAnswerInputInit.jsp"%>
 </head>

 <body  onload="initForm();" >
   <form action="" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    
 
     <Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerInfo);">
          </TD>
          <TD class= titleImg>
            客户列表
          </TD>
        </TR>
      </Table>

    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCustomerGrid" >
            </span>
          </TD>
        </TR>
		  </table>
		 
    </Div>
        <Div id= "divPage"  style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
<br>

            <input name="NoticeIn" style="display:'none'" class=cssButton type=button value="通知录入" onclick="inputNotice()">
            <input name="AskIn" style="display:'none'"  class=cssButton type=button value="咨询录入" onclick="inputConsult()">
            
            <!--隐藏域-->
            <input name ="fmtransact" type="hidden">
            
            <Input type='hidden'  name=AvaiFlag >
		 	<Input type='hidden'  name=NotAvaliReason >
       	    <Input type='hidden'  name=LogTime >
       	    <Input type='hidden'  name=LogDate >
       	    <Input type='hidden'  name=AnswerMode >		
			<Input type='hidden'  name=Operator >
			<Input type='hidden'  name=MngCom >
			<Input type='hidden'  name=MakeDate >
			<Input type='hidden'  name=MakeTime >
			<Input type='hidden'  name=ModifyDate >
			<Input type='hidden'  name=ModifyTime >
			<Input type='hidden'  name=OtherNo >
			<Input type='hidden'  name=OtherNoType >
			<Input type='hidden'  name=LogState >
			<Input type='hidden'  name=SwitchDate >
			<Input type='hidden'  name=SwitchTime >
			<Input type='hidden'  name=ReplyFDate >
			<Input type='hidden'  name=DealFDate >
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
