<%
//Name：ICaseCureSaveAll.jsp
//Function：账单录入完毕
//Date：2006-7-14
//Author  ：Xx
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>
<%
  System.out.println("begin......");
  RegisterBL tRegisterBL   = new RegisterBL();
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  VData tVData = new VData();
  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
  String CaseNo = request.getParameter("CaseNo");
  tLLCaseSchema.setCaseNo(CaseNo);
  
  try
  {
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tG);
    if(tRegisterBL.submitData(tVData,"RECEIPT||UPDATE"))
    {
      Content = "保存成功";
      FlagStr = "Succ";
    }
    else
    {
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="Fail")
  {
    tError = tRegisterBL.mErrors;
    if (tError.needDealError())
    {
      Content = " 保存失败，原因是："+tError.getFirstError();
      FlagStr = "Fail";
      tVData.clear();
    }
  }
  else
  {
    Content = "保存成功！";
    FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>