<%
/*******************************************************************************
 * Name     :ICaseInit.jsp
 * Function :初始化“立案－费用明细信息”的程序
 * Author   :LiuYansong
 * Date     :2003-7-23
 */
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
  var turnPage = new turnPageClass();
function initForm()
{
  try
  {
  
  initAffixGrid();

  
  }
  catch(re)
  {
    alert("ICaseCureInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}




function initAffixGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="20px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="材料序号";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=0;


    iArray[2]= new Array("类型","60","60","0");
    iArray[3]= new Array("录入标记","60","60","0");
   
    AffixGrid = new MulLineEnter("fm","AffixGrid");
    AffixGrid.mulLineCount = 1;
    AffixGrid.displayTitle = 1;
    AffixGrid.locked = 1;
    AffixGrid.canChk =1	;
    AffixGrid.canSel =1	;
    AffixGrid.hiddenPlus=1;  
    AffixGrid.hiddenSubtraction=1;
    AffixGrid.loadMulLine(iArray);
    AffixGrid.selBoxEventFuncName = "ShowRela";
  }
  catch(ex)
  {
    alter(ex);
  }
}
</script>
