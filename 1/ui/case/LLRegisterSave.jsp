<%
//Name    ：RegisterSave.jsp
//Function：对立案主界面的信息保存
//Author   :LiuYansong
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
    <%@page import="com.sinosoft.utility.*"%>
    <%@page import="com.sinosoft.lis.schema.*"%>
    <%@page import="com.sinosoft.lis.vschema.*"%>
    <%@page import="com.sinosoft.lis.db.*"%>
    <%@page import="com.sinosoft.lis.vdb.*"%>
    <%@page import="com.sinosoft.lis.sys.*"%>
    <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.llcase.*"%>
    <%@page import="com.sinosoft.lis.vbl.*"%>
    <%@page contentType="text/html;charset=GBK" %>

<%

    LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
    LLRegisterSet tLLRegisterSet = new LLRegisterSet();

   LLCaseSchema tLLCaseSchema = new LLCaseSchema();
 

    LLAffixSchema tLLAffixSchema=new LLAffixSchema();
    LLAffixSet tLLAffixSet=new LLAffixSet();
    
    LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
    LLAppClaimReasonSet tLLAppClaimReasonSet = new LLAppClaimReasonSet();

    RegisterUI tRegisterUI   = new RegisterUI();
  
    CErrors tError = null;
    String transact = "INSERT";

    String tRela  = "";
    String FlagStr = "";
    String Content = "";

    //处理转意字符
    String Path = application.getRealPath("config//Conversion.config");
	//tLLRegisterSchema.setAccidentReason(StrTool.Conversion(request.getParameter("AccidentReason"),Path));
	tLLRegisterSchema.setAccidentCourse(StrTool.Conversion(request.getParameter("AccidentCourse"),Path));
	//tLLRegisterSchema.setRemark(StrTool.Conversion(request.getParameter("Remark"),Path));
    tLLRegisterSchema.setAccidentSite(StrTool.Conversion(request.getParameter("AccidentSite"),Path));
    //tLLRegisterSchema.setRgtReason(StrTool.Conversion(request.getParameter("RgtReason"),Path));
   
    //tLLRegisterSchema.setRptNo(request.getParameter("RptNo"));
    tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
    tLLRegisterSchema.setRgtType(request.getParameter("RgtType"));
    tLLRegisterSchema.setAppAmnt(request.getParameter("AppAmnt"));
    //tLLRegisterSchema.setCaseGetMode(request.getParameter("CaseGetMode"));
    tLLRegisterSchema.setGetMode(request.getParameter("paymode"));
    tLLRegisterSchema.setRgtantName(request.getParameter("RgtantName"));
    System.out.println("fdj................................");
    tLLRegisterSchema.setRgtObj("1"); //个人客户
    tLLRegisterSchema.setRgtObjNo(request.getParameter("InsuredNo"));
     System.out.println("fdj................................"+tLLRegisterSchema.getRgtObjNo()); 
    tLLRegisterSchema.setRgtantSex(request.getParameter("Sex"));
    tLLRegisterSchema.setRelation(request.getParameter("Relation"));
    tLLRegisterSchema.setRgtantAddress(request.getParameter("RgtantAddress"));
    tLLRegisterSchema.setRgtantPhone(request.getParameter("RgtantPhone"));
    tLLRegisterSchema.setRgtDate(request.getParameter("RgtDate"));
   // tLLRegisterSchema.setDeathDate    (request.getParameter("DeathDate"));
    tLLRegisterSchema.setAccidentSite (request.getParameter("AccidentSite"));
    tLLRegisterSchema.setAccidentDate (request.getParameter("AccidentDate"));
    
   // tLLRegisterSchema.setAccidentDate(request.getParameter("AccidentDate"));
   // tLLRegisterSchema.setAgentCode(request.getParameter("AgentCode"));
   // tLLRegisterSchema.setAgentGroup(request.getParameter("AgentGroup"));
    //tLLRegisterSchema.setHandler(request.getParameter("Handler"));
    //tLLRegisterSchema.setMngCom(request.getParameter("MngCom"));
    //tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
    //tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setIDNo(request.getParameter("IDNo"));
    tLLRegisterSchema.setIDType(request.getParameter("IDType"));
    tLLRegisterSchema.setApplyerType(request.getParameter("ApplyerType"));
    //tLLRegisterSchema.setHandler1(request.getParameter("Handler1"));
    //tLLRegisterSchema.setHandler1Phone(request.getParameter("Handler1Phone"));
    //tLLRegisterSchema.setAccName(request.getParameter("AccName"));
   // tLLRegisterSet.add(tLLRegisterSchema);
    
    tLLCaseSchema.setCustomerNo   (request.getParameter("CustomerNo"));
    tLLCaseSchema.setCustomerName (request.getParameter("CustomerName"));
    tLLCaseSchema.setIDType       (request.getParameter("IDType"));
    tLLCaseSchema.setIDNo         (request.getParameter("IDNo"));
    tLLCaseSchema.setOtherIDType(request.getParameter("OtherIDType"));
    tLLCaseSchema.setOtherIDNo(request.getParameter("OtherIDNo"));
    tLLCaseSchema.setDeathDate    (request.getParameter("DeathDate"));
    tLLCaseSchema.setAccidentSite (request.getParameter("AccidentSite"));
    tLLCaseSchema.setAccidentDate (request.getParameter("AccidentDate"));
    tLLCaseSchema.setPhone        (request.getParameter("Phone"));
    tLLCaseSchema.setPostalAddress(request.getParameter("PostalAddress"));
    

/*
    String[] tChk = request.getParameterValues("InpCaseGridChk");
    String[] strNumberCase=request.getParameterValues("CaseGridNo");
    String[] strCaseNo=request.getParameterValues("CaseGrid1");
    String[] strCustomerNo=request.getParameterValues("CaseGrid2");
    String[] strCustomerName=request.getParameterValues("CaseGrid3");
    String[] strCustomerSex=request.getParameterValues("CaseGrid4");
    String[] strIDType=request.getParameterValues("CaseGrid5");
    String[] strIDNo=request.getParameterValues("CaseGrid6");
    String[] strCustomerAge=request.getParameterValues("CaseGrid7");
    String[] strAccidentType=request.getParameterValues("CaseGrid8");
    String[] strSubRptNo=request.getParameterValues("CaseGrid9");

	  int intLength=0;
	  if(strNumberCase!=null)
	  intLength=strNumberCase.length;
    for(int i=0;i<intLength;i++)
    {
      if(tChk[i].equals("0"))
        continue;
      tLLCaseSchema=new LLCaseSchema();
      tLLCaseSchema.setCustomerNo(strCustomerNo[i]);
      tLLCaseSchema.setCustomerName(strCustomerName[i]);
      tLLCaseSchema.setCustomerSex(strCustomerSex[i]);
      tLLCaseSchema.setIDType(strIDType[i]);
      tLLCaseSchema.setIDNo(strIDNo[i]);
      tLLCaseSchema.setCustomerAge(strCustomerAge[i]);
      tLLCaseSchema.setIDType(strIDType[i]);
      tLLCaseSchema.setAccidentType(strAccidentType[i]);
      tLLCaseSchema.setSubRptNo(strSubRptNo[i]);
      tLLCaseSet.add(tLLCaseSchema);
    }
*/
    String[] strNumberAffix=request.getParameterValues("AffixGridNo");
  
    String[] strAffixType=request.getParameterValues("AffixGrid1");
    String[] strAffixCode=request.getParameterValues("AffixGrid2");
    String[] strAffixName=request.getParameterValues("AffixGrid3");
    String[] strSupplyDate=request.getParameterValues("AffixGrid4");
    String[] strValiFlag=request.getParameterValues("AffixGrid5");
    String[] strAffixNo=request.getParameterValues("AffixGrid6");
    
	 int	intLength=0;
	  if(strNumberAffix!=null)
	  intLength=strNumberAffix.length;
  	for(int i=0;i<intLength;i++)
    {
			tLLAffixSchema=new LLAffixSchema();
			tLLAffixSchema.setAffixCode(strAffixCode[i]);
			tLLAffixSchema.setAffixName(strAffixName[i]);
			tLLAffixSchema.setAffixType(strAffixType[i]);
			tLLAffixSchema.setSupplyDate(strSupplyDate[i]);
			tLLAffixSchema.setValiFlag(strValiFlag[i]);
			tLLAffixSchema.setAffixNo(strAffixNo[i]);
			tLLAffixSet.add(tLLAffixSchema);

    }
    
     String[] strAppReasonNo=request.getParameterValues("AppReasonGridNo");
  
    String[] strReasonCode=request.getParameterValues("AppReasonGrid1");
    String[] strReason=request.getParameterValues("AppReasonGrid2");
    
	intLength=0;
	if(strAppReasonNo!=null)
	 intLength=strAppReasonNo.length;
  	for(int i=0;i<intLength;i++)
    {
			tLLAppClaimReasonSchema=new LLAppClaimReasonSchema();
			tLLAppClaimReasonSchema.setReasonCode(strReasonCode[i]);
			tLLAppClaimReasonSchema.setReason(strReason[i]);			
			
			tLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);

    }
    
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");

  	VData tVData = new VData();
    try
    {
   		tVData.addElement(tLLRegisterSchema);

 		tVData.addElement(tLLCaseSchema);
   		tVData.addElement(tLLAffixSet);
   		tVData.addElement(tLLAppClaimReasonSet);
   		tVData.addElement(tG);
      tRegisterUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = transact+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }

    String rgtNo="";
    if (FlagStr=="")
    {
      tError = tRegisterUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 保存成功";
        FlagStr = "Succ";
        tVData.clear();
        tVData = tRegisterUI.getResult();
       
        String a_RgtName="";
        String a_HandlerName = "";
        LLRegisterSchema ttLLRegisterSchema =(LLRegisterSchema)tVData.getObjectByObjectName("LLRegisterSchema",0) ;
        rgtNo = ttLLRegisterSchema.getRgtNo();
      //  a_RgtName=(String)tVData.get(0);
    //    a_HandlerName=(String)tVData.get(1);
       // LLRegisterSchema ttLLRegisterSchema = new LLRegisterSchema();
       // LLRegisterBLSet yLLRegisterBLSet = new LLRegisterBLSet();
      //  yLLRegisterBLSet.set((LLRegisterBLSet)tVData.getObjectByObjectName("LLRegisterBLSet",0));
        //ttLLRegisterSchema.setSchema(yLLRegisterBLSet.get(1));
     
        //System.out.println("RegisterNo==="+ttLLRegisterSchema.getRgtNo());
        //System.out.println("经办人是======"+ttLLRegisterSchema.getHandler());
      }
      }
       
%>
 <script language="javascript">
  
  parent.fraInterface.fm.all("RgtNo").value="<%=rgtNo%>";
  
	
	
	</script>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
        </script>
</html>