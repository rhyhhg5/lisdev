<%
//Name:ReportInit.jsp
//function：
//author: 
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 
%>
<script language="JavaScript">
function initInpBox1()
{
  try
  {
    fm.all('101').value = "";    fm.all('101r').value = "";
    fm.all('102').value = "";    fm.all('102r').value = "";
    fm.all('103').value = "";    fm.all('103r').value = "";
    fm.all('104').value = "";    fm.all('104r').value = "";
    fm.all('105').value = "";    fm.all('105r').value = "";
    fm.all('106').value = "";    fm.all('106r').value = "";
    fm.all('107').value = "";    fm.all('107r').value = "";
    fm.all('108').value = "";    fm.all('108r').value = "";
    fm.all('109').value = "";    fm.all('109r').value = "";
    fm.all('110').value = "";    fm.all('110r').value = "";
    fm.all('111').value = "";    fm.all('111r').value = "";
    fm.all('112').value = "";    fm.all('112r').value = "";
    fm.all('113').value = "";    fm.all('113r').value = "";
    fm.all('114').value = "";    fm.all('114r').value = "";
    fm.all('115').value = "";    fm.all('115r').value = "";
    fm.all('116').value = "";    fm.all('116r').value = "";
    fm.all('117').value = "";    fm.all('117r').value = "";
    fm.all('118').value = "";    fm.all('118r').value = "";
    fm.all('119').value = "";    fm.all('119r').value = "";
    fm.all('120').value = "";    fm.all('120r').value = "";
    fm.all('121').value = "";    fm.all('121r').value = "";
    fm.all('122').value = "";    fm.all('122r').value = "";
    fm.all('123').value = "";    fm.all('123r').value = "";
    fm.all('124').value = "";    fm.all('124r').value = "";
    fm.all('125').value = "";    fm.all('125r').value = "";
    fm.all('151').value = "";    fm.all('151r').value = "";
    fm.all('152').value = "";    fm.all('152r').value = "";
    fm.all('153').value = "";    fm.all('153r').value = "";
    fm.all('154').value = "";    fm.all('154r').value = "";
    fm.all('155').value = "";    fm.all('155r').value = "";
    fm.all('156').value = "";    fm.all('156r').value = "";
    fm.all('157').value = "";    fm.all('157r').value = "";
    fm.all('158').value = "";    fm.all('158r').value = "";
    fm.all('159').value = "";    fm.all('159r').value = "";
    fm.all('160').value = "";    fm.all('160r').value = "";
    fm.all('161').value = "";    fm.all('161r').value = "";
    fm.all('DiagSum').value = "";
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initInpBox2()
{
  try
  {
    fm.all('201').value = "";    fm.all('201r').value = "";
    fm.all('202').value = "";    fm.all('202r').value = "";
    fm.all('203').value = "";    fm.all('203r').value = "";
    fm.all('204').value = "";    fm.all('204r').value = "";
    fm.all('205').value = "";    fm.all('205r').value = "";
    fm.all('206').value = "";    fm.all('206r').value = "";
    fm.all('207').value = "";    fm.all('207r').value = "";
    fm.all('208').value = "";    fm.all('208r').value = "";
    fm.all('209').value = "";    fm.all('209r').value = "";
    fm.all('210').value = "";    fm.all('210r').value = "";
    fm.all('211').value = "";    fm.all('211r').value = "";
    fm.all('212').value = "";    fm.all('212r').value = "";
    fm.all('213').value = "";    fm.all('213r').value = "";
    fm.all('214').value = "";    fm.all('214r').value = "";
    fm.all('215').value = "";    fm.all('215r').value = "";
    fm.all('216').value = "";    fm.all('216r').value = "";
    fm.all('217').value = "";    fm.all('217r').value = "";
    fm.all('218').value = "";    fm.all('218r').value = "";
    fm.all('219').value = "";    fm.all('219r').value = "";
    fm.all('220').value = "";    fm.all('220r').value = "";
    fm.all('221').value = "";    fm.all('221r').value = "";
    fm.all('222').value = "";    fm.all('222r').value = "";
    fm.all('223').value = "";    fm.all('223r').value = "";
    fm.all('224').value = "";    fm.all('224r').value = "";
    fm.all('225').value = "";    fm.all('225r').value = "";
    fm.all('226').value = "";    fm.all('226r').value = "";
    fm.all('HospitalSum').value = "";
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initCheckGrid();
    initAccountGrid();
    
    easyQuery();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//账单费用项目明细信息
function initIFeeCaseCureGrid()
{
  
  var strsqlinit="1 and code like #"+fm.all('FeeType').value+"%#"; 
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         	//列宽
    iArray[0][2]=10;          		//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许
              	
    iArray[1]=new Array();
    iArray[1][0]="费用项目代码"; 
    iArray[1][1]="100px";            
    iArray[1][2]=100;            
    iArray[1][3]=1;              			
//    iArray[1][4]="llfeetypes"; 
//    iArray[1][5]="1|2|8"; 
 //   iArray[1][6]="0|1|2"; 
 //   iArray[1][15]=fm.all("FeeAtti").value;
//    iArray[1][16]=fm.all("FeeType").value; 
//    iArray[1][19]="1";

    iArray[2]=new Array();
    iArray[2][0]="费用项目名称"; 
    iArray[2][1]="100px";      
    iArray[2][2]=100;          
    iArray[2][3]=1;

    iArray[3]=new Array();
    iArray[3][0]="费用金额";    
    iArray[3][1]="100px";
    iArray[3][2]=100;    
    iArray[3][3]=1;
    
    iArray[4]=new Array();
    iArray[4][0]="社保外费用"; 
    iArray[4][1]="100px";      
    iArray[4][2]=50;          
    iArray[4][3]=1;

    iArray[5]=new Array();
    iArray[5][0]="先期给付";    
    iArray[5][1]="100px";
    iArray[5][2]=50;    
    iArray[5][3]=1;
    
    iArray[6]=new Array();
    iArray[6][0]="不合理费用";    
    iArray[6][1]="100px";
    iArray[6][2]=50;    
    iArray[6][3]=1;
    
    iArray[7]=new Array();
    iArray[7][0]="有效标记编号";    
    iArray[7][1]="100px";
    iArray[7][2]=100;    
    iArray[7][3]=3;

    iArray[8]=new Array();
    iArray[8][0]="给付责任";    
    iArray[8][1]="100px";
    iArray[8][2]=50;    
    iArray[8][3]=0;
    

    IFeeCaseCureGrid = new MulLineEnter( "fm" , "IFeeCaseCureGrid" ); 
    //这些属性必须在loadMulLine前
//    IFeeCaseCureGrid.mulLineNum=2;
    IFeeCaseCureGrid.mulLineCount = 0;   
    IFeeCaseCureGrid.displayTitle = 1;
    IFeeCaseCureGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    IFeeCaseCureGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
//		IFeeCaseCureGrid.addEventFuncName="addFeeTypeLine"; 
//			BnfGrid.addEventFunParm="['spanBnfGrid0']"; //传入的参数,可以省略该项      }
    IFeeCaseCureGrid.loadMulLine(iArray);      
  }
  catch(ex)
  {
    alert(ex.message);
  }
}


function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=1;
      
    iArray[1]=new Array("团体批次号","110px","0","0");
    iArray[2]=new Array("理赔号","110px","0","0");
    iArray[3]=new Array("姓名","50px","0","0");
    iArray[4]=new Array("案件状态代码","0px","0","3");
    iArray[5]=new Array("客户号","0px","0","3");
    iArray[6]=new Array("申请日期","70px","0","0");
    iArray[7]=new Array("结案日期","70px","0","0");;
    iArray[8]=new Array("案件状态","55px","0","0");
    iArray[9]=new Array("处理时效(天)","0px","0","3");
    iArray[10]=new Array("idno","0px","0","3");
    iArray[12]=new Array("操作员姓名","60px","20","0");
    iArray[13]=new Array("操作员代码","60px","20","0");
    iArray[11]=new Array("管理机构","60px","20","0");


    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =5;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canSel =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid. selBoxEventFuncName = "DealFeeInput";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initAccountGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
      
    iArray[1]=new Array("类别","40px","0","0");
    iArray[2]=new Array("日期","70px","0","0");
    iArray[3]=new Array("账单金额","70px","0","0");
    iArray[4]=new Array("诊断","150px","0","0");
    iArray[5]=new Array("事件","200px","0","0");
    iArray[6]=new Array("理赔金额","150px","0","0");
    iArray[7]=new Array("feeno","0px","0","3");;
    iArray[8]=new Array("1","90px","0","3");


    AccountGrid = new MulLineEnter("fm","AccountGrid");
    AccountGrid.mulLineCount =1;
    AccountGrid.displayTitle = 1;
    AccountGrid.locked = 1;
    AccountGrid.canSel =1;
    AccountGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    AccountGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    AccountGrid. selBoxEventFuncName = "showDetail";
    AccountGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

 </script>