<html>
	<%
	//Name：LLSuveryReplyList.jsp
	//Function：调查员回复列表（调查员信箱）
	//Date：2005-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="java.util.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String Operator=tG.Operator;
	String Comcode=tG.ManageCom;

	String CurrentDate= PubFun.getCurrentDate();
	String AheadDays="-30";
	FDate tD=new FDate();
	Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
	FDate fdate = new FDate();
	String afterdate = fdate.getString( AfterDate );
	%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLSuveryReplyList.js"></SCRIPT>
		<%@include file="LLSuveryReplyListInit.jsp"%>
		<script language="javascript">
			function initDate(){
				fm.SurveyStartDate.value="<%=afterdate%>";
				fm.SurveyEndDate.value="<%=CurrentDate%>";
				var usercode="<%=Operator%>";
				fm.comcode.value="<%=Comcode%>";
				fm.MngCom.value="<%=Comcode%>";
				fm.Surveyer.value=usercode;

				var strSQL="select username from llclaimuser where usercode='"+usercode+"'"
					+" union select username from llsocialclaimuser where usercode='"+usercode+"'";
				var arrResult = easyExecSql(strSQL);
				if(arrResult != null){
					fm.SurveyerName.value= arrResult[0][0];
				}
				
				//优化，自动带出机构名称
				var tSql="select name from ldcom where comcode='"+fm.MngCom.value+"'";
				var tResult = easyExecSql(tSql);
				if(tResult != null){
					fm.ComName.value= tResult[0][0];
				}
			}
		</script>
	</head>

	<body  onload="initDate();initForm();" >
		<form action="" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/InputButton.jsp"%>
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQueryPart);">
					</TD>
					<TD class= titleImg>
						调查工作查询条件
					</TD>
				</TR>
			</table>
			<Div  id= "divQueryPart" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>调查回复人</TD>
						<TD  class= input8><input class= codeno readonly name="Surveyer" onclick="Dispatch();return showCodeList('optname',[this,SurveyerName], [0,1],null,Str,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,SurveyerName], [0,1],null,Str,'1');"  onkeydown="QueryOnKeyDown()"><input class=codename name= SurveyerName></TD>
						<TD  class= title8>调查机构</TD>
						<TD  class= title8><Input class="codeno" readonly name=MngCom verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,200);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,200);"><input class=codename name=ComName></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>案件号</TD><TD  class= input8><input class= common name="OtherNo" onkeydown="QueryOnKeyDown()"></TD>
						<TD  class= title8>调查类别</TD><TD  class= input8><input class= codeno name="SurveyType" CodeData= "0|^0|即时调查^1|一般调查^2|再次调查" onClick="showCodeListEx('SurveyType',[this,SurveyTypeName],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('SurveyType',[this,SurveyTypeName],[0,1],'', '', '', 1);"><input class=codename name=SurveyTypeName></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>提起日期从</TD><TD  class= input8><input class= 'coolDatePicker' dateFormat="Short" name="SurveyStartDate" onkeydown="SurveyQuery();"></TD>
						<TD  class= title8>到</TD><TD  class= input8><input class= 'coolDatePicker' dateFormat="Short" name="SurveyEndDate" onkeydown="SurveyQuery();"></TD>
					</TR>
				</table>
			</DIV>

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSurveyList);">
					</TD>
					<TD class= titleImg>
						调查工作列表
					</TD>
				</TR>
			</table>
			<Div  id= "divSurveyList" align  = center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanCheckGrid" >
							</span>
						</TD>
					</TR>
				</table>
				<INPUT VALUE="首   页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
				<INPUT VALUE="尾   页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
			</Div>
			<br>
			<input type=button class=cssButton style="width:70px" name="AskIn" value="调查回复" onclick="SurveyReply()">
			<input type=button class=cssButton style="width:70px" name="prtRpt" value="调查报告" onclick="SurveyPrint()">
			<input type=button class=cssButton style="width:70px" name="InqFee" value="直接调查费" onclick="SurveyFee()">
			<input type=button class=cssButton style="width:70px" name="SumUp" value="调查费汇总" onclick="SumFee()">
			
			<table>
			  <TR>
			   	<TD>
			   	 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRemark);">
			   	</TD>
				<TD class= titleImg>
				   调查指导意见
				</TD>
			  </TR>
			</table>
			<div id="divRemark" style= "display: ''">
			 <table  class= common>
			     <TR  class= common>
			       <TD  class= title>留言记录</TD>
			     </TR>
			     <TR  class= common>
			       <TD  class= input>
			          <textarea name="result" cols="100%" rows="5" witdh=25% class="common" readonly="true" ></textarea>
			       </TD>
			     </TR>
			  </table>
			</Div>
			
			<!--隐藏域-->
			<Input type="hidden" class= common name="fmtransact" >
			<Input type="hidden" class= common name="tSurveyNo" >
			<Input type="hidden" class= common name="comcode" >
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
