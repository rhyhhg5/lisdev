/*******************************************************************************
 * Name: RegisterInput.js
 * Function:立案主界面的函数处理程序
 * Author:LiuYansong
 */
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var case_flag ;//判断是否选择了分案号,初始化时是1，若没有选中则是0。
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tSaveFlag = "0";
var turnPage = new turnPageClass();    
var onSelResData = 0;
/*******************************************************************************
 * Name        :main_init
 * Author      :LiuYansong
 * CreateDate  :2003-12-16
 * ModifyDate  :
 * Function    :初始化主程序
 * Parameter   :A_jsp---fm.action;Target_Flag (0--无target，1有target),T_jsp--target 对应的页面 ;Info---提示信息
 *
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info)
{
  var i = 0;
  var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action ="./"+A_jsp;
  //alert("target标志是"+Target_Flag);
  if(Target_Flag=="1")
  {
    fm.target = "./"+T_jsp;
  }
  else
  {
    fm.target = "fraSubmit";
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

/*******************************************************************************
name :showTemPolInfo()
function deal temporary Pol Info
date :2003-04-09
creator :刘岩松
*/
function showTemPolInfo()
{
  var row;
  var a_count=0;//判断选中了多少行
  var t = CaseGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = CaseGrid.getChkNo(i);
    if(varCount==true)
    {
       a_count++;
       row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert("请您先执行保存操作！");
    return;
  }
  else
  {
    var varSrc = "&CaseNo=" + CaseGrid.getRowColData(row,1);
    varSrc += "&InsuredNo=" + CaseGrid.getRowColData(row,2);
    varSrc += "&CustomerName=" + CaseGrid.getRowColData(row,3);
    varSrc += "&AccidentType=" + CaseGrid.getRowColData(row,8);
    varSrc += "&RgtNo=" + fm.RgtNo.value;
    var newWindow = window.open("./FrameMainLSBDXX.jsp?Interface=TemPolInfoInput.jsp"+varSrc,"CasePolicyInput",'width=700,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}
//处理保单明细
function showCasePolicy()
{
  var row;
  var a_count=0;//判断选中了多少行
  var t = CaseGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = CaseGrid.getChkNo(i);
    if(varCount==true)
    {
      a_count++;
      row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert("请您先执行保存操作！");
    return;
  }
  else
  {
    var varSrc = "&CaseNo=" + CaseGrid.getRowColData(row,1);
    varSrc += "&InsuredNo=" + CaseGrid.getRowColData(row,2);
    varSrc += "&CustomerName=" + CaseGrid.getRowColData(row,3);
    varSrc += "&AccidentType=" + CaseGrid.getRowColData(row,8);
    varSrc += "&RgtNo=" + fm.RgtNo.value;
    var newWindow = window.open("./FrameMainFABD.jsp?Interface=CasePolicyInput.jsp"+varSrc,"CasePolicyInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}
//费用明细信息
function showFeeInfo()
{
  var row;
  var a_count=0;//判断选中了多少行
  var t = CaseGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = CaseGrid.getChkNo(i);
    if(varCount==true)
    {
      a_count++;
      row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert("请您先执行保存操作！");
    return;
  }
  else
  {
    fm.DisplayFlag.value="0";//标志是在立案界面中进入的费用明细
    var varSrc = "&RgtNo=" +fm.RgtNo.value;
    varSrc += "&CaseNo=" + CaseGrid.getRowColData(row,1);
    varSrc += "&CustomerNo=" + CaseGrid.getRowColData(row,2);
    varSrc += "&CustomerName=" + CaseGrid.getRowColData(row,3);
    var newWindow = window.open("./FrameMainFAZL.jsp?Interface=ICaseCureInput.jsp"+varSrc,"ICaseCureInput");
  }
}
function showAccident()
{
 /*
  var row;
  var a_count=0;//判断选中了多少行
  var t = CaseGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = CaseGrid.getChkNo(i);
    if(varCount==true)
    {
      a_count++;
      row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert("请您先执行保存操作！");
    return;
  }
  else
  {
    fm.DisplayFlag.value="0";//0表示在立案中进入的事故明细
    var varSrc = "&RgtNo=" +fm.RgtNo.value;
    varSrc += "&CaseNo=" + CaseGrid.getRowColData(row,1);
    varSrc += "&CustomerNo=" + CaseGrid.getRowColData(row,2);
    varSrc +="&CustomerName=" + CaseGrid.getRowColData(row,3);
    varSrc +="&DisplayFlag="+fm.DisplayFlag.value;
    var newWindow = window.open("./FrameMainAccident.jsp?Interface=CaseAccidentInput.jsp"+varSrc,"CaseAccidentInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
  */
  var varSrc ="";
  var newWindow = window.open("./FrameMainAccident.jsp?Interface=CaseAccidentInput.jsp"+varSrc,"CaseAccidentInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}
//提交，保存按钮对应操作
//添加控制，不能对其点击两次的控制
function submitForm()
{
 

    if (confirm("您确实想保存该记录吗?"))
    {
      tSaveFlag = "1";
      var rowNum1=CaseGrid.mulLineCount;
      var rowNum2=AffixGrid.mulLineCount;
      dealCaseGetMode();
      if(mode_flag=="0")
        return;
      else
      {
        //
        //保险金领取方式
        if ((fm.CaseGetMode.value=="")||(fm.CaseGetMode.value=="null"))
        {
          alert ("请您输入保险金领取方式!");
          return;
        }
        //申请经办人
        if ((fm.Handler1.value=="")||(fm.Handler1.value=="null"))
        {
          alert ("请您输入申请经办人!");
          return;
        }
        //申请经办人电话
        if ((fm.Handler1Phone.value=="")||(fm.Handler1Phone.value=="null"))
        {
          alert ("请您输入申请经办人电话!");
          return;
        }
        //出险日期
        if ((fm.AccidentDate.value=="")||(fm.AccidentDate.value=="null"))
        {
          alert ("请您输入出险日期!");
          return;
        }
        //申请权利人性别
        if ((fm.Sex.value=="")||(fm.Sex.value=="null"))
        {
          alert ("请您输入申请权利人性别!");
          return;
        }
        //申请权利人证件类型
        if ((fm.IDType.value=="")||(fm.IDType.value=="null"))
        {
          alert ("请您输入申请权利人证件类型!");
          return;
        }
        //申请权利人证件号码
        if ((fm.IDNo.value=="")||(fm.IDNo.value=="null"))
        {
          alert ("请您输入申请权利人证件号码!");
          return;
        }
        //申请权利人身份
        if ((fm.ApplyType.value=="")||(fm.ApplyType.value=="null"))
        {
          alert ("请您输入申请权利人身份!");
          return;
        }
        //申请权利人与被保人关系
        if ((fm.Relation.value=="")||(fm.Relation.value=="null"))
        {
          alert ("请您输入申请权利人与被保人关系!");
          return;
        }
        //申请权利人电话
        if ((fm.RgtantPhone.value=="")||(fm.RgtantPhone.value=="null"))
        {
          alert ("请您输入申请权利人电话!");
          return;
        }
        //申请权利人地址
        if ((fm.RgtantAddress.value=="")||(fm.RgtantAddress.value=="null"))
        {
          alert ("请您输入申请权利人地址!");
          return;
        }

        //权利人姓名
        if ((fm.RgtantName.value=="")||(fm.RgtantName.value=="null"))
        {
          alert ("请您输入申请权利人性别!");
          return;
        }


        if(fm.BankAccNo.value!=fm.RgtantMobile.value)
        {
          alert("账号录入错误，请您从新录入！！！");
          return;
        }
        if (fm.RgtNo.value!="")
        {
          alert("该案件已经立过案,不能再保存!");
          return;
        }
        for(var j=0;j<rowNum1;j++)
        {
          var tValue1 = CaseGrid.getRowColData(j,2);
          if ((tValue1=="")||(tValue1=="null"))
          {
            alert("请您录入客户号，若没有该用户请将分立案录入框中的该条记录取消！");
            return;
          }
        }

        for(var m=0;m<rowNum2;m++)
        {
          var tValue2 = AffixGrid.getRowColData(m,1);
          if ((tValue2=="")||(tValue2=="null"))
          {
            alert("请您录入立案附件信息！");
            return;
          }
        }

        if ((fm.RgtantName=="")||(fm.RgtantName=="null"))
        {
          alert ("请您输入申请权利人姓名姓名!");
          return;
        }
        if ((fm.AccidentDate=="")||(fm.AccidentDate=="null"))
        {
          alert("请您输入出险日期!");
          return;
        }
        else
        {
          main_init("RegisterSave.jsp","0","_blank","保存");
          tSaveFlag = "0";
        }
      }
   }
    else
    {
      alert("您取消了修改操作！");
    }
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  alert();
  showInfo.close();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if (confirm("您确实想修改该记录吗?"))
  {
    var rowNum1=CaseGrid.mulLineCount;
    var rowNum2=AffixGrid.mulLineCount;
     dealCaseGetMode();

    if(fm.BankAccNo.value!=fm.RgtantMobile.value)
    {
      alert("账号录入错误，请您从新录入！！！");
      return;
    }
    for(var j=0;j<rowNum1;j++)
    {
      var tValue1 = CaseGrid.getRowColData(j,2);
      if ((tValue1=="")||(tValue1=="null"))
      {
        alert("请您录入客户号，若没有该用户请将分立案录入框中的该条记录取消！");
        return;
      }
    }
    for(var m=0;m<rowNum2;m++)
    {
      var tValue2 = AffixGrid.getRowColData(m,1);
      if ((tValue2=="")||(tValue2=="null"))
      {
        alert("请您录入立案附件信息！");
        return;
      }
    }
    if ((fm.RgtantName.value=="")||(fm.RgtantName.value=="null"))
    {
      alert ("请您输入申请权利人姓名姓名!");
      return;
    }
    if ((fm.RgtDate.value=="")||(fm.RgtDate.value=="null"))
    {
      alert("请您输入立案日期!");
      return;
    }
    if ((fm.AccidentDate.value=="")||(fm.AccidentDate.value=="null"))
    {
      alert("请您输入出险日期!");
      return;
    }
    if(fm.CaseState.value=="已撤件")
    {
      alert("该案件已撤件，不能对其进行修改！！！");
      return;
    }
    else
    {
      fm.fmtransact.value = "UPDATE||MAIN";
      main_init("RegisterUpdate.jsp","0","_blank","修改");
    }
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  window.open("FrameRegisterQuery.jsp");
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  alert("您不能删除立案信息！！！");
  return;
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function submitFormSurvery()
{
  
    var row;
    var a_count=0;//判断选中了多少行
    var t = CaseGrid.mulLineCount;//得到Grid的行数
    for(var i=0;i<t;i++)
    {
      varCount = CaseGrid.getChkNo(i);
      if(varCount==true)
      {
        a_count++;
        row=i;
      }
    }
    if(a_count>1)
    {
      alert("您只能选中一行记录！");
      return;
    }
    else if(a_count<1)
    {
      alert("请您选中一条分案记录!");
      return;
    }
    else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
    {
      alert("请您先执行保存操作！");
      return;
    }
    else
    {
      var varSrc = "&CaseNo=" + CaseGrid.getRowColData(row,1);
      varSrc += "&InsuredNo=" + CaseGrid.getRowColData(row,2);
      varSrc += "&CustomerName=" + CaseGrid.getRowColData(row,3);
      varSrc += "&RgtNo=" + fm.RgtNo.value;
      varSrc += "&StartPhase=0";
      varSrc += "&Type=1";
      var newWindow = window.open("./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
    }
  
  
}
//打印理赔申请书的函数
function PLPsqs()
{
  main_init("PLPsqs.jsp","1","f1print","打印");
  showInfo.close();
}
//连接报案查询界面的函数
function RgtQueryRpt()
{
  tSaveFlag = "Rpt";
  showInfo=window.open("FrameReportQuery.jsp");
}

//案件撤销的程序
function CaseCancel()
{
  if (confirm("您确实想修改该记录吗?"))
  {
    if(fm.RgtNo.value=="")
    {
      alert("请您先查询出要进行撤销的案件");
      return;
    }
    if(fm.RgtReason.value=="")
    {
      alert("请您先录入案件撤销原因，否则不能进行撤销！！！");
      return;
    }
    main_init("CaseCancel.jsp","0","_blank","撤销");
  }
  else
  {
    alert("您取消了案件撤销操作！！！！！");
  }
}
/*******************************************************************************
 * Name         :dealCaesGetMode
 * Author       :LiuYansong
 * CreateDate   :2003-12-16
 * ModifyDate   :
 * Function     :处理保险金领取方式的函数，若是4或7则要录入关于银行的一些信息
 *
 */
function dealCaseGetMode()
{
  mode_flag="1";
  if(fm.CaseGetMode.value=="4"||fm.CaseGetMode.value=="7")
  {
    if(fm.BankCode.value=="")
    {
      alert("请您录入开户银行的信息！！！");
      mode_flag="0";
      return;
    }
    if(fm.AccName.value=="")
    {
      alert("请您录入户名信息！！！！");
      mode_flag="0";
      return;
    }
    if(fm.BankAccNo.value=="")
    {
      alert("请您录入账户信息！！！");
      mode_flag="0";
      return;
    }
  }
}
//刘岩松添加根据报案号进行查询
/*******************************************************************************
function submitForm1()
{
  if((fm.RptNo.value=="")||(fm.RptNo.value=="null"))
  {
    alert("请您收入要查询的报案号！");
    return;
  }
  else
  {
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    initCaseGrid();
    fm.action = './RegisterQueryOut1.jsp';
    fm.submit();
  }
}
//刘岩松添加根据号码类型和号码进行查询
function submitForm2()
{
  if((fm.RgtObj.value=="")||(fm.RgtObjNo.value=="")||(fm.RgtObj.value=="null")||(fm.RgtObjNo.value=="null"))
  {
    alert("请您收入要查询的号码类型和号码！");
  }
  else
  {
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    initCaseGrid();
    fm.action = './RegisterQueryOut2.jsp';
    fm.submit(); //提交
  }
}
*/

function afterQuery( rptNo )
{
	if ( tSaveFlag=="Rpt" )
	{
		if ( rptNo!=null )
		{
		//报案信息
		var strSQL="select RptNo,RptDate"
		//+",AccidentDate,AccidentSite,AccidentReason,AccidentCourse+
		+" from llreport where rptno='"+ rptNo+"'";
		if ( EasyShowForm(strSQL,1,fm) )
		{
			fm.AccidentSite.className = "readonly";		
			fm.AccidentSite.readOnly ="true"	
		   	fm.RptDate.className = "readonly"
		   	fm.RptDate.readOnly ="true"
			fm.AccidentDate.className = "readonly";
			fm.AccidentDate.readOnly ="true"
			fm.AccidentReason.readOnly ="true"
			fm.AccidentCourse.readOnly ="true"
			fm.Remark.readOnly ="true"
			//fm..readOnly ="true";
		}
	else
		{
			fm.AccidentSite.className = "common8";			
			fm.AccidentSite.readOnly ="true"
		   	fm.RptDate.className = "common8";
		   	fm.RptDate.readOnly ="true"
			fm.AccidentDate.className = "common8";
			fm.AccidentDate.readOnly ="true"
			fm.AccidentReason.readOnly ="false"
			fm.AccidentCourse.readOnly ="false"
			fm.Remark.readOnly ="false"			
			}
		//相关客户
		strSQL = "select '',CustomerNo,CustomerName,'','','','',AccidentType,subrptno "
		 +"from LLSubReport where SubRptNo in "
		 +" ( select distinct SubRptNo from LLreportRela where rptno='" + rptNo + "')";
		 turnPage.queryModal(strSQL, CaseGrid);
	 }
	 }
	//execEasyQuery(strSQL);
	
}
function ClientQuery()
{
	if(fm.all('InsuredNo1').value!="")
	{

		var arrResult = new Array();
		var strSQL="select InsuredNo,Name,Sex,Birthday,IDType,IDNo "
		+"from LCInsured where 1=1 "
		+ getWherePart("InsuredNo", "InsuredNo1")
		+ getWherePart("Name", "CustomerName1")
		+ getWherePart("ContNo", "ContNo") 
		+ getWherePart("IDType", "IDType") 
		+ getWherePart("IDNo", "IDNo")     
		alert(strSQL);              
		arrResult=easyExecSql(strSQL);
		//alert(arrResult[0][0]);          
		if(arrResult != null)              
		{
			try{fm.all('CustomerNo1').value=arrResult[0][0]}catch(ex){alert(ex.message+"InsuredNo")}
			try{fm.all('CustomerName2').value=arrResult[0][1]}catch(ex){alert(ex.message+"Name")}
			try{fm.all('Sex').value=arrResult[0][2]}catch(ex){alert(ex.message+"Sex")}
			try{fm.all('CBirthday').value=arrResult[0][3]}catch(ex){alert(ex.message+"CBirthday")}
			try{fm.all('CIDType').value=arrResult[0][4]}catch(ex){alert(ex.message+"CIDType")}
			try{fm.all('CIDNo').value=arrResult[0][5]}catch(ex){alert(ex.message+"CIDNo")}
		}
		else
		{
			alert("没有符合条件的数据！");
		}
		strSQL="select subrptno,accdate,accplace,accidenttype,accsubject from llsubreport where customerno='"
		+fm.all('CustomerNo1').value+"' and accdate in(select add_months(accdate,-1) from llsubreport)";
		turnPage.queryModal(strSQL,InsuredEventGrid);
 		if(InsuredEventGrid.mulLineCount==0)
        {
			
        	TopDiv.style.display='';
        	alert(InsuredEventGrid.mulLineCount);
        	fm.EventFlag.value='1';
        }

	}
    else
	{
		alert("请输入查询条件！");
	}
}
/*点击事件原因点选按钮*/
function onSelRes()
{
	var row = AppReasonGrid.getSelNo()-1;
	onSelResData=AppReasonGrid.getRowColData(row,1);
}

function openAffix()
{
	  var varSrc ="";
	var len =AppReasonGrid.getSelNo() - 1;
	if(len<0)
	{
		alert("请选中事件！");
	}
else
	{
		var Reason = AppReasonGrid.getRowColData(len,1);
	  showInfo = window.open("./CaseAffixMain.jsp?LoadFlag="+fm.LoadFlag.value+"&CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.GrpRgtNo.value+"&Reason="+Reason);
	} 
}
function ReasonSave()
{
	if(fm.GrpRgtNo.value==null||fm.GrpRgtNo.value=="")
	{
		alert("请输入客户数据");
	}
	else
	{
		alert(1);
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		alert(2);
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action ="./ReasonSave.jsp";
		fm.submit();
	}
}

function afterAffix(ShortCountFlag,SupplyDateResult)
{
	alert("afterAffix");
	if(ShortCountFlag!=1)
	{
		fm.AffixGetDate.value=SupplyDateResult;
	}
}
