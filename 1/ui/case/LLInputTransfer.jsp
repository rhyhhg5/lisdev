<html>
	<%
	//Name：LLInputTransfer.jsp
	//Function：录入人员变更
	//Date：2007-03-14 16:49:22
	//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String Operator=tG.Operator;
	String Comcode=tG.ManageCom;
	String Handler= request.getParameter("Handler");
	String CurrentDate= PubFun.getCurrentDate();
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);
  String AheadDays="-30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
	%>

	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLInputTransfer.js"></SCRIPT>
		<%@include file="LLInputTransferInit.jsp"%>
		
		<script language="javascript">
		function initDate(){
		  fm.RgtDateS.value="<%=afterdate%>";
      fm.RgtDateE.value="<%=CurrentDate%>";
      fm.OrganCode.value="<%=Comcode%>";
			var Handler="<%=Handler%>";
			var Operator="<%=Operator%>";
		}
		</script>
	</head>
	<body  onload="initDate();initForm();">
		<form action="./LLInputTransferSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<TR>
					<TD><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQuery);"></TD>
					<TD class= titleImg>查询条件</TD>
				</TR>
			</table>
			<Div  id= "divQuery" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD class= title>管理机构</TD>
						<TD class= input><Input class= "codeno"  name=OrganCode onkeydown="QueryOnKeyDown();" ondblclick="getstr();return showCodeList('comcode',[this], [0],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode', [this], [0],null,str,'1');" ><input class=codename name=ComName></TD>
						<TD class= title>当前录入人员</TD>
						<TD class= input><Input class= "codeno"  name=Operator onkeydown="QueryOnKeyDown();" ondblclick="return showCodeList('llinputer',[this, optname], [0, 1]);" onkeyup="return showCodeListKey('llinputer', [this, optname], [0, 1]);" ><Input class=codename readonly name=optname></TD>
						<TD class= title>团体批次号</TD>
						<TD class= input><Input class=common name=RgtNo onkeydown="QueryOnKeyDown()"></TD>
					</TR>
					<TR  class= common>
						<TD class= title>理赔号码</TD>
						<TD class= input><Input class=common8 name=CaseNo onkeydown="QueryOnKeyDown()"></TD>
						<TD class= title>客户号</TD>
						<TD class= input><Input class=common name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
						<TD class= title>客户名称</TD>
						<TD class= input><Input class=common name=CustomerName onkeydown="QueryOnKeyDown()"></TD>
					</TR>
					<TR  class= common>
          	<TD  class= title8>受理日期从</TD>
          	<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
          	<TD  class= title8>到</TD>
          	<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
	        </TR>
				</table>
			</Div>
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseList);">
					</TD>
					<TD class= titleImg>
						待录入案件列表
					</TD>
				</TR>
			</table>

			<Div  id= "divCaseList" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanCheckGrid" >
							</span>
						</TD>
					</TR>
				</table>

				<Div  align=center style= "display: '' ">
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
				</Div>
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>录入人员</TD>
						<TD class= input><Input class= "codeno"  name=llusercode onkeydown="QueryOnKeyDown();" ondblclick="return showCodeList('llinputer',[this, llusername], [0, 1]);" onkeyup="return showCodeListKey('llinputer', [this, llusername], [0, 1]);" ><Input class=codename readonly name=llusername></TD>
						<TD></TD><TD></TD></TR>
							<TR  class= common8>
								<TD  class= title8   >备注</TD>
								<TD >
									<textarea name="Remark" cols="60%"  class="common"  ></textarea>
								</TD>
								<TD >
								<input type="button" class=cssButton name="Send" value="人工变更" onclick="submitForm()"> </TD>
							</TD>
						</TR>

						

				</table>
				<hr>
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClaimerInfo);">
						</TD>
						<TD class= titleImg>
							录入人员工作量统计
						</TD>
					</TR>
				</table>
				<Div  id= "divClaimerInfo" style= "display: ''">
					<table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanClaimerGrid" >
								</span>
							</TD>
						</TR>
					</table>
				</Div>
				<Div  align=center style= "display: '' ">
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
				</Div>
				<Input type=hidden  class=code name="Hand" >
			</form>
			<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

		</body>
	</html>
