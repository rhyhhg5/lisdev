<html>
<%
//Name:RegisterInput.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author ：LiuYansong
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
     
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LLApealInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLApealInputInit.jsp"%>
</head>
<body  onload="initForm();">
  <form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <%@include file="CaseTitle.jsp"%>
     <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,InsuredEvent);">
    	</td>
    	<td class= titleImg>
    	 原案件查询 
    	</td>
    </tr>
    
      </table>
<table  class= common>
<TR  class= common8>
<TD  class= title8>理赔号</TD><TD  class= input8><Input class=common name="CustomerNoQ" ></TD>
<TD  class= title8>客户号</TD><TD  class= input8><Input class=common name="CustomerTypeQ"  ></TD>
<TD  class= title8>客户姓名</TD><TD  class= input8><Input class=common name="ContNo" ></TD>
</TR>
<TR  class= common8>
<TD  class= title8>证件类型</TD><TD  class= input8><Input class=code name="IDType"  ></TD>
<TD  class= title8>证件号码</TD><TD  class= input8><Input class=common name="CustomerTypeQ"  ></TD>
<TD  class= title8><input class=cssButton type=button value="客户查询" onclick="ClientQuery()"></td>
</TR>
</table>


    <table>
      <TR>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegisterInfo);">
        </td>
        <td class= titleImg>
          个人申诉信息
        </TD>
    	</TR>
    </table>

    <Div id= "divRegisterInfo" style= "display: ''">
    <table class=common>
       <TR  class= common>
         <TD  class= title8>
            分类
          </TD>
          <TD  class= input8>
            <Input class=code name=RgtantName >
          </TD>
          
        <TD  class= title8>
            申诉人姓名
          </TD>
          <TD  class= input8>
            <Input class= common name=RgtantName >
          </TD>
     
          <TD  class= title8>
            申诉人性别
          </TD>
          <TD  class= input8>
          	<Input class="code" name=Sex verify="性别|NOTNULL" CodeData="0|^0|男^1|女^2|不详"
              ondblClick="showCodeListEx('SexRegister',[this],[0,1,2,3]);"
              onkeyup="showCodeListKeyEx('SexObjRegister',[this],[0,1,2,3]);">
        	</TD>
       	    </TR>
       	<TR  class= common>
        	<TD class= title8>
        		申诉人证件类型
        	</TD>
        	<TD class= input8>
        		<Input class="code" name=IDType ondblclick="return showCodeList('IDType',[this]);"
              onkeyup="return showCodeListKey('IDType',[this]);" >
        	</TD>
     
        	<TD class= title8>
        		申诉人证件号码
        	</TD>
        	<TD class= input8>
        		<Input class= common name=IDNo>
        	</TD>
          <TD  class= title8>
            申诉人与被保人关系
          </TD>
          <TD  class= input8>
            <Input class="code" name=Relation
              ondblclick="return showCodeList('Relation',[this]);"
              onkeyup="return showCodeListKey('Relation',[this]);" >
          </TD>
            </TR>

       	<TR  class= common>
          <TD  class= title8>
            申诉人电话
          </TD>
          <TD  class= input8>
            <Input class= common name=RgtantPhone >
          </TD>
       
          <TD  class= title8>
            申诉人地址
          </TD>
          <TD  class= input8>
            <Input class= common name=RgtantAddress >
          </TD>
	  <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common name="" >
          </TD>
         
          </tr>
          	<TR  class= common>
	  <TD  class= title8>
            回执发送方式
          </TD>
          <TD  class= input8>
            <Input class= code name=ReturnMode >
          </TD>
            <TD  class= title8>
            申诉原因
          </TD>
          <TD  class= input8>
            <Input class=code name=AppealReason >
          </TD>
          </tr>
          
    <TR  class= common>
    
	<TD  class= title colspan="6">申诉原因描述 </TD>
	</TR>
	<TR  class= common>
		<TD  class= input colspan="6">
		    <textarea name="AccidentReason" cols="100%" rows="3" witdh=25% class="common"></textarea>
		</TD>
	</TR>
    </table>
    </Div>

<!--
 <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,AppReason);">
    	</td>
    	<td class= titleImg>
    	 申诉、错误原因
    	</td>
    </tr>
      </table>
      	<Div  id= "AppReason" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanApealReasonGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
	 -->
	   <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClientInfo);">
    	</td>
    	<td class= titleImg>
    	 客户信息 
    	</td>
    </tr>
    </table>
    <div id= "divClientInfo" style= "display: ''" >
<table  class= common>
<TR  class= common8>
<TD  class= title8>客户号码</TD><TD  class= input8><Input class= readonly name="CustomerNo" readonly></TD>
<TD  class= title8>客户名称</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
<TD  class= title8>客户性别</TD><TD  class= input8><Input class= readonly name="CustomerType" readonly></TD>
</TR>
<TR  class= common8>
<TD  class= title8>出生日期</TD><TD  class= input8><Input class= readonly name="CustomerNo"   readonly></TD>
<TD  class= title8>证件类型</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
<TD  class= title8>证件号码</TD><TD  class= input8><Input class= readonly name="CustomerType" readonly></TD>
</TR>
<TR  class= common8>
<TD  class= title8>工作单位</TD><TD  class= input8><Input class= readonly name="CustomerNo"   readonly></TD>
<TD  class= title8>联系地址</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
<TD  class= title8>联系电话</TD><TD  class= input8><Input class= readonly name="CustomerType" readonly></TD>

<TR  class= common8>    
<TD  class= title8>
    	出险开始日期
  	</TD>

  	<TD  class= input8>
    	<Input class="readonly" readonly  name=AccStartDate >
  	</TD>   
 <TD  class= title8>
            	出险结束日期
          	</TD>
          	<TD  class= input8>
            	<input class="readonly" readonly  name="AccidentDate"  >
          	</TD>        
  	  	  
            <TD  class= title8>死亡日期</TD><TD  class= input8><Input  class="readonly" readonly  name="DeathDate"></TD>
  
       		
</TR>
<TR  class= common8>
<TD  class= title8>
            	出险地点
          	</TD>
      
          	<TD  class= input8>
            	<Input class="readonly" readonly  name=AccidentSite >
          	</TD> </TR>
	<TR  class= common>
	<TD  class= title colspan="6">事故经过描述</TD>
	</TR>
	<TR  class= common>
		<TD  class= input colspan="6">
		    <textarea name="AccidentReason" readonly cols="100%" rows="3" witdh=25% class="common"></textarea>
		</TD>
	</TR>
	</table>
   	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanAppReasonGrid" >
					</span>
				</td>
			</tr>
   </table>
   <div >
   <table class= common>
   <tr class= common8>
   <TD  class= title>
         
     </TD>
     <TD  class= input>
     </TD>
    <td class=title>      
   	</td>
     <TD  class= title>
            材料齐备日期
     </TD>
     <TD  class= input>
            <Input class="readonly" readonly name=AffixGetDate >
     </TD>
    <td class=input>      
   		<input class=cssButton type=button value="申请材料选择" onclick="openAffix()">
   </td>
   </tr>
   </table>
   </div>

<hr/>
    <Div  id= "divnormalquesbtn" style= "display: ''" align= right>
  <!--
    <input class=cssButton type=button value="出险受理信息" onclick="InccidentInput()">
    -->
    <input class=cssButton type=button value="申诉保存" onclick="CaseCancel()">
    <input class=cssButton  type=button value="打印理赔受理回执" onclick="PLPsqs()">
    </Div>

    <hr/>

    </Div>
   
  

 <input type=hidden id="fmtransact" name="fmtransact">
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>