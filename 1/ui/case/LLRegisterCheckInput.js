/*******************************************************************************
* Name: LLRegisterCheckInput.js
* Function:检录界面的函数处理程序
* Author:wujs,Xx
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var case_flag ;//判断是否选择了分案号,初始化时是1，若没有选中则是0。
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();   

//#1769 案件备注信息的录入和查看功能 add by Houyd
function showCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}
	//#1769 案件备注信息的录入和查看功能 自动弹出页面只弹一次
	//获取session中的ShowCaseRemarkFlag
	var tShowCaseRemarkFlag = fm.ShowCaseRemarkFlag.value;
	//alert(tShowCaseRemarkFlag);
	if(tShowCaseRemarkFlag == "" || tShowCaseRemarkFlag == null || tShowCaseRemarkFlag == "null"){
		//tShowCaseRemarkFlag = "1";//准备存入session的标记:0-未弹出；1-已弹出
		//alert(tShowCaseRemarkFlag);		
	}else{
		return ;
	}	
	var tSql = "select 1 from llcaseremark where caseno='"+aCaseNo+"' with ur";
	var tResult = easyExecSql(tSql);
	if(tResult != null){
		pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  		showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
  	}	
	
	var LowerStr = "select rgttype from llregister where 1=1 and rgtno = '" + aCaseNo+"' with ur";
	var tLowerStr = easyExecSql(LowerStr);
	if(tLowerStr) {
		var ttRgtType = tLowerStr[0][0];
		if(ttRgtType == "13") {
			fm.LowerQuestion.disabled=false;
		}else {
			fm.LowerQuestion.disabled=true;
		}
	}
}

function OnLower(){
	  var pathStr="./LLOnlinClaimQuestionMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&CustomerName="+fm.CustomerName.value+"&Handler="+fm.Handler.value;
	  showInfo=OpenWindowNew(pathStr,"EventInput","middle");
	}

function openCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}

	pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
}

function preDeal(){
  var varSrc = "&CaseNo=" + fm.all('CaseNo').value;
  parent.window.location = "./FrameMainZD.jsp?Interface=ICaseCureInput.jsp" +varSrc;
}

function nextDeal(){
  var varSrc="&CaseNo=" + fm.all('CaseNo').value ;
  parent.window.location = "./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+varSrc;
}

function backDeal(){
  top.close();
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

//"按疾病平均花费统计" 功能方案按钮在检录页面，按钮名称 "疾病统计信息"。
function DiseaseStatistics(){
	var varSrc = "";
  	pathStr="./FrameMainDiseaseStatistics.jsp?Interface=LLDiseaseStatisticsInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLDiseaseStatisticsInput","middle",800,500);
}

function shouCheck()
{	
	var rowNum=InsuredEventGrid. mulLineCount;
	var sqlsCheckSubrptNo="select subrptno  from llcaserela where CaseNo='"+fm.CaseNo.value+"' ";
	var CheckSubrptNo= easyExecSql(sqlsCheckSubrptNo);

    for(var i=0;i<rowNum;i++)
    {
    var value2=InsuredEventGrid.getRowColDataByName (i,"Subno");
  	 for(var j=0;j<CheckSubrptNo.length;j++){
  	   if (CheckSubrptNo[j][0]==value2){
  	      InsuredEventGrid.checkBoxSel(i+1);
  	    }
      }
    }
}
//TODO：理赔二核页面跳转
function submitLLUnderWrit(){

  if((fm.CustomerNo.value==""||fm.CustomerNo.value==null)||
  (fm.CustomerName.value==""||fm.CustomerName.value==null))
  {
  	alert("请录入客户号码!");
  	return false;
  }
  if(fm.RgtNo.value.substring(0,1) == 'P'){
		alert("团体批次下不可提起二核！");
		return false;
	}
	
	//TODO:临时校验，社保案件不可提起理赔二核
    var tSocial = fm.CaseNo.value;
	if(!checkSocial('',tSocial,'')){
		return false;
	}	
//by lyc #2157	
//	var tRgtStateSql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"'";
//	var tRgtState = easyExecSql(tRgtStateSql);
//	if(tRgtState != null){
//		if(tRgtState[0][0] == '07' || tRgtState[0][0] == '13'){
//		
//			alert("案件处于调查状态或延迟状态，不可提起二核");
//			return false;
//		}
//		if(tRgtState[0][0] == '09' || tRgtState[0][0] == '11'){
//		
//			alert("案件处于结案状态或通知状态，不可提起二核");
//			return false;
//		}
//		if(tRgtState[0][0] == '14'){
//		
//			alert("案件处于撤件状态，不可提起二核");
//			return false;
//		}
//	}
  
  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  pathStr="./FrameMainLLUnderWrit.jsp?Interface=LLUnderWritInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"LLUnderWritInput","middle",800,500);
}
//事件关联保存后查询
function queryEventInfo(){
	if(fm.AllEvent.checked){
		initInsuredEventGrid(1)
	// #3500 发生地点查询
  	var strSQL = "select a.SubRptNo,AccDate,(select codename from ldcode1 where codetype='province1' and code=accprovincecode), " +
	"(select codename from ldcode1 where codetype='city1' and code=acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=acccountycode fetch first 1 row only), " +
	"AccPlace,InHospitalDate,OutHospitalDate,AccDesc,"
  	+"case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," 
  	+"AccidentType,(select b.caserelano from llcaserela b where b.SubRptNo=a.SubRptNo and b.caseno='"
  	+fm.CaseNo.value+"'),a.accprovincecode,a.acccitycode,a.acccountycode from LLSubReport a where a.customerno='" + fm.CustomerNo.value +"' order by AccDate desc" ;
  	
  	turnPage1.pageLineNum = 20;
	turnPage1.queryModal(strSQL,InsuredEventGrid);
    shouCheck();
	}
	else{
		initInsuredEventGrid(0);
		// #3500 查询发生地点
  	var strSQL = "select a.SubRptNo,AccDate,(select codename from ldcode1 where codetype='province1' and code=accprovincecode), " +
	"(select codename from ldcode1 where codetype='city1' and code=acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=acccountycode fetch first 1 row only), " +
	"AccPlace,InHospitalDate,OutHospitalDate,AccDesc,"
  	+"case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," 
  	+"AccidentType,b.caserelano,a.accprovincecode,a.acccitycode,a.acccountycode from LLSubReport a,llcaserela b where b.SubRptNo=a.SubRptNo "
  	+" and b.caseno='"+fm.CaseNo.value+"' order by AccDate desc" ;
  	arrResult = easyExecSql(strSQL);
  	if(arrResult){
//  	  displayMultiline(arrResult, InsuredEventGrid);
    turnPage1.pageLineNum = 20;
	turnPage1.queryModal(strSQL,InsuredEventGrid);
  	}
  }
  	divDisease.style.display='none';
  	divSeDisease.style.display='none';
  	divAccident.style.display='none';
  	divOperation.style.display='none';
  	divOtherInfo.style.display='none';
  	divDeformity.style.display='none';
  	divInvalidism.style.display='none';//伤残	add by Houyd #1777 新伤残评定标准及代码系统定义
  	
  	showCaseRemark();	//#1769 案件备注信息的录入和查看功能 add by Houyd
}

function showFeeRela(){
  //查询案件所有账单
  strSQL = "select distinct a.ReceiptNo,a.HospitalName,a.FeeDate,a.CaseRelaNo,a.MainFeeNo,'',a.sumfee,a.HospitalCode from llfeemain a "
  +" where a.caseno='" + fm.CaseNo.value +"'";
  var feeArr= easyExecSql(strSQL );
  if ( feeArr){
    displayMultiline( feeArr, ICaseCureGrid);
  }
}

function submitFormCheck(){
  var IsMannual = false;
  var OFNum = OtherFactorGrid.mulLineCount;
  if (OFNum>0){
    for (i=0;i<OFNum;i++){
      var OtherFactCode = OtherFactorGrid.getRowColData(i,2);
      if (OtherFactCode=='9918'||OtherFactCode=='9150'||OtherFactCode=='9151'){
        IsMannual=true;
        break;
      }
    }
  }
  var rowsNo = ICaseCureGrid.mulLineCount;
  if ( rowsNo>0)
  {
    var checkRc = false;
    var kk=0;
    for(kk=0;kk<rowsNo;kk++){
      if(ICaseCureGrid.getChkNo(kk)){
          checkRc=true;
          //若“理赔案件-->理赔处理-->账单录入”页面的“账单种类”为“2-住院”的，检录页面的“手术信息”为必录项
    	  var tMainFeeNo = ICaseCureGrid.getRowColData(kk,5);
    	  var tReceiptNo = ICaseCureGrid.getRowColData(kk,1);
    	  var caseRelaNoLine=InsuredEventGrid.getSelNo ();//得到被选中的行号，事件号
  		  if ( caseRelaNoLine>0){
      		//fm.CaseRelaNo.value = InsuredEventGrid.getRowColData(caseRelaNoLine-1,9);
      	  }else{
      	  	alert("请选择事件信息！");
      	  	return false;
      	  }
		  //校验是否录入“手术信息”
    	  //var tCheckSql = "select 1 from llfeemain a where a.feetype='2' and a.caseno='"+fm.CaseNo.value+"' and a.caserelano='"+fm.CaseRelaNo.value+"' and a.mainfeeno='"+tMainFeeNo+"' and exists (select 1 from lloperation b where 1=1 and b.caserelano=a.caserelano and b.caseno=a.caseno) with ur";   
    	  var tCheckSql = "select 1 from llfeemain a where a.feetype='2' and a.caseno='"+fm.CaseNo.value+"' and a.mainfeeno='"+tMainFeeNo+"' with ur";   
    	  var tCheckArr = easyExecSql(tCheckSql);
  		  if (tCheckArr) {
  		    var rowNum = DegreeOperationGrid.mulLineCount ;	
			if(rowNum == 0){
				alert("账单"+tReceiptNo+"的“账单种类”为“2-住院”的，检录页面的“手术信息”为必录项!");
				return false ;
			}else if(rowNum >= 1){
				divOperation.style.display='';
				for(var p=0 ; p < rowNum ; p++){
					var tOperationCode = DegreeOperationGrid.getRowColData(p,2);	                 
					if( tOperationCode.trim()=="" || tOperationCode==null || tOperationCode.trim()==''){
						alert("账单"+tReceiptNo+"的“账单种类”为“2-住院”的，检录页面的“手术信息”为必录项!");
						DegreeOperationGrid.setFocus(p,1,DegreeOperationGrid);
						return false ;
					}				
				}		
			}			  	   	  
      	}   	  
      }
    }
    if(!checkRc){
      if(!confirm("尚未关联帐单，要继续保存吗"))
      return false;
    }
  }
  var rowNum=DiseaseGrid.mulLineCount;
  if (IsMannual==false){
    if (rowNum<=0){
      alert("请录入疾病信息！");
      return false;
    }
    else{
      var DiseaCode=DiseaseGrid. getRowColData(0,4);
      if(DiseaCode==null ||DiseaCode==""){
        alert("请录入疾病信息！");
        return false;
      }
    }
    if(!checkDisease()) {
   	return false;
   }
  }
  
  var tStrSQL = "select 1 From llcase a,llcaserela b,llcasecure c "
              + " where a.caseno='"+ fm.CaseNo.value 
              + "' and a.caseno=b.caseno and a.caseno=c.caseno and b.caserelano=c.caserelano "
              + " union select 1 From llcase a,llcaserela b,llotherfactor c "
              + " where a.caseno='"+ fm.CaseNo.value
              + "' and a.caseno=b.caseno and a.caseno=c.caseno and b.caserelano=c.caserelano "
              + " and c.factortype = '9' and c.factorcode in ('9918','9150','9151') with ur";
  var tHavecure = easyExecSql(tStrSQL);
  if (!tHavecure) {
  	alert("未保存检录信息");
  	return false;
  }
  
  easyQueryPolicy();
  var rowPolicy=CasePolicyGrid.mulLineCount;
  if (rowPolicy <= 0) {
     alert("查询被保险人保单出错");
     return false;  
  }
  
    //TODO:临时校验，社保案件不可保存检录信息
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
  //# 3409 社保相关业务申诉纠错不做是否有扫描件的校验**start**
  
  //#3064商险扫描件校验  针对商业保险理赔案件
  var tCaseno = fm.CaseNo.value;
  var tOperator =fm.cOperator.value;
  
  //# 3425 微医项目不对扫描件进行校验 **start** casetype='10'--微医
  var tWYSQL="select 1 from llcase a,llhospcase b where a.caseno=b.caseno and b.casetype='10' and a.caseno='"+tCaseno+"' with ur"; 
  var tWYRSQL="select 1 from llappeal a,llhospcase b,llcase c where a.appealno=c.caseno and a.caseno=b.caseno and b.casetype='10' and a.appealno='"+tCaseno+"' with ur";
  var tWY=easyExecSql(tWYSQL);
  var tWYR=easyExecSql(tWYRSQL);
  if(tWY=='1' || tWYR=='1'){
//	  alert("微医项目不进行影像件校验");
  }else if((tCaseno.substring(0,1)=='S'||tCaseno.substring(0,1)=='R') && tOperator.substring(0,1)=='s'){
//	  alert(tCaseno.substring(0,1));
//	  alert(tOperator.substring(0,1));
  }else{
  var sqlScan1 = "Select CHECKGRPCONT(a.rgtobjno) from llregister a,llcase b where a.rgtno=b.rgtno and b.caseno = '"+tCaseno+"'";   
  var sScan1 = easyExecSql(sqlScan1);
  //若有扫描件，案件继续操作，若无扫描件，点击确认时，阻断并提示“请补充扫描理赔资料后再进行后续操作”。
  if(sScan1=='N'){
	  var sqlScan2 ="select count(docid) from ES_DOC_RELATION where busstype='LP' and bussno='"+tCaseno+"' " ;
	  var sScan2 = easyExecSql(sqlScan2);
//	  alert("sScan2=="+sScan2);
	  if(sScan2<1){
		  alert("请补充扫描理赔资料后再进行后续操作");
		  return false;
	  }
    }
  }
  // # 3409 社保相关业务申诉纠错不做是否有扫描件的校验**end**
  //TODO:新增效验，理赔二核时不可保存检录信息
	var sqlUW = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstateUW=easyExecSql(sqlUW);
	if (rgtstateUW!=null){
	     if(rgtstateUW[0][0]=='16'){
	       alert("理赔二核中不可保存检录信息，必须等待二核结束！");
	       return false;
	     }
	}

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  fm.confirm.disabled=true;
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="LLRegisterCheckSave.jsp";
  fm.fmtransact.value ="CHECKFINISH" ;
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  fm.backmain1.disabled=false;
   fm.confirm.disabled=false;
   fm.mysave.disabled=false;
  FlagDel = FlagStr;
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    if ( fm.fmtransact.value =="FEERELASAVE" ){
      showFeeRela();
    }
  }
  initTitle();
}

function SelectICaseCureGrid(){
  initICaseCureGrid();
  initDiseaseGrid();
  initDeformityGrid();
  initInvalidismGrid();//伤残	add by Houyd #1777 新伤残评定标准及代码系统定义
  initSeriousDiseaseGrid();
  initOtherFactorGrid();
  initDegreeOperationGrid();
  initAccidentGrid();
  initSDiseaseProofGrid("重疾依据名称");
  initDisabilityGrid();
  	divDisease.style.display='none';
  	divSeDisease.style.display='none';
  	divAccident.style.display='none';
  	divOperation.style.display='none';
  	divOtherInfo.style.display='none';
  	divDeformity.style.display='none';
  	divInvalidism.style.display='none';//伤残	add by Houyd #1777 新伤残评定标准及代码系统定义
  	divDisability.style.display='none';
  	divReceipt.style.display='';
  divCheckInfo.style.display='';
  showFeeRela();
  //关联事件对应的复选框被选中
  var caseRelaNoLine=InsuredEventGrid.getSelNo ();//得到被选中的行号
  if ( caseRelaNoLine>0){

      fm.CaseRelaNo.value = InsuredEventGrid.getRowColData(caseRelaNoLine-1,12);
      if(fm.CaseRelaNo.value=='')
      	return;
      if(fm.RiskCode.value!='1605')
      queryCheckInfo();
      else{
        if(fm.RgtState.value=='扫描状态'||fm.RgtState.value=='受理状态'||fm.RgtState.value=='查迄状态'){
          var tempCaseRelaNo = fm.CaseRelaNo.value;
          var tempCaseNo = fm.CaseNo.value;
          sqls="select CaseNo,caserelano from llcaserela where subrptno='"+selectvalue+"' order by caserelano";
          var xrr = easyExecSql(sqls)
          if(xrr){
            fm.CaseNo.value=xrr[0][0];
            fm.CaseRelaNo.value=xrr[0][1];
            queryCheckInfo();
          }
          fm.CaseRelaNo.value = tempCaseRelaNo;
          fm.CaseNo.value=tempCaseNo;
        }
        else{
          queryCheckInfo();
        }
      }
      var rowNum=ICaseCureGrid.mulLineCount;

      for(i=1;i<=rowNum;i++){
        if(fm.CaseRelaNo.value!=''){
          if(fm.CaseRelaNo.value==ICaseCureGrid.getRowColData(i-1,4)){
            ICaseCureGrid.checkBoxSel(i);
          }
        }
      }
    }
}


/*****************************************************/
//提交，保存按钮对应操作
function submitForm(){
  var selNo = InsuredEventGrid.getSelNo()-1;
  if ( selNo<0){
    alert("请选择一个事故信息");
    return false;
  }

  var rowsNo = ICaseCureGrid.mulLineCount;
  if ( rowsNo>0){
    var checkRc = false;
    var kk=0;
    for(kk=0;kk<rowsNo;kk++){
      if(ICaseCureGrid.getChkNo(kk)){
          checkRc=true;
          //若“理赔案件-->理赔处理-->账单录入”页面的“账单种类”为“2-住院”的，检录页面的“手术信息”为必录项
    	  var tMainFeeNo = ICaseCureGrid.getRowColData(kk,5);
    	  var tReceiptNo = ICaseCureGrid.getRowColData(kk,1);
    	  var caseRelaNoLine=InsuredEventGrid.getSelNo ();//得到被选中的行号，事件号
  		  if ( caseRelaNoLine>0){
      		//fm.CaseRelaNo.value = InsuredEventGrid.getRowColData(caseRelaNoLine-1,9);
      	  }else{
      	  	alert("请选择事件信息！");
      	  	return false;
      	  }
		  //校验是否录入“手术信息”
    	  //var tCheckSql = "select 1 from llfeemain a where a.feetype='2' and a.caseno='"+fm.CaseNo.value+"' and a.caserelano='"+fm.CaseRelaNo.value+"' and a.mainfeeno='"+tMainFeeNo+"' and exists (select 1 from lloperation b where 1=1 and b.caserelano=a.caserelano and b.caseno=a.caseno) with ur";   
    	  var tCheckSql = "select 1 from llfeemain a where a.feetype='2' and a.caseno='"+fm.CaseNo.value+"' and a.mainfeeno='"+tMainFeeNo+"' with ur";   
    	  var tCheckArr = easyExecSql(tCheckSql);
  		  if (tCheckArr) {
  		    var rowNum = DegreeOperationGrid.mulLineCount ;	
			if(rowNum == 0){
				alert("账单"+tReceiptNo+"的“账单种类”为“2-住院”的，检录页面的“手术信息”为必录项!");
				return false ;
			}else if(rowNum >= 1){
				divOperation.style.display='';
				for(var p=0 ; p < rowNum ; p++){
					var tOperationCode = DegreeOperationGrid.getRowColData(p,2);	                 
					if( tOperationCode.trim()=="" || tOperationCode==null || tOperationCode.trim()==''){
						alert("账单"+tReceiptNo+"的“账单种类”为“2-住院”的，检录页面的“手术信息”为必录项!");
						DegreeOperationGrid.setFocus(p,1,DegreeOperationGrid);
						return false ;
					}				
				}		
			}			  	   	  
      	}
      }
    }
    if(!checkRc){
      if(!confirm("尚未关联帐单，要继续保存吗"))
      return false;
    }
  }

  InsuredEventGrid.checkBoxSel(InsuredEventGrid.getSelNo());
  var IsMannual = false;
  var OFNum = OtherFactorGrid.mulLineCount;
  if (OFNum>0){
    for (i=0;i<OFNum;i++){
      var OtherFactCode = OtherFactorGrid.getRowColData(i,2);
      if (OtherFactCode=='9918'||OtherFactCode=='9150'||OtherFactCode=='9151'){
        IsMannual=true;
        break;
      }
    }
  }

  var rowNum=DiseaseGrid.mulLineCount;
  if (IsMannual==false){
    if (rowNum<=0){
      alert("请录入疾病信息！");
      return false;
    }
    else{
      var DiseaCode=DiseaseGrid. getRowColData(0,4);
      if(DiseaCode==null ||DiseaCode==""){
        alert("请录入疾病信息！");
        return false;
      }
    }
    if(!checkDisease()) {
   	return false;
   }
  }
  //------------------------------------------------------------------------------------------
	var ZJNum=DiseaseGrid.mulLineCount;//疾病的行数     #2718
	var SJNum=DegreeOperationGrid.mulLineCount;//手术的行数
	var n=0;     var m=0;
		for( n=0;n<SJNum;n++){
		var ICDOPSName=DegreeOperationGrid.getRowColData(n,1);
		var ICDOPSCode=DegreeOperationGrid.getRowColData(n,2);
		var DegreeSQL="select 1 from LLOpsDisease where ICDOPSName= '"+ICDOPSName+"' and ICDOPSCode= '"+ICDOPSCode+"'" ;
						var DegreeArr = easyExecSql(DegreeSQL);
						if(DegreeArr=="" || DegreeArr==null){
							continue;
						}
						
		for( m=0;m<ZJNum;m++){
			var Ss=DiseaseGrid.getRowColData(m,15);
			var ICDName = DiseaseGrid.getRowColData(m,3);
			var ICDCode=DiseaseGrid.getRowColData(m,4);
			if(Ss!="无"){
			var tZJSSSql="select 1 from LLOpsDisease where ICDOPSCode= '"+ICDOPSCode+"' and ICDCode= '"+ICDCode+"' " ;
			var tZJSSArr = easyExecSql(tZJSSSql);
			if(!tZJSSArr){

				if(confirm("疾病名称为"+ICDName+"录入的疾病信息与手术信息匹配不合理，请确认继续操作？")){
				}else{
					return false;
					}
				}
		}}

	    }
		
		
		//#3889 校验肿瘤形态学代码//tmm
		for( n=0;n<ZJNum;n++){
		var tumflag=DiseaseGrid.getRowColData(n,5);
		var LDNAME=DiseaseGrid.getRowColData(n,6);
		var LDCODE=DiseaseGrid.getRowColData(n,7);
			if(tumflag==2){
				
				if(LDCODE=="" || LDNAME==""){
					alert("请录入正确的肿瘤形态学代码和名称");
					return false;
				}
			}
			if(tumflag==3){
				alert("请从C00-D48.9中选择正确代码，并录入正确的肿瘤形态学代码和肿瘤形态学名称");
				return false;
			}
		}

	    //------------------------------------------------------------------------------------------
  //3633 出险内容
  var SDNum = SeriousDiseaseGrid.mulLineCount;
  for (var n=0;n<SDNum;n++) {
  	if (SeriousDiseaseGrid.getRowColData(n,2)!=""&&SeriousDiseaseGrid.getRowColData(n,2)!=null) {
  		var DiagnoseDate=SeriousDiseaseGrid.getRowColData(n,11);
  		if (DiagnoseDate==null||DiagnoseDate=="") {
  		    	alert("请录入确诊日期");
  		    	return false;
  		} else{
  			if(!llcheckInputDate(DiagnoseDate)){
  				alert("请核实确诊日期录入是否正确");
  	  		    return false;
  			}
  		}
  	}
  }
  
  var DNum = DisabilityGrid.mulLineCount;
  for (var n=0;n<DNum;n++) {
	var DisabilityDate=DisabilityGrid.getRowColData(n,3);
	var ObservationDate=DisabilityGrid.getRowColData(n,4);
	if(ObservationDate!=null&&ObservationDate!=""){
			if(!llcheckInputDate(ObservationDate)){
				alert("请核实观察期结束日期录入是否正确");
	  		    return false;
			}
	}
  	if (DisabilityDate!=null&&DisabilityDate!=""){
  		if(!llcheckInputDate(DisabilityDate)){
				alert("请核实失能日期录入是否正确");
	  		    return false;
			}
  	}
  	if (DisabilityGrid.getRowColData(n,2)=='1') {
  		
  		if (DisabilityDate==null||DisabilityDate=="") {
  		    	alert("请录入失能日期");
  		    	return false;
  		} 
  		
  	}

  }
  
  //残疾信息-鉴定日期不能为空
  //#1233 《百万安行个人护理保险》等三款产品定义
  var DeNum = DeformityGrid.mulLineCount;
  for (var n=0;n<DeNum;n++) {
  	if (DeformityGrid.getRowColData(n,1)!='') {
  		var DeformityDate=DeformityGrid.getRowColData(n,7);
  		if (DeformityDate==null||DeformityDate=="") {
  		    	alert("请录入鉴定日期");
  		    	return false;
  		} else{
  			if(!llcheckInputDate(DeformityDate)){
  				alert("请核实鉴定日期录入是否正确");
  	  		    return false;
  				
  			}
  		}
  	}
  }
  
  //伤残信息-鉴定日期不能为空
  //#1233 《百万安行个人护理保险》等三款产品定义
  var InNum = InvalidismGrid.mulLineCount;
  for (var n=0;n<InNum;n++) {
  	if (InvalidismGrid.getRowColData(n,1)!='') {
  		var InvalidismDate=InvalidismGrid.getRowColData(n,7);
  		//alert(InvalidismDate);
  		if (InvalidismDate==null||InvalidismDate=="") {
  		    	alert("请录入鉴定日期");
  		    	return false;
  		}else{
  			  	if(!llcheckInputDate(InvalidismDate)){
  				alert("请核实鉴定日期录入是否正确");
  	  		    return false;
  			}
  		}  
  	}
  }
  //3561 轻症疾病确诊日期必录
  var InNum = MildGrid.mulLineCount;
  for (var n=0;n<InNum;n++) {
  	if (MildGrid.getRowColData(n,1)!='') {
  		var MDiseaseDate=MildGrid.getRowColData(n,11);
  		if (MDiseaseDate==null||MDiseaseDate=="") {
  		    	alert("请录入确诊日期");
  		    	return false;
  		}else{
  			if(!llcheckInputDate(MDiseaseDate)){
  				alert("请核实确诊日期录入是否正确");
  	  		    return false;
  			}
  		} 
  	}
  }
  
    //TODO:临时校验，社保案件不可保存检录信息
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
  
     //续期待核销的不能继续操作，有续期应收的提示是否继续
  if(!checkDueFee())
  {
    return false;  
  }
 fm.mysave.disabled=true;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLRegisterCheckSaveAll.jsp";
  fm.submit(); //提交
}

/**
校验万能险死亡日期后产生续期续保应收状态
a)	若续期催收成功、正转出，则提示先到保全做万能续期回退
b)	若续保续期已抽档但未收费，系统提示“该保单有续期续保应收记录，是否继续？”，若继续操作则作废原续期续保应收记录的收费操作
*/
function checkDueFee()
{
	var DeathSql ="select DeathDate from llcase where caseno='"+ fm.CaseNo.value +"'";
	var DeathDate = easyExecSql(DeathSql);
	var CheckSql="";
	if(DeathDate && DeathDate[0][0] != "" && DeathDate[0][0] != null)
	{
		CheckSql=" and c.lastpaytodate>='"+ DeathDate +"'"
	}else{
		for(var rowNum=0;rowNum<DisabilityGrid.mulLineCount;rowNum++)
		{
		  if (DisabilityGrid.getRowColData(rowNum,2)!=""&&DisabilityGrid.getRowColData(rowNum,2)!=null) {
		  	  var DisDate=DisabilityGrid.getRowColData(rowNum,4);//观察期结束日
			  //万能险死亡的话用死亡日期判断、失能的话用观察期结束日期判断
			  CheckSql=" and c.lastpaytodate>=(date('"+ DisDate +"') + 1 days)"					 
		  }
		}  
	}	
	 var sql = "select count(1) "
            +" from LJSPayB a,lcpol b,ljapayperson c"
            +" where a.otherno=b.contno and a.getnoticeno = c.getnoticeno and b.polno=c.polno "
            + " and b.grpcontno=c.grpcontno and b.contno=c.contno and b.grppolno=c.grppolno "
            + " and b.insuredno = '" + fm.CustomerNo.value + "' "
            +" and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='4') "  //万能保单
            +" and a.dealState in ('1', '5') "+CheckSql;
    if(CheckSql=="") {
    } else {
    	var rs = easyExecSql(sql);
	 	if(rs && rs[0][0] >=1)
	 	{
	      alert("万能保单出险后有续期实收记录，请先将续期实收保费转出！");
	      return false;
	    }	
    }     　
  
    return true;
}


/**
   校验疾病代码和疾病名称是否一致
   #2365 （需求编号：2015-015）部分ICD疾病代码+疾病名称与就诊性别不符情况，嵌入核心业务系统校验规则
*/
function checkDisease() {
    for(var rowNum=0;rowNum<DiseaseGrid.mulLineCount;rowNum++) {
		var sql="select ICDCode,ICDName from lddisease where icdcode = '"+ DiseaseGrid. getRowColData(rowNum,4) + "' and icdname = '" + DiseaseGrid. getRowColData(rowNum,3)+"' with ur " ;
        var dis = easyExecSql(sql);
        if(!dis){
            alert("请录入正确的疾病代码/疾病名称");
          	return false;
          	
        }else{
        	var tIDCCheckCode=dis[0][0];
        	var tIDCCheckName=dis[0][1];
        	var tSQL = "select codename from ldcode a where a.codetype='llidcchecksex' and a.code='"+tIDCCheckCode+"' with ur";
        	var tResult = easyExecSql(tSQL);
        	if(tResult){
        		var tSex = tResult[0][0];
        		var tCheckSQL = "select 1 from ldperson b where b.customerno='"+fm.CustomerNo.value+"' and b.sex='"+tSex+"' with ur";
        		var tSecResult = easyExecSql(tCheckSQL);
        		if(!tSecResult){
        			tSex = tSex=='0'?'男性':'女性';
        			alert("疾病诊断为"+tIDCCheckName+"，疾病诊断为"+tIDCCheckCode+"，被保险人必须是"+tSex+"，请核实！");
        			return false;
        		}
        	}
        }       
	}
	return true;
}


function EventInsert(){
	  var rowNum=InsuredEventGrid.mulLineCount;
	  var flag = 0;
	  if(fm.AllEvent.checked){
	  	for(i=0;i<rowNum;i++){
	  	  if(InsuredEventGrid.getChkNo(i)||!(InsuredEventGrid.getRowColData(i,1))){
	  		// #3500 校验发生地点**start**
	  	  if((InsuredEventGrid.getRowColData(i,3)== null) || (InsuredEventGrid.getRowColData(i,3)=="")){
	    	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(省)信息");
	    	  InsuredEventGrid.setFocus(i,1,InsuredEventGrid);
	          return false;
	      }
	      if((InsuredEventGrid.getRowColData(i,4)== null) || (InsuredEventGrid.getRowColData(i,4)=="")){
	    	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(市)信息");
	    	  InsuredEventGrid.setFocus(i,1,InsuredEventGrid);
	          return false;
	      }
	      if((InsuredEventGrid.getRowColData(i,5)== null) || (InsuredEventGrid.getRowColData(i,5)=="")){
	    	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(县)信息");
	    	  InsuredEventGrid.setFocus(i,1,InsuredEventGrid);
	          return false;
	      }
	   // 校验发生地点省市县编码
	      var checkAccCode=checkAccPlace(InsuredEventGrid.getRowColData(i,13),InsuredEventGrid.getRowColData(i,14),InsuredEventGrid.getRowColData(i,15))
	      if(checkAccCode !=""){
	   	   alert(checkAccCode);
	   	   return false;
	      }
	      // #3500 校验发生地点**end**
	      //3633 发生日期 入院日期  出院日期
	      var Hdate = InsuredEventGrid.getRowColData(i,2) //发生日期
	      var InhospDate = InsuredEventGrid.getRowColData(i,7) //入院日期
	      var OuthosDate = InsuredEventGrid.getRowColData(i,8) //出院日期
	      if(Hdate==null||Hdate==""){
	    	  alert("第"+(i+1)+"行发生日期有误，请核实事件发生日期"); 
	    	  return false;
	      }else{
	    	  if(!isDate(Hdate)){
	    		  alert("第"+(i+1)+"行发生日期有误，请核实事件发生日期");  
	    		  return false;
	    	  }else{
	    		  if(!llcheckInputDate(Hdate)){
	    			  alert("请核实第"+(i+1)+"行发生日期录入是否正确");  
	        		  return false; 
	    		  }
	    	  }
	      }
	      if(InhospDate!=null&&InhospDate!=""){
	    	  if(!isDate(InhospDate)){
	    		  alert("第"+(i+1)+"行入院日期有误，请核实事件入院日期"); 
	        	  return false;
	    	  }else{
	    		  if(!llcheckInputDate(InhospDate)){
	    			  alert("请核实第"+(i+1)+"行入院日期录入是否正确");  
	        		  return false; 
	    		  } 
	    	  }
	      }
	      if(OuthosDate!=null&&OuthosDate!=""){
	    	  if(!isDate(OuthosDate)){
	    		  alert("第"+(i+1)+"行入院日期有误，请核实事件入院日期"); 
	        	  return false;
	    	  }else{
	    		  if(!llcheckInputDate(OuthosDate)){
	    			  alert("请核实第"+(i+1)+"行出院日期录入是否正确");  
	        		  return false; 
	    		  } 
	    	  }
	      }
	      flag = 1;
	  	  }
	  	}
  var nowDate=getCurrentDate();
  if(InsuredEventGrid.checkValue2(InsuredEventGrid.name,InsuredEventGrid)== false) return false;
  for(var rowNum=0;rowNum<InsuredEventGrid. mulLineCount;rowNum++){
    var happenDate=InsuredEventGrid.getRowColData(rowNum,2);
    var happenDate1=modifydate(happenDate);
    var day=dateDiff(happenDate1,nowDate,"D");
    if(day<0){
      alert("您在第"+(rowNum+1)+"行输入的发生日期不应晚于当前时间");
      return false;
    }
    var inDate=InsuredEventGrid.getRowColData(rowNum,7);
    inDate=modifydate(inDate);
    var outDate=InsuredEventGrid.getRowColData(rowNum,8);
    if (outDate.length!=0){
      outDate=modifydate(outDate);
      day1=dateDiff(inDate,outDate,"D");
      if(dateDiff(inDate,outDate,"D")<0){
        alert("您在第"+(rowNum+1)+"行输入的出院日期不应早于住院时间");
        return false;
      }
    }
  }

  	if(!flag){
  	  alert("你没有选择要保存的信息");
  	  return flase;
  	}
  }
  
    //TODO:临时校验，社保案件不可保存事件
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
  
  //TODO:新增效验，理赔二核时不可保存事件
	var sqlUW = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstateUW=easyExecSql(sqlUW);
	if (rgtstateUW!=null){
	     if(rgtstateUW[0][0]=='16'){
	       alert("理赔二核中不能保存事件，必须等待二核结束！");
	       return false;
	     }
	}
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLRegisterCheckSaveEvent.jsp";
  fm.submit(); //提交
}

function queryCheckInfo(){
	var sqlPart = " where caseno='"+ fm.CaseNo.value +"' and CaseRelaNo='" + fm.CaseRelaNo.value +"' ";
  //一般疾病,根据MainDiseaseFlag判断主要疾病,放在第一位
  var strSQL ="select Diagnose,DiseaseName,DiseaseName,DiseaseCode,LDName,LDName,LDCode,Risklevel,Riskcontinueflag,HospitalCode,HospitalName,"
  +"DoctorName,SerialNo,DoctorNo,OperationFlag  from llcasecure "+sqlPart
  +" and SeriousFlag='0' order by MainDiseaseFlag desc";
  var arr = easyExecSql(strSQL );
  if (arr){
  	divDisease.style.display='';
  	displayMultiline( arr, DiseaseGrid);
  }

  //重疾,根据MainDiseaseFlag判断主要疾病,放在第一位
  strSQL ="select DiseaseName,DiseaseCode,HospitalName,HospitalCode,DoctorName,DiagnoseDesc,'',"
  +"SeriousDutyFlag,JudgeDepend,case when SeriousDutyFlag='0' then '不成立' else '成立' end ,"
  +"DiagnoseDate from llcasecure "+sqlPart+" and SeriousFlag='1' order by MainDiseaseFlag desc" ;
  var brr = easyExecSql(strSQL );
  if ( brr ){
  	divSeDisease.style.display='';
    displayMultiline( brr, SeriousDiseaseGrid);
    fm.SDLoadFlag.value='1'
  }
  //意外
  strSQL ="select AccidentNo,Type,TypeName,'',Name,Code,ReasonCode,Reason,Remark from llaccident "
  +sqlPart;
  var crr = easyExecSql(strSQL );
  if ( crr ){
  	divAccident.style.display='';
  	displayMultiline( crr, AccidentGrid);
  }
  //手术
  strSQL ="select OperationName,OperationCode,OpFee,OpLevel,SerialNo from lloperation "+sqlPart;
  var drr = easyExecSql(strSQL );
  if ( drr ){
  	divOperation.style.display='';
  	displayMultiline( drr, DegreeOperationGrid);
  }
  //其它录入要素
  strSQL ="select a.codename,b.FactorCode,b.FactorName,b.Value,b.remark,b.FactorType from LLOtherFactor b, ldcode a "
  +sqlPart+" and  a.codetype='llotherfactor' and a.code=b.factortype ";
  var drr = easyExecSql(strSQL );
  if ( drr ){
  	divOtherInfo.style.display='';
  	displayMultiline( drr, OtherFactorGrid);
  }
  //残疾
  strSQL = "select Name,Code,DeformityGrade,DeformityRate,DiagnoseDesc,'',JudgeDate,JudgeOrganName,JudgeOrgan from llcaseinfo "
  + " where caseno='"+ fm.CaseNo.value +"' and CaseRelaNo='" + fm.CaseRelaNo.value +"' and type not in ('3') with ur ";
  var err = easyExecSql(strSQL);
  if (err){
  	divDeformity.style.display='';
  	displayMultiline(err,DeformityGrid);
  }
  
  //伤残	add by Houyd #1777 新伤残评定标准及代码系统定义,用llcasinfo中的type区分伤残信息与残疾信息
  //type 空:
  //type 1:残疾类 意外残疾
  //type 2:残疾类 烧伤
  //type 3:伤残类
  strSQL = "select a.Name,(select codealias from ldcode where codetype='desc_invalidism' and code= a.Code),a.code,a.DeformityGrade,a.DeformityRate,a.DiagnoseDesc,a.JudgeDate,a.JudgeOrganName,a.JudgeOrgan from llcaseinfo a "
  + " where a.caseno='"+ fm.CaseNo.value +"' and a.CaseRelaNo='" + fm.CaseRelaNo.value +"' and a.type in ('3') with ur ";
  var err = easyExecSql(strSQL);
  if (err){
  	divInvalidism.style.display='';
  	displayMultiline(err,InvalidismGrid);
  }
  
  //失能
  strSQL = "select (case when DisabilityFlag='1' then '是' else '否' end),DisabilityFlag,DisabilityDate,ObservationDate from LLDisability "
  +sqlPart;
  var err = easyExecSql(strSQL);
  if (err){
  	divDisability.style.display='';
  	displayMultiline(err,DisabilityGrid);
  }
//轻症,根据MainDiseaseFlag判断主要疾病,放在第一位
  strSQL ="select DiseaseName,DiseaseCode,HospitalName,HospitalCode,DoctorName,DiagnoseDesc,'',"
  +"SeriousDutyFlag,JudgeDepend,case when SeriousDutyFlag='0' then '不成立' else '成立' end ,"
  +"DiagnoseDate from llcasecure "+sqlPart+" and SeriousFlag='6' order by MainDiseaseFlag desc" ;
  var brr = easyExecSql(strSQL );
  if ( brr ){
  	divMDisease.style.display='';
    displayMultiline( brr, MildGrid);
    fm.SDLoadFlag.value='1'
  }
}

function CaseChange(){
  try{
    initTitle();
    initInsuredEventGrid(0);
    initICaseCureGrid();
    if(fm.CaseNo.value!='')
    queryEventInfo();
    showFeeRela();

    initDiseaseGrid();
    initDeformityGrid();
    initInvalidismGrid();//伤残	add by Houyd #1777 新伤残评定标准及代码系统定义
    initSeriousDiseaseGrid();
    initOtherFactorGrid();
    initDegreeOperationGrid();
    initAccidentGrid();
  }
  catch(re){
    //alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

function easyQueryPolicy(){
  strSQL0 = "select contno,InsuredName,riskcode,amnt,cvalidate,polno from ";
  wherePart = " where insuredno='"+ fm.CustomerNo.value +"' and appflag='1' and stateflag<>'4' "
  if(fm.RiskCode.value=='1605')
  	wherePart+=" and RiskCode='"+fm.RiskCode.value+"'";
  strSQL = strSQL0+" lcpol "+wherePart+" union "+strSQL0+" lbpol "+wherePart;
  var arr = easyExecSql(strSQL );
  if(arr&&arr.length>0)
	  displayMultiline( arr, CasePolicyGrid);
}

function submitFormSurvery(){
  strSQL="select otherno from LLSurvey where otherno='"+fm.CaseNo.value+"'";
  arrResult = easyExecSql(strSQL);
  
  if(arrResult){
    if(!confirm("该案件提起过调查，您确定要继续吗？")){
      return false;
    	}
    }
    
    //TODO:临时校验，社保案件不可提起调查
//    var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
    
    //TODO:新增效验，理赔二核时不可提起调查
	var sqlUW = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstateUW=easyExecSql(sqlUW);
	if (rgtstateUW!=null){
	     if(rgtstateUW[0][0]=='16'){
	       alert("理赔二核中不能提起调查，必须等待二核结束！");
	       return false;
	     }
	}
    var varSrc = "&CaseNo=" + fm.CaseNo.value;
    varSrc += "&InsuredNo=" + fm.CustomerNo.value;
    varSrc += "&CustomerName=" + fm.CustomerName.value;
    varSrc += "&RgtNo=" + fm.RgtNo.value;
    varSrc += "&StartPhase=1";
    varSrc += "&Type=1";

    pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
    showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
  
}

function ShowSurveyReply(){
  strSQL="select otherno from LLSurvey where otherno='"+fm.CaseNo.value+"'";
  arrResult = easyExecSql(strSQL);

  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&StartPhase=1";
  varSrc += "&Type=2";
  pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}

function Affix(){
  var varSrc ="";
  var pathStr="./CaseAffixMain.jsp?LoadFlag=9&CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value;
  OpenWindowNew(pathStr,"","middle",450,700);
}

function afterAffix(ShortCountFlag,SupplyDateResult){
  var strsql = "select a.RgtState,b.codename from LLcase a, ldcode b where a.caseno='"
  +fm.CaseNo.value +"' and b.codetype='llrgtstate' and b.code=a.rgtstate"
  arrResult = easyExecSql(strsql);
  if ( arrResult ){
    fm.RgtState.value= arrResult[0][1];
  }
}

function afterCodeSelect( cName, Filed){

}

//重疾责任依据初始化
function OpenProof(a){//a指上一个MulLine的行名称
  divSDProof.style.display='';
  var SDCode = fm.all(a).all('SeriousDiseaseGrid2').value;//重疾编码
  var SDProofCode = fm.all(a).all('SeriousDiseaseGrid6').value;//重疾责任依据编码
  var SDName = "重疾责任依据";
  fm.RowNo.value = eval(a.substring(22));//第23个字符为行号码
  if(fm.SDLoadFlag.value=='1'){//通过案件查询出的重疾信息
  	fm.RowNo.value-=1;
  }
  if(SDCode!="" &&SDCode!=null){//前一MulLine传入的重疾编码
  	SDName=fm.all(a).all('SeriousDiseaseGrid1').value+"的重疾责任依据";
  }
  
  initSDiseaseProofGrid(SDName);
  var str0= "select proofcode,proofname,Judgement,remark from llmsdiseaseproof where code='"+SDCode+"' order by proofcode";
  var bresult = easyExecSql(str0);
  if(bresult){
    displayMultiline( bresult, SDiseaseProofGrid);
  }
  var rowNum=SDiseaseProofGrid.mulLineCount;
  for(i=0;i<rowNum;i++){
    var ttProofCode=SDiseaseProofGrid.getRowColData(i,1)
    if(SDProofCode.indexOf(ttProofCode)>=0){
    	SDiseaseProofGrid.checkBoxSel(i+1);
    }   
  }
}

//轻症责任依据初始化
function OpenMDProof(a){//a指上一个MulLine的行名称
  divMDProof.style.display='';
  var MDCode = fm.all(a).all('MildGrid2').value;//轻症编码
  var MDProofCode = fm.all(a).all('MildGrid6').value;//轻症责任依据编码
  var MDName = "轻症责任依据";
//  alert(MDName);
  fm.RowNo.value = eval(a.substring(22));//第23个字符为行号码
//  alert("1");
  if(fm.MDLoadFlag.value=='1'){//通过案件查询出的重疾信息
  	fm.RowNo.value-=1;
  }
  if(MDCode!="" &&MDCode!=null){//前一MulLine传入的重疾编码
  	MDName=fm.all(a).all('MildGrid1').value+"的轻症责任依据";
  }
//  alert("2");
  initMDiseaseProofGrid(MDName);
  var str0= "select proofcode,proofname,Judgement,remark from llmsdiseaseproof where code='"+MDCode+"' order by proofcode";
  var bresult = easyExecSql(str0);
  if(bresult){
    displayMultiline( bresult, MDiseaseProofGrid);
  }
//  alert("3");
  var rowNum=MDiseaseProofGrid.mulLineCount;
  for(i=0;i<rowNum;i++){
    var ttMDProofCode=MDiseaseProofGrid.getRowColData(i,1)
    if(MDProofCode.indexOf(ttMDProofCode)>=0){
    	MDiseaseProofGrid.checkBoxSel(i+1);
    }   
  }
//  alert("4");
}




//选择不同的重疾责任依据，返回是否成立的标记
//0-只要选择即不成立；1-全部选择才成立；2-选择任意一项即成立
function backMain(){
  var rowNum=SDiseaseProofGrid.mulLineCount;
  var tProof = '';
  var tProofCode='';
  var tProofFlag=true;
  var tsecondFlag = false;
  var tmulFlag = false;
  var tThirdFlag = false;//3-至少满足几项条件
  var tFlagLimitCount=0;//计数
  for(i=0;i<rowNum;i++){
    var tflag = SDiseaseProofGrid.getRowColData(i,3);
    var tFlagLimit = SDiseaseProofGrid.getRowColData(i,4);
    
    if(SDiseaseProofGrid.getChkNo(i)){//只要有选择，进入判断
      if(tflag=='0'){
      	tProofFlag=false;
      }
      if(tflag=='3'){
      	tFlagLimitCount = tFlagLimitCount+1;
      }
      if(tflag=='2'){
        tsecondFlag=true;
        tmulFlag = true;
      }
      
      if(tFlagLimitCount>=tFlagLimit && tFlagLimitCount>0){
  		tThirdFlag=true;	
  	  }else if(tFlagLimitCount<tFlagLimit && tFlagLimitCount>0){
  	  	tProofFlag=false;
  	  }
      tProof+=SDiseaseProofGrid.getRowColData(i,2)+' ';
      tProofCode+=SDiseaseProofGrid.getRowColData(i,1)+',';
    }
    else{
      if(tflag=='1'){
      	tProofFlag=false;
      }     
    }
  }

  if(tsecondFlag){
    if(tProofFlag&&tmulFlag){//描述均为2
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,8,'1');
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,10,'成立');
    }
    else{//描述为2，还存在0或1
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,8,'0');
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,10,'不成立');
    }
  }else if(tThirdFlag){//描述为3
  	SeriousDiseaseGrid.setRowColData(fm.RowNo.value,8,'1');
    SeriousDiseaseGrid.setRowColData(fm.RowNo.value,10,'成立');
  }
  else{
    if(tProofFlag){//描述均为1
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,8,'1');
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,10,'成立');
    }
    else{//描述为1，还包含0,3
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,8,'0');
      SeriousDiseaseGrid.setRowColData(fm.RowNo.value,10,'不成立');
    }
  }
  
  SeriousDiseaseGrid.setRowColData(fm.RowNo.value,6,tProofCode);
  SeriousDiseaseGrid.setRowColData(fm.RowNo.value,9,tProof);
  divSDProof.style.display='none';
}
//3561
//选择不同的轻症责任依据，返回是否成立的标记
//0-只要选择即不成立；1-全部选择才成立；2-选择任意一项即成立
function backMDMain(){
var rowNum=MDiseaseProofGrid.mulLineCount;
var tProof = '';
var tProofCode='';
var tProofFlag=true;
var tsecondFlag = false;
var tmulFlag = false;
var tThirdFlag = false;//3-至少满足几项条件
var tFlagLimitCount=0;//计数
for(i=0;i<rowNum;i++){
  var tflag = MDiseaseProofGrid.getRowColData(i,3);
  var tFlagLimit = MDiseaseProofGrid.getRowColData(i,4);
  
  if(MDiseaseProofGrid.getChkNo(i)){//只要有选择，进入判断
    if(tflag=='0'){
    	tProofFlag=false;
    }
    if(tflag=='3'){
    	tFlagLimitCount = tFlagLimitCount+1;
    }
    if(tflag=='2'){
      tsecondFlag=true;
      tmulFlag = true;
    }
    
    if(tFlagLimitCount>=tFlagLimit && tFlagLimitCount>0){
		tThirdFlag=true;	
	  }else if(tFlagLimitCount<tFlagLimit && tFlagLimitCount>0){
	  	tProofFlag=false;
	  }
    tProof+=MDiseaseProofGrid.getRowColData(i,2)+' ';
    tProofCode+=MDiseaseProofGrid.getRowColData(i,1)+',';
  }
  else{
    if(tflag=='1'){
    	tProofFlag=false;
    }     
  }
}

if(tsecondFlag){
  if(tProofFlag&&tmulFlag){//描述均为2
	MildGrid.setRowColData(fm.RowNo.value,8,'1');
	MildGrid.setRowColData(fm.RowNo.value,10,'成立');
  }
  else{//描述为2，还存在0或1
	MildGrid.setRowColData(fm.RowNo.value,8,'0');
	MildGrid.setRowColData(fm.RowNo.value,10,'不成立');
  }
}else if(tThirdFlag){//描述为3
	MildGrid.setRowColData(fm.RowNo.value,8,'1');
	MildGrid.setRowColData(fm.RowNo.value,10,'成立');
}
else{
  if(tProofFlag){//描述均为1
	MildGrid.setRowColData(fm.RowNo.value,8,'1');
	MildGrid.setRowColData(fm.RowNo.value,10,'成立');
  }
  else{//描述为1，还包含0,3
	MildGrid.setRowColData(fm.RowNo.value,8,'0');
	MildGrid.setRowColData(fm.RowNo.value,10,'不成立');
  }
}

MildGrid.setRowColData(fm.RowNo.value,6,tProofCode);
MildGrid.setRowColData(fm.RowNo.value,9,tProof);
divMDProof.style.display='none';
}

//显示临时保单信息
function showTemPolInfo(){
  var varSrc ="";
  showInfo = window.open("./FrameMainLSBDXX.jsp?Interface=TemPolInfoInput.jsp"+varSrc,"CasePolicyInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

/*******************************************************************************
* Name          :deal_DeformityRate
* Author        :LiuYansong
* CreateDate    :2003-12-18
* Function      :判断理算中的实际赔付金额的函数
*/
function deal_DeformityRate(j){
  rate_flag = "1";//初始化标志
  var DeformityGrade = DeformityGrid.getRowColData(j,1);
  var AppDeformityRate =DeformityGrid.getRowColData(j,5);
  var DeformityRate = DeformityGrid.getRowColData(j,6);
  var Remark = DeformityGrid.getRowColData(j,7);
  if(DeformityRate==""||DeformityRate=="null"){
    alert("请您录入实际给付比例");
    rate_flag = "0";
    return;
  }

  if(DeformityGrade=="Y03"){
    if(DeformityRate>0.5){
      alert("重要器官切除的给付比例不能超过50%！");
      rate_flag = "0"
      return;
    }
  }

  if(DeformityGrade=="Y01"||DeformityGrade=="Y02"||DeformityGrade=="Y04"){
    if(DeformityRate>1){
      alert("给付比例不能超过100%！");
      rate_flag = "0";
      return;
    }
  }

  if(rate_flag==1){
    if(!(DeformityRate==AppDeformityRate)){
      if(Remark==""||Remark=="null"){
        alert("给付比例与实际给付比例不相等，请在备注中录入不相等的原因！！");
        rate_flag = "0";
        return;
      }
    }
    else{
      rate_flag = "1";
    }
  }
}

function getObservationDate(parm1) {
	var DisabilityDate = fm.all(parm1).all('DisabilityGrid3').value;
	if (isDate(DisabilityDate)) {
		var tSQL = "select case when date('"+DisabilityDate+"')+180 days - 6 years > custbirthday "
	         + " then date('"+DisabilityDate+"')+180 days else custbirthday + 6 years end "
           + " from llcase where caseno = '"+fm.CaseNo.value+"' with ur";
    var bresult = easyExecSql(tSQL);
    var tObservationDate = bresult[0][0];
    DisabilityGrid.setRowColData(parm1,4,tObservationDate);
  } else {
  	alert("失能日期错误！");
  }
}

function setHosInfo(parm1) {
	var tHospitalCode="";
	var tHospitalName="";
	var rowNum=ICaseCureGrid.mulLineCount;
	for (var i=0; i<rowNum; i++) {
		if (ICaseCureGrid.getChkNo(i)) {
			tHospitalCode=ICaseCureGrid.getRowColDataByName(i,"HospitalCode");
			tHospitalName=ICaseCureGrid.getRowColDataByName(i,"HospitalName");
			break;
		}
	}
	DiseaseGrid.setRowColData(DiseaseGrid.mulLineCount-1,10,tHospitalCode);
	DiseaseGrid.setRowColData(DiseaseGrid.mulLineCount-1,11,tHospitalName);
}
