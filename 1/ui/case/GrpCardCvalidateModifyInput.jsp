<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAMarketInput.jsp
//程序功能：F1报表生成
//创建日期：2008-07-25
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.utility.*" %>
<%
    GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
%>

<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="GrpCardCvalidateModifyInput.js"></SCRIPT>  
 <%@include file="GrpCardCvalidateModifyInit.jsp"%> 
 
<script language="javascript">
   function initDate(){
		
  }
   </script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body  onload="initDate();initForm();">    
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			卡折个单保险期间平移
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
      	<TR  class= common>
               
      	 <TD  class= title8>团体印刷号</TD>
      	 <TD  class= input8><Input class= common name="PrtNo" ></TD>
				 <TD  class= title8>团体保单号</TD>
      	 <TD  class= input8><Input class= common name="GrpContNo" ></TD>
      	</TR>
      	<TR class = common >
      	 <TD  class= title8>客户号码</TD>
      	 <TD  class= input8><Input class= common name="InsuredNo" ></TD>
				 <TD  class= title8>客户姓名</TD>
      	 <TD  class= input8><Input class= common name="InsuredName" ></TD>
        </tr>
    </table>
          
    <Div  id= "divLLAppClaimReasonGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpContGrid" >
            </span>
          </TD>
        </TR>
      </table>	
         <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">      
    </Div> 
        <table>
        <tr>
		 <td>
		   <input class=cssButton style='width:80px;' type=button value="查 询" onclick="SearchGrpCont()">
		   <input class=cssButton style='width:80px;' type=button value="修 改" onclick="modify();">
		 </td>
        </TR>  
        
       
    </table>
    <input type=hidden name=operate>
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		<iframe id="printfrm" src="" width=10 height=10></iframe>
    
</body>
</html> 
