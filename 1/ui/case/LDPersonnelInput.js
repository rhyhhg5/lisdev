//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
//window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
// # 3091 理赔直系亲属身份证校验**start**
	if(!checkFamilyGrid()){
		  return false;
	}
// # 3091 理赔直系亲属身份证校验**end**
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDPersonnel.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
	 //# 3340 人员信息档案管理模块**start**
	    if(!checkFamilyGrid()){
	 	  return false;
	   }
	 //# 3340 人员信息档案管理模块**end**
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDPersonnelQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
//	alert("1111");
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
        fm.all('UserCode').value= arrResult[0][0];
        fm.all('CustomerNo').value= arrResult[0][1];
        fm.all('Name').value= arrResult[0][2];
        fm.all('OccupationGrade').value= arrResult[0][3];
        fm.all('ManageCom').value=arrResult[0][4]
        fm.all('Sex').value= arrResult[0][5];
        fm.all('Birthday').value= arrResult[0][6];
        fm.all('IDType').value= arrResult[0][7];
        fm.all('IDTypeName').value= arrResult[0][8];
        fm.all('IDNo').value= arrResult[0][9];
        fm.all('speciality').value= arrResult[0][10];
        fm.all('specialityName').value= arrResult[0][11];
        fm.all('OccupationGrade').value= arrResult[0][12];
        fm.all('OccupationGradeName').value= arrResult[0][13];
        fm.all('Email').value= arrResult[0][14];
        fm.all('Mobile').value= arrResult[0][15];
        fm.all('Phone').value= arrResult[0][16];
        fm.all('SexName').value= arrResult[0][17];
        // # 3340人员信息档案管理模块**start**
        fm.all('DocBack').value= arrResult[0][18];
        fm.all('WorkAge').value= arrResult[0][19];
        fm.all('certificationType').value= arrResult[0][20];
        fm.all('certificationName').value= arrResult[0][21];
        fm.all('DocBackName').value = arrResult[0][22];
        
        //显示用户状态
        var strSQL2="select stateflag  from llclaimuser where usercode='"+fm.all('UserCode').value+"' with ur ";
        var aStateFlag= easyExecSql(strSQL2);
//        alert(aStateFlag);
        if(aStateFlag=='1')
        	fm.all('stateflag').value='有效';
	    if(aStateFlag=='2')
	        fm.all('stateflag').value='休假';
	    if(aStateFlag=='3')
	        fm.all('stateflag').value='离职';
	    if(aStateFlag=='4')
	        fm.all('stateflag').value='离岗';
	    if(aStateFlag=='9')
	        fm.all('stateflag').value='其他无效情况';
        
    	initFamilyDisGrid();  
    	var strSQL1="select RelationName,RelationIDNO,case when relation='0' then '本人' when Relation='1' then '夫妻' " +
    			" when Relation='2' then '父母' when Relation='3' then '子女' else ''end ," +" Relation,serialno from llrelationclaim" +
    			" where usercode='"+fm.UserCode.value+"' with ur" ;
    	turnPage.queryModal(strSQL1, FamilyDisGrid);

    	// # 3340人员信息档案管理模块**end**

	}
	
}               
        
function checkidtype() {
  if(fm.IDType.value=="") {
    alert("请先选择证件类型！");
    fm.IDNo.value="";
  }
}

function getBirthdaySexByIDNo(iIdNo) {
  if(fm.all('IDType').value=="0") {
    var tBirth = getBirthdatByIdNo(iIdNo);
      if(tBirth.length > 10){
        alert("您录入的身份证号有误!");
      }
      else{
      fm.all('Birthday').value=getBirthdatByIdNo(iIdNo);
      fm.all('Sex').value=getSexByIDNo(iIdNo);
      fm.SexName.value=(fm.Sex.value=='0'?'男':'女');
    }
  }
}

// #3340 人员信息档案管理模块**start**
function checkFamilyGrid(){
//		alert("1111111111");
		var Count=FamilyDisGrid.mulLineCount;
		for(var RelaNum=0;RelaNum<Count;RelaNum++){
			
				var tRelaName = FamilyDisGrid.getRowColData(RelaNum,1);
				if(tRelaName=="" || tRelaName==null){
					alert("第"+(RelaNum+1)+"行直系亲属录入信息不全，请输入亲属姓名！");
					return false;
				}
				
				var tIDNo=FamilyDisGrid.getRowColData(RelaNum,2);
				if(tIDNo=="" || tIDNo==null){
					alert("第"+(RelaNum+1)+"行直系亲属录入信息不全，请录入身份证号！");
					return false;
				}
				if(!checkIdCard(tIDNo)){
					return false;
				}
				var tRelaType=FamilyDisGrid.getRowColData(RelaNum,3);
				if(tRelaType==""||tRelaType==null){
					alert("第"+(RelaNum+1)+"行直系亲属录入信息不全，请录入与员工关系！");
					return false;
				}
				
				
			}
		
		return true;
	}
	//# 3091 理赔人员亲属身份证校验 **end**

