<%@page contentType="text/html;charset=GBK" %>

<%
//name :TemPolReturnDetail.jsp
//function: show Temporary Pol Info
//date :2003-04-09
//creator :刘岩松
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>


<%
  //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    String tRgtNo = "";
    String tInsuredNo = "";

    System.out.println("Begin to execute 'TemPolReturnDetail.jsp'!!!");
    tRgtNo=request.getParameter("RgtNo");
    tInsuredNo = request.getParameter("InsuredNo");
    GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		System.out.println("rgtno=="+tRgtNo);
    System.out.println("客户号码是===="+tInsuredNo);
    System.out.println("登陆机构是＝＝＝＝"+tG);
    VData tVData = new VData();
    tVData.addElement(tRgtNo);
    tVData.addElement(tInsuredNo);
    tVData.addElement(tG);

    ShowTemPolInfoUI tShowTemPolInfoUI = new ShowTemPolInfoUI();
    if (!tShowTemPolInfoUI.submitData(tVData,"QUERY||MAIN"))
    {
    		Content = " 查询失败，原因是: " + tShowTemPolInfoUI.mErrors.getError(0).errorMessage;
    		FlagStr = "Fail";
    }
    else
    {
			tVData.clear();
			tVData = tShowTemPolInfoUI.getResult();
			LCPolSet mLCPolSet = new LCPolSet();
			SSRS ssrs = new SSRS();
			ssrs = ((SSRS)tVData.getObjectByObjectName("SSRS",0));
			int n = ssrs.getMaxRow();
			System.out.println("get Register "+n);
		%>
			<script language="javascript">
				parent.fraInterface.TemPolInfoGrid.clearData();
			</script>
		<%
			for (int i = 1; i <= n; i++)
			{
        String SaleChnl = "";
        if(!(ssrs.GetText(i,8).equals("")||ssrs.GetText(i,8)==null))
        {
          if(ssrs.GetText(i,8).equals("01"))
            SaleChnl = "团险";
          if(!(ssrs.GetText(i,8).equals("01")))
             SaleChnl = "个险";
        }


		%>
		  <script language="javascript">
		   	parent.fraInterface.TemPolInfoGrid.addOne("TemPolInfoGrid")
		   	parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,1,"<%=ssrs.GetText(i,1)%>");
		   	parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,2,"<%=ssrs.GetText(i,2)%>");
		   	parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,3,"<%=ssrs.GetText(i,3)%>");
		   	parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,5,"<%=ssrs.GetText(i,4)%>");
		   	parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,6,"<%=ssrs.GetText(i,5)%>");
		   	parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,7,"<%=ssrs.GetText(i,6)%>");
		   	parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,8,"<%=ssrs.GetText(i,7)%>");
        parent.fraInterface.TemPolInfoGrid.setRowColData(<%=i-1%>,4,"<%=SaleChnl%>");
		  </script>
			<%
			}
  	}

    if (FlagStr == "Fail")
    {
    		tError = tShowTemPolInfoUI.mErrors;
    		if (!tError.needDealError())
    		{
    				Content = " 查询成功! ";
    				FlagStr = "Succ";
    		}
    		else
    		{
    				Content = " 查询失败，原因是:" + tError.getFirstError();
    				FlagStr = "Fail";
    		}
    }
    System.out.println("------end------");
    System.out.println(FlagStr);
    System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>