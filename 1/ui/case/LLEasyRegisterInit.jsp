<%
//Name：LLEasyRegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2008-06-01 17:44:28
//Author  ：MN
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

String LoadFlag= request.getParameter("LoadFlag");
LoadFlag= LoadFlag==null?"":LoadFlag;
String RgtNo= request.getParameter("RgtNo") ;

//RgtNo=RgtNo== null ?"":RgtNo;
String CaseNo= request.getParameter("CaseNo");
//CaseNo=CaseNo==null?"":CaseNo;
String LoadC="";
if(request.getParameter("LoadC")!=null)
{
LoadC = request.getParameter("LoadC");
}
%>

<script language="JavaScript">
  var loadFlag="<%=LoadFlag%>";
  var RgtNo = '<%=RgtNo%>';
  RgtNo= (RgtNo=='null'?"":RgtNo);
  var CaseNo = '<%=CaseNo %>';
  CaseNo= (CaseNo=='null'?"":CaseNo);

  function initInpBox( )
  {
    try {
      fm.reset();
      fm.CustomerNo.value="";
//      fm.CaseOrder.value="1";
//      fm.CaseOrderName.value="普通";
      fm.LoadFlag.value = loadFlag;
      fm.RgtNo.value = RgtNo;
      fm.CaseNo.value = CaseNo;
      fm.LoadC.value="<%=LoadC%>";
      fm.RiskCode.value = "<%=request.getParameter("RiskCode")%>"
      if(fm.RiskCode.value=='1605'){
        tiAppMoney.style.display='';
        idAppMoney.style.display='';
        titemp0.style.display='none';
        idtemp0.style.display='none';
      }
      else{
        tiAppMoney.style.display='none';
        idAppMoney.style.display='none';
        titemp0.style.display='';
        idtemp0.style.display='';
      }
      if (fm.LoadC.value=='2'){
        //			fm.CaseNo.focus();
        aa.style.display='none';
        divnormalquesbtn.style.display='none';
        //    	CaseChange();
      }

    }
    catch(ex) {
      alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initForm(){
    try{
      titleGrp.style.display='none';
      inputGrp.style.display='none';
      if(loadFlag=="0")
      {
        titleGrp.style.display='';
        inputGrp.style.display='';
        div1.style.display='none';
        divRegisterInfo.style.display='none';
        fm.Relation.value='05';
        fm.RgtType.value='9';
      }
      initInpBox();
      initEventGrid();
      initEasySecuGrid();
      initSGSecurityGrid();
      initClaimDetailGrid();
      initClaimResultGrid();
      checkRgtFlag();
      initQuery();
      initDate();
      <%GlobalInput mG = new GlobalInput();
      mG=(GlobalInput)session.getValue("GI");
      %>
      fm.Handler.value = "<%=mG.Operator%>";
      fm.MngCom.value = "<%=mG.ManageCom%>"
      fm.ModifyDate.value = getCurrentDate();
    }
    catch(re){
      alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
    }
  }

  // 保单信息列表的初始化
  function initEventGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","0px","10","0");
      iArray[1]=new Array("事件号","100px","100","0");

      iArray[2]=new Array("发生日期","80px","100","1");
      iArray[2][9]="发生日期|notnull&date";

      iArray[3]=new Array("发生地点","150px","100","1");

      iArray[4] = new Array("事故类型","80px","0","3");
      iArray[5] = new Array("事故主题","200px","0","3");
      iArray[6] = new Array("医院名称","200px","0","3");

      iArray[7]=new Array("入院日期","80px","100","1");
      iArray[7][9]="入院日期|date";

      iArray[8]=new Array("出院日期","80px","100","1");
      iArray[8][9]="出院日期|date";

      iArray[9] = new Array("事件信息","200px","1000","1");

      iArray[10] = new Array("事件类型","60px","10","2")
      iArray[10][10]="AccType";
      iArray[10][11]="0|^1|疾病|^2|意外"
      iArray[10][12]="10|11"
      iArray[10][13]="1|0"

      iArray[11] = new Array("事件类型","60px","10","3")

      EventGrid = new MulLineEnter("fm","EventGrid");
      EventGrid.mulLineCount = 0;
      EventGrid.displayTitle = 1;
      EventGrid.locked = 0;
      EventGrid.canChk =1	;
      EventGrid.hiddenPlus=0;
      EventGrid.hiddenSubtraction=1;
      EventGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }
  
    function initEasySecuGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","100","0");
      iArray[1]=new Array("MainFeeNo","0px","100","1");
      iArray[2]=new Array("账单号码","40px","100","1");
      iArray[3]=new Array("医院代码","30px","100");
      iArray[3][3]= 2;
      iArray[3][4]="hospital";
      iArray[3][5]="3|4";
      iArray[3][6]="0|1";
      iArray[3][14]= "1100088";

      iArray[4]=new Array("医院名称","50px","100","1");
      iArray[4][14]= "汇总医院";
      iArray[5]=new Array("发生费用时间","40px","100","1");
      iArray[5][9]="发生费用日期|notnull&date";
      iArray[5][14]= fm.AccDate.value;
     
      iArray[6]=new Array("费用总金额","40px","100","1");
      iArray[7]=new Array("自付一","40px","100","1");
      iArray[8]=new Array("小额门急诊","40px","100","1");
      iArray[9]=new Array("大额门急诊","40px","100","1");
      iArray[10]=new Array("自费","40px","100","1");

      EasySecuGrid = new MulLineEnter( "fm" , "EasySecuGrid" );
      EasySecuGrid.mulLineCount = 1;
      EasySecuGrid.displayTitle = 1;
      EasySecuGrid.canSel = 1;
      EasySecuGrid.canChk = 0;
      EasySecuGrid.locked = 0;
      EasySecuGrid.selBoxEventFuncName = "getFeeInfo";
      EasySecuGrid.loadMulLine(iArray);
      
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }
  
  function initSGSecurityGrid()
  {
    var iArray = new Array();
    var tInsuState = fm.InsuredStat.value;
    try
    {
      iArray[0]=new Array("序号","30px","100","0");
      iArray[1]=new Array("MainFeeNo","0px","100","1");
      iArray[2]=new Array("账单号码","40px","100","1");
      iArray[3]=new Array("医院代码","30px","100");
      iArray[3][3]= 2;
      iArray[3][4]="hospital";
      iArray[3][5]="3|4";
      iArray[3][6]="0|1";
      iArray[3][14]= "1100088";

      iArray[4]=new Array("医院名称","50px","100","1");
      iArray[4][14]= "汇总医院";
      iArray[5]=new Array("发生费用时间","40px","100","1");
      iArray[5][9]="发生费用日期|notnull&date";
      iArray[5][14]= fm.AccDate.value;
     
      iArray[6]=new Array("费用总金额","40px","100","1");
      iArray[7]=new Array("门诊大额医疗支付","50px","100","1");
      if (tInsuState==2) {
      	iArray[8]=new Array("补充金额","40px","100","1");
      }else{
        iArray[8]=new Array("自付一","40px","100","1");
      }
      iArray[9]=new Array("自付二","40px","100","1");
      iArray[10]=new Array("自费","40px","100","1");

      SGSecurityGrid = new MulLineEnter( "fm" , "SGSecurityGrid" );
      SGSecurityGrid.mulLineCount = 1;
      SGSecurityGrid.displayTitle = 1;
      SGSecurityGrid.canSel = 1;
      SGSecurityGrid.canChk = 0;
      SGSecurityGrid.locked = 0;
      SGSecurityGrid.selBoxEventFuncName = "getFeeInfo";
      SGSecurityGrid.loadMulLine(iArray);
      
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initQuery(){

    var tRgtNo = fm.RgtNo .value;
    var tCaseNo = fm.CaseNo.value;
    ClientafterQuery(tRgtNo,tCaseNo);

  }

  function checkRgtFlag(){
    strSQL=" select rgtclass, apppeoples from LLRegister where rgtno = '" + fm.all('RgtNo').value + "'";
    arrResult=easyExecSql(strSQL);
    fm.all('rgtflag').value = '0';
    if(arrResult){
      fm.all('AppNum').value = arrResult[0][1];
      var RgtClass;
      try{RgtClass = arrResult[0][0]} catch(ex) {alert(ex.message + "RgtClass")}
      strSQL1 = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
      arrResult1 = easyExecSql(strSQL1);
      if (RgtClass == '1')
      {
        titleGrp.style.display='';
        inputGrp.style.display='';
        divRgtFinish.style.display='';
        titleAppNum.style.display='';
        inputAppNum.style.display='';
        fm.all('rgtflag').value = "1";
        div1.style.display='none';
        divRegisterInfo.style.display='none';
      }
    }
  }

function initClaimResultGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号","30px","100","0");

    iArray[1]=new Array("保单号码","80px","10","0");

    iArray[2]=new Array("险种代码","50px","10","2");
    iArray[2][4]="llclaimrisk"
    iArray[2][15]="保单号码"
    iArray[2][17]="1"

    iArray[3]= new Array("给付责任","120px","100","0");

    iArray[4]= new Array("账单金额","50px","100","0");
    iArray[5]= new Array("拒付金额","50px","100","0");
    iArray[6]= new Array("给付比例","100px","100","0");
    iArray[7]= new Array("免赔额","50px","100","0");
    iArray[8]= new Array("限额","50px","100","0");

    iArray[9]= new Array("理算金额","60px","100","0");
    iArray[10]= new Array("核算赔付金额","80px","100","3");
    iArray[11]= new Array("实赔金额","60px","100","0");

    iArray[12]= new Array("险种保单号","80px","100","3");
    iArray[13]= new Array("赔案号","80px","100","3");
    iArray[14]= new Array("给付责任代码","80px","100","3");
    iArray[15]= new Array("给付责任类型","80px","100","3");
    iArray[16]= new Array("责任代码","80px","100","3");
    iArray[17]= new Array("受理事故号","80px","100","3");
    iArray[18]= new Array("是否判断保额","80px","100","0");
    iArray[19]= new Array("保额","80px","100","0");
    iArray[20]= new Array("历史赔付","80px","100","0");

    ClaimResultGrid = new MulLineEnter( "fm" , "ClaimResultGrid" );
    ClaimResultGrid.mulLineCount = 1;
    ClaimResultGrid.displayTitle = 1;

    ClaimResultGrid.canSel=0;
    ClaimResultGrid.canChk=1;
    ClaimResultGrid.hiddenPlus=1;
    ClaimResultGrid.hiddenSubtraction=1;

    ClaimResultGrid.loadMulLine(iArray);

  }
  catch(ex)
  {
    alert(ex);
  }
}

function initClaimDetailGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号","30px","100","0");

    iArray[1]=new Array("保单号码","80px","10","0");

    iArray[2]=new Array("险种代码","50px","10","2");
    iArray[2][4]="llclaimrisk"
    iArray[2][15]="保单号码"
    iArray[2][17]="1"

    iArray[3]= new Array("给付责任","120px","100","0");

    iArray[4]= new Array("账单金额","50px","100","0");
    iArray[5]= new Array("拒付金额","50px","100","0");
    iArray[6]= new Array("先期给付","50px","100","0");
    iArray[7]= new Array("通融/协议给付比例","100px","100","0");
    iArray[8]= new Array("免赔额","50px","100","3");
    iArray[9]= new Array("溢额","50px","100","0");

    iArray[10]= new Array("赔付结论","60px","100","2","llclaimdecision","10|20","1|0");

    iArray[11]= new Array("赔付结论依据","80px","100","2","llclaimdecision_1", "11|21","1|0");
    iArray[11][15]="赔付结论代码"
    iArray[11][17]="20"

    iArray[12]= new Array("理算金额","60px","100","0");
    iArray[13]= new Array("核算赔付金额","80px","100","3");
    iArray[14]= new Array("实赔金额","60px","100","0");

    iArray[15]= new Array("险种保单号","80px","100","3");
    iArray[16]= new Array("赔案号","80px","100","3");
    iArray[17]= new Array("给付责任代码","80px","100","3");
    iArray[18]= new Array("给付责任类型","80px","100","3");
    iArray[19]= new Array("责任代码","80px","100","3");
    iArray[20]= new Array("赔付结论代码","80px","100","3");
    iArray[21]= new Array("赔付结论依据代码","80px","100","3");
    iArray[22]= new Array("受理事故号","80px","100","3");
    iArray[23]= new Array("比例/金额","50px","10","2")
    iArray[23][10]="IsRate";
    iArray[23][11]="0|^1|比例|^2|金额"
    iArray[23][12]="23"
    iArray[23][13]="0"

    ClaimDetailGrid = new MulLineEnter( "fm" , "ClaimDetailGrid" );
    ClaimDetailGrid.mulLineCount = 1;
    ClaimDetailGrid.displayTitle = 1;

    ClaimDetailGrid.canSel=0;
    ClaimDetailGrid.canChk=1;
    ClaimDetailGrid.hiddenPlus=1;
    ClaimDetailGrid.hiddenSubtraction=1;

    ClaimDetailGrid.loadMulLine(iArray);

  }
  catch(ex)
  {
    alert(ex);
  }
}

</script>