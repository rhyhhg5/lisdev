<html>
<%
//Name：GrpRgtQuery.jsp
//function：弹出页面查询团体案件信息
//Date：2005-12-23 16:49:22
//Author：Xx
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="GrpRgtQuery.js"></SCRIPT>
   <%@include file="GrpRgtQueryInit.jsp"%>
 </head>

 <body  onload="initForm();" >
   <form action="./LLGrpClaimCloseConfListSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegisterSearch);">
         </TD>
         <TD class= titleImg>
         团体案件列表
         </TD>
       </TR>
      </table>

	    <div id= "divGrpRegisterSearch" style= "display: ''" >
		<table  class= common>
		<TR  class= common8>
		<TD  class= title8>团体批次号</TD>
		<TD  class= input8><Input class= common name="srRgtNo"  onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>团体客户号</TD>
		<TD  class= input8><Input class=common name="srCustomerNo" onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>单位名称</TD>
		<TD  class= input8><Input class=common name="srGrpName" onkeydown="QueryOnKeyDown();"></TD>
	
		</TR>
		<tr>
		<TD  class= title8>申请人</TD>
		<TD  class= input8><input class= common name="srRgtantName" onkeydown="QueryOnKeyDown();"></TD>	
		<TD  class= title8>申请日期 起始</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateStart onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>结束</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateEnd onkeydown="QueryOnKeyDown();"></TD>			
		</tr>
		</table>
	    </div>

   <input name="AskIn" style="display:'none'"  class=cssButton type=button value="查 询" onclick="SearchGrpRegister()">
   

    <Div  id= "divCustomerInfo" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpRegisterGrid" >
            </span>
          </TD>
        </TR>
      </table>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 	
    </Div>
    
        <hr>
        
<input name="AskIn" style="display:''"  class=cssButton type=button value="返回" onclick="returnParent()">
     
     <hr>
<Div  id= "divGrpCaseInfo" style= "display: 'none'">
	  <table>
	    	<tr>
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
	    	</td>
	    	<td class= titleImg>
	    	个人客户信息
	    	</td>
	    </tr>
	    </table>
    <Div  id= "divGrpCaseGrid" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpCaseGrid" >
            </span>
          </TD>
        </TR>
      </table>	
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">      
    </Div> 
</Div>  
     
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="operate" >
     <Input type="hidden" class= common name="RgtNo" >
     <Input type="hidden" class= common name="RgtState" >
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
