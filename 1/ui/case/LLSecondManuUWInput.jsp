<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ManuUWInput.jsp.
//程序功能：理赔个人人工核保
//创建日期：2005-12-29 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	
	String tMissionID = "";
	String tSubMissionID = "";
	String tCaseNo = "";
	String tBatNo = "";
	String tInsuredNo="";	
	
	tMissionID = request.getParameter("MissionID");
	tSubMissionID =  request.getParameter("SubMissionID");
	tCaseNo = request.getParameter("CaseNo");
	tBatNo = request.getParameter("BatNo");
	tInsuredNo = request.getParameter("InsuredNo");
	
%>
<html>
<%
  //个人下个人
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>

	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";

	var CaseNo = "<%=tCaseNo%>";
	
	var BatNo = "<%=tBatNo%>"

	var InsuredNo = "<%=tInsuredNo%>"
	var uwgrade = "";
    var appgrade= "";
    var uwpopedomgrade = "";
    var codeSql = "code";
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LLSecondManuUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLSecondManuUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./ManuUWCho.jsp">
  	
  	 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
    		</td>
    		<td class= titleImg>
    			 合同信息
    		</td>
    	    </tr>
        </table>	
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolAddGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
  	
    <!-- 保单查询条件 -->
    <div id="divSearch">
    	    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>投保单号</TD>
          <TD  class= input> <Input class= common name=QProposalNo > </TD>
          <TD  class= title>印刷号码</TD>
          <TD  class= input> <Input class=common name=QPrtNo> </TD>          
          <TD  class= title> 管理机构  </TD>
          <TD  class= input>  <Input class="code" name=QManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 核保级别  </TD>
          <TD  class= input>  <Input class="code" name=UWGradeStatus value= "1" CodeData= "0|^1|本级保单^2|下级保单" ondblClick="showCodeListEx('Grade',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('Grade',[this,''],[0,1]);">  </TD>
          <TD  class= title>  投保人名称 </TD>
          <TD  class= input> <Input class=common name=QAppntName > </TD>  
          <TD  class= title>  核保人  </TD>
          <TD  class= input>   <Input class= common name=QOperator value= "">   </TD>     
        </TR>
        <TR  class= common>
          <TD  class= title>  保单状态 </TD>
          <TD  class= input>  <Input class="code" readonly name=State value= "" CodeData= "0|^1|未人工核保^2|核保已回复^3|核保未回复" ondblClick="showCodeListEx('State',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('State',[this,''],[0,1]);"> </TD>

        </TR>
        <TR  class= common>
          <TD  class= input>   <Input  type= "hidden" class= Common name = QComCode >  </TD>

       </TR>
    </table>
          <INPUT VALUE="查  询" class=cssButton TYPE=button onclick="easyQueryClick();"> 
          <!--INPUT VALUE="撤单申请查询" class=common TYPE=button onclick="withdrawQueryClick();"--> 
          <INPUT type= "hidden" name= "Operator" value= ""> 
    </div>
    <!--合同信息-->
	<DIV id=DivLCContButton STYLE="display:'none'">
	<table id="table1">
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivLCCont);">
				</td>
				<td class="titleImg">合同信息
				</td>
			</tr>
	</table>
	</DIV>
	<div id="DivLCCont" STYLE="display:'none'">
		<table class="common" id="table2">
			<tr CLASS="common">
				<td CLASS="title" nowrap>印刷号码 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="PrtNo" VALUE CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="14">
	    		</td>
				<td CLASS="title" nowrap>管理机构 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="ManageCom"  MAXLENGTH="10" CLASS="readonly" readonly>
	    		</td>
				<td CLASS="title" nowrap>销售渠道 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SaleChnl" CLASS="readonly" readonly MAXLENGTH="2">
	    		</td>
			</tr>
			<tr CLASS="common">
				<td CLASS="title">代理人编码 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="AgentCode" MAXLENGTH="10" CLASS="readonly" readonly>
	    		</td>
				<td CLASS="title">备注 
	    		</td>
				<td CLASS="input" COLSPAN="3">
					<input NAME="Remark" CLASS="readonly" readonly MAXLENGTH="255">
	    		</td>	    		
			</tr>
			  <input NAME="ProposalContNo" type=hidden CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="20">
				<input NAME="AgentGroup" type=hidden CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="12">
				<input NAME="AgentCode1" type=hidden MAXLENGTH="10" CLASS="readonly" readonly>
				<input NAME="AgentCom" type=hidden CLASS="readonly" readonly>
				<input NAME="AgentType" type=hidden CLASS="readonly" readonly>
		</table>
	</div>
	<div id=DivLCSendTrance STYLE="display:'none'">
		<table class = "common">
				<tr CLASS="common">
			    <td CLASS="title">上报标志 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SendFlag" MAXLENGTH="10" CLASS="readonly" readonly>
	    		</td>
				<td CLASS="title">核保结论 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SendUWFlag" CLASS="readonly" readonly>
	    		</td>
				<td CLASS="title">核保意见
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SendUWIdea" CLASS="readonly" readonly>
	    		</td>
			</tr>
		</table>
	</div>
	
	 
	<DIV id=DivLCAppntIndButton STYLE="display:'none'">
	<!-- 投保人信息部分 -->
	<table>
	<tr>
	<td>
	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
	</td>
	<td class= titleImg>
	投保人信息
	</td>
	</tr>
	</table>
	</DIV>
	
	<DIV id=DivLCAppntInd STYLE="display:'none'">
	 <table  class= common>
	        <TR  class= common>        
	          <TD  class= title>
	            姓名
	          </TD>
	          <TD  class= input>
	            <Input CLASS="readonly" readonly name=AppntName>
	          </TD>
	          <TD  class= title>
	            性别
	          </TD>
	          <TD  class= input>
	            <Input name=AppntSex CLASS="readonly" >
	          </TD>
	          <TD  class= title>
	            年龄
	          </TD>
	          <TD  class= input>
	          <input CLASS="readonly" readonly name="AppntBirthday">
	          </TD>
	        </TR>
        
	        <TR  class= common>
	            <Input CLASS="readonly" readonly type=hidden name="AppntNo">
	            <Input CLASS="readonly" readonly  type=hidden name="AddressNo">
	          <TD  class= title>
	            职业
	          </TD>
	          <TD  class= input>
	          <input CLASS="readonly" readonly name="OccupationCode">
	          </TD>	         
	          <TD  class= title>
	            国籍
	          </TD>
	          <TD  class= input>
	          <input CLASS="readonly" readonly name="NativePlace">
	          </TD>
	          <TD  class= title>
           	 VIP标记
          	</TD>
          	<TD  class= input>
            	<Input class="readonly" readonly name=VIPValue >
            	
          	</TD>
	        </TR>
	        <TR>
	        <TD  class="title" nowrap>
            	黑名单标记
          	</TD>
         	 <TD  class= input>
            	<Input class="readonly" readonly name=BlacklistFlag >
            
          	</TD>
	        </TR>
	      </table>   
	</DIV>
    <!-- 保单查询结果部分（列表） -->
   <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单查询结果
    		</td>
    	</tr>  	
    </table>
    </Div>
         <Div  id= "divLCPol1" style= "display: ''" align = center>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();">      
    </div>
	    <INPUT VALUE="投保单明细" class=cssButton TYPE=button onclick="showPolDetail();"> 
	    <INPUT VALUE="扫描件查询" class=cssButton  TYPE=button onclick="ScanQuery();">  
	    <INPUT VALUE="投保人既往投保信息" class=cssButton TYPE=button onclick="showApp(1);"> 
	    <input value="体检录入" class=cssButton type=button onclick="showHealth();" width="200">
	    <input value="契调录入" class=cssButton type=button onclick="showRReport();">   
	    <input value="记事本" class=cssButton type=button onclick="showNotePad();" width="200">   
	      <input value = "进入险种信息" class = cssButton type = button onclick="entryRiskInfo()">
 
   <hr></hr>
   <Div  id= "divMain" style= "display: 'none'">
    <!--附加险-->
    <Div  id= "divLCPol2" style= "display: 'none'" >
    
         					
    </Div>

	<table class= common border=0 width=100% >
	 
	  <div id=divUWButton1 style="display:''">
          <span id= "divAddButton1" style= "display: ''">
        
          </span>                      
        
           	<!--input value="承保计划变更" class=cssButton type=button onclick="showChangePlan();"-->
         
          	<!--input value="承保计划变更结论录入" class=cssButton type=button onclick="showChangeResultView();"-->
            
          
        <br></br>   
     </div>
    </table>
    	  
          <INPUT  type= "hidden" class= Common name= UWGrade value= "">
          <INPUT  type= "hidden" class= Common name= AppGrade value= "">
          <INPUT  type= "hidden" class= Common name= PolNo  value= "">
          <INPUT  type= "hidden" class= Common name= "ContNo"  value= "">
          <INPUT  type= "hidden" class= Common name= "YesOrNo"  value= "">
          <INPUT  type= "hidden"  class= Common name= "MissionID"  value= "">
          <INPUT  type= "hidden"  class= Common name= "SubMissionID"  value= "">
          <INPUT  type= "hidden"  class= Common name= "SubConfirmMissionID"  value= "">
          <INPUT  type= "hidden" class= Common name= SubNoticeMissionID  value= "">
          <INPUT  type= "hidden" class= Common name= UWPopedomCode value= "">
          <Input type = "hidden" class = common name = "CaseNo" value = "">
          <Input type = "hidden" class= common name = "BatNo" value = "">
          
    <div id = "divUWResult" style = "display: ''">
        <!-- 核保结论 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>核保结论：</td></tr>
        </table>
	   		 	   	
        <table  class= common align=center>
          
            
            <TR>
                <TD class= title>
                    <!--span id= "UWResult"> 保全核保结论 <Input class="code" name=uwstate value= "1" CodeData= "0|^1|本级保单^2|下级保单" ondblClick="showCodeListEx('uwstate',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('uwstate',[this,''],[0,1]);">  </span-->
                    核保结论
                </td>
                
                <td class=input>
                    <Input class=codeno name=uwState ondblclick="return showCodeList('contuwstate',[this,uwStatename],[0,1]);" onkeyup="return showCodeListKey('contuwstate',[this,uwStatename],[0,1]);"><Input class=codename name=uwStatename readonly>                    
                </td>
                
                <!--td class=title8>
                    上报标志
                </td>
                                
                <td class=input8>
                    <Input class="codeno" name=uwUpReport ondblclick="return showCodeList('uwUpReport',[this,uwUpReportname],[0,1]);" onkeyup="return showCodeListKey('uwUpReport',[this,uwUpReportname],[0,1]);" onFocus="showUpDIV();"><Input class="codename" name=uwUpReportname readonly>
                </td-->
                                      
            </TR>               
        </table>            
            
        <!--****************************************************************
            修改人：续涛，修改时间20050420，修改原因：呈报
            ****************************************************************
        -->
            
        <div id = "divUWup" style = "display: ''">
        <table  class= common align=center>
            <TR>
                
                             
                <!--td class=title8> 
                    核保师级别
                </td>
                
                         
                <td class=input8>
                    <Input class="codeno" name=uwPopedom ondblclick="return showCodeList('uwPopedom',[this,uwPopedomname],[0,1]);" onkeyup="return showCodeListKey('uwPopedom',[this,uwPopedomname],[0,1]);"><Input class="codename" name=uwPopedomname readonly>
                </td-->
                <td class=title></td>
                <td class=input></td>
                                      
            </TR>                        
        </table>
        </div>
        
         <table class=common>
		
		<tr>
		      	<TD height="24"  class= title>
            		核保意见
          	</TD>
          	<td></td>
          	</tr>
		<tr>
      		<TD  class= input> <textarea name="UWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
      		<td></td>
      		</tr>
	  </table>
</div>
    
  	<div id = "divUWSave" style = "display: ''">
    		<INPUT VALUE="确  定" class=cssButton TYPE=button onclick="submitForm(0);">
    		<INPUT VALUE="取  消" class=cssButton TYPE=button onclick="cancelchk();">
        <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="InitClick();"> 
    </div>        
  	<div id = "divUWAgree" style = "display: 'none'">
    		<INPUT VALUE="同  意" class=cssButton TYPE=button onclick="submitForm(1);">
    		<INPUT VALUE="不同意" class=cssButton TYPE=button onclick="submitForm(2);">
    		<INPUT VALUE="返  回" class=cssButton TYPE=button onclick="InitClick();">  		
  	</div>

    <div id = "divChangeResult" style = "display: 'none'">
      	  <table  class= common align=center>
          	<TD height="24"  class= title>
            		承保计划变更结论录入:
          	</TD>
		<tr></tr>          
      		<TD  class= input><textarea name="ChangeIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
    	 </table>
    	 <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="showChangeResult();">
    	 <INPUT VALUE="取  消" class=cssButton TYPE=button onclick="HideChangeResult();">
    </div>
  </Div>
   <br></br>   
 <P>
      	<Input value = "总单确认" class = cssButton Type=button onclick="fullSubmit()">
</p>	     
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>  
</body>
</html>
