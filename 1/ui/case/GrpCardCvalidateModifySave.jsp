<%
//程序名称：BlackListInput.jsp
//程序功能：
//创建日期：2008-07-25
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LCContSchema tLCContSchema   = new LCContSchema();
	
  GrpCardCvalidateModifyUI tGrpCardCvalidateModifyUI = new GrpCardCvalidateModifyUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operate");
  tOperate=tOperate.trim();
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tRadio[] = request.getParameterValues("InpGrpContGridSel");
  String tGrpContNo[] = request.getParameterValues("GrpContGrid2");
  String tInsuredNo[] = request.getParameterValues("GrpContGrid5");
  String tCValidate[] = request.getParameterValues("GrpContGrid8");
  String tContNo[] = request.getParameterValues("GrpContGrid4");
  String GrpContNo="";
  String InsuredNo="";
  String CValidate="";
  String ContNo="";
  for (int index=0; index< tRadio.length;index++){
    if(tRadio[index].equals("1")){
      GrpContNo = tGrpContNo[index];
      InsuredNo = tInsuredNo[index];
      CValidate = tCValidate[index];
      ContNo = tContNo[index];
      System.out.println("GrpContNo:"+GrpContNo);     
      System.out.println("InsuredNo:"+InsuredNo);
      System.out.println("CValiDate:"+CValidate);
      System.out.println("ContNo:"+ContNo);
    }
  }
  tLCContSchema.setGrpContNo(GrpContNo);
  tLCContSchema.setInsuredNo(InsuredNo);
  tLCContSchema.setCValiDate(CValidate);
  tLCContSchema.setContNo(ContNo);
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLCContSchema);
	tVData.addElement(tG);
  try
  {
    tGrpCardCvalidateModifyUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tGrpCardCvalidateModifyUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	//Content = " 操作失败，原因是:" + tError.getFirstError();
    	Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

