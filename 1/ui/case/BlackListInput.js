//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//修改黑名单
function UpdateForm()
  
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
 var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//  fm.hideOperate.value=mOperate;
  fm.hideOperate.value="UPDATE";
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  showSubmitFrame(mDebug);
  fm.submit(); //提交
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
    //执行下一步操作
  }
  queryBlack();
  initList();
  
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("在BlackList.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{   var result = false;
    var count = BlackListReasonGrid.mulLineCount ;
    for( index=0;index<count ;index++) {
        var tchk = BlackListReasonGrid.getChkNo(index);
   		
   		if(tchk == true) {
   		    result = true;
   		}
    }	
    
    if( result == false ){
			alert( "请先选择录入原因!" );
			return false;
	}
	
  //下面增加相应的代码
  fm.hideOperate.value="INSERT";
    submitForm();
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	var tSel = BlackListGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
			alert( "请先选择一条记录。" );
			return false;
	}
	
	var result = false;
    var count = BlackListReasonGrid.mulLineCount ;
    for( index=0;index<count ;index++) {
        var tchk = BlackListReasonGrid.getChkNo(index);
   		
   		if(tchk == true) {
   		    result = true;
   		}
    }	
    
    if( result == false ){
			alert( "请先选择录入原因!" );
			return false;
	}
	
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
    fm.hideOperate.value="UPDATE";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./BlackListQuery.html");
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	var tSel = BlackListGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
			alert( "请先选择一条记录。" );
			return false;
	}
	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    fm.hideOperate.value="DELETE";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//Click事件，当点击普通客户查询时触发该函数
function ComQuery()
{
  //下面增加相应的代码
//  if (fm.all('BlackListNo').value = "")
  	showInfo=window.open("./ComClientQuery.html");
//  else
//  	{
//  		mOperate="QUERY||MAIN";
//  		submitForm();
//  	}
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all( 'BlackListNo' ).value = arrResult[0][0];
		fm.all('BlackName').value = arrResult[0][1];
		fm.all('BlackListType').value = arrResult[0][2];
		fm.all( 'BlackListMakeDate' ).value = "";
		fm.all('BlackListOperator').value = tOperator;
		fm.all('BlackListReason').value = "";
		fm.all('addBlack').disabled=false;
		fm.all('uptBlack').disabled=true;
 	}
}

function afterBlackQuery( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all( 'BlackListNo' ).value = arrResult[0][0];
		fm.all('BlackName').value = arrResult[0][2];
		var tBlackListType = arrResult[0][1];
		if (tBlackListType == "0")
			fm.all('BlackListType').value = "个人客户";
		if (tBlackListType == "1")
			fm.all('BlackListType').value = "集体客户";
        fm.all( 'BlackListMakeDate' ).value = arrResult[0][4];
		fm.all('BlackListOperator').value = arrResult[0][3];
		fm.all('BlackListReason').value = arrResult[0][5];
		fm.all('addBlack').disabled=true;
		fm.all('uptBlack').disabled=false;
 	}
}

//Click事件，当点击特殊客户查询时触发该函数
function EspQuery()
{
  //下面增加相应的代码
  	showInfo=window.open("./BlackListQuery.html");

}

function initList()
{
	 try
  {
    var strSQL1="select code,codename,codealias from LDCode where codetype='blacklistreason' and  substr(code,1,1)='5' order by code";
    arr1=easyExecSql(strSQL1);
		displayMultiline(arr1,BlackListReasonGrid);
  }
  catch(ex)
  {
  //	alert("查询附件表出错！");
	}
}

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		queryClient();
	}
}

function queryClient()
{
	strSQL = "select CustomerNo,Name,IDNo from ldperson where 1=1 "
	+getWherePart("CustomerNo","BlackListNo");
	//+getWherePart("Name","BlackName");
  	var arr=easyExecSql(strSQL);
  	if(arr!=null)
  	{
  		fm.BlackListNo.value=arr[0][0];
  		fm.BlackName.value=arr[0][1];
  		fm.IDNo.value   =arr[0][2];
  	  var strSql1="select phone,Postaladdress,zipcode from lcaddress where CustomerNo='"+fm.BlackListNo.value+"'";
          strSql1+=" and AddressNo='1'";
  	  var arr1=easyExecSql(strSql1);
  	  if(arr1!=null)
  	  {
  	  	fm.Phone.value=arr1[0][0];   
  	  	fm.Address.value=arr1[0][1];   
  	    fm.Zipcode.value=arr1[0][2];   
      }
  	}
  	fm.strsqlinit1.value="1 and insuredno= #"+fm.all('BlackListNo').value+"#"; 
}

function queryBlack()
{
	initBlackListGrid();
	var strSQL = "select blacklistserialno,blacklisttype,relaothernotype,relaotherno,relacontno from ldblacklist "
	           + " where blacklistno = '"+fm.BlackListNo.value+"' with ur";
	var tBlackList=easyExecSql(strSQL);
	if (tBlackList) {
		displayMultiline(tBlackList,BlackListGrid);
	}
}

function queryReason(){
	var tblackno = BlackListGrid.getSelNo ();
	var tBlackListNo =  BlackListGrid.getRowColData(tblackno-1,1);
	var strSql = "select blacklistserialno,BlackListType,relaothernotype,relacontno,relaotherno,blacklistoperator,blacklistmakedate "
	           + " from ldblacklist where blacklistserialno='"
	           + tBlackListNo+"'";
  var brr = easyExecSql(strSql);
  if(brr){
    var blacklistserialno = brr[0][0];
    fm.BlackListType.value=brr[0][1];
    if (brr[0][1]=='0') {
    	fm.BlackListTypeName.value="被保险人";
    }
    //fm.Origin.value=brr[0][2];
    if( brr[0][2]=='0'){
      fm.all('OriginName').value = "核保";
      fm.all('Origin').value = "0";
		}
		if( brr[0][2]=='1'){
			fm.all('OriginName').value = "理赔";
			fm.all('Origin').value = "1";
		}
    fm.ContNo.value=brr[0][3];
    fm.CaseNo.value=brr[0][4];
    fm.BlackListOperator.value = brr[0][5];
    fm.BlackListMakeDate.value = brr[0][6];
    BlackListReasonGrid.checkBoxAllNot();
    tsql = "select BlackListreasonkind from ldblacklistreasondetail where blacklistserialno = '"
    +tBlackListNo+"'";
			var br = easyExecSql ( tsql );
			//对已关联事件打勾
			if ( br)
			{
				pos =0;
				for ( i=0;i<arr1.length;i++)
				{
					for ( j=0;j<br.length ;j++)
					{
						if ( arr1[i][0]==	br[j][0] )
						{
							BlackListReasonGrid.checkBoxSel(i+1);

						}
					}
				}
			}
  }
}