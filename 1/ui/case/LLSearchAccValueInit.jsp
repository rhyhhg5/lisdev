<%
//程序名称：PayAffirmInit.jsp
//程序功能：给付确认的初始化
//创建日期：
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%

%>

<script language="JavaScript">
  function initInpBox()
  {
    try
    {
      <%
        GlobalInput mG = new GlobalInput();
        mG=(GlobalInput)session.getValue("GI");
        String mCurrentDate = PubFun.getCurrentDate();
      %>
      fm.all('InsuredNo').value = "<%=request.getParameter("InsuredNo")%>";
      fm.CalDate.value = "<%=mCurrentDate%>";
      fm.CurrentDate.value = "<%=mCurrentDate%>";
      var tSQL = "select Name from ldperson where CustomerNo='"+fm.InsuredNo.value+"'";
		  var xrr = easyExecSql(tSQL);
		  if(xrr!= null){
		  	fm.CustomerName.value = xrr[0][0];
		  }
     }
      catch(ex)
      {
        alter("在LLRgtSurveyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
      }
  }

function initForm()
{
  try
  {
  	initInpBox();
    initPolGrid();
    initOmnipotenceAccTraceGrid();
    easyQuery();
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 被保人信息列表的初始化
function initPolGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";    	           //列名
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=100;            			//列最大值
    iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号";         			//列名
    iArray[1][1]="100px";         			//列宽
    iArray[1][2]=10;          			    //列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]= new Array("险种编码","60px","100","0");
    iArray[3]= new Array("险种名称","170px","100","0");
    iArray[4]= new Array("生效日期","80px","100","0");
    iArray[5]= new Array("失效日期","80px","100","0");
    iArray[6]= new Array("险种保单号","80px","100","3");
    iArray[7]= new Array("账户类型","80px","100","3");
    PolGrid = new MulLineEnter( "fm" , "PolGrid" );
    //这些属性必须在loadMulLine前
    PolGrid.mulLineCount = 1;
    PolGrid.displayTitle = 1;
    PolGrid.canSel=1;
    PolGrid.canChk=0;
    PolGrid.hiddenPlus=1;
    PolGrid.hiddenSubtraction=1;
    PolGrid.selBoxEventFuncName = "onSelSearchAcc";

    PolGrid.loadMulLine(iArray);

    //这些操作必须在loadMulLine后
    //SubInsuredGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex);
  }
}

function initOmnipotenceAccTraceGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=3;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="发生时间";      //列名PayDate
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
   
    iArray[2]=new Array();
    iArray[2][0]="业务类型号";      //列名MoneyType
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=3;   
    
    iArray[3]=new Array();
    iArray[3][0]="业务类型";      //列名MoneyType汉字
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[4]=new Array();
    iArray[4][0]="结算月份";      //列名BalanMonth
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[5]=new Array();
    iArray[5][0]="业务号";      //列名OtherNo
    iArray[5][1]="180px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="增加金额";      //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[7]=new Array();
    iArray[7][0]="减少金额";      //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[8]=new Array();
    iArray[8][0]="最终金额";      //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[9]=new Array();
    iArray[9][0]="生成日期";      //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=3;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[10]=new Array();
    iArray[10][0]="生成时间";      //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=3;             //是否允许输入,1表示允许，0表示不允许
    
    OmnipotenceAccTraceGrid = new MulLineEnter("fm", "OmnipotenceAccTraceGrid"); 
	  //设置Grid属性
    OmnipotenceAccTraceGrid.mulLineCount = 10;
    OmnipotenceAccTraceGrid.displayTitle = 1;
    OmnipotenceAccTraceGrid.locked = 1;
    OmnipotenceAccTraceGrid.canSel = 0;	
    OmnipotenceAccTraceGrid.canChk = 0;
    OmnipotenceAccTraceGrid.hiddenSubtraction = 1;
    OmnipotenceAccTraceGrid.hiddenPlus = 1;
    OmnipotenceAccTraceGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在OmnipotenceAccInit.jsp-->initOmnipotenceAccTraceGrid函数中发生异常:初始化界面错误!");
  }
}

</script>