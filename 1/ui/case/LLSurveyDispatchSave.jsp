<%
//程序名称：LLSurveyReplySave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
 	<%@page import="com.sinosoft.lis.pubfun.*"%>
 	<%@page import="com.sinosoft.lis.db.*"%>
	<%@page contentType="text/html;charset=GBK" %>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLInqApplySchema tLLInqApplySchema   = new LLInqApplySchema();
  LLInqDispatchBL tLLInqDispatchBL   = new LLInqDispatchBL();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
  	System.out.println("transact : "+transact);
			String tRNum = request.getParameter("selno");
      String tOtherNo[] = request.getParameterValues("CheckGrid1");
      String tOtherNoType[] = request.getParameterValues("CheckGrid10");
      String tSurveyNo[] = request.getParameterValues("CheckGrid7");
      String tSurveyCom[] = request.getParameterValues("CheckGrid3");
      String tInqNo[] = request.getParameterValues("CheckGrid7");
      String ttSurveyCom = "";
      String tOperator = request.getParameter("Operator");
      System.out.println("daole");
      String tResult = request.getParameter("newResult");
      System.out.println(tResult);
      int i = Integer.parseInt(tRNum);
      tLLInqApplySchema.setOtherNo(tOtherNo[i]);
      tLLInqApplySchema.setOtherNoType(tOtherNoType[i]);
      tLLInqApplySchema.setSurveyNo(tSurveyNo[i]);
      tLLInqApplySchema.setInqNo(tInqNo[i]);
      ttSurveyCom = tSurveyCom[i];
      String tDealWith = request.getParameter("DealWith");
      String tDealer = "";
      if(!"".equals(request.getParameter("Surveyer")))
      	tDealer = request.getParameter("Surveyer");
      else
      	tDealer = request.getParameter("Dealer");
      String tComCode = request.getParameter("ComCode");
      String thdlComCode = request.getParameter("hdlComCode");
      String tComName = request.getParameter("ComName");
      if (ttSurveyCom.equals(tComCode))
      	tLLInqApplySchema.setLocFlag("0");
      else
      	tLLInqApplySchema.setLocFlag("1");
      if (tDealWith.equals("0"))
      {
      	tLLInqApplySchema.setSurveySite(tComName);
      	
      	String tDealerCom = null;
      	LDUserDB tLDUserDB = new LDUserDB();
      	LDUserSet tLDUserSet = new LDUserSet();
      	
      	tLDUserDB.setUserCode(tDealer);
      	tDealerCom = tLDUserDB.query().get(1).getComCode();
      	System.out.println(tDealerCom);
      	tLLInqApplySchema.setInqDept(tDealerCom);
      	tLLInqApplySchema.setSubFlag(tDealWith);
      	tLLInqApplySchema.setRemark(tResult);
      	if (thdlComCode.equals("86"))
      	{
      		tLLInqApplySchema.setDipatcher(tDealer);
      		
      	} else {
      		tLLInqApplySchema.setInqPer(tDealer);
      	}
      }else if(tDealWith.equals("2")){//目前核心已经没有调查案件转发的功能 此处存储逻辑形同虚设
        tLLInqApplySchema.setSubFlag(tDealWith);
      	tLLInqApplySchema.setSuber(tOperator);
        tLLInqApplySchema.setInqPer(tDealer);
        tLLInqApplySchema.setRemark(tResult);
      }else {
      	tLLInqApplySchema.setSubFlag(tDealWith);
      	tLLInqApplySchema.setSuber(tOperator);
      	tLLInqApplySchema.setDipatcher(tDealer);
      	tLLInqApplySchema.setDispatchCom("86");
      	tLLInqApplySchema.setRemark(tResult);
      }
   
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
		tVData.add(tLLInqApplySchema);
  	tVData.add(tG);
  	System.out.println("transact"+transact);
  	System.out.println("tVData"+tVData);
    tLLInqDispatchBL.submitData(tVData,transact);    
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLInqDispatchBL .mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
