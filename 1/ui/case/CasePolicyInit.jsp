<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
  var turnPage = new turnPageClass();
  function initInpBox()
  {
    try
    {
     fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
     fm.CaseRelaNo.value = '<%= request.getParameter("CaseRelaNo") %>';
     fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
     fm.CustomerNo.value ="<%=request.getParameter("CustomerNo")%>"
    }
    catch(ex)
    {
      alert("在LLCasePolicyInputInit.jsp-->InitInpBox函数中发生异常：初始化界面错误!");
    }
  }
  function initSelBox()
  {
    try
    {
    }
    catch(ex)
    {
      alert("在LLCasePolicyInputInit.jsp-->InitSelBox函数中发生异常：初始化界面错误!");
    }
  }
  function initForm()
  {
    try
    {

      initCasePolicyGrid();
      initInpBox();
      easyQuery();

    }
    catch(re)
    {
      alert("LLCasePolicyInputInit.jsp-->InitForm函数中发生异常111：初始化界面错误!");
    }
  }
//立案分案明细
  function initCasePolicyGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";
      iArray[0][1]="30px";
      iArray[0][2]=40;
      iArray[0][3]=0;

      
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("保单号码","100px","0",0);
    iArray[2]=new Array("被保险人姓名","100px","0",0);
    iArray[3]=new Array("险种代码","100px","0",0);
    iArray[4]=new Array("保险金额","100px","0",0);
    iArray[5]=new Array("生效日期","180px","0",0);
    iArray[6]=new Array("险种号","180px","0",3);
    

      CasePolicyGrid = new MulLineEnter( "fm" , "CasePolicyGrid" );
      CasePolicyGrid.displayTitle = 1;
      CasePolicyGrid.mulLineCount = 2;
      CasePolicyGrid.canChk =1;
      CasePolicyGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      CasePolicyGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)

      CasePolicyGrid.loadMulLine(iArray);
      CasePolicyGrid.lock ();
    }
    catch(ex)
    {
      alert(ex);
    }
  }
  
  function initPolicyDutyGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("保单号码","100px","0",0);
    iArray[2]=new Array("责任类型","100px","0",0);
   iArray[3]=new Array("责任代码","100px","0",0);
    iArray[4]=new Array("责任名称","100px","0",0);
   iArray[5]=new Array("预理算金额","100px","0",0);
   
   

    PolicyDutyGrid = new MulLineEnter("fm","PolicyDutyGrid");
    PolicyDutyGrid.mulLineCount = 1;
    PolicyDutyGrid.displayTitle = 1;
    PolicyDutyGrid.locked = 0;
    PolicyDutyGrid.canChk =1;
    PolicyDutyGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    PolicyDutyGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   PolicyDutyGrid. selBoxEventFuncName = "onSelSelected";
    PolicyDutyGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
</script>