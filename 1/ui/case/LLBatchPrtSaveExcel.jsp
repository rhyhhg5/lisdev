<%
  //程序名称：LLBatchPrtSaveExcel.jsp
  //程序功能：
  //创建日期：2013-11-26 20:00:00
  //创建人  ：Houyd
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.lang.String"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<%
  //接收信息，并作校验处理。
  NewLLBatchPrtExcelBL tNewLLBatchPrtExcelBL = new NewLLBatchPrtExcelBL();
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";

  String realpath = application.getRealPath("/").
  substring(0,application.getRealPath("/").length());//UI地址,为了生成Excel文件
  String temp = realpath.substring(realpath.length() - 1, realpath
			.length());
  if (!temp.equals("/")) {
		realpath = realpath + "/";
  }
  System.out.println(realpath);
  int tCount = 0;
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  System.out.println("Start Product XML:" + PubFun.getCurrentTime());
  tG.ClientIP = request.getHeader("X-Forwarded-For");
  if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		tG.ClientIP = request.getRemoteAddr(); 
  }
  tG.ServerIP = tG.GetServerIP();
  System.out.println("IP's address :" + tG.ClientIP);
  System.out.println("IP's address :" + tG.ClientIP);
  
  transact = request.getParameter("fmtransact");
  System.out.println("transact : " + transact);
  String tRgtNo = request.getParameter("GrpRgtNo");
  String tSCaseNo = request.getParameter("StartCaseNo");
  String tECaseNo = request.getParameter("EndCaseNo");
  String tRgtDateS = request.getParameter("RgtDateS");
  String tRgtDateE = request.getParameter("RgtDateE");
  String tEndDateS = request.getParameter("EndDateS");
  String tEndDateE = request.getParameter("EndDateE");
  String tCaseNoBatch = request.getParameter("SingleCaseNo");
  String tBatchType = request.getParameter("selno");
  String tDetailExcelPrt = request.getParameter("DetailExcel"); //理赔给付细目表（Excel）

  
  String outname=" "; 
  String outpathname=" ";
  
  TransferData PrintElement = new TransferData();
  PrintElement.setNameAndValue("RgtNo", tRgtNo);
  PrintElement.setNameAndValue("SCaseNo", tSCaseNo);
  PrintElement.setNameAndValue("ECaseNo", tECaseNo);
  PrintElement.setNameAndValue("RgtDateS", tRgtDateS);
  PrintElement.setNameAndValue("RgtDateE", tRgtDateE);
  PrintElement.setNameAndValue("EndDateS", tEndDateS);
  PrintElement.setNameAndValue("EndDateE", tEndDateE);
  PrintElement.setNameAndValue("CaseNoBatch", tCaseNoBatch);
  PrintElement.setNameAndValue("BatchType", tBatchType);
  PrintElement.setNameAndValue("DetailExcelPrt", tDetailExcelPrt); //理赔给付细目表（Excel）
  PrintElement.setNameAndValue("realpath", realpath);//应用实际路径
  
  System.out.println("BatchType：" + tBatchType);
  System.out.println("DetailExcelPrt：" + tDetailExcelPrt);
  System.out.println("realpath：" + realpath);
  
  
    VData tVData = new VData();
    tVData.add(PrintElement);
    tVData.add(tG);
    System.out.println("transact：" + transact);
    System.out.println("tVData：" + tVData);
    

    	if (!tNewLLBatchPrtExcelBL.submitData(tVData, "PRINT")) {
    		FlagStr = "Fail";
    		Content = tNewLLBatchPrtExcelBL.mErrors.getFirstError().toString();

    	}else{

    		Content = "操作成功!";
    	    FlagStr = "Success";
    		outname = tNewLLBatchPrtExcelBL.getFileName();
    		outpathname = realpath + "vtsfile/" + outname;
    		try 
    		{
    				outname = java.net.URLEncoder.encode(outname, "UTF-8");
    				outname = java.net.URLEncoder.encode(outname, "UTF-8");
    				outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
    				outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
    				System.out.println("outpathname="+outpathname);
    		} 
    		catch (UnsupportedEncodingException e) 
    		{
    				e.printStackTrace();
    		}   
    		System.out.println("--------成功----------");
    		System.out.println(outname+"========="+outpathname);
    		
    	}   
%>
    		

    <html>
    <script language="javascript">
    	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=outname%>","<%=outpathname%>");
    	parent.fraInterface.afterSubmit3("<%=FlagStr%>","<%=Content%>","<%=outname%>","<%=outpathname%>");
    </script>
    </html>