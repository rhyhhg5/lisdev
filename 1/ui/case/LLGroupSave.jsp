<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLAffixSave.jsp
//程序功能：
//创建日期：2005-02-5 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
 
  CErrors tError = null;
  LLValidityBL tLLValidityBL = new LLValidityBL();
  LICaseTimeRemindSchema tLICaseTimeRemindSchema = new LICaseTimeRemindSchema();
  LICaseTimeRemindSet tLICaseTimeRemindSet = new LICaseTimeRemindSet();
  PubFun pf = new PubFun();
  PubFun1 pf1 = new PubFun1();
  FDate fDate = new FDate();
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  
  //输出参数
  String FlagStr = "";
  String Content = "";

  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);

  
    System.out.println("开始存储证件有效期有问题的数据");
    String serialno = "LS"+pf.getCurrentDate2()+""+ pf1.CreateMaxNo("LSH_TimeRemind", 8);
    System.out.println("serialno:"+serialno);
    tLICaseTimeRemindSchema.setSerialNo(serialno);
    tLICaseTimeRemindSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLICaseTimeRemindSchema.setinsuredno(request.getParameter("CustomerNo"));
    tLICaseTimeRemindSchema.setinsuredname(request.getParameter("RgtantName"));
    tLICaseTimeRemindSchema.setidtype(request.getParameter("IDType"));
    tLICaseTimeRemindSchema.setidno(request.getParameter("IDNo"));
    tLICaseTimeRemindSchema.setIDStartDate(request.getParameter("IDStartDate"));
    tLICaseTimeRemindSchema.setIDEndDate(request.getParameter("IDEndDate"));
    tLICaseTimeRemindSchema.setReminddate(pf.getCurrentDate());
    tLICaseTimeRemindSchema.setRemindTime(pf.getCurrentTime());
    tLICaseTimeRemindSchema.setaddress(request.getParameter("RgtantAddress"));
    tLICaseTimeRemindSchema.setManageCom(tG.ManageCom);
    tLICaseTimeRemindSchema.setOperator(tG.Operator);
    tLICaseTimeRemindSchema.setMakeDate(pf.getCurrentDate());
    tLICaseTimeRemindSchema.setMakeTime(pf.getCurrentTime());
    tLICaseTimeRemindSchema.setmodifydate(pf.getCurrentDate());
    tLICaseTimeRemindSchema.setmodifytime(pf.getCurrentTime());
	  
    tLICaseTimeRemindSet.add(tLICaseTimeRemindSchema);
    
    
	   try
	   {		   
		   tVData.add(tLICaseTimeRemindSet);
		   tVData.add(tG);		   
		   tLLValidityBL.submitData(tVData,strOperate);
	   }
	 catch(Exception ex)
	    {
	      Content = "保存失败，原因是:" + ex.toString();
	      System.out.println("aaaa"+ex.toString());
	      FlagStr = "Fail";
	    }
	   
	   
	   if ("".equals(FlagStr))
	   { 
	     tError = tLLValidityBL.mErrors;
	     if (!tError.needDealError())
	     {                          
	     	Content = " 保存成功! ";
	     	FlagStr = "Success";
	     	tVData.clear();
	     	tVData=tLLValidityBL.getResult();              
	     }
	     else                                                                           
	     {
	     	Content = " 保存失败，原因是:" + tError.getFirstError();
	     	FlagStr = "Fail";
	     }
	   }
	    System.out.println("结束了");
  %>
	<%=Content%> 		 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>