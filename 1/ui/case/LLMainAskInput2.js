//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
//	alert(fm.AskMode.value);
	if(fm.SaveFlag.value=="1")
	{
		alert("该咨/询通知已保存!");
	  return false;
	}
		
  var i = 0;
  if (fm.Answer.value=="")
  {
  	fm.AskType.value="1";
  }
else
	{
		fm.AskType.value="0";
	}
  if(!checkDate(fm.AccDate1.value,fm.AccEndDate1.value)
  ||!checkDate(fm.InHospitalDate.value,fm.OutHospitalDate.value)){
  	alert("请确认日期先后顺序是否正确!");
  	return false;}
  if(!testDate()) 
  {
  	return false;}
 
  if( verifyInput2() == false ) return false;
 

  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.SaveFlag.value="1";
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
	initTitle();
	updateEventGrid();
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
       
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLMainAskInput2.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
   if(!checkDate(fm.AccDate1.value,fm.AccEndDate1.value)||!checkDate(fm.InHospitalDate.value,fm.OutHospitalDate.value)){
  	alert("请确认日期先后顺序是否正确!");
  	return false;}
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDiseaseQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//判断查询输入窗口的案件类型是否是回车， 
//如果是回车调用查询客户函数
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClientQuery();
		UpdateGrid();
	}
}
//查询客户信息
function ClientQuery()
{
	//openWindow("ClientQueryMain.html","客户查询");
	//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  	var strSql="select CustomerNo,Name,IDNo,Grpname from ldperson where CustomerNo='"+fm.LogerNo.value+"' or Name='"+fm.LogName.value+"'";

  	var arr=easyExecSql(strSql);
  	if(arr!=null)
  	{
  		fm.LogerNo.value=arr[0][0];
  		fm.LogName.value=arr[0][1];
  		fm.IDNo.value   =arr[0][2];
  		fm.LogComp.value = arr[0][3];
  	  var strSql1="select phone,mobile,email,Postaladdress,zipcode from lcaddress where CustomerNo='"+fm.LogerNo.value+"'";
          strSql1+=" and AddressNo='1'";
  	  var arr1=easyExecSql(strSql1);
  	  if(arr1!=null)
  	  {
  	  	fm.Phone.value=arr1[0][0];   
  	  	fm.Mobile.value=arr1[0][1];   
  	  	fm.Email.value   =arr1[0][2];
  	  	fm.AskAddress.value=arr1[0][3];   
  	    fm.PostCode.value=arr1[0][4];   
      }
  	}
}
//查询医院名称
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitalCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}
//回复咨询
 function ReplySave()
{
  if(fm.AskType.value=="0")
  {
      if( fm.ConsultNo.value =="")
      {
          alert("没有咨询信息");
          return false;
      }
  }
      if(fm.AskType.value=="2")
      {
      	if(fm.CNNo.value=="")
      	{
      	alert("没有咨询通知信息");
      	return false;
      	}
      }	
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.action= "./LLAnswerInfoSave.jsp";
      fm.submit();
}
//添加事件
function EventSave()
{
	if(fm.AskType.value=="0")
	{
	  if ( fm.ConsultNo.value =="")
	  {
		  alert("请先保存咨询信息");
		  return 
	  }
	}
	//if(fm.AskType.value=="1")
	//{
	//	if(fm.NoticeNo.value=="")
	//	{
	//		 alert("请先保存咨询信息");
	//		 return
	//	}
	//}	   
	//if(fm.AskType.value=="2")
	//{
	//	if ( fm.CNNo.value=="")
	//	{
	//		 alert("请先保存咨询通知信息");
	//		 return
	//	}
	//}	
	fm.fmtransact.value="INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action= "./LLSubReportSave.jsp";
    fm.submit();	
}
//保存事件后初始化事件列表
function updateEventGrid(EventNo)
{
	if (fm.fmtransact.value=="INSERT||MAIN")
	{

    	if(fm.AskType.value==0)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate,LLSubReport.AccDesc"
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		var arr=easyExecSql(strsql);
		}
		if(fm.AskType.value==1)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate,LLSubReport.AccDesc"
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		}
		if(fm.AskType.value==2)
		{	
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate,LLSubReport.AccDesc"
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		fm.Answer.value=arr;
		}
	
		turnPage.queryModal(strsql, SubReportGrid);
	}
	if(fm.fmtransact.value=="DELETE||RELA")
	{

		if(fm.AskType.value==0)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject"
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.ConsultNo.value +"')";
		}
		if(fm.AskType.value==1)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject"
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.noticeno='" + fm.NoticeNo.value +"')";
		}
		if(fm.AskType.value==2)
		{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject"
		+" from LLSubReport  where LLSubReport.SubRptNo in ( select LLAskRela.SubRptNo from LLAskRela where LLAskRela.ConsultNo='" + fm.CNNo.value +"')";
		}
		turnPage.queryModal(strsql, SubReportGrid);
	}

}
//选择咨询类型判断显示不显示回复内容
function afterCodeSelect(ObjType,Obj)
{
	if(ObjType=="AskType" && Obj.value!="")
	{
		if(Obj.value==0)
		{
			ConsultInfo2.style.display="";
		}
		if(Obj.value==1)
		{
			ConsultInfo2.style.display="none";
		}
		if(Obj.value==2)
		{
			ConsultInfo2.style.display="";
		}
	}
}

function checkDate(begin,end){
	if(modifydate(begin)<=modifydate(end))
		return true;
	else
		return false;
}

function CaseChange()
{
   initTitle();
   ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}
function ConsultChange()
{
		fm.fmtransact.value="INSERT||MAIN";
	updateEventGrid();
	var strSQL="select llmainask.LogName,llmainask.LogerNo,llmainask.Phone,"
							+"llmainask.Email,llmainask.LogComp,llmainask.AskAddress,llmainask.PostCode,"
							+"(select ldcode.codename from "
												+"ldcode where ldcode.codetype='llaskmode'and ldcode.code=llmainask.AskMode),"
							+"(select ldcode.codename from "
												+"ldcode where ldcode.codetype='llreturnmode'and ldcode.code=llmainask.AnswerMode),"
							+"llmainask.Mobile,llmainask.AskMode,llmainask.AnswerMode"
							+" from llmainask where llmainask.logno=(select LLConsult.LogNo from LLConsult where LLConsult.Consultno='"+fm.ConsultNo.value+"')";
	var arr = easyExecSql(strSQL);
	if (arr)
	{
		fm.LogName.value=arr[0][0];
		fm.LogerNo.value=arr[0][1];
		fm.Phone.value=  arr[0][2];
		fm.Email.value=  arr[0][3];
		fm.LogComp.value=arr[0][4];
		fm.AskAddress.value=arr[0][5];
		fm.PostCode.value=  arr[0][6];
		fm.AskModeName.value=   arr[0][7];
		fm.AnswerModeName.value=   arr[0][8];
		fm.Mobile.value=arr[0][9];
		fm.AskMode.value=arr[0][10];
		fm.AnswerMode.value=arr[0][11];

		}
		var strSQL="select idno from ldperson where customerno='"+fm.LogerNo.value+"'";
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.IDNo.value=arr[0][0];
		}
		var strSQL="select a.CustStatus,(select b.codename from ldcode b where b.codetype='llcuststatus' and b.code=a.CustStatus),a.acccode,case when a.acccode='1' then '意外' else '疾病' end from LLConsult a where a.ConsultNo='"+fm.ConsultNo.value+"'";
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.CustStatus.value=arr[0][0];
			fm.CustStatusName.value=arr[0][1];
			fm.AccType.value=arr[0][2];
			fm.AccTypeName.value=arr[0][3];
		}
		var strSQL="select Answer from LLAnswerInfo where ConsultNo='"+fm.ConsultNo.value+"'";
		//fm.CContent.value=strSQL;
		var arr = easyExecSql(strSQL);
		{
			fm.Answer.value=arr[0][0];
		}
		var strSQL="select CContent from LLConsult where ConsultNo='"+fm.ConsultNo.value+"'";
		var arr = easyExecSql(strSQL);
		{
			fm.CContent.value=arr[0][0];
		}
		QueryCommHop();
}
function initMain()
{
	var a=fm.ConsultNo.value;
	if(a.length>0)
	{
		ConsultChange();
	}
}

//用于校验日期先后
//出险日期为避填
//出险日期>住院日期
//住院日期<出院日期
function testDate()
{

	for(i=0;i<SubReportGrid.mulLineCount;i++)
	{
		var accDate=SubReportGrid.getRowColData(i,2);
		if(accDate=="")
		{
			alert("请输入出险日期！");
			return false;
		}
		var inDate=SubReportGrid.getRowColData(i,4);
		var outDate=SubReportGrid.getRowColData(i,5);
		var accDate = modifydate(accDate);
		if(inDate.length!=0){
			inDate = modifydate(inDate);
			if(dateDiff(accDate,inDate,"D")<0)
			{
				alert("第"+(i+1)+"行，住院日期晚于出险日期！");
				return false;
			}
		}
		if (outDate.length!=0){
			outDate = modifydate(outDate);
			if(dateDiff(inDate,outDate,"D")<0)
			{
				alert("第"+(i+1)+"行，住院日期晚于出院日期！");
				return false;
			}
		}
	}
		return true;
}
function UpdateGrid()
{
	if (fm.CDateE.value.length==0)
		fm.CDateE.value=getCurrentDate();
	if(fm.CDateS.value.length==0)
		fm.CDateS.value=newDate(fm.CDateE.value,-2);
		
	   var dpart=getWherePart("AccDate","CDateS",">=") + getWherePart("AccDate","CDateE","<=");
	var strSql="select SubRptNo,AccDate,AccPlace,InHospitalDate,OutHospitalDate,AccDesc "+
							"from LLSubReport where CustomerNo='"+fm.LogerNo.value+"'";
			strSql+=dpart;
			strSql+=" order by AccDate desc";
//			alert(strSql);
			turnPage.queryModal(strSql, SubReportGrid);
}

function QueryCommHop()
{
	if(fm.ConsultNo.value!="")
	{
	
	var strSQL="select a.HospitalCode,a.HospitalName,b.codename,a.HosAtti,a.Sessionroom,a.Bed from LLCommendHospital a,ldcode b where ConsultNo='"
					+ fm.ConsultNo.value +"' and b.codetype='llhospiflag' and b.code=a.HosAtti";
	//turnPage.queryModal(strSQL, EventGrid);
	arrResult = easyExecSql(strSQL);
	if ( arrResult )
	{
		 displayMultiline(arrResult,CommHospitalGrid);
	
   }
	 
	}
}

//打印接口
function printPage()
{
	if(fm.ConsultNo.value=="")
	{
		alert("请先保存数据");
		return false;
	}
	showInfo = window.open("./CsuRplPrt.jsp?ConsultNo="+fm.ConsultNo.value,"PrintPage",'width=600,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function newDate(oldDate,months)   
{
	if(oldDate==""||months=="")
	{
		return false;
	}
	if (typeof(oldDate) == "string") {
		origDate = getDate(oldDate);
	}
	var oldD=origDate.getDate();
	var oldM=origDate.getMonth();
	var oldY=origDate.getFullYear();
	var tempM=oldM+months;
	var newM=tempM;
	var newY=oldY;
while (tempM>12||tempM<1)
	{
		if (tempM>12)
		{
			newM=tempM-12;
			newY=oldY+1;
		}
		if(tempM<1)
		{
			newM=temM+12;
			newY=oldY-1;
		}
	}
	
	newD=oldD;
	splitOp='-';
	newDate=newY+splitOp+newM+splitOp+newD;
//	alert(newDate);
	return newDate;
}

function submitFormSurvery()
{

	var varSrc = "&CaseNo=" + fm.ConsultNo.value;
	varSrc += "&InsuredNo=" + fm.LogerNo.value;
	varSrc += "&CustomerName=" + fm.LogName.value;
	varSrc += "&StartPhase=2"
	//alert(varSrc);
	//showInfo = window.open("./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
	pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}
