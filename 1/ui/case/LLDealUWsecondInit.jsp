<%
//Name:ReportInit.jsp
//function：
//author:zhangxing
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>


<%
 String tCaseNo = "";
 String tInsuredNo = "";

 tCaseNo = request.getParameter("CaseNo");
 tInsuredNo = request.getParameter("InsuredNo");
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
       
  }
  catch(ex)
  {
    alter("在SecondUWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在SecondUWInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm(tCaseNo,tInsuredNo)
{
  try
  {
    
    initInpBox();
    initContGrid();
    initPolGrid();
    initHide(tCaseNo,tInsuredNo);
    easyQueryClick();
  

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initContGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="赔案号";
    iArray[1][1]="120px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="批次号";
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    

    iArray[3]=new Array();
    iArray[3][0]="合同号码";
    iArray[3][1]="100px";
    iArray[3][2]=120;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="投保人名称";
    iArray[4][1]="100px";
    iArray[4][2]=60;
    iArray[4][3]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="核保结论";
    iArray[5][1]="100px";
    iArray[5][2]=120;
    iArray[5][3]=0;
    
    iArray[6]=new Array();
    iArray[6][0]="核保意见";
    iArray[6][1]="150px";
    iArray[6][2]=120;
    iArray[6][3]=0;      

    ContGrid = new MulLineEnter("fm","ContGrid");
    ContGrid.mulLineCount =2;
    ContGrid.displayTitle = 1;
    ContGrid.locked = 1;
    ContGrid.canSel =1;
    ContGrid.canChk = 0;;
    ContGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ContGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    ContGrid. selBoxEventFuncName = "onSelSelected";
    ContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}


function initPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="赔案号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="批次号";
    iArray[2][1]="90px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    

    iArray[3]=new Array();
    iArray[3][0]="险种名称";
    iArray[3][1]="130px";
    iArray[3][2]=120;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="投保人名称";
    iArray[4][1]="100px";
    iArray[4][2]=60;
    iArray[4][3]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="核保结论";
    iArray[5][1]="100px";
    iArray[5][2]=120;
    iArray[5][3]=0;

    iArray[6]=new Array();
    iArray[6][0]="核保意见";
    iArray[6][1]="150px";
    iArray[6][2]=120;
    iArray[6][3]=0;   


    PolGrid = new MulLineEnter("fm","PolGrid");
    PolGrid.mulLineCount =1;
    PolGrid.displayTitle = 1;
    PolGrid.locked = 1;
    PolGrid.canSel =0;
    PolGrid.canChk = 0;;
    PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    PolGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    PolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initHide(tCaseNo,tInsuredNo)
{
	fm.all('CaseNo').value = tCaseNo;
  fm.all('InsuredNo').value = tInsuredNo;
 
	
}

 </script>