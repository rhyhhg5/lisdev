<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：LLAgentComCardClaimRptCSV.jsp
	//程序功能：CSV报表生成
	//创建日期：2012-09-25
	//创建人  ：zhangjl
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
	System.out.println("start");
	CError cError = new CError();
	boolean operFlag = true;

	//直接获取页面的值
	String Content = "";
	String MngCom = request.getParameter("ManageCom");  //管理机构
	String agentCom = request.getParameter("AgentCom"); //中介机构
	String acType = request.getParameter("ACType"); //中介机构类型
	String agentComState = request.getParameter("AgentComState"); //中介机构状态
	String signDate = request.getParameter("SignDate"); //财务结算起期
	String endSignDate = request.getParameter("EndSignDate"); //财务结算起期
	String StartDate = request.getParameter("StartDate"); //结案起期
	String EndDate = request.getParameter("EndDate"); //结案止期

	TransferData Para = new TransferData();

	Para.setNameAndValue("MngCom", MngCom);
	Para.setNameAndValue("AgentCom", agentCom);
	Para.setNameAndValue("ACType", acType);
	Para.setNameAndValue("AgentComState", agentComState);
	Para.setNameAndValue("SignDate", signDate);
	Para.setNameAndValue("EndSignDate", endSignDate);
	Para.setNameAndValue("StartDate", StartDate);
	Para.setNameAndValue("EndDate", EndDate);

	//  ------------------------------
	RptMetaDataRecorder rpt = new RptMetaDataRecorder(request);
	//   	-------------------------------
	String date = PubFun.getCurrentDate().replaceAll("-", "");
	String time = PubFun.getCurrentTime3().replaceAll(":", "");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String fileNameB = tG.Operator + "_" + FileQueue.getFileName() + ".vts";
	Para.setNameAndValue("tFileNameB", fileNameB);
	System.out.println("=========" + fileNameB);
	String operator = tG.Operator;
	System.out.println("@中介机构卡折业务赔款明细报表(个险中介)CSV@");
	System.out.println("Operator:" + operator);
	System.out.println("ManageCom:" + tG.ManageCom);
	System.out.println("Operator:" + tG.Operator);
	Para.setNameAndValue("tOperator", operator);
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	String CSVFileName = "";

	tVData.addElement(tG);
	tVData.addElement(Para);

	LLAgentComCardClaimReportCSVBL tLLAgentComCardClaimReportCSVBL = new LLAgentComCardClaimReportCSVBL();

	if (!tLLAgentComCardClaimReportCSVBL.submitData(tVData, "PRINT")) {
		operFlag = false;
		Content = tLLAgentComCardClaimReportCSVBL.mErrors.getFirstError().toString();
%>
<%=Content%>
<%
	return;
	} else {
		System.out.println("--------成功----------");
		CSVFileName = tLLAgentComCardClaimReportCSVBL.getFileName() + ".csv";

		if ("".equals(CSVFileName)) {
			operFlag = false;
			Content = "没有得到要显示的数据文件";
%>

<%=Content%>
<%
		return;
		}
	}

	Readhtml rh = new Readhtml();
		String tsql = "Select sysvarvalue from LDSysvar where sysvar = 'PDFstrUrl'";
        ExeSQL mExeSQL = new ExeSQL();
        String realpath = mExeSQL.getOneValue(tsql);
	System.out.println("realpath=="+realpath);
	rpt.updateReportMetaData(StrTool.replace(CSVFileName, ".csv", ".zip"));
	
	String outpathname=realpath+CSVFileName;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	//outpathname = ""; //	本地调试用
	System.out.println("outpathname="+outpathname);
	try {
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	String[] InputEntry = new String[1];
	InputEntry[0] = outpathname;
	System.out.println(InputEntry[0]+"sdfsfsdfsdfsd");
	String tZipFile = StrTool.replace(outpathname, ".csv", ".zip");
	System.out.println("tZipFile == " + tZipFile);
	rh.CreateZipFile(InputEntry, tZipFile);
%>

<html>
<%@page contentType="text/html;charset=GBK"%>
<a
	href="../f1print/download.jsp?filename=<%=StrTool.replace(CSVFileName,".csv",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>

