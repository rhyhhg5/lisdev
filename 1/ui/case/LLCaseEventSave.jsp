<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLCaseEventSave.jsp
//程序功能：
//创建日期：2005-01-12 09:36:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLSubReportSchema tLLSubReportSchema   = new LLSubReportSchema();
  LLCaseRelaSchema  tLLCaseRelaSchema = new LLCaseRelaSchema();
  CaseEventSaveBL tCaseEventSaveBL   = new CaseEventSaveBL();
  
  //输出参数
  CErrors tError = null;            
  String FlagStr = "";
  String Content = "";
  int j=0;
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");

  String Path = application.getRealPath("config//Conversion.config");
  
    tLLSubReportSchema.setSubRptNo("");
    tLLSubReportSchema.setCustomerNo(request.getParameter("InsuredNo"));
    tLLSubReportSchema.setCustomerName(request.getParameter("Name"));   
    tLLSubReportSchema.setCustSituation(request.getParameter("CustSituation1"));
    tLLSubReportSchema.setAccSubject(request.getParameter("AccSubject1"));
    tLLSubReportSchema.setAccidentType(request.getParameter("AccidentType1"));
    tLLSubReportSchema.setAccDate(request.getParameter("AccDate1"));
    tLLSubReportSchema.setAccEndDate(request.getParameter("AccEndDate1"));
    tLLSubReportSchema.setAccDesc(StrTool.Conversion(request.getParameter("AccDesc1"),Path));   
    tLLSubReportSchema.setAccPlace(request.getParameter("AccPlace1"));  
    tLLSubReportSchema.setHospitalCode(request.getParameter("HospitalCode1"));
    tLLSubReportSchema.setHospitalName(request.getParameter("HospitalName1"));
    tLLSubReportSchema.setInHospitalDate(request.getParameter("InHospitalDate1"));
    tLLSubReportSchema.setOutHospitalDate(request.getParameter("OutHospitalDate1")); 
    tLLSubReportSchema.setSeriousGrade(request.getParameter("SeriousGrade1"));
         
    tLLCaseRelaSchema.setCaseNo(request.getParameter("CaseNo"));
      
  try
  {
  	VData tVData = new VData();
	tVData.add(tLLSubReportSchema);
	tVData.add(tLLCaseRelaSchema);
  	tVData.add(tG);
    	tCaseEventSaveBL.submitData(tVData, "EVENTSAVE|CASE");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("**********3" + FlagStr);
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if ("".equals(FlagStr))
  {

    tError = tCaseEventSaveBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("**********3" + FlagStr + "\n" + Content);
  
  //添加各种预处理
%>                      

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
