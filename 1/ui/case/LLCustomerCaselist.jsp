<html>
<%
//Name：LLCustomerCaseList.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@page import="java.util.*"%> 
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
    String AheadDays="-30";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
 %>
 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLCustomerMail.js"></SCRIPT>
   <%@include file="LLCustomerMailInit.jsp"%>
   <script language="javascript">
   function initDate(){
      fm.RgtDateS.value="<%=afterdate%>";
      fm.RgtDateE.value="<%=CurrentDate%>";
      var usercode="<%=Operator%>";
      var comcode="<%=Comcode%>";
      fm.OrganCode.value=comcode;

   }
   </script>
 </head>
<body  onload="initDate();initForm();" >
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         理赔案件信箱
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
       <table  class= common>
	<TR  class= common8>
		<TD  class= title8>
			<input type="radio" name="typeRadio"  value="0"  onclick="easyQuery()">咨询类 
			<input type="radio" name="typeRadio"   value="1"  onclick="easyQuery()">通知类  
			<input type="radio" name="typeRadio"  checked value="3" onclick="easyQuery()">申请类
			<input type="radio" name="typeRadio" value="4" onclick="easyQuery()">申诉类
			<input type="radio" name="typeRadio" value="5" onclick="easyQuery()">错误处理类	
		 </td>
	 </tr>
	 <TR  class= common8>
		 <td>			 
			 <input type="radio" value="01" checked name="RgtState" onclick="easyQuery()">未结案</input>
			 <input type="radio" value="02" name="RgtState" onclick="easyQuery()">结案</input>
			 <input type="radio" value="03" name="RgtState" onclick="easyQuery()">通知</input>
			 <input type="radio" value="04" name="RgtState" onclick="easyQuery()">所有案件</input>
		 </TD>                                             
	</TR>
       </table>
     </DIV>

     <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput2);">
         </TD>
         <TD class= titleImg>
         客户信箱
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
	<TR  class= common8>
	<TD  class= title>理赔号</TD><TD  class= input> 
		<Input class=common8 name=CaseNo onkeydown="QueryOnKeyDown()"></TD>
		
	<TD  class= title>客户姓名</TD><TD  class= input> 
	<Input class=common8 name=CustomerName onkeydown="QueryOnKeyDown()" ></TD>	
	<TD  class= title>客户号</TD><TD  class= input> 
	<Input class=common8 name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
	<TR  class= common8>
	</tr>
	<TD  class= title>团体批次号</TD><TD  class= input> 
				<Input class=common8 name=RgtNo onkeydown="QueryOnKeyDown()"></TD>
				<Input type=hidden name=OrganCode ondblClick="showCodeList('station',[this],[0]);" onkeyup="showCodeListKey('station',[this],[0]);" onkeydown="QueryOnKeyDown()" ></TD>
				<TD  class= title8>受理日期从</TD>
		<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title8>到</TD>
		<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
	</tr>
       </table>
     </Div>

    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
		<br>      
			<Div id= "divPage" align=center style= "display: 'none' ">
			    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
			</Div>
		</Div>
    <br>
    <hr>
		<input type=button class=cssButton style="align:right" value="原始信息查询" OnClick="ShowInfoPage();">
		<input type=button class=cssButton value="保单查询" OnClick="ContInfoPage();">  
    
 <Div style= "display: 'none' ">   
 <hr/>       
			<input type="radio" name="typeRadio" value="2">咨询通知类 
 <input  class=cssButton type=button value="咨询通知" onclick="DealConsult()">
<input  class=cssButton type=button value="受理申请" onclick="DealApp()">
<input  class=cssButton type=button value="受理申请(团体)" onclick="DealAppGrp()">
<input  class=cssButton type=button value="账单录入" onclick="DealFeeInput()">
<input  class=cssButton type=button value="检录" onclick="DealCheck()">
<input  class=cssButton type=button value="理算" onclick="DealClaim()">
<input  class=cssButton type=button value="审批审定" onclick="DealApprove()">
 <hr/>
<input  class=cssButton type=button value="给付通知书打印" onclick="ClaimGetPrint()">
<input  class=cssButton type=button value="给付明细表打印" onclick="ClaimDetailPrint()">
<input  class=cssButton type=button value="给付凭证打印" onclick="GPrint()">
</Div>
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="ComCode" >

  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
