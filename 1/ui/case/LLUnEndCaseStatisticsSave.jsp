<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK"%>

<%
	//程序名称：LLUnEndCaseStatisticsSave.jsp
	//程序功能：未决案件统计表
	//创建日期：2010-6-22
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>

<%
	//接收信息，并作校验处理。
	boolean operFlag = true;
	LLUnEndCaseStatisticsUI tLLUnEndCaseStatisticsUI = new LLUnEndCaseStatisticsUI();

	//输出参数
	CErrors mErrors = null;
	CErrors tError = new CErrors();
	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String CSVFileName = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String operator = tG.Operator;

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");

	String tManageCom = request.getParameter("ManageCom");
	System.out.println(tManageCom);
	String tRgtDateS = request.getParameter("RgtDateS");
	tRgtDateS = AgentPubFun.formatDate(tRgtDateS, "yyyy-MM-dd");
	System.out.println(tRgtDateS);
	String tRgtDateE = request.getParameter("RgtDateE");
	tRgtDateE = AgentPubFun.formatDate(tRgtDateE, "yyyy-MM-dd");
	System.out.println(tRgtDateE);
	String tRiskProp = request.getParameter("RiskProp");
	System.out.println(tRiskProp);
	String tSaleChnl = request.getParameter("SaleChnl");
	System.out.println(tSaleChnl);
	String tEndDate = request.getParameter("EndDate");
	tEndDate = AgentPubFun.formatDate(tEndDate, "yyyy-MM-dd");
	System.out.println(tEndDate);
	String tType = request.getParameter("Type");
	System.out.println(tType);
	String StatsType = request.getParameter("StatsType");//统计类型
	System.out.println("StatsType:"+StatsType);

	//	********************************************************
	RptMetaDataRecorder rpt = new RptMetaDataRecorder(request);
	//	********************************************************

	String date = PubFun.getCurrentDate().replaceAll("-", "");
	String time = PubFun.getCurrentTime3().replaceAll(":", "");
	String fileNameB = tG.Operator + "_" + FileQueue.getFileName()
			+ ".vts";
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("FileNameB", fileNameB);
	System.out.println("@未决案件统计表@");
	System.out.println("Operator:" + operator);
	System.out.println("ManageCom:" + tG.ManageCom);
	System.out.println("Operator:" + tG.Operator);
	VData tVData = new VData();
	VData mResult = new VData();
	tVData.addElement(tManageCom);
	tVData.addElement(tRgtDateS);
	tVData.addElement(tRgtDateE);
	tVData.addElement(tRiskProp);
	tVData.addElement(tSaleChnl);
	tVData.addElement(tEndDate);
	tVData.addElement(tType);
	tVData.addElement(StatsType);
	tVData.addElement(tTransferData);
	tVData.addElement(tG);

	XmlExport txmlExport = new XmlExport();
	if (!tLLUnEndCaseStatisticsUI.submitData(tVData, "PRINT")) {
		operFlag = false;
		Content = tLLUnEndCaseStatisticsUI.mErrors.getFirstError()
				.toString();
%>
<%=Content%>
<%
	return;
	} else {
		System.out.println("--------成功----------");
		CSVFileName = tLLUnEndCaseStatisticsUI.getFileName() + ".csv";

		if ("".equals(CSVFileName)) {
			operFlag = false;
			Content = "没有得到要显示的数据文件";
%>
<%=Content%>
<%
	return;
		}
	}

	Readhtml rh = new Readhtml();

	String realpath = application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
	String temp = realpath.substring(realpath.length() - 1, realpath
			.length());
	if (!temp.equals("/")) {
		realpath = realpath + "/";
	}
	System.out.println(realpath);
	rpt.updateReportMetaData(StrTool.replace(CSVFileName, ".csv", ".zip"));
	String outpathname=realpath+"vtsfile/"+CSVFileName;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	try {
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	String[] InputEntry = new String[1];
	InputEntry[0] = outpathname;
	System.out.println(InputEntry[0]);
	String tZipFile = StrTool.replace(outpathname, ".csv", ".zip");
	System.out.println("tZipFile == " + tZipFile);
	rh.CreateZipFile(InputEntry, tZipFile);
%>

<html>
<%@page contentType="text/html;charset=GBK"%>
<a
	href="../f1print/download.jsp?filename=<%=StrTool.replace(CSVFileName,".csv",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>