<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskInputSava.jsp
//程序功能：工单管理工单录入数据保存页面
//创建日期：2005-01-17 14:27:43
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String FlagStr;
	String Content ="";
	String tWorkNo = "";
	String tAccpetNo = "";
	String tDetailWorkNo = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	//输入参数
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	tLGWorkSchema.setCustomerNo(request.getParameter("AppntNo"));
	tLGWorkSchema.setStatusNo("3");
	tLGWorkSchema.setTypeNo("03");
	tLGWorkSchema.setApplyTypeNo("3");
	tLGWorkSchema.setRemark(request.getParameter("理赔合同处理"));
	
	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
	
	String tRadio[] = request.getParameterValues("InpContGridSel");
	String tContNo[] = request.getParameterValues("ContGrid1");  
  for (int index=0; index< tRadio.length;index++){
    if(tRadio[index].equals("1")){
      tLLCaseSchema.setGrpNo(tContNo[index]);    
    }
  }

	VData tVData = new VData();
	tVData.add(tLGWorkSchema);
	tVData.add(tLLCaseSchema);
	tVData.add(tGI);
	
	LLContDealBL tLLContDealBL = new LLContDealBL();
	if (tLLContDealBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
		Content = "数据保存失败！";
	}
	else
	{
		//设置显示信息
		VData tRet = tLLContDealBL.getResult();
		LGWorkSchema mLGWorkSchema = new LGWorkSchema();
		mLGWorkSchema.setSchema((LGWorkSchema) tRet.getObjectByObjectName("LGWorkSchema", 0));
		TransferData mTransferData = new TransferData();
		mTransferData = (TransferData) tRet.getObjectByObjectName("TransferData", 0);
		if (mTransferData != null) {
			  String tIndiInfo = (String)mTransferData.getValueByName("IndiInfo");
			  System.out.println(tIndiInfo);
			  System.out.println(Content);
			  Content = Content+tIndiInfo;
			  System.out.println(Content);
		}
		 
		tWorkNo = mLGWorkSchema.getWorkNo();
		tAccpetNo = mLGWorkSchema.getAcceptNo();
		tDetailWorkNo = mLGWorkSchema.getDetailWorkNo();
		
		//把WorkNo保存到session中
		session.setAttribute("WORKNOINPUT", tWorkNo);				
		 
		FlagStr = "Succ";
		Content += "数据保存成功，受理号为：" + tAccpetNo;
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit3("<%=FlagStr%>","<%=Content%>","<%=tAccpetNo%>");
</script>
</html>

