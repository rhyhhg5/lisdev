<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 String loadflag =request.getParameter("loadflag");
 if ( loadflag==null ) loadflag="";
 String logNo = request.getParameter("logNo");
 if ( logNo==null ) logNo="";
 String CustomerNo = request.getParameter("CustomerNo");
 if (CustomerNo==null) CustomerNo="";
 String ConsultNo = request.getParameter("ConsultNo");
 if (ConsultNo==null) ConsultNo="";
 String AskType = request.getParameter("AskType");
 if (AskType==null) AskType="";
  String CustomerName = request.getParameter("CustomerName");
 if (CustomerName==null) CustomerName="";
 
%>
<script language="JavaScript">
var loadflag="<%=loadflag%>";
function initInpBox()
{
  try
  {
     fm.LogNo.value="<%=logNo%>";
     fm.ConsultNo.value="<%=ConsultNo%>";
     fm.CustomerNo.value="<%=CustomerNo%>";
     fm.AskType.value ="<%=AskType%>";
     fm.CustomerName.value="<%=CustomerName%>";
     
     var arrResult = new Array();
     var strSQL="select Answer from LLAnswerInfo where ConsultNo='"+<%=ConsultNo%>+"'";
     arrResult= easyExecSql(strSQL);
     if(arrResult != null)
     {
     	fm.Answer.value=arrResult[0][0];
	 }
	 strSQL="select ExpertNo,AvaiFlag from LLConsult where ConsultNo='"+<%=ConsultNo%>+"'";
	 arrResult= easyExecSql(strSQL);
	 if(arrResult != null)
	 {
	 	
	 	fm.ExpertNo.value=arrResult[0][0];
	 	fm.AvaiFlag.value=arrResult[0][1];
	 }

  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    //initCustomerGrid();
    initSubReportGrid();
    initCustomer();
    
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initSubReportGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="事件号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="发生日期";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="发生地点";
    iArray[3][1]="150px";
    iArray[3][2]=60;
    iArray[3][3]=0;
  



   iArray[4] = new Array("事故类型","80px","0","0");
   iArray[5] = new Array("事故主题","200px","0","0");
   
    SubReportGrid = new MulLineEnter("fm","SubReportGrid");
    SubReportGrid.mulLineCount = 0;
    SubReportGrid.displayTitle = 1;
    SubReportGrid.locked = 1;
    SubReportGrid.canSel =1	;
    SubReportGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.loadMulLine(iArray);
    SubReportGrid.selBoxEventFuncName = "ShowRela";
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initCustomerGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="客户名称";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="客户类型";
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="疾病代码";
    iArray[3][1]="100px";
    iArray[3][2]=60;
    iArray[3][3]=0;
    
    iArray[4]=new Array()
    iArray[4][0]="是否咨询专家";
    iArray[4][1]="80px";
    iArray[4][2]=100;
    iArray[4][3]=0;

    iArray[5]=new Array()
    iArray[5][0]="专家名称";
    iArray[5][1]="150px";
    iArray[5][2]=100;
    iArray[5][3]=0;

    iArray[6]=new Array();
    iArray[6][0]="期望回复时间";
    iArray[6][1]="100px";
    iArray[6][2]=100;
    iArray[6][3]=0;

    iArray[7]=new Array();
    iArray[7][0]="咨询主题";
    iArray[7][1]="250px";
    iArray[7][2]=100;
    iArray[7][3]=0;

    iArray[8]=new Array();
    iArray[8][0]="个人咨询号";
    iArray[8][1]="0px";
    iArray[8][2]=100;
    iArray[8][3]=3;

   iArray[9]=new Array("个人客户号","0px","0",3);
   iArray[10]=new Array("咨询级别","0px","0",3);
   iArray[11]=new Array("咨询内容","0px","0",3);
   iArray[12]=new Array("客户现状","0px","0",3);

    CustomerGrid = new MulLineEnter("fm","CustomerGrid");
    CustomerGrid.mulLineCount = 0;
    CustomerGrid.displayTitle = 1;
    CustomerGrid.locked = 1;
    CustomerGrid.canSel =1;
    CustomerGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CustomerGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    CustomerGrid. selBoxEventFuncName = "onSelSelected";
    CustomerGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
 </script>