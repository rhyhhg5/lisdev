<html>
<%
//Name：LLCustomerMessage.jsp
//Function：短信通知页面
//Date：2007-09-19 16:49:22
//Author：MN
%>
 <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@page import = "com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
  <%@page import = "com.sinosoft.lis.vschema.*"%>
  <%@page import = "com.sinosoft.lis.llcase.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%
    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
    String Operator=tG.Operator;
    String Comcode=tG.ManageCom;
    String CurrentDate= PubFun.getCurrentDate();
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);
    String AheadDays="-1";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
  %>
  
  <head>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLCustomerMessage.js"></SCRIPT>
    <script language="javascript">
      function initDate(){
        fm.RgtDateS.value="<%=afterdate%>";
        fm.RgtDateE.value="<%=afterdate%>";
        fm.ManageCom.value="<%=Comcode%>";
        //优化，自动带出机构名称
			var tSql="select name from ldcom where comcode='"+fm.ManageCom.value+"'";
			var tResult = easyExecSql(tSql);
			if(tResult != null){
				fm.ComName.value= tResult[0][0];
			}
      }
    </script>
  </head>
	<body onload="initDate();">  
  <form action="./LLCustomerMessageSave.jsp" method=post name=fm target="f1print">
    <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
          </TD>
          <TD class= titleImg>
            短信通知
          </TD>
        </TR>
      </table>
          <Div  id= "divLLLLMainAskInput1" style= "display: ''">
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title8>统计日期从</TD>
          <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
          <TD  class= title8>到</TD>
          <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>        
        </TR>
        <TR  class= common>
          <TD  class= title>短信通知</TD>
		      <TD  class= input> <input class=codeno CodeData="0|3^1|个险待处理^2|个险给付^3|团险给付 "  name=DealWith1 ondblclick="return showCodeListEx('DealWith1',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith1',[this,DealWithName],[0,1]);"><input class=codename name=DealWithName></TD>
			  </tr>
		</table>
		</DIV>
		<Input type="hidden" class= common name="RgtState" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 