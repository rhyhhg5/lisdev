<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：LLTreatmentInfoSave.jsp
	//程序功能：诊疗项目信息
	//创建日期：2015-02-10
	//创建人  ：Liyunxia
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
//输入参数

LLCaseTreatmentInfoSchema tLLCaseTreatmentInfoSchema = new LLCaseTreatmentInfoSchema();
LLCaseTreatmentInfoUI tLLCaseTreatmentInfoUI = new LLCaseTreatmentInfoUI();
//输出参数
String FlagStr = "";
String Content = "";
GlobalInput tGI = new GlobalInput(); //repair:
tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
//后面要执行的动作：添加，修改，删除
String transact=request.getParameter("Transact");

if(tGI==null)
{
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
}
else
{
	String Operator  = tGI.Operator ;  //保存登陆管理员账号
	String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
	CErrors tError = null;
    String tBmCert = "";
    
    System.out.println("transact:"+transact);
    if("INSERT".equals(transact))
    {
    	tLLCaseTreatmentInfoSchema.setSerialNo(PubFun1.CreateMaxNo("TREMENT", 20));
    	tLLCaseTreatmentInfoSchema.setTreatmentCode(request.getParameter("TreatmentCode"));
    	tLLCaseTreatmentInfoSchema.setTreatmentName(request.getParameter("TreatmentName"));
    	tLLCaseTreatmentInfoSchema.setTreatmentDetail(request.getParameter("TreatmentDetail"));
    	tLLCaseTreatmentInfoSchema.setTreatmentExplain(request.getParameter("TreatmentExplain"));
    	tLLCaseTreatmentInfoSchema.setTreatmentCategory(request.getParameter("TreatmentCategory"));
    	tLLCaseTreatmentInfoSchema.setPhonetic(request.getParameter("Phonetic"));
    	tLLCaseTreatmentInfoSchema.setPackagingCode(request.getParameter("PackagingCode"));
    	tLLCaseTreatmentInfoSchema.setComDescription(request.getParameter("ComDescription"));
    	tLLCaseTreatmentInfoSchema.setChargeUnit(request.getParameter("ChargeUnit"));
    	tLLCaseTreatmentInfoSchema.setUnitPrice(request.getParameter("UnitPrice"));
    	tLLCaseTreatmentInfoSchema.setRemark(request.getParameter("Remark"));
    	tLLCaseTreatmentInfoSchema.setMngCom(request.getParameter("MngCom"));
    	tLLCaseTreatmentInfoSchema.setAreaCode(request.getParameter("AreaCode"));
    	tLLCaseTreatmentInfoSchema.setOperator(Operator);
    	tLLCaseTreatmentInfoSchema.setMakeDate(PubFun.getCurrentDate());
    	tLLCaseTreatmentInfoSchema.setMakeTime(PubFun.getCurrentTime());
    	tLLCaseTreatmentInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLLCaseTreatmentInfoSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else if("DELETE".equals(transact))
    {
    	String tSerialNo =request.getParameter("SerialNo");
    	tLLCaseTreatmentInfoSchema.setSerialNo(tSerialNo);
    }
    else if("UPDATE".equals(transact))
    {
    	String tSerialNo =request.getParameter("SerialNo");
    	
    	LLCaseTreatmentInfoDB tLLCaseTreatmentInfoDB = new LLCaseTreatmentInfoDB();
    	tLLCaseTreatmentInfoDB.setSerialNo(tSerialNo);
    	tLLCaseTreatmentInfoDB.getInfo();
    	LLCaseTreatmentInfoSchema cLLCaseTreatmentInfoSchema = tLLCaseTreatmentInfoDB.getSchema();
        
    	tLLCaseTreatmentInfoSchema.setSerialNo(tSerialNo);
    	tLLCaseTreatmentInfoSchema.setTreatmentCode(request.getParameter("TreatmentCode"));
    	tLLCaseTreatmentInfoSchema.setTreatmentName(request.getParameter("TreatmentName"));
    	tLLCaseTreatmentInfoSchema.setTreatmentDetail(request.getParameter("TreatmentDetail"));
    	tLLCaseTreatmentInfoSchema.setTreatmentExplain(request.getParameter("TreatmentExplain"));
    	tLLCaseTreatmentInfoSchema.setTreatmentCategory(request.getParameter("TreatmentCategory"));
    	tLLCaseTreatmentInfoSchema.setPhonetic(request.getParameter("Phonetic"));
    	tLLCaseTreatmentInfoSchema.setPackagingCode(request.getParameter("PackagingCode"));
    	tLLCaseTreatmentInfoSchema.setComDescription(request.getParameter("ComDescription"));
    	tLLCaseTreatmentInfoSchema.setChargeUnit(request.getParameter("ChargeUnit"));
    	tLLCaseTreatmentInfoSchema.setUnitPrice(request.getParameter("UnitPrice"));
    	tLLCaseTreatmentInfoSchema.setRemark(request.getParameter("Remark"));
    	tLLCaseTreatmentInfoSchema.setMngCom(request.getParameter("MngCom"));
    	tLLCaseTreatmentInfoSchema.setAreaCode(request.getParameter("AreaCode"));
    	tLLCaseTreatmentInfoSchema.setOperator(Operator);
    	tLLCaseTreatmentInfoSchema.setMakeDate(cLLCaseTreatmentInfoSchema.getMakeDate());
    	tLLCaseTreatmentInfoSchema.setMakeTime(cLLCaseTreatmentInfoSchema.getMakeTime());
    	tLLCaseTreatmentInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLLCaseTreatmentInfoSchema.setModifyTime(PubFun.getCurrentTime());
    }
    
	try{
	  // 准备传输数据 VData
	   VData tVData = new VData();
	   tVData.add(tLLCaseTreatmentInfoSchema);
	   tLLCaseTreatmentInfoUI.submitData(tVData,transact);
  	}
 	 catch(Exception ex)
  	{
	    Content = "保存失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
  	}
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
      tError = tLLCaseTreatmentInfoUI.mErrors;
      if (!tError.needDealError())
      {     
        if("INSERT".equals(transact))
  	    {
        	Content ="保存成功！";
	  	    FlagStr = "Success";
  	    }
  	    else if("DELETE".equals(transact))
  	    {
  	    	Content ="删除成功！";
	  	    FlagStr = "Success";
  	    }
  	    else if("UPDATE".equals(transact))
  	    {
  	    	Content ="修改成功！";
	  	    FlagStr = "Success";
  	    }
      }
      else                                                                           
     {
    	    if("INSERT".equals(transact))
	  	    {
    	    	Content = "保存失败，原因是:" + tError.getFirstError();
   	  	     	FlagStr = "Fail";
	  	    }
	  	    else if("DELETE".equals(transact))
	  	    {
	  	    	Content = "删除失败，原因是:" + tError.getFirstError();
		  	    FlagStr = "Fail";
	  	    }
	  	    else if("UPDATE".equals(transact))
	  	    {
	  	    	Content = "修改失败，原因是:" + tError.getFirstError();
		  	    FlagStr = "Fail";
	  	    }
     }
  }
}
  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>