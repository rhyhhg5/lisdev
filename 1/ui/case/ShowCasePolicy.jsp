<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
 * Name     ：ShowCasePolicy.jsp
 * Function ：显示分案保单明细的信息
 * Author   :LiuYansong
 * Date     :2003-8-6
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%
  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";

  String RgtNo = request.getParameter("RgtNo");
  String CaseNo = request.getParameter("CaseNo");

  VData tVData = new VData();
  tVData.addElement(RgtNo);
  tVData.addElement(CaseNo);
  CasePolicyUI mCasePolicyUI = new CasePolicyUI();
  if (!mCasePolicyUI.submitData(tVData,"INIT"))
  {
    Content = " 查询失败，原因是: " + mCasePolicyUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }
  else
  {
    tVData.clear();
    tVData = mCasePolicyUI.getResult();
    LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
    mLLCasePolicySet.set((LLCasePolicySet)tVData.getObjectByObjectName("LLCasePolicySet",0));
    int n = mLLCasePolicySet.size();
    if(n>0)
    {
      System.out.println("查询的记录是 "+n);
      String Strtest = "0|" + n + "^" + mLLCasePolicySet.encode();
      System.out.println("QueryResult: " + Strtest);
      %>
      <script language="javascript">
      try
      {
        parent.fraInterface.displayQueryResult1('<%=Strtest%>');
      }
      catch(ex) {}
      </script>
      <%
    }
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
//  if (FlagStr == "Fail")
//  {
//    tError = mShowPolInfoUI.mErrors;
//    if (!tError.needDealError())
//    {
//      Content = " 初始化成功！！！ ";
//      FlagStr = "Succ";
//    }
//    else
//    {
//      Content = " 初始化失败，原因是:" + tError.getFirstError()+"！！！！";
//      FlagStr = "Fail";
//    }
//  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
//  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>