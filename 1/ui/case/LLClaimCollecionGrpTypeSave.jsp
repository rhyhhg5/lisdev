<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLClaimCollecionGrpTypeSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LDCode1Set tLDCode1Set   = new LDCode1Set();
OLDBusinessCode1SetUI tOLDBusinessCode1SetUI   = new OLDBusinessCode1SetUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  TransferData td=new TransferData();
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tRadio[] = request.getParameterValues("InpBusinessGrpContGridSel");
  String tCode[] =  request.getParameterValues("BusinessGrpContGrid2");
  String tCode1[] = request.getParameterValues("BusinessGrpContGrid3");
  String tCodeName[] = request.getParameterValues("BusinessGrpContGrid4");
  td.setNameAndValue("zdywlx",request.getParameter("zdywlx"));//add by zzh 101112
  td.setNameAndValue("grpcontno",request.getParameter("grpcontno"));//add by zzh 101112
  for (int index=0;index<tRadio.length;index++) {
    if ("1".equals(tRadio[index])) {
        LDCode1Schema tLDCode1Schema = new LDCode1Schema();
        tLDCode1Schema.setCodeType("llgrptype");
        tLDCode1Schema.setCode(tCode[index]);
        tLDCode1Schema.setCode1(tCode1[index]);
        tLDCode1Schema.setCodeName(tCodeName[index]);
        tLDCode1Set.add(tLDCode1Schema);
    }
  }
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLDCode1Set);
  	tVData.add(td);//add by zzh 101112
  	tOLDBusinessCode1SetUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDBusinessCode1SetUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
