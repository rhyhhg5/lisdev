<%
//Name：LLClaimCollectionInit.jsp
//Function：承保赔付汇总表
//Date：2009-07-29
//Author  ：maning
%>

<script language="JavaScript">
  function initInpBox( )
  {
    try {

    }
    catch(ex) {
      alert("在LLClaimCollectionInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在LLClaimCollectionInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initForm(){
    try{
      
      initInpBox();
      initBusinessGrid();
      initBusinessGrpContGrid();
     }
    catch(re){
      alert("LLClaimCollectionInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
    }
  }

  // 保单信息列表的初始化
  function initBusinessGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("组编码","40px","100","1");
      iArray[2]=new Array("组名称","60px","100","1");
      
      BusinessGrid = new MulLineEnter("fm","BusinessGrid");
      BusinessGrid.mulLineCount = 0;
      BusinessGrid.displayTitle = 1;
      BusinessGrid.locked = 0;
      BusinessGrid.canChk = 0;
      BusinessGrid.canSel = 1;
      BusinessGrid.hiddenPlus=0;
      BusinessGrid.hiddenSubtraction=0;
      BusinessGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }
  
  function initBusinessGrpContGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("组","80px","100","2");
      iArray[1][4] = "llusergroup";
      iArray[1][5]="2|1";
      iArray[1][6]="0|1";
      
      iArray[2]=new Array("组编码","80px","100","3");
      iArray[3]=new Array("用户编码","80px","100","2");
      iArray[3][4] = "llusercode";
      iArray[3][5]="3|4";
      iArray[3][6]="0|1";
      iArray[3][15]="stateflag";
      iArray[3][16]="#1#";
      iArray[3][19]="1";
      iArray[4]=new Array("用户名称","80px","100","0");
      
      BusinessGrpContGrid = new MulLineEnter("fm","BusinessGrpContGrid");
      BusinessGrpContGrid.mulLineCount = 0;
      BusinessGrpContGrid.displayTitle = 1;
      BusinessGrpContGrid.locked = 0;
      BusinessGrpContGrid.canChk = 0;
      BusinessGrpContGrid.canSel = 1;
      BusinessGrpContGrid.hiddenPlus=0;
      BusinessGrpContGrid.hiddenSubtraction=0;
      BusinessGrpContGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }

</script>