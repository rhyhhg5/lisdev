<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorUWManu.jsp
//程序功能：保全人工核保
//创建日期：2005-08-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LPPEdorUWManu.js"></SCRIPT>
  <%@include file="./LPPEdorUWManuInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>人工核保</title>
</head>
<body onload="initForm();">
<form name=fm action="./LPPEdorUWManuSave.jsp"" target=fraSubmit method=post>
<br>
	<div id= "divButton" style="display: ''" >
    <input type="button" class=cssButton  value="案件扫描影印件" onclick="showEdorScan();">
	<input type="button" class=cssButton  value="既往核保信息" onclick="showUWInfo();">
	<input type="button" class=cssButton  value="调查报告" onclick="showReportInfo();">
  </div>
<br>
	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWErrList);"> 
      </td>
      <td class= titleImg>自核信息 </td>
    </tr>
  </table>
	<Div  id= "divUWErrList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanUWErrGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage" align=center style= "display: 'none' ">
			<INPUT value="首  页" class=cssButton type=button onclick="turnPage.firstPage(); showCodeName();"> 
			<INPUT value="上一页" class=cssButton type=button onclick="turnPage.previousPage(); showCodeName();"> 					
			<INPUT value="下一页" class=cssButton type=button onclick="turnPage.nextPage(); showCodeName();"> 
			<INPUT value="尾  页" class=cssButton type=button onclick="turnPage.lastPage(); showCodeName();"> 
		</Div>
	</Div>
	<hr>
	<div>
		<input value="体检录入" class=cssButton type=button onclick="healthInput();" >
		<input value="体检回销" class=cssButton type=button onclick="healthBack();" > 
		<INPUT value="契调录入" class=cssButton type=button onclick="reportInput();">
		<input value="契调回销" class=cssButton type=button onclick="reportBack();">
		<INPUT value="问题件录入" class=cssButton type=button onclick="questionInput();">
		<input value="问题件回销" class=cssButton type=button onclick="questionBack();">
	</div>
	<!-- 理赔查询信息 -->
       <%@include file="LPUW.jsp"%>
	<hr>
	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolList);"> 
      </td>
      <td class= titleImg>项目信息 </td>
      <td>
      	&nbsp;&nbsp;<!--input value="健康告知" class=cssButton type=button onclick="healthImpart();"-->
      </td>
    </tr>
  </table>
	<Div  id= "divPolList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanPolGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage2" align=center style= "display: 'none' ">
			<INPUT value="首  页" class=cssButton type=button onclick="turnPage2.firstPage(); showCodeName();"> 
			<INPUT value="上一页" class=cssButton type=button onclick="turnPage2.previousPage(); showCodeName();"> 					
			<INPUT value="下一页" class=cssButton type=button onclick="turnPage2.nextPage(); showCodeName();"> 
			<INPUT value="尾  页" class=cssButton type=button onclick="turnPage2.lastPage(); showCodeName();"> 
		</Div>
	 </Div>
	 	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseList);"> 
      </td>
      <td class= titleImg>理赔信息 </td>
      <td>
      	&nbsp;&nbsp;<!--input value="健康告知" class=cssButton type=button onclick="healthImpart();"-->
      </td>
    </tr>
  </table>
	<Div  id= "divCaseList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanCaseGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage6" align=center style= "display: 'none' ">
			<INPUT value="首  页" class=cssButton type=button onclick="turnPage6.firstPage(); showCodeName();"> 
			<INPUT value="上一页" class=cssButton type=button onclick="turnPage6.previousPage(); showCodeName();"> 					
			<INPUT value="下一页" class=cssButton type=button onclick="turnPage6.nextPage(); showCodeName();"> 
			<INPUT value="尾  页" class=cssButton type=button onclick="turnPage6.lastPage(); showCodeName();"> 
		</Div>
	 </Div>	 
  	<table>
		  <tr>
	      <td>
	        <IMG id="impartImg"  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this, divImpart);">
	      </td>
	      <td class= titleImg>
	        健康告知
	      </td>
		  </tr>
		</table>
		<div id= "divImpart" style= "display: 'none'">
      <table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
        <tr>
					<td colspan="2" class="mulinetitle2" height="20">问卷</td>
				</tr>
  			<tr height="25"> 
			    <td class="mulinetitle3">
			    	<input type="hidden" name="ImpartCode" value="001">
			    	<input type="hidden" name="ImpartParamName1" value="PayMode">
			    	&nbsp;1. 目前您医疗费用支付方式：
			    	<input type="radio" name="Detail1" value="0">社会医疗保险
			    	<input type="radio" name="Detail1" value="1">商业医疗保险
			    	<input type="radio" name="Detail1" value="2">自费
			    	<input type="radio" name="Detail1" value="3">其他（请详细说明）
			    </td>
			    <td class="mulinetitle3" width="300">
			    	详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="002">
			  		<input type="hidden" name="ImpartParamName2" value="Income">
			  		<input type="hidden" name="ImpartParamName2" value="Source">
			  		&nbsp;2.您目前收入<input name="Detail2" class = "common66">万元，主要来源为：
			  		<input type="radio" name="Detail2" value="0">薪资
			  		<input type="radio" name="Detail2" value="1">营业收入
			  		<input type="radio" name="Detail2" value="2">房屋出租
			  		<input type="radio" name="Detail2" value="3">证券投资
			  		<input type="radio" name="Detail2" value="4">银行利息
			  		<input type="radio" name="Detail2" value="5">其他（请详细说明）
			  	</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="003">
						&nbsp;3.（<input type="radio" name="Detail3" value="0">是<input type="radio" name="Detail3" value="1">否）  您曾否投保过医疗险、重大疾病险、意外险或寿险？ 如“是”，请就投保公司、险种、保额作详细说明。
			  	</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="004">
						&nbsp;4.（<input type="radio" name="Detail4" value="0">是<input type="radio" name="Detail4" value="1">否）  您是否有投保本保单前未告知的疾病、残疾及器官缺失或功能不全？如有，请详细说明。
					</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="005">
						&nbsp;5.（<input type="radio" name="Detail5" value="0">是<input type="radio" name="Detail5" value="1">否）  自投保至今，您是否有身体不适、检查结果异常、就诊或被诊断为某种疾病？如有，清详细说明。
					</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="006">
						&nbsp;6.（<input type="radio" name="Detail6" value="0">是<input type="radio" name="Detail6" value="1">否）  自投保至今您是否发生过保险赔付（包括其他公司），或正准备申请赔付？如有，请详细说明。
					</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			</table>
	  </div>
	   <div>
	  <table>
	        <tr>
	  	         <td class=input>
	   		 	   	<input value="待回复" class=cssButton type=button onclick="dealUWState(2);">
	   		 	   	<input value="待处理" class=cssButton type=button onclick="dealUWState(1);">
	   		 	 </td>
	   	   </tr>
	  </table>
	  </div>
<div id="divUW" style="display: '<%if (hasUW == true) out.print("none");%>'">
	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWResult);"> 
      </td>
      <td class= titleImg>核保信息 </td>
    </tr>
  </table>
  <Div  id= "divUWResult" style= "display: ''" align=center>  
  <table class= common>
    <TR class= common> 
      <TD class= title> 核保结论 </TD>
      <TD class= input>
        <Input class="codeNo" name="PassFlag1" CodeData="0|^1|标准承保^2|变更承保^3|拒保" verify="核保结论|notnull" ondblclick="return showCodeListEx('PassFlag1',[this,PassFlagName],[0,1]);" onkeyup="return showCodeListEx('PassFlag1',[this,PassFlagName],[0,1]);"><Input class="codeName" name="PassFlagName"  elementtype="nacessary" readonly>
      </TD> 
	    <TD class= title id="tdDisagreeDeal1" style="display:'none'"> 客户不同意处理 </TD>
	    <TD class= input colspan="3" id="tdDisagreeDeal2" style="display:'none'">
	      <Input class="codeNo" name="DisagreeDeal" CodeData="0|^1|终止申请^2|险种解约^3|终止续保" verify="客户不同意处理|notnull&code:DisagreeDeal" ondblclick="return showCodeListEx('DisagreeDeal',[this,DisagreeDealName],[0,1]);" onkeyup="return showCodeListEx('DisagreeDeal',[this,DisagreeDealName],[0,1]);"><Input class="codeName" name="DisagreeDealName"  elementtype="nacessary" readonly>
	    </TD>
	  </TR>
	  <TR class= common id="trAddCondition" style="display:'none'"> 
      <TD class= title> 附加条件</TD>
      <TD class= input colspan="5">
      	<input type="checkbox" name="AddFeeFlag" value="1" onclick="showAddPrem();">风险加费
      	<input type="checkbox" name="SpecFlag" value="1" onclick="showAddSpec();">免除责任
      	<input type="checkbox" name="SubMultFlag" value="1" onclick="showSubMult();">降低档次
      	<input type="checkbox" name="SubAmntFlag" value="1" onclick="showSubAmnt();">降低保额
      </TD>
    </TR>
	  <TR class= common> 
      <TD class= title> 核保意见</TD>
      <TD class= input colspan="5">
      	<textarea name="UWIdea" verify="核保意见" cols="100%" rows="3" class="common"></textarea>
      </TD>
    </TR>
 		<TR class= common id="trAddFee" style="display:'none'">
 			<TD class= common colspan="7">
 				<table class= common>
 					<TR class= common >
	 			   <td class=common><b>加费信息 </b></td>
	  			</TR>
	  			<TR class= common >
	 				  <td text-align: left colSpan=1>
  						<span id="spanAddFeeGrid" >
  						</span>
	  				</td>
	  			</TR>
	  		</table>
 			</TD>
 		</TR>
 		<TR class= common id="trAddSpec" style="display:'none'">
 			<TD class= common colspan="7">
 				<table class= common>
 					<TR class= common >
	 			   <td class=common><b>免责信息 </b></td>
	  			</TR>
	  			<TR class= common >
		 		  	 <td text-align: left colSpan=1>
		  					<span id="spanSpecGrid" >
		  					</span>
		  			 </td>
	  			</TR>
	  		</table>
 			</TD>
 		</TR>
		<TR class=common id="trSubMult" style="display:'none'">
			<TD class= common colspan="7">
				<table class= common>
				 	<TR class= common >
	 			   <td class=common><b>降档信息 </b></td>
	  			</TR>
					<TR class= common >
						<td class= title>原档次 </td>
						<td class= input>
							<input class=readonly name="initMult" readonly>
						</td>
						<td class= title>降低后档次</td>
						<td class= input>
							<input class=common name="InputMult">
						</td>
					</TR>
	  		</table>
			</TD>
		</TR>
	  <TR class=common id="trSubAmnt" style="display:'none'">
			<TD class= common colspan="7">
				<table class= common>
				 	<TR class= common >
	 			   <td class=common><b>减额信息 </b></td>
	  			</TR>
					<TR class= common >
						<td class= title>原保额 </td>
						<td class= input>
							<input class=readonly name="initAmnt" readonly>
						</td>
						<td class= title>降低后保额</td>
						<td class= input>
							<input class=common name="InputAmnt">
						</td>
					</TR>
	  		</table>
			</TD>
		</TR>	
 		<!--
 		<TR class= common id="trRemark" style="display:'none'">
 			<TD class= common colspan="7">
 			<table class= common>
		    <TR class= common> 
		    	<TD class=""> <b>修改特别约定 </b></TD>
		    </TR>
		    <TR class= common>
		      <TD class= title> 特约原因 </TD>
		      <TD class= input>
		     		 <textarea name="SpecReason" cols="115%" rows="2" class="common"></textarea>
		      </TD>
		    </TR>
		    <TR class= common>
		      <TD class= title> 特别约定 </TD>
		      <TD class= input>
		     		 <textarea name="Remark" cols="115%" rows="2" class="common"></textarea>
		      </TD>
		     </TR>
 			</TABLE>
 			</TD>
 		</TR>
 		-->
  </TABLE>
 </Div>
  <br>
  <div style="display: ''" >
    <input type="button" class=cssButton name="confirm" value="保存结论" onclick="saveDecision();">
     <input type="button" class=cssButton name="SendLetter" value="下发通知书" onclick="sendLetter();">
	<input type="button" class=cssButton name="redo" value="核保完毕" onclick="confirmDecision();">
  </div>
</div>
<div id="divUWInfo" style="display: '<%if (hasUW == false) out.print("none");%>'">
  <table>
    <tr>
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divManuUWList);"> 
      </td>
      <td class= titleImg>人工核保信息 </td>
    </tr>
  </table>
	<Div  id= "divManuUWList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanManuUWGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage5" align=center style= "display: 'none' ">
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage5.firstPage(); showCodeName();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage5.previousPage(); showCodeName();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage5.nextPage(); showCodeName();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage5.lastPage(); showCodeName();"> 
		</Div>
	</div>
</div>
  <input type="hidden" name="MissionId">
  <input type="hidden" name="SubMissionId">
  <input type="hidden" name="ActivityId">
  <input type="hidden" name="EdorNo">
	<input type="hidden" name="EdorType">
	<input type="hidden" name="ContNo">
	<input type="hidden" name="PrtNo">
	<input type="hidden" name="CaseNo">
	<input type="hidden" name="PolNo">
	<input type="hidden" name="AppntNo">
	<input type="hidden" name="InsuredNo">
	<input type="hidden" name="fmtransact">
	<input type="hidden" name="PassFlag">
	
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>