//程序名称：RReportQuery.js
//程序功能：生存调查报告查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();


//提交，保存按钮对应操作
function submitForm() 
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}


//提交数据后执行的操作
function afterSubmit(flag, content)
{
	showInfo.close();
	window.focus();
	if (flag == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}


/*********************************************************************
 *  生调信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function saveRReport()
{
	var rowNum = QuestGrid.mulLineCount ;
	 
	if(rowNum<=0){
		alert("请选择契调回销编号！");
		return false;
	}
	var i = 0 ;
	for(i ; i < rowNum ; i++){
		var cn = QuestGrid.getRowColData(i,5);
		if(cn.trim()=="" || cn==null || cn.trim()==''){
			alert("请填入契约调查结果！") ;
			return false;
		}
	}
	
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action= "./PEdorRReportQuerySave.jsp";
  fm.submit(); //提交
}

function easyQueryLCRRClick()
{
	var sql = "select a.PrtSeq,a.ContNo,b.InsuredNo,b.InsuredName,a.makedate "+
	                ",b.PrtNo,b.PolApplyDate,b.CValiDate,a.AppntName,b.AgentCode,a.ManageCom,a.customerno,a.Appntno from LPRReport a ,lccont b " 
                   + "where 1=1 and a.contno=b.contno  "
	                 + "and a.ContNo = '" + fm.ContNo.value + "' "
	                 + "and a.EdorNo = '" + fm.EdorNo.value + "' "
				           + " order by a.makedate, a.maketime";	 
  turnPage.queryModal(sql, LCRReportGrid);
}

function onClickedReport()
{
  var selNo = LCRReportGrid.getSelNo() - 1;
  fm.PrtSeq.value = LCRReportGrid.getRowColData(selNo, 1);
  fm.ContNo.value = LCRReportGrid.getRowColData(selNo, 2);
  fm.InsuredNo.value = LCRReportGrid.getRowColData(selNo, 3);
  fm.InsuredName.value = LCRReportGrid.getRowColData(selNo, 4);
  var sql = "select b.RReportItemCode,a.name,b.RReportItemName,b.RRITEMCONTENT,b.RRITEMRESULT " +
            "from LPRReport a, LPRReportItem b " + 
            "where a.EdorNo = b.EdorNo " +
            "and a.PrtSeq = '" + fm.PrtSeq.value + "' " +
            "and a.EdorNo = '" + fm.EdorNo.value + "'";
  turnPage2.queryModal(sql, QuestGrid);
  queryResultInfo();
}

function queryResultInfo()
{
  var sql = "select Contente, ReplyContente from LPRReport " +
        "where EdorNo = '" + fm.EdorNo.value + "' " +
        "and PrtSeq = '" + fm.PrtSeq.value + "' ";
  var result = easyExecSql(sql);
  if (result != null)
  {
    fm.Contente.value = result[0][0];
    fm.ReplyContente.value = result[0][1];
  }
}