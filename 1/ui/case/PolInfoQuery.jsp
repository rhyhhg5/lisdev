<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22
//创建人  ：gn
//更新记录：  更新人    更新日期     更新原因/内容
//修改：
//修改日期：2004-6-17
%>
<%
	String tContNo = "";
	String tIsCancelPolFlag = "";
	String tCustomerNo = "";
	try
	{
		tContNo = request.getParameter("ContNo");
		tCustomerNo = request.getParameter("CustomerNo");
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
		System.out.println("tContNodfdf="+tContNo);
	}
	catch( Exception e )
	{
		tContNo = "";
		tIsCancelPolFlag = "0";
	}
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="PolInfoQuery.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="PolInfoQueryInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>保单查询 </title>
</head>

<body  onload="initForm(),easyQueryClick();" >
  <form  name=fm > 
  
  
    <table>
    	<tr>
        <td class=common>
			
    		</td>
    		<td class= titleImg>
    			
    		</td>
    	</tr>           
    </table>           
           
           
   <div id="DivLCCont" STYLE="display:'none'">
			<table class="common" id="table2">
				<tr CLASS="common">
					<td CLASS="title">合同投保单号码
		    		        </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="GrpContNo" type=hidden VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20">
		    		       
					<input NAME="ContNo" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20">
		    		         </td>
					<td CLASS="title">印刷号码
		    		         </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="PrtNo" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="14" verify="印刷号码|notnull&amp;len=14">
		    		        </td>
					<td CLASS="title">管理机构
		    		         </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="ManageCom" VALUE MAXLENGTH="10" CLASS="code"ondblclick="return showCodeList('comcode',[this],null,null,'#1# andLength(trim(comcode))=8','1',1);" onkeyup="returnshowCodeListKey('comcode',[this],null,null,'#1# andLength(trim(comcode))=8','1',1);" verify="管理机构|code:station&amp;notnull">
		    		         </td>
				</tr>
				<tr CLASS="common">
					<td CLASS="title">销售渠道
		    		         </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="SaleChnl" VALUE CLASS="common" MAXLENGTH="2">
		    		           </td>
					<td CLASS="title">代理人编码
		    		           </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="AgentCode" VALUE MAXLENGTH="10" CLASS="code"ondblclick="return queryAgent();" onkeyup="return queryAgent2();">
		    		         </td>
					<td CLASS="title">代理人组别
		    		          </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="AgentGroup" VALUE CLASS="readonly" readonlyTABINDEX="-1" MAXLENGTH="12" verify="代理人组别|notnull">
		    		        </td>
				</tr>
				<tr CLASS="common">
		
					<td CLASS="title">代理机构
		    		         </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="AgentCom" VALUE CLASS="code" ondblclick="showCodeList('AgentCom',[this],null,null,'1 and #1# = #1# and ManageCom like #' + fm.all('ManageCom').value.substring(0,4) + '%# and #' + fm.all('ManageCom').value + '# is not null  ','1');" onkeyup="return showCodeListKey('AgentCom',[this],null,null,'1 and #1# = #1# and ManageCom like #' + fm.all('ManageCom').value.substring(0,4) + '%# and #' + fm.all('ManageCom').value + '# is not null  ','1');" verify="代理机构|code:AgentCom">
					</td>
		    </tr>
      </table>
   </div>
  
  
  
    <table>
    	<tr>
		    		<td class= titleImg> 			
		   					
		    		</td>
    	</tr>
    </table>
  
  
<DIV id=DivLCAppntInd STYLE="display:'none'">
 <table  class= common>
        <TR  class= common>       
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=AppntName verify="投保人姓名|notnull&len<=20" >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="code" name=AppntSex verify="投保人性别|notnull&code:Sex"ondblclick="return showCodeList('Sex',[this]);" onkeyup="returnshowCodeListKey('Sex',[this]);">
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="coolDatePicker" dateFormat="short" name="AppntBirthday" verify="投保人出生日期|notnull&date" >
          </TD>
        </TR>
       
        <TR  class= common>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntIDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name="AppntIDNo" verify="投保人证件号码|len<=20">
          </TD>
        </TR>
       
        <TR  class= common>
          <TD  class= title>
            职业
          </TD>
          <TD  class= input>
          <input class="code" name="WorkType" verify="投保人职业|code:NativePlace" ondblclick="returnshowCodeList('NativePlace',[this]);" onkeyup="returnshowCodeListKey('NativePlace',[this]);">
          </TD>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class= common name="AppntRgtAddress" verify="投保人户口所在地|len<=80" >
          </TD>
          </TR>  
   </table>
   </div>
  
  <br>
  
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick= "showPage(this,divPol1);">
    		</td>
    		<td class= titleImg>
    			 保单险种信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span>
  			  	</td>
  				</tr>
    		</table>
    		<table>
              		
	    		<INPUT class=CssButton VALUE="保全查询" TYPE=hidden onclick="PerEdorQueryClick();"> 
	    		<INPUT class=CssButton VALUE="保单明细查询" TYPE=hidden onclick="getQueryDetail();"> 
					<INPUT class=CssButton VALUE="理赔查询" TYPE=hidden onclick="ClaimGetQuery();">
					<INPUT class=CssButton VALUE="扫描件查询" TYPE=hidden onclick="ScanQuery();"> 
					<INPUT class=CssButton VALUE="备注信息" TYPE=hidden onclick="ShowRemark();"> 
								   
					<INPUT class=CssButton VALUE="返  回" TYPE=hidden onclick="GoBack();"> 
    			<input type=hidden name=insuredno>     	    
  			</table>		
  	</Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>
	var tContNo = "<%=tContNo%>"; 
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
	
	fm.all('ContNo').value = "<%=tContNo%>";
</script>
</html>


