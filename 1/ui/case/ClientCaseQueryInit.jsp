<%
//程序名称：ClientCaseQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-21 09:37:42
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CaseNo').value = "";
    fm.all('RgtNo').value = "";
    fm.all('RgtType').value = "";
    fm.all('RgtState').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').value = "";
    fm.all('AccidentType').value = "";
    fm.all('ReceiptFlag').value = "";
    fm.all('HospitalFlag').value = "";
    fm.all('SurveyFlag').value = "";
    fm.all('AffixGetDate').value = "";
    fm.all('FeeInputFlag').value = "";
    fm.all('InHospitalDate').value = "";
    fm.all('OutHospitalDate').value = "";
    fm.all('InvaliHosDays').value = "";
    fm.all('InHospitalDays').value = "";
    fm.all('DianoseDate').value = "";
    fm.all('PostalAddress').value = "";
    fm.all('Phone').value = "";
    fm.all('AccStartDate').value = "";
    fm.all('AccidentDate').value = "";
    fm.all('AccidentSite').value = "";
    fm.all('DeathDate').value = "";
    fm.all('CustBirthday').value = "";
    fm.all('CustomerSex').value = "";
    fm.all('CustomerAge').value = "";
    fm.all('IDType').value = "";
    fm.all('IDNo').value = "";
    fm.all('Handler').value = "";
    fm.all('UWState').value = "";
    fm.all('Dealer').value = "";
    fm.all('AppealFlag').value = "";
    fm.all('GetMode').value = "";
    fm.all('GetIntv').value = "";
    fm.all('CalFlag').value = "";
    fm.all('UWFlag').value = "";
    fm.all('DeclineFlag').value = "";
    fm.all('EndCaseFlag').value = "";
    fm.all('EndCaseDate').value = "";
    fm.all('MngCom').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在ClientCaseQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initClientCaseGrid();  
  }
  catch(re) {
    alert("ClientCaseQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ClientCaseGrid;
function initClientCaseGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="案件号";   
		iArray[1][1]="35px";   
		iArray[1][2]=20;        
		iArray[1][3]=3;
    
    iArray[2]=new Array(); 
		iArray[2][0]="理赔号";   
		iArray[2][1]="135px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="案件类型";   
		iArray[3][1]="70px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="客户号";   
		iArray[4][1]="100px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
		iArray[5]=new Array(); 
		iArray[5][0]="客户名称";   
		iArray[5][1]="100px";   
		iArray[5][2]=20;        
		iArray[5][3]=0;
		
		iArray[6]=new Array(); 
		iArray[6][0]="出险类型";   
		iArray[6][1]="35px";   
		iArray[6][2]=20;        
		iArray[6][3]=3;
		
		iArray[7]=new Array(); 
		iArray[7][0]="联系地址";   
		iArray[7][1]="100px";   
		iArray[7][2]=20;        
		iArray[7][3]=0;
		
		iArray[8]=new Array(); 
		iArray[8][0]="联系电话";   
		iArray[8][1]="100px";   
		iArray[8][2]=20;        
		iArray[8][3]=0;	
    
    ClientCaseGrid = new MulLineEnter( "fm" , "ClientCaseGrid" ); 
    //这些属性必须在loadMulLine前

    ClientCaseGrid.mulLineCount = 3;   
    ClientCaseGrid.displayTitle = 1;
    ClientCaseGrid.hiddenPlus = 1;
    ClientCaseGrid.hiddenSubtraction = 1;
    ClientCaseGrid.canSel = 1;
    ClientCaseGrid.canChk = 0;
    //ClientCaseGrid.selBoxEventFuncName = "showOne";

    ClientCaseGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ClientCaseGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
