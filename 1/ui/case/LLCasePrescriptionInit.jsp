<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
var turnPage = new turnPageClass();
function initInpBox()
{
  try{
    fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
    fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
   
    fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
    fm.FeeType.value = '<%= request.getParameter("FeeType") %>';
    var LoadFlag = '<%= request.getParameter("LoadFlag") %>';
    var strSQL3="select customername from llcase where caseno='"+fm.CaseNo.value+"'";
	var arrResult3 = easyExecSql(strSQL3);
	
	if(arrResult3 != null && arrResult3 !="" ){ 
		 fm.CustomerName.value = arrResult3[0][0];
     }
   
    if(LoadFlag=='2'){
      fm.confirmButton.style.display = 'none';
    }
  }
  catch(ex)
  {
    alert(ex.message);
    alert("在LLCasePrescriptionInit.jspInitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{  
  try                 
  {
  	var strSQL="SELECT P.DrugName, P.DrugQuantity, P.TimesPerDay, P.QuaPerTime"
  	+ " FROM LLCasePrescription P WHERE P.CASENO = '" + fm.CaseNo.value + "'";

	
	arrResult = easyExecSql(strSQL);
    if (arrResult){
   		displayMultiline(arrResult,LLCasePrescriptionGrid);
    }
    
    if(fm.CaseNo.value!=""){
       var strSQL="SELECT P.DiagnoseType" 
  	 + " FROM LLCasePrescription P WHERE P.CASENO = '" + fm.CaseNo.value + "'";
      arrResult = easyExecSql(strSQL);
      if(arrResult){
       fm.DiagnoseType.value = arrResult[0][0];
      }
    }
  }
  catch(ex)
  {
    alert("在LLCasePrescriptionInit.jspInitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initLLCasePrescriptionGrid();
    initSelBox();
    easyQuery();
    
    //getCaseRemark();
    
       
  }
  catch(ex)
  {
    alert("LLCasePrescriptionInit.jspInitForm函数中发生异常:初始化界面错误!");
  }
}
//移动光标mulline计算，都得靠自己啊
function calVales(){
	var num = CaseDrugGrid.mulLineCount;
	//alert(num);
	if(num != 0){
		// 通过 getElementsByName 获得都有 mulline 控件 
		var inputs1 =document.getElementsByName("CaseDrugGrid5");//单价 
		// 为每一个button绑定onclick事件，alert一下 
		for(var i=0;i<inputs1.length;i++){ 
			//alert(inputs1.length);
			inputs1[i].onchange = function(){ 
				calPrice(); 
			} 
		} 
		var inputs2 =document.getElementsByName("CaseDrugGrid6");//数量 
		// 为每一个button绑定onclick事件，alert一下 
		for(var i=0;i<inputs2.length;i++){ 
			//alert(inputs2.length);
			inputs2[i].onchange = function(){ 
				calPrice(); 
			} 
		} 
	}
}


//药品剂量明细
function initLLCasePrescriptionGrid()
{
    var iArray = new Array();
      
    try
    {
      
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]= new Array("药品名称","60px","100","1");
      iArray[1][21]="DrugName";
      
      //iArray[1][10]="chargedetailstype";  //引用代码："CodeName"为传入数据的名称
      //iArray[1][11]="0|^1|药品^2|诊疗项目^3|服务设施"; //"CodeContent" 是传入要下拉显示的代码
      //iArray[1][12]="1|11";     //引用代码对应第几列，'|'为分割符
      //iArray[1][13]="1|0";    //上面的列中放置引用代码中第几位值
      
           
      iArray[2]=new Array();
      iArray[2][0]="药品剂量";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;            			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="DrugQuantity";
       
      iArray[3]=new Array();
      iArray[3][0]="日次数";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="TimesPerDay";
      
      iArray[4]=new Array();
      iArray[4][0]="次用量";         			//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[4][21]="QuaPerTime";

      
	
     

      //iArray[8]= new Array("给付类别","60px","100","2","detailsgivetype","8|10","1|0");
      //iArray[8][10]="detailsgivetype";  //引用代码："CodeName"为传入数据的名称
      //iArray[8][11]="0|^1|甲类^2|乙类^3|丙类"; //"CodeContent" 是传入要下拉显示的代码
      //iArray[8][12]="8|10";     //引用代码对应第几列，'|'为分割符
      //iArray[8][13]="1|0";    //上面的列中放置引用代码中第几位值
      

      
	  
      LLCasePrescriptionGrid = new MulLineEnter( "fm" , "LLCasePrescriptionGrid" ); 
      LLCasePrescriptionGrid.mulLineCount = 1; 
      LLCasePrescriptionGrid.displayTitle = 1;
      LLCasePrescriptionGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      LLCasePrescriptionGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)     
      LLCasePrescriptionGrid.canSel=0;
      LLCasePrescriptionGrid.canChk=0;
      //CaseDrugGrid.addEventFuncName="calVales";//“+”你写的JS函数名，不加扩号
      
      //CaseDrugGrid.delEventFuncName="calVales";//“-”你写的JS函数名，不加扩号

      
      LLCasePrescriptionGrid.loadMulLine(iArray);      
      
      }
      catch(ex)
      {
        //alert(ex);
       

      }
  }

</script>