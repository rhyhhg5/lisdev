<%
//程序名称：PolQueryInit.jsp
//程序功能：
//创建日期：2002-12-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">
var tIsCancelPolFlag = "";
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  { 
  		fm.all('ContNo').value = '<%=request.getParameter("ContNo")%>';
  		fm.all('PrtNo').value = '<%=request.getParameter("PrtNo")%>';
  		fm.all('CustomerNo').value = '<%=request.getParameter("CustomerNo")%>';
  		fm.all('GrpContNo').value = '<%=request.getParameter("GrpContNo")%>'; 
  		tIsCancelPolFlag = '<%=request.getParameter("IsCancelPolFlag")%>';
  }
  catch(ex)
  {
    alert("在PolDetailQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("PolDetailQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initPolGrid(); 
  }
  catch(re)
  {
    alert("PolDetailQueryInit.jsp->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

var PolGrid; 
// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0; 
      iArray[1][4]="";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="6";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[1][18]=250;
      iArray[1][19]= 0 ;
      
      iArray[2]=new Array();                                                  
      iArray[2][0]="险种名称";         		  //列名                                  
      iArray[2][1]="200px";            	//列宽                              
      iArray[2][2]=100;            			//列最大值                            
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        
     
      
      iArray[3]=new Array();
      iArray[3][0]="保额";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="保费";         		//列名
      iArray[4][1]="40px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;  
                  			
      iArray[5]=new Array();
      iArray[5][0]="档次/计划";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

			iArray[6]=new Array();
      iArray[6][0]="特别约定";         		//列名
      iArray[6][1]="150px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="生效日期";         	//列名
      iArray[7][1]="60px";            	//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[8]=new Array();
      iArray[8][0]="保单年度";         	//列名
      iArray[8][1]="40px";            	//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
     

      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>

      