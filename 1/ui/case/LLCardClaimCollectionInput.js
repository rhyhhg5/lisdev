var turnPage = new turnPageClass();  
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	//if (verifyInput() == false)
    //return false;
    var sum;
    var CvaliDateNum=checkCouple(fm.CvaliDateS.value,fm.CvaliDateE.value);
    var JieSuanNum=checkCouple(fm.JieSuanS.value,fm.JieSuanS.value);
    var ActiveDateNum=checkCouple(fm.ActiveDateS.value,fm.ActiveDateS.value);
    sum=CvaliDateNum+JieSuanNum+ActiveDateNum;
    
    if(sum!=2)//sum为0,1,3,4,5,6都不可以，只有2才可以，
    {
    	alert("保单生效、结算、激活期间不能全为空，而且一次只能选一个！");
    	return;
    }
    else if(CvaliDateNum!=2&&JieSuanNum!=2&&ActiveDateNum!=2)//为2但两个不同期间各填一个的情况
    {
    	alert("起止期要同时存在！");
    	return;
    }
    
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
  fm.action = "LLCardClaimCollectionRpt.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "LLClaimCollection";
	fm.submit();
	showInfo.close();
}
//判断起止日期存在情况，当都没有时，返回0，当只有一个有时返回1，当都有时，返回2
function checkCouple(startDate,endDate)
{
	if(startDate==''&&endDate=='')
		return 0;
	if((startDate==''&&endDate!='')||(startDate!=''&&endDate==''))
		return 1;
	if(startDate!=''&&endDate!='')
		return 2;
	return -1;
}                                       
function GrpTypeSearch() {
	var strSQL = "select code,codename from ldcode where codetype = 'llgrptype' order by code with ur";
	turnPage.queryModal(strSQL,BusinessGrid);
}

function GrpTypeSave(operate) {
	//var Count=BusinessGrid.mulLineCount;
  //if(Count==0){
  //	alert("没有业务类型信息!");
  //	return false;
  //}
  var selNo = BusinessGrid.getSelNo();
  if ( selNo<1 ) {
  	alert("请选择重点业务");
  	return false;
  }

  	if (BusinessGrid.getRowColData(selNo-1,2)==null
  	    ||BusinessGrid.getRowColData(selNo-1,2)==""){
  	    	alert("重点业务名称不能为空！");
  	    	return false;
    }

	
	if (operate==1){
		
		var strSQL = "select 1 from ldcode where "+
		"codetype='llgrptype' and code='"+BusinessGrid.getRowColData(selNo-1,1)+"'";
		//alert(strSQL);
		var arrResult = easyExecSql(strSQL);
		if(arrResult != null) {
			alert("已经有该重点业务！");
			return false;
		}
		fm.fmtransact.value = "INSERT";
	} else if (operate==2) {
		var strSQL = "select 1 from ldcode1 where codetype='llgrptype' and code in (select code from ldcode where "+
		"codetype='llgrptype' and code='"+BusinessGrid.getRowColData(selNo-1,1)+"')";
		//alert(strSQL);
		var arrResult = easyExecSql(strSQL);
		if(arrResult != null) {
			alert("该重点业务下还有保单，请先删除保单！");
			return false;
		}
		fm.fmtransact.value = "DELETE";
	}
	else if(operate==3) {
		fm.fmtransact.value = "UPDATE";
	}
	else
		alert('未知操作');
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "LLClaimCollecionTypeSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();
	//alert('hello');
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  if(FlagStr!=null&&FlagStr=="Success1")
  	GrpTypeSearch();
}

function GrpContSearch() {
	var strSQL = "select a.codename,a.code,b.code1,b.codename from ldcode a,ldcode1 b "
	           + " where a.codetype=b.codetype and b.codetype='llgrptype' "
	           + " and a.code=b.code "
	           + getWherePart("b.code","GrpType")
	           + getWherePart("b.code1","ContNo")
	           + " with ur ";

	turnPage.queryModal(strSQL,BusinessGrpContGrid);
}

function GrpContTypeSave(operate) {
	var Count=BusinessGrpContGrid.mulLineCount;
  if(Count==0){
  	alert("没有保单信息!");
  	return false;
  }
  
  var selNo = BusinessGrpContGrid.getSelNo();
  if ( selNo<1 ) {
  	alert("请选择保单");
  	return false;
  }
  if (BusinessGrpContGrid.getRowColData(selNo-1,2)==null
      ||BusinessGrpContGrid.getRowColData(selNo-1,2)=="") {
      	alert("请选择业务类型");
      	return false;
  }
  
  var tGrpContNo = BusinessGrpContGrid.getRowColData(selNo-1,3);
  if (tGrpContNo==null ||tGrpContNo=="") {
      	alert("请输入保单号");
      	return false;
  }
	
	var strSQL = "select grpname from lcgrpcont where grpcontno = '"+tGrpContNo
	           + "' union select grpname from lbgrpcont where grpcontno = '"+tGrpContNo+"'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult == null) {
		alert("保单查询失败");
		return false;
	} else {
		BusinessGrpContGrid.setRowColData(selNo-1,4,arrResult[0][0]);
	}
		
	if (operate==1){
		fm.fmtransact.value = "DELETE&INSERT";
	} else if (operate==2) {
		fm.fmtransact.value = "DELETE";
	}
	else if(operate==3)
	{
		fm.fmtransact.value = "UPDATE";
	}
	else
	{
		alert("GrpContTypeSave没有参数");
		return;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.action = "LLClaimCollecionGrpTypeSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();
}

