<%
//程序名称：BlackListInput.jsp
//程序功能：
//创建日期：2008-07-25
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LCPolSchema tLCPolSchema   = new LCPolSchema();
	
  GrpContRiskAmntModifyUI tGrpContRiskAmntModifyUI = new GrpContRiskAmntModifyUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operate");
  tOperate=tOperate.trim();
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tRadio[] = request.getParameterValues("InpGrpContGridSel");
  String tGrpContNo[] = request.getParameterValues("GrpContGrid1");
  String tRiskCode[] = request.getParameterValues("GrpContGrid5");
  String tContPlanCode[] = request.getParameterValues("GrpContGrid4");
  String tRiskAmnt[] = request.getParameterValues("GrpContGrid7");
  String GrpContNo="";
  String RiskCode="";
  String ContPlanCode="";
  String RiskAmnt="";  
  for (int index=0; index< tRadio.length;index++){
    if(tRadio[index].equals("1")){
      GrpContNo = tGrpContNo[index];
      RiskCode = tRiskCode[index];
      ContPlanCode = tContPlanCode[index];
      RiskAmnt = tRiskAmnt[index];
      System.out.println("GrpContNo:"+GrpContNo);     
      System.out.println("Riskcode:"+RiskCode);
      System.out.println("ContPlanCode:"+ContPlanCode);
      System.out.println("RiskAmnt:"+RiskAmnt);
    }
  }
  tLCPolSchema.setGrpContNo(GrpContNo);
  tLCPolSchema.setRiskCode(RiskCode);
  tLCPolSchema.setContPlanCode(ContPlanCode);
  tLCPolSchema.setRiskAmnt(RiskAmnt);
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLCPolSchema);
	tVData.addElement(tG);
  try
  {
    tGrpContRiskAmntModifyUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tGrpContRiskAmntModifyUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	//Content = " 操作失败，原因是:" + tError.getFirstError();
    	Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

