
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var tSaveFlag = "0";
var mDebug="1";
var mAction = "";
var tSaveType="";

var turnPage = new turnPageClass(); 


function modify()
{
	var selno = GrpContGrid.getSelNo()-1;
	if (selno < 0)
	{
		alert("请选择保单");
		return false;
	}
	var CValidate = GrpContGrid.getRowColDataByName(selno,"CValiDate");
	if(!getOneMonthError(CValidate)){
		alert("生效日期错误");
		return false;
	}

    fm.all("operate").value='UPDATE';
    var showStr="正在保存数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

   	fm.action="./GrpCardCvalidateModifySave.jsp";
  	fm.submit();
	
}
function SearchGrpCont(){
	if ((fm.PrtNo.value==null||fm.PrtNo.value=="")
	     &&(fm.GrpContNo.value==null||fm.GrpContNo.value=="")) {
	     	alert("请输入团体印刷号或团体保单号");
	     	return false;
	}
	if ((fm.InsuredNo.value==null||fm.InsuredNo.value=="")
	     &&(fm.InsuredName.value==null||fm.InsuredName.value=="")) {
	     	alert("请输入客户号码或客户姓名");
	     	return false;
	}
	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句
  
  var strSql = " select a.prtno,a.grpcontno,a.appntname,a.contno,a.insuredno,a.insuredname,a.insuredidno,a.cvalidate,a.cinvalidate"
             + " from lccont a where 1=1"
             + getWherePart("a.prtno","PrtNo")
             + getWherePart("a.grpcontno","GrpContNo")
             + getWherePart("a.insuredno","InsuredNo")
             + getWherePart("a.insuredname","InsuredName");
  turnPage.queryModal(strSql,GrpContGrid);
  showInfo.close();

}

function afterSubmit(FlagStr, content)
{
  showInfo.close();
  SearchGrpCont();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
  
}

