//程序名称：ScanContInput.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
  if(!verifyInput2())
    return false;
  var ApplyDate = fm.InputDate.value;
  if(strCurDay!==ApplyDate)
  {
    alert("申请日期不是当天！");
    return false;
  }
  if( verifyInput2() == false )
    return false;
  cPrtNo = fm.PrtNo.value;
  cManageCom = fm.ManageCom.value;
  if(cPrtNo == "")
  {
    alert("请录入印刷号！");
    return;
  }
  if(cManageCom == "")
  {
    alert("请录入管理机构！");
    return;
  }
  if(isNumeric(cPrtNo)==false)
  {
    alert("印刷号应为数字");
    return false;
  }
  if(type=='2')//对于集体保单的申请
  {
    if (GrpbeforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }

  }
  else //对于个人保单的申请
  {
    if (beforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }
  }
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      
   
  }

}


function GoToInput()
{

	var i = 0;
  var checkFlag = 0;
  for (i=1; i<=GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getChkNo(i-1))
    { 
      checkFlag = true;
      if(GrpGrid.getRowColData(i-1,1)==null||GrpGrid.getRowColData(i-1,1)=="")
       {
       	alert("第"+" "+i+" "+"行的第 1 列不能为空值！");
      	return;
       }
      if(GrpGrid.getRowColData(i-1,2)==null||GrpGrid.getRowColData(i-1,2)=="")
       {
       	alert("第"+" "+i+" "+"行的第 2 列不能为空值！");
      	return;
       }
      continue;
    }
  }

//判断业务表中是否已经有该帐单项目的数据，若有，则不允许保存。  
  for (i=1; i<=GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getChkNo(i-1))
    { 
      var sql = "select count(*) from llfeeotheritem where avlireason = '"
                +GrpGrid.getRowColData(i-1,3)+"' and '"+fm.all('ManageCom').value
                +"' = '86'||substr(caseno,2,2)";
      var arr = easyExecSql(sql);
      if(arr[0][0]>0)
      {
       alert("该机构下"+GrpGrid.getRowColData(i-1,3)+"项目已经存在，不能修改！");
       return;	
      }
      

      continue;
    }
  }  
  

  if (!checkFlag)
  {
    alert("请选择要保存的信息！");
    return;
  }	
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
}


function beforeSubmit()
{
  var strSQL = "";
  strSQL = "select missionprop1 from lwmission where 1=1 "
           + " and activityid = '0000001098' "
           + " and processid = '0000000003'"
           + " and missionprop1='"+fm.PrtNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }

  strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
           "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }
}
function GrpbeforeSubmit()
{
  var strSQL = "";
  strSQL = "select missionprop1 from lwmission where 1=1 "
           + " and activityid = '0000002098' "
           + " and processid = '0000000004'"
           + " and missionprop1='"+fm.PrtNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (turnPage.strQueryResult)
  {

    return false;
  }

  strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
           "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }
}

function queryGrpGrid()
{ //alert(222);
	var strSQL = "";
	strSQL = "select '','',code,codename from ldcode where codetype = 'llsecureason'";
  turnPage.queryModal(strSQL,GrpGrid);
}


function afterCodeSelect( cCodeName, Field )
{
	try{	 
       	var strSQL = "";
       	strSQL = "select (select b.code from ldcode b where b.codetype = 'llsecufeeitem' and b.codealias = a.code and b.comcode = '"+fm.all('ManageCom').value+"' and othersign is null),(select b.codename from ldcode b where b.codetype = 'llsecufeeitem' and b.codealias = a.code and b.comcode = '"+fm.all('ManageCom').value+"' and othersign is null),a.code,a.codename from ldcode a where a.codetype = 'llsecureason'";
        turnPage.queryModal(strSQL,GrpGrid);
	   }
	   catch(ex)
	   {
	   	alert(ex.message);
	   }
}