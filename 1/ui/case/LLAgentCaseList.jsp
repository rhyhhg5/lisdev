<html>
  <%
  //Name：LLMainAskInput.jsp
  //Function：登记界面的初始化
  //Date：2004-12-23 16:49:22
  //Author：wujs
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@page import = "com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
  <%@page import = "com.sinosoft.lis.vschema.*"%>
  <%@page import = "com.sinosoft.lis.llcase.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%
    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
    String Operator=tG.Operator;
    String Comcode=tG.ManageCom;
    String CurrentDate= PubFun.getCurrentDate();
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);
    String AheadDays="-30";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
  %>

  <head>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLAgentCaseList.js"></SCRIPT>
    <%@include file="LLAgentCaseListInit.jsp"%>
    <script language="javascript">
      function initDate(){
        fm.RgtDateS.value="<%=afterdate%>";
        fm.RgtDateE.value="<%=CurrentDate%>";
        var usercode="<%=Operator%>";
        var comcode="<%=Comcode%>";
        fm.AgentCode.value="";
        fm.OrganCode.value=comcode;
      }
    </script>
  </head>
  <body  onload="initDate();initForm();">
    <form action="" method=post name=fm target="fraSubmit">
      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
          </TD>
          <TD class= titleImg>
            业务员案件列表
          </TD>
        </TR>
      </table>

      <Div  id= "divLLLLMainAskInput1" style= "display: ''">
        <table  class= common>
          <TR  class= common8>
            <TD  class= title8>
              <input type="radio" name="StateRadio"  value="0" onclick="classify()">全部案件
              <input type="radio" name="StateRadio"  value="1" onclick="classify()">未结案
              <input type="radio" name="StateRadio"  value="2" onclick="classify()">已结案
            </TD>
          </TR>
          <TR  class= common8>
            <TD  class= title8>
              <input type="radio" name="typeRadio" onclick="classify()" value="0">咨询类
              <input type="radio" name="typeRadio" onclick="classify()" value="9">通知类
              <input type="radio" name="typeRadio" onclick="classify()" checked value="1">申请类
              <input type="radio" name="typeRadio" onclick="classify()" value="4">申诉类
              <input type="radio" name="typeRadio" onclick="classify()" value="5">错误处理类
              <!--<input type="checkbox" value="1">申诉类
              <input type="checkbox" value="1">错误处理类-->
            </td>
          </tr>
          <tr>
            <td>
            <input type="checkbox" value="1" name="llclaimdecision" onclick="classify()">正常给付</input>
          <input type="checkbox" value="2" name="llclaimdecision" onclick="classify()" >部分给付</input>
        <input type="checkbox" value="3" name="llclaimdecision" onclick="classify()">全额拒付</input>
      <input type="checkbox" value="4" name="llclaimdecision" onclick="classify()">协议给付</input>
    <input type="checkbox" value="5" name="llclaimdecision" onclick="classify()">通融给付</input>
  </td>
</tr>
</table>

</DIV>


<table  class= common>
  <TR  class= common8>
    <TD  class= title>管理机构</TD><TD  class= input>
      <Input class= "codeno"  name=OrganCode onkeydown="QueryOnKeyDown();" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);" ><Input class=codename  name=ComName>
      <TD  class= title>业务员姓名</TD><TD  class= input>
        <Input class= "code8"  name=optname ondblclick="return showCodeList('agentname',[this,GroupAgentCode,AgentCode], [0,1,3],null,null,null,1);" onkeyup="return showCodeListKey('agentname', [this,GroupAgentCode,AgentCode], [0, 1,3],null,null,null,1);" >
        <TD  class= title>业务员代码</TD><TD  class= input>
        <Input  class='common8'  name=GroupAgentCode></TD>
        <TD  class= input>
        <Input  type=hidden  name=AgentCode></TD>
      </tr>
      <TR  class= common>
      <Input type=hidden name=RgtNo onkeydown="QueryOnKeyDown()"></TD>
      <TD  class= title8>受理日期从</TD>
      <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
      <TD  class= title8>到</TD>
      <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>

    </TR>
  </table>
</Div>
<input style="display:''"  class=cssButton type=button value="查 询" onclick="classify()">
<Div  id= "divCustomerInfo" style= "display: ''">
  <table  class= common>
    <TR  class= common>
      <TD text-align: left colSpan=1>
        <span id="spanCheckGrid" >
        </span>
      </TD>
    </TR>
  </table>


  <Div  align=center style= "display: '' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  <!--隐藏域-->
<Input  class='common8' type=hidden name=SQL></TD>
<Input type="hidden" class= common name="fmtransact" >
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
