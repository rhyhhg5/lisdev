<%
//Name:ReportInit.jsp
//function：
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initGrpRegisterGrid();
    SearchGrpRegister();
    initGrpCaseGrid();	
    initBackListGrid();
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpRegisterGrid()
{
   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("团体批次号","120px","0",0);
    iArray[2]=new Array("团体客户号","100px","0",0);
    iArray[3]=new Array("单位名称","120px","0",0);
    iArray[4]=new Array("团体合同号","100px","0",0);
    iArray[5]=new Array("申请人","70px","0",0);
    iArray[6]=new Array("申请日期","80px","0",0);
    iArray[7]=new Array("申请人数","60px","0",0);
    iArray[8]=new Array("案件状态","80px","0",0);
    iArray[9]=new Array("案件状态","80px","0",3);
    iArray[10]=new Array("申报金额","80px","0",3);
    
    GrpRegisterGrid = new MulLineEnter( "fm" , "GrpRegisterGrid" );
    
    GrpRegisterGrid.mulLineCount = 10;
    GrpRegisterGrid.displayTitle = 1;
    GrpRegisterGrid.canChk =0;
    GrpRegisterGrid.canSel =1;
    GrpRegisterGrid.hiddenPlus=1;
    GrpRegisterGrid.hiddenSubtraction=1;
    GrpRegisterGrid.locked = 1;  
    GrpRegisterGrid.selBoxEventFuncName = "queryGrpCaseGrid"; 
    GrpRegisterGrid.loadMulLine(iArray);
   } 
  catch(ex)
  {
    alert(ex);
  }    
}

function initGrpCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号", "30px", "10", "0");
    iArray[1] = new Array("理赔号", "40px", "0", "0");
    iArray[2] = new Array("医保号", "40px", "0", "0");
    iArray[3] = new Array("客户号", "40px", "0", "0");
    iArray[4] = new Array("姓名", "40px", "0", "0");
    iArray[5] = new Array("性别", "30px", "0", "0");
    iArray[6] = new Array("身份证号", "50px", "0", "0");
    iArray[7] = new Array("医院编码", "50px", "0", "0");
    iArray[8] = new Array("住院号", "40px", "0", "0");
    iArray[9] = new Array("住院天数", "50px", "0", "0");
    iArray[10]= new Array("经办时间","50px","100","3");
    iArray[11]= new Array("总额","40px","100","0");
    iArray[12]= new Array("纳入统筹额","60px","100","0");
    iArray[13]= new Array("统筹支付额","60px","100","0");
    iArray[14]= new Array("大病支付额","60px","100","0");
    iArray[15]= new Array("现金支付额","60px","1000","0");
    iArray[16]= new Array("实赔金额","50px","40","0")

    GrpCaseGrid = new MulLineEnter("fm","GrpCaseGrid");
    GrpCaseGrid.mulLineCount =5;
    GrpCaseGrid.displayTitle = 1;
    GrpCaseGrid.locked = 1;
    GrpCaseGrid.canChk =1;
    GrpCaseGrid.hiddenPlus=1;  
    GrpCaseGrid.hiddenSubtraction=1; 
    GrpCaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initBackListGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号", "30px", "10", "0");
    iArray[1] = new Array("理赔号", "90px", "0", "0");
    iArray[2] = new Array("医保号", "50px", "0", "0");
    iArray[3] = new Array("客户号", "50px", "0", "0");
    iArray[4] = new Array("姓名", "40px", "0", "0");
    iArray[5]= new Array("35000-49000","50px","100","3");
    iArray[6]= new Array("大病支付额","40px","1000","0");
    iArray[7]= new Array("实赔金额","40px","40","0")
    iArray[8]= new Array("回退原因","40px","10","2")
    iArray[8][4]="llcasebackreason"
    iArray[8][5]="8|9";       //引用代码对应第几列，'|'为分割符
    iArray[8][6]="0|1";
    iArray[8][9]="回退原因|NOTNULL";
    iArray[8][17]="3"     
    iArray[8][19]="1";      
    iArray[9]= new Array("备注","100px","10","1")

    BackListGrid = new MulLineEnter("fm","BackListGrid");
    BackListGrid.mulLineCount =0;
    BackListGrid.displayTitle = 1;
    BackListGrid.locked = 0;
    BackListGrid.canChk =0;
    BackListGrid.hiddenPlus=1;  
    BackListGrid.hiddenSubtraction=0; 
    BackListGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
function initCheckGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array("序号","30px","10","0");
			iArray[1]=new Array("案件号","90px","30","0");
			iArray[2]=new Array("调查序号","60px","30","0");
			iArray[3]=new Array("调查类别","60px","60","0");
			iArray[4]=new Array("提起日期","60px","30","0");;
			iArray[5]=new Array("提调人","60px","50","0");
			iArray[6]=new Array("案件状态","0px","30","3");
			iArray[7]=new Array("调查类别代码","0px","30","3");
			iArray[8]=new Array("调查项目号","0px","20","3");
			iArray[9]=new Array("调查报告状态","120px","50","0");
			iArray[10]=new Array("调查号","0px","30","3");
			iArray[11]=new Array("涉案金额","60px","30","0");
			CheckGrid = new MulLineEnter("fm","CheckGrid");
			CheckGrid.mulLineCount =10;
			CheckGrid.displayTitle = 1;
			CheckGrid.locked = 1;
			CheckGrid.canSel =1;
			CheckGrid.hiddenPlus=1;
			CheckGrid.hiddenSubtraction=1;
			CheckGrid.loadMulLine(iArray);
		}
		catch(ex){
			alter(ex.message);
		}
	}
 </script>