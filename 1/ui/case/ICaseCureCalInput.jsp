<html>
<%
/*******************************************************************************
 * Name     :ICaseCureInput.jsp
 * Function :立案－费用明细信息的初始化页面程序
 * Author   :LiuYansong
 * Date     :2003-7-23
 */
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="ICaseCureCalInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ICaseCureCalInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ICaseCureSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏ICaseCure1的信息 -->

  
 <!--
    <Div  id= "divICaseCure1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD class= title8>
            案件号
          </TD>

          <TD class= input8>
            <Input class="readonly" readonly name=RgtNo>
          </td>
         
     <TD  class= title8>客户号</TD><TD  class= input8> <input class=common  name=CustomerNo</TD>
     <TD  class= title8>客户名称</TD><TD  class= input8> <input class=common  name=CustomerName</TD>
        </TR><TR  class= common>
        <TD  class= title8>医院代码</TD><TD  class= input8> <input class=code  name=HospitalCode</TD>
       <TD  class= title8>医院名称</TD><TD  class= input8> <input class=common  name=HospitalName</TD>

<TD  class= title8>账单号码</TD><TD  class= input8> <input class=common  name=ReceiptNo</TD>
 </TR><TR  class= common>
<TD  class= title8>账单种类</TD><TD  class= input8> <input class=code  name=FeeType</TD>
<TD  class= title8>账单属性</TD><TD  class= input8> <input class=code  name=FeeAtti</TD>
<TD  class= title8>帐单类型</TD><TD  class= input8> <input class=code  name=FeeAffixType</TD>
 </TR><TR  class= common>
<TD  class= title8>账单日期</TD><TD  class= input8> <input class=common  name=FeeDate</TD>
<TD  class= title8>治疗起开始日期</TD><TD  class= input8> <input class=common  name=CureStartDate</TD>
<TD  class= title8>治疗结束日期</TD><TD  class= input8> <input class=common  name=CureEndDate</TD>
</tr>
       </table>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseCure2);">
    		</td>
    		<td class= titleImg>
    			 费用类型
    		</td>
    	</tr>
    </table>
-->
 <table  class= common>
        <TR  class= common>
          <TD class= title8>
            案件号
          </TD>

          <TD class= input8>
            <Input class="readonly" readonly name=RgtNo>
          </td>
         
     <TD  class= title8>保单号</TD><TD  class= input8> <input class=readonly  name=CustomerNo</TD>
     <TD  class= title8>保单责任名称</TD><TD  class= input8> <input class=readonly  name=CustomerName</TD>
        </TR>
        </table>
  <Div  id= "divCaseCure2" style= "display: ''">
  
      <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divICaseCure1);">
      </td>
      <td class= titleImg>
        原始账单明细
      </td>
    	</tr>
    </table>

    <table  class= common>
    	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanICaseCureGrid" >
					</span>
				</td>
			</tr>
		</table>
</div>


  
  <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDegreeOperation);">
      </td>
      <td class= titleImg>
        关联账单明细
      </td>
    	</tr>
    </table>
    
    <Div id= "divDegreeOperation" style = "display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanFeeRelaGrid" >
            </span>
          </td>
        </tr>
      </table>
    </div>

         <INPUT VALUE="药品明细" class=cssButton TYPE=hidden onclick="submitForm();">
         <INPUT VALUE="非药品明细" class=cssButton TYPE=hidden onclick="submitForm();">
         <INPUT VALUE="保 存" class=cssButton TYPE=button onclick="submitForm();">
         <INPUT VALUE="返 回" class=cssButton TYPE=button onclick="top.close();">
    </Div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
  
</body>
<script language="javascript">
try
{
  //fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
  //fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  //fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';
  //fm.CustomerNo.value =' <%= request.getParameter("CustomerNo") %>';
}
catch(ex)
{
  alert(ex);
}
</script>
</html>