<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";

//获取页面数据

String tsOperate = request.getParameter("cOperate");
String tsOperateCN = "";
tsOperateCN = "推算";

//初始化
LLFeeMainSchema mFeeMainSchema = new LLFeeMainSchema();

//账单主信息
String tMainFeeNo = request.getParameter("MainFeeNo");
String tCaseNo = request.getParameter("CaseNo");
mFeeMainSchema.setMainFeeNo(tMainFeeNo);
mFeeMainSchema.setCaseNo(tCaseNo);

System.out.println("UI will begin ");

LLSecurityCalBL tLLSecurityCalBL = new LLSecurityCalBL();
VData tVData = new VData();
try
{
  // 准备传输数据 VData
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  tVData.add(tGI);
  tVData.add(mFeeMainSchema);

  tLLSecurityCalBL.submitData(tVData,tsOperate);
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

if ("".equals(FlagStr))
{
  tError = tLLSecurityCalBL.mErrors;
  if (!tError.needDealError())
  {
    tVData =  tLLSecurityCalBL.getResult();
    mFeeMainSchema = (LLFeeMainSchema) tVData.getObjectByObjectName(
    "LLFeeMainSchema", 0);
    String tipword = "";
    TransferData mTransferData = new TransferData();
    mTransferData=(TransferData) tVData.getObjectByObjectName("TransferData",0);
    if (mTransferData!=null)
    tipword = (String) mTransferData.getValueByName("tipword");
    tMainFeeNo = mFeeMainSchema.getMainFeeNo();
    System.out.println("aftersubmit " + tipword);
    Content = "账单信息" + tsOperateCN + "成功"+tipword;
    FlagStr = "Succ";
  }
  else
  {
    System.out.println("fail" + tError.getFirstError());
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=tMainFeeNo%>");
</script>
</html>