<html>
	<%
	//Name：LLSurveyReplyConf.jsp
	//Function：调查审核
	//Date：2004-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLSurveyReplyConf.js"></SCRIPT>
		<%@include file="LLSurveyReplyConfInit.jsp"%>
	</head>

	<body  onload="initForm();" >
		<form action="./LLSurveyReplySave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						调查工作
					</TD>
				</TR>
			</table>


			<table  class= common>
				<TR  class= common8>
					<TD class= title8>客户号码</TD>
					<TD class= input8><input class= common name="CustomerNo"></TD>
					<TD class= title8>客户名称</TD>
					<TD class= input8><input class= common name="CustomerName"></TD>
					<TD class= title8>客户性别</TD>
					<TD class= input8><input class= common name="Sex"></TD>
				</TR>
				<TR  class= common8  style="display:'none'">
					<TD class= title8>调查类型</TD>
					<TD class= input8><input class= common name="SurveyType"></TD>
					<TD class= title8>调查类别</TD>
					<TD class= input8><input class= common name="SurveyClass"></TD>
					<TD class= title8>提起日期</TD>
					<TD class= input8><input class= common name="SurveyStartDate"></TD>
				</TR>
				<TR  class= common8 style= "display:'none'">
					<TD class= title8>提起原因</TD>
					<TD class= input8><input class= common name="SurveyRCode"></TD>
					<TD class= title8>调查地点</TD>
					<TD class= input8><input class= common name="SurveySite"></TD>
					<TD class= title8>事件号码</TD>
					<TD class= input8><input class= common name="SubRptNo"></TD>
				</TR>
				<TR  class= common>
					<TD  class= title colspan="6">调查内容</TD>
				</TR>
				<TR  class= common>
					<TD  class= input colspan="6">
						<textarea name="Content" cols="100%" rows="6" witdh=25% readonly class="common"></textarea>
					</TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>回复人</TD><TD  class= input8><input class= common readonly name="SurveyOperator"></TD>
					<!--<TD  class= title8>回复时间</TD><TD  class= input8><input class= common name="ConfDate"></TD>-->
				</TR>

				<TR  class= common>
					<TD  class= title colspan="6">院内调查回复</TD>
				</TR>
				<TR  class= common>
					<TD  class= input colspan="6">
						<textarea name="result" cols="100%" rows="6" witdh=25% readonly class="common"></textarea>
					</TD>
				</TR>
				<TR  class= common>
					<TD  class= title colspan="6">院外调查回复</TD>
				</TR>
				<TR  class= common>
					<TD  class= input colspan="6">
						<textarea name="resultOut" cols="100%" rows="6" witdh=25% readonly class="common"></textarea>
					</TD>
				</TR>


				<TR  class= common8>
					<TD  class= title8>审核结论</TD>
					<TD  class= input8><input class= code name="SurveyFlag" CodeData= "0|^0|通过^1|继续调查" ondblClick="showCodeListEx('SurveyFlag',[this,''],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('SurveyFlag',[this,''],[0,1],'', '', '', 1);"></TD>
				</TR>
				<TR  class= common>
					<TD  class= title colspan="6">审核意见</TD>
				</TR>

				<TR  class= common>
					<TD  class= input colspan="6">
						<textarea name="ConfNote" cols="100%" rows="3"  class="common"></textarea>
					</TD>
				</TR>



			</table>
			<Div align="left">
				<input name="AskIn" style="display:''"  class=cssButton type=button value="保 存" onclick="LLSurveyReplyConf()">
				<input name="BlackList" style="display:''"  class=cssButton type=button value="标记黑名单" onclick="submitBlackList()">
				<input name="Return" style="display:''"  class=cssButton type=button value="返 回" onclick="CloseWindows()">
				<input name="ShowConfReturn" style="display:''"  class=cssButton type=button value="查看扫描件" onclick="ShowConf()">
			</div>


			<!--隐藏域-->
			<Input type="hidden" class= common name="fmtransact" >
			<input type=hidden id="SurveyNo" name="SurveyNo">
			<input type=hidden id="OtherNo" name="OtherNo">
			<input type=hidden id="OtherNoType" name="OtherNoType">
			<input type=hidden id="StartPhase" name="StartPhase">
			<input type=hidden id="SurveyRDesc" name="SurveyRDesc">
			<input type=hidden id="SurveyEndDate" name="SurveyEndDate">
			<input type=hidden id="StartMan" name="StartMan">
			<input type=hidden id="Confer" name="Confer">
			<input type=hidden id="MngCom" name="MngCom">
			<input type=hidden id="Operator" name="Operator">
			<input type=hidden id="MakeDate" name="MakeDate">
			<input type=hidden id="MakeTime" name="MakeTime">
			<input type=hidden id="ModifyDate" name="ModifyDate">
			<input type=hidden id="ModifyTime" name="ModifyTime">
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
