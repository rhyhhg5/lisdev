<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLNOPolicyInput.jsp
//程序功能：F1报表生成
//创建日期：2011-10-25
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLNOPolicyInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>
	function initDate(){
   fm.StartDate.value="<%=afterdate%>";
   fm.EndDate.value="<%=CurrentDate%>";
  }
</script>
</head>
<body onload="initDate();" >    
  <form action="./LLNOCasePolicySaveCSV.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom   verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName elementtype=nacessary></TD>    
        </TR>
        <TR  class= common>    
          <TD  class= title>险种类型</TD>
          <TD  class= input> <input class="codeno" CodeData="0|2^1|个险^2|团险"   name=ContType ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input class=codename name=ContTypeName></TD> 
		  <TD  class= title>险种编码</TD>
          <TD  class= input><Input class=codeno  name="RiskCode" onClick="showCodeList('riskprop2',[this,RiskName],[0,1],null,fm.ContType.value,'riskprop',1,250);" onkeyup="showCodeListKeyEx('riskprop2',[this,RiskName],[0,1],null,fm.ContType.value,'riskprop',1,250);" ><Input class=codename name= RiskName></TD> 
        </TR>
        <TR class = common>
          <TD  class= title>保单号</TD>
          <TD  class= input> <input class=common name=ContNo ></TD> 
          <TD  class= title>批次号</TD>
          <TD  class= input> <input class=common name=RgtNo ></TD> 
        </TR>
      	<TR  class= common>
      	  <TD  class= title>理算起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary  > </TD> 
          <TD  class= title>理算止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary > </TD> 
		 </TR>
		
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
              <INPUT VALUE="打 印" class="cssButton" TYPE="button" onclick="CasePolicyCSV()"></TD>
			</tr>
		</table>
	 <table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">管理机构：案件受理机构</td></tr><br>
            <tr class="common"><td class="title">险种类型：根据产品定义，将所有险种分为个险、团险，非必选</td></tr><br>
			<tr class="common"><td class="title">险种编码：理赔案件对应的险种编码，如果“险种类型”选择了个险，则险种编码查询出符合条件的所有个险险种编码，如“险种类型”为空，则查询的是全部险种</td></tr><br>
			<tr class="common"><td class="title">保单号：&nbsp;&nbsp;理赔案件对应的保单号</td></tr><br>
			<tr class="common"><td class="title">批次号：&nbsp;&nbsp;案件所属批次号</td></tr><br>
			<tr class="common"><td class="title">理算起期：统计的最后一次理算确认时间起期</td></tr><br>
			<tr class="common"><td class="title">理算止期：统计的最后一次理算确认时间止期</td></tr><br> 
			
		</Div>



	<Input type="hidden" class= common name="RiskRate" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 