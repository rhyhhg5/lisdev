<%
/*******************************************************************************
 * Name     :SecondUWInit.jsp
 * Function :“案件审核”的“二次核保”的初始化程序
 * Date     :2003-08-01
 * Author   :LiuYansong
 */
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
function initInpBox( )
{
  try
  {
    fm.all('UWRegister').value = "";
  }
  catch(ex)
  {
    alert("在SecondUWInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSecondUWGrid();
    showSecondUWInfo();
  }
  catch(re)
  {
    alert("SecondUWInit.jsp-->initForm函数中发生异常:初始化界面错误!");
  }
}
//初始化保单的信息
function initSecondUWGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号码";    	//列名
    iArray[1][1]="150px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="险种名称";         			//列名
    iArray[2][1]="200px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="险种代码";         			//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="投保人";         		//列名
    iArray[4][1]="180px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;

    iArray[5]= new Array();
    iArray[5][0]="被保险人";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=0;

    iArray[6]= new Array();
    iArray[6][0]="保险金额";
    iArray[6][1]="100px";
    iArray[6][2]=100;
    iArray[6][3]=0;

    iArray[7]= new Array();
    iArray[7][0]="份数";
    iArray[7][1]="30px";
    iArray[7][2]=100;
    iArray[7][3]=0;


    iArray[8]=new Array();
    iArray[8][0]="事故者类型";         		//列名
    iArray[8][1]="180px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

    SecondUWGrid = new MulLineEnter( "fm" , "SecondUWGrid" );
    //这些属性必须在loadMulLine前
    SecondUWGrid.mulLineCount = 0;
    SecondUWGrid.displayTitle = 1;
    SecondUWGrid.canChk = 1;
    SecondUWGrid.locked = 1;
    SecondUWGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>