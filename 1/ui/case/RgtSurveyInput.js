//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();

//#1738 社保调查，普通商团案件查询当前待处理人
function QuerySTDealer(CaseNo,Surveyer)
{
	var RgtStateSql = "select rgtstate from llcase where caseno='"+CaseNo+"'";
	var RgtStateAry = easyExecSql(RgtStateSql);
	if(!RgtStateAry){
		//咨询通知类案件无案件状态
		return true;
	}
	var RgtState = RgtStateAry[0][0];
	var strSQL="";
	switch (RgtState)
	{				
		case "02":
		case "03":
		case "04":
		case "06":
		strSQL="select Handler,(select username from lduser where usercode=Handler) from llcase where caseno='"+CaseNo+"' with ur" ;//案件处理人
		break;
		case "13":
		case "01":
		case "09":
		case "11":
		case "12":
		case "14":
		strSQL="select Rigister,(select username from lduser where usercode=Rigister) from llcase where caseno='"+CaseNo+"' with ur" ;//案件受理人
		break;
		case "07":
		strSQL="select case when inqper is null then Dipatcher else inqper end,(select username from lduser where usercode=case when inqper is null then Dipatcher else inqper end) from LLInqApply where otherno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;
		break;
		case "08":
		strSQL="select StartMan,(select username from lduser where usercode=StartMan) from llsurvey where otherno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;//案件处理人
		break;
		case "05":
		case "10":
		strSQL="select AppClmUWer,(select username from lduser where usercode=AppClmUWer) from LLClaimUWMain where caseno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;//案件处理人
		break;
		
	}
	var arrResult = easyExecSql(strSQL);	
	if(arrResult)
	{
		if(Surveyer != arrResult[0][0]){
			alert("案件待处理人："+arrResult[0][0]+"("+arrResult[0][1]+");登录用户与案件待处理人不符！");					
			return false;
		}
	}	
	return true;
}
//#1738 社保调查，社保案件查询当前待处理人，此处逻辑暂时与商团险一致，日后可能会调整此处逻辑
function QuerySBDealer(CaseNo,Surveyer)
{
	var RgtStateSql = "select rgtstate from llcase where caseno='"+CaseNo+"'";
	var RgtStateAry = easyExecSql(RgtStateSql);
	if(!RgtStateAry){
		//咨询通知类案件无案件状态
		return true;
	}
	var RgtState = RgtStateAry[0][0];
	var strSQL="";
	switch (RgtState)
	{				
		case "02":
		case "03":
		case "04":
		case "06":
		strSQL="select Handler,(select username from lduser where usercode=Handler) from llcase where caseno='"+CaseNo+"' with ur" ;//案件处理人
		break;
		case "13":
		case "01":
		case "09":
		case "11":
		case "12":
		case "14":
		strSQL="select Rigister,(select username from lduser where usercode=Rigister) from llcase where caseno='"+CaseNo+"' with ur" ;//案件受理人
		break;
		case "07":
		strSQL="select case when inqper is null then Dipatcher else inqper end,(select username from lduser where usercode=case when inqper is null then Dipatcher else inqper end) from LLInqApply where otherno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;
		break;
		case "08":
		strSQL="select StartMan,(select username from lduser where usercode=StartMan) from llsurvey where otherno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;//案件处理人
		break;
		case "05":
		case "10":
		strSQL="select AppClmUWer,(select username from lduser where usercode=AppClmUWer) from LLClaimUWMain where caseno='"+CaseNo+"' order by makedate,maketime desc fetch first 1 rows only with ur" ;//案件处理人
		break;
		
	}
	var arrResult = easyExecSql(strSQL);	
	if(arrResult)
	{
		if(Surveyer != arrResult[0][0]){
			alert("社保案件待处理人："+arrResult[0][0]+"("+arrResult[0][1]+");登录用户与案件待处理人不符！");					
			return false;
		}
	}	
	return true;
}

//提交，保存按钮对应操作
function submitForm(){
	if(fm.Content.value==''){
		alert("调查内容不能为空!");
		return false;
	}
	if(fm.InsuredNo.value==""||fm.InsuredNo.value==null){
		alert("客户号码不能为空!");
		return false;
	}
	var tCaseNo = fm.CaseNo.value;
	var tSurveyer = fm.Surveyer.value;
	//1.判断是否社保案件
	 //此处判断需要支持社保的申诉纠错案件
     if(fm.CaseNo.value.substring(0,1) == 'R' || fm.CaseNo.value.substring(0,1) == 'S')
	 {
	    var SQLCaseNo = "select caseno from llcase where caseno=(select distinct caseno from LLAppeal where appealno ='"+fm.CaseNo.value+"')";
  	    var JCCaseNo = easyExecSql(SQLCaseNo);
  	    tCaseNo = JCCaseNo[0][0];
	 }
	 var sql1 = "Select CHECKGRPCONT(rgtobjno) from llregister where rgtno =(select rgtno from llcase where caseno = '"+tCaseNo+"') with ur";   
     var tarr1 = easyExecSql(sql1);   
     if(tarr1){
        if(tarr1[0][0]=='Y')
       	{
       		if(!QuerySBDealer(tCaseNo,tSurveyer)){
				return false;
			}
       	}else{
       			if(!QuerySTDealer(tCaseNo,tSurveyer)){
					return false;
				}
       	}
     }else{
     	if(!QuerySTDealer(tCaseNo,tSurveyer)){
			return false;
		}
     }

	var i = 0;
	fm.fmtransact.value="INSERT||MAIN"
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	showSubmitFrame(mDebug);
	fm.action = './RgtSurveySave.jsp';
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
	showInfo.close();
	if (FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	EasyQuery();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm(){
	try{
		initForm();
	}
	catch(re){
		alert("在RgtSurveyInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//取消按钮对应操作
function cancelForm(){
	showDiv(operateButton,"true");
	showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick(){
	if ((fm.SurveyFlag.value==1)||(fm.SurveyFlag.value==2)){
		alert ("您不能对本次调查报告进行删除！");
		return;
	}
	else{
		if (confirm("您确实要删除该记录吗？")){
			var i = 0;
			var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或连接其他的界面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			showSubmitFrame(mDebug);
			fm.action = './RgtSurveyDelete.jsp';
			fm.submit();//提交
			resetForm();
		}
		else{
			mOperate="";
			alert("您已经取消了修改操作！");
		}
	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function SurveyFee(){
}

function EasyQuery(){
	//对于rgtstate=2的情况，属于历史数据，目前‘咨询通知’案件的调查的rgtstate=1
	//对于SurveyClass，系统中不存在‘自动提调’
	var sql ="select a.otherno,substr(a.surveyno,18),b.MngCom,b.startman,b.makedate,b.surveyenddate,"+
	" case b.RgtState when '2' then '咨询通知' when '0' then '受理状态' when '1' then '检录状态' else CodeName('llrgtstate',b.RgtState) end,"+
	" a.InqNo,a.inqper,case b.SurveyClass when '0' then '即时提调' when '1' then '一般提调' else '自动提调' end,"+
	" a.Surveyno,b.surveysite from llinqapply a,llsurvey b where b.surveyno=a.surveyno and  b.customerno='"+fm.all('InsuredNo').value+"'";
	turnPage.pageLineNum = 20;
	turnPage.queryModal(sql,PastGrid);
}

function ShowContent(){
	var selno = PastGrid.getSelNo();
	//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = PastGrid.getRowColData(selno-1,1);
	var SurveyNo = PastGrid.getRowColData(selno-1,11);

	var strSQL="select Content,result,ohresult,ConfNote,SurveyFlag,SurveySite,otherno,SurveyClass,startman,surveysite from LLSurvey where surveyno='"+SurveyNo+"'";
	var arrResult = easyExecSql(strSQL);

	if(arrResult != null){
	    if(arrResult[0][5]==null){
	    arrResult[0][5] = '';
	    }
	    if(arrResult[0][0]==null){
	        arrResult[0][0]='';
	    }
		fm.Content.value=arrResult[0][0];
		if(arrResult[0][1]==null){
		arrResult[0][1]='';
		}
		fm.Result.value=arrResult[0][1];
		if(arrResult[0][2]==null){
		    arrResult[0][2]='';
		}
		fm.resultOut.value=arrResult[0][2];
		if(arrResult[0][3]==null){
		    arrResult[0][3]='';
		}
		fm.ConfNote.value=arrResult[0][3];
		fm.CasePay.value=arrResult[0][5];

		if(arrResult[0][4] =="3"){
			fm.UWFlag.value=0;
			fm.UWFlagName.value="通过";
			}
		else{
			fm.UWFlag.value=1;
			fm.UWFlagName.value="继续调查";
		}
		
		if (arrResult[0][6]==null) {
			arrResult[0][6] = '';
		}
		fm.CaseNo.value = arrResult[0][6];
		
		strCountSQL="select count(*) from llsurvey where otherno='"+fm.CaseNo.value+"'";
		var arrCount=easyExecSql(strCountSQL);
		if(arrCount!= null){
			fm.all('SerialNo').value=arrCount[0][0];
		}
		
		if (arrResult[0][7]==null) {
			arrResult[0][7] = '';
		}
		if (arrResult[0][7]=='0') {
			fm.SurvType.value = '0';
			fm.SurvTypeName.value = '即时调查';
	  } else {
	  	fm.SurvType.value = '1';
			fm.SurvTypeName.value = '一般调查';
	  }
		
		if (arrResult[0][8]==null) {
			arrResult[0][8] = '';
		}
		fm.Surveyer.value = arrResult[0][8];
	}
}

