var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";
//提交，保存按钮对应操作
function submitForm()
{

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 
    if (fm.fmtransact.value=="DELETE||MAIN")
    {
    	fm.reset();
    	initCustomerGrid();
    }
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
 function addClick()
{

  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (confirm("您确实想修改该记录吗?"))
 	{
 		
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //showSubmitFrame(mDebug);
      fm.fmtransact.value = "UPDATE||MAIN"
      fm.submit(); //提交
    }//end of else
 
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{

  window.open("./LLMainAskQuery.html");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function deleteClick()
{
	  if ( fm.LogNo.value=="")
      {
      	alert("请先查询数据");
      	return false;
    }
    
	if (confirm("删除记录会删除所有咨询记录，您确实想删除该记录吗?"))
 	{	
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      showSubmitFrame(mDebug);
      fm.fmtransact.value = "DELETE||MAIN"
      fm.submit(); //提交
 }//end of else 
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function showCustomerInfo()
{
	window.open("../sys/FrameCQPersonQuery.jsp");
}

function afterCodeSelect( cCodeName, Field ) {
}

function afterQuery(arr)
{
}


function SurveyReplySave()
{  if(fm.result.value.trim()==""&&fm.OHresult.value.trim()=="")
    {
    	alert("必须输入回复内容！");
    	return;
    }

    for (var n=0;n<AffixGrid.mulLineCount;n++) {
    	if (!isInteger(AffixGrid.getRowColData(n,3))) {
    		alert("调查材料件数需为数字");
    		return false;
    	}
    }

	  var i = 0;
	  fm.fmtransact.value="INSERT||REPLY";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLSurveyReplySave.jsp";
  	fm.submit();
}
function CloseWindows()
{
	top.close();
}

function easyQuery()
{
   var strSQL = "SELECT CerType,remark,cercount from llinqcertificate where surveyno='"+fm.SurveyNo.value+"'";
   turnPage.queryModal(strSQL,AffixGrid);
}
