//               该文件中包含客户端需要处理的函数和事件


var showInfo;
var mDebug="1";

//TODO：提交，保存按钮对应操作
function submitForm()
{
  var sql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  var rgtstate=easyExecSql(sql);
  if (rgtstate!=null){
     if(rgtstate[0][0]=='07'){
       alert("调查状态不能撤件，必须先进行调查回复！");
       return false;
     }
     if(rgtstate[0][0]=='16'){
       alert("理赔二核中不能撤件，必须等待二核结束！");
       return false;
     }
  }
  
	if(fm.CancleReason.value=='')
	{
		alert("撤件原因不能为空");
		return false;
	}
	
  var sql = "select 1 from LLHospcase where caseno in(select caseno from llcase where rgtno ='"+fm.CaseNo.value+"' and rgttype != '8' ) and claimno is not null ";
  var Hospcase = easyExecSql(sql);
  if (Hospcase!=null && Hospcase!= "null" && Hospcase != ""){
       alert("医保通案件不允许撤件");
       return false;
  }
	
  	fm.fmtransact.value = "CASECANCEL";
  	if(fm.LoadFlag.value=='2')
  		fm.fmtransact.value = "RGTCANCEL";
    var showStr="正在执行撤件，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./CaseCancel.jsp";
    fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
     parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	alert("您不能执行修改操作");
	return;
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var varSrc = "&ClmCaseNo=" + fm.CaseNo.value;
  varSrc += "&Type=1";
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  var newWindow = window.open("./RgtSurveyQuery1.jsp?Interface=RgtSurveyQuery.jsp"+varSrc,"RgtSurveyQuery",'toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{

}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function submitForm1()
{
	if (fm.RptObjNo.value=="")
	{
		alert("请您收入要查询的号码类型和号码！");
 		return ;
  }
  else
  {
  	var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	initSubReportGrid();
  	fm.action = './ReportQueryOut1.jsp';
  	fm.submit(); //提交
  }
}
//完成按照”客户号“在lcpol中进行查询，显示该客户的保单明细
function showInsuredLCPol()
{
	var varInsuredNo;
	var varCount;
	varCount = SubReportGrid.getSelNo();
	varInsuredNo=SubReportGrid.getRowColData(varCount-1,1);
	if ((varInsuredNo=="null")||(varInsuredNo==""))
	{
		alert("客户号为空，不能进行查询操作！");
		return;
	}

	if (varCount==0)
    {
    		alert("请您选择一个客户号！");
    		return;
    }

	var varSrc = "&InsuredNo=" + SubReportGrid.getRowColData(SubReportGrid.getSelNo()-1,1);
	var newWindow = window.open("./FrameMainReportLCPol.jsp?Interface=ReportLCPolInput.jsp"+varSrc,"ReportLCPolInput");
}
