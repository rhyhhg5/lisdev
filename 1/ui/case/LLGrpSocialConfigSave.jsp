<%
//程序名称：BlackListInput.jsp
//程序功能：
//创建日期：2003-01-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  

  LLAppClaimReasonSet tLLAppClaimReasonSet   = new LLAppClaimReasonSet();
  LLAppClaimReasonSchema tLLAppClaimReasonSchema   = new LLAppClaimReasonSchema();
  LLGrpSocialConfigBL tLLGrpSocialConfigBL = new LLGrpSocialConfigBL();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operate");
  tOperate=tOperate.trim();               
  String FlagStr = "Fail";
  String Content = "";
  String operator="";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  
  if("insert".equals(tOperate))
  {
	  operator=tG.Operator;
	  String grpContNo=request.getParameter("grpContNo");

	  tLLAppClaimReasonSchema.setRgtNo(grpContNo);
	  tLLAppClaimReasonSchema.setOperator(operator);
	  tLLAppClaimReasonSchema.setCaseNo("社保保单");
	  tLLAppClaimReasonSchema.setReasonCode("SB");
	  tLLAppClaimReasonSchema.setReasonType("8");
	  tLLAppClaimReasonSchema.setCustomerNo("88888888");

	  FlagStr="";
	  tVData.addElement(tLLAppClaimReasonSchema);
	  tVData.addElement(tLLAppClaimReasonSet);
	  tVData.addElement(tG);
	  try
	  {
		  tLLGrpSocialConfigBL.submitData(tVData,tOperate);
	  }
	  catch(Exception ex)
	  {
	    Content = "操作失败，原因是:" + ex.toString();
	    System.out.println(Content);
	    FlagStr = "Fail";
	  }
  }
  if("del".equals(tOperate))
  {
	  operator=tG.Operator;
	  String grpContNo[]=request.getParameterValues("LLAppClaimReasonGrid1");
	  String mngcom[]=request.getParameterValues("LLAppClaimReasonGrid2");
	  String operators[]=request.getParameterValues("LLAppClaimReasonGrid3");
	  String makedate[]=request.getParameterValues("LLAppClaimReasonGrid4");
	  String maketime[]=request.getParameterValues("LLAppClaimReasonGrid5");
	  String modifydate[]=request.getParameterValues("LLAppClaimReasonGrid6");
	  String modifytime[]=request.getParameterValues("LLAppClaimReasonGrid7");
	  String tChk[] = request.getParameterValues("InpLLAppClaimReasonGridChk"); 
	  if(tChk!=null)
	  {
		for(int i=0;i<tChk.length;i++)
		{
		  if(tChk[i].equals("1"))
		  {
			  LLAppClaimReasonSchema t_LLAppClaimReasonSchema   = new LLAppClaimReasonSchema();
			  t_LLAppClaimReasonSchema.setRgtNo(grpContNo[i]);
			  t_LLAppClaimReasonSchema.setMngCom(mngcom[i]);
			  t_LLAppClaimReasonSchema.setOperator(operators[i]);
			  t_LLAppClaimReasonSchema.setMakeDate(makedate[i]);
			  t_LLAppClaimReasonSchema.setMakeTime(maketime[i]);
			  t_LLAppClaimReasonSchema.setModifyDate(modifydate[i]);
			  t_LLAppClaimReasonSchema.setModifyTime(modifytime[i]);
			  t_LLAppClaimReasonSchema.setCaseNo("社保保单");
			  t_LLAppClaimReasonSchema.setReasonType("8");
			  t_LLAppClaimReasonSchema.setReasonCode("SB");
			  System.out.println("循环进行第"+i+"次  tLLAppClaimReasonSchema.getRgtNo:"+t_LLAppClaimReasonSchema.getRgtNo());			    
			  tLLAppClaimReasonSet.add(t_LLAppClaimReasonSchema);
		  }
	    }
	    FlagStr="";
	    tVData.addElement(tLLAppClaimReasonSchema);
		tVData.addElement(tLLAppClaimReasonSet);
		System.out.println("---->>"+tLLAppClaimReasonSet.size());
		tVData.addElement(tG);
	    try
	    {
	    	tLLGrpSocialConfigBL.submitData(tVData,tOperate);
	    }catch(Exception ex)
	    {
	      Content = "操作失败，原因是:" + ex.toString();
	      System.out.println(Content);
	      FlagStr = "Fail";
	    } 
	  }
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLLGrpSocialConfigBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

