var turnPage = new turnPageClass();  
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
//	var month = fm.MEndDate.value;
//	var year = fm.YEndDate.value;
	
//	fm.StartDate.value = fm.YStartDate.value+"-"+fm.MStartDate.value+"-1";
//	fm.EndDate.value = year+"-"+month+"-"+getDays(month, year);
	
//	fm.MngName.value = easyExecSql("select Name from ldcom where comcode = '"+fm.MngCom.value+"'");
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
//  fm.action = "LLClaimCollectionRpt.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "LLClaimCollection";
	fm.submit();
	showInfo.close();
}
                                               
function GrpTypeSearch() {
	var strSQL = "select code,codename from ldcode where codetype = 'llgrptype' order by code with ur";
	turnPage.queryModal(strSQL,BusinessGrid);
}

function GrpTypeSave(operate) {
	//var Count=BusinessGrid.mulLineCount;
  //if(Count==0){
  //	alert("没有业务类型信息!");
  //	return false;
  //}
  var selNo = BusinessGrid.getSelNo();
  if ( selNo<1 ) {
  	alert("请选择重点业务");
  	return false;
  }

  	if (BusinessGrid.getRowColData(selNo-1,2)==null
  	    ||BusinessGrid.getRowColData(selNo-1,2)==""){
  	    	alert("重点业务名称不能为空！");
  	    	return false;
    }

	
	if (operate==1){
		
		var strSQL = "select 1 from ldcode where "+
		"codetype='llgrptype' and code='"+BusinessGrid.getRowColData(selNo-1,1)+"'";
		//alert(strSQL);
		var arrResult = easyExecSql(strSQL);
		if(arrResult != null) {
			alert("已经有该重点业务！");
			return false;
		}
		fm.fmtransact.value = "INSERT";
	} else if (operate==2) {
		var strSQL = "select 1 from ldcode1 where codetype='llgrptype' and code in (select code from ldcode where "+
		"codetype='llgrptype' and code='"+BusinessGrid.getRowColData(selNo-1,1)+"')";
		//alert(strSQL);
		var arrResult = easyExecSql(strSQL);
		if(arrResult != null) {
			alert("该重点业务下还有保单，请先删除保单！");
			return false;
		}
		fm.fmtransact.value = "DELETE";
	}
	else if(operate==3) {
		fm.fmtransact.value = "UPDATE";
	}
	else
		alert('未知操作');
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "LLClaimCollecionTypeSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();
	//alert('hello');
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  if(FlagStr!=null&&FlagStr=="Success1")
  	GrpTypeSearch();
}

function GrpContSearch() {
	var strSQL = "select a.codename,a.code,b.code1,b.codename from ldcode a,ldcode1 b "
	           + " where a.codetype=b.codetype and b.codetype='llgrptype' "
	           + " and a.code=b.code "
	           + getWherePart("b.code","GrpType")
	           + getWherePart("b.code1","ContNo")
	           + " with ur ";

	turnPage.queryModal(strSQL,BusinessGrpContGrid);
}

function GrpContTypeSave(operate) {
	var Count=BusinessGrpContGrid.mulLineCount;
  if(Count==0){
  	alert("没有保单信息!");
  	return false;
  }
  
  var selNo = BusinessGrpContGrid.getSelNo();
  if ( selNo<1 ) {
  	alert("请选择保单");
  	return false;
  }
  if (BusinessGrpContGrid.getRowColData(selNo-1,2)==null
      ||BusinessGrpContGrid.getRowColData(selNo-1,2)=="") {
      	alert("请选择业务类型");
      	return false;
  }
  
  var tGrpContNo = BusinessGrpContGrid.getRowColData(selNo-1,3);
  if (tGrpContNo==null ||tGrpContNo=="") {
      	alert("请输入保单号");
      	return false;
  }
	
	var strSQL = "select grpname from lcgrpcont where grpcontno = '"+tGrpContNo
	           + "' union select grpname from lbgrpcont where grpcontno = '"+tGrpContNo+"'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult == null) {
		alert("保单查询失败");
		return false;
	} else {
		BusinessGrpContGrid.setRowColData(selNo-1,4,arrResult[0][0]);
	}
		
	if (operate==1){
		fm.fmtransact.value = "DELETE&INSERT";
	} else if (operate==2) {
		fm.fmtransact.value = "DELETE";
	}
	else if(operate==3)
	{
		fm.fmtransact.value = "UPDATE";
	}
	else
	{
		alert("GrpContTypeSave没有参数");
		return;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.action = "LLClaimCollecionGrpTypeSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();
}

function SelectBusinessGrpContGrid()
{
	var selNo = BusinessGrpContGrid.getSelNo();
  	if ( selNo<1 ) {
  		alert("请选择保单");
  		return false;
  	}
	fm.zdywlx.value=BusinessGrpContGrid.getRowColData(selNo-1,2)
	fm.grpcontno.value=BusinessGrpContGrid.getRowColData(selNo-1,3)
//	fm.zdywlx.value=BusinessGrpContGrid.getRowColData(selNo-1,4)
}
function SelectBusinessGrid()
{
	var selNo = BusinessGrid.getSelNo();
  	if ( selNo<1 ) {
  		alert("请选择重点业务");
  		return false;
  	}
	fm.zdywcode.value=BusinessGrid.getRowColData(selNo-1,1)
}

