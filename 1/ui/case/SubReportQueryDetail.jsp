<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：SubReportQuery.jsp
//程序功能：查询分报案信息
//创建日期：2002-08-13 11:10:36
//创建人  ：lixy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	System.out.println("---hahahahah---");
  // 保单信息部分
  LLSubReportSchema tLLSubReportSchema   = new LLSubReportSchema();
  
  tLLSubReportSchema.setSubRptNo(request.getParameter("SubRptNo"));


    
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLLSubReportSchema);

  // 数据传输
  SubReportUI tSubReportUI   = new SubReportUI();
	if (!tSubReportUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tSubReportUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tSubReportUI.getResult();
		
		// 显示
		LLSubReportSet mLLSubReportSet = new LLSubReportSet(); 
		mLLSubReportSet.set((LLSubReportSet)tVData.getObjectByObjectName("LLSubReportBLSet",0));
		int n = mLLSubReportSet.size();
		System.out.println("get report "+n);
		LLSubReportSchema mLLSubReportSchema = mLLSubReportSet.get(1);
		%>
		<script language="javascript">
		   top.opener.fm.SubReportNo.value="<%=mLLSubReportSchema.getSubRptNo()%>";
		   top.opener.fm.CustomerNo.value="<%=mLLSubReportSchema.getCustomerNo()%>";
		   top.opener.fm.CustomerName.value="<%=mLLSubReportSchema.getCustomerName()%>";
		   top.opener.emptyUndefined();
		</script>
			<%
		 // end of for
	} // end of else
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tSubReportUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	top.close();
</script>
</html>

