<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalPrintSave.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//修改人  ：朱向峰
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String FlagStr = "";
String Content = "";
String tOperate = "";
String customerno = "";
String addressno = "";
//获得mutline中的数据信息
int row = 0;
String tRadio[] = request.getParameterValues("InpContGridSel");  
row = Integer.parseInt(request.getParameter("Opt"));
System.out.println("row----------------"+row);

String tInsurdNo[] = request.getParameterValues("ContGrid3");
String tPrtNo[] = request.getParameterValues("ContGrid2");
if(tInsurdNo.length<0){
	System.out.println("被保人号获取失败!");
	Content="被保人号获取失败!";
	FlagStr="Fail";
}
if(tPrtNo.length<0){
	System.out.println("印刷号获取失败!");
	Content="印刷号获取失败!";
	FlagStr="Fail";
}
String email = request.getParameter("email");
System.out.println("tInsurdNo[] = " + tInsurdNo[row - 1]);
System.out.println("tPrtNo[] = " + tPrtNo[row - 1]);
System.out.println("email = " + email);

//获得session中的人员喜讯你
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//更新Email
String addressnosql = "select lcad.customerno,lcad.addressno from lcaddress lcad,lccont lcc,Lcappnt lcap where lcad.customerno = lcc.appntno and lcap.addressno = lcad.addressno and lcap.contno = lcc.contno and lcc.prtno in ('" + tPrtNo[row-1] + "')";
SSRS tSSRS = new ExeSQL().execSQL(addressnosql);

customerno=tSSRS.GetText(1,1);
if("".equals(customerno)||null==customerno){
	System.out.println("客户号获取失败!");
	Content="客户号获取失败!";
	FlagStr="Fail";
}else{
	addressno=tSSRS.GetText(1,2);
	if("".equals(addressno)||null==addressno){
		System.out.println("地址号获取失败!");
		Content="地址号获取失败!";
		FlagStr="Fail";
	}else{
		String sql = "update lcaddress set email = '"+ email +"' where customerno = '" + tInsurdNo[row - 1] + "' and addressno = '" + addressno + "'";
		MMap tmap = new MMap();
		tmap.put(sql, "UPDATE");
		VData mInputData = new VData();
		mInputData.clear();
		mInputData.add(tmap);
		PubSubmit tPubSubmit = new PubSubmit();
		if(!tPubSubmit.submitData(mInputData, "UPDATE")){
			System.out.println("数据出错");
			FlagStr="Fail";
			Content="处理失败!";
		}else{
			FlagStr="true";
			Content="处理成功!";
		}
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=customerno%>","<%=addressno%>");
</script>
</html>