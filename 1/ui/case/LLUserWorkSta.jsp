<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LLUserWorkSta.jsp
//程序功能：
//创建日期：2005-04-17
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LLUserWorkSta.js"></SCRIPT> 
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<script>
	function initDate(){
   fm.StartDate.value="<%=afterdate%>";
   fm.EndDate.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
  }
</script>
</head>

<body onload="initElementtype();initDate();">    
  <form action="" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title>管理机构</TD><TD  class= input>
    			<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
       
          <TD  class= title>统计起期</TD>
          <TD  class= input> 
          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> 
          </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> 
          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> 
          </TD> 
        </TR>
        <TR  class= common>
		      <TD  class= title>案件类型</TD>
          <TD  class= input> <input class="codeno" CodeData="0|2^1|普通案件^2|批量处理案件"  name=CaseType ondblclick="return showCodeListEx('CaseType',[this,CaseTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CaseType',[this,CaseTypeName],[0,1]);"><input class=codename name=CaseTypeName></TD> 
        </TR>
        <TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input> <input class="codeno" value="3" CodeData="0|3^1|商业保险^2|社会保险^3|全部"  verify="业务类型|notnull"  name=StatsType ondblclick="return showCodeListEx('StatsType',[this,StatsTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StatsType',[this,StatsTypeName],[0,1]);"><input class=codename value="全部" name=StatsTypeName></TD> 
        </TR>
    </table>

    <hr>

    <input type="hidden" name=op value="">
    <input type="hidden" name="Operator" value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title>统   计</TD>
					<TD  class= input8> 
						<input class=codeno CodeData="0|6^0|用户工作量统计^1|分组用户工作量统计"  name=DealWith 
							ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" 
							onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input class=codename name=DealWithName>
					</TD>
          <TD  class= title> </TD>
          <TD  class= input> <Input name=td0 class=readonly readonly > </TD> 
          <TD  class= title> </TD>
          <TD  class= input> <Input name=td1 class=readonly readonly> </TD> 
			</tr>
		</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 