<%
//Name:ReportInit.jsp
//function：理赔二核
//author:Xx
//Date:2005-08-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
 GlobalInput tG = (GlobalInput)session.getValue("GI");
 String tCaseNo = "";
 String tInsuredNo = "";

 tCaseNo = request.getParameter("CaseNo");
 if(tCaseNo.equals("null"))
  tCaseNo="";
 tInsuredNo += request.getParameter("InsuredNo");
 if(tInsuredNo.equals("null"))
  tInsuredNo="";
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.CaseNo.value = "<%=tCaseNo%>";
  	fm.InsuredNo.value = "<%=tInsuredNo%>";
  	fm.Operator.value = "<%=tG.Operator%>";
  	fm.CurrentOperator.value = "<%=tG.Operator%>";
  }
  catch(ex)
  {
    alter("在LLContDealInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLContDealInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initContDealGrid();
    initContGrid();
    initPolGrid();
    initEdorItemGrid();
        
    DealContSearch();
    ContSearch();
  }
  catch(re)
  {
    alter("在LLContDealInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initContDealGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="保单号";
    iArray[1][1]="60px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    iArray[1][21]="contno";
    
    iArray[2]=new Array();
    iArray[2][0]="合同处理号";
    iArray[2][1]="60px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][21]="EdorAcceptNo";
    
    iArray[3]=new Array();
    iArray[3][0]="客户号";
    iArray[3][1]="40px";
    iArray[3][2]=80;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="客户名";
    iArray[4][1]="60px";
    iArray[4][2]=100;
    iArray[4][3]=1;

    iArray[5]=new Array();
    iArray[5][0]="经办人";
    iArray[5][1]="60px";
    iArray[5][2]=100;
    iArray[5][3]=1;
    iArray[5][21]="operator";
    
    iArray[6]=new Array();
    iArray[6][0]="案件号";
    iArray[6][1]="60px";
    iArray[6][2]=100;
    iArray[6][3]=3;
    
    iArray[7]=new Array();
    iArray[7][0]="申请日期";
    iArray[7][1]="60px";
    iArray[7][2]=100;
    iArray[7][3]=3;
    iArray[7][21]="Edorappdate";

    ContDealGrid = new MulLineEnter("fm","ContDealGrid");
    ContDealGrid.mulLineCount =5;
    ContDealGrid.displayTitle = 1;
    ContDealGrid.locked = 1;
    ContDealGrid.canSel =1
    ContDealGrid.canChk = 0;;
    ContDealGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ContDealGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    ContDealGrid.selBoxEventFuncName = "onSelContDeal";
    ContDealGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initContGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="保单号";
	  iArray[1][1]="120px";
	  iArray[1][2]=100;
	  iArray[1][3]=0;
	  iArray[1][21]='contno';
	  
	  iArray[2]=new Array();
	  iArray[2][0]="状态";
	  iArray[2][1]="50px";
	  iArray[2][2]=100;
	  iArray[2][3]=3;  
	  iArray[2][21]='state';
	  
	  iArray[3]=new Array();
	  iArray[3][0]="投保人";  
	  iArray[3][1]="100px";
	  iArray[3][2]=100;
	  iArray[3][3]=3;
	  iArray[3][21]='AppntNo';
	  
	  iArray[4]=new Array();
	  iArray[4][0]="主被保险人";
	  iArray[4][1]="100px";
	  iArray[4][2]=100;
	  iArray[4][3]=0;
	  
	  iArray[5]=new Array();
	  iArray[5][0]="生效日期";
	  iArray[5][1]="100px";
	  iArray[5][2]=100;
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array();
	  iArray[6][0]="交至日期";
	  iArray[6][1]="100px";
	  iArray[6][2]=100;
	  iArray[6][3]=0;
	  
	  iArray[7]=new Array();
	  iArray[7][0]="交费间隔";
	  iArray[7][1]="80px";
	  iArray[7][2]=100;
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array();
	  iArray[8][0]="期交保费";
	  iArray[8][1]="80px";
	  iArray[8][2]=100;
	  iArray[8][3]=0;
	  
	  iArray[9]=new Array();
	  iArray[9][0]="业务员代码";
	  iArray[9][1]="80px";
	  iArray[9][2]=100;
	  iArray[9][3]=0;
	  
	  iArray[10]=new Array();
	  iArray[10][0]="集体合同号";
	  iArray[10][1]="80px";
	  iArray[10][2]=100;
	  iArray[10][3]=3;
	  
	  iArray[11]=new Array();
	  iArray[11][0]="业务员姓名";
	  iArray[11][1]="70px"; 
	  iArray[11][2]=100;
	  iArray[11][3]=0;
	  
	  iArray[12]=new Array();
	  iArray[12][0]="保单状态";
	  iArray[12][1]="70px"; 
	  iArray[12][2]=100;
	  iArray[12][3]=0;
	  
	  iArray[13]=new Array();
	  iArray[13][0]="保单冻结状态";
	  iArray[13][1]="100px"; 
	  iArray[13][2]=100;
	  iArray[13][3]=0;
    
    ContGrid = new MulLineEnter("fm","ContGrid");
    ContGrid.mulLineCount =5;
    ContGrid.displayTitle = 1;
    ContGrid.locked = 1;
    ContGrid.canSel =1
    ContGrid.canChk = 0;;
    ContGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ContGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    ContGrid.selBoxEventFuncName = "onSelCont";
    ContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initPolGrid()
{                               
	var iArray = new Array();
	try
	{
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="0px";            		//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="险种序号";
	  iArray[1][1]="50px"; 
	  iArray[1][2]=30;
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array();
	  iArray[2][0]="被保险人号码";
	  iArray[2][1]="80px";
	  iArray[2][2]=100;
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array();
	  iArray[3][0]="被保险人姓名";
	  iArray[3][1]="80px";
	  iArray[3][2]=100;
	  iArray[3][3]=0;
	
	  iArray[4]=new Array();
	  iArray[4][0]="险种代码";
	  iArray[4][1]="50px";
	  iArray[4][2]=100;
	  iArray[4][3]=0; 
	  
	  iArray[5]=new Array();
	  iArray[5][0]="险种名称";
	  iArray[5][1]="180px";
	  iArray[5][2]=100;
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array();
	  iArray[6][0]="保额/份数";
	  iArray[6][1]="80px";
	  iArray[6][2]=100;
	  iArray[6][3]=0;
	  

    iArray[7]=new Array();
	  iArray[7][0]="档次";
	  iArray[7][1]="80px";
	  iArray[7][2]=100;
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array();
	  iArray[8][0]="期交保费";
	  iArray[8][1]="80px";
	  iArray[8][2]=100;
	  iArray[8][3]=0;        
	  
	  iArray[9]=new Array();
	  iArray[9][0]="生效日期"
	  iArray[9][1]="80px";
	  iArray[9][2]=100;
	  iArray[9][3]=0;
	  
	  iArray[10]=new Array();
	  iArray[10][0]="交至日期";
	  iArray[10][1]="80px";
	  iArray[10][2]=100;
	  iArray[10][3]=0;
	  
	  iArray[11]=new Array();
	  iArray[11][0]="保单号码";
	  iArray[11][1]="80px";
	  iArray[11][2]=100;
	  iArray[11][3]=3;
	  
	  iArray[12]=new Array();
	  iArray[12][0]="集体保单号码";
	  iArray[12][1]="100px";
	  iArray[12][2]=100;
	  iArray[12][3]=3;
	  
	  iArray[13]=new Array();
	  iArray[13][0]="险种状态";
	  iArray[13][1]="70px";
	  iArray[13][2]=100;
	  iArray[13][3]=0;
	  
	  iArray[14]=new Array();
	  iArray[14][0]="被保人出生日期";
	  iArray[14][1]="100px";
	  iArray[14][2]=100;
	  iArray[14][3]=3;
	  iArray[14][21]="InsuredBirthDay";
	  
		PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
		//这些属性必须在loadMulLine前
		PolGrid.mulLineCount = 0;   
		PolGrid.displayTitle = 1;
		//PolGrid.canChk=1;
		PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		PolGrid.hiddenSubtraction=1;
		PolGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}       

function initEdorItemGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="合同处理受理号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="批单号";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=3;
    
    iArray[3]=new Array();
    iArray[3][0]="合同处理类型";
    iArray[3][1]="80px";
    iArray[3][2]=100;
    iArray[3][3]=0;   
    
    iArray[4]=new Array();
    iArray[4][0]="保单号";
    iArray[4][1]="120px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="投保人";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=3;
    
    iArray[6]=new Array();
    iArray[6][0]="客户号";
    iArray[6][1]="80px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    
    iArray[7]=new Array();
    iArray[7][0]="合同处理生效日期";
    iArray[7][1]="100px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    
    iArray[8]=new Array();
    iArray[8][0]="处理状态";
    iArray[8][1]="80px";
    iArray[8][2]=100;
    iArray[8][3]=0;
    
    iArray[9]=new Array();
    iArray[9][0]="险种号";
    iArray[9][1]="80px";
    iArray[9][2]=100;
    iArray[9][3]=3;
    
    iArray[10]=new Array();
    iArray[10][0]="合同处理类型";
    iArray[10][1]="80px";
    iArray[10][2]=100;
    iArray[10][3]=3;
  
	  EdorItemGrid = new MulLineEnter( "fm" , "EdorItemGrid" ); 
	  //这些属性必须在loadMulLine前
	  EdorItemGrid.mulLineCount = 0;   
	  EdorItemGrid.displayTitle = 1;
	  EdorItemGrid.canSel =1;
	  EdorItemGrid.selBoxEventFuncName ="getEdorItemDetail" ;     //点击RadioBox时响应的JS函数
	  EdorItemGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	  EdorItemGrid.hiddenSubtraction=1;
	  EdorItemGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}
   
</script>