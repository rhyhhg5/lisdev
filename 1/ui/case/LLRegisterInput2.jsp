<html>
<%
//Name:RegisterInput.jsp
//Function：立案界面的初始化程序
//Date：2005-02-16 17:44:28
//Author ：YangMing
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LLRegisterInput2.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLRegisterInit2.jsp"%>
</head>
<body  onload="initForm();initElementtype();">

  <form action="" method=post name=fm target="fraSubmit">
    <!--
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>-->
    		
    <%@include file="CaseTitle2.jsp"%>
    <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerSearch);">
    	</td>
    	<td class= titleImg>
    	 客户查询 
    	</td>
    </tr>
    
    
      </table>
<div id="divCustomerSearch" style="display:''">      
   <table  class= common>
	<TR  class= common8>
	<TD  class= title8 id='titleClientNo' style="display:'none'">客户序号</TD><TD  class= input8 id='inputClientNo' style="display:'none'"><Input readOnly class=readonly readonly name="ClientNo" ></TD>
	<TD  class= title8>客户姓名</TD><TD  class= input8><Input readOnly class=common name="CustomerTypeQ" ></TD>
	<TD  class= title8>客户号码</TD><TD  class= input8><Input readOnly class=common name="CustomerNoQ" ></TD>
	<TD  class= title8>性别</TD><TD  class = input8 ><Input readOnly class=common name="Sex" ></TD>
	</tr><TR  class= common8>
	<TD  class= title8>证件号码</TD><TD  class= input8><Input readOnly class=common name="tIDNo" ></TD>
	<TD  class= title8>证件类型</TD><TD  class= input8><Input readOnly class=codeno name="tIDType" ><Input readOnly class= codename name=tIDTypeName></TD>
	         
	<TD  class= title8>出生日期</TD><TD  class= input8><Input readOnly class=common name="CBirthday"  ></TD>
	<!--TD  class= title8><Input readOnly class=cssButton type=button value="客户查询" onclick="ClientQuery()"></td-->
	</TR>
    </table>
</div>   
  <div id="div1" style="display:''">
  <table>
      <TR>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegisterInfo);">
        </td>
        <td class= titleImg>
          个人申请信息
        </TD>
    	</TR>
    </table>
</div>    

 <Div id= "divRegisterInfo" style="display: ''">
  <table class=common>
    <TR class=common style="display:'none'" >
     <TD  class= title8>申请人与被保人关系</TD><TD  class= input8><Input readOnly class="codeno" name=Relation  onClick="return showCodeList('llRelation',[this,RelationName],[0,1]);" onkeyup="showCodeListKeyEx('llRelation',[this,RelationName],[0,1]);" ><Input readOnly class="codename"  elementtype=nacessary name=RelationName></TD>   
     <TD  class= title8>受理方式</TD><TD  class= input8><Input readOnly class=codeno  name=RgtType onClick="showCodeList('LLAskMode',[this,RgtTypeName],[0,1]);" onkeyup="showCodeListKeyEx('LLAskMode',[this,RgtTypeName],[0,1]);"><Input readOnly class=codename name= RgtTypeName elementtype=nacessary verify="受理方式|notnull" ></TD>  
	 <TD  class= title8>回执发送方式</TD><TD  class= input8>
	  <Input readOnly class= codeno name=ReturnMode onClick="showCodeList('LLreturnMode',[this,ReturnModeName],[0,1]);" onkeyup="showCodeListKeyEx('LLreturnMode',[this,ReturnModeName],[0,1]);"><Input readOnly class= codename name=ReturnModeName ></TD></TR>
     <TR  class= common>
	 <TD  class= title8>申请人姓名</TD><TD  class= input8><Input readOnly class= common name=RgtantName elementtype=nacessary verify="申请人姓名|notnull"></TD>
     <TD class= title8>申请人证件号码</TD><TD class= input8><Input readOnly class= common name=IDNo></TD>
     <TD class= title8>申请人证件类型</TD><TD class= input8> <Input readOnly class="codeno" name=IDType ><Input readOnly class="codename"  name=IDTypeName ></TD>
     
     </TR>
     <TR  class= common>
     <TD  class= title8>申请人电话</TD><TD  class= input8><Input readOnly class= common name=RgtantPhone ></TD>
     <TD  class= title8>申请人地址</TD><TD  class= input8><Input readOnly class= common name=RgtantAddress ></TD>
     <TD  class= title8>邮政编码</TD><TD  class= input8><Input readOnly class= common name="PostCode" verify="zipcode">
     </TD>
     </tr>
    </table>
  </Div>
 <Div id= "divGetMode" style="display: ''">
  <table class=common>
     <TR  class= common>
			 <TD  class= title8>受益金领取方式</TD><TD  class= input8><Input readOnly class="codeno" name=paymode ><Input readOnly class="codename" name=paymodename  elementtype=nacessary verify="受益金领取方式|notnull" ></TD>
     <TD  class= title8></TD><TD  class= input8><Input readOnly class= common name="temp0" type=hidden ></TD>
     <TD  class= title8></TD><TD  class= input8><Input readOnly class= common name="temp1" type=hidden></td>
     </TR>
    </table>
  </Div>
<div id='divBankAcc'>
	<table  class= common>
		<TR  class= common8>
			<TD  class= title8>银行编码</TD>
			<TD  class= input8><Input readOnly class="codeno"  name=BankCode onclick="return showCodeList('bank',[this,BankName],[0,1]);" onkeyup="return showCodeListKey('bank',[this,BankName],[0,1]);" ><Input readOnly class="codename"  elementtype=nacessary name=BankName ></TD>
			<TD  class= title8>银行账号</TD><TD  class= input8><Input readOnly class= common name="BankAccNo"></TD>
			<TD  class= title8>银行账户名</TD><TD  class= input8><Input readOnly class= common name="AccName"></TD>
		</TR>
	</table>
</div>

<table style="display:'none'">
      <TR>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEventInfo);">
        </td>
        <td class= titleImg>
          申请原因
        </TD>
    	</TR>
    </table>
    <table class=common>
	<TR  class= common8 style="display:'none'">    	
		<TD  class= title8>事故者现状</TD>
		<TD  class= input8><Input readOnly class=codeno name="CustStatus" onClick="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" onkeyup="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);"><Input readOnly class= codename name="CustStatusName"  ></TD>
		<TD  class= title8>死亡日期</TD>
		<TD  class= input8><Input readOnly class="coolDatePicker" dateFormat="short"  name="DeathDate" verify="死亡日期|date"></TD>         			
		<TD  class= title8></TD>
		<TD  class= input8></TD>
	</TR>
   </table>
   
    <Div id= "divEventInfo" style="display: 'none'">
    
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="01">门诊费用 </input>
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="02">住院费用 </input>
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="03">医疗津贴 </input>
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="04">重大疾病 </input>
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="05">身 故     </input>
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="06">护 理     </input>
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="07">失 能     </input>
    <Input readOnly TYPE="checkBox" NAME="appReasonCode" value="08">伤 残     </input>

    
  	    

<div id = "AppReasonGrid" style="display: 'none'">
 	<table  class= common>
      	<tr  class= common>
  	  		<td text-align: left colSpan=1>
			<span id="spanAppReasonGrid" >
			</span>
		</td>
	</tr>
 </table>
</div>
   <div>
   <table class= common>
   <tr class= common8>
   <TD  class= title>
         
     </TD>
     <TD  class= input>
     	<Input readOnly TYPE="checkBox" NAME="SimpleCase" value="01">简易案件 </input>
     </TD>
    <td class=title>      
   	</td>
     <TD  class= title>
            材料齐备日期
     </TD>
     <TD  class= input>
            <Input readOnly class=readonly readonly name=AffixGetDate >
     </TD>
     </tr>
   </table>
   </div>
	</div>
<Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg>
    			 客户事件信息
    		</TD>
    	</TR>
    </Table> 
  <Div  id= "divLDPerson1" style= "display: ''" align = center>
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanEventGrid" ></span> 
  	</TD>
      </TR>
    </Table>
<div id = "IdAppeal" style="display: 'none'">

	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,AppealInfo);">
    		</TD>
    		<TD class= titleImg>
    			 申诉信息
    		</TD>
    	</TR>
    </Table> 
    <div id = "AppealInfo" style="display: ''">
 	<table  class= common>
 	  <TR  class= common>
       <TD  class= title8>申诉原因</TD>
       <TD  class= input8>
       <Input readOnly class=code   type=hidden name=AppeanRCode >
       <Input readOnly    class=code name="AppealReason" elementtype=nacessary ondblClick="showCodeList('llAppeanReason',[this,AppeanRCode],[1,0]);" onkeyup="showCodeListKey('llAppeanReason',[this,AppeanRCode],[1,0]);"></TD>  
       </tr>
    
       <TD  class= title8>备注</TD>
       <TD  class= input colspan="5">
	   <textarea name="AppealRDesc" cols="100%" rows="4" witdh=25% class="common">
		</textarea>
	  </TD>
      </TR>
        
        
        
        </table>

 
  </div>
</div>
	
	<!--
	 <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,AppAffixList);">
    	</td>
    	<td class= titleImg>
    	 申请材料清单
    	</td>
    </tr>
      </table>
      	<Div  id= "AppAffixList" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanAffixGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
	<div align='right'>
	
	&nbsp;&nbsp;&nbsp;&nbsp;
	</div>

	-->
	
	</br>
	    <Div  id= "aa" style= "display: ''" align= right> 
	<Input readOnly class=cssButton  id="IdAppConf" type=button value="申请确认" onclick="submitForm()">
	</div>
	
    <hr/>
    <Div  id= "divnormalquesbtn" style= "display: 'none'" align= right>    
    <Input readOnly class=cssButton type=button value="申请材料选择" onclick="openAffix()">
    <Input readOnly class=cssButton  type=hidden value="账户更替" onclick="BnfModify()">
    <Input readOnly class=cssButton  type=hidden value="事件信息" onclick="OpenEvent()">
    <Input readOnly class=cssButton  type=button name=BankAccM value="账户修改" onclick="OpenBnf()">    
    <Input readOnly class=cssButton  type=button value="提起调查" onclick="submitFormSurvery1()">    
	<Input readOnly class=cssButton VALUE="查  询"  TYPE=hidden onclick="return queryClick();">
	<Input readOnly class=cssButton id='AppealDeal'  type=hidden value="申诉处理" onclick="OnAppeal()">    
    <Input readOnly class=cssButton  type=button value="打印理赔受理回执" onclick="PrintPage()">
    </Div>

   
    <Div  id= "divRgtFinish" style= "display: 'none'" align= right>    	
	<Input readOnly class=cssButton  type=button  value="团体申请完毕" onclick="RgtFinish()">
	<Input readOnly class=cssButton  type=button  value="返回" onclick="top.close()">
    
    </Div>
    </Div>
   <div id= "divClientInfo" style= "display: 'none'" >
   申请金额<Input readOnly class= "common"  name=AppAmnt >
  
<table  class= common>
<TR  class= common8>
<TD  class= title8>客户号码</TD><TD  class= input8><Input readOnly class= readonly name="InsuredNo" readonly ></TD>
<TD  class= title8>客户姓名</TD><TD  class= input8><Input readOnly class= readonly name="Name" readonly ></TD>
	<TD  class= title8>保单号码</TD><TD  class= input8><Input readOnly class=common name="ContNo" onkeydown="QueryOnKeyDown()" type=hidden></TD>
</TR>
<TR  class= common8>

<TD  class= title8>证件类型</TD><TD  class= input8><Input readOnly class= readonly name="CIDType" readonly></TD>
<TD  class= title8>证件号码</TD><TD  class= input8><Input readOnly class= readonly name="CIDNo" readonly></TD>
</TR>
<TR  class= common8>
<TD  class= title8>工作单位</TD><TD  class= input8><Input readOnly class= readonly name=""   readonly></TD>
<TD  class= title8>联系地址</TD><TD  class= input8><Input readOnly class= readonly name="PostalAddress" readonly></TD>
<TD  class= title8>联系电话</TD><TD  class= input8><Input readOnly class= readonly name="Phone" readonly></TD>

</table>
</div>
  
    

 <Input readOnly type=hidden id="fmtransact" name="fmtransact">
 <Input readOnly type=hidden id="Reason" name="Reason">

 <Input readOnly type=hidden id="ReContNo" name="ReContNo">
 <Input readOnly type=hidden id="LoadFlag" name="LoadFlag" value="1">	
<Input readOnly type= hidden id="AppealFlag" name="AppealFlag" value="0">
 <Input readOnly type=hidden id="operate" name="operate">
 <Input readOnly type=hidden id="rgtflag" name="rgtflag">
 <Input readOnly type=hidden id="realpeoples" name="realpeoples">
	<Input readOnly name ="LoadC" type="hidden">
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>


</body>
</html>