<html>
  <%
  //Name:LLGrpRegisterInput.jsp
  //Function：团体立案界面的初始化程序
  //Date：2002-07-21 17:44:28
  //Author ：Xx
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String Comcode=tG.ManageCom;
  String CurrentDate= PubFun.getCurrentDate(); 
  String AheadDays="-90";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
  %>
 <%
   String sql = "select case when current time>='08:00:00' and current time<'17:30:00' then 'Y' else 'N' end from dual"; 
   String sql1 = "select case when dayofweek(current date)=7 or dayofweek(current date)=1 then 'Y' else 'N' end from dual with ur";
   ExeSQL tExeSQL = new ExeSQL();
   String rel = tExeSQL.getOneValue(sql);
   String rel1 = tExeSQL.getOneValue(sql1);
%>
  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="LLGrpRegisterInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LLGrpRegisterInit.jsp"%>
  </head>
  <script language="javascript">
  
   var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
  
    function initDate(){
      fm.ManageCom.value="<%=Comcode%>";
      fm.StartDate.value="<%=afterdate%>";
      fm.EndDate.value="<%=CurrentDate%>";
    }
  </script>
  <body  onload="initDate();initForm();initElementtype();">
    <form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">

      <Div  id= "divUnclose" style= "display: ''">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegister);">
            </td>
            <td class= titleImg>
              未结团体批次
            </td>
          </tr>
        </table>
        <Div  id= "divGrpRegister" align=center style= "display: ''">
          <table  class= common>
            <TR class = common>
              <TD  class= title>受理起期</TD>
              <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' > </TD>
              <TD  class= title>受理止期</TD>
              <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' > </TD> 
              <TD class = title> <INPUT VALUE="查询" class="cssButton" TYPE="button" onclick="SearchGrpRegister()"> </TD>
       	    </TR>          
            <TR  class= common>
              <TD text-align: left colSpan=5>
                <span id="spanGrpRegisterGrid" >
                </span>
              </TD>
            </TR>
          </table>
          <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();">
          <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();">
          <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();">
          <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">
        </Div>
      </Div>
      <HR>
      <table >
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpcontRemarkInfo);">
          </td>
          <td class= titleImg>
            团体保单特约信息
          </td>
          <TD >
            <input class=cssButton type=button value="查看扫描" onclick="ShowScan();">
          </TD>
        </tr>
      </table>

      <div id= "divGrpcontRemarkInfo" style= "display: ''" >
        <table  class= common>
          <TR  class= common8>
            <TD  class= input8>
              <textarea name="GrpRemark" cols="95%" rows="3" witdh=25% class="common"  ></textarea>
            </TD>
          </TR>
        </table>
      </div>
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClientInfoTitle);">
          </td>
          <td class= titleImg>
            团体案件信息
          </td>
        </tr>
      </table>

      <div id= "divClientInfoTitle" style= "display: ''" >
        <table  class= common>
          <TR  class= common8>
            <TD class= title8>团体批次号</TD>
            <TD class= input8><Input class= common name="RgtNo" onkeydown="QueryOnKeyDown_RgtNo()" ></TD>
            <TD class= title8>处理人</TD>
            <TD class= input8><Input class= readonly name="Handler" readonly ></TD>
            <TD class= title8>处理日期</TD>
            <TD class= input8><Input class= readonly name="ModifyDate" readonly ></TD>
          </TR>
        </table>
      </div>
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,idClientQuery);">
          </td>
          <td class= titleImg>
            团体客户查询
          </td>
        </tr>
      </table>
      <div id ='idClientQuery'  style= "display: ''">
        <table  class= common>
          <TR  class= common8>
            <TD class= title8>团体号</TD>
            <TD class= input8><Input class=common name="CustomerNo" elementtype=nacessary verify="团体客户号|notnull" onkeydown="QueryOnKeyDown();"></TD>
            <TD class= title8>单位名称</TD>
            <TD class= input8><Input class=common name="GrpName" onkeydown="QueryOnKeyDown();" ></TD>
            <TD class= title8>保单号码</TD>
            <TD class= input8><Input class=common name="GrpContNo" onkeydown="QueryOnKeyDown();"></TD>
          </TR>
          <tr>
            <TD class= title8>投保人数</TD>
            <TD class= input8><Input class="common"  name=PeopleNo elementtype=nacessary verify="投保人数|notnull"></TD>
            <TD class= title8 id='idGrpAcc' style= "display: 'none'">团体帐户余额</TD>
            <TD class= input8 id='idGrpAccBala' style= "display: 'none'"><Input class="common"  name=AccBala ></TD>
            <TD class= title9 id='idGrpAccNow' style= "display: 'none'">当前账户可用余额</TD>
            <TD class= input9 id='idGrpAccBalaNow' style= "display: 'none'"><Input class="common"  name=AccBalaNow ></TD>
          </tr>
        </table>
      </div>
      <table>
        <TR>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegisterInfo);">
          </td>
          <td class= titleImg>申请信息</TD>
        </TR>
      </table>

      <Div id= "divGrpRegisterInfo" style= "display: ''">
        <table class=common>
          <TR class= common>
            <TD class= title8>联系人</TD>
            <TD class= input8><Input class= common name=RgtantName ></TD>
            <TD class= title8>证件类型</TD>
            <TD class= input8><Input class="codeno" name=IDType onclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);" verify="证件类型|code:IDType"><Input class="codename"  name=IDTypeName ></TD>
            <TD class= title8>证件号码</TD>
            <TD class= input8><Input class= common name=IDNo ></TD>
            <!--
            <TD class= title8>证件类型</TD>
            <TD class= input8><Input class="codeno" name=IDType onclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);" verify="证件类型|notnull&code:IDType"><Input class="codename"  name=IDTypeName elementtype=nacessary></TD>
            <TD class= title8>证件号码</TD>
            <TD class= input8><Input class= common name=IDNo elementtype=nacessary verify="证件号码|notnull"></TD>
          	-->
          </tr>
          <TR  class= common8>
					<TD  class= title8>证件生效日期</TD><TD  class= input8><Input class="coolDatePicker" name="IDStartDate" dateForm="short" onblur="fillDate()"></TD>
					<TD  class= title8>证件失效日期</TD><TD  class= input8><Input class="coolDatePicker" name="IDEndDate"    dateForm="short" onblur="fillDate()"></TD>				
		  </TR>
          
          <TR  class= common>
            <TD class= title8>联系电话</TD>
            <TD class= input8><Input class= common name=RgtantPhone verify="电话号码|num&&len<=30"></TD>
            <TD class= title8>联系地址</TD>
            <TD class= input8><Input class=common name=RgtantAddress ></TD>
            <TD class= title8>邮政编码</TD>
            <TD class= input8><Input class= common name=PostCode  verify="邮政编码|zipcode&INT"></TD>
          </tr>
          <TR  class= common>
            <TD class= title8>受理方式</TD>
            <TD class= input8><Input class=codeno name=RgtType onClick="showCodeList('LLAskMode',[this,RgtTypeName],[0,1]);" onkeyup="showCodeListKey('LLAskMode',[this,RgtTypeName],[0,1]);"  verify="受理方式|notnull&code:LLAskMode" ><Input class=codename name=RgtTypeName elementtype=nacessary></TD>
            <TD class= title8>联系手机</TD>
            <TD class= input8><Input class= common name=Mobile verify="手机号码|num&&len>=8"></TD>
            <TD class= title8>电子邮箱</TD>
            <TD class= input8><Input class= common name=Email verify="电子邮箱|Email"></TD>
          </tr>
          <TR class= common>
            <TD class= title8>申请人数</TD>
            <TD class= input8><Input class= common name=AppPeoples elementtype=nacessary verify="申请人数|notnull"></TD>
            <TD class= title8 >给付方式</TD>
            <TD class= input8><Input class=codeno name=TogetherFlag  onClick="showCodeList('lltogetherflag',[this,TogetherFlagName],[0,1]);" onkeyup="showCodeListKey('lltogetherflag',[this,TogetherFlagName],[0,1]);" verify="给付方式|notnull&code:lltogetherflag" ><Input class= codename name=TogetherFlagName elementtype=nacessary></TD>
            <TD class= title8 >赔款领取方式</TD>
            <TD class= input8>
            <Input class= codeno name=CaseGetMode onClick="showCodeList('paymode',[this,CaseGetModeName],[0,1],null,str,'1');" onkeyup="showCodeListKey('paymode',[this,CaseGetModeName],[0,1],null,str,'1');" verify="保险金领取方式|code:llgetmode" ><Input class= codename name=CaseGetModeName ></TD>
          </TR>
          <tr class= common id="titleBank">
            <TD class= title8>银行编码</TD>
            <TD class= input8><Input class="codeno"  name=BankCode onclick="return showCodeList('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" onkeyup="return showCodeListKey('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" ><Input class="codename"  name=BankName></TD>
            <TD class= title8>签约银行</TD> 
            <TD class= input8><Input class="codename" name=SendFlag readonly="true" ></TD>
            <TD class= title8></TD>
            <TD class= input8></TD>		          
          </TR>
          <TR class= common id="titleBankCode">  
            <TD class= title8>银行账户</TD>
            <TD class= input8><Input class= common  name=BankAccNo ></TD>
            <TD class= title8>账户名</TD>
            <TD class= input8><Input class= common  name=AccName ></TD>
            <TD class= title8></TD>
            <TD class= input8></TD>	
          </tr>
          <TR  class= common id='idApplyAmnt'>
            <TD class= title8>申报金额</TD>
            <TD class= input8><Input class= common name=AppAmnt elementtype=nacessary verify="申报金额|notnull" ></TD>
		  	    <TD  class= title8>申请日期</TD>
            	<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name="AppDate" onblur="fillDate()" elementtype=nacessary ></TD>
            
            <TD class= title8></TD>
            <TD class= input8></TD>	
            <TD class= title8></TD>
            <TD class= input8></TD>	
          </TR>
        </TABLE>
        <TABLE class=common>
          <TR>
            <TD  class= title8 >备注</TD>
          </tr>
          <TR  class= common>
            <TD class= input><textarea name="Remark" cols="100%" rows="4" width=25% class="common"></textarea></TD>
          </tr>
        </table>
      </Div>
      <table class= common>
    		<tr class= common8>    		
    		  <TD  class= input8>
    			   <INPUT TYPE="checkBox" NAME="PrePaidFlag" value="01"> 回销预付赔款
    		  </TD>
    		  <TD class= title8></TD>
          <TD class= input8></TD>
          <TD class= title8></TD>
          <TD class= input8></TD>
    	  </tr>
      </table>
    </br>
    <Div  id= "divgrpconf" style= "display: ''" align= left>
      <input class=cssButton style='width:80px;' type=button name='asksave1' value="确  认" onclick="AskSave();" id = "sub1">
      <input class=cssButton style='width:80px;' type=button value="理赔受理书" onclick="GrpPrt()" id = "sub2">
      <input class=cssButton style='width:80px;' type=button value="撤  件" onclick="DealCancel();" id = "sub3">
    </Div>
    <Div  id= "divnormalquesbtn" style= "display: ''" align= right>
      <input class=cssButton style='width: 80px;' type=button value="  批量导入  " onclick="DiskImport();" id = "sub4">
      <input class=cssButton style='width: 140px;' type=hidden value="  批次导入状态查询  " onclick="GrpRgtstateQuery()" id = "sub5">
    </Div>

    <hr/>

  <Div  id= "divGrpCaseInfo" style= "display: 'none'">
    <table>
      <tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
        </td>
        <td class= titleImg>个人客户信息</td>
      </tr>
    </table>
    <Div  id= "divGrpCaseGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpCaseGrid" >
            </span>
          </TD>
        </TR>
      </table>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
    </Div>
  </Div>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="LoadFlag" name="LoadFlag" value="0">
  <input type=hidden id="operate" name="operate">
  <input type=hidden id="RiskCode" name="RiskCode">
  <input type=hidden id="ApplyerType" name="ApplyerType" value="5">
  <Input type=hidden id="ManageCom" name="ManageCom" >
   <input type=hidden id="idlongeffflag" name="idlongeffflag">
  <input type=hidden name="LoadC" >
  <input type=hidden name="LoadD" >
    	<INPUT  type= hidden name="result" value=<%=rel %>>
	<INPUT  type= hidden name="isweek" value=<%=rel1 %>>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>