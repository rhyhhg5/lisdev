<%
//程序名称：LCInuredListInput.jsp
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LCInuredListInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LCInuredListInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initRiskStatementGrid();
    initRiskGrid();
    QueryRSList();
    QueryRSUList();
  }
  catch(re)
  {
    alert("LCInuredListInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initRiskStatementGrid()
{
    var iArray = new Array();

    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";
      iArray[0][1]="30px";
      iArray[0][2]=10;
      iArray[0][3]=0;

      iArray[1]=new Array();
      iArray[1][0]="险种编号";
      iArray[1][1]="100px";
      iArray[1][2]=100;
      iArray[1][3]=0;

      iArray[2]=new Array();
      iArray[2][0]="险种名称";
      iArray[2][1]="200px";
      iArray[2][2]=100;
      iArray[2][3]=0;

      iArray[3]=new Array();
      iArray[3][0]="条款";
      iArray[3][1]="100px";
      iArray[3][2]=100;
      iArray[3][3]=0;
      iArray[3][7]="showTKPDF";

      iArray[4]=new Array();
      iArray[4][0]="费率表";
      iArray[4][1]="100px";
      iArray[4][2]=100;
      iArray[4][3]=0;
      iArray[4][7]="showFLPDF";

      RiskStatementGrid = new MulLineEnter( "fm" , "RiskStatementGrid" );
      RiskStatementGrid.mulLineCount = 10;
      RiskStatementGrid.displayTitle = 1;
	  	RiskStatementGrid.hiddenPlus = 1;
      RiskStatementGrid.hiddenSubtraction = 1;
      RiskStatementGrid.selBoxEventFuncName = "showPDF";
      RiskStatementGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initRiskGrid()
{
    var iArray = new Array();

    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";
      iArray[0][1]="30px";
      iArray[0][2]=10;
      iArray[0][3]=0;

      iArray[1]=new Array();
      iArray[1][0]="险种编号";
      iArray[1][1]="100px";
      iArray[1][2]=100;
      iArray[1][3]=0;

      iArray[2]=new Array();
      iArray[2][0]="险种名称";
      iArray[2][1]="200px";
      iArray[2][2]=100;
      iArray[2][3]=0;

      RiskGrid = new MulLineEnter( "fm" , "RiskGrid" );
      RiskGrid.mulLineCount = 10;
      RiskGrid.displayTitle = 1;
	  	RiskGrid.hiddenPlus = 1;
      RiskGrid.hiddenSubtraction = 1;
      RiskGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
