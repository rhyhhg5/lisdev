<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLCaseTraceQueryInput.jsp
//程序功能：理赔案件轨迹查询Excel打印
//创建日期：2014-03-04
//创建人  ：Houyd
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLCaseTraceQueryInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>

</script>
</head>
<body onload="" >    
  <form action="./LLCaseTraceQuerySave.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
        <TR class = common>
          <TD  class= title>理赔号</TD>
          <TD  class= input> <input class=common name=CaseNo ></TD> 		
    </table>
    <hr>
    <table class=common>
    	<tr class=common>
              <TD><INPUT VALUE="理赔案件轨迹打印" class="cssButton" TYPE="button" onclick="CaseTraceQuery()"/></TD>
		</tr>
	</table>
	 <table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
		 	<tr class="common">
		 		<td class="title">操作步骤：包括受理、录入、检录、理算、审批、审定、抽检、结案、通知、给付等</td>
		 	</tr><br>
            <tr class="common">
            	<td class="title">操作开始日期：每个操作步骤的开始日期</td>
            </tr><br>
			<tr class="common">
				<td class="title">操作开始时间：每个操作步骤的开始时间</td>
			</tr><br>
			<tr class="common">
				<td class="title">操作结束日期：每个操作步骤的结束时间</td>
			</tr><br>
			<tr class="common">
				<td class="title">操作结束时间：每个操作步骤的结束时间</td>
			</tr><br>
			<tr class="common">
				<td class="title">操作人员代码：每个操作步骤的人员代码</td>
			</tr><br>
			<tr class="common">
				<td class="title">操作人员姓名：人员代码所对应的人员姓名</td>
			</tr><br> 
			<tr class="common">
				<td class="title">所属机构代码：操作人员的所属机构代码</td>
			</tr><br>
			<tr class="common">
				<td class="title">所属机构名称：机构代码所对应的机构名称</td>
			</tr><br>
			
		</Div>
		<input type="hidden" name=operate value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html> 