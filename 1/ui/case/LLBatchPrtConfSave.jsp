<%
//LLBatchPrtConfSave.jsp
//程序功能：
//创建日期：2005-07-23 11:53:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
 	<%@page import="com.sinosoft.lis.pubfun.*"%>
 	<%@page import="java.lang.String"%>
	<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。

  LLBatchPrtBL tLLBatchPrtBL   = new LLBatchPrtBL();
  VData tVData = new VData();
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String CaseNoBatch="";
  String tPrintServerPath = "";
  int    tCount=0;
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  System.out.println("Start Product XML:"+PubFun.getCurrentTime());
 //tG.ClientIP = request.getRemoteAddr();
 tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
 tG.ServerIP =tG.GetServerIP();
 System.out.println("IP's address :"+tG.ClientIP);
 System.out.println("IP's address :"+tG.ClientIP);

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("Operator");
  	System.out.println("transact : "+transact);
		String CaseNo[]=request.getParameterValues("CaseGrid1");
		String tRNum[] = request.getParameterValues("InpCaseGridChk");
   	for(int i=0;i<tRNum.length;i++){
	  if(tRNum[i].equals("1")){
	  CaseNoBatch += CaseNo[i];
	  }
    String tOutXmlFile  = application.getRealPath("");
		tLLBatchPrtBL   = new LLBatchPrtBL();
		TransferData PrintElement = new TransferData();
		PrintElement.setNameAndValue("CaseNoBatch",CaseNoBatch);
		PrintElement.setNameAndValue("BatchType","3");
		PrintElement.setNameAndValue("DetailPrt","on");
		PrintElement.setNameAndValue("GrpDetailPrt","off");
		PrintElement.setNameAndValue("NoticePrt","on");
		PrintElement.setNameAndValue("GetDetailPrt","off");
		LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
	  tLDSysVarSechma.setSysVarValue(tOutXmlFile);
    try{
  // 准备传输数据 VData
  	tVData = new VData();
  	tVData.add(tLDSysVarSechma);
		tVData.add(PrintElement);
  	tVData.add(tG);
    if(tLLBatchPrtBL.submitData(tVData,transact))
    {
      if (transact.equals("PRINT"))
      {
        tCount = tLLBatchPrtBL.getCount();
        System.out.println("~~~~~~~~~~~~~~~~~~~"+tCount);
        String tFileName[] = new String[tCount];
        VData tResult = new VData();
        tResult = tLLBatchPrtBL.getResult();
        tFileName=(String[])tResult.getObject(0);
        String mFileNames = "";
        for (int k = 0;k<=(tCount-1);k++)
        {
          System.out.println("~~~~~~~~~~~~~~~~~~~"+k);
          System.out.println(tFileName[k]);
          mFileNames = mFileNames + tFileName[k]+":";
        }
          System.out.println("=============================================");
          System.out.println("~~~~~~~~~~~~~~~~~~~"+mFileNames);
        String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
        ExeSQL mExeSQL = new ExeSQL();
        tPrintServerPath =  mExeSQL.getOneValue(sql);
%>
        <html>
        <script language="javascript">
            var printfrm = parent.fraInterface.document.getElementById("printfrm");
            printfrm.src = "<%=tPrintServerPath%>?afilename=<%=mFileNames%>";
        </script>
        </html>
<%
		 System.out.println("Product XML Over:"+PubFun.getCurrentTime());
		     }
       }
		}catch(Exception ex)
						  {
						    Content += "操作失败，原因是:" + ex.toString();
						    FlagStr = "Fail";
						  }

   }


//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLBatchPrtBL .mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
