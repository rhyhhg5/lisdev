<html>
<%
  //Name：LLBatchPrtInput.jsp
  //Function：批量打印页面
  //Date：2005-07-23 16:49:22
  //Author：Xx
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  String Operator = tG.Operator;
  String Comcode = tG.ManageCom;
  String CurrentDate = PubFun.getCurrentDate();
  String AheadDays = "-1";
  FDate tD = new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate), Integer.parseInt(AheadDays), "D", null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString(AfterDate);
%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLBatchPrtInput.js"></SCRIPT>
<%@include file="LLBatchPrtInputInit.jsp"%>
<script language="javascript">
			function initDate(){
				fm.EndDateS.value="<%=afterdate%>";
				fm.EndDateE.value="<%=CurrentDate%>";
				var usercode="<%=Operator%>";
				fm.ComCode.value="<%=Comcode%>";
			}
		</script>
</head>
<body onload="initForm();initDate();">
<form action="./LLBatchPrtSave.jsp" method=post name=fm target="fraSubmit">
<table>
  <TR>
    <TD>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divPrtRequest);">
    </TD>
    <TD class=titleImg>打印处理</TD>
  </TR>
</table>
<Div id="divPrtRequest" style="display: ''">
  <table class=common>
    <TR class=common8>
      <TD width=120>
        <input type="radio" name="IsBatch" value="1" checked>
        批次打印
</input>      </TD>
    </TR>
    <TR class=common8>
      <TD class=title8>
        <INPUT VALUE="团体批次查询" style='width:80px;' class=cssButton TYPE=button onclick="QueryBatch();">
      </TD>
    </TR>
  </table>
  <table class=common>
    <TR class=common8>
      <TD class=title8>团体批次号</TD>
      <TD class=input8>
        <input class="common" name=GrpRgtNo>
      </TD>
      <TD class=title8>客户名称</TD>
      <TD class=input8>
        <input class="common" name=GrpName>
    <!-- 
      </TD>
      <TD class=title8>份  数</TD>
      <TD class=input8>
        <input class="common" name=BatchNumber value="1">
      </TD>
      -->
    </TR>
  </table>
  <br>
  <table class=common>
    <TR class=common8>
      <TD>
        <input type="radio" name="IsBatch" value="2" width=80 onclick="">
        连号打印
</input>      </TD>
    </TR>
  </table>
  <table class=common>
    <TR>
      <TD class=title8 style="width: '7%'">        受理日期
        &nbsp
      </TD>
      <TD class=title8 style="width: '1%'">从</TD>
      <TD class=input8>
        <input class="coolDatePicker" dateFormat="short" name=RgtDateS onkeydown="QueryOnKeyDown()">
      </TD>
      <TD class=title8 style="width: '8%'">      </TD>
      <TD class=title8 style="width: '2%'">到</TD>
      <TD class=input8>
        <input class="coolDatePicker" dateFormat="short" name=RgtDateE onkeydown="QueryOnKeyDown()">
      </TD>
      <TD class=title8>      </TD>
      <TD class=input8>
        <input class="readonly" readonly>
      </TD>
    </TR>
  </table>
  <table class=common>
    <TR>
      <TD class=title8 style="width: '7%'">        通知日期
        &nbsp
      </TD>
      <TD class=title8 style="width: '1%'">从</TD>
      <TD class=input8>
        <input class="coolDatePicker" dateFormat="short" name=EndDateS onkeydown="QueryOnKeyDown()">
      </TD>
      <TD class=title8 style="width: '8%'">      </TD>
      <TD class=title8 style="width: '2%'">到</TD>
      <TD class=input8>
        <input class="coolDatePicker" dateFormat="short" name=EndDateE onkeydown="QueryOnKeyDown()">
      </TD>
      <TD class=title8>      </TD>
      <TD class=input8>
        <input class="readonly" readonly>
      </TD>
    </TR>
  </table>
  <table class=common>
    <TR class=common8>
      <TD class=title8 style="width: '7%'">起始案件号</TD>
      <TD class=title8 style="width: '1%'">        &nbsp&nbsp
      </TD>
      <TD class=input8>
        <input class="common" name=StartCaseNo>
      </TD>
      <TD class=title8 style="width: '7%'">终止案件号</TD>
      <TD class=title8 style="width: '2%'">      </TD>
      <TD class=input8>
        <input class="common" name=EndCaseNo>
      </TD>
      <TD class=title8>      </TD>
      <TD class=input8>
        <input class="readonly" readonly>
      </TD>
    </TR>
  </table>
  <br>
  <table class=common>
    <TR class=common8>
      <TD>
        <input type="radio" name="IsBatch" value="3">单号打印</input>
      </TD>
    </TR>
  </table>
  <table class=common>
    <TR class=common8>
      <TD class=title8 style="width: '3%'">独立案件号输入</TD>
      <TD class=input8>
        <input class="common" name=SingleCaseNo style="width: '95%'">
      </TD>
    </TR>
  </table>
</div>
<HR>
<Div id="divPrtType" style="display: ''"><!--liuli 08/03/03 将复选框改为单选框-->
  <TABLE class=common align=center width=50%>
    <TR class=common8>
      <TD class=title8>
        <input type="radio" name="Prt" value="0">
        理赔决定通知书
</input>      </TD>
      <TD class=title8>
        <input type="radio"  name="Prt" value="1">
        理赔给付细目表
</input>      </TD>
      <TD class=title8>
        <input type="radio" name="Prt" value="2" checked>
        赔付汇总细目表
</input>      </TD>
      <TD class=title8>
        <input type="radio" name="Prt" value="3">
        给付凭证
</input>      </TD>
	<TD class=title8>
		<input type="radio"
			name="Prt" value="4"> 理赔给付细目汇总表 </input>
		</TD>
	<TD class=title8>
        <input type="radio" name="Prt" value="5">拒赔通知书</input>     
        </TD>
    </TR>
    <TR>
    <TD class=title8>
        <input type="radio" name="Prt" value="6">理赔给付细目表（excel）</input>     
        </TD>	
    <TD class=title8>
        <input type="radio" name="Prt" value="7">赔付汇总细目表（excel）</input>     
        </TD>	
    </TR>
  </table>
</div>
<br>
<div id="divConfirm" style="display: ''">
  <INPUT VALUE="信息确认" class=cssButton style='width:80px;' TYPE=hidden onclick="BatchQueue();">
  <INPUT VALUE="打  印" class=cssButton style='width:80px;' TYPE=button onclick="BatchPrint();">
  <INPUT VALUE="重  置" class=cssButton style='width:80px;' TYPE=button onclick="Reset();">
</div>
<input name=sql type=hidden class=common>
  <!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
  <!--隐藏域-->
<Input type="hidden" class=common name="fmtransact">
<Input type="hidden" class=common name="selno">
<Input type="hidden" class=common name="ComCode">
<Input type="hidden" class=common name="Notice">
<Input type="hidden" class=common name="Detail">
<Input type="hidden" class=common name="DetailX">
<Input type="hidden" class=common name="Decline">
<Input type="hidden" class=common name="GrpDetail">
<Input type="hidden" class=common name="GetDetail">
<Input type="hidden" class=common name="DetailExcel">
<Input type="hidden" class=common name="GrpDetailExcel">
</form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
<iframe name="printfrm" src="" width=0 height=0></iframe>
<form id=printform target="printfrm" action="" method = "post">
<input type=hidden name=filename value=""/>
</form>
</body>
</html>
