<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String tCurrentDate = PubFun.getCurrentDate();
    String tStrSubType = (String)request.getParameter("SubType");
%>                            

<script language="JavaScript">
  
var tManageCom = "<%=tGI.ComCode%>";
var tCurrentDate = "<%=tCurrentDate%>";
var tSubType = "<%=tStrSubType%>";

function initForm()
{
    try
    {	
   	 initContGrid();
    	initInpBox();
        
        
    }
    catch(ex)
    {
        alert("初始化界面错误，" + ex.message);
    }
}

function initInpBox(){
	try{
			
			fm.all("ManageCom").value=tManageCom;
			var sql ="select Name from ldcom where  comcode='"+tManageCom+"'";
			var strQueryResult = easyQueryVer3(sql,1,1,1);
			arrSelected = decodeEasyQueryResult(strQueryResult);
			if(arrSelected!=null && arrSelected!=""){
				fm.all("ManageComName").value=arrSelected;
			}
		}
	  catch(ex)
    {
        alert("页面初始化错误" + ex.message);
    }	
}
function initPage(){
	fm.BatchNo.value="";                             
	fm.OSTotal.value="";                             
	fm.RiskCode.value="";                         
	fm.BalanceStartDate.value="";                 
	fm.BalanceEndDate.value="";                    
	fm.PayYear.value="";                           
	fm.ManageCom.value="";                          
	fm.OSAccount.value="";                         
	fm.OSAccountNo.value="";                        
	fm.OSBank.value="";                            
	fm.Transfertype.value="";                     
	fm.Operator.value="";                       
	fm.AuditOpinion.value=""; 	                     
	fm.ManageComName.value=""; 	                 
	fm.OSBankName.value="";                        
	fm.TransferTypeName.value="";              

}


function initContGrid()
{
    var iArray = new Array();
        
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=20;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="外包批次号";         		//列名
        iArray[1][1]="80px";            		//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[1][21]="BatchNo";
        
        iArray[2]=new Array();
        iArray[2][0]="险种号";         		//列名
        iArray[2][1]="70px";            		//列宽
        iArray[2][2]=80;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[2][21]="RiskCode"; 
        
        iArray[3]=new Array();
        iArray[3][0]="外包费用总额";         		//列名
        iArray[3][1]="50px";            		//列宽
        iArray[3][2]=100;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[3][21]="OSTotal";
        
        iArray[4]=new Array();
        iArray[4][0]="操作用户";         		//列名
        iArray[4][1]="80px";            		//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[4][21]="Operator";
        
        iArray[5]=new Array();
        iArray[5][0]="录入时间";         		//列名
        iArray[5][1]="80px";            		//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[5][21]="Makedate";
        
        iArray[6]=new Array();
        iArray[6][0]="审批状态";         		//列名
        iArray[6][1]="80px";            		//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[6][21]="AUDITSTATE";
        ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
        //这些属性必须在loadMulLine前
        ContGrid.mulLineCount = 0;   
        ContGrid.displayTitle = 1;
        ContGrid.locked = 1;
        ContGrid.canSel = 1;
        ContGrid.canChk = 0;
        ContGrid.hiddenPlus=1;   
        ContGrid.hiddenSubtraction=1;
        ContGrid.selBoxEventFuncName ="getContInfo";
        ContGrid.loadMulLine(iArray);
        
    }
    catch(ex)
    {
        alert("初始化" + ex.message);
    }
}

</script>