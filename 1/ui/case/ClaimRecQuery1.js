//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;
var CaseNo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var CustomerNo='';

// 查询按回车
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClaimQuery();
//		UpdateGrid();
	}
}
function ClaimQuery()
{
	CustomerNo=	fm.CustomerNo.value;
	strSQL="Select CaseNo,RgtDate,EndCaseDate,coalesce((select sum(sumfee) from llfeemain where caseno=c.caseno and caserelano is not null),0),coalesce((select realpay from llclaim where caseno=c.caseno),0),"
				+"(select count(subrptno) from llcaserela where caseno=c.caseno),(select count(distinct riskcode ) from llclaimdetail where  caseno=c.caseno ),"
				+" handler,rgtstate from llcase c where CustomerNo='"+CustomerNo+"'";
	turnPage1.queryModal(strSQL, CaseGrid);
	
	
}


//计算历史统计信息
function CalInfo()
{	
			var strSQL = "select p.getdutykind,coalesce(sum(p.realpay),0) from llclaimpolicy p  "
								 + "where p.insuredno='"+CustomerNo+"' and getdutykind ='100' and p.clmno in (select clmno from llclaim c where  c.givetype is not null) group by p.getdutykind; "
			var arr = easyExecSql(strSQL);
			if( arr!=null){
			fm.InHosTotalPay.value = arr[0][1];
			}else{
			fm.InHosTotalPay.value =0;	
				}
			var strSQL1 = "select p.getdutykind,coalesce(sum(p.realpay),0) from llclaimpolicy p  "
								 + "where p.insuredno='"+CustomerNo+"' and getdutykind ='200' and p.clmno in (select clmno from llclaim c where  c.givetype is not null) group by p.getdutykind; "
			var arr1 = easyExecSql(strSQL1);
			if( arr1!=null){
			fm.DoorTotalPay.value = arr1[0][1];
			}else{
			fm.DoorTotalPay.value = 0;	
				}
			var strSQL2 = "select coalesce(sum(realhospdate),0) from llfeemain where CustomerNo='"+CustomerNo+"'"		;						 + "where p.insuredno='"+CustomerNo+"' and getdutykind ='200' and p.clmno in (select clmno from llclaim c where  c.givetype is not null) group by p.getdutykind; "
			var arr2 = easyExecSql(strSQL2);
			if( arr2!=null){
			fm.TotalDays.value = arr2[0][0];
			}else{
			fm.TotalDays.value = 0;	
				}
		
}

//Mulline单选框事件
function ShowCaseInfo()
{  
			initClaimPayGrid();
			var selno = CaseGrid.getSelNo();

		  CaseNo = CaseGrid.getRowColData(selno-1,1);
	
	
		strSQL="select case when (select conttype from lcpol where polno=p.polno union select conttype from lbpol where polno=p.polno) ='1' "
					+" then  p.contno else p.grpcontno end,"
					+" (select riskshortname from lmrisk where riskcode=p.riskcode) ,"
					+" (select amnt from lcpol where  polno=p.polno union select amnt from lbpol where  polno=p.polno),"
					+" (select sum(sumfee) from llfeemain where  caseno=p.caseno and caserelano in "
					+" (select caserelano from llclaimpolicy where caseno=p.caseno and polno=p.polno)),"
					+" case when (select givetype from llclaim where caseno=p.caseno) is  null then 0 else sum(p.realpay) end ,p.riskcode,p.polno "
					+" from llclaimpolicy p where  caseno='"+CaseNo+"'"
					+" group by caseno,polno,contno,grpcontno,p.riskcode" ; 
  		turnPage2.queryModal(strSQL, ClaimPayGrid);

}

function ShowPayInfo(){
	
	var selno = ClaimPayGrid.getSelNo();

	var PolNo = ClaimPayGrid.getRowColData(selno-1,7);
//门诊
			strSQL=" select count(mainfeeno),coalesce(sum(sumfee),0),"
						+" case when (select givetype from llclaim where caseno=m.caseno) is null then 0 else "
						+" (select coalesce(sum(realpay),0) from llclaimpolicy where caseno=m.caseno getdutykind ='200') end "
						+" from llfeemain m  where caseno='"+CaseNo+"' and feetype='1' and caserelano in "
						+" (select caserelano from llclaimpolicy where caseno=m.caseno and polno='"+PolNo+"')"
						+" group by m.caseno ";
		  var arr = easyExecSql(strSQL);
		  if( arr!=null){
			fm.DoorFeeCount.value= arr[0][0];
			fm.DoorFee.value=arr[0][1];
			fm.DoorFeePay.value=arr[0][2];
		}else{
			fm.DoorFeeCount.value= 0;
			fm.DoorFee.value=0;
			fm.DoorFeePay.value=0;
			}
//住院	
			strSQL1=" select count(mainfeeno),coalesce(sum(sumfee),0),"
						+" case when (select givetype from llclaim where caseno=m.caseno) is null then 0 else "
						+" (select coalesce(sum(realpay),0) from llclaimpolicy where caseno=m.caseno and getdutykind ='100') end "
						+" from llfeemain m  where caseno='"+CaseNo+"' and feetype in ('2','3') and caserelano in "
						+" (select caserelano from llclaimpolicy where caseno=m.caseno and polno='"+PolNo+"')"
						+" group by m.caseno ";
		  var arr1 = easyExecSql(strSQL1);
		  if( arr1!=null){
			fm.InHosFeeCount.value= arr1[0][0];
			fm.InHosFee.value=arr1[0][1];
			fm.InHosFeePay.value=arr1[0][2];
		}else{
			fm.InHosFeeCount.value= 0;
			fm.InHosFee.value=0;
			fm.InHosFeePay.value=0;
			}
//门特病			
//		strSQL2=" select count(mainfeeno),coalesce(sum(sumfee),0),'',"
//					+" from llfeemain m  where caseno='"+CaseNo+"' and feetype='3' and caserelano in "
//					+" (select caserelano from llclaimpolicy where caseno=m.caseno and polno='"+PolNo+"')"
//					+" group by m.caseno ";
//	  var arr2 = easyExecSql(strSQL2);
//	  if( arr2!=null){
//		fm.SpeDoorFeeCount.value= arr2[0][0];
//		fm.SpeDoorFee.value=arr2[0][1];
//		fm.SpeDoorFeePay.value=0;
//	}else{
//		fm.SpeDoorFeeCount.value=0;
//		fm.SpeDoorFee.value=0;
//		fm.SpeDoorFeePay.value=0;
//		}
	}


// 数据返回父窗口
function returnParent()
{
	//alert(tDisplay);
	if (tDisplay=="1")
	{
	    var arrReturn = new Array();
	    var tSel = ClaimPayGrid.getSelNo();
	    //alert(tSel);
	    if( tSel == 0 || tSel == null )
	    	alert( "请先选择一条记录，再点击返回按钮。" );
	    else
	    {
	    	try
	    	{
	    		arrReturn = getQueryResult();
	    		
	    		top.opener.afterQuery( arrReturn );
	    		
	    		top.close();
	    	}
	    	catch(ex)
	    	{
	    		alert( "请先选择一条非空记录，再点击返回按钮。");
	    		
	    	}
	    	
	    }
	 }
	 else
	 {
	    top.close(); 
	 }
}

function getQueryResult()
{
	var arrSelected = null;
	var tRow = ClaimPayGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
		      return arrSelected;
	
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = ClaimPayGrid.getRowData(tRow-1);
	
	return arrSelected;
}

function getstr()
{
	//alert("aaa");
	str="1 and code like #"+fm.OrganCode.value+"%#";
}
function getRights(){
	var strSQL="select claimpopedom from llclaimuser where usercode='"+fm.Operator.value+"'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		var Rights = arrResult[0][0];
		//alert(Rights);

	}
	Str="1 and claimpopedom < #"+Rights+"#";
}


//原始信息查询页面
function ShowInfoPage()
{
	var selno = ClaimPayGrid.getSelNo();//alert(CaseGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = ClaimPayGrid.getRowColData(selno-1,2);
	OpenWindowNew("./ClaimUnderwriteInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
}    

//保单查询
function ContInfoPage()
{
		var selno = ClaimPayGrid.getSelNo();//alert(CaseGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = ClaimPayGrid.getRowColData(selno-1,2);
		var ContNo = easyExecSql("select distinct grpcontno from llclaimpolicy where caseno = '"+CaseNo+"'");
		window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
}


function afterCodeSelect( cCodeName, Field )
{
  //选择了处理
  if( cCodeName == "DealWith")
  {
    LoadC="1";
    switch(fm.all('DealWith').value){
      case "1":
      DealApp();                 //受理申请
      window.focus();              
      break;
      case "2":
      DealAppGrp();                 //团体受理申请
      window.focus();              
      break;
      case "3":
      DealFeeInput();              //帐单录入
      window.focus();              //咨询通知
      break;
      case "4":
      DealCheck();                //检录
      window.focus();              //咨询通知
      break;
      case "5":
      DealClaim();               //理算
      window.focus();              //咨询通知
      break;
      case "6":
      DealApprove();               //审批审定
      window.focus();              //咨询通知

    }
  }
}

function DealApp()
{

  var selno = CaseGrid.getSelNo()-1;
  var caseNo="";
  var RgtNo="";
//  alert(selno);
  if (selno >= 0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }
  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }
  
  var varSrc= "&RgtNo="+ RgtNo + "&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
 
  var newWindow = OpenWindowNew("./FrameMainCaseRequest.jsp?Interface=LLRegisterInput.jsp"+varSrc,"","left");

}

function DealAppGrp()
{
  var selno = CaseGrid.getSelNo()-1;
  var caseNo;
  if (selno >=0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }
  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }
  var varSrc="&RgtNo="+ RgtNo;
  varSrc += "&LoadC="+LoadC;
  var newWindow = OpenWindowNew("./FrameMainGrpCheck.jsp?Interface=LLGrpRegisterInput.jsp"+varSrc,"","left");

}

function DealFeeInput()
{	var CaseNo="";
  var selno = CaseGrid.getSelNo()-1;
  if (selno>=0)
  {
    CaseNo = CaseGrid.getRowColData(selno, 1);
  }
  var varSrc = "&CaseNo=" + CaseNo;
  varSrc += "&LoadC="+LoadC;
  var newWindow =OpenWindowNew("./FrameMainZD.jsp?Interface=ICaseCureInput.jsp" + varSrc,"","left");

}

//检录
function DealCheck()
{
  var caseNo ="";
  var selno = CaseGrid.getSelNo()-1;
  if (selno >=0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }
  var varSrc="&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
  var newWindow = OpenWindowNew("./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
  varSrc,"检录","left");

}

//理算
function DealClaim()
{
  var caseno = '';
  var selno = CaseGrid.getSelNo()-1;
  if (selno >=0)
  {
    caseno = CaseGrid.getRowColData(selno,1);
  }
  var varSrc="&CaseNo=" + caseno ;
  varSrc += "&LoadC="+LoadC;
  var newWindow = OpenWindowNew("./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+
  varSrc,"理算","left");
}

//审批审定
function DealApprove()
{
  var selno = CaseGrid.getSelNo()-1;
  var caseNo = "";
  if (selno>=0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }

  var varSrc = "&CaseNo=" + caseNo;
  varSrc += "&LoadC="+LoadC;
  var newWindow = OpenWindowNew("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + varSrc,"","left");

}