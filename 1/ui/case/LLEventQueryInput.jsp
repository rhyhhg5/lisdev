<%
//程序名称：LLSubReportInput.jsp
//程序功能：功能描述
//创建日期：2005-02-04 09:43:38
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LLEventQueryInput.js"></SCRIPT> 
  <%@include file="LLEventQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLLSubReportGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      分报案号(事件号)
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SubRptNo >
    </TD>
    <TD  class= title>
      关联客户号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      关联客户的名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerName >
    </TD>
    <TD  class= title>
      关联客户类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      事件主题
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccSubject >
    </TD>
    <TD  class= title>
      事故类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccidentType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      发生日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccDate >
    </TD>
    <TD  class= title>
      终止日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccEndDate >
    </TD>
  </TR>
  <TR  class= common>
    
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      事故地点
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccPlace >
    </TD>
    <TD  class= title>
      医院代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HospitalCode >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      医院名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HospitalName >
    </TD>
    <TD  class= title>
      入院日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InHospitalDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      出院日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OutHospitalDate >
    </TD>
    
  </TR>
  
</table>
  </Div>
  <div id="div1" style="display: 'none'">
  	<table>
  		<TD  class= title>
	      备注
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=Remark >
	    </TD>
	  	<TD  class= title>
	      事故者状况
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=CustSituation >
	    </TD>
	  	<TD  class= title>
	      事故描述
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=AccDesc >
	    </TD>
	  	<TR  class= common>
	    <TD  class= title>
	      重大事件标志
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SeriousGrade >
	    </TD>
	    <TD  class= title>
	      调查报告标志
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SurveyFlag >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      操作员
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=Operator >
	    </TD>
	    <TD  class= title>
	      管理机构
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MngCom >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      入机日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeDate >
	    </TD>
	    <TD  class= title>
	      入机时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeTime >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      最后一次修改日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=ModifyDate >
	    </TD>
	    <TD  class= title>
	      最后一次修改时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=ModifyTime >
	    </TD>
	  </TR>
  	</table>
  </div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLSubReport1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLLSubReport1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLLSubReportGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
