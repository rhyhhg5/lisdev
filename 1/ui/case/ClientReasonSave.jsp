<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ClienReasonSave.jsp
//程序功能：
//创建日期：2005-02-18 08:49:52
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	//声明Schema Set
	
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);
  	
	LLAppClaimReasonSet tLLAppClaimReasonSet   = new LLAppClaimReasonSet();
	LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
	LLAppealSchema tLLAppealSchema = new LLAppealSchema();
	LLSubReportSet tLLSubReporSet = new LLSubReportSet();
	LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
	//声明VData
	VData tVData = new VData();
	//声明后台传送对象
	ClientRegisterBL tClientRegisterBL   = new ClientRegisterBL();
	//输出参数
	CErrors tError = null;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String EventFlag="";	//事件申请标记 为1 有客户存在事件，为0不存在事件
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");

	
/*将申请信息填充*/
System.out.println("<--submit LLRegisterSchema-->");
if (strOperate.equals("INSERT||MAIN")) 	
{
    tLLRegisterSchema.setRgtState("13");//案件状态
    tLLRegisterSchema.setRgtObj("2");						//号码类型 0总单 1分单 2个单 3客户
    tLLRegisterSchema.setRgtObjNo(request.getParameter("CustomerNo"));
    System.out.println("RgtObjNo:"+tLLRegisterSchema.getRgtObjNo());
    tLLRegisterSchema.setRgtClass("0");
    tLLRegisterSchema.setCustomerNo(request.getParameter("CustomerNo"))	;
    tLLRegisterSchema.setRgtType(request.getParameter("RgtType"));         		//受理方式
    tLLRegisterSchema.setRgtantName(request.getParameter("RgtantName"));     	//申请人姓名
    tLLRegisterSchema.setRelation(request.getParameter("Relation"));        		//与被保险人关系
    tLLRegisterSchema.setRgtantAddress(request.getParameter("RgtantAddress"));	//申请人地址
    tLLRegisterSchema.setRgtantPhone(request.getParameter("RgtantPhone"));     	//申请人电话
    tLLRegisterSchema.setRgtantMobile(request.getParameter("Mobile"));    //申请人手机
    tLLRegisterSchema.setEmail(request.getParameter("Email"));           //E-mail
    tLLRegisterSchema.setPostCode(request.getParameter("PostCode"));        		//邮编
    tLLRegisterSchema.setAppAmnt(request.getParameter("AppAmnt"));       		//预估申请金额  
    System.out.println("银行信息");
    tLLRegisterSchema.setGetMode(request.getParameter("paymode"));
   	tLLRegisterSchema.setAppDate(request.getParameter("AppDate"));
   	System.out.println(tLLRegisterSchema.getAppDate()+"==$%^$%^&&======&&&&&&(((())))==========");
    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setAccName(request.getParameter("AccName"));
    tLLRegisterSchema.setReturnMode(request.getParameter("ReturnMode"));    		//回执发送方式  
    tLLRegisterSchema.setIDType(request.getParameter("IDType"));        			//申请人证件类型
    tLLRegisterSchema.setIDNo(request.getParameter("IDNo"));          			//申请人证件号码
    tLLRegisterSchema.setRgtObj("1"); //个人客户                              
    tLLCaseSchema.setRgtType("1");
}

if (strOperate.equals("UPDATE||MAIN"))
{
  tLLRegisterSchema.setRelation("05");
  tLLCaseSchema.setRgtType("1");
  tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
}    

   /******************************************************/
     //申请原因
	String tNum[] = request.getParameterValues("appReasonCode");
	String appResonName[] = {"门诊费用","住院费用","医疗津贴","重大疾病","身 故","护 理","失能","伤残"};

	int AppReasonNum = 0;
	if (tNum != null) 
	{
		AppReasonNum = tNum.length;
	}

	String SimpleCase = "";
	if (request.getParameterValues("SimpleCase") != null){
  	tLLCaseSchema.setCaseProp("06");
	}else{
	  tLLCaseSchema.setCaseProp("08");
	}

	if (request.getParameterValues("EasyCase") != null) {
	  tLLCaseSchema.setCaseProp("09");
	}
	if (request.getParameterValues("FenCase") != null) {
	  tLLCaseSchema.setCaseProp("11");
	}
	if (request.getParameterValues("HeadCase") != null) {
	  tLLCaseSchema.setCaseProp("08");
	}
	System.out.println("888888888888888888888888888受理申请选择的上报方式:"+tLLCaseSchema.getCaseProp());
//	if (request.getParameterValues("CaseFlag") != null) {
//	System.out.println("3333333333333333333333333333CaseFlag:I");
//	  tLLCaseSchema.setCaseFlag("I");
//	}else{
//	   tLLCaseSchema.setCaseFlag("G");
//	}
	if (request.getParameterValues("PrePaidFlag") != null) {
		  tLLCaseSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
	}else{
		tLLCaseSchema.setPrePaidFlag("");  // 个案标记llcase 批次案件标记llregister
	}

	/*将原因信息填充*/
	for (int i = 0; i < AppReasonNum; i++)	
	{  
    LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();                              
 		tLLAppClaimReasonSchema.setReasonCode(tNum[i]);        //原因代码                                                         
		//tLLAppClaimReasonSchema.setReason(appResonName[i]);                //申请原因                                             
		tLLAppClaimReasonSchema.setCustomerNo(request.getParameter("CustomerNo")); 
		tLLAppClaimReasonSchema.setReasonType("0");
		
		tLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);
	}
	
	//事件信息
	String tRNum[] = request.getParameterValues("InpEventGridChk");
	String eventno[] = request.getParameterValues("EventGrid1");
	String accdate[] = request.getParameterValues("EventGrid2");   
	String accProvinceCode[] =request.getParameterValues("EventGrid15"); // #3500 发生地点省  
	String accCityCode[] =request.getParameterValues("EventGrid16");     //发生地点市
	String accCountyCode[]=request.getParameterValues("EventGrid17");    //发生地点县
	String accplace[] = request.getParameterValues("EventGrid6");  //发生地点
	String accindate[] = request.getParameterValues("EventGrid10");   //入院日期
	String accoutdate[] = request.getParameterValues("EventGrid11");  //出院日期
	String accdesc[] = request.getParameterValues("EventGrid12");	  //事件信息
	String acctype[] = request.getParameterValues("EventGrid14");	  //事件类型

	
	System.out.println("<--submit mulline-->");
	
	if (tRNum != null) 
	{
		System.out.println("<-tRNum-->"+ tRNum.length);
  	for (int i = 0; i < tRNum.length; i++)	
  	{   
			if ( "1".equals( tRNum[i]) || eventno[i]==null|| eventno[i].equals(""))
			{
	  			System.out.println("<-eventno[i]-->"+ eventno[i]);
	  			LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
		      tLLSubReportSchema.setSubRptNo(eventno[i]);
			    tLLSubReportSchema.setCustomerNo(request.getParameter("CustomerNo"));
			    tLLSubReportSchema.setCustomerName(request.getParameter("CustomerName"));      
			    tLLSubReportSchema.setAccDate(accdate[i]);
			    tLLSubReportSchema.setAccDesc(accdesc[i]); 
			    tLLSubReportSchema.setAccProvinceCode(accProvinceCode[i]); //# 3500 发生地点省
			    tLLSubReportSchema.setAccCityCode(accCityCode[i]);      // 发生地点市
			    tLLSubReportSchema.setAccCountyCode(accCountyCode[i]);	//发生地点县
			    System.out.println("发生地点省:"+accProvinceCode[i]+","+"发生地点市："+accCityCode[i]+"发生地点县："+accCountyCode[i]+"。");
			    tLLSubReportSchema.setAccPlace( accplace[i]);  
			    tLLSubReportSchema.setInHospitalDate( accindate[i]);
			    tLLSubReportSchema.setOutHospitalDate( accoutdate[i]); 
			    tLLSubReportSchema.setAccidentType(acctype[i]);
					
					tLLSubReporSet.add(tLLSubReportSchema);
			}
		}
	}
	/**************************************************/ 
	System.out.println("lllllllllll"+tLLCaseSchema.getRgtType());
    tLLCaseSchema.setRgtState("13");//案件状态
    tLLCaseSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLCaseSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLCaseSchema.setPostalAddress(request.getParameter("RgtantAddress"));
    tLLCaseSchema.setMobilePhone(request.getParameter("MobilePhone"));
    tLLCaseSchema.setDeathDate(request.getParameter("DeathDate"));
    tLLCaseSchema.setCustBirthday(request.getParameter("CBirthday"));
    tLLCaseSchema.setCustomerSex(request.getParameter("Sex"));
    //tLLCaseSchema.setAccidentType(request.getParameter("CaseOrder"));
    tLLCaseSchema.setIDType(request.getParameter("tIDType")); 
    tLLCaseSchema.setIDNo(request.getParameter("tIDNo"));
    tLLCaseSchema.setCustState(request.getParameter("CustStatus"));
    tLLCaseSchema.setCaseGetMode(request.getParameter("paymode"));
    tLLCaseSchema.setBankCode(request.getParameter("BankCode"));
    tLLCaseSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLCaseSchema.setAccName(request.getParameter("AccName"));
    tLLCaseExtSchema.setCaseNo(request.getParameter("CaseNo"));//2807领款人和被保人关
    System.out.println("被保险人关系!!!!!!"+tLLCaseExtSchema.getCaseNo());
    tLLCaseExtSchema.setRelaDrawerInsured(request.getParameter("RelaDrawerInsured"));//2807领款人和被保人关
  //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求。
    tLLCaseExtSchema.setDrawerIDType(request.getParameter("DrawerIDType"));
    tLLCaseExtSchema.setDrawerID(request.getParameter("DrawerID"));
  //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
    tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
    tLLCaseSchema.setSurveyFlag("0");
    tLLCaseSchema.setAccdentDesc(request.getParameter("ContRemark"));
    tLLCaseSchema.setOtherIDNo(request.getParameter("OtherIDNo"));
    tLLCaseSchema.setOtherIDType("5");
    if(request.getParameter("RiskCode").equals("1605"))
	    tLLCaseSchema.setRiskCode(request.getParameter("RiskCode"));
		
		//保存案件时效
		tLLCaseOpTimeSchema.setRgtState("01");
		tLLCaseOpTimeSchema.setStartDate(request.getParameter("OpStartDate"));
		tLLCaseOpTimeSchema.setStartTime(request.getParameter("OpStartTime"));
    
	//医保卡报案号关联
    String ContNo = request.getParameter("ContNo");
	System.out.println("Test------ContNo------"+ContNo);
    if(!"".equals(ContNo)&&ContNo!=null){
    	//添加险种校验
    	String riskcodecheck = "select riskcode from lcpol where contno= '"+ContNo+"' "
    						   +" union select riskcode from lbpol where contno= '"+ContNo+"' "
    						   +" fetch first 1 rows only with ur";
    	SSRS tSSRS0 = new ExeSQL().execSQL(riskcodecheck);
    	if(!"".equals(tSSRS0) && tSSRS0 != null && tSSRS0.getMaxRow()>0){
    		String riskcode = tSSRS0.GetText(1,1);
        	System.out.println("riskcode--------"+riskcode);
        	if("123202".equals(riskcode)||"220602".equals(riskcode)){
        		String sql = "select distinct y.consultno from ybkreport y,lccont lc,YBK_N01_LIS_ResponseInfo ybk"
        				+" where  y.PolicySequenceNo = ybk.PolicySequenceNo" 
        				+" and ybk.prtno = lc.prtno"
        				+" and lc.contno= '"+ContNo
        				+"' order by y.consultno desc"
        				+" fetch first 1 rows only"
        				+" with ur";
        		SSRS tSSRS = new ExeSQL().execSQL(sql);
	        	if(!"".equals(tSSRS) && tSSRS != null && tSSRS.getMaxRow()>0){
	        		String NoticeNo = tSSRS.GetText(1,1);
	        		System.out.println("YBK------NoticeNo------"+NoticeNo);
	            	tLLCaseExtSchema.setConsultNo(NoticeNo);
	        	}
        	}
    	}
    }
  try                                 
  {                                   
  // 准备传输数据 VData               
	//VData传送
		System.out.println("<--Star Submit VData-->");
		tVData.add(tLLAppClaimReasonSet);
		tVData.add(tLLCaseSchema);
		tVData.add(tLLCaseExtSchema);//2807领款人和被保人关
		tVData.add(tLLRegisterSchema);
		tVData.add(tLLAppealSchema);
		tVData.add( tLLSubReporSet );
		tVData.add(tLLCaseOpTimeSchema);

  	tVData.add(tG);
  	System.out.println("<--Into tClientRegisterBL-->");
    tClientRegisterBL.submitData(tVData,strOperate);
  } 
  catch(Exception ex)
  { 
    Content = "保存失败，原因是: " + ex.toString();
    FlagStr = "Fail";
  } 
    
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if ("".equals(FlagStr))
  { 
    tError = tClientRegisterBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	tVData.clear();
    	tVData=tClientRegisterBL.getResult();              
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理
  LLCaseSchema mLLCaseSchema = new LLCaseSchema(); 
  
  mLLCaseSchema.setSchema(( LLCaseSchema )tVData.getObjectByObjectName( "LLCaseSchema", 0 ));
      LLFirstDutyFilterBL tFirstDutyFilterBL = new LLFirstDutyFilterBL();
      TransferData CaseNo = new TransferData();
      CaseNo.setNameAndValue("CaseNo",mLLCaseSchema.getCaseNo());
      VData tVData1 = new VData();
		try{
			tVData1.add(CaseNo);
			tVData1.add(tG);
			System.out.println("tG"+mLLCaseSchema.getCaseNo());
			tFirstDutyFilterBL.submitData(tVData1, "INSERT");
		}
		catch (Exception ex){
			Content = "保存失败，原因是: " + ex.toString();
			FlagStr = "Fail";
		}
		            tVData1.clear();
  System.out.println("结束了");
  //作废续期应收
  TransferData tTransfer = new TransferData();
  tTransfer = (TransferData) tVData.getObjectByObjectName("TransferData", 0);
  if (tTransfer != null) {
	  String tIndiInfo = (String)tTransfer.getValueByName("IndiInfo");
	  System.out.println(tIndiInfo);
	  System.out.println(Content);
	  Content = Content + tIndiInfo;
	  System.out.println(Content);
  }
  %>
	   <script language="javascript">
	    parent.fraInterface.fm.all("CaseNo").value = "<%=mLLCaseSchema.getCaseNo()%>";
	    parent.fraInterface.fm.all("RgtNo").value = "<%=mLLCaseSchema.getRgtNo()%>";
	    parent.fraInterface.fm.all("Handler").value = "<%=mLLCaseSchema.getOperator()%>";
	    parent.fraInterface.fm.all("ModifyDate").value = "<%=mLLCaseSchema.getModifyDate()%>";
		</script>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


<!--  
    tLLRegisterSchema.setRgtClass(request.getParameter(""));        //申请类型 0个人 1团体 2家庭单
    tLLRegisterSchema.setApplyerType(request.getParameter(""));     //申请人身份1 -- 被保险人、2 -- 事故收益人/继承人，3 -- 监护人、4委托人9 -- 其他
    tLLRegisterSchema.setRgtantSex(request.getParameter(""));       //生请人姓名
    tLLRegisterSchema.setRgtantMobile(request.getParameter(""));    //申请人手机
    //tLLRegisterSchema.setRgtDate(request.getParameter(""));       	//立案日期
    tLLRegisterSchema.setAccidentReason(request.getParameter(""));	//出险原因
    tLLRegisterSchema.setAccidentCourse(request.getParameter(""));	//出险过程和结果
    tLLRegisterSchema.setAccidentReason(request.getParameter(""));	//出险原因
    tLLRegisterSchema.setHandler1(request.getParameter(""));      	//经办人        
    tLLRegisterSchema.setHandler1Phone(request.getParameter("")); 	//经办人联系电话
    tLLRegisterSchema.setAccidentDate(request.getParameter(""));  	//出险日期      
    tLLRegisterSchema.setRgtReason(request.getParameter(""));     	//立案撤销原因  
    tLLRegisterSchema.setAppPeoples(request.getParameter(""));    	//申请人数    
    tLLRegisterSchema.setGetMode(request.getParameter(""));       	//赔付金领取方式
    tLLRegisterSchema.setGetIntv(request.getParameter(""));       	//赔付金领取间隔
    tLLRegisterSchema.setCaseGetMode(request.getParameter(""));   	//保险金领取方式
    tLLRegisterSchema.setRemark(request.getParameter(""));        	//备注          
    tLLRegisterSchema.setHandler(request.getParameter(""));       	//审核人        
    tLLRegisterSchema.setTogetherFlag(request.getParameter(""));  	//统一给付标记  
    tLLRegisterSchema.setRptFlag(request.getParameter(""));       	//报案标志      
    tLLRegisterSchema.setCalFlag(request.getParameter(""));       	//核算标记      
    tLLRegisterSchema.setUWFlag(request.getParameter(""));        	//核赔标记      
    tLLRegisterSchema.setDeclineFlag(request.getParameter(""));   	//拒赔标记      
    tLLRegisterSchema.setEndCaseFlag(request.getParameter(""));   	//结案标记      
    tLLRegisterSchema.setEndCaseDate(request.getParameter(""));   	//结案日期      
    tLLRegisterSchema.setMngCom(request.getParameter(""));        	//管理机构      
    tLLRegisterSchema.setClmState(request.getParameter(""));      	//赔案状态      
    tLLRegisterSchema.setBankCode(request.getParameter(""));      	//银行编码      
    tLLRegisterSchema.setBankAccNo(request.getParameter(""));     	//银行帐号      
    tLLRegisterSchema.setAccName(request.getParameter(""));       	//银行帐户名
*/    
-->
