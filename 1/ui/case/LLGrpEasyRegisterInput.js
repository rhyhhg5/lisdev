/*******************************************************************************
* Name: LLGrpEasyRegisterInput.js
* Function:简易理赔团体立案主界面的函数处理程序
* Author:MN
*/
var showInfo;
var mDebug="0";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

function main_init(A_jsp,Target_Flag,T_jsp,Info)
{
  var i = 0;
  var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action ="./"+A_jsp;
  //alert("target标志是"+Target_Flag);
  if(Target_Flag=="1"){
    fm.target = "./"+T_jsp;
  }
  else{
    fm.target = "fraSubmit";
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "paymode"){
    if(Field.value=="1"||Field.value=="2"){
      titleBank.style.display='none';
      titleBankCode.style.display='none';
    }
    else{
      titleBank.style.display='';
      titleBankCode.style.display='';
    }
  }
}

//显示特约信息

function showRemark() {

  var arrResult = new Array();
  var polResult = new Array();
  var contResult = new Array();
  var temp="";
  var strSQL="select '保单'||grpcontno||'约定:'||remark from lcgrpcont "+
  "where grpcontno='"+fm.GrpContNo.value+"'";
  arrResult=easyExecSql(strSQL);

  var polSQL="Select '保障计划'||CONTPLANCODE||'下险种'||RISKCODE||coalesce((select codename from ldcode where codetype='CalTitle' and code=c.calfactor),'')||'约定:'||remark FROM LCCONTPLANDUTYPARAM c "+
  " where grpcontno='"+fm.GrpContNo.value+"' AND CONTPLANCODE!='11' and remark !='' "+
  "GROUP BY CONTPLANCODE,RISKCODE,remark,calfactor";
  polResult=easyExecSql(polSQL);

  /**
  ****************
  if(arrResult != null&&polResult !=null)
  {

  try{
  temp=""+polResult;
  fm.all('GrpRemark').value=arrResult+"\n"+temp.replace(/\,/g,"\n");

  }
  catch(ex){
  alert(ex.message)}
  }else
  {
  try{fm.all('GrpRemark').value="无特别约定，请依照条款处理"}catch(ex){alert(ex.message)}
  }

  ******************
  */
  if(arrResult != null&&arrResult != ''){
    try{
      temp += arrResult+"\n";
    }
    catch(ex){
      alert(ex.message)
    }
  }
  if(polResult !=null&&polResult != ''){
    try{
      temp += polResult+"\n";
    }
    catch(ex){
      alert(ex.message)
    }
  }

  fm.all('GrpRemark').value=temp.replace(/\,/g,"\n");
  if(temp==""){
    fm.all('GrpRemark').value="无特别约定，请依照条款处理";
  }
}

//提交，保存按钮对应操作
//添加控制，不能对其点击两次的控制

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
 fm.asksave1.disabled=false;
 fm.GrpEnd.disabled=false;
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try{
    initForm();
  }
  catch(re){
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}


//打印理赔申请书的函数
function PLPsqs()
{
  main_init("PLPsqs.jsp","1","f1print","打印");
  showInfo.close();
}
//连接报案查询界面的函数
function RgtQueryRpt()
{
  tSaveFlag = "Rpt";
  showInfo=window.open("FrameReportQuery.jsp");
}

/*******************************************************************************
 * Name         :dealCaesGetMode
 * Author       :LiuYansong
 * CreateDate   :2003-12-16
 * ModifyDate   :
 * Function     :处理保险金领取方式的函数，若是4或7则要录入关于银行的一些信息
 *
 */
function dealCaseGetMode()
{
    if(fm.BankCode.value==""){
      alert("请您录入开户银行的信息！！！");
      return false;
    }
    if(fm.AccName.value==""){
      alert("请您录入户名信息！！！！");
      return false;
    }
    if(fm.BankAccNo.value==""){
      alert("请您录入账户信息！！！");
      return false;
    }
}

function afterQuery( rptNo )
{
	
}
	
function submitFormSurvery()
{
  var varSrc ="";
   showInfo = window.open("./FrameMainRgtSurvey.jsp?Interface=LLSurvey.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function GrpQuery()
{
	if(fm.all('CustomerNo').value!=null && fm.all('CustomerNo').value!='' || 
		(fm.all('GrpContNo').value!=null && fm.all('GrpContNo').value!='') ||
		(fm.all('GrpName').value!=null && fm.all('GrpName').value!='') ){
		var arrResult = new Array();
		var strSQL="select  a.customerno, a.name, g.grpcontno,g.Peoples2, a.claimbankcode, a.claimbankaccno, a.claimaccname, a.AddressNo,'', b.riskcode" +
					" from lcgrpcont g, LCGrpAppnt a,lcgrppol b" +
					" where a.grpcontno = g.grpcontno and g.appflag = '1' and b.grpcontno=g.grpcontno "
				+ getWherePart("g.AppntNo", "CustomerNo" )
				+ getWherePart("a.Name", "GrpName" )
				+ getWherePart( "g.GrpContNo","GrpContNo" );
				+ getWherePart("g.AppntNo", "CustomerNo" )
				+ getWherePart("a.Name", "GrpName" )
				+ getWherePart( "g.GrpContNo","GrpContNo" );
	    arrResult=easyExecSql(strSQL);
	    if(arrResult != null){
	    	if(arrResult.length==1){
		    	try{fm.all('CustomerNo').value=arrResult[0][0]}catch(ex){alert(ex.message+"GrpNo")}
		    	try{fm.all('GrpName').value=arrResult[0][1]}catch(ex){alert(ex.message+"ReturnGrpName")}		    	
		    	try{fm.all('GrpContNo').value=arrResult[0][2]}catch(ex){alert(ex.message+"GrpContNo")}
		    	try{fm.all('PeopleNo').value=arrResult[0][3]}catch(ex){alert(ex.message+"PeopleNo")}
		    	try{fm.all('BankCode').value=arrResult[0][4]}catch(ex){alert(ex.message+"BankCode")}
		    	try{fm.all('BankAccNo').value=arrResult[0][5]}catch(ex){alert(ex.message+"BankAccNo")}
		    	try{fm.all('AccName').value=arrResult[0][6]}catch(ex){alert(ex.message+"AccName")}
		    	try{fm.all('CaseGetMode').value=arrResult[0][8]}catch(ex){alert(ex.message+"CaseGetMode")}
		    	switch (arrResult[0][8])
		    	{
		    		case '1': fm.all('CaseGetModeName').value='现金';break;
		    		case '2': fm.all('CaseGetModeName').value='现金支票';break;
		    		case '4': fm.all('CaseGetModeName').value='银行转账';
		    	}
		    	if(fm.all('PeopleNo').value == null ||fm.all('PeopleNo').value == "0"){
		    		alert("投保人数为空！");
		    		return;
		    	} 
		    	var addressno = "";
		    	try{addressno=arrResult[0][7]}catch(ex){alert(ex.message+"addressno")}
		    	try{fm.all('RiskCode').value=arrResult[0][9]}catch(ex){alert(ex.message+"RiskCode")}
		    	
	    		strSQL="select riskcode from lmriskapp where risktype3='7' and riskcode='"+fm.RiskCode.value+"'";
					var arr = easyExecSql(strSQL);
					if(arr)
					{
						tSQL = "select insuaccbala from lcinsureacc a,lcpol b where a.POLNO=b.POLNO and b.grpcontno='"+
									fm.GrpContNo.value+"' and poltypeflag='2' AND a.INSUACCNO =(select INSUACCNO  from lmrisktoacc"+
									" where INSUACCName='团体特需医疗理赔帐户' and riskcode='"+fm.RiskCode.value+"')";
						Brr=easyExecSql(tSQL);
						fm.AccBala.value = Brr[0][0];
						idGrpAcc.style.display='';   	
						idGrpAccBala.style.display='';
					}else{
							idGrpAcc.style.display='none';   	
							idGrpAccBala.style.display='none';
						}
		    	strSQL=	"select linkman1,phone1,GrpAddress from LCGrpAddress " +
		    		"where customerno = '" + fm.all('CustomerNo').value +
		    		"' and addressno = '" + addressno + "'";
	    	    	arrResult=easyExecSql(strSQL);
	    	    	if(arrResult != null) 
	    	    	{
		    	    	try{fm.all('RgtantName').value=arrResult[0][0]}catch(ex){alert(ex.message+"LinkMan")}
		    	    	try{fm.all('RgtantPhone').value=arrResult[0][1]}catch(ex){alert(ex.message+"TelPhone")}
		    	    	try{fm.all('RgtantAddress').value=arrResult[0][2]}catch(ex){alert(ex.message+"GrpAddress")}
	    	    	}
			showRemark();
	    	}
		else
		{
   			var varSrc = "&CustomerNo=" + fm.CustomerNo.value;
      			varSrc += "&GrpContNo=" + fm.GrpContNo.value;
      			varSrc += "&GrpName=" + fm.GrpName.value;
      			showInfo = window.open("./FrameMainLCGrpQuery.jsp?Interface= LCGrpQuery.jsp"+varSrc,"LCGrpQuery",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0'); 
      						
		} 
	    }
	    else
	    {
			alert("没有符合条件的数据！");
			fm.all('PeopleNo').value = "";
			return;
	    }
	}
	else
	{
		alert("请输入查询条件");
	}

	
}

/*申请保存*/
function AskSave()
{	 
	  if(fm.GrpRemark.value.length>1600)
	  {
	  	alert("团体保单特约信息超过1600个汉字,无法保存!");
	  	return;
	  	}
		if(fm.all('CustomerNo').value!=null && fm.all('CustomerNo').value!='' || 
		(fm.all('GrpContNo').value!=null && fm.all('GrpContNo').value!='') ||
		(fm.all('GrpName').value!=null && fm.all('GrpName').value!='') ){
		var arrResult = new Array();
		var strSQL="select  a.customerno, a.name, g.grpcontno,g.Peoples2, a.claimbankcode, a.claimbankaccno, a.claimaccname, a.AddressNo,'', b.riskcode" +
					" from lcgrpcont g, LCGrpAppnt a,lcgrppol b" +
					" where a.grpcontno = g.grpcontno and g.appflag = '1' and b.grpcontno=g.grpcontno"
				+ getWherePart("g.AppntNo", "CustomerNo" )
				+ getWherePart("a.Name", "GrpName" )
				+ getWherePart( "g.GrpContNo","GrpContNo" );
				+ getWherePart("g.AppntNo", "CustomerNo" )
				+ getWherePart("a.Name", "GrpName" )
				+ getWherePart( "g.GrpContNo","GrpContNo" );
			arrResult=easyExecSql(strSQL);
	    if(arrResult != null){
	    }
	    else
	    {
			alert("没有该团体客户信息！");
			return;
	    }
	}
	
	//校验各种保全
	if(!checkBQGrp()){
		return false;
	}
	
	if(fm.IDType.value=="0" ){
		if(!checkIdCard(fm.IDNo.value))
  		return false;
	}
	var i = 0;
	if(fm.all('RgtNo').value != null && fm.all('RgtNo').value != "")
	{
//		alert("团体申请已经保存过！");
//		return;
	}
	if( verifyInput2() == false ) return false;
	if(fm.AppPeoples.value<fm.PeopleNo.value){
		if(!confirm("您输入申请人数小于投保人数，确认吗？"))
		return false;
		}
	if(fm.all('CaseGetMode').value != "1"&&fm.all('CaseGetMode').value != "2"&&(fm.all('TogetherFlag').value=="3"||fm.all('TogetherFlag').value=="2"))
	{
		if(fm.BankCode.value==""){
      alert("请您录入开户银行的信息！！！");
      return false;
    }
    if(fm.AccName.value==""){
      alert("请您录入户名信息！！！");
      return false;
    }
    if(fm.BankAccNo.value==""){
      alert("请您录入账户信息！！！");
      return false;
    }
		if(fm.CaseGetMode.value=='4'){
    	var tBankSQL="SELECT * FROM ldbank WHERE bankcode='"+fm.BankCode.value+"' AND cansendflag='1'";
    	if (!easyExecSql(tBankSQL)>0){
    		if(confirm("该银行不支持银行转帐，是否修改为银行汇款")){
    			fm.CaseGetMode.value='11';
    		} else {
    		  return false;
    	  }
    	}
    }
	}
	var rgtN1 = fm.RgtantName.value;
	var rgtTel1 = fm.RgtantPhone.value;  
    var len = 0,len1 = 0;  
    if(rgtN1!=null){ 
    for (var i=0; i<rgtN1.length; i++) {   
        if (rgtN1.charCodeAt(i)>127 || rgtN1.charCodeAt(i)==94) {   
            len += 3;   
        } else {   
            len ++;   
        }   
    }   
  
	
	 if(len>60){
	 alert("联系人名字过长。");
	 return;
	 }
	}
	if(rgtTel1!=null){
	for (var i=0; i<rgtTel1.length; i++) {   
        if (rgtTel1.charCodeAt(i)>127 || rgtTel1.charCodeAt(i)==94) {   
            len1 += 3;   
        } else {   
            len1 ++;   
        }   
    }   
	 if(len1.length>18){
	 alert("联系电话长度过长。");
	 return;
	 }
	}
	fm.fmtransact.value="INSERT||MAIN";
	fm.asksave1.disabled=true;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('operate').value = "INSERT||MAIN";
	fm.action = "./LLGrpRegisterSave.jsp";

	fm.submit();
	
	
}

function OpenClientInccident(){
 	if(fm.RgtNo.value==null||fm.RgtNo.value==""){
  	  alert("没有团体申请信息！");
  	  return ;
  }
  	
  var varSrc= "&RgtNo="+fm.RgtNo.value;
  var pathStr="./FrameMainAA.jsp?Interface=LLEasyRegisterInput.jsp"+varSrc;
  var newWindow=OpenWindowNew(pathStr,"个人录入","left");
}

//判断查询输入窗口的案件类型是否是回车， 
//如果是回车调用查询客户函数
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		GrpQuery();
	}
	
}


function QueryOnKeyDown_RgtNo()
{
	var keycode = event.keyCode;

	if(keycode=="13")
	{
		doSearchShow();
		queryGrpCaseGrid();
	}
}

function doSearchShow()
{
	var jsRgtNo = fm.all('RgtNo').value;
	if(jsRgtNo == null || jsRgtNo == "") 
	{
		alert("请输入团体批次号！");
		return;
	}
	 var strSQL=" SELECT "
							+"(select codename from ldcode where ldcode.codetype='llaskmode' and ldcode.code=llregister.RgtType),"
	 						+"APPPEOPLES,RGTANTNAME,RGTANTPHONE,RGTANTADDRESS,POSTCODE,"
							+"(select codename from ldcode where ldcode.codetype='lltogetherflag' and ldcode.code=llregister.TOGETHERFLAG),"
	 						+"REMARK,HANDLER1,RGTDATE, " +
					 		"BANKCODE, BANKACCNO, ACCNAME, "
							+"(select codename from ldcode where ldcode.codetype='llgetmode' and ldcode.code=llregister.CaseGetMode),"
							+"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llregister.idtype),"
	 						+"IDNo, CustomerNo, GrpName, RgtObjNo,rgttype,togetherflag,CaseGetMode,idtype,APPAMNT,rgtantmobile,email  " +
	 		" FROM LLREGISTER WHERE RGTNO = '" + jsRgtNo + "'" ;
	
	 var arr = easyExecSql(strSQL);

	 if (arr==null) {
	 	 alert("团体立案信息查询失败！");
	 	 return;
	}
	
	 if (arr!=null)
	 {
	 	
	 	arr[0][0]==null||arr[0][0]=='null'?'0':fm.RgtTypeName.value=arr[0][0];
	 	arr[0][1]==null||arr[0][1]=='null'?'0':fm.AppPeoples.value=arr[0][1];
	 	arr[0][2]==null||arr[0][2]=='null'?'0':fm.RgtantName.value=arr[0][2];
	 	arr[0][3]==null||arr[0][3]=='null'?'0':fm.RgtantPhone.value=arr[0][3];
	 	arr[0][4]==null||arr[0][4]=='null'?'0':fm.RgtantAddress.value=arr[0][4];
	 	arr[0][5]==null||arr[0][5]=='null'?'0':fm.PostCode.value=arr[0][5];
	 	arr[0][6]==null||arr[0][6]=='null'?'0':fm.TogetherFlagName.value=arr[0][6];
	 	arr[0][7]==null||arr[0][7]=='null'?'0':fm.Remark.value=arr[0][7];
	 	arr[0][8]==null||arr[0][8]=='null'?'0':fm.Handler.value=arr[0][8];
	 	arr[0][9]==null||arr[0][9]=='null'?'0':fm.ModifyDate.value=arr[0][9];
    arr[0][10]==null||arr[0][10]=='null'?'0':fm.BankCode.value=arr[0][10];
	 	arr[0][11]==null||arr[0][11]=='null'?'0':fm.BankAccNo.value=arr[0][11];
	 	arr[0][12]==null||arr[0][12]=='null'?'0':fm.AccName.value=arr[0][12];	 	 	
	 	arr[0][13]==null||arr[0][13]=='null'?'0':fm.CaseGetModeName.value=arr[0][13];	 
	 	arr[0][14]==null||arr[0][14]=='null'?'0':fm.IDTypeName.value=arr[0][14];
	 	arr[0][15]==null||arr[0][15]=='null'?'0':fm.IDNo.value=arr[0][15];	 		 		
		arr[0][16]==null||arr[0][16]=='null'?'0':fm.CustomerNo.value=arr[0][16];
		arr[0][17]==null||arr[0][17]=='null'?'0':fm.GrpName.value=arr[0][17];
		arr[0][18]==null||arr[0][18]=='null'?'0':fm.GrpContNo.value=arr[0][18];
	 	arr[0][19]==null||arr[0][19]=='null'?'0':fm.RgtType.value=arr[0][19];
	 	arr[0][20]==null||arr[0][20]=='null'?'0':fm.TogetherFlag.value=arr[0][20];
	 	arr[0][21]==null||arr[0][21]=='null'?'0':fm.CaseGetMode.value=arr[0][21];	 
	 	arr[0][22]==null||arr[0][22]=='null'?'0':fm.IDType.value=arr[0][22];
	 	arr[0][23]==null||arr[0][22]=='null'?'0':fm.AppAmnt.value=arr[0][23];
	 	arr[0][24]==null||arr[0][24]=='null'?'0':fm.Mobile.value=arr[0][24];
		arr[0][25]==null||arr[0][25]=='null'?'0':fm.Email.value=arr[0][25];
	}
	strSQL="select peoples2 from lcgrppol where GrpContNo='"+fm.GrpContNo.value+"'";
	var err = easyExecSql(strSQL);
	if(err)
	{
		fm.PeopleNo.value=err[0][0];
	}

	showRemark();
}

function queryGrpCaseGrid()
{
	var jsRgtNo = fm.all('RgtNo').value;
	if(jsRgtNo == null || jsRgtNo == "") 
		return;
	 	
	  var strSql = " SELECT C.CASENO, C.CUSTOMERNO, C.CUSTOMERNAME, C.RGTDATE, b.codename" + 
	  	       " FROM LLCASE C, ldcode b  " + 
	  	       " WHERE C.RGTNO = '" + jsRgtNo + "'" +
	  	       " and b.codetype='llrgtstate' and b.code = c.rgtstate "
	  	       " ORDER BY C.CASENO ";
	  turnPage.pageLineNum=5;
	  turnPage.queryModal(strSql,GrpCaseGrid);
	  divGrpCaseInfo.style.display='';
}

function afterLCGrpQuery( arrReturn )
{

		if(arrReturn.length==1) 
		{
	  	try{fm.all('CustomerNo').value=arrReturn[0][0]}catch(ex){alert(ex.message+"GrpNo")}
	  	try{fm.all('GrpName').value=arrReturn[0][1]}catch(ex){alert(ex.message+"ReturnGrpName")}		    	
	  	try{fm.all('GrpContNo').value=arrReturn[0][2]}catch(ex){alert(ex.message+"GrpContNo")}
	  	try{fm.all('PeopleNo').value=arrReturn[0][3]}catch(ex){alert(ex.message+"PeopleNo")}
	  	try{fm.all('BankCode').value=arrReturn[0][4]}catch(ex){alert(ex.message+"BankCode")}
	  	try{fm.all('BankAccNo').value=arrReturn[0][5]}catch(ex){alert(ex.message+"BankAccNo")}
	  	try{fm.all('AccName').value=arrReturn[0][6]}catch(ex){alert(ex.message+"AccName")}

	  	if(fm.all('PeopleNo').value == null ||
	  		fm.all('PeopleNo').value == "0")
	  	{
	  		alert("投保人数为空！");
	  		return;
	  	} 
	  	var addressno = "";
	  	try{addressno=arrReturn[0][7]}catch(ex){alert(ex.message+"addressno")}
	  	strSQL=	"select linkman1,phone1,GrpAddress from LCGrpAddress " +
	  		"where customerno = '" + fm.all('CustomerNo').value +
	  		"' and addressno = '" + addressno + "'";
		    	arrReturn=easyExecSql(strSQL);
		    	if(arrReturn != null) 
		    	{
	  	    	try{fm.all('RgtantName').value=arrReturn[0][0]}catch(ex){alert(ex.message+"AccName")}
	  	    	try{fm.all('RgtantPhone').value=arrReturn[0][1]}catch(ex){alert(ex.message+"AccName")}	    	    		
	  	    	try{fm.all('RgtantAddress').value=arrReturn[0][2]}catch(ex){alert(ex.message+"GrpAddress")}

		    	}
		    	
		}	
		showRemark();	
}

function GrpPrt()
{
  main_init("../f1print/GrpPayColPrt.jsp","1","f1print","打印");
  showInfo.close();
}

function DealCancel()
{
	var varSrc="&CaseNo=" + fm.RgtNo.value +"&CustomerNo="+fm.CustomerNo.value+"&CustomerName="+fm.GrpName.value+"&LoadFlag=2";
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function ShowScan()
{
  var arrResult = new Array();
  if(fm.GrpContNo.value!=""&&fm.GrpContNo.value!=null){
    var	strSQL="select prtno from lcgrpcont where grpcontno='"
    +fm.GrpContNo.value+"'";
    arrResult=easyExecSql(strSQL);
    if(arrResult != null){
      if(arrResult.length==1){
        var tPrtNo=arrResult[0];
        window.open("../sys/ProposalEasyScan.jsp?prtNo="+ tPrtNo + "&SubType=&BussType=&BussNoType=" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
      }
    }
    else{
      alert("没有符合条件的数据！")
    }
  }
  else{
    alert("请先指定保单！");
  }
}

function SearchGrpRegister()
{
  var strSql = " SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE, R.APPPEOPLES, b.codename " +
  " FROM LLREGISTER R , ldcode b " +
  " WHERE R.RGTOBJ = '0' AND R.RGTCLASS = '1' and R.declineflag is null " +
  " AND RGTSTATE in ('01','02') and b.codetype='llgrprgtstate' and b.code = r.rgtstate "
  + getWherePart("r.rgtdate","StartDate",">=")
  + getWherePart("r.rgtdate","EndDate","<=")
  + getWherePart("R.applyertype","ApplyerType")
  + getWherePart("r.MngCom","ManageCom","like")
  + " order by r.rgtno desc";
  turnPage1.queryModal(strSql,GrpRegisterGrid);
}

function getRegisterInfo(){
  var selno = GrpRegisterGrid.getSelNo();
  if (selno <= 0){
    return ;
  }
  fm.RgtNo.value = GrpRegisterGrid.getRowColData(selno - 1, 1);
  doSearchShow();
  queryGrpCaseGrid();
}

function getCaseInfo(){
  var selno = GrpCaseGrid.getSelNo();
  if (selno <= 0){
    return ;
  }
  fm.CaseNo.value = GrpCaseGrid.getRowColData(selno - 1, 1);
  var varSrc= "&RgtNo="+fm.RgtNo.value+"&CaseNo="+fm.CaseNo.value;
  var pathStr="./FrameMainAA.jsp?Interface=LLEasyRegisterInput.jsp"+varSrc;
  var newWindow=OpenWindowNew(pathStr,"个人录入","left");
}

function InsureAffirm(){
  if(fm.RgtNo.value==null||fm.RgtNo.value==""){
    alert("没有团体申请信息！");
    return ;
  }
  var varSrc= "&RgtNo="+fm.RgtNo.value+"&GrpContNo="+fm.GrpContNo.value+"&RiskCode="+fm.RiskCode.value+"&AppPeoples="+fm.AppPeoples.value;
	pathStr="./FrameMainSim.jsp?Interface=InsuredAffirmInput.jsp"+varSrc;
  var newWindow=OpenWindowNew(pathStr,"简易录入","left");
}

function EndGrp(){
	if (fm.RgtNo.value==null||fm.RgtNo.value==""){
		alert("请选择批次");
		return ;
	}
	fm.fmtransact.value="UPDATE||RgtEnd";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('operate').value = "UPDATE||RgtEnd";
	fm.action = "./LLGrpEasyRegisterSave.jsp";
  fm.GrpEnd.disabled=true;
	fm.submit();
	
}

//by gzh 20120913 增加对保全的处理
function checkBQGrp(){
	var tError = "";//非阻断错误
	var tZDError = "";//阻断错误
	var grpcontno=fm.GrpContNo.value;
	//提示不阻断
	var tIngSql = " select b.grpcontno,b.edortype,b.edorno,"
			 + " (select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) "
			 + " from lpedorapp a , lpgrpedoritem b "
			 + " where a.edoracceptno= b.edoracceptno "
			 + " and a.edorstate !='0' " //正在进行中的保全项目
			 + " and b.grpcontno ='"+grpcontno+"'"
			 + " and b.edortype in ('RR','RS','AC','WJ','YS')"
			 + " group by b.grpcontno,b.edortype,b.edorno ";
	var tIngArr = easyExecSql(tIngSql);
	if(tIngArr){
		for(var i=0;i<tIngArr.length;i++){
			//提示不阻断
			tError +="该团单正在进行"+tIngArr[i][3]+"保全操作,工单号为："+tIngArr[i][2]+"。 \n";
		}
	}
	//提示阻断
	var tIngSql1 = " select b.grpcontno,b.edortype,b.edorno,"
			 + " (select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) "
			 + " from lpedorapp a , lpgrpedoritem b "
			 + " where a.edoracceptno= b.edoracceptno "
			 + " and a.edorstate !='0' " //正在进行中的保全项目
			 + " and b.grpcontno ='"+grpcontno+"'"
			 + " and b.edortype in ('WT','XT','GA','TQ','TA','SG','CT','TF','LQ','ZB')"
			 + " group by b.grpcontno,b.edortype,b.edorno ";
	var tIngArr1 = easyExecSql(tIngSql1);
	if(tIngArr1){
		for(var i=0;i<tIngArr1.length;i++){
			tZDError +="该团单正在进行"+tIngArr1[i][3]+"保全操作,工单号为："+tIngArr1[i][2]+"。 \n";
		}
	}
	//已做完保全项目	
	var tDoneSql = " select b.grpcontno,b.edortype,b.edorno,"
			 + " (select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) "
			 + " from lpedorapp a , lpgrpedoritem b "
			 + " where a.edoracceptno= b.edoracceptno "
			 + " and a.edorstate ='0' " //已经完成的保全项目
			 + " and b.grpcontno ='"+grpcontno+"'"
			 + " and b.edortype in ('RS','WT','XT','CT')"
			 + " group by b.grpcontno,b.edortype,b.edorno ";
	var tDoneArr = easyExecSql(tDoneSql);
	if(tDoneArr){
		for(var i=0;i<tDoneArr.length;i++){
			tError +="该团单已做过"+tDoneArr[i][3]+"保全操作,工单号为："+tDoneArr[i][2]+"。 \n";
		}
	}
	//满期处理
	var tMJSql = " select temp.GrpContNo,temp.edorName,temp.edorstate "
	           + " from "
	           + " ( "
	           + " select a.grpcontno GrpContNo,'团单满期给付' edorName," //团单满期给付
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate"
			   + " from ljsgetdraw a where feefinatype='TF' and riskcode='170206' "
			   + " and grpcontno='"+grpcontno+"' "
			   + " union all "
			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'1' edorstate" //特需险满期给付
			   + " from lgwork a where a.typeno='070015' "
			   + " and a.contno='"+grpcontno+"' "
			   + " and a.statusno not in ('5','8') "
			   + " union all "
			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'0' edorstate" //特需险满期给付
			   + " from lgwork a where a.typeno='070015' "
			   + " and a.contno='"+grpcontno+"' "
			   + " and a.statusno = '5' "
			   + " ) as temp "
			   + " group by temp.GrpContNo,temp.edorName,temp.edorstate "
			   + " with ur "; 
	var tMJArr = easyExecSql(tMJSql);
	if(tMJArr){
		for(var i=0;i<tMJArr.length;i++){
			tTorG = "团单(保单号码："+tMJArr[i][0]+")";
			if(tMJArr[i][2] == "0"){
				tError +="该团单已做过"+tMJArr[i][1]+"保全操作。 \n";
			}else{
				tZDError +="该团单正在进行"+tMJArr[i][1]+"保全操作。 \n";
			}
		}
	}
	//所在团单正在定期结算
	var tDJSql = " select distinct a.contno "
			   + " from lgwork a "
			   + " where a.contno = '"+grpcontno+"' "
			   + " and a.contno not in (select code from ldcode where codetype='lp_dqjs_pass') "
			   + " and a.typeno like '06%' and a.statusno not in ('5','8') ";
	var tDJArr = 	easyExecSql(tDJSql);
	if(tDJArr){
		for(var i=0;i<tDJArr.length;i++){
			tZDError += "该团单正在进行定期结算保全操作。 \n";
		}
	}
	if(tError != ""){//提示不阻断处理
		tError +="是否继续？";
		if(!confirm(tError)){
			return false;
		}
	}
	if(tZDError != ""){//提示不阻断处理
		alert(tZDError);
		return false;
	}
	return true;
}
