var turnPage = new turnPageClass();  
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
//	var month = fm.MEndDate.value;
//	var year = fm.YEndDate.value;
	
//	fm.StartDate.value = fm.YStartDate.value+"-"+fm.MStartDate.value+"-1";
//	fm.EndDate.value = year+"-"+month+"-"+getDays(month, year);
	
//	fm.MngName.value = easyExecSql("select Name from ldcom where comcode = '"+fm.MngCom.value+"'");
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
  fm.action = "LLClaimCollectionRpt.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "LLClaimCollection";
	fm.submit();
	showInfo.close();
}
                                               
function GrpTypeSearch() {
	var strSQL = "select code,codename from ldcode where codetype = 'llusergroup' order by code with ur";
	turnPage.queryModal(strSQL,BusinessGrid);
}

function GrpTypeSave(operate) {
	var Count=BusinessGrid.mulLineCount;
  if(Count==0){
  	alert("没有组信息!");
  	return false;
  }
  
  var selNo = BusinessGrid.getSelNo();
  if ( selNo<1 ) {
  	alert("请选择一条组信息");
  	return false;
  }
  
//  for (var i=0;i<Count;i++) {
  	if (BusinessGrid.getRowColData(selNo-1,1)==null
  	    ||BusinessGrid.getRowColData(selNo-1,1)==""){
  	    	alert("组编码不能为空！");
  	    	return false;
    }
//  }
  
//  for (var i=0;i<Count;i++) {
  	if (BusinessGrid.getRowColData(selNo-1,2)==null
  	    ||BusinessGrid.getRowColData(selNo-1,2)==""){
  	    	alert("组名称不能为空！");
  	    	return false;
    }
//  }
	
	if (operate==1){
		fm.fmtransact.value = "DELETE&INSERT";
	} else if (operate==2) {
		fm.fmtransact.value = "DELETE||MAIN";
		var strGroupSQL = "select count(1) from ldcode1 where codetype = 'llusergroup' and code='"
		                + BusinessGrid.getRowColData(selNo-1,1) +"' with ur";
		var arrResult = easyExecSql(strGroupSQL);
		if (arrResult[0][0] > 0) {
		  alert("该组下有配置的理赔员，不能删除！");
		  return false;
		}
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "LLUserGroupSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  GrpTypeSearch();
}
function afterSubmit2( FlagStr, content ) {
  showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  GrpContSearch();
}

function GrpContSearch() {
	var strSQL = "select a.codename,a.code,b.code1,b.codename from ldcode a,ldcode1 b "
	           + " where a.codetype=b.codetype and b.codetype='llusergroup' "
	           + " and a.code=b.code "
	           + getWherePart("b.code","GrpType")
	           + getWherePart("b.code1","ClaimUserCode")
	           + " with ur ";

	turnPage.queryModal(strSQL,BusinessGrpContGrid);
}

function GrpContTypeSave(operate) {
	var Count=BusinessGrpContGrid.mulLineCount;
  if(Count==0){
  	alert("没有用户信息!");
  	return false;
  }
  
  var selNo = BusinessGrpContGrid.getSelNo();
  if ( selNo<1 ) {
  	alert("请选择用户");
  	return false;
  }
  if (BusinessGrpContGrid.getRowColData(selNo-1,2)==null
      ||BusinessGrpContGrid.getRowColData(selNo-1,2)=="") {
      	alert("请选择组");
      	return false;
  }
  
  var tGrpContNo = BusinessGrpContGrid.getRowColData(selNo-1,3);
  if (tGrpContNo==null ||tGrpContNo=="") {
      	alert("请选择用户");
      	return false;
  }
	
	for (var i=0;i<Count-1;i++) {
		for (var j=i+1;j<Count;j++) {
			if (BusinessGrpContGrid.getRowColData(i,3)
			    ==BusinessGrpContGrid.getRowColData(j,3)) {
				alert("用户"+BusinessGrpContGrid.getRowColData(j,3)+"列表中存在多条");
				return false;
			}
		}
	}
		
	if (operate==1){
		fm.fmtransact.value = "DELETE&INSERT";
	} else if (operate==2) {
		fm.fmtransact.value = "DELETE";
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.action = "LLUserGroupDetailSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();
}

