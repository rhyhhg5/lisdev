<html>
	<%
	//Name：LLMainAskInput.jsp
	//Function：登记界面的初始化
	//Date：2004-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.llcase.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLSecondUW.js"></SCRIPT>
		<SCRIPT src="ContInsuredLCImpart.js"></SCRIPT> <!--保障状况告知 ＆ 健康状况告知 引用函数-->
		<%@include file="LLSecondUWInit.jsp"%>
	</head>
	<body  onload="initForm('<%=tCaseNo%>','<%=tInsuredNo%>');" >

		<form action="./LLSecondUWSave.jsp" method=post name=fm target="fraSubmit">
			<Div align="left">
				<Table>
					<TR>
						<TD class=common>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
						</TD>
						<TD class= titleImg >
							客户信息
						</TD>
					</TR>
				</Table>
			</div>
			<div id='divLDPerson1'>
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>理赔号</TD><TD  class= input8><input class= common name="CaseNo" onkeydown="QueryOnKeyDown();" ></TD>
						<TD  class= title8>客户号</TD><TD  class= input8><input class= common name="InsuredNo" onkeydown="QueryOnEnter();"></TD>
						<TD  class= title8>客户姓名</TD><TD  class= input8><input class= common name="CustomerName" onkeydown="QueryOnEnter();"></TD>
					</TR>
				</table>
			</div>

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						客户保单信息
					</TD>
				</TR>
			</table>

			<Div  id= "divCont" align= center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanPolGrid" >
							</span>
						</TD>
					</TR>
				</table>
				<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
				<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
			</Div>
			<P>
				<DIV id=DivLCImpart STYLE="display:''">
					<!-- 告知信息部分（列表） -->
					<table>
						<tr>
							<td class=common>
								<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart0);">
							</td>
							<td class= titleImg>
								保障状况告知
							</td>
						</tr>
					</table>
				</div>

				<div  id= "divLCImpart0" style= "display: 'none'">
					<table class = common>
						<tr id=InfoRow1>
							<td>1.您目前年收入 <input name="Impartbox1" class = "common6"> 万元，主要来源为：<input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 薪资 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 营业收入 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 房屋出租 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 证券投资 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 银行利息 &nbsp&nbsp 其它 <input name="Impartbox1" class = "common6"></td>
						</tr>
						<tr id=InfoRow2>
							<td>2.目前您医疗费用支付方式: <input type="checkbox" name="Impartbox2" class="box">&nbsp&nbsp 社会医疗保险 <input type="checkbox" name="Impartbox2" class="box">&nbsp&nbsp 商业医疗保险 <input type="checkbox" name="Impartbox2" class="box">&nbsp&nbsp 自费 &nbsp&nbsp 其它 <input name="Impartbox2" class = "common6"></td>
						</tr>
						<tr id=InfoRow3>
							<td>3.您目前是否有正在生效的商业人身保险（医疗保险，重大疾病保险，意外险或寿险）产品？<!-- <input type="checkbox" name="" class="box">&nbsp&nbsp 是 <input type="checkbox" name="" class="box">&nbsp&nbsp 否<input class="common7"></td>-->
						</tr>
						<tr id=InfoRow4>
							<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox3" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox3" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox3" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate1" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate2" class = "coolDatePicker1" dateFormat="short"></td>
						</tr>
						<tr id=InfoRow5>
							<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox4" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox4" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox4" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate3" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate4" class = "coolDatePicker1" dateFormat="short"></td>
						</tr>
						<tr id=InfoRow6>
							<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox5" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox5" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox5" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate5" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate6" class = "coolDatePicker1" dateFormat="short"></td>
						</tr>
						<tr id=InfoRow7>
							<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox6" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox6" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox6" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate7" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate8" class = "coolDatePicker1" dateFormat="short"></td>
						</tr>
						<tr id=InfoRow8>
							<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox17" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox17" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox17" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate16" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate17" class = "coolDatePicker1" dateFormat="short"></td>
						</tr>
						<tr id=InfoRow9>
							<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox18" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox18" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox18" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate18" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate19" class = "coolDatePicker1" dateFormat="short"></td>
						</tr>
						<tr id=InfoRow10>
							<td>4.过去三年内，有无人身险事宜被商业保险公司拒保，延期，加费，免责的投保经历或向保险公司剔出过理赔申请？<!-- <input type="checkbox" name="" class="box">&nbsp&nbsp 是 <input type="checkbox" name="" class="box">&nbsp&nbsp 否<input class="common7"></td>-->
						</tr>
						<tr id=InfoRow11>
							<td>&nbsp&nbsp&nbsp&nbsp 时间 <input name="ImpartGridDate9" class = "coolDatePicker1" dateFormat="short"> &nbsp&nbsp  事由 <input name="Impartbox7" class = "common6"> &nbsp&nbsp 结果 <input name="Impartbox7" class = "common6"></td>
						</tr>
						<tr id=InfoRow12>
							<td>&nbsp&nbsp&nbsp&nbsp 时间 <input name="ImpartGridDate10" class = "coolDatePicker1" dateFormat="short"> &nbsp&nbsp  事由 <input name="Impartbox8" class = "common6"> &nbsp&nbsp 结果 <input name="Impartbox8" class = "common6"></td>
						</tr>
					</table>
				</div>

				<DIV id=DivLCImpart STYLE="display:''">
					<!-- 告知信息部分（列表） -->
					<table>
						<tr>
							<td class=common>
								<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart2);">
							</td>
							<td class= titleImg>
								健康状况告知
							</td>
						</tr>
					</table>
				</div>

				<div  id= "divLCImpart2" style= "display: 'none'">
					<table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
						<tr>
							<td colspan="7" class="mulinetitle2" height="20">&nbsp;</td>
						</tr>
						<tr id=InfoRow13>
							<td colspan="7" class="mulinetitle3">您是否有直接寄至我公司的健康告知表格&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp身高 <input name="Detail04" class = "common66"> 厘米(cm) &nbsp&nbsp&nbsp&nbsp 体重 <input name="Detail04" class = "common66"> 千克(kg)</td>
						</tr>
						<tr id=InfoRow14>
							<td width="104" class="mulinetitle3">5.<input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="6" class="mulinetitle3">你是否目前或曾经有吸烟的习惯？&nbsp&nbsp&nbsp&nbsp如是，请回答&nbsp&nbsp&nbsp&nbsp目前(<input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 否)吸烟&nbsp&nbsp每日吸烟量 <input name="Detail05" class = "common66"> 支&nbsp&nbsp吸烟持续时间 <input name="Detail05" class = "common66"> 年</td>
						</tr>
						<tr id=InfoRow15>
							<td class="mulinetitle3">6.<input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="6" class="mulinetitle3">你是否目前或曾经有饮酒的习惯？&nbsp&nbsp&nbsp&nbsp如是，请回答&nbsp&nbsp&nbsp&nbsp目前(<input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 否)饮酒&nbsp&nbsp种类 <input name="Detail06" class = "common66"> &nbsp&nbsp量 <input name="Detail06" class = "common66"> ml/周 &nbsp&nbsp 时间 <input name="Detail06" class = "common66"> 年</td>
						</tr>
						<tr id=InfoRow16>
							<td class="mulinetitle3">7.<input type="checkbox" name="Detail07" class="box" onclick="InputDetailToMuline07();">&nbsp&nbsp 是 <input type="checkbox" name="Detail07" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">您在过去五年内有无进行危险运动或不良嗜好？</td>
							<td width="101" class="mulinetitle3">15.<input type="checkbox" name="Detail15" class="box" onclick="InputDetailToMuline15();">&nbsp&nbsp 是 <input type="checkbox" name="Detail15" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="2" class="mulinetitle3">您在最近三个月中是否有发热、疼痛、大小便异常或者其他不适症状？</td>
						</tr>
						<tr id=InfoRow17>
							<td class="mulinetitle3">8.<input type="checkbox" name="Detail08" class="box" onclick="InputDetailToMuline08();">&nbsp&nbsp 是 <input type="checkbox" name="Detail08" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">您过去五年内是否曾住院检查或治疗(包括疗养院、康复医院)？</td>
							<td class="mulinetitle3">16.<input type="checkbox" name="Detail16" class="box" onclick="InputDetailToMuline16();">&nbsp&nbsp 是 <input type="checkbox" name="Detail16" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="2" class="mulinetitle3">您是否接受过HIV（艾滋病）病毒检测并为阳性？</td>
						</tr>
						<tr id=InfoRow18>
							<td class="mulinetitle3">9.<input type="checkbox" name="Detail09" class="box" onclick="InputDetailToMuline09();">&nbsp&nbsp 是 <input type="checkbox" name="Detail09" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">您过去五年内是否做过手术（包括门诊手术）？</td>
							<td rowspan="2" class="mulinetitle3">17.<input type="checkbox" name="Detail17" class="box" onclick="InputDetailToMuline17();">&nbsp&nbsp 是 <input type="checkbox" name="Detail17" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="2" rowspan="2" class="mulinetitle3">您是否有其他以上未提及的疾病、残疾及器官缺失或功能不全？</td>
						</tr>
						<tr id=InfoRow19>
							<td class="mulinetitle3">10.<input type="checkbox" name="Detail10" class="box" onclick="InputDetailToMuline10();">&nbsp&nbsp 是 <input type="checkbox" name="Detail10" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">您过去五年内是否接受过心理或药物成瘾性治疗?</td>
						</tr>
						<tr id=InfoRow20>
							<td class="mulinetitle3">11.<input type="checkbox" name="Detail11" class="box" onclick="InputDetailToMuline11();">&nbsp&nbsp 是 <input type="checkbox" name="Detail11" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">您在过去三年内是否因疾病而持续治疗超过2周？</td>
							<td rowspan="2" class="mulinetitle3">18.<input type="checkbox" name="Detail18" class="box" onclick="InputDetailToMuline218();">&nbsp&nbsp 是 <input type="checkbox" name="Detail18" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="2" rowspan="2" class="mulinetitle3">(2周岁以下儿童回答)&nbsp&nbsp出生体重 <input name="Detail18" class = "common66"> 公斤是否有新生儿窒息、产伤、先天性疾病、发育迟缓等？</td>
						</tr>
						<tr id=InfoRow21>
							<td class="mulinetitle3">12.<input type="checkbox" name="Detail12" class="box" onclick="InputDetailToMuline12();">&nbsp&nbsp 是 <input type="checkbox" name="Detail12" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">最近一年内您是否参加身体检查并发现结果异常?</td>
						</tr>
						<tr id=InfoRow22>
							<td class="mulinetitle3">13.<input type="checkbox" name="Detail13" class="box" onclick="InputDetailToMuline13();">&nbsp&nbsp 是 <input type="checkbox" name="Detail13" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">您目前或过去三月内是否使用过药物？</td>
							<td rowspan="2" class="mulinetitle3">19.<input type="checkbox" name="Detail19" class="box"  onclick="InputDetailToMuline219();">&nbsp&nbsp 是 <input type="checkbox" name="Detail19" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="2" rowspan="2" class="mulinetitle3">是否怀孕？如您目前怀孕，请告知怀孕 <input name="Detail19" class = "common66"> 月</td>
						</tr>
						<tr id=InfoRow23>
							<td class="mulinetitle3">14.<input type="checkbox" name="Detail14" class="box"  onclick="InputDetailToMuline14();">&nbsp&nbsp 是 <input type="checkbox" name="Detail14" class="box">&nbsp&nbsp 否<input class="common7"></td>
							<td colspan="3" class="mulinetitle3">您过去三个月内是否有过医院门诊检查或治疗？</td>
						</tr>
					</table>
					<table  class= common>
						<tr  class= common>
							<td text-align: left colSpan=1>
								<span id="spanImpartDetailGrid" >
								</span>
							</td>
						</tr>
					</table>
				</DIV>
				<table class=common>
			<tr class=common>
				<td CLASS="title">特别约定 
	    		</td>
				<td CLASS="input" COLSPAN="7">
					<input NAME="Remark" CLASS="readonly4" readonly MAXLENGTH="255" COLSPAN="7">
	    		</td>	 
			</tr>
		</table>
<DIV id=DivAddFee STYLE="display:'none'">
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>加费信息</td>
	     </tr>
       <tr  class= common>
      	 <td text-align: left colSpan=1>
  					<span id="spanAddFeeGrid" >
  					</span>
  			 </td>
  			</tr>
    </table>
    <table  class= common>
    	<tr>
	        <td class= titleImg align= center>免责信息</td>
    	</tr>
    	<tr>
  			 <td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span>
  			 </td>
  		 </tr>
  		</table>
  	</div>
					<table class=title>
						<TR  class= common>
							<TD  class= titleImg> 出险人目前健康状况介绍 </TD>
						</TR>
						<TR  class= common>
							<TD  class= common>
								<textarea name="Note" cols="120" rows="3" class="common" >
								</textarea>
							</TD>
						</TR>
					</table>
			</P>
			<p>
				<input name="Suggest" style="display:''"  class=cssButton type=button value="续保建议" onclick="RenewSuggest()">
				<input name="AddFee" style="display:''"  class=cssButton type=button value="加    费" onclick="DealAddFee()">
				<input name="Exclusion" style="display:''"  class=cssButton type=button value="免    责" onclick="DealExclusion()">
				<input name="Cease" style="display:''"  class=cssButton type=button value="解约退费" onclick="DealCease()">
				<input name="Exemption" style="display:''"  class=cssButton type=button value="豁免保费" onclick="DealExemption()">
				<input name="Check" style="display:''"  class=cssButton type=button value="送    核" onclick="DealCont()">
				<input name="Pass" style="display:''"  class=cssButton type=button value="正常通过" onclick="top.close()">
			</p>
			<input type=hidden name="ContNo" >
			<input type=hidden name="ContNoA" >
			<input type=hidden name="sql" >
		</form>
				<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
			</body>
		</html>
