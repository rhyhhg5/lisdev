<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLCardClaimCollectionRpt.jsp
//程序功能：F1报表生成
//创建日期：2010-12-3
//创建人  ：zzh
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
		System.out.println("start");
  	CError cError = new CError();
  	boolean operFlag=true;
  	
  	//直接获取页面的值
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
  	String MngCom 				= request.getParameter("ManageCom");	
  	String ManageName 			= request.getParameter("ManageComName");
  	
  	String CvaliDateS 		    = request.getParameter("CvaliDateS");		//生效起期
  	String CvaliDateE 		    = request.getParameter("CvaliDateE");		//生效止期
  	String JieSuanS 		    = request.getParameter("JieSuanS");		//结算起期
  	String JieSuanE 		    = request.getParameter("JieSuanE");		//结算止期
  	String ActiveDateS 		    = request.getParameter("ActiveDateS");		//激活起期
  	String ActiveDateE 		    = request.getParameter("ActiveDateE");		//激活止期
  	String EndCaseDateS 		= request.getParameter("EndCaseDateS");		//结案起期
  	String EndCaseDateE 		= request.getParameter("EndCaseDateE");		//结案止期
  	
  	TransferData Para = new TransferData();
      
  	Para.setNameAndValue("MngCom",MngCom);
  	Para.setNameAndValue("ManageName",ManageName);
  	Para.setNameAndValue("CvaliDateS",CvaliDateS);
  	Para.setNameAndValue("CvaliDateE",CvaliDateE);
  	Para.setNameAndValue("JieSuanS",JieSuanS);
  	Para.setNameAndValue("JieSuanE",JieSuanE);
  	Para.setNameAndValue("ActiveDateS",ActiveDateS);
  	Para.setNameAndValue("ActiveDateE",ActiveDateE);
  	Para.setNameAndValue("EndCaseDateS",EndCaseDateS);
  	Para.setNameAndValue("EndCaseDateE",EndCaseDateE);

   	
//  ------------------------------
   	RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);
//   	-------------------------------
  	
  	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	String fileNameB = tG.Operator + "_" + FileQueue.getFileName()+".vts";
	Para.setNameAndValue("tFileNameB",fileNameB);
	String operator = tG.Operator;
	System.out.println("@卡折套餐承保赔付汇总统计报表@");
	System.out.println("Operator:"+operator);
	System.out.println("ManageCom:"+tG.ManageCom);
	System.out.println("Operator:"+tG.Operator);
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();

    tVData.addElement(tG);    
    tVData.addElement(Para);
 
    
    LLCardClaimCollectionUI tLLCardClaimCollectionUI = new LLCardClaimCollectionUI();
	XmlExport txmlExport = new XmlExport();    
    if(!tLLCardClaimCollectionUI.submitData(tVData,"PRINT"))
    {
       	operFlag=false;
       	Content=tLLCardClaimCollectionUI.mErrors.getFirstError().toString();                 
    }
    else
    {  
    	System.out.println("--------成功----------");  
		mResult = tLLCardClaimCollectionUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	
	ExeSQL tExeSQL = new ExeSQL();
	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + fileNameB;
	//获取存放临时文件的路径
	//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
	//String strRealPath = tExeSQL.getOneValue(strSql);
	String strRealPath = application.getRealPath("/").replace('\\','/');
	String strVFPathName = strRealPath + "//" +strVFFileName;
	
	CombineVts tcombineVts = null;	
	if (operFlag==true)
	{
		//合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
	
		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);
	
		//把dataStream存储到磁盘文件
		
		String urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    //System.out.println("存储文件到"+strVFPathName);
		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		System.out.println("==> Write VTS file to disk ");
	
		System.out.println("===strVFFileName : "+strVFFileName);				
//		*********************************
		  rpt.updateReportMetaData(fileNameB);
		  //**********************************
	//本来打算采用get方式来传递文件路径
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
	}
	
	else
	{
    	FlagStr = "Fail";

%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
	
	//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");	
	
</script>
</html>
<%
  	}

%>
