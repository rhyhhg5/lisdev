//批量导入
var mDebug="0";
var mOperate="";
var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

function QueryOnKeyDown(){

	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		var tSql = "select Code, CodeName, ComCode, OtherSign "+
				" from ldcode where 1 = 1 and codetype = 'desc_wound' "+
				getWherePart('Code', 'Code')+
				getWherePart('CodeName', 'CodeName','like')+
				"order by code with ur";
		
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		return false;
	}
    else turnPage.queryModal(tSql, EvaluateGrid);
	}
}


function easyQuery()
{  
	var tSql = "select Code, CodeName, ComCode, OtherSign "+
				" from ldcode where 1 = 1 and codetype = 'desc_wound' "+
				getWherePart('Code', 'Code')+
				getWherePart('CodeName', 'CodeName','like')+
				"order by code with ur";
		
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		return false;
	}
    else turnPage.queryModal(tSql, EvaluateGrid);
	
}

function queryEvaluateGrid()
{
  var selno = EvaluateGrid.getSelNo();
  if (selno <= 0)
  {
    return ;
  }
  var tCode = EvaluateGrid.getRowColData(selno - 1, 1);
  fm.tCode.value = tCode;
  if(tCode == null || tCode == "")
  {
    alert("残疾代码为空！");
    return;
  }

  var strSql = "select Code, CodeName, ComCode, OtherSign "
  	+" from ldcode where 1 = 1 and codetype = 'desc_wound' "
  	+" and code='"+tCode+"' order by Code with ur";
 	 
  divGrpCaseInfo.style.display = '';
  
  var arr = easyExecSql(strSql );
		if(arr)
		{ 
			fm.all('mCode').value = arr[0][0];
			fm.all('mCodeName').value = arr[0][1];
			fm.all('mComCode').value = arr[0][2];
			fm.all('mOtherSign').value = arr[0][3];					
		}	   
  
}

//页面的增删改
function submitForm(k)
{ 
   var chkFlag=false;
   for (i=0;i<EvaluateGrid.mulLineCount;i++){
       if(EvaluateGrid.getSelNo(i)!='0'){
            chkFlag=true;
       }
  }

  if(k==0)
  {  //alert(1);
	 fm.operate.value="UPDATE"	
  }
  else if(k==1)
  {  //alert(2);
  	 fm.operate.value="DELETE"	
  }
  
  if(!chkFlag){
  	 alert("请选择待修改/删除的记录");
  	 return false;
  }
	
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "fraSubmit";
  fm.action="./LLDescWoundBatchSave.jsp"
  fm.submit();
  
}

//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content){
  showInfo.close();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  	easyQuery();
  }
}
