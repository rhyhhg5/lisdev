<%
//Name:ReportInit.jsp
//function：
//author:zht
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLClaimConfListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLClaimConfListInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initSelBox();
    initCaseGrid();

  }
  catch(re)
  {
    alter("在LLClaimConfListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="理赔号";
    iArray[1][1]="120px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="客户号码";
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="客户名称";
    iArray[3][1]="100px";
    iArray[3][2]=60;
    iArray[3][3]=0;
    

    iArray[4]=new Array("申请日期","80px","0","0");
    iArray[5]=new Array("核算标记","80px","0","0");
    iArray[6]=new Array("案件状态", "80px", "0", "0");


    CaseGrid = new MulLineEnter("fm","CaseGrid");
    CaseGrid.mulLineCount =10;
    CaseGrid.displayTitle = 1;
    CaseGrid.locked = 1;
    CaseGrid.canSel =1;
    CaseGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CaseGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CaseGrid. selBoxEventFuncName = "onSelSelected";
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

</script>