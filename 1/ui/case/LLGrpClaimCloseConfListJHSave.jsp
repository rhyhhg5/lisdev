<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLAffixSave.jsp
//程序功能：
//创建日期：2005-02-5 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  
  CErrors tError = null;
  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  
  //输出参数
  String FlagStr = "";
  String Content = "";
    
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);
  String tsrRgtNo = request.getParameter("srRgtNo");
  String tsrCustomerNo = request.getParameter("srCustomerNo");
  String tsrGrpName = request.getParameter("srGrpName");
  String tsrRgtantName = request.getParameter("srRgtantName");
  String tRgtDateStart = request.getParameter("RgtDateStart");
  String tRgtDateEnd = request.getParameter("RgtDateEnd");
  String tManageCom = request.getParameter("ManageCom");

  if (strOperate.equals("GIVE|ENSURE")) 
  {
  
   LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
   tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
   
  String tSql = "SELECT R.RGTNO  FROM LLREGISTER R  "
 			   +" WHERE R.RGTOBJ = '0' AND R.RGTCLASS = '1' AND R.declineflag is null "
 			   +"  and r.MngCom like '8612%' and togetherflag='4' and r.rgtstate = '03' "
 			   +" and r.MngCom like '8612%' and exists ( select 1 from llcase a ,llhospcase b where a.caseno=b.caseno and casetype='03' and hospitcode='JH02' and a.rgtno=r.rgtno) " ;
   if(tManageCom !=null&&!"".equals(tManageCom) ){
	   tSql += " and r.MngCom like '"+tManageCom+"%' ";
   }
   if(tsrRgtNo !=null&&!"".equals(tsrRgtNo)){
	   tSql += " and r.RGTNO = '"+tsrRgtNo+"' ";
   }
   if(tsrCustomerNo !=null&&!"".equals(tsrCustomerNo)){
	   tSql += " and r.CUSTOMERNO = '"+tsrCustomerNo+"' ";
   }
   if(tsrGrpName !=null&&!"".equals(tsrGrpName)){
	   tSql += " and r.GRPNAME = '"+tsrGrpName+"' ";
   }
   if(tsrRgtantName !=null&&!"".equals(tsrRgtantName)){
	   tSql += " and r.RGTANTNAME = '"+tsrRgtantName+"' ";
   }
   if(tRgtDateStart !=null&&!"".equals(tRgtDateStart)){
	   tSql += " and r.RGTDATE >= '"+tRgtDateStart+"' ";
   }
   if(tRgtDateEnd !=null&&!"".equals(tRgtDateEnd)){
	   tSql += " and r.RGTDATE <= '"+tRgtDateEnd+"' ";
   }
   tSql += "order by r.rgtno ";
   
   System.out.println("==== sSql == " + tSql);
   SSRS tSSRS = new SSRS();
   ExeSQL mExeSQL = new ExeSQL();
   tSSRS = mExeSQL.execSQL(tSql);
   System.out.println("==== tSSRS.getMaxRow() == " + tSSRS.getMaxRow()); 
   
   if(tSSRS == null || tSSRS.getMaxRow() == 0){
	    Content = "批量给付确认失败，原因是:没有需要给付确认的批次" ;
	    System.out.println("aaaa：批量给付确认失败");
	    FlagStr = "Fail";
	    }else{
	    	for (int i = 1; i <= tSSRS.getMaxRow(); i++){
	    		System.out.println("<-rgtno[i]-->"+ tSSRS.GetText(i, 1));
	    		FlagStr="";
	    		 GrpGiveEnsureBL tGrpGiveEnsureJHBL = new GrpGiveEnsureBL();
	    		tLLRegisterSchema.setRgtNo(tSSRS.GetText(i, 1));
	    		  if (request.getParameterValues("PrePaidFlag") != null) {
	    			   tLLRegisterSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
	    			}else{
	    			   tLLRegisterSchema.setPrePaidFlag("");  // 个案标记llcase 批次案件标记llregister
	    			}
	    			   try
	    			   {		   
	    				   tVData.add(tLLRegisterSchema);
	    				   tVData.add(tG);		   
	    				   tGrpGiveEnsureJHBL.submitData(tVData, strOperate);
	    				//  FlagStr="";
	    			   }
	    			 catch(Exception ex)
	    			    {
	    			      Content = "第"+i+"个批次"+tSSRS.GetText(i, 1)+"确认失败，原因是:" + ex.toString();
	    			      System.out.println("aaaa"+ex.toString());
	    			      FlagStr = "Fail";
	    			      break;
	    			    }
	    			    if (FlagStr=="")
	    			    {
	    				    tError = tGrpGiveEnsureJHBL.mErrors;
	    				    if (!tError.needDealError())
	    				    {                          
	    				      Content ="确认成功！"+tGrpGiveEnsureJHBL.getBackMsg();
	    				    	FlagStr = "Succ";
	    				    	tVData.clear();
	    						tVData = tGrpGiveEnsureJHBL.getResult();
	    				    }
	    				    else                                                                           
	    				    {
	    				    	Content = "第"+i+"个批次"+tSSRS.GetText(i, 1)+"确认失败，原因是:" + tError.getFirstError();
	    				    	FlagStr = "Fail";
	    				    	break;
	    				    }
	    			     }	
	    	}
	    }
        
  }		
  	

  %>

   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
   
   
   
 