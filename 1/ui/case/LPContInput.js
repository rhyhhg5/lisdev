var turnPage = new turnPageClass();

//点击查询按钮触发的事件
function queryClick() {
	if(!verifyInput()){
		return false;
	}
	var strCodeName = ""; //日期
	var startDate = fm.all("startDate").value;
	var endDate = fm.all("endDate").value;
	if(startDate!=null&&startDate!=""&&endDate!=null&&endDate!=""&&startDate > endDate) {
		alert("日期填写有误");
		return;
	}
	if(startDate!=""&&startDate!=null&&endDate!=null&&endDate!="") {
		strCodeName = "and a.Codename between '" + startDate + "' and '" + endDate + "'";
	}
	if(startDate!=""&&startDate!=null&&(endDate==null||endDate=="")) {
		strCodeName = "and a.Codename >='" + startDate + "'"; 
	}
	if(endDate!=null&&endDate!=""&&(startDate==null||startDate=="")) {
		strCodeName = "and a.Codename <='" + endDate + "'"; 
	}
	//alert("strCodeName=" + strCodeName);
	
	var strComcode = "";  //保单机构
	if(fm.all("ManageCom").value!=null &&fm.all("ManageCom").value!="") {
		strComcode = "and c.managecom like '" + fm.all("ManageCom").value + "%'";
	}else {
		strComcode = " and c.managecom like  '" + fm.all("fmcode").value + "%'";
	}
	
	var strCode = ""; //保单号
	if(fm.all("Code").value!=null&&fm.all("Code").value!="") {
		strCode = "and a.Code = '" + fm.all("Code").value + "'";
	}
	
	var strCodeType = "";
	
	if(fm.all("ThridCompanyName2").value!=null&&fm.all("ThridCompanyName2").value!="") {
		strCodeType = "and b.codename = '" + fm.all("ThridCompanyName2").value + "'";
	} 
	
	var strSQL = "select c.managecom,a.code,a.Codename,c.GrpName,c.Peoples2,a.Codealias,b.codename " +
				"from ldcode a ,ldcode b,lcgrpcont c where 1=1 and a.codetype=b.code and a.code = c.grpcontno " + 
				"and a.comcode like '86%' " +
				strCodeName + 
				strComcode + 
				strCode + 
				strCodeType + 
				"order by a.codename desc with ur";
	//alert("strSQL=" + strSQL);
	turnPage.queryModal(strSQL,LDCodeGrid);
}