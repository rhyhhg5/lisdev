<%
/*******************************************************************************
* Name     :ICaseCureInit.jsp
* Function :初始化“立案－费用明细信息”的程序
* Author   :Xx
* Date     :2005-7-23
*/
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//添加页面控件的初始化。
String LoadC="";
if(request.getParameter("LoadC")!=null){
LoadC = request.getParameter("LoadC");
}
String LoadD="";
if(request.getParameter("LoadD")!=null){
	LoadD = request.getParameter("LoadD");
}

String tShowCaseRemarkFlag = "";//
tShowCaseRemarkFlag = (String)session.getAttribute("ShowCaseRemarkFlag");
System.out.println("tShowCaseRemarkFlag:"+tShowCaseRemarkFlag);
%>
<script language="JavaScript">

  var turnPage = new turnPageClass();

  function initInpBox(){
    try{
      //fm.all('HospitalCode').value = '';
      //fm.all('HospitalName').value = '';
      //fm.all('FixFlag').value = '';
      //fm.all('FeeType').value = '';
      //fm.all('FeeTypeName').value = '';
      //fm.all('FeeAtti').value = '';
      //fm.all('FeeAttiName').value = '';
      //fm.all('FeeAffixType').value = '';
      //fm.all('FeeAffixTypeName').value = '';
      //fm.all('HosGrade').value = '';
      //fm.all('CompSecuNo').value = '';
      //fm.all('SecurityNo').value = '';
      fm.all('ReceiptNo').value = '';
      fm.all('FeeDate').value = '';
      fm.all('HospStartDate').value = '';
      fm.all('HospEndDate').value = '';
      fm.all('RealHospDate').value = '';
      fm.all('SeriousWard').value = '';
      fm.all('MainFeeNo').value = '';
      fm.all('inpatientNo').value='';
      fm.all('FirstInHos').value = '';
      fm.all('ApplyAmnt').value = '';
      fm.all('FeeInSecu').value = '';
      fm.all('FeeOutSecu').value = '';
      fm.all('TotalSupDoorFee').value = '';
      fm.all('TotalApplyAmnt').value = '';
      fm.all('PayReceipt').value = '';
      fm.all('RefuseReceipt').value = '';
      fm.all('SupDoorFee').value = '';
      fm.all('YearSupDoorFee').value = '';
      fm.all('PlanFee').value = '';
      fm.all('YearPlayFee').value = '';
      fm.all('SupInHosFee').value = '';
      fm.all('YearSupInHosFee').value='';
      fm.all('SecurityFee').value = '';
      fm.all('SelfPay1').value = '';
      fm.all('SelfPay2').value = '';
      fm.all('SelfAmnt').value='';
      fm.all('SecuRefuseFee').value='';
      fm.all('DrugFee').value = '';
      fm.all('ExamFee').value='';
      fm.all('OtherFee').value='';
      fm.all('RefuseDrugFee').value = '';
      fm.all('RefuseExamFee').value='';
      fm.all('RefuseOtherFee').value='';
      fm.all('FeeStart').value = '';
      fm.all('FeeEnd').value='';
      fm.all('FeeDays').value='';
      fm.all('SumFee').value='';
      fm.all('RetireAddFee').value='';
      fm.all('YearRetireAddFee').value='';
			var tCaseNo = '<%= request.getParameter("CaseNo") %>';
      fm.all('CaseNo').value = tCaseNo=='null'?'':tCaseNo;

      <%
      GlobalInput mG = new GlobalInput();
      mG=(GlobalInput)session.getValue("GI");
      %>
      fm.cOperator.value = "<%=mG.Operator%>";

      fm.MakeDate.value = getCurrentDate();

      var strSQL1="select operator,modifydate from llfeemain where caseno='"+fm.CaseNo.value
      +"' order by modifydate desc,modifytime desc ";
      var arrResult1 = easyExecSql(strSQL1);
      if(arrResult1){
          fm.cOperator.value = arrResult1[0][0];
          fm.MakeDate.value = arrResult1[0][1]
      }

      if(fm.all('CaseNo').value==''){
        fm.MngCom.value="<%=mG.ManageCom%>";
      }
      else{
        fm.MngCom.value="86"+fm.all('CaseNo').value.substr(1,2);
      }

      var strSQL2="select name from ldcom where comcode='"+fm.MngCom.value+"'";
      var arrResult2 = easyExecSql(strSQL2);
      if(arrResult2){
        fm.ComName.value= arrResult2[0][0];
      }
    }
    catch(ex){
      alert("在ICaseCureInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  function initSelBox(){
    try{

    }
    catch(ex){
      alert("在ICaseCureInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm(){
    try{
      initInpBox();
      initIFeeCaseCureGrid(fm.FeeAtti.value);
      initDutyRelaGrid();
      initSecuAddiGrid();
      initSecuSpanGrid();
      initQDSecuGrid();
      initSecuItemGrid();
      showSecuItem();
      initFeeItemGrid();
      showFeeItem();
      fm.LoadC.value="<%=LoadC%>";
      fm.ShowCaseRemarkFlag.value = "<%=tShowCaseRemarkFlag%>";
      if (fm.LoadC.value=='2'){
        divconfirm.style.display='none';
        titleCaseduty.style.display='none';
        divCaseduty.style.display='none';
        Contral.style.display = 'none';
        fm.FeeAtti.onclick = "";
        fm.FeeType.onclick = "";
        fm.FeeAffixType.onclick = "";
        fm.FirstInHos.onclick = "";
        fm.InsuredStat.onclick = "";
      }
      if(fm.LoadC.value=='3'){
        fm.idNext.style.display='none';
      }
       fm.LoadD.value="<%=LoadD%>";
       if (fm.LoadD.value=='1'){
          document.getElementById('sub1').disabled = true;
          document.getElementById('sub2').disabled = true;
          document.getElementById('sub3').disabled = true;
          document.getElementById('sub4').disabled = true;
          document.getElementById('idNext').disabled = true;
       }
      initQuery();
      initQueryZD();
            
      showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出

    }
    catch(re)
    {
      alert("ICaseCureInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }

  //账单费用项目明细信息
  function initIFeeCaseCureGrid(FeeAtti)
  {
  	var feetype = (fm.all('FeeType').value=='1')?'1':'2';
    var strsqlinit="1 and code like #"+feetype+"%#";
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","10","0");

      iArray[1]=new Array("费用项目代码","100px","100","2");
      iArray[1][4]="llfeeitemtype";
      iArray[1][5]="1|2|8";
      iArray[1][6]="0|1|2";
      iArray[1][15]="1";
      iArray[1][16]=feetype;
      iArray[1][19]="1";

      iArray[2]=new Array("费用项目名称","105px","100","0");
      iArray[3]=new Array("费用金额","105px","100","1");
      iArray[4]=new Array("社保外费用","105px","50","1");
      iArray[5]=new Array("先期给付","105px","50","1");
      iArray[6]=new Array("不合理费用","105px","50","1");
      iArray[7]=new Array("有效标记编号","105px","5","3");
      iArray[8]=new Array("给付责任","105px","50","0");
      if(FeeAtti=='1'||FeeAtti=='2'){
        iArray[4][0]="全自费金额";
        iArray[5][0]="先自付金额";
        iArray[6][0]="符合共付范围金额";
      }

      IFeeCaseCureGrid = new MulLineEnter( "fm" , "IFeeCaseCureGrid" );
      IFeeCaseCureGrid.mulLineCount = 5;
      IFeeCaseCureGrid.displayTitle = 1;
      IFeeCaseCureGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initDutyRelaGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","20","3");
      iArray[1]=new Array("责任编号","80px","50","0");
      iArray[2]=new Array("责任名称","150px","50","0");
      iArray[3]=new Array("合同号","100px","50","0");
      iArray[4]=new Array("给付起始日期","80px","50","0");
      iArray[5]=new Array("保额","80px","50","0");
      iArray[6]=new Array("档次","80px","5","0");

      DutyRelaGrid = new MulLineEnter( "fm" , "DutyRelaGrid" );
      DutyRelaGrid.mulLineCount = 3;
      DutyRelaGrid.displayTitle = 1;
      DutyRelaGrid.hiddenPlus=1;
      DutyRelaGrid.hiddenSubtraction=1;
      DutyRelaGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initSecuAddiGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","0px","20","3");
      iArray[1]=new Array("账单合计金额","120px","500","1");
      iArray[2]=new Array("社保支付金额","120px","500","1");
      iArray[3]=new Array("社保补充支付金额","120px","500","1");
      iArray[4]=new Array("未赔付金额","120px","500","1");

      SecuAddiGrid = new MulLineEnter( "fm" , "SecuAddiGrid" );
      SecuAddiGrid.mulLineCount = 1;
      SecuAddiGrid.displayTitle = 1;
      SecuAddiGrid.hiddenPlus=1;
      SecuAddiGrid.hiddenSubtraction=1;
      SecuAddiGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initSecuSpanGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","20","3");
      iArray[1]=new Array("分档区间起点","120px","500","0");
      iArray[2]=new Array("分档区间终点","120px","500","0");
      iArray[3]=new Array("统筹比例","120px","500","0");
      iArray[4]=new Array("统筹支付","120px","500","1");
      iArray[5]=new Array("个人负担","120px","500","1");
      iArray[6]=new Array("合计","120px","500","1");
      iArray[7]=new Array("编号","120px","500","3");

      SecuSpanGrid = new MulLineEnter( "fm" , "SecuSpanGrid" );
      SecuSpanGrid.mulLineCount = 3;
      SecuSpanGrid.displayTitle = 1;
      SecuSpanGrid.hiddenPlus=1;
      SecuSpanGrid.hiddenSubtraction=1;
      SecuSpanGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initQDSecuGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","0px","20","3");
      iArray[1]=new Array("项目编号","16px","500","3");
      iArray[2]=new Array("项目","105px","500","0");
      iArray[3]=new Array("全额统筹","105px","500","1");
      iArray[4]=new Array("部分统筹","105px","500","1");
      iArray[5]=new Array("部分统筹自负部分","105px","500","1");
      iArray[6]=new Array("全额自费","105px","500","1");
      iArray[7]=new Array("合计","105px","500","1");

      QDSecuGrid = new MulLineEnter( "fm" , "QDSecuGrid" );
      QDSecuGrid.mulLineCount = 3;
      QDSecuGrid.displayTitle = 1;
      QDSecuGrid.hiddenPlus=1;
      QDSecuGrid.hiddenSubtraction=1;
      QDSecuGrid.loadMulLine(iArray);
      var initSql = "select code,codename from ldcode where codetype='llfeeitemtype' and substr(code,1,2)='QD' order by code";
      turnPage.queryModal(initSql,QDSecuGrid);
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initSecuItemGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","0px","10","0");

      iArray[1]=new Array("费用项目","90px","100","0");
      iArray[2]=new Array("费用代码","0px","100","3");
      iArray[3]=new Array("费用归属","0px","100","3");
      iArray[4]=new Array("金额","40px","100","1");

      iArray[5]=new Array("费用项目","90px","100","0");
      iArray[6]=new Array("费用代码","0px","100","3");
      iArray[7]=new Array("费用归属","0px","100","3");
      iArray[8]=new Array("金额","40px","100","1");

      iArray[9]=new Array("费用项目","90px","100","0");
      iArray[10]=new Array("费用代码","0px","100","3");
      iArray[11]=new Array("费用归属","0px","100","3");
      iArray[12]=new Array("金额","40px","100","1");

      iArray[13]=new Array("费用项目","90px","100","0");
      iArray[14]=new Array("费用代码","0px","100","3");
      iArray[15]=new Array("费用归属","0px","100","3");
      iArray[16]=new Array("金额","40px","100","1");

      SecuItemGrid = new MulLineEnter( "fm" , "SecuItemGrid" );
      SecuItemGrid.mulLineCount = 1;
      SecuItemGrid.displayTitle = 1;
      SecuItemGrid.canSel = 0;
      SecuItemGrid.hiddenPlus=1;
      SecuItemGrid.hiddenSubtraction=1;
      SecuItemGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initFeeItemGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","0px","10","0");

      iArray[1]=new Array("费用项目","60px","100","0");
      iArray[2]=new Array("费用代码","0px","100","3");
      iArray[3]=new Array("金额","60px","100","1");

      iArray[4]=new Array("费用项目","60px","100","0");
      iArray[5]=new Array("费用代码","0px","100","3");
      iArray[6]=new Array("金额","60px","100","1");

      iArray[7]=new Array("费用项目","60px","100","0");
      iArray[8]=new Array("费用代码","0px","100","3");
      iArray[9]=new Array("金额","60px","100","1");

      iArray[10]=new Array("费用项目","60px","100","0");
      iArray[11]=new Array("费用代码","0px","100","3");
      iArray[12]=new Array("金额","60px","100","1");

      FeeItemGrid = new MulLineEnter( "fm" , "FeeItemGrid" );
      FeeItemGrid.mulLineCount = 1;
      FeeItemGrid.displayTitle = 1;
      FeeItemGrid.canSel = 0;
      FeeItemGrid.hiddenPlus=1;
      FeeItemGrid.hiddenSubtraction=1;
      FeeItemGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

</script>

