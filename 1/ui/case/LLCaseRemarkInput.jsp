<html>
	<%
	//程序名称：LLCaseRemarkInput.jsp
	//程序功能：
	//创建日期：
	//创建人 ：
	//更新记录： 更新人  更新日期   更新原因/内容
	%>

	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<%
	%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="LLCaseRemarkInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLCaseRemarkInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action='./LLCaseRemarkSave.jsp' method=post name=fm target="fraSubmit">
			<Div id= "divLLCase" style= "display: ''">
				<table style= "display: ''">
					<tr>
						<td>
							<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSurveyInfo);">
						</td>
						<td class= titleImg>
							案件备注信息查看
						</td>
					</tr>
				</table>
				<Div id= "divLLCaseRemarkInfo" style= "display: ''">
					<table class= common>
						<TR class= common>
							<TD>
								<span id="spanLLCaseRemarkGrid">
								</span>
							</TD>
						</TR>
					</table>
					<br>
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
				</DIV>				
			</Div>
			
			<table style= "display: 'none'">
				<tr>
					<td>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
					</td>
					<td class= titleImg>
						案件回退信息查看
					</td>
				</tr>
			</table>

			<Div id= "divLLCase" style= "display: ''">
				<table class= common>
					<TR class= common>
						<TD>
							<span id="spanLLCaseBackGrid">
							</span>
						</TD>
					</TR>					
				</table>					
				<br>
				<INPUT style= "display: 'none'" CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
				<INPUT style= "display: 'none'" CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
				<INPUT style= "display: 'none'" CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
				<INPUT style= "display: 'none'" CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
				
				
				<table class=common>
					<TR class= common>
						<TD class= titleImg>案件备注信息录入</TD>
					</tr>
					<TR class= common>
						<TD class= input>
						<textarea name="Remark" cols="100%" rows="6" witdh=25% class="common"></textarea>
						</TD>
					</tr>					
				</table>
			</Div>
			<div id="divOperate" style="display: ''">
				<INPUT VALUE="保 存" TYPE=button class=cssbutton onclick="submitForm();">
				<INPUT VALUE="重 置" TYPE=button class=cssbutton onclick="window.location.reload();">
				<INPUT VALUE="返 回" TYPE=button class=cssbutton onclick="top.close();">
			</div>
			<input type=hidden name="CaseNo">
			<input type=hidden id="fmtransact" name="fmtransact">
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
