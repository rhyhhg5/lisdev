<html>
<%
/*******************************************************************************
 * Name     :ICaseCureInput.jsp
 * Function :立案－费用明细信息的初始化页面程序
 * Author   :LiuYansong
 * Date     :2003-7-23
 */
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="LLFeeItemCheckList.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLFeeItemCheckListInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏ICaseCure1的信息 -->
   
      <Div  id= "divCaseCure2" style= "display: ''">
 
      <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAffixInfo);">
      </td>
      <td class= titleImg>
        案件影像列表
      </td>
    	</tr>
    </table>
      <Div  id= "divAffixInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanAffixGrid" >
            </span>
          </TD>
        </TR>
		  </table>
     <INPUT VALUE="关联保存" class=cssButton TYPE=button onclick="">
     <INPUT VALUE="开始录入" class=cssButton TYPE=button onclick="">
    </Div>

      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>

</body>

</html>