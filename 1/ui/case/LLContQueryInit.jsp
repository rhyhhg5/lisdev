<%
//Name:ReportInit.jsp
//function：理赔二核
//author:Xx
//Date:2005-08-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>


<%
 String tCaseNo = "";
 String tInsuredNo = "";

 tCaseNo = request.getParameter("CaseNo");
 if(tCaseNo.equals("null"))
  tCaseNo="";
 tInsuredNo += request.getParameter("InsuredNo");
 if(tInsuredNo.equals("null"))
  tInsuredNo="";
String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");

%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alter("在SecondUWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在SecondUWInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(tCaseNo,tInsuredNo)
{
  try
  {
    initInpBox();
    initPolGrid();
    initHide(tCaseNo,tInsuredNo);
    fm.CustomerName.value = '<%=CustomerName%>';
    var tsql="select name from ldperson where CustomerNo='"+fm.InsuredNo.value+"'";
    var xrr = easyExecSql(tsql);
    if(xrr)
      fm.CustomerName.value = xrr[0][0];
    easyQueryClick();
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="合同号码";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="投保人名称";
    iArray[2][1]="200px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="客户风险等级";
    iArray[3][1]="80px";
    iArray[3][2]=80;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="管理机构";
    iArray[4][1]="80px";
    iArray[4][2]=80;
    iArray[4][4]=0;

    iArray[5]=new Array();
    iArray[5][0]="团体合同号";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=1;

    iArray[6]=new Array();
    iArray[6][0]="投保书号";
    iArray[6][1]="60px";
    iArray[6][2]=100;
    iArray[6][3]=3;

    iArray[7]=new Array();
    iArray[7][0]="生效日期";
    iArray[7][1]="80px";
    iArray[7][2]=100;
    iArray[7][3]=1;

    iArray[8]=new Array();
    iArray[8][0]="满期日期";
    iArray[8][1]="80px";
    iArray[8][2]=100;
    iArray[8][3]=1;

    iArray[9]=new Array();
    iArray[9][0]="保单类型";
    iArray[9][1]="80px";
    iArray[9][2]=100;
    iArray[9][3]=3;
    
    iArray[10]=new Array();
    iArray[10][0]="投保单号";
    iArray[10][1]="80px";
    iArray[10][2]=100;
    iArray[10][3]=3;
    
    iArray[11]=new Array();
    iArray[11][0]="保单状态";
    iArray[11][1]="80px";
    iArray[11][2]=100;
    iArray[11][3]=1;

    iArray[12]=new Array();
    iArray[12][0]="终止日期";
    iArray[12][1]="80px";
    iArray[12][2]=100;
    iArray[12][3]=1;
    
    iArray[13]=new Array();
    iArray[13][0]="险种名称";
    iArray[13][1]="113px";
    iArray[13][2]=100;
    iArray[13][3]=0;
//wdx新增
    iArray[14]=new Array();
    iArray[14][0]="个人保额";
    iArray[14][1]="97px";
    iArray[14][2]=100;
    iArray[14][3]=0;
    
    iArray[15]=new Array();
    iArray[15][0]="保障计划";
    iArray[15][1]="113px";
    iArray[15][2]=100;
    iArray[15][3]=0;
    
    iArray[16]=new Array();
    iArray[16][0]="职业类别";
    iArray[16][1]="113px";
    iArray[16][2]=100;
    iArray[16][3]=0;
//wdx新增
    iArray[17]=new Array();
    iArray[17][0]="补充医疗保险费率折扣";
    iArray[17][1]="130px";
    iArray[17][2]=100;
    iArray[17][3]=0;
    
    iArray[18]=new Array();
    iArray[18][0]="保单险种号";
    iArray[18][1]="87px";
    iArray[18][2]=100;
    iArray[18][3]=3;
    
    PolGrid = new MulLineEnter("fm","PolGrid");
    PolGrid.mulLineCount =5;
    PolGrid.displayTitle = 1;
    PolGrid.locked = 1;
    PolGrid.canSel =1
    PolGrid.canChk = 0;;
    PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    PolGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    PolGrid. selBoxEventFuncName = "onSelSelected";
    PolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}


function initHide(tCaseNo,tInsuredNo)
{
	fm.all('CaseNo').value = tCaseNo;
  fm.all('InsuredNo').value = tInsuredNo;
	
}

 </script>