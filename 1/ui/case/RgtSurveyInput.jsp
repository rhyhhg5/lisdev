<html>
	<%
	//程序名称：RgtSurveyInput.jsp
	//程序功能：理赔提调
	//创建日期：
	//创建人 ：
	//更新记录： 更新人  更新日期   更新原因/内容
	%>

	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<%
	%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="RgtSurveyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="RgtSurveyInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action='./RgtSurveySave.jsp' method=post name=fm target="fraSubmit">
      <table>
				<tr>
					<td>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCustomer);">
					</td>
					<td class= titleImg>
						客户信息
					</td>
				</tr>
			</table>
			
			<Div id= "divLLCase1" style= "display: ''">
				<table class= common>
					<TR class= common>
						<TD class= title>客户号</TD>
						<TD class= input><Input class= "readonly" readonly name=InsuredNo></TD>
						<TD class= title>客户姓名</TD>
						<TD class= input><Input class= "readonly" readonly name=CustomerName></TD>
						<TD class= title></TD>
						<TD class= input></TD>
					</TR>
				</table>
				
				<table style= "display: ''">
					<tr>
						<td>
							<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSurveyInfo);">
						</td>
						<td class= titleImg>
							既往调查
						</td>
					</tr>
				</table>
				<Div id= "divSurveyInfo" style= "display: ''">
					<table class= common>
						<TR class= common>
							<TD>
								<span id="spanPastGrid">
								</span>
							</TD>
						</TR>
					</table>
					<br>
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
				</DIV>
				
			</Div>
			
			<table>
				<tr>
					<td>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
					</td>
					<td class= titleImg>
						提调信息
					</td>
				</tr>
			</table>

			<Div id= "divLLCase1" style= "display: ''">
				<table class= common>
					<TR class= common style= "display: 'none'">
						<TD class= title>调查报告状态</TD>
						<TD class= input><Input class= "readonly" readonly name=SurveyFlag></TD>
						<TD class= title>案件类型</TD>
						<TD class= input><Input class= "readonly" readonly name=SurveyType></TD>
					</tr>

					<TR class= common>
						<TD class= title>理赔号</TD>
						<TD class= input><Input class= "readonly" readonly name=CaseNo></TD>
						<TD class= title>提调次数</TD>
						<TD class= input><Input class= "readonly" readonly name=SerialNo></TD>
						<TD class= title>提调人</TD>
						<TD class= input><Input class= "readonly" readonly name=Surveyer></TD>
					</TR>
					<TR class= common>
					  <TD class= title>调查类型</TD>
						<TD class= input><Input class="codeno" name="SurvType" CodeData="0|3|^0|即时调查^1|一般调查" name=SurveyTypeName ondblclick="return showCodeListEx('SurvType',[this,SurvTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('SurvType',[this,SurvTypeName],[0,1]);"><input class=codename name='SurvTypeName' elementtype=nacessary></TD>
						<TD class= title>涉案金额</TD>
						<TD class= input><Input name=CasePay></TD>
						
					</TR>
				</table>
				
				<table class=common>
					<TR class= common>
						<TD class= titleImg>调查内容</TD>
					</tr>
					<TR class= common>
						<TD class= input>
						<textarea name="Content" cols="100%" rows="6" witdh=25% class="common"></textarea></TD>
						</TD>
					</tr>
					<TR class= common>
						<TD class= titleImg colspan="3">院内调查回复</TD>
					</TR>
					<TR class= common>
						<TD class= input colspan="3">
							<textarea name="Result" cols="100%" rows="6" witdh=25% readonly class="common"></textarea>
						</TD>
					</TR>
					<TR class= common>
						<TD class= titleImg colspan="3">院外调查回复</TD>
					</TR>
					<TR class= common>
						<TD class= input colspan="3">
							<textarea name="resultOut" cols="100%" rows="6" witdh=25% readonly class="common"></textarea>
						</TD>
					</TR>
				</table>
			</Div>
			<div id="div3" style="display: ''">
				<TR class= common>
					<TD class= title>审核结论</TD>
					<TD class= input>
						<Input class="codeno" name="UWFlag" CodeData="0|3|^0|通过^1|继续调查" name=UWFlagName ondblclick="return showCodeListEx('UWFlag',[this,UWFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('UWFlag',[this,UWFlagName],[0,1]);"><input class=codename name='UWFlagName'>
					</TD>
				</TR>
				<TR class= common>
					<TD class= titleImg colspan="6">审核意见</TD>
				</TR>
				<TR class= common>
					<TD class= input colspan="6">
						<textarea name="ConfNote" cols="100%" rows="4" class="common"></textarea>
					</TD>
				</TR>
			</div>

			<div id="div2" style="display: ''">
				<INPUT VALUE="保 存" TYPE=button class=cssbutton onclick="submitForm()">
				<INPUT VALUE="重 置" TYPE=button class=cssbutton onclick="window.location.reload();">
				<INPUT VALUE="返 回" TYPE=button class=cssbutton onclick="top.close();">
			</div>
			<input type=hidden name="StartPhase">
			<input type=hidden id="fmtransact" name="fmtransact">
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
