<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：taskLetterBackUWSave.jsp
//程序功能：
//创建日期：2006-02-25
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  
<%             
	String FlagStr = "";
	String Content = "";
	String transact = request.getParameter("loadFlag");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	//取得函件信息
	
	String edorType[] = request.getParameterValues("CustomerAnwserGrid3");
  String polNo[] = request.getParameterValues("CustomerAnwserGrid4");
   String CustomerReply[] = request.getParameterValues("CustomerAnwserGrid6");
   
	LGLetterSchema tLGLetterSchema = new LGLetterSchema();
	tLGLetterSchema.setEdorAcceptNo(request.getParameter("edorAcceptNo"));
	tLGLetterSchema.setSerialNumber(request.getParameter("serialNumber"));
	tLGLetterSchema.setBackFlag(request.getParameter("backFlag"));
	tLGLetterSchema.setBackDate(request.getParameter("backDate"));
	
	System.out.print("看这里"+request.getParameter("edorAcceptNo"));
	
	 LPUWMasterSet tLPUWMasterSet = new LPUWMasterSet();
	 int count = 0;
	 if (polNo != null)
	 {
	 	count = polNo.length;
	 }
	for (int i = 0; i < count; i++)
  {
    LPUWMasterSchema tLPUWMasterSchema = new LPUWMasterSchema();
    tLPUWMasterSchema.setEdorNo(request.getParameter("edorAcceptNo"));
    tLPUWMasterSchema.setPolNo(polNo[i]);
    tLPUWMasterSchema.setEdorType(edorType[i]);
    tLPUWMasterSchema.setCustomerReply(CustomerReply[i]);
    tLPUWMasterSet.add(tLPUWMasterSchema);
  }
	

	
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGLetterSchema);
	tVData.add(tG);
	tVData.add(tLPUWMasterSet);
	
	//调用后台提交数据
	LTaskLetterBackUWUI tTaskLetterBackUWUI = new LTaskLetterBackUWUI();
  	if (!tTaskLetterBackUWUI.submitData(tVData, transact))
	{
		//失败
		FlagStr = "Fail";
		
		//需要处理后台错误
		if(tTaskLetterBackUWUI.mErrors.needDealError())
		{
			System.out.println(tTaskLetterBackUWUI.mErrors.getErrContent());
			
			Content = tTaskLetterBackUWUI.mErrors.getErrContent();
		}
		else
		{
			Content = "数据保存失败!";
		}
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}
	
	Content = PubFun.changForHTML(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>
</html>