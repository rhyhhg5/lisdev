<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InsuredUWInfoChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-08-19 11:10:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

GlobalInput tG = new GlobalInput();

tG=(GlobalInput)session.getValue("GI");

if(tG == null) {
	out.println("session has expired");
	return;
}

LLUWMasterSchema tLLUWMasterSchema = new LLUWMasterSchema();
LLCUWMasterSchema tLLCUWMasterSchema = new LLCUWMasterSchema();

String tPolNo=request.getParameter("PolNo");
String tUWIdea=request.getParameter("UWIdea");

String tPassFlag=request.getParameter("uwstate");
String tSugUWFlag = request.getParameter("SugUWFlag");
String tLoadFlag = request.getParameter("LoadFlag");

tLLUWMasterSchema.setPolNo(tPolNo);
tLLUWMasterSchema.setProposalNo(tPolNo);
tLLUWMasterSchema.setPassFlag(tPassFlag);
tLLUWMasterSchema.setUWIdea(tUWIdea);
tLLUWMasterSchema.setSugPassFlag(tSugUWFlag);

//如果是加费的话，要区别健康加费与职业加�
//核保结论取消加费，将加费作为一个标志进行处理，
//核保结论统一为正常通过，变更承保，拒保

//封装加费信息
LCPremSet tLCPremSet = new LCPremSet();
String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
String tNo[] = request.getParameterValues("AddFeeGridNo");
int n = 0;
if(tNo != null )
n = tNo.length ;
if(n>0)
{
	for(int i=0 ; i<n ; i++ )
	{
		LCPremSchema tLCPremSchema = new LCPremSchema();
		tLCPremSchema.setPrem(tAddFeeValue[i]);
		tLCPremSchema.setRate(tAddFeeRate[i]);
		tLCPremSchema.setPayStartDate(tPayStartDate[i]);
		tLCPremSchema.setPayEndDate(tPayEndDate[i]);
		tLCPremSet.add(tLCPremSchema);
	}
}


// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.add(tLCUWMasterSchema);
tVData.add(tLCPremSet);

ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
try {
	System.out.println("this will save the data!!!");
	tManuUWRiskSaveUI.submitData(tVData,"");
	} catch(Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail")) {
		tError = tManuUWRiskSaveUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
		%>
		<html>
		<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
		</script>
		</html>
