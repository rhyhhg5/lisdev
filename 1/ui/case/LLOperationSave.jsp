<%
//程序名称：LLOperationSave.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容

%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import=" java.text.*"%>
<%@page import= "java.util.*"%>


<%
//接收信息，并作校验处理。
//输入参数
//保单明细类
LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();

//险种明细类
LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();

//陪案明细类
LLClaimSchema tLLClaimSchema = new LLClaimSchema();

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
TransferData RecalFlag = new TransferData();

String transact="";
String FlagStr = "";
String Content = "";
CErrors tError = null;
String tOpt=request.getParameter("Opt");
System.out.println("opt=="+tOpt);
String tCaseNo = request.getParameter("CaseNo");

//执行保存UPDATA操作  OperationTwoGrid
if("UPDATE".equals(tOpt)){
	transact = "UPDATE";
	
	//获取陪案明细的值
	String tGiveType1 = request.getParameter("GiveType1");
	String tGiveType2 = request.getParameter("GiveType2");
	if(!tGiveType1.equals(tGiveType2)) 
	{
		tLLClaimSchema.setCaseNo(tCaseNo);
		tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));
		tLLClaimSchema.setGiveType(tGiveType2);
		tLLClaimSchema.setGiveTypeDesc(request.getParameter("GiveTypeDesc2"));
	}
	
	//获取险种明细修改的值
	String[] str1PolNo = request.getParameterValues("OperationRisk2Grid7");
	String[] str1GiveTypeDesc = request.getParameterValues("OperationRisk2Grid3");
	String[] str1GiveType2 = request.getParameterValues("OperationRisk2Grid4");
	String[] str1GiveReasonDesc = request.getParameterValues("OperationRisk2Grid5");
	String[] str1GiveReason2 = request.getParameterValues("OperationRisk2Grid6");
	
	String[] str1GiveType1 = request.getParameterValues("OperationRisk1Grid4");
	String[] str1GiveReason1 = request.getParameterValues("OperationRisk1Grid6");
	
	int PolNoCount1 = str1PolNo.length;
	for(int i = 0; i < PolNoCount1 ; i++)
	{
		if(!str1GiveType2[i].equals(str1GiveType1[i]) || !str1GiveReason2[i].equals(str1GiveReason1[i]))
		{
			LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
			tLLClaimPolicySchema.setCaseNo(tCaseNo);
			tLLClaimPolicySchema.setPolNo(str1PolNo[i]);
			tLLClaimPolicySchema.setGiveTypeDesc(str1GiveTypeDesc[i]);
			tLLClaimPolicySchema.setGiveType(str1GiveType2[i]);
			tLLClaimPolicySchema.setGiveReasonDesc(str1GiveReasonDesc[i]);
			tLLClaimPolicySchema.setGiveReason(str1GiveType2[i]);
			tLLClaimPolicySet.add(tLLClaimPolicySchema);
		}
	}
	
	//获取保单责任明细修改的值
	String[] str2GetDutyCode = request.getParameterValues("OperationTwoGrid5");
	String[] str2GiveTypeDesc = request.getParameterValues("OperationTwoGrid6");
	String[] str2GiveType2 = request.getParameterValues("OperationTwoGrid7");
	String[] str2GiveReasonDesc = request.getParameterValues("OperationTwoGrid8");
	String[] str2GiveReason2 = request.getParameterValues("OperationTwoGrid9");
	
	String[] str2GiveType1 = request.getParameterValues("OperationOneGrid7");
	String[] str2GiveReason1 = request.getParameterValues("OperationOneGrid9");
	
	
	int GetDutyCodeCount = str2GetDutyCode.length;
	for(int i = 0; i < GetDutyCodeCount ; i++ )
	{
		//判断是否修改，没有修改的
		if(!str2GiveType2[i].equals(str2GiveType1[i]) || !str2GiveReason2[i].equals(str2GiveReason1[i]))
		{
			LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
			tLLClaimDetailSchema.setCaseNo(tCaseNo);
			tLLClaimDetailSchema.setGetDutyCode(str2GetDutyCode[i]);
			tLLClaimDetailSchema.setGiveTypeDesc(str2GiveTypeDesc[i]);
			tLLClaimDetailSchema.setGiveType(str2GiveType2[i]);
			tLLClaimDetailSchema.setGiveReasonDesc(str2GiveReasonDesc[i]);
			tLLClaimDetailSchema.setGiveReason(str2GiveReason2[i]);
			tLLClaimDetailSet.add(tLLClaimDetailSchema);
		}	
	}
	
	LLOperationUI tLLOperationUI = new LLOperationUI();
	
	boolean cs = true;
	VData tVData = new VData();
	try{
	    tVData.addElement(tG);
	    tVData.addElement(tLLClaimSchema);
	    tVData.addElement(tLLClaimPolicySet);
	    tVData.addElement(tLLClaimDetailSet);
	    cs = tLLOperationUI.submitData(tVData,transact);
	  }
	  catch(Exception ex){
	    Content = transact+"失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }
	  
	  if (FlagStr==""){
	    tError = tLLOperationUI.mErrors;
	    if (cs || !tError.needDealError()){
	      Content = "保存成功";
	      FlagStr = "Succ";
	    }
	    else{
	      Content = " 保存失败，原因是:" + tError.getFirstError();
	      FlagStr = "Fail";
	    }
	  }
}

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
