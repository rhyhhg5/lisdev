<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ： yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
  String tContNo = "";
  String tGrpContNo = "";
  String tContType ="";
  tContNo = request.getParameter("ContNo");
 
  tGrpContNo = request.getParameter("GrpContNo");
 
	tContType= request.getParameter("ContType");
%>                            
<SCRIPT>
var turnPage = new turnPageClass();
var strSQL;
var mulLineC;
var tContNo = "<%=tContNo%>"
var tGrpContNo = "<%=tGrpContNo%>"
var tContType = "<%=tContType%>"

function initForm()
{
	initbqViewGrid();

}

//查询作业历史(个单)
function queryPEndorItemGrid()
{
	strSQL ="select edoracceptno,contno,makedate,"+
					"(select edorname from lmedoritem where appobj='I' and edorcode=edortype),getmoney,operator "+
		      ",(select codename from ldcode where codetype='endortype' and code=edorstate) from lpedoritem "+
					"where edoracceptno not in (select edoracceptno from lpgrpedoritem)"+
					
					"and contno='" +tContNo+ "'"+
					" union " +
					"select edoracceptno,grpcontno,makedate,"+
					"(select edorname from lmedoritem where appobj='G' and edorcode=edortype),getmoney,operator "+
					",(select codename from ldcode where codetype='endortype' and code=edorstate) from lpgrpedoritem "+
					"where grpcontno='"+tGrpContNo+"'"+
					"order by makedate ";
					
					
					
	turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, bqViewGrid);
	
	mulLineC=bqViewGrid.mulLineCount;
	

	return true;
}

/* 查看批单 */
function getRowData()
{	

	var WorkNo;
  var tSel = bqViewGrid.getSelNo();
  	if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击返回按钮。" );
		else
		{
		   WorkNo = bqViewGrid.getRowColData(tSel-1, 1);
		  }
  
if (tContType=='2')
{
  window.open("../f1print/GrpEndorsementF1PJ1.jsp?EdorNo=" + WorkNo+"&ShowToolBar= false ");
 }
 if(tContType=='1')
 { 
 	
 	window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+WorkNo+"");
	}  


	
}


//初始化项目列表
function initbqViewGrid()
{                               
	var iArray = new Array();
	try
	{
	  	iArray[0]=new Array();
	  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  	iArray[0][1]="30px";            		//列宽
	  	iArray[0][2]=10;            			//列最大值
	  	iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
	  	iArray[1]=new Array();
	  	iArray[1][0]="受理号";
	  	iArray[1][1]="100px";
	  	iArray[1][2]=100;
	  	iArray[1][3]=0;
	  	
	  	iArray[2]=new Array();
	  	iArray[2][0]="合同号";
	  	iArray[2][1]="100px";
	  	iArray[2][2]=100;
	  	iArray[2][3]=0;
	  	
	  	iArray[3]=new Array();
	  	iArray[3][0]="受理时间";
	  	iArray[3][1]="100px";
	  	iArray[3][2]=100;
	  	iArray[3][3]=0;

	  	
	  	iArray[4]=new Array();
	  	iArray[4][0]="变更项目";
	  	iArray[4][1]="100px";
	  	iArray[4][2]=100;
	  	iArray[4][3]=0;      
	  	
	  	iArray[5]=new Array();
	  	iArray[5][0]="补/退费金额";
	  	iArray[5][1]="100px";
	  	iArray[5][2]=100;
	  	iArray[5][3]=0;  
	  	
	  	iArray[6]=new Array();
	  	iArray[6][0]="经办人";
	  	iArray[6][1]="100px";
	  	iArray[6][2]=100;
	  	iArray[6][3]=0;       
	  	
	  	iArray[7]=new Array();
	  	iArray[7][0]="状态";
	  	iArray[7][1]="100px";
	  	iArray[7][2]=100;
	  	iArray[7][3]=0;        
	  	
	  	
		bqViewGrid = new MulLineEnter( "fm" , "bqViewGrid" ); 
		//这些属性必须在loadMulLine前
		bqViewGrid.mulLineCount = 0;   
		bqViewGrid.displayTitle = 1;
		  bqViewGrid.locked = 1;
      bqViewGrid.canSel = 1;
		  bqViewGrid.selBoxEventFuncName ="getRowData" ;     //点击RadioBox时响应的JS函数
      bqViewGrid.hiddenPlus = 1;
      bqViewGrid.hiddenSubtraction = 1;
      bqViewGrid.loadMulLine(iArray); 
      
	}
	catch(ex)
	{
	  alert(ex);
	}
	queryPEndorItemGrid();
	
}


</SCRIPT>