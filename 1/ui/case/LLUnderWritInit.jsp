<%
  //程序名称：LLUnderwritInit.jsp
  //程序功能：理赔二核送核
  //创建日期：2013-11-26
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
function initForm()
{
	try{
		initInpBox();
		initContDetailGrid();
		initUnderWritGrid();
		queryContDetail();
		queryUnderWrit();
	}
	catch(re){
		alert("LLUnderWritInit.jspInitForm函数中发生异常:初始化界面错误!");
	}
}

function initInpBox()
	{
		try
		{
				try
				{
					
					fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
					if(fm.CaseNo.value=="null")
						fm.CaseNo.value="";
		            fm.CustomerNo.value = '<%= request.getParameter("InsuredNo") %>';

					fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
					var tSQL = "select Name from ldperson where CustomerNo='"+fm.CustomerNo.value+"'";
				    var xrr = easyExecSql(tSQL);
				    if(xrr!= null){
				  	  fm.CustomerName.value = xrr[0][0];
				    }
				}
				catch(ex)
				{
					alert(ex);
				}
				<%GlobalInput mG = new GlobalInput();
				mG=(GlobalInput)session.getValue("GI");
				%>
		}
		catch(ex)
		{
			alter("在LLUnderWritInit.jsp,InitInpBox函数中发生异常:初始化界面错误!");
		}
	}

function initContDetailGrid()
{
	var iArray = new Array();
	try{ 
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="保单号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="保单状态";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="生效日期";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="失效日期";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="险种代码";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="险种名称";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;


		ContDetailGrid = new MulLineEnter( "fm" , "ContDetailGrid" ); 

		ContDetailGrid.mulLineCount=1;
		ContDetailGrid.displayTitle=1;
		ContDetailGrid.canSel=0;
		ContDetailGrid.canChk=0;
		ContDetailGrid.hiddenPlus=1;
		ContDetailGrid.hiddenSubtraction=1;

		ContDetailGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initUnderWritGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="案件号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="险种代码";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="险种名称";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="二核提起时间";
		iArray[4][1]="90px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="提起二核用户代码";
		iArray[5][1]="120px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="提起二核用户姓名";
		iArray[6][1]="120px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="二核结论";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="理赔结论";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="保单险种号";
		iArray[9][1]="60px";
		iArray[9][2]=100;
		iArray[9][3]=3;


		UnderWritGrid = new MulLineEnter( "fm" , "UnderWritGrid" ); 

		UnderWritGrid.mulLineCount=1;
		UnderWritGrid.displayTitle=1;
		UnderWritGrid.canSel=1;
		UnderWritGrid.canChk=0;
		UnderWritGrid.hiddenPlus=1;
		UnderWritGrid.hiddenSubtraction=1;
		//查询既往二核信息
		UnderWritGrid.selBoxEventFuncName = "queryPassFlag";
		
		UnderWritGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
