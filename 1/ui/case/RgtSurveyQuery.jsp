<%@page contentType="text/html;charset=GBK" %> 
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="RgtSurveyQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="RgtSurveyQueryInit.jsp"%>
  <title>报案查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./RgtSurveyQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 报案信息部分 fraSubmit-->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLLReport1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            案件号
          </TD>
          <TD  class= input>
            <Input class= common readonly name=ClmCaseNo >
          </TD>
          <TD  class= title>
            案件类型
          </TD>
          <TD  class= input>
            <Input class=code readonly name=Type verify="案件类型|NOTNULL" CodeData="0|^0|报案分案^1|分案" ondblClick="showCodeListEx('SurveyType',[this],[0,1,2,3]);" onkeyup="showCodeListKeyEx('SurveyType',[this],[0,1]);">
          </TD>
          </TR>
          
          <TR  class= common>
          <TD  class= title>
          立案号
          <TD  class= input>
            <Input class= common readonly name=RgtNo >
          </TD>
          <TD  class= title>
          调查人姓名
          <TD  class= input>
            <Input class= common name=SurveyorName >
          </TD>
          <input type=hidden name="Content">   
          </TR>
      </table>
    </Div>
          <INPUT class=common VALUE="查  询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT class=common VALUE="返  回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport2);">
    		</td>
    		<td class= titleImg>
    			 调查报告信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanRgtSurveyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button class=common onclick="getFirstPage();" > 
      <INPUT VALUE="上一页" TYPE=button class=common onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class=common onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" TYPE=button class=common onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
  <script language="javascript">
  try
  {
    fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
    fm.ClmCaseNo.value = '<%= request.getParameter("ClmCaseNo") %>';
    fm.Type.value = '<%= request.getParameter("Type") %>';
  		   				   		
  }
  catch(ex)
  {
    alert(ex);
  }
 
</script>

</html>