<%
//程序名称：LLCaseCancelInit.jsp
//程序功能：给付确认的初始化
//创建日期：
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  
%>
                          
<script language="JavaScript">
function initInpBox()
{
 try
 {
 	var LoadFlag = '<%=request.getParameter("LoadFlag")%>';
 	fm.LoadFlag.value=LoadFlag;
 if(LoadFlag=='2')
 {
	 	divRgt.style.display='';
	 	divCase.style.display='none';
		try
		{
			fm.CaseNo.value = "";
			fm.CustomerNo.value = "";
			fm.CustomerName.value = "";
		 	fm.RgtNo.value = '<%=request.getParameter("CaseNo")%>';
		  fm.GrpNo.value = '<%= request.getParameter("CustomerNo") %>';
		  var tsql = "SELECT grpname,cancelreason,(select codename from ldcode where codetype='llcasecancel' and code=cancelreason),rgtreason FROM llregister WHERE rgtno='"+fm.RgtNo.value+"'";
		  var xrr = easyExecSql(tsql);
		  if(xrr)
		    fm.GrpName.value = xrr[0][0]; 
		    fm.CancleReason.value = xrr[0][1];
		    fm.CancleReasonName.value = xrr[0][2];
		    fm.CancleRemark.value = xrr[0][3];
		}
		catch(ex)
		{
		  alert(ex);
		}
 }
 else
 	{
		try
		{    
	 	divRgt.style.display='none';
	 	divCase.style.display='';
			fm.RgtNo.value = "";
			fm.GrpNo.value = "";
			fm.GrpName.value ="";
		 	fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
		  fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
		  fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>'; 
		  var tsql = "select CustomerName,canclereason,(select codename from ldcode where codetype='llcasecancel' and code=canclereason),cancleremark from llcase where CaseNo='"+fm.CaseNo.value+"'";
		  var xrr = easyExecSql(tsql);
		  if(xrr){
		    fm.CustomerName.value = xrr[0][0];
		    fm.CancleReason.value = xrr[0][1];
		    fm.CancleReasonName.value = xrr[0][2];
		    fm.CancleRemark.value = xrr[0][3];
		  }
		}
		catch(ex)
		{
		  alert(ex);
		}
	}
	var LoadD = '<%=request.getParameter("LoadD")%>';
 	fm.LoadD.value=LoadD;
	if(LoadD=='1')
	{
	   document.getElementById('sub1').disabled = true;
	}
	
	
 	  <%GlobalInput mG = new GlobalInput();
  	mG=(GlobalInput)session.getValue("GI");
  	%>
 }
 catch(ex)
 {
 alter("在LLRgtSurveyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
 }
 }
 function initForm()
 {
 try
 {
 initInpBox();
 }
 catch(re)
 {
 alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
 }
 }

</script>