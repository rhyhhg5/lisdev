<%
//Name:LLClaimCloseConfListInit.jsp
//function：批量通知给付个人案件
//author:Xx
//Date:2006-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
function initInpBox(){
  try{
  
  }
  catch(ex){
    alert("在LLClaimConfListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox(){
  try{
  }
  catch(ex){
    alert("在LLClaimConfListInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(){
  try{
    initInpBox();
    initSelBox();
    initCaseGrid();
    easyQueryClick();
  }
  catch(re){
    alert("在LLClaimConfListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCaseGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10",0);
    iArray[0][0]="序号";
    iArray[0][1]="20px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array("理赔号","130px","20",0);
    iArray[2]=new Array("客户姓名","60px","100",0);
    iArray[3]=new Array("客户号码","70px","20",0);
    iArray[4]=new Array("案件赔款","60px","10","0");
    iArray[5]=new Array("给付方式","55px","10","2");
    iArray[5][4]= "llgetmode";
    iArray[5][5]= "5|11";
    iArray[5][6]= "1|0";
    iArray[6]=new Array("领款人","50px","100","1");
    iArray[7]=new Array("领款人证件号码","130px","20","1");
    iArray[8]=new Array("银行编码","50px","20","2");
    iArray[8][4] = "llbank";
    iArray[9]=new Array("银行账号","100px","30","1");
    iArray[10]=new Array("账户名","50px","100","1");
    iArray[11]=new Array("给付方式代码","0px","10","3");
    iArray[12]=new Array("批次号","0px","10","3");
    iArray[13]=new Array("回销预付赔款","80px","10","2","prepaidflag");
    iArray[13][5]="13|16";
    iArray[13][6]="1|0";
     
    iArray[14]=new Array("回销预付赔款金额","100px","10","0");     
    iArray[15]=new Array("实际给付赔款","80px","10","0"); 
    iArray[16]=new Array("回销预付标记","80px","10","3"); 

    CaseGrid = new MulLineEnter("fm","CaseGrid");
    CaseGrid.mulLineCount =10;
    CaseGrid.displayTitle = 1;
    CaseGrid.locked = 1;
    CaseGrid.canChk =1;
    CaseGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CaseGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CaseGrid. selBoxEventFuncName = "onSelSelected";
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

</script>