<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp" %>
<%
%>
<%@page import="java.net.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
    
    //response.reset();
	String filePath = request.getParameter("filePath");//即将下载的文件的相对路径    
	String fileName =request.getParameter("fileName");//下载文件时显示的文件保存名称
	System.out.println("DownLoadPageParams----->filePath:"+filePath+";fileName:"+fileName);
	String urlto=request.getRequestURL().toString();
	String context=request.getContextPath();
	String url=request.getRequestURI();
	//int x=urlto.indexOf(url);
	//System.out.println(x);
	String str=urlto.substring(0,urlto.indexOf(url));
	System.out.println("str="+str);
	
//	System.out.println("getRequestURL:"+urlto);
//	System.out.println("getContextPath:"+context);
//	System.out.println("getRequestURI:"+url);
	
	//System.out.println("###request.getPathInfo()" + request.getPathInfo());
	//###request.getRequestURL()http://localhost:8080/lis/case/ReportDownLoadSave.jsp
	//###request.getContextPath()/lis
	//###request.getRequestURI()/lis/case/ReportDownLoadSave.jsp
	//###request.getPathInfo()null
    if( fileName.endsWith(".xls") ) 
    {
    	response.setContentType("application/x-download");//设置为下载application/x-download
    	//通过设置头标题来显示文件传送到前端浏览器的文件信息
   		 response.addHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(fileName,"UTF-8"));
    	//addHeader(String name, String value);
    	System.out.println("#############" + filePath + fileName); 
    	try
    	{
       	 	RequestDispatcher dis = application.getRequestDispatcher(filePath + fileName);
     		//一个RequestDispatcher对象可以用来转发请求到资源或包含在响应中的资源。给定的路径。
        	if(dis!= null)
        	{
            	dis.forward(request,response);
        	}
        	response.flushBuffer();
    	}
    	catch(Exception e)
    	{
        	e.printStackTrace();
    	}
    	finally
    	{
    	}
    }
    else if ( fileName.endsWith(".vts") ) 
    {
    	try
    	{
			System.out.println("开始打开报表!");
			ExeSQL tExeSQL = new ExeSQL();
			System.out.println("==> Write VTS file to disk ");
          
	        String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
			String strFilePath = tExeSQL.getOneValue(strSql);
			String strVFFileName = filePath + fileName;
			//获取存放临时文件的路径
			String strRealPath = application.getRealPath("/").replace('\\','/');
			String strVFPathName = strRealPath +"//"+ strVFFileName;	  
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
	  	} catch (Exception ex)  {
	  		
	  	}
	}
    else if ( fileName.endsWith(".zip") || fileName.endsWith(".csv")) 
    {
    	String realpath = application.getRealPath("/").substring(0,application.getRealPath("/").length()).replace("\\","/");//UI地址
    	String temp = realpath.substring(realpath.length() - 1, realpath
    			.length());
    	if (!temp.equals("/")) {
    		realpath = realpath + "/";
    	}
    	
    	filePath = realpath+filePath+fileName;
%>
<html>
<a href="../f1print/CJDownLoad.jsp?filename=<%=fileName%>&filenamepath=<%=filePath%>">点击下载</a>
</html>
<%
    	
	}
%>
