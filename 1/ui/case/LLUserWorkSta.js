//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var DealWithNam ;

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function Quantity() //用户工作量
{
	if (verifyInput() == false)
    return false;
//	var tStartDate = fm.StartDate.value;
//    var tEndDate = fm.EndDate.value;
//    var result = fm.result.value;
//    var isWeek = fm.isweek.value;
//    if(isWeek!='Y'){
//       if(result=='Y'){
//           if(dateDiff(tStartDate,tEndDate,"M")>3){
//               alert("上班时间，统计期最多为三个月！");
//               return false;
//           }
//       }
//    }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Quantity";
	fm.action = "LLUserQuantySub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function QuantityGroup()
{
	if (verifyInput() == false)
    return false;
    var mStatsType = fm.StatsType.value;
    if ("1"==mStatsType || "2"==mStatsType) {
        alert("分组用户工作量统计暂不支持‘商业保险’、‘社会保险’的统计");
        return false;        
    }
  	var i = 0;
  	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  	//showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "QuantityGroup";
	fm.action = "LLUserQuantyGroupSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function Quality()
{
	if (verifyInput() == false)
    return false;
	var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  if(dateDiff(tStartDate,tEndDate,"M")>3){
    alert("统计期最多为三个月！");
    return false;
  }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Quality";
	fm.action = "LLUserQualitySub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function Efficiency()
{
	if (verifyInput() == false)
    return false;
  var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  if(dateDiff(tStartDate,tEndDate,"M")>3){
    alert("统计期最多为三个月！");
    return false;
  }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Efficiency";
	fm.action = "LLUserEfficientSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function afterCodeSelect( cCodeName, Field )
{
	//选择了处理
	if( cCodeName == "DealWith")
	{
		switch(fm.all('DealWith').value){
			case "0":
			Quantity();             //用户工作量统计
    //  alert("该报表需求已改变，尚处于开发阶段。")
			break;
			case "1":
			QuantityGroup();		//分组用户工作量统计
		//	Quality();              //工作质量统计
			break;
			case "2":
			Efficiency();           //工作效率统计
		}
	}
}
