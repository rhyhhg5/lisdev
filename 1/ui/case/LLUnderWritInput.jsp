<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：LLUnderwritInput.jsp
 //程序功能：理赔二核送核
 //创建日期：2013-12-13
 //创建人  ：Houyd
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="LLUnderWritInput.js"></SCRIPT>
  <%@include file="LLUnderWritInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./LLUnderWritSave.jsp" method=post name=fm target="fraSubmit">

<table  class="common" >
  <tr class="common">
<TD  class= title>理赔案件</TD><TD  class= input><Input class= readonly name="CaseNo" readonly></TD>
<TD  class= title>客户号</TD><TD  class= input><Input class= readonly name="CustomerNo" readonly></TD>
<TD  class= title>客户姓名</TD><TD  class= input><Input class= readonly name="CustomerName" readonly></TD>
  </tr>
</table>

      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divReceipt);">
          </td>
          <td class= titleImg>
            保单信息
          </td>
        </tr>
      </table>
      <div id='divReceipt' style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanContDetailGrid" >
              </span>
            </td>
          </tr>
        </table> 
       </div>

	<Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisease);">
            </TD>
            <TD class=titleImg>
              既往理赔二核记录
            </TD>
          </TR>
        </Table>
        <Div id = "divDisease" style = "display: ''">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanUnderWritGrid" >
                </span>
              </TD>
            </TR>
          </Table>
         </div>



<table  class="common" >
  <tr class="common">
  </tr>
</table>
<input class="readonly" name="UnderWritTitle" readonly value="二核意见" >
<br><BR>
<textarea class="common" name="UnderWritComment" cols="100%" rows="2" readonly></textarea>
<br><BR>
<input class="readonly" name="RemarkTitle" readonly value="备注" >
<br><BR>
<textarea class="common" id="Remark" name="Remark" cols="100%" rows="2" ></textarea>
<br><BR>
<div align=right>
<input value="提起二核"  onclick="submitUnderWrit()" class="cssButton" type="button" >
<input value="返回"  onclick="top.close();" class="cssButton" type="button" >
</div>
<br>
<input type=hidden name='RgtNo'>
<input type=hidden id="fmtransact" name="fmtransact">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
