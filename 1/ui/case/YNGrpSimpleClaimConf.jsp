<html>
<%
//Name： YNGrpSimpleClaimConf.jsp
//Function：云南大单团体审定
//Date：2004-12-23 16:49:22
//Author：Xx
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom; 	  	
   if(Comcode.length()<4||Comcode.substring(0,3)!="8653"){
   Comcode="8653";
   }
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-60";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="YNGrpSimpleClaimConf.js"></SCRIPT>
   <%@include file="YNGrpSimpleClaimConfInit.jsp"%>
   <script language="javascript">
   function initDate(){
   fm.RgtDateStart.value="<%=afterdate%>";
   fm.RgtDateEnd.value="<%=CurrentDate%>";
   fm.Operator.value="<%=Operator%>";
       try
  {     
	    fm.all('ManageCom').value="<%=Comcode%>"; 
        var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ComName').value=arrResult[0][0];
           }  
 
  }
  catch(ex)
  {
    alert("在GrpSimpleClaimConf.jsp-->initDate函数中发生异常:初始化界面错误!");
  } 
  }
   </script>
 </head>

 <body  onload="initDate();initForm();" >
   <form action="./YNGrpSimpleClaimConfSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegisterSearch);">
         </TD>
         <TD class= titleImg>
         团体批次案件列表
         </TD>
       </TR>
      </table>

	    <div id= "divGrpRegisterSearch" style= "display: ''" >
		<table  class= common>
		<TR  class= common8>
		<TD  class= title>管理机构</TD>
        <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>
		<TD  class= title8>团体批次号</TD>
		<TD  class= input8><Input class= common name="srRgtNo"  onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>团体客户号</TD>
		<TD  class= input8><Input class=common name="srCustomerNo" onkeydown="QueryOnKeyDown();"></TD>					
		</TR>
		<tr>
		<TD  class= title8>批次状态</TD>
		<TD  class= input8><Input onClick="showCodeList('llgrprgtstate',[this,RgtStateName],[0,1]);" onkeyup="showCodeListKeyEx('llgrprgtstate',[this,RgtStateName],[0,1]);" class=codeno name="GrpRgtState" onkeydown="QueryOnKeyDown()"><Input class= codename name=RgtStateName onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title8>申请日期 起始</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateStart onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>结束</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateEnd onkeydown="QueryOnKeyDown();"></TD>							
		</TR>
		<tr>
		<TD  class= title8>单位名称</TD>
		<TD  class= input8><Input class=common name="srGrpName" onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>申请人</TD>
		<TD  class= input8><input class= common name="srRgtantName" onkeydown="QueryOnKeyDown();"></TD>				
		</tr>
		</table>
	    </div>

   <input name="AskIn" style="display:'none'"  class=cssButton type=button value="查 询" onclick="SearchGrpRegister()">
   

    <Div  id= "divCustomerInfo" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpRegisterGrid" >
            </span>
          </TD>
        </TR>
      </table>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 	
    </Div>
     <hr>
<Div  id= "divGrpCaseInfo" style= "display: 'none'">
	  <table>
	    	<tr>
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
	    	</td>
	    	<td class= titleImg>
	    	案件列表
	    	</td>
	    </tr>
	  </table>
      <table  class= common>
				<TR  class= common8>
					<TD  class= title8 colspan=3>
							<input type="radio" name="StateRadio"  value="0" onclick="queryGrpCaseGrid()">全部案件 
							<input type="radio" name="StateRadio"  value="1" checked onclick="queryGrpCaseGrid()">待审定 
							<input type="radio" name="StateRadio"  value="4" onclick="queryGrpCaseGrid()">审定上报
							<input type="radio" name="StateRadio"  value="5" onclick="queryGrpCaseGrid()">审定抽检
							<input type="radio" name="StateRadio"  value="2" onclick="queryGrpCaseGrid()">审定回退
							<input type="radio" name="StateRadio"  value="3" onclick="queryGrpCaseGrid()">审定通过
				 	</TD>
				</TR>
	    </table>
    <Div  id= "divGrpCaseGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpCaseGrid" >
            </span>
          </TD>
        </TR>
      </table>	
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">      
    </Div> 
    <br>
<Div  id= "divButton" align=right style= "display: ''">
<input name="AskIn" style="display:''"  class=cssButton type=button value="查看扫描件" onclick="ScanQuery()">
<input name="AskIn" style="display:''"  class=cssButton type=button value="查看详情" onclick="CaseInfo()">
<input name="RSDTG" style="display:''"  class=cssButton type=button value="批量审定" onclick="RGTEnsure()">
<input name="SDTG" style="display:''"  class=cssButton type=button value="审定通过" onclick="CaseEnsure()">
<input name="CASERETURN" style="display:''"  class=cssButton type=button value="案件回退" onclick="CaseBack()">
<input name="TSurvey" style="display:''"  class=cssButton type=button value="提起调查" onclick="submitFormSurvery()">
<input name="SurveyR" style="display:''"  class=cssButton type=button value="调查报告" onclick="SurveyPrint()">
</div>
<Div  id= "divBackList" style= "display: 'none'">
        <hr>
	  <table>
	    	<tr>
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
	    	</td>
	    	<td class= titleImg>
	    	回退列表
	    	</td>
	    </tr>
	  </table>
    <Div  id= "divGrpCaseGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanBackListGrid" >
            </span>
          </TD>
        </TR>
      </table>	
    </Div> 
<Div  id= "divButton" align=right style= "display: ''">
<input name="BackEnsure1" style="display:''"  class=cssButton type=button value="确认" onclick="BackEnsure()">
<input name="AskIn" style="display:''"  class=cssButton type=button value="关闭" onclick="CancelBack()">
</div>
</div>
        <hr>
    <div  id= "divClaimTotal" style= "display: ''">
    <table  class= common>
<TR  class= common8>
<TD  class= title8>理算金额</TD><TD  class= input8><Input class= readonly name="StandPay" readonly></TD>
<TD  class= title8>申报金额</TD><TD  class= input8><Input class= common name="AppAmnt" ></TD>
<!--
<TD  class= title8>实赔金额</TD><TD  class= input8><Input class= common name="RealPay" readonly></TD>
-->
</TR> 
</table>
</div>
<Div  id= "divButton" align=right style= "display: 'none'">
<input name="AskIn" style="display:''"  class=cssButton type=button value="团体结案" onclick="CaseBack()">
</div>

</Div>  
        
<!--
<input class="codeno" name="GRprt" CodeData="0|2^1|团体赔付报告1^2|团体赔付报告2" onclick="return showCodeListEx('grprint',[this,GRprtName],[0,1]);" onkeyup="return showCodeListKeyEx('grprint',[this,GRprtName],[0,1]);"><input class=codename  name=GRprtName>
-->
  
     
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="operate" >
     <Input type="hidden" class= common name="Operator" >
     <Input type="hidden" class= common name="RgtNo" >
      <Input type="hidden" class= common name="SurveyNo" >
      <Input type="hidden" class= common name="NewStateRadio" >
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
