<html>
<%
/*******************************************************************************
 * Name     :QueryCheck.jsp
 * Function :理赔－理算审核查询的初始化页面程序
 * Date     :2003-12-22
 * Author   :LiuYansong
 */
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>


<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="QueryCheck.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="QueryCheckInit.jsp"%>

</head>
<body  onload="initForm();" >
  <form action="./CasePolicySave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏LLCase1的信息 -->
     <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
      </td>
      <td class= titleImg>
        立案分案信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLLCase1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD class= title>
            立案号
          </TD>
          <TD class= input>
            <Input class="readonly" readonly name = RgtNo>
            <Input type = hidden name = ClmUWNo><!--用来记录该案件的第几次审核-->
            <Input type = hidden name = UpClmUWer><!--用来记录签批人的信息>-->
          </TD>

        </TR>

      </table>
    </Div>
    <!--案件审核信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCasePolicy);">
    		</td>
    		<td class= titleImg>
    			 案件审核信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divCasePolicy" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCasePolicyGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
  <INPUT VALUE = "返回明细信息" type = button class = common onclick = ShowCheckInfo()>
  <INPUT VALUE="关  闭" TYPE=button class=common onclick="top.close();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';

}
catch(ex)
{
  alert(ex);
}
</script>
</html>