<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：EasyScanQuery.jsp
//程序功能：扫描件显示
//创建日期：2002-09-28 17:06:57
//创建人  ：胡博
//更新记录：  更新人    更新日期     更新原因/内容
//更新记录:dingjw 2010-03-19   增加了支持查询多种单证的方法
%>

<html>
<head>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<%@include file="../common/EasyScanQuery/EasyScanQueryKernel.jsp"%>
	
	<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
	<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/EasyScanQuery/DebugAutoMove.js"></SCRIPT>
</head>

<%
  //存储图片信息
  String[] arrPic;
  
  String prtNo = request.getParameter("prtNo");
  String clientUrl = (String)session.getValue("ClientURL");//LQ 2004-04-20
   
  //查询多种单证的标志
  String multi = request.getParameter("MULTI");
  String easyway=request.getParameter("EASYWAY");
  System.out.println("-----Easyway is "+easyway+"-----");
  //只通过main表查询

  if(multi == null || multi.equals(""))
  {
      if(StrTool.cTrim(easyway).equals("SCAN"))
  	  {
	         System.out.println("---in main---");
	         String Docid =request.getParameter("DocID");
	         System.out.println("Docid is "+Docid);
	         String DocCode=request.getParameter("DocCode");
	         String BussType = request.getParameter("BussType");
	         String SubType = request.getParameter("SubType");
	         System.out.println("ClientUrl=" + clientUrl);			 //DJB 2005-12-30
	         arrPic = easyScanQueryKernelEasyway(Docid,DocCode,BussType,SubType,clientUrl);
	         System.out.println("---EasyScanQuery easyway End---");
      }
      else
      {
            System.out.println("---in relation---");
            String BussNoType = request.getParameter("BussNoType");
            String BussType = request.getParameter("BussType");
            String SubType = request.getParameter("SubType");
            System.out.println("ClientUrl=" + clientUrl);			 //LQ 2004-04-20
            arrPic = easyScanQueryKernel(prtNo,BussNoType,BussType,SubType,clientUrl);
            System.out.println("---EasyScanQuery End---");
      }
  }
  //查询多种单证
  else
  {
      String[] BussNoType = (String[]) session.getAttribute("BussNoType");
      String[] BussType = (String[]) session.getAttribute("BussType");
      String[] SubType = (String[]) session.getAttribute("SubType");
      String Order = request.getParameter("Order");
      
      session.removeAttribute("BussNoType");
      session.removeAttribute("BussType");
      session.removeAttribute("SubType");
      
      System.out.println("easyScanQueryKernelMuli start...");
      arrPic = easyScanQueryKernelMuli(prtNo,BussNoType,BussType,SubType,clientUrl,Order);
      System.out.println("easyScanQueryKernelMuli end...");
  }
  
  System.out.println("*********************arrPic=" + arrPic);
  
  if (arrPic != null) 
  {
    %>

	<body border="0">
		<!-- 显示提示文字 -->
		<font color=red style="font-size:9pt;"><center id="centerPic" class=common></center></font>
		
		<!-- 使用该DIV类可以控制图片的显示方向 -->
		<DIV ID="filterDIV" STYLE="position:absolute;
		filter:progid:DXImageTransform.Microsoft.BasicImage(grayscale=0, xray=0, mirror=0, invert=0, opacity=1, rotation=0)">
		<img border="0" width="100%" id="service" src="">
		</DIV>
		
		<!---->
		<span id="spanPosition"  style="display: 'none'; position:absolute; slategray">
		  <img border="0" id="Rect" src="./frame.GIF">
		    <br>
		  <Input class= common2 name="RectCont" readOnly STYLE="display:'none'">
		</span>
	</body>
</html>

<script>
try 
{ 
  //获取每个图片的完整URL信息
  var arrPicName = new Array();
  <%
	  for (int i=0; i<arrPic.length; i++) 
  {%>
	    arrPicName[<%=i%>] = '<%=arrPic[i]%>';
	  <%}
  %>

  //将存有文件路径的数组传递给图片控制的桢
  window.top.fraInterface.pic_name = arrPicName;
  
  //初始化图片页码
  top.pic_place = 0;

  //显示第一幅图片
  window.service.src = arrPicName[0]; 
  //显示文件路径和名称，也用于提示文件不存在
  window.centerPic.innerHTML = "PageDown:下一页 | PageUp:上一页 | Ctrl和+:放大图片 | Ctrl和-:缩小图片 | *:恢复原图 | Ctrl和方向键（数字键盘，NumLock）:控制图片窗口 | Alt和方向键:控制录入窗口";

  var prtNo = "<%=prtNo%>"; 
  //获取扫描图片类型
  /*
  var arrResult = top.queryScanType();
  for (i=0; i<arrResult.length; i++) {
    if (prtNo.substring(2,4) == arrResult[i][0]) {
      goToPic(arrResult[i][1]);
      try { top.fraInterface.fm.all(arrResult[i][2]).focus(); } catch(e) {}
    }
  }*/
} catch(ex) { 
  alert("EasyScanQuery.jsp:" + ex.message); 
  window.centerPic.innerHTML = "该扫描件图片不存在";
}

</script>

<%
}
	else 
	{
		%>
		<body border="0">
		<center id="centerPic">该印刷号对应的扫描件图片不存在</center>
		</body>
		</html>
		<%
	}
%>

<script>
  try {
    if (top.fraInterface.LoadFlag == "99") {
      window.document.body.onmousemove = mouseMove;
      window.document.body.onmousedown = mouseDown;
    }
  } catch(e) {}
</script>