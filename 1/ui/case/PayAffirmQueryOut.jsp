<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：PayAffirmQueryOut.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
  
   tLLClaimSchema.setRgtNo(request.getParameter("RgtNo"));
   tLLCaseSchema.setCustomerName(request.getParameter("CustomerName"));
   tLLRegisterSchema.setRgtObj(request.getParameter("RgtObj"));
   tLLRegisterSchema.setRgtObjNo(request.getParameter("RgtObjNo"));
   
   GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
	String tFlag=request.getParameter("MenuFlag");    
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLLClaimSchema);
	tVData.addElement(tLLCaseSchema);
	tVData.addElement(tLLRegisterSchema);
	tVData.addElement(tFlag);
	tVData.addElement(tG);
	

  // 数据传输
  ClaimUI tClaimUI   = new ClaimUI();
	if (!tClaimUI.submitData(tVData,"QUERYNEW"))
	{
      Content = " 查询失败，原因是: " + tClaimUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tClaimUI.getResult();

	// 显示
		String tReturnStr=(String)tVData.getObjectByObjectName("String",0);
		System.out.println("ReturnString is :"+tReturnStr);
		if (tReturnStr.length()!=0)
		{
		   	%>
		<script language="javascript">
		try {
		 parent.fraInterface.displayQueryResult('<%=tReturnStr%>');
		 } catch(ex) {}
		</script>
			<%
		}
		
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tClaimUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>