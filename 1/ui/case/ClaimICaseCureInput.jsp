<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：CrtHtml程序创建
//更新记录：  
//更新人:刘岩松
//更新日期：2002-09-24
//更新原因/内容:去掉＂附件序号 ＂录入对话框
                    //完成＂赔付标志＂的双击选择
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT> 
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="ClaimICaseCureInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ClaimICaseCureInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ClaimICaseCureSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏ICaseCure1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divICaseCure1);">
      </td>
      <td class= titleImg>
        分案诊疗主要信息
      </td>
    	</tr>
    </table>

    <Div  id= "divICaseCure1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            分案号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CaseNo >
          </TD>
          </TR>
        <TR  class= common>
          <TD  class= title>
            出险人客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            出险人名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            出险类型
          </TD>
          <TD  class= input>
            <Input class= code name=AccidentType ondblclick="return showCodeList('GetDutyKind',[this]);" onkeyup="return showCodeListKey('GetDutyKind',[this]);">
          </TD>
         </TR>
       </table>
    <!--分案诊疗信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseCure2);">
    		</td>
    		<td class= titleImg>
    			 分案诊疗明细信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divCaseCure2" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanICaseCureGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
         <INPUT VALUE="保存" class=common TYPE=button onclick="submitForm();"> 					
         <INPUT VALUE="返回" class=common TYPE=button onclick="top.close();"> 					


    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';
  fm.CustomerNo.value =' <%= request.getParameter("CustomerNo") %>'; 
}
catch(ex)
{
  alert(ex);
}
 
</script>

</html>