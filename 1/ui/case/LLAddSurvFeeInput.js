var showInfo;
var mDebug="0";
var turnPage1=new turnPageClass();
var turnPage2=new turnPageClass();
//间接调查费保存
function submitForm()
{
	for(i = 0; i <SurveyCaseGrid.mulLineCount; i++)
	{		
		if(SurveyCaseGrid.getChkNo(i))
		{
		  	var strSql="select distinct riskcode from llsurvey a,lcpol b "
		  			+" where a.customerno=b.insuredno and a.otherno='"+SurveyCaseGrid.getRowColData(i,1)+"'"
		  			+" union select distinct riskcode from llsurvey a,lbpol b "
		  			+" where a.customerno=b.insuredno and a.otherno='"+SurveyCaseGrid.getRowColData(i,1)+"'";
		   var arrResult = easyExecSql(strSql )
		   if(arrResult.length =1 )
		   {
			 var riskSql="select count(1) from lmriskapp where risktype='M' and risktype<>'7' and riskcode='"+arrResult[0][0] +"'";
			 if(easyExecSql(riskSql)>=1)
			 {
				alert("案件"+SurveyCaseGrid.getRowColData(i,1)+"的被保险人只投保了健管产品不能录入间接调查费!");
				return false ;
			 }	
		   } 
		}
	}
  fm.fmtransact.value = "INSERT";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:360px");
  fm.action = "./LLAddSurvFeeSave.jsp";
  fm.submit(); //提交
}
//间接调查费审核
function submitConf()
{
  fm.fmtransact.value = "CONFIRM";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLAddSurvFeeSave.jsp";
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
    updateCaseGrid();
    queryIndFee();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else{
    strSql = "select sum(FeeSum),min(FeeSum) from LLCaseIndFee where feedate='"+fm.MEndDate.value+"'  and Mngcom like '"+fm.comcode.value+"%%'";
    arr = easyExecSql(strSql);
    if(arr){
      fm.Sum.value=arr[0][0];
      fm.Average.value=arr[0][1];
      fm.Handler.value=fm.Operator.value;
    }
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    top.opener.initTitle();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm(){
  try{
    initForm();
  }
  catch(re){
    alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

function QueryOnKeyDown(){
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13"){
    updateCaseGrid();
    queryIndFee();
  }
}

function afterCodeSelect(ObjType,Obj){
  if(ObjType=='Month'){
    updateCaseGrid();
    queryIndFee();
  }
}
//本月调查案件
function updateCaseGrid(){
  month=fm.Month.value/1;
  year= fm.Year.value/1;
  days=getDays(month, year);
  fm.MStartDate.value = year+"-"+month+"-"+"01";
  fm.MEndDate.value = year+"-"+month+"-"+days;

  caseSql="select a.caseno ono,(select count(c.surveyno) from llsurvey c where c.otherno =a.caseno),b.codename,a.surveyflag"
        +" from llcase a,ldcode b where b.code=a.rgtstate and b.codetype='llrgtstate' and a.rgtstate in ('11','12')"
        +" and exists (select 1 from ljaget where (otherno=a.caseno or otherno=a.rgtno) and ManageCom like '"+fm.comcode.value +"%'"
        +" and (finstate<>'FC' or finstate is null)"
        +" and makedate>='"+fm.MStartDate.value+"' and makedate<='"+fm.MEndDate.value+"') and Mngcom like '"+fm.comcode.value+"%'"
        +" and (surveyflag='1' or (surveyflag='3' and a.caseno in (select otherno from llcaseindfee where Mngcom like '"
        +fm.comcode.value+"%' and feedate='"+fm.MEndDate.value+"'))) and not exists (select 1 from llinqapply where otherno=a.caseno and substr(inqdept,1,4)<>substr(mngcom,1,4)) ";
  consultSql="select a.consultno ono,(select count(c.surveyno) from llsurvey c where c.otherno =a.consultno),'',a.surveyflag"
       +" from llconsult a where 1=1 and makedate>='"+fm.MStartDate.value+"' and makedate <='"+fm.MEndDate.value+"' and Mngcom like '"+fm.comcode.value+"%'"
       +" and (surveyflag='1' or (surveyflag='3' and a.consultno in (select otherno from llcaseindfee where Mngcom like '"
       +fm.comcode.value+"%' and feedate='"+fm.MEndDate.value+"'))) and not exists (select 1 from llinqapply where otherno=a.consultno and substr(inqdept,1,4)<>substr(mngcom,1,4)) ";
  repealSql="select a.caseno ono,(select count(c.surveyno) from llsurvey c where c.otherno =a.caseno),b.codename,a.surveyflag "
        +" from llcase a,ldcode b where b.code=a.rgtstate and b.codetype='llrgtstate' and a.rgtstate ='14' "
        +" and a.CancleDate>='"+fm.MStartDate.value+"' and a.CancleDate<='"+fm.MEndDate.value+"' and Mngcom like '"+fm.comcode.value+"%'"
        +"and (surveyflag='1' or (surveyflag='3' and a.caseno in (select otherno from llcaseindfee where Mngcom like '"
        +fm.comcode.value+"%' and feedate='"+fm.MEndDate.value+"'))) and not exists (select 1 from llinqapply where otherno=a.caseno and substr(inqdept,1,4)<>substr(mngcom,1,4)) ";      
  strSql = "("+caseSql +" union all "+consultSql+" union all "+repealSql+") order by ono";
  arrResult = easyExecSql(strSql);
  if (arrResult){
  	displayMultiline(arrResult,SurveyCaseGrid);
    for ( i=0;i<arrResult.length;i++){
      if ( arrResult[i][3]== '3'){
          SurveyCaseGrid.checkBoxSel(i+1);
      }
    }
  }else{
  	initSurveyCaseGrid();
  	}
}

function queryIndFee(){
  month=fm.Month.value/1;
  year= fm.Year.value/1;
  days=getDays(month, year);
  fm.MStartDate.value = year+"-"+month+"-"+"01";
  fm.MEndDate.value = year+"-"+month+"-"+days;
  subsqlpart = " where b.feeitem=a.code and feedate='"+fm.MEndDate.value
  	+"' and b.Mngcom like '"+fm.comcode.value+"%'";
  feeSql = "select a.codename,a.code,nvl((select b.feesum from llindirectfee b "+subsqlpart+"),0),"
  +"(select case b.uwstate when '1' then '审核通过' when '4' then '异地审核通过' else '待审核' end from llindirectfee b,ldcode c "+subsqlpart
  +" and c.code=b.uwstate and c.codetype='llindfeeuw'),(select distinct b.IndirectSN from llindirectfee b,ldcode c "+subsqlpart+")"
  +" from ldcode a where a.codetype='llindsurvfee'";
  turnPage1.queryModal(feeSql,AddFeeGrid);
  strSql = "select sum(FeeSum),min(FeeSum) from LLCaseIndFee where feedate='"+fm.MEndDate.value+"'  and Mngcom like '"+fm.comcode.value+"%'";

  arr = easyExecSql(strSql);
  if(arr){
      fm.Sum.value=arr[0][0];
      fm.Average.value=arr[0][1];
    fm.Handler.value=fm.Operator.value;
  }
      fm.Sum.value=fm.Sum.value=='null'?'':fm.Sum.value;
      fm.Average.value=fm.Average.value=='null'?'':fm.Average.value;
}

