/*******************************************************************************
 * Name: RegisterInput.js
 * Function:立案主界面的函数处理程序
 * Author:wujs
 */
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var case_flag ;//判断是否选择了分案号,初始化时是1，若没有选中则是0。
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tSaveFlag = "0";
var turnPage = new turnPageClass();    
var turnPage1 = new turnPageClass();  
var turnPage2 = new turnPageClass();  
var turnPage3 = new turnPageClass();  

var feeArr = new Array();

function showTemPolInfo()
{
  var varSrc ="";
  showInfo = window.open("./FrameMainLSBDXX.jsp?Interface=TemPolInfoInput.jsp"+varSrc,"CasePolicyInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

//费用明细信息
function showFeeInfo()
{
  var varSrc="";
  showInfo = window.open("./FrameMainFAZL.jsp?Interface=ICaseCureCheckInput.jsp"+varSrc,"ICaseCureInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}


function afterQuery( rptNo )
{
	
}

//**********************************//
function EventQuery()
{
	var varSrc ="&AskType=3&ParamNo=" + fm.CaseNo.value+"&CustomerNo="+fm.CustomerNo.value +"&CustomerName="+fm.CustomerName.value;	
    //showInfo = window.open("./FrameMainCaseRela.jsp?Interface=LLSubReportRelaQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
    pathStr="./FrameMainCaseRela.jsp?Interface=LLSubReportRelaQuery.jsp"+varSrc;
    showInfo =OpenWindowNew(pathStr,"RgtSurveyInput","middle",750,450);
}

//事件关联保存后查询



function afterCodeSelect( cName, Filed)
{
  if(cName=='AccidentTypeList')
  {
    displayAccidentInfo();
  }
  if(cName=='DeformityTypeList')
  {
    displayDeformityInfo()
  }
}



function initFM()
{
    initDiseaseGrid();
//    initDeformityGrid();
//    initSeriousDiseaseGrid();
//    initDegreeOperationGrid();
	var selno = ClientGrid.getSelNo()-1;
	if (selno <0)
	{
			// alert("请选择要账单录入的个人受理");
	}
	else
	{
			CustomerNo = ClientGrid.getRowColData(selno, 2);    
			CaseNo = ClientGrid.getRowColData(selno, 7); 
			SubRptNo = ClientGrid.getRowColData(selno, 5); 
	}

	fm.all('AccNum').value = easyExecSql("select count(SubRptNo) from LLCaseRela where CaseNo = '"+CaseNo+"'");
	fm.all('PatientNum').value = easyExecSql(" select count(DiseaseCode) from LLCaseCure where CaseNo = '"+CaseNo+"' ");

	//一般疾病
	var strSQL =" select CaseNo, DiseaseName,DiseaseCode,Diagnose, '',"
						+ " (select realpay from llclaim where CaseNo = '"+CaseNo+"')"
						+ "  from llcasecure  where 1=1 " 
						+ " and SeriousFlag='0' and CustomerNo='"+CustomerNo+"' and CaseNo = '"+CaseNo+"'"
						;
  turnPage1.queryModal( strSQL, DiseaseGrid);

//	//重疾
//	strSQL ="select distinct DiseaseName,DiseaseCode,'',HospitalName,DoctorName,'' from llcasecure  where 1=1" 
//	  +" and SeriousFlag='1' and CustomerNo='"+CustomerNo+"' and CaseNo = '"+CaseNo+"'" ; //根据MainDiseaseFlag判断主要疾病,放在第一位
//	var brr = easyExecSql(strSQL );
//  turnPage2.queryModal( strSQL, SeriousDiseaseGrid);  
//	//意外残疾
//// 	strSQL = "  select Name,Code,ReasonCode,Reason,Remark from llaccident  where 1=1" 
////	  		 + "  and CustomerNo='"+CustomerNo+"'" ; 
////	var crr = easyExecSql(strSQL );
////  displayMultiline( strSQL, AccidentGrid);     
//	//手术	
//    strSQL ="select distinct a.OperationName,a.OperationCode,a.OpFee,a.OpLevel from lloperation a, llcase b  where a.caseno=b.caseno " 
//	  +"  and b.CustomerNo='"+CustomerNo+"' and CaseNo = '"+CaseNo+"'" ; 
//	var drr = easyExecSql(strSQL );
//  turnPage3.queryModal( strSQL, DegreeOperationGrid);  
//		//其它录入要素
//	
////    strSQL ="select a.codename,b.FactorCode,b.FactorName,b.Value,b.remark,b.FactorType from LLOtherFactor b, ldcode a "
////	  +"  and CustomerNo='"+CustomerNo+"'"
////      +" and  a.codetype='llotherfactor' and a.code=b.factortype ";
////	var drr = easyExecSql(strSQL );
////	alert(drr);
////	if ( drr )displayMultiline( drr, OtherFactorGrid);  
//	//残疾
//	strSQL = "select Name,Code,DeformityGrade,DeformityRate,DiagnoseDesc,JudgeDate,JudgeOrganName,JudgeOrgan from llcaseinfo where 1=1 "
//	  +"  and CustomerNo='"+CustomerNo+"'" ; 
//	var err = easyExecSql(strSQL);
//	if (err) displayMultiline(err,DeformityGrid);
}


function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}

function easyQuery()
{
	var comCode = fm.ComCode.value;
	if(fm.RgtDateS.value == "" || fm.RgtDateE.value == "") 
	{ alert("请输入受理起止日期"); return false;}
	//咨询类
	var strSQL ="";
	var anow=getCurrentDate();
		strSQL =" select distinct a.Name,b.Customerno,case when a.sex='0' then '男' else  '女' end,"
					+ " to_char((to_date('"+anow+"')-to_date(a.birthday))/365),b.subrptno,a.idno, c.caseno, b.accdesc,"
					+ " b.AccDate, b.AccPlace, b.InHospitalDate, b.OutHospitalDate "
					+ " from ldperson a, LLSubReport b , llcaserela c, llcase d where 1=1 "
					+ " and b.subrptno = c.subrptno and a.customerno = b.customerno and c.caseno = d.caseno "
					+ getWherePart("a.CustomerNo","CustomerNo")
					+ getWherePart("a.IDNo","IDNo")
					+ getWherePart("c.caseno","CaseNo")
					+ " and d.rgtdate <= '"+fm.all('RgtDateE').value+"'"
  				+ " and d.rgtdate >= '"+fm.all('RgtDateS').value+"'"	
  				+" and d.mngcom like '"+comCode+"%%'" 
					+ " and Name like '%%" + fm.CustomerName.value +"%%' order by AccDate desc, customerno,caseno"
				;//alert(strSQL);
		turnPage.pageLineNum = 5;
		turnPage.queryModal(strSQL, ClientGrid);
}

//原始信息查询页面
function ShowInfoPage()
{
	var selno = ClientGrid.getSelNo();//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = ClientGrid.getRowColData(selno-1,7);
	OpenWindowNew("./LLRegisterCheckInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
}

//保单查询
function ContInfoPage()
{
		var selno = ClientGrid.getSelNo();//alert(CheckGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = ClientGrid.getRowColData(selno-1,7);
		var ContNo = easyExecSql("select distinct grpcontno from llclaimpolicy where caseno = '"+CaseNo+"'");
		if(ContNo=='00000000000000000000')
		{
			ContNo = easyExecSql("select distinct contno from llclaimpolicy where caseno = '"+CaseNo+"'");
			window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
			}else
				{
					window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
	      }

}