//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm(){
	if(fm.CaseNo.value==''){
		alert("前台传入案件号为空!");
		return false;
	}
	var i = 0;
	fm.fmtransact.value="INSERT||MAIN"
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = './LLCaseRemarkSave.jsp';
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
	showInfo.close();
	if (FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	EasyQuery();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm(){
	try{
		initForm();
	}
	catch(re){
		alert("在LLCaseRemarkInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//取消按钮对应操作
function cancelForm(){
	showDiv(operateButton,"true");
	showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick(){
	
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function SurveyFee(){
}

function EasyQuery(){
	var tCaseNo=fm.CaseNo.value;
	var sql ="select * from ("
		+" select (select name from ldcom where comcode=a.inputercomcode),a.inputercode,a.inputername,a.makedate||' '||a.maketime time,a.remark "
		+" from llcaseremark a where a.caseno='"+tCaseNo+"' "
		+" union all select (select name from ldcom where comcode=a.mngcom),a.operator,(select username from lduser where usercode=a.operator),a.makedate||' '||a.maketime time,a.remark "
		+" from llcaseback a where a.caseno='"+tCaseNo+"' "
		+" ) X "
		+" order by X.time with ur";
	turnPage.pageLineNum = 10;
	turnPage.queryModal(sql,LLCaseRemarkGrid);
}

function ShowContent(){
	var selno = PastGrid.getSelNo();
	//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = PastGrid.getRowColData(selno-1,1);
	var SurveyNo = PastGrid.getRowColData(selno-1,11);

	var strSQL="select Content,result,ohresult,ConfNote,SurveyFlag,SurveySite,otherno,SurveyClass,startman,surveysite from LLSurvey where surveyno='"+SurveyNo+"'";
	var arrResult = easyExecSql(strSQL);

	if(arrResult != null){
	    if(arrResult[0][5]==null){
	    arrResult[0][5] = '';
	    }
	    if(arrResult[0][0]==null){
	        arrResult[0][0]='';
	    }
		fm.Content.value=arrResult[0][0];
		if(arrResult[0][1]==null){
		arrResult[0][1]='';
		}
		fm.Result.value=arrResult[0][1];
		if(arrResult[0][2]==null){
		    arrResult[0][2]='';
		}
		fm.resultOut.value=arrResult[0][2];
		if(arrResult[0][3]==null){
		    arrResult[0][3]='';
		}
		fm.ConfNote.value=arrResult[0][3];
		fm.CasePay.value=arrResult[0][5];

		if(arrResult[0][4] =="3"){
			fm.UWFlag.value=0;
			fm.UWFlagName.value="通过";
			}
		else{
			fm.UWFlag.value=1;
			fm.UWFlagName.value="继续调查";
		}
		
		if (arrResult[0][6]==null) {
			arrResult[0][6] = '';
		}
		fm.CaseNo.value = arrResult[0][6];
		
		strCountSQL="select count(*) from llsurvey where otherno='"+fm.CaseNo.value+"'";
		var arrCount=easyExecSql(strCountSQL);
		if(arrCount!= null){
			fm.all('SerialNo').value=arrCount[0][0];
		}
		
		if (arrResult[0][7]==null) {
			arrResult[0][7] = '';
		}
		if (arrResult[0][7]=='0') {
			fm.SurvType.value = '0';
			fm.SurvTypeName.value = '即时调查';
	  } else {
	  	fm.SurvType.value = '1';
			fm.SurvTypeName.value = '一般调查';
	  }
		
		if (arrResult[0][8]==null) {
			arrResult[0][8] = '';
		}
		fm.Surveyer.value = arrResult[0][8];
	}
}

