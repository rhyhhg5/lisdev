<html> 
<%
  //程序名称：LLDescWoundBatchInput.jsp
  //程序功能：#1777 新伤残评定标准及代码系统定义
  //创建日期：
  //创建人  ：Houyd
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<%
%>
<head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LLDescWoundBatchInput.js"></SCRIPT>
  <%@include file="LLDescWoundBatchInit.jsp"%>
  
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
 
  <title>伤残评定标准批量导入</title>   
</head>
<body  onload="initForm();initElementtype();" >
	<!--initDate();-->
  
 <form action="./LLDescWoundBatchSave.jsp" method=post name=fm target="fraSubmit"> 
    <table class=common border=0 width=100%>
     <tr>
      <td class=titleImg align=center>请输入查询信息：</td>
     </tr>
    </table>    
    
    <table class=common align=center>
     <TR  class=common>
      <TD  class=title>
          残疾代码
      </TD>
      <TD  class=input>
       <Input class=common name=Code onkeydown="QueryOnKeyDown()">
      </TD>       
      <TD  class=title>
          残疾名称
      </TD>
      <TD  class=input>
       <Input class=common  name=CodeName  onkeydown="QueryOnKeyDown()">
      </TD>
     </TR>  
     <TR  class=common>
      <TD  class=title>
       <INPUT VALUE="查询" class="cssButton" TYPE="button" onclick="easyQuery()">
      </TD>       
     </TR>  
    </table>
    
    <table class=common border=0 width=100%>
     <tr>
      <td class=titleImg align=center>信息上载：</td>
     </tr>
    </table>  
    
    <iframe name="UploadiFrame" src="LLDescWoundUpLoadInput.jsp" frameborder="0" scrolling="no" height="40px" width="100%"></iframe>
    <table>
     <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAA);">
      </td>
      <td class=titleImg>
          查询信息
      </td>
     </tr>
    </table>
    <Div  id="divLAA" style= "display:''">
    <table  class=common>
     <tr  class=common>
      <td text-align: left colSpan=1>
      <span id="spanEvaluateGrid" >
      </span> 
      </td>
     </tr>
    </table>
    <Div  align=center style= "display: '' ">
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton >                  
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
     </Div>
    </div>
    <br>
    
    <Div  id= "divGrpCaseInfo" style= "display: 'none'">
	  <table>
	    	<tr>
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
	    	</td>
	    	<td class= titleImg>
	    	伤残评定标准信息变更
	    	</td>
	    </tr>
	  </table>
      	<Table class= common>
          <TR  class= common> 
            <TD  class= title>残疾代码</TD>
            <TD  class= input><Input class= "readonly" name=mCode readonly></TD>
            <TD  class= title>残疾名称</TD>
            <TD  class= input><Input class= common name=mCodeName ></TD>
          </TR>
          <TR  class= common>
            <TD  class= title>残疾级别</TD>
            <TD  class= input><input class=common name=mComCode></TD>
            <TD  class= title>给付比例</TD>
            <TD  class= input><Input class= common name=mOtherSign></TD>
          </TR>
         </table>
         <div align=right>        
        	<INPUT VALUE="修  改" name="ModifyButton" class="cssButton" TYPE="button" onclick="submitForm(0)">
        	<INPUT VALUE="删  除" name="DeleteButton" class="cssButton" TYPE="button" onclick="submitForm(1)">
      	 </div>  
        </Div>       
	    
    <input type=hidden id="fmtransact" name="fmtransact"> 
    <input type=hidden id="tCode" name="tCode"> 
    <input type=hidden id="operate" name="operate"> 
    
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
