<%
//程序名称：LLSubReportQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-04 09:43:38
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('SubRptNo').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').value = "";
    fm.all('CustomerType').value = "";
    fm.all('AccSubject').value = "";
    fm.all('AccidentType').value = "";
    fm.all('AccDate').value = "";
    fm.all('AccEndDate').value = "";
    fm.all('AccDesc').value = "";
    fm.all('CustSituation').value = "";
    fm.all('AccPlace').value = "";
    fm.all('HospitalCode').value = "";
    fm.all('HospitalName').value = "";
    fm.all('InHospitalDate').value = "";
    fm.all('OutHospitalDate').value = "";
    fm.all('Remark').value = "";
    fm.all('SeriousGrade').value = "";
    fm.all('SurveyFlag').value = "";
    fm.all('Operator').value = "";
    fm.all('MngCom').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LLSubReportQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLLSubReportGrid();  
  }
  catch(re) {
    alert("LLEventQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LLSubReportGrid;
function initLLSubReportGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
	iArray[1][0]="事件号码";  
	iArray[1][1]="30px";  
	iArray[1][3]=0;  
	
    iArray[2]=new Array();
	iArray[2][0]="客户号码";  
	iArray[2][1]="30px";  
	iArray[2][3]=0;    
	
	iArray[3]=new Array();
	iArray[3][0]="客户姓名";  
	iArray[3][1]="30px";  
	iArray[3][3]=0;   
	
	iArray[4]=new Array();
	iArray[4][0]="客户类型";  
	iArray[4][1]="30px";  
	iArray[4][3]=0;   
	
	iArray[5]=new Array();
	iArray[5][0]="事故类型";  
	iArray[5][1]="30px";  
	iArray[5][3]=0;   
	
	iArray[6]=new Array();
	iArray[6][0]="事故主题";  
	iArray[6][1]="30px";  
	iArray[6][3]=0;   
	
	iArray[7]=new Array();
	iArray[7][0]="事故描述";  
	iArray[7][1]="30px";  
	iArray[7][3]=0;   
    
    LLSubReportGrid = new MulLineEnter( "fm" , "LLSubReportGrid" ); 
    //这些属性必须在loadMulLine前

    LLSubReportGrid.mulLineCount = 3;   
    LLSubReportGrid.displayTitle = 1;
    LLSubReportGrid.hiddenPlus = 1;
    LLSubReportGrid.hiddenSubtraction = 1;
    LLSubReportGrid.canSel = 1;
    LLSubReportGrid.canChk = 0;
    //LLSubReportGrid.selBoxEventFuncName = "showOne";

    LLSubReportGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LLSubReportGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
