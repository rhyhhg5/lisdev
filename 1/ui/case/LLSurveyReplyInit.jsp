<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String tSurveyNo = request.getParameter("tSurveyNo");
String tCaseNo = request.getParameter("CaseNo");
String tLoadC = request.getParameter("LoadC");
String InqNo = request.getParameter("InqNo");
System.out.println(tLoadC);
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    var tCaseNo = "<%=tCaseNo%>";
    fm.OtherNo.value = tCaseNo;
    var tSurveyNo = '<%=tSurveyNo%>';
    var LoadC = "<%=tLoadC%>";
    var tInqNo = "<%=InqNo%>"
    if (LoadC=="2")
    {
    	divreply.style.display='none';
    	fm.sbutton.style.display='none';
    }
    var sql="select remark from llinqapply where otherno='"+tCaseNo+"' with ur";
			var result_1=easyExecSql(sql);
			if(result_1!=null){
			fm.all('resultlala').value=result_1[0][0];
			}
    fm.SurveyNo.value=tSurveyNo;
    var strSQL = "select *  from LLSurvey where SurveyNo='"+tSurveyNo+"'";
    arrResult = new Array();
    arrResult = easyExecSql(strSQL);
    if(arrResult!=null)
    {
				fm.all('SurveyNo').value= arrResult[0][0];
        fm.all('OtherNo').value= arrResult[0][1];
        fm.all('OtherNoType').value= arrResult[0][2];
        fm.all('CustomerNo').value= arrResult[0][5];
        fm.all('CustomerName').value= arrResult[0][6];
        fm.all('SurveyType').value= arrResult[0][8];
        if (arrResult[0][8]=='0')
        	fm.all('SurveyTypeName').value= '即时调查';
        if (arrResult[0][8]=='1')
        	fm.all('SurveyTypeName').value= '一般调查';
        if (arrResult[0][8]=='2')
        	fm.all('SurveyTypeName').value= '再次调查';
        fm.all('Content').value= arrResult[0][12];
        fm.all('result').value= arrResult[0][13];
        fm.all('SurveyStartDate').value= arrResult[0][14];
        fm.all('SurveyFlag').value= arrResult[0][16];
        fm.all('StartMan').value= arrResult[0][18];
        fm.all('AccDesc').value= arrResult[0][29];
        fm.all('OHresult').value = arrResult[0][30]
    }
    strSQL="select count(*) from LLSurvey where OtherNo='"+fm.all('OtherNo').value+"'";
    brr = easyExecSql(strSQL);
    if(brr)
    	fm.all('SerialNo').value=brr[0][0];
    strSQL = "select InqDesc,InqConclusion from llinqapply where InqNo='"+tInqNo+"'";
    crr = easyExecSql(strSQL);
    if(crr){
        fm.all('Content').value= crr[0][0];
//        fm.all('result').value= crr[0][1];
      }
    strSQL = "SELECT A.IDNO,B.OCCUPATIONNAME,A.GRPNAME from ldperson A,ldoccupation B where B.OCCUPATIONCODE=A.OCCUPATIONCODE AND A.customerno='"+fm.all('CustomerNo').value+"'";
    drr = easyExecSql(strSQL);
    if (drr){
    	fm.IDNo.value = drr[0][0];  
    	fm.Occupation.value = drr[0][1]; 
    	fm.Company.value = drr[0][2];  
    }
    strSQL = "SELECT postaladdress from lcaddress where customerno='"+fm.all('CustomerNo').value+"' order by addressno";
    err = easyExecSql(strSQL);
    if (err){
    	fm.CompAddress.value = err[0][0];
    	if (err.length>1)  
    	fm.HomeAddress.value = err[1][0];  
    }
    if (fm.all('OtherNoType').value=='0'){
    	strSQL = "SELECT a.askaddress,a.answermode,c.codename from llmainask a ,llconsult b, ldcode c where c.codetype='llreturnmode' and c.code=a.answermode and b.logno=a.logno and b.consultno='"+fm.all('OtherNo').value+"'";
    frr = easyExecSql(strSQL);
    fm.Appnt.value=fm.all('CustomerName').value;
	    if (frr){
	    	fm.Phone.value = frr[0][0];
	    	fm.AnswerMode.value = frr[0][1];
	    	fm.AnswerModeName.value = frr[0][2];  
	    }
    }
  else{
    strSQL = "SELECT a.rgtantphone,a.rgtantname,a.returnmode,c.codename from llregister a,llcase b,ldcode c where c.codetype='llreturnmode' and c.code=a.returnmode and b.rgtno=a.rgtno and b.caseno='"+fm.all('OtherNo').value+"'";
    grr = easyExecSql(strSQL);
    if (grr){
	    	fm.Phone.value = grr[0][0];
	    	fm.Appnt.value=grr[0][1];
	    	fm.AnswerMode.value = grr[0][2];
	    	fm.AnswerModeName.value = grr[0][3];  
    }
   }
   easyQuery();
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initAffixGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="材料编码";
    iArray[1][1]="20px";
    iArray[1][2]=100;
    iArray[1][3]=2;
	  iArray[1][4]="llsurveyaffix";
	  iArray[1][5]="1|2";  
	  iArray[1][6]="0|1"; 
	  iArray[1][15]="Affixtypecode";
	  iArray[1][16]="02";

    iArray[2]=new Array("材料名称","40px","100","0");
    iArray[3]=new Array("件数","20px","10","1");
    AffixGrid = new MulLineEnter("fm","AffixGrid");
    AffixGrid.mulLineCount =0;
    AffixGrid.displayTitle = 1;
    AffixGrid.locked = 0;
    AffixGrid.canSel =0;
    AffixGrid.canChk =0;
    AffixGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    AffixGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    AffixGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
function initForm()
{
  try
  {
    
		initAffixGrid();
    initInpBox();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 </script>