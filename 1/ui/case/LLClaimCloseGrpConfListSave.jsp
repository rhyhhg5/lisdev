<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLClaimCloseConfSave.jsp
//程序功能：批次给付确认
//创建日期：2006-11-21
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String transact = "";
		String comCode="";
		String rgtNo="";
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");	

        transact = request.getParameter("Operator");
        System.out.println("操作方式为："+transact);
        comCode=tG.ComCode;
        rgtNo=request.getParameter("RgtNo");
        System.out.println("批次号为："+rgtNo);
        String sql="select c.caseno,c.customername, c.customerno, m.realpay, c.casegetmode,c.CustomerName,c.IDNo,c.bankcode,c.bankaccno,c.accname,c.casegetmode,c.rgtno,c.prepaidflag"
                   +" from llcase c, llclaim m,llregister r "
                   +" where  c.caseno=m.caseno and c.rgtstate='09'"
                   +" and c.rgtno=r.rgtno"
                   +" and (r.togetherflag is null or r.togetherflag<>'3')"
                   +" and m.realpay>0"
                   +" and c.mngcom like '"+comCode+"%' "
                   +" and c.rgtno='"+rgtNo+"' with ur";
        
      System.out.println("my love sql:"+sql);
        ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		
		VData tVData = new VData();
		LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
	  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	  LLCaseSet tLLCaseSet = new LLCaseSet();
	  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
	  LJAGetSet tLJAGetSet = new LJAGetSet();
if(tSSRS.getMaxRow()>0){
   	for(int i=1;i<=tSSRS.getMaxRow();i++){  
   		
   		tVData = new VData();
   		tLJAGetInsertBL = new LJAGetInsertBL();
   		tLLCaseSchema = new LLCaseSchema();
  		tLJAGetSchema = new LJAGetSchema();
  		tLLCaseSchema.setCaseNo(tSSRS.GetText(i,1));
  		tLLCaseSchema.setRgtNo(rgtNo);
  		tLLCaseSchema.setPrePaidFlag(tSSRS.GetText(i,13));
  		 String customername=tSSRS.GetText(i,6);
  		 String accName=tSSRS.GetText(i,10);
  		 String caseNo=tSSRS.GetText(i,1);
			tLJAGetSchema.setOtherNo(caseNo);
			tLJAGetSchema.setOtherNoType("5");
			tLJAGetSchema.setAccName(accName);
			tLJAGetSchema.setBankAccNo(tSSRS.GetText(i,9));
			tLJAGetSchema.setPayMode(tSSRS.GetText(i,11));
			 System.out.println("2222222222222:"+tSSRS.GetText(i,11));
			tLJAGetSchema.setBankCode(tSSRS.GetText(i,8));
			if(customername.equals("")||customername==null){
			customername=accName;
			}
			tLJAGetSchema.setDrawer(customername);
			tLJAGetSchema.setDrawerID(tSSRS.GetText(i,7));
		  tVData.add(tLLCaseSchema);
		  tVData.add(tLJAGetSchema);  
		  tVData.add(tG);	
		  if(!tLJAGetInsertBL.submitData(tVData,transact)) {
			  tError=tLJAGetInsertBL.mErrors;
			  Content += "<br>案件"+caseNo+",操作失败 "+ tError.getFirstError();
			  FlagStr = "Fail";
		  } else {
			  Content += "<br>案件"+caseNo+"操作成功" + tLJAGetInsertBL.getBackMsg();
			  System.out.println("LLContAutoDealBL============Begin==========:");
		  	  try{
		  		  LLContAutoDealBL tLLContAutoDealBL = new LLContAutoDealBL();
		  		  tLLContAutoDealBL.submitData(tVData,"");
		  		  System.out.println(tLLContAutoDealBL.getBackMsg());
		  		  Content += tLLContAutoDealBL.getBackMsg();
		  		  System.out.println("Content=="+Content);
		  	  }catch(Exception tex) {
		  		  Content += "合同终止保存失败，原因是:" + tex.toString()+"，请到理赔合同处理中进行操作!";
		  	  }
		  }
		  System.out.println("循环次数"+i);
	}
}else{
            tError=tLJAGetInsertBL.mErrors;
		    Content += "该批次下无给付案件"+ tError.getFirstError();
		  	FlagStr = "Fail";
}
   
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
  tError=tLJAGetInsertBL.mErrors;
    if (!tError.needDealError())
    {                          
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }


//添加各种预处理
%>
<html>
	<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
