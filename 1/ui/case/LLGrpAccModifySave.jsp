<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLGrpAccModifySave.jsp
//程序功能：
//创建日期：2013-11-8 12:43:00
//创建人  ：侯亚东
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
  LJAGetSchema tLJAGetSchema = new LJAGetSchema();

  
  CErrors tError = null;
  LLGrpAccModifyUI tLLGrpAccModifyUI = new LLGrpAccModifyUI();
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  
  //输出参数
  String FlagStr = "";
  String Content = "";
  
  String grpflag = request.getParameter("grpflag");
  System.out.println("==== grpflag == " + grpflag);
  
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);
  
  
//动作为修改。
  if (strOperate.equals("MODIFY")) 
  {
   	tLJAGetSchema.setActuGetNo(request.getParameter("ActuGetNo2"));
  	//tLJAGetSchema.setOtherNo(request.getParameter("OtherNo2"));
   	tLJAGetSchema.setPayMode(request.getParameter("PayMode"));
   	tLJAGetSchema.setSumGetMoney(request.getParameter("GetMoney"));
  	tLJAGetSchema.setDrawer(request.getParameter("Drawer"));
  	tLJAGetSchema.setDrawerID(request.getParameter("DrawerID"));
  	tLJAGetSchema.setBankCode(request.getParameter("BankCode"));
  	tLJAGetSchema.setBankAccNo(request.getParameter("BankAccNo"));
  	tLJAGetSchema.setAccName(request.getParameter("AccName"));
  	tLJAGetSchema.setCanSendBank("0");
  	
  	//团体给付
  	tLLRegisterSchema.setRgtNo(request.getParameter("OtherNo2"));
    tLLRegisterSchema.setCaseGetMode(request.getParameter("PayMode")); 
    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setAccName(request.getParameter("AccName"));
    
  
    try
	   {
	   	
	   	   tVData.add(tLLRegisterSchema);			   
		   tVData.add(tLJAGetSchema);		   
		   tVData.add(tG);		   
		   tLLGrpAccModifyUI.submitData(tVData,strOperate);
	   }
	 catch(Exception ex)
	    {
	      Content = "保存失败，原因是:" + ex.toString();
	      System.out.println("保存失败，原因是:"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tLLGrpAccModifyUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tLLGrpAccModifyUI.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getLastError();
		    	FlagStr = "Fail";
		    }
	     }	     
	    
  }
  		
  %>	

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
   
   
   
 