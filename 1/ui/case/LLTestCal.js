 var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";






function Calc()
{
	  if(fm.all('ManageCom').value=="")
	   {
	   	alert("请输入管理机构！");
	   	return;
	   }  
	  if(fm.all('SecurityType').value=="")
	   {
	   	alert("请输入基金类型！");
	   	return;
	   }    
	  if(fm.all('FeeType').value=="")
	   {
	   	alert("请输入费用类型！");
	   	return;
	   }     
	  if(fm.all('Amuont').value=="")
	   {
	   	alert("请输入帐单金额！");
	   	return;
	   }
	   	   	   
	   if(fm.all('LevelCode').value=="")
	   {
		    var sql ="select distinct levelcode from llsecurityfactor where 1=1"
				    			+ getWherePart('comcode','ManageCom')
				    			+ getWherePart('SecurityType','SecurityType')
				  	  		+ getWherePart('FeeType','FeeType')
				  		  	+ getWherePart('InsuredStat','InsuredStat')
				  			  + getWherePart('FirstInHos','FirstInHos');
		    var arr = easyExecSql(sql );
		    if(!arr)
		    {
		  	  alert("未查找到相关数据");
		  	  return;
		    }
		  else if(arr.length > 1)
		  	{
		  	 alert("查询结果不唯一，请输入医院等级");
		  	 return;	
		  	}
	   }
	   
	   if(fm.all('InsuredStat').value=="")
	   {	
		    var sql ="select distinct InsuredStat from llsecurityfactor where 1=1"
				    			+ getWherePart('comcode','ManageCom')
				    			+ getWherePart('SecurityType','SecurityType')
				  	  		+ getWherePart('FeeType','FeeType')
				  		  	+ getWherePart('LevelCode','LevelCode')
				  			  + getWherePart('FirstInHos','FirstInHos');
				  			  
		    var arr = easyExecSql(sql);
		    if(!arr)
		    {
		  	  alert("未查找到相关数据");
		  	  return;
		    }
		  else if(arr.length > 1)
		  	{
		  	 alert("查询结果不唯一，请输入参保人员性质");
		  	 return;	
		  	}
	   }	   
	   
	   if(fm.all('FirstInHos').value=="")
	   {
		    var sql ="select distinct FirstInHos from llsecurityfactor where 1=1"
				    			+ getWherePart('comcode','ManageCom')
				    			+ getWherePart('SecurityType','SecurityType')
				  	  		+ getWherePart('FeeType','FeeType')
				  		  	+ getWherePart('LevelCode','LevelCode')
				  			  + getWherePart('InsuredStat','InsuredStat');
		    var arr = easyExecSql(sql);
		    if(!arr)
		    {
		  	  alert("未查找到相关数据");
		  	  return;
		    }
		  else if(arr.length > 1)
		  	{
		  	 alert("查询结果不唯一，请输入住院次数");
		  	 return;	
		  	}
	   }
	   initGetModeGrid();
		   var tsql ="select SecurityType,levelcode,InsuredStat,"
		              +      "FeeType,FirstInHos,getlimit,"
		              +      "peakline,downlimit,uplimit,getrate "
		              + "from llsecurityfactor where 1=1"
				    			+ getWherePart('comcode','ManageCom')
				    			+ getWherePart('SecurityType','SecurityType')
				  	  		+ getWherePart('FeeType','FeeType')
				  		  	+ getWherePart('LevelCode','LevelCode')
				  			  + getWherePart('InsuredStat','InsuredStat')
				  			  + getWherePart('FirstInHos','FirstInHos')
				  			  +" order by downlimit asc";
		   var tarr = easyExecSql(tsql);
		   if(!tarr)
	     {
 		     alert("未查找到相关数据！");	
 		     return;
	     }
		   var Amuont = fm.all('Amuont').value;
		   var Secufee = fm.all('Secufee').value;
		   var SFlag = false;
		   var Mflag = false;
		   if(fm.all('FeeType').value=="1")
		   {
		   	Mflag = true;
		   }

//GetModeGrid.addOne("GetModeGrid");

		   if(!Mflag)     //住院
		   {
		   	if(Amuont>tarr[0][6])
		   	{
		   		Amuont = tarr[0][6]
		   	}
		    for (var i=0;i<tarr.length;i++)
		      {
		      	if(SFlag)
		      	 {
		      	 	break;
		      	 }
		      	GetModeGrid.addOne("GetModeGrid");
		      	GetModeGrid.setRowColData(i,1,tarr[i][7]); //起点
		      	GetModeGrid.setRowColData(i,2,tarr[i][8]); //终点
		      	GetModeGrid.setRowColData(i,3,tarr[i][9]); //报销比例
		      	
		      	if(Amuont-(tarr[i][8]-tarr[i][7])>0)   //该段未报销完。
		      	{ 
		      		Amuont = Amuont-(tarr[i][8]-tarr[i][7]);  //剩余：Amuont  该段报销额：tarr[i][8]-tarr[i][7]
		      		GetModeGrid.setRowColData(i,4,(tarr[i][8]-tarr[i][7])*tarr[i][9]+"");
		      		GetModeGrid.setRowColData(i,5,tarr[i][8]-tarr[i][7]-(tarr[i][8]-tarr[i][7])*tarr[i][9]+"");
		      		GetModeGrid.setRowColData(i,6,tarr[i][8]-tarr[i][7]+"");
		      	}
		        else                                  //该段已报销完。
		        { 
		        	//alert(Amuont);                                     
		        	                                         //剩余：0      该段报销额：Amuont
		         	GetModeGrid.setRowColData(i,4,Amuont*tarr[i][9]+"");
		         	GetModeGrid.setRowColData(i,5,Amuont-Amuont*tarr[i][9]+"");
		         	GetModeGrid.setRowColData(i,6,Amuont+"");
		         	SFlag = true;
		        }        
		      	
		      } 
		   }
		  else       //门诊
		   {
	        if(fm.all('Secufee').value=="")
	        {
	        	alert("请输入门诊累计帐单金额！");
	         	return;
	        }
	        if(Secufee-tarr[0][8]>0)   //既往帐单金额已经超过起付线.
	        {
	        	if(Amuont-(-Secufee)-tarr[0][6]>0)
	        	{
	        	   Amuont=tarr[0][6]-Secufee;
	        	}
		        for (var i=1;i<tarr.length;i++)
		          {
		          	if(SFlag)
		          	 {
		          	 	break;
		          	 }
		          	GetModeGrid.addOne("GetModeGrid");
		          	GetModeGrid.setRowColData(i-1,1,tarr[i][7]); //起点
		          	GetModeGrid.setRowColData(i-1,2,tarr[i][8]); //终点
		          	GetModeGrid.setRowColData(i-1,3,tarr[i][9]); //报销比例
		          	
		          	if(Amuont-(tarr[i][8]-tarr[i][7])>0)   //该段未报销完。
		          	{ 
		          		Amuont = Amuont-(tarr[i][8]-tarr[i][7]);  //剩余：Amuont  该段报销额：tarr[i][8]-tarr[i][7]
		          		GetModeGrid.setRowColData(i-1,4,(tarr[i][8]-tarr[i][7])*tarr[i][9]+"");
		          		GetModeGrid.setRowColData(i-1,5,tarr[i][8]-tarr[i][7]-(tarr[i][8]-tarr[i][7])*tarr[i][9]+"");
		          		GetModeGrid.setRowColData(i-1,6,tarr[i][8]-tarr[i][7]+"");
		          	}
		            else                                  //该段已报销完。
		            { 
		            	//alert(Amuont);                                     
		            	                                         //剩余：0      该段报销额：Amuont
		             	GetModeGrid.setRowColData(i-1,4,Amuont*tarr[i][9]+"");
		             	GetModeGrid.setRowColData(i-1,5,Amuont-Amuont*tarr[i][9]+"");
		             	GetModeGrid.setRowColData(i-1,6,Amuont+"");
		             	SFlag = true;
		            }        
		          	
		          }

	        }
	        
	        else                      //既往帐单金额未超过起付线.
	        { 
	        	if(Amuont-(-Secufee)-tarr[0][6]>0)
	        	{
	        	   Amuont=tarr[0][6]-Secufee;
	        	}
	 		      for (var i=0;i<tarr.length;i++)
		        {
		         if(SFlag)
		          {
		         	 	break;
		          }
		          GetModeGrid.addOne("GetModeGrid");
		          GetModeGrid.setRowColData(i,1,tarr[i][7]); //起点
		          GetModeGrid.setRowColData(i,2,tarr[i][8]); //终点
		          GetModeGrid.setRowColData(i,3,tarr[i][9]); //报销比例
		          
		         if(i==0)
		         {
		        	if(Amuont-(-Secufee)-(tarr[i][8]-tarr[i][7])>0)   //该段未报销完（i=0）
		        	{ 
		        		Amuont = Amuont-(-Secufee)-(tarr[i][8]-tarr[i][7]);  //剩余：Amuont  该段报销额：tarr[i][8]-tarr[i][7]-Secufee
		        		
		        	 	GetModeGrid.setRowColData(i,4,(tarr[i][8]-tarr[i][7]-Secufee)*tarr[i][9]+"");
		        	  GetModeGrid.setRowColData(i,5,tarr[i][8]-tarr[i][7]-Secufee-(tarr[i][8]-tarr[i][7]-Secufee)*tarr[i][9]+"");
		          	GetModeGrid.setRowColData(i,6,tarr[i][8]-tarr[i][7]-Secufee+"");
		          }
		          else                                  //该段已报销完。
		          { 
		         	                                     
		         	                                         //剩余：0      该段报销额：Amuont
		          	GetModeGrid.setRowColData(i,4,Amuont*tarr[i][9]+"");
		          	GetModeGrid.setRowColData(i,5,Amuont-Amuont*tarr[i][9]+"");
		          	GetModeGrid.setRowColData(i,6,Amuont+"");
		          	SFlag = true;
		          }        
		         }
		         else
		         { 
		            	if(Amuont-(tarr[i][8]-tarr[i][7])>0)   //该段未报销完。
		            	{ 
		            		
		            		Amuont = Amuont-(tarr[i][8]-tarr[i][7]);  //剩余：Amuont  该段报销额：tarr[i][8]-tarr[i][7]
		            		GetModeGrid.setRowColData(i,4,(tarr[i][8]-tarr[i][7])*tarr[i][9]+"");
		            		GetModeGrid.setRowColData(i,5,tarr[i][8]-tarr[i][7]-(tarr[i][8]-tarr[i][7])*tarr[i][9]+"");
		            		GetModeGrid.setRowColData(i,6,tarr[i][8]-tarr[i][7]+"");
		            	}
		              else                                  //该段已报销完。
		              {                                     
		              	                                          //剩余：0      该段报销额：Amuont
		               	GetModeGrid.setRowColData(i,4,Amuont*tarr[i][9]+"");
		               	GetModeGrid.setRowColData(i,5,Amuont-Amuont*tarr[i][9]+"");
		               	GetModeGrid.setRowColData(i,6,Amuont+"");
		               	SFlag = true;
		              }
		         	
		         } 
		        
		        }       	
	        	
	        	
	        }
	        
		   
		   }
		  
}


function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      
   
  }
}

