//该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function CaseTraceQuery()
{
	if (verifyInput() == false)
    	return false;
    
    var tCaseNo = fm.CaseNo.value;
    if(tCaseNo == null||tCaseNo == '')
    {	
    		alert("理赔案件号不能为空！");
    		fm.CaseNo.focus();
    		return;
	
    }
    
    if(tCaseNo.substring(0,1) == 'P')
    {	
    		alert("只支持个案查询，请重新输入案件号！");
    		fm.CaseNo.focus();
    		return;
	
    }

  var i = 0;

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	fm.target = "f1print";
	fm.action = "LLCaseTraceQuerySave.jsp"
	fm.operate.value = "PRINT";
	fm.submit();
	showInfo.close();
}

//生成Excel后可以下载保存
function afterSubmit(  FlagStr, content,outname,outpathname  )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
   // showDiv(inputButton,"false");
   if(outname==null){
      var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else{
       var varSrc="outname="+outname+"&outpathname="+outpathname;
       window.open("../case/LLBatchPrtInputDown.jsp?"+varSrc);
   } 
  }
}
