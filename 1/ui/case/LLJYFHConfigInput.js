//批量导入
var mDebug="0";
var mOperate="";
var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

function queryAll()
{  
	var tSql = "select code, comcode, othersign from ldcode where codetype = 'lp_jyfh_pass' order by othersign desc";
	    
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		return false;
	}
    else turnPage.queryModal(tSql, EvaluateGrid);
	
}

//单击记录前面的单选框将信息显示在上面对应的位置
function onClick(){
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
		{
			checkFlag = EvaluateGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		var tCode = EvaluateGrid.getRowColData(checkFlag - 1, 1);
		fm.all('SpecialGroupPolicy').value = tCode;	
	}
}

//提交前校验特殊团体保单号是否ldcode表中存在
function beforeSubmit1()
{
	var tSql = "select code, comcode, othersign from ldcode where codetype = 'lp_jyfh_pass' "
		+ getWherePart('code', 'SpecialGroupPolicy')
		+" order by othersign desc ";
	    
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(!strQueryResult)
	{  
		return true;
	}
    else return false;
} 

//提交前校验特殊团体保单号是否在lbgrpcont，lcgrpcont表中存在
function beforeSubmit()
{
	var tSql = "select 1 from SYSIBM.SYSDUMMY1 " +
			"where (exists (select grpcontno from lbgrpcont where grpcontno = '" + fm.all('SpecialGroupPolicy').value + "')" +
			" or exists (select grpcontno from lcgrpcont  where grpcontno = '" + fm.all('SpecialGroupPolicy').value + "'))";
	    
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(strQueryResult)
	{  
		return true;
	}
    else return false;
} 

function queryClick()
{  
	var tSql = "select code, comcode, othersign from ldcode where codetype = 'lp_jyfh_pass' "
		+ getWherePart('code', 'SpecialGroupPolicy')
		+" order by othersign desc ";
	    
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		return false;
	}
    else turnPage.queryModal(tSql, EvaluateGrid);
	
}

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
	  if(beforeSubmit1())
      {
		  fm.all('Transact').value ="INSERT";	
		  var i = 0;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  mAction = "INSERT";
		  fm.submit(); //提交 
      }
	  else
	  {
		  alert("该特殊团体保单号已经存在！");  
		  return false;
	  }
  }
  else
  {
	  alert("该保单号不存在！"); 
	  return false;
  }
}

//***************************************************
//* 点击“删除”进行的操作
//***************************************************
function deleteClick()
{
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
		{
			checkFlag = EvaluateGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		var tCode = EvaluateGrid.getRowColData(checkFlag - 1, 1);
		if(tCode== null || tCode == ""){
			alert("删除时，获取特殊团体保单号失败！");
			return false;
		}
		if (confirm("您确实想删除该记录吗?"))
		{
			var i = 0;
			var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.all('Transact').value ="DELETE";	
			fm.action = "./LLJYFHConfigSave.jsp?Code="+tCode;
			fm.submit(); //提交
			fm.action="";
		}
		else
		{
			alert("您取消了删除操作！");
		}
	}else{
		alert("请选择需要删除的黑名单信息！");
		return false;
	}
}

//Click事件，当点击“修改”时触发该函数
function updateClick()
{
	if(beforeSubmit())
	{
		if(beforeSubmit1())
		{
			var checkFlag = 0;
			for (i=0; i<EvaluateGrid.mulLineCount; i++)
			{
				if (EvaluateGrid.getSelNo(i))
			    {
			      checkFlag = EvaluateGrid.getSelNo();
			      break;
			    }
			}
			if(checkFlag){
				var tCode = EvaluateGrid.getRowColData(checkFlag - 1, 1);
				if(tCode== null || tCode == ""){
					alert("修改时，获取特殊团体保单号失败！");
					return false;
				}
				if (confirm("您确实想修改该记录吗?"))
				{
					var i = 0;
					var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
					showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
					fm.all('Transact').value ="UPDATE";	
					fm.action = "./LLJYFHConfigSave.jsp?Code="+tCode;
					fm.submit(); //提交
					fm.action="";
				}
				else
				{
					alert("您取消了修改操作！");
				}
			}else{
				alert("请选择需要修改的特殊团体保单号信息！");
			  	return false;
			}
			return true;
		}
		else
		{
			alert("该特殊团体保单号已经存在！");  
			  return false;
		}
		
	  }
	  else
	  {
		  alert("该保单号不存在！"); 
		  return false;
	  }
} 


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{  
  showInfo.close();
  if (FlagStr == "Fail" )
  {              
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //初始化
    //initForm();
  }
  else
  {  
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    queryAll();
  
  }
 
}
