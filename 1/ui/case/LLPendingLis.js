//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var organcode="";

//校验录入数据的合法性
function checkData()
{
	if(fm.StartDate.value == null || fm.StartDate.value == "" 
		|| fm.EndDate.value == null||fm.EndDate.value=="")
	{
		alert("统计日期不能为空");
		
		return false;
	}
}

function checktype()
{
	if(fm.operateType.value == "") 
	{
		alert("请选择操作类型")
		return false;
		}
	if(fm.operateType.value != "" && fm.operateType.value != null
     && (fm.organcode.value == null || fm.organcode.value == "")
     && (fm.Groupno.value == null || fm.Groupno.value == "")
     && (fm.usercode.value == null || fm.usercode.value == ""))	
	{
		alert("请指定到机构或小组或组员");
		
		return false;
	}
}

/*********************************************************************
 *  上下级机构判断
 *  参数  ：  pManageCom String : 管理机构
 *  返回值：  true 可查看该机构保单；false 不可查看
 *********************************************************************
 */
function IsValiCom(pManageCom)
{

	if (comCode =='86' || comCode =='86000000' ) return true;
	if ((pManageCom =='86' || pManageCom =='86000000') && (comCode =='86' || comCode =='86000000') ) return true;
	
	if (pManageCom.length >4 && comCode.substring(0,4) == pManageCom.substring(0,4) 
		&& comCode <= pManageCom  ) return true;
	if (pManageCom.length == 4 && comCode.substring(0,4) == pManageCom.substring(0,4))
	{
		var tManage = pManageCom+ "0000" ;
		if (comCode == tManage||comCode == pManageCom) return true;
		
	}
	return false;
	
}


//提交，保存按钮对应操作
function submitForm1()
{
    if(fm.organcode.value==''){
      alert("请选择机构!");
      return false;
    }
    var result = fm.result.value;
    var isWeek = fm.isweek.value;
    if(isWeek!='Y'){
        if(result=='Y'){
            var tStartDate = fm.StartDate.value;
            var tEndDate = fm.EndDate.value;
            if(dateDiff(tStartDate,tEndDate,"M")>3){
//                alert("上班时间，统计期最多为三个月！");
//                return false;
            }
         }
    }     
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Pending";
	fm.action = "LLPendingLisSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function submitForm2()
{
    if(fm.organcode.value==''){
      alert("请选择机构!");
      return false;
    }
 	var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  if(dateDiff(tStartDate,tEndDate,"M")>3){
//    alert("上班时间，统计期最多为三个月！");
//    return false;
  }    
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "PendingState";
	fm.action = "LLPendingStateLisSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function submitForm3()
{
    if(fm.organcode.value==''){
      alert("请选择机构!");
      return false;
    }
 	var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  var result = fm.result.value;
  var isWeek = fm.isweek.value;
  if(isWeek!='Y'){
      if(result=='Y'){
          if(dateDiff(tStartDate,tEndDate,"M")>3){
//              alert("上班时间，统计期最多为三个月！");
//              return false;
          } 
      }
   }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "PendingCom";
	fm.action = "LLPendingReportSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function submitForm4()
{
    if(fm.organcode.value==''){
      alert("请选择机构!");
      return false;
    }
 	var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  if(dateDiff(tStartDate,tEndDate,"M")>3){
//    alert("统计期最多为三个月！");
//    return false;
  } 
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "PendingClaimer";
	fm.action = "LLPendingClaimerSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function lcReload()
{
window.location.reload();
}



			function submitForm()
			{
				//选择了处理
				var cCodeName = fm.DealWith.value;
				
				if( cCodeName == "0")
				{
					submitForm1(); 				   //作业人员待处理案件统计
				}else if(cCodeName == "2"){
					submitForm3();                 //机构待处理案件统计
				}
			}