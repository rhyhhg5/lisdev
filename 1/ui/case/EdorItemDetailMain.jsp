<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：EdorItemDetailMain.jsp
//程序功能：保全项目明细录入主框架
//创建日期：2005-09-27 11:39:42
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>

<%
	String edorAcceptNo = request.getParameter("edorAcceptNo");
	String itemDetailUrl = request.getParameter("itemDetailUrl");
	String grpContNo = request.getParameter("GrpContNo");
	String ContNo = request.getParameter("ContNo");
	String edorType = request.getParameter("EdorType");
	String CaseNo = request.getParameter("CaseNo");
	
	itemDetailUrl = itemDetailUrl + "?EdorNo=" + edorAcceptNo + "&GrpContNo=" + grpContNo
				+ "&CaseNo=" + CaseNo + "&ContNo=" + ContNo + "&EdorType=" + edorType;
	
	//查询扫描件相关类型
	String[] BussNoType = null;
	String[] BussType = null;
	String[] SubType = null;
	
	SSRS tSSRS = new SSRS();
	ExeSQL tExeSQL = new ExeSQL();
	String sql = 	"select distinct BussNoType, BussType, SubType "
					+ "from ES_DOC_RELATION "
					+ "where bussNo='" + edorAcceptNo + "' "
					+ "   and (subType = 'BQ01' or subType = 'BQ02') "
					+ "   and bussNoType = '91' ";
	
	tSSRS = tExeSQL.execSQL(sql);
	if(tSSRS != null)
	{
	  BussNoType = new String[tSSRS.getMaxRow()];
	  BussType = new String[tSSRS.getMaxRow()];
	  SubType = new String[tSSRS.getMaxRow()];
	  
	  for(int i = 1; i <= tSSRS.getMaxRow(); i++)
	  {
  		BussNoType[i -1] = tSSRS.GetText(i, 1);
  		BussType[i -1] = tSSRS.GetText(i, 2);
  		SubType[i -1] = tSSRS.GetText(i, 3);
		}
		session.setAttribute("BussNoType", BussNoType);
		session.setAttribute("BussType", BussType);
		session.setAttribute("SubType", SubType);
	}
%>
<html>
<head>
<title>明细录入 </title>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>   
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<SCRIPT>	
	var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	window.resizeTo(intPageWidth,intPageHeight);
	window.moveTo(-1, -1);
	window.focus();	
	
	var initWidth = 0;
	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数
	
	function queryScanType()
	{
    	var strSql = "select code1, codename, codealias "
    	             + "from ldcode1 "
    	             + "where codetype='scaninput'";
    	var a = "";

    	return a;
  	} 
</SCRIPT>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="no" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="20,0,0,*,0" cols="*">
	    <frame id="fraMenu" name="fraMenu" noresize scrolling="no" src="">
	    <frame id="fraScanList" name="fraScanList" scrolling="auto" src="">
	    <frame id="fraPic" name="fraPic" scrolling="auto" src="">
	    <frame id="fraInterface" name="fraInterface" scrolling="auto" src= "<%=itemDetailUrl%>">
		  <frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
