<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSurveyReplySave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLSurveySchema tLLSurveySchema   = new LLSurveySchema();
  RgtSurveyUI tRgtSurveyUI   = new RgtSurveyUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
  	System.out.println("transact : "+transact);
    tLLSurveySchema.setSurveyNo(request.getParameter("SurveyNo"));
    tLLSurveySchema.setOtherNo(request.getParameter("OtherNo"));
    tLLSurveySchema.setOtherNoType(request.getParameter("OtherNoType"));
    tLLSurveySchema.setSubRptNo(request.getParameter("SubRptNo"));
    tLLSurveySchema.setStartPhase(request.getParameter("StartPhase"));
    tLLSurveySchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLSurveySchema.setCustomerName(request.getParameter("CustomerName"));
    tLLSurveySchema.setSurveyClass(request.getParameter("SurveyClass"));
    tLLSurveySchema.setSurveyType(request.getParameter("SurveyType"));
    tLLSurveySchema.setSurveySite(request.getParameter("SurveySite"));
    tLLSurveySchema.setSurveyRCode(request.getParameter("SurveyRCode"));
    tLLSurveySchema.setSurveyRDesc(request.getParameter("SurveyRDesc"));
    tLLSurveySchema.setContent(request.getParameter("Content"));
    tLLSurveySchema.setresult(request.getParameter("result"));
    tLLSurveySchema.setSurveyStartDate(request.getParameter("SurveyStartDate"));
    tLLSurveySchema.setSurveyEndDate(request.getParameter("SurveyEndDate"));
    tLLSurveySchema.setSurveyFlag(request.getParameter("SurveyFlag"));
    tLLSurveySchema.setSurveyOperator(request.getParameter("SurveyOperator"));
    tLLSurveySchema.setStartMan(request.getParameter("StartMan"));
    tLLSurveySchema.setConfer(request.getParameter("Confer"));
    tLLSurveySchema.setConfNote(request.getParameter("ConfNote"));
     tLLSurveySchema.setConfDate(request.getParameter("ConfDate"));
    tLLSurveySchema.setMngCom(request.getParameter("MngCom"));
    tLLSurveySchema.setOperator(request.getParameter("Operator"));
    tLLSurveySchema.setMakeDate(request.getParameter("MakeDate"));
    tLLSurveySchema.setMakeTime(request.getParameter("MakeTime"));
    tLLSurveySchema.setModifyDate(request.getParameter("ModifyDate"));
    tLLSurveySchema.setModifyTime(request.getParameter("ModifyTime"));
    //System.out.println("confNote:"+request.getParameter("Operator")+"flag"+request.getParameter("MakeDate")+request.getParameter("MakeTime")+request.getParameter("ModifyTime")+"sssss");
   
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLLSurveySchema);
  	tVData.add(tG);
  	System.out.println("transact"+transact);
  	System.out.println("tVData"+tVData);
    tRgtSurveyUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tRgtSurveyUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
