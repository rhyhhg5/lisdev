var ShowInfo;
var mDebug="0";
//打印按钮对应操作
function Print()
{
  var tManageCom  = fm.ManageCom.value; //机构号码
  var tRgtState = fm.RgtState.value;//案件状态
  var tUserCode = fm.UserCode.value;//处理人
  var tUpUserCode   = fm.UpUserCode.value;  //处理人上级
  var tPNo   = fm.PNo.value;  //批次号
  var tCaseNo   = fm.CaseNo.value;  //理赔号
  var tZJNo   = fm.ZJNo.value;  //证件号码
  var tStartDateNo   = fm.StartDateNo.value;  //时效天数起期
  var tEndDateNo    = fm.EndDateNo.value;    //时效天数止期
 
  
//判断是否全都选中
   var sum =tEndDateNo-tStartDateNo;
     if((tStartDateNo&&!tEndDateNo)||(!tStartDateNo&&tEndDateNo)||(sum<0))
   {
     alert("'时效天数起期'和'时效天数止期'必须同时有值，并且止期必须大于等于起期!");
     fm.StartDateNo.value="";
     fm.EndDateNo.value="";
     return false;
   }
   
   var re = /^[0-9]+.?[0-9]*$/;
     if(tStartDateNo&&!re.test(tStartDateNo)){
     alert("'时效天数起期'和'时效天数止期'必须是数字，请正确的填写数字");
      return;
      }
     if(tEndDateNo&&!re.test(tEndDateNo)){
     alert("'时效天数起期'和'时效天数止期'必须是数字，请正确的填写数字");
      return;
      }
  
    
    //判断各项起止日期是否同时存在
   else if(tManageCom==''&&tRgtState==''&&tUserCode==''&&tUpUserCode==''&&tPNo==''&&tCaseNo==''&&tZJNo==''&&tStartDateNo==''&&tEndDateNo=='')
   {
     alert("请录入查询条件！");
     return;
   }
   var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   fm.action = "./LLNOCaseStatisticsSave.jsp";
   fm.fmtransact.value = "PRINT";
   fm.target = "Print";
   fm.submit();
   showInfo.close();    
}


function checkCouple(StartDate,EndDate)
{
  if(StartDate==''&&EndDate=='')
		return 0;
  if((StartDate==''&&EndDate!='')||(StartDate!=''&&EndDate=='')||(StartDate>EndDate))
		return 1;
  if(StartDate!=''&&EndDate!='')
		return 2;
	
   return -1;

}






