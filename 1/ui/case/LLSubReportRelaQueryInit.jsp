<%
//程序名称：LLSubReportQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-12 09:36:37
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
              
<script language="JavaScript">
function initInpBox() { 
  try {     
   fm.AskType.value="<%=request.getParameter("AskType")%>";
   fm.ParamNo.value="<%=request.getParameter("ParamNo")%>";
   fm.CustomerNo.value ="<%=request.getParameter("CustomerNo")%>";
   fm.CustomerName.value ="<%=StrTool.unicodeToGBK(request.getParameter("CustomerName"))%>";
  }
  catch(ex) {
    alert("在LLSubReportQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLLSubReportGrid();  
    easyQueryClick();
  }
  catch(re) {
    alert("LLSubReportQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LLSubReportGrid;
function initLLSubReportGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array("事件号","100px","0","0");
    iArray[2]=new Array("发生时间","100px","0","0");
    iArray[3]=new Array("发生地点","150px","0","0");
    iArray[4]=new Array("事件类型","80px","0","3");
    iArray[5]=new Array("事件主题","200px","0","3");
    iArray[6]=new Array("医院名称","120px","0","0");
    iArray[7]=new Array("事故描述","200px","0","0");
    
    LLSubReportGrid = new MulLineEnter( "fm" , "LLSubReportGrid" ); 
    //这些属性必须在loadMulLine前

    LLSubReportGrid.mulLineCount = 0;   
    LLSubReportGrid.displayTitle = 1;
    LLSubReportGrid.hiddenPlus = 1;
    LLSubReportGrid.hiddenSubtraction = 1;
  //LLSubReportGrid.canSel = 1;
    LLSubReportGrid.canChk = 1;
  LLSubReportGrid.selBoxEventFuncName = "showOne";

    LLSubReportGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LLSubReportGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
