<html> 
<% 
//程序名称：
//程序功能：合同处理-合同终止
//创建日期：2010-10-15 16:49:22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="../bq/PEdor.js"></SCRIPT>
	<SCRIPT src="./PEdorTypeHZ.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="./PEdorTypeHZInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="../bq/PEdorTypeCTSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 合同处理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 合同处理类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
        	<input class = "readonly" readonly name="ContNo">
        </td>   
    	</tr>
    </table> 
		<%@include file="../bq/ULICommon.jsp"%> 
    <Div id= "divPolInfo" style= "display: ''">
			<table>
			  <tr>
		      <td>
		      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
		      </td>
		      <td class= titleImg>
		         保单险种信息
		      </td>
			  </tr>
			</table>
	    <Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
			  		<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
				  	</td>
					</tr>
				</table>					
	    </div>
    </DIV>
    <br>
    <table  class= common>
      <TR class= common>
        <TD class= title> 合同处理原因 </TD>
        <TD class= input>
		      <input class="codeNo" name="reason_tb" readOnly verify="合同处理原因|notnull&code:reason_tb&len<=10" CodeData="0|2^070|其他^076|条款协议约定" ondblclick="return showCodeListEx('reason_tb',[this,reason_tbName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('reason_tb',[this,reason_tbName],[0,1]);"><Input class="codeName" name=reason_tbName readonly elementtype=nacessary>
        </TD>
        <TD  class= title> 合同终止日期 </TD>
        <TD class=input>
          <Input class= "readonly" name=EndDate readonly >
        </TD> 
      </TR>
    </table> 
    <br>
    <div>
	   <Input type=button name="save" class = cssButton value="保  存" onclick="edorTypeCTSave()">
		 <Input  type=Button name="goBack" class = cssButton value="返  回" onclick="edorTypeCTReturn()">
		</div>

		 <input type=hidden id="fmtransact" name="fmtransact">
		 <input type=hidden id="ContType" name="ContType">
		 <input type=hidden name="EdorAcceptNo">
		 <input type=hidden name="Operator">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
