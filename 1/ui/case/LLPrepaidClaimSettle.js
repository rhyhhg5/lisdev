//该文件中包含客户端需要处理的函数和事件

//程序名称：LLPrepaidClaimInput.js
//程序功能：预付赔款录入
//创建日期：2010-11-25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.Apply.disabled=false;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var tSQL="select dealer from llprepaidclaim where prepaidno='"+ fm.all("PrepaidNo").value +"'";
    var arrResult = easyExecSql(tSQL);
    if(arrResult && fm.all('fmtransact').value=="Prepaid||Apply" )
    {
    	alert(content+"\n 已报上级"+arrResult[0][0]+"审批!");  
    }else{
     var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    //执行下一步操作
    if (fm.all('fmtransact').value=="Prepaid||Apply" || fm.all('fmtransact').value=="Prepaid||Cancel") {
        searchPrepaid();
    }
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//根据投保单位名称、团体保单号查询保单
function searchGrpCont()
{
  if (fm.ManageCom.value==null || fm.ManageCom.value=="") {
      alert("请选择管理机构");
      return false;
  }
  
  if ((fm.SGrpName.value==null || fm.SGrpName.value=="")
      && (fm.SGrpContNo.value==null || fm.SGrpContNo.value=="")) {
      alert("请录入投保单位名称或团体保单号");
      return false;
  }
  
  var GrpContSQL = " SELECT b.managecom, b.grpcontno, b.grpname, b.cvalidate, b.cinvalidate, a.applyamount+a.regainamount "
                 + " FROM llprepaidgrpcont a, lcgrpcont b "
                 + " WHERE a.grpcontno=b.grpcontno AND b.managecom LIKE '" + fm.ManageCom.value + "%'"
                 + " AND a.state='1' "
                 + getWherePart("b.grpname","SGrpName")
                 + getWherePart("b.grpcontno","SGrpContNo")
                 + " order by b.grpcontno ";
  turnPage.queryModal(GrpContSQL,GrpContGrid);
}

//选中后保单后操作
function onSelGrpInfo()
{
  var tSelNo=GrpContGrid.getSelNo();
  var GrpContNo = GrpContGrid.getRowColDataByName(tSelNo-1,"grpcontno");
  
  fm.GrpContNo.value = GrpContNo;
  
  if (GrpContNo==null || GrpContNo=="") {
      return false;  
  }
  
  var GrpPolSQL = " SELECT a.riskcode, b.riskname, "
                + " (decimal(NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
                + " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
                + " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0),12,2)), "
                + " a.applyamount+a.regainamount, "
                + " (SELECT NVL(SUM(pay),0) FROM ljagetclaim WHERE othernotype='5' AND grpcontno=a.grpcontno AND grppolno=a.grppolno), "
                + " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
                + " AND m.rgtstate not in ('11','12') AND n.grpcontno=a.grpcontno AND n.grppolno=a.grppolno), "
                + " a.prepaidbala, a.prepaidbala, a.grppolno "
                + " FROM llprepaidgrppol a,lmrisk b "
                + " WHERE a.riskcode=b.riskcode "
                + " AND a.grpcontno='"+GrpContNo
                + "' order by a.riskcode ";
  turnPage1.queryModal(GrpPolSQL,PrepaidClaimGrid);
}

//回收预付赔款申请
function submitForm()
{
  if (!verifyInput2()) {
      return false;
  }
  
  if (fm.GrpContNo.value==null || fm.GrpContNo.value=="") {
      alert("请选择团体保单或者录入预付赔款号回车查询");
      return false;
  }
  
  //查询数据准备
  var tSelNo=GrpContGrid.getSelNo();
  var GrpContNo = GrpContGrid.getRowColDataByName(tSelNo-1,"grpcontno");
  var ClaimCount=PrepaidClaimGrid.mulLineCount; 
  var RiskCode = "";
	for (var i=0; i<ClaimCount; i++){
		  if(PrepaidClaimGrid.getChkNo(i)==true){
	     	RiskCode = PrepaidClaimGrid.getRowColData(i,1);
	     }	    
	} 	
  //险种层次查询回收款项的实际金额
  var sql1 = "select prepaidbala from llprepaidgrppol where grpcontno='"+GrpContNo+"' and riskcode = '"+RiskCode+"' ";
  var sql2 = "select (a.money + b.money) from(" +
  		"select sum(money) money from llprepaidtrace " +
  		"where grpcontno='"+GrpContNo+"' and riskcode = '"+RiskCode+"' and money >= 0.00 and state = '1' ) a," +
  		"(select sum(money) money from llprepaidtrace " +
  		"where grpcontno='"+GrpContNo+"' and riskcode = '"+RiskCode+"' and money < 0.00 and moneytype = 'PK' ) b,"+
  		" (select nvl(sum(money),0) money from llprepaidtrace q,LLPrepaidClaim z"+
		" where q.grpcontno='"+GrpContNo+"' and q.riskcode = '"+RiskCode+"' and q.money < 0.00 and  q.otherno = z.PrepaidNo and z.rgtstate <> '07' and q.moneytype = 'HS'"+
		" ) c";
  //实际的金额tMoney1
  var tMoney1 = 0;
  var tGRPPolMoney1 = easyExecSql(sql1 );
  var tTraceMoney1 = easyExecSql(sql2);
  //险种层次判断以哪个金额为准
 if(tGRPPolMoney1 - tTraceMoney1 >= 0){
 	tMoney1 = tTraceMoney1;
 }else{
 	tMoney1 = tGRPPolMoney1;
 }
 
 //保单层次查询回收款项的实际金额
  var sql3 = "select prepaidbala from llprepaidgrpcont where grpcontno = '"+GrpContNo+"' ";
  var sql4 = "select (a.money + b.money + c.money) from("
			+" select nvl(sum(money),0)  money from llprepaidtrace "
			+" where grpcontno='"+GrpContNo+"' and money >= 0.00 and state = '1' ) a,"
			+" (select nvl(sum(money),0) money from llprepaidtrace "
			+" where grpcontno='"+GrpContNo+"' and money < 0.00 and moneytype = 'PK'"
			+" ) b,"
			+" (select nvl(sum(money),0) money from llprepaidtrace q,LLPrepaidClaim z"
			+" where q.grpcontno='"+GrpContNo+"' and q.money < 0.00 and  q.otherno = z.PrepaidNo and z.rgtstate <> '07' and q.moneytype = 'HS'"
			+" ) c";
  //实际的金额tMoney1
  var tMoney2 = 0;
  var tGRPPolMoney2 = easyExecSql(sql3 );
  var tTraceMoney2 = easyExecSql(sql4 );
  //保单层次判断以哪个金额为准
 if(tGRPPolMoney2 - tTraceMoney2 >= 0){
 	tMoney2 = tTraceMoney2;
 }else{
 	tMoney2 = tGRPPolMoney2;
 }

  var ClaimCount=PrepaidClaimGrid.mulLineCount;
  var chkFlag = false;
  for (var i=0; i<ClaimCount; i++){
    if(PrepaidClaimGrid.getChkNo(i)){
      var realpay = PrepaidClaimGrid.getRowColDataByName(i,"realpay");
      if (realpay > 0) {
          if(!confirm("该保单下有未完成理赔案件，是否进行清账操作")) {
             return false;
          }
      }
      var money = PrepaidClaimGrid.getRowColDataByName(i,"money");
      if (money == null || money == "") {
          alert("实际回收赔款不能为空");
          PrepaidClaimGrid.setFocus(i,8);
          return false;
      }
      
      if (!isNumeric(money)) {
          alert("实际回收赔款必须为数字");
          PrepaidClaimGrid.setFocus(i,8);
          return false;
      }
            
      if (money<=0) {
          alert("实际回收赔款必须大于0");
          PrepaidClaimGrid.setFocus(i,8);
          return false;
      }

      if (Number(tMoney2) < Number(money)) {
          alert("保单下回收金额不能大于预付赔款余额");
          PrepaidClaimGrid.setFocus(i,8);
          return false;
      } 
      
      if (Number(money) > Number(tMoney1)) {
          alert("险种下回收金额不能大于预付赔款余额");
          PrepaidClaimGrid.setFocus(i,8);
          return false;
      } 
      
      chkFlag = true;
    }
  }
  
  if (chkFlag == false){
    alert("请选择回收预付赔款险种");
    return false;
  }
  
  if (fm.PayMode.value!="1" && fm.PayMode.value!="2") {
      if (fm.BankCode.value==null || fm.BankCode.value=="") {
          alert("请选择银行");
          return false;
      }
  }
  
  if (fm.PayMode.value=="4" || fm.PayMode.value=="11") {
      if (fm.AccNo.value==null || fm.AccNo.value=="") {
          alert("请录入银行账号");
          return false;
      }
      
      if (fm.AccName.value==null || fm.AccName.value=="") {
          alert("请录入账户名");
          return false;
      }
  }
  
  fm.Apply.disabled=true;
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.all('fmtransact').value="Prepaid||Apply";
  fm.action = "./LLPrepaidClaimSettleSave.jsp";
  fm.target = "fraSubmit";
  fm.submit();
}

// 查询申请的预付赔款信息
function searchPrepaid()
{
  if (fm.PrepaidNo.value==null || fm.PrepaidNo.value=="") {
      return false;
  }
  
  var tPrepaidSQL = " SELECT a.prepaidno, CODENAME('llprepaidstate',a.rgtstate), "
                  + " a.grpname, a.grpcontno, "
                  + " a.bankcode, "
                  + " (SELECT bankname FROM ldbank where bankcode=a.bankcode), "
                  + " (SELECT CASE WHEN cansendflag='1' THEN '是' ELSE '否' END FROM ldbank where bankcode=a.bankcode), "
                  + " a.bankaccno, a.accname, a.paymode, CODENAME('paymode',a.paymode) "
                  + " FROM llprepaidclaim a "
                  + " WHERE a.rgttype='2' AND a.prepaidno='"+fm.PrepaidNo.value+"'";
  var PrepaidArr = easyExecSql(tPrepaidSQL);
  
  if (PrepaidArr) {
      fm.GrpContNo.value = PrepaidArr[0][3];
      fm.BankCode.value = PrepaidArr[0][4];
      fm.BankName.value = PrepaidArr[0][5];
      fm.SendFlag.value = PrepaidArr[0][6];
      fm.AccNo.value = PrepaidArr[0][7];
      fm.AccName.value = PrepaidArr[0][8];
      fm.PayMode.value = PrepaidArr[0][9];
      fm.PayModeName.value = PrepaidArr[0][10];
  }
  
  var tPrepaidDetail = " SELECT a.riskcode, b.riskname, "
                    + " (decimal(NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
                    + " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
                    + " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0),12,2)), "
                    + " c.applyamount+c.regainamount, "
                    + " (SELECT NVL(SUM(pay),0) FROM ljagetclaim WHERE othernotype='5' AND grpcontno=a.grpcontno AND grppolno=a.grppolno), "
                    + " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
                    + " AND m.rgtstate not in ('11','12') AND n.grpcontno=a.grpcontno AND n.grppolno=a.grppolno), "
                    + " c.prepaidbala, -a.money, a.grppolno "
                    + " FROM llprepaidclaimdetail a, lmrisk b, llprepaidgrppol c, llprepaidclaim d"
                    + " WHERE a.grpcontno=c.grpcontno AND a.grppolno=c.grppolno AND a.riskcode=b.riskcode "
                    + " AND a.prepaidno=d.prepaidno AND d.rgttype='2' "
                    + " AND a.prepaidno='"+fm.PrepaidNo.value+"'"
                    + " order by a.riskcode";
  turnPage1.queryModal(tPrepaidDetail,PrepaidClaimGrid);
  
  var ClaimCount=PrepaidClaimGrid.mulLineCount;
  for (var i=1; i<=ClaimCount; i++){
       PrepaidClaimGrid.checkBoxSel(i);
  }  
}

// 查询预付赔款信息
function searchPrepaidList() {
  if (fm.GrpContNo.value==null || fm.GrpContNo.value=="") {
      alert("请选择团体保单或者录入预付赔款号回车查询");
      return false;
  }
  
  var tPrepaidSQL = " SELECT a.prepaidno, CODENAME('pprgttype',a.rgttype), a.summoney, "
                  + " a.rgtdate, a.register, CODENAME('llprepaidstate',a.rgtstate), a.noticedate "
                  + " FROM llprepaidclaim a "
                  + " WHERE a.rgtstate in ('05','06') "
                  + " AND a.grpcontno='"+fm.GrpContNo.value+"'";
  turnPage2.queryModal(tPrepaidSQL,PrepaidClaimDetailGrid);
}

// 预付赔款撤销
function cancel() 
{
  if (fm.PrepaidNo.value==null || fm.PrepaidNo.value=="") {
      alert("请输入预付赔款号");  
      return false;
  }
  
  fm.Cancel.disabled=true;
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.all('fmtransact').value="Prepaid||Cancel";
  fm.action = "./LLPrepaidClaimInputSave.jsp";
  fm.target = "fraSubmit";
  fm.submit();
}
function printBackGetNotice()
{
	if(GrpContGrid.getSelNo() ==0 )
	{
		alert("请选择一条保单信息！");
	    return false;
	}
	var selNo = GrpContGrid.getSelNo();
	var tGrpContNo = GrpContGrid.getRowColData(selNo-1, 2);

	var ClaimCount=PrepaidClaimGrid.mulLineCount;
	var tSumPay =0;
	var chkFlag=false;
	for (var i=0; i<ClaimCount; i++){
		  if(PrepaidClaimGrid.getChkNo(i)==true){
		    chkFlag = true;
	     	tSumPay = parseFloat(tSumPay)+ parseFloat(PrepaidClaimGrid.getRowColData(i,8));
	     }	    
	} 	
	if(chkFlag == false)
	{
		alert("请至少选择一条险种信息！");
	    return false;
	}
	var showStr="正在准备打印数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action = "./LLprepaidClaimPrintSave.jsp?Code=lp007&OtherNo="+tGrpContNo+"&OtherNoType='Y'&StandbyFlag3=0&StandbyFlag4="+tSumPay+"";
	fm.submit();
}
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}