var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var tSaveType="";
//提交，保存按钮对应操作
function submitForm()
{
  
  if ( fm.LogNo.value!="")
      {
      	alert("你不能执行改操作");
      	return false;
      	}
    if (confirm("您确实想保存该记录吗?"))
    {
      tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="INSERT||MAIN";
        fm.submit(); //提交
        tSaveFlag ="0";

      
    }
    else
    {
      alert("您取消了修改操作！");
    }

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
		fm.QDRgtGiveEnsure1.disabled=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 
    if (fm.fmtransact.value=="DELETE||MAIN")
    {
    	fm.reset();
    	initCustomerGrid();
    }
    SearchGrpRegister();
    divGrpCaseInfo.style.display = 'none';
 //   showDiv(operateButton,"true");
 //   showDiv(inputButton,"false");
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


function UWClaim()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}
		
		//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		var varSrc="";
		var newWindow = window.open("./FrameMain.jsp?Interface=PayAffirmInput.jsp"+varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SearchGrpRegister()
{
	  // 书写SQL语句
  var strSql = " SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE, R.APPPEOPLES, b.codename,R.rgtstate,'5000' " 
           +" FROM LLREGISTER R , ldcode b " 
           +" WHERE R.RGTOBJ = '0' AND R.RGTCLASS = '1' AND R.declineflag is null " 
           +" and b.codetype='llgrprgtstate' and b.code = r.rgtstate "
           + getWherePart("r.MngCom","ManageCom","like")
           + getWherePart("R.ApplyerType","ApplyerType")
           + getWherePart("R.CUSTOMERNO","srCustomerNo")
           + getWherePart("R.rgtobjno","GrpContNo")
           + getWherePart("R.GRPNAME","srGrpName")
    sqlpart = " ORDER BY r.rgtno ";
  strSql+=sqlpart;
  turnPage.queryModal(strSql,GrpRegisterGrid);
}

function RgtGiveEnsure()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}
	
	fm.all('RgtNo').value = GrpRegisterGrid.getRowColData(selno-1, 1);
	var trgtstate = GrpRegisterGrid.getRowColData(selno-1, 9);
	if(trgtstate=='04'||trgtstate=='05'){
	  alert("该团体案件已做完给付确认！");
	  return false;
	}
	if(trgtstate=='01'){
	  alert("当前状态下不能做该团体案件的给付确认！");
	  return false;
	}
  	fm.all('operate').value	= "GIVE|ENSURE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
  
}

//给付凭证打印
function GPrint()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印给付凭证的团体案件");
	      return ;
	}
	
  fm.target="f1print";
  fm.fmtransact.value="PRINT";
  	
	rgtNo = GrpRegisterGrid.getRowColData( selno-1, 1);
	
		strSQL="select ActuGetNo from LJAGet where OtherNo='"+rgtNo+"'" +"and OtherNoType='5'";
		arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
		        ActuGetNo = arrResult[0][0];

    		}	
    	if(ActuGetNo == null || ActuGetNo == "")
    	{
    		alert("实付号码为空，团体案件尚未给付确认！");
    		return;
    	}
	var newWindow = window.open("./LLClaimMainListPrintSave.jsp?ActuGetNo=" + ActuGetNo + "&RgtClass=1"  + "&fmtransact=PRINT");		
}
// 打印明细表
function GrpColPrt()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印给付明细的团体案件");
	      return ;
	}
	
	RgtNo = GrpRegisterGrid.getRowColData( selno-1, 1);
	strSQL="select rgtstate,togetherflag from llregister where rgtno='"+RgtNo+"'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
	      rgtstate = arrResult[0][0];
	      togetherflag = arrResult[0][1];

  }	
  	if((rgtstate == "01" || rgtstate == "02")&&togetherflag=="3")
  	{
  		alert("此状态下不能打印！");
  		return;
  	}
  fm.target="f1print";
  fm.fmtransact.value="PRINT";
  	
	
	var newWindow = window.open("PayColPrt.jsp?RgtNo=" + RgtNo+ "&fmtransact=PRINT");		
}

function GRPrint()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印赔付报告的团体保单");
	      return ;
	}
	
	GrpNo = GrpRegisterGrid.getRowColData( selno-1, 2);
	GrpContNo = GrpRegisterGrid.getRowColData( selno-1, 4);
  fm.target="GRPrt1";
  fm.fmtransact.value="PRINT";
  	
  alert("正式环境暂时取消此处打印功能，请登录查询机，在'理赔案件-->理赔统计-->团体保单赔付报告'下继续操作");
  return false;
  //var newWindow = window.open("GrpReportPrt.jsp?GrpNo=" + GrpNo+ "&GrpContNo="+GrpContNo+"&fmtransact=PRINT");		
}
function GRDPrint()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印赔付报告的团体保单");
	      return ;
	}
	
	GrpNo = GrpRegisterGrid.getRowColData( selno-1, 2);
	GrpContNo = GrpRegisterGrid.getRowColData( selno-1, 4);
  fm.target="GRPrt2";
  fm.fmtransact.value="PRINT";
  alert("正式环境暂时取消此处打印功能，请登录查询机，在'理赔案件-->理赔统计-->团体保单赔付报告'下继续操作");
  return false;
  //var newWindow = window.open("GrpReportPrtD.jsp?GrpNo=" + GrpNo+ "&GrpContNo="+GrpContNo+"&fmtransact=PRINT");		
}

function QueryOnKeyDown()
{
	var keycode = event.keyCode;

	if(keycode=="13")
	{
		SearchGrpRegister();		
	}
}

function DealCancel()
{
	var rgtno = '';
	var selno = GrpRegisterGrid.getSelNo()-1;
	if (selno >=0)
	{
		rgtno = GrpRegisterGrid.getRowColData(selno,1);
		GrpName = GrpRegisterGrid.getRowColData(selno,3);
		GrpNo = GrpRegisterGrid.getRowColData(selno,2);
	}
	var varSrc="&CaseNo=" + rgtno +"&CustomerNo="+GrpNo+"&CustomerName="+GrpName+"&LoadFlag=2";
showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=750,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

	function afterCodeSelect( cCodeName, Field )
	{
		if( cCodeName == "grprint" && Field.value=="1")
		{
			GRPrint();
		}
		if( cCodeName == "grprint" && Field.value=="2")
		{
			GRDPrint();
		}
	}

function QDRgtGiveEnsure()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}
	
	fm.all('RgtNo').value = GrpRegisterGrid.getRowColData(selno-1, 1);
	var trgtstate = GrpRegisterGrid.getRowColData(selno-1, 9);
	if(trgtstate=='04'||trgtstate=='05'){
	  alert("该团体案件已做完给付确认！");
	  return false;
	}
	if(trgtstate=='01'){
	  alert("当前状态下不能做该团体案件的给付确认！");
	  return false;
	}
  	fm.all('operate').value	= "QDGIVE|ENSURE";
  	fm.QDRgtGiveEnsure1.disabled=true;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
  
}
