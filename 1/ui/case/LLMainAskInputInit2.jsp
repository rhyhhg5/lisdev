<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
		String CaseNo = "";
		if(request.getParameter("CaseNo")!=null)
		{
				CaseNo = request.getParameter("CaseNo");
		}
		String LoadC = "";
		if(request.getParameter("LoadC")!=null)
		{
			LoadC = request.getParameter("LoadC");
		}
		    
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
		fm.ConsultNo.value="<%=CaseNo%>";
    initInpBox();
    initCommHospitalGrid();
    initTitle();   
    initSubReportGrid();
    fm.LoadC.value="<%=LoadC%>";
    if (fm.LoadC.value=='2')
    {
 //    alert(fm.LoadC.value);
    divconfirm.style.display='none';}
    initMain(); 
    	  <%GlobalInput mG = new GlobalInput();
  	mG=(GlobalInput)session.getValue("GI");
  	%>
     fm.Handler.value = "<%=mG.Operator%>";
     fm.ModifyDate.value = getCurrentDate();
    
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initSubReportGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="事件号";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="发生日期";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="发生地点";
    iArray[3][1]="150px";
    iArray[3][2]=60;
    iArray[3][3]=0;

   iArray[4] = new Array("入院日期","80px","20","0");
   iArray[5] = new Array("出院日期","80px","20","0");
	 iArray[6] = new Array("事件信息","140px","20","0");
   
    SubReportGrid = new MulLineEnter("fm","SubReportGrid");
    SubReportGrid.mulLineCount = 0;
    SubReportGrid.displayTitle = 1;
    SubReportGrid.locked = 0;
    SubReportGrid.canChk =1	;
    SubReportGrid.canSel =0	;

    SubReportGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.selBoxEventFuncName = "ShowRela";
    SubReportGrid.loadMulLine(iArray);
    

  }
  catch(ex)
  {
    alter(ex);
  }
}

function initCommHospitalGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
    
    iArray[1]=new Array();
    iArray[1][0]="医院代码";
    iArray[1][1]="80px";
    iArray[1][2]=80;
    iArray[1][3]=2;
    iArray[1][4]="llhospiquery";
    iArray[1][5]="1|2|3|4";     
    iArray[1][6]="0|1|2|3";     
    iArray[1][15]="Hospitname";
    iArray[1][17]="2";
    iArray[1][19]=1;

    iArray[2]=new Array();
    iArray[2][0]="医院名称";
    iArray[2][1]="150px";
    iArray[2][2]=100;
    iArray[2][3]=1;

    iArray[3]=new Array();
    iArray[3][0]="医院属性";
    iArray[3][1]="100px";
    iArray[3][2]=60;
    iArray[3][3]=0;
   
    iArray[4]=new Array();
    iArray[4][0]="医院属性代码";
    iArray[4][1]="100px";
    iArray[4][2]=60;
    iArray[4][3]=3;
    
    iArray[5]=new Array("科室","105px","100",1);
    iArray[6]=new Array("床位","105px","100",1);

    CommHospitalGrid = new MulLineEnter("fm","CommHospitalGrid");
    CommHospitalGrid.mulLineCount = 0;
    CommHospitalGrid.displayTitle = 1;
    CommHospitalGrid.loadMulLine(iArray);
    

  }
  catch(ex)
  {
    alter("ex");
  }
}

 
 
 </script>