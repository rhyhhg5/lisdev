//批量导入
var mDebug="0";
var mOperate="";
var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

var str="";
function getstr()
{
  str="1 and comcode like #"+fm.MngCom.value+"%#";
}

function onClick(){
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
		{
			checkFlag = EvaluateGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		fm.all('TreatmentCode').value = EvaluateGrid.getRowColData(checkFlag - 1, 1);	
		fm.all('TreatmentName').value = EvaluateGrid.getRowColData(checkFlag - 1, 2);
		fm.all('TreatmentDetail').value = EvaluateGrid.getRowColData(checkFlag - 1, 3);
		fm.all('TreatmentExplain').value = EvaluateGrid.getRowColData(checkFlag - 1, 4);
		fm.all('TreatmentCategory').value = EvaluateGrid.getRowColData(checkFlag - 1, 5);
		fm.all('Phonetic').value = EvaluateGrid.getRowColData(checkFlag - 1, 6);
		fm.all('PackagingCode').value = EvaluateGrid.getRowColData(checkFlag - 1, 7);
		fm.all('ComDescription').value = EvaluateGrid.getRowColData(checkFlag - 1, 8);
		fm.all('ChargeUnit').value = EvaluateGrid.getRowColData(checkFlag - 1, 9);
		fm.all('UnitPrice').value = EvaluateGrid.getRowColData(checkFlag - 1, 10);
		fm.all('Remark').value = EvaluateGrid.getRowColData(checkFlag - 1, 11);
		fm.all('MngCom_ch').value = EvaluateGrid.getRowColData(checkFlag - 1, 12);
		fm.all('AreaName').value = EvaluateGrid.getRowColData(checkFlag - 1, 13);
		fm.all('MngCom').value = EvaluateGrid.getRowColData(checkFlag - 1, 14);
		fm.all('AreaCode').value = EvaluateGrid.getRowColData(checkFlag - 1, 15);
		fm.all('DisTreatmentCategory').value = EvaluateGrid.getRowColData(checkFlag - 1, 17);
	}
}

//校验单价是否为数字并且保留两位小数
function checkNum(obj) {  
    //检查是否是非数字值  
    if (isNaN(obj.value)) {  
        obj.value = "";  
        alert("该项只能输入数字！");  
    }
    else
    {
    	if (obj != null) {  
            //检查小数点后是否为两位
            if (obj.value.toString().split(".").length > 1 && obj.value.toString().split(".")[1].length > 2) {  
                alert("小数点后只能保留两位！");  
                obj.value = "";  
            }  
        }  
        if (obj != null) {  
            //检查小数点后是否为两位
            if (obj.value.toString().length > 16) {  
                alert("最多只能输入16位数字！");  
            }  
        }  
    }
}  

//增删改查后页面自动刷新
function queryAll()
{  
	var tSql = "select TreatmentCode,TreatmentName,TreatmentDetail,TreatmentExplain,TreatmentCategory,Phonetic,PackagingCode,ComDescription,ChargeUnit,UnitPrice,Remark," +
			"(select name from ldcom where comcode = ll.MngCom) MngCom_ch," +
			"(select codename from LDCode where codetype = 'hmareacode' and code = ll.AreaCode) AreaName," +
			"MngCom,AreaCode,SerialNo,(select codename from ldcode where codetype = 'treatment' and code = ll.TreatmentCategory) DisTreatmentCategory  from LLCaseTreatmentInfo ll where 1 = 1 "
			+ getWherePart('TreatmentCode', 'TreatmentCode') 
			+ getWherePart('TreatmentName', 'TreatmentName') 
			" order by SerialNo";
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	turnPage.queryModal(tSql, EvaluateGrid);
}

//点击”查询“按钮时的操作
function queryClick()
{  
	if("" == (fm.all('TreatmentCode').value) && "" == (fm.all('TreatmentName').value))
	{
		alert("请录入‘诊疗项目编码’或‘项目名称’进行查询");
		return false;
	}
	else
	{
		var tSql = "select TreatmentCode,TreatmentName,TreatmentDetail,TreatmentExplain,TreatmentCategory,Phonetic,PackagingCode,ComDescription,ChargeUnit,UnitPrice,Remark," +
				"(select name from ldcom where comcode = ll.MngCom) MngCom_ch," +
				"(select codename from LDCode where codetype = 'hmareacode' and code = ll.AreaCode) AreaName," +
				"MngCom,AreaCode,SerialNo,(select codename from ldcode where codetype = 'treatment' and code = ll.TreatmentCategory) DisTreatmentCategory  from LLCaseTreatmentInfo ll where 1 = 1 "
				+ getWherePart('TreatmentCode', 'TreatmentCode') 
				+ getWherePart('TreatmentName', 'TreatmentName') 
				" order by SerialNo";
		//执行查询并返回结果
		var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
		if(!strQueryResult)
		{  alert("没有符合条件的信息！"); 
			return false;
		}
	    else turnPage.queryModal(tSql, EvaluateGrid);
	}
}

//保存
function submitForm()
{
	if (verifyInput() == false)
	    return false;
	fm.all('Transact').value ="INSERT";	
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	mAction = "INSERT";
	fm.submit(); //提交 
}

//***************************************************
//* 点击“删除”进行的操作
//***************************************************
function deleteClick()
{
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
		{
			checkFlag = EvaluateGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		var tSerialNo = EvaluateGrid.getRowColData(checkFlag - 1, 16);
		if(tSerialNo== null || tSerialNo == ""){
			alert("删除时，获取诊疗项目信息失败！");
			return false;
		}
		if (confirm("您确实想删除该记录吗?"))
		{
			var i = 0;
			var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.all('Transact').value ="DELETE";	
			fm.action = "./LLTreatmentInfoSave.jsp?SerialNo="+tSerialNo;
			fm.submit(); //提交
		}
		else
		{
			alert("您取消了删除操作！");
		}
	}else{
		alert("请选择需要删除的诊疗项目信息！");
		return false;
	}
}

//Click事件，当点击“修改”时触发该函数
function updateClick()
{
	if (verifyInput() == false)
	    return false;
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
	    {
	      checkFlag = EvaluateGrid.getSelNo();
	      break;
	    }
	}
	if(checkFlag){
		var tSerialNo = EvaluateGrid.getRowColData(checkFlag - 1, 16);
		if(tSerialNo== null || tSerialNo == ""){
			alert("修改时，获取诊疗项目信息失败！");
			return false;
		}
		if (confirm("您确实想修改该记录吗?"))
		{
			var i = 0;
			var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.all('Transact').value ="UPDATE";	
			fm.action = "./LLTreatmentInfoSave.jsp?SerialNo="+tSerialNo;
			fm.submit(); //提交
		}
		else
		{
			alert("您取消了修改操作！");
		}
	}else{
		alert("请选择需要修改的诊疗项目信息！");
	  	return false;
	}
	return true;
}

//诊疗项目信息导入
function TreatmentInfoUpload()
{
  getImportPath();
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmlode.action = "./LLTreatmentInfoImport.jsp?ImportPath="+ImportPath;
  fmlode.submit(); //提交
  
}

function getImportPath () {
	  var strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) {
	    alert("未找到上传路径");
	    return;
	  }
	  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	  //查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	  ImportPath = turnPage.arrDataCacheSet[0][0];
}

//模板下载
function downLoad() {
	var formAction = fm.action;
    fm.action = "LLTreatmentInfoTemplateDownLoad.jsp";
    fm.submit();
    fm.target = "fraSubmit";
    fm.action = formAction;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    queryAll();
  }
}

