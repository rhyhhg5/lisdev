	//Name：LLSimpleClaimConf.js
	//Function：批量审批
	//Date：2013-07-02
	//Author：ZJL
	
var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var tSaveType="";

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}
 
 function getRights()
{   
    var strSQL="select claimpopedom from llclaimuser where usercode='"+fm.Operator.value+"'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult != null)
    {
    	var Rights = arrResult[0][0];
    	//alert(Rights);
    
    }
    Str="1 and claimpopedom < #"+Rights+"#";
}   

function easyQuery()
{
  var startDate = fm.RgtDateS.value;
  var endDate = fm.RgtDateE.value;
  var Printf = fm.Printf.value;
  var OrganCode = fm.OrganCode.value;              
  var Operator = fm.Operator.value;              
  if (startDate==""||startDate==null||endDate==""||endDate==null){
  	alert("请输入受理日期");
  	return false;
  }
  if (Printf==""||Printf==null){
  	alert("请输入处理类型");
  	return false;
  }
    if (OrganCode==""||OrganCode==null){
  	alert("请输入管理机构");
  	return false;
  }
    if (Operator==""||Operator==null){
  	alert("请输入处理人");
  	return false;
  }
  
  if (Printf == "0")
  {
    var rgtstate = "04";
    strSQL1 ="and rgtstate = '04' order by a.rgtdate,a.caseno";
  }
  
  if (Printf == "1")
  {
	var rgtstate = "05";
    strSQL1 ="and rgtstate = '05' order by a.rgtdate,a.caseno";
  }
  
  if (Printf == "2")
  {
	var rgtstate = "10";
    strSQL1 ="and rgtstate = '10' order by a.rgtdate,a.caseno";
  }
	var strSQL0 = "select a.rgtno,a.caseno,a.CustomerName,a.CustomerNo,a.rgtdate,"
       + "a.Dealer,b.codename,a.rgtstate"
       +" from llcase a, ldcode b where b.codetype='llrgtstate' and b.code=a.rgtstate "
       + getWherePart("a.rgtdate","RgtDateS",">=")
       + getWherePart("a.rgtdate","RgtDateE","<=")
       + getWherePart("a.Dealer","Operator")
       + getWherePart("a.MngCom","OrganCode","like")
       ;
       strSQL = strSQL0+strSQL1;
		  if ( strSQL !="" )
		  {
		      turnPage.queryModal(strSQL, CheckGrid);
		  }

}

function submitForm()
{
	var rowsNo = CheckGrid.mulLineCount;
   if ( rowsNo>0){
   	var checkRc = false;
   	var kk=0;
   	for(kk=0;kk<rowsNo;kk++){
   	 if(CheckGrid.getChkNo(kk)){
   	   checkRc=true;
   	 }
	}
	  	
    if(!checkRc){
      alert("请选择一个保单！");
      return false;
    }
  } else {
     alert("没有保单数据");
     return false;
  }
  
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action="./LLSimpleClaimConfSave.jsp";
    fm.submit(); //提交
}

function getstr()
{
  str="1 and comcode like #"+fm.OrganCode.value+"%#";
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		window.location.reload();
	/*	top.close();
		top.opener.location.reload();
		parent.opener.top.focus(); */
	}
}
    
    
    
    
    
    
    
    
    
    
    