<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：LLDiagnosisInfoSave.jsp
	//程序功能：诊断数据信息（含门诊特殊病）信息
	//创建日期：2015-02-09
	//创建人  ：Liyunxia
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
//接收信息，并作校验处理。
//输入参数

LLCaseDiagnosticInfoSchema tLLCaseDiagnosticInfoSchema = new LLCaseDiagnosticInfoSchema();
LLCaseDiagnosticInfoUI tLLCaseDiagnosticInfoUI = new LLCaseDiagnosticInfoUI();
//输出参数
String FlagStr = "";
String Content = "";
GlobalInput tGI = new GlobalInput(); //repair:
tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
//后面要执行的动作：添加，修改，删除
String transact=request.getParameter("Transact");

if(tGI==null)
{
  System.out.println("页面失效,请重新登陆");   
  FlagStr = "Fail";        
  Content = "页面失效,请重新登陆";  
}
else
{
  String Operator  = tGI.Operator ;  //保存登陆管理员账号
  String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  CErrors tError = null;
  String tBmCert = "";
  
  System.out.println("transact:"+transact);
  if("INSERT".equals(transact))
  {
	  tLLCaseDiagnosticInfoSchema.setSerialNo(PubFun1.CreateMaxNo("DIAGNOSTIC", 20));
	  tLLCaseDiagnosticInfoSchema.setDiseaseCode(request.getParameter("DiseaseCode"));
	  tLLCaseDiagnosticInfoSchema.setICD10(request.getParameter("ICD10"));
	  tLLCaseDiagnosticInfoSchema.setICD9(request.getParameter("ICD9"));
	  tLLCaseDiagnosticInfoSchema.setDiseaseName(request.getParameter("DiseaseName"));
	  tLLCaseDiagnosticInfoSchema.setEnglishName(request.getParameter("EnglishName"));
	  tLLCaseDiagnosticInfoSchema.setDiseaseClassification(request.getParameter("DiseaseClassification"));
	  tLLCaseDiagnosticInfoSchema.setSubCategory(request.getParameter("SubCategory"));
	  tLLCaseDiagnosticInfoSchema.setPhonetic(request.getParameter("Phonetic"));
	  tLLCaseDiagnosticInfoSchema.setRemark(request.getParameter("Remark"));
	  tLLCaseDiagnosticInfoSchema.setMngCom(request.getParameter("MngCom"));
	  tLLCaseDiagnosticInfoSchema.setAreaCode(request.getParameter("AreaCode"));
	  tLLCaseDiagnosticInfoSchema.setOperator(Operator);
	  tLLCaseDiagnosticInfoSchema.setMakeDate(PubFun.getCurrentDate());
	  tLLCaseDiagnosticInfoSchema.setMakeTime(PubFun.getCurrentTime());
	  tLLCaseDiagnosticInfoSchema.setModifyDate(PubFun.getCurrentDate());
	  tLLCaseDiagnosticInfoSchema.setModifyTime(PubFun.getCurrentTime());
  }
  else if("DELETE".equals(transact))
  {
  	  String tSerialNo =request.getParameter("SerialNo");
  	  tLLCaseDiagnosticInfoSchema.setSerialNo(tSerialNo);
  }
  else if("UPDATE".equals(transact))
  {
	  String tSerialNo =request.getParameter("SerialNo");
  	
	  LLCaseDiagnosticInfoDB tLLCaseDiagnosticInfoDB = new LLCaseDiagnosticInfoDB();
	  tLLCaseDiagnosticInfoDB.setSerialNo(tSerialNo);
      tLLCaseDiagnosticInfoDB.getInfo();
      LLCaseDiagnosticInfoSchema cLLCaseDiagnosticInfoSchema = tLLCaseDiagnosticInfoDB.getSchema();
	  
  	  tLLCaseDiagnosticInfoSchema.setSerialNo(tSerialNo);
  	  tLLCaseDiagnosticInfoSchema.setDiseaseCode(request.getParameter("DiseaseCode"));
	  tLLCaseDiagnosticInfoSchema.setICD10(request.getParameter("ICD10"));
	  tLLCaseDiagnosticInfoSchema.setICD9(request.getParameter("ICD9"));
	  tLLCaseDiagnosticInfoSchema.setDiseaseName(request.getParameter("DiseaseName"));
	  tLLCaseDiagnosticInfoSchema.setEnglishName(request.getParameter("EnglishName"));
	  tLLCaseDiagnosticInfoSchema.setDiseaseClassification(request.getParameter("DiseaseClassification"));
	  tLLCaseDiagnosticInfoSchema.setSubCategory(request.getParameter("SubCategory"));
	  tLLCaseDiagnosticInfoSchema.setPhonetic(request.getParameter("Phonetic"));
	  tLLCaseDiagnosticInfoSchema.setRemark(request.getParameter("Remark"));
	  tLLCaseDiagnosticInfoSchema.setMngCom(request.getParameter("MngCom"));
	  tLLCaseDiagnosticInfoSchema.setAreaCode(request.getParameter("AreaCode"));
	  tLLCaseDiagnosticInfoSchema.setOperator(Operator);
	  tLLCaseDiagnosticInfoSchema.setMakeDate(cLLCaseDiagnosticInfoSchema.getMakeDate());
	  tLLCaseDiagnosticInfoSchema.setMakeTime(cLLCaseDiagnosticInfoSchema.getMakeTime());
	  tLLCaseDiagnosticInfoSchema.setModifyDate(PubFun.getCurrentDate());
	  tLLCaseDiagnosticInfoSchema.setModifyTime(PubFun.getCurrentTime());
  }
  
	try{
	  // 准备传输数据 VData
	   VData tVData = new VData();
	   tVData.add(tLLCaseDiagnosticInfoSchema);
	   tLLCaseDiagnosticInfoUI.submitData(tVData,transact);
	}
	 catch(Exception ex)
	{
	    Content = "保存失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
    tError = tLLCaseDiagnosticInfoUI.mErrors;
    if (!tError.needDealError())
    {     
      if("INSERT".equals(transact))
	    {
      	    Content ="保存成功！";
	  	    FlagStr = "Success";
	    }
	    else if("DELETE".equals(transact))
	    {
	    	Content ="删除成功！";
	  	    FlagStr = "Success";
	    }
	    else if("UPDATE".equals(transact))
	    {
	    	Content ="修改成功！";
	  	    FlagStr = "Success";
	    }
    }
    else                                                                           
   {
  	    if("INSERT".equals(transact))
	  	    {
  	    	Content = "保存失败，原因是:" + tError.getFirstError();
 	  	     	FlagStr = "Fail";
	  	    }
	  	    else if("DELETE".equals(transact))
	  	    {
	  	    	Content = "删除失败，原因是:" + tError.getFirstError();
		  	    FlagStr = "Fail";
	  	    }
	  	    else if("UPDATE".equals(transact))
	  	    {
	  	    	Content = "修改失败，原因是:" + tError.getFirstError();
		  	    FlagStr = "Fail";
	  	    }
   }
}
}
	

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>