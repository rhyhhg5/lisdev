<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LAMarketInput.jsp
	//程序功能：F1报表生成
	//创建日期：2007-11-13
	//创建人  ：xx
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String Comcode = tG.ManageCom;
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLGrpSocialConfigInput.js"></SCRIPT>
<%@include file="LLGrpSocialConfigInit.jsp"%>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
</head>
<body onload="initForm();">
<form action="" method=post name=fm target="fraSubmit">
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLAAgent1);"></td>
		<td class=titleImg>社保业务保单配置</td>
	</tr>
</table>
<table class=common border=0 width=100%>
	<TR class=common>

		<TD class=title>保单机构</TD>
		<TD class=input><Input class="codeno" name="ManageCom"
			ondblclick="return showCodeList('comcode',[this,organname],[0,1],null,null,null,1);"
			onkeyup="return organname('comcode',[this,organname],[0,1],null,null,null,1);"
			readonly><Input class=codename name=organname></TD>
		<TD class=title8>团体保单号</TD>
		<TD class=input8><Input class=common type='text' name="grpContNo"></TD>

	</tr>
</table>

<Div id="divLLAppClaimReasonGrid" align=center style="display: ''">
<table class=common>
	<TR class=common>
		<TD text-align: left colSpan=1>
		   <span id="spanLLAppClaimReasonGrid"> </span>
		</TD>
	</TR>
</table>
<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
<INPUT VALUE="尾  页"  class=cssButton TYPE=button onclick="turnPage.lastPage();"></Div>
<table>
	<tr>
		<td>
		<input class=cssButton style='width:80px;' type=button value="查 询" onclick="SearchLLAppClaimReason()"> 
		<input class=cssButton style='width:80px;' type=button value="保 存" onclick="saveGrpContNo();"> 
		<input class=cssButton style='width:80px;' type=button value="删 除" onclick="delGrpContNo();">
		</td>
	</TR>
</table>
<input type=hidden name=operate></form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
