<%
//Name:LLSurveyDispatchInit.jsp
//function：调查主管信箱
//author:Xx
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="java.util.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String Operator=tG.Operator;
	String Comcode=tG.ManageCom;
	String CurrentDate= PubFun.getCurrentDate();
	FDate tD=new FDate();
	Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),(-60),"D",null);
	FDate fdate = new FDate();
	String afterdate = fdate.getString( AfterDate );
%>
<script language="JavaScript">
  function initInpBox(){
    try{
			fm.RgtDateS.value="<%=afterdate%>";
			fm.RgtDateE.value="<%=CurrentDate%>";
			fm.Inspector.value="<%=Operator%>";
			fm.ComCode.value="<%=Comcode%>";
			fm.hdlComCode.value="<%=Comcode%>";
			fm.OrganCode.value="<%=Comcode%>";

			var strSQL="select username from llclaimuser where usercode='"+fm.Inspector.value+"'"
				+" union select username from llsocialclaimuser where usercode='"+fm.Inspector.value+"'";
			var arrResult = easyExecSql(strSQL);
			if(arrResult != null){
				fm.InspectorName.value= arrResult[0][0];
			}
			
			//优化，自动带出机构名称
			var tSql="select name from ldcom where comcode='"+fm.OrganCode.value+"'";
			var tResult = easyExecSql(tSql);
			if(tResult != null){
				fm.OrganName.value= tResult[0][0];
			}
      if(fm.ComCode.value=="86"){
        idhq.style.display='';
        idbranch.style.display='none';
        fm.DealWith.value="0"
      }
      else{
        idhq.style.display='none';
        idbranch.style.display='';
        fm.result.readOnly=true;
      }
    }
    catch(ex){
      alter("在LLSurveyDispatchInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在LLLSurveyDispatchInit-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm(){
    try{
      initInpBox();
      initCheckGrid();
      easyQuery();
    }
    catch(re){
      alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }

  function initCheckGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","0px","10","0");
      iArray[1]=new Array("案件号","150px","0","0");
      iArray[2]=new Array("调查序号","50px","0","0");
      iArray[3]=new Array("提调机构","80px","0","0");
      iArray[4]=new Array("提调人","60px","0","0");
      iArray[5]=new Array("提调日期","80px","0","0");
      iArray[6]=new Array("提调状态","60px","0","0");
      iArray[7]=new Array("调查项目号","80px","0","3");
      iArray[8]=new Array("调查人","60px","0","0");
      iArray[9]=new Array("调查类型","80px","0","0");
      iArray[10]=new Array("调查号类型","80px","0","3");
      iArray[11]=new Array("提调机构代码","80px","0","3");
      iArray[12]=new Array("提调人代码","60px","0","3");
      iArray[13]=new Array("涉案金额","60px","0","0");
      iArray[14]=new Array("案件状态","60px","0","0");


      CheckGrid = new MulLineEnter("fm","CheckGrid");
      CheckGrid.mulLineCount =10;
      CheckGrid.displayTitle = 1;
      CheckGrid.locked = 1;
      CheckGrid.canSel =1;
      CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
      CheckGrid.loadMulLine(iArray);
      CheckGrid.selBoxEventFuncName = "DealSurvey";
    }
    catch(ex){
      alter(ex);
    }
  }

</script>