var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";

function easyQuery(){
	var typeRadio="";
	for(i = 0; i <fm.OpType.length; i++){
		if(fm.OpType[i].checked){
			typeRadio=fm.OpType[i].value;
			break;
		}
	}
	if(typeRadio=='01'){
		divNew.style.display='';
		divAttribute.style.display='none';
		fm.DelButton.style.display='';
	}
	if(typeRadio=='02'){
		divNew.style.display='none';
		divAttribute.style.display='';
		fm.DelButton.style.display='none';
	}
}

function afterCodeSelect(ObjType,Obj){
  if(ObjType=='llrgtreason'){
    easyQueryClick();
  }
    if(ObjType=='llmaffixtype'){
    easyQueryBaseClick();
  }
}

function easyQueryClick()
{
	
  // 书写SQL语句

  var strSQL= "SELECT a.affixcode, b.affixname FROM LLMAppReasonAffix a,llmaffix b "
  			  +" where  a.affixcode=b.affixcode "
  			  + getWherePart("reasoncode","codetype")
  			  +"  with ur "
  			  ;
                                
 arrResult = easyExecSql(strSQL);
	if ( arrResult!=null )
	{
		 displayMultiline(arrResult,AffixGrid);  
  	}
	else
	{
		AffixGrid.clearData();
	}

}

function easyQueryBaseClick()
{
	
  // 书写SQL语句

  var strSQL= "SELECT AffixTypeCode,AffixTypeName,AffixCode,AffixName FROM llmaffix "
  			  +" where 1=1 "
  			  + getWherePart("AffixTypeCode","AffixType")
  			  +"  with ur ";
                                
 arrResult = easyExecSql(strSQL);
	if ( arrResult!=null )
	{
		 displayMultiline(arrResult,AffixBaseGrid);  
  	}
	else
	{
		AffixBaseGrid.clearData();
	}

}

function ModifySave()
{
		var typeRadio="";
	for(i = 0; i <fm.OpType.length; i++){
		if(fm.OpType[i].checked){
			typeRadio=fm.OpType[i].value;
			break;
		}
	}
		if(typeRadio=='01'){
	  	var AffixCount=AffixBaseGrid.mulLineCount;			
				if(AffixCount==0){
					alert("没有材料");
					  return alse;
					}
		  for (i=0;i<AffixCount;i++){
		  	if(AffixBaseGrid.getRowColData(i,1).length >6) {
		  	alert("材料类型代码不能大于6位");
		  	    return false;
		  	  }
			if(AffixBaseGrid.getRowColData(i,2)==null||AffixBaseGrid.getRowColData(i,2)=="") {
				alert("材料类型名称不能为空");
				return false;
			}  
			if(AffixBaseGrid.getRowColData(i,3).length >6) {
				alert("材料代码不能大于6位");
				return false;
			}  
			if(AffixBaseGrid.getRowColData(i,4)==null||AffixBaseGrid.getRowColData(i,4)=="") {
				alert("材料名称不能为空");
				return false;
			}  						
	}
}
		if(typeRadio=='02'){
				  	var ACount=AffixGrid.mulLineCount;			
					
				for (i=0;i<ACount;i++){
				if(AffixGrid.getRowColData(i,1)==null||AffixGrid.getRowColData(i,1)=="") {
		  	alert("材料代码不能为空！");
		  	    return false;
		  	  }
					var strSQL2 ="SELECT AffixName FROM llmaffix "
											+" where AffixCode='"+AffixGrid.getRowColData(i,1)+"'";
						var brr = easyExecSql(strSQL2 );
				  if ( brr==null ){
				  alert("请选择系统中已有的材料!");
				   return false;
				}
				  }
			}
		 fm.all('operate').value = "INSERT";
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	   fm.action ="./LLReasonAffixSave.jsp";
	   fm.submit();

}
function DeleteClick()
{

	var AffixCount=AffixBaseGrid.mulLineCount;
  if(AffixCount==0){
			alert("没有材料");
			  return false;
					}
  var chkFlag=false;
  for (i=0;i<AffixCount;i++){
    if(AffixBaseGrid.getChkNo(i)==true){
      chkFlag=true;
    }
  }
  if (chkFlag==false){
    alert("请选中要删除的材料!");
    return false;
  }
			fm.all('operate').value = "DELETE";
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	   fm.action ="./LLReasonAffixSave.jsp";
	   fm.submit();

}


function afterSubmit( FlagStr, content )
{
  showInfo.close();

  easyQuery();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      
    easyQueryBaseClick();
 easyQueryClick();
  }
}
