<%
  //程序名称：LLPrepaidClaimInputInit.jsp
  //程序功能：预付赔款录入
  //创建日期：2010-11-25
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm()
{
	try{
		initGrpContGrid();
		initPrepaidClaimGrid();
		initPrepaidClaimDetailGrid();
	}
	catch(re){
		alert("LLPrepaidClaimInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initGrpContGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="管理机构";
		iArray[1][1]="40px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="团体保单号";
		iArray[2][1]="70px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		iArray[2][21]="grpcontno";

		iArray[3]=new Array();
		iArray[3][0]="投保单位名称";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="生效日期";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="失效日期";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="预付赔款合计";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		
		GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 

		GrpContGrid.mulLineCount=1;
		GrpContGrid.displayTitle=1;
		GrpContGrid.canSel=1;
		GrpContGrid.canChk=0;
		GrpContGrid.hiddenPlus=1;
		GrpContGrid.hiddenSubtraction=1;
		GrpContGrid.selBoxEventFuncName = "onSelGrpInfo";

		GrpContGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

function initPrepaidClaimGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="险种编码";
		iArray[1][1]="40px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="险种名称";
		iArray[2][1]="100px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="实收保费";
		iArray[3][1]="50px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="预付赔款合计";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="给付确认赔款";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="未给付确认赔款";
		iArray[6][1]="70px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		iArray[6][21]="realpay";
		
		iArray[7]=new Array();
		iArray[7][0]="应回收预付赔款";
		iArray[7][1]="70px";
		iArray[7][2]=100;
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="实际回收赔款";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=1;
		iArray[8][21]="money";
		
		iArray[9]=new Array();
		iArray[9][0]="团体险种号";
		iArray[9][1]="0px";
		iArray[9][2]=100;
		iArray[9][3]=3;

		PrepaidClaimGrid = new MulLineEnter( "fm" , "PrepaidClaimGrid" ); 

		PrepaidClaimGrid.mulLineCount=1;
		PrepaidClaimGrid.displayTitle=1;
		PrepaidClaimGrid.canSel=0;
		PrepaidClaimGrid.canChk=1;
		PrepaidClaimGrid.hiddenPlus=1;
		PrepaidClaimGrid.hiddenSubtraction=1;

		PrepaidClaimGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

function initPrepaidClaimDetailGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="预付赔款号";
		iArray[1][1]="90px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="预付类型";
		iArray[2][1]="50px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="预付金额";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="申请日期";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="申请人";
		iArray[5][1]="40px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="预付赔款状态";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		
		iArray[7]=new Array();
		iArray[7][0]="通知日期";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		PrepaidClaimDetailGrid = new MulLineEnter( "fm" , "PrepaidClaimDetailGrid" ); 

		PrepaidClaimDetailGrid.mulLineCount=1;
		PrepaidClaimDetailGrid.displayTitle=1;
		PrepaidClaimDetailGrid.canSel=0;
		PrepaidClaimDetailGrid.canChk=0;
		PrepaidClaimDetailGrid.hiddenPlus=1;
		PrepaidClaimDetailGrid.hiddenSubtraction=1;

		PrepaidClaimDetailGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

</script>
