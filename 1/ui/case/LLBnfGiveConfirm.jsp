<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：LJAGetSave.jsp
//程序功能：
//创建日期：2005-06-27 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//接收信息，并作校验处理。
//输入参数
LLCaseSchema tLLCaseSchema = new LLCaseSchema();
LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
LJAGetSchema tLJAGetSchema   = new LJAGetSchema();
LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
//输出参数
String FlagStr = "";
String Content = "";

GlobalInput tGI = new GlobalInput();
tGI=(GlobalInput)session.getValue("GI");
if(tGI==null)
{
  FlagStr = "Fail";
  Content = "页面失效,请重新登陆";
}
else //页面有效
{
  String Operator  = tGI.Operator ;
  String ManageCom = tGI.ComCode  ;

  CErrors tError = null;
  String tBmCert = "";

  //后面要执行的动作：添加，修改，删除
  String RgtNo=request.getParameter("RgtNo");
  System.out.println(RgtNo);
  String CaseNo=request.getParameter("CaseNo");
  String AccName=request.getParameter("AccName");
  String BankAccNo=request.getParameter("BankAccNo");
  String BankCode=request.getParameter("BankCode");
  String paymode=request.getParameter("paymode");
  String transact=request.getParameter("fmtransact");
  System.out.println("transact:"+transact+request.getParameter("CaseNo"));

  if (request.getParameterValues("PrePaidFlag") != null) {
	  tLLCaseSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
  }else{
	  tLLCaseSchema.setPrePaidFlag("");  // 个案存llcase 批次案件llregister
  }
  tLLCaseSchema.setCaseNo(CaseNo);
  tLLCaseSchema.setAccModifyReason(request.getParameter("ModifyReason"));
  tLJAGetSchema.setOtherNo(request.getParameter("CaseNo"));
  tLJAGetSchema.setOtherNoType("5");
  tLJAGetSchema.setAccName(request.getParameter("AccName"));
  tLJAGetSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLJAGetSchema.setPayMode(request.getParameter("paymode"));
  System.out.println("给付方式：：：：：：：：：：："+tLJAGetSchema.getPayMode());
  tLJAGetSchema.setBankCode(request.getParameter("BankCode"));
  tLJAGetSchema.setDrawer(request.getParameter("Drawer"));
  tLJAGetSchema.setDrawerID(request.getParameter("DrawerID")); 
  //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
  tLLCaseExtSchema.setDrawerIDType(request.getParameter("DrawerIDType"));
  tLLCaseExtSchema.setDrawerID(request.getParameter("DrawerID"));
  //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
  try{
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tLLCaseExtSchema);
    tVData.addElement(tLJAGetSchema);
    tVData.addElement(tGI);
    System.out.println("submit======================:");
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    if (tLJAGetInsertBL.submitData(tVData,transact))
    {    
    	tError = tLJAGetInsertBL.mErrors;
   	    if (!tError.needDealError())
   	    {	   	   
	   	    Content = "给付确认保存成功" + tLJAGetInsertBL.getBackMsg();
	   	    FlagStr = "Succ";
   	      	   	    
	  	    System.out.println("LLContAutoDealBL============Begin==========:");
	  	    try{				
				String tsql = "select 1 from llclaimdetail where caseno = '"+CaseNo+"' and riskcode in (select code from ldcode where codetype='lxb' ) with ur ";
		    	SSRS tSSRS = new ExeSQL().execSQL(tsql);   	
		    	
		    	
		  	  	  if(tSSRS.MaxRow>0){
		  	  	  	LPSecondUWBL tLPSecondUWBL  = new LPSecondUWBL();
		  	  	  	tLPSecondUWBL.submitData(tVData,"");
		  	  	  }else{
					LLContAutoDealBL tLLContAutoDealBL = new LLContAutoDealBL();
					tLLContAutoDealBL.submitData(tVData,"");
					System.out.println(tLLContAutoDealBL.getBackMsg());
					Content += tLLContAutoDealBL.getBackMsg();
		  	  	  }
		  	  	  System.out.println("Content=="+Content);
		  		
	  	    }catch(Exception tex) {
	  	    	Content += "合同终止保存失败，原因是:" + tex.toString()+"，请到理赔合同处理中进行操作!";
	  	    	}
	  	 }else{
   	      Content = "给付确认保存失败，原因是:" + tError.getFirstError();
   	      FlagStr = "Fail";
   	     }   	       		    	    	
	}else{
    	  tError = tLJAGetInsertBL.mErrors;
     	  Content = "给付确认保存失败，原因是:" + tError.getFirstError();
     	  FlagStr = "Fail";
    }  
  }
  catch(Exception ex)
  {
    Content = "给付确认保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  } 
  tGI.Operator = Operator; 
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

