<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
  String LoadC="";
  String RgtNo = "";
	if(request.getParameter("LoadC")!=null)
	{
		LoadC = request.getParameter("LoadC");
	}
	if(request.getParameter("RgtNo")!=null)
	{
		RgtNo = request.getParameter("RgtNo");
	}
%>

<script language="JavaScript">

function initInpBox( )
{
  try {
     fm.RgtNo.value="<%=RgtNo%>";
    	  <%GlobalInput mG = new GlobalInput();
  	mG=(GlobalInput)session.getValue("GI");
  	%>
     fm.Handler.value = "<%=mG.Operator%>";
     fm.ModifyDate.value = getCurrentDate();
     var BusinessType  = "";
     try{
     	BusinessType = '<%=request.getParameter("BusinessType")%>';
    }
    catch(ex){
    }
    if(BusinessType!=''&&BusinessType!='null'){
    	fm.ApplyerType.value = '5';
    }
    if(BusinessType=='QDSC'){
    	idApplyAmnt.style.display='';
    }
      
    var tManageCom = <%=mG.ManageCom%>;
    if (tManageCom.length>4){
     	tManageCom = tManageCom.substring(0,4);
    }
     
    //var tSQL = "SELECT 1 FROM dual WHERE '"+tManageCom+"' IN ('8611','8612','8631','8632','8695')";
    //var tMFlag = easyExecSql(tSQL);
    //if (tMFlag==null||tMFlag==""){
     	SimpleCase1.style.display='';
    	SimpleCase2.style.display='none';
     	fm.SimpleCase[1].checked = "true";
    //} else {
     	//SimpleCase1.style.display='none';
     	//SimpleCase2.style.display='';
     	//fm.SimpleCase[0].checked = "true";
    //}
     fm.CustomerNo.value="";
     fm.GrpName.value="";
     fm.GrpContNo.value="";
     fm.PeopleNo.value="";

     fm.BankCode.value="";
     fm.BankAccNo.value="";
     fm.AccName.value="";
     
     fm.RgtType.value="";
     fm.AppPeoples.value=""; 
     fm.RgtantName.value="";
     fm.RgtantPhone.value="";
     fm.RgtantAddress.value="";
     fm.PostCode.value="";
     fm.TogetherFlag.value="";
     fm.AppAmnt.value="";
     fm.Remark.value="";

     fm.CaseGetMode.value="";
     fm.IDType.value="";
     fm.IDTypeName.value="";
     fm.IDNo.value="";
     fm.RiskCode.value="";

  } catch(ex) {
    alert("在RegisterInputInit.jsp-->initInpBox函数中发生异常aaaaaaaaa:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常yyyyyyy:初始化界面错误!");
  }
}

function initForm()
{
  try
  {

    initInpBox();
    initGrpRegisterGrid();
    SearchGrpRegister();
    initGrpCaseGrid();
    fm.LoadC.value="<%=LoadC%>";
    if (fm.LoadC.value=='2'){
      divgrpconf.style.display='none';
    }
    if(fm.RgtNo.value!=''){
  		doSearchShow();
  		queryGrpCaseGrid();
    }
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpRegisterGrid()
{
   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("团体批次号","120px","0",0);
    iArray[2]=new Array("团体客户号","100px","0",0);
    iArray[3]=new Array("单位名称","120px","0",0);
    iArray[4]=new Array("团体合同号","100px","0",0);
    iArray[5]=new Array("申请人","60px","0",0);
    iArray[6]=new Array("申请日期","80px","0",0);
    iArray[7]=new Array("申请人数","60px","0",0);
    iArray[8]=new Array("案件状态","60px","0",0);
    GrpRegisterGrid = new MulLineEnter( "fm" , "GrpRegisterGrid" );
    
    GrpRegisterGrid.mulLineCount = 5;
    GrpRegisterGrid.displayTitle = 1;
    GrpRegisterGrid.canChk =0;
    GrpRegisterGrid.canSel =1;
    GrpRegisterGrid.hiddenPlus=1;
    GrpRegisterGrid.hiddenSubtraction=1; 
    GrpRegisterGrid.locked = 1;
    GrpRegisterGrid.selBoxEventFuncName = "getRegisterInfo";
    GrpRegisterGrid.loadMulLine(iArray);
   } 
  catch(ex)
  {
    alert(ex);
  }    
}

function initGrpCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
    
    iArray[1]=new Array("理赔号", "120px", "0", "0");
    iArray[2]=new Array("客户号", "100px", "0", "0");
    iArray[3]=new Array("客户姓名", "100px", "0", "0");
    iArray[4]=new Array("申请日期", "100px", "0", "0");
    iArray[5]=new Array("案件状态", "100px", "0", "0");
    GrpCaseGrid = new MulLineEnter("fm","GrpCaseGrid");
    GrpCaseGrid.mulLineCount =5;
    GrpCaseGrid.displayTitle = 1;
    GrpCaseGrid.locked = 1;
    GrpCaseGrid.canSel =0;
    GrpCaseGrid.hiddenPlus=1;  
    GrpCaseGrid.hiddenSubtraction=1; 
    GrpCaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

</script>