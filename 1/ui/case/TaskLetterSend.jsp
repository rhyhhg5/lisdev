<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskLetterPrintOption.jsp
//程序功能：
//创建日期：2005-
//创建人  ：Yang yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskLetterSend.js"></SCRIPT>
  <%@include file="TaskLetterSendInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>函件下发 </title>
</head>
<body  onload="initForm();">
<center>
	<br><br>
	<form name=fm action="" target=fraSubmit method=post>
		<table class= common>
			<tr  class= common>
		  		<td  class= input align=center>
					<input type=radio name="printMode" value="1">即时打印
					<input type=radio name="printMode" value="2">批量打印
		  		</td>
		  	</tr>
		  	<tr class=common>
		  		<td  class= input align=center>
					<INPUT VALUE="确  认" TYPE=button Class="cssButton" name="" onclick="printLetter();">
					<INPUT VALUE="暂不打印" TYPE=button Class="cssButton" name="" onclick="printLater();">
					<INPUT VALUE="取  消" TYPE=button Class="cssButton" name="" onclick="goBack();">
		  		</td>
			</tr>
		</table>
		<input type=hidden name="serialNumber" value="">
		<input type=hidden name="edorAcceptNo" value="">
		<input type=hidden name="letterType" value="">
		<input type=hidden name="addressNo" value="">
		<input type=hidden name="backFlag" value="">
		<input type=hidden name="backDate" value="">
		
		<input type=hidden name="printType" value="">
	</Form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</center>
</body>
</html>
