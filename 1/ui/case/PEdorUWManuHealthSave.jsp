<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuHealthSave.jsp
//程序功能：保全人工核保体检资料录入
//创建日期：2006-02-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String flag;
  String content;

  GlobalInput gi = (GlobalInput)session.getValue("GI");	  
	
	String serialNo[] = request.getParameterValues("HealthGridNo");
	String healthcode[] = request.getParameterValues("HealthGrid1");
	String healthname[] = request.getParameterValues("HealthGrid2");
	String ifEmpty[] = request.getParameterValues("HealthGrid3");
	
	LPPENoticeSchema tLPPENoticeSchema = new LPPENoticeSchema();
  tLPPENoticeSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPPENoticeSchema.setContNo(request.getParameter("ContNo"));
  tLPPENoticeSchema.setCustomerNo(request.getParameter("InsuredNo"));
  tLPPENoticeSchema.setPEAddress(request.getParameter("Hospital"));
  tLPPENoticeSchema.setPEDate(request.getParameter("EDate"));
  tLPPENoticeSchema.setRemark(request.getParameter("Note"));
  
  LPPENoticeItemSet tLPPENoticeItemSet = new LPPENoticeItemSet();
  for (int i = 0; i < serialNo.length; i++)
  {
    LPPENoticeItemSchema tLPPENoticeItemSchema = new LPPENoticeItemSchema();
    tLPPENoticeItemSchema.setPEItemCode(healthcode[i]);
    tLPPENoticeItemSchema.setPEItemName(healthname[i]); 	
    tLPPENoticeItemSchema.setFreePE(ifEmpty[i]);  
    tLPPENoticeItemSet.add(tLPPENoticeItemSchema);
  }
  
  VData data = new VData();
  data.add(gi);
  data.add(tLPPENoticeSchema); 
  data.add(tLPPENoticeItemSet);
  PEdorUWManuHealthUI tPEdorUWManuHealthUI = new PEdorUWManuHealthUI();
  if (!tPEdorUWManuHealthUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorUWManuHealthUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！体检通知书号为" + tPEdorUWManuHealthUI.getPrtSeq();
  }
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>
