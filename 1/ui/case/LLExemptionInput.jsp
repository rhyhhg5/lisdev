<html>
<%
//程序名称：LLExemptionInput.jsp
//程序功能：豁免保费
//创建日期：
//创建人  ：Houyd
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LLExemptionInput.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLExemptionInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
  <form  action='./LLExemptionSave.jsp' method=post name=fm target="fraSubmit">
    <table>
      <tr>
	      <td>
  	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
    	  </td>
      	<td class= titleImg>
      	  案件基本信息
      	</td>
    	</tr>
    </table>

  <Div  id= "divLLCase1" style= "display: ''">
		<table  class= common>
			<TR  class= common id= 'divCase'>
				<TD  class= title>
					理赔号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=CaseNo >
				</TD>
				<TD  class= title>
					客户号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=CustomerNo >
				</TD>
				<TD  class= title>
					客户姓名
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=CustomerName  >
				</TD>
			</TR>
     </table>
    </div>
    <Div  id= "divClaimPay" style= "display: ''">
    <table style= "display: ''">
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimPay);">
        </td>
        <td class= titleImg> 豁免保单明细</td>
      </tr>
    </table>
    <table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
				<span id="spanExemptionGrid" >
				</span>
			</td>
		</tr>
    </table>
  </div>
     <table  class= common>
       <TR  class= common>
          <TD  class= title colspan=6>
            备注
          </TD>
       </tr>
       <TR  class= common>
          <TD  class= input >
			<textarea name="Remark" cols="90%" rows="4" width=25% class="common"></textarea>
	  	  </TD>
       </tr>  
      </table>
    <INPUT VALUE="确认豁免保费" TYPE=button class=cssbutton onclick="submitForm(0);">
    <INPUT VALUE="取消豁免保费" TYPE=button class=cssbutton onclick="submitForm(1);">
    <INPUT VALUE="返  回" TYPE=button class=cssbutton onclick="top.close();"> 
    
    	<input type=hidden id="fmtransact" name="fmtransact">			
    	<input type=hidden id="LoadFlag" name="LoadFlag">
    	<input type=hidden name='ClaimPolGridNum'>
    	<input type=hidden id="cOperator" name="cOperator">

    	
          </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
 
</script>
</html>
