<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSubmittedAffixListSave.jsp
//程序功能：
//创建日期：2005-04-05
//创建人  ：DongXin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

  <%
  	System.out.println("<-Star SavePage->");
    LLAffixSet tLLAffixSet= new LLAffixSet();
    LLAffixBL tLLAffixBL= new LLAffixBL();
    VData tVData = new VData();
    
      CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  GlobalInput tG = new GlobalInput(); 
	  tG=(GlobalInput)session.getValue("GI");
	  transact = request.getParameter("fmtransact");	
	  System.out.println(transact);

    
    
	String tAffixType[] = request.getParameterValues("AffixGrid1"); //材料类型
	String tAffixCode[] = request.getParameterValues("AffixGrid2"); //材料代码
	String tAffixName[] = request.getParameterValues("AffixGrid3"); //材料名称
	String tSupplyDate[] = request.getParameterValues("AffixGrid4");//提供日期
	String tProperty[] = request.getParameterValues("AffixGrid5");	//材料属性
	String tCount[] = request.getParameterValues("AffixGrid6");		//材料数量
	String tShortCount[] = request.getParameterValues("AffixGrid7");//缺少材料
	String tAffixNo[] = request.getParameterValues("AffixGrid8");
	String tSerialNo[] = request.getParameterValues("AffixGrid9");
	//int mShortCount[] = new int[100];//用于判断材料是否齐备
	//String ShortCountFlag ="";//1材料不齐备 0材料齐备
	//String mSupplyDate[] = new String[100];//用于判断材料齐备日期
	//String SupplyDateResult="";//用于存放齐备的日期
	String AffixNo="";
	String tChk[] = request.getParameterValues("InpAffixGridChk"); 
	 for(int index=0;index<tChk.length;index++)
	 {
	   if(tChk[index].equals("1") && tAffixNo[index]!=null )
	   {
	   	System.out.println("index : "+index);
	     LLAffixSchema tLLAffixSchema = new LLAffixSchema();
	     System.out.println("AffixType:"+tAffixType[index]);
	     tLLAffixSchema.setAffixType(tAffixType[index]);
	     tLLAffixSchema.setAffixCode(tAffixCode[index]);
	     tLLAffixSchema.setAffixName(tAffixName[index]);
	     	     
	     tLLAffixSchema.setSupplyDate(  tSupplyDate[index] );	     	     
	     tLLAffixSchema.setShortCount(tShortCount[index]);
	     tLLAffixSchema.setCount(tCount[index]);
	     tLLAffixSchema.setProperty(tProperty[index]);
	     tLLAffixSchema.setAffixNo(tAffixNo[index]);
	     tLLAffixSchema.setSerialNo(tSerialNo[index]);
	     System.out.println("RgtNo : "+request.getParameter("RgtNo"));
	     tLLAffixSchema.setRgtNo(request.getParameter("RgtNo"));
	     tLLAffixSchema.setCaseNo(request.getParameter("CaseNo"));

	     tLLAffixSchema.setReasonCode("13");
	     System.out.println("ReasonFlag : "+request.getParameter("ReasonFlag"));
	     tLLAffixSet.add(tLLAffixSchema);
	   }           
	 }
	 
if (FlagStr.equals("")) 
{
	try
	{
	
		// 准备传输数据 VData
		tVData.add(tLLAffixSet);
		tVData.add(tG);
		if (!tLLAffixBL.submitData(tVData,transact)) 
		{
		  
			FlagStr = "Fail";
		}
	}
	catch(Exception ex)
	{
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}	 

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	 // if (FlagStr.equals("")) 
	//  {
	    tError = tLLAffixBL.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 保存成功! ";
	    	FlagStr = "Success";
	    	tVData.clear();
	    	tVData = tLLAffixBL.getResult();
	    }
	    else                                                                           
	    {
	    	Content = " 保存失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	 // }

 }
%>
             

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>