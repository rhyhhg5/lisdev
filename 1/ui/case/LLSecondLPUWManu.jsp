<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorUWManu.jsp
//程序功能：保全人工核保
//创建日期：2005-08-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LLSecondLPUWManu.js"></SCRIPT>
  <%@include file="./LLSecondLPUWManuInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>人工核保</title>	
</head>
<body onload="initForm();">
<form name=fm action="./LPPEdorUWManuSave.jsp" target=fraSubmit method=post>
<br>
	<div id= "divButton" style="display: ''" >
    <input type="button" class=cssButton  value="案件扫描影印件" onclick="showEdorScan();">
	<input type="button" class=cssButton  value="既往核保信息" onclick="showUWInfo();">
	<input type="button" class=cssButton  value="调查报告" onclick="showInvestInfo();">
  </div>
<br>
	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWErrList);"> 
      </td>
      <td class= titleImg>核保信息 </td>
    </tr>
  </table>
	<Div  id= "divUWErrList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanUWErrGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage" align=center style= "display: 'none' ">
			<INPUT value="首  页" class=cssButton type=button onclick="turnPage.firstPage(); showCodeName();"> 
			<INPUT value="上一页" class=cssButton type=button onclick="turnPage.previousPage(); showCodeName();"> 					
			<INPUT value="下一页" class=cssButton type=button onclick="turnPage.nextPage(); showCodeName();"> 
			<INPUT value="尾  页" class=cssButton type=button onclick="turnPage.lastPage(); showCodeName();"> 
		</Div>
	</Div>
	<hr>
	
	 <table class= common>
				<TR class=common>
					<TD class=title8> 理赔案件查看
					</TD>
					<TD class=input8>
						<input class=codeno
							CodeData="0|8^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|撤件^8|材料退回登记 "
							name=DealWith
							ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);">
						<input class=codename name=DealWithName>
					</TD>
				</TR>      
	 </table>
	
	
	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolList);"> 
      </td>
      <td class= titleImg>项目信息 </td>
      <td>
      	&nbsp;&nbsp;<!--input value="健康告知" class=cssButton type=button onclick="healthImpart();"-->
      </td>
    </tr>
  </table>
	<Div  id= "divPolList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanPolGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage2" align=center style= "display: 'none' ">
			<INPUT value="首  页" class=cssButton type=button onclick="turnPage2.firstPage(); showCodeName();"> 
			<INPUT value="上一页" class=cssButton type=button onclick="turnPage2.previousPage(); showCodeName();"> 					
			<INPUT value="下一页" class=cssButton type=button onclick="turnPage2.nextPage(); showCodeName();"> 
			<INPUT value="尾  页" class=cssButton type=button onclick="turnPage2.lastPage(); showCodeName();"> 
		</Div>
	 </Div>
	 	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseList);"> 
      </td>
      <td class= titleImg>理赔信息 </td>
      <td>
      	&nbsp;&nbsp;<!--input value="健康告知" class=cssButton type=button onclick="healthImpart();"-->
      </td>
    </tr>
  </table>
	<Div  id= "divCaseList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanCaseGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage6" align=center style= "display: 'none' ">
			<INPUT value="首  页" class=cssButton type=button onclick="turnPage6.firstPage(); showCodeName();"> 
			<INPUT value="上一页" class=cssButton type=button onclick="turnPage6.previousPage(); showCodeName();"> 					
			<INPUT value="下一页" class=cssButton type=button onclick="turnPage6.nextPage(); showCodeName();"> 
			<INPUT value="尾  页" class=cssButton type=button onclick="turnPage6.lastPage(); showCodeName();"> 
		</Div>
	 </Div>	 

<div id="divUW" style="display: '<%if (hasUW == true) out.print("none");%>'">
	<table>
    <tr> 
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWResult);"> 
      </td>
      <td class= titleImg>核保信息 </td>
    </tr>
  </table>
  <Div  id= "divUWResult" style= "display: ''" align=center>  
  <table class= common>
    <TR class= common> 
      <TD class= title> 核保结论 </TD>
      <TD class= input>
        <Input class="codeNo" name="PassFlag1" CodeData="0|^1|标准承保^2|变更承保^3|拒保" verify="核保结论|notnull" ondblclick="return showCodeListEx('PassFlag1',[this,PassFlagName],[0,1]);" onkeyup="return showCodeListEx('PassFlag1',[this,PassFlagName],[0,1]);"><Input class="codeName" name="PassFlagName"  elementtype="nacessary" readonly>
      </TD> 
	  </TR>
	  <TR class= common> 
      <TD class= title> 核保意见</TD>
      <TD class= input colspan="5">
      	<textarea name="UWIdea" verify="核保意见" cols="100%" rows="3" class="common"></textarea>
      </TD>
    </TR>
  </TABLE>
 </Div>
  <br>
  <div style="display: ''" >
    <input type="button" class=cssButton name="confirm" value="保存结论" onclick="saveDecision();">
	<input type="button" class=cssButton name="redo" value="核保完毕" onclick="confirmDecision();">
  </div>
</div>
<div id="divUWInfo" style="display: '<%if (hasUW == false) out.print("none");%>'">
  <table>
    <tr>
      <td> 
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divManuUWList);"> 
      </td>
      <td class= titleImg>人工核保信息 </td>
    </tr>
  </table>
	<Div  id= "divManuUWList" style= "display: ''" align=center>  
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanManuUWGrid" >
				</span> 
		  	</td>
		</tr>
		</table>
		<Div id= "divPage5" align=center style= "display: 'none' ">
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage5.firstPage(); showCodeName();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage5.previousPage(); showCodeName();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage5.nextPage(); showCodeName();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage5.lastPage(); showCodeName();"> 
		</Div>
	</div>
</div>
  <input type="hidden" name="MissionId">
  <input type="hidden" name="SubMissionId">
  <input type="hidden" name="ActivityId">
	<input type="hidden" name="ContNo">
	<input type="hidden" name="PrtNo">
	<input type="hidden" name="CaseNo">
	<input type="hidden" name="PolNo">
	<input type="hidden" name="EdorNo">
	<input type="hidden" name="EdorType">
	<input type="hidden" name="InsuredNo">
	<input type="hidden" name="fmtransact">
	<input type="hidden" name="PassFlag">
	
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>