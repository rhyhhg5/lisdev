<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：LLRegisterSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  LLRegisterUpdateBL tLLRegisterUpdateBL   = new LLRegisterUpdateBL();
  
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
   String Operator  = tGI.Operator ;  //保存登陆管理员账号
   String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  
  CErrors tError = null;
  String tBmCert = "";
  
  //后面要执行的动作：添加，修改，删除
  String RgtNo=request.getParameter("RgtNo");
  String CaseNo = request.getParameter("CaseNo");
  String AccName=request.getParameter("AccName");
  String BankAccNo=request.getParameter("BankAccNo");
  String BankCode=request.getParameter("BankCode");
  String paymode=request.getParameter("paymode");
  
  String transact=request.getParameter("fmtransact");
System.out.println("transact:"+transact+RgtNo); 
    
    tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));    
    tLLRegisterSchema.setAccName(request.getParameter("AccName"));
    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setGetMode(request.getParameter("paymode"));
    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));    
    tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
    tLLCaseSchema.setAccName(request.getParameter("AccName"));
    tLLCaseSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLCaseSchema.setCaseGetMode(request.getParameter("paymode"));
    tLLCaseSchema.setBankCode(request.getParameter("BankCode"));    
    
 try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
  
   tVData.addElement(tGI);
   tVData.addElement(tLLRegisterSchema);
   tVData.addElement(tLLCaseSchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  if ( tLLRegisterUpdateBL.submitData(tVData,transact))
  {		
		    	System.out.println("------return");
			    	
		    	tVData.clear();
		    	//tVData = tLLRegisterUpdateBL.getResult();

	}
  }
  catch(Exception ex)
  {
    Content = "修改失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLRegisterUpdateBL.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="修改成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "修改失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
   }
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

