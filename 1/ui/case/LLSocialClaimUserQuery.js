var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询操作
function submitForm()
{
  initRegisterGrid();
  if( verifyInput2() == false ) return false;
  var strSQL = "select UserCode,UserName,ComCode,UpUserCode "
             +" from LLSocialclaimUser where ComCode like '"+fm.Station.value+"%'"
             +  	getWherePart( 'UserCode','UserCode' )
             +  getWherePart('UserName','UserName')
             +  getWherePart('HandleFlag','HandleFlag')
             " order by UserCode ";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
    alert("在该管理机构下没有满足查询条件的社保业务用户");
    return "";
  }

  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.pageDisplayGrid = RegisterGrid;
  turnPage.strQuerySql     = strSQL;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

//显示操作
function displayQueryResult(strResult)
{
  var filterArray          = new Array(0,1,2,3,4);
  turnPage.strQueryResult  = strResult;
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.useSimulation   = 1;
  turnPage.pageDisplayGrid = RegisterGrid;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}


//返回父级
function returnParent()
{
  var closeflag = 0;
  fm.fmtransact.value = "QUERY";
  var tRow=RegisterGrid.getSelNo();
  if(tRow==0)
  {
    top.close();
  }
  else{
    tCol=1;
	var tUserCode = RegisterGrid.getRowColData(tRow-1,tCol);	
	try
	{
		top.opener.afterQuery( tUserCode );			
	}catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口:" + ex.message );
		}
		top.close();
	}  
}

