<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLAnswerInfoSave.jsp
//程序功能：
//创建日期：2005-01-14 15:54:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLAnswerInfoSchema tLLAnswerInfoSchema   = new LLAnswerInfoSchema();
  OLLAnswerInfoUI tOLLAnswerInfoUI   = new OLLAnswerInfoUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  
   transact = request.getParameter("fmtransact");
    String Path = application.getRealPath("config//Conversion.config");
    
    
    
    
   
    tLLAnswerInfoSchema.setLogNo(request.getParameter("LogNo"));
      
   // tLLAnswerInfoSchema.setAskState(request.getParameter("AskState"));
   // tLLAnswerInfoSchema.setAnswerOperator(request.getParameter("AnswerOperator"));
    tLLAnswerInfoSchema.setAnswer(StrTool.Conversion(request.getParameter("Answer"),Path));
   // tLLAnswerInfoSchema.setManageCom(request.getParameter("ManageCom"));
   // tLLAnswerInfoSchema.setAnswerDate(request.getParameter("AnswerDate"));
   // tLLAnswerInfoSchema.setAnswerTime(request.getParameter("AnswerTime"));
   // tLLAnswerInfoSchema.setSendFlag(request.getParameter("SendFlag"));
   // tLLAnswerInfoSchema.setRemark(request.getParameter("Remark"));
   // tLLAnswerInfoSchema.setMakeDate(request.getParameter("MakeDate"));
   // tLLAnswerInfoSchema.setMakeTime(request.getParameter("MakeTime"));
   // tLLAnswerInfoSchema.setModifyDate(request.getParameter("ModifyDate"));
   // tLLAnswerInfoSchema.setModifyTime(request.getParameter("ModifyTime"));
  String askType=request.getParameter("AskType");
   tLLAnswerInfoSchema.setConsultNo(request.getParameter("ConsultNo"));
 
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLLAnswerInfoSchema);
  	tVData.add(tG);
    tOLLAnswerInfoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLAnswerInfoUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println(Content);
     String SerialNo ="";
  LLAnswerInfoSchema  rLLAnswerInfoSchema = null;
  if ( FlagStr.equals("Success"))
  {
  
    VData res = tOLLAnswerInfoUI.getResult();
    if ( res!=null &&  transact.equals("INSERT||MAIN"))
    {
    	System.out.println("res not null");
    	System.out.println( res.size() );
    	rLLAnswerInfoSchema=(LLAnswerInfoSchema)res.getObjectByObjectName("LLAnswerInfoSchema", 0);
       if (	rLLAnswerInfoSchema!=null ){
       System.out.println("rLLAnswerInfoSchema not null");
	    SerialNo= String.valueOf(rLLAnswerInfoSchema.getSerialNo());
	    }
    }
  }
  //添加各种预处理
%>                      

<html>
<script language="javascript">
  //parent.fraInterface.fm.SerialNo.value="<%=SerialNo%>";
	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	
</script>
</html>
