<html>
  <%
  //Name：LLIssueInput.jsp
  //Function：问题件
  //Date：2004-12-23 16:49:22
  //Author：wujs
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLIssueInput.js"></SCRIPT>
    <%@include file="LLIssueInit.jsp"%>
  </head>

  <body  onload="initForm();" >
    <form action="./LLIssueSave.jsp" method=post name=fm target="fraSubmit">
      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseInfo);">
          </TD>
          <TD class= titleImg>
            案件信息
          </TD>
        </TR>
      </table>
      <div id="divCaseInfo" style="display:''" align=center>
        <table  class= common>
          <TR  class= common8>
            <TD class= title8>案件号码</TD><TD class=input8><input class=common name="CaseNo"></TD>
            <TD class= title8>客户姓名</TD><TD class=input8><input class=common name="CustomerName"></TD>
            <TD class= title8>客户号码</TD><TD class=input8><input class=common name="CustomerNo"></TD>
          </TR>
        </table> 
      </div>
      <Table>
        <TR>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divIssueList);">
          </TD>
          <TD class= titleImg>
            问题件列表
          </TD>
        </TR>
      </Table>
      <Div  id= "divIssueList" style= "display: ''" align = center>
        <Table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanIssueGrid" ></span>
            </TD>
          </TR>
        </Table>
      </Div>
      <table class=common>
				<TR >
					<TD  class= title8></TD>
					<TD  class= input8 ><Input class= readonly readonly ></TD>
					<TD  class= input8 ><Input class= readonly readonly ></TD>
					<TD  class= input8 ><Input class= readonly readonly ></TD>
					<TD  class= title8></TD>
					<TD  class= input8 ><Input class= readonly readonly ></TD>
					</TD>
				</tr>
				<TR>
					<TD  class= title8>主  题</TD>
					<TD  class= input8 colspan=5><Input class= common name=Subject style=width:97% ></TD>
					</TD>
				</tr>
			</table>
			<table class= common>
        <TR  class= common>
          <TD  class= titleImg colspan="8">留言记录</TD>
        </TR>
        <TR  class= common>
          <TD  class= input colspan="8">
            <textarea name="Record" cols="100%" rows="8" witdh=25% class="common" readonly = "true"></textarea>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= titleImg >最新发言</TD>
					<TD  class= title> 返回状态</TD>
					<TD class=Input8><Input class="codeno"  CodeData="0|5^01|受理^02|账单录入^03|检录^04|理算^05|审批" name=BackState ondblclick="return showCodeListEx('BackState',[this,BackStateName],[0,1]);" onkeyup="return showCodeListKeyEx('BackState',[this,BackStateName],[0,1]);"  elementtype=nacessary verify="回退状态|notnull"><Input class="codename" name=BackStateName ></TD>
					<TD  class= title>指定处理人</TD>
					<TD class=Input8><Input class=codeno name="llusercode" onclick="return showCodeList('llissueop',[this,llusername],[0,1],null,fm.all('BackState').value,fm.all('Claimer').value,1);" onkeyup="return showCodeListKey('llissueop',[this,llusername],[0,1],null,'',Claimer,1);"><Input class="codename" name=llusername ></TD>
					<TD  class= title></TD>
					<TD  class= input8 ><Input class= readonly readonly ></TD>
        </TR>

        <TR  class= common>
          <TD  class= input colspan="8">
            <textarea name="Content" cols="100%" rows="4" witdh=25% class="common"></textarea>
          </TD>
        </TR>
      </table>
      <Div align="left">
        <input name="AskIn" style="display:''"  class=cssButton type=button value="提 问" onclick="Ask()">
        <input name="AskIn" style="display:''"  class=cssButton type=button value="回 复" onclick="Answer()">
      </div>


      <!--隐藏域-->
      <Input type="hidden" class= common name="fmtransact" >
      <Input type="hidden" class= common name="RgtState" >
      <Input type="hidden" class= common name="RgtNo" >
      <Input type="hidden" class= common name="Operator" >
      <Input type="hidden" class= common name="AskOrAnswer" >
      <Input type="hidden" class= common name="IssueNo" >
       <Input type="hidden" class= common name="Claimer" >
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>
