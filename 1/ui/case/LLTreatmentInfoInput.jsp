<html> 
<%
  //程序名称：LLTreatmentInfoInput.jsp
  //程序功能：诊疗项目信息
  //创建日期：2015-02-10 00:00:00
  //创建人  ：Liyunxia
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LLTreatmentInfoInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLTreatmentInfoInit.jsp"%>
  
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
 
<title>诊疗项目信息</title>   
</head>
<body onload="initForm(); initElementtype();" >
<form action="./LLTreatmentInfoSave.jsp" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<%@include file="../common/jsp/InputButton.jsp"%>
			<hr>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDHospital1);">
    		</td>
    		 <td class= titleImg>
        		 诊疗项目信息
       	 </td>   		 
    	</tr>
    </table>
<Div  id= "divLDHospital1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      诊疗项目编码
    </TD>
    <TD  class= input>
      <Input class=common name=TreatmentCode elementtype=nacessary  verify="诊疗项目编码|NOTNULL&len<=30">
    </TD>
    <TD  class= title>
      项目名称
    </TD>
    <TD  class= input>
      <Input class=code name=TreatmentName elementtype=nacessary  verify="项目名称|NOTNULL&len<=100 ">
    </TD>    
  	<TD  class= title>
      项目内涵
    </TD>
    <TD  class= input>
      <Input class=common name=TreatmentDetail >
    </TD>
  	
  </TR>
  <TR  class= common>
    <TD  class= title>
      说明
    </TD>
    <TD  class= input>
      <Input class=common name=TreatmentExplain >
    </TD> 
    <TD  class= title>
      类别
    </TD>
    <TD  class= input>
    	<Input class= "codeno"  name=TreatmentCategory  ondblclick="return showCodeList('treatment',[this,DisTreatmentCategory],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('treatment',[this,DisTreatmentCategory],[0,1],null,null,null,1);"  ><Input class=codename  name=DisTreatmentCategory ></TD>
    </TD>
	<TD  class= title>
      拼音简码
    </TD>
    <TD  class= input>
	  <Input class= 'code' name=Phonetic >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      打包编码
    </TD>
    <TD  class= input>
      <Input class=common name=PackagingCode >
    </TD> 
    <TD  class= title>
      组合描述
    </TD>
    <TD  class= input>
      <Input class=common name=ComDescription>
    </TD>
	<TD  class= title>
      计价单位
    </TD>
    <TD  class= input>
	  <Input class= 'common' name=ChargeUnit>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      单价
    </TD>
    <TD  class= input>
    	<input class= common name=UnitPrice onkeyup="checkNum(this)" />    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      备注
    </TD>
		<TD  class= input colspan="6">
		    <textarea name="Remark" cols="100%" rows="3"  >
		    </textarea>
		</TD> 
  </TR>     
  <TR  class= common>
    <TD  class= title>管理机构</TD><TD  class= input>
		<Input class= "codeno"  name=MngCom onkeydown="QueryOnKeyDown();" ondblclick="getstr();return showCodeList('comcode',[this,MngCom_ch], [0,1],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode', [this,MngCom_ch], [0,1],null,str,'1');" verify="管理机构|notnull"><Input class=codename  name=MngCom_ch elementtype=nacessary>

    <TD  class= title>地区名称</TD><TD  class= input>
		<Input class= "codeno"  name=AreaCode    ondblclick="return showCodeList('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);" onkeyup="return showCodeListKey('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);"   verify="地区名称|notnull&len<=20" ><Input class=codename  name=AreaName elementtype=nacessary></TD>
  </TR>
</table>
</Div>
<Div id=CurrentlyInfo style="display: ''">  
	 <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		 <td class= titleImg>
        		 诊疗项目信息列表（请至少录入‘诊疗项目编码’或‘项目名称’进行查询）
       	 </td>   		 
    	</tr>
    </table> 
	<Div id="comHospital1" style="display:'' ">
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanEvaluateGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
	</div>
</div>

   
    <input type=hidden id="fmtransact" name="Transact">
    <input type=hidden id="fmAction" name="fmAction">

    <input type=hidden name="SpecialClass">
    <input type=hidden name="Operator">
    <input type=hidden name="MakeDate">
    <input type=hidden name="MakeTime">
		<input type=hidden name="ModifyDate">
		<input type=hidden name="ModifyTime">
		<input type=hidden name="LoadFlag">
		<input type = hidden name = HiddenBtn>
		<Input type=hidden name=FlowNo ondblclick="generateFlowNo();">
</form>
<hr>
  <form action="./LLTreatmentInfoImport.jsp" method=post name=fmlode target="fraSubmit" ENCTYPE="multipart/form-data" >
  	<table class = common >    	
    <TR  >
      <TD  width='8%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <Input  type="file"　width="100%" name=FileName class= common>
        <INPUT  VALUE="诊疗项目信息导入" class="cssbutton" TYPE=button onclick = "TreatmentInfoUpload();" >
        <INPUT  VALUE="导入模板下载" class="cssbutton" TYPE=button onclick = "downLoad();" >
        <Input  type="hidden"　width="100%" name="insuredimport" value ="1">
      </TD>
    </TR>
    
	    <input type=hidden name=ImportFile>
	</table>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
