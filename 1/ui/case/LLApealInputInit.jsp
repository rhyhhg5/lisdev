<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">

function initInpBox( )
{
  try {
     fm.reset();

  } catch(ex) {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常aaaaaaaaa:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常yyyyyyy:初始化界面错误!");
  }
}

function initForm()
{
  try
  {

    initInpBox();
   // initCaseGrid();

  // initAffixGrid();
   // initBnfGrid();
   // initInsuredEventGrid();
    initAppReasonGrid();
  //  initApealReasonGrid();
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//立案附件信息
function initAffixGrid()
{
    var iArray = new Array();

      try
      {
      
      
          iArray[0]=new Array("序号","30px","0",0);
	      iArray[1]=new Array("材料代码","100px","0",2,"Relaction");
	      iArray[2]=new Array("附件名称","180px","0",0);
	      iArray[3]=new Array("有无标记","60px","0",2);
	
      AffixGrid = new MulLineEnter( "fm" , "AffixGrid" );
      //这些属性必须在loadMulLine前
      AffixGrid.mulLineCount = 1;
      AffixGrid.displayTitle = 1;
      AffixGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //AffixGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }

function initBnfGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("客户名称","100px","0",1);
   iArray[2]=new Array("与被保险人关系","100px","0",2,"Relaction");
   iArray[3]=new Array("领款方式","80px","0",2);
   iArray[4]=new Array("银行代码","80px","0",2);
   iArray[5]=new Array("账号","100px","0",1);
   iArray[6]=new Array("户名","100px","0",1);

    BnfGrid = new MulLineEnter("fm","BnfGrid");
    BnfGrid.mulLineCount = 1;
    BnfGrid.displayTitle = 1;
    BnfGrid.locked = 0;
    BnfGrid.canChk =1;
    BnfGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    BnfGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   BnfGrid. selBoxEventFuncName = "onSelSelected";
    BnfGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initInsuredEventGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("事件号","100px","0",0);
   iArray[2]=new Array("发生日期","100px","0",0);
    iArray[3]=new Array("发生地点","100px","0",0);
   iArray[4]=new Array("事件类型","100px","0",0);
   iArray[5]=new Array("主题","300px","0",0);
   

    InsuredEventGrid = new MulLineEnter("fm","InsuredEventGrid");
    InsuredEventGrid.mulLineCount = 1;
    InsuredEventGrid.displayTitle = 1;
    InsuredEventGrid.locked = 0;
    InsuredEventGrid.canChk =1;
    InsuredEventGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    InsuredEventGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   InsuredEventGrid. selBoxEventFuncName = "onSelSelected";
    InsuredEventGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initAppReasonGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("原因代码","100px","0",2);
    iArray[2]=new Array("申请原因","300px","0",0);
   

    AppReasonGrid = new MulLineEnter("fm","AppReasonGrid");
    AppReasonGrid.mulLineCount = 1;
    AppReasonGrid.displayTitle = 1;
    AppReasonGrid.canSel =1;
    AppReasonGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    AppReasonGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   AppReasonGrid. selBoxEventFuncName = "onSelSelected";
    AppReasonGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
function initApealReasonGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("原因代码","100px","0",2);
    iArray[2]=new Array("申请原因","300px","0",0);
   

    ApealReasonGrid= new MulLineEnter("fm","ApealReasonGrid");
    ApealReasonGridmulLineCount = 1;
    ApealReasonGrid.displayTitle = 1;
    ApealReasonGrid.locked = 0;
    ApealReasonGrid.canChk =0;
    ApealReasonGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ApealReasonGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   ApealReasonGrid. selBoxEventFuncName = "onSelSelected";
    ApealReasonGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
//立案分案信息
function initCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="分案号";    	//列名
    iArray[1][1]="0px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="事故者客户号";         			//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="事故者名称";         			//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=2;
    iArray[4][4]="Sex";
    //是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]= new Array();
    iArray[5][0]="证件类型";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=2;
    iArray[5][4]="IDType";

    iArray[6]= new Array();
    iArray[6][0]="证件号码";
    iArray[6][1]="150px";
    iArray[6][2]=100;
    iArray[6][3]=1;

    iArray[7]= new Array();
    iArray[7][0]="年龄";
    iArray[7][1]="40px";
    iArray[7][2]=100;
    iArray[7][3]=1;

    iArray[8]=new Array();
    iArray[8][0]="事故类型";
    iArray[8][1]="50px";
    iArray[8][2]=100;
    iArray[8][3]=0;

    iArray[9]=new Array();
    iArray[9][0]="分报案号码";
    iArray[9][1]="0px";
    iArray[9][2]=100;
    iArray[9][3]=0;

    CaseGrid = new MulLineEnter( "fm" , "CaseGrid" );
    //这些属性必须在loadMulLine前
    CaseGrid.mulLineCount = 0;
    CaseGrid.displayTitle = 1;
    CaseGrid.canChk =1;
    CaseGrid.hiddenPlus=1;
    CaseGrid.locked = 1;
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>