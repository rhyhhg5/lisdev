<%
//程序名称：LLBnfInputInit.jsp
//程序功能：
//创建日期：2005-2-26 12:07
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
String CaseNo=request.getParameter("CaseNo");
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
var turnPage=new turnPageClass();
function initInpBox()
{ 
  try
  {       
	  
  }
  catch(ex)
  {
    alert("在LDPersonQueryInit.jsp-->InitInpBox1函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LDPersonQueryInit.jsp-->InitSelBox2函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
	fm.CaseNo.value="<%=CaseNo%>";
  initInpBox(); 
  initCaseRelaGrid();
	CaseRelaQuery();  
  }
  catch(re)
  {
    alert("LDPersonQueryInit.jsp-->InitForm函数中发生异常3:初始化界面错误!"+re.message);
  }
}

// 保单信息列表的初始化
function initCaseRelaGrid()
  {                               
    var iArray = new Array();
        try
      {
      iArray[0]=new Array();
      iArray[0][0]="";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();  
	  	iArray[1][0]="关联号";//合同号码
	  	iArray[1][1]="60px";   
	  	iArray[1][2]=200;       
	  	iArray[1][3]=3;  
	  
	    iArray[2]=new Array();
	    iArray[2][0]="X"; 		//列名
	    iArray[2][1]="150px";		//列宽
	    iArray[2][2]=40;			//列最大值
	    iArray[2][3]=3;			//是否允许输入,1表示允许，0表示不允许

	  CaseRelaGrid = new MulLineEnter( "fm" , "CaseRelaGrid" ); 
      //这些属性必须在loadMulLine前
      CaseRelaGrid.mulLineCount = 3;   
      CaseRelaGrid.displayTitle = 1;
      CaseRelaGrid.locked = 0;     
      CaseRelaGrid.canSel = 1;
      CaseRelaGrid.canChk = 0;
      CaseRelaGrid.hiddenPlus=1;
      CaseRelaGrid.hiddenSubtraction=1;
	    CaseRelaGrid. selBoxEventFuncName = "PrintPage";
      CaseRelaGrid.loadMulLine(iArray);  
     
      }
      
      
      catch(ex)
      {
        alert(ex);
      }
}



</script>
