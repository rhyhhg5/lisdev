<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSurvFeeSumSave.jsp
//程序功能：查勘费汇总
//创建日期：2005-08-19 11:10:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

GlobalInput tG = new GlobalInput();

tG=(GlobalInput)session.getValue("GI");

if(tG == null) {
	out.println("session has expired");
	return;
}

String aYear = request.getParameter("Year");
String aMonth = request.getParameter("Month");
TransferData aTransferData = new TransferData();
aTransferData.setNameAndValue("Year",aYear);
aTransferData.setNameAndValue("Month",aMonth);
aTransferData.setNameAndValue("BatchNo",request.getParameter("BatchNo"));

LLInqFeeStaSet aLLInqFeeStaSet = new LLInqFeeStaSet();

String tSurveyNo[] = request.getParameterValues("SurveyCaseGrid1");
String tOtherNo[] = request.getParameterValues("SurveyCaseGrid1");
String tSurvFee[] = request.getParameterValues("SurveyCaseGrid2");
String tIndFee[] = request.getParameterValues("SurveyCaseGrid3");

String tNo[] = request.getParameterValues("SurveyCaseGridNo");
int n = 0;
if(tNo != null )
n = tNo.length ;
if(n>0)
{
	for(int i=0 ; i<n ; i++ )
	{
		LLInqFeeStaSchema tLLInqFeeStaSchema = new LLInqFeeStaSchema();
		tLLInqFeeStaSchema.setOtherNo(tOtherNo[i]);
		tLLInqFeeStaSchema.setInqFee(tSurvFee[i]);
		tLLInqFeeStaSchema.setIndirectFee(tIndFee[i]);
		aLLInqFeeStaSet.add(tLLInqFeeStaSchema);
	}
}

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.add(aLLInqFeeStaSet);
tVData.add(aTransferData);

LLInqGiveEnsureUI tLLInqGiveEnsureUI = new LLInqGiveEnsureUI();
try {
	System.out.println("this will save the data!!!");
	tLLInqGiveEnsureUI.submitData(tVData,"GIVE||ENSURE");
	} catch(Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail")) {
		tError = tLLInqGiveEnsureUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
		%>
		<html>
		<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
		</script>
		</html>
