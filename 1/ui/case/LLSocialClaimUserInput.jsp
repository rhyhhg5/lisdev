<html>
<%
	//Name:RegisterInput.jsp
	//Function：立案界面的初始化程序
	//Date：2002-07-21 17:44:28
	//Author ：LiuYansong
%>
<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput GI = new GlobalInput();
	GI = (GlobalInput) session.getValue("GI");
%>
<script>
var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
var ComCode = "<%=GI.ComCode%>";//记录登陆机构


</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LLSocialClaimUserInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LLSocialClaimUserInit.jsp"%>
<script language="javascript">
function initDate()
{
 fm.Station.value=manageCom;
}
</script>
</head>
<body onload="initForm();initDate();initElementtype();">
<form action="./LLSocialClaimUserSave.jsp" method=post name=fm target="fraSubmit">
<table class=common align=center>
	<tr align=right>
		<td class=button>&nbsp;&nbsp;</td>
		<td class=button width="10%" align=right><INPUT class=cssButton name="saveButton"   VALUE="保  存" TYPE=button onclick="return submitForm();"></td>
		<td class=button width="10%" align=right><INPUT class=cssButton name="modifyButton" VALUE="修  改" TYPE=button onclick="return updateClick();"></td>
		<td class=button width="10%" align=right><INPUT class=cssButton name="querybutton"  VALUE="查  询" TYPE=button onclick="return queryClick();"></td>
		<td class=button width="10%" align=right><INPUT class=cssButton name="deleteButton" VALUE="删  除" TYPE=button onclick="return deleteClick();"></td>
	</tr>
</table>
<table class=common>
	<tr class=titleImg>
		<TD class=titleImg>社保业务理赔用户维护信息</TD>
	</tr>
</table>
<Div id="divRegisterInfo" style="display: ''">
<table class=common>
	<TR>
		<TD class=title>用户机构代码</TD>
		<TD class=input><Input class="code" name=Station
			verify="机构代码|notnull&code:ComCode"
			ondblclick="return showCodeList('ComCode',[this, StationName], [0, 1]);"
			onkeyup="return showCodeListKey('ComCode', [this, StationName], [0, 1]);" elementtype=nacessary>
		</TD>
		<TD class=title>用户机构名称</TD>
		<TD class=input><Input class="readonly" readonly name=StationName ></TD>
	</TR>

	<TR class=common>
		<TD class=title>用户编码</TD>
		<TD class=input><Input class="code" name=UserCode
		    verify="用户编码|notnull"
			ondblclick="return showCodeList('UserCode',[this, UserName], [0, 1],null,fm.Station.value,'ComCode');"
			onkeyup="return showCodeListKey('UserCode', [this, UserName], [0, 1],null,fm.Station.value,'ComCode');" elementtype=nacessary>
		</TD>
		<TD class=title>用户名称</TD>
		<TD class=input><Input class=common name=UserName >
		</TD>
	</TR>

	<TR class=common>
		<TD class=title>社保业务结案金额</TD>
		<TD class=input><Input class=common name="SocialMoney"  verify="社保业务结案金额|notnull" elementtype=nacessary></TD>
		<TD class=title>上级用户代码</TD>
		<TD class=input><Input class="codeno" name=UpUserCode
			ondblclick="return getUserName(this,UpUserName);"
			onkeyup="return getUserName(this,UpUserName);"><Input
			class="codename" name=UpUserName ></TD>
	</TR>
	<!--以上隐藏-->
	<TR class=common style="display:'none'">
		<TD class=title>立案权限</TD>
		<TD class=input><Input class="code" value="1" name=RgtFlag
			CodeData="0|^0|无立案权限^1|有立案权限"
			ondblClick="showCodeListEx('RgtFlag_1',[this],[0,1]);"
			onkeyup="showCodeListKeyEx('RgtFlag_1',[this],[0,1]);" ></TD>


	</TR>
	<TR>
		<TD class=title>参加案件分配</TD>
		<TD class=input><Input class=codeno name=HandleFlag
		    verify="参加案件分配|notnull"
			CodeData="0|^0|否^1|是"
			ondblClick="showCodeListEx('HandleFlag_1',[this,HandleFlagName],[0,1]);"
			onkeyup="showCodeListKeyEx('HandleFlag_1',[this,HandleFlagName],[0,1]);"><Input
			class=codename name=HandleFlagName elementtype=nacessary></TD>
		<TD class=title>案件分配上限</TD>
		<TD class=input><Input class=common name="DispatchRate" onblur="checkNumber(DispatchRate);"></TD>
	</TR>
	
	<TR class=common>
		<TD class=title>调查权限</TD>
		<TD class=input><Input class="codeno" name=ServeyFlag
			CodeData="0|^0|无调查权限^1|调查主管^2|普通调查员"
			ondblClick="showCodeListEx('ServeyFlag_1',[this,ServeyFlagName],[0,1]);"
			onkeyup="showCodeListKeyEx('ServeyFlag_1',[this,ServeyFlagName],[0,1]);"><Input
			class=codename name=ServeyFlagName ></TD>
		<TD class=title>案件抽检比例</TD>
		<TD class=input><Input class=common name="ClaimSpotRate" onblur="checkNumber(ClaimSpotRate);"></TD>

	</TR>
	<TR>
		<TD class=title>案件抽检金额</TD>
		<TD class=input><Input class=common name="PrepaidLimit"></TD>

		<TD class=title>用户有效状态</TD>
		<TD class=input><Input class="codeno" name=StateFlag
			verify="用户有效状态|NOTNULL" 
			CodeData="0|^1|有效^2|休假^3|离职^4|离岗^9|其他无效情况"
			ondblClick="showCodeListEx('StateFlag_1',[this,StateFlagName],[0,1]);"
			onkeyup="showCodeListKeyEx('StateFlag_1',[this,StateFlagName],[0,1]);"><Input
			class=codename name=StateFlagName elementtype=nacessary></TD>

	</TR>
</table>
<table class=common>
	<tr class=titleImg>
		<TD class=titleImg>社保业务预付赔款维护信息</TD>
	</tr>
</table>
<table class=common>
					<TR>
						<TD  class= title>
							预付赔款审批权限
						</TD>
						<TD  class= input>
							<Input class="codeno" name=PrepaidFlag 
							CodeData="0|^0|否^1|是"
							ondblClick="showCodeListEx('PrepaidFlag',[this,PrepaidFlagName],[0,1]);"
							onkeyup="showCodeListKeyEx('PrepaidFlag',[this,PrepaidFlagName],[0,1]);"><Input class= "codename" name=PrepaidFlagName >
						</TD>
						<TD  class= title>
							预付赔款审批额度
						</TD>
						<TD  class= input>
							<Input class=common name="PrepaidMoney"  onblur="checkNumber(PrepaidMoney);">
						</TD>
					</TR>
					<TR>
						<TD  class= title>
							预付赔款审批上级用户
						</TD>
						<TD class = input>
						  <Input class= "codeno"  name=PrepaidUpUserCode ondblclick="return getUserName(this,PrepaidUpUserCodeName);" onkeyup="return getUserName(this,PrepaidUpUserCodeName);" ><Input class= "codename" name=PrepaidUpUserCodeName >
						</TD>

					</TR>
</table>


<table class=common>
	<TR class=common>
		<TD class=title>备注</TD>
	</TR>
	<TR class=common>
		<TD class=input><textarea name=Remark cols="110%" rows="3" witdh=25% class="common"></textarea></TD>
	</TR>
</table>
	<input type=hidden id="fmtransact" name="fmtransact">
</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
