<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLAccModifySave.jsp
//程序功能：
//创建日期：2005-02-5 08:49:52
//创建人  ：chenchuan
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
  LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
  CErrors tError = null;
  LLAccModifyUI tLLAccModifyUI = new LLAccModifyUI();
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  
  //输出参数
  String FlagStr = "";
  String Content = "";
  
  String grpflag = request.getParameter("grpflag");
  System.out.println("==== grpflag == " + grpflag);
  
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);
  
  
//动作为修改。
  if (strOperate.equals("MODIFY")) 
  {
  	
  	tLJAGetSchema.setActuGetNo(request.getParameter("ActuGetNo2"));
  	tLJAGetSchema.setOtherNo(request.getParameter("PolNo"));
  	tLJAGetSchema.setPayMode(request.getParameter("PayMode"));
  	tLJAGetSchema.setSumGetMoney(request.getParameter("GetMoney"));
  	tLJAGetSchema.setDrawer(request.getParameter("Drawer"));
  	tLJAGetSchema.setDrawerID(request.getParameter("DrawerID"));
  	tLJAGetSchema.setBankCode(request.getParameter("BankCode"));
  	tLJAGetSchema.setBankAccNo(request.getParameter("BankAccNo"));
  	tLJAGetSchema.setAccName(request.getParameter("AccName"));
  	tLJAGetSchema.setCanSendBank("M");
    
    if(grpflag.equals("MORE")) //  团体给付
	  {
	    tLLRegisterSchema.setRgtNo(request.getParameter("PolNo"));
	    tLLRegisterSchema.setCaseGetMode(request.getParameter("PayMode")); 
	    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
	    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
	    tLLRegisterSchema.setAccName(request.getParameter("AccName"));
	    
   	}
   	else     //个人给付
   	{
   	  tLLCaseSchema.setCaseNo(request.getParameter("PolNo"));
   	  tLLCaseSchema.setGetMode(request.getParameter("PayMode"));
   	  tLLCaseSchema.setCaseGetMode(request.getParameter("PayMode"));
   	  tLLCaseSchema.setBankCode(request.getParameter("BankCode"));
   	  tLLCaseSchema.setBankAccNo(request.getParameter("BankAccNo"));
   	  tLLCaseSchema.setAccName(request.getParameter("AccName"));
   	//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
      tLLCaseExtSchema.setDrawerIDType(request.getParameter("DrawerIDType"));
      tLLCaseExtSchema.setDrawerID(request.getParameter("DrawerID"));
      //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
   	}
	  
	   try
	   {
	   	 if(grpflag.equals("MORE")) //  团体给付
	   	 {
	   	   tVData.add(tLLRegisterSchema);	
	   	 } 		   
		   else 
		   {
		   	 tVData.add(tLLCaseSchema);
		   	 tVData.add(tLLCaseExtSchema);
		   }
	   	   	
		   tVData.add(tLJAGetSchema);		   
		   tVData.add(tG);		   
		   tLLAccModifyUI.submitData(tVData,strOperate);
	   }
	 catch(Exception ex)
	    {
	      Content = "保存失败，原因是:" + ex.toString();
	      System.out.println("aaaa"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tLLAccModifyUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tLLAccModifyUI.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getLastError();
		    	FlagStr = "Fail";
		    }
	     }	     
	    
  }
  		

//这个留着确认时候用。
  if (strOperate.equals("CONFIRM"))
  {
    tLJAGetSchema.setActuGetNo(request.getParameter("ActuGetNo2"));
  	tLJAGetSchema.setCanSendBank(request.getParameter("0"));
  	tLJAGetSchema.setSaleChnl(request.getParameter("PolNo"));
    if(grpflag.equals("MORE")) //  团体给付
	  {
  	 tLJAGetSchema.setSerialNo("Tuan");
  	}
    else
    {
     tLJAGetSchema.setSerialNo("Ge");
    }
     	 
	   try
	   {
		   tVData.add(tLJAGetSchema);		   
		   tVData.add(tG);		   
		   tLLAccModifyUI.submitData(tVData,strOperate);
	   }
	 catch(Exception ex)
	    {
	      Content = "保存失败，原因是:" + ex.toString();
	      System.out.println("aaaa"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tLLAccModifyUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tLLAccModifyUI.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
	     }	      
  }
  %>	

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
   
   
   
 