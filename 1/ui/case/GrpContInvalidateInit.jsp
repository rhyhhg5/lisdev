<%
//Name:ReportInit.jsp
//function：
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">

function initForm()
{

  try
  {
   
    
    initLLAppClaimReasonGrid();
    fm.ManageCom.value = "<%=tGI.ManageCom%>";
    SearchLLAppClaimReason();
    
  }
  catch(re)
  {
    alter("在GrpContInvaliedate.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initLLAppClaimReasonGrid()
{

   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("团体保单号","120px","0",0);
    iArray[2]=new Array("保单机构代码","80px","0",0);
    iArray[3]=new Array("操作人","80px","0",0);
    iArray[4]=new Array("操作日期","80px","0",0);
    iArray[5]=new Array("操作时间","80px","0",0);
    iArray[6]=new Array("修改日期","80px","0",0);
    iArray[7]=new Array("修改时间","80px","0",0);
   
    
    LLAppClaimReasonGrid = new MulLineEnter( "fm" , "LLAppClaimReasonGrid" );
    LLAppClaimReasonGrid.mulLineCount =7;
    LLAppClaimReasonGrid.displayTitle = 1;
    LLAppClaimReasonGrid.locked = 1;
    LLAppClaimReasonGrid.canChk =1;
    LLAppClaimReasonGrid.hiddenPlus=1;  
    LLAppClaimReasonGrid.hiddenSubtraction=1; 
    LLAppClaimReasonGrid.loadMulLine(iArray);
  } 
  catch(ex)
  {
    alert(ex);
  }    
}


 </script>