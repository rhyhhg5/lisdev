var turnPage = new turnPageClass();  

//查询功能
function dealInfo(i){

	if(i == 0){//查询
		if(!verifyInput()) {
  			return false;
  		}
  		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");		
  		fm.operate.value='QUERY||MAIN';
	}else if(i == 1){
		if (!beforeSubmit()){		
			return false;
		}
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");		
		fm.operate.value='INSERT||MAIN';
	}
  
  	fm.submit();	
}

function queryResult(k){
	 var partSql="";
	 if(k == 0){
	 	partSql=" and SettlementState not in('01','02') ";
	 	fm.queryMode.value = "0";
	 }else if(k == 1){
	 	partSql=" and SettlementState in('01','02') ";
	 	fm.queryMode.value = "1";
	 }
	 var strSql="select surveyCaseNo,settlementNo,paymentVoucherNo,InsuranceComName,RemoteSurveyComName," +
    			"totalAmount,surveyStartDate,surveyEndDate,createDate,name,medicareCode," +
    			"idNo,InsuranceComCode,RemoteSurveyComCode,actugetno,'' " +
    			"from SocialSecurityFeeClaim o "+
  		" where 1=1 " 
  		+ getWherePart("o.surveyEndDate","RgtDateS",">=")
  		+ getWherePart("o.surveyEndDate","RgtDateE","<=")
  		+ getWherePart("o.InsuranceComCode","cbdUnitIdT","like")
  		+ getWherePart("o.RemoteSurveyComCode","ydUnitIdT","like")
  		+ getWherePart("o.name","nameT","like")
  		+ getWherePart("o.medicareCode","medicareCodeT","like")
  		+ getWherePart("o.idNo","idNoT","like")
  		+ partSql
  		+"order by settlementNo";
  	turnPage.queryModal(strSql,ContGrid);
  	var arr = easyExecSql(strSql);		   
  	if (arr==null) {
	 	 alert("没有查询到满足条件的调查费信息，请核实查询条件");
	 	 return;
	}
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  if(!verifyInput()) {
  	return false;
  }
  
  if(!check()){
  	return false;  	
  } 
  return true;
}          
//校验功能 
function check(){
	
	//校验结算信息，必须选择一条记录
	var chkFlag=false;
   	for (var i=0;i<ContGrid.mulLineCount;i++){
       if(ContGrid.getSelNo()!='0'){
       		var n = ContGrid.getSelNo();//返回被选中的行的行号，从1开始
            chkFlag=true;
            //判断“成本中心“字段
            var tCostCenter=ContGrid.getRowColData(n-1,16);//成本中心
            var tcbdUnitId=ContGrid.getRowColData(n-1,13);//参保地机构
            if(tCostCenter != null && tCostCenter != ""){
            	//var paySql ="Select 1 from labranchgroup where managecom like '"+tcbdUnitId+"%' and costcenter = '"+tCostCenter+"' with ur";		  			
		  		//var arr = easyExecSql(paySql);
		  		//if(!arr)
		  		//{
		  			//alert("成本中心录入错误，请核实!");	
		  			//return false;		
		  		//}	
		  		if(tcbdUnitId.substring(2,6) != tCostCenter.substring(0,4)){
		  			alert("录入参保地机构"+tcbdUnitId+"的中间四位代码"+tcbdUnitId.substring(2,6)+"和成本中心的"+tCostCenter+"前四位代码"+tCostCenter.substring(0,4)+"不一致，请保证一致！");
		  			return false;
		  		}
		  		if(tCostCenter.length != 10){
		  			alert("请核对录入的成本中心是否满足规则！");
		  			return false;
		  		}
		  				  			  
            }else{
            	alert("请录入成本中心!");
            	return false;
            }
       }
  	}
   	if (chkFlag==false){
       alert("请选中待处理记录！");
       return false;
   	}
	//如果处于已结算查询下，不许继续点击结算
	if(fm.queryMode.value == "1"){
		alert("此案件已经结算，不可重复操作");
		return false;
	}	
	return true;
}

function afterSubmit(FlagStr, content)
{
	
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    	queryResult(0);
    }
    else if (FlagStr == "SuccessQuery"){
    	//var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		//showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    	queryResult(0);
    
    }else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		queryResult(0);
		top.opener.focus();
		top.opener.location.reload();
		top.close();
    }
}