<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：LLDealApplicationExplain.jsp
 //程序功能：理赔机构受理申请规则配置
 //创建日期：2012-3-22
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>


<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="LLDealApplicationExplain.js"></SCRIPT>
  <%@include file="LLDealApplicationExplainInit.jsp"%>
</head>
<%
    GlobalInput GI = new GlobalInput();
    GI = (GlobalInput)session.getValue("GI");
  %>
<script>
   function initDate(){
	    	fm.opcomcode.value="<%=GI.ManageCom%>";
	    	fm.Operator.value="<%=GI.Operator%>";
  }
</script>
<body  onload="initForm();initDate();">
<form action="./LLDealApplicationExplainSave.jsp" method=post name=fm target="fraSubmit">

 
<div id="divQuer" align=left>
<table  class="common" >
  <tr >
    <td class="title">管理机构</td>
    <td class="input"><Input class="codeno" name="ManageCom" verify="管理机构|code:comcode4&NOTNULL&INT" 
    ondblclick="return showCodeList('comcode4',[this,ComName],[0,1],null,null,null,1);" 
    onkeyup="return showCodeListKey('comcode4',[this,ComName],[0,1],null,null,null,1);" readonly="true" ><input class="codename" name="ComName" elementtype="nacessary"></td> 
  </tr>
</table>
</div>
<table class=common>
	<tr class= titleImg>
		<TD class= titleImg > 理赔机构受理申请规则配置 </TD>
	</tr>
</table>
 <div  id= "divCaseDis" align=center style= "display: ''">
            <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanDealAppcationGrid" >
                </span>
              </TD>
            </TR>
          </table>
</div>
<div align=right>

<input class=cssButton name="serchButton" VALUE="查  询"  TYPE=button onclick="return serchClick();">
<input class=cssButton name="saveButton"   VALUE="保  存"  TYPE=button onclick="return submitForm();">
<input class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
			 					

</div>
 
<input type=hidden name="Operator">
<input type=hidden name="fmtransact">
<input type=hidden name="transact">
<input type=hidden name="opcomcode">

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

