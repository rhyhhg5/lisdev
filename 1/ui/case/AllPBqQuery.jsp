<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%

	String tPolNo = "";
	try
	{
		tPolNo = request.getParameter("PolNo");
	}
	catch( Exception e )
	{
		tPolNo = "";
	}
	String tflag = "";
		try
	{
		tflag = request.getParameter("flag");

	}
	catch( Exception e )
	{
		tflag = "";
	}
%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>   
<Script>
var tflag = "<%=tflag%>"
var tPolNo = "<%=tPolNo%>"
var comCode = <%=tG.ComCode%>
</Script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="AllPBqQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AllPBqQueryInit.jsp"%>
  <title>合同处理查询 </title>
</head>
<body  onload="initForm();">
  <form action="./AllPBqQuerySubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入合同处理查询条件：
			</td>
		</tr>
	</table>
	
    <table  class= common align=center>
        <TR>
  		    <TD  class= title> 管理机构 </TD>
          <TD  class= input>
            <Input class= "codeno" name=ManageCom value="<%=tG.ComCode%>" verify="管理机构|notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly ><Input class=codename style="width:80" readonly name=ManageComName>
          </TD>
  		    <TD  class= title>
              申请日期
          </TD>
          <TD  class= input>
              <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >
          </TD>
          <TD  class= title>
              合同处理类别
          </TD>
          <TD  class= input>
              <Input class= "codeNo" name=EdorType CodeData="0|3^CT|解约^HZ|理赔合同终止^RB|解约回退" ondblclick="return showCodeListEx('EdorType',[this,EdorTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('EdorType',[this,EdorTypeName],[0,1]);" ><Input class= "codeName" name=EdorTypeName readonly >
          </TD>
        </TR>
      	<TR  class= common>
            <td class=title>
            案件号
            </td>
            <td class= input>
                <Input type="input" class="common" name=CaseNo>
            </td>
            <td class=title>
                   合同处理号
            </td>
            <td class= input>
                <Input class="common"  name=EdorNo>
            </td>
            <td class=title>
                   客户号
            </td>
            <td class= input>
                <Input class="common"  name=InsuredNo>
            </td>

        </TR>
        
    </table>

          <INPUT VALUE="查询" class=cssButton TYPE=button onclick="easyQueryClick();"> 

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 合同处理信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table class= common align=center>
    		<tr class= common>
    			<td  class= input>
    				<INPUT class=cssButton VALUE="打  印" TYPE=button onclick="PrtEdor();">     			
    			</td>
    		
    		</tr>
    	</table>
    	<input type=hidden name=Transact >
    	<Div id= "divPage2" align="center" style= "display: '' ">
        <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage(); "> 
        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage(); "> 					
        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage(); "> 
        <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage(); ">				
  	  </div>
  	</div>
  	 <input type=hidden id ="RiskCode" name = "RiskCode">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
