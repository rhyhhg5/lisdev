<html>
  <%
  //Name：LLMainAskInput.jsp
  //Function：登记界面的初始化
  //Date：2004-12-23 16:49:22
  //Author：wujs
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@page import = "com.sinosoft.lis.schema.*"%>
  <%@page import = "com.sinosoft.lis.vschema.*"%>
  <%@page import = "com.sinosoft.lis.llcase.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLMCaseSpotTraceList.js"></SCRIPT>
    <%@include file="LLMCaseSpotTraceListInit.jsp"%>
  </head>

  <body  onload="initForm();" >
    <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput);">
          </TD>
          <TD class= titleImg>
            案件抽检
          </TD>
        </TR>
      </table>

      <Div  id= "divLLLLMainAskInput" style= "display: ''">
        <table  class= common>
          <TR  class= common8>
            <TD  class= title8>被抽检机构</TD>
            <TD  class= input8>
              <Input class="code" name=SpotMngCom verify="机构代码|notnull&code:Station" ondblclick="return showCodeList('Station',[this], [0]);" onkeyup="return showCodeListKey('Station', [this], [0]);">
            </TD>
            <TD  class= title8>被抽检人级别</TD>
            <TD  class= input8>
              <Input class="code" name=ClaimPopedom ondblClick="showCodeList('llclaimpopedom',[this]);"  onkeyup="showCodeListKey('llclaimpopedom',[this]);">
            </TD>
            <TD  class= title8>抽检比例</TD><TD  class= input8><input class= common name="SpotRate"></TD>
          </TR>
          <TR  class= common8>
            <TD  class= title8>抽检日期从</TD><TD  class= input8><input class= 'coolDatePicker' dateFormate="Short" name="SpotDate" ></TD>
            <TD  class= title8>到</TD><TD  class= input8><input class= 'coolDatePicker' dateFormate="Short" name="SpotDate"></TD>
          </TR>
        </table>
        <input name="AskIn" style="display:''"  class=cssButton type=button value="抽 检" onclick="SurveyReply()">
      </DIV>
      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
          </TD>
          <TD class= titleImg>
            案件抽检轨迹列表
          </TD>
        </TR>
      </table>

      <Div  id= "divLLLLMainAskInput1" style= "display: 'none'">
        <table  class= common>
          <TR  class= common8>
            <TD  class= title8>案件号</TD><TD  class= input8><input class= common name="CaseNo" ></TD>
            <TD  class= title8>抽检机构</TD><TD  class= input8><input class= common name="SpotMngCom"></TD>
            <TD  class= title8>抽检人</TD><TD  class= input8><input class= common name="Spoter"></TD>
          </TR>
        </table>
      </DIV>
      <Div  id= "divCustomerInfo" align=center style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanSpotTraceGrid" >
              </span>
            </TD>
          </TR>
        </table>
        <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
        <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
      </Div>
      <input name="Query" style="display:''"  class=cssButton type=button value="查 询" onclick="QueryClick()">

      <!--隐藏域-->
      <Input type="hidden" class= common name="fmtransact" >
      <Input type="hidden" class= common name="tSurveyNo" >

    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>
