<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  LLCasePolicySet tLLCasePolicySet=new LLCasePolicySet();
  CasePolicyUI tCasePolicyUI   = new CasePolicyUI();

  CErrors tError = null;
  String transact = "INSERT";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
	//读取立案明细信息

  String strCaseNo = request.getParameter("CaseNo");
  String strRgtNo  = request.getParameter("RgtNo");
  String strCustomerNo = request.getParameter("CustomerNo");
  String CaseRelaNo = request.getParameter("CaseRelaNo");

  String[] tChk = request.getParameterValues("InpCasePolicyGridChk");
  String[] strPolNo=request.getParameterValues("CasePolicyGrid6");

  if ( tChk!=null )
  {
  int intLength=tChk.length;
  for(int i=0;i<intLength;i++)
  {
    if(tChk[i].equals("0")) //未选
      continue;

    LLCasePolicySchema tLLCasePolicySchema=new LLCasePolicySchema();
    tLLCasePolicySchema.setCaseNo(strCaseNo);
    tLLCasePolicySchema.setRgtNo(strRgtNo);
    tLLCasePolicySchema.setPolNo(strPolNo[i]);
    tLLCasePolicySchema.setCaseRelaNo(CaseRelaNo);
    tLLCasePolicySet.add(tLLCasePolicySchema);
  }
 

    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");

    try
    {
      VData tVData = new VData();
      tVData.addElement(strCustomerNo);
      tVData.addElement(strCaseNo);
      tVData.addElement(tLLCasePolicySet);
      tVData.addElement(tG);
      tCasePolicyUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = transact+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
   }

    if (FlagStr=="")
    {
      tError = tCasePolicyUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 保存成功";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 保存失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>