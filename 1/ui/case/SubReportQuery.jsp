<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="SubReportQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="SubReportQueryInit.jsp"%>
  <title>分报案查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./SubReportQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 报案信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLLReport1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            分报案号
          </TD>
          <TD  class= input>
            <Input class= common name=SubRptNo >
          </TD>          
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            报案号
          </TD>
          <TD  class= input>
            <Input class= common name=RptNo >
          </TD>          
        </TR>

      </table>
    </Div>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport2);">
    		</td>
    		<td class= titleImg>
    			 分报案信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanSubReportGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button class=common onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button class=common onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class=common onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" TYPE=button class=common onclick="getLastPage();"> 					
  				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
