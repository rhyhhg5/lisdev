<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";

//获取页面数据

String tsOperate = request.getParameter("cOperate");
String tsOperateCN = "";
if (tsOperate.equals("INSERT"))
tsOperateCN = "保存";
if (tsOperate.equals("UPDATE"))
tsOperateCN = "修改";
if (tsOperate.equals("DELETE"))
tsOperateCN = "删除";

//初始化
LLFeeMainSchema mFeeMainSchema = new LLFeeMainSchema();
LLSecurityReceiptSchema mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
LLCaseReceiptSet mReceiptSet = new LLCaseReceiptSet();
LLCaseReceiptSchema mReceiptSchema = null;
LLFeeOtherItemSet mFeeOtherItemSet = new LLFeeOtherItemSet();
LLFeeOtherItemSchema mFeeOtherItemSchema = null;

//账单主信息
String tsMainFeeNo = request.getParameter("MainFeeNo");
String tsCustomerName = StrTool.unicodeToGBK(request.getParameter("CustomerName"));
String tsFeeAtti = request.getParameter("FeeAtti");
String tsFirstInHos = request.getParameter("FirstInHos");
String tsMngCom = request.getParameter("MngCom");
mFeeMainSchema.setMainFeeNo(tsMainFeeNo);
mFeeMainSchema.setCaseNo(request.getParameter("CaseNo"));
mFeeMainSchema.setRgtNo(request.getParameter("RgtNo"));
mFeeMainSchema.setCustomerNo(request.getParameter("CustomerNo"));
mFeeMainSchema.setCustomerName(tsCustomerName);
mFeeMainSchema.setCustomerSex(request.getParameter("CustomerSex"));
mFeeMainSchema.setHospitalCode(request.getParameter("HospitalCode"));
mFeeMainSchema.setHospitalName(request.getParameter("HospitalName"));
mFeeMainSchema.setReceiptNo(request.getParameter("ReceiptNo"));
mFeeMainSchema.setFeeType(request.getParameter("FeeType"));
mFeeMainSchema.setFeeAtti(tsFeeAtti);
mFeeMainSchema.setFeeAffixType(request.getParameter("FeeAffixType"));
mFeeMainSchema.setFeeDate(request.getParameter("FeeDate"));
mFeeMainSchema.setHospStartDate(request.getParameter("HospStartDate"));
mFeeMainSchema.setHospEndDate(request.getParameter("HospEndDate"));
mFeeMainSchema.setRealHospDate(request.getParameter("RealHospDate"));
mFeeMainSchema.setSeriousWard(request.getParameter("SeriousWard"));
mFeeMainSchema.setInHosNo(request.getParameter("inpatientNo"));
mFeeMainSchema.setAge(request.getParameter("Age"));
mFeeMainSchema.setCompSecuNo(request.getParameter("CompSecuNo"));
mFeeMainSchema.setSecurityNo(request.getParameter("SecurityNo"));
mFeeMainSchema.setInsuredStat(request.getParameter("InsuredStat"));
mFeeMainSchema.setHosGrade(request.getParameter("HosGrade"));
mFeeMainSchema.setHosDistrict(request.getParameter("UrbanFlag"));
mFeeMainSchema.setMngCom(request.getParameter("MngCom"));
mFeeMainSchema.setReceiptType(request.getParameter("ReceiptType"));
mFeeMainSchema.setIssueUnit(request.getParameter("IssueUnit"));
mFeeMainSchema.setMedicareType(request.getParameter("MedicareType"));   // # 3500
//分案收据明细
if (tsFeeAtti.equals("0")||tsFeeAtti.equals("5"))
{
  String tsFeeItemCode[] = request.getParameterValues("IFeeCaseCureGrid1");	//费用项目代码
  String tsFeeItemName[] = request.getParameterValues("IFeeCaseCureGrid2");	//费用项目名称
  String tsFee[] = request.getParameterValues("IFeeCaseCureGrid3");	        //费用金额
  String tsSelfAmnt[]= request.getParameterValues("IFeeCaseCureGrid4");			//自费金额
  String tsPreAmnt[]=request.getParameterValues("IFeeCaseCureGrid5");			  //先行给付金额
  String tsRefuseAmnt[]=request.getParameterValues("IFeeCaseCureGrid6");		//不合理费用
  int feeItemCount = tsFeeItemCode.length;
  for(int i=0;i<tsFeeItemCode.length;i++){
  System.out.println("++++++++++" +tsFeeItemCode[i]);
  }
  System.out.println("============"+feeItemCount);
  for (int i = 0; i < feeItemCount; i++)
  {
    if (tsFeeItemCode[i] != null && !tsFeeItemCode[i].equals("") &&
    tsFee[i] != null && !tsFee[i].equals(""))
    {
      mReceiptSchema = new LLCaseReceiptSchema();
      mReceiptSchema.setFeeItemCode(tsFeeItemCode[i]);
      mReceiptSchema.setFeeItemName(tsFeeItemName[i]);
      mReceiptSchema.setFee(tsFee[i]);
      mReceiptSchema.setSelfAmnt(tsSelfAmnt[i]);
      mReceiptSchema.setPreAmnt(tsPreAmnt[i]);
      mReceiptSchema.setRefuseAmnt(tsRefuseAmnt[i]);
      mReceiptSchema.setAvaliFlag("0");
      mReceiptSet.add(mReceiptSchema);
    }
  }
}

if ((tsFeeAtti.equals("1")&&(!tsMngCom.equals("8694")||!tsMngCom.equals("8637")))||tsFeeAtti.equals("4"))
{
  mLLSecurityReceiptSchema.setRgtNo(request.getParameter("RgtNo"));
  mLLSecurityReceiptSchema.setCaseNo(request.getParameter("CaseNo"));
  String tsFeeItemName1[] = request.getParameterValues("FeeItemGrid1");	//费用项目名称
  String tsFeeItemCode1[] = request.getParameterValues("FeeItemGrid2");	//费用项目代码
  String tsFee1[] = request.getParameterValues("FeeItemGrid3");	        //费用金额
  String tsFeeItemName2[] = request.getParameterValues("FeeItemGrid4");	//费用项目名称
  String tsFeeItemCode2[] = request.getParameterValues("FeeItemGrid5");	//费用项目代码
  String tsFee2[] = request.getParameterValues("FeeItemGrid6");	        //费用金额
  String tsFeeItemName3[] = request.getParameterValues("FeeItemGrid7");	//费用项目名称
  String tsFeeItemCode3[] = request.getParameterValues("FeeItemGrid8");	//费用项目代码
  String tsFee3[] = request.getParameterValues("FeeItemGrid9");	        //费用金额
  String tsFeeItemName4[] = request.getParameterValues("FeeItemGrid10");	//费用项目名称
  String tsFeeItemCode4[] = request.getParameterValues("FeeItemGrid11");	//费用项目代码
  String tsFee4[] = request.getParameterValues("FeeItemGrid12");	        //费用金额
  int feeItemCount = tsFeeItemCode1.length;
  for (int i = 0; i < feeItemCount; i++)
  {
    if (tsFee1[i] != null && !tsFee1[i].equals(""))
    {
      mReceiptSchema = new LLCaseReceiptSchema();
      mReceiptSchema.setFeeItemCode(tsFeeItemCode1[i]);
      mReceiptSchema.setFeeItemName(tsFeeItemName1[i]);
      mReceiptSchema.setFee(tsFee1[i]);
      mReceiptSchema.setAvaliFlag("0");
      mReceiptSet.add(mReceiptSchema);
    }
    if (tsFee2[i] != null && !tsFee2[i].equals(""))
    {
      mReceiptSchema = new LLCaseReceiptSchema();
      mReceiptSchema.setFeeItemCode(tsFeeItemCode2[i]);
      mReceiptSchema.setFeeItemName(tsFeeItemName2[i]);
      mReceiptSchema.setFee(tsFee2[i]);
      mReceiptSchema.setAvaliFlag("0");
      mReceiptSet.add(mReceiptSchema);
    }
    if (tsFee3[i] != null && !tsFee3[i].equals(""))
    {
      mReceiptSchema = new LLCaseReceiptSchema();
      mReceiptSchema.setFeeItemCode(tsFeeItemCode3[i]);
      mReceiptSchema.setFeeItemName(tsFeeItemName3[i]);
      mReceiptSchema.setFee(tsFee3[i]);
      mReceiptSchema.setAvaliFlag("0");
      mReceiptSet.add(mReceiptSchema);
    }
    if (tsFee4[i] != null && !tsFee4[i].equals(""))
    {
      mReceiptSchema = new LLCaseReceiptSchema();
      mReceiptSchema.setFeeItemCode(tsFeeItemCode4[i]);
      mReceiptSchema.setFeeItemName(tsFeeItemName4[i]);
      mReceiptSchema.setFee(tsFee4[i]);
      mReceiptSchema.setAvaliFlag("0");
      mReceiptSet.add(mReceiptSchema);
    }
  }
  String tsSecuFeeName1[] = request.getParameterValues("SecuItemGrid1");	//费用项目名称
  String tsSecuFeeCode1[] = request.getParameterValues("SecuItemGrid2");	//费用项目代码
  String tsSecuFeeReason1[] = request.getParameterValues("SecuItemGrid3");	        //费用金额
  String tsSecuFee1[] = request.getParameterValues("SecuItemGrid4");	        //费用金额
  String tsSecuFeeName2[] = request.getParameterValues("SecuItemGrid5");	//费用项目名称
  String tsSecuFeeCode2[] = request.getParameterValues("SecuItemGrid6");	//费用项目代码
  String tsSecuFeeReason2[] = request.getParameterValues("SecuItemGrid7");	        //费用金额
  String tsSecuFee2[] = request.getParameterValues("SecuItemGrid8");	        //费用金额
  String tsSecuFeeName3[] = request.getParameterValues("SecuItemGrid9");	//费用项目名称
  String tsSecuFeeCode3[] = request.getParameterValues("SecuItemGrid10");	//费用项目代码
  String tsSecuFeeReason3[] = request.getParameterValues("SecuItemGrid11");	        //费用金额
  String tsSecuFee3[] = request.getParameterValues("SecuItemGrid12");	        //费用金额
  String tsSecuFeeName4[] = request.getParameterValues("SecuItemGrid13");	//费用项目名称
  String tsSecuFeeCode4[] = request.getParameterValues("SecuItemGrid14");	//费用项目代码
  String tsSecuFeeReason4[] = request.getParameterValues("SecuItemGrid15");	        //费用金额
  String tsSecuFee4[] = request.getParameterValues("SecuItemGrid16");	        //费用金额
  int SecuFeeCount = tsSecuFeeCode1.length;
  for (int i = 0; i < SecuFeeCount; i++)
  {
    if (tsSecuFee1[i] != null && !tsSecuFee1[i].equals(""))
    {
      mFeeOtherItemSchema = new LLFeeOtherItemSchema();
      mFeeOtherItemSchema.setItemCode(tsSecuFeeCode1[i]);
      mFeeOtherItemSchema.setDrugName(tsSecuFeeName1[i]);
      mFeeOtherItemSchema.setAvliReason(tsSecuFeeReason1[i]);
      mFeeOtherItemSchema.setFeeMoney(tsSecuFee1[i]);
      mFeeOtherItemSchema.setAvliFlag("0");
      mFeeOtherItemSet.add(mFeeOtherItemSchema);
    }
    if (tsSecuFee2[i] != null && !tsSecuFee2[i].equals(""))
    {
      mFeeOtherItemSchema = new LLFeeOtherItemSchema();
      mFeeOtherItemSchema.setItemCode(tsSecuFeeCode2[i]);
      mFeeOtherItemSchema.setDrugName(tsSecuFeeName2[i]);
      mFeeOtherItemSchema.setAvliReason(tsSecuFeeReason2[i]);
      mFeeOtherItemSchema.setFeeMoney(tsSecuFee2[i]);
      mFeeOtherItemSchema.setAvliFlag("0");
      mFeeOtherItemSet.add(mFeeOtherItemSchema);
    }
    if (tsSecuFee3[i] != null && !tsSecuFee3[i].equals(""))
    {
      mFeeOtherItemSchema = new LLFeeOtherItemSchema();
      mFeeOtherItemSchema.setItemCode(tsSecuFeeCode3[i]);
      mFeeOtherItemSchema.setDrugName(tsSecuFeeName3[i]);
      mFeeOtherItemSchema.setAvliReason(tsSecuFeeReason3[i]);
      mFeeOtherItemSchema.setFeeMoney(tsSecuFee3[i]);
      mFeeOtherItemSchema.setAvliFlag("0");
      mFeeOtherItemSet.add(mFeeOtherItemSchema);
    }
    if (tsSecuFee4[i] != null && !tsSecuFee4[i].equals(""))
    {
      mFeeOtherItemSchema = new LLFeeOtherItemSchema();
      mFeeOtherItemSchema.setItemCode(tsSecuFeeCode4[i]);
      mFeeOtherItemSchema.setDrugName(tsSecuFeeName4[i]);
      mFeeOtherItemSchema.setAvliReason(tsSecuFeeReason4[i]);
      mFeeOtherItemSchema.setFeeMoney(tsSecuFee4[i]);
      mFeeOtherItemSchema.setAvliFlag("0");
      mFeeOtherItemSet.add(mFeeOtherItemSchema);
    }
  }
}
if (tsFeeAtti.equals("1")&&(tsMngCom.equals("8694")||tsMngCom.equals("8637")))
{
  String tFeeItemCode[] = request.getParameterValues("QDSecuGrid1");	//费用项目代码
  String tFeeItemName[] = request.getParameterValues("QDSecuGrid2");	//费用项目名称
  String tWPlanFee[] = request.getParameterValues("QDSecuGrid3");	    //全额统筹
  String tPPlanFee[]=request.getParameterValues("QDSecuGrid4");			  //部分统筹
  String tSelfPay2[]=request.getParameterValues("QDSecuGrid5");		    //不合理费用
  String tSelfAmnt[]= request.getParameterValues("QDSecuGrid6");			//自费金额
  String tFee[]=request.getParameterValues("QDSecuGrid7");		        //不合理费用

  String tPSI[] = request.getParameterValues("SecuSpanGrid4");
  String tSelfPay1[] = request.getParameterValues("SecuSpanGrid5");
  String tSpanName[] = request.getParameterValues("SecuSpanGrid7");
  int spanNo = tSpanName.length;
  String tsSelfPay1 = "0";
  String tsSuperAmnt = "0";
  String tsPlanFee = "0";
  for(int j=0;j<spanNo;j++){
    if(tSpanName[j].equals("88"))
    tsSuperAmnt = tSelfPay1[j];
    if(tSpanName[j].equals("99")){
      tsSelfPay1 = tSelfPay1[j];
      tsPlanFee = tPSI[j];
    }
  }


  for (int i = 0; i < 8; i++)
  {
    mReceiptSchema = new LLCaseReceiptSchema();
    mReceiptSchema.setFeeItemCode(tFeeItemCode[i]);
    mReceiptSchema.setFeeItemName(tFeeItemName[i]);
    mReceiptSchema.setFee(tFee[i]);
    mReceiptSchema.setWholePlanFee(tWPlanFee[i]);
    mReceiptSchema.setPartPlanFee(tPPlanFee[i]);
    mReceiptSchema.setSelfPay2(tSelfPay2[i]);
    mReceiptSchema.setSelfFee(tSelfAmnt[i]);
    mReceiptSet.add(mReceiptSchema);
  }
  mLLSecurityReceiptSchema.setRgtNo(request.getParameter("RgtNo"));
  mLLSecurityReceiptSchema.setCaseNo(request.getParameter("CaseNo"));
  mLLSecurityReceiptSchema.setSuperAmnt(tsSuperAmnt);
  mLLSecurityReceiptSchema.setSelfPay1(tsSelfPay1);
  mLLSecurityReceiptSchema.setPlanFee(tsPlanFee);
}

//特需帐单明细
if (tsFeeAtti.equals("3"))
{
  String tssumFee[]=request.getParameterValues("SecuAddiGrid1");
  String tsSocialPlanAmnt[]=request.getParameterValues("SecuAddiGrid2");
  String tsOtherOrganAmnt[]=request.getParameterValues("SecuAddiGrid3");
  String tsRemnant[]=request.getParameterValues("SecuAddiGrid4");
  int sumFeeCount = tssumFee.length;
  if (sumFeeCount>0){
    mFeeMainSchema.setSumFee(tssumFee[0]);
    mFeeMainSchema.setSocialPlanAmnt(tsSocialPlanAmnt[0]);
    mFeeMainSchema.setOtherOrganAmnt(tsOtherOrganAmnt[0]);
    mFeeMainSchema.setRemnant(tsRemnant[0]);
  }
}

//社保手工报销账单
if (tsFeeAtti.equals("2"))
{
	mFeeMainSchema.setSumFee(request.getParameter("ApplyAmnt"));
  mLLSecurityReceiptSchema.setRgtNo(request.getParameter("RgtNo"));
  mLLSecurityReceiptSchema.setCaseNo(request.getParameter("CaseNo"));
  mLLSecurityReceiptSchema.setApplyAmnt(request.getParameter("ApplyAmnt"));
  mLLSecurityReceiptSchema.setFeeInSecu(request.getParameter("FeeInSecu"));
  mLLSecurityReceiptSchema.setFeeOutSecu(request.getParameter("FeeOutSecu"));
  mLLSecurityReceiptSchema.setTotalSupDoorFee(request.getParameter("TotalSupDoorFee"));
  mLLSecurityReceiptSchema.setPayReceipt(request.getParameter("PayReceipt"));
  mLLSecurityReceiptSchema.setRefuseReceipt(request.getParameter("RefuseReceipt"));
  mLLSecurityReceiptSchema.setSupDoorFee(request.getParameter("SupDoorFee"));
  mLLSecurityReceiptSchema.setYearSupDoorFee (request.getParameter("YearSupDoorFee"));
  mLLSecurityReceiptSchema.setPlanFee(request.getParameter("PlanFee"));
  mLLSecurityReceiptSchema.setYearPlayFee(request.getParameter("YearPlayFee"));
  mLLSecurityReceiptSchema.setSupInHosFee(request.getParameter("SupInHosFee"));
  mLLSecurityReceiptSchema.setYearSupInHosFee(request.getParameter("YearSupInHosFee"));
  mLLSecurityReceiptSchema.setSecurityFee (request.getParameter("SecurityFee"));
  mLLSecurityReceiptSchema.setSmallDoorPay(request.getParameter("SmallDoorPay"));
  mLLSecurityReceiptSchema.setEmergencyPay(request.getParameter("EmergencyPay"));
  mLLSecurityReceiptSchema.setSelfPay1(request.getParameter("SelfPay1"));
  mLLSecurityReceiptSchema.setSelfPay2(request.getParameter("SelfPay2"));
  mLLSecurityReceiptSchema.setSecuRefuseFee(request.getParameter("SecuRefuseFee"));
  mLLSecurityReceiptSchema.setSelfAmnt(request.getParameter("SelfAmnt"));
  if(request.getParameter("InsuredStat").equals("2")){
    mLLSecurityReceiptSchema.setRetireAddFee(request.getParameter("RetireAddFee"));
    mLLSecurityReceiptSchema.setYearRetireAddFee(request.getParameter("YearRetireAddFee"));
  }
  if (!request.getParameter("DrugFee").equals("") && request.getParameter("DrugFee")!=null)
  {
    mReceiptSchema = new LLCaseReceiptSchema();
    mReceiptSchema.setFeeItemCode("401");
    mReceiptSchema.setFeeItemName("药费");
    mReceiptSchema.setFee(request.getParameter("DrugFee"));
    mReceiptSchema.setRefuseAmnt(request.getParameter("RefuseDrugFee"));
    mReceiptSet.add(mReceiptSchema);
  }
  if (!request.getParameter("ExamFee").equals("") && request.getParameter("ExamFee")!=null)
  {
    mReceiptSchema = new LLCaseReceiptSchema();
    mReceiptSchema.setFeeItemCode("402");
    mReceiptSchema.setFeeItemName("检查、治疗、化验");
    mReceiptSchema.setFee(request.getParameter("ExamFee"));
    mReceiptSchema.setRefuseAmnt(request.getParameter("RefuseExamFee"));
    mReceiptSet.add(mReceiptSchema);
  }
  if (!request.getParameter("MaterialFee").equals("") && request.getParameter("MaterialFee")!=null)
  {
    mReceiptSchema = new LLCaseReceiptSchema();
    mReceiptSchema.setFeeItemCode("404");
    mReceiptSchema.setFeeItemName("材料");
    mReceiptSchema.setFee(request.getParameter("MaterialFee"));
    mReceiptSchema.setRefuseAmnt(request.getParameter("RefuseMaterialFee"));
    mReceiptSet.add(mReceiptSchema);
  }
  if (!request.getParameter("OtherFee").equals("") && request.getParameter("OtherFee")!=null)
  {
    mReceiptSchema = new LLCaseReceiptSchema();
    mReceiptSchema.setFeeItemCode("403");
    mReceiptSchema.setFeeItemName("其他");
    mReceiptSchema.setFee(request.getParameter("OtherFee"));
    mReceiptSchema.setRefuseAmnt(request.getParameter("RefuseOtherFee"));
    mReceiptSet.add(mReceiptSchema);
  }
}

System.out.println("UI will begin ");

ICaseCureUI tICaseCureUI = new ICaseCureUI();
VData tVData = new VData();
try
{
  // 准备传输数据 VData
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  tVData.add(tGI);
  tVData.add(mFeeMainSchema);
  tVData.add(mReceiptSet);
  tVData.add(mFeeOtherItemSet);
  tVData.add(mLLSecurityReceiptSchema);
  tICaseCureUI.submitData(tVData,tsOperate);
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

if ("".equals(FlagStr))
{
  tError = tICaseCureUI.mErrors;
  if (!tError.needDealError())
  {
    tVData =  tICaseCureUI.getResult();
    mFeeMainSchema = (LLFeeMainSchema) tVData.getObjectByObjectName(
    "LLFeeMainSchema", 0);
    String tipword = "";
    TransferData mTransferData = new TransferData();
    mTransferData=(TransferData) tVData.getObjectByObjectName("TransferData",0);
    if (mTransferData!=null)
    tipword = (String) mTransferData.getValueByName("tipword");
    tsMainFeeNo = mFeeMainSchema.getMainFeeNo();
    System.out.println("aftersubmit " + tipword);
    Content = "账单信息" + tsOperateCN + "成功"+tipword;
    FlagStr = "Succ";
  }
  else
  {
    System.out.println("fail" + tError.getFirstError());
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=tsMainFeeNo%>");
</script>
</html>