<%
//程序名称：LDPersonQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String CustomerNo = ""+request.getParameter("CustomerNo");
if(CustomerNo.equals("null")) CustomerNo="";
String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
if(CustomerName.equals("null")) CustomerName="";
String ContNo = request.getParameter(""+"ContNo");
if(ContNo.equals("null")) ContNo="";
String GrpContNo = request.getParameter(""+"GrpContNo");
if(GrpContNo.equals("null")) GrpContNo="";
String IDType = request.getParameter(""+"IDType");
if(IDType.equals("null")) IDType="";
String IDNo = request.getParameter(""+"IDNo");
if(IDNo.equals("null")) IDNo="";
String RgtNo = request.getParameter(""+"RgtNo");
if(RgtNo.equals("null")) RgtNo="";
String OtherIDNo = request.getParameter(""+"OtherIDNo");
if(OtherIDNo.equals("null")) OtherIDNo="";
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
	// 保单查询条件
      fm.CustomerNo.value = '<%=CustomerNo %>';
	  fm.CustomerName.value = top.opener.fm.CustomerName.value;
	  fm.ContNo.value = '<%= ContNo %>'; 
	  fm.GrpContNo.value = '<%= GrpContNo%>';
	  fm.IDType.value = '<%= IDType %>';
	  fm.IDNo.value = '<%= IDNo %>';
	  fm.RgtNo.value = '<%= RgtNo %>';
	  fm.OtherIDNo.value = '<%= OtherIDNo %>';
  }
  catch(ex)
  {
    alert(ex.message);
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert(ex.message);
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();  
    initPersonGrid();
    afterQuery();
  }
  catch(re)
  {
    alert(ex.message);
  }
}

// 保单信息列表的初始化
function initPersonGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号码";         		//列名
      iArray[1][1]="160px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="姓名";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="性别";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="出生日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="证件类型";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="证件号码";         		//列名
      iArray[6][1]="140px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      //用于返回合同号，隐藏项      
      iArray[7]=new Array();  
	  iArray[7][0]="合同号码";
	  iArray[7][1]="140px";   
	  iArray[7][2]=200;       
	  iArray[7][3]=3;  
	         
    iArray[8]=new Array();  
	  iArray[8][0]="合同号";
	  iArray[8][1]="140px";   
	  iArray[8][2]=200;       
	  iArray[8][3]=3;     
	      
    iArray[9]=new Array();  
	  iArray[9][0]="idtype";
	  iArray[9][1]="140px";   
	  iArray[9][2]=200;       
	  iArray[9][3]=3;         

    iArray[10]=new Array();  
	  iArray[10][0]="otheridno";
	  iArray[10][1]="140px";   
	  iArray[10][2]=200;       
	  iArray[10][3]=3;         

    iArray[11]=new Array();  
	  iArray[11][0]="otheridtype";
	  iArray[11][1]="140px";   
	  iArray[11][2]=200;       
	  iArray[11][3]=3;
	  
	  iArray[12]=new Array();  
	  iArray[12][0]="addressno";
	  iArray[12][1]="140px";   
	  iArray[12][2]=200;       
	  iArray[12][3]=3;      
	  
	  //#2329 （需求编号：2014-229）客户基本信息要素录入需求
	  iArray[13]=new Array();  
	  iArray[13][0]="sexname";
	  iArray[13][1]="140px";   
	  iArray[13][2]=200;       
	  iArray[13][3]=3;  
	  
	  iArray[14]=new Array();  
	  iArray[14][0]="nativeplace";
	  iArray[14][1]="140px";   
	  iArray[14][2]=200;       
	  iArray[14][3]=3;  
	  
	  iArray[15]=new Array();  
	  iArray[15][0]="occupationtype";
	  iArray[15][1]="140px";   
	  iArray[15][2]=200;       
	  iArray[15][3]=3;  
	  
	  iArray[16]=new Array();  
	  iArray[16][0]="IDStartDate";
	  iArray[16][1]="140px";   
	  iArray[16][2]=200;       
	  iArray[16][3]=3;   
	  
	  iArray[17]=new Array();  
	  iArray[17][0]="IDEndDate";
	  iArray[17][1]="140px";   
	  iArray[17][2]=200;       
	  iArray[17][3]=3;    

      PersonGrid = new MulLineEnter( "fm" , "PersonGrid" ); 
      //这些属性必须在loadMulLine前
      PersonGrid.mulLineCount = 10;   
      PersonGrid.displayTitle = 1;
      PersonGrid.locked = 1;
      PersonGrid.canSel = 1;
      PersonGrid.hiddenPlus=1;
      PersonGrid.hiddenSubtraction=1;
      PersonGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>