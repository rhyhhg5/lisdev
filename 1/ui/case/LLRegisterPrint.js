var showInfo;
//var mDebug="1";
//var tSaveFlag = "0";
//var turnPage = new turnPageClass();
var tSaveType="";

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ASCII码是13
	if(keycode=="13")
	{
		queryGrp();
	}
}

function queryGrp()
{
	strSql = "select caseno from llcase where 1=1 "+
		getWherePart("rgtdate","RgtDateS",">=")+getWherePart("rgtdate","RgtDateE","<=")+
		"  and mngcom = '"+fm.ComCode.value+"' order by caseno ";
	fm.sql.value = strSql;
	var arr = easyExecSql(strSql);
	if(arr)
	{
		count = arr.length-1;
		fm.StartCaseNo.value = arr[0][0];
		fm.EndCaseNo.value = arr[count][0];
	}
	else{
	  fm.StartCaseNo.value='';
	  fm.EndCaseNo.value='';
	}
}

		//对CodeSelect选择后的判
function afterCodeSelect( cCodeName, Field )
{
}
function getstr()
{
}



function submitForm()
{
	//var dztype = false;
	for(i = 0; i <fm.IsBatch.length; i++){
		if(fm.IsBatch[i].checked){
			fm.selno.value=fm.IsBatch[i].value;
			break;
		}
	}
	switch(fm.selno.value){
		case "1":
		if (fm.GrpRgtNo.value==""||fm.GrpRgtNo.value ==null||fm.GrpRgtNo.value=="0")
		{
			alert("该批次号不存在！")
			return false;
		}
		break;
		case "2":
		if (fm.StartCaseNo.value==""||fm.StartCaseNo.value ==null)
		{
			alert("请填入起始案件号！")
			return false;
		}
		if (fm.EndCaseNo.value==""||fm.EndCaseNo.value ==null)
		{
			alert("请填入终止案件号！")
			return false;
		}
		var SNo=fm.StartCaseNo.value.substring(3);
		var ENo = fm.EndCaseNo.value.substring(3);
		var MngFlagS = fm.StartCaseNo.value.substring(0,3);
		var MngFlagE = fm.EndCaseNo.value.substring(0,3);
		if(MngFlagS!=MngFlagE)
		{
			alert("起始案件号和终止案件号来自于不同的机构，无法确定打印项目！");
			return false;
		}
		if ((SNo/1)>(ENo/1))
		{
			alert("起始案件号不能大于终止案件号！");
			return false;
		}
		break;
		case "3":
		if (fm.SingleCaseNo.value==""||fm.SingleCaseNo.value ==null)
		{
			alert("请填入需要打印的单号！")
			return false;
		}
		break;
	}
//	if (dztype==false )
//	{
//		alert("请选定打印项目！")
//		return false;
//	}
    var showStr="正在打印，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    //var urlStr="./LLBatchPrtSave.jsp";
		//showInfo2=window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
   // showDiv(inputButton,"false");
  }
}


function BatchPrint()
{
  	fm.fmtransact.value = "batch";
		submitForm();
}

function QueryBatch()
{
	//alert("rgtstate:"+fm.RgtState.value);
	var varSrc;
	var newWindow = OpenWindowNew("./FrameMainGrpRgtQuery.jsp?Interface=GrpRgtQuery.jsp","批量打印","left");
}

function Reset()
{
  fm.GrpRgtNo.value='';
  fm.GrpName.value='';
  fm.BatchNumber.value='';
  fm.RgtDateS.value = '';
  fm.RgtDateE.value = '';
  fm.StartCaseNo.value = '';
  fm.EndCaseNo.value='';
  fm.SingleCaseNo.value='';
  fm.NoticePrt.checked=false;
  fm.DetailPrt.checked=false;
  fm.GrpDetailPrt.checked=false;
  fm.GetDetailPrt.checked=false;
  fm.IsBatch(0).checked = false;
  fm.IsBatch(1).checked = false;
  fm.IsBatch(2).checked = false;
}
