var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

// 查询核保信息
function queryAll(){

	
	var tSQL = "";
	
	if(fm.contno.value==""&&fm.caseno.value=="")
	{
	 alert("请填写案件号或保单号！");
	 return false;
	
	}
	
	if(fm.caseno.value!=''&&fm.caseno.value!=null&&fm.contno.value!=''&&fm.contno.value!=null)
	{
	alert("案件号或保单号只能选择一个！！");
	return false;
	
	}
	
	
	if(fm.caseno.value!=''&&fm.caseno.value!=null)
	{
	//alert("获取案件号成功！");
	
	
	tSQL="select defaultoperator,'已核保' from LBMission where activityid in('0000001181','0000001182')"
	+" and (missionprop1 ='"+fm.caseno.value+"' or missionprop5 ='"+fm.caseno.value+"')"
	+" union all"
	+" select defaultoperator,'未核保' from LwMission where activityid in('0000001181','0000001182')"
	+" and (missionprop1 ='"+fm.caseno.value+"' or missionprop5 ='"+fm.caseno.value+"')"
	
	turnPage1.queryModal(tSQL, CaseGrid);
	}
	
	
	if(fm.contno.value!=''&&fm.contno.value!=null)
	{
	//alert("获取保单号成功！");
	tSQL="select defaultoperator,'已核保' from LBMission where activityid in('0000001181','0000001182')"
	+" and (missionprop1 in(select caseno from llcase where customerno in(select insuredno from lcinsured where contno='"+fm.contno.value+"')) "
	+" or missionprop5 in(select caseno from llcase where customerno in(select insuredno from lcinsured where contno='"+fm.contno.value+"')) "
	+" )union all"
	+" select defaultoperator,'未核保' from LwMission where activityid in('0000001181','0000001182')"
	+" and (missionprop1 in(select caseno from llcase where customerno in(select insuredno from lcinsured where contno='"+fm.contno.value+"')) "
	+" or missionprop5 in(select caseno from llcase where customerno in(select insuredno from lcinsured where contno='"+fm.contno.value+"')))";
	turnPage1.queryModal(tSQL, CaseGrid);
	}
	
	 if (!turnPage1.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
}


/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content) {
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail") {

		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		window.focus();
		window.location.reload();
	}
}
