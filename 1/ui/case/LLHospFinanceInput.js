var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var showInfo;

//提交，保存按钮对应操作
function applyHC()
{
	if (fm.HospitalCode.value==null || fm.HospitalCode.value=="") {
		alert("请选择医院");
		return false;
	}  
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.Operate.value = "INSERT";
	fm.action ="./LLHospFinanceSave.jsp";
	fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,HCNo )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    if (fm.Operate.value=="INSERT") {
    	fm.HCNo.value = HCNo;
    } 
    search();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LCCustomerRCConfirm.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

// 事件响应函数，当用户改变CodeSelect的值时触发
function afterCodeSelect(cCodeName, Field)
{

}


function search()
{
	
	if (fm.AppDateS.value==""||fm.AppDateS.value==null
	    ||fm.AppDateE.value==""||fm.AppDateE.value==null) {
	    	alert("请输入申请日期");
	    	return false;
	}
	
	strSQL = "select a.hcno,a.hospitcode,(select hospitname from ldhospital where hospitcode=a.hospitcode),"
	       + "a.appdate,a.managecom,a.applyer,"
	       + "coalesce((select sum(sumgetmoney) from ljaget where otherno=a.hcno and othernotype='5' and (finstate!='FC' or finstate is null)),0),"
	       + "case when a.state='1' then '已确认' else '未确认' end from llhospget a where a.managecom like '"+fm.ManageCom.value+"%' "
		     + getWherePart('a.hcno','HCNo')
		     + getWherePart('a.appdate','AppDateS','>=')
		     + getWherePart('a.appdate','AppDateE','<=')
		     + getWherePart('a.hospitcode','HospitalCode')
		     + getWherePart('a.state','State');
  
	turnPage.queryModal(strSQL,HospGetInfo);
	fm.HCNoT.value="";
}

function closePage()
{
  top.close();
}

function afterPrint(outname,outpathname) {
	showInfo.close();
	fileUrl.href = "../f1print/download.jsp?filename="+outname+"&filenamepath="+outpathname;
  fileUrl.click();
}

function cancel() {
	if (fm.HCNoT.value==null||fm.HCNoT.value=="") {
		alert("请选择一条记录！");
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.Operate.value = "DELETE";
	fm.action ="./LLHospFinanceSave.jsp";
	fm.submit(); //提交
}

function SelectHospGetInfo()
{
	var vRow = HospGetInfo.getSelNo();

	if( vRow == null || vRow == 0 ) {
		return;
	}
	
	fm.HCNoT.value = HospGetInfo.getRowColData(vRow-1,1);
}

function deal()
{
	if (fm.HCNoT.value==null||fm.HCNoT.value=="") {
		alert("请选择一条记录！");
		return false;
	}

	var varSrc = "&HCNo=" + fm.HCNoT.value;
	pathStr="./FrameMainHospGetDeal.jsp?Interface=LLHospGetDealInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLHospGetDeal","left");
}
function printPDF () {
	var selno = HospGetInfo.getSelNo();
	var HCNo;
	var tState = "0";
	
	if (selno <= 0) {
		alert("请选择要打印的批次");
	  return ;
	}

	HCNo = HospGetInfo.getRowColData( selno-1, 1);
	var strSQL="select state from llhospget where hcno='"+HCNo+"'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null) {
		tState = arrResult[0][0];
  }
  
  if(tState != "1") {
  	alert("批次还未确认！");
  	return;
  }
  
  var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.fmtransact.value="PRINT";
	fm.action = "../uw/PDFPrintSave.jsp?Code=lp012&RePrintFlag=&MissionID=&SubMissionID=&OtherNoType=12&PrtFlag=&fmtransact=PRINT&OtherNo="+ HCNo +"&PrtSeq=&StandbyFlag3=1";
	fm.submit();
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}