<%
//程序名称：LLRealPayReport.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.schema.LOPRTManagerSchema"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.*"%>
<%
	System.out.println("start");
	CError cError = new CError();
	boolean operFlag=true;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = request.getParameter("strOper");
    //*****************************************
      RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);
    //******************************************
	LLReportAgeUI tLLReportAgeUI = new LLReportAgeUI();
	String StartDate = request.getParameter("StartDate");
	String EndDate = request.getParameter("EndDate");
	String ManageCom = request.getParameter("ManageCom");
	String AgeSpan = request.getParameter("AgeSpan");
	
	String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
	String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");

		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput)session.getValue("GI");
		XmlExport txmlExport = new XmlExport();

	if(sd.compareTo(ed) > 0)
	{
		operFlag=false;
		Content = "操作失败，原因是:统计止期比统计统计起期早";
	}
else
	{
		TransferData PrintElement = new TransferData();
		PrintElement.setNameAndValue("StartDate",StartDate);
		PrintElement.setNameAndValue("EndDate",EndDate);
		PrintElement.setNameAndValue("ManageCom",ManageCom);
		PrintElement.setNameAndValue("AgeSpan",AgeSpan);
		
		
		VData tVData = new VData();
		VData mResult = new VData();
		CErrors mErrors = new CErrors();
		//tVData.addElement(tLOPRTManagerSchema);
		tVData.addElement(PrintElement);
		tVData.addElement(tG);
			if(!tLLReportAgeUI.submitData(tVData,strOperation))
			{
				operFlag=false;
				Content=tLLReportAgeUI.mErrors.getFirstError().toString();
			}
			else
			{
				mResult = tLLReportAgeUI.getResult();
				txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
				if(txmlExport==null)
				{
					operFlag=false;
					Content="没有得到要显示的数据文件";
				}
			}
		}
	ExeSQL tExeSQL = new ExeSQL();
	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strFileNameP=tG.Operator + "_" + FileQueue.getFileName()+".vts";
	System.out.println("strFileNameP="+strFileNameP);
	//*******************************
		 rpt.updateReportMetaData(strFileNameP);
	//**********************************
	String strVFFileName = strFilePath + strFileNameP;

	//获取存放临时文件的路?
	//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
	//String strRealPath = tExeSQL.getOneValue(strSql);
	String strRealPath = application.getRealPath("/").replace('\\','/');
	String strVFPathName = strRealPath + "//" +strVFFileName;
	
	CombineVts tcombineVts = null;
	if (operFlag==true)
	{
		//合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
	
		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);
	
		//把dataStream存储到磁盘文件
		//System.out.println("存储文件到"+strVFPathName);
		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		System.out.println("==> Write VTS file to disk ");
	
		System.out.println("===strVFFileName : "+strVFFileName);
		//本来打算采用get方式来传递文件路?
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
	}
	
	else
	{
		FlagStr = "Fail";
	
		%>
		<html>
		<%@page contentType="text/html;charset=GBK" %>
		<script language="javascript">
		alert("<%=Content%>");
		top.close();
	
		//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");
	
	</script>
	</html>
	<%
}

%>