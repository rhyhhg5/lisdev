var turnPage = new turnPageClass();
//弹出一个页面层来导入
function diskImport()
{ 
   if(""==fm.all("RiskCode").value){
	   alert("险种编码不能为空");
	   return;
   }
   var LogmanageCom = fm.all("LogmanageCom").value;
   var ManageLen = fm.all("LogmanageCom").value.length;
   if(ManageLen!="8"){
	   alert("您需要以八位机构登录才可以进行此操作");
	   return;
   }
   fm.all('BatchNo').readOnly=true;
   var BatchNo=fm.all('BatchNo').value;
   var RiskCode=fm.all('RiskCode').value;
   var strUrl = "../case/OutsourcingCostImport.jsp?LogmanageCom="+LogmanageCom+"&RiskCode="+RiskCode;
   showInfo=window.open(strUrl,"保单号导入","width=500, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=yes");
   
}    

//保存功能
function saveOSInfo(){
	if (!beforeSubmit()){		
		return false;
	}	 
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.operate.value='INSERT||MAIN';
	fm.submit();
	fm.all('BatchNo').readOnly=false;
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
 
  if(!verifyInput()) {
  	return false;
  }
  
  if(!check()){
  	return false;
  	
  }
  
  
  
  
  return true;
}          
//校验功能 
function check(){
	var currentOperate =fm.all("currentOperate").value;
	var Transfertype=fm.all('Transfertype').value;
	var OSAccountNo=fm.all('OSAccountNo').value;
	var OSbank=fm.all('OSbank').value;
	var BatchNo=fm.all('BatchNo').value;
	var Operater = fm.all("Operator").value;
	
	if(Operater!=currentOperate){
		alert("当前登录用户和录入人员必须一致！");
		return;
	}
	if(Transfertype==''){
		alert("转账方式不能为空");
		return;
	}
	if(Transfertype=='1'&&fm.all("OSTotal").value==''){ 
		alert("现金支付！外包费用总额不能为空");
		return;
	}
	//校验支付，当不是现金的时候，外包单位账户号码和外包单位开户行不能为空
	if( Transfertype!=''&&Transfertype!=1 ){
			if(OSAccountNo==''|| OSbank==''){
				alert("非现金支付！外包单位账户号码和外包单位开户行不能为空");
				return false;
			}
	}
	//#2875外包费用录入功能完善
	var batno=fm.BatchNo.value;
	var ManageCom=fm.ManageCom.value;
	var stSL="select grpcontno from LLOutcoucingAverage where batchno='"+batno+"'";
	var GrpNo=new Array();
	GrpNo =easyExecSql(stSL);	
	for(var a=0;a<GrpNo.length;a++){
		var stL="select ManageCom from lcgrpcont where grpcontno='"+GrpNo[a]+"'";
		var MaCom=easyExecSql(stL);	
		if(MaCom!=ManageCom){
			alert("导入的保单的管理机构必须和页面上录入的管理机构一致");
			return false;
		}	
	}
	//校验批次号是否存在，如果存在则不能保存
	var strSQL ="select distinct  1 from LLOUTSOURCINGTOTAL where  Batchno = '"+BatchNo+"' ";
	var strQueryResult = easyQueryVer3(strSQL,1,1,1);
	arrSelected = decodeEasyQueryResult(strQueryResult);
	if(arrSelected!=1){
		
	}else{
		alert("批次号已经存在，请刷新页面重新录入！");
		return false;
	}
	
	//校验保单号是否已经导入，没导入则不能通过
	var strSQL ="select distinct 1 from LLOUTCOUCINGAVERAGE where   Batchno ='"+BatchNo+"' and P1='0'";
	var strQueryResult = easyQueryVer3(strSQL,1,1,1);
	arrSelected = decodeEasyQueryResult(strQueryResult);
	if(arrSelected!=1){
		alert("请先导入保单号！");
		return false;
	}
	
	
	return true
}

//查询功能
function queryOSInfo(){
   if(!verifyInput()) {
  	return false;
  }
 
  
  var strSql="select BATCHNO,RISKCODE,OSTOTAL,OPERATOR,MAKEDATE,case" +
  		" when AUDITSTATE ='0' then '待审核'" +
  		" when AUDITSTATE ='1' then '结案状态'" +
  		" when AUDITSTATE ='2' then '审核未通过'" +
  		" when AUDITSTATE ='3' then '撤件状态'" +
  		" else null end AUDITSTATE from LLOUTSOURCINGTOTAL o where 1=1" 
		+ getWherePart("o.BATCHNO","BatchNo")
  		+ getWherePart("o.OSTotal","OSTotal")
  		+ getWherePart("o.RiskCode","RiskCode")
  		+ getWherePart("o.BalanceStartDate","BalanceStartDate",">=")
  		+ getWherePart("o.BalanceEndDate","BalanceEndDate","<=")
  		+ getWherePart("o.PayYear","PayYear")
  		+ getWherePart("o.ManageCom","ManageCom","like")
  		+ getWherePart("o.Operator","Operator")
  		+ getWherePart("o.OSAccount","OSAccount")
  		+ getWherePart("o.OSAccountNo","OSAccountNo")
  		+ getWherePart("o.OSBank","OSBank")
  		+ getWherePart("o.Transfertype","Transfertype")
  		+"order by BATCHNO";
  var arr = easyExecSql(strSql);		   
  if (arr==null) {
	 	 alert("外包信息信息查询失败！(可尝试去掉一些查询条件)");
	 	 return;
	}
  
  turnPage.queryModal(strSql,ContGrid);
	
	
}
//删除功能
function deleteOSInfo(){
	var currentOperate =fm.all("currentOperate").value;
	var BatchNo  = fm.all("BatchNo").value;
	if(BatchNo == null || BatchNo == "") 
	{
		alert("请输入要删除的批次号！");
		return;
	}
	var sql = "select AUDITSTATE ,OPERATOR from LLOUTSOURCINGTOTAL where BatchNo='"+BatchNo+"'";
	var arr = easyExecSql(sql);
	
	
	if(arr == null || arr == "") 
	{
		alert("该批次号不存在，请重新输入！");
		return;
	}
	var AuditState = arr[0][0];
	var Operator= arr[0][1];
	//alert(AuditState+"========="+Operator);
	if(Operator!=currentOperate){
		alert("您没有删除权限！");
		return;
	}
	
	if(AuditState==1){
		alert("该批次号已经结案，不能删除！");
		return ;
	}
	if(AuditState==3){
		alert("该批次号已经是撤件状态，不需要重新删除！");
		return;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.operate.value='DELETE||MAIN';
	fm.submit();
	
}

//将得到的信息放到页面上
function getContInfo(){
  var selno = ContGrid.getSelNo();
  if (selno <= 0){
    return ;
  }
  fm.BatchNo.value= ContGrid.getRowColData(selno - 1, 1);
  doSearchShow();
  
}
//查询信息放到页面上上
function doSearchShow(){
	var BatchNo = fm.all("BatchNo").value;
	if(BatchNo == null || BatchNo == "") 
	{
		alert("请输入批次号！");
		return;
	}
	initPage();
	var sql = "select BATCHNO,OSTOTAL,RISKCODE,BALANCESTARTDATE," +
			         "BALANCEENDDATE,PAYYEAR,MANAGECOM,OSACCOUNT," +
			         "OSACCOUNTNO,OSBANK,TRANSFERTYPE,OPERATOR,AUDITOPINION " +
			         "from LLOUTSOURCINGTOTAL where BatchNo='"+BatchNo+"'";
	
	var arr = easyExecSql(sql);		         
			         
	if(arr[0][6]!=null&&arr[0][6]!=""){
		var sqlManageComName ="select Name from ldcom where  comcode='"+arr[0][6]+"'";
		var strQueryResult = easyQueryVer3(sqlManageComName,1,1,1);
		var ManageComName = decodeEasyQueryResult(strQueryResult);
	}

	if(arr[0][9]!=null&&arr[0][9]!=""){
		var sqlOSBankName=" select bankname from ldbank where bankcode='"+arr[0][9]+"'";      
		var strQueryResult = easyQueryVer3(sqlOSBankName,1,1,1);
		var OSBankName = decodeEasyQueryResult(strQueryResult);
	}
	
	if(arr[0][10]!=null&&arr[0][10]!=""){
		var sqlTransferTypeName ="select codename from ldcode where codetype='paymode' and code='"+arr[0][10]+"'";
		var strQueryResult = easyQueryVer3(sqlTransferTypeName,1,1,1);
		var TransferTypeName = decodeEasyQueryResult(strQueryResult);
	}
	
	
	if (arr==null) {
	 	 alert("外包信息信息查询失败！");
	 	 return;
	}else{
		arr[0][0]==null||arr[0][0]=='null'?'0':fm.BatchNo.value=arr[0][0];
	 	arr[0][1]==null||arr[0][1]=='null'?'0':fm.OSTotal.value=arr[0][1];
	 	arr[0][2]==null||arr[0][2]=='null'?'0':fm.RiskCode.value=arr[0][2];
	 	arr[0][3]==null||arr[0][3]=='null'?'0':fm.BalanceStartDate.value=arr[0][3];
	 	arr[0][4]==null||arr[0][4]=='null'?'0':fm.BalanceEndDate.value=arr[0][4];
	 	arr[0][5]==null||arr[0][5]=='null'?'0':fm.PayYear.value=arr[0][5];
	 	arr[0][6]==null||arr[0][6]=='null'?'0':fm.ManageCom.value=arr[0][6];
	 	arr[0][7]==null||arr[0][7]=='null'?'0':fm.OSAccount.value=arr[0][7];
	 	arr[0][8]==null||arr[0][8]=='null'?'0':fm.OSAccountNo.value=arr[0][8];
	 	arr[0][9]==null||arr[0][9]=='null'?'0':fm.OSBank.value=arr[0][9];
    	arr[0][10]==null||arr[0][10]=='null'?'0':fm.Transfertype.value=arr[0][10];
	 	arr[0][11]==null||arr[0][11]=='null'?'0':fm.Operator.value=arr[0][11];
	 	arr[0][12]==null||arr[0][12]=='null'?'0':fm.AuditOpinion.value=arr[0][12];	
	 	
	 	ManageComName==null||ManageComName=='null'?'0':fm.ManageComName.value=ManageComName;	
	 	OSBankName==null||OSBankName=='null'?'0':fm.OSBankName.value=OSBankName;	
	 	TransferTypeName==null||TransferTypeName=='null'?'0':fm.TransferTypeName.value=TransferTypeName;	
	}	
		fm.all('BatchNo').readOnly=true; 
		fm.all('OSTotal').readOnly=true;
		fm.all('RiskCode').readOnly=true;
		fm.all('BalanceStartDate').readOnly=true;
		fm.all('BalanceEndDate').readOnly=true;
		fm.all('PayYear').readOnly=true;
		fm.all('ManageCom').readOnly=true;
		fm.all('OSAccount').readOnly=true;
		fm.all('OSAccountNo').readOnly=true;
		fm.all('OSBank').readOnly=true;
		fm.all('Transfertype').readOnly=true;
		fm.all('Operator').readOnly=true;
		fm.all('ManageComName').readOnly=true;       
		fm.all('OSBankName').readOnly=true;       
		fm.all('TransferTypeName').readOnly=true;        
			         
}



function afterSubmit(FlagStr, content)
{
	
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.opener.focus();
		top.opener.location.reload();
		top.close();
    }
}
//审核
function audit(state){
	var currentOperate =fm.all("currentOperate").value;
	var BatchNo  = fm.all("BatchNo").value;
	var LogmanageCom = fm.all("LogmanageCom").value;
	   var ManageLen = fm.all("LogmanageCom").value.length;
	   if(ManageLen!="8"){
		   alert("您需要以八位机构登录才可以进行此操作");
		   return;
	   }
	fm.auditState.value=state;
	if(BatchNo == null || BatchNo == "") 
	{
		alert("请输入要审批的批次号！");
		return;
	}
	var sql = "select AUDITSTATE ,OPERATOR from LLOUTSOURCINGTOTAL where BatchNo='"+BatchNo+"'";
	var arr = easyExecSql(sql);
	
	
	if(arr == null || arr == "") 
	{
		alert("该批次号还未保存，请重新输入！");
		return;
	}
	//审核状态
	var AuditState = arr[0][0];
	//录入人员
	var Operator= arr[0][1];
	//alert(Operator+"和"+currentOperate);
	if(Operator==currentOperate){
		alert("录入人员和审核人员不能是同一个操作人员！");
		return;
	}
	
	if(AuditState==1){
		alert("该批次号已经结案，不需要再审核！");
		return;
	}else if(AuditState==2){
		alert("该批次号审核未通过！不需要再审核！");
		return;
	}else if(AuditState==3){
		alert("该批次号已经撤件，不能审核");
		return;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.operate.value='UPDATE||MAIN';
	fm.submit();
	
}

//导出保单
function exportCont(){
	//定义查询的数据
	var BatchNo  = fm.all("BatchNo").value;
	if(BatchNo == null || BatchNo == "") 
		{
			alert("请输入要导出的批次号！");
			return;
		}
	
	var strSQL ="select distinct 1 from LLOUTCOUCINGAVERAGE where   Batchno ='"+BatchNo+"' ";
	var strQueryResult = easyQueryVer3(strSQL,1,1,1);
	arrSelected = decodeEasyQueryResult(strQueryResult);
	if(arrSelected!=1){
		alert("该批次号不存在,请重新输入正确的批次号！");
		return ;
	}
	var  strSQL ="select GRPCONTNO,OSAVERAGE from LLOUTCOUCINGAVERAGE a where 1=1"
			+ getWherePart('a.BatchNo','BatchNo');
			
	fm.querySql.value = strSQL;
	  
	//定义列名
	var strSQLTitle = "select '保单号','本保费分摊费用'  from dual where 1=1 ";
	fm.querySqlTitle.value = strSQLTitle;
	  
	//定义表名
	fm.all("Title").value="select '导出保单号' from dual where 1=1  ";  
	var beforeAction = fm.action;
	fm.action = " ../case/OutsourcingCostExportSave.jsp";
	fm.submit();
	fm.action=beforeAction;
}
//打印给付凭证
function osprint(){
	var BatchNo  = fm.all("BatchNo").value;
	/*var LogmanageCom = fm.all("LogmanageCom").value;
	   var ManageLen = fm.all("LogmanageCom").value.length;
	   if(ManageLen!="8"){
		   alert("您需要以八位机构登录才可以进行此操作");
		   return;
	   }*/
	if(BatchNo == null || BatchNo == "") 
	{
		alert("请输入要打印的批次号！");
		return;
	}
	//校验批次号是否已经存到LJAGet表中
	var strSQL="select 1 from LJAGet where OtherNo='"+BatchNo+"'" +"and OtherNoType='19'";
	var strQueryResult = easyQueryVer3(strSQL,1,1,1);
	arrSelected = decodeEasyQueryResult(strQueryResult);
	if(arrSelected!=1){
		alert("只有审核通过的批次才能打印凭证（具体请查看您输入的批次审批状态）！");
		return;
	}
	var showStr="正在准备打印数据，请稍后...";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	var beforeAction = fm.action;
	fm.action = "../uw/PDFPrintSave.jsp?Code=lp018&OtherNo="+BatchNo+"";
	fm.submit();
	fm.action=beforeAction;	
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}











