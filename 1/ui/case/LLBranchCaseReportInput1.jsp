<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLBranchCaseReportInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
   String sql = "select case when current time>='08:00:00' and current time<'17:30:00' then 'Y' else 'N' end from dual"; 
   String sql1 = "select case when dayofweek(current date)=7 or dayofweek(current date)=1 then 'Y' else 'N' end from dual with ur";
   ExeSQL tExeSQL = new ExeSQL();
   String rel = tExeSQL.getOneValue(sql);
   String rel1 = tExeSQL.getOneValue(sql1);
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLBranchCaseReportInput1.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./LLBranchCaseReport.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
         <TR  class= common>
		      <TD  class= title>案件类型</TD>
          <TD  class= input> <input class="codeno" CodeData="0|5^1|全部案件^2|常规案件^3|批次受理案件^4|批次处理案件^5|不含回销预付赔款案件"  name=CaseType ondblclick="return showCodeListEx('CaseType',[this,CaseTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CaseType',[this,CaseTypeName],[0,1]);"><input class=codename name=CaseTypeName></TD> 
        </TR>
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="BranchCase()"></TD>
			</tr>
		</table>
		 <INPUT  type= hidden name="result" value=<%=rel %>>
		 <INPUT  type= hidden name="isweek" value=<%=rel1 %>>
	
  </form>
  <table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">机构：理赔案件的受理机构</td></tr><br>
            <tr class="common"><td class="title">申请件数：统计期间内受理申请的案件数</td></tr><br>
            <tr class="common"><td class="title">撤销件数：统计期间内撤件的案件数</td></tr><br>
            <tr class="common"><td class="title">当期申请结案件数：统计期间内受理并结案的案件数</td></tr><br>
            <tr class="common"><td class="title">当期申请结案赔款：统计期间内受理并结案的案件赔款</td></tr><br>
            <tr class="common"><td class="title">结案件数：统计期间内结案的案件数</td></tr><br>
            <tr class="common"><td class="title">结案赔款：统计期间内结案的案件赔款</td></tr><br>
            <tr class="common"><td class="title">结案平均处理时间（天）：统计期间内结案案件的操作时间/结案件数</td></tr><br>
            <tr class="common"><td class="title">回销预付赔款：统计期内回销预付赔款总额</td></tr><br>
		</Div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 