 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
    String tStrSubType = (String)request.getParameter("SubType");
    
    //页面时间显示
   	String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
    String AheadDays="-90";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
%>                            

<script language="JavaScript">
  
var tManageCom = "<%=tGI.ComCode%>";
var tCurrentDate = "<%=tCurrentDate%>";
var tSubType = "<%=tStrSubType%>";

function initDate(){
   	fm.RgtDateS.value="<%=afterdate%>";
   	fm.RgtDateE.value="<%=CurrentDate%>";
}

function initForm()
{
    try
    {	
    	initDate();
   	 	initContGrid();
    	initInpBox();
        
        
    }
    catch(ex)
    {
        alert("初始化界面错误，" + ex.message);
    }
}

function initInpBox(){
	try{
		fm.all("ydUnitIdT").value=tManageCom;
		var sql ="select codeName from ldcode where codetype='ssfmng' and code='"+tManageCom+"'";
		var strQueryResult = easyQueryVer3(sql,1,1,1);
		arrSelected = decodeEasyQueryResult(strQueryResult);
		if(arrSelected!=null && arrSelected!=""){
			fm.all("ydUnitNameT").value=arrSelected;
		}	
			
	}
	  catch(ex)
    {
        alert("页面初始化错误" + ex.message);
    }	
}
function initPage(){
	fm.RgtDateS.value="";                             
	fm.RgtDateE.value="";                             
	fm.cbdUnitNameT.value="";                         
	fm.ydUnitNameT.value="";                 
	fm.nameT.value="";                    
	fm.medicareCodeT.value="";                           
	fm.idNoT.value="";                                     
}


function initContGrid()
{
    var iArray = new Array();

        
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=20;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="调查案件号";         		//列名
        iArray[1][1]="80px";            		//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[1][21]="SurveyCaseNo";
        
        iArray[2]=new Array();
        iArray[2][0]="结算号";         		//列名
        iArray[2][1]="70px";            		//列宽
        iArray[2][2]=80;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[2][21]="SettlementNo"; 
        
        iArray[3]=new Array();
        iArray[3][0]="给付凭证号";         		//列名
        iArray[3][1]="55px";            		//列宽
        iArray[3][2]=100;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[3][21]="PaymentVoucherNo";
        
        iArray[4]=new Array();
        iArray[4][0]="参保地机构";         		//列名
        iArray[4][1]="80px";            		//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[4][21]="InsuranceComName";
        
        iArray[5]=new Array();
        iArray[5][0]="(异地)调查机构";         		//列名
        iArray[5][1]="80px";            		//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[5][21]="RemoteSurveyComName";
        
        iArray[6]=new Array();
        iArray[6][0]="结算总金额";         		//列名
        iArray[6][1]="80px";            		//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[6][21]="TotalAmount";
        
        iArray[7]=new Array();
        iArray[7][0]="受理时间";         		//列名
        iArray[7][1]="80px";            		//列宽
        iArray[7][2]=100;            			//列最大值
        iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[7][21]="SurveyStartDate";
        
        iArray[8]=new Array();
        iArray[8][0]="调查结案时间";         		//列名
        iArray[8][1]="80px";            		//列宽
        iArray[8][2]=100;            			//列最大值
        iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[8][21]="SurveyEndDate";
        
        iArray[9]=new Array();
        iArray[9][0]="结算单生成时间";         		//列名
        iArray[9][1]="80px";            		//列宽
        iArray[9][2]=100;            			//列最大值
        iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[9][21]="CreateDate";
        
        iArray[10]=new Array();
        iArray[10][0]="患者";         		//列名
        iArray[10][1]="80px";            		//列宽
        iArray[10][2]=100;            			//列最大值
        iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[10][21]="Name";
        
        iArray[11]=new Array();
        iArray[11][0]="医保编号";         		//列名
        iArray[11][1]="80px";            		//列宽
        iArray[11][2]=100;            			//列最大值
        iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[11][21]="MedicareCode";
        
        iArray[12]=new Array();
        iArray[12][0]="身份证号码";         		//列名
        iArray[12][1]="80px";            		//列宽
        iArray[12][2]=100;            			//列最大值
        iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[12][21]="IdNo";
        
        iArray[13]=new Array();
        iArray[13][0]="参保地机构代码";         		//列名
        iArray[13][1]="80px";            		//列宽
        iArray[13][2]=100;            			//列最大值
        iArray[13][3]=2;  						//是否允许输入,1表示允许，0表示不允许 
        iArray[13][4]="";            			
        iArray[13][21]="InsuranceComCode";
        
        iArray[14]=new Array();
        iArray[14][0]="(异地)调查机构代码";         		//列名
        iArray[14][1]="80px";            		//列宽
        iArray[14][2]=100;            			//列最大值
        iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[14][21]="RemoteSurveyComCode";
        
        iArray[15]=new Array();
        iArray[15][0]="实付号码";         		//列名
        iArray[15][1]="80px";            		//列宽
        iArray[15][2]=100;            			//列最大值
        iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[15][21]="ActuGetNo";
        
        iArray[16]=new Array();
        iArray[16][0]="成本中心";         		//列名
        iArray[16][1]="80px";            		//列宽
        iArray[16][2]=100;            			//列最大值
        iArray[16][3]=2;						//则该列的每一个输入框的样式都是代码选择的风格
        iArray[16][4]="";               			//是否引用代码: null或者" "为不引用 
        iArray[16][21]="CostCenter";
        
        
        ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
        //这些属性必须在loadMulLine前
        ContGrid.mulLineCount = 0;   
        ContGrid.displayTitle = 1;
        ContGrid.locked = 1;
        ContGrid.canSel = 1;
        ContGrid.canChk = 0;
        ContGrid.hiddenPlus=1;   
        ContGrid.hiddenSubtraction=1;
        ContGrid.selBoxEventFuncName ="";
        ContGrid.loadMulLine(iArray);
        
    }
    catch(ex)
    {
        alert("初始化" + ex.message);
    }
}



</script>