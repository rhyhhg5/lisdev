<%
//Name:ContReDefInit.jsp
//function：非标准业务配置界面
//author:Xx
//Date:2006-12-10
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>


<%

%>
<script language="JavaScript">
function initInpBox(){
  try{
  }
  catch(ex){
    alter("在ContReDefInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox(){
  try{
  }
  catch(ex){
    alert("在ContReDefInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(){
  try{
    initInpBox();
    initContGrid();
    initPolGrid();
    initPlanGrid();
    initDutyGrid();
    initGetDutyGrid();
     initGetRateGrid('1');
     initFeeItemGrid();
//    easyQueryClick();
  }
  catch(re){
    alter("在ContReDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initContGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("客户号码","50px","10","0");
    iArray[2]=new Array("团体名称","100px","10","0");
    iArray[3]=new Array("保单号码","50px","10","0");
    iArray[4]=new Array("生效日期","35px","10","0");
    iArray[5]=new Array("总保费","35px","10","0");
    iArray[6]=new Array("承保机构","35px","10","0");

    ContGrid = new MulLineEnter("fm","ContGrid");
    ContGrid.mulLineCount =2;
    ContGrid.locked = 1;
    ContGrid.canSel =1;
    ContGrid.canChk = 0;
    ContGrid.hiddenPlus=1;
    ContGrid.hiddenSubtraction=1;
    ContGrid.selBoxEventFuncName = "onSelCont";
    ContGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

function initPolGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("险种代码","60px","10","0");
    iArray[2]=new Array("险种名称","190px","10","0");
    iArray[3]=new Array("总保费","50px","10","0");
    iArray[4]=new Array("参保人数","50px","10","0");

    PolGrid = new MulLineEnter("fm","PolGrid");
    PolGrid.mulLineCount =3;
    PolGrid.locked = 1;
    PolGrid.canSel =1
    PolGrid.canChk = 0;;
    PolGrid.hiddenPlus=1;
    PolGrid.hiddenSubtraction=1;
    PolGrid.selBoxEventFuncName = "onSelPol";
    PolGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

function initPlanGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("保障计划","60px","10","0");
    iArray[2]=new Array("人员类别","240px","10","0");
    iArray[3]=new Array("参保人数","50px","10","0");

    PlanGrid = new MulLineEnter("fm","PlanGrid");
    PlanGrid.mulLineCount =3;
    PlanGrid.locked = 1;
    PlanGrid.canSel =0
    PlanGrid.canChk = 1;;
    PlanGrid.hiddenPlus=1;
    PlanGrid.hiddenSubtraction=1;
    PlanGrid.selBoxEventFuncName = "onSelSelected";
    PlanGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

function initDutyGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("保障计划","50px","10","0");
    iArray[2]=new Array("险种责任","80px","10","0");
    iArray[3]=new Array("要素名称","50px","10","0");
    iArray[4]=new Array("要素说明","50px","10","0");
    iArray[5]=new Array("要素值","50px","10","0");
    iArray[6]=new Array("特别说明","100px","10","0");

    DutyGrid = new MulLineEnter("fm","DutyGrid");
    DutyGrid.mulLineCount =3;
    DutyGrid.locked = 1;
    DutyGrid.canSel =0;
    DutyGrid.canChk = 0;
    DutyGrid.hiddenPlus=1;
    DutyGrid.hiddenSubtraction=1;
    DutyGrid.selBoxEventFuncName = "onSelSelected";
    DutyGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

function initGetDutyGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("险种责任","80px","10","1");
    iArray[2]=new Array("免赔额","40px","10","1");
    iArray[3]=new Array("免赔扣除类型","50px","10","2");
    iArray[4]=new Array("GetType","0px","10","3");
    iArray[5]=new Array("给付比例","40px","10","1");
    iArray[6]=new Array("限额","40px","10","1");
    iArray[7]=new Array("保额","40px","10","1");
    iArray[8]=new Array("是否考虑保/限额","50px","10","2");
    iArray[9]=new Array("AmntFlag","0px","10","3");

    GetDutyGrid = new MulLineEnter("fm","GetDutyGrid");
    GetDutyGrid.mulLineCount =3;
    GetDutyGrid.locked = 1;
    GetDutyGrid.canSel =1;
    GetDutyGrid.canChk = 0;
    GetDutyGrid.hiddenPlus=1;
    GetDutyGrid.hiddenSubtraction=1;
    GetDutyGrid.selBoxEventFuncName = "onSelSelected";
    GetDutyGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

function initGetRateGrid(GRType){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("金额下限","40px","10","0");
    iArray[2]=new Array("金额上限","40px","10","0");
    iArray[3]=new Array("给付比例","40px","10","0");
    if(GRType!='1'){
    	iArray[1][0] = "起始金额";
    	iArray[2][0] = "终止金额";
    }

    GetRateGrid = new MulLineEnter("fm","GetRateGrid");
    GetRateGrid.mulLineCount =2;
    GetRateGrid.locked = 0;
    GetRateGrid.canSel =0
    GetRateGrid.canChk = 0;;
    GetRateGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

function initFeeItemGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("费用项目","40px","10","0");
    iArray[2]=new Array("项目代码","0px","10","3");
    iArray[3]=new Array("日均限额","30px","10","1");
    iArray[4]=new Array("总限额","30px","10","1");
    iArray[5]=new Array("给付比例","30px","10","1");

    FeeItemGrid = new MulLineEnter("fm","FeeItemGrid");
    FeeItemGrid.mulLineCount =2;
    FeeItemGrid.locked = 0;
    FeeItemGrid.canSel =0
    FeeItemGrid.canChk = 0;
    FeeItemGrid.hiddenPlus=1;
    FeeItemGrid.hiddenSubtraction=1;
    FeeItemGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

 </script>