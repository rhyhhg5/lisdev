<%
//Name：LLAcciDentInit.jsp
//Function：立案界面的初始化程序
//Date：2010-08-18 17:44:28
//Author  ：
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	GlobalInput mG = new GlobalInput();
	mG=(GlobalInput)session.getValue("GI");
	String tCustomerNo = request.getParameter("InsuredNo");
	String tCustomerName = new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");;	
%>
<script language="JavaScript">
function initForm()
{
  try
  { 
    initInpBox();
    initSelBox();   
    initEventGrid(); 
	initQuery();
	
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {     
  	fm.CustomerNo.value = '<%= tCustomerNo%>';     
  	var sql="select name from ldperson where customerno='"+fm.CustomerNo.value+"' ";
  	var arrResult = easyExecSql(sql );
  	if(arrResult)
  	{
  		fm.CustomerName.value = arrResult[0][0];
 	}                          
  }
  catch(ex)
  {
    alert("在LLClaimProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {   
  }
  catch(ex)
  {
    alert("在LLClaimProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
 function initEventGrid()
 {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","40px","10","0");
      iArray[1]=new Array("事件号","140px","100","0");

      iArray[2]=new Array("发生日期","80px","100","1");
      iArray[2][9]="发生日期|notnull&date";

      iArray[3]=new Array("发生地点","150px","100","1");

      iArray[4] = new Array("事故类型","80px","0","3");
      iArray[5] = new Array("事故主题","200px","0","3");
      iArray[6] = new Array("医院名称","200px","0","3");

      iArray[7]=new Array("入院日期","80px","100","1");
      iArray[7][9]="入院日期|date";

      iArray[8]=new Array("出院日期","80px","100","1");
      iArray[8][9]="出院日期|date";

      iArray[9] = new Array("事件信息","200px","1000","1");

      iArray[10] = new Array("事件类型","60px","10","2")
      iArray[10][10]="AccType";
      iArray[10][11]="0|^1|疾病|^2|意外"
      iArray[10][12]="10|11"
      iArray[10][13]="1|0"

      iArray[11] = new Array("事件类型","60px","10","3")

      EventGrid = new MulLineEnter("fm","EventGrid");
      EventGrid.mulLineCount = 0;
      EventGrid.displayTitle = 1;
      EventGrid.locked = 0;
      EventGrid.canChk =0	;
      EventGrid.hiddenPlus=1;
      EventGrid.hiddenSubtraction=1;
      EventGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }

</script>