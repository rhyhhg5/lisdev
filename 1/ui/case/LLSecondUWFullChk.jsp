<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContUWSave.jsp
//程序功能：合同单人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  //接收信息
 
 
	String tSendUW[] = request.getParameterValues("PolAddGridNo");
	String tContNo[] = request.getParameterValues("PolAddGrid1");

	String tCaseNo = request.getParameter("CaseNo");
	String tBatNo = request.getParameter("BatNo");

	int tSendUWCount = tSendUW.length;	
	
	System.out.println("tSendUWCount" + tSendUWCount );
	
 
  LLCUWBatchSet tLLCUWBatchSet = new LLCUWBatchSet();
	for (int i = 0; i < tSendUWCount; i++)
	{
		if (!tSendUW[i].equals("") )
		{
		
	  	 LLCUWBatchSchema tLLCUWBatchSchema = new LLCUWBatchSchema();
	     tLLCUWBatchSchema.setContNo(tContNo[i]);
	    tLLCUWBatchSchema.setBatNo(tBatNo);
	     tLLCUWBatchSchema.setCaseNo(tCaseNo);
	   
	   	     
		    tLLCUWBatchSet.add( tLLCUWBatchSchema );
		    
		}
	}
 
 
  // 准备传输数据 VData
  VData tVData = new VData();	
  tVData.addElement(tLLCUWBatchSet);
  tVData.addElement(tG);  
 

  // 数据传输
 SecondUWFullMakeUI tSecondUWFullMakeUI = new SecondUWFullMakeUI();
 
	if (!tSecondUWFullMakeUI.submitData(tVData,""))
	{

      Content = " 保存失败，原因是: " + tSecondUWFullMakeUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tSecondUWFullMakeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
  System.out.println("Content:"+Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>

