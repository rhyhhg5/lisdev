<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLPendingLis.jsp
//程序功能：待处理案件统计
//创建日期：2005-10-10
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
   ExeSQL tExeSQL = new ExeSQL();
   String sql = "select case when current time>='08:00:00' and current time<'17:30:00' then 'Y' else 'N' end from dual"; 
   String sql1 = "select case when dayofweek(current date)=7 or dayofweek(current date)=1 then 'Y' else 'N' end from dual with ur";
   String rel = tExeSQL.getOneValue(sql);
   String rel1 = tExeSQL.getOneValue(sql1);
%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
   
   String CurrentDate = PubFun.getCurrentDate();
	CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
%>   
<script>

	var comCode = "<%=tG.ComCode%>";
	
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLPendingLis.js"></SCRIPT> 

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>理赔待处理案件统计</title>
</head>
		<body>   
			<Div  id= "divLLpending" style= "display: ''" >  
					<form action="./LLPendingLisSub.jsp" method=post name=fm target="f1print">
		<table class= common border=0 width=100%>		
							<TR  class= common>	

						  <TD  class= title>机构</TD><TD  class= input>
    			    <Input class= "codeno"  name=organcode  ondblclick="return showCodeList('comcode',[this,organname],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,organname],[0,1],null,null,null,1);" ><Input class=codename  name=organname></TD>
							<TD  class= title>处理人</TD><TD  class= input>
    			    <Input class= "codeno"  name=handlecode ondblclick="return showCodeList('optname',[this,handlename],[0,1],null,fm.organcode.value,'comcode',1);" onkeyup="return showCodeListKey('optname',[this,handlename],[0,1],null,organcode,'comcode',1);" ><Input class=codename  name=handlename></TD>

							
							</TR>
				    <TR  class= common>
				      <TD  class= title>案件类型</TD><TD class=input>
						<input class="codeno"
							CodeData="0|5^1|常规案件^2|批量案件^3|简易理赔案件^4|批量受理导入案件^5|回销预付赔款" name=ContType
							ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);"
							onkeydown="QueryOnKeyDown();"
							onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input class=codename name=ContTypeName>
					</TD></TR>
							<TR  class= common>	
							<TD  class= title>统计起期</TD>
							<TD  class= input> 
							<Input name=StartDate  style="width:160" class='coolDatePicker' value="<%=CurrentDate%>" dateFormat='short' verify="统计起期|Date" > </TD> 
							<TD  class= title>统计止期</TD>
							<TD  class= input > 
					
							<Input name=EndDate style="width:160" class='coolDatePicker' value="<%=CurrentDate%>" dateFormat='short' verify="统计止期|Date" > </TD> 							
							</TR>		
							
							 <TR  class= common>			 
									<TD  class= title>统   计</TD>
					<TD  class= input8> <input class=codeno CodeData="0|6^0|作业人员待处理案件统计^2|机构待处理案件统计"  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input class=codename name=DealWithName></TD>
         	 	    <TD  class= title>当前待处理机构</TD>
					<TD  class= input8> <input class=codeno CodeData="0|6^1|总公司^2|分公司"  name=MagcomType ondblclick="return showCodeListEx('MagcomType',[this,MagcomTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('MagcomType',[this,MagcomTypeName],[0,1],null,null,null,1);"><input class=codename name=MagcomTypeName></TD>
         	 	
							 		</TR>		 
						</table>
												
					<INPUT VALUE="打  印" class="cssButton" type="button" onclick="submitForm()"/>
						
				<input type="hidden" name=op value="">
	            <INPUT  type= hidden name="result" value=<%=rel %>>
	            <INPUT  type= hidden name="isweek" value=<%=rel1 %>>
			</form>
		</Div>  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 