//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
var turnPageItem = new turnPageClass(); 
var mainState="0";
var delEdorApp="0";
var delEdorMain="0";
//var cEdorNo;
var cPolNo;
var alertflag = "0"; //10-29
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
function easyQueryClick()
{
//	alert("here");
	// 初始化表格
	initItemGrid();
	initMainGrid();
	initAppGrid();
	
	// 书写SQL语句
	var strSQL = "";			
	  strSQL=" select a.EdorAcceptNo,a.OtherNo,a.EdorAppName,a.EdorState,a.EdorAppDate "
	       + "from LPedorapp a, LGWork b, llcontdeal d "
	       + "where a.edorAcceptNo = b.workNo "
	       + " and a.edoracceptno=d.edorno "
	       + "  and a.edorstate<>'0' and a.OtherNoType in ('1','3') "
	       + "  and b.statusno not in ('5','8') "
         + getWherePart( 'a.EdorAcceptNo','EdorAcceptNo' )
         + getWherePart( 'a.EdorState','EdorAppState')
         + "  and d.managecom like '" + fm.ManageCom.value + "%%' "
         + getWherePart( 'd.insuredno','OtherNo' )
         + getWherePart( 'a.OtherNoType','OtherNoType' )
         +"order by a.MakeDate desc,a.MakeTime desc";
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
//  alert(turnPage.strQueryResult);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = EdorAppGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
//  alert("here");
}

function getMainDetail()
{
	
	initItemGrid();
	initMainGrid();
	var tsel=EdorAppGrid.getSelNo(); 
	fm.all('hEdorAcceptNo').value=EdorAppGrid.getRowColData(tsel-1,1);
	
  
  var strSQL = "";
	
	
  strSQL=" select a.edorno,a.contno,a.edorState,a.makedate,a.maketime from lpedorMain a where a.EdorAcceptNo='"+EdorAppGrid.getRowColData(tsel-1,1)+"' and a.edorstate<>'0'  "
         +"order by a.MakeDate desc,a.MakeTime desc";
  
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  //turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //alert(strSQL);	


turnPageItem.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPageItem.strQueryResult) {
    delEdorApp="1";
    return false;
  }
  
  delEdorApp="0";  

  turnPageItem.arrDataCacheSet = clearArrayElements(turnPageItem.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPageItem.arrDataCacheSet = decodeEasyQueryResult(turnPageItem.strQueryResult,0,0,turnPageItem);
  
  //设置初始化过的MULTILINE对象
  turnPageItem.pageDisplayGrid = EdorMainGrid;    
          
  //保存SQL语句
  turnPageItem.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPageItem.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet2 = turnPageItem.getData(turnPageItem.arrDataCacheSet, turnPageItem.pageIndex, turnPageItem.pageLineNum);
  arrGrid = arrDataSet2;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet2, turnPageItem.pageDisplayGrid,turnPageItem);

 }

function getItemDetail()
{
	
	initItemGrid();
	var tsel=EdorMainGrid.getSelNo();


	fm.all('EdorNo').value=EdorMainGrid.getRowColData(tsel-1,1);
        fm.all('EdorMainState').value=EdorMainGrid.getRowColData(tsel-1,3);       
  
  var strSQL = "";
	
	
  strSQL=" select distinct a.edorNo,a.contno,a.edortype,a.edorstate,a.MakeDate,a.MakeTime,a.InsuredNo,a.PolNo from lpedorItem a where a.edorNo='" +fm.all('EdorNo').value+ "' and a.edorstate<>'0'  order by a.MakeDate desc,a.MakeTime desc"
  
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  //turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);	


turnPageItem.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPageItem.strQueryResult) {
    return false;
  }

  turnPageItem.arrDataCacheSet = clearArrayElements(turnPageItem.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPageItem.arrDataCacheSet = decodeEasyQueryResult(turnPageItem.strQueryResult,0,0,turnPageItem);
  
  //设置初始化过的MULTILINE对象
  turnPageItem.pageDisplayGrid = EdorItemGrid;    
          
  //保存SQL语句
  turnPageItem.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPageItem.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet2 = turnPageItem.getData(turnPageItem.arrDataCacheSet, turnPageItem.pageIndex, turnPageItem.pageLineNum);
  arrGrid = arrDataSet2;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet2, turnPageItem.pageDisplayGrid,turnPageItem);

 }

function CancelEdorApp()
{
  //20080529 modify zhanggm 送审或送核的工单不能撤销
  if(!checkUW())
  {
    return false;
  }
  
  if(!checkRight()) {
  	return false;
  }

    if(fm.CancelAppReasonCode.value == "")
    {
        alert("撤销原因不能为空");
        return false;
    }
    
	var arrReturn = new Array();
				
		var tSel = EdorAppGrid.getSelNo();
	
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else if(delEdorApp=="1")
		{

			fm.all('EdorAcceptNo').value = EdorAppGrid.getRowColData(tSel-1,1);
                        fm.all('EdorAppState').value=EdorAppGrid.getRowColData(tSel-1,4);			
			fm.all("Transact").value = "I&EDORAPP";
	    fm.all("DelFlag").value="3";     // flag 为3 删除申请 
    
			// showSubmitFrame(mDebug);

		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
			fm.submit();
			fm.CancelMainReasonCode.value='';
			fm.CancelItemReasonCode.value='';
			fm.CancelAppReasonCode.value='';
			fm.delMainReason.value='';
			fm.delAppReason.value='';
			fm.delItemReason.value=''; 
		  
		}else{
		    alert( "合同处理申请下还有批单，不可撤销。" );
		    fm.CancelMainReasonCode.value='';
			  fm.CancelItemReasonCode.value='';
			  fm.CancelAppReasonCode.value='';
			  fm.delMainReason.value='';
			  fm.delAppReason.value='';
			  fm.delItemReason.value=''; 
		    return false;
		}
}

function CancelEdorMain()
{
  //20080529 modify zhanggm 送审或送核的工单不能撤销
  if(!checkUW())
  {
    return false;
  }
  
  if(!checkRight()) {
  	return false;
  }
  
    if(fm.CancelMainReasonCode.value == "")
    {
        alert("撤销原因不能为空");
        return false;
    }
    
	var arrReturn = new Array();
				
		var tSel = EdorMainGrid.getSelNo();
	
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else
		{
			fm.all('EdorNo').value = EdorMainGrid.getRowColData(tSel-1,1);
			fm.all('ContNo').value=EdorMainGrid.getRowColData(tSel-1,2);
                        fm.all('EdorMainState').value=EdorMainGrid.getRowColData(tSel-1,3);			
			fm.all("Transact").value = "I&EDORMAIN";
			//fm.all()
	                fm.all("DelFlag").value="2";     // flag 为2 删除批单 
    
			// showSubmitFrame(mDebug);

		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		   fm.submit();

		}
		fm.CancelMainReasonCode.value='';
	  fm.CancelItemReasonCode.value='';
		fm.CancelAppReasonCode.value='';
	  fm.delMainReason.value='';
		fm.delAppReason.value='';
	  fm.delItemReason.value=''; 
}

	//撤销批单
function CancelEdorItem()
{
//20080529 modify zhanggm 送审或送核的工单不能撤销
  if(!checkUW())
  {
    return false;
  }
  
  if(!checkRight()) {
  	return false;
  }
  
    if(fm.CancelItemReasonCode.value == "")
    {
        alert("撤销原因不能为空");
        return false;
    }
    
	var arrReturn = new Array();
				
		var tSel = EdorItemGrid.getSelNo();
	
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else
		{
		  fm.all('EdorNo').value = EdorItemGrid.getRowColData(tSel-1,1);
			fm.all('ContNo').value=EdorItemGrid.getRowColData(tSel-1,2);
			fm.all('EdorType').value=EdorItemGrid.getRowColData(tSel-1,3);
			fm.all('EdorItemState').value=EdorItemGrid.getRowColData(tSel-1,4);
			fm.all('MakeDate').value=EdorItemGrid.getRowColData(tSel-1,5);
			fm.all('MakeTime').value=EdorItemGrid.getRowColData(tSel-1,6);	
			fm.all('InsuredNo').value=EdorItemGrid.getRowColData(tSel-1,7);
			fm.all('PolNo').value=EdorItemGrid.getRowColData(tSel-1,8);	
			//alert(fm.all('InsuredNo').value);
			//alert(fm.all('PolNo').value);
					
			fm.all("Transact").value = "I&EDORITEM";
	    fm.all("DelFlag").value="1";  //flag 为1 删除项目
	
			// showSubmitFrame(mDebug);
			var state =EdorItemGrid.getRowColData(tSel-1,4);
			//if(state=='2' || state=='0') 020805
			if(state=='0')
			{
				alert ("该合同处理项目已经确认,不能删除!");
			}
		  else
		  	{
					  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
					  fm.submit(); 
		  }
	}
			 fm.CancelMainReasonCode.value='';
			 fm.CancelItemReasonCode.value='';
		 	 fm.CancelAppReasonCode.value='';
			 fm.delMainReason.value='';
			 fm.delAppReason.value='';
			 fm.delItemReason.value=''; 
	
}

function afterSubmit( FlagStr, content,Result )
{

  showInfo.close();
  
  if (FlagStr == "Fail" )
 {   
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	         
			if(fm.all("DelFlag").value=="3") //删申请
			{ 	
			        //initForm();				
				easyQueryClick();
			 }
			 if(fm.all("DelFlag").value=="2") //删批单
			{ 				
				getMainDetail();
				initItemGrid();
			}
			if(fm.all("DelFlag").value=="1") //删项目
			{
				getItemDetail();
				getMainDetail(); 
			}			 				
	
			    
			    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
	
		//电话催缴时因客户不愿缴费而撤销合同处理申请
		if(fm.all("DelFlag").value=="3") //删申请
		{ 	
			if(loadFlag == "TaskPhoneHasten")
			{
				top.close();
				top.opener.top.focus();
				top.opener.top.fraInterface.afterAppCancel();
			}
		}
	}
}

//20080529 add zhanggm 送审或送核的工单不能撤销
//20081008 modify zhanggm 送审的工单可以撤销，送核的工单不能撤销
function checkUW()
{
  var tSel = EdorAppGrid.getSelNo();
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择合同处理申请信息，再点击申请撤销按钮！" );
    return false;
  }
  var edorno = EdorAppGrid.getRowColData(tSel-1,1);
  //送审
  /*
  var sql = "select 1 from LGWork where StatusNo in ('4') and WorkNo = '" + edorno + "' with ur";
  var result = easyQueryVer3(sql);
  var returnInfo = "送审或送核不能撤销！";
  if(result)
  {
    returnInfo = "合同处理申请 " + edorno + " 现在是待审批状态，不能撤销！";
    alert(returnInfo);
    return false;
  }
  */
  //送核
  sql = "select 1 from LPEdorApp where UWState in ('5') and EdorAcceptNo = '" + edorno + "' with ur";
  result = easyQueryVer3(sql);
  if(result)
  {
    returnInfo = "合同处理申请 " + edorno + " 现在是核保状态，不能撤销！";
    alert(returnInfo);
    return false;
  }
  return true;
}

function checkRight(){
	//案件处理人用户失效时，支持其有效上级（包含非直接上级）做理赔合同处理
	//Add By Houyd	
	//首先查询该受理号对应的申请人
	var appoperatorSQL="select appoperator from llcontdeal where edorno='"+fm.EdorAcceptNo.value+"'";
	var appoperatorResult = easyExecSql(appoperatorSQL);
	var appoperator=appoperatorResult[0][0];//受理申请人
	//其次查询受理人是否有权限
	var checkRightSQL="select 1 from llclaimuser where usercode='"+appoperator+"' and stateflag='1'";
	var result = easyExecSql(checkRightSQL);
	if (result) {
  		if (fm.Operator.value==appoperator) {
  			return true;
   	 	}else{
   	 		alert("您没有权限进行处理，只能由："+appoperator+"操作！");
   	 		return false;
   	 	}
  	}else{
		var upUser = appoperator;	
		while (true) {
			var rightSQL = "select a.upusercode,a.stateFlag"
	                 + " From llclaimuser a where a.usercode='"+appoperator+"'";
	    	var rightResult = easyExecSql(rightSQL);

	    	if (!rightResult) {
	    		alert("没有权限进行操作");
	    		return false;
	    	}
	    
	    	if ("1"==rightResult[0][1]) {
	    		break;
	    	}
	            
	    	upUser = rightResult[0][0];
	    	appoperator = upUser;
	  }	  
	  if (fm.Operator.value==upUser) {
	  	return true;
	  }else{
	  	alert("您没有合同处理申请权限，请用"+upUser+"进行处理");
	  	return false;
	  }
  }
}