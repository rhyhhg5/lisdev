<%
//程序名称：LLSurvFeeInit.jsp
//程序功能：
//创建日期：2005-2-26 12:07
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
var turnPage=new turnPageClass();
function initInpBox(){ 
  try{
  }
  catch(ex){
    alert("在LLSurvFeeInit.jsp-->InitInpBox1函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox(){  
  try{
  }
  catch(ex){
    alert("在LLSurvFeeInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(){
  try{
	initInpBox(); 
	initGiveFeeGrid();
  	initIndirectFeeGrid();
  	initInqFeeGrid();
  	queryInqFee();
  	queryIndFee();
  	queryGiveFee();
  }
  catch(re){
    alert("LLSurvFeeInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

// 保单信息列表的初始化
function initInqFeeGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("理赔号","80px","50","0");
    iArray[2]=new Array("调查序号","40px","50","0");
    iArray[3]=new Array("直接调查费","40px","50","0");
    iArray[4]=new Array("调查人","40px","60","0");
    iArray[5]=new Array("审核人","40px","60","0");
    iArray[6]=new Array("调查号","40px","50","3");

    InqFeeGrid = new MulLineEnter("fm","InqFeeGrid");
    InqFeeGrid.mulLineCount =3;
    InqFeeGrid.displayTitle = 1;
    InqFeeGrid.locked = 1;
    InqFeeGrid.canChk =1;
    InqFeeGrid.hiddenPlus=1;
    InqFeeGrid.hiddenSubtraction=1;
    InqFeeGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

// 保单信息列表的初始化
function initIndirectFeeGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("理赔号","80px","50","0");
    iArray[2]=new Array("间接调查费","40px","50","0");
    iArray[3]=new Array("录入人","40px","60","0");
    iArray[4]=new Array("审核人","40px","60","0");

    IndirectFeeGrid = new MulLineEnter("fm","IndirectFeeGrid");
    IndirectFeeGrid.mulLineCount =3;
    IndirectFeeGrid.displayTitle = 1;
    IndirectFeeGrid.locked = 1;
    IndirectFeeGrid.canChk = 1;
    IndirectFeeGrid.hiddenPlus=1;
    IndirectFeeGrid.hiddenSubtraction=1;
    IndirectFeeGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

function initGiveFeeGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10","0");
    iArray[1]=new Array("调查费结算号","80px","50","0");
    iArray[2]=new Array("结算日期","40px","50","0");
    iArray[3]=new Array("结算金额","40px","60","0");
    iArray[4]=new Array("操作人","40px","60","0");
    iArray[5]=new Array("给付日期","40px","50","0");

    GiveFeeGrid = new MulLineEnter("fm","GiveFeeGrid");
    GiveFeeGrid.mulLineCount =3;
    GiveFeeGrid.displayTitle = 1;
    GiveFeeGrid.locked = 1;
    GiveFeeGrid.canChk = 0;
    GiveFeeGrid.canSel = 1;
    GiveFeeGrid.hiddenPlus=1;
    GiveFeeGrid.hiddenSubtraction=1;
    GiveFeeGrid.loadMulLine(iArray);
    GiveFeeGrid.selBoxEventFuncName = "queryGiveInfo";
  }
  catch(ex){
    alter(ex);
  }
}

</script>
