var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var tSaveType="";

//#2000 团体赔付汇总细目表增加下载功能,借用理赔案件-->理赔处理-->团体结案 页面中“团体汇总明细”
//全国各地区均使用Excel打印
function NewGrpColPrt()
{
	var tRgtNo = fm.GrpRgtNo.value;
	var dztype = false;
	for(i = 0; i <fm.IsBatch.length; i++){
		if(fm.IsBatch[i].checked){
			fm.selno.value=fm.IsBatch[i].value;
			break;
		}
	}
	if(fm.selno.value == '1'){
		if (tRgtNo == '' || tRgtNo == null)
		{
	      	alert("请输入团体批次号或者进行查询");
	      	return ;
		}
	}else{
		alert("赔付汇总细目表只支持‘批次打印’");
		return ;
	}
	
	var ActuGetNo="";
	strSQL="select rgtstate,togetherflag from llregister where rgtno='"+tRgtNo+"'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
	      rgtstate = arrResult[0][0];
	      togetherflag = arrResult[0][1];

    }
  	if(rgtstate == "01" || rgtstate == "02")
  	{
  		alert("此状态下不能打印！");
  		return;
  	}
   	var newWindow = window.open("PayColPrtBJ.jsp?RgtNo=" + tRgtNo+ "&fmtransact=PRINT");			
}

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ASCII码是13
	if(keycode=="13")
	{
//		queryGrp();
	}
}

function queryGrp()
{
	strSql = "select caseno from llcase where 1=1 "+
		getWherePart("rgtdate","RgtDateS",">=")+getWherePart("rgtdate","RgtDateE","<=")+
		" and rgtstate in ('09','11','12') and mngcom = '"+fm.ComCode.value+"' order by caseno ";
	fm.sql.value = strSql;
	var arr = easyExecSql(strSql);
	if(arr)
	{
		count = arr.length-1;
		fm.StartCaseNo.value = arr[0][0];
		fm.EndCaseNo.value = arr[count][0];
	}
	else{
	  fm.StartCaseNo.value='';
	  fm.EndCaseNo.value='';
	}
}

		//对CodeSelect选择后的判
function afterCodeSelect( cCodeName, Field )
{
}
function getstr()
{
}



function submitForm()
{
	//初始化fm.action
	fm.action="./LLBatchPrtSave.jsp";
	var dztype = false;
	for(i = 0; i <fm.IsBatch.length; i++){
		if(fm.IsBatch[i].checked){
			fm.selno.value=fm.IsBatch[i].value;
			break;
		}
	}
	switch(fm.selno.value){
		case "1":
			if (fm.GrpRgtNo.value == null || fm.GrpRgtNo.value == "") {
				alert("请输入团体批次号或者进行查询");
				return false;
			}
/*		if (fm.BatchNumber.value==""||fm.BatchNumber.value ==null||fm.BatchNumber.value=="0")
		{
			alert("该批次号不存在或该批次下无可打印案件！")
			return false;
		}	
*/			
		break;
		case "2":
			if ((fm.StartCaseNo.value == null || fm.StartCaseNo.value == "")
					&& (fm.EndCaseNo.value == null || fm.EndCaseNo.value == "")
					&& (fm.RgtDateS.value == null || fm.RgtDateS.value == "")
					&& (fm.RgtDateE.value == null || fm.RgtDateE.value == "")
					&& (fm.EndDateS.value == null || fm.EndDateS.value == "")
					&& (fm.EndDateE.value == null || fm.EndDateE.value == "")) {
				alert("请输入连号打印条件");
				return false;
			}
			
			if ((fm.RgtDateS.value != null && fm.RgtDateS.value != "" 
				&& (fm.RgtDateE.value == null || fm.RgtDateE.value == ""))
					|| (fm.RgtDateE.value != null && fm.RgtDateE.value != ""
						&& (fm.RgtDateS.value == null || fm.RgtDateS.value == ""))) {
				alert("请输入受理日期");
				return false;
			}
			
			if ((fm.EndDateS.value != null && fm.EndDateS.value != "" 
				&& (fm.EndDateE.value == null || fm.EndDateE.value == ""))
					|| (fm.EndDateE.value != null && fm.EndDateE.value != ""
						&& (fm.EndDateS.value == null || fm.EndDateS.value == ""))) {
				alert("请输入结案日期");
				return false;
			}
			
			if ((fm.StartCaseNo.value != null && fm.StartCaseNo.value != "" 
				&& (fm.EndCaseNo.value == null || fm.EndCaseNo.value == ""))
					|| (fm.EndCaseNo.value != null && fm.EndCaseNo.value != ""
						&& (fm.StartCaseNo.value == null || fm.StartCaseNo.value == ""))) {
				alert("请输入起始终止案件号");
				return false;
			}
			
			if (fm.StartCaseNo.value != null && fm.StartCaseNo.value != ""
				&& fm.EndCaseNo.value != null && fm.EndCaseNo.value != "") {
				var SNo=fm.StartCaseNo.value.substring(3);
				var ENo = fm.EndCaseNo.value.substring(3);
				var MngFlagS = fm.StartCaseNo.value.substring(0,3);
				var MngFlagE = fm.EndCaseNo.value.substring(0,3);
				
				if(MngFlagS!=MngFlagE) {
					alert("起始案件号和终止案件号来自于不同的机构，无法确定打印项目！");
					return false;
				}
				if ((SNo/1)>(ENo/1)) {
					alert("起始案件号不能大于终止案件号！");
					return false;
				}
			}
		break;
		
		case "3":
		if (fm.SingleCaseNo.value==""||fm.SingleCaseNo.value ==null)
		{
			alert("请填入需要打印的单号！")
			return false;
		}
		break;
	}
	fm.Detail.value='off';
	fm.Notice.value='off';
    fm.GrpDetail.value='off';
    fm.GetDetail.value='off';
    fm.DetailX.value='off';
    fm.Decline.value='off';
    fm.DetailExcel.value='off';
    fm.GrpDetailExcel.value='off';

   for( i=0;i<=7;i++)
   {
        if(fm.Prt[i].checked){
         	dztype=true;
         	switch(i){
         	  case 0:  fm.Notice.value='on';  break;
         	  case 1:  fm.Detail.value='on';  break;
         	  case 2:  fm.GrpDetail.value='on';  break;
         	  case 3:  fm.GetDetail.value='on';  break;
         	  case 4:  fm.DetailX.value='on';  break;
         	  case 5:  fm.Decline.value='on';  break;
         	  case 6:  fm.DetailExcel.value='on';  break;
         	  case 7:  fm.GrpDetailExcel.value='on'; break;
         	}
         	break;
        }   	
   }
//	if (fm.Prt.checked||fm.GrpDetailPrt.checked||fm.NoticePrt.checked||fm.GetDetailPrt.checked)
//	{
//		dztype=true;
//		if(fm.DetailPrt.checked)
//			fm.Detail.value='on';
//		if(fm.NoticePrt.checked)
//			fm.Notice.value='on';
//		if(fm.GrpDetailPrt.checked)
//			fm.GrpDetail.value='on';
//       if(fm.GetDetailPrt.checked)
//           fm.GetDetail.value='on';//给付凭证打印
//	}
	if (dztype==false )
	{
		alert("请选定打印项目！")
		return false;
	}
	
	if(fm.DetailX.value=='on'){
		if(fm.selno.value=='1'){
			fm.action="./LLBatchPrtSaveCVS.jsp";
		}else{
		    alert("理赔给付细目汇总表只针对批次打印！");
		    return false;
		}	
	}
	//TODO:理赔给付明细表（Excel）打印
	if(fm.DetailExcel.value=='on'){
		if(fm.selno.value=='1' || fm.selno.value=='2' || fm.selno.value=='3'){
			fm.action="./LLBatchPrtSaveExcel.jsp";			
		}else{
		    return false;
		}	
	}
	//#2000 团体赔付汇总细目表增加下载功能,借用理赔案件-->理赔处理-->团体结案 页面中“团体汇总明细”
	//修改，全国均使用Excel下载模式
	if(fm.GrpDetailExcel.value=='on'){
		if(fm.selno.value=='1'){
			NewGrpColPrt();
			return;
		}else{
		    return false;
		}	
	}
	if(fm.Decline.value=='on'){
	     if(fm.selno.value=='1'){
	     	var grprgtno=fm.GrpRgtNo.value;
	     	if(!grprgtno.match("[P][1-9]+")){
	     	  alert("请输入正确的批次号");
	     	  return false;
	     	}
	        strSql = "select 1 from llcase a,LLClaimDecline b where 1=1 "+
		             getWherePart("b.rgtno","GrpRgtNo")+
		             " and a.rgtstate in ('11','12') and a.caseno=b.caseno with ur ";
	        var arr = easyExecSql(strSql);
	        if(arr==null){
	           alert("该批次下无拒赔案件信息或拒赔案件缺少足够的拒赔信息！");
	           return false;
	        }
	        
	     }
	     
	     fm.action="../f1print/ClaimDeclinePrintP.jsp";
	}
	
    var showStr="正在打印，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    //var urlStr="./LLBatchPrtSave.jsp";
	//showInfo2=window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
}

function afterSubmit(  FlagStr, content,outname,outpathname  )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
   // showDiv(inputButton,"false");
   if(outname==null){
      var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else{
       var varSrc="outname="+outname+"&outpathname="+outpathname;
       window.open("../case/LLBatchPrtInputDown.jsp?"+varSrc);
   } 
  }
}


function BatchPrint()
{
  	fm.fmtransact.value = "batch";
		submitForm();
}

function BatchQueue()
{
  	fm.fmtransact.value = "INSERT";
		submitForm();
}

function QueryBatch()
{
	var varSrc="&RgtState=03";
	var newWindow = OpenWindowNew("./FrameMainGrpRgtQuery.jsp?Interface=GrpRgtQuery.jsp"+
	varSrc,"批量打印","left");
}

function Reset()
{
  fm.GrpRgtNo.value='';
  fm.GrpName.value='';
//  fm.BatchNumber.value='';
  fm.RgtDateS.value = '';
  fm.RgtDateE.value = '';
  fm.StartCaseNo.value = '';
  fm.EndCaseNo.value='';
  fm.SingleCaseNo.value='';
  fm.Prt[0].checked=false;
  fm.Prt[1].checked=false;
  fm.Prt[2].checked=false;
  fm.Prt[3].checked=true;
  fm.Prt[4].checked=false;
  fm.Prt[5].checked=false;
  fm.Prt[6].checked=false;
  fm.Prt[7].checked=false;
  
  fm.IsBatch(0).checked = true;
  fm.IsBatch(1).checked = false;
  fm.IsBatch(2).checked = false;
}
