<%
//程序名称：ClaimUnderwriteInput.js
//程序功能：审批审定页面函数
//创建日期：2002-06-19 11:10:36
//创建人  ：Wujs
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>


<script language="JavaScript">

//输入框的初始化（单记录部分）
function initInpBox(){
  try{
    var tRgtNo= '<%=request.getParameter("RgtNo")%>';
    fm.all('RgtNo').value = tRgtNo=='null'?'':tRgtNo;
     var tGrpContNo= '<%=request.getParameter("GrpContNo")%>';
    fm.all('GrpContNo').value = tGrpContNo=='null'?'':tGrpContNo;
  }
  catch(ex){
    alert("ClaimUnderwriteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm(){
  try{
  	initInpBox();
		initAccountGrid();
		queryLLSprint();
 	  <%GlobalInput mG = new GlobalInput();
  		mG=(GlobalInput)session.getValue("GI");
  	%>
  	 fm.Handler.value = "<%=mG.Operator%>";
     var strSQL2="select grpname from llregister where  rgtno='"+fm.RgtNo.value+"'";
		 var arrResult2 = easyExecSql(strSQL2);
		 if(arrResult2 != null && arrResult2 !="" ){
		 fm.GrpName.value = arrResult2[0][0];
		}
	}

  catch(re)
  {
    alert("ClaimUnderwriteInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

function initAccountGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("团体保单号","140px","100","0");
      iArray[3]=new Array("批次号","140px","100","0");
      iArray[4]=new Array("分案号","140px","100","0");
      iArray[2]=new Array("单位名称","140px","0","0");
      iArray[5]=new Array("应付金额","140px","0","0");
      iArray[6]=new Array("实付金额","140px","0","0");
      iArray[7]=new Array("拨付金额","140px","0","0");
      iArray[8]=new Array("拨付日期","140px","0","0");
      AccountGrid = new MulLineEnter("fm","AccountGrid");
      AccountGrid.mulLineCount =3;
      AccountGrid.displayTitle = 1;
      AccountGrid.locked = 1;
      AccountGrid.canSel =0;
      AccountGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      AccountGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
      //  CaseGrid. selBoxEventFuncName = "onSelSelected";
      AccountGrid.loadMulLine(iArray);
    }
      catch(ex)
      {
        alert(ex);
      }
}

</script>