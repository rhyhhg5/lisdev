/*******************************************************************************
* Name: RegisterInput.js
* Function:立案主界面的函数处理程序
* Author:LiuYansong
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var case_flag ;//判断是否选择了分案号,初始化时是1，若没有选中则是0。
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tSaveFlag = "0";
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
/*******************************************************************************
* Name        :main_init
* Author      :LiuYansong
* CreateDate  :2003-12-16
* ModifyDate  :
* Function    :初始化主程序
* Parameter   :A_jsp---fm.action;Target_Flag (0--无target，1有target),T_jsp--target 对应的页面 ;Info---提示信息
*
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info)
{
	var i = 0;
	var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action ="./"+A_jsp;
	//alert("target标志是"+Target_Flag);
	if(Target_Flag=="1")
	{
		fm.target = "./"+T_jsp;
	}
else
	{
		fm.target = "fraSubmit";
	}
	//showSubmitFrame(mDebug);
	fm.submit(); //提交
}

function showTemPolInfo()
{
	var row;
	var a_count=0;//判断选中了多少行
	var t = CaseGrid.mulLineCount;//得到Grid的行数
	for(var i=0;i<t;i++)
	{
		varCount = CaseGrid.getChkNo(i);
		if(varCount==true)
		{
			a_count++;
			row=i;
		}
	}
	if(a_count>1)
	{
		alert("您只能选中一行记录！");
		return;
	}
else if(a_count<1)
	{
		alert("请您选中一条分案记录!");
		return;
	}
else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
	{
		alert("请您先执行保存操作！");
		return;
	}
else
	{
		var varSrc = "&CaseNo=" + CaseGrid.getRowColData(row,1);
		varSrc += "&InsuredNo=" + CaseGrid.getRowColData(row,2);
		varSrc += "&CustomerName=" + CaseGrid.getRowColData(row,3);
		varSrc += "&AccidentType=" + CaseGrid.getRowColData(row,8);
		varSrc += "&RgtNo=" + fm.RgtNo.value;
		var newWindow = window.open("./FrameMainLSBDXX.jsp?Interface=TemPolInfoInput.jsp"+varSrc,"CasePolicyInput",'width=700,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
	}
}
//处理保单明细
function showCasePolicy()
{
	var row;
	var a_count=0;//判断选中了多少行
	var t = CaseGrid.mulLineCount;//得到Grid的行数
	for(var i=0;i<t;i++)
	{
		varCount = CaseGrid.getChkNo(i);
		if(varCount==true)
		{
			a_count++;
			row=i;
		}
	}
	if(a_count>1)
	{
		alert("您只能选中一行记录！");
		return;
	}
else if(a_count<1)
	{
		alert("请您选中一条分案记录!");
		return;
	}
else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
	{
		alert("请您先执行保存操作！");
		return;
	}
else
	{
		var varSrc = "&CaseNo=" + CaseGrid.getRowColData(row,1);
		varSrc += "&InsuredNo=" + CaseGrid.getRowColData(row,2);
		varSrc += "&CustomerName=" + CaseGrid.getRowColData(row,3);
		varSrc += "&AccidentType=" + CaseGrid.getRowColData(row,8);
		varSrc += "&RgtNo=" + fm.RgtNo.value;
		var newWindow = window.open("./FrameMainFABD.jsp?Interface=CasePolicyInput.jsp"+varSrc,"CasePolicyInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
	}

}
//费用明细信息
function showFeeInfo()
{
	var row;
	var a_count=0;//判断选中了多少行
	var t = CaseGrid.mulLineCount;//得到Grid的行数
	for(var i=0;i<t;i++)
	{
		varCount = CaseGrid.getChkNo(i);
		if(varCount==true)
		{
			a_count++;
			row=i;
		}
	}
	if(a_count>1)
	{
		alert("您只能选中一行记录！");
		return;
	}
else if(a_count<1)
	{
		alert("请您选中一条分案记录!");
		return;
	}
else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
	{
		alert("请您先执行保存操作！");
		return;
	}
else
	{
		fm.DisplayFlag.value="0";//标志是在立案界面中进入的费用明细
		var varSrc = "&RgtNo=" +fm.RgtNo.value;
		varSrc += "&CaseNo=" + CaseGrid.getRowColData(row,1);
		varSrc += "&CustomerNo=" + CaseGrid.getRowColData(row,2);
		varSrc += "&CustomerName=" + CaseGrid.getRowColData(row,3);
		var newWindow = window.open("./FrameMainFAZL.jsp?Interface=ICaseCureInput.jsp"+varSrc,"ICaseCureInput");
	}
}
function showAccident()
{

	var varSrc ="";
	var newWindow = window.open("./FrameMainAccident.jsp?Interface=CaseAccidentInput.jsp"+varSrc,"CaseAccidentInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	FlagDel = FlagStr;
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
else
	{
		var strsql = "select a.RgtState,b.codename,a.Operator,a.MakeDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
		+" and b.codetype='llrgtstate' and b.code=a.rgtstate"
		arrResult = easyExecSql(strsql);

		if ( arrResult )
		{
			fm.RgtState.value= arrResult[0][1];
			fm.Handler.value= arrResult[0][2];
			//fm.ModifyDate.value= arrResult[0][4];
			fm.ModifyDate.value = getCurrentDate();

			initTitle();
			ClientafterQuery( fm.RgtNo.value,fm.CaseNo.value );
		}

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//showDiv(operateButton,"true");
		//showDiv(inputButton,"false");
		if(fm.appReasonCode[04].checked == true)
		{
			var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNoQ.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerTypeQ.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
			showInfo=OpenWindowNew(pathStr,"EventInput","middle");
		}
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//取消按钮对应操作
function cancelForm()
{
	showDiv(operateButton,"true");
	showDiv(inputButton,"false");
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
	//下面增加相应的代码
	showDiv(operateButton,"false");
	showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (confirm("您确实想修改该记录吗?"))
	{
		var rowNum1=CaseGrid.mulLineCount;
		var rowNum2=AffixGrid.mulLineCount;
		dealCaseGetMode();

		if(fm.BankAccNo.value!=fm.RgtantMobile.value)
		{
			alert("账号录入错误，请您从新录入！！！");
			return;
		}
		for(var j=0;j<rowNum1;j++)
		{
			var tValue1 = CaseGrid.getRowColData(j,2);
			if ((tValue1=="")||(tValue1=="null"))
			{
				alert("请您录入客户号，若没有该用户请将分立案录入框中的该条记录取消！");
				return;
			}
		}
		for(var m=0;m<rowNum2;m++)
		{
			var tValue2 = AffixGrid.getRowColData(m,1);
			if ((tValue2=="")||(tValue2=="null"))
			{
				alert("请您录入立案附件信息！");
				return;
			}
		}
		if ((fm.RgtantName.value=="")||(fm.RgtantName.value=="null"))
		{
			alert ("请您输入申请权利人姓名姓名!");
			return;
		}
		if ((fm.RgtDate.value=="")||(fm.RgtDate.value=="null"))
		{
			alert("请您输入立案日期!");
			return;
		}
		if ((fm.AccidentDate.value=="")||(fm.AccidentDate.value=="null"))
		{
			alert("请您输入出险日期!");
			return;
		}
		if(fm.CaseState.value=="已撤件")
		{
			alert("该案件已撤件，不能对其进行修改！！！");
			return;
		}
	else
		{
			fm.fmtransact.value = "UPDATE||MAIN";
			main_init("RegisterUpdate.jsp","0","_blank","修改");
		}
	}
else
	{
		mOperate="";
		alert("您取消了修改操作！");
	}
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	//下面增加相应的代码
	//window.open("ClientCaseQueryInput.jsp");
	OpenWindowNew("ClientCaseQueryInput.jsp","","middle",720,400);
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	alert("您不能删除立案信息！！！");
	return;
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
else
	{
		cDiv.style.display="none";
	}
}
function submitFormSurvery()
{

	var row;
	var a_count=0;//判断选中了多少行
	var t = CaseGrid.mulLineCount;//得到Grid的行数
	for(var i=0;i<t;i++)
	{
		varCount = CaseGrid.getChkNo(i);
		if(varCount==true)
		{
			a_count++;
			row=i;
		}
	}
	if(a_count>1)
	{
		alert("您只能选中一行记录！");
		return;
	}
else if(a_count<1)
	{
		alert("请您选中一条分案记录!");
		return;
	}
else if((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
	{
		alert("请您先执行保存操作！");
		return;
	}
else
	{
		var varSrc = "&CaseNo=" + fm.CaseNo.value;
		varSrc += "&InsuredNo=" + fm.InsuredNo.value;
		varSrc += "&CustomerName=" + fm.Name.value;
		varSrc += "&RgtNo=" + fm.RgtNo.value;
		varSrc += "&StartPhase=0";
		varSrc += "&Type=1";
		showInfo = window.open("./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
	}


}
//打印理赔申请书的函数
function PLPsqs()
{
	main_init("PLPsqs.jsp","1","f1print","打印");
	showInfo.close();
}
//连接报案查询界面的函数
function RgtQueryRpt()
{
	tSaveFlag = "Rpt";
	showInfo=window.open("FrameReportQuery.jsp");
}

//案件撤销的程序
function CaseCancel()
{
	if (confirm("您确实想修改该记录吗?"))
	{
		if(fm.RgtNo.value=="")
		{
			alert("请您先查询出要进行撤销的案件");
			return;
		}
		if(fm.RgtReason.value=="")
		{
			alert("请您先录入案件撤销原因，否则不能进行撤销！！！");
			return;
		}
		main_init("CaseCancel.jsp","0","_blank","撤销");
	}
else
	{
		alert("您取消了案件撤销操作！！！！！");
	}
}
/*******************************************************************************
* Name         :dealCaesGetMode
* Author       :LiuYansong
* CreateDate   :2003-12-16
* ModifyDate   :
* Function     :处理保险金领取方式的函数，若是4或7则要录入关于银行的一些信息
*
*/
function dealCaseGetMode()
{
	mode_flag="1";
	if(fm.CaseGetMode.value=="4"||fm.CaseGetMode.value=="7")
	{
		if(fm.BankCode.value=="")
		{
			alert("请您录入开户银行的信息！！！");
			mode_flag="0";
			return;
		}
		if(fm.AccName.value=="")
		{
			alert("请您录入户名信息！！！！");
			mode_flag="0";
			return;
		}
		if(fm.BankAccNo.value=="")
		{
			alert("请您录入账户信息！！！");
			mode_flag="0";
			return;
		}
	}
}

function afterQuery( rptNo )
{
	if ( tSaveFlag=="Rpt" )
	{
		if ( rptNo!=null )
		{
			//报案信息
			var strSQL="select RptNo,RptDate"
			//+",AccidentDate,AccidentSite,AccidentReason,AccidentCourse+
			+" from llreport where rptno='"+ rptNo+"'";
			if ( EasyShowForm(strSQL,1,fm) )
			{
				fm.AccidentSite.className = "readonly";
				fm.AccidentSite.readOnly ="true"
				fm.RptDate.className = "readonly"
				fm.RptDate.readOnly ="true"
				fm.AccidentDate.className = "readonly";
				fm.AccidentDate.readOnly ="true"
				fm.AccidentReason.readOnly ="true"
				fm.AccidentCourse.readOnly ="true"
				fm.Remark.readOnly ="true"
				//fm..readOnly ="true";
			}
		else
			{
				fm.AccidentSite.className = "common8";
				fm.AccidentSite.readOnly ="true"
				fm.RptDate.className = "common8";
				fm.RptDate.readOnly ="true"
				fm.AccidentDate.className = "common8";
				fm.AccidentDate.readOnly ="true"
				fm.AccidentReason.readOnly ="false"
				fm.AccidentCourse.readOnly ="false"
				fm.Remark.readOnly ="false"
			}
			//相关客户
			strSQL = "select '',CustomerNo,CustomerName,'','','','',AccidentType,subrptno "
			+"from LLSubReport where SubRptNo in "
			+" ( select distinct SubRptNo from LLreportRela where rptno='" + rptNo + "')";
			turnPage.queryModal(strSQL, CaseGrid);
		}
	}
	//execEasyQuery(strSQL);

}

function InccidentInput()
{
	showInfo = window.open("./FrameMain.jsp?Interface=LLClientInccidentInput.jsp","",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}
function ClientQuery()
{
	var  wherePart = getWherePart("a.InsuredNo", 'CustomerNoQ' )
	+ getWherePart("a.name", 'CustomerTypeQ' )
	+ getWherePart( 'a.ContNo','ContNo' );
	if ( wherePart == "")
	{
		alert("请输入查询条件");
		return false;
	}

	var strSQL ="select distinct a.insuredno,a.name,a.sex,a.birthday,"
	+"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=a.IDType),"
	+"a.idno,'' ,'',a.IDType "
	+"from lcinsured a where 1=1 "
	+ wherePart
	//+ " and b.AddressNo= a.AddressNo ";
	var arr= easyExecSql( strSQL,1,0,1 );


	if ( arr==null )
	{
		alert("没有符合条件的数据");
		return false;
	}
	try
	{
		//如果查询出的数据是一条，显示在页面，如果是多条数据填
		//出一个页面，显示在mulline中
		if(arr.length==1)
		{
			afterLLRegister(arr);
			QueryEvent()


		}
	else
		{
			var varSrc = "&CustomerNo=" + fm.CustomerNoQ.value;
			varSrc += "&CustomerName=" + fm.CustomerTypeQ.value;
			varSrc += "&ContNo=" + fm.ContNo.value;
			varSrc += "&IDType=" + fm.tIDType.value;
			varSrc += "&IDNo=" + fm.tIDNo.value;
			showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
		}

	}catch(ex)
	{
		alert("错误:"+ ex.message);
	}

}

function openAffix()
{
	var varSrc ="";
	//showInfo = window.open("./CaseAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value);
	pathStr="./CaseAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value+"&LoadC="+fm.LoadC.value;
	//	  alert(pathStr);
	showInfo = OpenWindowNew(pathStr,"","middle");
}

function submitFormSurvery1()
{

	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNoQ.value;
	varSrc += "&CustomerName=" + fm.CustomerTypeQ.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	varSrc += "&StartPhase=0";
	varSrc += "&Type=1";
	//alert(varSrc);
	//showInfo = window.open("./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
	pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}

function submitForm()
{
	if(fm.tIDType.value=="0" && (fm.tIDNo.value.length!=18 && fm.tIDNo.value.length!=15)){
		alert("客户身份证位数不对，请重新输入");
		return false;
	}
	if(fm.IDType.value=="0" && (fm.IDNo.value.length!=18 && fm.IDNo.value.length!=15)){
		alert("申请人身份证位数不对，请重新输入");
		return false;
	}
	var EventCount=EventGrid.mulLineCount;
	var chkFlag=false;
	for (i=0;i<EventCount;i++)
	{
		if(EventGrid.getChkNo(i)==true)
		{
			chkFlag=true;
		}
	}
	if (chkFlag==false){
		alert("请选中关联事件");
		return false;
	}
	if(fm.AppealFlag.value=="0")
	{ //处理申请确�
		var nowDate=getCurrentDate();
		if(EventGrid.checkValue2(EventGrid.name,EventGrid)== false) return false;

		for(var rowNum=0;rowNum<EventGrid. mulLineCount;rowNum++)
		{
			var happenDate=EventGrid.getRowColData(rowNum,2);
			var happenDate1=modifydate(happenDate);
			var day=dateDiff(happenDate1,nowDate,"D");
			if(day<0){
				alert("您在第"+(rowNum+1)+"行输入的发生日期不应晚于当前时间");
				return false;
			}
			var inDate=EventGrid.getRowColData(rowNum,7);
			inDate=modifydate(inDate);
			var outDate=EventGrid.getRowColData(rowNum,8);
			if (outDate.length!=0)
			{
				outDate=modifydate(outDate);
				day1=dateDiff(inDate,outDate,"D");
				if(dateDiff(inDate,outDate,"D")<0){
					alert("您在第"+(rowNum+1)+"行输入的出院日期不应早于住院时间");
					return false;
				}
			}
		}



		if ( fm.CaseNo.value !='' )
		{
			alert( "案件已保存，您不能执行此操作");
			return false;
		}
		if(fm.CustomerNoQ.value==null||fm.CustomerNoQ.value=="")
		{
			alert("请输入客户数据");
		}
		else
		{
			if (fm.all('rgtflag').value == "1")
			{
				fm.all('operate').value = "UPDATE||MAIN";

				strSQL = " select AppPeoples from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
				arrResult = easyExecSql(strSQL);
				if(arrResult != null)
				{
					var AppPeoples;
					try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
				}
				else
				{
					alert("团体案件信息查询失败！");
					return;
				}

				strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
				arrResult = easyExecSql(strSQL);
				if(arrResult != null)
				{
					var RealPeoples;
					try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
				}
				if ( (eval(AppPeoples) - eval(RealPeoples)) < 5 )
				{
					alert("团体申请人数：" + AppPeoples + "实际已录入人数：" + RealPeoples);
				}
				if ( eval(RealPeoples) >= eval(AppPeoples) )
				{
					if (!confirm("团体申请人数：" + AppPeoples + ", 实际已录入人数：" + RealPeoples + "你确认要补加个人客户吗?"));
					{
						return;
					}
				}
			}
			else
			{
				fm.all('operate').value = "INSERT||MAIN";
			}

			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.action ="./ClientReasonSave.jsp";
			fm.submit();
		}
	}
	else
	{
		if(fm.AppeanRCode.value==null||fm.AppeanRCode.value=="")
		{
			alert("请选择申诉原因！");
			return false;
		}
		fm.all('operate').value = "APPEALINSERT||MAIN";
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.action ="./AppealSave.jsp";
		fm.submit();

	}
}


function afterAffix(ShortCountFlag,SupplyDateResult)
{
	var strsql = "select a.RgtState,b.codename,AffixGetDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
	+" and b.codetype='llrgtstate' and b.code=a.rgtstate"
	arrResult = easyExecSql(strsql);
	if ( arrResult )
	{
		fm.RgtState.value= arrResult[0][1];
		fm.AffixGetDate.value= arrResult[0][2];
	}


	fm.ModifyDate.value = getCurrentDate();

	// if ( arrResult[0][0] =="13"

	//}

}
function SaveAffixDate()
{
	if(fm.AffixGetDate.value!=""&&fm.AffixGetDate.value!=null)
	{
		var strSQL="update llcase set affixgetdate='"+fm.AffixGetDate.value+"' where caseno='"+fm.CaseNo.value+"'";
		easyExecSql(strSQL);
	}
}
//ClientCaseQueryInput.js查询返回接口2005-2-21 13:09
function ClientafterQuery(RgtNo,CaseNo)
{
	//团体批次
	if ( RgtNo!=null && RgtNo.length>0)
	{
		var arrResult = new Array();
		var strSQL="select "
		+"(select codename from ldcode where ldcode.codetype='llaskmode' and ldcode.code=llregister.RgtType),"
		+"AppAmnt,"
		+"(select codename from ldcode where ldcode.codetype='llreturnmode' and ldcode.code=llregister.ReturnMode),"
		+"RgtantName,"
		+"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llregister.IDType),"
		+"IDNo,"
		+"(select codename from ldcode where ldcode.codetype='llrelation' and ldcode.code=llregister.relation),"
		+"RgtantPhone,RgtantAddress,PostCode,"
		+"(select codename from ldcode where ldcode.codetype='llgetmode' and ldcode.code=llregister.getmode), "
		+"IDType,bankcode,bankaccno,accname,RgtType,ReturnMode,relation,getmode, "
		+"(select codename from ldcode where ldcode.codetype='bank' and ldcode.code=llregister.bankcode) "
		+"from LLRegister where rgtno='"+RgtNo+"'";
		arrResult = easyExecSql(strSQL);
		if( arrResult!=null)
		{
			fm.all('RgtTypeName').value= arrResult[0][0];
			fm.all('AppAmnt').value= arrResult[0][1];
			fm.all('ReturnModeName').value= arrResult[0][2];
			fm.all('RgtantName').value= arrResult[0][3];
			fm.all('IDTypeName').value= arrResult[0][4];
			fm.all('IDNo').value= arrResult[0][5];
			fm.all('RelationName').value= arrResult[0][6];
			fm.all('RgtantPhone').value= arrResult[0][7];
			fm.all('RgtantAddress').value= arrResult[0][8];
			fm.all('PostCode').value= arrResult[0][9];
			fm.all('paymodeName').value= arrResult[0][10];
			fm.all('IDType').value=arrResult[0][11];
			fm.all('BankCode').value= arrResult[0][12];
			fm.all('BankAccNo').value= arrResult[0][13];
			fm.all('AccName').value=arrResult[0][14];
			fm.all('RgtType').value=arrResult[0][15];
			fm.all('ReturnMode').value=arrResult[0][16];
			fm.all('Relation').value=arrResult[0][17];
			fm.all('paymode').value = arrResult[0][18];
			fm.all('BankName').value = arrResult[0][19];
			showBank();
		}

		//个人案件
		if (CaseNo!=null && CaseNo.length>0)
		{
			var strSQL="select insuredno,name,sex,birthday,"
			+"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=lcinsured.IDType),"
			+"idno,AddressNo ,idtype "
			+" from lcinsured where insuredno=(select customerno from "
			+"llcase where caseno='"+CaseNo+"')"

			arrResult = easyExecSql(strSQL);
			if( arrResult != null )
			{
				fm.CustomerNoQ.value    =arrResult[0][0];
				fm.CustomerTypeQ.value         =arrResult[0][1];
				fm.Sex.value          =fm.Sex.value= easyExecSql("select codename from ldcode where codetype = 'sex' and code = '"+arrResult[0][2]+"'");
				fm.CBirthday.value    =arrResult[0][3];
				fm.tIDTypeName.value      =arrResult[0][4];
				fm.tIDNo.value        =arrResult[0][5];
				fm.tIDType.value = arrResult[0][7]

			}
			strSQL="select AccStartDate,AccidentDate,DeathDate,AccidentSite,"
			+"AccdentDesc,AffixGetDate,"
			+"(select codename from ldcode where ldcode.codetype='llcuststatus' and ldcode.code=llcase.custstate),custstate"
			+" from llcase where caseno='"+CaseNo+"'";
			//	alert(strSQL);
			arrResult = easyExecSql(strSQL);
			if( arrResult != null )
			{
				//fm.all('AccStartDate').value= arrResult[0][0];
				// fm.all('AccidentDate').value= arrResult[0][1];
				fm.all('DeathDate').value= arrResult[0][2];
				fm.all('CustStatusName').value= arrResult[0][6];
				fm.all('CustStatus').value= arrResult[0][7];
				// fm.all('AccidentSite').value= arrResult[0][3];
				//  fm.all('AccidentReason').value= arrResult[0][4];
				if ( arrResult[0][5]!=null || arrResult[0][5]!='null' )
				{
					fm.all('AffixGetDate').value =arrResult[0][5] ;
				}
			}
			strSQL="select ReasonCode,Reason from LLAppClaimReason where 1=1 "
			+"and rgtno='"+RgtNo+"' "
			+" and caseno='"+CaseNo+"'";
			//turnPage.queryModal(strSQL,AppReasonGrid);
			arrResult = easyExecSql(strSQL);
			if ( arrResult!=null )
			{

				for (i=0;i< arrResult.length ;i++)
				{

					//fm.all("appReasonCode"+ arrResult[i][0])
					var ind= eval(arrResult[i][0])-1;


					fm.appReasonCode[ind].checked = true;

				}
			}
			//事件查询
			QueryEvent();
			fm.all("CaseNo").value = CaseNo;
			fm.all("RgtNo").value =  RgtNo;
			strSQL="select RgtState,Operator,ModifyDate,handler from llcase where caseno='"+CaseNo+"'";
			arrResult = easyExecSql(strSQL);
			if(arrResult!=null)
			{
				fm.all("Handler").value = arrResult[0][1];
				fm.all("ModifyDate").value = arrResult[0][2];
				if(arrResult[0][0]=="01")
				{
					fm.RgtState.value="案件受理";
				}
				if(arrResult[0][0]=="02")
				{
					fm.RgtState.value="扫描状态";
				}
				if(arrResult[0][0]=="03")
				{
					fm.RgtState.value="检录状态";
				}
				if(arrResult[0][0]=="04")
				{
					fm.RgtState.value="理算状态";
				}
				if(arrResult[0][0]=="05")
				{
					fm.RgtState.value="审批状态";
				}
				if(arrResult[0][0]=="06")
				{
					fm.RgtState.value="审定状态";
				}
				if(arrResult[0][0]=="07")
				{
					fm.RgtState.value="调查状态";
				}
				if(arrResult[0][0]=="08")
				{
					fm.RgtState.value="查讫状态";
				}
				if(arrResult[0][0]=="09")
				{
					fm.RgtState.value="结案状态";
				}
				if(arrResult[0][0]=="10")
				{
					fm.RgtState.value="抽检状态";
				}
				if(arrResult[0][0]=="11")
				{
					fm.RgtState.value="通知状态";
				}
				if(arrResult[0][0]=="12")
				{
					fm.RgtState.value="给付状态";
				}
				if(arrResult[0][0]=="13")
				{
					fm.RgtState.value="延迟状态";
				}
				if(arrResult[0][0]=="14")
				{
					fm.RgtState.value="撤件状态";
				}
				if(arrResult[0][3].substring(0,3)=="bcm")
				{
					fm.SimpleCase.checked = "true";
				}
			}
		}
	}
}
function afterLLRegister(arr)
{

	if(arr!=null)
	{
		fm.CustomerNoQ.value    =arr[0][0];
		fm.CustomerTypeQ.value  =arr[0][1];
//		fm.Sex.value          =arr[0][2];
		fm.CBirthday.value    =arr[0][3];
		fm.tIDTypeName.value      =arr[0][4];
		fm.tIDType.value      =arr[0][8];
		fm.tIDNo.value        =arr[0][5];
		fm.ReContNo.value     =arr[0][6];

		fm.InsuredNo.value    =arr[0][0];
		fm.Name.value         =arr[0][1];
		fm.Sex.value          =esayExecSql("select codename from ldcode where codetype = 'sex' and code = '"+arr[0][2]+"'");alert(fm.Sex.value);
		fm.CBirthday.value    =arr[0][3];
		fm.CIDType.value      =arr[0][4];
		fm.CIDNo.value        =arr[0][5];
		fm.ReContNo.value     =arr[0][7];
		var addresscode = arr[0][6];
		strSQL = "select b.PostalAddress,b.Phone from lcaddress b where b.AddressNo='" + addresscode +"'";
		arr = easyExecSql( strSQL , 1,0,1);
		if (arr)
		{
			fm.PostalAddress.value=arr[0][0];
			fm.Phone.value        =arr[0][1];
		}
	}
}
//弹出窗口事件查询
function OpenEvent()
{
	if(fm.CustomerNoQ.value=="")
	{
		alert("客户号码为空，请确认！");
		return false;
	}

	var varSrc = "&InsuredNo=" + fm.CustomerNoQ.value + "&Name=" + fm.Name.value + "&CaseNo=" + fm.CaseNo.value;
	//showInfo = window.open("./FrameMainEventQuery.jsp?Interface=EventInputQuery.jsp"+varSrc, "EventInput",'width=830,height=400,top=100,left=100,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	pathStr="./FrameMainEventQuery.jsp?Interface=EventInputQuery.jsp"+varSrc;
	showInfo=OpenWindowNew(pathStr,"EventInput","left");
}
//判断查询输入窗口的案件类型是否是回车，
//如果是回车调用查询客户函数
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClientQuery();
	}
}
//弹出窗口显示复选框选择受益人
function OpenBnf()
{
	//	if(fm.CustomerNoQ.value!="")
	//{
	//showInfo = window.open("./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNoQ.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerTypeQ.value+"&RgtNo="+fm.RgtNo.value,"EventInput",'width=650,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
	var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNoQ.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerTypeQ.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
	showInfo=OpenWindowNew(pathStr,"EventInput","middle");
	//	}
	//Else
	//	{
	//	alert("被保险人为空！");
	//	}
}
//打印接口
function PrintPage()
{
	if(fm.CaseNo.value=="")
	{
		alert("请先保存数据");
		return false;
	}
	showInfo = window.open("./PayAppPrt.jsp?CaseNo="+fm.CaseNo.value,"PrintPage",'width=600,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function afterCodeSelect( cCodeName, Field ) {
	if( cCodeName == "llRelation" && Field.value=="00")
	{
		fm.RgtantName.value = fm.CustomerTypeQ.value;
		fm.IDTypeName.value=fm.tIDTypeName.value;
		fm.IDType.value = fm.tIDType.value;
		fm.IDNo  .value=fm.tIDNo.value;

	}
	if( cCodeName == "llgetmode")
	{
		if(Field.value=="1")
		{
			divBankAcc.style.display='none';
			fm.BankAccM.disabled=true;
		}
	else
		{
			divBankAcc.style.display='';
			fm.BankAccM.disabled=false;
			var strSQL ="select distinct a.Bankcode,a.Bankaccno,a.Accname "
			+"from lcinsured a where name='"+fm.CustomerTypeQ.value+"'";
			var arr= easyExecSql( strSQL,1,0,1 );
				if (arr){
				fm.BankCode.value=arr[0][0];
				fm.BankAccNo.value=arr[0][1];
				fm.AccName.value=arr[0][2];
			}
		}
	}
}

function RgtFinish()
{
	strSQL = " select AppPeoples,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
	arrResult = easyExecSql(strSQL);

	if(arrResult != null)
	{
		if ( arrResult[0][1]!='01')
		{
			alert("团体案件状态不允许该操作！");
			return;
		}
		var AppPeoples;
		try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
	}
else
	{
		alert("团体案件信息查询失败！");
		return;
	}


	strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		var RealPeoples;
		try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
	}
	strSQL = " select customername from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
	brr = easyExecSql(strSQL)
	if(brr)
	{
		count = brr.length;
	}

	if ( RealPeoples < AppPeoples )
	{
		if(confirm("团体申请下个人申请尚未全部申请完毕！" + "团体申请人数：" + AppPeoples + "实际申请人数：" + RealPeoples+ "\n您确认要结束本团体批次吗?"))
		{
			fm.realpeoples.value=RealPeoples;
			alert("本批次申请人数为："+RealPeoples+"人")
		}
	else
		{
			return;
		}
	}

//	if ( RealPeoples == AppPeoples )
//	{
			fm.realpeoples.value=RealPeoples;
		fm.all('operate').value = "UPDATE||MAIN";
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

		fm.action = "./LLGrpRegisterSave.jsp";
		fm.submit();
//	}

}

function QueryApplyReason()
{

	var strSQL = " select code, codename,'' from ldcode where codetype = 'llrgtreason'";
	turnPage.queryModal(strSQL,AppReasonGrid);
}

function CaseChange()
{
	initTitle();
	ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}

function QueryEvent()
{
	if(fm.CustomerNoQ.value!="")
	{

		var strSQL="select SubRptNo,AccDate,AccPlace,AccidentType,AccSubject,HospitalName,InHospitalDate,OutHospitalDate,AccDesc,"+
					"case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," +"AccidentType from LLSubReport where CustomerNo='"
		+ fm.CustomerNoQ.value +"' order by AccDate desc";
		//turnPage.queryModal(strSQL, EventGrid);
		arrResult = easyExecSql(strSQL);
		if ( arrResult )
		{
			displayMultiline(arrResult,EventGrid);

			//查询关联上的事件
			strSQL = "select subrptno from llcaserela where caseno='" + fm.CaseNo.value +"'";
			var br = easyExecSql ( strSQL );
			//对已关联事件打勾
			if ( br)
			{
				pos =0;
				for ( i=0;i<arrResult.length;i++)
				{
					for ( j=0;j<br.length ;j++)
					{
						if ( arrResult[i][0]==	br[j][0] )
						{
							EventGrid.checkBoxSel(i+1);

						}
					}
				}
			}
		}
	}
}
/**
*咨询通知信息
**/
function OnAppeal()
{

	IdAppeal.style.display='';

	fm.IdAppConf.value ="申诉保存";
	tAppealFlag="1";//申诉Flag状态改变

}

function showBank()
{
		if(fm.paymode.value=="1")
		{
			divBankAcc.style.display='none';
			fm.BankAccM.disabled=true;
		}
	else
		{
			divBankAcc.style.display='';
			fm.BankAccM.disabled=false;
		}
}

