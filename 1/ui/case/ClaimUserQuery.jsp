<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput GI = new GlobalInput();
GI = (GlobalInput)session.getValue("GI");
%>
<script>
  var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
  var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="ClaimUserQuery.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="ClaimUserQueryInit.jsp"%>
    <title>理赔岗位权限查询 </title>
   <script language="javascript">
   function initDate(){
      fm.Station.value=manageCom;
   }
   </script>
  </head>
  <body  onload="initForm();initDate();" >

    <form action="./ClaimUserSave.jsp" method=post name=fm target="fraSubmit">
      <!-- 报案信息部分 -->
      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
          </td>
          <td class= titleImg>
            请您输入查询条件：
          </td>
        </tr>
      </table>

      <Div  id= "divLLReport1" style= "display: ''">
        <table class=common>
          <TR>
            <TD  class= title>
              用户机构代码
            </TD>
            <TD  class= input>
              <Input class="code" name=Station verify="机构代码|notnull&code:ComCode" ondblclick="return showCodeList('ComCode',[this, StationName], [0, 1]);" onkeyup="return showCodeListKey('ComCode', [this, StationName], [0, 1]);">
            </TD>
            <TD  class= title>
              用户机构名称
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name= StationName >
            </TD>
          </TR>
          <TR class=common >
            <TD  class= title >
              用户代码
            </TD>
            <TD  class= input>
              <Input class= "common"  name=UserCode >
            </TD>
            <TD  class= title >
              用户名称
            </TD>
            <TD  class= input>
              <Input class="common"  name=UserName >
            </TD>
          </TR>

          <TR class= common style="display:'none'">
            <TD class= title>
              用户核赔权限
            </TD>
            <TD class=input>
              <Input class="code" name=ClaimPopedom
              ondblClick="showCodeList('llClaimPopedom',[this])"
              onkeyup="showCodeListKey('llClaimPopedom',[this])">
            </TD>
            <TD class= title>
              上级用户代码
            </TD>
            <TD class = input>
              <Input class= "common"   name=UpUserCode >
            </TD>
          </TR>
          <TR  class= common style="display:'none'">
            <TD  class= title>
              立案权限
            </TD>
            <TD  class= input>
              <Input class="code" name=RgtFlag verify="方式|NOTNULL" CodeData="0|^0|无立案权限^1|有立案权限" ondblClick="showCodeListEx('RgtFlag_1',[this],[0,1]);" onkeyup="showCodeListKeyEx('RgtFlag_1',[this],[0,1]);">
            </TD>
          </TR>
          <TR  class= common style="display:'none'">
            <TD  class= title>
              理算审核权限
            </TD>
            <TD  class= input>
              <Input class="code" name=CheckFlag verify="方式|NOTNULL"
              CodeData="0|^0|无理算审核权限^1|有理算审核权限"
              ondblClick="showCodeListEx('CheckFlag_1',[this],[0,1]);"
              onkeyup="showCodeListKeyEx('CheckFlag_1',[this],[0,1]);">
            </TD>
            <TD  class= title>
              案件签批权限
            </TD>
            <TD  class= input>

              <Input class="code" name=HandleFlag verify="方式|NOTNULL"
              CodeData="0|^0|无案件签批权限^1|有案件签批权限"
              ondblClick="showCodeListEx('HandleFlag_1',[this],[0,1]);"
              onkeyup="showCodeListKeyEx('HandleFlag_1',[this],[0,1]);">
            </TD>
          </tr>

          <!--------------------->
          <TR  class= common>
            <TD  class= title>
              理赔员权限
            </TD>
            <TD  class= input>
              <Input class="code" name=ClaimDeal verify="方式|NOTNULL"
              CodeData="0|^0|无权限^1|有理赔权限"
              ondblClick="showCodeListEx('RgtFlag_1',[this],[0,1]);"
              onkeyup="showCodeListKeyEx('RgtFlag_1',[this],[0,1]);">
            </TD>
            <TD  class= title>
              调查权限
            </TD>
            <TD  class= input>
              <Input class="code" name=ServeyFlag verify="方式|NOTNULL"
              CodeData="0|^0|无调查权限^1|有调查权限"
              ondblClick="showCodeListEx('ServeyFlag_1',[this],[0,1]);"
              onkeyup="showCodeListKeyEx('ServeyFlag_1',[this],[0,1]);">
            </TD>
          </TR>
          <TR  class= common>
            <TD  class= title>
              用户有效状态
            </TD>
            <TD  class= input>
              <Input class="code" name=StateFlag verify="方式|NOTNULL" CodeData="0|^1|有效^2|休假^3|离职^9|其他无效情况" ondblClick="showCodeListEx('StateFlag_1',[this],[0,1,2,3]);" onkeyup="showCodeListKeyEx('StateFlag_1',[this],[0,1,2,3]);">
              <input type=hidden id="fmtransact" name="fmtransact">
            </TD>
          </TR>
        </table>
      </Div>
      <INPUT class=cssButton VALUE="查  询" TYPE=button onclick="submitForm();return false;">
      <INPUT class=cssButton VALUE="返  回" TYPE=button onclick="returnParent();">
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
          </td>
          <td class= titleImg>
            理赔岗位权限信息
          </td>
        </tr>
      </table>
      <Div  id= "divLLReport2" style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanRegisterGrid" >
              </span>
            </td>
          </tr>
        </table>
        <INPUT VALUE="首  页" TYPE=button class=cssButton onclick="turnPage.firstPage();" >
        <INPUT VALUE="上一页" TYPE=button class=cssButton onclick="turnPage.previousPage();">
        <INPUT VALUE="下一页" TYPE=button class=cssButton onclick="turnPage.nextPage();">
        <INPUT VALUE="尾  页" TYPE=button class=cssButton onclick="turnPage.lastPage();">
      </div>
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>