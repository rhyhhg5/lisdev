<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLClaimCollectionRpt.jsp
//程序功能：F1报表生成
//创建日期：2005-07-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
	System.out.println("start");
  	CError cError = new CError();
  	boolean operFlag=true;
  	
  	//直接获取页面的值
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
  	String MngCom 				= request.getParameter("ManageCom");	
  	String ManageName 			= request.getParameter("ManageComName");
  	String EndCaseDateS 		= request.getParameter("EndCaseDateS");		//结案起期
  	String EndCaseDateE 		= request.getParameter("EndCaseDateE");		//结案止期
  	String CvaliDateS 		    = request.getParameter("CvaliDateS");		//生效起期
  	String CvaliDateE 		    = request.getParameter("CvaliDateE");		//生效止期
  	String SaleChnl 			= request.getParameter("SaleChnl");	
  	String Sort 				= request.getParameter("Sort");


  	String ClaimRatioStd   		= request.getParameter("ClaimRatioStd");	
  	String ContNo 				= request.getParameter("GrpContNo");
  	String GrpName 				= request.getParameter("GrpName");
  	String Person 				= request.getParameter("Person");
  	String Prem 				= request.getParameter("Prem");
  	String GrpTypeS 			= request.getParameter("GrpTypeS");
  	String RiskCode 			= request.getParameter("RiskCode");
  	

	String StatsType = request.getParameter("StatsType");//统计类型
	System.out.println("StatsType:"+StatsType);

  	TransferData Para = new TransferData();
      
  	Para.setNameAndValue("MngCom",MngCom);
  	Para.setNameAndValue("ManageName",ManageName);
  	Para.setNameAndValue("CvaliDateS",CvaliDateS);
  	Para.setNameAndValue("CvaliDateE",CvaliDateE);
  	Para.setNameAndValue("EndCaseDateS",EndCaseDateS);
  	Para.setNameAndValue("EndCaseDateE",EndCaseDateE);
   	Para.setNameAndValue("SaleChnl",SaleChnl);
   	Para.setNameAndValue("Sort",Sort);
   	Para.setNameAndValue("GrpTypeS",GrpTypeS);
   	Para.setNameAndValue("RiskCode",RiskCode);

   	Para.setNameAndValue("ClaimRatioStd",ClaimRatioStd);
   	Para.setNameAndValue("ContNo",ContNo);
   	Para.setNameAndValue("GrpName",GrpName);
   	Para.setNameAndValue("Person",Person); 			  
   	Para.setNameAndValue("Prem",Prem); 	
   	Para.setNameAndValue("StatsType",StatsType);	
   	
//  ------------------------------
   	RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);
//   	-------------------------------
  	String date=PubFun.getCurrentDate().replaceAll("-","");  
	String time=PubFun.getCurrentTime3().replaceAll(":","");
  	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	String fileNameB = tG.Operator + "_" + FileQueue.getFileName()+".vts";
	Para.setNameAndValue("tFileNameB",fileNameB);
	String operator = tG.Operator;
	System.out.println("@承保赔付汇总统计报表@");
	System.out.println("Operator:"+operator);
	System.out.println("ManageCom:"+tG.ManageCom);
	System.out.println("Operator:"+tG.Operator);
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();

    tVData.addElement(tG);    
    tVData.addElement(Para);
 
    
    LLClaimCollectionUI tLLClaimCollectionUI = new LLClaimCollectionUI();
	XmlExport txmlExport = new XmlExport();    
    if(!tLLClaimCollectionUI.submitData(tVData,"PRINT"))
    {
       	operFlag=false;
       	Content=tLLClaimCollectionUI.mErrors.getFirstError().toString(); 
       	%>
   		<%=Content%>  
   		<%return;
    }
    else
    {  
    	System.out.println("--------成功----------");  
		mResult = tLLClaimCollectionUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	
	   		%>
	   		
	   		<%=Content%>  
	   		<%return;
	  	}
	}
    
    Readhtml rh=new Readhtml();
	  rh.XmlParse(txmlExport.getInputStream()); //相当于XmlExport.getInputStream();
	  
	  String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
	  String temp=realpath.substring(realpath.length()-1,realpath.length());
	  if(!temp.equals("/"))
	  {
		  realpath=realpath+"/"; 
	  }
	  String templatename=rh.getTempLateName();//模板名字
	  String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
	  System.out.println("*********************templatepathname= " + templatepathname);
	  System.out.println("************************realpath="+realpath);
	  
	  String outname="ClaimCollection"+tG.Operator+date+time+".xls";
	  String outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	  //*******************************
	  rpt.updateReportMetaData(outname);
	  //**********************************
	 
	  //*********************************
	  rh.setReadFileAddress(templatepathname);
	  rh.setWriteFileAddress(outpathname);
	  rh.start("vtsmuch");
	  try{
	  outname = java.net.URLEncoder.encode(outname, "UTF-8");
	  outname = java.net.URLEncoder.encode(outname, "UTF-8");
	  //outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	  //outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
    }catch(Exception ex)
    {
     ex.printStackTrace();
    }
      String[] InputEntry = new String[1];
      InputEntry[0] = outpathname;
      String tZipFile = realpath+"vtsfile/"+ StrTool.replace(outname,".xls",".zip");
		System.out.println("tZipFile == " + tZipFile);
      rh.CreateZipFile(InputEntry, tZipFile);



%>

<html>
<%@page contentType="text/html;charset=GBK" %>
<a href="../f1print/download.jsp?filename=<%=StrTool.replace(outname,".xls",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>

