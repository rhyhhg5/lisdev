<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initCheckGrid();
    initSubReportGrid();
    easyQuery();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=1;
      
    iArray[1]=new Array("咨询通知号","150px","0","0");
    iArray[2]=new Array("姓名","80px","0","0");
    iArray[3]=new Array("身份证号","100px","0","3");
    iArray[4]=new Array("电话号码","0px","0","3");
    iArray[5]=new Array("手机号码","0px","0","3");
    iArray[6]=new Array("地址","150px","0","3");
    iArray[7]=new Array("咨询通知日期","100px","0","0");
    iArray[8]=new Array("操作员","0px","0","3");;
    iArray[9]=new Array("类型","50px","0","0");
    iArray[10]=new Array("IDNo","0px","0","3");
    iArray[12]=new Array("操作员姓名","90px","20","0");
    iArray[13]=new Array("操作员代码","90px","20","0");
    iArray[11]=new Array("管理机构","90px","20","0");
    iArray[14]=new Array("来源","0px","20","3");


    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =10;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canSel =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid. selBoxEventFuncName = "DealConsult";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
function initSubReportGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="事件号";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="发生日期";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="发生地点";
    iArray[3][1]="150px";
    iArray[3][2]=60;
    iArray[3][3]=0;

   iArray[4] = new Array("入院日期","80px","20","0");
   iArray[5] = new Array("出院日期","80px","20","0");
	 iArray[6] = new Array("事件信息","140px","20","0");
   
    SubReportGrid = new MulLineEnter("fm","SubReportGrid");
    SubReportGrid.mulLineCount = 0;
    SubReportGrid.displayTitle = 1;
    SubReportGrid.locked = 0;
    SubReportGrid.canChk =1	;
    SubReportGrid.canSel =0	;

    SubReportGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.selBoxEventFuncName = "ShowRela";
    SubReportGrid.loadMulLine(iArray);
    

  }
  catch(ex)
  {
    alter(ex);
  }
}
 </script>