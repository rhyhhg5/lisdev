<%
/*******************************************************************************
 * Name     :FrameMainAccident.jsp
 * Function :显示立案－事故/伤残的主界面
 * Author   :LiuYansong
 * Date     :2003-7-15
 */
%>
<!--Root="../../" -->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<title>事故/伤残明细</title>
<script language="javascript">
function focusMe()
{
  window.focus();
}
</script>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">

	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">

	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="about:blank" >
	<frameset name="fraSet" cols="0%,*,0%" frameborder="no" border="1" framespacing="0" rows="*">
		<!--菜单区域-->
		<frame name="fraMenu" scrolling="yes" noresize src="about:blank">
		<!--交互区域-->
		<%
			String szSrc = request.getParameter("Interface");
      szSrc +="?RgtNo=" + request.getParameter("RgtNo");
			szSrc += "&CaseNo=" + request.getParameter("CaseNo");
			szSrc += "&CustomerNo=" + request.getParameter("CustomerNo");
			szSrc += "&CustomerName=" + StrTool.unicodeToGBK(request.getParameter("CustomerName"));
      szSrc += "&DisplayFlag=" + request.getParameter("DisplayFlag");
			System.out.println(szSrc);
		%>
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="<%= szSrc %>">
    	<!--下一步页面区域-->
    	<frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff" onblur="focusMe();>
	</body>
</noframes>
</html>
