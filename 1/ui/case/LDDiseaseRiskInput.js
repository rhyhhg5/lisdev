var turnPage = new turnPageClass();  
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
  fm.action = "LLClaimCollectionRpt.jsp";
	fm.fmtransact.value = "PRINT";
	fm.target = "LLClaimCollection";
	fm.submit();
	showInfo.close();
}
function ICDSearch(){
	if(fm.ICDCode.value==''&&fm.ICDName.value=='')
	{
		alert("疾病编码和疾病名称不能全为空！");
		return;
	}
	var strSQL = "select icdcode,icdname,risklevel,riskcontinueflag from lddisease where 1=1 "+
	getWherePart("icdcode","ICDCode")+ getWherePart("icdname","ICDName","like")+" with ur";
	turnPage.queryModal(strSQL,DiseaseRiskGrid);
}

function DiseaseRiskSave(){
	var selNo = DiseaseRiskGrid.getSelNo();
	if ( selNo<1 ) {
		alert("请选择疾病");
		return false;
	}
	if(DiseaseRiskGrid.getRowColData(selNo-1,3)=='' && DiseaseRiskGrid.getRowColData(selNo-1,4)=='')
	{
		alert("风险等级和存续期不能都为空！");
		return false;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "LDDiseaseRiskSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //ICDSearch();
}
