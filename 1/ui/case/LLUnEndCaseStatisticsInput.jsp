<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<html>

<%
 //程序名称：LLUnEndCaseStatisticsInput.jsp
 //程序功能：
 //创建日期：2010-6-22
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-30";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="LLUnEndCaseStatistics.js"></SCRIPT>
  <script>
	function initDate(){
	  fm.RgtDateS.value="<%=afterdate%>";
	  fm.RgtDateE.value="<%=CurrentDate%>";
	  fm.EndDate.value="<%=CurrentDate%>";
	  fm.ManageCom.value="<%=tG1.ManageCom%>";
	}
  </script>
</head>
<body  onload="initDate();initElementtype();">
<form action="./LLUnEndCaseStatisticsSave.jsp" method=post name=fm target="fraSubmit">
<table >
	<tr>
		<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUnEnd);">
		</td>
		<td class= titleImg>
			未决案件统计
		</td>
	</tr>
</table>
<div id="divUnEnd">
<table class= common border=0 width=100%>
	<tr class= common>
		<td class= title>
			统计机构
		</TD>
		<TD  class= input>
			<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="统计机构|notnull"><Input class=codename  name=ManageComName></TD> 
		<TD class= title>险种</TD>
		<TD class= input><input class=codeno name=RiskProp CodeData="0|^1|个险^2|团险" ondblclick="return showCodeListEx('RiskProp',[this,RiskPropName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('RiskProp',[this,RiskPropName],[0,1],null,null,null,1);" value="1" readonly ><input class=codename name=RiskPropName value="个险"></TD>
		<TD class= title>渠道</TD>
		<TD class= input><input class=codeno name=SaleChnl CodeData="0|^1|个险^2|团险^3|银保" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName></TD>
    </TR>
    <TR  class= common>
        <TD  class= title>
             受理起期
        </TD>
        <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=RgtDateS  verify="受理起期|DATE&NOTNULL"   >  
        </TD>
        <TD  class= title>
             受理止期
        </TD>
        <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=RgtDateE verify="受理止期|DATE&NOTNULL"  >  
        </TD>
        <TD  class= title>
             统计日期
        </TD>
        <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="统计日期|DATE&NOTNULL"  >  
        </TD>
     </TR>  
     <TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input> <input class="codeno" value="3" CodeData="0|3^1|商业保险^2|社会保险^3|全部"  verify="业务类型|notnull" name=StatsType ondblclick="return showCodeListEx('StatsType',[this,StatsTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StatsType',[this,StatsTypeName],[0,1]);"><input class=codename value="全部" name=StatsTypeName></TD> 
        </TR>   
     <tr class="common">
         <td class="title">统计</td>
         <td class="input"><input class=codeno name=Type CodeData="0|^1|已受理未理算^2|已理算未结案" ondblclick="return showCodeListEx('Type',[this,TypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Type',[this,TypeName],[0,1],null,null,null,1);"><input class=codename name=TypeName></TD>
         <TD  class= title></TD>
        <TD  class= input></TD>
        <TD  class= title></TD>
        <TD  class= input></TD>
     </tr>
</TABLE>
</div>
<table>
 <TR>
   <TD>
     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
   </TD>
	<TD class= titleImg>
		   字段说明：
    </TD>
 </TR>
</table>
		<Div  id= "divCare" >
		  <table class= common border=0 width=100%>
		    <tr class="common"><td class="common">统计机构：</td><td class="common">选择总公司，打印所有分公司三级机构数据，一个分公司下三级机构展示后，合计展示分公司数据；选择分公司，打印该分公司下三级机构数据，三级机构展示后，合计展示分公司数据；选择三级机构，打印该三级机构数据。</td></tr>     
		    <tr class="common"><td class="common">险种：</td><td class="common">根据产品定义，将所有险种分为个险、团险</td></tr>
		    <tr class="common"><td class="common">渠道：</td><td class="common">根据保单业务员的所属类型划分，个险：个险业务员，团险：团险、中介、电销业务员；银保：银代业务员</td></tr>
		 	<tr class="common"><td class="common">已受理未理算案件数：</td><td class="common">机构下该险种客户受理日期在受理统计区间内，案件出险日期在保单有效期内，且理算时间在统计日期后（不包括统计日期）的案件数，如果案件已经撤件，撤件日期在统计日期后（不包括统计日期）</td></tr>
			<tr class="common"><td class="common">已理算未结案案件数：</td><td class="common">机构下该险种已经理算的案件，受理日期在受理统计区间内，且结案日期在统计日期后（不包括统计日期）的案件数</td></tr>
			<tr class="common"><td class="common">已理算未结案赔款：</td><td class="common">机构下该险种已经理算的案件，受理日期在受理统计区间内，且结案日期在统计日期后（不包括统计日期）的赔款</td></tr>     
		  </table>
		</Div>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
