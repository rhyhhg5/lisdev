 
<%
//程序名称：
//程序功能：
//创建日期：2002-07-21 21:03:23
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="./SurveyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./SurveyInit.jsp"%>
</head>

<body  onload="initForm();" >
  <form action="./SurveyOperate.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= input width="26%"> 
        <Input class=common type=Button value="待确认的调查报告查询" onclick="QuerySurvey()">
      </TD>
      <!--
      <TD  class= input width="26%"> 
        <Input class=common type=Button value="调查报告接收" onclick="AcceptSurvey()">
      </TD>
      -->
    <TD  class= input width="26%"> 
        <Input class=common type=Button value="调查报告修改" onclick="UpdateSurvey()">
      </TD>
      <TD  class= input width="26%"> 
        <Input class=common type=Button value="调查报告确认" onclick="EnsureSurvey()">
      </TD>
      <TD  class= input width="26%"> 
        <Input class=common type=Button value="已经确认的调查报告查询" onclick="EndSurvey()">
      </TD>
      
    </TR>
  </TABLE> 

      <table  class= common>
        <TR  class= common width="26%">
          <TD  class= title width= "26%">
						立案号
          </TD>
          <TD  class= input>
            <Input class= common name=RgtNo readonly >
          </TD>
          </TR>
          </table>
	    <table class=common >
	    <tr class=common >
      <td class=title>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
      </td>
      <td class= titleImg>
				案件信息        
      </td>
    	</tr>
    </table>
  <Div  id= "divLLCase1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <Input class= common name=CaseNo type=hidden>
          <Input class= common name=SubReportNo type=hidden >










        </TR>
        <TR  class= common>
          <TD  class= title>
            事故者客户号
          </TD>
          <TD  class= input>
            <Input class= common name=CustomerNo readonly >
          </TD>
          <TD  class= title>
            事故者名称
          </TD>
          <TD class= input>
            <Input class= common name=CustomerName readonly >
          </TD>
        </TR>
      </table>
    </Div>
    <!--调查报告信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSurvey);">
    		</td>
    		<td class= titleImg>
    			 调查报告信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divSurvey" style= "display: ''">
    <table  class= common>
     <tr  class= common>
				<TD class=title>
					序号/调查次数
				</TD>
				<TD class= input readonly >
            <Input class=common name=SerialNo Readonly >
        </TD>
				<TD class=title>
					调查报告状态
				</TD>
				<TD class= input readonly >
            <Input class=common name=SurveyFlag Readonly >
        </TD>
			</tr>
	    <tr  class= common>
				    <Input class=common name=ClmCaseNo type=hidden >
            <Input class=common name=Type type=hidden >
     </tr>
	    <tr  class= common>
				<TD class=title>
					调查人
				</TD>
				<TD class= input>
            <Input class=common name=Surveyor readonly >
        </TD>
        <TD class=title>
					调查地点
				</TD>
				<TD class= input>
            <Input class=common name=SurveySite>
        </TD>
			</tr>
			<tr  class= common>
				<TD class=title>
					调查开始时间
				</TD>
				<TD class= input>
            <Input class=common name=SurveyStartDate readonly >
        </TD>
        <TD class=title>
					调查结束时间
				</TD>
				<TD class= input>
            <Input class=common name=SurveyEndDate readonly >
        </TD>
			</tr>
			</table>
			<table  class= common>
			<tr>
				<td class=title>
					调查提示
				</td>
			</tr>
			<tr>
				<td class=input>
					<textarea class=common  name=Content readonly rows="4" cols="100">
					</textarea>
				</td>
			</tr>
			<tr>
				<td class=title>
					调查报告内容
				</td>
			</tr>
			<tr>
				<td class=input>
					<textarea class=common name=Result rows="10" cols="100">
					</textarea>
				</td>
			</tr>
		</table>
	</div>
   <input type=hidden name="Opt">
   <input type=hidden id="fmtransact" name="fmtransact">
   
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>

