<%
//程序名称：LLDrugInfoInit.jsp
//程序功能：药品信息
//创建日期：2015-02-11
//创建人  ：Liyunxia
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

  if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">

function initInpBox()
{
  try
  {
  

}
  catch(ex)
  {
  	
    alert("在LLDrugInfoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initEvaluateGrid();
    //easyQueryClick();
  }
  catch(re)
  {
    alert("BlackListPersonQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var EvaluateGrid;          //定义为全局变量，提供给displayMultiline使用
// 黑名单信息列表的初始化
function initEvaluateGrid()
{

    var iArray = new Array();
  try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px";            		//列宽
      iArray[0][2]=4;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[1]=new Array();
      iArray[1][0]="药品编码";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=40;            			//列最大值
      iArray[1][3]=0;   

      iArray[2]=new Array();
      iArray[2][0]="药品类别";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=40;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="药品通用名称";      		//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;
      
      iArray[4]=new Array();
      iArray[4][0]="商品名";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=40;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="英文名";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=40;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="药品分类";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=40;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="次类别";         		//列名
      iArray[7][1]="150px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="备注";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="医保剂型";      		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="药品规格";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=40;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="拼音简码";      		//列名
      iArray[11][1]="150px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[12]=new Array();
      iArray[12][0]="生产企业名称";      		//列名
      iArray[12][1]="150px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[13]=new Array();
      iArray[13][0]="包装信息";      		//列名
      iArray[13][1]="150px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[14]=new Array();
      iArray[14][0]="给药途径";      		//列名
      iArray[14][1]="150px";            		//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[15]=new Array();
      iArray[15][0]="医保序号";      		//列名
      iArray[15][1]="150px";            		//列宽
      iArray[15][2]=200;            			//列最大值
      iArray[15][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[16]=new Array();
      iArray[16][0]="费用等级";      		//列名
      iArray[16][1]="150px";            		//列宽
      iArray[16][2]=200;            			//列最大值
      iArray[16][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[17]=new Array();
      iArray[17][0]="限定支付范围";      		//列名
      iArray[17][1]="150px";            		//列宽
      iArray[17][2]=200;            			//列最大值
      iArray[17][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[18]=new Array();
      iArray[18][0]="限门诊使用";      		//列名
      iArray[18][1]="150px";            		//列宽
      iArray[18][2]=200;            			//列最大值
      iArray[18][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[19]=new Array();
      iArray[19][0]="限住院使用";      		//列名
      iArray[19][1]="150px";            		//列宽
      iArray[19][2]=200;            			//列最大值
      iArray[19][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[20]=new Array();
      iArray[20][0]="工伤标识";      		//列名
      iArray[20][1]="150px";            		//列宽
      iArray[20][2]=200;            			//列最大值
      iArray[20][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[21]=new Array();
      iArray[21][0]="生育标识";      		//列名
      iArray[21][1]="150px";            		//列宽
      iArray[21][2]=200;            			//列最大值
      iArray[21][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[22]=new Array();
      iArray[22][0]="最高限价";      		//列名
      iArray[22][1]="150px";            		//列宽
      iArray[22][2]=200;            			//列最大值
      iArray[22][3]=3;                  //是否允许输入,1表示允许，0表示不允许

      iArray[23]=new Array();
      iArray[23][0]="管理机构";      		//列名
      iArray[23][1]="150px";            		//列宽
      iArray[23][2]=200;            			//列最大值
      iArray[23][3]=0;                  //是否允许输入,1表示允许，0表示不允许

      iArray[24]=new Array();
      iArray[24][0]="地区名称";      		//列名
      iArray[24][1]="150px";            		//列宽
      iArray[24][2]=200;            			//列最大值
      iArray[24][3]=0;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[25]=new Array();
      iArray[25][0]="流水号";      		//列名
      iArray[25][1]="150px";            		//列宽
      iArray[25][2]=200;            			//列最大值
      iArray[25][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[26]=new Array();
      iArray[26][0]="药品类别实际值";      		//列名
      iArray[26][1]="150px";            		//列宽
      iArray[26][2]=200;            			//列最大值
      iArray[26][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[27]=new Array();
      iArray[27][0]="费用等级实际值";      		//列名
      iArray[27][1]="150px";            		//列宽
      iArray[27][2]=200;            			//列最大值
      iArray[27][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[28]=new Array();
      iArray[28][0]="限门诊使用实际值";      		//列名
      iArray[28][1]="150px";            		//列宽
      iArray[28][2]=200;            			//列最大值
      iArray[28][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[29]=new Array();
      iArray[29][0]="限住院使用实际值";      		//列名
      iArray[29][1]="150px";            		//列宽
      iArray[29][2]=200;            			//列最大值
      iArray[29][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[30]=new Array();
      iArray[30][0]="工伤标识实际值";      		//列名
      iArray[30][1]="150px";            		//列宽
      iArray[30][2]=200;            			//列最大值
      iArray[30][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[31]=new Array();
      iArray[31][0]="生育标识实际值";      		//列名
      iArray[31][1]="150px";            		//列宽
      iArray[31][2]=200;            			//列最大值
      iArray[31][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[32]=new Array();
      iArray[32][0]="管理机构实际值";      		//列名
      iArray[32][1]="150px";            		//列宽
      iArray[32][2]=200;            			//列最大值
      iArray[32][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      iArray[33]=new Array();
      iArray[33][0]="地区名称实际值";      		//列名
      iArray[33][1]="150px";            		//列宽
      iArray[33][2]=200;            			//列最大值
      iArray[33][3]=3;                  //是否允许输入,1表示允许，0表示不允许
      
      EvaluateGrid = new MulLineEnter( "fm" , "EvaluateGrid" );
      //EvaluateGrid.selBoxEventFuncName = "getPara";
      //这些属性必须在loadMulLine前
      EvaluateGrid.displayTitle = 1;
      EvaluateGrid.mulLineCount = 5;
      EvaluateGrid.hiddenPlus=1;
      EvaluateGrid.hiddenSubtraction=1;
      EvaluateGrid.canSel = 1;
      EvaluateGrid.canChk =0;  // 1为显示CheckBox列，0为不显示 (缺省值)
      EvaluateGrid.selBoxEventFuncName='onClick';
      EvaluateGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //EvaluateGrid.setRowColData(1,1,"1");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
