//程序名称：PEdorBudget.js
//程序功能：退保试算
//创建日期：2006-04-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

//判断是否是回车键
function isEnterDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if (keycode == "13")
	{
		return true;
	}
	else
	{
		return false;
	}
}

//响应回车事件，查询保单
function queryContOnKeyDown()
{
  if (isEnterDown())
  {
    queryCont();
  }
}

//查询保单信息
function queryCont()
{
  if(trim(fm.contNoTop.value) == "" 
      && trim(fm.appntNoTop.value) == "" 
      && trim(fm.appntNameTop.value) == "")
  {
    alert("请录入查询条件。");
    return false;
  }
  
  initLCContGrid();
  initLCPolGrid();
  
  var sql = "  select contNo, appntName, appntNo, polApplyDate, cValiDate, "
            + "   (select max(payToDate) "
            + "   from LCPol "
            + "   where ContNo = a.ContNo), "
            + "   (select max(endDate) "
            + "   from LCPol "
            + "   where ContNo = a.ContNo), "
            + "   '缴费频次', prem "
            + "from LCCont a "
            + "where appFlag = '1' "
            + "   and contType = '1' "
            + getWherePart("contNo", "contNoTop")
            + getWherePart("appntNo", "appntNoTop")
            + getWherePart("appntName", "appntNameTop", "like");
  turnPage1.pageDivName = "divPage";
	turnPage1.queryModal(sql, LCContGrid);
	
	if((trim(fm.contNoTop.value) != "" || trim(fm.appntNoTop.value) != ""
	    || trim(fm.appntNameTop.value) != "") && LCContGrid.mulLineCount != 0)
	{
	  setOneContInfo(0);
	}
}

//相应保单列表的单选框单击事件
function setContInfoOnClick()
{
  var row = LCContGrid.getSelNo() - 1;
  setOneContInfo(row);
}

//显示选中的保单信息
function setOneContInfo(row)
{
  fm.contNo.value = LCContGrid.getRowColDataByName(row, "contNo");
  fm.appntName.value = LCContGrid.getRowColDataByName(row, "appntName");
  fm.appntNo.value = LCContGrid.getRowColDataByName(row, "appntNo");
  fm.polApplyDate.value = LCContGrid.getRowColDataByName(row, "polApplyDate");
  fm.cValiDate.value = LCContGrid.getRowColDataByName(row, "cValiDate");
  fm.payToDate.value = LCContGrid.getRowColDataByName(row, "payToDate");
  fm.cInValiDate.value = LCContGrid.getRowColDataByName(row, "cInValiDate");
  fm.prem.value = LCContGrid.getRowColDataByName(row, "prem");
  fm.payToDateLongPol.value = fm.payToDate.value;
  
  //得到缴费频次
/*  
  var sql = "  select distinct b.codeName "
            + "from LCPol a, LDCode b "
            + "where char(a.payIntv) = b.code "
            + "   and b.codeType = 'payintv' "
            + "   and contNo = '" + fm.contNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    if(result.length == 1)
    {
      fm.payIntv.value = result[0][0];
    }
  }
*/  
  //得到保单保全状态
  sql = "  select 1 "
        + "from LPEdorApp a, LPEdorItem b "
        + "where a.edorAcceptNo = b.edorNo "
        + "   and b.contNo = '" + fm.contNo.value + "' "
        + "   and a.edorState != '0' "
        + "   and b.edorNo != '" + fm.edorNo.value + "' ";
  result = easyExecSql(sql);
  if(result)
  {
    fm.edorState.value = "有正在进行保全作业";
  }
  else
  {
    fm.edorState.value = "无正在进行保全作业";
  }
  
  //得到理赔状态
  /*
  sql = "  select 1 "
        + "from LLClaimDetail "
        + "where contNo = '" + fm.contNo.value + "' "
        + "   and caseNo not in "
        + "      (select otherNo from LJAGetClaim where contNo = '" + fm.contNo.value + "')";
  */
  sql = " select 1 from llregister where customerno in (select insuredno from lcinsured where contno = '"
  		+fm.contNo.value+"') and rgtstate != '12' and rgtstate !='14' ";
  result = easyExecSql(sql);
  if(result)
  {
    fm.claimState.value = "有正在进行理赔作业";
  }
  else
  {
    fm.claimState.value = "无正在进行理赔作业";
  }
  
  //得到险种信息
  setLCPolInfo(fm.contNo.value);
}

//得到险种信息
function setLCPolInfo(contNo)
{
  var sql = "  select polNo, a.riskSeqNo, a.insuredNo, a.insuredName, a.riskCode, b.riskName, "
            + "   a.CValiDate, a.amnt, a.mult, a.prem, (select sum(SumActuPayMoney) from LJAPayPerson where PolNo = a.PolNo), a.endDate,"
            + "case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
            + ", '' "
            + "from LCPol a, LMRiskApp b "
            + "where a.riskCode = b.riskCode "
            + "   and a.ContType = '1' "
            + "   and contNo = '" + contNo + "' "
            + "order by riskSeqNo";
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, LCPolGrid);

	for(var i = 0; i < LCPolGrid.mulLineCount; i++)
	{
	  //计算理赔额
	  sql = "  select sum(a.pay) "
	        + "from LJAGetClaim a, llcase b "
	        + "where a.otherNo = b.caseNo "
	        + "   and b.endCaseDate >= '" + LCPolGrid.getRowColDataByName(i, "cValiDate") + "' "
	        + "   and b.endCaseDate <= '" + LCPolGrid.getRowColDataByName(i, "endDate") + "' "
	        + "   and polNo = '" + LCPolGrid.getRowColDataByName(i, "polNo") + "' ";
	  var result = easyExecSql(sql);
	  var claimPay = "0";
	  if(result && result[0][0] != "null")
	  {
	    claimPay = result[0][0];
	  }
	  LCPolGrid.setRowColDataByName(i, "claimPay", claimPay);
	  
	  //计算赔付率
	  var pay = parseFloat(claimPay);
	  var prem = parseFloat(LCPolGrid.getRowColDataByName(i, "sumPrem"));
	  
	  var claimPayRate = "0";
	  if(prem != 0)
	  {
	    claimPayRate = (pay/prem);
	  }
	  LCPolGrid.setRowColDataByName(i, "claimPayRate", "" + pointFour(claimPayRate));
	}
}

//退保试算提交
function edorBudget()
{
  if(!checkData())
  {
    return false;
  }
  
  var showStr="正在解约试算，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.action = "../case/LLContDealBudgetSave.jsp";
  fm.submit();
}

function checkData()
{
  var checkedCount = 0;
  for(var i = 0; i < LCPolGrid.mulLineCount; i++)
  {
    if (LCPolGrid.getChkNo(i))
    {
      checkedCount++;
    }
  }
  if(checkedCount == 0)
  {
    alert("请选择险种")
    return false;
  }
  
  return true;
}

function afterSubmit(flag, content)
{
  try { showInfo.close(); } catch(ex) { }
	window.focus();
	getSumMoney();
	if (flag == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	setUliInfo();
}

/**返回父页面
 *1、在协议退保时，需要把退保试算数据显示在协议退保明细页面，并关闭本页面
  2、在综合查询菜单中，返回按钮无效
  3、其他情况下，直接关闭本页面
 */
function returnParent()
{
  //把退保试算数据显示在协议退保明细页面
  try
  {
    for(var i = 0; i < LCPolGrid.mulLineCount; i++)
    {
      top.opener.setBudgetResultToXTDetail();
    }
  }
  catch(ex)
  {}
  //关闭本窗口
  try
  {
    if(top.opener != undefined)
    {
      top.opener.focus();
      top.close();
    }
  }
  catch(ex)
  {}
}

function getSumMoney()
{
	var sumMoney=0;
	
	for(var i = 0; i < LCPolGrid.mulLineCount; i++)
	{
			
		if (LCPolGrid.getChkNo(i)) 
		{
			sumMoney +=  parseFloat(LCPolGrid.getRowColDataByName(i, "getMoney"),2) ;	
		}
	}
	fm.GetMoney.value = sumMoney;
}

function   tofloat(f,dec)   {     
	if(dec<0)   
	return   "Error:dec<0!";     
	result=parseInt(f)+(dec==0?"":".");     
	f-=parseInt(f);     
	if(f==0)     
	for(i=0;i<dec;i++)   result+='0';     
	else   {     
	for(i=0;i<dec;i++)   f*=10;     
	result+=parseInt(Math.round(f));     
	}     
	return   result;     
}  

//20090113 ZhangGuoming 
//文字说明仅针对万能险：
//该试算结果未扣除解约手续费，因追加保费、部分领取等操作，可能会造成该试算结果与实际解约金额不同。
function setUliInfo()
{
  var hasULI = false;
  for(var i = 0; i < LCPolGrid.mulLineCount; i++)
  {
    var sql = "select 1 from LMRiskApp where riskcode = '" + LCPolGrid.getRowColData(i,5) + "' "
            + "and RiskType4 = '4'";
    if(easyQueryVer3(sql))
    {
      hasULI=true;
    }
  }
  if(hasULI)
  {
    divUliInfo.style.display="";
  }
  else
  {
    divUliInfo.style.display="none";
  }
}

//20090113 Zhanggm 万能保单事项
//只有万能保单可以点击【万能保单事项】按钮，否则提示：非万能保单不能查看该页面。
function showOmniInfo()
{
  if(LCContGrid.mulLineCount == 0)
  {
    alert("请先查询保单");
    return false;
  }
  if(LCPolGrid.mulLineCount == 0)
  {
    alert("请先选择解约试算保单");
    return false;
  }  
  var hasULI = false;
  for(var i = 0; i < LCPolGrid.mulLineCount; i++)
  {
    var sql = "select 1 from LMRiskApp where riskcode = '" + LCPolGrid.getRowColData(i,5) + "' and RiskType4 = '4'";
    if(easyQueryVer3(sql))
    {
      hasULI=true;
    }
  }
  if(hasULI)
  {
    var cContNo = fm.all('ContNo').value;
    var sql = "select polno,riskcode from lcpol a where contno = '"+cContNo+"' "
            + "and exists (select 1 from lmriskapp where RiskCode = a.riskcode and RiskType4 = '4') ";
    var rs = easyExecSql(sql);
    if(rs == null || rs[0][0] == null || rs[0][0] == "")
    {
      alert( "此保单不是万能险保单！");
    }
    else
    {
      var cPolNo = rs[0][0];
      var cRiskCode = rs[0][1];
  	  var sql = "select min(InsuAccNo) from LMRiskToAcc where RiskCode = '" + cRiskCode + "'";
  	  var arrReturn = easyExecSql(sql);
  	  var cInsuAccNo = arrReturn[0][0];
      window.open("../bq/OmniInfo.jsp?ContNo="+cContNo+"&PolNo=" + cPolNo + "&RiskCode=" + cRiskCode + "&InsuAccNo=" + cInsuAccNo);
  	}
  }
  else
  {
    alert("非万能保单不能查看该页面");
  }
}
function prtNotice()
{
	if(fm.all('GetMoney').value == null || fm.all('GetMoney').value =='')
	{
		alert("尚未退保试算，不能打印！");
		return false;
	}
	
//if(fm.all('BankCode').value == null || fm.all('BankCode').value =='')
//{
//	alert("请录入贷款银行！");
//	return false;
//}
//
//if(fm.all('ApproveCode').value == null || fm.all('ApproveCode').value =='')
//{
//	alert("请录入审核人！");
//	return false ;
//}
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=ZY001&OtherNo="+fm.all('contNoTop').value+"&StandbyFlag1="+fm.all('GetMoney').value;
					 
	fm.submit();
	
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}