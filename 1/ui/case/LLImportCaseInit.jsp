<%
//程序名称：LCInuredListInput.jsp
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String LoadFlag= request.getParameter("LoadFlag");
LoadFlag= LoadFlag==null?"":LoadFlag;
String RgtNo= request.getParameter("RgtNo") ;
String CaseNo= request.getParameter("CaseNo");
String GrpContNo = request.getParameter("GrpContNo");
String AppPeoples = request.getParameter("AppPeoples");
String LoadC="";
if(request.getParameter("LoadC")!=null)
{
LoadC = request.getParameter("LoadC");
}
%>
<script language="JavaScript">
  var loadFlag="<%=LoadFlag%>";
  var RgtNo = '<%=RgtNo%>';
  RgtNo= (RgtNo=='null'?"":RgtNo);
  var CaseNo = '<%=CaseNo %>';
  CaseNo= (CaseNo=='null'?"":CaseNo);
function initInpBox()
{ 
  try
  {                                   
      fm.LoadFlag.value = loadFlag;
      fm.RgtNo.value = RgtNo;
      fm.AppPeople.value = "<%=AppPeoples%>";
      fm.GrpContNo.value = "<%=GrpContNo%>";
      fm.LoadC.value="<%=LoadC%>";
      fm.RiskCode.value = "<%=request.getParameter("RiskCode")%>"
  }
  catch(ex)
  {
    alert("在LCInuredListInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LCInuredListInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initGrpCaseGrid();
    queryGrpCaseGrid();
    initDiskErrQueryGrid();
    QueryErrList();
    queryList();
  }
  catch(re)
  {
    alert("LCInuredListInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initGrpCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
    
    iArray[1]=new Array("理赔号", "90px", "0", "0");
    iArray[2]=new Array("社保号", "100px", "0", "0");
    iArray[3]=new Array("客户姓名", "100px", "0", "0");
    iArray[4]=new Array("客户号", "100px", "0", "0");
    iArray[5]=new Array("申请日期", "100px", "0", "0");
    iArray[6]=new Array("申请金额", "100px", "0", "0");
    GrpCaseGrid = new MulLineEnter("fm","GrpCaseGrid");
    GrpCaseGrid.displayTitle = 1;
    GrpCaseGrid.canSel =1;
//    GrpCaseGrid.canChk = 1
    GrpCaseGrid.hiddenPlus=1;  
    GrpCaseGrid.hiddenSubtraction=1; 
    GrpCaseGrid.loadMulLine(iArray);
//    GrpCaseGrid.selBoxEventFuncName = "ShowCase";
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initDiskErrQueryGrid()
{
    var iArray = new Array();
      
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="30px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array("理赔号", "90px", "0", "0");
      iArray[2]=new Array("身份证号", "100px", "0", "0");
      iArray[3]=new Array("客户姓名", "100px", "0", "0");
      iArray[4]=new Array("客户号", "100px", "0", "0");
      iArray[5]=new Array("申请日期", "100px", "0", "0");
      iArray[6]=new Array("申请金额", "100px", "0", "0");
      iArray[7]=new Array("费用总额", "100px", "0", "0");
      iArray[8]=new Array("起付线", "100px", "0", "0");
      iArray[9]=new Array("统筹支付", "100px", "0", "0");
      iArray[10]=new Array("统筹自费", "100px", "0", "0");
      iArray[11]=new Array("大额救助自负", "100px", "0", "0");
                                                                                                           
      DiskErrQueryGrid = new MulLineEnter( "fm" , "DiskErrQueryGrid" ); 
      //这些属性必须在loadMulLine前
      DiskErrQueryGrid.mulLineCount = 0;   
      DiskErrQueryGrid.displayTitle = 1;
	  	DiskErrQueryGrid.hiddenPlus = 1;
      DiskErrQueryGrid.hiddenSubtraction = 1;
      DiskErrQueryGrid.canChk = 1;
//      DiskErrQueryGrid.selBoxEventFuncName = "showErrList";
      DiskErrQueryGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
