
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var tSaveFlag = "0";
var mDebug="1";
var mAction = "";
var tSaveType="";

var turnPage = new turnPageClass(); 


function modify()
{
	var selno = GrpContGrid.getSelNo()-1;
	if (selno < 0)
	{
		alert("请选择保单");
		return false;
	}
	if(!isNumeric(GrpContGrid.getRowColDataByName(selno,"RiskAmnt"))){
		alert("保额录入错误");
		return false;
	}

    fm.all("operate").value='UPDATE';
    var showStr="正在保存数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

   	fm.action="./GrpContRiskAmntModifySave.jsp";
  	fm.submit();
	
}
function SearchGrpCont(){
	if( verifyInput2() == false ) return false;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句
  
  var strSql = " SELECT distinct a.grpcontno,a.appntname,a.appntno,a.contplancode,a.riskcode,b.riskname,a.riskamnt" +
  " FROM lcpol a,lmrisk b "
  +"WHERE a.riskcode=b.riskcode" 
  + getWherePart("a.grpcontno","grpContNo")
  + getWherePart("a.riskcode","RiskCode");
   turnPage.queryModal(strSql,GrpContGrid);
  showInfo.close();

}

function afterSubmit(FlagStr, content)
{
  showInfo.close();
  SearchGrpCont();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
  
}

