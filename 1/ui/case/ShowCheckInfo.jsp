<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
 * Name     ：ShowCheckInfo.jsp
 * Function ：显示审核履历的程序
 * Author   :LiuYansong
 * Date     :2003-12-22
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%
  //输出参数
  System.out.println("开始执行ShowCheckInfo.jsp页面");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";


  String tRgtNo = request.getParameter("RgtNo");


  VData tVData = new VData();
  tVData.addElement(tRgtNo);
  ShowCheckInfoUI mShowCheckInfoUI = new ShowCheckInfoUI();
  if (!mShowCheckInfoUI.submitData(tVData,"INIT"))
  {
    Content = " 查询失败，原因是: " + mShowCheckInfoUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }
  else
  {
    tVData.clear();
    tVData = mShowCheckInfoUI.getResult();

    LLClaimUWMDetailSet tLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
    tLLClaimUWMDetailSet.set((LLClaimUWMDetailSet)tVData.getObjectByObjectName("LLClaimUWMDetailSet",0));

    int n = tLLClaimUWMDetailSet.size();
    if(n>0)
    {
      System.out.println("查询的记录是 "+n);
      String Strtest = "0|" + n + "^" + tLLClaimUWMDetailSet.encode();
      System.out.println("QueryResult: " + Strtest);
      %>
      <script language="javascript">
      try
      {
        parent.fraInterface.displayQueryResult('<%=Strtest%>');
      }
      catch(ex) {}
      </script>
      <%
    }
  }

System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>