<html>
	<%
	//Name:RegisterInput.jsp
	//Function：立案界面的初始化程序
	//Date：2005-02-16 17:44:28
	//Author ：YangMing
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%
  
 		String CurrentDate= PubFun.getCurrentDate();   
    String CurrentTime= PubFun.getCurrentTime();
  %>
	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="LLQDSimpleClaimInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLQDSimpleClaimInit.jsp"%>
   <script language="javascript">
   function initDate(){
     fm.OpStartTime.value="<%=CurrentTime%>";
     fm.OpStartDate.value="<%=CurrentDate%>";
   }
   </script>
	</head>
	<body  onload="initForm();initElementtype();">
		<form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">
	  <table >
	    	<tr>
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpcontRemarkInfo);">
	    		</td>
	    		<td class= titleImg>
	    			 客户特约信息 
	    		</td>
	    	<TD >
	    		 <input class=cssButton type=button value="保单信息" onclick="ContQuery();">
	    	</TD>
	    	</tr>
	    </table>
	    
	<div id= "divGrpcontRemarkInfo" style= "display: ''" >
		<table  class= common>
			<TR  class= common8>
				<TD  class= input8>
		  		<textarea name="PerRemark" cols="95%" rows="2" witdh=25% class="common" readonly ></textarea>
				</TD>
			</TR>
		</table>
  </div>
  
	<%@include file="CaseTitle.jsp"%>
	  <div id="divCustomerSearch" style="display:''">
			<table>
				<tr>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerSearch);">
					</td>
					<td class= titleImg>
						已录入名单
					</td>
				</tr>
			</table>
    <Div  id= "divLDPerson1" style= "display: ''" align= center>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanGrpCaseGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">      
    </Div>
  </div>
	  <div id="divCustomerSearch" style="display:''">
			<table>
				<tr>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerSearch);">
					</td>
					<td class= titleImg>
						客户查询
					</td>
				</tr>
			</table>
			<table  class= common>
				<TR  class= common8>
					<TD  class= title8 id='titleClientNo' style="display:'none'">客户序号</TD><TD  class= input8 id='inputClientNo' style="display:'none'"><Input class=readonly readonly name="ClientNo"  onkeydown="QueryOnKeyDown()"></TD>
			    <TD  class= title8>医保号码</TD><TD  class= input8><Input class=common name="SecurityNo" onkeydown="QueryOnKeyDown()" ></TD>
					<TD  class= title8>客户姓名</TD><TD  class= input8><Input class=common name="CustomerName"  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>客户号码</TD><TD  class= input8><Input class=common name="CustomerNo" onkeydown="QueryOnKeyDown()"></TD>
				</tr>
				<TR  class= common8>
					<TD  class= title8>身份证号</TD><TD  class= input8><Input class=common name="IDNo"  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>性别</TD><TD class= input8><input class=codeno name=Sex CodeData="0|2^0|男^1|女" onclick="return showCodeListEx('CustomerSex',[this,SexName],[0,1]);" onkeyup="return showCodeListKeyEx('CustomerSex',[this,SexName],[0,1]);"><input class=codename  name=SexName></TD>
					<TD  class= title8>出生日期</TD><TD class= input8><Input class=common name="CBirthday"  ></TD>
					<TD  class= title8>人员状态</TD><TD class= Input8><input class=codeno name=InsuredStat CodeData="0|2^1|在职^2|退休" onclick="return showCodeListEx('insurestat',[this,InsuredStatName],[0,1]);" onkeyup="return showCodeListKeyEx('insurestat',[this,InsuredStatName],[0,1]);"><input class=codename  name=InsuredStatName></TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>单位名称</TD><TD class= input8><Input class=common name="GrpName"></TD>
					<TD  class= title8>医保编码</TD><TD  class= input8><input class=code  name=HosSecNo onclick="return showCodeList('llsechosquery',[this,HospitalName,HospitalCode],[0,1,2],null,this.value,'Securityno',1,240);" onkeyup="return showCodeListKey('llsechosquery',[this,HospitalName,HospitalCode],[0,1],null,this.value,'Securityno',1,240);"></TD>
          <TD  class= title8>医院名称</TD>
          <TD  class= input8> <input class=common  name=HospitalName elementtype=nacessary verify="医院名称|notnull"></TD>
          <TD  class= title8>医院代码</TD>
          <TD  class= input8> <input class=code  name=HospitalCode onclick="return showCodeList('llhospiquery',[this,HospitalName,HosSecNo],[0,1,5],null,fm.HospitalName.value,'Hospitname',1,240);" onkeyup="return showCodeListKey('llhospiquery',[this,HospitalName,HosSecNo],[0,1,5],null,fm.HospitalName.value,'Hospitname',1,240);" elementtype=nacessary verify="医院代码|notnull"></TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>住院号</TD><TD  class= input8><Input class=common name="inpatientNo"  ></TD>
					<TD  class= title8>住院天数</TD><TD class= input8> <input class=common  name=RealHospDate onfocus="caldate()"></TD>
					<TD  class= title8>医疗形式</TD><TD class= input8 ><input class=codeno name=CureType CodeData="0|2^1|门诊大病^2|住院费用^3|异地住院" onclick="return showCodeListEx('curetype',[this,CureTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('curetype',[this,CureTypeName],[0,1]);"><input class=codename  name=CureTypeName></TD>
					<TD  class= title8>结算方式</TD><TD class= input8 ><input class=codeno name=BudgetType CodeData="0|2^1|本地报销^2|异地报销" onclick="return showCodeListEx('budgettype',[this,BudgetTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('budgettype',[this,BudgetTypeName],[0,1]);"><input class=codename  name=BudgetTypeName></TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>入院诊断</TD><TD class= input8><Input class=common name="SICDName" ></TD>
					<TD  class= title8>入院ICD编码</TD> <TD class= input8><Input onClick="return showCodeList('lldiseas',[this,SICDName],[1,0],null,fm.SICDName.value,'ICDName',1,240);" onkeyup="return showCodeListKey('lldiseas',[this,SICDName],[1,0],null,fm.SICDName.value,'ICDName',1,240);" class=code name="SICDCode" ></TD>
					<TD  class= title8>出院诊断</TD><TD class= input8><Input class=common name="MICDName" ></TD>
					<TD  class= title8>出院ICD编码</TD> <TD class= input8><Input onClick="return showCodeList('lldiseas',[this,MICDName],[1,0],null,fm.MICDName.value,'ICDName',1,240);" onkeyup="return showCodeListKey('lldiseas',[this,MICDName],[1,0],null,fm.MICDName.value,'ICDName',1,240);" class=code name="MICDCode" ></TD>
				</TR>
				<TR  class= common8>
          <TD  class= title8>入院日期</TD><TD class= input8><input class="coolDatePicker"  dateFormat="short"  name=HospStartDate onblur="caldate()"  verify="日期|date"></TD>
          <TD  class= title8>出院日期</TD><TD class= input8><input class="coolDatePicker"  dateFormat="short"  name=HospEndDate onblur="caldate()" verify="日期|date"></TD>
					<TD  class= title8>医院结算时间</TD><TD class= input8> <input class="coolDatePicker"  dateFormat="short"  name=FeeDate onblur="fillDate()" verify="医院结算日期|date"></TD>
					<TD  class= title8>医保结算时间</TD><TD class= input8> <input class="coolDatePicker"  dateFormat="short"  name=SFeeDate onblur="fillDate()" verify="医保结算日期|date"></TD>
				</TR>
			</table>
		</div>
		<table>
			<TR>
				<td>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEventInfo);">
				</td>
				<td class= titleImg>
					申请原因
				</TD>
			</TR>
		</table>
		<table class=common>
			<TR  class= common8>
				<TD  class= title8>事故者现状</TD>
				<TD  class= input8><Input class=codeno name="CustStatus" onClick="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" onkeyup="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" verify="事故者现状|code:llcuststatus&INT"><Input class= codename name="CustStatusName"  ></TD>
				<TD  class= title8>死亡日期</TD>
				<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="DeathDate" verify="死亡日期|date"></TD>
				<TD  class= title8></TD>
				<TD  class= input8></TD>
			</TR>
		</table>
    <Table>
    	<TR>
    		<TD class=common>
    			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg>
    			账单信息
    		</TD>
    	</TR>
    </Table>
    <Div  id= "divLDPerson1" style= "display: ''" align = center>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanFeeGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    </Div>
    <Div  id= "divLDPerson1" style= "display: 'none'" align = center>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanEventGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    </Div>
</br>
<Div  id= "aa" style= "display: ''" align= right>
	<input class=cssButton  id="IdAppConf" type=button value="申请确认" onclick="submitForm()">
	<input class=cssButton  id="IdAppConf" type=button value="清    空" onclick="initForm()">
	<input class=cssButton type=button value="撤  件" onclick="DealCancel()">
	<input class=cssButton  type=button value="提起调查" onclick="submitFormSurvery()">
</div>

<hr/>
<Div  id= "divnormalquesbtn" style= "display: ''" align= right>
	<input class=cssButton  type=hidden value="打印理赔受理回执" onclick="PrintPage()">
</Div>

<Div  id= "divRgtFinish" style= "display: 'none'" align= right>
	<input class=cssButton  type=button  value="团体申请完毕" onclick="RgtFinish()">
	<input class=cssButton  type=button  value="返回" onclick="top.close()">
</Div>
</Div>

	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="Reason" name="Reason">

	<input type=hidden id="GrpContNo" name="GrpContNo">
	<input type=hidden id="LoadFlag" name="LoadFlag" value="1">
	<input type=hidden id="operate" name="operate">
	<input type=hidden id="rgtflag" name="rgtflag">
	<input type=hidden id="realpeoples" name="realpeoples">
	<input type=hidden id="AccFlag" name="AccFlag">
	<input type=hidden id="OpStartDate" name="OpStartDate">
	<input type=hidden id="OpStartTime" name="OpStartTime">
	<input name ="LoadC" type="hidden">
	<input name ="RiskCode" type="hidden">
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>


</body>
</html>