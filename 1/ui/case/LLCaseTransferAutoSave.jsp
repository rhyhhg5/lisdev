<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSurveyReplySave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
 
  LLCaseSet tLLCaseSet = new LLCaseSet();
 LLCaseDB tLLCaseDB = new LLCaseDB();
  CaseTransferAutoBL tCaseTransferAutoBL   = new CaseTransferAutoBL();

  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

  String CaseNo ="";
  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
	String Handler= request.getParameter("Handler");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录


  String tRNum[] = request.getParameterValues("InpCheckGridChk");
  String tsOtherNo[] = request.getParameterValues("CheckGrid2");
  String Operate = request.getParameter("llusercode");
	String Remark = request.getParameter("Remark");

  	if (tsOtherNo != null) 
		{
        int CaseCount = tsOtherNo.length; 
        int ChkCount = 0;
       for (int i = 0; i < CaseCount; i++) 
        {
          if ( tsOtherNo[i] != null && !tsOtherNo[i].equals(""))        		
        	{
	        	System.out.println("111111111111111111111"+tRNum[i]);
	        	if("1".equals( tRNum[i])){	
	        	ChkCount=ChkCount+1;
	        	LLCaseSchema tLLCaseSchema = new LLCaseSchema();        	
	        	tLLCaseSchema.setCaseNo(tsOtherNo[i]);	
	  
	        	tLLCaseSet.add(tLLCaseSchema);      
	
	        	System.out.println("00000000000000000000000000"+ChkCount+"111111"+tsOtherNo[i]);
		         
		        }     	
        	} 
        }

      }
 
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLLCaseSet);
  	tVData.add(tG);
	  
  	System.out.println("Handler999999999999999"+Handler);
  	System.out.println("tVData88888888888"+tVData);
    tCaseTransferAutoBL.submitData(tVData,Handler);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseTransferAutoBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
