<%
//程序名称：BlackListInput.jsp
//程序功能：
//创建日期：2003-01-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LLAppClaimReasonSchema tLLAppClaimReasonSchema   = new LLAppClaimReasonSchema();
   LLAppClaimReasonSet tLLAppClaimReasonSet   = new LLAppClaimReasonSet();
	
  GrpContInvalidateUI tGrpContInvalidateUI = new GrpContInvalidateUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String operator=tG.Operator;
  String grpContNo=request.getParameter("grpContNo");
  tLLAppClaimReasonSchema.setRgtNo(grpContNo);
  tLLAppClaimReasonSchema.setOperator(operator);
  tLLAppClaimReasonSchema.setCaseNo("结合市场");
  tLLAppClaimReasonSchema.setReasonCode("98");
  tLLAppClaimReasonSchema.setReasonType("9");
  tLLAppClaimReasonSchema.setCustomerNo("88888888");
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLLAppClaimReasonSchema);
	tVData.addElement(tG);
  try
  {
    tGrpContInvalidateUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tGrpContInvalidateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	//Content = " 操作失败，原因是:" + tError.getFirstError();
    	Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

