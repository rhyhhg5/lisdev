/*******************************************************************************
* Name: LLRegisterCheckInput.js
* Function:检录界面的函数处理程序
* Author:wujs,Xx
*/
var showInfo;
var turnPage = new turnPageClass();
function initQuery()
{
  	//一般疾病,根据MainDiseaseFlag判断主要疾病,放在第一位
    var strSQL ="select Diagnose,DiseaseName,DiseaseName,DiseaseCode,HospitalCode,HospitalName,"
 	 	+" DoctorName,SerialNo,DoctorNo,OperationFlag  from llcasecure a,llcase b "
 	 	+" where a.caseno=b.caseno and b.customerno='"+fm.CustomerNo.value+"'"
  		+" and SeriousFlag='0' order by MainDiseaseFlag desc";
  	var arr = easyExecSql(strSQL );
  	if (arr)
  	{
	  	displayMultiline( arr, DiseaseGrid);
  	}

  	//重疾,根据MainDiseaseFlag判断主要疾病,放在第一位
	  strSQL ="select DiseaseName,DiseaseCode,HospitalName,HospitalCode,DoctorName,DiagnoseDesc,'',"
		  +" SeriousDutyFlag,JudgeDepend,case when SeriousDutyFlag='0' then '不成立' else '成立' end ,"
		  +" DiagnoseDate from llcasecure a,llcase b "
		  +" where a.caseno=b.caseno and b.customerno='"+fm.CustomerNo.value+"'"
		  +" and SeriousFlag='1' order by MainDiseaseFlag desc" ;
	  var brr = easyExecSql(strSQL );
	  if ( brr )
	  {
	    displayMultiline( brr, SeriousDiseaseGrid);
	    fm.SDLoadFlag.value='1'
	  }
	  //意外
	  strSQL ="select AccidentNo,Type,TypeName,'',Name,Code,ReasonCode,Reason,a.Remark "
	  	+" from llaccident a,llcase b "
		+" where a.caseno=b.caseno and b.customerno='"+fm.CustomerNo.value+"'";
	  var crr = easyExecSql(strSQL );
	  if ( crr )
	  {
	  	displayMultiline( crr, AccidentGrid);
	  }
	  //手术
	  strSQL ="select OperationName,OperationCode,OpFee,OpLevel,SerialNo from lloperation a,llcase b"
	  	+" where a.caseno=b.caseno and b.customerno='"+fm.CustomerNo.value+"'";
	  var drr = easyExecSql(strSQL );
	  if ( drr )
	  {
	  	displayMultiline( drr, DegreeOperationGrid);
	  }	 
	  //残疾
	  strSQL = "select Name,Code,DeformityGrade,DeformityRate,DiagnoseDesc,'',JudgeDate,JudgeOrganName,JudgeOrgan "
	  	+" from llcaseinfo a,llcase b "
	  	+" where a.caseno=b.caseno and b.customerno='"+fm.CustomerNo.value+"'";
	  var err = easyExecSql(strSQL);
	  if (err)
	  {
	  	displayMultiline(err,DeformityGrid);
	  }
	  
	  //失能
	  strSQL = "select (case when DisabilityFlag='1' then '是' else '否' end),DisabilityFlag,DisabilityDate,"
	  	+" ObservationDate from LLDisability a,llcase b"
	  	+" where a.caseno=b.caseno and b.customerno='"+fm.CustomerNo.value+"'";
	  var err = easyExecSql(strSQL);
	  if (err)
	  {
	  	displayMultiline(err,DisabilityGrid);
 	  }
}
