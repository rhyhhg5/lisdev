<%
//程序名称：LLBranchCaseReport.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head>
</head>
<body>
<%


String flag = "0";
String FlagStr = "";
String Content = "";
GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
	String operator=tG.Operator;


//设置模板名称
String FileName = "LLPendingReport";
String ManageCom = request.getParameter("organcode");
System.out.println(ManageCom);
String StartDate = request.getParameter("StartDate");
System.out.println(StartDate);
String EndDate = request.getParameter("EndDate");
System.out.println(EndDate);
String caseType = request.getParameter("ContType");
System.out.println(caseType);
//待处理机构
String MagcomType = request.getParameter("MagcomType");
System.out.println("MagcomType:"+MagcomType);

String partSQL = "";
        if("1".equals(caseType)){
        	partSQL = " and  exists  (select 1 from llregister where rgtno=c.rgtno and (applyertype='0' or applyertype is null)) ";
        	
        }else if("2".equals(caseType)){
        	partSQL = " and accdentdesc='磁盘导入'";
        	
        }else if("3".equals(caseType)){
        	partSQL = " and caseprop='09' ";
        	
        }else if("4".equals(caseType)){
        	partSQL = " and accdentdesc='批量受理' ";
        	
        }else if("5".equals(caseType)){//TODO:新增预付赔款
        	partSQL = " and prepaidflag='1' ";
        }else{
        	partSQL = "";
        }
       
String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");

String comlength = "";
if(ManageCom.length()==2){
  comlength="4";
}
else {
  comlength="8";
  }
  

//新增“机构待处理案件统计”
if("1".equals(MagcomType)){
		System.out.println("总公司处理。。。");
		comlength="4";
		ManageCom="86";
		partSQL += " and dealer in (select usercode  from lduser ldu where ldu.comcode='86')";
	
}else if("2".equals(MagcomType)){
	System.out.println("分公司处理。。。");
	comlength="8";
	ManageCom=ManageCom.substring(0, 4);
	partSQL += " and dealer in (select usercode from lduser where comcode like '"+ManageCom+"%')";
}
System.out.println("partSQL:"+partSQL);

if(sd.compareTo(ed) > 0)
{
flag = "1";
FlagStr = "Fail";
Content = "操作失败，原因是:统计止期比统计统计起期早";
}



JRptList t_Rpt = new JRptList();
String tOutFileName = "";
if(flag.equals("0"))
{
t_Rpt.m_NeedProcess = false;
t_Rpt.m_Need_Preview = false;
t_Rpt.mNeedExcel = true;

StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
String CurrentDate = PubFun.getCurrentDate();
CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");


t_Rpt.AddVar("StartDate", StartDate);
t_Rpt.AddVar("EndDate", EndDate);
t_Rpt.AddVar("managecom",ManageCom);
t_Rpt.AddVar("operator",operator);
t_Rpt.AddVar("ComLength",comlength);
t_Rpt.AddVar("ManageComName", ReportPubFun.getMngName(ManageCom));
t_Rpt.AddVar("PARTSQL", partSQL);
String YYMMDD = "";
YYMMDD = StartDate.substring(0, StartDate.indexOf("-")) + "年"
       + StartDate.substring(StartDate.indexOf("-") + 1,StartDate.lastIndexOf("-")) + "月"
       + StartDate.substring(StartDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("StartDateN", YYMMDD);
YYMMDD = "";
YYMMDD = EndDate.substring(0, EndDate.indexOf("-")) + "年"
       + EndDate.substring(EndDate.indexOf("-") + 1,EndDate.lastIndexOf("-")) + "月"
       + EndDate.substring(EndDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("EndDateN", YYMMDD);
YYMMDD = "";
YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("MakeDate", YYMMDD);

RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);

t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName;
System.out.println("strVFPathName : "+ strVFPathName);
System.out.println("=======Finshed in JSP===========");
System.out.println(tOutFileName);

rpt.updateReportMetaData(strVFFileName);

response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >
