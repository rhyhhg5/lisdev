<%
//程序名称：LLRegisterPrintSave.jsp
//程序功能：
//创建日期：2007-11-23 17:53:36
//创建人  ：LiuLi
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.lang.String"%>
<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
   
	//输出参数
	//String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	//tG.ClientIP = request.getRemoteAddr();
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
	tG.ServerIP =tG.GetServerIP();
	System.out.println("------操作员IP: "+tG.ClientIP);
	System.out.println("------服务器IP: "+tG.ClientIP);



  	transact = request.getParameter("fmtransact");
  	System.out.println("transact : "+transact);
	String tRgtNo = request.getParameter("GrpRgtNo");
    String tSCaseNo = request.getParameter("StartCaseNo");
    String tECaseNo = request.getParameter("EndCaseNo");
    String tCaseNoBatch = request.getParameter("SingleCaseNo");
    String tBatchType = request.getParameter("selno");

	TransferData PrintElement = new TransferData();
	PrintElement.setNameAndValue("RgtNo",tRgtNo);
	PrintElement.setNameAndValue("SCaseNo",tSCaseNo);
	PrintElement.setNameAndValue("ECaseNo",tECaseNo);
	PrintElement.setNameAndValue("CaseNoBatch",tCaseNoBatch);
	PrintElement.setNameAndValue("BatchType",tBatchType);

		// 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(PrintElement);
  	tVData.add(tG);
    LLRegisterPrintBL tLLRegisterPrintBL   = new LLRegisterPrintBL();
    if(!tLLRegisterPrintBL.submitData(tVData,"batch"))
    {
		Content = tLLRegisterPrintBL.mErrors.getFirstError().toString();
    	FlagStr = "true";	
	}
	else{	
      Content = "打印成功！";
      FlagStr = "Fail";
    }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
