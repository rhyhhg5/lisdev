<%
//Name：LLRegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%GlobalInput mG = new GlobalInput();
   	mG=(GlobalInput)session.getValue("GI");
   	String tShowCaseRemarkFlag = "";//
	tShowCaseRemarkFlag = (String)session.getAttribute("ShowCaseRemarkFlag");
	System.out.println("tShowCaseRemarkFlag:"+tShowCaseRemarkFlag);
%>
<%

String LoadFlag= request.getParameter("LoadFlag");
LoadFlag= LoadFlag==null?"":LoadFlag;
String RgtNo= request.getParameter("RgtNo") ;

//RgtNo=RgtNo== null ?"":RgtNo;
String CaseNo= request.getParameter("CaseNo");
//CaseNo=CaseNo==null?"":CaseNo;
String LoadC="";
if(request.getParameter("LoadC")!=null)
{
LoadC = request.getParameter("LoadC");
}
String LoadD="";
if(request.getParameter("LoadD")!=null)
{
LoadD = request.getParameter("LoadD");
}
%>

<script language="JavaScript">
  var loadFlag="<%=LoadFlag%>";
  var RgtNo = '<%=RgtNo%>';
  RgtNo= (RgtNo=='null'?"":RgtNo);
  var CaseNo = '<%=CaseNo %>';
  CaseNo= (CaseNo=='null'?"":CaseNo);

  function initInpBox( )
  {
    try {
      fm.reset();
      fm.CustomerNo.value="";
//      fm.CaseOrder.value="1";
//      fm.CaseOrderName.value="普通";
      fm.LoadFlag.value = loadFlag;
      fm.RgtNo.value = RgtNo;
      fm.CaseNo.value = CaseNo;
      fm.LoadC.value="<%=LoadC%>";
      fm.RiskCode.value = "<%=request.getParameter("RiskCode")%>"
      if(fm.RiskCode.value=='1605'){
        tiAppMoney.style.display='';
        idAppMoney.style.display='';
        titemp0.style.display='none';
        idtemp0.style.display='none';
      }
      else{
        tiAppMoney.style.display='none';
        idAppMoney.style.display='none';
        titemp0.style.display='';
        idtemp0.style.display='';
      }
      if (fm.LoadC.value=='2'){
        //			fm.CaseNo.focus();
        aa.style.display='none';
        divnormalquesbtn.style.display='none';
        //    	CaseChange();
      }
      fm.LoadD.value="<%=LoadD%>";
      if(fm.LoadD.value=='1')
      {
          // add new
       document.getElementById('sub1').disabled = true;
       document.getElementById('sub2').disabled = true;
       document.getElementById('sub3').disabled = true;
       document	.getElementById('sub4').disabled = true;
       document	.getElementById('sub5').disabled = true;
       document	.getElementById('sub6').disabled = true;//理赔二核按钮隐藏
       document	.getElementById('IdAppConf').disabled = true;
          
      }
      
    }
    catch(ex) {
      alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initForm(){
    try{ 
      titleGrp.style.display='none';
      inputGrp.style.display='none';
      if(loadFlag=="0")
      {
        titleGrp.style.display='';
        inputGrp.style.display='';
        div1.style.display='none';
        divRegisterInfo.style.display='none';
        fm.Relation.value='05';
        fm.RgtType.value='9';
      }
      initInpBox();
      initEventGrid();
      checkRgtFlag();
      fm.ShowCaseRemarkFlag.value = "<%=tShowCaseRemarkFlag%>";
      initQuery();
      initDate();
      initCheckBox();
      
      showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出
    
      fm.Handler.value = "<%=mG.Operator%>";
      var tCaseNo = "<%=request.getParameter("CaseNo")%>";
      if (tCaseNo!=null&&tCaseNo!=""&&tCaseNo!="null") {
      	var sql = "select rigister from llcase where caseno='"+tCaseNo+"'";
      	
      	var arrResult1=easyExecSql(sql);
      	//alert(arrResult1);
      	if(arrResult1==null || arrResult1===""){
      		fm.Handler.value = "<%=mG.Operator%>";
      	}else{
      		fm.Handler.value = arrResult1;
      	}
      }
      fm.ModifyDate.value = getCurrentDate();
    }
    catch(re){
      alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
    }
  }

  // 保单信息列表的初始化
  function initEventGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","0px","10","0");
      
       iArray[1]=new Array();
        iArray[1][0]="事件号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="Subno";
        
      iArray[2]=new Array("发生日期","80px","100","1");
      iArray[2][9]="发生日期|notnull&date";

      iArray[3]=new Array("发生地点(省)","90px","100","2");
      iArray[3][4]="province1";
      iArray[3][5]="3|15";
      iArray[3][6]="1|0";
      
      iArray[4] =new Array("发生地点(市)","90px","100","2");
      iArray[4][4]="city1";
      iArray[4][5]="4|16";
      iArray[4][6]="1|0";
      iArray[4][15]="code1";
      iArray[4][17]="15";
      
      iArray[5] = new Array("发生地点(县)","90px","100","2");
      iArray[5][4]="county1";
      iArray[5][5]="5|17";
      iArray[5][6]="1|0";
      iArray[5][15]="code1";
      iArray[5][17]="16";
		
      iArray[6] = new Array("发生地点","150px","100","1")
      
      iArray[7] = new Array("事故类型","80px","0","3");
      iArray[8] = new Array("事故主题","200px","0","3");
      iArray[9] = new Array("医院名称","200px","0","3");

      iArray[10]=new Array("入院日期","80px","100","1");
      iArray[10][9]="入院日期|date";

      iArray[11]=new Array("出院日期","80px","100","1");
      iArray[11][9]="出院日期|date";

      iArray[12] = new Array("事件信息","200px","1000","1");

      iArray[13] = new Array("事件类型","60px","10","2")
      iArray[13][10]="AccType";
      iArray[13][11]="0|^1|疾病|^2|意外"
      iArray[13][12]="13|14"
      iArray[13][13]="1|0"

      iArray[14] = new Array("事件类型","60px","10","3")
      iArray[15] = new Array("发送地点省编码","60px","10","3")
      iArray[16] = new Array("发生地点市编码","60px","10","3")
	  iArray[17] = new Array("发生地点县编码","60px","10","3") 
    	  
      EventGrid = new MulLineEnter("fm","EventGrid");
      EventGrid.mulLineCount = 0;
      EventGrid.displayTitle = 1;
      EventGrid.locked = 0;
      EventGrid.canChk =1	;
      EventGrid.hiddenPlus=0;
      EventGrid.hiddenSubtraction=1;
      EventGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }

  function initQuery(){
    var tRgtNo = fm.RgtNo .value;
    var tCaseNo = fm.CaseNo.value;
    ClientafterQuery(tRgtNo,tCaseNo);
  }
  
   function initCheckBox(){

       divSimpleCase.style.display='none';
       divEasyCase.style.display='none';
       divHeadCase.style.display='none';
       divFenCase.style.display='none';
    var tarrResult ;
    var tComcode="<%=mG.ComCode%>";
    var tstrSQL1=" select code1,othersign from ldcode1 where codetype='LLRegStyle'  and code=substr('"+tComcode+"',1,4) with ur";
    if(tComcode!="86"){
    tarrResult=easyExecSql(tstrSQL1);
    }

     if(tarrResult){
     for(i=0;i<tarrResult.length;i++){
      if(tarrResult[i][0]=="1"){
  	     divEasyCase.style.display='';
  	    }
  	   if(tarrResult[i][0]=="2"){
  	     divSimpleCase.style.display='';
  	    }
  	   if(tarrResult[i][0]=="3"){
  	     divFenCase.style.display='';
  	    }
  	    if(tarrResult[i][0]=="4"){
  	     divHeadCase.style.display='';
  	    }
  	   if(tarrResult[i][0]=="5"){
  	       if(fm.RgtNo.value!=null&&fm.RgtNo.value!=""){
        var trptflag=" select rptflag from llregister where RgtNo='"+fm.RgtNo.value+"' with ur";
        tarrrptflag=easyExecSql(trptflag);
        if(tarrrptflag[0][0]=="1"){
          fm.SimpleCase.checked = true;
        }else if(tarrrptflag[0][0]=="2"){
          fm.EasyCase.checked = true;
        }else if(tarrrptflag[0][0]=="3"){
          fm.FenCase.checked = true;
        }else if(tarrrptflag[0][0]=="4"){
          fm.HeadCase.checked = true;
        }else{
  	   	   fm.all('CaseRule').value = tarrResult[i][1];
  	  	   if(fm.all('CaseRule').value=="1"){
  	         fm.EasyCase.checked = true;
  	       }else if(fm.all('CaseRule').value=="2"){
  	          fm.SimpleCase.checked = true;
  	       }else if(fm.all('CaseRule').value=="3"){
  	           fm.FenCase.checked = true;
  	       }else if(fm.all('CaseRule').value=="4"){
  	           fm.HeadCase.checked = true;
  	       }
  	     }	     
  	    }else{
  	      	fm.all('CaseRule').value = tarrResult[i][1];
  	       if(fm.all('CaseRule').value=="1"){
  	       fm.EasyCase.checked = true;
  	       }else if(fm.all('CaseRule').value=="2"){
  	       fm.SimpleCase.checked = true;
  	       }else if(fm.all('CaseRule').value=="3"){
  	       fm.FenCase.checked = true;
  	       }else if(fm.all('CaseRule').value=="4"){
  	       fm.HeadCase.checked = true;
  	       }
  	    }
  	    }
     }

     }else{
       divSimpleCase.style.display='';
       divEasyCase.style.display='';
    //   divHeadCase.style.display='';
     }
       	    if(fm.all('RgtNo').value!=0&&fm.all('RgtNo').value!=""&&fm.all('RgtNo').value!=null){
  	      //置预付赔款标志
    var tprePaid=" select prepaidflag from llregister where rgtno='"+fm.all('RgtNo').value+"' with ur";
    var tprePaidResult=easyExecSql(tprePaid);
    if(tprePaidResult[0][0]=="1"){
    	 fm.PrePaidFlag.checked = true;
    }
    }
   
  }

  function checkRgtFlag(){
  	var arrResult ;
  	var tRgtNo = fm.all('RgtNo').value;
  	if (tRgtNo!=null&&tRgtNo!="") {
  		strSQL=" select rgtclass, apppeoples from LLRegister where rgtno = '" + tRgtNo + "'";
  		arrResult=easyExecSql(strSQL);
    }
    fm.all('rgtflag').value = '0';
    if(arrResult){
      fm.all('AppNum').value = arrResult[0][1];
      var RgtClass;
      try{RgtClass = arrResult[0][0]} catch(ex) {alert(ex.message + "RgtClass")}
      strSQL1 = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
      arrResult1 = easyExecSql(strSQL1);
      if (RgtClass == '1')
      {

    	//表示团
    		var tostrSQL=" select TogetherFlag from LLRegister where rgtno = '" + tRgtNo + "'";
    		var toarrResult=easyExecSql(tostrSQL);
  	    if(toarrResult){
  	    	if(toarrResult[0][0]=="3" || toarrResult[0][0]=="4"){ //统一给付
  				//受理团体，隐藏相关控件
  	    		//divGetMode_T.style.display='';
  	    		//divGetMode.style.display='none';
  	    		//
  	    		  document.getElementById("divGetMode").innerHTML="<table class=common> 				" 
  	  	    		  											+"  <TR > 					"
  	  	    		  											+"  <TD class= title8>受益金领取方式</TD> 	"
  	  	    		  											+"  <TD class= input8><Input class=\"codeno\" name=paymode onclick=\"return showCodeList('paymode',[this,paymodename],[0,1],null,str,'1');\" onkeyup=\"return showCodeListKey('paymode',[this,paymodename],[0,1],null,str,'1');\" verify=\"受益金领取方式|code:llgetmode&INT&notnull\"><Input class=\"codename\" name=paymodename  elementtype=nacessary ></TD>" 			
  	  	    		  											+"  <TD class= title8 id=tiAppMoney style= \"display: 'none'\">本次理赔申报金额</TD> 		"
  	  	    		  											+"  <TD class= input8 id=idAppMoney style= \"display: 'none'\"><Input class= common name=\"ApplyMoney\"  ></TD>" 	  
  	  	    		  											+"  <TD class= title8 id=titemp0></TD> 		"
  	  	    		  											+"  <TD id=idtemp0 class= input8><Input class= common name=\"sql\" type=hidden></td>" 	    
  	  	    		  											+"  <TD class= title8></TD> 	"
  	  	    		  											+"  <TD class= input8><Input class= common name=\"temp1\" type=hidden></td>" 	
  	  	    		  											+"  </TR> 			"
  	  	    		  											+"  <table class=common id='divBankAcc'>"
  	  	    		  											+"  <TR  class= common8 >"
  	  	    		  											+"  <TD class= title8>银行编码</TD>"
  	  	    		  											+"  <TD class= input8><Input class=\"codeno\"  name=BankCode onclick=\"return showCodeList('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);\" onkeyup=\"return showCodeListKey('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);\" ><Input class=\"codename\"  elementtype=nacessary name=BankName ></TD>"
  	  	    		  											+"  <TD class= title8>签约银行</TD> "
  	  	    		  											+"  <TD class= input8><Input class=\"codename\" name=SendFlag readonly=\"true\" ></TD>"
  	  	    		  											+"  <TD class= title8></TD>"
  	  	    		  											+"  <TD class= input8></TD>	"			
  	  	    		  											+"  </TR>"
  	  	    		  											+"  <TR>	"
  	  	    		  											+"  <TD class= title8>银行账号</TD>"
  	  	    		  											+"  <TD class= input8><input class=common name=BankAccNo elementtype=nacessary ></TD>"
  	  	    		  											+"  <TD class= title8>银行账户名</TD>"
  	  	    		  											+"  <TD class= input8><input class= common name=AccName elementtype=nacessary></TD>"
  	  	    		  											+"  <TD class= title8>领款人和被保人关系</TD> "
  	  	    		  											+"  <TD class= input8><Input class=codeno name=RelaDrawerInsured onClick=\"return showCodeList('relation',[this,RelaDrawerInsuredName],[0,1],null,1,'1');\" onkeyup=\"showCodeListKeyEx('relation',[this,RelaDrawerInsuredName],[0,1],null,1,'1');\"  ><Input class=\"codename\" name=RelaDrawerInsuredName ></TD>"
  	  	    		  											+"  </TR>"
  	  	    		  											+"  </table>"
																+"  </table>	";
  	    		
  	    	}
  	    }
          
        titleGrp.style.display='';
        inputGrp.style.display='';
        divRgtFinish.style.display='';
        titleAppNum.style.display='';
        inputAppNum.style.display='';
        fm.all('rgtflag').value = "1";
        div1.style.display='none';
        divRegisterInfo.style.display='none';
      }
    }
  }
</script>