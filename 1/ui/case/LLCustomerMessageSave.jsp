<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLCustomerMessageSave.jsp
//程序功能：短信通知
//创建日期：2007-10-11
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>
<%
System.out.println("理赔短信通知报表");
//------------------------------
RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);
//------------------------------- 
String flag = "0";
String FlagStr = "";
String Content = "";
String Operate = "Print";


String tZipFile="";
String outname="";

GlobalInput tG = new GlobalInput();
tG = (GlobalInput)session.getValue("GI");
	
String operator=tG.Operator;


String ManageCom = request.getParameter("ManageCom");
String RgtState = request.getParameter("RgtState");
String RgtDateS = request.getParameter("RgtDateS");
String RgtDateE = request.getParameter("RgtDateE");
System.out.println(ManageCom);

	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
  	tVData.addElement(ManageCom);
  	tVData.addElement(operator);
  	tVData.addElement(ReportPubFun.getMngName(ManageCom));
  	tVData.addElement(RgtState);
  	tVData.addElement(RgtDateS);
  	tVData.addElement(RgtDateE);
  	XmlExport txmlExport = new XmlExport();
  	//vts打印的工具类
	//CombineVts tcombineVts = null;
	CError cError = new CError( );
	CErrors tError = null;
  LLCustomerMessageUI tLLCustomerMessageUI = new LLCustomerMessageUI();
  System.out.println("Start 后台处理...");
  
  try{
    if(!tLLCustomerMessageUI.submitData(tVData,Operate))
    {
      //mErrors.copyAllErrors(tLLCustomerMessageUI.mErrors);
      //cError.moduleName = "LAManagecomPol2Report.jsp";
      //cError.functionName = "submitData";
      //cError.errorMessage = "tLLCustomerMessageUI";
      //mErrors.addOneError(cError);
      Content=tLLCustomerMessageUI.mErrors.getFirstError().toString(); 
      	%>
  		<%=Content%>  
  		<%return;
    }else{
    	System.out.println("取数完毕...准备打印报表...");
    	mResult = tLLCustomerMessageUI.getResult();			
  		txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
  		if(txmlExport==null)
  		{
   			Content="没有得到要显示的数据文件";	
   			%>   		
   			<%=Content%>  
   			<%return;
  		}
    }
       	String date=PubFun.getCurrentDate().replaceAll("-","");  
		String time=PubFun.getCurrentTime3().replaceAll(":","");
    
	  	System.out.println("开始打开报表!");
	    Readhtml rh=new Readhtml();
	    rh.XmlParse(txmlExport.getInputStream()); //相当于XmlExport.getInputStream();
	    
		String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
		String temp=realpath.substring(realpath.length()-1,realpath.length());
		if(!temp.equals("/"))
		{
			realpath=realpath+"/"; 
		}
		String templatename=rh.getTempLateName();//模板名字
		String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
		System.out.println("*********************templatepathname= " + templatepathname);
		System.out.println("************************realpath="+realpath);
	  
		outname="LLCustomerMessage"+tG.Operator+date+time+".xls";
		String outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
		
	//*******************************
	//rpt.updateReportMetaData(outname);
	//**********************************
 
	//*********************************
	rh.setReadFileAddress(templatepathname);
	rh.setWriteFileAddress(outpathname);
	rh.start("vtsmuch");
	try{
		outname = java.net.URLEncoder.encode(outname, "UTF-8");
		outname = java.net.URLEncoder.encode(outname, "UTF-8");
	  	//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	  	//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
    }catch(Exception ex)
    {
     	ex.printStackTrace();
    }
    String[] InputEntry = new String[1];
    InputEntry[0] = outpathname;
    tZipFile = realpath+"vtsfile/"+ StrTool.replace(outname,".xls",".zip");
	System.out.println("tZipFile == " + tZipFile);
    rh.CreateZipFile(InputEntry, tZipFile);
	    
		//ExeSQL tExeSQL = new ExeSQL();
		//获取临时文件名
		//String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		//String strFilePath = tExeSQL.getOneValue(strSql);
		//String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
		//获取存放临时文件的路径
		//String strRealPath = application.getRealPath("/").replace('\\','/');
		//String strVFPathName = strRealPath +"//"+ strVFFileName;
	  	//合并VTS文件
		//String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";

		//tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
		//ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		//tcombineVts.output(dataStream);
		//把dataStream存储到磁盘文件
		//System.out.println("存储文件到"+strVFPathName);
		//AccessVtsFile.saveToFile(dataStream,strVFPathName);
		//System.out.println("==> Write VTS file to disk ");
	
		//System.out.println("===strVFFileName : "+strVFFileName);
		//本来打算采用get方式来传递文件路径
		//response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
	}
	catch(Exception ex)
	{
		Content = "失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
    	tError = tLLCustomerMessageUI.mErrors;
    	if (!tError.needDealError())
    	{                          
      		Content = " 处理失败! ";
      		FlagStr = "Succ";
    	}
    	else                                                                           
    	{
    		Content = " 操作失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
    }
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<a href="../f1print/download.jsp?filename=<%=StrTool.replace(outname,".xls",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>
