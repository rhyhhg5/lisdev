

<%
//程序名称：LLInsuredNoInit.jsp
//程序功能：增加证件号码的查询
//创建日期：2016-02-25
//创建人  ：tx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>

<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
String strOperator =globalInput.Operator;  
%>
<script language="JavaScript">
//输入框的初始化 
 function initInpBox()	
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLInsuredNoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
} 



//初始化表单
function initForm()
{ 

  try 
  {
   
    initInpBox();//把文本框设空
    initInsuredGrid(); //初始化列名
    
  }
  catch(re) 
  {
    alert("在LLInsuredNoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
} 

//客户信息Mulline的初始化
 function initInsuredGrid(){
  var iArray = new Array();
  
  try{
	  
    iArray[0]=new Array("序号","30px","10",0);
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="团单号";
    iArray[1][1]="40px";
    iArray[1][2]=100;
    iArray[1][3]=0;
  
	iArray[2]=new Array();
    iArray[2][0]="险种名称";
    iArray[2][1]="50px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="投保人姓名";
    iArray[3][1]="30px";
    iArray[3][2]=100;
    iArray[3][3]=0;
    
    iArray[4]=new Array();
    iArray[4][0]="投保人证件号码";
    iArray[4][1]="60px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="被保人姓名";
    iArray[5][1]="30px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    
    iArray[6]=new Array();
    iArray[6][0]="被保人证件号码";
    iArray[6][1]="40px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    
    iArray[7]=new Array();
    iArray[7][0]="受益人姓名";
    iArray[7][1]="30px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    
    iArray[8]=new Array();
    iArray[8][0]="受益人证件号码";
    iArray[8][1]="40px";
    iArray[8][2]=100;
    iArray[8][3]=0;
    
    iArray[9]=new Array();
    iArray[9][0]="累计已缴保费";
    iArray[9][1]="50px";
    iArray[9][2]=100;
    iArray[9][3]=0;
    
    iArray[10]=new Array();
    iArray[10][0]="保险金额";
    iArray[10][1]="30px";
    iArray[10][2]=100;
    iArray[10][3]=0;
    
    iArray[11]=new Array();
    iArray[11][0]="投保日期";
    iArray[11][1]="40px";
    iArray[11][2]=100;
    iArray[11][3]=0;
    
    iArray[12]=new Array();
    iArray[12][0]="保险起期";
    iArray[12][1]="40px";
    iArray[12][2]=100;
    iArray[12][3]=0;
    
    iArray[13]=new Array();
    iArray[13][0]="保险止期";
    iArray[13][1]="40px";
    iArray[13][2]=100;
    iArray[13][3]=0;
    
    iArray[14]=new Array();
    iArray[14][0]="退保金额";
    iArray[14][1]="30px";
    iArray[14][2]=100;
    iArray[14][3]=0;
    
    iArray[15]=new Array();
    iArray[15][0]="给付金额";
    iArray[15][1]="40px";
    iArray[15][2]=100;
    iArray[15][3]=0;
    
    iArray[16]=new Array();
    iArray[16][0]="赔付金额";
    iArray[16][1]="40px";
    iArray[16][2]=100;
    iArray[16][3]=0;
    
    iArray[17]=new Array();
    iArray[17][0]="保单是否有效";
    iArray[17][1]="20px";
    iArray[17][2]=100;
    iArray[17][3]=0;
    
    iArray[18]=new Array();
    iArray[18][0]="退保日期";
    iArray[18][1]="40px";
    iArray[18][2]=100;
    iArray[18][3]=0;
    

    InsuredGrid = new MulLineEnter("fm","InsuredGrid");
    InsuredGrid.mulLineCount =0;
    InsuredGrid.displayTitle = 0;
    InsuredGrid.locked = 1;
    InsuredGrid.canChk =1;
    InsuredGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    InsuredGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CaseGrid. selBoxEventFuncName = "onSelSelected";
    InsuredGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  } 
}

</script>







