<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initCheckGrid();
    initClaimPolGrid();
    initClaimDetailGrid();
    easyQuery();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=1;
      
    iArray[1]=new Array("团体批次号","110px","0","0");
    iArray[2]=new Array("理赔号","110px","0","0");
    iArray[3]=new Array("姓名","60px","0","0");
    iArray[4]=new Array("结案日期","0px","0","3");
    iArray[5]=new Array("客户号","0px","0","3");
    iArray[6]=new Array("受理日期","70px","0","0");
    iArray[7]=new Array("处理人","0px","0","3");;
    iArray[8]=new Array("案件状态","60px","0","0");
    iArray[9]=new Array("给付类型","60px","0","0");
    iArray[10]=new Array("IDNo","0px","0","3");
    iArray[12]=new Array("操作员姓名","60px","20","0");
    iArray[13]=new Array("操作员代码","60px","20","0");
    iArray[11]=new Array("管理机构","60px","20","0");


    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =5;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canSel =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid. selBoxEventFuncName = "DealClaim";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

// 被保人信息列表的初始化
function initClaimPolGrid()
  {
    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	           //列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保单号";         			//列名
      iArray[3][1]="120px";         			//列宽
      iArray[3][2]=10;          			    //列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
            
      iArray[2]= new Array("险种名称","150px","100","0" ); 
      iArray[1]= new Array("给付责任类型","80px","100","0" );                           
      iArray[4]= new Array("原理算赔付金额","0px","100","3");
      iArray[5]= new Array("核算赔付金额","80px","100","0");
      iArray[6]= new Array("实赔额","80px","100","0");
      iArray[7]= new Array("险种保单号","0px","100","3");
      iArray[8]= new Array("赔案号","0px","100","3");
      iArray[9]= new Array("赔付结论","0px","100","3");
      iArray[10]= new Array("赔付结论代码","0px","100","3");
      iArray[11]= new Array("险种代码","0px","100","3");
      iArray[12]= new Array("受理事故号","0px","100","3");  

      
     

      ClaimPolGrid = new MulLineEnter( "fm" , "ClaimPolGrid" );
      //这些属性必须在loadMulLine前
      ClaimPolGrid.mulLineCount = 0;
      ClaimPolGrid.displayTitle = 1;
      ClaimPolGrid.canSel=1;
      ClaimPolGrid.canChk=0;
      ClaimPolGrid.hiddenPlus=1;   
      ClaimPolGrid.hiddenSubtraction=1;
			ClaimPolGrid.selBoxEventFuncName = "CalRatio";
      ClaimPolGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initClaimDetailGrid()
  {
    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="80px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
     
      

      iArray[2]=new Array();
      iArray[2][0]="险种代码";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="0px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
       iArray[2][4]="llclaimrisk"
      iArray[2][15]="保单号码"
      iArray[2][17]="3"
  
      iArray[3]= new Array("给付责任","120px","100","3");
      
      iArray[4]= new Array("账单金额","50px","100","1");
      iArray[5]= new Array("拒付金额","50px","100","1");
      iArray[6]= new Array("先期给付","50px","100","1");
      iArray[7]= new Array("通融/协议给付比例","130px","100","1");
      iArray[8]= new Array("免赔额","50px","100","3");
      iArray[9]= new Array("溢额","0px","100","3");
      
      iArray[10]= new Array("赔付结论","60px","100","0","llclaimdecision","10|20","1|0");       
      
      iArray[11]= new Array("赔付结论依据","0px","100","3","llclaimdecision_1", "11|21","1|0");
      iArray[11][15]="赔付结论代码"
      iArray[11][17]="20"  
          
      iArray[12]= new Array("理算金额","60px","100","0");
      iArray[13]= new Array("核算赔付金额","80px","100","3");
      iArray[14]= new Array("实赔金额","60px","100","0");  
                     
      iArray[15]= new Array("险种保单号","80px","100","3");
      iArray[16]= new Array("赔案号","80px","100","3");
      iArray[17]= new Array("给付责任代码","80px","100","3");
      iArray[18]= new Array("给付责任类型","80px","100","3");
      iArray[19]= new Array("责任代码","80px","100","3");
      iArray[20]= new Array("赔付结论代码","80px","100","3");
      iArray[21]= new Array("赔付结论依据代码","80px","100","3");
      iArray[22]= new Array("受理事故号","80px","100","3");
  
      
      
      ClaimDetailGrid = new MulLineEnter( "fm" , "ClaimDetailGrid" );
      //这些属性必须在loadMulLine前
      ClaimDetailGrid.mulLineCount = 1;
      ClaimDetailGrid.displayTitle = 1;

      ClaimDetailGrid.canSel=0;
      ClaimDetailGrid.canChk=0;
      ClaimDetailGrid.hiddenPlus=1;   
      ClaimDetailGrid.hiddenSubtraction=1;
  
      ClaimDetailGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面



      }
      catch(ex)
      {
        alert(ex);
      }
}
 </script>