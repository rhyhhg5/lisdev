<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LOMixCbsUserRela.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LOMixCbsUserRelaInit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LOMixCbsUserRelaSave.jsp" method=post name=fm target="fraSubmit">
    <%//@include file="../common/jsp/OperateButton.jsp"%>
    <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="return submitForm();">
			</td>
			
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="deleteButton" VALUE="删  除"  TYPE=button onclick="return deleteClick();">
			</td>
		</tr>
	</table>
</span>
    <%//@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLADimission1);">
    
    <td class=titleImg>
      集团出单人员信息
    </td> 
    </td>
    </tr>
    </table>
    <Div  id= "divLADimission1" style= "display: ''"> 
    	<table  class= common>
    		<TR  class= common> 
        		<TD  class= title>
         		 代理方出单人员工号 
        		</TD>
        		<TD  class= input> 
          			<Input name=operatorcode class=common verify="代理方出单人员工号|NotNull&len<=10" elementtype=nacessary > 
        		</TD>
        		<td class="title8">子公司代码</td>
        		<td class="input8">
            		<input class="codeNo" name="comp_cod" value="000085" readonly="readonly" /><input class="codename" name="comp_nam" value="中国人民健康保险股份有限公司" readonly="readonly" elementtype="nacessary" />
        		</td>
        		<td class="title8">管理机构编号</td>
        		<td class="input8">
            		<input class="codeNo" name="man_org_cod" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,man_org_nam],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,man_org_nam],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="man_org_nam" readonly="readonly" elementtype="nacessary" />
        		</td>
      		</TR> 
			<TR  class= common> 
        		<TD  class= title>
          		姓名
        		</TD>
        		<TD  class= input>
          			<Input name=sales_nam class= common verify="姓名|len<=20" >
        		</TD>
    			<TD  class= title>
          		出生日期 
        		</TD>
        		<TD  class= input>
          			<Input name=date_birthd class='coolDatePicker' dateFormat='short'  verify="出生日期|Date" > 
        		</TD>
        	</TR>
      		<!--  <TR  class= common> 
      			<TD  class= title>
          		性别代码
        		</TD>
        		<TD  class= input>
          			<Input NAME=Sex CLASS=codeNo CodeData="0|^0|未知的性别^1|男性^2|女性^9|未说明的性别"   verify="性别代码|code:Sex" ondblclick="return showCodeListEx('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Sex',[this,SexName],[0,1],null,null,null,1);" ><input class=codename name=SexName readonly=true>
    			</TD> 
       			<TD  class= title>
      			证件类型代码
    			</TD>
    			<TD  class= input>
      				<Input class= 'common' name=idtyp_cod >
   	 			</TD>
   	 			<TD  class= title>
      			证件号码
    			</TD>
    			<TD  class= input>
      				<Input class= 'common' name=paperworkid >
    			</TD>  
 			</TR>  -->
			 <TR  class= common>
				<TD  class= title>
          		联系电话 
        		</TD>
        		<TD  class= input> 
          			<Input name=telephone class= common > 
        		</TD>
        		<TD  class= title>
          		联系手机 
        		</TD>
        		<TD  class= input> 
          			<Input name=sales_mob class= common > 
        		</TD>
         		<TD  class= title>
          		电子邮件 
        		</TD>
        		<TD  class= input> 
          			<Input name=sales_mail class= common > 
        		</TD> 
      		</TR> 
      		<TR  class= common>
				<TD  class= title>
          		在职状态代码 
        		</TD>
        		<TD  class= input> 
          			<Input name=status_cod class= common > 
        		</TD>
        		<TD  class= title>
          		在职状态名称 
        		</TD>
        		<TD  class= input> 
          			<Input name=status class= common > 
        		</TD>
         		<TD  class= title>
          		职能 
        		</TD>
        		<TD  class= input> 
          			<Input name=function class= common > 
        		</TD> 
      		</TR> 
      		<TR  class= common> 
         		<TD  class= title>
          		入职时间  
        		</TD>
        		<TD  class= input> 
          			<Input name=entrant_date class='coolDatePicker' dateFormat='short' verify="入职时间|Date" >
        		</TD>
         		<TD  class= title>
          		离司时间  
        		</TD>
        		<TD  class= input> 
          			<Input name=dimission_date class='coolDatePicker' dateFormat='short' >
        		</TD>
			</TR>            
      		<TR  class= common> 
				<TD  class= title>
          		财险委托方机构编码
        		</TD>
        		<TD  class= input> 
          			<Input name=prt_entrust_orgid class= common>
        		</TD>
        		<TD  class= title>
          		财险委托方机构名称
        		</TD>
        		<TD  class= input> 
          			<Input name=prt_entrust_orgname class= common>
        		</TD>
        		<TD  class= title>
          		健康险委托方机构编码
        		</TD>
        		<TD  class= input> 
          			<Input name=li_entrust_orgid class= common>
        		</TD>
      		</TR>
      		<TR  class= common> 
      			<TD  class= title>
          		健康险委托方机构名称
        		</TD>
        		<TD  class= input> 
          			<Input name=li_entrust_orgname class= common>
        		</TD>
				<TD  class= title>
          		寿险委托方机构编码
        		</TD>
        		<TD  class= input> 
          			<Input name=hi_entrust_orgid class= common>
        		</TD>
        		<TD  class= title>
          		寿险委托方机构名称
        		</TD>
        		<TD  class= input> 
          			<Input name=hi_entrust_orgname class= common>
        		</TD>
        	</TR>
        	<TR  class= common> 
        	    <TD  class= title>
          		财险审批时间
        		</TD>
        		<TD  class= input> 
          			<Input name=prt_check_date class='coolDatePicker' dateFormat='short' verify="财险审批时间|Date" >
        		</TD>
				<TD  class= title>
          		健康险审批时间
        		</TD>
        		<TD  class= input> 
          			<Input name=li_check_date class='coolDatePicker' dateFormat='short' verify="健康险审批时间|Date" >
        		</TD>
        		<TD  class= title>
          		寿险审批时间
        		</TD>
        		<TD  class= input> 
          			<Input name=hi_check_date class='coolDatePicker' dateFormat='short' verify="寿险审批时间|Date" >
        		</TD>
        	</TR>
      		<TR  class= common> 
        
      		</TR>      
    	</table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=DepartTimes value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
