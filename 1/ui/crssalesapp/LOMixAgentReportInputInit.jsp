<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initAgentGrid();
	divPage.style.display="";                 
    fm.all('ManageCom').value = ManageCom;    
}
var AgentGrid;
function initAgentGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//列名
    iArray[0][1]="30px";         	//列名    
    iArray[0][3]=0;         		//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         	 //列名
    iArray[1][1]="50px";            //列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="销售人员编码";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="销售人员名称";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="反馈信息";      //列名
    iArray[4][1]="120px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    iArray[5]=new Array();
    iArray[5][0]="生成时间";      //列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    AgentGrid = new MulLineEnter("fm", "AgentGrid"); 
    //设置Grid属性
    AgentGrid.mulLineCount = 0;
    AgentGrid.displayTitle = 1;
    AgentGrid.locked = 1;
    AgentGrid.canSel = 0;
    AgentGrid.canChk = 0;
    AgentGrid.hiddenSubtraction = 1;
    AgentGrid.hiddenPlus = 1;
 	AgentGrid.selBoxEventFuncName = "";
    AgentGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
                                      

</script>
