<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：RepeatCalSave.jsp
//程序功能：
//创建日期：2003-07-22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="RepeatCalInput.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>  

<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  String WageYear  = request.getParameter("WageYear");
  String WageMonth = request.getParameter("WageMonth");
  String tIndexCalNo = WageYear+WageMonth;
  String tBranchType = request.getParameter("BranchType");
  LAWageSet tLAWageSet = new LAWageSet(); 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
	  int lineCount = 0;
	  String tChk[] = request.getParameterValues("InpAgentQueryGridChk");
	  int number = 0; 
	  for(int index=0;index<tChk.length;index++)
          {
            if(tChk[index].equals("1"))           
              number++;
//            if(tChk[index].equals("0"))      
//              System.out.println("该行未被选中");
          }               
	if(number==0)
	{
       		Content = " 失败，原因:没有选择要调整的员工！";
       		FlagStr = "Fail";		
	}
	else
	{
	  
	  
	  
	  
	  String tAgentCode[] = request.getParameterValues("AgentQueryGrid1");
	  String tF23[] = request.getParameterValues("AgentQueryGrid5");
	  String tF24[] = request.getParameterValues("AgentQueryGrid6");
	  String tLastMoney[] = request.getParameterValues("AgentQueryGrid7");
	  
	  lineCount = tChk.length; //行数
	  System.out.println("length= "+String.valueOf(lineCount));
	  LAWageSchema tLAWageSchema;
	  for(int i=0;i<lineCount;i++)
	  {
	    if(tChk[i].trim().equals("1"))
	    {
	      tLAWageSchema = new LAWageSchema();
	      tLAWageSchema.setIndexCalNo(tIndexCalNo);
	      tLAWageSchema.setAgentCode(tAgentCode[i]);//代理人代码
	      tLAWageSchema.setF23(tF23[i]);
	      tLAWageSchema.setF24(tF24[i]);
	      tLAWageSchema.setLastMoney(tLastMoney[i]);
	      tLAWageSet.add(tLAWageSchema);
	      System.out.println("Agentcode:"+tAgentCode[i]);
	    }
	    System.out.println("i:"+tChk[i]);
	  }

   VData tVData = new VData();
   tVData.addElement(tGI);
   tVData.addElement(WageYear);
   tVData.addElement(WageMonth);
   tVData.add(request.getParameter("ManageCom"));
   tVData.addElement(tBranchType);
    System.out.println("BranchType:"+tBranchType);
   tVData.addElement(tLAWageSet);

    	CalRepeatWageBL mCalRepeatWageBL = new CalRepeatWageBL();
    	mCalRepeatWageBL.submitData(tVData);
    	


   //如果在Catch中发现异常，则不从错误类中提取错误信息
   try
   {
     tError = mCalRepeatWageBL.mErrors;
     if (!tError.needDealError())
     {                          
       Content = " 保存成功! ";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content = " 失败，原因:" + tError.getFirstError();
       FlagStr = "Fail";
     }
   } 
   catch(Exception ex)
   {
     Content = " 失败，原因:" + ex.toString();
     FlagStr = "Fail";    
   }    
   }
   System.out.println(Content);                  
}//页面有效区

%>                      
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

