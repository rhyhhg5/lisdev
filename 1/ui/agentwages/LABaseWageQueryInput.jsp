<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LABaseWageQueryInput.jsp
//程序功能：
//创建日期：2003-6-28
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LABaseWageQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LABaseWageQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>基薪信息</title>
</head>
<body  onload="initForm();" >
  <form action="./LABaseWageQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABaseWageGrid1);">
      </td>
      <td class= titleImg>
        请您输入查询条件： 
      </td>
    	</tr>   
    </table>
  <Div  id= "divLABaseWageGrid1" style= "display: ''">    
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      员工编码
    </TD>
    <TD  class= input>
      <!--Input class= common name=AgentCode -->
      <Input class="codeno" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName], [0,1],null,acodeSql,'1',null,500);" onkeyup="return showCodeListKey('AgentCode', [this,AgentCodeName], [0,1],null,acodeSql,'1',null,500);"><input class=codename name=AgentCodeName readonly=true >
    </TD>
    <TD  class= title>
      展业类型
    </TD>
    <TD  class= input>
      <Input class="codeno" name=BranchType verify="展业机构类型|code:BranchType" ondblclick="showCodeList('branchtype',[this,BranchTypeName],null,null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],null,null,mcodeSql,'1',null);" ><input class=codename name=BranchTypeName readonly=true >
    </TD>
  </TR>
</table>
 </Div>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABaseWageGrid);">
    		</td>
    		<td class= titleImg>
    			 基薪信息结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLABaseWageGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLABaseWageGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var acodeSql = "1 and (BranchType=#2# or BranchType=#3#) and (AgentState = #01# or AgentState = #02#)";
  var mcodeSql = "1 and (code = #2# or code = #3#)";
</script>
