<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>

<%
  //输出参数
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String branchtype = request.getParameter("BranchType");
  String branchtype2 = request.getParameter("BranchType2");
  String FlagStr = "Fail";
  String Content = "";
  ExeSQL tExeSQL = new ExeSQL();
  String arrCount[] = request.getParameterValues("InpSetGridChk");
  String tmanagecom[] = request.getParameterValues("SetGrid1");
  String tGroupAgentCode[] = request.getParameterValues("SetGrid4");
  String tPropertyFyc[] = request.getParameterValues("SetGrid6");
  String tLifeFyc[] = request.getParameterValues("SetGrid7");
  String tWageNo[] = request.getParameterValues("SetGrid8");
 
  LABAgentSelCommissionUI tLABAgentSelCommissionUI = new LABAgentSelCommissionUI();
  LACrossWageSchema tLACrossWageSchema;
  LACrossWageSet tLACrossWageSet = new LACrossWageSet();
  int lineCount = arrCount.length;
  for(int i=0;i<lineCount;i++)
  {
  	if(arrCount[i].equals("1"))
    {
  		tLACrossWageSchema = new LACrossWageSchema();
  		tLACrossWageSchema.setManageCom(tmanagecom[i]);
  		tLACrossWageSchema.setBranchType(branchtype);
  		tLACrossWageSchema.setBranchType2(branchtype2);
	    String sql ="select agentcode from laagent where  groupagentcode ='"+tGroupAgentCode[i]+"'";
	    tLACrossWageSchema.setAgentCode(tExeSQL.getOneValue(sql));
	    tLACrossWageSchema.setWageNo(tWageNo[i]);
//	    tLAActiveChargeSchema.setPropertyPrem(tPreoperyPrem[i]);
//	    tLAActiveChargeSchema.setLifePrem(tLifePrem[i]);
	    tLACrossWageSchema.setPropertyFYC(tPropertyFyc[i]);
	    tLACrossWageSchema.setLifeFyc(tLifeFyc[i]);
//	    tLAActiveChargeSchema.setSumCharge(tSumCharge[i]);//1  是考核标准 
//	    System.out.println(tSumCharge[i]+"---"+tmanagecom[i]);
	    tLACrossWageSet.add(tLACrossWageSchema);
    }
  }

System.out.println("tOperate11:"+tOperate);
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tLACrossWageSet);

System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");	
    tLABAgentSelCommissionUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABAgentSelCommissionUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  
   
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
