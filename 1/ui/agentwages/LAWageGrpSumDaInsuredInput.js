   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
//提数操作

function checkAgentCode()
{
   var sql=" select agentcode  from laagent   where  1=1 "
            + getWherePart("groupagentcode","GroupAgentCode")
            + getWherePart("ManageCom","ManageCom",'like')	;
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
   if (!strQueryResult) 
    {
     alert("此管理机构没有此销售员！");  
     fm.GroupAgentCode.value="";
     return;
    }
     var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);

  fm.all('AgentCode').value  =tArr[0][0];
  
  return;
}
 
 
 
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  
  fm.submit(); //提交
  

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

            

/*****************************************************
*  注: 薪资确认后查询
*  
******************************************************/
function AgentWageGatherQuery()
{

 if(fm.all('StartDate').value==''||fm.all('EndDate').value=='')
  {
    alert("请填写薪资起始截止日期");
    return ;	
  }
  if(fm.all('ManageCom').value==''){
  	
   alert("请填写管理机构");
    return ;		
  }
 divAgentQuery.style.display='';
 initAgentQueryGrid();
 showRecord();
}

/*******************************************************
  * 注:查询调用函数
  *
  *******************************************************/
function showRecord()
{ 
  var StartDate = fm.all('StartDate').value;
  var EndDate = fm.all('EndDate').value;	    
	//查询SQL，返回结果字符串
  var tbranchtype = fm.BranchType.value;        
  var tbranchtype2 = fm.BranchType2.value;      
  var strSql2="";

  if(fm.Flag.value=='0'){
  
  	strSql2 = "select substr(a.ManageCom,1,4),(select name  from ldcom where comcode=substr(a.ManageCom,1,4) ) ,'','','',"
	             +"sum(a.f22),sum(w02),sum(a.k20),sum(a.f12),sum(a.W10),sum(a.f07),sum(a.f09),sum(a.f23),sum(a.f20),sum(f13),sum(a.f24),sum(a.w08),"
	             +"sum(a.F26),sum(a.K12),sum(a.ShouldMoney),sum(a.LastMoney),sum(a.K01),sum(a.K02),sum(a.K03),sum(a.K04),sum(a.CurrMoney),sum(a.SumMoney) "
	             +" from LAWage a,labranchgroup b,laagent c " 
                 +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
                 +" and IndexCalNo >= '"+StartDate+"' and IndexCalNo <= '"+EndDate+"'  and (b.state<>'1' or b.state is null) "
                 +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
                 +" and a.branchtype = '" + trim(tbranchtype)
			     +" 'and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"	    
                 +"  group by substr(a.ManageCom,1,4) ";
   
  }else if (fm.Flag.value=='1'){

     strSql2 = "select substr(a.ManageCom,1,4),(select name  from ldcom where comcode=substr(a.ManageCom,1,4) ) ,b.branchattr,'','',"
	             +"sum(a.f22),sum(w02),sum(a.k20),sum(a.f12),sum(a.W10),sum(a.f07),sum(a.f09),sum(a.f23),sum(a.f20),sum(f13),sum(a.f24),sum(a.w08),"
	             +"sum(a.F26),sum(a.K12),sum(a.ShouldMoney),sum(a.LastMoney),sum(a.K01),sum(a.K02),sum(a.K03),sum(a.K04),sum(a.CurrMoney),sum(a.SumMoney) "
	             +" from LAWage a,labranchgroup b,laagent c " 
                 +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
                 +" and IndexCalNo >= '"+StartDate+"' and IndexCalNo <= '"+EndDate+"'  and (b.state<>'1' or b.state is null) "
                 +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
                 +" and a.branchtype = '" + trim(tbranchtype)
			     + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
			     +getWherePart('b.branchattr','AgentGroup')		    
                 +" group by substr(a.ManageCom,1,4) ,b.branchattr";   
                 
  }else if (fm.Flag.value=='2'){
  	
  		  strSql2 = "select substr(a.ManageCom,1,4),(select name  from ldcom where comcode=substr(a.ManageCom,1,4) ) ,b.branchattr,getUniteCode(a.agentcode),c.name,"
	             +"sum(a.f22),sum(w02),sum(a.k20),sum(a.f12),sum(a.W10),sum(a.f07),sum(a.f09),sum(a.f23),sum(a.f20),sum(f13),sum(a.f24),sum(a.w08),"
	             +"sum(a.F26),sum(a.K12),sum(a.ShouldMoney),sum(a.LastMoney),sum(a.K01),sum(a.K02),sum(a.K03),sum(a.K04),sum(a.CurrMoney),sum(a.SumMoney) "
	             +" from LAWage a,labranchgroup b,laagent c " 
                 +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode   "
                 +" and IndexCalNo >= '"+StartDate+"' and IndexCalNo <= '"+EndDate+"'  and (b.state<>'1' or b.state is null) "
                 +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
                 +" and a.branchtype = '" + trim(tbranchtype)
			     + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
			     +getWherePart('a.agentcode','AgentCode')		    
                 +" group by substr(a.ManageCom,1,4) ,b.branchattr ,a.agentcode, c.name";  
                 }            
        
	//查询SQL，返回结果字符串
	
  turnPage.queryModal(strSql2, AgentQueryGrid);  
}


function AgentWageDownLoad()
{
  var StartDate = fm.all('StartDate').value;
  var EndDate = fm.all('EndDate').value;	    
  var tbranchtype = fm.BranchType.value;        
  var tbranchtype2 = fm.BranchType2.value;  
  var strSql="";

 if(fm.Flag.value=='0'){
    
  	strSql = "select substr(a.ManageCom,1,4),(select name  from ldcom where comcode=substr(a.ManageCom,1,4) ) ,'','','',"
	             +"sum(a.f22),sum(w02),sum(a.k20),sum(a.F12),sum(a.f07),sum(a.f09),sum(a.f23),sum(a.f20),sum(f13),sum(a.f24),sum(a.w08),"
	             +"sum(a.F26),sum(a.K12),sum(a.LastMoney),sum(a.K01),sum(a.K02),sum(a.K03),sum(a.K04),sum(a.CurrMoney),sum(a.SumMoney) "
	             +" from LAWage a,labranchgroup b,laagent c " 
                 +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
                 +" and IndexCalNo >= '"+StartDate+"' and IndexCalNo <= '"+EndDate+"'  and (b.state<>'1' or b.state is null) "
                 +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
                 +" and a.branchtype = '" + trim(tbranchtype)
			     +" 'and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"	    
                 +"  group by substr(a.ManageCom,1,4) ";
         
  }else if (fm.Flag.value=='1'){
  	
     strSql = "select substr(a.ManageCom,1,4),(select name  from ldcom where comcode=substr(a.ManageCom,1,4) ) ,b.branchattr,'','',"
	             +"sum(a.f22),sum(w02),sum(a.k20),sum(a.F12),sum(a.f07),sum(a.f09),sum(a.f23),sum(a.f20),sum(f13),sum(a.f24),sum(a.w08),"
	             +"sum(a.F26),sum(a.K12),sum(a.LastMoney),sum(a.K01),sum(a.K02),sum(a.K03),sum(a.K04),sum(a.CurrMoney),sum(a.SumMoney) "
	             +" from LAWage a,labranchgroup b,laagent c " 
                 +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode "
                 +" and IndexCalNo >= '"+StartDate+"' and IndexCalNo <= '"+EndDate+"'  and (b.state<>'1' or b.state is null) "
                 +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
                 +" and a.branchtype = '" + trim(tbranchtype)
			     + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
			     +getWherePart('b.branchattr','AgentGroup')		    
                 +" group by substr(a.ManageCom,1,4) ,b.branchattr"; 
                 
  }else if (fm.Flag.value=='2'){
  		  strSql = "select substr(a.ManageCom,1,4),(select name  from ldcom where comcode=substr(a.ManageCom,1,4) ) ,b.branchattr,getUniteCode(a.agentcode),c.name,"
	             +"sum(a.f22),sum(w02),sum(a.k20),sum(a.F12),sum(a.f07),sum(a.f09),sum(a.f23),sum(a.f20),sum(f13),sum(a.f24),sum(a.w08),"
	             +"sum(a.F26),sum(a.K12),sum(a.LastMoney),sum(a.K01),sum(a.K02),sum(a.K03),sum(a.K04),sum(a.CurrMoney),sum(a.SumMoney) "
	             +" from LAWage a,labranchgroup b,laagent c " 
                 +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
                 +" and IndexCalNo >= '"+StartDate+"' and IndexCalNo <= '"+EndDate+"'  and (b.state<>'1' or b.state is null) "
                 +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
                 +" and a.branchtype = '" + trim(tbranchtype)
			     + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
			     +getWherePart('a.agentcode','AgentCode')		    
                 +" group by substr(a.ManageCom,1,4) ,b.branchattr ,a.agentcode, c.name";  
                 }            
    fm.querySql.value = strSql;
  
//定义列名
var strSQLTitle = "select '分公司机构编码','分公司机构名称','销售团队编码','业务员代码','业务员姓名','基本工资','基本工资补发','月度提奖','季度达成奖','年终奖','育成奖','岗位工资','绩效工资','半年绩效补发','月度管理津贴','年效益奖','半年岗位工资补发','扣款','上次薪资余额','提奖加款','其他加款','提奖扣款','其他扣款资','本期应发薪资','本期实发薪资'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '薪资汇总下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();
}	

