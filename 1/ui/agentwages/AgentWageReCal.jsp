<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：AgentWageReCal.jsp
//程序功能：
//创建日期：2004-3-02
//创建人  ：zy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="AgentWageReCal.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentWageReCalInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();" >    
  <form method=post name=fm target="fraSubmit" action="./AgentWageReCalSave.jsp">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		薪资重算
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            员工编码
          </TD>
          <TD  class= input>
          	<Input class=common name=AgentCode >
            <!--Input class=common name=AgentCode verify="代理人编码|code:AgentCode"-->
          </TD>          
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);" >
          </TD>                   
        </TR>
        <TR  class= common>
          <TD  class= title>
            佣金所属年
          </TD>
          <TD  class= input>
            <Input class=common  name=WageYear verify="佣金所属年|NOTNULL" >
          </TD>
          <TD  class= title>
            佣金所属月
          </TD>
          <TD  class= input>
            <Input class=common name=WageMonth verify="佣金所属月|NOTNULL" >
          </TD>
        </TR>
        <TR class=input>              
         <TD class=common>
          <input type =button class=common value="查询" onclick="RepeatCalQuery();">    
        </TD>
        <TD class=common>
          <input type =button class=common value="重算" onclick="RepeatCalSave();">            
        </TD>
       </TR>          
      </table>
      <input type=hidden name=BranchType value=""> 
      <input type=hidden name=Flag value=0> 
     <div id="AgentQueryGrid" style="display:''">
   <table class=common>
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanAgentQueryGrid"></span>
     </td>
    </tr>
   </table>      
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">     
  </div>   

     </Table>
   </Div>	     
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


