<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>  
<%
//程序名称：AgentMonthCal.jsp
//程序功能：
//创建日期：2003-07-09
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="AgentMonthCal.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentMonthInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();" >    
  <form action="./AgentMonthCalSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		员工月基础信息计算
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code'  name=ManageCom verify="管理机构|NOTNULL" onDblClick="return showCodeList('ComCode',[this]);" >
          </TD>        
          <TD  class= title>
            计算年月
          </TD>
          <TD  class= input>
            <Input class=common  name=YearMonth verify="计算年月|NOTNULL" > 
          </TD>          
        </TR>
        <TR class=input>     
         <TD class=common>
          <input type =button class=common value="计算" onclick="Calculate();">    
        </TD>
       </TR>          
      </table>
    </Div>  
    <input type=hidden name=BranchType value=''> 
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>