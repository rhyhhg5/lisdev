<%
//程序名称：LAWageRadix2Init.jsp
//程序功能：
//创建日期：2008-07-29 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>     
<script language="JavaScript">
var StrSql=" 1 and branchtype=#"+<%=BranchType%>+"# and branchtype2=#"+<%=BranchType2%>+"#  ";
</script>
                                      
<script language="JavaScript">

function initInpBox()
{ 
  try
  {         
      fm.all('ManageCom').value = <%=tG.ManageCom%>;
	  var sql="select name from ldcom where comcode ='"+<%=tG.ManageCom%>+"'";
	  var array=easyExecSql(sql);
	  fm.all('ManageComName').value =array[0][0];
	  var sqlArea="select code2 from LDCodeRela where relatype = 'comtoareatype1' and code1=substr('"+<%=tG.ManageCom%>+"',1,4) with ur";
	  var arrayArea=easyExecSql(sqlArea);
	  fm.all('ComStyle').value =arrayArea[0][0];
      fm.all('ComStyleName').value = arrayArea[0][0]+"类地区";
      fm.all('RadixType').value = "";
      fm.all('RadixTypeName').value = "";
      fm.all('BranchType').value = <%=BranchType%>;
      fm.all('BranchType2').value = <%=BranchType2%>;

  }
  catch(ex)
  {
    alert("LAWageRadix2Init.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {

    initInpBox();
    initArchieveWP1017Grid();
  }
  catch(re)
  {
    alert("LAWageRadix2Init.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//岗位津贴
var ArchieveWP1017Grid;
function initArchieveWP1017Grid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();                                                 
    iArray[1][0]="职级";         		//列名                             
    iArray[1][1]="40px";            		//列宽                             
    iArray[1][2]=200;            			//列最大值                           
    iArray[1][3]=2;                                                        
    iArray[1][4]="agentgrade";                                           
    iArray[1][5]="1|2";              	                                     
    iArray[1][6]="0|1";                                                    
    iArray[1][9]="职级|code:agentgrade";                             
    iArray[1][15]= "1";                                                    
    iArray[1][16]= StrSql;                                                 
                                                                           
    iArray[2]=new Array();                                                 
    iArray[2][0]="职级名称";         		//列名                         
    iArray[2][1]="100px";            		//列宽                             
    iArray[2][2]=200;            			//列最大值                           
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="FYC上限";          		        //列名
    iArray[3][1]="80px";      	      		//列宽         			//列最大值
    iArray[3][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[3][3]=1;   
    iArray[3][9]="FYC上限|NotNull&&NUM";  
    
    iArray[4]=new Array();
    iArray[4][0]="FYC下限";          		        //列名
    iArray[4][1]="80px";      	      		//列宽         			//列最大值
    iArray[4][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[4][3]=1;   
    iArray[4][9]="FYC上限|NotNull&&NUM";  
    
   
   
    iArray[5]=new Array();
    iArray[5][0]="岗位津贴提取比例";          		        //列名
    iArray[5][1]="80px";      	      		//列宽         			//列最大值
    iArray[5][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[5][3]=1;   
    iArray[5][9]="C1|NotNull&&NUM";  
   
    iArray[6]=new Array();
    iArray[6][0]="Idx号";          		        //列名
    iArray[6][1]="0px";      	      		//列宽         			//列最大值
    iArray[6][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[6][3]=0; 
  
    ArchieveWP1017Grid = new MulLineEnter( "fm" , "ArchieveWP1017Grid" ); 
    //这些属性必须在loadMulLine前

    ArchieveWP1017Grid.mulLineCount = 0;  
    ArchieveWP1017Grid.canChk = 1; 
    ArchieveWP1017Grid.displayTitle = 1;
    ArchieveWP1017Grid.hiddenPlus = 0;
    ArchieveWP1017Grid.hiddenSubtraction = 1;    
  //  ArchieveCGrid.canSel=1;
  //  ArchieveCGrid.selBoxEventFuncName = "showOne";
    ArchieveWP1017Grid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ArchieveCGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
