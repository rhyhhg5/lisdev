<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAActiveFycCheckInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI"); 
  String CurrentDate= PubFun.getCurrentDate(); 

%>

<script>
   var manageCom = <%=tG.ManageCom%>;
 
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAActiveFycCheckInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAActiveFycCheckInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAActiveFycCheckSave.jsp" method=post name=fm target="fraSubmit">  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent2);">
    <td class=titleImg>
      查询条件
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent2" style= "display: ''">
    <table  class= common>
        <TR  class= common>
         <TD  class= title>
            管理机构
         </TD>
         <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|NOTNULL"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
         </TD> 
          <TD  class= title>
           薪资年月
          </TD>
          <TD  class= input>
          <Input class=common name=WageNo verify="薪资年月|NOTNULL&len=6"  elementtype=nacessary>
           <font color="red">(yyyymm)
          </TD>    
        </TR>
        <TR  class= common>         
          <TD  class= title>
            业务员代码
          </TD>          
          <TD  class= input>
            <Input class=common name=AgentCode >
          </TD>  
        </TR>
    </table> 
   
          <Input type=button class= cssButton name=queryb value="查询" onclick="queryAgent();">
          <input type=button class=cssButton value='重置' onclick="clearGrid();">
           <input type =button class=cssbutton value="下载" onclick="ListDownLoad();">  
       
          <p> <font color="#ff0000">注：佣金为0，可能原因主要有以下两种情况：</font></p>
          <p> <font color="#ff0000">1、险种的提奖比例没有录入或没有符合条件的提奖比例，导致佣金为0</font></p>
          <p> <font color="#ff0000">2、险种的提奖比例录入时间或最近修改时间在保单收费之后，导致佣金为0</font></p>
          <p> <font color="#ff0000">如果是以上两种情况，请与IT沟通，修改提奖比例并提交申请进行数据维护，保证薪资的计算正确。</font></p>
        </div>
 <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 提奖为0的查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
  	</div>
  	 <tr>
	    	<td>
	    		<p> </p>
	    	<!--input type=button class=cssButton name=saveb value='保存' onclick="return submitSave()">
	    	<!--input type=button class=cssButton value='重置' onclick="clearAll();">
	    </td>
  	 <!--p> <font color="#ff0000">选择的人员保单信息如下，人员调动后下列保单将同时迁往新的机构，如果需要将保单仍归属于原有机构，请对相应的保单先做保单归属变更 </font></p-->
  	 <table>
    	<tr>
        	<td class=common>
		      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			 对应险种信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage1.lastPage();"> 						
  	</div>
	   
	    </tr>
    </table>
   
    <input type=hidden name=hideAgentCode value=''>  
    <input type=hidden name=BranchType2 value=''>
    <Input  type=hidden name=BranchType value='' >
    
    <input type=hidden name=hideBranchLevel value=''>
    <input type=hidden name=hideUpBranch value=''>
    <input type=hidden name=hideAgentGroup value=''>
    <input type=hidden id="fmAction" name="fmAction">  
    <input type=hidden class=Common name=querySql > 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
