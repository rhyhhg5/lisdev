  <%
//程序名称：UpdateWageNoInit.jsp
//程序功能：
//创建日期：2016-11-23
//创建人  ：yy
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                
    fm.all('ManageCom').value = '';
    fm.all('ContNo').value = '';
    fm.all('GroupAgentCode').value = '';
    fm.all('GrpContNo').value = ''; 
  }
  catch(ex)
  {
    alert("在UpdateWageNoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var WageNoGrid ;
 
// 保单信息列表的初始化
function initWageNoGrid()
  {                               
    var iArray = new Array();
    try
	{
       
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            		//列最大值
	      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	 
	      iArray[1]=new Array();
	      iArray[1][0]="commisionsn";         	//列名
	      iArray[1][1]="80px";              	//列宽
	      iArray[1][2]=200;            	        //列最大值
	      iArray[1][3]=1;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[2]=new Array();
	      iArray[2][0]="管理机构";         	//列名
	      iArray[2][1]="80px";              	//列宽
	      iArray[2][2]=120;            	        //列最大值
	      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[3]=new Array();
	      iArray[3][0]="管理机构名称";         	//列名
	      iArray[3][1]="100px";            		//列宽
	      iArray[3][2]=200;            	        //列最大值
	      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[4]=new Array();
	      iArray[4][0]="业务员代码 ";         	//列名
	      iArray[4][1]="80px";            		//列宽
	      iArray[4][2]=200;            	        //列最大值
	      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[5]=new Array();
	      iArray[5][0]="业务员名称 ";         		//列名
	      iArray[5][1]="80px";            		//列宽
	      iArray[5][2]=200;              //列最大值
	      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	              
	      iArray[6]=new Array();
	      iArray[6][0]="团单号";         		//列名
	      iArray[6][1]="80px";            		//列宽
	      iArray[6][2]=200;              //列最大值
	      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[7]=new Array();
	      iArray[7][0]="个单号";         		//列名
	      iArray[7][1]="80px";            		//列宽
	      iArray[7][2]=200;              //列最大值
	      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	            
	      iArray[8]=new Array();
	      iArray[8][0]="险种";         		//列名
	      iArray[8][1]="60px";            		//列宽
	      iArray[8][2]=200;              //列最大值
	      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	               
	      iArray[9]=new Array();
	      iArray[9][0]="保费";         		//列名
	      iArray[9][1]="60px";            		//列宽
	      iArray[9][2]=200;              //列最大值
	      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	             
	      iArray[10]=new Array();
	      iArray[10][0]="提奖比例";         		//列名
	      iArray[10][1]="80px";            		//列宽
	      iArray[10][2]=200;              //列最大值
	      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	               
	      iArray[11]=new Array();
	      iArray[11][0]="提奖";         		//列名
	      iArray[11][1]="60px";            		//列宽
	      iArray[11][2]=200;              //列最大值
	      iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	               
	      iArray[12]=new Array();
	      iArray[12][0]="oldWageNo";         		//列名
	      iArray[12][1]="80px";            		//列宽
	      iArray[12][2]=200;              //列最大值
	      iArray[12][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      	
	      iArray[13]=new Array();
	      iArray[13][0]="newWageNo";         		//列名
	      iArray[13][1]="80px";            		//列宽
	      iArray[13][2]=200;              //列最大值
	      iArray[13][3]=1;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[14]=new Array();
	      iArray[14][0]="caldate";         		//列名
	      iArray[14][1]="80px";            		//列宽
	      iArray[14][2]=200;              //列最大值
	      iArray[14][3]=1;                   	//是否允许输入,1表示允许，0表示不允许
	      
      WageNoGrid = new MulLineEnter( "fm" , "WageNoGrid" ); 
      //这些属性必须在loadMulLine前
      WageNoGrid.mulLineCount = 10;   
      WageNoGrid.displayTitle = 1;
      WageNoGrid.hiddenPlus = 1;
      WageNoGrid.canChk=1;
      WageNoGrid.hiddenSubtraction = 1;
      WageNoGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
 }
function initForm()
{
  try
  {
    initInpBox();
    initWageNoGrid();
  }
  catch(re)
  {
    alert("UpdateWageNoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
