<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AdjustPolSave.jsp
//程序功能：
//创建日期：2003-07-15 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  System.out.println("111");
  
  LACommisionSchema mLACommisionSchema = new LACommisionSchema();
  LACommisionSet mLACommisionSet = new LACommisionSet();
  LACommisionSchema tLACommisionSchema = new LACommisionSchema();
  AdjustBankPolUI tAdjustBankPolUI  = new AdjustBankPolUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";

  GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);
  String tManageCom=request.getParameter("ManageCom");
  String tAgentCom=request.getParameter("AgentCom");
  String tAgentCode=request.getParameter("AgentCode");
  String tCustumNo=request.getParameter("CustumNo");
  String tSignDate=request.getParameter("SignDate");
  
  tLACommisionSchema.setManageCom(tManageCom);  
  tLACommisionSchema.setAgentCom(tAgentCom); 
  tLACommisionSchema.setAgentCode(tAgentCode);
  tLACommisionSchema.setP12(tCustumNo);
  tLACommisionSchema.setSignDate(tSignDate);
  

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin prepare LACommisionSet...");
  
  //取得Muline信息
  int lineCount = 0;
  String aAgentcode[] = request.getParameterValues("AgentQueryGrid1");
  //String tBranchAttr[] = request.getParameterValues("AgentQueryGrid2");
  String tPolno[] = request.getParameterValues("AgentQueryGrid3");
  String tAppnt[] = request.getParameterValues("AgentQueryGrid4");
  String tInsure[] = request.getParameterValues("AgentQueryGrid5");
  String tSigndate[] = request.getParameterValues("AgentQueryGrid6");
  String tGetpoldate[] = request.getParameterValues("AgentQueryGrid7");
  String tTransmoney[] = request.getParameterValues("AgentQueryGrid8");
  String aAgentcom[] = request.getParameterValues("AgentQueryGrid9");
  lineCount = aAgentcode.length; //行数
  System.out.println("length= "+String.valueOf(lineCount));
  for(int i=0;i<lineCount;i++)
  {
     mLACommisionSchema = new LACommisionSchema();
     mLACommisionSchema.setAgentCode(aAgentcode[i]);  
     mLACommisionSchema.setPolNo(tPolno[i]); 
     mLACommisionSchema.setP11(tAppnt[i]);
     mLACommisionSchema.setP13(tInsure[i]);
     mLACommisionSchema.setSignDate(tSigndate[i]);
     
     mLACommisionSchema.setGetPolDate(tGetpoldate[i]);
     mLACommisionSchema.setTransMoney(tTransmoney[i]);
     mLACommisionSchema.setAgentCom(aAgentcom[i]); 
     
     //System.out.println("i= "+String.valueOf(i)+" | "+ tSigndate[i]); 
     mLACommisionSet.add(mLACommisionSchema);    
  }
  System.out.println("end 录入信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(mLACommisionSet);
  tVData.addElement(tLACommisionSchema);
  System.out.println("add over");
  try
  {
    tAdjustBankPolUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tAdjustBankPolUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>