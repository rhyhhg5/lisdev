<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：AgentWageGatherInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="LAWageGrpDaInsuredInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAWageGrpDaInsuredInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>

	<%
	String tTitleAgent="";
	if("02".equals(BranchType2))
	{
	tTitleAgent = "服务专员";
}else if("01".equals(BranchType2))
	{
	tTitleAgent = "业务员";
	}
	%>
<body  onload="initForm();initElementtype();" >    
  <form action="./LAWageGrpDaInsuredSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		管理机构业务员月终薪资计算确认
       		 </td>   		     
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom   verify="管理机构|NOTNULL"
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'4','char(length(trim(comcode)))',1);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,'4','char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageComName readOnly  elementtype=nacessary> 
          </TD> 
           <TD  class= title>
            薪资所属年
          </TD>
          <TD  class= input>
            <Input class=common  name=WageYear verify="薪资所属年|NOTNULL"  elementtype=nacessary ><font color="red">yyyy
          </TD>        
        </TR>
        <TR  class= common>
         
          <TD  class= title>
            薪资所属月
          </TD>
          <TD  class= input>
            <Input class=common name=WageMonth verify="薪资所属月|NOTNULL"  elementtype=nacessary ><font color="red">mm
          </TD>
        </TR>
        
        <TR class=input>     
         <TD class=common>
          <input type =button class=cssbutton value="确认" onclick="AgentWageGatherSave();">    
        </TD>
          
      </table>
    </Div>  
     <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		管理机构业务员月终薪资查询
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom1  
            ondblclick="return showCodeList('ComCode',[this,ManageCom1Name],[0,1],null,'4','char(length(trim(comcode)))',1);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageCom1Name],[0,1],null,'4','char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageCom1Name readOnly  elementtype=nacessary> 
          </TD> 
           <TD  class= title>
            薪资所属年
          </TD>
          <TD  class= input>
            <Input class=common  name=WageYear1 elementtype=nacessary >
          </TD>        
        </TR>
        <TR  class= common>
         
          <TD  class= title>
            薪资所属月
          </TD>
          <TD  class= input>
            <Input class=common name=WageMonth1  elementtype=nacessary >
          </TD>
          <TD  class= title>
            <%=tTitleAgent%>代码
          </TD>
          <TD  class= input>
            <Input class= common name=GroupAgentCode onchange = "return checkAgentCode()">
          </TD>         
        </TR>
         <TR  class= common>
          
          <TD  class= title>
            业务员姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
         <TD class=title>销售人员类型</TD>
		<TD class=input><Input name=AgentType class='codeno'
			ondblclick="return showCodeList('agenttypecode',[this,AgentTypeName],[0,1]);"
			onkeyup="return showCodeListKey('agenttypecode',[this,AgentTypeName],[0,1]);"><Input
			class=codename name=AgentTypeName readOnly elementtype=nacessary>
			</TD>                 
        </TR>
        <TR class=input>     
         <TD class=common>
          <input type =button class=cssbutton value="查询" onclick="AgentWageGatherQuery();">   
          <input type =button class=cssbutton value="下载" onclick="AgentWageDownLoad();">     
        </TD>
       </TR> 
         
      </table>
    </Div>  
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  	</TD>
      </TR>
     </Table>					
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
   </Div>	     
   <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
     <input type=hidden id="fmAction" name="fmAction">  
       <input type=hidden class=Common name=querySql > 
       <input type=hidden name=AgentCode value=''>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>