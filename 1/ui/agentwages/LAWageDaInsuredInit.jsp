  <%
//程序名称：AgentWageGatherInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
        
  	fm.all('ManageCom').value = ''; 
  	            
    fm.all('WageYear').value = '';
    
    fm.all('WageMonth').value = '';
    
    fm.all('BranchType').value = '<%=BranchType%>';     
    fm.all('BranchType2').value = '<%=BranchType2%>';
  }
  catch(ex)
  {
    alert("在AgentWageGatherInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var AgentQueryGrid ;
 
// 保单信息列表的初始化
function initAgentQueryGrid()
  {                               
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
 
      iArray[1]=new Array();
      iArray[1][0]="营销员代码";         	//列名
      iArray[1][1]="80px";              	//列宽
      iArray[1][2]=200;            	        //列最大值
      iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="姓名";         	//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=120;            	        //列最大值
      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="销售单位";         		//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=200;            	        //列最大值
      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="管理机构";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
   
      iArray[5]=new Array();
      iArray[5][0]="综合拓展佣金"
      iArray[5][1]="100px";
      iArray[5][2]=200;
      iArray[5][3]=0;
     
      iArray[6]=new Array();                                                                       
      iArray[6][0]="首年度佣金";         		//列名                                                   
      iArray[6][1]="60px";            		//列宽                                                   
      iArray[6][2]=200;            	        //列最大值                                                 
      iArray[6][3]=0;                                                                              
                                                                                                   
      iArray[7]=new Array();                                                                       
      iArray[7][0]="续期佣金";         		//列名                                                       
      iArray[7][1]="60px";            		//列宽                                                   
      iArray[7][2]=200;            	        //列最大值                                                 
      iArray[7][3]=0;                                                                              
                                                                                                   
      iArray[8]=new Array();                                                                       
      iArray[8][0]="续保佣金";         		//列名                                                       
      iArray[8][1]="80px";            		//列宽                                                   
      iArray[8][2]=200;            	        //列最大值                                                 
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[9]=new Array();                                                                       
      iArray[9][0]="续期服务奖";         		//列名                                                   
      iArray[9][1]="80px";            		//列宽                                                   
      iArray[9][2]=200;            	        //列最大值                                                 
      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
      
      iArray[10]=new Array();                                                    
      iArray[10][0]="孤儿单服务奖金";         		//列名                                
      iArray[10][1]="80px";            		//列宽                                
      iArray[10][2]=200;            	        //列最大值                              
      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许               
      
      iArray[11]=new Array();                                                                     
      iArray[11][0]="绩优达标奖";         		//列名                                                  
      iArray[11][1]="60px";            		//列宽                                                  
      iArray[11][2]=200;            	        //列最大值                                            
      iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                 
                                                                                                  
      iArray[12]=new Array();                                                                     
      iArray[12][0]="健代产佣金";         		//列名                                                  
      iArray[12][1]="80px";            		//列宽                                                  
      iArray[12][2]=200;            	        //列最大值                                            
      iArray[12][3]=0;
      
      iArray[13]=new Array();                                                                     
      iArray[13][0]="健代寿佣金";         		//列名                                                  
      iArray[13][1]="80px";            		//列宽                                                  
      iArray[13][2]=200;            	        //列最大值                                            
      iArray[13][3]=0;
      
      
      iArray[14]=new Array();                                                                     
      iArray[14][0]="岗位津贴";         		//列名                                                  
      iArray[14][1]="60px";            		//列宽                                                  
      iArray[14][2]=200;            	        //列最大值                                            
      iArray[14][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                 
                                                                                                  
      iArray[15]=new Array();                                                                     
      iArray[15][0]="季度奖";         		//列名                                                      
      iArray[15][1]="60px";            		//列宽                                                  
      iArray[15][2]=200;            	        //列最大值                                            
      iArray[15][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                 
                                                                                                  
      iArray[16]=new Array();                                                                     
      iArray[16][0]="增员奖";         		//列名                                                      
      iArray[16][1]="60px";            		//列宽                                                  
      iArray[16][2]=200;            	        //列最大值                                            
      iArray[16][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                 
                                                                                                  
      iArray[17]=new Array();                                                                     
      iArray[17][0]="晋升奖";         		//列名                                                      
      iArray[17][1]="60px";            		//列宽                                                  
      iArray[17][2]=200;            	        //列最大值                                            
      iArray[17][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                 
                       	//是否允许输入,1表示允许，0表示不允许
//      iArray[15]=new Array();
//      iArray[15][0]="职务津贴补发";         		//列名
//      iArray[15][1]="80px";            		//列宽
//      iArray[15][2]=200;            	        //列最大值
//      iArray[15][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
     
      iArray[18]=new Array();                                                                         
      iArray[18][0]="职务津贴";         		//列名                                                      
      iArray[18][1]="80px";            		//列宽                                                      
      iArray[18][2]=200;            	        //列最大值                                                
      iArray[18][3]=0;                                                                                
                   
      iArray[19]=new Array();                                                                      
      iArray[19][0]="管理津贴";         		//列名                                                   
      iArray[19][1]="80px";            		//列宽                                                   
      iArray[19][2]=200;            	        //列最大值                                             
      iArray[19][3]=0;                                                                             
                       	//是否允许输入,1表示允许，0表示不允许                                                      
      iArray[20]=new Array();                                                                      
      iArray[20][0]="直接养成津贴";         		//列名                                                   
      iArray[20][1]="80px";            		//列宽                                                   
      iArray[20][2]=200;            	        //列最大值                                             
      iArray[20][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[21]=new Array();                                                                      
      iArray[21][0]="间接养成津贴";         		//列名                                                   
      iArray[21][1]="80px";            		//列宽                                                   
      iArray[21][2]=200;            	        //列最大值                                             
      iArray[21][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[22]=new Array();                                                                      
      iArray[22][0]="培训津贴（筹）";         		//列名                                                   
      iArray[22][1]="60px";            		//列宽                                                   
      iArray[22][2]=200;            	        //列最大值                                             
      iArray[22][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[23]=new Array();                                                                      
      iArray[23][0]="财务支持费用（筹）";         		//列名                                               
      iArray[23][1]="80px";            		//列宽                                                   
      iArray[23][2]=200;            	        //列最大值                                             
      iArray[23][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[24]=new Array();                                                                      
      iArray[24][0]="加款(含财补加款)";         		//列名                                               
      iArray[24][1]="60px";            		//列宽                                                   
      iArray[24][2]=200;            	        //列最大值                                             
      iArray[24][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[25]=new Array();                                                                      
      iArray[25][0]="财补加款";         		//列名                                                   
      iArray[25][1]="60px";            		//列宽                                                   
      iArray[25][2]=200;            	        //列最大值                                             
      iArray[25][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[26]=new Array();                                                                      
      iArray[26][0]="差勤实扣";         		//列名                                                   
      iArray[26][1]="60px";            		//列宽                                                   
      iArray[26][2]=200;            	        //列最大值                                             
      iArray[26][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                  
      iArray[27]=new Array();                                                                      
      iArray[27][0]="季度团建补发津贴";         		//列名                                               
      iArray[27][1]="80px";            		//列宽                                                   
      iArray[27][2]=200;            	        //列最大值                                             
      iArray[27][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[28]=new Array();                                                                      
      iArray[28][0]="年度团建补发津贴";         		//列名                                               
      iArray[28][1]="80px";            		//列宽                                                   
      iArray[28][2]=200;            	        //列最大值                                             
      iArray[28][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[29]=new Array();                                                                      
      iArray[29][0]="推荐津贴";         		//列名                                                   
      iArray[29][1]="80px";            		//列宽                                                   
      iArray[29][2]=200;            	        //列最大值                                             
      iArray[29][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[30]=new Array();                                                                      
      iArray[30][0]="税前薪资";         		//列名                                                   
      iArray[30][1]="80px";            		//列宽                                                   
      iArray[30][2]=200;            	        //列最大值                                             
      iArray[30][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                  
      iArray[31]=new Array();                                                                      
      iArray[31][0]="养老金计提";         		//列名                                                   
      iArray[31][1]="80px";            		//列宽                                                   
      iArray[31][2]=200;            	        //列最大值                                             
      iArray[31][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[32]=new Array();                                                                      
      iArray[32][0]="个人所得税";         		//列名                                                   
      iArray[32][1]="80px";            		//列宽                                                   
      iArray[32][2]=200;            	        //列最大值                                             
      iArray[32][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[33]=new Array();                                                                      
      iArray[33][0]="个人增值税";         		//列名                                                   
      iArray[33][1]="80px";            		//列宽                                                   
      iArray[33][2]=200;            	        //列最大值                                             
      iArray[33][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[34]=new Array();                                                                      
      iArray[34][0]="个人城建税";         		//列名                                                   
      iArray[34][1]="80px";            		//列宽                                                   
      iArray[34][2]=200;            	        //列最大值                                             
      iArray[34][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[35]=new Array();                                                                      
      iArray[35][0]="教育费附加税";         		//列名                                                   
      iArray[35][1]="80px";            		//列宽                                                   
      iArray[35][2]=200;            	        //列最大值                                             
      iArray[35][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                  
      iArray[36]=new Array();                                                                      
      iArray[36][0]="地方教育费附加税";         		//列名                                               
      iArray[36][1]="80px";            		//列宽                                                   
      iArray[36][2]=200;            	        //列最大值                                             
      iArray[36][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[37]=new Array();                                                                      
      iArray[37][0]="其它附加税";         		//列名                                                   
      iArray[37][1]="80px";            		//列宽                                                   
      iArray[37][2]=200;            	        //列最大值                                             
      iArray[37][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[38]=new Array();                                                                      
      iArray[38][0]="扣款";         		//列名                                                       
      iArray[38][1]="60px";            		//列宽                                                   
      iArray[38][2]=200;            	        //列最大值                                             
      iArray[38][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
                                                                                                   
      iArray[39]=new Array();                                                                      
      iArray[39][0]="上次佣金余额指标";         		//列名                                               
      iArray[39][1]="120px";            		//列宽                                               
      iArray[39][2]=200;            	        //列最大值                                             
      iArray[39][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
      
      iArray[40]=new Array();
      iArray[40][0]="本期应发佣金指标";         		//列名
      iArray[40][1]="120px";            		//列宽
      iArray[40][2]=200;            	        //列最大值
      iArray[40][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[41]=new Array();                                                                      
      iArray[41][0]="本期实发佣金指标";         		//列名                                               
      iArray[41][1]="120px";            		//列宽                                               
      iArray[41][2]=200;            	        //列最大值                                             
      iArray[41][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                  
      
   
//      iArray[23]=new Array();
//      iArray[23][0]="差勤实扣";         		//列名
//      iArray[23][1]="60px";            		//列宽
//      iArray[23][2]=200;            	        //列最大值
//      iArray[23][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[24]=new Array();
//      iArray[24][0]="税前薪资";         		//列名
//      iArray[24][1]="60px";            		//列宽
//      iArray[24][2]=200;            	        //列最大值
//      iArray[24][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[25]=new Array();
//      iArray[25][0]="养老金计提";         		//列名
//      iArray[25][1]="80px";            		//列宽
//      iArray[25][2]=200;            	        //列最大值
//      iArray[25][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//     
//      iArray[26]=new Array();
//      iArray[26][0]="个人所得税";         		//列名
//      iArray[26][1]="80px";            		//列宽
//      iArray[26][2]=200;            	        //列最大值
//      iArray[26][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[27]=new Array();
//      iArray[27][0]="个人营业税";         		//列名
//      iArray[27][1]="80px";            		//列宽
//      iArray[27][2]=200;            	        //列最大值
//      iArray[27][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[28]=new Array();
//      iArray[28][0]="扣款";         		//列名
//      iArray[28][1]="80px";            		//列宽
//      iArray[28][2]=200;            	        //列最大值
//      iArray[28][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[29]=new Array();
//      iArray[29][0]="上次佣金余额指标";         		//列名
//      iArray[29][1]="60";            		//列宽
//      iArray[29][2]=200;            	        //列最大值
//      iArray[29][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//
//      iArray[30]=new Array();
//      iArray[30][0]="本期应发佣金指标";         		//列名
//      iArray[30][1]="120px";            		//列宽
//      iArray[30][2]=200;            	        //列最大值
//      iArray[30][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[31]=new Array();
//      iArray[31][0]="本期实发佣金指标";         		//列名
//      iArray[31][1]="120px";            		//列宽
//      iArray[31][2]=200;            	        //列最大值
//      iArray[31][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      AgentQueryGrid = new MulLineEnter( "fm" , "AgentQueryGrid" ); 
      //这些属性必须在loadMulLine前
      AgentQueryGrid.mulLineCount = 10;   
      AgentQueryGrid.displayTitle = 1;
      AgentQueryGrid.hiddenPlus = 1;
      AgentQueryGrid.hiddenSubtraction = 1;
      AgentQueryGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
 }
function initForm()
{
  try
  {
    initInpBox();
    
    initAgentQueryGrid();
  }
  catch(re)
  {
    alert("AgentWageGatherInputInit.jsp-->InitForm函数中发生异常:初始化界面错误2!");
  }
}

</script>
