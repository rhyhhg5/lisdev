   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}

//提数操作
function submitForm()
{
	if (fm.all('ManageCom').value==null||fm.all('ManageCom').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentWageBankInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         
function AgentWageBankSave()
{
  fm.target="fraSubmit";
  //首先检验录入框
  if(!verifyInput()) return false;
  //校验是否该月佣金已算过
  var tWageNo = fm.WageYear.value + fm.WageMonth.value;
  var strSql = "select AgentCode from LAWage " 
               +"where IndexCalNo = '"+tWageNo+"'"
               +getWherePart('ManageCom','ManageCom','like')
               +getWherePart('BranchType');

  //查询SQL，返回结果字符串
  var strQueryResult = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strQueryResult) 
  {  
    alert("该月佣金已经确认，无法再计算！");
     return false;
  }	
  

   var tBranchType=fm.all('BranchType').value;
   var tBranchType2=fm.all('BranchType2').value;
   var tManageCom=fm.all('ManageCom').value;
    var IndexCalNo=fm.WageYear.value + fm.WageMonth.value;
   if (tBranchType=='2' && tBranchType2=='01')
   { 
      var tSql = "select 'Y' from LAAgent where ManageCom like '" +tManageCom + "%' "
              + " and BranchType = '2'  and BranchType2 ='01' "
              + " and EmployDate <= (select enddate from lastatsegment where stattype='91' and yearmonth= " +IndexCalNo + ")"
              + " and outworkdate is null  " 
              + " and exists (select 'X' from latree where agentcode=laagent.agentcode and (SpeciFlag is null or SpeciFlag<>'01'))" 
              + " and agentstate in ('03','04') "
              ;
      strQueryResult = easyQueryVer3(tSql, 1, 1, 1);   
      if(strQueryResult)
      { 	
            alert("分公司下存有离职登记人员，无法进行薪资试算!");
            return false; 
      }        
   }
  
  //var strS = "select agentcode from LAWage " 
  //             +" where IndexCalNo = (select max(distinct indexcalno) from lawage where managecom like '"+fm.all('ManageCom').value+"%' and branchtype='2' and branchtype2='01' ) "
  //             +" and state='0' "
  //             +getWherePart('ManageCom','ManageCom','like')
  //             +getWherePart('BranchType')
  //             +getWherePart('BranchType2');
  //
  //查询SQL，返回结果字符串
  //var strQueryResultS = easyQueryVer3(strS, 1, 1, 1);  

  //判断是否查询成功
 // if (strQueryResultS) 
 // {  
 //   alert("当前最大薪资月份薪资信息没有确认，请确认后再进行此月的薪资计算！");
 //    return false;
 // }	
  
  var tsql = "select a.modifydate ,current date from  lacontfycrate a,lacommision b where a.grpcontno=b.grpcontno "
           + " and a.riskcode = b.riskcode  and a.Flag='N' and b.wageno='"+tWageNo+"' and  a.modifydate= current date " 
           +getWherePart('b.ManageCom','ManageCom','like')           
           +getWherePart('b.BranchType','BranchType')
           +getWherePart('b.BranchType2','BranchType2');
           
  strQueryResult = easyQueryVer3(tsql, 1, 1, 1);   
  if(strQueryResult)
  { 	
    alert("非标准业务提奖比例为今天录入,明天才能够计算薪资!");
    return false; 
  }
    var strSql = "select max(indexcalno) from lawage "
                  +"where 1=1 and state = '0' and indexcalno>='201205'"
               +getWherePart('ManageCom','ManageCom','like')
               +getWherePart('BranchType')
               +getWherePart('BranchType2');
  var strQueryResult = easyQueryVer3(strSql, 1, 1, 1); 
   	 var arr = decodeEasyQueryResult(strQueryResult);
   	 var temp = arr[0][0];
   	 if(temp!=''&&temp!=null){
   	 if(temp<tWageNo)
   	 {
     alert(temp+"月份薪资确认未审核发放,本月不能进行薪资计算");
     return false;
     }
  }
           
  divAgentQuery.style.display='none'; 
   //if (fm.all('BranchType').value=='3')
 	fm.action="./AgentWageBankSave.jsp"
   //else if (fm.all('BranchType').value=='2')
 	//fm.action="./AgentWageCorpSave.jsp";
   //else
   //	{
    //      alert("输入的展业机构类型有误，请重新输入！！！");
    //      return;	
   	//}
  submitForm();	
}  
                  
//执行打印
function AgentWageBankPrt()
{
  fm.target="f1Print";
//首先检验录入框
  if(!verifyInput()) return false;
//   window.open("./AgentWageBankQuery.jsp?WageYear="+WageYear+);
  if(fm.all('WageYear').value==''||fm.all('WageMonth').value==''||fm.all('BranchType').value=='')
   {
     alert("请填写打印条件");
     return ;	
   }
  if (fm.all('ManageCom').value==null||fm.all('ManageCom').value=='')
   {
	alert("管理机构不能为空!");
	return false;
   }
  if (fm.all('BranchType').value=='3')
 	fm.action="./AgentWBF1PSave.jsp";
  else if (fm.all('BranchType').value=='2')
 	fm.action="./AgentWCF1PSave.jsp";  
  fm.submit();	
}

//执行查询
function AgentWageBankQuery()
{

  fm.target="fraSubmit";
	    //首先检验录入框
  if(!verifyInput()) return false;
 
 if(fm.all('BranchType').value==null ||fm.all('BranchType').value=='')
  {
    alert("请填写查询条件");
    return ;	
  }
  divAgentQuery.style.display='';
  initAgentQueryGrid();
  if (fm.all('BranchType').value=='3')
 	showBankRecord();
 else if (fm.all('BranchType').value=='2')
 	showCorpRecord();   
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showBankRecord()
{
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");

  var Year = fm.all('WageYear').value;
  var Month = fm.all('WageMonth').value;
  var strSql = "select c.groupAgentCode,c.Name,b.branchattr,a.ManageCom,a.f22,a.f23,a.f24,a.LastMoney,a.k04,a.k05,a.k06,a.k07,a.k08,a.k09,a.f26,a.f27,a.f29,a.ShouldMoney,a.k02,a.f28,a.CurrMoney,a.summoney from LAWage a,labranchgroup b,laagent c " 
               +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode and c.BranchType='3' and IndexCalNo = ("+Year+Month+") and (b.state<>'1' or b.state is null)"
                +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
               +tReturn
               +" order by a.AgentCode";

  //alert(strSql);	    
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}


//显示数据的函数，和easyQuery及MulLine 一起使用
function showCorpRecord()
{
	
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");

  var Year = fm.all('WageYear').value;
  var Month = fm.all('WageMonth').value;	    
	//查询SQL，返回结果字符串
	var tbranchtype = fm.BranchType.value;        
  var tbranchtype2 = fm.BranchType2.value;      

	var strSql2 = "select c.groupAgentCode,c.Name,b.branchattr,a.ManageCom,"
	             +"a.f22,w02,a.k20,a.f12,a.W10,a.f07,a.f09,a.f23,a.f20,f13,a.f24,a.w08,"
	             +"a.F26,a.K12,a.ShouldMoney,a.LastMoney,a.K01,a.K02,a.K03,a.K04,a.CurrMoney,a.SumMoney "
	             +" from LAWagetemp a,labranchgroup b,laagent c " 
               +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
               +"and IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null) "
               +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
               +" and a.branchtype = '" + trim(tbranchtype)
			   + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
               +" order by a.AgentCode";	
	
  turnPage.strQueryResult  = easyQueryVer3(strSql2, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql2; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}


function WageDownLoad()
{
  if(!verifyInput()) return false;
  
  var Year = fm.all('WageYear').value;
  var Month = fm.all('WageMonth').value;
  var managecom=	fm.all('ManageCom').value;    
  var strSql11111 ="select c.groupAgentCode,c.Name,b.branchattr,a.ManageCom,"
	             +"a.f22,w02,a.k20,a.f12,a.f07,a.f09,a.f23,a.f20,f13,a.f24,a.w08,"
	             +"a.F26,a.K12,a.ShouldMoney,a.LastMoney,a.K01,a.K02,a.K03,a.K04,a.CurrMoney,a.SumMoney "
	             +" from LAWage a,labranchgroup b,laagent c " 
              +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
              +" and IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null) "
              +" and a.ManageCom like '"+trim(managecom)+"%' "
              //+getWherePart('a.ManageCom','ManageCom','like')
              +" and a.branchtype = '2'  and  a.branchtype2 = '01' "
              +" order by a.AgentCode";	  

    fm.querySql.value = strSql11111;
    var oldAction = fm.action;
    fm.action = "GrpAgentWageXLS.jsp";
    fm.submit();
}