<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
 <html>  
<%
//程序名称：AdjustPol.jsp
//程序功能：
//创建日期：2003-06-30
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="AdjustPol.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AdjustPolInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form action="./AdjustPolSave.jsp"  method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		客户经理业绩调整
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>          
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom  verify="代理机构|NOTNULL"  ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);">
          </TD>        
        </TR>
        <TR  class= common>
          <TD  class= title>
            客户经理代码
          </TD>
          <TD  class= input>
            <Input class=common  name=AgentCode  verify="客户经理代码|NOTNULL">
          </TD>
          <TD  class= title>
            被保人客户号
          </TD>
          <TD  class= input>
            <Input class=common name=CustumNo >
          </TD>
        </TR>
        <TR class=input>     
         <TD  class= title width="25%">
            承保日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=SignDate verify="承保日期|NOTNULL">
          </TD> 
         <TD class=common>
          <input type =button class=common value="查询" onclick="BankQuery();">    
        </TD>   
       </TR>          
      </table>
    </Div>  
    <!--input type=hidden name=BranchType value=""-->
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>	     
  </form>   
   <input type =button class=common value="确认" onclick="BankConFirm();"> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


<script>
  var mcodeSql = "#1# and (code = #2# or code = #3#)";
</script>