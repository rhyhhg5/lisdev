   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
 
 
 function initEdorType(cObj)
{	
	mEdorType = " #1# and SellFlag =#Y#  ";
	showCodeList('AgentCom',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and SellFlag =#Y# ";
	showCodeListKey('AgentCom',[cObj], null, null, mEdorType, "1");
}
 
//提数操作
function submitForm()
{
	if (fm.all('ManageCom').value==null||fm.all('ManageCom').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  //showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentWageBankInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         
function AgentWageBankSave()
{
  fm.target="fraSubmit";
  //校验是否该月佣金已算过
  var tWageNo = fm.WageYear.value + fm.WageMonth.value;
  var strSql = "select AgentCode from LAWage " 
               +"where IndexCalNo = '"+tWageNo+"'"
               +getWherePart('ManageCom')
               +getWherePart('BranchType');

  //alert(strSql);	    
  //查询SQL，返回结果字符串
  var strQueryResult = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strQueryResult) 
  {  
    alert("该月佣金已计算过，无法再计算！");
    return false;
  }	
  
  //首先检验录入框
  if(!verifyInput()) return false;
  divAgentQuery.style.display='none'; 
   //if (fm.all('BranchType').value=='3')
 	fm.action="./AgentWageBankSave.jsp"
   //else if (fm.all('BranchType').value=='2')
 	//fm.action="./AgentWageCorpSave.jsp";
   //else
   //	{
    //      alert("输入的展业机构类型有误，请重新输入！！！");
    //      return;	
   	//}
  submitForm();	
}  
                  
//执行打印
function BankConFirm()
{	
	fm.action="./AdjustPolSave.jsp";
  submitForm();
}

//执行查询
function BankQuery()
{
  fm.target="fraSubmit";
	    //首先检验录入框
  if(!verifyInput()) return false;
 
 divAgentQuery.style.display='';
 initAgentQueryGrid(); 
 	showBankRecord();
 
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showBankRecord()
{
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");
  
  var strSql="select agentcode,b.name,polno,p11,p13,signdate,getpoldate,transmoney,a.AgentCom from lacommision a,lacom b where " 
              +"branchtype='3' and a.agentcom=b.agentcom "
              +getWherePart('a.ManageCom','ManageCom','like')
              +getWherePart('a.AgentCom','AgentCom')
              +getWherePart('a.AgentCode','AgentCode')
              +getWherePart('a.p12','CustumNo')
              +getWherePart('SignDate')
              +tReturn
              +" order by a.AgentCode";
  
  

  //alert(strSql);	    
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}


