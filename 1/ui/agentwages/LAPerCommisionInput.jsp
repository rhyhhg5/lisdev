<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-6-25 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LAPerCommision.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAPerCommisionInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>扎帐提数计算查询 </title>
</head>
<body  onload="initForm();">
  <form action="./LACommisionF1PSave.jsp" method=post name=fm target="f1print" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入查询条件：
			</td>
		</tr>
	</table>
	
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>
          <TD  class= title>
            绩效计算年月
          </TD>
          <TD  class= input>
            <Input class= common name=WageNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            开始日期
          </TD>
          <TD  class= input>
            <!--Input class=common name=BeginDate -->
            <input name=BeginDate class='coolDatePicker' dateFormat='short'>
          </TD>
          <TD  class= title>
            结束日期
          </TD>
          <TD  class= input>
            <!--Input class=common name=EndDate -->
            <input name=EndDate class='coolDatePicker' dateFormat='short'>
          </TD>
        </TR>
        <TR>
          <TD  class= title>
            员工代码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode verify="员工编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this], [0,1],null,acodeSql,'1',null);" onkeyup="return showCodeListKey('AgentCode', [this], [0,1],null,acodeSql,'1',null);">
          </TD>
          <TD  class= title>
            分支机构
          </TD>
          <TD  class= input>
            <!--Input class="code" name=BranchAttr  ondblclick="showCodeList('BranchAttr',[this],null,null,xcodeSql,'1',null);" onkeyup="return showCodeListKey('BranchAttr',[this],null,null,xcodeSql,'1',null);" -->
            <Input class=common name=BranchAttr>
          </TD>
          <!--TD  class= title>
            展业类型
          </TD>
          <TD  class= input>
            <Input class="code" name=BranchType  ondblclick="showCodeList('branchtype',[this],null,null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('branchtype',[this],null,null,mcodeSql,'1',null);" >
          </TD-->
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">
          <!--INPUT VALUE="打印" TYPE=button onclick="printPol();"--> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 结算信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  	 <input type=hidden id="RiskCode" name="RiskCode">
  	 <input type=hidden id="fmtransact" name="fmtransact">
  	 <Input type=hidden id="BranchType" name="BranchType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var mcodeSql = "#1# and (code = #2# or code = #3#)"; 
  var acodeSql = "#1# and BranchType=#3# and (AgentState = #01# or AgentState = #02#)";
  var xcodeSql = "#1# and BranchType=#3#";
</script>
