//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//批单打印下拉框险种条件已经被写死
function initEdorType(cObj)
{
	var tRiskCode = fm.all('RiskCode').value;
	mEdorType = " 1 and RiskCode=#112103# and AppObj=#I#";
	//alert(mEdorType);
	showCodeList('EdorCode',[cObj], null, null, mEdorType, "1");
	//alert('bbb');
}

function actionKeyUp(cObj)
{
	var tRiskCode = fm.all('RiskCode').value;
	mEdorType = " 1 and RiskCode=#112103# and AppObj=#I#";
	//alert(mEdorType);
	showCodeListKey('EdorCode',[cObj], null, null, mEdorType, "1");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
function easyQueryClick()
{
	var tSQL=fm.all('BranchType').value;
	if (tSQL!='')
	{
		tSQL= getWherePart( 'a.BranchType','BranchType' );
	}
        else
        {
        	tSQL=" and (a.BranchType='2' or a.BranchType='3') "	;
        }
        
        var BranchAttr=fm.all('BranchAttr').value;
        var dSql ;
        if (BranchAttr!='')
	{
		dSql= " and a.BranchAttr like '" + BranchAttr + "%%'";
	}
        else
        {
        	dSql=""	;
        }
        
	// 初始化表格
	initPolGrid();
	
	var tReturn = getManageComLimitlike("a.ManageCom");
	// 书写SQL语句
	var strSQL = "select a.p14,a.PolNo,b.Name,c.name,a.CalDate,a.FYC,a.GrpFYC,a.DepFYC,a.TransMoney,a.SignDate,a.getpolDate,a.CValiDate from LACommision a,LAAgent b,LACom c"
	        +" where a.AgentCode=b.AgentCode and a.AgentCom=c.AgentCom "
				  + tReturn
					+ tSQL
					+ dSql
					+ getWherePart( 'a.PolNo','PolNo' )
					+ getWherePart( 'a.WageNo','WageNo' )
					+ getWherePart( 'a.AgentCode','AgentCode' )
					+ getWherePart( 'a.caldate','BeginDate','>=' )
					+ getWherePart( 'a.caldate','EndDate','<=' )
					+ "order by a.p14,a.Polno,a.WageNo";
//	alert(strSQL);
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}


//查询按钮
function QueryClick()
{
	initPolGrid();
	fm.all('Transact').value ="QUERY|EDOR";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}




function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	//arrSelected[0] = arrGrid[tRow-1];
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//alert(arrSelected[0][0]);
	return arrSelected;
}


//提交，保存按钮对应操作
function printPol()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);

  fm.fmtransact.value = "PRINT";
  fm.submit();
  showInfo.close();
	
}