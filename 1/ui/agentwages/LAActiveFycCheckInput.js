 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;
// var agentNo = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 1);
// var agentName = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 2);
// var sql="";
// sql="select distinct  contno from  lccont where agentcode='"+agentNo+"' and grpcontno='00000000000000000000' and signdate is null "
//   +" union all "
//   +" select distinct  grpcontno  from  lccont where agentcode='"+agentNo+"' and signdate is null ";	
// strQueryResult = easyQueryVer3(sql, 1, 1, 1); 
//// alert(strQueryResult);
//// return false;
// if(strQueryResult)
//   {
//    var tArr = new Array();
//    tArr = decodeEasyQueryResult(strQueryResult);	    
//     if (confirm("由于团险业务员"+agentName+"(工号"+agentNo+")：下辖未签单保单："+tArr+"，如继续进行人员调整，则以上未签单保单将划归新调入营业部；如需求将以上保单划归该业务员原属营业部，请先中止人员调整的操作，待完成出单后再进行人员调整操作。  您确实对该业务员进行团队调整吗?"))
//     {             
//       return true; 
//     }
//    else
//    {
//       mOperate="";
//       alert("您取消了该操作！");
//       return false; 
//     }     
//
// }


  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作


}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//验证字段的值
function BranchConfirm()
{
	var tReturn = parseManageComLimitlike();
  if((trim(fm.all('BranchCode').value)=="")||(fm.all('BranchCode').value==null))
  {
    alert("请输入目标展业机构代码！");
    fm.all('BranchCode').focus();
    return false;
  }
  var strSQL = "";

  //查询出非停业且展业级别在营业组以上的
  strSQL = "select BranchAttr,Name,BranchType,BranchManager,BranchLevel,UpBranch,AgentGroup,Branchmanagername,BranchType2 from LABranchGroup where 1=1 "//xijh增加Branchmanagername
         + tReturn
	     +" and BranchAttr = '"+trim(fm.all('BranchCode').value)+"' and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null)"
	     +getWherePart('BranchType','BranchType')
	     +getWherePart('BranchType2','BranchType2');

  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!strQueryResult) {
  	//查询失败
    alert("该展业机构无效！");
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    //fm.all('BranchType').value = "";
    fm.all('BranchManager').value = "";
    fm.all('ManagerName').value ="";
    fm.all('hideAgentCode').value = "";
    fm.all('hideBranchLevel').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('hideAgentGroup').value = "";
    return false;
  }

  var arr = decodeEasyQueryResult(strQueryResult);
  //if (((trim(arr[0][2])=='3')&&(trim(arr[0][4])!='3'))||((trim(arr[0][2])=='1')&&(trim(arr[0][4])!='01')))//jiangcx add for BK
  
  if ((trim(arr[0][2])=='1')&&(trim(arr[0][8])=='01') && (trim(arr[0][4])!='01'))//xjh修改，保留原有银代部分，去掉个险部分，因为个险部分已经将调整范围扩大
  {
  	//判定展业机构类型
    alert('业务员只能在营业组间调动！');
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    //fm.all('BranchType').value = "";
    fm.all('BranchManager').value = "";
    fm.all('ManagerName').value ="";
    fm.all('hideAgentCode').value = "";
    fm.all('hideBranchLevel').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('hideAgentGroup').value = "";
    return false;

  }
  fm.all('BranchName').value = arr[0][1];
  fm.all('BranchType').value = arr[0][2];
  fm.all('BranchManager').value = arr[0][3];//xjh增加，显示机构管理人员代码 
	fm.all('ManagerName').value = arr[0][7];//xjh增加，显示机构管理人名称
  fm.all('hideBranchLevel').value = arr[0][4]; 
  fm.all('hideUpBranch').value = arr[0][5];
  fm.all('hideAgentGroup').value = arr[0][6];
 
  if(arr[0][2]=='2'){
  	//如果是法人则不作目标管理人员检测。
  	//fm.BranchManager.style.readonly='true';此句无效
  	return true;
  }
  
  if((arr[0][3]==null)||(trim(arr[0][3])==''))
  {
    alert('请先确定目标机构的管理人员！');
  	fm.all('BranchManager').value = '';
  	fm.all('hideAgentCode').value = '';
  	fm.all('BranchManager').focus();
  	return false;
  }
  else
  {
  	strSQL = "select Name,AgentCode from LAAgent where AgentCode = '"+trim(arr[0][3])+"'";
  	strQueryResult = easyQueryVer3(strSQL,1,0,1);
  	if (!strQueryResult)
  	{
  	   fm.all('BranchManager').value = '';
  	   fm.all('hideAgentCode').value = '';
  	   //fm.BranchManager.disabled = false;
  	   alert('请先确定目标机构的管理人员！');
  	   fm.all('BranchManager').focus();
  	   return false;
  	}
  	else
  	{
  	   arr = decodeEasyQueryResult(strQueryResult);
           fm.all('ManagerName').value = arr[0][0];
  	   fm.all('hideAgentCode').value = arr[0][1];
  	   fm.all('BranchManager').value = arr[0][1];
  	   //fm.BranchManager.disabled = true;
  	   //alert('ssds');
  	}
  }
  return true;

}
function changeManager()
{
   if (getWherePart('BranchManager')=='')
     return false;
   var strSQL = "select AgentCode,Name from LAAgent where 1=1 and (AgentState is null or AgentState < '03')"
                + getWherePart('AgentCode','BranchManager')
                + getWherePart('AgentGroup','BranchCode','<>');
//        alert(strSQL);
   var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('该代理人无效！');
   	fm.all('BranchManager').value = '';
   	fm.all('ManagerName').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   fm.all('ManagerName').value = arr[0][1];
}

function queryAgent()
{

  if (verifyInput() == false)
    return false;
  var strSQL = "";
	strSQL = "select commisionsn,MANAGECOM,contno,riskcode,agentcode,payintv,payyears"
	//+",'Y'"
	+" ,case when renewcount=0 then (payyear+1) else  (renewcount+1)  end "
	+",transmoney,branchtype3 "

	     +" from lacommision "
	     +" where transmoney<>0 and fyc=0 and branchtype='5' and branchtype2='01'"
//	     +" AND RISKCODE<>'1601'"
//	     +" and substr(payplancode,1,6)<>'000000' "
	     +" and commdire='1' "
	     + getWherePart('WageNo','WageNo')
	     + getWherePart('ManageCom','ManageCom')	    
	     + getWherePart('AgentCode','AgentCode')	 ;
	
   strSQL=strSQL+" ORDER BY MANAGECOM,RISKCODE,PAYINTV,PAYYEARS WITH UR";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
  	alert('不存在符合条件的有效纪录！');
  	return false;
  }
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3,4,5,6,7,8,9]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentGrid;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function submitSave()
{

   var mCount = AgentGrid.mulLineCount;

   if ((mCount == null)||(mCount == 0))
   {
   	alert("请查询出要调动的人员！");
   	return false;
   }
   //submitForm();
  //在这里加上校验
   
//   if((ArchieveGrid.getRowColData(i,1) == null)||(ArchieveGrid.getRowColData(i,1) == ""))
//			{
//				alert("第"+(i+1)+"行职级代码不能为空");
//				ArchieveGrid.setFocus(i,1,ArchieveGrid);
//				selFlag = false;
//				break;
//			}
   var isChk=false;
   for (i=0;i<mCount;i++)
   {
   	if( AgentGrid.getSelNo( i ))
   	{
   	   isChk=true;
       /**
       var agentcode=ArchieveGrid.getRowColData(i,1);
       */
   	   break;
   	}
   }
   if (isChk==false)
   {
     alert("请选出要调动的人员！");
     return false;
   }
   else
     submitForm();

}

function clearAll()
{
//   fm.all('BranchCode').value = '';
//   //fm.all('BranchType').value = '';
//   fm.all('BranchName').value = '';
//   fm.all('BranchManager').value = '';
//   fm.all('ManagerName').value = '';
//   fm.all('hideAgentCode').value = '';
//   fm.all('hideBranchLevel').value = '';
//   fm.all('hideUpBranch').value='';
//   fm.all('hideAgentGroup').value='';
   clearGrid();
}
function clearGrid()
{
   fm.all('ManageCom').value = '';
   fm.all('ManageComName').value = '';
   fm.all('WageNo').value = '';
  // fm.all('AgentSeries').value = '';
   fm.all('AgentCode').value = '';
   AgentGrid.clearData("AgentGrid");
   AgentGrid.clearData("ContGrid");
}
/*********************************************************************
 *  通过业务员查询保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setContValue()
{
	 initContGrid()                      
   var managecom = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 2);
   var riskcode = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 4);
   var tF01 = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 10);
   var  ttManageCom=managecom.substring(0,4);
	 //保单性质为1：已签单； 2：撤保或退保
	 //查询保单信息
	 var strSQL = "select managecom,riskcode,f01,payintv,f03,f04,curyear,rate,makedate,modifydate  "
	 +" from laratecommision "
   +" where branchtype='5' and branchtype2='01' " 
   +" and F01 = '"+tF01+"'"
   +" and managecom like '"+ttManageCom+"%'"
   +" and riskcode='"+riskcode+"'"
   +" order by managecom,riskcode,f01,payintv,f03,curyear with ur";
     
    turnPage1.queryModal(strSQL, ContGrid);    
    showCodeName();
    
 }
 
 
 
 function ListDownLoad()
{

  if (verifyInput() == false)
    return false;
// if(AgentQueryGrid.mulLineCount==0)
// {
//    alert("列表中没有数据可下载");
//    return;
// }
// else {
 // 书写SQL语句
   // 拼SQL语句，从页面采集信息

 var  strSql = "select commisionsn,MANAGECOM,contno,riskcode,agentcode,payintv,payyears"
	//+",'Y'"
	+" ,case when renewcount=0 then (payyear+1) else  (renewcount+1)  end "
	+",transmoney ,branchtype3"

	     +" from lacommision "
	     +" where transmoney<>0 and fyc=0 and branchtype='5' and branchtype2='01'"
//	     +" AND RISKCODE<>'1601'"
//	     +" and substr(payplancode,1,6)<>'000000' "
	     +" and commdire='1' "
	     + getWherePart('WageNo','WageNo')
	     + getWherePart('ManageCom','ManageCom')	    
	     + getWherePart('AgentCode','AgentCode')	 ;
	
   strSql=strSql+" ORDER BY MANAGECOM,RISKCODE,PAYINTV,PAYYEARS WITH UR";
   fm.querySql.value = strSql;
    var oldAction = fm.action;
    fm.action = "LAActiveFycCheckXLS.jsp";
    fm.submit();
    fm.action = oldAction;
//  }
}