<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：佣金基数表设置录入界面
//创建时间：2014-12-1
//创建人  ：yangyang
// above is miaoxiangzheng's new_add
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
%>
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">
var manageCom = <%=tG.ManageCom%>;
var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";

</script>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAActiveSelCommissionInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LAActiveSelCommissionInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>
   
<body  onload="initForm();initElementtype();" >
 <form action="./LAActiveSelCommissionSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
   <TR  class= common> 
		<TD  class= title>
            管理机构
    </TD>
        <TD  class= input>
            <Input class="codeno" name=ManageCom 
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary > 
     </TD> 
       <td class=title>
     业务员
     </td>
     <td class= input>
     	<Input class=codeno name=GrpAgentCode class='codeno'
       ondblclick="return showCodeList('agentcode',[this,GrpAgentName],[0,1],null,msql,'1',1);"
       onkeyup="return showCodeListKey('agentcode',[this,GrpAgentName],[0,1],null,msql,'1',1);"
       ><input class=codename name=GrpAgentName readonly=true ></td>
   </TR>
   <tr>	
    <TD  class= title>
            佣金年月起期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=StartYearMonth  elementtype=nacessary ><font color="red">(yyyymm)
          </TD>
            <TD  class= title>
            佣金年月止期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=EndYearMonth elementtype=nacessary ><font color="red">(yyyymm)
          </TD>
   </tr>
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="下  载" class=cssButton onclick="return DoNewDownload();">
    		<input type=button value="修  改" class=cssButton onclick="return DoSave();">
    		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 
    	</td>
    </tr>      
  </table>
  <br/><font color="#ff0000">(注：1.若业务员没有健代产佣金金额或健代寿佣金金额请录入0。) 
					</font>
  <br/><font color="#ff0000">(注：2.若管理机构所在的业务员已经薪资试算，录入当月的交叉销售佣金后，请重新进行薪资试算。) 
					</font>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>交叉销售佣金计算基数设置</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
  </div>
  <br><hr>
  <Input type=hidden name=BranchType  value =<%=BranchType %>>   
  <Input type=hidden name=BranchType2 value =<%=BranchType2 %>> 
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="hideOperate" name="hideOperate">
  <input type=hidden class=Common name=querySql >
  <Input type=hidden class="readonly" name=diskimporttype> 
  </form>
<form action="./LADiskImportActiveCommSave.jsp" method=post name=fm2 enctype="multipart/form-data" target="fraSubmit">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportFile);">
    		</td>
    		<td class= titleImg>交叉销售佣金磁盘导入</td>
    		
    	</tr>
  </table>
  <div id="divImportFile" style="display:''">   

  <table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">文件名</TD>
				<TD class=input>
					<Input type="file" class= "common" style="width:300px" name="FileName" id="FileName"/>
				</TD><td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()"></td>
			</TR>
  </table>
				  <Input type=button class=cssButton name="goDiskImport" value="导  入" id="goDiskImport"  onclick="diskImport()">
				  <Input type=button class=cssButton value="重  置" onclick="clearImport()">

<!--
 <Input type=button class=cssButton name="goDiskImport" value="磁盘导入" id="goDiskImport"  onclick="diskImport()">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()">
-->
<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportQuery);">
    		</td>
    		<td class= titleImg>交叉销售佣金磁盘导入日志查询</td>
    		
    	</tr>
  </table>
  <div id="divImportQuery" style="display:''">
  	<table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">导入文件批次</TD>
				<TD class=input>
					<Input type="text" class= "common"  name="FileImportNo"/>
				</TD>
				
				<TD  class=title style="width:100px">
				查询类型
				</td>
				<td class=input>
				<Input class="codeno" name=queryType  CodeData="0|^0|全部|^1|导入错误行|^2|导入成功行" ondblclick="return showCodeListEx('querytypelist',[this,queryName],[0,1]);" onkeyup="return showCodeListKeyEx('querytypelist',[this,queryName],[0,1]);" onchange=""><input class=codename name=queryName  readonly=true >
				</TD>
				<td></td>
			</TR>
   </table>
  <Input type=button class=cssButton value="查  询" onclick="queryImportResult()"/>
  <div id="divImportResultGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanImportResultGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage2.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage2.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage2.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage2.lastPage();">
  </div>
  </div>  
 </div>
  <Input type=hidden name=BranchType2> 
  <Input type=hidden  name=BranchType >
  <Input type=hidden name=diskimporttype>
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




