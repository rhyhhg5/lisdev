<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWageConfirmInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAWageConfirmGrpInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAWageConfirmGrpInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
  <title>薪酬审核发放 </title>
</head>
	<%
	String tTitleAgent="";
	if("02".equals(BranchType2))
	{
	tTitleAgent = "服务专员";
}else if("01".equals(BranchType2))
	{
	tTitleAgent = "业务人员";
	}
	%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAWageConfirmGrpSubmit.jsp" method=post name=fm target="fraSubmit">
  <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		管理机构业务员月终薪资审核发放
       		 </td>   		     
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom   verify="管理机构|NOTNULL"
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'4','char(length(trim(comcode)))',1);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,'4','char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageComName readOnly  elementtype=nacessary> 
          </TD> 
           <TD  class= title>
            薪资所属年
          </TD>
          <TD  class= input>
            <Input class=common  name=WageYear verify="薪资所属年|NOTNULL&len=4&int"  elementtype=nacessary ><font color="red">yyyy
          </TD>        
        </TR>
        <TR  class= common>
         
          <TD  class= title>
            薪资所属月
          </TD>
          <TD  class= input>
            <Input class=common name=WageMonth verify="薪资所属月|NOTNULL&len=2&int&value<=12&value>=01"  elementtype=nacessary ><font color="red">mm
          </TD>
        </TR>
      </table>
      <Input type=hidden class=common name=IndexCalNo>  
      <Input type=hidden class= common name=GroupAgentCode >
      <Input type=hidden class= common name=AgentGroup >
      <input type=hidden name=BranchType value='<%=BranchType%>'>
      <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
      <input type=hidden name=AgentCode value=''>
      <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 				
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
    <Div  id= "divAGroupGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanWageGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 		
      <p>
        <INPUT VALUE="薪酬审核" class=cssbutton TYPE=button onclick="submitForm();"> 
      </p>			
    </div>     
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
