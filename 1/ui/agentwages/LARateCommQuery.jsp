<%
//程序名称：LARateCommQuery.jsp
//程序功能：
//创建日期：2003-07-08 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LARateCommQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LARateCommQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>银代提奖比例信息 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPlan1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 代理机构</td>
        <td  class= input> <Input class="code" name=AgentCom  ondblclick="showCodeList('AgentCom',[this]);" onkeyup="return showCodeListKey('AgentCom',[this]);" >  </td>        
        <td  class= title> 险种	</td>
        <td  class= input> <input name=RiskCode class="code" verify="险种|code:Riskcode" 
		         ondblclick="return showCodeList('riskcode',[this]);" onkeyup="return showCodeListKey('riskcode',[this]);"> </td>

      </tr>
      <TR>
          <TD  class= title width="25%">展业类型</TD>
          <TD  class= input>
            <Input class=readonly  readonly name=BranchType  >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="code" name=Sex verify="被保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
          </TD>
      </TR>  
      <tr  class= common>  
        <td  class= title>投保年龄</td>
        <td  class= input> <input class=common name=AppAge > </td>
        <td  class= title>保险年期</td>
        <td  class= input><input class=common name=Years > 	</td>
      </tr>
      <tr  class= common> 
        <td  class= title>交费间隔</td>
        <td  class= input><input class=common name=PayIntv > 	</td>
        <td  class= title>手续费比率</td>
        <td  class= input><input name=Rate class= common > </td>
      </tr>
      <tr  class= common> 
        <td  class= title>提奖类型</td>
        <TD  class= input><Input class="code" name=CalType  CodeData="0|^01|组提奖计算^02|部提奖计算^09|银代个人提奖^10|团体提奖" ondblclick="showCodeListEx('CalType',[this],[0]);"  onkeyup="showCodeListKeyEx('CalType',[this],[0]);">
          </TD>
        <td  class= title>保单类型</td>
        <td  class= input><input class='code' name=PolType 
		         CodeData="0|^0|优惠业务^1|正常业务" 
		         ondblclick="showCodeListEx('PolType',[this],[0]);"  onkeyup="showCodeListKeyEx('PolType',[this],[0]);"</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 渠道类型</td>
        <td  class= input> 
		          <input name=ChannelType class='code' 
		                           ondblclick="return showCodeList('ChannelType',[this]);" 
                                  onkeyup="return showCodeListKey('ChannelType',[this]);" >	</td>
        <td  class= title>保单年度</td>
        <td  class= input><input class=common name=CurYear > 	</td>
        </tr>
      <tr  class= common>         
        <td  class= title> 操作员代码</td>
        <td  class= input> <input name=Operator class= common > </td>		    
      </tr>
      </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPlanGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPlanGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPlanGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 				
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
