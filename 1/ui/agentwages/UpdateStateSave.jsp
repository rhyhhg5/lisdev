<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：UpdateStateSave.jsp
//程序功能：Save.jsp
//创建日期：2017-10-19
//创建人  ：yangjian
%>
<SCRIPT src="RepeatCalInput.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentquery.*"%>

<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";

/*   String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim(); */
  
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  
  LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
	  int lineCount = 0;
	  // 在JSP中获取CheckBox每一行
	  String tChk[] = request.getParameterValues("InpUpdateStateGridChk");
	  // 在JSP中得到MulLine中的值 第一列
	  String tManageCom[] = request.getParameterValues("UpdateStateGrid1");
	  String tWageNo[] = request.getParameterValues("UpdateStateGrid3");
	  String tState[]= request.getParameterValues("UpdateStateGrid5");
	  String tBranchtype[]= request.getParameterValues("UpdateStateGrid6");
	  String tBranchtype2[]= request.getParameterValues("UpdateStateGrid8");
	  int number = 0; 
	  for(int index=0;index<tChk.length;index++)
      {
          if(tChk[index].equals("1")) 
          {
        	  LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
        	  tLAWageHistorySchema.setManageCom(tManageCom[index]);
        	  tLAWageHistorySchema.setWageNo(tWageNo[index]);
        	  tLAWageHistorySchema.setState(tState[index]);
        	  tLAWageHistorySchema.setBranchType(tBranchtype[index]);
        	  tLAWageHistorySchema.setBranchType2(tBranchtype2[index]);
        	  tLAWageHistorySet.add(tLAWageHistorySchema);
          }
	  }
	}

//准备传输数据 VData
  VData tVData = new VData();
  tVData.addElement(tGI);
  tVData.addElement(tLAWageHistorySet);

  UpdateStateBL tUpdateStateBL = new UpdateStateBL();
  tUpdateStateBL.submitData(tVData,"State");

 
   
    	
   //如果在Catch中发现异常，则不从错误类中提取错误信息
   try
   {
     tError = tUpdateStateBL.mErrors;
     if (!tError.needDealError())
     {                          
       Content = " 保存成功! ";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content = " 失败，原因:" + tError.getFirstError();
       FlagStr = "Fail";
     }
   } 
   catch(Exception ex)
   {
     Content = " 失败，原因:" + ex.toString();
     FlagStr = "Fail";    
   }    
   System.out.println(Content);  
   
   /* try
   {
 	  tLAWageHistoryOfStateUpdateBL.submitData(tVData,"State");
   }
   catch(Exception ex)
   {
     Content = "保存失败，原因是:" + ex.toString();
     System.out.println(Content);
     FlagStr = "Fail";
   }

   if (!FlagStr.equals("Fail"))
   {
     tError = tLAWageHistoryOfStateUpdateBL.mErrors;
     if (!tError.needDealError())
     {
     	Content = " 保存成功! ";
     	FlagStr = "Succ";
     }
     else
     {

     	Content = " 保存失败，原因是:" + tError.getFirstError();
     	FlagStr = "Fail";
     	System.out.println(Content);
     }
   }
   System.out.println(Content); */

%>                      
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

