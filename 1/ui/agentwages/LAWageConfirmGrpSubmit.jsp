<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAContWriteSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAWageSchema mLAWageSchema = new LAWageSchema();
  LAWageSet  mLAWageSet = new LAWageSet();
  LAAgentSchema  mLAAgentSchema=new  LAAgentSchema();
  LAGrpWageUI mLAGrpWageUI = new LAGrpWageUI();
  LAWageUI mLAWageUI = new LAWageUI();
  //输出参数
  CErrors tError = null;
  CErrors tError2 = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String flag="0";            //验证往后台传入SET还是SHEMA
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

	ExeSQL tExeSQL =new ExeSQL();
  System.out.println("begin LAContAccessory schema...");
  //取得佣金信息
  mLAWageSchema.setIndexCalNo(request.getParameter("IndexCalNo"));
  mLAWageSchema.setAgentCode(request.getParameter("AgentCode"));
  mLAWageSchema.setAgentGroup(request.getParameter("AgentGroup"));
  mLAWageSchema.setManageCom(request.getParameter("ManageCom"));
  mLAWageSchema.setBranchType(request.getParameter("BranchType"));
  mLAWageSchema.setBranchType2(request.getParameter("BranchType2"));
  mLAAgentSchema.setName(request.getParameter("Name"));
  //mLAWageSchema.setState("0");
  
  int lineCount = 0;
  String tSel[] = request.getParameterValues("InpWageGridChk");
  String tIndexcalno[] = request.getParameterValues("WageGrid1");
  String tAgentcode[] = request.getParameterValues("WageGrid2");
  String tManagecom[] = request.getParameterValues("WageGrid4"); 
  //String tSummoney[] = request.getParameterValues("WageGrid6");
 // String tState[] = request.getParameterValues("WageGrid5");
 String cAgentcode[] = request.getParameterValues("WageGrid8");
   System.out.println("update123 LAWage1111111111111111111111111111111111:---" + mLAAgentSchema.getName()+flag);
  VData tVData = new VData();
  FlagStr="";
        tVData.add(tG);
        tVData.addElement(mLAWageSchema);
        tVData.addElement(mLAAgentSchema);
        tVData.addElement(mLAWageSet);
        tVData.addElement("0");
        tVData.addElement(request.getParameter("BranchType"));
        tVData.addElement(request.getParameter("BranchType2"));
        tVData.addElement(request.getParameter("ManageCom"));
        tVData.addElement(request.getParameter("IndexCalNo"));
        
  
  try
  {
    System.out.println("begin to submit UI");
    if( "2".equals(mLAWageSchema.getBranchType()))
   	{
    	mLAGrpWageUI.submitData(tVData,tOperate);
   	}
    if( "3".equals(mLAWageSchema.getBranchType()) && "01".equals(mLAWageSchema.getBranchType2()))
   	{
    	mLAWageUI.submitData(tVData,tOperate);
   	}
    
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
	  if( "2".equals(mLAWageSchema.getBranchType()))
	   	{
           tError = mLAGrpWageUI.mErrors;
	   	}
	  if( "3".equals(mLAWageSchema.getBranchType()) && "01".equals(mLAWageSchema.getBranchType2()))
	   	{
         tError = mLAWageUI.mErrors;
	   	}
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>