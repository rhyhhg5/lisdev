<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LARateComm.jsp
//程序功能：银代提奖比例录入
//创建人  ：销售管理
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="GrpLARateComm.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="GrpLARateCommInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./GrpLARateCommSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
   <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
     <td class=titleImg>
      提奖比例维护信息
     </td>
    </td>
    </table>
  <Div  id= "divLAPlan1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        
        <td  class= title> 险种	</td>
        <td  class= input> <input name=RiskCode class="code" verify="险种|code:Riskcode" 
              ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);"> </td>
        <TD  class= title width="25%">展业类型</TD>
          <TD  class= input>
            <Input class=common name=BranchType verify="展业类型|NOTNULL" >
          </TD>
        </tr>
      
      <tr  class= common>  
        <td  class= title>投保年龄</td>
        <td  class= input> <input class=common name=AppAge verify="投保年龄|notnull"> </td>
        <td  class= title>保险年期</td>
        <td  class= input><input class=common name=Years verify="保险年期|notnull"> 	</td>
      </tr>
      <tr  class= common> 
        <td  class= title>交费间隔</td>
        <td  class= input><input class="code" name=PayIntv  verify="交费间隔|notnull" CodeData="0|^0|趸交^1|月交^12|年交^-1|不定期交" ondblclick="showCodeListEx('PayIntv',[this],[0]);"  onkeyup="showCodeListKeyEx('PayIntv',[this],[0]);"> 	</td>
        <td  class= title>提奖比例</td>
        <td  class= input><input name=Rate class= common verify = "手续费比率|notnull"> </td>
      </tr>
      <tr  class= common> 
        <td  class= title>保单年度</td>
        <td  class= input><input class=common name=CurYear verify="保单年度|notnull"> 	</td>
        <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom  verify="管理机构|notnull&code:comcode" 
            ondblclick="initEdor(this);"  onkeyup="actKey(this);"> 
          </TD>
      </tr>
      <tr  class= common> 
        <td  class= title>提奖类型</td>
        <TD  class= input><Input class="code" name=CalType verify="提奖类型|notnull"  CodeData="0|^10|团体提奖" ondblclick="showCodeListEx('PolType',[this],[0]);"  onkeyup="showCodeListKeyEx('PolType',[this],[0]);">
          </TD>
        <td  class= title>保单类型</td>
        <td  class= input><input class='code' name=PolType verify="计划类型|notnull"
		         CodeData="0|^0|优惠业务^1|正常业务" 
		         ondblclick="showCodeListEx('PolType',[this],[0]);"  onkeyup="showCodeListKeyEx('PolType',[this],[0]);"</td>
      </tr>
      <tr  class= common> 
        <td  class= title>众悦管理费水平上限</td>
        <TD  class= input><Input class=common name=F03 ></TD>
        <td  class= title>众悦管理费水平下限</td>
        <td  class= input><input class=common name=F04 ></td>
      </tr>
      <tr  class= common>         
        <td  class= title> 操作员代码</td>
        <td  class= input> <input name=Operator class= 'readonly'readonly > </td>		    
      
      
          </tr>
    </table>
  </Div>    
    <input name=hideOperate type=hidden value = ''>
    <input name=F03B type=hidden value = ''>
    <input name=RiskCodeB type=hidden value = ''>
    <input name=F04B type=hidden value = ''>
    <input name=BranchTypeB type=hidden value = ''>
    <input name=AppAgeB type=hidden value = ''>
    <input name=YearsB type=hidden value = ''>
    <input name=PayIntvB type=hidden value = ''>
    <input name=RateB type=hidden value = ''>
    <input name=CalTypeB type=hidden value = ''>
    <input name=PolTypeB type=hidden value = ''>
    <input name=CurYearB type=hidden value = ''>
    <input name=OperatorB type=hidden value = ''>
    <input name=ManageComB type=hidden value = ''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
<script>
  var sql = "";
  var sqlField = "";
</script>