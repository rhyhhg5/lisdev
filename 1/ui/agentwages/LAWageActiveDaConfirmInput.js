//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();
//修改人：解青青  时间：2014-11-24
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
    
}
//提交，保存按钮对应操作
function submitForm()
{
	fm.BranchType.disabled = false;
	if(!verifyInput()) 
  { return; }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 // alert(fm.all('ManageCom').value);
  	if(fm.all('ManageCom').value==''||fm.all('ManageCom').value==null)
  	{
	alert("请录入管理机构!");
	return;
}

 	if(fm.all('IndexCalNo').value==''||fm.all('IndexCalNo').value==null)
  	{
	alert("请录入佣金年月!");
	return;
	}
	var tmanagecom =fm.all('ManageCom').value;
	var twageno=fm.all('IndexCalNo').value;
  if(!confirm("确定对管理机构为"+tmanagecom+"薪资年月为"+twageno+"下所有的员工进行薪酬审核吗？")){
  	return false;
  }
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
//  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroupQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
   var strSql = "select a.state from LAWageHistory a" 
               +"where 1=1 "
               +getWherePart('a.wageno','IndexCalNo')             
               +getWherePart('a.ManageCom','ManageCom')
               +getWherePart('a.BranchType2','BranchType2')  
               +getWherePart('a.BranchType2','BranchType2')  ;

  //alert(strSql);	    
	//查询SQL，返回结果字符串
  var strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strQueryResult) {  
    alert("该月佣金没有确认或计算，不能审核发放！");
    return false;
  }	
  else{
  var arr1=decodeEasyQueryResult(strQueryResult1);
	for(var j=0;j<=arr1.length;j++)
	{
	if(arr1[j][0]!='13')
	 {  
	    alert("该月佣金没有确认或计算或已经审核发放，不能审核发放！");
	    return false;
	 }   
	}
 }
  //添加操作	
   var tAgentGroup =fm.all('AgentGroup').value;
   //alert("easyQueryClick1");
   // 书写SQL语句
   var strSQL = "";
   var tReturn = getManageComLimitlike("a.ManageCom");
   strSQL = "select c.GroupAgentCode from LAWage a,labranchgroup b,laagent c where 1=1 and a.agentcode=c.agentcode and a.agentgroup = b.agentgroup and a.state='0'  and (b.state<>'1' or b.state is null)"
            + getWherePart('a.branchtype','BranchType')
            + getWherePart('b.branchattr','AgentGroup')
            + getWherePart('a.indexcalno','IndexCalNo')
            + getWherePart('a.BranchType2','BranchType2')           
            + getWherePart('a.managecom','ManageCom','like')+tReturn
            + getWherePart('c.name','name')
            + getWherePart('c.GroupAgentCode','GroupAgentCode');	 
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);   
   //判断是否查询成功
   if (!turnPage.strQueryResult)
   {
     alert("本月没有进行薪资计算或者已经进行审核发放，不需要审核发放！");
     return false;
   }	
   return true;	
    
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,500,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
/*
function returnParent()
{
  var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");	
	arrSelected = new Array();
	tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	//arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

*/

// 查询按钮
function easyQueryClick()
{
	if(!verifyInput()) 
  { return; }
	// 初始化表格
	initWageGrid();
	var tAgentGroup =fm.all('AgentGroup').value;
	//alert("easyQueryClick1");
	// 书写SQL语句
	var strSQL = "";
        var tReturn = getManageComLimitlike("a.ManageCom");
	strSQL = "select a.indexcalno,c.GroupAgentCode,c.name,a.managecom,b.branchattr, a.summoney,case when a.state='0' then '未发放' when a.state='1' then '已发放' else '财务已确认' end from LAWage a,labranchgroup b,laagent c where 1=1 and a.agentcode=c.agentcode and a.agentgroup = b.agentgroup   and (b.state<>'1' or b.state is null)"
	         + getWherePart('a.branchtype','BranchType')
	         + getWherePart('b.branchattr','AgentGroup')
	         + getWherePart('a.indexcalno','IndexCalNo')
	         + getWherePart('a.BranchType2','BranchType2')
	         
	         + getWherePart('a.managecom','ManageCom','like')+tReturn
	         + getWherePart('c.name','name')
	         + getWherePart('c.GroupAgentCode','GroupAgentCode');	
	         //fm.IndexCalNo.value =  strSQL;	 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = WageGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex   = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

/*
   function check()
{
 
  	alert("登录的用户没有权限进行审批");
 	fm.all('Agree').disabled = true ;	
 	
 	return false;


}
*/