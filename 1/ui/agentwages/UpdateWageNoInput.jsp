<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
 <html>  
<%
//程序名称：UpdateWageNoInput.jsp
//程序功能：
//创建日期：2016-11-23
//创建人  ：yy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="UpdateWageNo.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UpdateWageNoInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >    
  <form action=""  method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		薪资月修改
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input 
            class ='codename' name ='ManageComName' elementtype=nacessary>
          </TD>          
          <TD  class= title >
            薪资月
          </TD>
           <TD  class= input>
            <Input class=common name=WageNo >
          </TD>
          <TD  class= title >
            业务员代码
          </TD>
           <TD  class= input>
            <Input class=common name=GroupAgentCode >
          </TD>
          </tr>
          <tr class= common>
          <TD  class= title>
            团单号
          </TD>
          <TD  class= input>
            <Input class=common name=GrpContNo >
          </TD>
          <TD  class= title>
            个单号
          </TD>
          <TD  class= input>
            <Input class=common  name=ContNo  >
          </TD>
          <TD  class= title>
            主键commisionsn
          </TD>
          <TD  class= input>
            <Input class=common  name=CommisionSN  >
          </TD>
          <td></td>
        </TR>
        <TR class=input>     
         <TD class=common>
          <input type =button class=cssbutton value="查 询" onclick="WageNoQuery();">    
          <input type =button class=cssbutton value="修 改" onclick="WageNoUpdate();">
          </td>
       </TR>          
      </table>
    </Div>  
    <!--input type=hidden name=BranchType value=""-->
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanWageNoGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>	
   <font color ='red'>注：如果要把薪资修改为空，则需要填入 null</font>   
   <font color ='red'>注：修改薪资月同时后台逻辑也维护固化表薪资月</font>   
  </form>   
  <hr>
  <form action="" name ="fm2" target="fraSubmit">
  <table  class= common>
        <TR  class= common> 
          <TD  class= title >
           		薪资月
          </TD>
           <TD  class= input>
            <Input class=common name=WageNo elementtype=nacessary verify="薪资月|notnull&len=6&int">
          </TD>
          <td></td>
          </tr>
   </table>
   <input type='hidden' name ='type'>
  <br><input type =button class=cssbutton value="插入虚拟数据" onclick="InsertHistory();"> 
  <input type =button class=cssbutton value="刷新latempmision" onclick="RefreshTable();"> 
  <input type =button class=cssbutton value="删除虚拟数据" onclick="DeleteHistory();"> 
  <br><font color="red">1、修改薪资月后需要刷新latempMision表<br>2、刷新前需要确保没有机构进行薪资计算。如果有机构正在薪资计算，则不能刷新。
  <br>3、为确保在刷新的时候，没有机构进行薪资计算，需要向lawagehistory表中插入10个虚拟机构，状态为正在计算。
  <br>4、latempmision刷新完之后自动删除虚拟的数据
  <br>5、提供一个删除虚拟数据源的功能，在刷新latempmision失败时，可以用此功能删除之前插入的虚拟数据</font>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


<script>
  var mcodeSql = "#1# and (code = #2# or code = #3#)";
</script>