<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAPresenceQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAPresenceQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAPresenceQueryGrpInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>业务人员考勤信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAPresenceQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPresence1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPresence1" style= "display: ''">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
            业务人员代码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode >
          </TD>
          <TD  class= title>
            销售机构
          </TD>
          <TD  class= input>
            <Input class=common name=AgentGroup >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class=common name=ManageCom >
          </TD>
          <TD  class= title>
            纪录顺序号
          </TD>
          <TD  class= input>
            <Input class=common name=Idx >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            类别
          </TD>
          <TD  class= input>
            <Input class='code' name=AClass maxlength=1 ondblclick="return showCodeList('presenceaclass',[this]);" onkeyup="return showCodeListKey('presenceaclass',[this]);">
          </TD>
          <TD  class= title>
            考勤执行次数
          </TD>
          <TD  class= input>
            <Input class= common name=Times >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            金额
          </TD>
          <TD  class= input>
            <Input class=common name=SumMoney >
          </TD>
          <TD  class= title>
            批注
          </TD>
          <TD  class= input>
            <Input class= common name=Noti >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            执行日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=DoneDate dateFormat='short' >
          </TD>
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class=common name=Operator >
          </TD>
        </TR>
      </table>
          <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPresenceGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
      
  	<Div  id= "divPresenceGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPresenceGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
