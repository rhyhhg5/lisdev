<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：UpdateWageNoSave.jsp
//程序功能：
//创建日期：2016-11-24
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="RepeatCalInput.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>
<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  LACommisionSet tLACommisionSet = new LACommisionSet(); 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
	  int lineCount = 0;
	  String tChk[] = request.getParameterValues("InpWageNoGridChk");
	  String tCommisionSN[] = request.getParameterValues("WageNoGrid1");
	  String tNewWageNo[] = request.getParameterValues("WageNoGrid13");
	  int number = 0; 
	  for(int index=0;index<tChk.length;index++)
      {
          if(tChk[index].equals("1")) 
          {
        	  LACommisionSchema tLACommisionSchema = new LACommisionSchema();
        	  tLACommisionSchema.setCommisionSN(tCommisionSN[index]);
        	  tLACommisionSchema.setWageNo(tNewWageNo[index]);
        	  tLACommisionSet.add(tLACommisionSchema);
          }
	  }
	}

   VData tVData = new VData();
   tVData.addElement(tGI);
   tVData.addElement(tLACommisionSet);
   UpdateWageNoBL tUpdateWageNoBL = new UpdateWageNoBL();
   tUpdateWageNoBL.submitData(tVData,"WageNo");
    	
   //如果在Catch中发现异常，则不从错误类中提取错误信息
   try
   {
     tError = tUpdateWageNoBL.mErrors;
     if (!tError.needDealError())
     {                          
       Content = " 保存成功! ";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content = " 失败，原因:" + tError.getFirstError();
       FlagStr = "Fail";
     }
   } 
   catch(Exception ex)
   {
     Content = " 失败，原因:" + ex.toString();
     FlagStr = "Fail";    
   }    
   System.out.println(Content);                  

%>                      
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

