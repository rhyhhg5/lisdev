<%
//程序名称：UpdateStateInit.jsp
//程序功能：Init.jsp
//创建日期：2017-10-19
//创建人  ：yangjian
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initInpBox()
{ 
  try
  {                
    fm.all('ManageCom').value = '';
    fm.all('State').value = '';
    fm.all('BranchType').value = '';
    fm.all('BranchType2').value = '';
    fm.all('WageNo').value = ''; 
  }
  catch(ex)
  {
    alert("在UpdateStateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
	try
	  {
	    initInpBox();
	    initUpdateStateGrid();
	  }
	  catch(re)
	  {
	    alert("UpdateStateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	  }
}

function initUpdateStateGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="管理机构";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="管理机构名称";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="薪资月";         //列名
        iArray[3][1]="70px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;        
        
        iArray[4]=new Array();
        iArray[4][0]="计算状态(原)";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="计算状态(改)";         //列名
        iArray[5][1]="60px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="展业类型";         //列名
        iArray[6][1]="70px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
      	iArray[7]=new Array();
        iArray[7][0]="展业类型名称";         //列名
        iArray[7][1]="70px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许 */
        
        iArray[8]=new Array();
        iArray[8][0]="销售渠道";         //列名
        iArray[8][1]="70px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="销售渠道名称";         //列名
        iArray[9][1]="70px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许 */
        UpdateStateGrid = new MulLineEnter( "fm" , "UpdateStateGrid" ); 
        //这些属性必须在loadMulLine前
        UpdateStateGrid.mulLineCount = 10;   
        UpdateStateGrid.displayTitle = 1;
        UpdateStateGrid.hiddenPlus = 1;
        UpdateStateGrid.hiddenSubtraction = 1;
        UpdateStateGrid.locked=0;
        UpdateStateGrid.canSel=0;
        UpdateStateGrid.canChk=1;
        UpdateStateGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化UpdateStateGrid时出错："+ ex);
      }
    }
</script>