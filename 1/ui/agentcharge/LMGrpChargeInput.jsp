<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-11-05 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
 <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LMGrpChargeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LMGrpChargeInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMGrpChargeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMGrpCharge1);">
    		</td>
    		 <td class= titleImg>
        		 集体手续费信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLMGrpCharge1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            集体保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpPolNo OnChange="return checkGrpPolNo()">
          </TD>
          <!--TD  class= title>
            对公帐号
          </TD>
          <TD  class= input>
            <Input class= common name=ChargeAccount >
          </TD-->
        </TR>
        <TR  class= common>
          <TD  class= title>
            起始交费次数
          </TD>
          <TD  class= input>
            <Input class= common name=PayCountFrom OnChange="return checkFrom()">
          </TD>
          <TD  class= title>
            终止交费次数
          </TD>
          <TD  class= input>
            <Input class= common name=PayCountTo OnChange="return checkTo()">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            手续费类型
          </TD>
          <TD  class= input>
            <Input class=codeno name=ChargeType ondblclick="return showCodeList('grpcharge',[this,ChargeTypeName],[0,1]);" onkeyup="return showCodeListKey('grpcharge',[this,ChargeTypeName],[0,1]);" 
            ><Input class= codename name=ChargeTypeName >
            <!--<Input class= common name=ChargeType >-->
          </TD>
          <TD  class= title>
            手续费比率
          </TD>
          <TD  class= input>
            <Input class= common name=ChargeRate OnChange="return checkRate()">
          </TD>
        </TR>
        <!--TR  class= common>
          <TD  class= title>
            参数1
          </TD>
          <TD  class= input>
            <Input class= common name=Parm1 >
          </TD>
          <TD  class= title>
            参数2
          </TD>
          <TD  class= input>
            <Input class= common name=Parm2 >
          </TD>
        </TR-->
        <TR  class= common>
          <TD  class= title>
            备注
          </TD>
          <TD  class= input>
            <Input class= common name=Noti >
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
