<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LACommChargeSave.jsp
//程序功能：
//创建人  ：销售管理
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentcharge.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
  LARateChargeBSchema bLARateChargeBSchema = new LARateChargeBSchema();
  LARateChargeUI tLARateChargeUI = new LARateChargeUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate = tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
  
  tLARateChargeSchema.setAgentCom(request.getParameter("AgentCom"));
  tLARateChargeSchema.setRiskCode(request.getParameter("RiskCode"));
  tLARateChargeSchema.setStartDate(request.getParameter("StartDate"));
  tLARateChargeSchema.setEndDate(request.getParameter("EndDate"));
  tLARateChargeSchema.setAppAge(request.getParameter("AppAge"));
  tLARateChargeSchema.setYears(request.getParameter("Years"));
  tLARateChargeSchema.setPayIntv(request.getParameter("PayIntv"));
  tLARateChargeSchema.setRate(request.getParameter("Rate"));
  tLARateChargeSchema.setF01(request.getParameter("CalType"));
  tLARateChargeSchema.setF02(request.getParameter("PolType"));
  tLARateChargeSchema.setOperator(request.getParameter("Operator"));
  
  bLARateChargeBSchema.setAgentCom(request.getParameter("AgentComB"));
  bLARateChargeBSchema.setRiskCode(request.getParameter("RiskCodeB"));
  bLARateChargeBSchema.setStartDate(request.getParameter("StartDateB"));
  bLARateChargeBSchema.setEndDate(request.getParameter("EndDateB"));
  bLARateChargeBSchema.setAppAge(request.getParameter("AppAgeB"));
  bLARateChargeBSchema.setYears(request.getParameter("YearsB"));
  bLARateChargeBSchema.setPayIntv(request.getParameter("PayIntvB"));
  bLARateChargeBSchema.setRate(request.getParameter("RateB"));
  bLARateChargeBSchema.setF01(request.getParameter("CalTypeB"));
  bLARateChargeBSchema.setF02(request.getParameter("PolTypeB"));
  bLARateChargeBSchema.setOperator(request.getParameter("OperatorB"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLARateChargeSchema);
  tVData.addElement(bLARateChargeBSchema);
  tVData.add(tG);
  try
  {
    tLARateChargeUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLARateChargeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.Operator.value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

