//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

var querySQL;
var qyeryFlag = false;

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
    //首先检验录入框
  if(!verifyInput()) return false;
  var yearWhere = " and 1=1 ";
  var yearAndMonth = fm.all('WageNo').value;
  if(""!=yearAndMonth&&null!=yearAndMonth)
  {
	  if(yearAndMonth.length!=6)
	  {
		  alert("统计年份输入有误，请重新输入");
		  return false;
	  }
	  var year =yearAndMonth.substr(0,4);
	  var month = yearAndMonth.substr(4,6);
	  if(parseInt(year,10)<2000)
	  {
		  alert("统计年份输入有误，请重新输入");
		  return false;
	  }
	  if(parseInt(month,10)<1||parseInt(month,10)>12 )
	  {
		  alert("统计年份输入有误，请重新输入");
		  return false;
	  }
	  yearWhere+= "  and year(a.tmakedate)	='"+year+"' and month(a.tmakedate)	='"+month+"'";
  }
  			querySQL= "select a1,m.managecom,m.agentcom,b1,c1,c2,c3,c4,c5,c6,c7,c8,(c1+c2+c3+c4+c5+c6+c7-c8)  from (select (year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)) a1," 
  	  			+"a.managecom,a.agentcom,(select name from lacom b where  b.agentcom = a.agentcom) b1, "
  	  			//首期手续费
  	  			+"coalesce((select sum(firconcharge) from lachargefee b where b.commisionsn in (select commisionsn from lacharge c where c.managecom = a.managecom  and c.agentcom = a.agentcom  and c.chargestate<>'X'"
  		  		+"  and year(a.tmakedate) || substr('0' || month(a.tmakedate), length('0' || month(a.tmakedate)) - 1, 2) = year(c.tmakedate) ||substr('0' || month(c.tmakedate),length('0' || month(c.tmakedate)) - 1, 2)) "
  		  		+"  and exists (select 'x' from lacommision where commisionsn = b.commisionsn and payyear = 0 and ReNewCount >= 0)),0) c1, "
  	  			//续期手续费
  		  	    +"coalesce((select sum(firconcharge) from lachargefee b where b.commisionsn in (select commisionsn from lacharge c where c.managecom = a.managecom  and c.agentcom = a.agentcom  and c.chargestate<>'X'"
		  		+"  and year(a.tmakedate) || substr('0' || month(a.tmakedate), length('0' || month(a.tmakedate)) - 1, 2) = year(c.tmakedate) ||substr('0' || month(c.tmakedate),length('0' || month(c.tmakedate)) - 1, 2)) "
		  		+"  and exists (select 'x' from lacommision where commisionsn = b.commisionsn and payyear > 0 and ReNewCount = 0)),0) c2, "
		  		//继续奖加款
		  		+"  coalesce((select sum(money) from LARewardPunish  where managecom = a.managecom  and doneflag = '14' and agentcom = a.agentcom and wageno = year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)),0) c3, "
  		  		//月度奖加款
		  		+"  coalesce((select sum(money) from LARewardPunish  where managecom = a.managecom  and doneflag = '11' and agentcom = a.agentcom and wageno = year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)),0) c4, "
		  		//季度奖加款
		  		+" coalesce((select sum(money) from LARewardPunish  where managecom = a.managecom  and doneflag = '12' and agentcom = a.agentcom and wageno = year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)),0) c5, "
		  		//年度奖加款
		  		+"  coalesce((select sum(money) from LARewardPunish  where managecom = a.managecom  and doneflag = '13' and agentcom = a.agentcom and wageno = year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)),0) c6, "
		  		//其他加款
		  		+"  coalesce((select sum(money) from LARewardPunish  where managecom = a.managecom  and doneflag = '15' and agentcom = a.agentcom and wageno = year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)),0) c7, "
		  		//其他扣款
		  		+"  coalesce((select sum(money) from LARewardPunish  where managecom = a.managecom  and doneflag = '22' and agentcom = a.agentcom and wageno = year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)),0) c8 "
		     	+" from LACharge a where a.chargetype='15' and a.chargestate<>'X'  "
  	             + getWherePart('a.ManageCom', 'ManageCom','like')
  	          	 + getWherePart('a.BranchType', 'BranchType')
  	          	 + getWherePart('a.BranchType2', 'BranchType2')
  	          	 + getWherePart('a.AgentCom', 'AgentCom')
  	          	 ;
  				  
  	  			querySQL+= yearWhere+"  group  by a.ManageCom,a.AgentCom, (year(a.tmakedate) || substr('0' || month(a.tmakedate),length('0' || month(a.tmakedate)) - 1,2)))m "        	 
  	           ;   
  			
	turnPage.queryModal(querySQL, LACommisionGrid);   
	qyeryFlag  = true;
	
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("财务结算起期必须小于等于财务结算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#1# and BranchType2 in (#02#,#04#) and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
//页面下载
function doDownLoad(){
    
	if(!verifyInput()) return false;   
	if(false==qyeryFlag)
	{
		alert("请先查询！");
		return false;
	}
    fm.action = "./LAARiskChargePayReport.jsp";
    fm.all("querySQL").value = querySQL;
    fm.submit();
}


