<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChAudFewSave.jsp
//程序功能：中介手续费  特殊业务结算
//创建日期：2005-11-29 10:00
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentcharge.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String tChargeType = request.getParameter("ChargeType");
	String tChargeType2 = "FEW";
	LACommisionSet tLACommisionSet = new LACommisionSet();
	ChAudFewUI tChAudFewUI = new ChAudFewUI();

  //输出参数
  CErrors tError = null;

  String tOperate="";

  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //被选中的特殊业务
  int lineCount = 0;
  String tChk[] = request.getParameterValues("InpAgentQueryGridChk");
  String tGrpContNo[] = request.getParameterValues("AgentQueryGrid1");
  //String tCommisionSn[] = request.getParameterValues("AgentQueryGrid10");
  
  lineCount = tChk.length; //行数
  System.out.println("length= "+String.valueOf(lineCount));
  for(int i=0;i<lineCount;i++)
  {
    LACommisionSchema tLACommisionSchema = new LACommisionSchema();
    if(tChk[i].trim().equals("1"))
    {
      tLACommisionSchema.setGrpContNo(tGrpContNo[i]);
      System.out.println("GrpContNo:"+tGrpContNo[i]);
    }
    tLACommisionSet.add(tLACommisionSchema);
    System.out.println("i:"+tChk[i]);
  }

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tChargeType);
	tVData.add(tChargeType2);
	tVData.add(tG);
	tVData.add(tLACommisionSet);
	System.out.println("数据准备完毕，开始后台操作.");
  try
  {
    tChAudFewUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tChAudFewUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>