<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：ChAudRuleInput.jsp
//程序功能：
//创建日期：2005-12-5 16:18
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>
<script> 
   var BranchType=<%=BranchType%>;
   var BranchType2=<%=BranchType2%>;
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ChAudRuleInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ChAudRuleInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form method=post name=fm target="fraSubmit" action="./ChAudRuleSave.jsp">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		务查询
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            集体保单合同号
          </TD>
          <TD  class= input>
            <Input class="common" name=GrpContNo >
          </TD>
	        <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>

            <Input class="codeno" name=ManageCom  verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
            ><Input class=codename name=ManageComName readonly >
          </TD> 
        </TR>
        <TR  class= common>
          <TD  class= title>
            统计起期
          </TD>
          <TD  class= input>
            <Input name=StartDate class='coolDatePicker' dateFormat='short'>
          </TD>
	  <TD  class= title>
            统计止期
          </TD>
          <TD  class= input>
            <Input name=EndDate class='coolDatePicker' dateFormat='short' >
          </TD> 
        </TR>
      	<TR  class= common>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom verify="代理机构|code:agentcom"  ondblclick="return getagentcom(this,AgentComName);" onkeyup="return  getagentcom(this,AgentComName);">
          </TD>
          <TD  class= title>
            代理机构名称
          </TD>
          <TD class= input>  
            <Input class='readonly' name=AgentComName readonly > 
          </TD>
        </TR>
        <TR class=input>
         <TD class=common>
          <input type =button class=cssButton value="查 询" onclick="ChAudQuery();">
          <!--input type =button class=cssButton value="计 算" onclick="ChAudSave();"-->
        </TD>
       </TR>
      </table>
    </Div>
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span>
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>
	  <TD class=common>   					
      	     	<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
          </TD>
	  <TD class=common>
      		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
          </TD>
	  <TD class=common>
      		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
          </TD>
	  <TD class=common>
      		<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>
   <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent2);">
    		</td>
    		 <td class= titleImg>
        		手续费计算操作
       		 </td>   		 
    	</tr>
    </table>
    <Div id= "divAgent2" style= "display: ''">
      <Table  class= common>
	      <TR  class= common>
	          <TD  class= title>
	            计算起期
	          </TD>
	          <TD  class= input>
	            <Input name=ReckonStartDate class='coolDatePicker' dateFormat='short' elementtype=nacessary >
	          </TD>
		        <TD class= title>
	            计算止期
	          </TD>
	          <TD  class= input>
	            <Input name=ReckonEndDate class='coolDatePicker' dateFormat='short' elementtype=nacessary >
	          </TD> 
	        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ReckonManageCom verify="管理机构|code:comcode&NOTNULL&len>=4" 
            ondblclick="return showCodeList('comcode',[this,ReckonManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ReckonManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class=codename name=ReckonManageComName readOnly elementtype=nacessary> 
          </TD> 
          <TD  class= title>
            手续费类型
          </TD>
          <TD  class= input>
            <Input class=codeno name=ChargeType verify="手续费类型|NOTNULL"  ondblclick="return showCodeList('ChargeType',[this,ChargeTypeName],[0,1],null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('ChargeType',[this,ChargeTypeName],[0,1],null,mcodeSql,'1',null);" 
            ><Input class=codename elementtype=nacessary name=ChargeTypeName readonly >
          </TD>
        </TR>
      </Table>
      <input type =button class=cssButton value="计 算" onclick="ChAudSave();">
    </Div>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=ManageComCode value='<%=tG.ManageCom%>'>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
  var mcodeSql = "1 and code in (#11#,#12#)";
</script>