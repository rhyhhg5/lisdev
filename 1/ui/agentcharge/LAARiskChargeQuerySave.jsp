<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LAARiskChargePayUI tLAARiskChargePayUI = new LAARiskChargePayUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LAChargeSet tSetU = new LAChargeSet();	//用于更新
	  // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      tVData.add(tGI);
      //创建数据集
      if(tAction.equals("SELECTPAY")||tAction.equals("ALLDELAY")){
      	String tChk[] = request.getParameterValues("InpLACommisionGridChk"); 
      	String tManageCom[] = request.getParameterValues("LACommisionGrid1");
      	String tAgentCom[] = request.getParameterValues("LACommisionGrid3");
      	String tContNo[] = request.getParameterValues("LACommisionGrid5");
      	String tRiskCode[] = request.getParameterValues("LACommisionGrid6");
      	String tTMakeDate[] = request.getParameterValues("LACommisionGrid11");
      	String tReceiptNo[] = request.getParameterValues("LACommisionGrid12");
      	String tTransType[] = request.getParameterValues("LACommisionGrid13");
      	String tCharge[]= request.getParameterValues("LACommisionGrid9");
      	String tCommisionSn[]= request.getParameterValues("LACommisionGrid15");
      	for(int i=0;i<tChk.length;i++){
      		if(tChk[i].equals("1")){
          		//创建一个新的Schema
      	  		LAChargeSchema tSch = new LAChargeSchema();
         		tSch.setBranchType("1");
          		tSch.setBranchType2("02");
          		tSch.setChargeType("15");
          		tSch.setChargeState("0");
          		tSch.setManageCom(tManageCom[i].trim());
          		tSch.setAgentCom(tAgentCom[i].trim());
          		tSch.setContNo(tContNo[i].trim());
          		tSch.setRiskCode(tRiskCode[i].trim());
          		tSch.setTMakeDate(tTMakeDate[i].trim());
          		tSch.setReceiptNo(tReceiptNo[i].trim());
          		tSch.setTransType(tTransType[i].trim());
          		tSch.setCharge(tCharge[i].trim());
          		tSch.setCommisionSN(tCommisionSn[i].trim());
      	  		tSetU.add(tSch);
       		}
      	}
      	tVData.add(tSetU);
        System.out.println("Start tLAARiskChargePayUI Submit...SELECTPAY");
        tLAARiskChargePayUI.submitData(tVData,tAction);
      }else{
      	String tManageCom=request.getParameter("ManageCom");
      	String tAgentCom=request.getParameter("AgentCom");
      	String tStartDate=request.getParameter("StartDate");
      	String tEndDate=request.getParameter("EndDate");
      	String tGrpContNo=request.getParameter("GrpContNo");
      	String tCardNo=request.getParameter("CardNo");
      	String tWrapCode=request.getParameter("WrapCode");
      	String tContFlag = request.getParameter("ContFlag");
      	tVData.add(1,tManageCom);
      	tVData.add(2,tAgentCom);
      	tVData.add(3,tStartDate);
      	tVData.add(4,tEndDate);
      	tVData.add(5,tGrpContNo);
      	tVData.add(6,tCardNo);
      	tVData.add(7,tWrapCode);
      	tVData.add(8,tContFlag);
        System.out.println("Start tLAARiskChargePayUI Submit...ALLPAY");
        tLAARiskChargePayUI.submitData(tVData,tAction);
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAARiskChargePayUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
