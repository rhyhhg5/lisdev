<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PoundCalSave.jsp
//程序功能：
//创建日期：2003-7-29
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentcharge.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAComChargeSchema tLAComChargeSchema   = new LAComChargeSchema();

  PoundCalUI tPoundCalUI   = new PoundCalUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("Operate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");


    tLAComChargeSchema.setAgentCom(request.getParameter("AgentCom"));
    tLAComChargeSchema.setStartDate(request.getParameter("BeginDate"));
    tLAComChargeSchema.setStartEnd(request.getParameter("EndDate"));
    tLAComChargeSchema.setBranchType(request.getParameter("BranchType"));
    tLAComChargeSchema.setChargeType(request.getParameter("ChargeType"));
    
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLAComChargeSchema);
	tVData.add(tG);
  try
  {
    tPoundCalUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tPoundCalUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

