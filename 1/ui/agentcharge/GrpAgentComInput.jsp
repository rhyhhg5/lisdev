<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：GrpAgentComInput.jsp
//程序功能：
//创建日期：2004-05-24
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="GrpAgentComInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpAgentComInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form method=post name=fm target="fraSubmit" action="./GrpAgentComSave.jsp">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		代理机构设定
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            集体保单号码
          </TD>
          <TD  class= input>
            <Input class="common" name=GrpPolNo >
          </TD>
	  	  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);" >
          </TD> 
        </TR>

        <TR class=input>              
         <TD class=common>
          <input type =button class=common value="查询" onclick="GrpAgentComQuery();">    
        </TD>
        <TD class=common>
          <input type =button class=common value="修改" onclick="GrpAgentComSave();">    
        </TD>
       </TR>          
      </table>
    </Div>  
    <!--input type=hidden name=BranchType value=""-->
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  			</TD>
      </TR>
    </Table>	
    <Table>
      <TR  class= common>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom  ondblclick="return showCodeList('AgentCom',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('AgentCom',[this],null,null,null,null,1);">  
          </TD>
        </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>	     
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>