  <%
//程序名称：ChFinInit.jsp
//程序功能：
//创建日期：2004-03-11
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {    
    fm.all('AgentCom').value = '';            
    fm.all('ChargeType').value = '';
    fm.all('StartDate').value = '';            
    fm.all('EndDate').value = '';     
  }
  catch(ex)
  {
    alert("在ChFinInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var AgentQueryGrid ;
 
// 保单信息列表的初始化
function initAgentQueryGrid()
  {                               
    var iArray = new Array();
    try
	{
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            		//列最大值
	      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	 
	      iArray[1]=new Array();
	      iArray[1][0]="代理机构";         	//列名
	      iArray[1][1]="100px";              	//列宽
	      iArray[1][2]=200;            	        //列最大值
	      iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
	      iArray[2]=new Array();
	      iArray[2][0]="手续费类型";         		//列名
	      iArray[2][1]="100px";            		//列宽
	      iArray[2][2]=200;            	        //列最大值
	      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许		

	      iArray[3]=new Array();
	      iArray[3][0]="统计日期";         		//列名
	      iArray[3][1]="100px";            		//列宽
	      iArray[3][2]=200;            	        //列最大值
	      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许		
		
	      iArray[4]=new Array();
	      iArray[4][0]="集体保单号码";         		//列名
	      iArray[4][1]="150px";            		//列宽
	      iArray[4][2]=200;            	        //列最大值
	      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[5]=new Array();
	      iArray[5][0]="保单号码";         		//列名
	      iArray[5][1]="150px";            		//列宽
	      iArray[5][2]=200;            	        //列最大值
	      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	      iArray[6]=new Array();
	      iArray[6][0]="管理机构";         		//列名
	      iArray[6][1]="100px";            		//列宽
	      iArray[6][2]=200;            	        //列最大值
	      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
	      iArray[7]=new Array();
	      iArray[7][0]="险种编码";         		//列名
	      iArray[7][1]="100px";            		//列宽
	      iArray[7][2]=200;            	        //列最大值
	      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[8]=new Array();
	      iArray[8][0]="交易号";         		//列名
	      iArray[8][1]="100px";            		//列宽
	      iArray[8][2]=200;            	        //列最大值
	      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

	      iArray[9]=new Array();
	      iArray[9][0]="交易类别";         		//列名
	      iArray[9][1]="100px";            		//列宽
	      iArray[9][2]=200;            	        //列最大值
	      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[10]=new Array();
	      iArray[10][0]="第几次交费";         		//列名
	      iArray[10][1]="100px";            		//列宽
	      iArray[10][2]=200;            	        //列最大值
	      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	      iArray[11]=new Array();
	      iArray[11][0]="交易金额";         		//列名
	      iArray[11][1]="100px";            		//列宽
	      iArray[11][2]=200;            	        //列最大值
	      iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许        
	      
	      iArray[12]=new Array();
	      iArray[12][0]="手续费比例";         		//列名
	      iArray[12][1]="100px";            		//列宽
	      iArray[12][2]=200;            	        //列最大值
	      iArray[12][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	      iArray[13]=new Array();
	      iArray[13][0]="手续费金额";         		//列名
	      iArray[13][1]="100px";            		//列宽
	      iArray[13][2]=200;            	        //列最大值
	      iArray[13][3]=0;                   	//是否允许输入,1表示允许，0表示不允许             

      AgentQueryGrid = new MulLineEnter( "fm" , "AgentQueryGrid" ); 
      //这些属性必须在loadMulLine前
      AgentQueryGrid.mulLineCount = 0;   
      AgentQueryGrid.displayTitle = 1;
      AgentQueryGrid.hiddenPlus = 1;
      AgentQueryGrid.hiddenSubtraction = 1;
      AgentQueryGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
 }
 
function initForm()
{
  try
  {
    initInpBox();
    initAgentQueryGrid();
    initPolGrid();
  }
  catch(re)
  {
    alert("ChFinInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var initPolGrid ;
// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="机构代码";         		//列名
      iArray[1][1]="160px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="机构名称";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="手续费类型";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="应发金额";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="实发金额";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0; 									//是否允许输入,1表示允许，0表示不允许
        
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
