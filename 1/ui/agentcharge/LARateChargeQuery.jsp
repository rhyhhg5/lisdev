<%
//程序名称：LARateChargeQuery.jsp
//程序功能：
//创建日期：2003-10-28 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LARateChargeQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LARateChargeQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>代理机构手续费信息 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPlan1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 代理机构</td>
        <td  class= input> <Input class="code" name=AgentCom  ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);">  </td>        
        <td  class= title> 险种	</td>
        <td  class= input> <input name=RiskCode class="code" verify="险种|code:Riskcode" 
		         ondblclick="return showCodeList('riskbank',[this]);" onkeyup="return showCodeListKey('riskbank',[this]);"> </td>
      </tr>
      <TR>
          <TD  class= title width="25%">起始日期</TD>
          <TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="起始日期|NOTNULL"> </TD>      
          <TD  class= title width="25%">终止日期</TD>
          <TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=EndDate  verify="结束日期|NOTNULL"></TD>  
      </TR> 
      <tr  class= common>  
        <td  class= title>投保年龄</td>
        <td  class= input> <input class=common name=AppAge verify="投保年龄|notnull"> </td>
        <td  class= title>保险年期</td>
        <td  class= input><input class=common name=Years verify="保险年期|notnull"> 	</td>
      </tr>
      <tr  class= common> 
        <td  class= title>交费间隔</td>
        <td  class= input><input class=common name=PayIntv verify="交费间隔|notnull"> 	</td>
        <td  class= title>手续费比率</td>
        <td  class= input><input name=Rate class= common verify = "手续费比率|notnull"> </td>
      </tr>
      <tr  class= common> 
        <td  class= title>计算类型</td>
        <td  class= input> <input name=CalType class="code" verify="计算类型|notnull" 
		         CodeData="0|^51|机构代理手续费^52|机构劳务手续费^53|机构业务手续费^54|机构节余手续费" ondblclick="showCodeListEx('Cal',[this],[0]);"  onkeyup="showCodeListKeyEx('Cal',[this],[0]);"> </td>
        <td  class= title>保单类型</td>
        <td  class= input><input class='code' name=PolType verify="计划类型|notnull"
		         CodeData="0|^0|优惠业务^1|正常业务" 
		         ondblclick="showCodeListEx('PolType',[this],[0]);"  onkeyup="showCodeListKeyEx('PolType',[this],[0]);"</td>
      </tr>
      <tr  class= common>         
        <td  class= title> 操作员代码</td>
        <td  class= input> <input name=Operator class= common > </td>		    
      </tr>
      </table>
          <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPlanGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPlanGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPlanGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 				
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
