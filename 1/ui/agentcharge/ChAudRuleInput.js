var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//得到中介机构编码
function getagentcom(cObj,AgentComName)
{
	var sql=" 1 and actype in (#02#,#03#,#04#) ";
	showCodeList('AgentCom',[cObj,AgentComName],[0,1],null,sql, "1",1);
}

/***************************************************************
 * 功能说明：单击查询按钮触发的事件
 * 传入参数：无
 * 返 回 值：无
 ***************************************************************/
function ChAudQuery()
{
 	var tSQL = "";
 	
 	tSQL  = "select a.GrpContNo,a.ManageCom,d.Name,a.AgentCom,b.Name,a.CalDate,a.AgentCode,c.Name,";
 	tSQL += "    case";
 	tSQL += "      when (select Count(aa.ChargeState) from lacharge aa where aa.GrpContNo = a.GrpContNo) > 0 then '已结算'";
 	//tSQL += "      when (select Max(aa.ChargeState) from lacharge aa where aa.GrpContNo = a.GrpContNo) = '0' then '已经结算'";
 	//tSQL += "      when (select Max(aa.ChargeState) from lacharge aa where aa.GrpContNo = a.GrpContNo) = '1' then '提取审核'";
 	//tSQL += "      when (select Max(aa.ChargeState) from lacharge aa where aa.GrpContNo = a.GrpContNo) = '2' then '财务发放'";
 	tSQL += "      else '未结算'";
 	tSQL += "    end as aaa,";
 	tSQL += "    case";
 	tSQL += "      when (select Count(bb.CommisionSN) from LACommision bb where bb.GrpContNo = a.GrpContNo and P7 = 1) > 0 then '保全未结算'";
 	tSQL += "      else '未作新保全'";
 	tSQL += "    end as bbb";
 	tSQL += "  from LACommision a,LACom b,LAAgent c,lDcom d";
 	tSQL += " where a.AgentCom = b.AgentCom";
 	tSQL += "   and a.AgentCode = c.AgentCode";
 	tSQL += "   and a.ManageCom = d.ComCode";
 	tSQL += "   and a.BranchType = '2'";
 	tSQL += "   and a.BranchType2 = '02'";
 	tSQL += "   and a.ManageCom like '"+document.fm.ManageComCode.value+"%25'";
 	tSQL += getWherePart('a.GrpContNo','GrpContNo','like');
 	tSQL += getWherePart('a.ManageCom','ManageCom','like');
 	if(document.fm.StartDate.value != "" && document.fm.StartDate.value != null)
 	{
 		tSQL += " and a.CalDate >='"+document.fm.StartDate.value+"'";
 	}
 	if(document.fm.EndDate.value != "" && document.fm.EndDate.value != null)
 	{
 		tSQL += " and a.CalDate <='"+document.fm.EndDate.value+"'";
 	}
 	tSQL += getWherePart('a.AgentCom','AgentCom','like');
 	tSQL += " group by a.GrpContNo,a.ManageCom,d.Name,a.AgentCom,b.Name,a.CalDate,a.AgentCode,c.Name"
 	tSQL += " order by a.CalDate desc,a.GrpContNo";

 	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(tSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {  
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量	
  turnPage.pageDisplayGrid = AgentQueryGrid;
  //保存SQL语句
  turnPage.strQuerySql     = tSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

/***************************************************************
 * 功能说明：点击计算按钮触发的事件
 * 传入参数：无
 * 返 回 值：无
 ***************************************************************/
function ChAudSave()
{
	//对非空进行验证
 	if( verifyInput() == false ) return false;

 	var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.submit(); //提交
}

/***************************************************
 * 功能说明：提交后操作,服务器数据返回后执行的操作
 * 传入参数：FlagStr    后台处理成功标记
 *           content    提示内容
 * 返 回 值：无
 ***************************************************/
function afterSubmit( FlagStr, content )
{
 	showInfo.close();
 	
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
    initForm();
  }
}