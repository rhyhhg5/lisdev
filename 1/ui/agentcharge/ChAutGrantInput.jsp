<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：ChAutGrantInput.jsp
//程序功能：
//创建日期：2005-12-7 9:48
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>
<script> 
   var BranchType=<%=BranchType%>;
   var BranchType2=<%=BranchType2%>;
   var msql="1 ";
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>    
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="ChAutGrantInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ChAutGrantInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >    
  <form method=post name=fm target="fraSubmit" action="./ChAutGrantSave.jsp">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		手续费提取
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            中介机构编码
          </TD>
          <TD class= input>
            <Input class="code" name=AgentCom 
            ondblclick="return getagentcom(this,AgentComName);"
            onkeyup="return  getagentcom(this,AgentComName);">
          </TD>
          <TD  class= title>
            中介机构名称
          </TD>
          <TD class= input>
            <Input class="readonly" name=AgentComName readonly > 
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            所属管理机构
          </TD>
          <TD class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>=4" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
          <td  class= title> 中介机构类型 </td>  
	        <td  class= code> <input class="codeno" name="ACType" 
	        	   ondblclick="return showCodeList('ACType',[this,ACTypeName],[0,1]);"
	        	   onkeyup="return showCodeListKey('ACType',[this,ACTypeName],[0,1]);"
	        	   ><Input class=codename name=ACTypeName readOnly >
	        </td>
        </TR>
        <TR  class= common>
          <TD  class= title>
            统计起期
          </TD>
          <TD  class= input>
            <Input name=StartDate class='coolDatePicker' verify="统计起期|NOTNULL"  dateFormat='short' elementtype=nacessary>
          </TD>
	        <TD  class= title>
            统计止期
          </TD>
          <TD  class= input>
            <Input name=EndDate class='coolDatePicker' verify="统计止期|NOTNULL"  dateFormat='short' elementtype=nacessary>
          </TD> 
        </TR>
        <TR class= common>
          <TD  class= title>
            手续费类型
          </TD>
          <TD  class= input>
            <Input class=codeno name=ChargeType verify="手续费类型|NOTNULL"
                   ondblclick="return showCodeList('ChargeType',[this,ChargeTypeName],[0,1]);"
                   onkeyup="return showCodeListKey('ChargeType',[this,ChargeTypeName],[0,1]);" 
            ><Input class=codename  name=ChargeTypeName readonly elementtype=nacessary>
          </TD>
        </Tr>
      </table>
      <input type =button class=button value="查  询" onclick="ChDstQuery();">    
      <input type =button class=button value="审核发放" onclick="ChDstSave();">
    </Div>  
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
     	<INPUT CLASS=button VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    </TD>
	  <TD class=common>
    	<INPUT CLASS=button VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
    </TD>
	  <TD class=common>					
    	<INPUT CLASS=button VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    </TD>
	  <TD class=common>
    	<INPUT CLASS=button VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </TD>
	</TR>
	<input type=hidden name=Operate value=''>   
  </Table>
  </Div>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var mcodeSql = "1 and code in (#11#,#12#)";
</script>