<%
//程序名称：ContQueryChargeInit.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('BranchType').value = mBranchType;
    fm.all('BranchType2').value = mBranchType2;
  }
  catch(ex)
  {
    alert("ContQueryChargeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
   // alert(121);
    initContChargeQueryGrid(); 
   
  }
  catch(re)
  {
    alert("ContQueryChargeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var ContChargeQueryGrid;
function initContChargeQueryGrid() {  
//alert(12);                            
  var iArray = new Array();
//  alert(12);
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";         		//列名
    iArray[1][1]="60px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         		//列名
    iArray[2][1]="80px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="中介机构编码";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    iArray[4]=new Array();
    iArray[4][0]="中介机构名称";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="手续费金额";         		//列名
    iArray[5][1]="60px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[6]=new Array();
    iArray[6][0]="批次号";         		//列名
    iArray[6][1]="100px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="支付状态";         		//列名
    iArray[7][1]="100px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=3;         		//是否允许录入，0--不能，1--允许
    
    
   ContChargeQueryGrid = new MulLineEnter( "fm" , "ContChargeQueryGrid" ); 
    //这些属性必须在loadMulLine前
 
    ContChargeQueryGrid.mulLineCount = 0;   
    ContChargeQueryGrid.displayTitle = 1;
    ContChargeQueryGrid.hiddenPlus = 1;
    ContChargeQueryGrid.hiddenSubtraction = 1;
    ContChargeQueryGrid.canSel = 1;
    ContChargeQueryGrid.canChk = 0;
  //  ContChargeQueryGrid.selBoxEventFuncName = "showOne";
  
    ContChargeQueryGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ContChargeQueryGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
