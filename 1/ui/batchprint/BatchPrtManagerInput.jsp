<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：NBBPrintInput.jsp.
//程序功能：批量打印
//创建日期：2005-5-3
//创建人  ：HWM
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<title>批量打印</title>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="BatchPrtManagerInput.js"></SCRIPT>
  <%@include file="BatchPrtManagerInit.jsp"%>
</head>
<body onload="initForm()">
<form method=post name=fm target="fraSubmit"  action="./NBBPrintSaveWH.jsp"> 
	<table class= common border=0 width=100%>
		<tr>
			<td class= titleImg align= center>&nbsp;运营单证综合打印：</td>
		</tr>
	</table>
	<Div id="divNBBP" style="display: ''">
	<table  class= common align=center>
		<TR  class= common>
		   <INPUT VALUE="当前打印任务查询" TYPE=button class=cssButton onclick="easyQueryClick();">
		   <INPUT VALUE="&nbsp;生成新打印任务&nbsp;" class= cssButton TYPE=button onclick="NewPrtTask()">
		   <INPUT VALUE="&nbsp;&nbsp;&nbsp;&nbsp;打&nbsp;&nbsp;&nbsp;&nbsp;印&nbsp;&nbsp;&nbsp;&nbsp;" class= cssButton TYPE=button onclick="BatchPrintWH()">
		</TR>
		<tr class=common>
			<td class= title>  &nbsp;&nbsp;&nbsp;管理机构</td>
			<td class=input> <Input class=codeno name=ManageCom ondblclick="showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="showCodeListKey('station',[this,ManageComName],[0,1]);"><input class= codename  name=ManageComName readonly=true></td>
			<td class=title> </td>
			<td class=input><Input type=hidden ></td>
			<td class=title> </td>
			<td class=input><Input type=hidden ></td>
		</tr>
	</table>
  <BR>
	<TD  class= title>
		<input type=hidden id="intRecordNo" name="intRecordNo">
		<input type=hidden id="ArrPrtSeq"  name="ArrPrtSeq">
		<input type=hidden id="fmtransact" name="fmtransact">
	</TD>

	    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTask);">
    		</td>
    		<td class= titleImg>
    			 打印任务信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divTask" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanTaskGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=cssButton VALUE="首&nbsp;&nbsp;页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾&nbsp;&nbsp;页" TYPE=button onclick="turnPage.lastPage();">
    </Div>
    <HR>
    <Div id= "PrtListChnl" style= "display: '' ">
      <INPUT CLASS=cssButton VALUE="明细查询" TYPE=button onclick="ListQuery();"> 
      <INPUT CLASS=cssButton VALUE="撤销打印" TYPE=button onclick="CancelPrt();"> 					
      <INPUT CLASS=cssButton VALUE="打印设置" TYPE=button onclick="PrintSet();"> 
    </Div>
    <Div  id= "PrtTypeName" style= "display: 'none'">
				<BR>
          <!--INPUT CLASS=cssButton VALUE="&nbsp;&nbsp;&nbsp;批单批次号明细查询&nbsp;&nbsp;&nbsp;" TYPE=button onclick="PrnListQuery();"--> 
          <!--INPUT CLASS=cssButton VALUE="客户通知书批次号明细查询" TYPE=button onclick="ClientNoticeListQuery();"--> 					
          <!--INPUT CLASS=cssButton VALUE="收费通知书批次号明细查询" TYPE=button onclick="FeeNoticeListQuery();"--> 
          <!--INPUT CLASS=cssButton VALUE="&nbsp;续期收据批次号明细查询&nbsp;" TYPE=button onclick="BillPrnListQuery();"--> 					
          <!--INPUT CLASS=cssButton VALUE="&nbsp;&nbsp;&nbsp;失效通知书明细查询&nbsp;&nbsp;&nbsp;" TYPE=button onclick="DisableNoticeListQuery();"-->
          <INPUT CLASS=cssButton VALUE="退出查询" TYPE=button onclick="CancelListQuery();">                      
				<BR>
				<BR>
		    <table>
        	<tr>
            	<td class=common>
		    	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTask1);">
        		</td>
        		<td class= titleImg>
        			 明细查询信息
        		</td>
        	</tr>
        </table>
        
  	    <div  id= "divList" style= "display: ''">
          	<table  class= common>
              	<tr  class= common>
          	  		<td text-align: left colSpan=1>
  	    				<span id="spanListGrid" >
  	    				</span> 
  	    			</td>
  	    		</tr>
  	    	</table>
  	    </div>
        <Div id= "divPage1" align=center style= "display: '' ">
          <INPUT CLASS=cssButton VALUE="首&nbsp;&nbsp;页" TYPE=button onclick="turnPage1.firstPage();"> 
          <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
          <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
          <INPUT CLASS=cssButton VALUE="尾&nbsp;&nbsp;页" TYPE=button onclick="turnPage1.lastPage();">
        </Div>

  	    <HR>
		</Div>
  <Div id="divNBBP" style="display: 'none'">
    <tr class=common>
      <td class= title>  SQL </td>
			<td class=input><Input class=common name=strSql ></td>
    </tr>
  </Div>
   <Div id="divNBBP1" style="display: 'none'">
    <TR  class= common>
      <TD  class= title>
        请输入要保存文件的路径：
      </TD>
      <TD  class= input style= "width: '40%'">
        <Input class= common5  name=RealPath style= "width: '95%'"> 
      </TD>           
    </TR>
   </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
