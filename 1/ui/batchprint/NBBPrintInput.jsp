<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：NBBPrintInput.jsp.
//程序功能：批量打印
//创建日期：2005-5-3
//创建人  ：HWM
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<title>批量打印</title>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="NBBPrintInput.js"></SCRIPT>
  <%@include file="NBBPrintInit.jsp"%>
</head>
<body onload="initForm()">
<form method=post name=fm target="fraSubmit"  action="./NBBPrintSave.jsp"> 
	<table class= common border=0 width=100%>
		<tr>
			<td class= titleImg align= center>&nbsp;请输入查询条件：</td>
		</tr>
	</table>
	<Div id="divNBBP" style="display: ''">
	<table  class= common align=center>
		<TR  class= common>
			<TD  class= title>  印刷号码   </TD>
			<TD  class= input> <Input NAME=PrtNo class=common> </TD>
			<TD  class= title>  合同号  </TD>
			<TD  class= input>  <Input NAME=ContNo class=common>  </TD>
			<TD  class= title>  投保单号码   </TD>
			<TD  class= input>  <Input NAME=PolNo class=common>  </TD>
		</TR>
		<TR  class= common>  
			<TD  class= title>  暂收收据号  </TD>
			<TD  class= input>  <Input NAME=TempFeeNo class=common>  </TD>
			<TD  class= title>  起始日期  </TD>
			<TD  class= input> <Input class="coolDatePicker" dateFormat="short" name=StartDate> </TD>
			<TD  class= title> 截止日期 </TD>
			<TD  class= input> <Input class="coolDatePicker" dateFormat="short" name=EndDate></TD>
		</TR>
		<TR class = common>
      <TD class=title>部门选择</TD>
			<TD class=input><input class =codeno name=Department  ondblClick="showCodeList('Department',[this,ChnlType],[0,1]);"
				   onkeyup="showCodeListKey('Department',[this,ChnlType],[0,1]);"><input class = codename name=ChnlType readonly=true></TD>
		  <TD class = title></TD>
		  <TD class = title></TD>
		  <TD class = title></TD>
		  <TD class = title></TD>
		</TR>
		<tr class=common>
			<TD  class= title>  单证类型 </TD>
			<td class=input> <input class=codeno name=SaleChnl ondblClick="showCodeList('cardprinttype',[this,ChannelType],[0,1]);"
            onkeyup="showCodeListKey('cardprinttype',[this,ChannelType],[0,1]);"><input class=codename name=ChannelType></td>
			<td class= title>  打印类型  </td>
			<td class=input> <input class=codeno name=PrtTypeName  verify="打印类型|code:cardtype" ondblClick="showCodeList('cardtype',[this,PrtType],[0,1]);"
            onkeyup="showCodeListKey('cardtype',[this,PrtType],[0,1]);"><input class=codename name=PrtType readonly=true></td>
			<td class= title>  单证名称</td>
			<td class=input> <input class=codeno name=BillName  ondblclick="showCodeList('chnlcardname',[this,PrtName],[0,1],null,getDepartment(),'department',1);" onkeyup="return showCodeListKey('chnlcardname' ,[this,PrtName],[0,1],null,getDepartment(),'department');"><input class= codename  name=PrtName readonly=true></td>
		</tr>
		<tr class=common>
			<td class= title>  管理机构</td>
			<td class=input> <Input class="code" name=ManageCom ondblclick="showCodeList('station',[this]);" onkeyup="showCodeListKey('station',[this]);"></td>
			<td class=title>  代理人编号</td>
			<td class=input> <Input class="code" name=AppntName  ondblclick="return showCodeList('Agentcode', [this]);" onkeyup="return showCodeListKey('Agentcode', [this]);"><input type=hidden name=AgentCode><Input type=hidden name=AgentGroup ></td>
			<td class=title>  打印份数</td>
			<td class=input> <input class=common name=BPrtNum value=50><Input type=hidden name="BranchGroup"></td>
		</tr>
	</table>

			<Div  id= "PrtTypeName2" style= "display: 'none'">
				<HR>
				<TABLE class=common align=center width=50%>
					<TR  class= common8>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >缴费催办通知书</input></TD>
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >撤销申请通知书</input></TD>
					<TD class= title8><input type="checkbox"  name="GrpDetailPrt" >催办通知书</input></TD>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >个险填单问题通知</input></TD>
					</TR>
					<TR  class= common8>		
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >个险体检通知</input></TD>
					<TD class= title8><input type="checkbox"  name="GrpDetailPrt" >个险契调通知</input></TD>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >个险残疾问卷</input></TD>
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >个险财务问卷</input></TD>
					</TR>
          <TR  class= common8>
					<TD class= title8><input type="checkbox"  name="GrpDetailPrt" >个险疾病问卷</input></TD>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >个险缴费通知书</input></TD>
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >个险承保计划变更回执</input></TD>
					<TD class= title8><input type="checkbox"  name="GrpDetailPrt" >合同送达回执</input></TD>
					</TR>				
				</table>
				<HR>
			</div>
			<Div  id= "PrtTypeName5" style= "display: 'none'">
				<HR>
				<TABLE class=common align=center width=50%>
					<TR  class= common8>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >体检通知书（核保）</input></TD>
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >核保通知书（核保）</input></TD>
					<TD class= title8><input type="checkbox"  name="GrpDetailPrt" >生调通知书（核保）</input></TD>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >谢绝通知书</input></TD>
					</TR>
					<TR  class= common8>		
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >延期通知书</input></TD>
					<TD class= title8><input type="checkbox"  name="GrpDetailPrt" >补充材料通知书</input></TD>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >客户服务通知书</input></TD>
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >保全通知书补打</input></TD>
					</TR>
          <TR  class= common8>
					<TD class= title8><input type="checkbox"  name="GrpDetailPrt" >保全缴费通知书</input></TD>
					<TD class= title8><input type="checkbox"  name="NoticePrt" >批单(个)</input></TD>
					<TD class= title8><input type="checkbox" checked  name="DetailPrt" >批单补打</input></TD>
					<TD class= title8><input type=hidden></input></TD>
					</TR>				
				</table>
				<HR>
			</div>
	
  <br>
	<TD  class= title>
		<INPUT VALUE="查询" TYPE=button class=cssButton onclick="easyQueryClick();">
		<INPUT VALUE="选择打印" class= cssButton TYPE=button onclick="BatchPPrint()">
		<INPUT VALUE="全部打印" class= cssButton TYPE=button onclick="BatchPPrintWH()">
		<input type=hidden id="intRecordNo" name="intRecordNo">
		<input type=hidden id="ArrPrtSeq"  name="ArrPrtSeq">
		<input type=hidden id="fmtransact" name="fmtransact">
	</TD>

	    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTask);">
    		</td>
    		<td class= titleImg>
    			 打印任务信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divTask" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanTaskGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>
  <Div id="divNBBP" style="display: 'none'">
    <tr class=common>
      <td class= title>  SQL </td>
			<td class=input><Input class=common name=strSql ></td>
    </tr>
  </Div>
  <table  class= common>
    <TR  class= common>
      <TD  class= title>
        请输入要保存文件的路径：
      </TD>
      <TD  class= input style= "width: '40%'">
        <Input class= common5  name=RealPath style= "width: '95%'"> 
      </TD>           
    </TR>
  </table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
<script>
function getDepartment(){	
var department = "#" + fm.Department.value 
                 + "# and SaleChnl = #" + fm.SaleChnl.value 
                 + "# and cardtype = #" + fm.PrtTypeName.value + "#";
//alert(department);
return department;
}
</script>