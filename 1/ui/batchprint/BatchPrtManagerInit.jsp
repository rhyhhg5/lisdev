<%
//程序名称：BatchPrtManagerInit.jsp
//程序功能：
//创建日期：2005-08-29
//创建人  ：zhangjun
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strOperator = globalInput.Operator;
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
var ID="";
var Cols1="";	
var Cols2="";
var Cols3="";
var Cols4="";
var Cols5="";
var Cols6="";

var ListCols1="";
var ListCols2="";
var ListCols3="";
var ListCols4="";
var ListCols5="";
var ListCols6="";
var ListCols61="";
var ListCols7="";
var ListCols71="";

function initInpBox()
{ 
  
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ConFeeInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    ID="序号";
    
    Cols1="业务类型";	
    Cols2="打印批次号";
    Cols3="单证类型";
    Cols4="份数";
    Cols5="生成时间";
    Cols6="操作人";
    Cols7="个团标志"
    
    ListCols1="批单号";
    ListCols2="客户号";	
    ListCols3="投递人姓名";
    ListCols4="生成时间";
    ListCols5="经办人";
    ListCols6="NO";
    ListCols7="NO";
    ListCols61="0px";
    ListCols71="0px";
    
    initInpBox();
    initSelBox();  
    initTaskGrid();  
    initListGrid();
  }
  catch(re)
  {
    alert("ConFeeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 打印信息列表的初始化
var TaskGrid;
function initTaskGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
		iArray[0][0]=ID;
		iArray[0][1]="30px";
		iArray[0][2]=10;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]=Cols1;
		iArray[1][1]="140px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]=Cols2;         		//列名
		iArray[2][1]="120px";            	//列宽
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]=Cols3;
		iArray[3][1]="140px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]=Cols4;
		iArray[4][1]="100px";
		iArray[4][2]=100;
		iArray[4][3]=0;	

		iArray[5]=new Array();
		iArray[5][0]=Cols5;
		iArray[5][1]="100px";
		iArray[5][2]=200;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]=Cols6;
		iArray[6][1]="120px";
		iArray[6][2]=200;
		iArray[6][3]=0;

    iArray[7]=new Array();
	  iArray[7][0]=Cols7;         	//列名
	  iArray[7][1]="0px";            	//列宽
	  iArray[7][2]=100;            			//列最大值
	  iArray[7][3]=3; 
	  
    iArray[8]=new Array();
	  iArray[8][0]="工作流任务编码";         	//列名
	  iArray[8][1]="0px";            	//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=3; 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="工作流子任务编码";         	//列名
	  iArray[9][1]="0px";            	//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=3; 

    TaskGrid = new MulLineEnter( "fm" , "TaskGrid" ); 
    //这些属性必须在loadMulLine前
    TaskGrid.mulLineCount = 10;   
    TaskGrid.displayTitle = 1;
    TaskGrid.hiddenPlus = 1;
    TaskGrid.hiddenSubtraction = 1;
    TaskGrid.canSel = 1;
    TaskGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //BankGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
var ListGrid;
function initListGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
		iArray[0][0]=ID;
		iArray[0][1]="30px";
		iArray[0][2]=10;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]=ListCols1;
		iArray[1][1]="140px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]=ListCols2;         		//列名
		iArray[2][1]="120px";            	//列宽
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]=ListCols3;
		iArray[3][1]="140px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]=ListCols4;
		iArray[4][1]="100px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]=ListCols5;
		iArray[5][1]="100px";
		iArray[5][2]=200;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]=ListCols6;
		iArray[6][1]=ListCols61;
		iArray[6][2]=200;
		iArray[6][3]=0;

    iArray[7]=new Array();
	  iArray[7][0]=ListCols7;         	//列名
	  iArray[7][1]=ListCols71;            	//列宽
	  iArray[7][2]=100;            			//列最大值
	  iArray[7][3]=0; 
	  
    iArray[8]=new Array();
	  iArray[8][0]="工作流任务编码";         	//列名
	  iArray[8][1]="0px";            	//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=3; 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="工作流子任务编码";         	//列名
	  iArray[9][1]="0px";            	//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=3; 

    ListGrid = new MulLineEnter( "fm" , "ListGrid" ); 
    //这些属性必须在loadMulLine前
    ListGrid.mulLineCount = 10;   
    ListGrid.displayTitle = 1;
    ListGrid.hiddenPlus = 1;
    ListGrid.hiddenSubtraction = 1;
    ListGrid.canSel = 0;
    ListGrid.canChk = 1;
    ListGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //BankGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>