//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var strSQL = "";
var strCountSQL=""; 

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}

//（批量）选择打印
function BatchPPrint()
{
	fm.action="NBBPrintSave.jsp";
	fm.fmtransact.value="CONFIRM";
	var recordRecord=10;
	var aTime=1;
		if (fm.BillName.value == "bq009" || fm.BillName.value == "bq014") 
	{
		fm.action="BQBatchPrintSave.jsp"
	  fm.fmtransact.value="PRINT";
	}
	if (verifyInput() == false) return false; 
	var showStr="正在生成打印数据文件，请您稍候... ...";
	//var showStr="正在生成打印数据文件，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../barBean/barBeanStatus.jsp?content="+showStr+"&countNum="+recordRecord+"&oneTime="+aTime;    
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();
}
//（批量）全部打印
function BatchPPrintWH()
{
	fm.action="NBBPrintSaveWH.jsp";
	fm.fmtransact.value="CONFIRM";
	if (verifyInput() == false) return false;  
	var showStr="正在生成打印数据文件，请您稍候... ...";
	var recordRecord=10;
	var aTime=1;
	if (fm.BillName.value == "bq009" || fm.BillName.value == "bq014") 
	{
		fm.action="BQBatchPrintSaveWH.jsp"
	  fm.fmtransact.value="PRINT";
	}
	//var Count = 1;
	//opener.intCount=1;
	var urlStr="../barBean/barBeanStatus.jsp?content="+showStr+"&countNum="+recordRecord+"&oneTime="+aTime;  
	//alert(urlStr);
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();

}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}   

// 查询按钮
function easyQueryClick() {
  strSQL = "";
  strCountSQL=""; 
	//alert("PrtName:"+fm.PrtName.value);
	//alert("BillName:"+fm.BillName.value);
	//alert("ManageCom:"+fm.ManageCom.value);
	//if(fm.PrtName.value!='' && fm.BillName.value != '')
	if (fm.BillName.value != '')
		eval("query"+trim(fm.BillName.value)+"()");
} 
function querytb020()//个险缴费通知书
{
	// 书写SQL语句 
	
	//var intLineNum = fm.BPrtNum.value; LOPRTMANAGER
  initTBMutline();
	strSQL = "SELECT LOPRTManager.PrtSeq,LCCont.PrtNo,LCCont.AppntName,LOPRTManager.AgentCode, LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom FROM LOPRTManager,LAAgent,LABranchGroup,LCCont WHERE LOPRTManager.Code = '07' " 
	       + "and LAAgent.AgentCode = LOPRTManager.AgentCode "  
	       + "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "  
	       + "and LOPRTManager.OtherNo = LCCont.ContNO "	  
	       + getWherePart('LOPRTManager.OtherNo', 'PolNo')   
	       + getWherePart('LCCont.PrtNo', 'PrtNo')	  
	       + getWherePart('LCCont.AppntName', 'AppntName')	  
	       + getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
	       + getWherePart('LOPRTManager.AgentCode','AgentCode')  
	       + getWherePart('LAAgent.AgentGroup','AgentGroup')  
	       + " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"  
	       +" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	strCountSQL="SELECT COUNT(*) FROM LOPRTManager,LAAgent,LABranchGroup,LCCont WHERE LOPRTManager.Code = '07' " 
	          + "and LAAgent.AgentCode = LOPRTManager.AgentCode "  
	          + "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "  
	          + "and LOPRTManager.OtherNo = LCCont.ContNO "	  
	          + getWherePart('LOPRTManager.OtherNo', 'PolNo')   
	          + getWherePart('LCCont.PrtNo', 'PrtNo')	  
	          + getWherePart('LCCont.AppntName', 'AppntName')	  
	          + getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
	          + getWherePart('LOPRTManager.AgentCode','AgentCode')  
	          + getWherePart('LAAgent.AgentGroup','AgentGroup')  
	          + " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'" 
	          +" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";     
	 SetQueryBL();
	}
function querytb008() // 个人体检通知书查询
{
	// var intLineNum = fm.BPrtNum.value;
	initTBMutline();
	strSQL = "SELECT LOPRTManager.PrtSeq, LOPRTManager.OtherNo,LOPRTManager.AgentCode, LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom FROM LOPRTManager,LAAgent,LABranchGroup,LWMission WHERE LOPRTManager.Code = '03' "  //LOPRTManager.Code='03' 单证类型为核保
	       +"and LWMission.ActivityID = '0000001106' "
         +"and LWMission.ProcessID = '0000000003' "
         +"and LWMission.Missionprop3 = LOPRTManager.Prtseq "
	       + "and LAAgent.AgentCode = LOPRTManager.AgentCode "
	       + "and LABranchGroup.AgentGroup = LAAgent.AgentGroup"  	
			   + getWherePart('LOPRTManager.OtherNo', 'PolNo') 
			   //+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
			   + getWherePart('LOPRTManager.AgentCode','AgentCode')
			   + getWherePart('LAAgent.AgentGroup','AgentGroup')
			   + " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'" //登陆机构权限控制
			   +" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";	   
	 turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	 strCountSQL = "SELECT COUNT(*) FROM LOPRTManager,LAAgent,LABranchGroup,LWMission WHERE LOPRTManager.Code = '03' "  //LOPRTManager.Code='03' 单证类型为核保
	             +"and LWMission.ActivityID = '0000001106' "
               +"and LWMission.ProcessID = '0000000003' "
               +"and LWMission.Missionprop3 = LOPRTManager.Prtseq "
	             + "and LAAgent.AgentCode = LOPRTManager.AgentCode "
	             + "and LABranchGroup.AgentGroup = LAAgent.AgentGroup"	
			         + getWherePart('LOPRTManager.OtherNo', 'PolNo') 
			         //+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
			         + getWherePart('LOPRTManager.AgentCode','AgentCode')
			         + getWherePart('LAAgent.AgentGroup','AgentGroup')
			         + " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'" //登陆机构权限控制
			         +" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";		         
	SetQueryBL();
	}
//保全批单打印（个人）	
function querybq009()
{
    initBQMutline();
		strSQL = "select EdorAcceptNo, OtherNo,EdorAppName,EdorState,EdorAppDate,ManageCom from LPEdorApp where EdorAcceptNo in (select EdorAcceptNo from lpedorappprint) and (EdorState='0' or (EdorState='2' and UWState='9'))"//+" and"+tReturn
	                         +" and OtherNoType in ('1','3')"
				 +" "+ getWherePart( 'ManageCom' )
				 //+" "+ getWherePart( 'OtherNoType')
				 //+" " + getWherePart('EdorAcceptNo')
	                         + " order by EdorAppDate";                      
    turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
		strCountSQL = "select COUNT(*) from LPEdorApp where EdorAcceptNo in (select EdorAcceptNo from lpedorappprint) and (EdorState='0' or (EdorState='2' and UWState='9'))"//+" and"+tReturn
	                         +" and OtherNoType in ('1','3')"
				 +" "+ getWherePart( 'ManageCom' );
				 //+" "+ getWherePart( 'OtherNoType')
				 //+" " + getWherePart('EdorAcceptNo');  
				    
    SetQueryBL();	                         	                         
}	
//保全批单打印（团体）
function querybq014()
{
	  initBQGrpMutline();
		strSQL = "select GrpContNo,EdorNo, EdorAcceptNo,EdorAppNo,modifydate,ManageCom,EdorType from LPGrpEdorItem where EdorNo in (select EdorNo from lpedorprint) and (EdorState='0' or (EdorState='2' and UWFlag='9'))"//+" and"+tReturn
	        +" "+ getWherePart( 'ManageCom' )
	      //+" "+ getWherePart( 'GrpContNo' )
				// +" "+ getWherePart( 'EdorNo' )
	       +" order by modifydate";                      
    turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
		strCountSQL = "select COUNT(*) from LPGrpEdorItem where EdorNo in (select EdorNo from lpedorprint) and (EdorState='0' or (EdorState='2' and UWFlag='9'))"//+" and"+tReturn
	        +" "+ getWherePart( 'ManageCom' );
	      //+" "+ getWherePart( 'GrpContNo' )
				// +" "+ getWherePart( 'EdorNo' ) 
				    
    SetQueryBL();	 
	}
function querytb110001()
{
	var strSQL = "";
	strSQL = "SELECT ContNo,PrtNo,Prem,CValiDate,AppntName,PrintCount FROM LCCont A where ContType = '1'"
//		+ " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+comcode+"','Pj')=1 "
//		+ "AND t.Code = '110001'"
		+ " AND AppFlag = '1' "
		+ " AND ( PrintCount < 1 OR PrintCount IS NULL )"
//		+ " and EXISTS (select riskcode from LMRiskApp where NotPrintPol = '0' and riskcode=A.riskcode )"
		+ getWherePart( 'ContNo' )
		+ getWherePart( 'PrtNo' )
		+ getWherePart( 'AgentCode' )
		+ "AND SaleChnl= '"+ SaleChnlCode()+"'"
//		+ getWherePart( 'SaleChnl' )
		+ getWherePart('ManageCom', 'ManageCom', 'like')
//		+ strManageComWhere
//		+ "AND NOT EXISTS ( SELECT PolNo FROM LCPol WHERE A.PrtNo = PrtNo AND AppFlag <> '1' ) "
		+ " and ManageCom like '" + comcode + "%%'"
		+ "order by ManageCom,AgentGroup,AgentCode";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   	strCountSQL = "SELECT COUNT(*) FROM LCCont A where ContType = '1'"
		+ " AND AppFlag = '1' "
		+ " AND ( PrintCount < 1 OR PrintCount IS NULL )"
		+ getWherePart( 'ContNo' )
		+ getWherePart( 'PrtNo' )
		+ getWherePart( 'AgentCode' )
		+ "AND SaleChnl= '"+ SaleChnlCode()+"'"
		+ getWherePart('ManageCom', 'ManageCom', 'like')
		+ " and ManageCom like '" + comcode + "%%'"
		+ "order by ManageCom,AgentGroup,AgentCode";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  SetQueryBL();
}

//查询校验及查询设置
function SetQueryBL()
{
fm.strSql.value = strSQL;
alert(fm.strSql.value);
var RecordCount = easyExecSql(strCountSQL);
if(RecordCount)
{
	fm.intRecordNo.value = RecordCount[0][0];
} 
//alert("RecordCount::"+RecordCount[0][0]);
//alert(fm.intRecordNo.value);
		//判断是否查询成功
if (!turnPage.strQueryResult)
{
	alert("未查询到满足条件的数据！");
	initTaskGrid();
	return false;
}


 //设置查询起始位置
 turnPage.pageIndex = 0;
 //在查询结果数组中取出符合页面显示大小设置的数组
 turnPage.pageLineNum = 10 ;
 //查询成功则拆分字符串，返回二维数组
 turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
 //设置初始化过的MULTILINE对象
 turnPage.pageDisplayGrid = TaskGrid;
 //保存SQL语句
 turnPage.strQuerySql = strSQL ;
 arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
 //调用MULTILINE对象显示查询结果
 displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
 TaskGrid.checkBoxAll();
}

//用于当单证名称变化时，打印信息列表项名称的变化
function updatefield()
{
	alert("as");
	var iArray=new Array();
		iArray[0]="投保单号";
		iArray[1]="140px";
		iArray[2]=100;
		iArray[3]=0;

	TaskGrid.updateField(3,iArray);
}    
function initTBMutline()
{
	  ID="序号";
    Cols1="流水号";	
    Cols2="投保单号码";
    Cols3="代理人编码";
    Cols4="代理人组";
    Cols5="展业机构";
    Cols6="管理机构";
    initTaskGrid(); 
	} 
function initBQMutline()
{
		  ID="序号";
    Cols1="保全受理号";	
    Cols2="申请号";
    Cols3="申请人名称";
    Cols4="批改状态";
    Cols5="申请日期";
    Cols6="管理机构";
    initTaskGrid(); 
	}	   
function initBQGrpMutline()
{
		ID="序号";
    Cols1="集体合同号";	
    Cols2="批单号";
    Cols3="保全受理号";
    Cols4="批改申请号";
    Cols5="批改日期";
    Cols6="管理机构";
    Cols7="批改类型"
    initTaskGrid(); 
	}	
//function SaleChnlCode()
//{
//	 var SaleChnlNo="";
//	  switch(fm.SaleChnl.value)
//    {
//  	 case "团险直销":
//  	    SaleChnlNo="01";
//  	    break;
//  	 case "个人营销":
//  	    SaleChnlNo="02";
//  	    break;
//  	 case "银行代理":
//  	    SaleChnlNo="03";
//  	    break;         
//  	} 
//	  return SaleChnlNo;
//}
/*********************************************************************
 *  选择暂交费类型后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect(cCodeName, Field) {
	var Fieldnum;
	if(cCodeName == "cardtype" && fm.Department.value != "" && fm.SaleChnl.value != "") {  
			if (fm.Department.value == "01" && fm.SaleChnl.value == "01" && Field.value == "02"){
				Fieldnum = 2;
				}
			if (fm.Department.value == "02" && fm.SaleChnl.value == "01" && Field.value == "02"){
				Fieldnum = 5;
				}				
	 showPrtTypeNameInput(Fieldnum);
	}

}

function showPrtTypeNameInput(type) {
var i; 
  for (i=0; i<8; i++) {
    if ((i+1) == type) {
      fm.all("PrtTypeName" + (i+1)).style.display = '';
    } 
    else 
    {
    	try{
      fm.all("PrtTypeName" + (i+1)).style.display = 'none';
      }
      catch(ex){
      }
    } 
  } 
}