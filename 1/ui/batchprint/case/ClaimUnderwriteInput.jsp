<%
//程序名称：ClaimUnderwriteInput.jsp
//程序功能：审批审定页面
//创建日期：2002-06-19 11:10:36
//创建人  ：Wujs
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<%
GlobalInput tGlobalInput = new GlobalInput();
tGlobalInput = (GlobalInput)session.getValue("GI");
String usercode=tGlobalInput.Operator;
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./CaseCommon.js"></SCRIPT>
  <SCRIPT src="./ClaimUnderwriteInput.js"></SCRIPT>


  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./ClaimUnderwriteInit.jsp"%>
  
</head>

<body  onload="initForm();" >
	<form action="./ClaimUnderwriteSave.jsp" method=post name=fm target="fraSubmit">
	  <%@include file="LLRemark.jsp"%>
	  <%@include file="CaseTitle.jsp"%>
	<table  class= common>
    <TR  class= common8>
      <TD  class= title8>客户号</TD><TD  class= input8><Input class= readonly name="CustomerNo" readonly></TD>
      <TD  class= title8>客户姓名</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
      <TD  class= title8>客户性别</TD><TD  class= input8><Input class= readonly name="Sex" readonly></TD>
    </TR>
	</table>
    <table style= "display: 'none'">
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimPay);">
        </td>
        <td class= titleImg> 赔付保单明细</td>
      </tr>
    </table>
    <Div  id= "divClaimPay" style= "display: 'none'">
    <table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
				<span id="spanClaimPayGrid" >
				</span>
			</td>
		</tr>
    </table>
    </Div> 
    
    <table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimPol);">
        </td>
        <td class= titleImg> 保单责任明细</td>
      </tr>
    </table>
    <Div  id= "divClaimPol" style= "display: ''">
    <table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
				<span id="spanClaimPolGrid" >
				</span>
			</td>
		</tr>
    </table>
    </Div>
    
    <table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimTotal);">
        </td>
        <td class= titleImg> 计算赔付汇总</td>
      </tr>
    </table> 
      <div  id= "divClaimTotal" style= "display: ''">
      
    <table  class= common>
	<TR  class= common8>
		<TD  class= title8>先期给付</TD>
		<TD  class= input8><Input class= readonly name="PreGiveAmnt" readonly></TD>
		<TD  class= title8>自负金额</TD>
		<TD  class= input8><Input class= readonly name="SelfGiveAmnt" readonly></TD>
		<TD  class= title8>不合理费用</TD>
		<TD  class= input8><Input class= readonly name="RefuseAmnt" readonly></TD>
	</TR>
	<TR  class= common8>
		<TD  class= title8>理算金额</TD>
		<TD  class= input8><Input class= readonly name="StandPay" readonly></TD>
		<TD  class= title8>实赔金额</TD>
		<TD  class= input8><Input class= readonly name="RealPay" readonly></TD>
		<TD  class= title8>实赔天数</TD>
		<TD  class= input8><Input class= readonly name="RealHospDate" readonly></TD>
	</TR> 
	<TR  class= common8>
		<TD  class= title8>赔付金额</TD>
		<TD  class= input8><Input class= readonly name="AllGiveAmnt" readonly></TD>		
		<TD  class= title8>赔付结论</TD>
		<TD  class= input8><Input class= readonly name="AllGiveType" readonly></TD>		
	</TR> 
    </table>
</div>

<div id= "divSpecial" style= "display: 'none'">
<table>
      	<tr>
        	<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divLCBnf2);">
        	</td>
        	<td class= titleImg> 特殊给付 </td>
      	</tr>
    	</table>

	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
				<span id="spanSpecialGiveGrid" >
				</span>
			</td>
		</tr>
	</table>
  </div>  

 <table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divShowDetail);">
        </td>
        <td class= titleImg> 审批审定</td>
      </tr>
    </table>
    
      <Div  id= "divShowDetail" style="display: '';">
          
	      <table  class= common>
	     	<TR  class= common>
			<TD  class= title colspan="6">审批意见</TD>
		</TR>
		<TR  class= common>
			<TD  class= input colspan="6">
			    <textarea name="RemarkSP" cols="100%" rows="3"  class="common">
			    </textarea>
			</TD>
		</TR>
	        
	      </table>
    
    <TABLE class=common>
	<TR class=common8>
	    	<TD  class= title8>审定结论</TD>
            <TD  class= input8><Input class=codeno name="DecisionSD" CodeData="0|2^1|同意审批结论^2|不同意审批结论" ondblClick="return showCodeListEx('llclaimdecide',[this,DecisionSDName],[0,1]);" onkeyup="return showCodeListKeyEx('llclaimdecide',[this,DecisionSDName],[0,1]);"><Input class=codename name=DecisionSDName></TD>	    	
	    	<TD  class= title8></TD><TD  class= input8></td>
	    	<TD  class= title8></TD><TD  class= input8></td>
	</TR>
    </table>
    <TABLE class=common>
    	<TR  class= common8>
		<TD  class= title colspan="6">审定意见</TD>
	</TR>	
	<TR  class= common8>
		<TD  class= input colspan="6">
		    <textarea name="RemarkSD" cols="100%" rows="3"  class="common">
		    </textarea>
		</TD>
	</TR>
    </table>
  </div>  

 <table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divShowMark);">
        </td>
        <td class= titleImg> 案件备注</td>
      </tr>
    </table>
    
      <Div  id= "divShowMark" style="display: 'none';">
     <TABLE class=common>
	<TR  class= common8>
		<TD  class= input colspan="6">
		    <textarea name="RemarkBZ" cols="100%" rows="3"  class="common">
		    </textarea>
		</TD>
	</TR>
    </table>   
 </div>

<br>

 <div id=divconfirm align='left'>
<input type=hidden name="usercode" value="<%=usercode %>" />
   	<input class=cssButton style='width:60px;' type=button value="确  认" name="Btn_ManChk" onclick="Claim_SPSD()" id = "sub1">
    <input class=cssButton style='width:60px;' type=button value="扣除明细" onclick="openDrug();" id = "sub2">
<!--<input class=cssButton style='width:60px;' type=button value="告知审查" onclick="secondUW();"> -->
<!--<input class=cssButton style='width:60px;' type=button value="核保回复" onclick="secondUWView();"> -->
	<input class=cssButton style='width:60px;' type=button value="给付确认" name="Btn_GiveEn" onclick="GiveEnsure()" id = "sub3">
	<TD  class= input><INPUT TYPE="checkBox" NAME="ContDealFlag" value="0" style="display: 'none';"> </TD>
 </Div>
<hr> 
   <div id=divcontral align='right' >
   <input class=cssButton style='width:80px;' type=button value="下发问题件" name='LowerQuestion' onclick="OnLower()" id = "sub10" >
   <input style="display:''" class=cssButton type=button style='width: 100px;' value="案件备注信息" onclick="openCaseRemark()">
   	<input name="AskIn" style="display:''"  class=cssButton type=button style='width: 70px;' value="查看扫描件" onclick="ScanQuery()">
    <input class=cssButton style='width:70px;' type=button value="标记黑名单" name="BlackList" onclick="submitBlackList()" id = "sub4">
    <input class=cssButton style='width:70px;' type=button value="材料退回" onclick="openSubmittedAffix();" id = "sub5"> 
    <input class=cssButton type=button style='width: 60px;' value="调查报告" onclick="ShowSurveyReply()" >
   	<input class=cssButton style='width:70px;' type=button value="审批审定书" onclick="ClaimUWPrint();" id = "sub6">
		<input class=cssButton style='width:70px;' type=button value="案件回退" onclick="onCaseBack();" id = "sub7">
    <input class=cssButton style='width:70px;' type=button value="上 一 步" onclick="preDeal();" id = "sub8">
    <input class=cssButton style='width:70px;' type=button value="返    回" onclick="backDeal();">
  </div>
<hr>
	<input type=hidden name="cOperate">
	<input type=hidden name="LoadFlag" value="2">
	<input type=hidden name="Case_RgtState" value="">
	<input type=hidden name="GiveType" value="">
	<input type="hidden">
	<input type=hidden name="LoadD" value="">
	<input type=hidden name="ShowCaseRemarkFlag">
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
  
</body>
</html>
