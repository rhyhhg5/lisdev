<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>



<%
  LLCaseInfoSchema tLLCaseInfoSchema   = new LLCaseInfoSchema();
  LLCaseInfoSet tLLCaseInfoSet=new LLCaseInfoSet();
  CaseInfoUI tCaseInfoUI   = new CaseInfoUI();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "INSERT";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
	//读取立案明细信息
  String strCaseNo=request.getParameter("CaseNo");
 
  
  String[] strNumber=request.getParameterValues("PCaseInfoGridNo");
  String[] strCode=request.getParameterValues("PCaseInfoGrid1");
  String[] strName=request.getParameterValues("PCaseInfoGrid2");
  
  int intLength=strNumber.length;
  for(int i=0;i<intLength;i++)
  {
  	tLLCaseInfoSchema=new LLCaseInfoSchema();
  	tLLCaseInfoSchema.setCaseNo(strCaseNo);
  	tLLCaseInfoSchema.setCode(strCode[i]);
  	tLLCaseInfoSchema.setName(strName[i]);
  	tLLCaseInfoSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLCaseInfoSchema.setCustomerName(request.getParameter("CustomerName"));
    
    tLLCaseInfoSet.add(tLLCaseInfoSchema);
  }

    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
	
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLLCaseInfoSet);
   tVData.addElement(tG);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tCaseInfoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseInfoUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>