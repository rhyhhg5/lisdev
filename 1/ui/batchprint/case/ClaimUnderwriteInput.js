var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";
var tRowNo=0;
var temp = 1;

//#2684关于理赔环节黑名单监测规则修改的需求 add by zqs
function checkBlacklist(checkName,checkId){
	//需要验证的证件号与人名都为空
	if((checkId==""||checkId==null)&&(checkName==""||checkName==null)){
		return true;
	}
	//需要验证的证件号为空
	if(checkId==""||checkId==null){
		var strblack="select 1 from lcblacklist where trim(name)in('"+checkName+"') and (idno='' or idno is null) with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   	   	  		//alert("若“证件号码”在黑名单库中为空");
   	   	  		if(!confirm("该客户为黑名单客户，是否受理此案件？")){
   	   	      		return false;
   	   	    	}
		}
		return true;
	}
	//需要验证的姓名为空
	if(checkName==""||checkName==null){
		var strblack=" select 1 from lcblacklist where trim(idno)in('"+checkId+"')with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   			//alert("姓名”不匹配为空，“证件号码”匹配");
   			if(!confirm("该客户为黑名单客户，是否受理此案件？")){
   	   	      return false;
   	   	    }
   		}
   		return true;
	}
	
	//1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
	var strblack=" select name,idno from lcblacklist where trim(name)in('"+checkName+"') or trim(idno)in('"+checkId+"') with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   		var strblack1=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and trim(idno)in('"+checkId+"') with ur";
   		var arrblack1=easyExecSql(strblack1);
   		if(arrblack1){
   			alert("该客户为黑名单客户");
   	   	  	return false;
   		}
   		var strblack2=" select 1 from lcblacklist where trim(idno)in('"+checkId+"') with ur";
   		var arrblack2=easyExecSql(strblack2);
   		if(arrblack2){
   			if(!confirm("该客户为黑名单客户，是否受理此案件？")){
   	   	      return false;
   	   	    }
   		}
   		var strblack3=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and (trim(idno)in('') or idno is null) with ur";
   		var arrblack3=easyExecSql(strblack3);
   		if(arrblack3){
   			if(!confirm("该客户为黑名单客户，是否受理此案件？?")){
   	   	      return false;
   	   	    }
   		}
     }
     return true;
}

function OnLower(){
	  var pathStr="./LLOnlinClaimQuestionMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&CustomerName="+fm.CustomerName.value+"&Handler="+fm.Handler.value;
	  showInfo=OpenWindowNew(pathStr,"EventInput","middle");
	}

//#1769 案件备注信息的录入和查看功能 add by Houyd
function showCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}
	//#1769 案件备注信息的录入和查看功能 自动弹出页面只弹一次
	//获取session中的ShowCaseRemarkFlag
	var tShowCaseRemarkFlag = fm.ShowCaseRemarkFlag.value;
	if(tShowCaseRemarkFlag == "" || tShowCaseRemarkFlag == null || tShowCaseRemarkFlag == "null"){
		//tShowCaseRemarkFlag = "1";//准备存入session的标记:0-未弹出；1-已弹出
		//alert(tShowCaseRemarkFlag);		
	}else{
		return ;
	}	
	var tSql = "select 1 from llcaseremark where caseno='"+aCaseNo+"' with ur";
	var tResult = easyExecSql(tSql);
	if(tResult != null){
		pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  		showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
  	}	
	
	var LowerStr = "select rgttype from llregister where 1=1 and rgtno = '" + aCaseNo+"' with ur";
	var tLowerStr = easyExecSql(LowerStr);
	if(tLowerStr) {
		var ttRgtType = tLowerStr[0][0];
		if(ttRgtType == "13") {
			fm.LowerQuestion.disabled=false;
		}else {
			fm.LowerQuestion.disabled=true;
		}
	}
}
function openCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}

	pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
}

//#2018 审批审定页面增加扫描件阅览功能
function ScanQuery() {

  var pathStr="./ClaimUnderwriteEasyScan.jsp?RgtNo="+fm.CaseNo.value+"&SubType=LP1001&BussType=LP&BussNoType=21";
  var newWindow=OpenWindowNew(pathStr,"查看扫描件","left");	
}

function initQuery()
{ 
  var strSql = " select contno,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,a.OtherAmnt,"
	   + "a.ApproveAmnt,OutDutyAmnt,OverAmnt,a.GiveTypeDesc,a.GiveReasonDesc,a.ClaimMoney,a.StandPay,a.RealPay,"
	   +"a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.GiveType,a.GiveReason "
	   +" from LLClaimdetail a, LMDutyGetClm b where  "
	   +" ClmNo in ( select clmno from llclaimpolicy where caseno='"+ fm.CaseNo.value +"')"
	   +" and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind"
  		
	var arr = easyExecSql(strSql );
	if (arr){
		displayMultiline( arr, ClaimPolGrid); 
	}
	else{
	 	initClaimPolGrid();	
	}
	 
  strSql = " select contno,b.riskname,a.GetDutyKind,''," +
	            " a.StandPay,a.RealPay,a.polno,a.clmno," +
	            " a.GiveTypeDesc,a.GiveType,a.riskcode"+
	            " from LLClaimpolicy a, lmrisk b " +
	            " where caseno='"+ fm.CaseNo.value +"'" +
	            " and a.riskcode = b.riskcode " +
	            " order by a.polno";
  		
	var drr = easyExecSql(strSql );
	if (drr){
		displayMultiline( drr, ClaimPayGrid);  
	}
	else{
	 	initClaimPayGrid();		
	}	

  strSql ="select sum(PreGiveAmnt),sum(SelfGiveAmnt),sum(RefuseAmnt),sum(ClaimMoney),sum(RealPay)"
  +"  from LLClaimdetail where ClmNo in (select clmno from llclaim where  caseno='"+ fm.CaseNo.value +"')";
	var brr = easyExecSql(strSql );
	if ( brr ){
		// alert( brr);
		brr[0][0]==null||brr[0][0]=='null'?'0':fm.PreGiveAmnt.value  = brr[0][0];
		brr[0][1]==null||brr[0][1]=='null'?'0':fm.SelfGiveAmnt.value = brr[0][1];
		brr[0][2]==null||brr[0][2]=='null'?'0':fm.RefuseAmnt.value   = brr[0][2];
		brr[0][3]==null||brr[0][3]=='null'?'0':fm.StandPay.value     = brr[0][3];
		brr[0][4]==null||brr[0][4]=='null'?'0':fm.RealPay.value      = brr[0][4];		
	 }
	else{
		fm.PreGiveAmnt.value  = "";
		fm.SelfGiveAmnt.value = "";
		fm.RefuseAmnt.value   = "";
		fm.StandPay.value     = "";
		fm.RealPay.value      = "";
	}
  strSql="select sum(RealHospDate) from llfeemain where   caseno='"+ fm.CaseNo.value +"'";
	 var crr = easyExecSql(strSql );
	 if ( crr ){
	 	crr[0][0]==null||crr[0][0]=='null'?'0':fm.RealHospDate.value =crr[0][0];	 	
	 }
	else{
		fm.RealHospDate.value = "";
	}
	
	  var strSQL4="select RealPay,GiveTypeDesc,GiveType from LLClaim where caseno='"+fm.CaseNo.value+"'";
	 var arr = easyExecSql(strSQL4);
	 if (arr!=null){
	 	arr[0][0]==null||arr[0][0]=='null'?'0':fm.AllGiveAmnt.value=arr[0][0];
	 	arr[0][1]==null||arr[0][1]=='null'?'0':fm.AllGiveType.value=arr[0][1];
	 	arr[0][2]==null||arr[0][2]=='null'?'0':fm.GiveType.value=arr[0][2];
	}
	else{
		fm.AllGiveAmnt.value = "";
		fm.AllGiveType.value = "";
		fm.GiveType.value = "";
	}
	 strSQL4="select checkdecision1,checkdecision2,remark1,remark2 from llclaimuwmain where caseno='"+fm.CaseNo.value+"'";
	 var hrr = easyExecSql(strSQL4);
	 if (hrr!=null){
	 	hrr[0][1]==null||hrr[0][1]=='null'?'0':fm.DecisionSD.value=hrr[0][1];
	 	if(fm.DecisionSD.value=='2')
	 		fm.DecisionSDName.value="不同意审批结论";
	 	if(fm.DecisionSD.value=='1')
	 		fm.DecisionSDName.value="同意审批结论";
	 	hrr[0][2]==null||hrr[0][2]=='null'?'0':fm.RemarkSP.value=hrr[0][2];
	 	hrr[0][3]==null||hrr[0][3]=='null'?'0':fm.RemarkSD.value=hrr[0][3];	 	
	}	
	else{
		fm.DecisionSD.value = "";
		fm.RemarkSP.value = "";
		fm.RemarkSD.value = "";
	}
	strSQL4="select rgtstate,contdealflag from llcase where caseno='"+fm.CaseNo.value+"'";
	var mrr = easyExecSql(strSQL4);
	if (mrr!=null){
		mrr[0][0]==null||mrr[0][0]=='null'?'0':fm.Case_RgtState.value=mrr[0][0];
		if(mrr[0][1]=="0"||mrr[0][1]=="1"){
			fm.ContDealFlag.checked = "true";
    }
	}
	else{
		fm.Case_RgtState.value = "";
	}
	
	var strSQL5 = "select caseno from llappeal where CaseNo='"+fm.CaseNo.value
	            +"' union select caseno from llappeal where appealno='"+fm.CaseNo.value +"' with ur";
	var nrr = easyExecSql(strSQL5);

	if (nrr!=null){
		nrr[0][0]==null||nrr[0][0]=='null'?fm.RemarkBZ.value='':fm.RemarkBZ.value="原案件"+nrr[0][0]+",纠错/申诉案件";
		var strSQL6 = "select appealno from llappeal where caseno='"+nrr[0][0]+"' with ur";
		var appealCase = easyExecSql(strSQL6);
		fm.RemarkBZ.value += appealCase;
		divShowMark.style.display ="";
		
	}
	else{
		fm.RemarkBZ.value = "";
		divShowMark.style.display ="none";
		//alert(nrr);
	}
	showCaseRemark();	//#1769 案件备注信息的录入和查看功能 add by Houyd
}

//调查报告
function ShowSurveyReply(){
  strSQL="select otherno from LLSurvey where otherno='"+fm.CaseNo.value+"'";
  arrResult = easyExecSql(strSQL);

  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&StartPhase=1";
  varSrc += "&Type=2";
  pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}

//审批审定确认
function Claim_SPSD(){
	var tRgtState = fm.Case_RgtState.value;
	//查讫状态的案件，可以进行审批审定	add by Houyd
	if (tRgtState == "04" ||tRgtState == "06" ||tRgtState == "10" || tRgtState == "08"){
		if (fm.all('RemarkSP').value == null ||fm.all('RemarkSP').value == "") {
			alert("请输入审批意见");
			return;
		}
		fm.cOperate.value="APPROVE|SP"		
	}
	else if (tRgtState == "05" ) {
		if (fm.all('DecisionSD').value == null ||fm.all('DecisionSD').value == ''){
			alert("请输入审定结论");
			return;
		}
		if (fm.all('DecisionSD').value == '2'){
		  if (fm.all('RemarkSD').value == null ||fm.all('RemarkSD').value == "") {
		    alert("请输入审定意见");
		    return;
		  }
	  }
		fm.cOperate.value="APPROVE|SD"		
	}else if(tRgtState == "16"){
		alert("理赔二核中不可审批审定，必须等待二核结束！");
		return;
	}
	else{
		alert("案件在当前状态下不能做审批审定");
		return;
	}
	
	//TODO:临时校验，社保案件不可审批审定
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
	
	var paySql = "select e.accdate,b.paytodate from llclaimdetail a,lcpol b,lmriskapp c,llcaserela d,llsubreport e "
		       + " where a.polno=b.polno and a.riskcode=c.riskcode and a.caserelano=d.caserelano "
		       + " and d.subrptno=e.subrptno "
		       + " and c.riskprop='G' and e.accdate >= b.paytodate and a.caseno='"
		       + fm.CaseNo.value + "' union "
		       + " select e.accdate,b.paytodate from llclaimdetail a,lbpol b,lmriskapp c,llcaserela d,llsubreport e "
		       + " where a.polno=b.polno and a.riskcode=c.riskcode and a.caserelano=d.caserelano "
		       + " and d.subrptno=e.subrptno "
		       + " and c.riskprop='G' and e.accdate >= b.paytodate and a.caseno='"
		       + fm.CaseNo.value + "' with ur";
    var arr = easyExecSql(paySql);
    if (arr) {
    	for(var i=0; i<arr.length; i++) {
    		if(!confirm("出险日期"+ arr[i][0] +"在保费交至日"+ arr[i][1] +"之后，是否继续理赔")) {
    			return false;
    		}
    	}
    }
	 //556反洗钱黑名单客户的理赔审批监测功能
     /*var strblack=" select 1 from lcblacklist where trim(name)='"+fm.all('CustomerName').value+"' with ur";
      var crrblack=easyExecSql(strblack);
      if(crrblack){
       if(!confirm("该客户为黑名单客户，是否受理此案件？")){
          return false;
       }
      }*/
    //#2684关于理赔环节黑名单监测规则修改的需求
    var checkId1=0;
    var checkName1=fm.all('CustomerName').value;
    var strblack="select idno from llcase where caseNo = '" +fm.CaseNo.value+"' with ur";
    var arrblack=easyExecSql(strblack);
    if(arrblack){
   	   	checkId1 = arrblack[0][0].trim();
   	   }
    if(!checkBlacklist(checkName1,checkId1)){
       	return false;
       }
     var strblackgrp="select 1 from LCGrpcont where grpcontno in( "
		+"select grpcontno from lcinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000'  "
		+"union select grpcontno from lbinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000') "
		+"and exists (select 1 from lcblacklist where trim(name)=trim(LCGrpcont.grpname)) "
		+"union "
		+"select 1 from LbGrpcont where grpcontno in( "
		+"select grpcontno from lcinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000'  "
		+"union select grpcontno from lbinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000') "
		+"and exists (select 1 from lcblacklist where trim(name)=trim(LbGrpcont.grpname)) with ur"
	  var crrblackgrp=easyExecSql(strblackgrp);	     
       if(crrblackgrp){
	   if(!confirm("该客户所在团单为黑名单客户，是否受理此案件？")){
          return false;
       }
	  }  
//       #2352 by lyc 2015-9-17
       var strfxsql = "select a.grpcontno,a.appntname,a.contno from llclaimpolicy a where a.caseno='"+fm.CaseNo.value+"' ";
       var fxcontarr = easyExecSql(strfxsql);
       if(fxcontarr){
    	   for(var j=0; j<fxcontarr.length; j++){
    		   if(fxcontarr[j][0]=="00000000000000000000"){
    			   var appsql = "select appntname,appntsex,appntbirthday,idtype,idno from lcappnt where contno='"+fxcontarr[j][2]+"' " +
    			   		" union all select appntname,appntsex,appntbirthday,idtype,idno from lbappnt where contno='"+fxcontarr[j][2]+"'" ;
    			   var apparr = easyExecSql(appsql);
    			   if(apparr){
    				   var fxsql = "select max(HighestEvaLevel) from fx_FXQVIEW where AOGName= '"+ apparr[0][0] +"' " +
    				   		" and APPNTIDNO = '"+ apparr[0][4] +"' and APPNTBIRTHDAY = '"+ apparr[0][2] +"' and APPNTSEX= '"+ apparr[0][1] +"' " +
    				   		" and APPNTIDTYPE= '"+ apparr[0][3] +"' and AOGTYPE='1' ";
    				   var fxarr = easyExecSql(fxsql);
    				   if(fxarr){
    					   if(fxarr[0][0]=="3"){
    						   if(!confirm("保单"+ fxcontarr[j][2] +"的投保人"+ apparr[0][0] +"风险等级为高风险，是否继续操作？")){
    							   return false;
    						   }
    					   }else if(fxarr[0][0]=="2"){
    						   if(!confirm("保单"+ fxcontarr[j][2] +"的投保人"+ apparr[0][0] +"风险等级为中风险，是否继续操作？")){
    							   return false;
    						   }
    					   }
    				   }
    			   }
    		   }else{
    			   var fxgrpsql = "select max(HighestEvaLevel) from fx_FXQVIEW where  AOGTYPE='2' and AOGName= '"+ fxcontarr[j][1] +"'";
    			   var fxgrparr = easyExecSql(fxgrpsql);
    			   if(fxgrparr){
    				   if(fxgrparr[0][0]=="3"){
						   if(!confirm("保单"+ fxcontarr[j][2] +"的投保人"+ fxcontarr[0][1] +"风险等级为高风险，是否继续操作？")){
							   return false;
						   }
					   }else if(fxgrparr[0][0]=="2"){
						   if(!confirm("保单"+ fxcontarr[j][2] +"的投保人"+ fxcontarr[0][1] +"风险等级为中风险，是否继续操作？")){
							   return false;
						   }
					   }
    			   }
    		   }
    	   }
       }
       
       // # 3067 抽检规则调整**start** #3185
          var tGrpContSql="select distinct grpcontno from llclaimdetail where 1=1 and caseno='"+ fm.CaseNo.value +"' with ur";
          var tCont =easyExecSql(tGrpContSql);
          if(tCont !=null ){  
        //	alert(tCont);
          var mSocialSql="select  CHECKGRPCONT('"+tCont[0][0]+"') from dual with ur";
          var tContNo =easyExecSql(mSocialSql);
        //  alert(tContNo);
          if(tContNo[0][0]!='Y'){
         // alert("11111111111");
          var samplingSql="select togetherFlag from llregister where 1=1 and rgtno='"+ fm.RgtNo.value +"' with ur";
          var sqlRule =easyExecSql(samplingSql);
          if(sqlRule ==null ||sqlRule==''|| sqlRule[0][0] =='1'){
        	 var backStr ="select c.customername,c.accname ,c.deathdate,c.casegetmode from llcase c where 1=1  " +
        		"and c.caseno='"+fm.CaseNo.value+"' with ur";
        	  var samplingrule = easyExecSql(backStr);
        	  if(samplingrule !=null){
        		  if(samplingrule[0][2]==null ||samplingrule[0][2]==''){
        			  if(samplingrule[0][3]!='1' && samplingrule[0][3]!='2'){
        			//	  alert(samplingrule[0][3]);
        			  if(samplingrule[0][0] !=samplingrule[0][1]){
        				  if(!confirm("该案件领款人与被保险人姓名不一致，领款人是否已提交《理赔授权委托书》、《保险赔款权益转让书》或其他证明材料"))
        					  return false;
       	   }
          }
         }
        }
       }
      }
     }
      	// # 3067 抽检规则调整**end**
       
     if(!checkblack_WG(checkName1,checkId1)){
    	 return false;
     }
     
     var LowerSQL = "select 1 from LLCASEPROBLEM where 1=1 and caseno = '" + fm.CaseNo.value + "' and state='1' with ur";
     var tLowerSQL = easyExecSql(LowerSQL);
     if(tLowerSQL) {    	
    		 alert("问题件在未回复时，案件不能进行审批审定。");
    		 return false;
     }
          
          
  var showStr="正在核赔数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  fm.Btn_ManChk.disabled=true;
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function checkblack_WG( checkName,checkId ){
	 
	var  usercode = fm.usercode.value;
	//驗證被保險人
    //3668外国政要、国际组织高级管理人员及其特定关系人”理赔系统校验功能需求 
	var sb="select db2inst1.CHECKGRPCONT(rgtobjno) from llregister where rgtno = (select rgtno from llcase where caseno='"+fm.CaseNo.value+"') with ur";
	var sbval=easyExecSql(sb);
	var flag="N"; //标志社保
  	if(sbval){
  		flag=sbval[0][0];
  	}
	if(flag=="N"){// 标志，商保
		
		var upuser = "select a.usercode,a.claimpopedom,a.upusercode,"
				+ "(select b.claimpopedom from llclaimuser b where b.usercode=a.upusercode),"
				+ "(select stateflag from llclaimuser where usercode=a.upusercode),"
				+ " a.stateflag "
				+ " from llclaimuser a where a.usercode in ( '" + usercode + "' )";
		 
		    var upuserval=easyExecSql(upuser);
		  	if(upuserval){
		    	var upusercode=upuserval[0][2]; //上级编码
//		    	var upstateflag=upuserval[0][4]; //上级有效
		    	var stateflag=upuserval[0][5];//本级有效
		    	if((""==upusercode || upusercode==null) && "1"==stateflag  ){ //当前用户为最高权限，无上级并且是有效用户 
		    			var strblack="select * from LDForeignSpecialList where trim(name)in('"+checkName+"') and (trim(idno) in ('"+checkId+"') or idno is null or trim(idno) ='' ) with ur";
		    			var blackcount=easyExecSql(strblack);
		    				if(blackcount){
		    					if(!confirm("该案件被保险人为外国政要、国际组织高级管理人员及其特定关系人，请确认该被保险人身份信息")){
		    			   	   	      return false;
		    			   	   	 }
		    				}
		    			var bnf="select name,idno  from llbnf where 1=1 and caseno='"+fm.CaseNo.value+"' and exists (select 1 from llcase where 1=1 and  caseno='"+fm.CaseNo.value+"'  and DEATHDATE is not null   )";
		    			var bnfbnf=easyExecSql(bnf);
		    			if(bnfbnf){
		    				var name=bnfbnf[0][0];
		    				var idno=bnfbnf[0][1];
		    				var strblackbnf="select * from LDForeignSpecialList where trim(name)in( '"+name+"' ) and (trim(idno) in ('"+idno+"') or idno is null or trim(idno) ='' ) with ur";
		            		var blackcountbnf=easyExecSql(strblackbnf);
		            			if(blackcountbnf){
		            				if(!confirm("该案件身故受益人为外国政要、国际组织高级管理人员及其特定关系人，请确认该被保险人身份信息")){
		            			   	   	      return false;
		            			   	   	}
		            			}
		    			}
		    			
		    		}
		    } 
	} 
    
  	//驗證受益人
    //3668外国政要、国际组织高级管理人员及其特定关系人”理赔系统校验功能需求end
   	return true;
}
function GiveEnsure(){ 
	var givemoney = fm.AllGiveAmnt.value ;
	
	//TODO:临时校验，社保案件不可给付确认
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
	
	//TODO:新增效验，理赔二核时不可给付确认
	var sqlUW = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstateUW=easyExecSql(sqlUW);
	if (rgtstateUW!=null){
	     if(rgtstateUW[0][0]=='16'){
	       alert("理赔二核中不可给付确认，必须等待二核结束！");
	       return false;
	     }
	}
	
	showInfo = window.open("./LLBnfMain.jsp?InsuredNo=" +fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value+"&RgtNo="+fm.RgtNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+fm.LoadFlag.value+"&givemoney="+givemoney+"&RgtState="+fm.Case_RgtState.value,"EventInput",'width=700,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function afterSubmit(FlagStr, content)
{
      fm.Btn_ManChk.disabled=false;
      
  	showInfo.close();  
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	initTitle();
	 var sql = "select RgtState from llcase where  CaseNo='" + fm.CaseNo.value +"'";
	 var arr = easyExecSql( sql );	 
	 if (arr)
	 {
	   fm.Case_RgtState.value = arr[0][0];
	 } 	
	/*if(fm.Case_RgtState.value=='09'){
	    sql = "select RgtClass,TogetherFlag from LLRegister "+"where rgtNo='"+fm.RgtNo.value+"'"; 
      arrResult = easyExecSql(sql);
      if(arrResult!=null)
      {
         if(arrResult[0][0]==1&&arrResult[0][1] !='1')
           return;
      }
      if(fm.GiveType.value==null||fm.GiveType.value==''||fm.GiveType.value=='3')
        return;
      GiveEnsure();
    }*/
}

function preDeal() 
{
	var caseNo = fm.all('CaseNo').value;
	var varSrc="&CaseNo=" + caseNo ;
	parent.window.location = "./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+
	varSrc;   
}

function backDeal()
{
    top.close();
}
function ClaimUWPrint()
{
	if(fm.CaseNo.value=="")
	{
		alert("没有足够的案件信息！");
		return false;
	}
	var CaseNo = fm.CaseNo.value;  

  var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.target = "fraSubmit";
  fm.action = "../uw/PDFPrintSave.jsp?Code=lp015&OtherNo="+CaseNo;
  fm.submit();
}

function afterSave()
{
	
}

function CaseChange()
{
	initTitle();
    if(fm.CaseNo.value!='')   
	if (fm.RgtState.value == null ||
		fm.RgtState.value =="")
	{
		queryClear();
	}
	else
	{
		initQuery();
	}
	
}
function queryClear()
{
		initClaimPolGrid();
	 	initClaimPayGrid();
		fm.PreGiveAmnt.value  = "";
		fm.SelfGiveAmnt.value = "";
		fm.RefuseAmnt.value   = "";
		fm.StandPay.value     = "";
		fm.RealPay.value      = "";
		fm.RealHospDate.value = "";	
		fm.AllGiveAmnt.value = "";
		fm.AllGiveType.value = "";	
		fm.GiveType.value = "";
		fm.DecisionSD.value = "";
		fm.RemarkSP.value = "";
		fm.RemarkSD.value = "";		
		fm.Case_RgtState.value = "";			
		 	
}
function onCaseBack()
{
	var newWindow =OpenWindowNew("./FrameMainCaseReturn.jsp?CaseNo="+fm.CaseNo.value+"&RgtState="+fm.Case_RgtState.value,"casereturn","middle");		
	
	}
	
////打开已提交申请材料列表
function openSubmittedAffix()
{
	 
	  var varSrc ="";
	  //showInfo = window.open("./SubmittedAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value);
	  pathStr="./SubmittedAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value;
	  showInfo = OpenWindowNew(pathStr,"","middle");

}

function submitBlackList(){
    if(fm.CaseNo.value=='' || fm.CaseNo.value==null) {
    	alert("当前操作理赔号不能为空");
    	return false;
    }
	var strsql="select customerno,customername from llcase where caseno= '"+fm.CaseNo.value+"'";
	var arr = easyExecSql(strsql);
	if(arr){
		customerno=arr[0][0];
		customername=arr[0][1];
	}
	else{
		customerno = '';
		customername='';
	}
	var varSrc = "?CustomerNo=" + customerno;
	varSrc += "&CustomerName=" + customername;
	varSrc += "&CaseNo=" + fm.CaseNo.value;
	pathStr="./FrameMainBlackList.jsp?Interface=BlackListInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}

function secondUW(){
 var varSrc ="";
 pathStr="./LLSecondUWMain.jsp?CaseNo="+fm.CaseNo.value+"&InsuredNo="+fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value;
 showInfo = OpenWindowNew(pathStr,"","left");
}

function secondUWView(){
	  var varSrc ="";
     pathStr="./LLDealUWsecondInput.jsp?CaseNo="+fm.CaseNo.value+"&InsuredNo="+fm.CustomerNo.value;
     showInfo = OpenWindowNew(pathStr,"","left");
}

function openDrug(){
    var varSrc = "&CaseNo=" + fm.CaseNo.value;
    varSrc += "&RgtNo=" + fm.RgtNo.value;
    varSrc += "&MainFeeNo=";
    varSrc += "&LoadFlag=2" ;
    varSrc += "&CustomerNo=" + fm.CustomerNo.value;
    varSrc += "&CustomerName=" + fm.CustomerName.value;
    
    pathStr="./FrameMainCaseDrug.jsp?Interface=CaseDrugInput.jsp"+varSrc;
    showInfo = OpenWindowNew(pathStr,"CaseDrugInput","middle",800,330);
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}