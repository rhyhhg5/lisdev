<%
//程序名称：CaseDrugInput.jsp
//程序功能：
//创建日期：2006-01-21 20:09:20
//创建人  ：CrtHtml程序创建
//更新记录：
// 更新人:刘岩松    
//更新日期：2002-09-24
//更新原因/内容：将"费用项目名称"的列宽该成＂0＂
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
var turnPage = new turnPageClass();
function initInpBox()
{
  try{
    fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
    fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
    fm.MainFeeNo.value = '<%= request.getParameter("MainFeeNo") %>';
    fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
    fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName"))%>';
    var LoadFlag = '<%= request.getParameter("LoadFlag") %>';
    var strSQL3="select customername from llcase where caseno='"+fm.CaseNo.value+"'";
	var arrResult3 = easyExecSql(strSQL3);
	if(arrResult3 != null && arrResult3 !="" ){ 
		 fm.CustomerName.value = arrResult3[0][0];
     }
    if(LoadFlag=='2'){
      fm.confirmButton.style.display = 'none';
    }
  }
  catch(ex)
  {
    alert(ex.message);
    alert("在CaseReceiptInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在CaseReceiptInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCaseDrugGrid();
    easyQuery();
    sumFee();
    getCaseRemark();
  }
  catch(ex)
  {
    alert("CaseDrugInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//收据费用明细
function initCaseDrugGrid()
{
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="费用代码";         			//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="费用名称";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="总费用";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[3][21]="SumFee";

      iArray[4]=new Array();
      iArray[4][0]="统筹内金额";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[4][21]="SecuFee";

      iArray[5]=new Array();
      iArray[5][0]="自付二";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[5][21]="SelfPay2";

      iArray[6]=new Array();
      iArray[6][0]="自费金额";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[6][21]="SelfFee";

      iArray[7]=new Array();
      iArray[7][0]="不合理费用";         			//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=60;            			//列最大值
      iArray[7][3]=1; 
      iArray[7][21]="UnReasonableFee";     
      
      iArray[8]=new Array();
      iArray[8][0]="原因";    	         //列名
      iArray[8][1]="150px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=1;

      iArray[9]=new Array();
      iArray[9][0]="帐单号";         			//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=80;            			//列最大值
      iArray[9][3]=1;      

      CaseDrugGrid = new MulLineEnter( "fm" , "CaseDrugGrid" ); 
      CaseDrugGrid.mulLineCount = 0;   
      CaseDrugGrid.displayTitle = 1;
      CaseDrugGrid.loadMulLine(iArray);      
      
      }
      catch(ex)
      {
        //alert(ex);
        alert("在此出错");
      }
  }

</script>