<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AppealSave.jsp
//程序功能：
//创建日期：2005-04-04
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);
  	
	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	LLAppealSchema tLLAppealSchema = new LLAppealSchema();
	LLRegisterSchema tLLRegisterSchema=new LLRegisterSchema(); 
	LLSubReportSet tLLSubReportSet = new LLSubReportSet();
	LLAppClaimReasonSet tLLAppClaimReasonSet = new LLAppClaimReasonSet();
	LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
	LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
	
	VData tVData = new VData();
	ClientRegisterBackBL tClientRegisterBackBL   = new ClientRegisterBackBL();
	//输出参数
	CErrors tError = null;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String EventFlag="";	//事件申请标记 为1 有客户存在事件，为0不存在事件
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");

	/*将申请信息填充*/
System.out.println("<--submit LLRegisterSchema-->");
if (strOperate.equals("APPEALINSERT||MAIN")) 	
{                   
    tLLRegisterSchema.setRgtState("01");//案件状态
    tLLRegisterSchema.setRgtObj("2");						//号码类型 0总单 1分单 2个单 3客户
    tLLRegisterSchema.setRgtObjNo(request.getParameter("CustomerNo"));
    System.out.println("RgtObjNo:"+tLLRegisterSchema.getRgtObjNo());
    tLLRegisterSchema.setRgtClass("0");
    tLLRegisterSchema.setCustomerNo(request.getParameter("CustomerNo"))	;
    tLLRegisterSchema.setRgtType(request.getParameter("RgtType"));         		//受理方式
    tLLRegisterSchema.setRgtantName(request.getParameter("RgtantName"));     	//申请人姓名
    tLLRegisterSchema.setRelation(request.getParameter("Relation"));        		//与被保险人关系
    tLLRegisterSchema.setRgtantAddress(request.getParameter("RgtantAddress"));	//申请人地址
    tLLRegisterSchema.setRgtantPhone(request.getParameter("RgtantPhone"));     	//申请人电话
    tLLRegisterSchema.setPostCode(request.getParameter("PostCode"));        		//邮编
    String tAppAmnt = request.getParameter("AppAmnt");
    if (tAppAmnt.equals("null")){
       tAppAmnt="0";
    }
    tLLRegisterSchema.setAppAmnt(tAppAmnt);       		//预估申请金额  
    tLLRegisterSchema.setGetMode(request.getParameter("paymode"));
    tLLRegisterSchema.setAppDate(request.getParameter("AppDate"));		//2366分红申诉纠错添加申请日期
    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setAccName(request.getParameter("AccName"));
    tLLRegisterSchema.setReturnMode(request.getParameter("ReturnMode"));    		//回执发送方式  
    tLLRegisterSchema.setIDType(request.getParameter("tIDType"));        			//申请人证件类型
    tLLRegisterSchema.setIDNo(request.getParameter("tIDNo"));          			//申请人证件号码
    tLLRegisterSchema.setRgtObj("1"); //个人客户                              

  //申请原因
	String tNum[] = request.getParameterValues("appReasonCode");
	String appResonName[] = {"门诊费用","住院费用","医疗津贴","重大疾病","身 故","护 理","失能","伤残"};

	int AppReasonNum = 0;
	if (tNum != null) 
	{
		AppReasonNum = tNum.length;
	}
	for (int i = 0; i < AppReasonNum; i++)	
	{  
	    LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();                              
	 		tLLAppClaimReasonSchema.setReasonCode(tNum[i]);        //原因代码                                                         
			tLLAppClaimReasonSchema.setCustomerNo(request.getParameter("CustomerNo")); 
			tLLAppClaimReasonSchema.setReasonType("0");
		
			tLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);
	}
    
    if(request.getParameter("AppealType").equals("0"))
    {
   		tLLCaseSchema.setRgtType("4");
    	System.out.println("申诉");
  	}
    else
      tLLCaseSchema.setRgtType("5");
    tLLCaseSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLCaseSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLCaseSchema.setPostalAddress(request.getParameter("RgtantAddress"));
    tLLCaseSchema.setPhone(request.getParameter("RgtantPhone"));
    tLLCaseSchema.setDeathDate(request.getParameter("DeathDate"));
    tLLCaseSchema.setCustBirthday(request.getParameter("CBirthday"));
    tLLCaseSchema.setCustomerSex(request.getParameter("Sex"));
    tLLCaseSchema.setIDType(request.getParameter("tIDType")); 
    tLLCaseSchema.setIDNo(request.getParameter("tIDNo"));
    tLLCaseSchema.setCaseGetMode(request.getParameter("paymode"));
    tLLCaseSchema.setBankCode(request.getParameter("BankCode"));
    tLLCaseSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLCaseSchema.setAccName(request.getParameter("AccName"));
    tLLCaseSchema.setCustState(request.getParameter("CustStatus"));
    tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
    tLLCaseSchema.setCustState(request.getParameter("CustStatus"));
    if (request.getParameterValues("PrePaidFlag") != null) {
		  tLLCaseSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
	}else{
		tLLCaseSchema.setPrePaidFlag("");  // 个案标记llcase 批次案件标记llregister
	}
    tLLAppealSchema.setCaseNo(request.getParameter("CaseNo"));
    tLLCaseSchema.setReceiptFlag("1");
    System.out.println(request.getParameter("CaseNo"));
    tLLAppealSchema.setAppealType(request.getParameter("AppealType"));
    tLLAppealSchema.setAppeanRCode(request.getParameter("AppeanRCode"));
    tLLAppealSchema.setAppealReason(request.getParameter("AppealReason"));
    tLLAppealSchema.setAppealRDesc(request.getParameter("AppealRDesc"));
    //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
    tLLCaseExtSchema.setRelaDrawerInsured(request.getParameter("RelaDrawerInsured"));
    tLLCaseExtSchema.setDrawerIDType(request.getParameter("DrawerIDType"));
    tLLCaseExtSchema.setDrawerID(request.getParameter("DrawerID"));
  //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
    
	//事件信息
	String tRNum[] = request.getParameterValues("InpEventGridChk");
	String eventno[] = request.getParameterValues("EventGrid1");
	String accdate[] = request.getParameterValues("EventGrid2");
	String accProvinceCode[] =request.getParameterValues("EventGrid15"); // #3500 发生地点省  
	String accCityCode[] =request.getParameterValues("EventGrid16");     //发生地点市
	String accCountyCode[]=request.getParameterValues("EventGrid17");    //发生地点县
	String accplace[] = request.getParameterValues("EventGrid6");  //发生地点
	String accindate[] = request.getParameterValues("EventGrid10");   //入院日期
	String accoutdate[] = request.getParameterValues("EventGrid11");  //出院日期
	String accdesc[] = request.getParameterValues("EventGrid12");	  //事件信息
	String acctype[] = request.getParameterValues("EventGrid14");	  //事件类型

	
	System.out.println("<--submit mulline-->");
	
	if (tRNum != null) 
	{
		System.out.println("<-tRNum-->"+ tRNum.length);
		for (int i = 0; i < tRNum.length; i++)	
		{   
		
			if ( "1".equals( tRNum[i]) || eventno[i]==null|| eventno[i].equals(""))
			{
				System.out.println("<-eventno[i]-->"+ eventno[i]);
				LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
				 tLLSubReportSchema.setSubRptNo(eventno[i]);
			    tLLSubReportSchema.setCustomerNo(request.getParameter("CustomerNo"));
			    tLLSubReportSchema.setCustomerName(request.getParameter("CustomerName"));      
			    tLLSubReportSchema.setAccDate(accdate[i]);
			    tLLSubReportSchema.setAccDesc(accdesc[i]);   
			    tLLSubReportSchema.setAccPlace( accplace[i]);  
			    tLLSubReportSchema.setInHospitalDate( accindate[i]);
			    tLLSubReportSchema.setOutHospitalDate( accoutdate[i]); 
			    tLLSubReportSchema.setAccidentType(acctype[i]);
			    tLLSubReportSchema.setAccProvinceCode(accProvinceCode[i]); // #3500 发生地点省
			    tLLSubReportSchema.setAccCityCode(accCityCode[i]);  // 发生地点市
			    tLLSubReportSchema.setAccCountyCode(accCountyCode[i]); // 发生地点县
			    System.out.println("发生地点省:"+accProvinceCode[i]+","+"发生地点市："+accCityCode[i]+"发生地点县："+accCountyCode[i]+"。");
				   	
					tLLSubReportSet.add(tLLSubReportSchema);
			}
		}
	}
}

		tLLCaseOpTimeSchema.setRgtState("01");
		tLLCaseOpTimeSchema.setStartDate(request.getParameter("OpStartDate"));
		tLLCaseOpTimeSchema.setStartTime(request.getParameter("OpStartTime"));
  try                                 
  {                                   
    //准备传输数据 VData               
	//VData传送
	System.out.println("<--Star Submit VData-->");
	tVData.add(tLLAppClaimReasonSet);
	tVData.add(tLLCaseSchema);
	tVData.add(tLLAppealSchema);
	tVData.add(tLLCaseExtSchema);
	tVData.add(tLLRegisterSchema);
	tVData.add(tLLSubReportSet);
	tVData.add(tLLCaseOpTimeSchema);
  	tVData.add(tG);
  	System.out.println("<--Into tClientRegisterBackBL-->");
    tClientRegisterBackBL.submitData(tVData,strOperate);
  } 
  catch(Exception ex)
  { 
    Content = "保存失败，原因是: " + ex.toString();
    FlagStr = "Fail";
  } 
    
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if ("".equals(FlagStr))
  { 
    tError = tClientRegisterBackBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	tVData.clear();
    	tVData=tClientRegisterBackBL.getResult();              
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  
  //显示信息
  
  System.out.println("AferSubmit");
  LLCaseSchema mLLCaseSchema = new LLCaseSchema(); 
  
  System.out.println("tVData"+tVData);
  
  mLLCaseSchema.setSchema(( LLCaseSchema )tVData.getObjectByObjectName( "LLCaseSchema", 0 ));


  System.out.println("================================");
  System.out.println("CaseNo"+mLLCaseSchema.getCaseNo());
  System.out.println("RgtNo"+mLLCaseSchema.getRgtNo());  
  System.out.println("================================");
  
      LLFirstDutyFilterBL tFirstDutyFilterBL = new LLFirstDutyFilterBL();
      TransferData CaseNo = new TransferData();
      CaseNo.setNameAndValue("CaseNo",mLLCaseSchema.getCaseNo());
      VData tVData1 = new VData();
		try{
			tVData1.add(CaseNo);
			tVData1.add(tG);
			System.out.println("tG"+mLLCaseSchema.getCaseNo());
			tFirstDutyFilterBL.submitData(tVData1, "INSERT");
		}
		catch (Exception ex){
			Content = "保存失败，原因是: " + ex.toString();
			FlagStr = "Fail";
		}
 
 %> 
 
<html>
<script language="javascript">
	    parent.fraInterface.fm.all("CaseNo").value = "<%=mLLCaseSchema.getCaseNo()%>";
	   
	    parent.fraInterface.fm.all("RgtNo").value = "<%=mLLCaseSchema.getRgtNo()%>";
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	    
		</script>  
</html>
    