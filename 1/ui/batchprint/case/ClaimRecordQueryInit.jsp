<%
//程序名称：ProposalQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//返回按钮初始化
var str = "";
function initDisplayButton()
{
	tDisplay = <%=tDisplay%>;
	//alert(tDisplay);
	if (tDisplay=="1"||tDisplay=="2")
	{
		fm.Return.style.display='';
	}
	else if (tDisplay=="0")
	{
		fm.Return.style.display='none';
	}
}
function initQuery()
{
    try
    {
        //alert("asdfsdaf"+top.opener.fm.all('ContNo'));
        var tContNo = top.opener.fm.all('ContNo').value;
	    //alert(tContNo);
	    if (tContNo!=""&&tContNo!=null)
	    {
	    	fm.all('ContNo').value = tContNo; 
	    	easyQueryClick();
	    }
	 }
	 catch(ex)
	 {
	 }
}
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
    
  }
  catch(ex)
  {
    alert("在AllProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initClaimPayGrid();
	initDisplayButton();
	initQuery();
	
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initClaimPayGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="给付号码";         		//列名
      iArray[1][1]="0px";            		//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="理赔号";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      
      iArray[3]=new Array();
      iArray[3][0]="姓名";         		//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      

      iArray[4]=new Array();
      iArray[4][0]="客户号";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="合同号";         		//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="险种名称";         		//列名
      iArray[6][1]="150px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="赔付金额";         		//列名
      iArray[7][1]="40px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="理赔人";         		//列名
      iArray[8][1]="40px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="付款日期";         		//列名
      iArray[9][1]="45px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许


      ClaimPayGrid = new MulLineEnter( "fm" , "ClaimPayGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimPayGrid.mulLineCount = 5;   
      ClaimPayGrid.displayTitle = 1;
      ClaimPayGrid.locked = 1;
      ClaimPayGrid.canSel = 1;
      ClaimPayGrid.hiddenPlus = 1;
      ClaimPayGrid.hiddenSubtraction = 1;
      ClaimPayGrid.loadMulLine(iArray); 
      ClaimPayGrid.selBoxEventFuncName = "ShowInfo";
      
      
      //这些操作必须在loadMulLine后面
      //ClaimPayGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>