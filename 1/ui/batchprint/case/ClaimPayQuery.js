//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function getQueryDetail()
{  
	var arrReturn = new Array();
	var tSel = ClaimPayGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = ClaimPayGrid.getRowColData(tSel - 1,2);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		
		if (cPolNo == "")
		    return;
		    
		var GrpPolNo = ClaimPayGrid.getRowColData(tSel-1,1);
                var prtNo = ClaimPayGrid.getRowColData(tSel-1,3);
        //alert("dfdf");
        if( tIsCancelPolFlag == "0"){
	    	if (GrpPolNo =="00000000000000000000") {
	    	 	window.open("./AllProQueryMain.jsp?LoadFlag=6&prtNo="+prtNo,"window1");	
		    } else {
			window.open("./AllProQueryMain.jsp?LoadFlag=4");	
		    }
		} else {
		if( tIsCancelPolFlag == "1"){//销户保单查询
			if (GrpPolNo =="00000000000000000000")   {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} else {
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	    } else {
	    	alert("保单类型传输错误!");
	    	return ;
	    }
	 }
 }
}

//销户保单的查询函数
function getQueryDetail_B()
{
	
	var arrReturn = new Array();
	var tSel = ClaimPayGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	  var cPolNo = ClaimPayGrid.getRowColData(tSel - 1,1);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		if (cPolNo == "")
			return;
		var GrpPolNo = ClaimPayGrid.getRowColData(tSel-1,6);
	    if (GrpPolNo =="00000000000000000000") 
	    {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} 
			else 
			{
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	}
}



// 保单明细查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = ClaimPayGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cContNo = ClaimPayGrid.getRowColData(tSel - 1,1);				
		if (cContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0");	
	}
}

// 基本信息查询
function FunderInfo()
{
	var arrReturn = new Array();
	var tSel = ClaimPayGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cInsuredNo = ClaimPayGrid.getRowColData(tSel - 1,4);				
		if (cInsuredNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/CQPersonMain.jsp?CustomerNo="+cInsuredNo);	
	}
}
//个人业务
function PersonBiz()
{
	var arrReturn = new Array();
	var tSel = ClaimPayGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var ContNo = ClaimPayGrid.getRowColData(tSel - 1,1);				
		if (ContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&IsCancelPolFlag=0");	
	}
}
// 查询按回车
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClaimPayQuery();

	}
}

function ClaimPayQuery()
{ 
	var comCode = fm.ComCode.value;
	if(fm.RgtDateS.value == "" || fm.RgtDateE.value == "") 
	{ alert("请输入受理起止日期"); return false;}
 	clearP();
// 	GetGet();	
	// 书写SQL语句
	var strSQLall = "";
	var strSQL = "";
	strSQLall=" select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and COALESCE(a.caseno, a.caseno) = b.caseno and b.rgtdate between '"+fm.RgtDateS.value+"' and '"+fm.RgtDateE.value+"' and b.mngcom like '"+comCode+"%' "
	          + getWherePart("b.CustomerNo", "CustomerNo")
	    	 + getWherePart("b.IdNo", "IDNo")
	    	 + getWherePart("b.CustomerName", "CustomerName")
	      	 + getWherePart("b.CaseNo","CaseNo")
	      	 + getWherePart("(select drawer from ljaget where otherno=a.caseno)","Drawer")
	      	 + "with ur";
	fm.AllClaimNum.value = easyExecSql(strSQLall);
	strSQL = "select a.CaseNo,a.CustomerName, a.RgtDate,"
				 + " (select sum(d.SumFee) from llfeemain d where d.caseno = a.caseno ), "
				 + " (select sum(e.ClaimMoney) from llclaimdetail e where e.CaseNo = a.CaseNo)," 
				 + " (select sum(e.realpay) from llclaimdetail e where e.CaseNo = a.CaseNo),"
				 + " (select distinct f.codename from ldcode f where f.codetype = 'llrgtstate' and f.code = a.rgtstate), "
				 + " (select distinct g.givetypedesc from llclaim g where g.CaseNo = a.CaseNo),"
				 + " (select  h.confdate from ljagetclaim h where h.OtherNo = a.CaseNo and h.confdate is not null fetch first 1 rows only),"
				 + " a.CustomerNo,(select drawer from ljaget where otherno=b.caseno fetch first 1 rows only) "
				 + " from LLcase a,llclaim  b where 1=1 "
	    	 + getWherePart("a.CustomerNo", "CustomerNo")
	    	 + getWherePart("a.IdNo", "IDNo")
	    	 + getWherePart("a.CustomerName", "CustomerName")
	      	 + getWherePart("a.CaseNo","CaseNo")
	      	 + getWherePart("(select drawer from ljaget where otherno=b.caseno)","Drawer")
	      	 + " and a.rgtdate <= '"+fm.all('RgtDateE').value+"'"
	  		 + " and a.rgtdate >= '"+fm.all('RgtDateS').value+"'"	 
	      	 + " and b.caseno = a.caseno "
	      	 + " and a.mngcom like '"+comCode+"%%'" 
	      	 + " and a.endcasedate is not null order by a.RgtDate DESC "; 
      turnPage.queryModal(strSQL,ClaimPayGrid);
   
      
}

//历史给付信息查询
function GetGet()
{
		var sum = easyExecSql("select count(Caseno) from llclaim "); 
		if( sum[0][0] == "0"){alert("暂无理赔案件");return;}
		for(var i = 1; i < 6; i++)
		{
				var sql = " select count(Caseno) from llclaim where givetype = '"+i+"' "; 
				var count = easyExecSql(sql); 
				
				if(count != null) 
				{ 
						fm.all("get"+i).value = count[0][0];
						var a =  count[0][0]*100/sum[0][0]
						fm.all("rget"+i).value = a.toString().substring(0,5);								
				}
				
				else
				{alert("数据库中没有数据");return;}
		}
//		var temp = easyExecSql("select count(Caseno) from llclaim where givetype is null");
//		fm.all('get6').value = temp;
//		var b = temp*100/sum;
//		fm.rget6.value = b.toString().substring(0,4);  
}



//个人给付信息查询
function GetGetP()
{
		var selno = ClaimPayGrid.getSelNo();
		var customerno = ClaimPayGrid.getRowColData(selno-1,10);
		var sumP = easyExecSql("select count(b.CaseNo) from llcase a, llclaim b where a.customerno = '"+customerno+"' and a.caseno = b.caseno"); 
		if( sumP == "0"){alert("暂无此人理赔案件");return;}
		for(var i = 1; i < 6; i++)
		{
				var sql = " select count(b.CaseNo) from llcase a, llclaim b where a.customerno = '"+customerno+"' and a.caseno = b.caseno and b.givetype = '"+i+"' ";
				var countP = easyExecSql(sql);
				
				if(countP != null) 
				{
						fm.all("getP"+i).value = countP[0][0];
						var a =  countP[0][0]*100/sumP
						fm.all("rgetP"+i).value = a.toString().substring(0,5);								
				}
				
				else
				{alert("数据库中没有数据");return;}
		}
//		var temp = easyExecSql("select count(Caseno) from llclaim where givetype is null");
//		fm.all('get6').value = temp;
//		var b = temp*100/sum;
//		fm.rget6.value = b.toString().substring(0,4);  
}

function clearP()
{
			for(var i = 1; i < 6; i++)
		{
				
						fm.all("getP"+i).value = "";
					
						fm.all("rgetP"+i).value = "";								
		}
}

//原始信息查询页面
function ShowInfoPage()
{
	var selno = ClaimPayGrid.getSelNo();//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = ClaimPayGrid.getRowColData(selno-1,1);
	OpenWindowNew("./ClaimUnderwriteInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
}    

//保单查询
function ContInfoPage()
{
		var selno = ClaimPayGrid.getSelNo();//alert(CheckGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = ClaimPayGrid.getRowColData(selno-1,1);
		var ContNo = easyExecSql("select distinct grpcontno from llclaimpolicy where caseno = '"+CaseNo+"'");
		if(ContNo=='00000000000000000000')
		{
			ContNo = easyExecSql("select distinct contno from llclaimpolicy where caseno = '"+CaseNo+"'");
			window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
			}else
				{
					window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
	      }
}