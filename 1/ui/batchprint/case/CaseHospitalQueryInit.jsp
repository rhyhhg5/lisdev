<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
  var turnPage = new turnPageClass();
  var turnPage2 = new turnPageClass();
  function initInpBox()
  {
    try
    {
//     fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
//     fm.CaseRelaNo.value = '<%= request.getParameter("CaseRelaNo") %>';
//     fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
//     fm.CustomerNo.value ="<%=request.getParameter("CustomerNo")%>"
     fm.MngCom.value = comCode;

    }
    catch(ex)
    {
      alert("在LLClaimDetailInputInit.jsp-->InitInpBox函数中发生异常：初始化界面错误!");
    }
  }
  function initSelBox()
  {
    try
    {
    }
    catch(ex)
    {
      alert("在LLClaimDetailInputInit.jsp-->InitSelBox函数中发生异常：初始化界面错误!");
    }
  }
  function initForm()
  {
    try
    {
    	initInpBox();
      initClaimDetailGrid();
      initClaimInfoGrid();
    }
    catch(re)
    {
      alert("LLClaimDetailInputInit.jsp-->InitForm函数中发生异常111：初始化界面错误!");
    }
  }


//立案分案明细
  function initClaimDetailGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";
      iArray[0][1]="30px";
      iArray[0][2]=40;
      iArray[0][3]=0;

	iArray[0]=new Array("序号","30px","0",0);
	iArray[1]=new Array("医院名称","200px","0",0);
	iArray[2]=new Array("医院代码","60px","0",0);
	iArray[3]=new Array("机构","30px","0",0);
	iArray[4]=new Array("客户号","75px","0",0);
	iArray[5]=new Array("姓名","50px","0",0);
	iArray[6]=new Array("案件号","125px","0",0);
	iArray[7]=new Array("账单时间","75px","0",0);
	iArray[8]=new Array("住院日","75px","0",0);
	iArray[9]=new Array("案件结果","60px","0",0);
	iArray[10]=new Array("状态","30px","0",0);   

      ClaimDetailGrid = new MulLineEnter( "fm" , "ClaimDetailGrid" );
      ClaimDetailGrid.displayTitle = 1;
      ClaimDetailGrid.mulLineCount = 2;
      ClaimDetailGrid.canSel =0;
      ClaimDetailGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      ClaimDetailGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)

	  ClaimDetailGrid.selBoxEventFuncName="selEvent";

      ClaimDetailGrid.loadMulLine(iArray);
      ClaimDetailGrid.lock ();
    }
    catch(ex)
    {
      alert(ex);
    }
  }

//分案信息明细
  function initClaimInfoGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";
      iArray[0][1]="30px";
      iArray[0][2]=40;
      iArray[0][3]=0;

			iArray[0]=new Array("序号","30px","0",0);
			iArray[1]=new Array("医院名称","260px","0",0);
			iArray[2]=new Array("医院代码","70px","0",0);
			iArray[3]=new Array("机构","0px","0",3);
			iArray[4]=new Array("理赔件数","60px","0",0);
			iArray[5]=new Array("件数占比(%)","80px","0",0);
			iArray[6]=new Array("理赔金额","80px","0",0);
			iArray[7]=new Array("金额占比(%)","80px","0",0);

      ClaimInfoGrid = new MulLineEnter( "fm" , "ClaimInfoGrid" );
      ClaimInfoGrid.displayTitle = 1;
      ClaimInfoGrid.mulLineCount = 0;
      ClaimInfoGrid.canSel =0;
      ClaimInfoGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      ClaimInfoGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)

	  	ClaimInfoGrid.selBoxEventFuncName="";

      ClaimInfoGrid.loadMulLine(iArray);
      ClaimInfoGrid.lock ();
    }
    catch(ex)
    {
      alert(ex);
    }
  }


</script>