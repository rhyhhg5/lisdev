<%
//程序名称：ClaimRecQuery.jsp
//程序功能：既往理赔信息查询
//创建日期：2006-11-13 11:10:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="ClaimRecQuery.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="ClaimRecQueryInit.jsp"%>
		<title>理赔给付记录查询 </title>
	</head>
	<body  onload="initForm();initDate();" >
		<form method=post name=fm target="fraSubmit">
			<table class= common border=0 width=100%>
				<tr>
					<td class= titleImg align= center>客户信息</td>
				</tr>
			</table>
			<table  class= common align=center>
				<TR  class= common>
					<TD  class= title>客户姓名 </TD>
					<TD  class= input> <Input class= common name=CustomerName onkeydown="QueryOnKeyDown()"> </TD>
					<TD  class= title> 客户号码</TD>
					<TD  class= input> <Input class= common name=CustomerNo onkeydown="QueryOnKeyDown()"> </TD>
				</TR>
			</table>
			<INPUT VALUE="返  回" name=Return class = CssButton TYPE=button STYLE="display:none" onclick="returnParent();">
			<hr>
			<table class=common>
				<tr class=common>
					<td class=titleImg><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLC1);">历史赔案信息</td>
					<td class=title style="width:105px">
						门诊赔付金额累计
					</td>
					<td class=input style="width:70px">
						<input class= readonly readonly name=DoorTotalPay  style="width:70px">
					</td>
					<td class=title style="width:105px">
						住院赔付金额累计
					</td>
					<td class=input style="width:70px">
						<input class= readonly readonly name=InHosTotalPay  style="width:70px">
					</td>
					<td class=title style="width:105px">
						年度累计住院天数
					</td>
					<td class=input style="width:70px">
						<input class= readonly readonly name=TotalDays  style="width:70px">
					</td>
				</tr>
			</table>
			<Div  id= "divLC2" style= "display: ''" align = center>
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanCaseGrid" >
							</span>
						</td>
					</tr>
				</table>
				<br>
			</div>
			<hr>

			<Div  id= "divLC1" style= "display: ''" align = center>
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanClaimPayGrid" >
							</span>
						</td>
					</tr>
				</table>
				<br>
			</div>
			<table  style= "display: ''">
				<tr>
					<td>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivInfo);">
					</td>
					<td class= titleImg>
						险种赔付关联信息(注：门诊住院混合赔付的险种无法拆分显示门诊/住院赔付金额)
					</td>
				</tr>
			</table>
			<Div id= DivInfo>
				<table class= common>
					<tr class=common>
						<td class=title>门诊帐单数</td>
						<td class=input><Input readonly class= common name=DoorFeeCount ></td>
						<td class=title>门诊帐单金额</td>
						<td class=input><Input readonly class= common name=DoorFee ></td>
						<td class=title>门诊赔付金额</td>
						<td class=input><Input readonly class= common name=DoorFeePay ></td>
					</tr>
					<tr class=common>
						<td class=title>住院帐单数</td>
						<td class=input><Input readonly class= common name=InHosFeeCount ></td>
						<td class= title>住院帐单金额</td>
						<td class= input><Input readonly class= common name=InHosFee ></td>
						<td class= title>住院赔付金额</td>
						<td class= input><Input readonly class= common name=InHosFeePay ></td>
					</tr>

				</table>
			</Div>
			<hr>
	<TD  class= title8>案件信息查询</TD>
	<TD  class= input8> <input class=codeno CodeData="0|6^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定"  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input class=codename name=DealWithName></TD>
			<input type=hidden class=cssButton value="保单查询" OnClick="ContInfoPage();">
			<Input type="hidden" class= common name="ComCode" >
			</form>
			<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
			</body>
			</html>
