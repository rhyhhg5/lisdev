
<html>
<%
/*******************************************************************************
 * Name     :ClaimDetailInput.jsp
 * Function :立案－立案险种明细的初始化页面程序
 * Date     :2003-7-21
 * Author   :LiuYansong
 */
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.llcase.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
		
		String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  	  
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
    String AheadDays="-30";
        FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
		System.out.println("管理机构-----"+tG.ComCode);
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

  <SCRIPT src="CaseHospitalQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CaseHospitalQueryInit.jsp"%>

<script language="javascript">
	 var comCode = "<%=Comcode%>";
   function initDate(){
   		fm.RgtDateS.value="<%=afterdate%>";
   		fm.RgtDateE.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
   		
   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
</script>

</head>
<body  onload="initForm();initDate();" >
  <form action="./ClaimDetailSave.jsp" method=post name=fm target="fraSubmit">

  <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClientInfoTitle);">
    	</td>
    	<td class= titleImg>
    	 查询条件 
    	</td>
    </tr>
    </table>
    <div id= "divClientInfoTitle" style= "display: ''" >
<table  class= common>
<TR  class= common8>

    <TD  class= title id='titleCaseNo'> 管理机构</TD><TD  class= input>
  <Input class= "codeno"  name=MngCom  ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1],null,null,null,1);" ><Input class=codename  name=MngComName>
	
  <TD  class= title8>就诊时间范围</TD>
		<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title8>到</TD>
		<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
</TR>
<TR  class= common8>
<TD  class= title8><INPUT TYPE=Button class=cssButton value="查 询" onclick="easyQuery();"></TD>
</TR>
</table>

     
    <!--立案保单信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClaimDetail);">
    		</td>
    		<td class= titleImg>
    			 医院列表
    		</td>
    	</tr>
    </table>
	<Div  id= "divClaimDetail" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanClaimDetailGrid" >
					</span>
				</td>
			</tr>
		</table>
			 <Div  align=center style= "display: '' ">  
			    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		   </Div>    
  </div>
  <hr>
  <!--立案保单信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClaimInfo);">
    		</td>
    		<td class= titleImg>
    			 结果统计
    		</td>
    		&nbsp&nbsp&nbsp&nbsp&nbsp<td class= common><INPUT TYPE=Button class=cssButton value="查 询" onclick="hosInfo();"></td>
    	</tr>
    </table>
	<Div  id= "divClaimInfo" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanClaimInfoGrid" >
					</span>
				</td>
			</tr>
		</table>
			 <Div  align=center style= "display: '' ">  
			    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
			    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
			    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
			    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
		   </Div>    
  </div>
  

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>