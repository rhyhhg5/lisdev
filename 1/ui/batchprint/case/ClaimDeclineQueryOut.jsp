<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ClaimDeclineQueryOut.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

System.out.println("---hahahahah---");
  // 保单信息部分
  LLClaimDeclineSchema tLLClaimDeclineSchema   = new LLClaimDeclineSchema();
  
    tLLClaimDeclineSchema.setDeclineNo(request.getParameter("DeclineNo"));
    tLLClaimDeclineSchema.setDeclineType(request.getParameter("DeclineType"));
    tLLClaimDeclineSchema.setRelationNo(request.getParameter("RelationNo"));
    
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLLClaimDeclineSchema);

  // 数据传输
  ClaimDeclineUI tClaimDeclineUI   = new ClaimDeclineUI();
	if (!tClaimDeclineUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tClaimDeclineUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tClaimDeclineUI.getResult();
		
		// 显示
		LLClaimDeclineSet mLLClaimDeclineSet = new LLClaimDeclineSet(); 
		mLLClaimDeclineSet.set((LLClaimDeclineSet)tVData.getObjectByObjectName("LLClaimDeclineBLSet",0));
		int n = mLLClaimDeclineSet.size();
		System.out.println("get ClaimDecline "+n);
		for (int i = 1; i <= n; i++)
		{
		  	LLClaimDeclineSchema mLLClaimDeclineSchema = mLLClaimDeclineSet.get(i);
		   	%>
		   	<script language="javascript">
		   	  parent.fraInterface.ClaimDeclineGrid.addOne("ClaimDeclineGrid")
		   		parent.fraInterface.fm.ClaimDeclineGrid1[<%=i-1%>].value="<%=mLLClaimDeclineSchema.getDeclineNo()%>";
		   		parent.fraInterface.fm.ClaimDeclineGrid2[<%=i-1%>].value="<%=mLLClaimDeclineSchema.getDeclineType()%>";
		   		parent.fraInterface.fm.ClaimDeclineGrid3[<%=i-1%>].value="<%=mLLClaimDeclineSchema.getRelationNo()%>";
		   		parent.fraInterface.fm.ClaimDeclineGrid4[<%=i-1%>].value="<%=mLLClaimDeclineSchema.getMngCom()%>";
		   		parent.fraInterface.fm.ClaimDeclineGrid5[<%=i-1%>].value="<%=mLLClaimDeclineSchema.getOperator()%>";
			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tClaimDeclineUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

