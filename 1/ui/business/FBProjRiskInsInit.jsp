<script language="JavaScript">

function initProjForm(){
	try{
		fm.ProjCode.value = '';
		fm.ProjName.value = '';
		divProjRiskIns.style.display = 'none';
    	initProjRiskGrid();
	}
	catch(re){
		alert("在JProjectQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
	}
}

/*
function initInpBox(){
	
	fm.ProjCode.value = "";
	fm.ProjName.value = "";
	fm.ClientNo.value = "";
	fm.ClientName.value = "";
	fm.KindCode.value = "";
	fm.KindName.value = "";
	fm.RiskCode.value = "";
	fm.RiskName.value = "";
	fm.BidFlagName.value = "";
	fm.SubmitFlagName.value = "";
    fm.SubmitFlag.value = "";
    fm.Remark.value = "";
    fm.RemarkDsp.value ="";
	
	fm.UploadFile.disabled = true;
	
}
*/

function initProjRiskGrid()
{
	var iArray = new Array();    

	try
	{
		var strWidth  = screen.width-15;
		var strHeight = screen.height-70;
		var strTop = 0;
		var strLeft = 0;
		//序号 项目名称 客户编码 客户名称 项目状态 项目所属部门 项目创建日期 
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="5";
		iArray[0][2]=100;
		iArray[0][3]=0;
		
		iArray[1]=new Array();
		iArray[1][0]="保险公司";
		iArray[1][1]="20";
		iArray[1][2]=100;
		iArray[1][3]=0;

		
   		iArray[2]=new Array();
		iArray[2][0]="险种";
		iArray[2][1]="20";
		iArray[2][2]=100;
		iArray[2][3]=0;
	
		iArray[3]=new Array();
		iArray[3][0]="保额";
		iArray[3][1]="20";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="保费";
		iArray[4][1]="20";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		ProjRiskGrid = new MulLineEnter("fm","ProjRiskGrid"); 
		//这些属性必须在loadMulLine前
	
		
		ProjRiskGrid.locked=0;      
		ProjRiskGrid.mulLineCount = 0;   
		ProjRiskGrid.displayTitle = 1;
		ProjRiskGrid.canSel=0;
		ProjRiskGrid.canChk=0;
		ProjRiskGrid.hiddenPlus = 1;
		ProjRiskGrid.hiddenSubtraction = 1;
		ProjRiskGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex.message);
	}
}


</script>