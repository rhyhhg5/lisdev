var tempImage="C:/cut_tmp.jpg";

//计数器页面刷新的时候是自动初始化为0
var Count=0;
var JudgeFlag = true;
function showWindow(oArg,Flag){
	Count+=1;
	JudgeFlag=Flag;
   var ele = getAbsolutePos(oArg);
   //获得div的高度，这里固定为83
   var top = 83;
   //var top = 150;
   //设置层的y坐标
   document.getElementById("win").style.top = (ele.y - oArg.offsetTop - top);
   //层的x坐标不变
   //document.getElementById("win").style.left = ele.x;
   //暂时先固定悬浮层的x坐标
   document.getElementById("win").style.left = 10;
   //document.getElementById("imgpart").src = "";
   //显示图片
   //document.getElementById("imgpart").src = tempImage;
   //第一次获取截图的时候会有延时更长所以等待2秒
   if(Count==1){
   	setTimeout('List()',2200);
   }else{
   	setTimeout('List()',100);
   }
   //显示层
   //document.getElementById("win").style.display = "";
}

function List()
{
	//显示图片
	//document.frames['fraImgpart'].location.reload();
	//清除缓存先指向空
	document.getElementById("imgpart").src = "";
	//显示图片
	document.getElementById("imgpart").src = tempImage;
	//传过来的报文不为空的显示图片层
	if(JudgeFlag){
		//显示层
	document.getElementById("win").style.display = "";
	}
	//document.getElementById("win").style.display = "";
}
// 获取控件绝对位置
function getAbsolutePos(ele){
	var sLeft = 0, sTop = 0;
	if (ele.scrollLeft && ele.scrollTop){
		sLeft = ele.scrollLeft;
		sTop = ele.scrollTop;
	}
	
	var r = { x: ele.offsetLeft - sLeft, y: ele.offsetTop - sTop };
	
	if(ele.offsetParent){
		var tmp = getAbsolutePos(ele.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
}

function showPageSpe(img,spanID)
{
	if(spanID.style.display=="")
	{
		//关闭
		spanID.style.display="none";
		img.src="../common/images/butCollapse.gif";
		document.getElementById("win").style.display="none";
	}
	else
	{
		//打开
		spanID.style.display="";
		img.src="../common/images/butExpand.gif";
		document.getElementById("win").style.display="none";
	}
}
//因为录入界面的字段名和百星表的字段名不一样，所以转化成接口截图时候能识别的ID
function Reflect(OldId)
{
	var id ="";
	//车牌号
	if(OldId=="LicenseNo")
	{
		id ="LicensePlateNumber";
	}else if(OldId=="SupplierShowCode")
	{
		id ="InsuranceCompany";
	}else if(OldId=="ContNo")
	{
		id ="PolicyNO";
	}else if(OldId=="BussNature")
	{
		id ="";
	}else if(OldId=="UWDate")
	{
		id ="SignDate";
	}else if(OldId=="SignDate")
	{
		id ="PrintDate";
	}else if(OldId=="hour")
	{
		id ="PrintDate";
	}else if(OldId=="minute")
	{
		id ="PrintDate";
	}else if(OldId=="CValiDate")
	{
		id ="DurationOfInsurance";
	}else if(OldId=="CInValiDate")
	{
		id ="DurationOfInsurance";
	}else if(OldId=="POSNo")
	{
		id ="POS";
	}else if(OldId=="InsuredCustType")
	{
		id ="";
	}else if(OldId=="InsuredName")
	{
		id ="Name";
	}else if(OldId=="InsuredIDType")
	{
		id ="IDType";
	}else if(OldId=="InsuredIDNo")
	{
		id ="ID";
	}else if(OldId=="CarOwner")
	{
		id ="Owner";
	}else if(OldId=="PurchasePrice")
	{
		id ="PurchasesPrice";
	}else if(OldId=="UseNatureCode")
	{
		id ="Useage";
	}else if(OldId=="OutRiskCode")
	{
		id ="RiskName";
	}else if(OldId=="SumPrem")
	{
		id ="Premium";
	}else if(OldId=="tax1")
	{
		id ="VehicleAndVesselTax";
	}else if(OldId=="FrameNo")
	{
		id ="FrameNo";
	}else if(OldId=="EngineNo")
	{
		id ="EngineCode";
	}else if(OldId=="CarKindCode")
	{
		id ="Kind";
	}else if(OldId=="EnrollDate")
	{
		id ="RegisterDate";
	}else if(OldId=="ModelCode")
	{
		id ="Model";
	}else if(OldId=="SeatCount1")
	{
		id ="Passengers";
	}else if(OldId=="TonCount")// 
	{
		id ="CarryWeight";
	}else if(OldId=="PolicyPli")
	{
		id ="SpecialAgreement";
	}else if(OldId=="KindGrid1")//险别id
	{
		id ="ReserveColumn1";
	}
	else{
		id ="";
	}
	return id;
}
//鼠标离开按钮后事件
document.onmouseup = function (){
	input = window.event||input;
	input = input.srcElement||input.target;
	//当点击刷新，暂存，保单录入完毕按钮后 重新置计数器的值。
	if(input.name=="butFresh"){
		Count=0;
		fm.reset();
		fm.Policy.value="";
		showWinFlag = false;
	}else if(input.name=="butQuery"){
		Count=0;
	}else if(input.name=="butSave" || input.name=="butEnd"){
		Count=0;
		showWinFlag = false;
	}
}


