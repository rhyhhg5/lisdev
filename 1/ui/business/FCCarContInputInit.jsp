<%
//程序名称：FCPropContInputInit.jsp
//程序功能：
//创建日期：
//创建人  ：guyd
//更新记录：更新人    更新日期     更新原因/内容
%>                       
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
	try
	{
		fm.reset();
		fm.InnerContNo.value ='';
		fm.PolicyPrtNo.value ='18000000001';
		fm.RightSendFlag.value = "N";
		fm.RightSendFlagName.value = "否";
		fm.AccreditFlag.value = "N";
		fm.AccreditFlagName.value = "否";
		fm.UseNatureCode.value = "200";
		fm.UseNatureName.value = "非营业";
		fm.IsSingle1.value = "N";//默认为N	
		fm.State.value = <%=request.getParameter("state")%>;
		showCont();
		
	}
	catch(ex)
	{
		alert("在FCPropContInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}

function initButton()
{
	
}
                                     
function initForm()
{
  try
  {
    initInpBox();
    initButton();
    // initKindGrid();
  }
  catch(re)
  {
    alert("在FCPropContInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
}


function initKindGrid()
{                               
	var iArray = new Array();
	  
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="4";            			//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="险别编码";         			//列名
		iArray[1][1]="8";            			//列宽
		iArray[1][2]=50;            			//列最大值
		iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许    
		iArray[1][4]="riskkind";
		iArray[1][5]="1|2";
		iArray[1][6]="0|1";
		iArray[1][15]="1";
		iArray[1][16]=fm.RiskCode.value;
		iArray[1][9]="险别编码|notnull&len<=20";
		
		iArray[2]=new Array();
		iArray[2][0]="险别名称";         			//列名
		iArray[2][1]="8";            			//列宽
		iArray[2][2]=6;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="保额";         			//列名
		iArray[3][1]="8";            			//列宽
		iArray[3][2]=50;            			//列最大值
		iArray[3][3]=1;
		iArray[3][9]="保额|NUM&value>=0&Money:15-2";
		
		iArray[4]=new Array();
		iArray[4][0]="保费";         			//列名
		iArray[4][1]="8";            			//列宽
		iArray[4][2]=60;            			//列最大值
		iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		iArray[4][9]="保费|NUM&Money:15-2";
		
		
		KindGrid = new MulLineEnter( "fm" , "KindGrid" ); 
		//这些属性必须在loadMulLine前
		KindGrid.locked = 0;
		KindGrid.mulLineCount = 0;   
		KindGrid.displayTitle = 1;
		KindGrid.canSel = 0;
		KindGrid.hiddenPlus=0;
		KindGrid.hiddenSubtraction=0;
		KindGrid.canChk=0;
		KindGrid.loadMulLine(iArray);  

	}
	catch(ex)
	{
	alert(ex);
	}
}
</script>