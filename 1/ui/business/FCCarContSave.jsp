<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.CErrors"%>
<%@page import="com.sinosoft.utility.VData"%>
<%@page import="com.sinosoft.lis.schema.FCContSchema"%>
<%@page import="com.sinosoft.lis.schema.FCPolSchema"%>
<%@page import="com.sinosoft.lis.db.FCContDB"%>
<%@page import="com.sinosoft.lis.db.FCPolDB"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.utility.TransferData"%>
<%
String FlagStr = "";
String Content = "";
CErrors tError = null;

String tOperate = request.getParameter("hideOperate");
TransferData td = new TransferData();
/**从这里开始是车险保单随动录入需要的隐藏帧  begin**/
//影像随动录入主键ID
td.setNameAndValue("Policy", request.getParameter("Policy"));
//保单类型
td.setNameAndValue("PolicyType", request.getParameter("PolicyType"));
//被保人性别
td.setNameAndValue("InsuredSex", request.getParameter("InsuredSex1"));
//被保人性别名称
td.setNameAndValue("InsuredSexName", request.getParameter("InsuredSexName1"));
//被保人地址
td.setNameAndValue("InsuredPostalAddress", request.getParameter("InsuredPostalAddress1"));
//被保人生日
td.setNameAndValue("InsuredBirthday", request.getParameter("InsuredBirthday1"));
//被保人地址电子邮件
td.setNameAndValue("InsuredEMail", request.getParameter("InsuredEMail1"));
//被保人手机号
td.setNameAndValue("InsuredMobile", request.getParameter("InsuredMobile1"));
//被保人家庭电话号码
td.setNameAndValue("InsuredHomePhone", request.getParameter("InsuredHomePhone1"));
//被保人办公室电话号码
td.setNameAndValue("InsuredPhone", request.getParameter("InsuredPhone1"));

//ScanFlag区分是否是影像随动录入的标记
td.setNameAndValue("ScanFlag", request.getParameter("ScanFlag"));
//获得本机应用端口号
td.setNameAndValue("ServerPort", Integer.parseInt(String.valueOf(request.getServerPort())));
// 从这里开始是车险保单随动录入需要的隐藏帧  end 
td.setNameAndValue("isscanflag", request.getParameter("isscanflag"));
// 判断该保单是否有影响件 1：有影响件 0：没有影响件
//GlobalInput tG = new GlobalInput();
//tG=(GlobalInput)session.getValue("GI");
FCContSchema tFCContSchema = new FCContSchema();
FCPolSchema tFCPolSchema = new FCPolSchema();
String tInnerContNo = request.getParameter("InnerContNo");
String tItemNo = request.getParameter("ItemNo");
String tPolNo = request.getParameter("PolNo");

if(tInnerContNo!=null&&!tInnerContNo.equals("")&&tOperate.equals("INSERT||MAIN"))
{
	tOperate = "UPDATE||MAIN";
	FCContDB tFCContDB = new FCContDB();
	tFCContDB.setInnerContNo(tInnerContNo);
	if (tFCContDB.getInfo()) 
	{
		tFCContSchema.setSchema(tFCContDB.getSchema());
	}
	
	
  	FCPolDB tFCPolDB = new FCPolDB();
  	tFCPolDB.setInnerContNo(tInnerContNo);
  	tFCPolDB.setPolNo(tPolNo);
	if (tFCPolDB.getInfo()) 
	{
		tFCPolSchema.setSchema(tFCPolDB.getSchema());
	}
	
	//tFCContSchema.setState("0");
	//tFCPolSchema.setState("0");
	
}

if(tInnerContNo!=null&&!tInnerContNo.equals("")&&tOperate.equals("CONFIRM||INSERT"))
{
	tOperate = "CONFIRM||UPDATE";
	FCContDB tFCContDB = new FCContDB();
	tFCContDB.setInnerContNo(tInnerContNo);
	if (tFCContDB.getInfo()) 
	{
		tFCContSchema.setSchema(tFCContDB.getSchema());
	}
	
  	FCPolDB tFCPolDB = new FCPolDB();
  	tFCPolDB.setInnerContNo(tInnerContNo);
  	tFCPolDB.setPolNo(tPolNo);
	if (tFCPolDB.getInfo()) 
	{
		tFCPolSchema.setSchema(tFCPolDB.getSchema());
	}
//	tFCContSchema.setState("1");
	//tFCPolSchema.setState("1");
}

tFCContSchema.setInnerContNo(request.getParameter("InnerContNo"));
//tFCContSchema.setInnerGrpContNo(request.getParameter("InnerGrpContNo"));
//tFCContSchema.setGrpContNo(request.getParameter("GrpContNo"));
tFCContSchema.setContNo(request.getParameter("ContNo"));
tFCContSchema.setProposalContNo(request.getParameter("ContNo"));
tFCContSchema.setProjCode(request.getParameter("ProjCode"));
tFCContSchema.setSupplierCode(request.getParameter("SupplierCode"));
tFCContSchema.setPolicyPrtNo(request.getParameter("PolicyPrtNo"));
tFCContSchema.setInvoicePrtNo(request.getParameter("InvoicePrtNo"));
tFCContSchema.setInsCardPrtNo(request.getParameter("InsCardPrtNo"));
tFCContSchema.setCInsFlagPrtNo(request.getParameter("CInsFlagPrtNo"));
//tFCContSchema.setBizType(request.getParameter("BizType"));
//tFCContSchema.setContType(request.getParameter("ContType"));
//tFCContSchema.setBussProperty(request.getParameter("BussProperty"));
tFCContSchema.setBussNature(request.getParameter("BussNature"));
//tFCContSchema.setContSource(request.getParameter("ContSource"));
//tFCContSchema.setFamilyType(request.getParameter("FamilyType"));
//tFCContSchema.setCardFlag(request.getParameter("CardFlag"));
//tFCContSchema.setCorporation(request.getParameter("Corporation"));
//tFCContSchema.setBranchOffice(request.getParameter("BranchOffice"));
//tFCContSchema.setMgmtDivision(request.getParameter("MgmtDivision"));
//tFCContSchema.setManageCom(request.getParameter("ManageCom"));
tFCContSchema.setAgentGroup(request.getParameter("AgentGroup"));
//tFCContSchema.setAgentCom(request.getParameter("AgentCom"));
tFCContSchema.setOutManageCom(request.getParameter("OutManageCom"));
tFCContSchema.setGetManageCom(request.getParameter("GetManageCom"));
tFCContSchema.setPOSNo(request.getParameter("POSNo"));

//tFCContSchema.setPayPCommFlag(request.getParameter("PayPCommFlag"));
//tFCContSchema.setPayPComm(request.getParameter("PayPComm"));
//tFCContSchema.setAppntCustType(request.getParameter("AppntCustType"));
//tFCContSchema.setAppntNo(request.getParameter("AppntNo"));
//tFCContSchema.setAppntAddressNo(request.getParameter("AppntAddressNo"));
//tFCContSchema.setAppntName(request.getParameter("AppntName"));
//tFCContSchema.setAppntSex(request.getParameter("AppntSex"));
//tFCContSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
//tFCContSchema.setAppntIDType(request.getParameter("AppntIDType"));
//tFCContSchema.setAppntIDNo(request.getParameter("AppntIDNo"));
//tFCContSchema.setAppntOccup(request.getParameter("AppntOccup"));
//tFCContSchema.setAppntPostalAddress(request.getParameter("AppntPostalAddress"));
//tFCContSchema.setAppntZipCode(request.getParameter("AppntZipCode"));
//tFCContSchema.setAppntMobile(request.getParameter("AppntMobile"));
//tFCContSchema.setAppntPhone(request.getParameter("AppntPhone"));
tFCContSchema.setInsuredCustType(request.getParameter("InsuredCustType"));
tFCContSchema.setInsuredNo(request.getParameter("InsuredNo"));
//tFCContSchema.setInsuredAddressNo(request.getParameter("InsuredAddressNo"));
tFCContSchema.setInsuredName(request.getParameter("InsuredName"));
//tFCContSchema.setInsuredSex(request.getParameter("InsuredSex"));
//tFCContSchema.setInsuredBirthday(request.getParameter("InsuredBirthday"));
tFCContSchema.setInsuredIDType(request.getParameter("InsuredIDType"));
tFCContSchema.setInsuredIDNo(request.getParameter("InsuredIDNo"));
//tFCContSchema.setInsuredOccup(request.getParameter("InsuredOccup"));
//tFCContSchema.setInsuredPostalAddress(request.getParameter("InsuredPostalAddress"));
//tFCContSchema.setInsuredZipCode(request.getParameter("InsuredZipCode"));
//tFCContSchema.setInsuredMobile(request.getParameter("InsuredMobile"));
//tFCContSchema.setInsuredPhone(request.getParameter("InsuredPhone"));
//tFCContSchema.setInsureAppntRelation(request.getParameter("InsureAppntRelation"));
//tFCContSchema.setPayMode(request.getParameter("PayMode"));
tFCContSchema.setPayBank(request.getParameter("Policy"));//百星保单流水号
//tFCContSchema.setPayBankAccNo(request.getParameter("PayBankAccNo"));
//tFCContSchema.setPayAccName(request.getParameter("PayAccName"));
//tFCContSchema.setGetBank(request.getParameter("GetBank"));
//tFCContSchema.setGetBankAccNo(request.getParameter("GetBankAccNo"));
//tFCContSchema.setGetAccName(request.getParameter("GetAccName"));

//tFCContSchema.setBrokerCode(request.getParameter("BrokerCode"));
tFCContSchema.setAgentCode(request.getParameter("AgentCode"));
//tFCContSchema.setState(request.getParameter("State"));
//tFCContSchema.setPolState(request.getParameter("PolState"));
//tFCContSchema.setErrorTimes(request.getParameter("ErrorTimes"));
//tFCContSchema.setExamState(request.getParameter("ExamState"));
//tFCContSchema.setAddCard(request.getParameter("AddCard"));
//tFCContSchema.setReInvoiceDeliverFlag(request.getParameter("ReInvoiceDeliverFlag"));
//tFCContSchema.setLastNoteDate(request.getParameter("LastNoteDate"));
//tFCContSchema.setLastEdorDate(request.getParameter("LastEdorDate"));
//tFCContSchema.setLastClaimDate(request.getParameter("LastClaimDate"));
tFCContSchema.setSignDate(request.getParameter("SignDate"));
tFCContSchema.setSignTime(request.getParameter("SignTime"));
tFCContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));
tFCContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
tFCContSchema.setReceiveTime(request.getParameter("ReceiveTime"));
//tFCContSchema.setPolApplyDate(request.getParameter("PolApplyDate"));
tFCContSchema.setCValiDate(request.getParameter("CValiDate"));
tFCContSchema.setCInValiDate(request.getParameter("CInValiDate"));
//tFCContSchema.setInputOperator(request.getParameter("InputOperator"));
//tFCContSchema.setInputDate(request.getParameter("InputDate"));
//tFCContSchema.setInputTime(request.getParameter("InputTime"));
//tFCContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));
//tFCContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
//tFCContSchema.setReceiveTime(request.getParameter("ReceiveTime"));
//tFCContSchema.setCancelDate(request.getParameter("CancelDate"));
tFCContSchema.setRemark(request.getParameter("Remark").replaceAll("@E!"," "));
//tFCContSchema.setPolType(request.getParameter("PolType"));
//tFCContSchema.setPayIntv(request.getParameter("PayIntv"));
//tFCContSchema.setSignCom(request.getParameter("SignCom"));
//tFCContSchema.setPrintCount(request.getParameter("PrintCount"));
//tFCContSchema.setLostTimes(request.getParameter("LostTimes"));
//tFCContSchema.setPeoples(request.getParameter("Peoples"));
//tFCContSchema.setMult(request.getParameter("Mult"));
//tFCContSchema.setAmnt(request.getParameter("Amnt"));
tFCContSchema.setSumPrem(request.getParameter("SumPrem"));
tFCContSchema.setContPrem(request.getParameter("SumPrem"));//原始保单保费
//tFCContSchema.setPaytoDate(request.getParameter("PaytoDate"));
//tFCContSchema.setFirstPayDate(request.getParameter("FirstPayDate"));
//tFCContSchema.setAppFlag(request.getParameter("AppFlag"));
//tFCContSchema.setGetPolDate(request.getParameter("GetPolDate"));
//tFCContSchema.setGetPolTime(request.getParameter("GetPolTime"));
//tFCContSchema.setCustomGetPolDate(request.getParameter("CustomGetPolDate"));
//tFCContSchema.setPostalAddress(request.getParameter("PostalAddress"));
//tFCContSchema.setZipCode(request.getParameter("ZipCode"));
//tFCContSchema.setPostalAddressFlag(request.getParameter("PostalAddressFlag"));
//tFCContSchema.setImpawnFlag(request.getParameter("ImpawnFlag"));
//tFCContSchema.setPassword(request.getParameter("Password"));
//tFCContSchema.setOperator(request.getParameter("Operator"));
//tFCContSchema.setMakeDate(request.getParameter("MakeDate"));
//tFCContSchema.setMakeTime(request.getParameter("MakeTime"));
//tFCContSchema.setModifyDate(request.getParameter("ModifyDate"));
//tFCContSchema.setModifyTime(request.getParameter("ModifyTime"));
tFCContSchema.setProtocolNo(request.getParameter("ProtocolNo"));
//tFCContSchema.setSendPolDate(request.getParameter("SendPolDate"));
//tFCContSchema.setSendReceptionDate(request.getParameter("SendReceptionDate"));
//tFCContSchema.setInnerMasterContNo(request.getParameter("InnerMasterContNo"));
//tFCContSchema.setOrphanFlag(request.getParameter("OrphanFlag"));
tFCContSchema.setAgentFlag(request.getParameter("AgentFlag"));
tFCContSchema.setFirstFeeFlag(request.getParameter("FirstFeeFlag"));
//tFCContSchema.setCarFlag(request.getParameter("CarFlag"));
//tFCContSchema.setSupplierGetPolDate(request.getParameter("SupplierGetPolDate"));
//tFCContSchema.setIRate(request.getParameter("IRate"));
//tFCContSchema.setHandlingNo(request.getParameter("HandlingNo"));
//tFCContSchema.setHandlingAddressNo(request.getParameter("HandlingAddressNo"));
//tFCContSchema.setHandlingName(request.getParameter("HandlingName"));
//tFCContSchema.setHandlingSex(request.getParameter("HandlingSex"));
//tFCContSchema.setHandlingBirthday(request.getParameter("HandlingBirthday"));
//tFCContSchema.setHandlingIDType(request.getParameter("HandlingIDType"));
//tFCContSchema.setHandlingIDNo(request.getParameter("HandlingIDNo"));
//tFCContSchema.setHandlingOccup(request.getParameter("HandlingOccup"));
//tFCContSchema.setHandlingPostalAddress(request.getParameter("HandlingPostalAddress"));
//tFCContSchema.setHandlingZipCode(request.getParameter("HandlingZipCode"));
//tFCContSchema.setHandlingMobile(request.getParameter("HandlingMobile"));
//tFCContSchema.setHandlingPhone(request.getParameter("HandlingPhone"));
//tFCContSchema.setPremPrintTimes(request.getParameter("PremPrintTimes"));
//tFCContSchema.setOldSupplierCode(request.getParameter("OldSupplierCode"));
//tFCContSchema.setOldContNo(request.getParameter("OldContNo"));
//tFCContSchema.setFeePrintFlag(request.getParameter("FeePrintFlag"));
//tFCContSchema.setFeePrintTimes(request.getParameter("FeePrintTimes"));
//tFCContSchema.setCancelReason(request.getParameter("CancelReason"));
//tFCContSchema.setFirstPay(request.getParameter("FirstPay"));
//tFCContSchema.setAmount(request.getParameter("Amount"));
//tFCContSchema.setAmountRemark(request.getParameter("AmountRemark"));
//tFCContSchema.setSendContDate(request.getParameter("SendContDate"));
//tFCContSchema.setAppntMobile2(request.getParameter("AppntMobile2"));
//tFCContSchema.setInsuredMobile2(request.getParameter("InsuredMobile2"));
//tFCContSchema.setInputCom(request.getParameter("InputCom"));
//tFCContSchema.setReceiveCom(request.getParameter("ReceiveCom"));
tFCContSchema.setLicenseNo(request.getParameter("LicenseNo").trim());
tFCContSchema.setComFirstFeeFlag(request.getParameter("ComFirstFeeFlag"));
tFCContSchema.setUWDate(request.getParameter("UWDate"));
//tFCContSchema.setInsuredSupplierOccuCode(request.getParameter("InsuredSupplierOccuCode"));
//tFCContSchema.setAppntSupplierOccuCode(request.getParameter("AppntSupplierOccuCode"));
tFCContSchema.setSignMan(request.getParameter("SignMan"));
//tFCContSchema.setAppntHomePhone(request.getParameter("AppntHomePhone"));
tFCContSchema.setInsuredHomePhone(request.getParameter("InsuredHomePhone"));
//tFCContSchema.setHandlingHomePhone(request.getParameter("HandlingHomePhone"));
tFCContSchema.setRightSendFlag(request.getParameter("RightSendFlag"));
tFCContSchema.setHandlingMobile2(request.getParameter("HandlingMobile2"));
tFCContSchema.setChannelCode(request.getParameter("ChannelCode"));
tFCContSchema.setAccreditFlag(request.getParameter("AccreditFlag"));
// 新增字段
tFCContSchema.setAmnt(request.getParameter("PurchasePrice"));// 这地方改下
tFCContSchema.setReceiveCom(request.getParameter("SupplierShowCode"));
// 车主 
// 车信息录入
//
//
tFCContSchema.setHandlingNo(request.getParameter("FrameNo"));
tFCContSchema.setHandlingAddressNo(request.getParameter("EngineNo"));
tFCContSchema.setHandlingName(request.getParameter("CarKindCode"));
tFCContSchema.setHandlingSex(request.getParameter("ModelCode"));
tFCContSchema.setHandlingBirthday(request.getParameter("EnrollDate"));
tFCContSchema.setYearExamDate(request.getParameter("YearExamDate"));

tFCContSchema.setHandlingIDType(request.getParameter("SeatCount1"));
tFCContSchema.setHandlingIDNo(request.getParameter("TonCount"));
tFCContSchema.setHandlingOccup(request.getParameter("CountryNature"));
tFCContSchema.setHandlingPostalAddress(request.getParameter("LicenseColorCode"));
tFCContSchema.setHandlingZipCode(request.getParameter("UseYears"));
tFCContSchema.setPolicyPli(request.getParameter("hidePolicyPli"));
//tFCContSchema.setHandlingMobile(request.getParameter("HandlingMobile"));
//tFCContSchema.setHandlingPhone(request.getParameter("HandlingPhone"));
tFCContSchema.setState(request.getParameter("State"));
if(request.getParameter("SignMan")==null||request.getParameter("SignMan").equals(""))
{
	tFCContSchema.setSignManName(request.getParameter("SignManName"));
}


tFCPolSchema.setInnerContNo(tInnerContNo);
tFCPolSchema.setContNo(request.getParameter("ContNo"));
//tFCPolSchema.setPolNo(request.getParameter("PolNo"));
tFCPolSchema.setKindCode(request.getParameter("KindCode"));
tFCPolSchema.setRiskCode(request.getParameter("RiskCode"));
tFCPolSchema.setOutRiskCode(request.getParameter("OutRiskCode"));
tFCPolSchema.setPayIntv(request.getParameter("PayIntv"));
tFCPolSchema.setSumPrem(request.getParameter("SumPrem"));
tFCPolSchema.setContPrem(request.getParameter("SumPrem"));//原始保单保费
tFCPolSchema.settax1(request.getParameter("tax1"));
tFCPolSchema.settax2(request.getParameter("tax2"));
tFCPolSchema.setSignDate(request.getParameter("SignDate"));

tFCPolSchema.setSignTime(request.getParameter("SignTime"));
tFCPolSchema.setAgentCode(request.getParameter("AgentCode"));
tFCPolSchema.setSupplierCode(request.getParameter("SupplierCode"));
tFCPolSchema.setLicenseNo(request.getParameter("LicenseNo").trim());
tFCPolSchema.setProtocolNo(request.getParameter("ProtocolNo"));
tFCPolSchema.setFYCType(request.getParameter("FYCType"));
tFCPolSchema.setISSingle(request.getParameter("IsSingle1"));//是否单出
tFCPolSchema.setRuleID(request.getParameter("RuleID"));//规制ID

tFCPolSchema.setProtocolNo(request.getParameter("PolicyPrtNo"));

tFCPolSchema.setState(request.getParameter("State"));


//对于页面上数字的处理
String tSumPrem = request.getParameter("SumPrem");
String tax1 = request.getParameter("tax1");
String tax2 = request.getParameter("tax2");
if(tSumPrem==null||tSumPrem.equals(""))
{
	tFCContSchema.setSumPrem("0");
	tFCContSchema.setContPrem("0");
	tFCPolSchema.setSumPrem("0");
	tFCPolSchema.setContPrem("0");
}
if(tax1==null||tax1.equals(""))
{
	tFCPolSchema.settax1("0");
}
if(tax2==null||tax2.equals(""))
{
	tFCPolSchema.settax2("0");
}

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tFCContSchema);
tVData.add(tFCPolSchema);
tVData.add(tFCPolSchema);

GarContBL tGarContBL = new GarContBL();

try
{
	System.out.println("this will save the data!!!");
	tGarContBL.submitData(tVData,tOperate);
}
catch(Exception ex)
{
	ex.printStackTrace();
	FlagStr = "Fail";
}


if (!FlagStr.equals("Fail"))
{
	tError = tGarContBL.mErrors;
	if (!tError.needDealError())
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
    
%>
<html>
<script language="javascript">
	parent.fraInterface.afterContSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>