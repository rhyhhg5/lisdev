<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.ExeSQL"%>
<%@include file="FCCarContInput.jsp"%>
<SCRIPT src="ScanCarContInput.js"></SCRIPT>
<%@include file="ScanCarContInit.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<%
	String imgUrl = "";
	String sql = "select SYSVARVALUE from fdsysvar  where sysvar ='GetCarContServer'";
	imgUrl = new ExeSQL().getOneValue(sql);
	imgUrl += "VPSum/Navigator.action?method=getOcrMessage";
%>
<SCRIPT LANGUAGE="JavaScript">

//得到配置好的SRC路径   
var imgUrl = "<%=imgUrl%>";
var showWinFlag = false;
var state = "<%=request.getParameter("state")%>";

//新建一个数组,需要影像的字段组合
var iArray = new Array("LicenseNo","SupplierShowCode","ContNo","BussNature","UWDate","SignDate","CValiDate",
						"CInValiDate","POSNo","InsuredCustType","InsuredName","InsuredIDType","InsuredIDNo","CarOwner","PurchasePrice",
						"UseNatureCode","OutRiskCode","SumPrem","tax1","FrameNo","EngineNo","CarKindCode","ModelCode","EnrollDate","SeatCount1","TonCount","PolicyPli","KindGrid1");
var iArray1 = new Array("SupplierShowCode","BussNature","InsuredCustType","InsuredIDType","UseNatureCode","OutRiskCode","CarKindCode");						
var iArray2 = new Array("LicenseNo","ContNo","UWDate","SignDate","CValiDate",
						"CInValiDate","POSNo","InsuredName","InsuredIDNo","CarOwner","PurchasePrice","SumPrem","tax1","FrameNo","EngineNo","ModelCode","EnrollDate","SeatCount1","TonCount","PolicyPli");						

document.onclick=function(input){  
	//获取车险保单快速录入页面元素
    input = window.event||input;
	input = input.srcElement||input.target;
	document.getElementById("win").style.display="none";
    for(var i=0;i<iArray.length;i++) {
    	if(input.name==iArray[i]){
    		if(!NullOrBlank(fm.Policy.value)){
				showWinFlag = true;
			}
      		//1、获得输入框对象object，showWindow（）
      		var InpObj = document.getElementsByName(iArray[i]);
      		var bgcolor = InpObj[0].style.backgroundColor;
      		if(showWinFlag){
      			//2、截取图片
      			frameTest(Reflect(iArray[i]));
      			//3将输入框点亮
      			InpObj[0].style.backgroundColor = "#04BBBB";
      			//4.显示图片层
      			if(iArray[i]!="KindGrid1")
      			{
      			   showWindow(InpObj[0],!NullOrBlank(OldiArray[i]));
      			}
      		}
      		//当这个输入框在离开是触发onblur事件
      		InpObj[0].onblur = function(){
      			InpObj[0].style.backgroundColor = bgcolor;
      			if(iArray[i] == "InsuredName"){
	      				showCarOwnerName();
	      			}
	      		if(iArray[i] == "CValiDate"){
	      				showCInValiDate();
	      			}
      		}
      		break;
      	}
    }
} 


//截图片，在这之前还有一步，在录入保险公司，保单号，回车后，要置上fraImg的src
function frameTest(id){
	//var url = "http://10.88.12.189:8080/ImageCS/Navigator.action?method=getOcrMessage&imageid=12440000001226423806"; //同iframe的scr一致
	var imageid = fm.Policy.value;
	//imageid = '12111920351015177138';//测试使用
	var parma = "&imageid="+imageid;
	var url = imgUrl + parma + "#"+id;
	document.getElementById('fraImg').src = url; 
}

//键盘事件
//onkeyup 这个事件在用户放开任何先前按下的键盘键时发生。 onkeydown 这个事件在用户按下任何键盘键（包括系统按钮，如箭头键和功能键）时发生。
document.onkeyup = function (){
	//支持Tab建随动辅助功能
	if(event.keyCode==9){	
		document.getElementById("win").style.display="none";
		//获取车险保单快速录入页面元素
	    input = window.event||input;
		input = input.srcElement||input.target;
	    for(var i=0;i<iArray.length;i++) {
	    	if(input.name==iArray[i]){
	    		if(!NullOrBlank(fm.Policy.value)){
				showWinFlag = true;
				}
	      		//1、获得输入框对象object，showWindow（）
	      		var InpObj = document.getElementsByName(iArray[i]);
	      		var bgcolor = InpObj[0].style.backgroundColor;
	      		if(showWinFlag){
	      			//2、截取图片
	      			frameTest(Reflect(iArray[i]));
	      			//3将输入框点亮
	      			InpObj[0].style.backgroundColor = "#04BBBB";
	      			//4.显示图片层
	      			if(iArray[i]!="KindGrid1")
      			    {
	      			  showWindow(InpObj[0],!NullOrBlank(OldiArray[i]));
	      			}
	      		}
	      		//当这个输入框在离开是触发onblur事件
	      		InpObj[0].onblur = function(){
	      			InpObj[0].style.backgroundColor = bgcolor;
	      			if(iArray[i] == "InsuredName"){
	      				showCarOwnerName();
	      			}
	      			if(iArray[i] == "CValiDate"){
	      				showCInValiDate();
	      			}
	      		}
	      		break;
	      	}  
	    }
	}
	//回车事件
	if(event.keyCode==13){
		document.getElementById("win").style.display="none";
	}
}

document.ondblclick=function(input){  
	//获取车险保单快速录入页面元素
	input = window.event||input;
	input = input.srcElement||input.target;
	document.getElementById("win").style.display="none";
	for(var i=0;i<iArray1.length;i++) {
		if(input.name==iArray1[i]){
	  		var InpObj = document.getElementsByName(iArray1[i]);
	  		InpObj[0].style.backgroundColor = "#D7E1F6";
		}
	}
	for(var j=0;j<iArray2.length;j++) {
		if(input.name==iArray2[j]){
	  		var InpObj = document.getElementsByName(iArray2[j]);
	  		InpObj[0].style.backgroundColor = "#F7F7F7";
		}
	}
}

</SCRIPT>
<html>
<head>
<table class=common>
	<tr class=common>
		<font color=red>注:对于已经对接成功后的报文保单，输入保单号回车后，才能显示这张保单的随动效果</font>
	</tr>
</table>
</head>
</html>
<SCRIPT LANGUAGE="JavaScript">
window.onload=function()
{
	initForm();
	initElementtype();
	initForm1();
}

</SCRIPT>