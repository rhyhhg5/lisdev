var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var mRiskCode="";

window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function isTrueName(s){
	if(!NullOrBlank(s)){
		var patrn=/^[a-zA-Z0-9\u4E00-\u9FA5]+$/;
		if (!patrn.exec(s)){
			return false
		}
	}
	return true;
}

function isTrueNo(s){
	if(!NullOrBlank(s)){
		var patrn=/^[a-zA-Z0-9]+(\_([a-zA-Z0-9])+)*$/;
		if (!patrn.exec(s)){
			return false
		}
	}
	return true;
}
//不能含有中文 可输入横杠等字符的正则表达式
function IsChar(s)
{ 
 var Number = "0123456789.abcdefghijklmnopqrstuvwxyz-\/ABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@#$%^&*()_公安盐";
 for (i = 0; i < s.length;i++)
    {
        var c = s.charAt(i);
        if (Number.indexOf(c) == -1) return false;
    }
 return true
}


//保单信息查询
function queryCont()
{
	if(tInputType=="Comple")
    {
    	showInfo = openWindow("./FCPropContQueryMain.jsp?tPageFrom=Comple1");
    }
    else
    {
    	showInfo = openWindow("./FCPropContQueryMain.jsp?tPageFrom=full");
    }
	
}

//保单信息查询
function queryCopyCont()
{
  	showInfo = openWindow("./FCPropContQueryMain.jsp?tPageFrom=copycar");
}

function afterQuery(tInnerContNo)
{
	initInpBox();
    initButton();
    initKindGrid();
	
	fm.InnerContNo.value=tInnerContNo;
	
	//查询保单信息
	var queryCont=" select a.PolicyPrtNo,a.InvoicePrtNo,a.InsCardPrtNo,a.CInsFlagPrtNo,a.UWDate,a.BussNature,a.OutManageCom,a.GetManageCom,"
	+" a.SupplierCode,a.ContNo,a.SignDate,a.SignTime,a.CValiDate,a.CInValiDate,a.POSNo,a.PayPCommFlag,"
	+" a.PayPComm,a.InsuredCustType,a.InsuredName,a.InsuredIDType,a.InsuredIDNo,a.InsuredPostalAddress,a.InsuredZipCode,"
	+" a.InsuredMobile,a.AppntCustType,a.AppntName,a.AppntIDType,a.AppntIDNo,a.AppntPostalAddress,a.AppntZipCode,a.AppntMobile,"
	+" a.AgentCode,a.AgentGroup,a.PolicyPli,a.InnerContNo,"
	+" a.ProtocolNo,a.Corporation,a.AgentFlag,"
	+" a.AppntNo,a.InsuredNo,a.ComFirstFeeFlag,a.HandlingNo,a.HandlingAddressNo,a.HandlingName,a.HandlingIDType,a.HandlingIDNo,"
	+" a.HandlingPostalAddress,a.HandlingZipCode,a.HandlingMobile,a.OldSupplierCode,a.OldContNo,a.ProtocolNo,"
	+" a.RightSendFlag,a.SignMan,a.AppntMobile2,a.AppntPhone,a.AppntHomePhone,"
	+" a.InsuredMobile2,a.InsuredPhone,a.InsuredHomePhone,a.HandlingMobile2,a.HandlingPhone,"
	+" a.HandlingHomePhone,a.Remark,a.ChannelCode,a.ComFirstFeeFlag,a.AccreditFlag,a.InsuredMemberType,a.AppntMemberType,a.HandlingMemberType "	
	+",a.ReceiveOperator,a.ReceiveDate,a.ReceiveTime,a.ProjCode,a.SignManName "
	+" from FCCont a where a.InnerContNo='"+tInnerContNo+"' ";

	var ContResult=easyQueryVer3(queryCont,1,0,1);
	if(!ContResult){
	}
	else{
		var ContSelected = decodeEasyQueryResult(ContResult);
		
		fm.PolicyPrtNo.value=ContSelected[0][0];
		fm.InvoicePrtNo.value=ContSelected[0][1];
		fm.InsCardPrtNo.value=ContSelected[0][2];
		fm.CInsFlagPrtNo.value=ContSelected[0][3];
		fm.UWDate.value=ContSelected[0][4];
		fm.BussNature.value=ContSelected[0][5];
		fm.BussNatureName.value=getNameFromLDCode("BussNature2",ContSelected[0][5]);
		fm.OutManageCom.value=ContSelected[0][6]; 
		fm.OutManageComName.value=getNameByCode("ShortName","FDCom","ComCode",ContSelected[0][6]);
		fm.GetManageCom.value=ContSelected[0][7];
		fm.GetManageComName.value=getNameByCode("ShortName","FDCom","ComCode",ContSelected[0][7]);
		fm.SupplierCode.value=ContSelected[0][8];
		fm.SupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",ContSelected[0][8]);
		fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",ContSelected[0][8]);
		fm.ContNo.value=ContSelected[0][9];
		fm.SignDate.value=ContSelected[0][10];
		fm.SignTime.value=ContSelected[0][11];
		fm.hour.value = (fm.SignTime.value).substring(0,2);
		fm.minute.value = (fm.SignTime.value).substring(3,5);;
		fm.CValiDate.value=ContSelected[0][12];
		showCInValiDate2(ContSelected[0][13]);
		fm.POSNo.value=ContSelected[0][14];
		fm.PayPCommFlag.value=ContSelected[0][15];
		fm.PayPCommFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][15]);
		fm.PayPComm.value=ContSelected[0][16];
		fm.InsuredCustType.value=ContSelected[0][17];
		fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",ContSelected[0][17]);
		fm.InsuredName.value=ContSelected[0][18];
		fm.InsuredIDType.value=ContSelected[0][19];
		fm.InsuredIDTypeName.value=getNameFromLDCode("IDType",ContSelected[0][19]);
		fm.InsuredIDNo.value=ContSelected[0][20];
		fm.InsuredPostalAddress.value=ContSelected[0][21];
		fm.InsuredZipCode.value=ContSelected[0][22];
		fm.InsuredMobile.value=ContSelected[0][23];
		fm.AppntCustType.value=ContSelected[0][24];
		fm.AppntCustTypeName.value=getNameFromLDCode("CustType",ContSelected[0][24]);
		fm.AppntName.value=ContSelected[0][25];
		fm.AppntIDType.value=ContSelected[0][26];
		fm.AppntIDTypeName.value=getNameFromLDCode("IDType",ContSelected[0][26]);
		fm.AppntIDNo.value=ContSelected[0][27];
		fm.AppntPostalAddress.value=ContSelected[0][28];
		fm.AppntZipCode.value=ContSelected[0][29];
		fm.AppntMobile.value=ContSelected[0][30];
		fm.PolicyPli.value=unescapes(ContSelected[0][33]);
		fm.InnerContNo.value=ContSelected[0][34];
		//fm.ProtocolNo.value=ContSelected[0][35];
		//mProtocolNo = fm.ProtocolNo.value;
		fm.Corporation.value=ContSelected[0][36];
		fm.AgentFlag.value=ContSelected[0][37];
		fm.AppntNo.value=ContSelected[0][38];
		fm.InsuredNo.value=ContSelected[0][39];
		fm.ComFirstFeeFlag.value=ContSelected[0][40];
		fm.ComFirstFeeFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][40]);

		fm.HandlingNo.value=ContSelected[0][41];
		fm.HandlingAddressNo.value=ContSelected[0][42];
		fm.HandlingName.value=ContSelected[0][43];
		fm.HandlingIDType.value=ContSelected[0][44];
		fm.HandlingIDTypeName.value=getNameFromLDCode("idtype",ContSelected[0][44]);
		fm.HandlingIDNo.value=ContSelected[0][45];
		fm.HandlingPostalAddress.value=ContSelected[0][46];
		fm.HandlingZipCode.value=ContSelected[0][47];
		fm.HandlingMobile.value=ContSelected[0][48];
		fm.OldSupplierCode.value=ContSelected[0][49];
		fm.OldSupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",ContSelected[0][49]);
		fm.OldSupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",ContSelected[0][49]);
		fm.OldContNo.value=ContSelected[0][50];
		//fm.PKProtocolNo.value=ContSelected[0][51];
		fm.RightSendFlag.value=ContSelected[0][52];
		fm.RightSendFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][52]);
		fm.SignMan.value=ContSelected[0][53];
		if(NullOrBlank(fm.SignMan.value))
		{
			fm.SignManName.value=ContSelected[0][74];
		}
		else
		{
			fm.SignManName.value=getNameByCode("UserName","FDUser","UserCode",ContSelected[0][53]);
		}
		fm.AppntMobile2.value=ContSelected[0][54];
		fm.AppntPhone.value=ContSelected[0][55];
		fm.AppntHomePhone.value=ContSelected[0][56];
		fm.InsuredMobile2.value=ContSelected[0][57];
		fm.InsuredPhone.value=ContSelected[0][58];
		fm.InsuredHomePhone.value=ContSelected[0][59];
		fm.HandlingMobile2.value=ContSelected[0][60];
		fm.HandlingPhone.value=ContSelected[0][61];
		fm.HandlingHomePhone.value=ContSelected[0][62];
		fm.Remark.value=unescapes(ContSelected[0][63]);

		if(fm.BussNature.value=="02")
		{
			channel.style.display = "";
			fm.ChannelCode.value=ContSelected[0][64];
			fm.ChannelName.value=getNameByCode("RepairShopName","FDRepairShop","RepairShopCode",ContSelected[0][64]);
		}
		else
		{
			channel.style.display = "none";
			fm.ChannelCode.value="";
			fm.ChannelName.value="";
		}
		
		fm.AccreditFlag.value=ContSelected[0][66];
		fm.AccreditFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][66]);
		//客户人员 会员类别
		fm.InsuredMemberType.value=ContSelected[0][67];
		fm.InsuredMemberTypeName.value=getNameFromLDCode("membertype",ContSelected[0][67]);
		fm.AppntMemberType.value=ContSelected[0][68];
		fm.AppntMemberTypeName.value=getNameFromLDCode("membertype",ContSelected[0][68]);
		fm.HandlingMemberType.value=ContSelected[0][69];
		
		fm.ReceiveOperator.value = ContSelected[0][70];
		fm.ReceiveOperatorName.value = getNameByCode("UserName","FDUser","UserCode",ContSelected[0][70]);
		fm.ReceiveDate.value = ContSelected[0][71];
		fm.ReceiveTime.value = ContSelected[0][72];
		
		queryFCProjRiskIns(ContSelected[0][73]);
		
		fm.HandlingMemberTypeName.value=getNameFromLDCode("membertype",ContSelected[0][69]);
		
		if(fm.AppntNo.value==fm.InsuredNo.value)
		{
			fm.appnt.checked=true;
		}
		else
		{
			fm.appnt.checked=false;
		}
			
		if(fm.HandlingNo.value==fm.InsuredNo.value)
		{
			fm.handling.checked=true;
		}
			
		else{
			fm.handling.checked=false;	
		}
		showDisabled();
		
		fm.AgentCode.value=ContSelected[0][31];
		fm.AgentName.value=getNameByCode("Name","FAAgentTree","AgentCode",ContSelected[0][31]);
		fm.AgentGroup.value=ContSelected[0][32];
		//mAgentGroup = fm.AgentGroup.value;
		fm.AgentGroupName.value=getNameByCode("ShortName","FDCom","ComCode",ContSelected[0][32]);
		
		var sql = " select a.Score from FAScore a "
		+ " where a.InnerContNo='"+tInnerContNo+"' and a.AgentCode='"+fm.AgentCode.value+"'";
		
		var result=easyQueryVer3(sql,1,0,1);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result,0,0);
		if(resultArr!=null)
		{
			fm.AgentScore.value = resultArr[0][0];
		}
		
	    getCarByInnerContNo(tInnerContNo);
		queryRisk(tInnerContNo);
		
		
		if(tInputType=="Comple")
	    {
    		disableCard();
	    	disableFast();
	    }
	}
}

function afterQueryCopy(tInnerContNo)
{
	initInpBox();
    initButton();
    initKindGrid();
	
	//查询保单信息
	var queryCont=" select a.PolicyPrtNo,a.InvoicePrtNo,a.InsCardPrtNo,a.CInsFlagPrtNo,a.UWDate,a.BussNature,a.OutManageCom,a.GetManageCom,"
	+" a.SupplierCode,a.ContNo,a.SignDate,a.SignTime,a.CValiDate,a.CInValiDate,a.POSNo,a.PayPCommFlag,"
	+" a.PayPComm,a.InsuredCustType,a.InsuredName,a.InsuredIDType,a.InsuredIDNo,a.InsuredPostalAddress,a.InsuredZipCode,"
	+" a.InsuredMobile,a.AppntCustType,a.AppntName,a.AppntIDType,a.AppntIDNo,a.AppntPostalAddress,a.AppntZipCode,a.AppntMobile,"
	+" a.AgentCode,a.AgentGroup,a.PolicyPli,a.InnerContNo,"
	+" a.ProtocolNo,a.Corporation,a.AgentFlag,"
	+" a.AppntNo,a.InsuredNo,a.ComFirstFeeFlag,a.HandlingNo,a.HandlingAddressNo,a.HandlingName,a.HandlingIDType,a.HandlingIDNo,"
	+" a.HandlingPostalAddress,a.HandlingZipCode,a.HandlingMobile,a.OldSupplierCode,a.OldContNo,a.ProtocolNo,"
	+" a.RightSendFlag,a.SignMan,a.AppntMobile2,a.AppntPhone,a.AppntHomePhone,"
	+" a.InsuredMobile2,a.InsuredPhone,a.InsuredHomePhone,a.HandlingMobile2,a.HandlingPhone,"
	+" a.HandlingHomePhone,a.Remark,a.ChannelCode,a.AccreditFlag,a.InsuredMemberType,a.AppntMemberType,a.HandlingMemberType "
	+" from FCCont a where a.InnerContNo='"+tInnerContNo+"' ";

	var ContResult=easyQueryVer3(queryCont,1,0,1);
	if(!ContResult){
	}
	else{
		var ContSelected = decodeEasyQueryResult(ContResult);
		
		//fm.PolicyPrtNo.value=ContSelected[0][0];
		//fm.InvoicePrtNo.value=ContSelected[0][1];
		//fm.InsCardPrtNo.value=ContSelected[0][2];
		//fm.CInsFlagPrtNo.value=ContSelected[0][3];
		fm.UWDate.value=ContSelected[0][4];
		fm.BussNature.value=ContSelected[0][5];
		fm.BussNatureName.value=getNameFromLDCode("BussNature2",ContSelected[0][5]);
		fm.OutManageCom.value=ContSelected[0][6]; 
		fm.OutManageComName.value=getNameByCode("ShortName","FDCom","ComCode",ContSelected[0][6]);
		fm.GetManageCom.value=ContSelected[0][7];
		fm.GetManageComName.value=getNameByCode("ShortName","FDCom","ComCode",ContSelected[0][7]);
		fm.SupplierCode.value=ContSelected[0][8];
		fm.SupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",ContSelected[0][8]);
		fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",ContSelected[0][8]);
		//fm.ContNo.value=ContSelected[0][9];
		fm.SignDate.value=ContSelected[0][10];
		fm.SignTime.value=ContSelected[0][11];
		fm.hour.value = (fm.SignTime.value).substring(0,2);
		fm.minute.value = (fm.SignTime.value).substring(3,5);;
		fm.CValiDate.value=ContSelected[0][12];
		showCInValiDate2(ContSelected[0][13]);
		fm.POSNo.value=ContSelected[0][14];
		fm.PayPCommFlag.value=ContSelected[0][15];
		fm.PayPCommFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][15]);
		fm.PayPComm.value=ContSelected[0][16];
		fm.InsuredCustType.value=ContSelected[0][17];
		fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",ContSelected[0][17]);
		fm.InsuredName.value=ContSelected[0][18];
		fm.InsuredIDType.value=ContSelected[0][19];
		fm.InsuredIDTypeName.value=getNameFromLDCode("IDType",ContSelected[0][19]);
		fm.InsuredIDNo.value=ContSelected[0][20];
		fm.InsuredPostalAddress.value=ContSelected[0][21];
		fm.InsuredZipCode.value=ContSelected[0][22];
		fm.InsuredMobile.value=ContSelected[0][23];
		fm.AppntCustType.value=ContSelected[0][24];
		fm.AppntCustTypeName.value=getNameFromLDCode("CustType",ContSelected[0][24]);
		fm.AppntName.value=ContSelected[0][25];
		fm.AppntIDType.value=ContSelected[0][26];
		fm.AppntIDTypeName.value=getNameFromLDCode("IDType",ContSelected[0][26]);
		fm.AppntIDNo.value=ContSelected[0][27];
		fm.AppntPostalAddress.value=ContSelected[0][28];
		fm.AppntZipCode.value=ContSelected[0][29];
		fm.AppntMobile.value=ContSelected[0][30];
		fm.PolicyPli.value=unescapes(ContSelected[0][33]);
		//fm.InnerContNo.value=ContSelected[0][34];
		//fm.ProtocolNo.value=ContSelected[0][35];
		//mProtocolNo = fm.ProtocolNo.value;
		fm.Corporation.value=ContSelected[0][36];
		fm.AgentFlag.value=ContSelected[0][37];
		//fm.AppntNo.value=ContSelected[0][38];
		//fm.InsuredNo.value=ContSelected[0][39];
		fm.ComFirstFeeFlag.value=ContSelected[0][40];
		fm.ComFirstFeeFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][40]);
		//fm.HandlingNo.value=ContSelected[0][41];
		fm.HandlingAddressNo.value=ContSelected[0][42];
		fm.HandlingName.value=ContSelected[0][43];
		fm.HandlingIDType.value=ContSelected[0][44];
		fm.HandlingIDTypeName.value=getNameFromLDCode("idtype",ContSelected[0][44]);
		fm.HandlingIDNo.value=ContSelected[0][45];
		fm.HandlingPostalAddress.value=ContSelected[0][46];
		fm.HandlingZipCode.value=ContSelected[0][47];
		fm.HandlingMobile.value=ContSelected[0][48];
		fm.OldSupplierCode.value=ContSelected[0][49];
		fm.OldSupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",ContSelected[0][49]);
		fm.OldSupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",ContSelected[0][49]);
		fm.OldContNo.value=ContSelected[0][50];
		//fm.PKProtocolNo.value=ContSelected[0][51];
		fm.RightSendFlag.value=ContSelected[0][52];
		fm.RightSendFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][52]);
		
		fm.SignMan.value=ContSelected[0][53];
		fm.SignManName.value=getNameByCode("UserName","FDUser","UserCode",ContSelected[0][53]);
		fm.AppntMobile2.value=ContSelected[0][54];
		fm.AppntPhone.value=ContSelected[0][55];
		fm.AppntHomePhone.value=ContSelected[0][56];
		fm.InsuredMobile2.value=ContSelected[0][57];
		fm.InsuredPhone.value=ContSelected[0][58];
		fm.InsuredHomePhone.value=ContSelected[0][59];
		fm.HandlingMobile2.value=ContSelected[0][60];
		fm.HandlingPhone.value=ContSelected[0][61];
		fm.HandlingHomePhone.value=ContSelected[0][62];
		fm.Remark.value=ContSelected[0][63];
		
		if(fm.BussNature.value=="02")
		{
			channel.style.display = "";
			fm.ChannelCode.value=ContSelected[0][64];
			fm.ChannelName.value=getNameByCode("RepairShopName","FDRepairShop","RepairShopCode",ContSelected[0][64]);
		}
		else
		{
			channel.style.display = "none";
			fm.ChannelCode.value="";
			fm.ChannelName.value="";
		}
		
		fm.AccreditFlag.value=ContSelected[0][65];
		fm.AccreditFlagName.value=getNameFromLDCode("yesorno",ContSelected[0][65]);
		//客户人员 会员类别
		fm.InsuredMemberType.value=ContSelected[0][66];
		fm.InsuredMemberTypeName.value=getNameFromLDCode("membertype",ContSelected[0][66]);
		fm.AppntMemberType.value=ContSelected[0][67];
		fm.AppntMemberTypeName.value=getNameFromLDCode("membertype",ContSelected[0][67]);
		fm.HandlingMemberType.value=ContSelected[0][68];
		fm.HandlingMemberTypeName.value=getNameFromLDCode("membertype",ContSelected[0][68]);
		
		fm.AgentCode.value=ContSelected[0][31];
		fm.AgentName.value=getNameByCode("Name","FAAgentTree","AgentCode",ContSelected[0][31]);
		fm.AgentGroup.value=ContSelected[0][32];
		//mAgentGroup = fm.AgentGroup.value;
		fm.AgentGroupName.value=getNameByCode("ShortName","FDCom","ComCode",ContSelected[0][32]);
		
		var sql = " select a.Score from FAScore a "
		+ " where a.InnerContNo='"+tInnerContNo+"' and a.AgentCode='"+fm.AgentCode.value+"'";
		
		var result=easyQueryVer3(sql,1,0,1);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result,0,0);
		if(resultArr!=null)
		{
			fm.AgentScore.value = resultArr[0][0];
		}

		getCarByInnerContNo(tInnerContNo);
		queryOldRisk(tInnerContNo);
		
	}
}

function queryRisk(tInnerContNo)
{
	var sql="select a.PolNo,a.KindCode,a.RiskCode,a.OutRiskCode,"
	+" (select RiskShortName from FMRisk where RiskCode=a.RiskCode),"
	+" a.PayIntv,a.SumPrem,a.tax1,a.tax2,a.FYCType,"
	+" (select GradeName from FMProtocolgrade where indexcode=a.FYCType),"
	+" (select Remark from FMProtocolgrade where indexcode=a.FYCType),a.ISSINGLE "
	+" from FCPol a"
	+" where a.InnerContNo='"+tInnerContNo+"' "; 		
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.PolNo.value = resultArr[0][0];
		fm.KindCode.value = resultArr[0][1];
		fm.RiskCode.value = resultArr[0][2];
		mRiskCode = fm.RiskCode.value;
		fm.OutRiskCode.value = resultArr[0][3];
		fm.RiskName.value = resultArr[0][4];
		fm.PayIntv.value = resultArr[0][5];
		fm.SumPrem.value = resultArr[0][6];
		fm.tax1.value = resultArr[0][7];
		fm.tax2.value = resultArr[0][8];
		fm.FYCType.value = resultArr[0][9];
		fm.FYCTypeName.value = resultArr[0][10];
		fm.FYCTypeReMark.value = resultArr[0][11];
		fm.IsSingle1.value = resultArr[0][12];
		if(fm.IsSingle1.value=="Y"){
			fm.ISSingle.checked = true;
		}else{
			fm.ISSingle.checked = false;
		}
	}
	
    showKindList(tInnerContNo);
}

function queryOldRisk(tInnerContNo)
{
	var sql="select a.PolNo,a.KindCode,a.RiskCode,a.OutRiskCode,"
	+" (select RiskShortName from FMRisk where RiskCode=a.RiskCode),"
	+" a.PayIntv,a.SumPrem,a.tax1,a.tax2,a.FYCType,"
	+" (select GradeName from FMProtocolgrade where indexcode=a.FYCType)"
	+" from FCPol a"
	+" where a.InnerContNo='"+tInnerContNo+"' "; 		
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.PolNo.value = resultArr[0][0];
		fm.KindCode.value = resultArr[0][1];
		fm.RiskCode.value = resultArr[0][2];
		mRiskCode = fm.RiskCode.value;
		fm.OutRiskCode.value = resultArr[0][3];
		fm.RiskName.value = resultArr[0][4];
		fm.PayIntv.value = resultArr[0][5];
		fm.SumPrem.value = resultArr[0][6];
		fm.tax1.value = resultArr[0][7];
		fm.tax2.value = resultArr[0][8];
		//fm.FYCType.value = resultArr[0][9];
		//fm.FYCTypeName.value = resultArr[0][10];
	}
	
    showKindList(tInnerContNo);
}

function submitContForm()
{
	var i = 0;
	var showStr="正在查询数据，请您稍候";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;  
	
	showInfo=window.showModelessDialog(urlStr,window,WINDOWSSIZE);   
	fm.hideOperate.value=mOperate;
	
	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	
	if(tInputType=="Comple")
    {
    	removeFastDisabled();
    }
	
	fm.action="FCCarContFullSave.jsp";
	fm.submit(); //提交  
}

function submitPolForm()
{	
	var i = 0;
	var showStr="正在查询数据，请您稍候";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;  
	
	showInfo=window.showModelessDialog(urlStr,window,WINDOWSSIZE);   
	fm.hideOperate.value=mOperate;
	
	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	
	if(tInputType=="Comple")
    {
    	removeFastDisabled();
    }
    
	fm.action="FCCarContFullPolSave.jsp";
	fm.submit(); //提交  
}

//提交后操作,服务器数据返回后执行的操作
function afterContSubmit( FlagStr, content,tOperate,tInnerContNo,tPolNo,tItemNo)
{
	showInfo.close();
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
		showModalDialog(urlStr,window,WINDOWSSIZE);   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
		showModalDialog(urlStr,window,WINDOWSSIZE);      
	}
	
	if(tInputType=="Comple")
    {
    	disableFast();
    }
    
	if(tOperate=="DELETE||MAIN")
	{
		initForm();
	}
	else
	{
		fm.InnerContNo.value=tInnerContNo;
		fm.PolNo.value=tPolNo;
		fm.ItemNo.value=tItemNo;
	  	
	  	var sql="select a.AppntNo,a.AppntAddressNo,a.InsuredNo,a.InsuredAddressNo,a.HandlingNo,a.HandlingAddressNo"
	  	+" from FCCont a where a.InnerContNo='"+tInnerContNo+"'";
	  	
	  	var result=easyQueryVer3(sql,1,0,1);
	  	var arrResult = decodeEasyQueryResult(result);
	  	if(arrResult!=null)
	  	{
	  		fm.AppntNo.value = arrResult[0][0];
	  		fm.AppntAddressNo.value = arrResult[0][1];
	  		fm.InsuredNo.value = arrResult[0][2];
	  		fm.InsuredAddressNo.value = arrResult[0][3];
	  		fm.HandlingNo.value = arrResult[0][4];
	  		fm.HandlingAddressNo.value = arrResult[0][5];
	  		
	  		if(fm.AppntNo.value==fm.InsuredNo.value)
			{
				fm.appnt.checked=true;
			}
			else
			{
				fm.appnt.checked=false;
			}
				
			if(fm.HandlingNo.value==fm.InsuredNo.value)
			{
				fm.handling.checked=true;
			}
				
			else{
				fm.handling.checked=false;	
			}
			showDisabled();
	  	}
	}
	
}

function afterContEndSubmit( FlagStr, content )
{
	showInfo.close();
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
		showModalDialog(urlStr,window,WINDOWSSIZE);   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
		showModalDialog(urlStr,window,WINDOWSSIZE);      
	}
  
  	if(fm.CopyFlag.checked)
	{
		fm.InnerContNo.value="";
		fm.PolNo.value="";
		fm.ItemNo.value="";
		fm.InsuredNo.value="";
		
		fm.PolicyPrtNo.value="";
		fm.InvoicePrtNo.value="";
		fm.InsCardPrtNo.value="";
		fm.CInsFlagPrtNo.value="";
		fm.ContNo.value="";
		initPolForm();
		
		fm.CopyFlag.checked = false;
	}
	else
	{
		fm.InnerContNo.value="";
		fm.PolNo.value="";
		fm.ItemNo.value="";
		fm.InsuredNo.value="";
		
		fm.PolicyPrtNo.value="";
		fm.InvoicePrtNo.value="";
		fm.InsCardPrtNo.value="";
		fm.CInsFlagPrtNo.value="";
		fm.ContNo.value="";
		fm.InsuredNo.value="";
		fm.InsuredCustType.value="";
		fm.InsuredCustTypeName.value="";
		fm.InsuredName.value="";
		
		fm.CValiDate.value="";
		fm.CInValiDate.value="";
		
		initPolForm();
		initItemForm();
		
		fm.CopyFlag.checked = false;
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterPolSubmit( FlagStr, content )
{
	showInfo.close();
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
		showModalDialog(urlStr,window,WINDOWSSIZE);   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
		showModalDialog(urlStr,window,WINDOWSSIZE);      
	}
	
	if(tInputType=="Comple")
    {
    	disableFast();
    }
  
	initVerify();
	initPolForm();
	queryRisk(fm.InnerContNo.value);
}

//提交投保单信息前的校验、计算  
function beforeContSubmit()
{
	addContVerify();
	showSignManName();
	showChannelName();
	//车险保单完整录入的时候要校验代理人，补充录入的时候不做校验
	if(tInputType=="Full")
	{
		showAgentName();
	}
	
	if(fm.appnt.checked==true)
	{
		showAppnt();
	}
	if(fm.handling.checked==true)
	{
		showHandling();
	}
		
	removeDisabled();
	
	if(KindGrid.checkValue(null,KindGrid) == false)
	{
		return false;
	}
	
	clearVerify();
	if(verifyInput2() == false)
  	{
	    return false;
	}
	if(fm.BussNature.value=="02")
	{
		if(NullOrBlank(fm.ChannelCode.value))
		{
			showVerify("fm.ChannelCode","渠道编码不能为空！");
	  		return false;
		}
    }
	
	if(!isTrueNo(fm.PolicyPrtNo.value))
	{
		showVerify("fm.PolicyPrtNo","保单印刷号不合法！");
	  	return false;
    }
    if(!isTrueNo(fm.InvoicePrtNo.value))
	{
		showVerify("fm.InvoicePrtNo","发票印刷号不合法！");
	  	return false;
    }
	if(!isTrueNo(fm.InsCardPrtNo.value))
	{
		showVerify("fm.InsCardPrtNo","保险卡印刷号不合法！");
	  	return false;
    }
    if(!isTrueNo(fm.CInsFlagPrtNo.value))
	{
		showVerify("fm.CInsFlagPrtNo","交强险标志印刷号不合法！");
	  	return false;
    }
    if(!checkLicenseNo(fm.LicenseNo.value))
	{
		if(!confirm("车牌号不合法，是否继续？"))
		{
			return false;
		}
    }    
	if(!isNo2(fm.ContNo.value))
	{
		showVerify("fm.ContNo","保单号不合法！");
	  	return false;
    }
    /*if(!_checkContNo(fm.SupplierCode.value.substring(0,4),fm.ContNo.value.length))
	{
		showVerify("fm.ContNo","保单号长度与系统中设置的不符！");
		return false;
	}*/
	if(!checkFDInsContNo()){
   		return false;
    }  
	if(!NullOrBlank(fm.OldContNo.value))
	{
		if(!isNo2(fm.OldContNo.value))
		{
			showVerify("fm.OldContNo","被保人上张保单保单号不合法！");
		  	return false;
	    }
	    
	    if(!NullOrBlank(fm.OldSupplierShowCode.value))
	    {
	    	if(!_checkContNo(fm.OldSupplierShowCode.value.substring(0,4),fm.OldContNo.value.length))
			{
				showVerify("fm.OldContNo","被保人上张保单保单号长度与系统中设置的不符！");
				return false;
			} 
	    }
	}
    if(!NullOrBlank(fm.InsuredIDType.value)&&!NullOrBlank(fm.InsuredIDNo.value))
    {
    	if(fm.InsuredIDType.value=="01")
    	{
    		var errMess = checkIdCard2(fm.InsuredIDNo.value);
    		if(!NullOrBlank(errMess))
    		{
    			showVerify("fm.InsuredIDNo","被保人"+errMess);
	  			return false;
    		}
    	}
    }
    if(!NullOrBlank(fm.AppntIDType.value)&&!NullOrBlank(fm.AppntIDNo.value))
    {
    	if(fm.AppntIDType.value=="01")
    	{
    		var errMess = checkIdCard2(fm.AppntIDNo.value);
    		if(!NullOrBlank(errMess))
    		{
    			showVerify("fm.AppntIDNo","投保人"+errMess);
	  			return false;
    		}
    	}
    }
    if(!NullOrBlank(fm.HandlingIDType.value)&&!NullOrBlank(fm.HandlingIDNo.value))
    {
    	if(fm.HandlingIDType.value=="01")
    	{
    		var errMess = checkIdCard2(fm.HandlingIDNo.value);
    		if(!NullOrBlank(errMess))
    		{
    			showVerify("fm.HandlingIDNo","经办人"+errMess);
	  			return false;
    		}
    	}
    }
    if(!IsChar(fm.FrameNo.value))
	{
		if(!confirm("车架号不合法,是否继续？"))
		{
			return false;
		}
    }
    if(!IsChar(fm.EngineNo.value))
	{
		if(!confirm("发动机号不合法,是否继续？"))
		{
			return false;
		}
    }
    if(!isTrueNo(fm.VINNo.value))
	{
		showVerify("fm.VINNo","VIN码不合法！");
	  	return false;
    }
    
     //是否参与内部考核积分”选“是”，“是否放弃并授权”则不允许选择“是”
    if(fm.RightSendFlag.value=="Y")
    {
    	if(fm.AccreditFlag.value=="Y")
    	{
    		alert("是否参与内部考核积分”选“是”，“是否放弃并授权”则不允许选择“是”");
    		fm.AccreditFlag.focus();
    		return false;
    	}
    }
	   
   	if(!NullOrBlank(fm.hour.value))
   	{
	  	if(charToNum(fm.hour.value)> charToNum("23"))
	  	{
		    showVerify("fm.hour","出单时间小时大小应该在00-23之间！");
		    return false;
	    }
	    if(charToNum(fm.minute.value)> charToNum("60"))
	  	{
	  		showVerify("fm.minute","出单时间分钟大小应该在00-60之间！");
		    return false;
	    }
    }
    
	if(!NullOrBlank(fm.CValiDate.value)&&!NullOrBlank(fm.CInValiDate.value))
	{
		if(fm.CInValiDate.value<fm.CValiDate.value)
		{
			showVerify("fm.CValiDate","保单生效日期不能大于保单有效日期至！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.SignDate.value)&&!NullOrBlank(fm.UWDate.value))
	{
		if(fm.UWDate.value>fm.SignDate.value)
		{
			showVerify("fm.UWDate","签单日期不能大于出单日期！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.CValiDate.value)&&!NullOrBlank(fm.UWDate.value))
	{
		if(fm.UWDate.value>fm.CValiDate.value)
		{
			showVerify("fm.UWDate","签单日期不能大于保单生效日期！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.EnrollDate.value)&&!NullOrBlank(fm.YearExamDate.value))
	{
		if(fm.YearExamDate.value<fm.EnrollDate.value)
		{
			showVerify("fm.EnrollDate","初次登记日期不能大于年审到期日！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.ProduceDate.value)&&!NullOrBlank(fm.CarCheckTime.value))
	{
		if(fm.CarCheckTime.value<fm.ProduceDate.value)
		{
			showVerify("fm.ProduceDate","生产日期不能大于验车日期！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.SignDate.value))
	{
		if(fm.SignDate.value>tCurrentDate)
		{
			showVerify("fm.SignDate","出单日期不能大于当前日期！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.EnrollDate.value))
	{
		if(fm.EnrollDate.value>tCurrentDate)
		{
			showVerify("fm.EnrollDate","初次登记日期不能大于当前日期！");
			return false;
		}
	}
	
	//双击下拉校验
	if(NullOrBlank(fm.RiskCode.value))
	{
		showVerify("fm.OutRiskCode","请从下拉列表中选择险种！","codeno");
		return false;
	}
	var sql="select riskcode from fmrisk where OutRiskCode='"+fm.OutRiskCode.value+"'";
	var result = easyQueryVer3(sql,1,0);
	if(!result){
		showVerify("fm.OutRiskCode","请从下拉列表中选择险种！","codeno");
		return false;
	}
	
	var cache = "";
	var cache2 = "";
	var tAmnt = "0";
	var tPrem = "0";
	KindGrid.delBlankLine();
	var kindLen = KindGrid.mulLineCount;
	if(kindLen>0)
	{
		for(var i=0;i<kindLen;i++)
		{
			cache = KindGrid.getRowColData(i,1);
			sql = "select a.RiskKindName from FMKind a "
			+" where a.RiskCode='"+fm.RiskCode.value+"' and a.RiskKindCode='"+cache+"'";
			
			var result=easyQueryVer3(sql,1,0);
			if(!result)
			{
				alert("不存在对应的险别编码!");
				return false;
			}
			
			cache = KindGrid.getRowColData(i,3);
			cache2 = KindGrid.getRowColData(i,4);
			if(charToNum2(cache)>0&&charToNum2(cache2)>0)
			{	
				if(charToNum2(cache2)>=charToNum2(cache))
				{
					alert("第"+(i+1)+"行保额必须大于保费!");
					return false;
				}
			}
			tAmnt = fixMath(charToNum(tAmnt),charToNum2(cache),'+');
			tPrem = fixMath(charToNum3(tPrem),charToNum3(cache2),'+');
		}
		if(charToNum2(tPrem)!=charToNum3(fm.SumPrem.value))
		{
			if(!confirm("险别总保费不等于险种总保费，是否继续？"))
			{
				return false;
			}
		}
		
		if(kindLen>1)
		{
			for(var i=0;i<kindLen;i++)
			{
				for(var j=i+1;j<kindLen;j++)
				{
					if( KindGrid.getRowColData(i,1)==KindGrid.getRowColData(j,1) )
					{
						alert("险别不能重复!");
						return false;
					}
				}
			}
		}
	}
	
	if(!NullOrBlank(fm.PolicyPli.value))
	{
		if(fm.PolicyPli.value.length>250)
		{
			alert("保单特约长度不能大于250！");
			return false;
		}
	}
	if(!NullOrBlank(fm.Remark.value))
	{
		if(fm.Remark.value.length>250)
		{
			alert("补充录入备注长度不能大于250！");
			return false;
		}
	}
	if(!NullOrBlank(fm.ProjCode.value))
	{
		fm.ProtocolNo.value ="jingjixieyi";
	}else
	{
		if(!getProtocolNo())
		{
			alert("无法根据保单上的信息获取对应的协议,请确认是否录入信息准确！");
			return false;
		}
	}
	
  	fm.hidePolicyPli.value=escapes(fm.PolicyPli.value);
  	fm.hideRemark.value=escapes(fm.Remark.value);
  	fm.SignTime.value = fm.hour.value+":"+fm.minute.value+":"+fm.Second.value;
  	showHideCInValiDate();
  	
  	if(fm.ComFirstFeeFlag.value=="Y")
  	{
  		fm.FirstFeeFlag.value="N";
  	}
  	else
  	{
  		fm.FirstFeeFlag.value="Y";
  	}
  	
 /* if(!JudgeCertifyCard())
  	{
  		return false;
  	}
 */ 	

	return true;
}

function initVerify()
{
	var formsNum = 0;	//窗口中的FORM数
	var elementsNum = 0;	//FORM中的元素数
	var passFlag = true;	//校验通过标志
	//遍历所有FORM
	for (formsNum=0; formsNum<window.document.forms.length; formsNum++)
	{
		//遍历FORM中的所有ELEMENT
		for (elementsNum=0; elementsNum<window.document.forms[formsNum].elements.length; elementsNum++)
		{
			window.document.forms[formsNum].elements[elementsNum].verify="";
		}		
	}
}


function saveContClick()
{
	if(tInputType=="Comple")
    {
    	if(NullOrBlank(fm.InnerContNo.value))
		{
			alert("请先查询出一张快速录入的保单,再做补充录入!");
			return false;
		}
    }
	
	mOperate = "INSERT||MAIN";
	//下面增加相应的代码
	if(!beforeContSubmit())
	{
		initVerify();
		return false;
	}
	
	
	submitContForm();
}

function addPolClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  	
  if (!beforePolSubmit())
  {
    initVerify();
    return false;
  }
  
  submitPolForm();
}


function deletePolClick()
{
	if (NullOrBlank(fm.PolNo.value))
	{
		alert("请先选择一条险种再做删除！");
		return false;
	}
	//下面增加相应的代码
	mOperate="DELETE||MAIN"; 
	
	submitPolForm();
}      

function deleteContClick()
{
	if(NullOrBlank(fm.InnerContNo.value))
	{
		alert("请选择一条保单数据,再做删除!");
		return false;
	}
	if(!confirm("确定要删除该保单吗?"))
	{
		return false;
	}
    //下面增加相应的代码
  	mOperate="DELETE||MAIN"; 
  	
  	submitContForm();
}

//若投保人已存在，则带出各项信息
function queryAppntAddress(tCustomerNo,tFlag)
{
	if(NullOrBlank(fm.AppntPostalAddress.value)
	&& NullOrBlank(fm.AppntZipCode.value))
	{
		if(tFlag==1)
		{
			var sql= " select PostalAddress,ZipCode from FDPersonAddress"
			+" where CustomerNo='"+tCustomerNo+"' order by AddressType";
			
			var AppntResult=easyQueryVer3(sql,1,0);
			if(!AppntResult){
			}
			else{
				var AppntSelected = decodeEasyQueryResult(AppntResult);
				fm.AppntPostalAddress.value=AppntSelected[0][0];
				fm.AppntZipCode.value=AppntSelected[0][1];
			}
		}
		else if(tFlag==2)
		{
			var sql= " select GrpAddress,GrpZipCode from FDGrpAddress"
			+" where CustomerNo='"+tCustomerNo+"' order by AddressType";
									
			var AppntResult=easyQueryVer3(sql,1,0);
			if(!AppntResult){
			}
			else{
				var AppntSelected = decodeEasyQueryResult(AppntResult);
				fm.AppntPostalAddress.value=AppntSelected[0][0];
				fm.AppntZipCode.value=AppntSelected[0][1];
			}
		}
	}
}

//若被保人已存在，则带出各项信息
function queryInsuredAddress(tCustomerNo,tFlag)
{
	if(NullOrBlank(fm.InsuredPostalAddress.value)
	&& NullOrBlank(fm.InsuredZipCode.value))
	{
		if(tFlag==1)
		{
			var sql= " select PostalAddress,ZipCode from FDPersonAddress"
			+" where CustomerNo='"+tCustomerNo+"' order by AddressType";
			
			var InsuredResult=easyQueryVer3(sql,1,0);
			if(!InsuredResult){
			}
			else{
				var InsuredSelected = decodeEasyQueryResult(InsuredResult);
				fm.InsuredPostalAddress.value=InsuredSelected[0][0];
				fm.InsuredZipCode.value=InsuredSelected[0][1];
			}
		}
		else if(tFlag==2)
		{
			var sql= " select GrpAddress,GrpZipCode from FDGrpAddress"
			+" where CustomerNo='"+tCustomerNo+"' order by AddressType";
									
			var InsuredResult=easyQueryVer3(sql,1,0);
			if(!InsuredResult){
			}
			else{
				var InsuredSelected = decodeEasyQueryResult(InsuredResult);
				fm.InsuredPostalAddress.value=InsuredSelected[0][0];
				fm.InsuredZipCode.value=InsuredSelected[0][1];
			}
		}
	}
}

//若经办人已存在，则带出各项信息
function queryHandlingAddress(tCustomerNo,tFlag)
{
	if(NullOrBlank(fm.HandlingPostalAddress.value)
	&& NullOrBlank(fm.HandlingZipCode.value))
	{
		var sql= " select PostalAddress,ZipCode from FDPersonAddress"
		+" where CustomerNo='"+tCustomerNo+"' order by AddressType";
		
		var HandlingResult=easyQueryVer3(sql,1,0);
		if(!HandlingResult){
		}
		else{
			var HandlingSelected = decodeEasyQueryResult(HandlingResult);
			fm.HandlingPostalAddress.value=HandlingSelected[0][0];
			fm.HandlingZipCode.value=HandlingSelected[0][1];
		}
	}
}

function showKindList(tInnerContNo)
{
	KindGrid.clearData();
	initKindGrid();
	var sql=" select distinct a.RiskKindCode,a.RiskKindName,a.Amount,a.SumPrem"
	+" from FCItemKind a"
	+" where a.InnerContNo='"+tInnerContNo+"'";
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		displayMultiline(resultArr,KindGrid);
	}
}

//根据代理人来默认显示是否参与考核积分 和是否放弃授权
function showKhJfFlag()
{
	if(!NullOrBlank(fm.AgentCode.value))
	{
		var sql = "select a.Name,a.IsAssessScore,(select codename from fdcode where codetype='yesorno' and code =a.IsAssessScore ),"
				+" a.IsBackGra,(select codename from fdcode where codetype='yesorno' and code =a.IsBackGra ) "
				+" from FAAgentTree a where a.AgentCode='"+fm.AgentCode.value+"' "
				+getManageComLimitlike1("a.agentgroup");
		var result=easyQueryVer3(sql,1,0);
		if(!result&&!NullOrBlank(fm.AgentCode.value))
		{
		}
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			//是否参与内部考核积分
			if(!NullOrBlank(resultArr[0][1]))
			{
				fm.RightSendFlag.value = resultArr[0][1];
				fm.RightSendFlagName.value = resultArr[0][2];
			}else
			{
				fm.RightSendFlag.value = "N";
				fm.RightSendFlagName.value = "否";
			}
			//是否放弃并授权
			if(!NullOrBlank(resultArr[0][3]))
			{
				fm.AccreditFlag.value = resultArr[0][3];
				fm.AccreditFlagName.value = resultArr[0][4];
			}else
			{
				fm.AccreditFlag.value = "N";
				fm.AccreditFlagName.value = "否";
			}
		}
				
	}
}

function showAgentName()
{
	if(NullOrBlank(fm.AgentCode.value))
	{
		fm.AgentCode.value = "";
		fm.AgentName.value = "";
		fm.AgentGroup.value = "";
		fm.AgentGroupName.value = "";
		
		return;
	}
	

	var sql = "select a.Name,a.AgentState,a.ApproveState,a.EmployDate,a.OfficeFlag, "
	+" a.IsAssessScore,(select codename from fdcode where codetype='yesorno' and code =a.IsAssessScore ),"
	+" a.IsBackGra,(select codename from fdcode where codetype='yesorno' and code =a.IsBackGra ) "
	+" from FAAgentTree a "
	+" where a.AgentCode='"+fm.AgentCode.value+"'"
	+getManageComLimitlike2("a.corporation");
	var result=easyQueryVer3(sql,1,0);
	if(!result&&!NullOrBlank(fm.AgentCode.value))
	{
		showVerify("fm.AgentCode","代理人编码错误,不存在对应的代理人！");
		return;
	}
	
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		if(resultArr[0][1]==null||resultArr[0][1]==""||resultArr[0][1]=="03"||resultArr[0][1]=="04")
		{
			showVerify("fm.AgentCode","代理人编码对应代理人不是在职代理人！");
			return;
		}
		
		if(resultArr[0][2]==null||resultArr[0][2]==""||resultArr[0][2]=="N")
		{
			showVerify("fm.AgentCode","代理人编码对应代理人未经过审核！");
			return;
		}
		
		if(!NullOrBlank(resultArr[0][3])&&!NullOrBlank(fm.SignDate.value))
		{
			if(resultArr[0][3]>fm.SignDate.value)
			{
				showVerify("fm.AgentCode","代理人的入司时间不能大于出单日期！");
				return;
			}
		}
		
		if(!NullOrBlank(fm.ProjCode.value))
		{}else
		{
			if(NullOrBlank(resultArr[0][4])||resultArr[0][4]=="N")
			{
				showVerify("fm.AgentCode","无项目查询返回的须由工作室出单！");
		  		return;
			}
		}
		
		fm.AgentName.value = resultArr[0][0];
		
	}
	
	var sqlPart="";
	var sqlPart1="";
	if(tComType=="00"&&tComType=="01"&&tComType=="02")
	{
		sqlPart=getManageComLimitlike3("b.Corporation",UserComCode);
		sqlPart1=getManageComLimitlike3("b.Corporation",UserComCode);
	}
	else
	{
		sqlPart=" and b.Corporation='"+Corporate+"'";
		sqlPart1=" and b.Corporation='"+Corporate+"'";
	}
	
	if(!NullOrBlank(fm.ProjCode.value))
	{
	}else
	{
		sqlPart+=" and b.OfficeFlag='Y' ";
		sqlPart1+=" and a.OfficeFlag='Y' ";
	}
	sql = "select b.AgentGroup,(select ComName from FDCom where ComCode=b.AgentGroup) from FAAgentTree b"
	+" where b.AgentCode='"+fm.AgentCode.value+"' and b.BranchType<>'3'"
	+sqlPart;
	+getManageComLimitlike2("b.corporation");
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);

	if(resultArr!=null)
	{
		if(resultArr.length==1)
		{
			fm.AgentGroup.value = resultArr[0][0];
			fm.AgentGroupName.value = resultArr[0][1];
			
			afterCodeSelect( 'AgentToGroup', '');
		}
	}
	else
	{
		showVerify("fm.AgentCode","代理人编码错误,您没有该代理人的权限！");
		return;
	}
}

function afterQueryAgent(tAgentCode,tAgentName,tAgentGroup,tAgentGroupName,tCorporation,tAgentGrade,tRightSendFlag,tRightSendFlagName,
                         tAccreditFlag,tAccreditFlagName )
{
	fm.AgentCode.value = tAgentCode;
	fm.AgentName.value = tAgentName;
	fm.AgentGroup.value = tAgentGroup;
	fm.AgentGroupName.value = tAgentGroupName;
	fm.Corporation.value = tCorporation;
	//是否参与内部考核积分
	if(!NullOrBlank(tRightSendFlag))
	{
		fm.RightSendFlag.value = tRightSendFlag;
		fm.RightSendFlagName.value = tRightSendFlagName;
	}else
	{
		fm.RightSendFlag.value = "N";
		fm.RightSendFlagName.value = "否";
	}
	//是否放弃并授权
	if(!NullOrBlank(tAccreditFlag))
	{
		fm.AccreditFlag.value = tAccreditFlag;
		fm.AccreditFlagName.value = tAccreditFlagName;
	}else
	{
		fm.AccreditFlag.value = "N";
		fm.AccreditFlagName.value = "否";
	}
	fm.RightSendFlag.focus();
}

function queryAgent()
{
	var tProjCode = fm.ProjCode.value;
	openWindow("./FAAgentQuery.html?tPageFrom=prop&tProjCode="+tProjCode);
}

function queryCustomer(flag)
{
	if(flag==3)
	{
		openWindow("../customer/FDCustomerQueryMain.jsp?tType=InsuredCust");
	}
	if(flag==2)
	{
		openWindow("../customer/FDCustomerQueryMain.jsp?tType=AppntCust");
	}
	if(flag==1)
	{
		openWindow("../customer/FDCustomerQueryMain.jsp?tType=HandlingCust");
	}
}

function afterCodeSelect( cName, Filed)
{
	if( cName == 'inscom5'|| cName == 'inscom6' )
	{
		fm.KindCode.value = "";
		fm.RiskCode.value = "";
		fm.OutRiskCode.value = "";
		fm.RiskName.value = "";
		
		var sql="select ContNo,PolicyPrtNo,InvoicePrtNo,InsCardPrtNo,CInsFlagPrtNo from FCCont where InnerContNo="
		+" (select max(InnerContNo) from FCCont where SupplierCode = '" + fm.SupplierCode.value + "' and Corporation='" + Corporate + "')";
		
		var result=easyQueryVer3(sql,1,0);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			if(resultArr[0][0].length>6)
			{
				fm.ContNo.value = resultArr[0][0].substring(0,resultArr[0][0].length-6);
			}
			
			if(NullOrBlank(fm.PolicyPrtNo.value))
			{
				if(resultArr[0][1].length>2)
				{
					fm.PolicyPrtNo.value = resultArr[0][1].substring(0,resultArr[0][1].length-2);
				}
			}
			if(NullOrBlank(fm.InvoicePrtNo.value))
			{
				if(resultArr[0][2].length>2)
				{
					fm.InvoicePrtNo.value = resultArr[0][2].substring(0,resultArr[0][2].length-2);
				}
			}
			if(NullOrBlank(fm.InsCardPrtNo.value))
			{
				if(resultArr[0][3].length>2)
				{
					fm.InsCardPrtNo.value = resultArr[0][3].substring(0,resultArr[0][3].length-2);
				}
			}
			if(NullOrBlank(fm.CInsFlagPrtNo.value))
			{
				if(resultArr[0][4].length>2)
				{
					fm.CInsFlagPrtNo.value = resultArr[0][4].substring(0,resultArr[0][4].length-2);
				}
			}
		}
	}
	
	if( cName == 'riskcode8' ||cName == 'riskcode11' )
	{	
		if(fm.KindCode.value!='112')
		{
			fm.CInsFlagPrtNo.value="";
		}
		var tRiskCode=fm.RiskCode.value;
		if(mRiskCode!=tRiskCode)
		{
			mRiskCode = tRiskCode;
			initKindGrid();
			//根据险种判断手续费率是否是规制引擎来的
			if(!NullOrBlank(fm.RuleID.value)){
				fm.FYCType.disabled = true;
				fm.FYCTypeName.disabled = true;
			}else{//根据费率等级来
				getFYCType();
			}
		}
	}
	
	if( cName == 'AgentToGroup' )
	{
		var sql = "select a.ComCode from FDCom a "
		+" where ComType='03' and ComNature='Y' and  EndFlag = 'N'"
		+" and ComCode in(select ComCode from FDCom connect by PRIOR "
		+" UPComCode = ComCode start with  ComCode = '" + fm.AgentGroup.value + "')";
		
		var result=easyQueryVer3(sql,1,0);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			fm.Corporation.value = resultArr[0][0];
		}
	}
	
	if( cName == 'BussNature2' )
	{
		if(fm.BussNature.value=="02")
		{
			channel.style.display = "";
		}
		else
		{
			fm.ChannelCode.value = "";
			fm.ChannelName.value = "";
			channel.style.display = "none";
		}
	}
}

function getFYCType()
{
	fm.FYCType.value = "";
	fm.FYCTypeName.value = "";
	fm.FYCTypeReMark.value ="";
	
	var sql="select distinct a.GradeCode,b.GradeName,b.Remark from FMEstateProtocolFeeRate a,FMProtocolgrade b "
	+"where b.indexcode=a.GradeCode "
	+"and a.ManageCom in(select ComCode from FDCom connect by PRIOR UPComCode = ComCode start with  ComCode = '"+fm.Corporation.value+"') "
	+"and a.SupplierCode ='" +fm.SupplierCode.value+ "' "
	+"and a.RiskCode ='" +fm.RiskCode.value+ "' "
	+"and a.Iseffect ='Y' "
	+"and a.StartDate <='" +fm.SignDate.value+ "' "
	+"and a.EndDate >='" +fm.SignDate.value+ "' "
	+"and (a.SctrlDate>='" +fm.SignDate.value+ "' or a.SctrlDate is null)";
			
	var result=easyQueryVer3(sql,1,0);
	
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		if(resultArr.length==1)
		{
			fm.FYCType.value = resultArr[0][0];
			fm.FYCTypeName.value = resultArr[0][1];
			fm.FYCTypeReMark.value = resultArr[0][2];
		}
	}
}

function getProtocolNo()
{
	var sql =" select a.ProtocolNo from FMProtocol a,FMProtocolRisk b "
	+" where a.ProtocolNo=b.ProtocolNo"
	+" and a.ManageCom='"+fm.Corporation.value+"'"
	+" and a.CorpCode='"+fm.SupplierCode.value+"' and a.CorpType='01'"
	+" and a.TypeKind='02' and a.PStatus='1'"
	+" and a.PStartDate<='"+fm.SignDate.value+"' and a.PEndDate>='"+fm.SignDate.value+"'"
	+" and b.RiskCode='"+fm.RiskCode.value+"'"
	+" and b.StartDate<='"+fm.SignDate.value+"' and b.EndDate>='"+fm.SignDate.value+"'";

	var result=easyQueryVer3(sql,1,0);

	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.ProtocolNo.value = resultArr[0][0];
		return true;
	}
	else
	{
		var sql =" select a.ProtocolNo from FMProtocol a,FMProtocolRisk b,FDCom c "
		+" where a.ProtocolNo=b.ProtocolNo"
		+" and c.ComCode='"+fm.Corporation.value+"'"
		+" and a.ManageCom=c.UpComCode"
		+" and a.CorpCode='"+fm.SupplierCode.value+"' and a.CorpType='01'"
		+" and a.TypeKind='02' and a.PStatus='1'"
		+" and a.PStartDate<='"+fm.SignDate.value+"' and a.PEndDate>='"+fm.SignDate.value+"'"
		+" and b.RiskCode='"+fm.RiskCode.value+"'"
		+" and b.StartDate<='"+fm.SignDate.value+"' and b.EndDate>='"+fm.SignDate.value+"'";
		
		var result=easyQueryVer3(sql,1,0);
		
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			fm.ProtocolNo.value = resultArr[0][0];
			return true;
		}
	}
	return false;
}

function fullClick()
{
	parent.fraInterface.location.href="./ScanCarContInput.jsp?ThisSYSPageType=query";	//转换连接，比直接用a的方式快一些
	parent.VD.location.reload();	//用来清除缓存的操作，看看能否不采用这个方式
}


//根据生效日期,加1年减1天显示失效日期
function showCInValiDate()
{
	fm.CInValiDate.value = calDate(fm.CValiDate.value,"1","","-1");
}

//根据失效日期,减1天显示失效日期
function showCInValiDate2(tCInValiDate)
{
	fm.CInValiDate.value = calDate(tCInValiDate,"","","-1");
}

//保存失效日期时,加1天保存
function showHideCInValiDate()
{
	fm.hideCInValiDate.value = calDate(fm.CInValiDate.value,"","","1");
}

function addContVerify()
{
	initVerify();
	fm.PolicyPrtNo.verify="保单印刷号|len<=100";
	fm.InvoicePrtNo.verify="发票印刷号|len<=100";
	fm.InsCardPrtNo.verify="保险卡印刷号|len<=100";
	fm.CInsFlagPrtNo.verify="交强险标志印刷号|len<=100";
	fm.BussNature.verify="业务性质|code:BussNature2&notnull";
	fm.OutManageCom.verify="出单机构|notnull";
	fm.GetManageCom.verify="接单机构|notnull";
	fm.LicenseNo.verify="车牌号|notnull&len<=10";
	fm.ComFirstFeeFlag.verify="是否见费出单|code:yesorno&notnull";
	fm.RightSendFlag.verify="是否参与内部考核积分|code:yesorno&notnull";
	fm.AccreditFlag.verify="是否放弃并授权|code:yesorno&notnull";
	fm.SupplierShowCode.verify="保险公司|notnull";
	fm.ContNo.verify="保单号|notnull&len<=40";
	fm.SignDate.verify="出单日期|notnull&date";
	fm.hour.verify="出单时间的小时|notnull&NUM&len=2";
	fm.minute.verify="出单时间的分钟|notnull&NUM&len=2";
	fm.UWDate.verify="签单日期|notnull&date";
	fm.CValiDate.verify="保单生效日期|notnull&date";
	fm.CInValiDate.verify="保单有效日期至|notnull&date";
	fm.InsuredCustType.verify="被保人类型|notnull&code:CustType";
	fm.InsuredName.verify="被保人姓名|notnull&len<=60";
	fm.AgentCode.verify="代理人编码|notnull";
	fm.AgentName.verify="代理人姓名|notnull";
	//fm.AgentGroup.verify="代理人所在团队|notnull&code:AgentGroup";
	fm.AgentScore.verify="代理人积分|int&len<=14";
		
	if(tInputType=="Full" ||tInputType=="Comple")
	{
		fm.POSNo.verify="POS机交易号|len<=20";
		fm.PayPCommFlag.verify="是否垫付佣金|code:yesorno";
		fm.PayPComm.verify="垫付佣金金额|NUM&len<=16";
		fm.InsuredIDType.verify="被保人证件类型|code:allidtype";
		fm.InsuredIDNo.verify="被保人证件号码|len<=20";
		fm.InsuredPostalAddress.verify="地址|len<=40";
		fm.InsuredZipCode.verify="邮政编码|ZIPCODE";
		fm.InsuredMobile.verify="被保人手机|len<=15&TELEPHONE";
		fm.InsuredMobile2.verify="被保人小灵通|len<=15&TELEPHONE";
		fm.InsuredPhone.verify="被保人办公电话|len<=15&TELEPHONE";
		fm.InsuredHomePhone.verify="被保人家庭电话|len<=15&TELEPHONE";
		fm.AppntCustType.verify="投保人类型|code:CustType";
		fm.AppntName.verify="投保人姓名|len<=60";
		fm.AppntIDType.verify="投保人证件类型|code:allidtype";
		fm.AppntIDNo.verify="投保人证件号码|len<=20";
		fm.AppntPostalAddress.verify="投保人地址|len<=40";
		fm.AppntZipCode.verify="投保人邮政编码|ZIPCODE";
		fm.AppntMobile.verify="投保人手机|len<=15&TELEPHONE";
		fm.AppntMobile2.verify="投保人小灵通|len<=15&TELEPHONE";
		fm.AppntPhone.verify="投保人办公电话|len<=15&TELEPHONE";
		fm.AppntHomePhone.verify="投保人家庭电话|len<=15&TELEPHONE";

		fm.HandlingName.verify="经办人姓名|len<=60";
		fm.HandlingIDType.verify="经办人证件类型|code:allidtype";
		fm.HandlingIDNo.verify="经办人证件号码|len<=20";
		fm.HandlingPostalAddress.verify="经办人地址|len<=40";
		fm.HandlingZipCode.verify="经办人邮政编码|ZIPCODE";
		fm.HandlingMobile.verify="经办人手机|len<=15&TELEPHONE";
		fm.HandlingMobile2.verify="经办人小灵通|len<=15&TELEPHONE";
		fm.HandlingPhone.verify="经办人办公电话|len<=15&TELEPHONE";
		fm.HandlingHomePhone.verify="经办人家庭电话|len<=15&TELEPHONE";
		
		fm.OldSupplierShowCode.verify="被保人上张保单保险公司|code:AllInsCom";
		fm.OldContNo.verify=verify="被保人上张保单保单号|len<=40";
	}
	
	fm.CarOwner.verify="车主|len<=30";
	fm.CarKindCode.verify="车辆种类|code:carkind";
	fm.UseNatureCode.verify="使用性质|notnull&code:carusages";
	fm.FrameNo.verify="车架号|len<=30";
	fm.EngineNo.verify="发动机号|len<=30";
	fm.ModelCode.verify="厂牌车型|len<=50";
	fm.YearExamDate.verify="年审到期日|Date";
	fm.EnrollDate.verify="初次登记日期|Date";
	fm.BuyDate.verify="购置日期|Date";
	fm.VINNo.verify="VIN码|len=17";
	fm.RunAreaName.verify="行驶区域|len<=30";
	fm.HKFlag.verify="是否港澳车标志|code:yesorno";
	fm.HKLicenseNo.verify="港澳车牌号码|len<=10";
	fm.UseYears.verify="使用年限|len<=12&num";
	fm.RunMiles.verify="行驶里程|len<=14&num";
	fm.CountryNature.verify="国别性质|code:countrynature";
	fm.CountryCode1.verify="生产国家|code:country";
	fm.LicenseColorCode.verify="车牌底色|code:licensecolor";
	fm.SeatCount1.verify="座位数|len<=8&int";
	fm.TonCount.verify="吨位数|len<=8&num";
	fm.ExhaustScale.verify="排量|len<=4&num";
	fm.ColorCode.verify="车身颜色|code:carcolor";
	fm.SafeDevice.verify="安全配置|len<=15";
	fm.ParkSite.verify="固定停放地点|len<=15";
	fm.OwnerAddress.verify="购车人地址|len<=20";
	fm.ProduceDate.verify="生产日期|Date";
	fm.CarUsage.verify="购车用途|len<=10";
	fm.PurchasePrice.verify="新车购置价格(万元)|notnull&Money:8-4&value>=0&value<=1000";
	fm.InvoiceNo.verify="购车发票号|len<=10";
	fm.CarChecker.verify="验车人|len<=30";
	fm.CarCheckTime.verify="验车日期|Date";
	fm.CarCheckStatus.verify="验车情况|code:carcheck";
	fm.CarDealerName.verify="经销商名称|len<=60";
	
	fm.OutRiskCode.verify="险种编码|NOTNULL";
	if(NullOrBlank(fm.RuleID.value)){
		fm.FYCType.verify="费率等级|NOTNULL";
	}
	fm.SumPrem.verify="总保费|notnull&NUM&value>=0&Money:15-2";
	fm.tax1.verify="车船税|NUM&value>=0&Money:15-2";
	fm.tax2.verify="印花税|NUM&value>=0&Money:15-2";
}

function initPolForm()
{
	fm.PolNo.value = "";
	fm.RiskCode.value = "";
	fm.OutRiskCode.value = "";
	fm.RiskName.value = "";
	fm.SumPrem.value = "";
	fm.tax1.value = "";
	fm.tax2.value = "";
	fm.KindCode.value = "";
	fm.FYCType.value = "";
	fm.FYCTypeName.value = "";
	fm.FYCTypeReMark.value ="";
}

function initItemForm()
{	
	//车辆信息
	fm.CarOwner.value = "";
	fm.LicenseNo.value = "";
	fm.CarKindCode.value = "";
	fm.CarKindName.value = "";
	fm.UseNatureCode.value = "";
	fm.UseNatureName.value = "";
	fm.FrameNo.value = "";
	fm.EngineNo.value = "";
	fm.ModelCode.value = "";
	fm.YearExamDate.value = "";
	fm.EnrollDate.value = "";
	fm.BuyDate.value = "";
	fm.VINNo.value = "";
	fm.RunAreaName.value = "";
	fm.HKFlag.value = "N";
	fm.HKFlagName.value = "否";
	fm.HKLicenseNo.value = "";
	fm.UseYears.value = "";
	fm.RunMiles.value = "";
	fm.CountryNature.value = "";
	fm.CountryNatureName.value = "";
	fm.CountryCode1.value = "";
	fm.CountryCode1Name.value = "";
	fm.LicenseColorCode.value = "";
	fm.LicenseColorName.value = "";
	fm.SeatCount1.value = "";
	fm.TonCount.value = "";
	fm.ExhaustScale.value = "";
	fm.ColorCode.value = "";
	fm.ColorName.value = "";
	fm.SafeDevice.value = "";
	fm.ParkSite.value = "";
	fm.OwnerAddress.value = "";
	fm.ProduceDate.value = "";
	fm.CarUsage.value = "";
	fm.PurchasePrice.value = "";
	fm.InvoiceNo.value = "";
	fm.CarChecker.value = "";
	fm.CarCheckTime.value = "";
	fm.CarCheckStatus.value = "";
	fm.CarCheckStatusName.value = "";
	fm.CarDealerName.value = "";
}

function updatePolClick()
{
	if (NullOrBlank(fm.PolNo.value))
	{
		alert("请先选择一条险种再做修改！");
		return false;
	}
	//下面增加相应的代码
	mOperate="UPDATE||MAIN";
	
	if (!beforePolSubmit())
		return false;
	
	submitPolForm();
}

function getCarByInnerContNo(tInnerContNo)
{
	fm.ItemCode.value = "06";
	fm.ItemName.value = "车辆";
	fm.ItemType.value="CarInfo";
	
	var sql="select a.ItemNo,a.CarOwner,a.LicenseNo,a.CarKindCode,a.UseNatureCode,"
	+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,a.BuyDate,"
	+" a.VINNo,a.RunAreaName,a.HKFlag,a.HKLicenseNo,a.UseYears,a.RunMiles,"
	+" a.CountryNature,a.CountryCode,a.LicenseColorCode,a.SeatCount,a.TonCount,"
	+" a.ExhaustScale,a.ColorCode,a.SafeDevice,a.ParkSite,a.OwnerAddress,"
	+" a.ProduceDate,a.CarUsage,a.PurchasePrice,a.InvoiceNo,a.CarChecker,"
	+" a.CarCheckTime,a.CarCheckStatus,a.CarDealerName"
	+" from FCItemCar a where a.InnerContNo='"+tInnerContNo+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.ItemNo.value = resultArr[0][0];
		fm.CarOwner.value = resultArr[0][1];
		fm.LicenseNo.value = resultArr[0][2];
		fm.CarKindCode.value = resultArr[0][3];
		fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][3]);
		fm.UseNatureCode.value = resultArr[0][4];
		fm.UseNatureName.value = getNameFromLDCode("carusages",resultArr[0][4]);
		fm.FrameNo.value = resultArr[0][5];
		fm.EngineNo.value = resultArr[0][6];
		fm.ModelCode.value = resultArr[0][7];
		fm.YearExamDate.value = resultArr[0][8];
		fm.EnrollDate.value = resultArr[0][9];
		fm.BuyDate.value = resultArr[0][10];
		fm.VINNo.value = resultArr[0][11];
		fm.RunAreaName.value = resultArr[0][12];
		fm.HKFlag.value = resultArr[0][13];
		fm.HKFlagName.value = getNameFromLDCode("yesorno",resultArr[0][13]);
		fm.HKLicenseNo.value = resultArr[0][14];
		fm.UseYears.value = resultArr[0][15];
		fm.RunMiles.value = resultArr[0][16];
		fm.CountryNature.value = resultArr[0][17];
		fm.CountryNatureName.value = getNameFromLDCode("countrynature",resultArr[0][17]);
		fm.CountryCode1.value = resultArr[0][18];
		fm.CountryCode1Name.value = getNameFromLDCode("country",resultArr[0][18]);
		fm.LicenseColorCode.value = resultArr[0][19];
		fm.LicenseColorName.value = getNameFromLDCode("licensecolor",resultArr[0][19]);
		fm.SeatCount1.value = resultArr[0][20];
		fm.TonCount.value = resultArr[0][21];
		fm.ExhaustScale.value = resultArr[0][22];
		fm.ColorCode.value = resultArr[0][23];
		fm.ColorName.value = getNameFromLDCode("carcolor",resultArr[0][23]);
		fm.SafeDevice.value = resultArr[0][24];
		fm.ParkSite.value = resultArr[0][25];
		fm.OwnerAddress.value = resultArr[0][26];
		fm.ProduceDate.value = resultArr[0][27];
		fm.CarUsage.value = resultArr[0][28];
		fm.PurchasePrice.value = resultArr[0][29];
		fm.InvoiceNo.value = resultArr[0][30];
		fm.CarChecker.value = resultArr[0][31];
		fm.CarCheckTime.value = resultArr[0][32];
		fm.CarCheckStatus.value = resultArr[0][33];
		fm.CarCheckStatusName.value = getNameFromLDCode("carcheck",resultArr[0][33]);
		fm.CarDealerName.value = resultArr[0][34];
	}else
	{
		alert("系统中查询不到车牌号相同的车辆信息");
	}
}


function confirmClick()
{
	if(fm.FullFlag.value=="false")
		mOperate = "CONFIRM||FAST";
	else if(fm.FullFlag.value=="true")
		mOperate = "CONFIRM||FULL";
	
	if(NullOrBlank(fm.InnerContNo.value))
	{
		alert("请先选择一条保单数据,再做录入完毕!");
		return false;
	}
	//	车险，非车险，录入完毕的时候 产品定义不选择发放权益证类型的情况下，出单的 地方不 能够选发放权益证
	if(fm.RightSendFlag.value=="Y")
	{
		var sql="select count(SendBenefitType) from FMRisk where RiskCode ='"+fm.RiskCode.value+"'";
		var result=easyQueryVer3(sql,1,0);
		
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr==0)
		{
			alert("产品定义未选择是否参与内部考核积分，是否参与内部考核积分不能选择“是”");
			fm.RightSendFlag.focus();
			return false;
		}
	}
	
	var i = 0;
	var showStr="正在查询数据，请您稍候";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;  
	
	showInfo=window.showModelessDialog(urlStr,window,WINDOWSSIZE);   
	fm.hideOperate.value=mOperate;

	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	
	fm.action="FCCarContFullEndSave.jsp";
	fm.submit(); //提交
}

function showAppnt()
{
	if(fm.appnt.checked==true)
	{
		fm.all('AppntNo').value=fm.all('InsuredNo').value;
		fm.all('AppntCustType').value=fm.all('InsuredCustType').value;
		fm.all('AppntCustTypeName').value=fm.all('InsuredCustTypeName').value;
	  	fm.all('AppntName').value=fm.all('InsuredName').value;
	  	fm.all('AppntIDType').value=fm.all('InsuredIDType').value;
	  	fm.all('AppntIDTypeName').value=fm.all('InsuredIDTypeName').value;
	  	fm.all('AppntIDNo').value=fm.all('InsuredIDNo').value;
	  	fm.all('AppntPostalAddress').value=fm.all('InsuredPostalAddress').value;
	  	fm.all('AppntZipCode').value=fm.all('InsuredZipCode').value;
	  	fm.all('AppntMobile').value=fm.all('InsuredMobile').value;
	  	fm.all('AppntMobile2').value=fm.all('InsuredMobile2').value;
	  	fm.all('AppntPhone').value=fm.all('InsuredPhone').value;
	  	fm.all('AppntHomePhone').value=fm.all('InsuredHomePhone').value;
	  	fm.all('AppntMemberType').value=fm.all('InsuredMemberType').value;
	  	fm.all('AppntMemberTypeName').value=fm.all('InsuredMemberTypeName').value;
	  	
	  	fm.all('AppntCustType').disabled=true;
	  	fm.all('AppntCustTypeName').disabled=true;
	  	fm.all('AppntName').disabled=true;
	  	fm.all('AppntIDType').disabled=true;
	  	fm.all('AppntIDTypeName').disabled=true;
	  	fm.all('AppntIDNo').disabled=true;
	  	fm.all('AppntPostalAddress').disabled=true;
	  	fm.all('AppntZipCode').disabled=true;
	  	fm.all('AppntMobile').disabled=true;
	  	fm.all('AppntMobile2').disabled=true;
	  	fm.all('AppntPhone').disabled=true;
	  	fm.all('AppntHomePhone').disabled=true;
	  	fm.all('AppntMemberType').disabled=true;
	  	fm.all('AppntMemberTypeName').disabled=true;
	  	fm.butAppntCus.disabled=true;
	  	
	}
	else
	{
		fm.all('AppntNo').value="";
		fm.all('AppntCustType').value="";
		fm.all('AppntCustTypeName').value="";
	  	fm.all('AppntName').value="";
	  	fm.all('AppntIDType').value="";
	  	fm.all('AppntIDTypeName').value="";
	  	fm.all('AppntIDNo').value="";
	  	fm.all('AppntPostalAddress').value="";
	  	fm.all('AppntZipCode').value="";
	  	fm.all('AppntMobile').value="";
	  	fm.all('AppntMobile2').value="";
	  	fm.all('AppntPhone').value="";
	  	fm.all('AppntHomePhone').value="";
		fm.all('AppntMemberType').value="";
	  	fm.all('AppntMemberTypeName').value="";
	  	
	  	fm.all('AppntCustType').disabled=false;
	  	fm.all('AppntCustTypeName').disabled=false;
	  	fm.all('AppntName').disabled=false;
	  	fm.all('AppntIDType').disabled=false;
	  	fm.all('AppntIDTypeName').disabled=false;
	  	fm.all('AppntIDNo').disabled=false;
	  	fm.all('AppntPostalAddress').disabled=false;
	  	fm.all('AppntZipCode').disabled=false;
	  	fm.all('AppntMobile').disabled=false;
	  	fm.all('AppntMobile2').disabled=false;
	  	fm.all('AppntPhone').disabled=false;
	  	fm.all('AppntHomePhone').disabled=false;
	  	fm.all('AppntMemberType').disabled=false;
	  	fm.all('AppntMemberTypeName').disabled=false;
	  	
	  	fm.butAppntCus.disabled=false;
	}
}

function showHandling()
{
	if(fm.InsuredCustType.value=="1")
	{
		if(fm.handling.checked==true)
		{
			fm.all('HandlingNo').value=fm.all('InsuredNo').value;
		  	fm.all('HandlingName').value=fm.all('InsuredName').value;
		  	fm.all('HandlingIDType').value=fm.all('InsuredIDType').value;
		  	fm.all('HandlingIDTypeName').value=fm.all('InsuredIDTypeName').value;
		  	fm.all('HandlingIDNo').value=fm.all('InsuredIDNo').value;
		  	fm.all('HandlingPostalAddress').value=fm.all('InsuredPostalAddress').value;
		  	fm.all('HandlingZipCode').value=fm.all('InsuredZipCode').value;
		  	fm.all('HandlingMobile').value=fm.all('InsuredMobile').value;
		  	fm.all('HandlingMobile2').value=fm.all('InsuredMobile2').value;
		  	fm.all('HandlingPhone').value=fm.all('InsuredPhone').value;
		  	fm.all('HandlingHomePhone').value=fm.all('InsuredHomePhone').value;
		  	fm.all('HandlingMemberType').value=fm.all('InsuredMemberType').value;
		  	fm.all('HandlingMemberTypeName').value=fm.all('InsuredMemberTypeName').value;
		  	
		  	fm.all('HandlingName').disabled=true;
		  	fm.all('HandlingIDType').disabled=true;
		  	fm.all('HandlingIDTypeName').disabled=true;
		  	fm.all('HandlingIDNo').disabled=true;
		  	fm.all('HandlingPostalAddress').disabled=true;
		  	fm.all('HandlingZipCode').disabled=true;
		  	fm.all('HandlingMobile').disabled=true;
		  	fm.all('HandlingMobile2').disabled=true;
		  	fm.all('HandlingPhone').disabled=true;
		  	fm.all('HandlingHomePhone').disabled=true;
		  	fm.all('HandlingMemberType').disabled=true;
		  	fm.all('HandlingMemberTypeName').disabled=true;
		  	
		    fm.butHandlingCus.disabled=true; 	
		}
		else
		{
			fm.all('HandlingNo').value="";
		  	fm.all('HandlingName').value="";
		  	fm.all('HandlingIDType').value="";
		  	fm.all('HandlingIDTypeName').value="";
		  	fm.all('HandlingIDNo').value="";
		  	fm.all('HandlingPostalAddress').value="";
		  	fm.all('HandlingZipCode').value="";
		  	fm.all('HandlingMobile').value="";
		  	fm.all('HandlingMobile2').value="";
		  	fm.all('HandlingPhone').value="";
		  	fm.all('HandlingHomePhone').value="";
			fm.all('HandlingMemberType').value="";
		  	fm.all('HandlingMemberTypeName').value="";
		  	
			fm.all('HandlingName').disabled=false;
	  		fm.all('HandlingMobile').disabled=false;
	  		fm.all('HandlingMobile2').disabled=false;
		  	fm.all('HandlingPhone').disabled=false;
		  	fm.all('HandlingHomePhone').disabled=false;
		  	fm.all('HandlingIDType').disabled=false;
		  	fm.all('HandlingIDTypeName').disabled=false;
		  	fm.all('HandlingIDNo').disabled=false;
		  	fm.all('HandlingPostalAddress').disabled=false;
		  	fm.all('HandlingZipCode').disabled=false;
		  	fm.all('HandlingMemberType').disabled=false;
		  	fm.all('HandlingMemberTypeName').disabled=false;
		  	
		  	fm.butHandlingCus.disabled=false; 	
		}
	}
	else
	{
		if(fm.handling.checked==true)
		{
			alert("经办人必须为个人!");
			fm.handling.checked=false;
			return false;
		}
		
		
		if(tInputType!="Comple")
	    {
	    	fm.all('HandlingName').disabled=false;
		  	fm.all('HandlingMobile').disabled=false;
		  	fm.all('HandlingMobile2').disabled=false;
		  	fm.all('HandlingPhone').disabled=false;
		  	fm.all('HandlingHomePhone').disabled=false;
	    }

	  	fm.all('HandlingIDType').disabled=false;
	  	fm.all('HandlingIDTypeName').disabled=false;
	  	fm.all('HandlingIDNo').disabled=false;
	  	fm.all('HandlingPostalAddress').disabled=false;
	  	fm.all('HandlingZipCode').disabled=false;
	  	
	  	fm.butHandlingCus.disabled=false; 	
	}
}

function showDisabled()
{
	if(fm.appnt.checked==true)
	{
	  	fm.all('AppntCustType').disabled=true;
	  	fm.all('AppntCustTypeName').disabled=true;
	  	fm.all('AppntName').disabled=true;
	  	fm.all('AppntIDType').disabled=true;
	  	fm.all('AppntIDTypeName').disabled=true;
	  	fm.all('AppntIDNo').disabled=true;
	  	fm.all('AppntPostalAddress').disabled=true;
	  	fm.all('AppntZipCode').disabled=true;
	  	fm.all('AppntMobile').disabled=true;
	  	fm.all('AppntMobile2').disabled=true;
	  	fm.all('AppntPhone').disabled=true;
	  	fm.all('AppntHomePhone').disabled=true;
	  	fm.all('AppntMemberType').disabled=true;
	  	fm.all('AppntMemberTypeName').disabled=true;
	  	
	  	fm.butAppntCus.disabled=true;
	}
	else
	{
	  	fm.all('AppntCustType').disabled=false;
	  	fm.all('AppntCustTypeName').disabled=false;
	  	fm.all('AppntName').disabled=false;
	  	fm.all('AppntIDType').disabled=false;
	  	fm.all('AppntIDTypeName').disabled=false;
	  	fm.all('AppntIDNo').disabled=false;
	  	fm.all('AppntPostalAddress').disabled=false;
	  	fm.all('AppntZipCode').disabled=false;
	  	fm.all('AppntMobile').disabled=false;
	  	fm.all('AppntMobile2').disabled=false;
	  	fm.all('AppntPhone').disabled=false;
	  	fm.all('AppntHomePhone').disabled=false;
	  	fm.all('AppntMemberType').disabled=false;
	  	fm.all('AppntMemberTypeName').disabled=false;
	  	
	  	fm.butAppntCus.disabled=false;
	}
	
	if(fm.handling.checked==true)
	{
	  	fm.all('HandlingName').disabled=true;
	  	fm.all('HandlingIDType').disabled=true;
	  	fm.all('HandlingIDTypeName').disabled=true;
	  	fm.all('HandlingIDNo').disabled=true;
	  	fm.all('HandlingPostalAddress').disabled=true;
	  	fm.all('HandlingZipCode').disabled=true;
	  	fm.all('HandlingMobile').disabled=true;
	  	fm.all('HandlingMobile2').disabled=true;
	  	fm.all('HandlingPhone').disabled=true;
	  	fm.all('HandlingHomePhone').disabled=true;
	  	fm.all('HandlingMemberType').disabled=true;
	  	fm.all('HandlingMemberTypeName').disabled=true;
	  	
	    fm.butHandlingCus.disabled=true; 	
	}
	else
	{
		fm.all('HandlingName').disabled=false;
	  	fm.all('HandlingIDType').disabled=false;
	  	fm.all('HandlingIDTypeName').disabled=false;
	  	fm.all('HandlingIDNo').disabled=false;
	  	fm.all('HandlingPostalAddress').disabled=false;
	  	fm.all('HandlingZipCode').disabled=false;
	  	fm.all('HandlingMobile').disabled=false;
  		fm.all('HandlingMobile2').disabled=false;
	  	fm.all('HandlingPhone').disabled=false;
	  	fm.all('HandlingHomePhone').disabled=false;
	  	fm.all('HandlingMemberType').disabled=false;
	  	fm.all('HandlingMemberTypeName').disabled=false;
	  	
	  	fm.butHandlingCus.disabled=false; 	
	}
}

//查询投保人是否是已存在客户
function queryAppnt()
{
	//客户类型,姓名,证件类型,证件号码都不为空时才做处理
	if(fm.AppntCustType.value==null||fm.AppntCustType.value==''
	 ||fm.AppntName.value==null||fm.AppntName.value==''
	 ||fm.AppntIDType.value==null||fm.AppntIDType.value==''
	 ||fm.AppntIDNo.value==null||fm.AppntIDNo.value=='')
	{
		
	}
	else
	{
		//个人客户且证件类型为身份证
		if(fm.AppntCustType.value=="1"&&fm.AppntIDType.value=="01")
		{
			var tName = fm.AppntName.value;
			var tIDType = fm.AppntIDType.value;
			var tIDNo = fm.AppntIDNo.value;
			var tSex = calSexByIDNo(fm.AppntIDNo.value,"0");
			var tBirthday = calBirthByIDNo(fm.AppntIDNo.value);
		
			var sql="select CustomerNo from FDPerson where Name='"+tName+"' and Sex='"+tSex+"' and Birthday='"+tBirthday+"' and IDType='"+tIDType+"' and IDNo='"+tIDNo+"' ";
			
			var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			if(resultArr!=null)
			{
				fm.AppntNo.value = resultArr[0][0];
				
				queryAppntAddress(fm.AppntNo.value,1);
			}
		}
		else if(fm.AppntCustType.value=="2"&&fm.AppntIDType.value=="71")
		{
			var tName = fm.AppntName.value;
			var tIDNo = fm.AppntIDNo.value;
			
			var sql="select CustomerNo from FDGroup where GrpName='"+tName+"' and OrganComCode='"+tIDNo+"'";
			
			var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			if(resultArr!=null)
			{
				fm.AppntNo.value = resultArr[0][0];
				
				queryAppntAddress(fm.AppntNo.value,2);
			}
		}
	}
}

//查询被保人是否是已存在客户
function queryInsured()
{
	//客户类型,姓名,证件类型,证件号码都不为空时才做处理
	if(fm.InsuredCustType.value==null||fm.InsuredCustType.value==''
	 ||fm.InsuredName.value==null||fm.InsuredName.value==''
	 ||fm.InsuredIDType.value==null||fm.InsuredIDType.value==''
	 ||fm.InsuredIDNo.value==null||fm.InsuredIDNo.value=='')
	{
		
	}
	else
	{
		//个人客户且证件类型为身份证
		if(fm.InsuredCustType.value=="1"&&fm.InsuredIDType.value=="01")
		{
			var tName = fm.InsuredName.value;
			var tIDType = fm.InsuredIDType.value;
			var tIDNo = fm.InsuredIDNo.value;
			var tSex = calSexByIDNo(fm.InsuredIDNo.value,"0");
			var tBirthday = calBirthByIDNo(fm.InsuredIDNo.value);
		
			var sql="select CustomerNo from FDPerson where Name='"+tName+"' and Sex='"+tSex+"' and Birthday='"+tBirthday+"' and IDType='"+tIDType+"' and IDNo='"+tIDNo+"' ";
			
			var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			if(resultArr!=null)
			{
				fm.InsuredNo.value = resultArr[0][0];
				
				queryCustCar(fm.InsuredNo.value);
				queryInsuredAddress(fm.InsuredNo.value,1);
			}
		}
		else if(fm.InsuredCustType.value=="2"&&fm.InsuredIDType.value=="71")
		{
			var tName = fm.InsuredName.value;
			var tIDNo = fm.InsuredIDNo.value;
			
			var sql="select CustomerNo from FDGroup where GrpName='"+tName+"' and OrganComCode='"+tIDNo+"'";
			
			var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			if(resultArr!=null)
			{
				fm.InsuredNo.value = resultArr[0][0];
				
				queryCustCar(fm.InsuredNo.value);
				queryInsuredAddress(fm.InsuredNo.value,2);
			}
		}
	}
}

//查询经办人是否是已存在客户
function queryHandling()
{
	//客户类型,姓名,证件类型,证件号码都不为空时才做处理
	if(fm.HandlingName.value==null||fm.HandlingName.value==''
	 ||fm.HandlingIDType.value==null||fm.HandlingIDType.value==''
	 ||fm.HandlingIDNo.value==null||fm.HandlingIDNo.value=='')
	{
		
	}
	else
	{
		//个人客户且证件类型为身份证
		if(fm.HandlingIDType.value=="01")
		{
			var tName = fm.HandlingName.value;
			var tIDType = fm.HandlingIDType.value;
			var tIDNo = fm.HandlingIDNo.value;
			var tSex = calSexByIDNo(fm.HandlingIDNo.value,"0");
			var tBirthday = calBirthByIDNo(fm.HandlingIDNo.value);
		
			var sql="select CustomerNo from FDPerson where Name='"+tName+"' and Sex='"+tSex+"' and Birthday='"+tBirthday+"' and IDType='"+tIDType+"' and IDNo='"+tIDNo+"' ";
			
			var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			if(resultArr!=null)
			{
				fm.HandlingNo.value = resultArr[0][0];
				
				queryHandlingAddress(fm.HandlingNo.value,1);
			}
		}
	}
}

function queryCustCar(tCustomerNo)
{
	if(NullOrBlank(fm.ItemNo.value)&&NullOrBlank(fm.CarOwner.value)
	 &&NullOrBlank(fm.LicenseNo.value)&&NullOrBlank(fm.CarKindCode.value)
	 &&NullOrBlank(fm.UseNatureCode.value)&&NullOrBlank(fm.FrameNo.value)
	 &&NullOrBlank(fm.EngineNo.value)&&NullOrBlank(fm.ModelCode.value)
	 &&NullOrBlank(fm.YearExamDate.value)&&NullOrBlank(fm.EnrollDate.value)
	 &&NullOrBlank(fm.BuyDate.value))
	{
		var sql="select '',a.CarOwner,a.LicenseNo,a.CarKindCode,a.UseNatureCode,"
		+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,a.BuyDate,"
		+" a.VINNo,a.RunAreaName,a.HKFlag,a.HKLicenseNo,a.UseYears,a.RunMiles,"
		+" a.CountryNature,a.CountryCode,a.LicenseColorCode,a.SeatCount,a.TonCount,"
		+" a.ExhaustScale,a.ColorCode,a.SafeDevice,a.ParkSite,a.OwnerAddress,"
		+" a.ProduceDate,a.CarUsage,a.PurchasePrice,a.InvoiceNo,a.CarChecker,"
		+" a.CarCheckTime,a.CarCheckStatus,a.CarDealerName"
		+" from FDCustObjectCar a where a.CustomerNo='"+tCustomerNo+"'";
		
		var result=easyQueryVer3(sql,1,0);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			fm.CarOwner.value = resultArr[0][1];
			fm.LicenseNo.value = resultArr[0][2];
			fm.CarKindCode.value = resultArr[0][3];
			fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][3]);
			fm.UseNatureCode.value = resultArr[0][4];
			fm.UseNatureName.value = getNameFromLDCode("carusages",resultArr[0][4]);
			fm.FrameNo.value = resultArr[0][5];
			fm.EngineNo.value = resultArr[0][6];
			fm.ModelCode.value = resultArr[0][7];
			fm.YearExamDate.value = resultArr[0][8];
			fm.EnrollDate.value = resultArr[0][9];
			fm.BuyDate.value = resultArr[0][10];
			fm.VINNo.value = resultArr[0][11];
			fm.RunAreaName.value = resultArr[0][12];
			fm.HKFlag.value = resultArr[0][13];
			fm.HKFlagName.value = getNameFromLDCode("yesorno",resultArr[0][13]);
			fm.HKLicenseNo.value = resultArr[0][14];
			fm.UseYears.value = resultArr[0][15];
			fm.RunMiles.value = resultArr[0][16];
			fm.CountryNature.value = resultArr[0][17];
			fm.CountryNatureName.value = getNameFromLDCode("countrynature",resultArr[0][17]);
			fm.CountryCode1.value = resultArr[0][18];
			fm.CountryCode1Name.value = getNameFromLDCode("country",resultArr[0][18]);
			fm.LicenseColorCode.value = resultArr[0][19];
			fm.LicenseColorName.value = getNameFromLDCode("licensecolor",resultArr[0][19]);
			fm.SeatCount1.value = resultArr[0][20];
			fm.TonCount.value = resultArr[0][21];
			fm.ExhaustScale.value = resultArr[0][22];
			fm.ColorCode.value = resultArr[0][23];
			fm.ColorName.value = getNameFromLDCode("carcolor",resultArr[0][23]);
			fm.SafeDevice.value = resultArr[0][24];
			fm.ParkSite.value = resultArr[0][25];
			fm.OwnerAddress.value = resultArr[0][26];
			fm.ProduceDate.value = resultArr[0][27];
			fm.CarUsage.value = resultArr[0][28];
			fm.PurchasePrice.value = resultArr[0][29];
			fm.InvoiceNo.value = resultArr[0][30];
			fm.CarChecker.value = resultArr[0][31];
			fm.CarCheckTime.value = resultArr[0][32];
			fm.CarCheckStatus.value = resultArr[0][33];
			fm.CarCheckStatusName.value = getNameFromLDCode("carcheck",resultArr[0][33]);
			fm.CarDealerName.value = resultArr[0][34];
		}
	}
}

function removeDisabled()
{
	fm.all('AppntCustType').disabled=false;
  	fm.all('AppntCustTypeName').disabled=false;
  	fm.all('AppntName').disabled=false;
  	fm.all('AppntIDType').disabled=false;
  	fm.all('AppntIDTypeName').disabled=false;
  	fm.all('AppntIDNo').disabled=false;
  	fm.all('AppntPostalAddress').disabled=false;
  	fm.all('AppntZipCode').disabled=false;
  	fm.all('AppntMobile').disabled=false;
  	fm.all('AppntMobile2').disabled=false;
  	fm.all('AppntPhone').disabled=false;
  	fm.all('AppntHomePhone').disabled=false;
  	fm.all('AppntMemberType').disabled=false;
  	fm.all('AppntMemberTypeName').disabled=false;
  	
  	fm.all('HandlingName').disabled=false;
  	fm.all('HandlingIDType').disabled=false;
  	fm.all('HandlingIDTypeName').disabled=false;
  	fm.all('HandlingIDNo').disabled=false;
  	fm.all('HandlingPostalAddress').disabled=false;
  	fm.all('HandlingZipCode').disabled=false;
  	fm.all('HandlingMobile').disabled=false;
  	fm.all('HandlingMobile2').disabled=false;
  	fm.all('HandlingPhone').disabled=false;
  	fm.all('HandlingHomePhone').disabled=false;
  	fm.all('HandlingMemberType').disabled=false;
  	fm.all('HandlingMemberTypeName').disabled=false;
  	
  	
  	fm.butHandlingCus.disabled=false; 
  	fm.butAppntCus.disabled=false;
}

function disableFast()
{
	fm.butFull.disabled=true;
	fm.butCopy.disabled=true;
	fm.butDel.disabled=true;
	fm.butProj.disabled=true;
	fm.butChannel.disabled=true;
	fm.butAgent.disabled=true;
	fm.butQueryInsured.disabled=true;
	fm.butEnd.disabled=true;
	
	fm.AccreditFlag.disabled=true;
	fm.AccreditFlagName.disabled=true;
	
	fm.OutManageCom.disabled=true;
	fm.OutManageComName.disabled=true;
	fm.GetManageCom.disabled=true;
	fm.GetManageComName.disabled=true;
	fm.LicenseNo.disabled=true;
	fm.ComFirstFeeFlag.disabled=true;
	fm.ComFirstFeeFlagName.disabled=true;
	fm.BussNature.disabled=true;
	fm.BussNatureName.disabled=true;
	fm.RightSendFlag.disabled=true;
	fm.RightSendFlagName.disabled=true;
	fm.ChannelCode.disabled=true;
	fm.ChannelName.disabled=true;
	fm.SupplierShowCode.disabled=true;
	fm.SupplierName.disabled=true;
	fm.ContNo.disabled=true;
	fm.SignDate.disabled=true;
	fm.hour.disabled=true;
	fm.minute.disabled=true;
	fm.UWDate.disabled=true;
	fm.CValiDate.disabled=true;
	fm.CInValiDate.disabled=true;
	fm.InsuredCustType.disabled=true;
	fm.InsuredCustTypeName.disabled=true;
	fm.InsuredName.disabled=true;
	
	fm.AgentCode.disabled=true;
	fm.AgentName.disabled=true;
	fm.AgentGroup.disabled=true;
	fm.AgentGroupName.disabled=true;
	
	fm.OutRiskCode.disabled=true;
	fm.RiskName.disabled=true;
	fm.SumPrem.disabled=true;
	fm.tax1.disabled=true;
	fm.tax2.disabled=true;
	fm.FYCType.disabled=true;
	fm.FYCTypeName.disabled=true;
}

function removeFastDisabled()
{
	fm.butFull.disabled=false;
	fm.butCopy.disabled=false;
	fm.butProj.disabled=false;
	fm.butDel.disabled=false;
	fm.butChannel.disabled=false;
	fm.butAgent.disabled=false;
	fm.butQueryInsured.disabled=false;
	
	fm.PolicyPrtNo.disabled=false;
	fm.InvoicePrtNo.disabled=false;
	fm.AccreditFlag.disabled=false;
	fm.AccreditFlagName.disabled=false;
	
	fm.OutManageCom.disabled=false;
	fm.OutManageComName.disabled=false;
	fm.GetManageCom.disabled=false;
	fm.GetManageComName.disabled=false;
	fm.LicenseNo.disabled=false;
	fm.ComFirstFeeFlag.disabled=false;
	fm.ComFirstFeeFlagName.disabled=false;
	fm.BussNature.disabled=false;
	fm.BussNatureName.disabled=false;
	fm.RightSendFlag.disabled=false;
	fm.RightSendFlagName.disabled=false;
	fm.ChannelCode.disabled=false;
	fm.ChannelName.disabled=false;
	fm.SupplierShowCode.disabled=false;
	fm.SupplierName.disabled=false;
	fm.ContNo.disabled=false;
	fm.SignDate.disabled=false;
	fm.hour.disabled=false;
	fm.minute.disabled=false;
	fm.UWDate.disabled=false;
	fm.CValiDate.disabled=false;
	fm.CInValiDate.disabled=false;
	fm.InsuredCustType.disabled=false;
	fm.InsuredCustTypeName.disabled=false;
	fm.InsuredName.disabled=false;
	
	fm.AgentCode.disabled=false;
	fm.AgentName.disabled=false;
	fm.AgentGroup.disabled=false;
	fm.AgentGroupName.disabled=false;
		
	fm.OutRiskCode.disabled=false;
	fm.RiskName.disabled=false;
	fm.SumPrem.disabled=false;
	fm.tax1.disabled=false;
	fm.tax2.disabled=false;
	fm.FYCType.disabled=false;
	fm.FYCTypeName.disabled=false;
}

function showDetail()
{
	if(tInputType=="syscont")
    {
    	div1.style.display="none";
    	div2.style.display="";
    	div3.style.display="";
    	div4.style.display="";
    	div5.style.display="none";
    	div6.style.display="none";
    	div7.style.display="none";
    	div8.style.display="";
    	div9.style.display="none";
    	div10.style.display="none";
    	div12.style.display="none";
    	div13.style.display="none";
    	div14.style.display="";
    	FullRemark.style.display="";
    	
    	if(!NullOrBlank(mInnerContNo))
    	{
    		afterQuery(mInnerContNo);
    	}
    }
    else if(tInputType=="sysrisk")
    {
    	div1.style.display="none";
    	div2.style.display="none";
    	div3.style.display="none";
    	div4.style.display="none";
    	div5.style.display="none";
    	div6.style.display="none";
    	div7.style.display="none";
    	div8.style.display="none";
    	div9.style.display="";
    	div10.style.display="";
    	div12.style.display="none";
    	div13.style.display="none";
    	div14.style.display="";
    	
    	if(!NullOrBlank(mInnerContNo))
    	{
    		afterQuery(mInnerContNo);
    	}
    }
    else if(tInputType=="detail")
    {
    	div1.style.display="none";
    	div2.style.display="";
    	div3.style.display="";
    	div4.style.display="none";
    	div5.style.display="none";
    	div6.style.display="none";
    	div7.style.display="none";
    	div8.style.display="";
    	div9.style.display="";
    	div10.style.display="";
    	div12.style.display="none";
    	div13.style.display="none";
    	div14.style.display="";
    	
    	if(!NullOrBlank(mInnerContNo))
    	{
    		afterQuery(mInnerContNo);
    	}
    }
    else if(tInputType=="claim")//理赔查看保单信息，fanxj add
    {
    	div1.style.display="none";
    	div2.style.display="";
    	div3.style.display="";
    	div4.style.display="none";
    	div5.style.display="none";
    	div6.style.display="none";
    	div7.style.display="none";
    	div8.style.display="";
    	div9.style.display="";
    	div10.style.display="";
    	div12.style.display="none";
    	div13.style.display="none";
    	div14.style.display="";

    	if(!NullOrBlank(mInnerContNo))
    	{
    		afterQuery(mInnerContNo);
    	}
    }
    
    
}
//个人客户
function afterQueryPerCust(tCustomerNo,tType)
{ 
	var PerCustSQL ="";
	PerCustSQL = "select a.Name,"
        	   	+" a.IDType,"
        	   	+" (select CodeName from fdcode where codetype = 'idtype' and code = a.IDType),"
        		+" a.IDNo,"
        		+" a.Mobile,"
        		+" a.SmallMobile,"
        		+" a.OfficeTel,"
        		+" a.HomeTel,"
        		+" b.PostalAddress,"
        		+" b.ZipCode,"
        		+" a.MemberType"
   				+" from FDPerson a"
   				+" left join FDPersonAddress b on(a.CustomerNo=b.CustomerNo)"
  				+" where a.CustomerNo = '"+tCustomerNo+"' "
  				+" order by b.MakeDate desc,b.MakeTime desc"
  	var PerCustResult=easyQueryVer3(PerCustSQL,1,0,1);
	if(!PerCustResult){
	}
	else
	{
		var PerCustSelected = decodeEasyQueryResult(PerCustResult);
		//被保人信息
		if("InsuredCust"==tType)
		{
			fm.InsuredCustType.value ="1";
			fm.InsuredCustTypeName.value ="个人";
			fm.InsuredNo.value=tCustomerNo;
			fm.InsuredName.value=PerCustSelected[0][0];
			fm.InsuredIDType.value=PerCustSelected[0][1];
			fm.InsuredIDTypeName.value=PerCustSelected[0][2];
			fm.InsuredIDNo.value=PerCustSelected[0][3];
			fm.InsuredMobile.value=PerCustSelected[0][4];
			fm.InsuredMobile2.value=PerCustSelected[0][5];
			fm.InsuredPhone.value=PerCustSelected[0][6];
			fm.InsuredHomePhone.value=PerCustSelected[0][7];
			fm.InsuredPostalAddress.value=PerCustSelected[0][8];
			fm.InsuredZipCode.value=PerCustSelected[0][9];
			fm.InsuredMemberType.value=PerCustSelected[0][10];
			fm.InsuredMemberTypeName.value=getNameFromLDCode("membertype",PerCustSelected[0][10]);
			
		//被保人带出车辆信息	
			showCustObjectCarInfo(tCustomerNo);
		}
		//投保人信息
		if("AppntCust"==tType)
		{
			fm.AppntCustType.value ="1";
			fm.AppntCustTypeName.value ="个人";
			fm.AppntNo.value=tCustomerNo;
			fm.AppntName.value=PerCustSelected[0][0];
			fm.AppntIDType.value=PerCustSelected[0][1];
			fm.AppntIDTypeName.value=PerCustSelected[0][2];
			fm.AppntIDNo.value=PerCustSelected[0][3];
			fm.AppntMobile.value=PerCustSelected[0][4];
			fm.AppntMobile2.value=PerCustSelected[0][5];
			fm.AppntPhone.value=PerCustSelected[0][6];
			fm.AppntHomePhone.value=PerCustSelected[0][7];
			fm.AppntPostalAddress.value=PerCustSelected[0][8];
			fm.AppntZipCode.value=PerCustSelected[0][9];
			fm.AppntMemberType.value=PerCustSelected[0][10];
			fm.AppntMemberTypeName.value=getNameFromLDCode("membertype",PerCustSelected[0][10]);
		}
		//经办人信息
		if("HandlingCust"==tType)
		{
			fm.HandlingNo.value=tCustomerNo;
			fm.HandlingName.value = PerCustSelected[0][0];
			fm.HandlingIDType.value = PerCustSelected[0][1];
			fm.HandlingIDTypeName.value = PerCustSelected[0][2];
			fm.HandlingIDNo.value =  PerCustSelected[0][3];
			fm.HandlingMobile.value=PerCustSelected[0][4];
			fm.HandlingMobile2.value=PerCustSelected[0][5];
			fm.HandlingPhone.value=PerCustSelected[0][6];
			fm.HandlingHomePhone.value=PerCustSelected[0][7];
			fm.HandlingPostalAddress.value=PerCustSelected[0][8];
			fm.HandlingZipCode.value=PerCustSelected[0][9];
			fm.HandlingMemberType.value=PerCustSelected[0][10];
			fm.HandlingMemberTypeName.value=getNameFromLDCode("membertype",PerCustSelected[0][10]);
		}
	}			
  				
}
//单位客户
function afterQueryGrpCust(tCustomerNo,tType)
{
	var GrpCustSQL =" select a.GrpName,"
		        +" a.OrganComCode,"
		        +" a.BussLicence,"
		        +" a.Otheridno,"
		        +" a.TelOffice,"
		        +" b.GrpAddress,"
		        +" b.GrpZipCode,"
		        +" a.MemberType"
			    +" from FDGroup a"
			    +" left join FDGrpAddress b on(a.CustomerNo=b.CustomerNo)"
			    +" where a.CustomerNo ='"+tCustomerNo+"' "
			    +" order by b.MakeDate desc,b.MakeTime desc";
	  	var PerCustResult=easyQueryVer3(GrpCustSQL,1,0,1);
	if(!PerCustResult){
	}
	else
	{
		var PerCustSelected = decodeEasyQueryResult(PerCustResult);
		if("InsuredCust"==tType)
		{
			fm.InsuredCustType.value ="2";
			fm.InsuredCustTypeName.value ="单位";
			fm.InsuredNo.value=tCustomerNo;
			fm.InsuredName.value=PerCustSelected[0][0];
			if(PerCustSelected[0][1]!=null&&PerCustSelected[0][1]!="")
			{
				fm.InsuredIDType.value="71";
				fm.InsuredIDTypeName.value=getNameFromLDCode("idtype","71");
				fm.InsuredIDNo.value=PerCustSelected[0][1];
			}
			else if(PerCustSelected[0][2]!=null&&PerCustSelected[0][2]!="")
			{
			 	fm.InsuredIDType.value="73";
				fm.InsuredIDTypeName.value=getNameFromLDCode("idtype","73");
				fm.InsuredIDNo.value=PerCustSelected[0][2];
			}
			else if(PerCustSelected[0][3]!=null&&PerCustSelected[0][3]!="")
			{
				fm.InsuredIDType.value="99";
				fm.InsuredIDTypeName.value=getNameFromLDCode("idtype","99");
				fm.InsuredIDNo.value=PerCustSelected[0][3];
			}
			else 
			{
				fm.InsuredIDType.value="";
				fm.InsuredIDTypeName.value="";
				fm.InsuredIDNo.value="";
			}
			
			fm.InsuredMobile.value=PerCustSelected[0][4];
			fm.InsuredPostalAddress.value=PerCustSelected[0][5];
			fm.InsuredZipCode.value=PerCustSelected[0][6];
			fm.InsuredMemberType.value=PerCustSelected[0][7];
			fm.InsuredMemberTypeName.value=getNameFromLDCode("membertype",PerCustSelected[0][7]);
			
	 //被保人带出车辆信息		
		showCustObjectCarInfo(tCustomerNo);	
		}
		
	  if("AppntCust"==tType)
	  	{
		  	fm.AppntCustType.value ="2";
			fm.AppntCustTypeName.value ="单位";
			fm.AppntNo.value=tCustomerNo;
			fm.AppntName.value=PerCustSelected[0][0];
			if(PerCustSelected[0][1]!=null&&PerCustSelected[0][1]!="")
			{
				fm.AppntIDType.value="71";
				fm.AppntIDTypeName.value=getNameFromLDCode("idtype","71");
				fm.AppntIDNo.value=PerCustSelected[0][1];
			}
			else if(PerCustSelected[0][2]!=null&&PerCustSelected[0][2]!="")
			{
			 	fm.AppntIDType.value="73";
				fm.AppntIDTypeName.value=getNameFromLDCode("idtype","73");
				fm.AppntIDNo.value=PerCustSelected[0][2];
			}
			else if(PerCustSelected[0][3]!=null&&PerCustSelected[0][3]!="")
			{
				fm.AppntIDType.value="99";
				fm.AppntIDTypeName.value=getNameFromLDCode("idtype","99");
				fm.AppntIDNo.value=PerCustSelected[0][3];
			}
			else 
			{
				fm.AppntIDType.value="";
				fm.AppntIDTypeName.value="";
				fm.AppntIDNo.value="";
			}
			fm.AppntMobile.value=PerCustSelected[0][4];
			fm.AppntPostalAddress.value=PerCustSelected[0][5];
			fm.AppntZipCode.value=PerCustSelected[0][6];
			fm.AppntMemberType.value=PerCustSelected[0][7];
			fm.AppntMemberTypeName.value=getNameFromLDCode("membertype",PerCustSelected[0][7]);
		}
	}					    
	
}
//客户车辆信息
function showCustObjectCarInfo(tCustomerNo)
{
  var sql="select a.ItemNo,a.CarOwner,a.LicenseNo,a.CarKindCode,a.UseNatureCode,"
	+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,a.BuyDate,"
	+" a.VINNo,a.RunAreaName,a.HKFlag,a.HKLicenseNo,a.UseYears,a.RunMiles,"
	+" a.CountryNature,a.CountryCode,a.LicenseColorCode,a.SeatCount,a.TonCount,"
	+" a.ExhaustScale,a.ColorCode,a.SafeDevice,a.ParkSite,a.OwnerAddress,"
	+" a.ProduceDate,a.CarUsage,a.PurchasePrice,a.InvoiceNo,a.CarChecker,"
	+" a.CarCheckTime,a.CarCheckStatus,a.CarDealerName"
	+" from FDCustObjectCar a where a.CustomerNo='"+tCustomerNo+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.CarOwner.value = resultArr[0][1];
		fm.LicenseNo.value = resultArr[0][2];
		fm.CarKindCode.value = resultArr[0][3];
		fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][3]);
		fm.UseNatureCode.value = resultArr[0][4];
		fm.UseNatureName.value = getNameFromLDCode("carusages",resultArr[0][4]);
		fm.FrameNo.value = resultArr[0][5];
		fm.EngineNo.value = resultArr[0][6];
		fm.ModelCode.value = resultArr[0][7];
		fm.YearExamDate.value = resultArr[0][8];
		fm.EnrollDate.value = resultArr[0][9];
		fm.BuyDate.value = resultArr[0][10];
		fm.VINNo.value = resultArr[0][11];
		fm.RunAreaName.value = resultArr[0][12];
		fm.HKFlag.value = resultArr[0][13];
		fm.HKFlagName.value = getNameFromLDCode("yesorno",resultArr[0][13]);
		fm.HKLicenseNo.value = resultArr[0][14];
		fm.UseYears.value = resultArr[0][15];
		fm.RunMiles.value = resultArr[0][16];
		fm.CountryNature.value = resultArr[0][17];
		fm.CountryNatureName.value = getNameFromLDCode("countrynature",resultArr[0][17]);
		fm.CountryCode1.value = resultArr[0][18];
		fm.CountryCode1Name.value = getNameFromLDCode("country",resultArr[0][18]);
		fm.LicenseColorCode.value = resultArr[0][19];
		fm.LicenseColorName.value = getNameFromLDCode("licensecolor",resultArr[0][19]);
		fm.SeatCount1.value = resultArr[0][20];
		fm.TonCount.value = resultArr[0][21];
		fm.ExhaustScale.value = resultArr[0][22];
		fm.ColorCode.value = resultArr[0][23];
		fm.ColorName.value = getNameFromLDCode("carcolor",resultArr[0][23]);
		fm.SafeDevice.value = resultArr[0][24];
		fm.ParkSite.value = resultArr[0][25];
		fm.OwnerAddress.value = resultArr[0][26];
		fm.ProduceDate.value = resultArr[0][27];
		fm.CarUsage.value = resultArr[0][28];
		fm.PurchasePrice.value = resultArr[0][29];
		fm.InvoiceNo.value = resultArr[0][30];
		fm.CarChecker.value = resultArr[0][31];
		fm.CarCheckTime.value = resultArr[0][32];
		fm.CarCheckStatus.value = resultArr[0][33];
		fm.CarCheckStatusName.value = getNameFromLDCode("carcheck",resultArr[0][33]);
		fm.CarDealerName.value = resultArr[0][34];
		
		fm.InsuredNo.value = tCustomerNo;
	}
}
//车辆信息查询
function queryCarInfo()
{
	var tInsuredNo=fm.InsuredNo.value;
	openWindow("./FCCarInfoQueryMain.jsp?tCustomerNo="+tInsuredNo);
}

function afterQueryCarInfo(tCustomerNo,tItemno)
{
	var sql="select a.ItemNo,a.CarOwner,a.LicenseNo,a.CarKindCode,a.UseNatureCode,"
	+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,a.BuyDate,"
	+" a.VINNo,a.RunAreaName,a.HKFlag,a.HKLicenseNo,a.UseYears,a.RunMiles,"
	+" a.CountryNature,a.CountryCode,a.LicenseColorCode,a.SeatCount,a.TonCount,"
	+" a.ExhaustScale,a.ColorCode,a.SafeDevice,a.ParkSite,a.OwnerAddress,"
	+" a.ProduceDate,a.CarUsage,a.PurchasePrice,a.InvoiceNo,a.CarChecker,"
	+" a.CarCheckTime,a.CarCheckStatus,a.CarDealerName"
	+" from FDCustObjectCar a where a.CustomerNo='"+tCustomerNo+"' and a.itemno='"+tItemno+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.CarOwner.value = resultArr[0][1];
		fm.LicenseNo.value = resultArr[0][2];
		fm.CarKindCode.value = resultArr[0][3];
		fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][3]);
		fm.UseNatureCode.value = resultArr[0][4];
		fm.UseNatureName.value = getNameFromLDCode("carusages",resultArr[0][4]);
		fm.FrameNo.value = resultArr[0][5];
		fm.EngineNo.value = resultArr[0][6];
		fm.ModelCode.value = resultArr[0][7];
		fm.YearExamDate.value = resultArr[0][8];
		fm.EnrollDate.value = resultArr[0][9];
		fm.BuyDate.value = resultArr[0][10];
		fm.VINNo.value = resultArr[0][11];
		fm.RunAreaName.value = resultArr[0][12];
		fm.HKFlag.value = resultArr[0][13];
		fm.HKFlagName.value = getNameFromLDCode("yesorno",resultArr[0][13]);
		fm.HKLicenseNo.value = resultArr[0][14];
		fm.UseYears.value = resultArr[0][15];
		fm.RunMiles.value = resultArr[0][16];
		fm.CountryNature.value = resultArr[0][17];
		fm.CountryNatureName.value = getNameFromLDCode("countrynature",resultArr[0][17]);
		fm.CountryCode1.value = resultArr[0][18];
		fm.CountryCode1Name.value = getNameFromLDCode("country",resultArr[0][18]);
		fm.LicenseColorCode.value = resultArr[0][19];
		fm.LicenseColorName.value = getNameFromLDCode("licensecolor",resultArr[0][19]);
		fm.SeatCount1.value = resultArr[0][20];
		fm.TonCount.value = resultArr[0][21];
		fm.ExhaustScale.value = resultArr[0][22];
		fm.ColorCode.value = resultArr[0][23];
		fm.ColorName.value = getNameFromLDCode("carcolor",resultArr[0][23]);
		fm.SafeDevice.value = resultArr[0][24];
		fm.ParkSite.value = resultArr[0][25];
		fm.OwnerAddress.value = resultArr[0][26];
		fm.ProduceDate.value = resultArr[0][27];
		fm.CarUsage.value = resultArr[0][28];
		fm.PurchasePrice.value = resultArr[0][29];
		fm.InvoiceNo.value = resultArr[0][30];
		fm.CarChecker.value = resultArr[0][31];
		fm.CarCheckTime.value = resultArr[0][32];
		fm.CarCheckStatus.value = resultArr[0][33];
		fm.CarCheckStatusName.value = getNameFromLDCode("carcheck",resultArr[0][33]);
		fm.CarDealerName.value = resultArr[0][34];
	}
}

var oldInnerContNo="";
function getCont()
{
	if(NullOrBlank(fm.InnerContNo.value))
	{
		if(getContByLicenseNo(fm.LicenseNo.value))
		{
			getCarByInnerContNo(oldInnerContNo);
			//queryOldRisk(oldInnerContNo);
			afterCodeSelect('ManageCom2','');
			oldInnerContNo = "";
		}
		//没有保单则查询车辆信息和客户信息
		else
		{
			var temp = getCarByLicenseNo(fm.LicenseNo.value);
			if(temp==1)
			{alert(fm.InsuredNo.value);
				fm.InsuredName.value=getNameByCode("Name","FDPerson","CustomerNo",fm.InsuredNo.value);
				alert(fm.InsuredName.value)
				if(NullOrBlank(fm.InsuredName.value))
				{
					fm.InsuredCustType.value = "2";
					fm.InsuredCustTypeName.value = getNameFromLDCode("custtype",fm.InsuredCustType.value);
					fm.InsuredName.value=getNameByCode("GrpName","FDGroup","CustomerNo",fm.InsuredNo.value);
				}
				else
				{
					fm.InsuredCustType.value = "1";
					fm.InsuredCustTypeName.value = getNameFromLDCode("custtype",fm.InsuredCustType.value);
				}
			}
			else if(temp>1)
			{
				openWindow("../customer/FDCustomerQueryMain.jsp?tType=InsuredCust&LicenseNo="+fm.LicenseNo.value);
			}
		}
	}
}

function getContByLicenseNo(tLicenseNo)
{
	var sql="select a.PolicyPrtNo,a.InvoicePrtNo,a.InsCardPrtNo,a.CInsFlagPrtNo,a.UWDate,a.BussNature,a.OutManageCom,a.GetManageCom,"
	+" a.SupplierCode,a.ContNo,a.SignDate,a.SignTime,a.CValiDate,a.CInValiDate,a.POSNo,a.PayPCommFlag,"
	+" a.PayPComm,a.InsuredCustType,a.InsuredName,a.InsuredIDType,a.InsuredIDNo,a.InsuredPostalAddress,a.InsuredZipCode,"
	+" a.InsuredMobile,a.AppntCustType,a.AppntName,a.AppntIDType,a.AppntIDNo,a.AppntPostalAddress,a.AppntZipCode,a.AppntMobile,"
	+" a.AgentCode,a.AgentGroup,a.PolicyPli,a.InnerContNo,"
	+" a.ProtocolNo,a.Corporation,a.AgentFlag,"
	+" a.AppntNo,a.InsuredNo,a.ComFirstFeeFlag,a.HandlingNo,a.HandlingAddressNo,a.HandlingName,a.HandlingIDType,a.HandlingIDNo,"
	+" a.HandlingPostalAddress,a.HandlingZipCode,a.HandlingMobile,a.OldSupplierCode,a.OldContNo,a.ProtocolNo,"
	+" a.RightSendFlag,a.SignMan,a.AppntMobile2,a.AppntPhone,a.AppntHomePhone,"
	+" a.InsuredMobile2,a.InsuredPhone,a.InsuredHomePhone,a.HandlingMobile2,a.HandlingPhone,a.HandlingHomePhone,a.ChannelCode,a.RightSendFlag,a.AccreditFlag"
	+" from FCCont a where a.LicenseNo='"+tLicenseNo+"' order by a.MakeDate desc,a.MakeTime desc";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	//是否有保单
	if(resultArr!=null)
	{
		//fm.PolicyPrtNo.value=resultArr[0][0];
		//fm.InvoicePrtNo.value=resultArr[0][1];
		//fm.InsCardPrtNo.value=resultArr[0][2];
		//fm.CInsFlagPrtNo.value=resultArr[0][3];
		fm.UWDate.value=resultArr[0][4];
		fm.BussNature.value=resultArr[0][5];
		fm.BussNatureName.value=getNameFromLDCode("BussNature2",resultArr[0][5]);
		//fm.OutManageCom.value=resultArr[0][6]; 
		//fm.OutManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][6]);
		//fm.GetManageCom.value=resultArr[0][7];
		//fm.GetManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][7]);
		fm.SupplierCode.value=resultArr[0][8];
		fm.SupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",resultArr[0][8]);
		fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",resultArr[0][8]);
		//fm.ContNo.value=resultArr[0][9];
		fm.SignDate.value=resultArr[0][10];
		fm.SignTime.value=resultArr[0][11];
		fm.hour.value = (fm.SignTime.value).substring(0,2);
		fm.minute.value = (fm.SignTime.value).substring(3,5);;
		fm.CValiDate.value=resultArr[0][12];
		showCInValiDate2(resultArr[0][13]);
		fm.POSNo.value=resultArr[0][14];
		fm.PayPCommFlag.value=resultArr[0][15];
		fm.PayPCommFlagName.value=getNameFromLDCode("yesorno",resultArr[0][15]);
		fm.PayPComm.value=resultArr[0][16];
		fm.InsuredCustType.value=resultArr[0][17];
		fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",resultArr[0][17]);
		fm.InsuredName.value=resultArr[0][18];
		fm.InsuredIDType.value=resultArr[0][19];
		fm.InsuredIDTypeName.value=getNameFromLDCode("IDType",resultArr[0][19]);
		fm.InsuredIDNo.value=resultArr[0][20];
		fm.InsuredPostalAddress.value=resultArr[0][21];
		fm.InsuredZipCode.value=resultArr[0][22];
		fm.InsuredMobile.value=resultArr[0][23];
		fm.AppntCustType.value=resultArr[0][24];
		fm.AppntCustTypeName.value=getNameFromLDCode("CustType",resultArr[0][24]);
		fm.AppntName.value=resultArr[0][25];
		fm.AppntIDType.value=resultArr[0][26];
		fm.AppntIDTypeName.value=getNameFromLDCode("IDType",resultArr[0][26]);
		fm.AppntIDNo.value=resultArr[0][27];
		fm.AppntPostalAddress.value=resultArr[0][28];
		fm.AppntZipCode.value=resultArr[0][29];
		fm.AppntMobile.value=resultArr[0][30];
		fm.PolicyPli.value=unescapes(resultArr[0][33]);
		//fm.InnerContNo.value=resultArr[0][34];
		//fm.ProtocolNo.value=resultArr[0][35];
		//mProtocolNo = fm.ProtocolNo.value;
		fm.Corporation.value=resultArr[0][36];
		fm.AgentFlag.value=resultArr[0][37];
		fm.AppntNo.value=resultArr[0][38];
		fm.InsuredNo.value=resultArr[0][39];
		fm.ComFirstFeeFlag.value=resultArr[0][40];
		fm.ComFirstFeeFlagName.value=getNameFromLDCode("yesorno",resultArr[0][40]);

		fm.HandlingNo.value=resultArr[0][41];
		fm.HandlingAddressNo.value=resultArr[0][42];
		fm.HandlingName.value=resultArr[0][43];
		fm.HandlingIDType.value=resultArr[0][44];
		fm.HandlingIDTypeName.value=getNameFromLDCode("idtype",resultArr[0][44]);
		fm.HandlingIDNo.value=resultArr[0][45];
		fm.HandlingPostalAddress.value=resultArr[0][46];
		fm.HandlingZipCode.value=resultArr[0][47];
		fm.HandlingMobile.value=resultArr[0][48];
		fm.OldSupplierCode.value=resultArr[0][49];
		fm.OldSupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",resultArr[0][49]);
		fm.OldSupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",resultArr[0][49]);
		fm.OldContNo.value=resultArr[0][50];
		//fm.PKProtocolNo.value=resultArr[0][51];
		fm.SignMan.value=resultArr[0][53];
		fm.SignManName.value=getNameByCode("UserName","FDUser","UserCode",resultArr[0][53]);
		fm.AppntMobile2.value=resultArr[0][54];
		fm.AppntPhone.value=resultArr[0][55];
		fm.AppntHomePhone.value=resultArr[0][56];
		fm.InsuredMobile2.value=resultArr[0][57];
		fm.InsuredPhone.value=resultArr[0][58];
		fm.InsuredHomePhone.value=resultArr[0][59];
		fm.HandlingMobile2.value=resultArr[0][60];
		fm.HandlingPhone.value=resultArr[0][61];
		fm.HandlingHomePhone.value=resultArr[0][62];
		
		if(fm.BussNature.value=="02")
		{
			channel.style.display = "";
			fm.ChannelCode.value=resultArr[0][63];
			fm.ChannelName.value=getNameByCode("RepairShopName","FDRepairShop","RepairShopCode",resultArr[0][63]);
		}
		else
		{
			channel.style.display = "none";
			fm.ChannelCode.value="";
			fm.ChannelName.value="";
		}
		fm.RightSendFlag.value=resultArr[0][64];
		fm.RightSendFlagName.value=getNameFromLDCode("yesorno",resultArr[0][64]);
		fm.AccreditFlag.value=resultArr[0][65];
		fm.AccreditFlagName.value=getNameFromLDCode("yesorno",resultArr[0][65]);
		
		if(fm.AppntNo.value==fm.InsuredNo.value)
			{
				fm.appnt.checked=true;
			}
			else
			{
				fm.appnt.checked=false;
			}
				
			if(fm.HandlingNo.value==fm.InsuredNo.value)
			{
				fm.handling.checked=true;
			}
				
			else{
				fm.handling.checked=false;	
			}
			showDisabled();
		
		fm.AgentCode.value=resultArr[0][31];
		fm.AgentName.value=getNameByCode("Name","FAAgentTree","AgentCode",resultArr[0][31]);
		fm.AgentGroup.value=resultArr[0][32];
		//mAgentGroup = fm.AgentGroup.value;
		fm.AgentGroupName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][32]);
		
		oldInnerContNo=resultArr[0][34];
		
		var sql = " select a.Score from FAScore a "
		+ " where a.InnerContNo='"+oldInnerContNo+"' and a.AgentCode='"+fm.AgentCode.value+"'";
		
		var result=easyQueryVer3(sql,1,0,1);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result,0,0);
		if(resultArr!=null)
		{
			fm.AgentScore.value = resultArr[0][0];
		}
		
		return true;
	}
	
	return false;
}

function getCarByLicenseNo(tLicenseNo)
{
	var sql="select a.ItemNo,a.CarOwner,a.LicenseNo,a.CarKindCode,a.UseNatureCode,"
	+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,a.BuyDate,"
	+" a.VINNo,a.RunAreaName,a.HKFlag,a.HKLicenseNo,a.UseYears,a.RunMiles,"
	+" a.CountryNature,a.CountryCode,a.LicenseColorCode,a.SeatCount,a.TonCount,"
	+" a.ExhaustScale,a.ColorCode,a.SafeDevice,a.ParkSite,a.OwnerAddress,"
	+" a.ProduceDate,a.CarUsage,a.PurchasePrice,a.InvoiceNo,a.CarChecker,"
	+" a.CarCheckTime,a.CarCheckStatus,a.CarDealerName,a.CustomerNo "
	+" from FDCustObjectCar a where a.LicenseNo='"+tLicenseNo+"' order by a.MakeDate desc,a.MakeTime desc";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.CarOwner.value = resultArr[0][1];
		fm.LicenseNo.value = resultArr[0][2];
		fm.CarKindCode.value = resultArr[0][3];
		fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][3]);
		fm.UseNatureCode.value = resultArr[0][4];
		fm.UseNatureName.value = getNameFromLDCode("carusages",resultArr[0][4]);
		fm.FrameNo.value = resultArr[0][5];
		fm.EngineNo.value = resultArr[0][6];
		fm.ModelCode.value = resultArr[0][7];
		fm.YearExamDate.value = resultArr[0][8];
		fm.EnrollDate.value = resultArr[0][9];
		fm.BuyDate.value = resultArr[0][10];
		fm.VINNo.value = resultArr[0][11];
		fm.RunAreaName.value = resultArr[0][12];
		fm.HKFlag.value = resultArr[0][13];
		fm.HKFlagName.value = getNameFromLDCode("yesorno",resultArr[0][13]);
		fm.HKLicenseNo.value = resultArr[0][14];
		fm.UseYears.value = resultArr[0][15];
		fm.RunMiles.value = resultArr[0][16];
		fm.CountryNature.value = resultArr[0][17];
		fm.CountryNatureName.value = getNameFromLDCode("countrynature",resultArr[0][17]);
		fm.CountryCode1.value = resultArr[0][18];
		fm.CountryCode1Name.value = getNameFromLDCode("country",resultArr[0][18]);
		fm.LicenseColorCode.value = resultArr[0][19];
		fm.LicenseColorName.value = getNameFromLDCode("licensecolor",resultArr[0][19]);
		fm.SeatCount1.value = resultArr[0][20];
		fm.TonCount.value = resultArr[0][21];
		fm.ExhaustScale.value = resultArr[0][22];
		fm.ColorCode.value = resultArr[0][23];
		fm.ColorName.value = getNameFromLDCode("carcolor",resultArr[0][23]);
		fm.SafeDevice.value = resultArr[0][24];
		fm.ParkSite.value = resultArr[0][25];
		fm.OwnerAddress.value = resultArr[0][26];
		fm.ProduceDate.value = resultArr[0][27];
		fm.CarUsage.value = resultArr[0][28];
		fm.PurchasePrice.value = resultArr[0][29];
		fm.InvoiceNo.value = resultArr[0][30];
		fm.CarChecker.value = resultArr[0][31];
		fm.CarCheckTime.value = resultArr[0][32];
		fm.CarCheckStatus.value = resultArr[0][33];
		fm.CarCheckStatusName.value = getNameFromLDCode("carcheck",resultArr[0][33]);
		fm.CarDealerName.value = resultArr[0][34];
		fm.InsuredNo.value = resultArr[0][35];
		
		return resultArr.length;
	}else
	{
		alert("系统中查询不到车牌号相同的车辆信息");
	}
	
	return 0;
}

function showSignManName()
{
	if(NullOrBlank(fm.SignMan.value))
	{
		return;
	}
	
	var sql = "select a.UserName from FDUser a"
	+" where a.UserCode='"+fm.SignMan.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=decodeEasyQueryResult(result);
	if(!result)
	{
		alert("出单员编码错误,不存在对应的出单员!");
		fm.SignMan.value = "";
		fm.SignManName.value = "";		
		return;
	}
	else
	{
		fm.SignManName.value = resultArr[0][0];
	}
}

function showReceiveOperatorName()
{
	if(NullOrBlank(fm.ReceiveOperator.value))
	{
		fm.ReceiveOperator.value = "";
		return;
	}
	var sql = "select a.UserName from FDUser a"
	+" where a.UserCode='"+fm.ReceiveOperator.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=decodeEasyQueryResult(result);
	if(!result)
	{
		alert("录单员编码错误,不存在对应的录单员!");
		fm.ReceiveOperator.value = "";
		fm.ReceiveOperatorName.value = "";		
		return;
	}
	else
	{
		fm.ReceiveOperatorName.value = resultArr[0][0];
	}
}

function disableCard()
{
	var tPolicyPrtNo = fm.PolicyPrtNo.value;
	var tInvoicePrtNo = fm.InvoicePrtNo.value;
	var tInsCardPrtNo = fm.InsCardPrtNo.value;
	var tCInsFlagPrtNo = fm.CInsFlagPrtNo.value;
	var tSupplierCode = fm.SupplierCode.value;
	
	var tMasterInscom = tSupplierCode.substring(0,4);
	if(!NullOrBlank(tPolicyPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'J'"
	        +" and length(StartNo) = '"+tPolicyPrtNo.length+"' and StartNo <= '"+tPolicyPrtNo+"' and EndNo >= '"+tPolicyPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				fm.PolicyPrtNo.disabled=true;
			}
		}
	}
	
	if(!NullOrBlank(tInvoicePrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'I'"
	        +" and length(StartNo) = '"+tInvoicePrtNo.length+"' and StartNo <= '"+tInvoicePrtNo+"' and EndNo >= '"+tInvoicePrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				fm.InvoicePrtNo.disabled=true;
			}
		}
	}
	if(!NullOrBlank(tInsCardPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'H'"
	        +" and length(StartNo) = '"+tInsCardPrtNo.length+"' and StartNo <= '"+tInsCardPrtNo+"' and EndNo >= '"+tInsCardPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				fm.InsCardPrtNo.disabled=true;
			}
		}
	}
	if(!NullOrBlank(tCInsFlagPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'G'"
	        +" and length(StartNo) = '"+tCInsFlagPrtNo.length+"' and StartNo <= '"+tCInsFlagPrtNo+"' and EndNo >= '"+tCInsFlagPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				fm.CInsFlagPrtNo.disabled=true;
			}
		}
		
	}
}

function showChannelName()
{
	if(NullOrBlank(fm.ChannelCode.value))
	{
		fm.ChannelName.value = "";
		return;
	}
	
	var sql = "select a.RepairShopName from FDRepairShop a"
	+" where a.RepairShopCode='"+fm.ChannelCode.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=decodeEasyQueryResult(result);
	if(!result)
	{
		alert("渠道编码错误,不存在对应的渠道!");
		fm.ChannelCode.value = "";
		fm.ChannelName.value = "";		
		return;
	}
	else
	{
		fm.ChannelName.value = resultArr[0][0];
	}
	
	var sqlPart = "";
	if(tComType=="00"||tComType=="01"||tComType=="02")
	{
		sqlPart = getManageComLimitlike3("ComCode",""," and ComType = '03' ");
	}
	else
	{
		sqlPart = " and ComCode='"+Corporate+"'";
	}
	
	var sql="select RepairShopCode from FDRepairShopToCom"
	+" where RepairShopCode='"+fm.ChannelCode.value+"'"
	+ sqlPart;
	var result=easyQueryVer3(sql,1,0);
	if(!result)
	{
		alert("渠道编码错误,您登录的机构对与渠道【"+fm.ChannelName.value+"】没有关联关系！");
		fm.ChannelCode.value = "";
		fm.ChannelName.value = "";		
		return;
	}	
}

//实现../common/Calendar/Calendar.js中调用的方法
function _afterSelectCalendar(obj)
{
	if(obj==fm.CValiDate)
	{
		showCInValiDate();
	}
}


function queryChannel()
{
  showInfo=openWindow("./FCChannelQueryInput.html");
}

function afterQueryChannel(arrQueryResult)
{
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
	    arrResult = arrQueryResult;	
		fm.ChannelCode.value = arrResult[0]; 
		fm.ChannelName.value = arrResult[1];
   }
}

//下拉保险公司
function showSupplierClick(str1,str2,str3)
{
	//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		showCodeList('inscom6',[str1,str2,str3],[0,1,2],null,'1',fm.ProjCode.value,'1',300);
	}
	//代理业务查询保险公司
	else
	{
		showCodeList('inscom5',[str1,str2,str3],[0,1,2],null,null,null,null,300);
	}
}

function showSupplierKey(str1,str2,str3)
{
	//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		showCodeList('inscom6',[str1,str2,str3],[0,1,2],null,'1',fm.ProjCode.value,'1',300);
	}
	//代理业务查询保险公司
	else
	{
		showCodeList('inscom5',[str1,str2,str3],[0,1,2],null,null,null,null,300);
	}
}


//下拉险种
function showRiskClick(str1,str2,str3,str4,str5)
{
	if(NullOrBlank(fm.SupplierCode.value))
	{
		alert("请先选择保险公司！");
		return false;
	}
	if(NullOrBlank(fm.SignDate.value))
	{
		alert("请先录入出单日期！");
		return false;
	}
	if(NullOrBlank(fm.AgentCode.value))
	{
		alert("请先选择代理人！");
		return false;
	}
	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
	{
		alert("代理人信息有误！");
		return false;
	}
	//车险标志
	var carflag ="Y";
	//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		fm.RiskCons.value=fm.SupplierCode.value+","+fm.ProjCode.value+","+carflag;
	    showCodeList('riskcode11',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}else//财险代理业务
	{
		fm.RiskCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.AgentCode.value+","+fm.SignDate.value;
	    showCodeList('riskcode8',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}	
	
}

function showRiskKey(str1,str2,str3,str4,str5)
{
	if(NullOrBlank(fm.SupplierCode.value))
	{
		alert("请先选择保险公司！");
		return false;
	}
	if(NullOrBlank(fm.SignDate.value))
	{
		alert("请先录入出单日期！");
		return false;
	}
	if(NullOrBlank(fm.AgentCode.value))
	{
		alert("请先选择代理人！");
		return false;
	}
	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
	{
		alert("代理人信息有误！");
		return false;
	}
		
	//车险标志
	var carflag ="Y";	
		//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		fm.RiskCons.value=fm.SupplierCode.value+","+fm.ProjCode.value+","+carflag;
	    showCodeListKey('riskcode11',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}else//财险代理业务
	{
		fm.RiskCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.AgentCode.value+","+fm.SignDate.value;
	    showCodeListKey('riskcode8',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}	
}

function showFYCTypeClick(str1,str2,str3)
{
	if(NullOrBlank(fm.SupplierCode.value))
	{
		alert("请先选择保险公司！");
		return false;
	}
	if(NullOrBlank(fm.SignDate.value))
	{
		alert("请先录入出单日期！");
		return false;
	}
	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
	{
		alert("代理人信息有误！");
		return false;
	}
	if(NullOrBlank(fm.RiskCode.value))
	{
		alert("请先选择险种！");
		return false;
	}
		
	fm.FYCCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.RiskCode.value+","+fm.SignDate.value;
	showCodeList('fyctype2',[str1,str2,str3],[0,1,2],null,'1',fm.FYCCons.value,'1',300);
}

function showFYCTypeKey(str1,str2,str3)
{
	if(NullOrBlank(fm.SupplierCode.value))
	{
		alert("请先选择保险公司！");
		return false;
	}
	if(NullOrBlank(fm.SignDate.value))
	{
		alert("请先录入出单日期！");
		return false;
	}
	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
	{
		alert("代理人信息有误！");
		return false;
	}
	if(NullOrBlank(fm.RiskCode.value))
	{
		alert("请先选择险种！");
		return false;
	}
		
	fm.FYCCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.RiskCode.value+","+fm.SignDate.value;
	showCodeListKey('fyctype2',[str1,str2,str3],[0,1,2],null,'1',fm.FYCCons.value,'1',300);
}

function showCarOwnerName()
{
	if(fm.CarOwner.value!=fm.InsuredName.value)
	{
		fm.CarOwner.value=fm.InsuredName.value;
	}
}

function getPrintCont()
{
	var sql="select a.Policy from FCContNew a where a.ContNo='"+fm.ContNo.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	//是否有保单
	if(resultArr!=null&&resultArr.length>1)
	{
		openWindow("./FCPropContPrintQueryInput.jsp?ContNo="+fm.ContNo.value);
	}
	if(resultArr!=null&&resultArr.length==1)
	{
		getPrintContDetail(resultArr[0][0]);
	}
}

function getPrintContDetail(tPolicy)
{
	fm.Policy.value = tPolicy;
	
	var sql="select a.SupplierCode,a.SupplierName,a.POSNo,a.UWDate,a.SignDate,"
	+" a.SignTime,a.PolicyPli,a.InsuredName,a.InsuredIDType,a.InsuredIDTypeName,a.InsuredIDNo,"
	+" a.InsuredPostalAddress,a.InsuredMobile,a.InsuredHomePhone,a.InsuredPhone"
	+" from FCContNew a where a.Policy='"+tPolicy+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		if(NullOrBlank(fm.SupplierCode.value))
		{
			fm.SupplierCode.value=resultArr[0][0];
			fm.SupplierName.value=resultArr[0][1];
			fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",resultArr[0][0]);
		}
		fm.POSNo.value=resultArr[0][2];
		fm.UWDate.value=resultArr[0][3];
		fm.SignDate.value=resultArr[0][4];
		//fm.SignTime.value=resultArr[0][5];
		//fm.hour.value = (fm.SignTime.value).substring(0,2);
		//fm.minute.value = (fm.SignTime.value).substring(3,5);
		//fm.PolicyPli.value=resultArr[0][6];
		fm.InsuredName.value=resultArr[0][7];
		fm.InsuredIDType.value=resultArr[0][8];
		fm.InsuredIDTypeName.value=resultArr[0][9];
		fm.InsuredIDNo.value=resultArr[0][10];
		fm.InsuredPostalAddress.value=resultArr[0][11];
		fm.InsuredMobile.value=resultArr[0][12];
		fm.InsuredHomePhone.value=resultArr[0][13];
		fm.InsuredPhone.value=resultArr[0][14];
	}
	
	var sql="select a.CarOwner,a.LicenseNo,a.CarKindCode,a.CarKindName,a.UseNatureCode,a.UseNatureName,"
	+"a.VINNo,a.FrameNo,a.EngineNo,a.ModelCode,a.SeatCount,a.TonCount,a.ExhaustScale,a.EnrollDate,"
	+"a.UseYears,a.PurchasePrice,a.RunAreaName,a.ColorCode,a.ColorName"
	+" from FCCarNew a where a.Policy='"+tPolicy+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.CarOwner.value=resultArr[0][0];
		fm.LicenseNo.value=resultArr[0][1];
		fm.CarKindCode.value=resultArr[0][2];
		fm.CarKindName.value=resultArr[0][3];
		fm.UseNatureCode.value=resultArr[0][4];
		fm.UseNatureName.value=resultArr[0][5];
		fm.VINNo.value=resultArr[0][6];
		fm.FrameNo.value=resultArr[0][7];
		fm.EngineNo.value=resultArr[0][8];
		fm.ModelCode.value=resultArr[0][9];
		fm.SeatCount1.value=resultArr[0][10];
		fm.TonCount.value=resultArr[0][11];
		fm.ExhaustScale.value=resultArr[0][12];
		fm.EnrollDate.value=resultArr[0][13];
		fm.PurchasePrice.value=resultArr[0][15];
		fm.RunAreaName.value=resultArr[0][16];
		fm.ColorCode.value=resultArr[0][17];
		fm.ColorName.value=resultArr[0][18];
	}
	
	var sql="select a.RiskCode,a.RiskName,a.CValiDate,a.EndDate,a.SumPrem,a.tax1"
	+" from FCRiskNew a where a.Policy='"+tPolicy+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.RiskCode.value=resultArr[0][0];
		fm.KindCode.value=getNameByCode("MainFlag","fmrisk","RiskCode",resultArr[0][0]);
		fm.OutRiskCode.value=getNameByCode("OutRiskCode","fmrisk","RiskCode",resultArr[0][0]);
		fm.RiskName.value=resultArr[0][1];
		fm.CValiDate.value=resultArr[0][2];
		fm.CInValiDate.value = resultArr[0][3];
		fm.SumPrem.value=resultArr[0][4];
		fm.tax1.value=resultArr[0][5];
	}
	
	KindGrid.clearData();
	initKindGrid();
	var sql=" select a.RiskKindCode,a.RiskKindName,a.Amount,a.SumPrem"
	+" from FCRiskKindNew a where a.Policy='"+tPolicy+"'";
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		displayMultiline(resultArr,KindGrid);
	}
}

//经代项目查询
function projQueryClick()
{
	showInfo =openWindow("../broker/FBProjectQuery.jsp?pagetype=Prop");
}

//经代项目查询
function afterQueryPro(arrQueryResult){
	initForm();
	var ProjCode = arrQueryResult[0][0];
	
	if(ProjCode == null||ProjCode == ""){
		return false;
	}
	
	//投保客户信息
	var getClientSQL = "select RoleFlag,ClientNo from FCProjMain where ProjCode = '" + ProjCode + "'";
	var clientResult=easyQueryVer3(getClientSQL,1,0);
	var clientResultArr=decodeEasyQueryResult(clientResult);
	
	//客户信息
	if(clientResultArr!=null){ //个人客户
		fm.InsuredCustType.value = clientResultArr[0][0];
		fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",clientResultArr[0][0]);
		fm.InsuredNo.value = clientResultArr[0][1];
		fm.InsuredName.value=getNameByCode("CustomerName","V_FCCustomer","CustomerNo",clientResultArr[0][1]);
	}
	//查询项目下保险公司险种信息
	queryFCProjRiskIns(ProjCode);
}

//判断单证是否已经被使用

function JudgeCertifyCard()
{
	var tPolicyPrtNo = fm.PolicyPrtNo.value;
	var tInvoicePrtNo = fm.InvoicePrtNo.value;
	var tInsCardPrtNo = fm.InsCardPrtNo.value;
	var tCInsFlagPrtNo = fm.CInsFlagPrtNo.value;
	var tSupplierCode = fm.SupplierCode.value;
	
	var tMasterInscom = tSupplierCode.substring(0,4);
	
	//先用险种关联查询，如果查到了，直接看使用状态；
	//先用险种关联查询，如果查不到，就不用险种关联查询；
	var sqlPart = " and exists(select 1 from fmcardrisk where CertifyCode = a.CertifyCode and riskcode = '"+fm.RiskCode.value+"') "; //看保险公司险种 是否 用险种关联
	if(!NullOrBlank(tPolicyPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'J'"
	        +" and length(StartNo) = '"+tPolicyPrtNo.length+"' and StartNo <= '"+tPolicyPrtNo+"' and EndNo >= '"+tPolicyPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.PolicyPrtNo","保单印刷号【"+tPolicyPrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'J'"
	        +" and length(StartNo) = '"+tPolicyPrtNo.length+"' and StartNo <= '"+tPolicyPrtNo+"' and EndNo >= '"+tPolicyPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
			var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.PolicyPrtNo","保单印刷号【"+tPolicyPrtNo+"】已经被使用！");
					return false;
				}
			}
		}
	}
	
	if(!NullOrBlank(tInvoicePrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'I'"
	        +" and length(StartNo) = '"+tInvoicePrtNo.length+"' and StartNo <= '"+tInvoicePrtNo+"' and EndNo >= '"+tInvoicePrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.InvoicePrtNo","发票印刷号【"+tInvoicePrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'I'"
	        +" and length(StartNo) = '"+tInvoicePrtNo.length+"' and StartNo <= '"+tInvoicePrtNo+"' and EndNo >= '"+tInvoicePrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
	        var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.InvoicePrtNo","发票印刷号【"+tInvoicePrtNo+"】已经被使用！");
					return false;
				}
			}
	        
		}
	}
	if(!NullOrBlank(tInsCardPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'H'"
	        +" and length(StartNo) = '"+tInsCardPrtNo.length+"' and StartNo <= '"+tInsCardPrtNo+"' and EndNo >= '"+tInsCardPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.InsCardPrtNo","保险卡印刷号【"+tInsCardPrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'H'"
	        +" and length(StartNo) = '"+tInsCardPrtNo.length+"' and StartNo <= '"+tInsCardPrtNo+"' and EndNo >= '"+tInsCardPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
	        var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.InsCardPrtNo","保险卡印刷号【"+tInsCardPrtNo+"】已经被使用！");
					return false;
				}
			}
		}
	}
	if(!NullOrBlank(tCInsFlagPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'G'"
	        +" and length(StartNo) = '"+tCInsFlagPrtNo.length+"' and StartNo <= '"+tCInsFlagPrtNo+"' and EndNo >= '"+tCInsFlagPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.CInsFlagPrtNo","交强险标志印刷号【"+tCInsFlagPrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'G'"
	        +" and length(StartNo) = '"+tCInsFlagPrtNo.length+"' and StartNo <= '"+tCInsFlagPrtNo+"' and EndNo >= '"+tCInsFlagPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"; 
	        var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.CInsFlagPrtNo","交强险标志印刷号【"+tCInsFlagPrtNo+"】已经被使用！");
					return false;
				}
			}
	        
		}
    }
    
    return true;
}

function checkFDInsContNo()
{
	var tMasterInscom = fm.SupplierCode.value.substring(0,4);
	var tRiskCode = fm.RiskCode.value;
	var tContNoLen = fm.ContNo.value.length;
	var strSql1 = "select 'Y' from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03'";    
    var tResult1 = easyQueryVer4(strSql1, 1, 0, 1);
	if(tResult1 == 'Y'){			
		//校验险种层的保单号长度
		if(!checkRiskCodeCont(tMasterInscom,tRiskCode,tContNoLen)){
			alert("保单号长度与系统 险种层 设置的长度不符！");
			return false;
		}	
	}
	else{
		var strSql2 = "select 'Y' from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02'";    
    	var tResult2 = easyQueryVer4(strSql2, 1, 0, 1);
		if(tResult2 == 'Y'){			
			//校验险类层的保单号长度
			if(!checkRiskKindCont(tMasterInscom,tRiskCode,tContNoLen)){
				alert("保单号长度与系统 险类层 设置的长度不符！");
				return false;
			}			
		}
		else{
			var strSql3 = "select 'Y' from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and DefineType = '01'";    
    		var tResult3 = easyQueryVer4(strSql3, 1, 0, 1);
			if(tResult3 == 'Y'){				
				//校验保险公司层的保单号长度
				if(!checkMasterInscomCont(tMasterInscom,tContNoLen)){
					alert("保单号长度与系统 保险公司层 设置的长度不符！");
					return false;
				}	
			}
			else{
				alert("保单号对应的保险公司未设置保单号长度，请联系系统管理员!");
				return false;
			}
		}
	}
	return true;
}

function checkRiskCodeCont(tMasterInscom,tRiskCode,tContNoLen){
	var sql="select ContNoLenMode from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03'";    
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	
	if(resultArr!=null)
	{
		if(resultArr[0][0]=="01")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03' and ContNoLen='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);			
		    if(!result)
			{
				return false;
			}
		}
		else if(resultArr[0][0]=="02")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03' and DLen<='"+tContNoLen+"' and UpLen>='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
	}
	return true;	
}


function checkRiskKindCont(tMasterInscom,tRiskCode,tContNoLen){
	var sql="select ContNoLenMode from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02'";    
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		if(resultArr[0][0]=="01")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02' and ContNoLen='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
		else if(resultArr[0][0]=="02")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02' and DLen<='"+tContNoLen+"' and UpLen>='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
	}	
	return true;	
}

function checkMasterInscomCont(tMasterInscom,tContNoLen){
	var sql="select ContNoLenMode from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and DefineType = '01'";    
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		if(resultArr[0][0]=="01")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and DefineType = '01' and ContNoLen='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
		else if(resultArr[0][0]=="02")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and DefineType = '01' and DLen<='"+tContNoLen+"' and UpLen>='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
	}	
	return true;	
}

function isSingle()
{ 
	if(fm.ISSingle.checked)
	{
		fm.IsSingle1.value="Y";
	}
	else{ 
		fm.IsSingle1.value="N";
		
	} 
}



