<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.CErrors"%>
<%@page import="com.sinosoft.utility.VData"%>
<%@page import="com.sinosoft.jbs.schema.FCContSchema"%>
<%@page import="com.sinosoft.jbs.schema.FCItemCarSchema"%>
<%@page import="com.sinosoft.jbs.db.FCContDB"%>
<%@page import="com.sinosoft.jbs.db.FCItemCarDB"%>
<%@page import="com.sinosoft.jbs.business.FCCarContFullUI"%>
<%@page import="com.sinosoft.jbs.schema.FCPolSchema"%>
<%@page import="com.sinosoft.jbs.schema.FCItemMainSchema"%>
<%@page import="com.sinosoft.jbs.schema.FCItemKindSchema"%>
<%@page import="com.sinosoft.jbs.vschema.FCItemKindSet"%>
<%@page import="com.sinosoft.jbs.db.FCPolDB"%>
<%@page import="com.sinosoft.jbs.db.FCItemMainDB"%>
<%@page import="com.sinosoft.jbs.pubfun.GlobalInput"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String FlagStr = "";
String Content = "";
CErrors tError = null;

String tOperate = request.getParameter("hideOperate");

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

FCCarContFullUI tFCCarContFullUI = new FCCarContFullUI();
FCContSchema tFCContSchema = new FCContSchema();
FCItemCarSchema tFCItemCarSchema = new FCItemCarSchema();
FCPolSchema tFCPolSchema = new FCPolSchema();
FCItemMainSchema tFCItemMainSchema = new FCItemMainSchema();
FCItemKindSet tFCItemKindSet = new FCItemKindSet();

String tInnerContNo = request.getParameter("InnerContNo");
String tItemNo = request.getParameter("ItemNo");
String tPolNo = request.getParameter("PolNo");
String tFullFlag = request.getParameter("FullFlag");
String tAgentScore = request.getParameter("AgentScore");
String tInputType = request.getParameter("InputType");

if(tInnerContNo!=null&&!tInnerContNo.equals("")&&tOperate.equals("INSERT||MAIN"))
{
	tOperate = "UPDATE||MAIN";
	FCContDB tFCContDB = new FCContDB();
	tFCContDB.setInnerContNo(request.getParameter("InnerContNo"));
	if (tFCContDB.getInfo()) 
	{
		tFCContSchema.setSchema(tFCContDB.getSchema());
	}
	
	FCItemCarDB tFCItemCarDB = new FCItemCarDB();
  	tFCItemCarDB.setInnerContNo(tInnerContNo);
  	tFCItemCarDB.setItemNo(tItemNo);
  	if (tFCItemCarDB.getInfo()) 
  	{
	  	tFCItemCarSchema.setSchema(tFCItemCarDB.getSchema());
  	}
  	
  	FCPolDB tFCPolDB = new FCPolDB();
	tFCPolDB.setInnerContNo(tInnerContNo);
	tFCPolDB.setPolNo(tPolNo);
	if (tFCPolDB.getInfo()) 
	{
		tFCPolSchema.setSchema(tFCPolDB.getSchema());
	}
	
	FCItemMainDB tFCItemMainDB = new FCItemMainDB();
	tFCItemMainDB.setInnerContNo(tInnerContNo);
	tFCItemMainDB.setPolNo(tPolNo);
	tFCItemMainDB.setItemNo(tItemNo);
	if (tFCItemMainDB.getInfo()) 
	{
		tFCItemMainSchema.setSchema(tFCItemMainDB.getSchema());
	}
}

tFCContSchema.setInnerContNo(request.getParameter("InnerContNo"));
//tFCContSchema.setInnerGrpContNo(request.getParameter("InnerGrpContNo"));
//tFCContSchema.setGrpContNo(request.getParameter("GrpContNo"));
tFCContSchema.setContNo(request.getParameter("ContNo").trim());
tFCContSchema.setProposalContNo(request.getParameter("ContNo").trim());
tFCContSchema.setProjCode(request.getParameter("ProjCode"));
tFCContSchema.setSupplierCode(request.getParameter("SupplierCode"));
tFCContSchema.setPolicyPrtNo(request.getParameter("PolicyPrtNo"));
tFCContSchema.setInvoicePrtNo(request.getParameter("InvoicePrtNo"));
tFCContSchema.setInsCardPrtNo(request.getParameter("InsCardPrtNo"));
tFCContSchema.setCInsFlagPrtNo(request.getParameter("CInsFlagPrtNo"));
//tFCContSchema.setBizType(request.getParameter("BizType"));
//tFCContSchema.setContType(request.getParameter("ContType"));
//tFCContSchema.setBussProperty(request.getParameter("BussProperty"));
tFCContSchema.setBussNature(request.getParameter("BussNature"));
//tFCContSchema.setContSource(request.getParameter("ContSource"));
//tFCContSchema.setFamilyType(request.getParameter("FamilyType"));
//tFCContSchema.setCardFlag(request.getParameter("CardFlag"));
//tFCContSchema.setCorporation(request.getParameter("Corporation"));
//tFCContSchema.setBranchOffice(request.getParameter("BranchOffice"));
//tFCContSchema.setMgmtDivision(request.getParameter("MgmtDivision"));
//tFCContSchema.setManageCom(request.getParameter("ManageCom"));
tFCContSchema.setAgentGroup(request.getParameter("AgentGroup"));
//tFCContSchema.setAgentCom(request.getParameter("AgentCom"));
tFCContSchema.setOutManageCom(request.getParameter("OutManageCom"));
tFCContSchema.setGetManageCom(request.getParameter("GetManageCom"));
tFCContSchema.setPOSNo(request.getParameter("POSNo"));
tFCContSchema.setYearExamDate(request.getParameter("YearExamDate"));
tFCContSchema.setPayPCommFlag(request.getParameter("PayPCommFlag"));
tFCContSchema.setPayPComm(request.getParameter("PayPComm"));
tFCContSchema.setAppntCustType(request.getParameter("AppntCustType"));
tFCContSchema.setAppntNo(request.getParameter("AppntNo"));
tFCContSchema.setAppntAddressNo(request.getParameter("AppntAddressNo"));
tFCContSchema.setAppntName(request.getParameter("AppntName"));
tFCContSchema.setAppntSex(request.getParameter("AppntSex"));
tFCContSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
tFCContSchema.setAppntIDType(request.getParameter("AppntIDType"));
tFCContSchema.setAppntIDNo(request.getParameter("AppntIDNo"));
tFCContSchema.setAppntOccup(request.getParameter("AppntOccup"));
tFCContSchema.setAppntPostalAddress(request.getParameter("AppntPostalAddress"));
tFCContSchema.setAppntZipCode(request.getParameter("AppntZipCode"));
tFCContSchema.setAppntMobile(request.getParameter("AppntMobile"));
tFCContSchema.setAppntPhone(request.getParameter("AppntPhone"));
tFCContSchema.setInsuredCustType(request.getParameter("InsuredCustType"));
tFCContSchema.setInsuredNo(request.getParameter("InsuredNo"));
tFCContSchema.setInsuredAddressNo(request.getParameter("InsuredAddressNo"));
tFCContSchema.setInsuredName(request.getParameter("InsuredName").trim());
tFCContSchema.setInsuredSex(request.getParameter("InsuredSex"));
tFCContSchema.setInsuredBirthday(request.getParameter("InsuredBirthday"));
tFCContSchema.setInsuredIDType(request.getParameter("InsuredIDType"));
tFCContSchema.setInsuredIDNo(request.getParameter("InsuredIDNo"));
tFCContSchema.setInsuredOccup(request.getParameter("InsuredOccup"));
tFCContSchema.setInsuredPostalAddress(request.getParameter("InsuredPostalAddress"));
tFCContSchema.setInsuredZipCode(request.getParameter("InsuredZipCode"));
tFCContSchema.setInsuredMobile(request.getParameter("InsuredMobile"));
tFCContSchema.setInsuredPhone(request.getParameter("InsuredPhone"));
//tFCContSchema.setInsureAppntRelation(request.getParameter("InsureAppntRelation"));
//tFCContSchema.setPayMode(request.getParameter("PayMode"));
tFCContSchema.setPayBank(request.getParameter("Policy"));//百星保单流水号
//tFCContSchema.setPayBankAccNo(request.getParameter("PayBankAccNo"));
//tFCContSchema.setPayAccName(request.getParameter("PayAccName"));
//tFCContSchema.setGetBank(request.getParameter("GetBank"));
//tFCContSchema.setGetBankAccNo(request.getParameter("GetBankAccNo"));
//tFCContSchema.setGetAccName(request.getParameter("GetAccName"));
tFCContSchema.setPolicyPli(request.getParameter("hidePolicyPli"));
//tFCContSchema.setBrokerCode(request.getParameter("BrokerCode"));
tFCContSchema.setAgentCode(request.getParameter("AgentCode"));
//tFCContSchema.setState(request.getParameter("State"));
//tFCContSchema.setPolState(request.getParameter("PolState"));
//tFCContSchema.setErrorTimes(request.getParameter("ErrorTimes"));
//tFCContSchema.setExamState(request.getParameter("ExamState"));
//tFCContSchema.setAddCard(request.getParameter("AddCard"));
//tFCContSchema.setReInvoiceDeliverFlag(request.getParameter("ReInvoiceDeliverFlag"));
//tFCContSchema.setLastNoteDate(request.getParameter("LastNoteDate"));
//tFCContSchema.setLastEdorDate(request.getParameter("LastEdorDate"));
//tFCContSchema.setLastClaimDate(request.getParameter("LastClaimDate"));
tFCContSchema.setSignDate(request.getParameter("SignDate"));
tFCContSchema.setSignTime(request.getParameter("SignTime"));
tFCContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));
tFCContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
tFCContSchema.setReceiveTime(request.getParameter("ReceiveTime"));
//tFCContSchema.setPolApplyDate(request.getParameter("PolApplyDate"));
tFCContSchema.setCValiDate(request.getParameter("CValiDate"));
tFCContSchema.setCInValiDate(request.getParameter("hideCInValiDate"));
//tFCContSchema.setInputOperator(request.getParameter("InputOperator"));
//tFCContSchema.setInputDate(request.getParameter("InputDate"));
//tFCContSchema.setInputTime(request.getParameter("InputTime"));
//tFCContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));
//tFCContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
//tFCContSchema.setReceiveTime(request.getParameter("ReceiveTime"));
//tFCContSchema.setCancelDate(request.getParameter("CancelDate"));
tFCContSchema.setRemark(request.getParameter("hideRemark").replaceAll("@E!"," "));
//tFCContSchema.setPolType(request.getParameter("PolType"));
//tFCContSchema.setPayIntv(request.getParameter("PayIntv"));
//tFCContSchema.setSignCom(request.getParameter("SignCom"));
//tFCContSchema.setPrintCount(request.getParameter("PrintCount"));
//tFCContSchema.setLostTimes(request.getParameter("LostTimes"));
//tFCContSchema.setPeoples(request.getParameter("Peoples"));
//tFCContSchema.setMult(request.getParameter("Mult"));
//tFCContSchema.setAmnt(request.getParameter("Amnt"));
tFCContSchema.setSumPrem(request.getParameter("SumPrem"));
tFCContSchema.setContPrem(request.getParameter("SumPrem"));//原始保单保费
//tFCContSchema.setPaytoDate(request.getParameter("PaytoDate"));
//tFCContSchema.setFirstPayDate(request.getParameter("FirstPayDate"));
//tFCContSchema.setAppFlag(request.getParameter("AppFlag"));
//tFCContSchema.setGetPolDate(request.getParameter("GetPolDate"));
//tFCContSchema.setGetPolTime(request.getParameter("GetPolTime"));
//tFCContSchema.setCustomGetPolDate(request.getParameter("CustomGetPolDate"));
//tFCContSchema.setPostalAddress(request.getParameter("PostalAddress"));
//tFCContSchema.setZipCode(request.getParameter("ZipCode"));
//tFCContSchema.setPostalAddressFlag(request.getParameter("PostalAddressFlag"));
//tFCContSchema.setImpawnFlag(request.getParameter("ImpawnFlag"));
//tFCContSchema.setPassword(request.getParameter("Password"));
//tFCContSchema.setOperator(request.getParameter("Operator"));
//tFCContSchema.setMakeDate(request.getParameter("MakeDate"));
//tFCContSchema.setMakeTime(request.getParameter("MakeTime"));
//tFCContSchema.setModifyDate(request.getParameter("ModifyDate"));
//tFCContSchema.setModifyTime(request.getParameter("ModifyTime"));
tFCContSchema.setProtocolNo(request.getParameter("ProtocolNo"));
//tFCContSchema.setSendPolDate(request.getParameter("SendPolDate"));
//tFCContSchema.setSendReceptionDate(request.getParameter("SendReceptionDate"));
//tFCContSchema.setInnerMasterContNo(request.getParameter("InnerMasterContNo"));
//tFCContSchema.setOrphanFlag(request.getParameter("OrphanFlag"));
tFCContSchema.setAgentFlag(request.getParameter("AgentFlag"));
tFCContSchema.setFirstFeeFlag(request.getParameter("FirstFeeFlag"));
//tFCContSchema.setCarFlag(request.getParameter("CarFlag"));
//tFCContSchema.setSupplierGetPolDate(request.getParameter("SupplierGetPolDate"));
//tFCContSchema.setIRate(request.getParameter("IRate"));
tFCContSchema.setHandlingNo(request.getParameter("HandlingNo"));
tFCContSchema.setHandlingAddressNo(request.getParameter("HandlingAddressNo"));
tFCContSchema.setHandlingName(request.getParameter("HandlingName"));
tFCContSchema.setHandlingSex(request.getParameter("HandlingSex"));
tFCContSchema.setHandlingBirthday(request.getParameter("HandlingBirthday"));
tFCContSchema.setHandlingIDType(request.getParameter("HandlingIDType"));
tFCContSchema.setHandlingIDNo(request.getParameter("HandlingIDNo"));
tFCContSchema.setHandlingOccup(request.getParameter("HandlingOccup"));
tFCContSchema.setHandlingPostalAddress(request.getParameter("HandlingPostalAddress"));
tFCContSchema.setHandlingZipCode(request.getParameter("HandlingZipCode"));
tFCContSchema.setHandlingMobile(request.getParameter("HandlingMobile"));
tFCContSchema.setHandlingPhone(request.getParameter("HandlingPhone"));
//tFCContSchema.setPremPrintTimes(request.getParameter("PremPrintTimes"));
tFCContSchema.setOldSupplierCode(request.getParameter("OldSupplierCode"));
tFCContSchema.setOldContNo(request.getParameter("OldContNo"));
//tFCContSchema.setFeePrintFlag(request.getParameter("FeePrintFlag"));
//tFCContSchema.setFeePrintTimes(request.getParameter("FeePrintTimes"));
//tFCContSchema.setCancelReason(request.getParameter("CancelReason"));
//tFCContSchema.setFirstPay(request.getParameter("FirstPay"));
//tFCContSchema.setAmount(request.getParameter("Amount"));
//tFCContSchema.setAmountRemark(request.getParameter("AmountRemark"));
//tFCContSchema.setSendContDate(request.getParameter("SendContDate"));
tFCContSchema.setAppntMobile2(request.getParameter("AppntMobile2"));
tFCContSchema.setInsuredMobile2(request.getParameter("InsuredMobile2"));
//tFCContSchema.setInputCom(request.getParameter("InputCom"));
//tFCContSchema.setReceiveCom(request.getParameter("ReceiveCom"));
tFCContSchema.setLicenseNo(request.getParameter("LicenseNo").trim());
tFCContSchema.setComFirstFeeFlag(request.getParameter("ComFirstFeeFlag"));
tFCContSchema.setUWDate(request.getParameter("UWDate"));
//tFCContSchema.setInsuredSupplierOccuCode(request.getParameter("InsuredSupplierOccuCode"));
//tFCContSchema.setAppntSupplierOccuCode(request.getParameter("AppntSupplierOccuCode"));
tFCContSchema.setSignMan(request.getParameter("SignMan"));
tFCContSchema.setAppntHomePhone(request.getParameter("AppntHomePhone"));
tFCContSchema.setInsuredHomePhone(request.getParameter("InsuredHomePhone"));
tFCContSchema.setHandlingHomePhone(request.getParameter("HandlingHomePhone"));
tFCContSchema.setRightSendFlag(request.getParameter("RightSendFlag"));
tFCContSchema.setHandlingMobile2(request.getParameter("HandlingMobile2"));
tFCContSchema.setChannelCode(request.getParameter("ChannelCode"));
tFCContSchema.setAccreditFlag(request.getParameter("AccreditFlag"));
tFCContSchema.setInsuredMemberType(request.getParameter("InsuredMemberType"));
tFCContSchema.setAppntMemberType(request.getParameter("AppntMemberType"));
tFCContSchema.setHandlingMemberType(request.getParameter("HandlingMemberType"));
if(request.getParameter("SignMan")==null||request.getParameter("SignMan").equals(""))
{
	tFCContSchema.setSignManName(request.getParameter("SignManName"));
}

//以下为标的的具体信息
tFCItemCarSchema.setInnerContNo(tInnerContNo);
tFCItemCarSchema.setContNo(request.getParameter("ContNo"));
tFCItemCarSchema.setItemNo(tItemNo);
tFCItemCarSchema.setItemCode(request.getParameter("ItemCode"));
tFCItemCarSchema.setItemName(request.getParameter("ItemName"));
tFCItemCarSchema.setCarOwner(request.getParameter("CarOwner"));//车主

tFCItemCarSchema.setPolicyEndDate(request.getParameter("hideCInValiDate"));//保险到期日

tFCItemCarSchema.setLicenseNo(request.getParameter("LicenseNo").trim());//车牌号
tFCItemCarSchema.setHKFlag(request.getParameter("HKFlag"));//是否港澳标志
tFCItemCarSchema.setHKLicenseNo(request.getParameter("HKLicenseNo"));//港澳车牌号
tFCItemCarSchema.setLicenseColorCode(request.getParameter("LicenseColorCode"));//车牌底色
tFCItemCarSchema.setCarKindCode(request.getParameter("CarKindCode"));//车辆种类
tFCItemCarSchema.setEngineNo(request.getParameter("EngineNo"));//发动机号
tFCItemCarSchema.setVINNo(request.getParameter("VINNo"));//VIN码
tFCItemCarSchema.setFrameNo(request.getParameter("FrameNo"));//车架号
tFCItemCarSchema.setRunAreaName(request.getParameter("RunAreaName"));//行驶区域
tFCItemCarSchema.setRunMiles(request.getParameter("RunMiles"));//行驶里程
tFCItemCarSchema.setEnrollDate(request.getParameter("EnrollDate"));//初次登记日期

tFCItemCarSchema.setUseYears(request.getParameter("UseYears"));//使用年限
tFCItemCarSchema.setModelCode(request.getParameter("ModelCode"));//车型代码
tFCItemCarSchema.setCountryNature(request.getParameter("CountryNature"));//国别性质
tFCItemCarSchema.setCountryCode(request.getParameter("CountryCode1"));//生产国家
tFCItemCarSchema.setUseNatureCode(request.getParameter("UseNatureCode"));//使用性质
tFCItemCarSchema.setSeatCount(request.getParameter("SeatCount1"));//座位数
tFCItemCarSchema.setTonCount(request.getParameter("TonCount"));//吨位数
tFCItemCarSchema.setExhaustScale(request.getParameter("ExhaustScale"));//排量

tFCItemCarSchema.setColorCode(request.getParameter("ColorCode"));//车身颜色
tFCItemCarSchema.setSafeDevice(request.getParameter("SafeDevice"));//安全配置
tFCItemCarSchema.setParkSite(request.getParameter("ParkSite"));//固定放置地点
tFCItemCarSchema.setOwnerAddress(request.getParameter("OwnerAddress"));//购车人地址
tFCItemCarSchema.setProduceDate(request.getParameter("ProduceDate"));//生产日期
tFCItemCarSchema.setCarUsage(request.getParameter("CarUsage"));//购车用途
tFCItemCarSchema.setPurchasePrice(request.getParameter("PurchasePrice"));//新车购置价格
tFCItemCarSchema.setInvoiceNo(request.getParameter("InvoiceNo"));//购车发票号

tFCItemCarSchema.setBuyDate(request.getParameter("BuyDate"));//购车日期
tFCItemCarSchema.setYearExamDate(request.getParameter("YearExamDate"));//年审到期日

tFCItemCarSchema.setCarChecker(request.getParameter("CarChecker"));//验车人
tFCItemCarSchema.setCarCheckTime(request.getParameter("CarCheckTime"));//验车时间
tFCItemCarSchema.setCarCheckStatus(request.getParameter("CarCheckStatus"));//验车情况
tFCItemCarSchema.setCarDealerName(request.getParameter("CarDealerName"));//经销商名称

tFCPolSchema.setContNo(request.getParameter("ContNo"));
tFCPolSchema.setRiskCode(request.getParameter("RiskCode"));
tFCPolSchema.setOutRiskCode(request.getParameter("OutRiskCode"));
tFCPolSchema.setKindCode(request.getParameter("KindCode"));
tFCPolSchema.setPayIntv(request.getParameter("PayIntv"));
tFCPolSchema.setSumPrem(request.getParameter("SumPrem"));
tFCPolSchema.setContPrem(request.getParameter("SumPrem"));// 原始保单保费
String ttax1 = request.getParameter("tax1");
if(ttax1==null || ttax1.equals(""))
{
	ttax1="0";
}
tFCPolSchema.settax1(ttax1);
String ttax2 = request.getParameter("tax2");
if(ttax2==null || ttax2.equals(""))
{
	ttax2="0";
}
tFCPolSchema.settax2(ttax2);
tFCPolSchema.setSignDate(request.getParameter("SignDate"));
tFCPolSchema.setSignTime(request.getParameter("SignTime"));
tFCPolSchema.setAgentCode(request.getParameter("AgentCode"));
tFCPolSchema.setSupplierCode(request.getParameter("SupplierCode"));
tFCPolSchema.setLicenseNo(request.getParameter("LicenseNo").trim());
tFCPolSchema.setProtocolNo(request.getParameter("ProtocolNo"));
tFCPolSchema.setFYCType(request.getParameter("FYCType"));
tFCPolSchema.setISSingle(request.getParameter("IsSingle1"));//是否单出
tFCPolSchema.setRuleID(request.getParameter("RuleID"));//规制ID

tFCItemMainSchema.setInnerContNo(tInnerContNo);
tFCItemMainSchema.setPolNo(tPolNo);
tFCItemMainSchema.setItemNo(tItemNo);
tFCItemMainSchema.setItemCode(request.getParameter("ItemCode"));
tFCItemMainSchema.setItemName(request.getParameter("ItemName"));
tFCItemMainSchema.setRiskCode(request.getParameter("RiskCode"));
tFCItemMainSchema.setSupplierCode(request.getParameter("SupplierCode"));

String tKindCount[]  = request.getParameterValues("KindGridNo");
if(tKindCount!=null)
{
	int lineCount = tKindCount.length; //行数
	if(lineCount>0)
	{
		String tDutyCode[] = request.getParameterValues("KindGrid1");
		String tDutyName[] = request.getParameterValues("KindGrid2");
		String tAmnt[] = request.getParameterValues("KindGrid3");
		String tPrem[] = request.getParameterValues("KindGrid4");
	
		for(int i=0;i<lineCount;i++)
		{
			FCItemKindSchema tFCItemKindSchema = new FCItemKindSchema();
			tFCItemKindSchema.setInnerContNo(tInnerContNo);
			tFCItemKindSchema.setPolNo(tPolNo);
			tFCItemKindSchema.setRiskCode(request.getParameter("RiskCode"));
			tFCItemKindSchema.setItemNo(request.getParameter("ItemNo"));
			tFCItemKindSchema.setItemCode(request.getParameter("ItemCode"));
			tFCItemKindSchema.setItemName(request.getParameter("ItemName"));
			tFCItemKindSchema.setItemDetailNo("000000");
			tFCItemKindSchema.setRiskKindCode(tDutyCode[i]);
			tFCItemKindSchema.setRiskKindName(tDutyName[i]);
			if(tAmnt[i]==null ||tAmnt[i].equals(""))
			{
				tAmnt[i]="0";
			}
			tFCItemKindSchema.setAmount(tAmnt[i]);
			if(tPrem[i]==null ||tPrem[i].equals(""))
			{
				tPrem[i]="0";
			}
			tFCItemKindSchema.setSumPrem(tPrem[i]);
			tFCItemKindSchema.setStartDate(request.getParameter("CValiDate"));
			tFCItemKindSchema.setEndDate(request.getParameter("hideCInValiDate"));
			tFCItemKindSet.add(tFCItemKindSchema);
		}
	}
}

//对于页面上数字的处理
String tPayPComm = request.getParameter("PayPComm");
if(tPayPComm==null||tPayPComm.equals(""))
{
	tFCContSchema.setPayPComm("0");
}

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tAgentScore);
tVData.add(tInputType);
tVData.add(tFCContSchema);
tVData.add(tFCItemCarSchema);
tVData.add(tFCPolSchema);
tVData.add(tFCItemMainSchema);
tVData.add(tFCItemKindSet);
tVData.add(tG);

try
{
	System.out.println("this will save the data!!!");
	tFCCarContFullUI.submitData(tVData,tOperate);
}
catch(Exception ex)
{
	ex.printStackTrace();
	FlagStr = "Fail";
}


if (!FlagStr.equals("Fail"))
{
	tError = tFCCarContFullUI.mErrors;
	if (!tError.needDealError())
	{
		tInnerContNo = tFCCarContFullUI.getInnerContNo();
		tPolNo = tFCCarContFullUI.getPolNo();
		tItemNo = tFCCarContFullUI.getItemNo();
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
    
%>
<html>
<script language="javascript">
	parent.fraInterface.afterContSubmit("<%=FlagStr%>","<%=Content%>","<%=tOperate%>","<%=tInnerContNo%>","<%=tPolNo%>","<%=tItemNo%>");
</script>
</html>