<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：FCPropContInput.jsp
	//程序功能：
	//创建日期：2008-7-3 16:41:13
	//创建人  ：陈强
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<%
	//GlobalInput tG = new GlobalInput();
	//tG = (GlobalInput) session.getValue("GI");
	//String tComType = tG.ComType;
	//String tOperator = tG.Operator;

	String tComType = "1201198308";
	String tOperator = "001";
	
	String tCurrentDate = PubFun.getCurrentDate();
	String tCurrentTime = PubFun.getCurrentTime();
	String tCurrentHour = tCurrentTime.substring(0, 2);
	String tCurrentMinute = tCurrentTime.substring(3, 5);
	String tCurrentSecond = tCurrentTime.substring(6, 8);

	String tInputType = request.getParameter("InputType");
	String mInnerContNo = request.getParameter("InnerContNo");

	if (mInnerContNo == null || mInnerContNo.equals("null"))
		mInnerContNo = "";
%>
<html>
<head>
<script language="javascript">
	var tComType="<%=tComType%>";
	var tInputType="<%=tInputType%>";
	var tCurrentDate="<%=tCurrentDate%>";
	var mInnerContNo="<%=mInnerContNo%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer4.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="FCCarContFullInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FCCarContFullInputInit.jsp"%>
<%@include file="../common/jsp/FHManageComLimit.jsp"%>
</head>
<body onload="initForm();initElementtype();showDetail();">
<form action="./FCCarContFullSave.jsp" name=fm target="fraSubmit"
	method=post>
<div id="div1" style="display: ''">
<table class="common" align=center>
	<tr align=right>
		<td class=button width="1%"><INPUT type=button VALUE="完整录入"
			id="butFull" onclick="return fullClick();" class=cssButton
			TYPE=button></td>
		<td class=button><input value="保单复制" onClick="queryCopyCont();"
			id="butCopy" type="button" class=cssButtonLong></td>
		<td class=button width="10%"><INPUT id=butProj class=cssButton
			VALUE="项目查询" TYPE=hidden onclick="return projQueryClick();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="刷  新"
			TYPE=button onclick="return initForm();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="查  询"
			TYPE=button onclick="return queryCont();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="删  除"
			id="butDel" TYPE=button onclick="return deleteContClick();">
		</td>
	</tr>
</table>
<tr>
	<font color=red>提示：完整录入的保单将默认为补充录入完成<br>
</tr>

</div>
<div id="div2" style="display: ''">
<table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divPrtNo);"> 单证信息
		</TD>
	</TR>
</TABLE>

<Div id="divPrtNo" style="display: ''">
<TABLE class="common">
	<TR id="fullInput1" style="display: ''">
		<TD class=title>保单印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=PolicyPrtNo
			verify="保单印刷号|len<=100"></TD>
		<TD class=title>发票印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=InvoicePrtNo
			verify="发票印刷号|len<=100"></TD>
	</TR>
	<TR class="common">
		<TD class=title>保险卡印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=InsCardPrtNo
			verify="保险卡印刷号|len<=100"></TD>
		<TD class=title>交强险标志印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=CInsFlagPrtNo
			verify="交强险标志印刷号|len<=100"></TD>
	</TR>
</TABLE>
</Div>
</div>
<div id="div3" style="display: ''">
<table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divContInfo);">
		保单信息&nbsp;&nbsp;&nbsp;&nbsp;</TD>
	</TR>
</TABLE>

<Div id="divContInfo" style="display: ''">
<TABLE class="common">
	<TR class=common>
		<TD class=title>出单机构&nbsp;</TD>
		<TD class=input><input class=codeno name=OutManageCom
			onDblClick="return showCodeList('SettleMgmtDivision',[this,OutManageComName],[0,1],null,null,null,null,300);"
			onKeyUp="return showCodeListKey('SettleMgmtDivision',[this,OutManageComName],[0,1],null,null,null,null,300);"
			verify="出单机构|notnull&code:SettleMgmtDivision"><input
			class=codename name=OutManageComName readonly elementtype=nacessary
			onmouseover="showtitle(this);"></TD>
		<TD class=title>接单机构</TD>
		<TD class=input><input class=codeno name=GetManageCom
			onDblClick="return showCodeList('GetManageCom',[this,GetManageComName],[0,1],null,null,null,null,300);"
			onKeyUp="return showCodeListKey('GetManageCom',[this,GetManageComName],[0,1],null,null,null,null,300);"
			verify="接单机构|notnull&code:GetManageCom"><input class=codename
			name=GetManageComName readonly elementtype=nacessary
			onmouseover="showtitle(this);"></TD>
	</TR>
	<TR>
		<TD class=title>车牌号<font color=red size =2.5 >(新车录0)</TD>
		<TD class=input><Input class=common name=LicenseNo
			verify="车牌号|notnull&len<=10" elementtype=nacessary></TD>
		<TD class=title>是否见费出单</TD>
		<TD class=input><input class=codeno name=ComFirstFeeFlag
			onDblClick="return showCodeList('yesorno',[this,ComFirstFeeFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,ComFirstFeeFlagName],[0,1]);"
			verify="是否见费出单|code:yesorno&notnull"><input class=codename
			name=ComFirstFeeFlagName readonly elementtype=nacessary></TD>
	</TR>
	<TR>
		<TD class=title>业务性质</TD>
		<TD class=input><input class=codeno name=BussNature
			onDblClick="return showCodeList('BussNature2',[this,BussNatureName],[0,1]);"
			onKeyUp="return showCodeListKey('BussNature2',[this,BussNatureName],[0,1]);"
			verify="业务性质|code:BussNature2&notnull"><input class=codename
			name=BussNatureName readonly elementtype=nacessary></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</TR>
	<TR id="channel" style="display: 'none'">
		<TD class=title>渠道编码</TD>
		<TD class=input><Input class=common name=ChannelCode
			onblur="showChannelName()" verify="渠道编码|len<=20"> <input
			type="button" class=cssButtonLong id="butChannel" value="查  询"
			onClick="queryChannel();"></TD>
		<TD class=title>渠道名称</TD>
		<TD class=input><Input class=readonly name=ChannelName readonly></TD>
	</TR>
	<TR style="display: ''">
		<TD class=title>保险公司&nbsp;</TD>
		<TD class=input><input class=codeno name=SupplierShowCode
			onDblClick="return showSupplierClick(this,SupplierName,SupplierCode);"
			onKeyUp="return showSupplierKey(this,SupplierName,SupplierCode);"><input
			class=codename name=SupplierName readonly elementtype=nacessary
			onmouseover="showtitle(this);"></TD>
		<td class=title>保单号&nbsp;</td>
		<td class=input><input class=common name=ContNo
			verify="保单号|notnull&len<=40" elementtype=nacessary></td>
	</TR>
	<TR class=common>
		<TD class=title>签单日期</TD>
		<TD class=input><input name=UWDate value="<%=tCurrentDate%>"
			class=coolDatePicker dateFormat="short" verify="签单日期|notnull&date"
			elementtype=nacessary></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</TR>
	<TR class=common>
		<TD class=title>出单日期</TD>
		<TD class=input><input name=SignDate value="<%=tCurrentDate%>"
			class=coolDatePicker dateFormat="short" verify="出单日期|notnull&date"
			elementtype=nacessary></TD>
		<TD class=title>出单时间</TD>
		<TD class=input><input size=2 name=hour value="<%=tCurrentHour%>"
			verify="小时|NUM&len=2" elementtype=nacessary>&nbsp;时&nbsp;<input
			size=2 name=minute value="<%=tCurrentMinute%>" verify="分钟|NUM&len=2"
			elementtype=nacessary>&nbsp;分<Input name=Second type=hidden
			value="00"></TD>
	</TR>
	<TR class=common>
		<td class=title>保单生效日期&nbsp;</td>
		<td class=input><input name="CValiDate"
			onblur="showCInValiDate();" class=coolDatePicker dateFormat="short"
			verify="保单生效日期|notnull&date" elementtype=nacessary></td>
		<td class=title>保单有效日期至&nbsp;</td>
		<td class=input><input name="CInValiDate" class=coolDatePicker
			dateFormat="short" verify="保单有效日期至|notnull&date"
			elementtype=nacessary></td>
	</TR>
	<TR style="display: 'none'">
		<TD class=title>被保人上张保单保险公司&nbsp;</TD>
		<TD class=input><input class=codeno name=OldSupplierShowCode
			onDblClick="return showCodeList('AllInsCom',[this,OldSupplierName,OldSupplierCode],[0,1,2],null,null,null,null,300);"
			onKeyUp="return showCodeListKey('AllInsCom',[this,OldSupplierName,OldSupplierCode],[0,1,2],null,null,null,null,300);"
			verify="被保人上张保单保险公司|code:AllInsCom"><input class=codename
			name=OldSupplierName readonly onmouseover="showtitle(this);">
		</TD>
		<td class=title>被保人上张保单保单号&nbsp;</td>
		<td class=input><input class=common name=OldContNo
			verify="被保人上张保单保单号|len<=40"></td>
	</TR>

	<TR>
		<TD class=title>代理人编码</TD>
		<TD class=input><Input class=common name=AgentCode
			onblur="showAgentName();showKhJfFlag();" elementtype=nacessary><input
			type="button" class=cssButtonLong id="butAgent" value="查  询"
			onClick="queryAgent();"></TD>
		<TD class=title>代理人姓名</TD>
		<TD class=input><Input class=readonly name=AgentName readonly></TD>
	</TR>
	<TR>
		<TD class=title>是否参与内部考核积分</TD>
		<TD class=input><input class=codeno name=RightSendFlag
			onDblClick="return showCodeList('yesorno',[this,RightSendFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,RightSendFlagName],[0,1]);"
			verify="是否参与内部考核积分|code:yesorno&notnull"><input
			class=codename name=RightSendFlagName readonly elementtype=nacessary>
		</TD>
		<TD class=title>是否放弃并授权</TD>
		<TD class=input><input class=codeno name=AccreditFlag
			onDblClick="return showCodeList('yesorno',[this,AccreditFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,AccreditFlagName],[0,1]);"
			verify="是否放弃并授权|code:yesorno&notnull"><input class=codename
			name=AccreditFlagName readonly elementtype=nacessary></TD>
	</TR>
	<TR id="fullInput2" style="display: ''">
		<TD class=title>POS机交易号</TD>
		<TD class=input><Input class=common name=POSNo
			verify="POS机交易号|len<=20"></TD>
	</TR>
	<TR id="fullInput3" style="display: 'none'"></TR>
	<TR style="display: 'none'">
		<TD class=title>是否垫付佣金</TD>
		<TD class=input><input class=codeno name=PayPCommFlag
			onDblClick="return showCodeList('yesorno',[this,PayPCommFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,PayPCommFlagName],[0,1]);"
			verify="是否垫付佣金|code:yesorno"><input class=codename
			name=PayPCommFlagName readonly></TD>
		<TD class=title>垫付佣金金额</TD>
		<TD class=input><Input class=common name=PayPComm
			verify="垫付佣金金额|NUM&len<=10"></TD>
	</TR>

</table>
<div id="div4" style="display: ''">
<table class="common">

	<TR class=common>
		<td>&nbsp;</td>
	</TR>
	<TR class=common>
		<TD class=title>被保人类型&nbsp;</TD>
		<TD class=input><input class=codeno name=InsuredCustType
			onDblClick="return showCodeList('CustType',[this,InsuredCustTypeName],[0,1]);"
			onKeyUp="return showCodeListKey('CustType',[this,InsuredCustTypeName],[0,1]);"
			verify="被保人类型|notnull&code:CustType"><input class=codename
			name=InsuredCustTypeName readonly elementtype=nacessary><input
			type="button" class=cssButtonLong name="butQueryInsured" value="查  询"
			onClick="queryCustomer(3);"></TD>
		<TD class=title>被保人姓名&nbsp;</TD>
		<TD class=input><Input class=common name=InsuredName
			onblur="showCarOwnerName();" verify="被保人姓名|len<=60"
			elementtype=nacessary></TD>
	</TR>
	<TR id="fullInput4" style="display: ''">
		<td class=title>被保人证件类型</td>
		<TD class=input><Input class=codeno name=InsuredIDType
			ondblClick="showCodeList('IDType',[this,InsuredIDTypeName],[0,1],null,fm.InsuredCustType.value,fm.InsuredCustType.value,'1');"
			onkeyup="showCodeListKey('IDType',[this,InsuredIDTypeName],[0,1],null,fm.InsuredCustType.value,fm.InsuredCustType.value,'1');"
			verify="被保人证件类型|code:allidtype"><input class=codename
			name=InsuredIDTypeName readonly></TD>
		<td class=title>被保人证件号码</td>
		<td class=input><input class=common name=InsuredIDNo
			verify="被保人证件号码|len<=20"></td>
	</TR>
</table>
<div id="div5" style="display: ''">
<table class="common">
	<TR>
		<td class=title>地址</td>
		<td class=input><input class=common3 name=InsuredPostalAddress
			verify="地址|len<=40"></td>
		<td class=title>邮政编码</td>
		<td class=input><input class=common name=InsuredZipCode
			verify="邮政编码|ZIPCODE"></td>
	</TR>
	<TR id="fullInput6" style="display: ''">
		<td class=title>被保人手机</td>
		<td class=input><input class=common name=InsuredMobile
			verify="被保人手机|len<=15&TELEPHONE"></td>
		<td class=title>被保人小灵通</td>
		<td class=input><input class=common name=InsuredMobile2
			verify="被保人小灵通|len<=15&TELEPHONE"></td>
	</TR>
	<TR id="fullInput61" style="display: ''">
		<td class=title>被保人办公电话</td>
		<td class=input><input class=common name=InsuredPhone
			verify="被保人办公电话|len<=15&TELEPHONE"></td>
		<td class=title>被保人家庭电话</td>
		<td class=input><input class=common name=InsuredHomePhone
			verify="被保人家庭电话|len<=15&TELEPHONE"></td>
	</TR>
	<TR>
		<TD class=title>被保人会员类别</TD>
		<TD class=input><input class=codeno name=InsuredMemberType
			verify="被保人会员类别|code:MemberType"
			ondblClick="showCodeList('MemberType',[this,InsuredMemberTypeName],[0,1]);"
			onkeyup="showCodeListKey('MemberType',[this,InsuredMemberTypeName],[0,1]);"><input
			class=codename name=InsuredMemberTypeName readonly></td>
		<TD class=title></TD>
		<TD class=input></td>
	</TR>

</table>
</div>
<table class="common">
	<TR id="fullInput7" style="display: ''">
		<td><input type="checkbox" name="appnt" value=""
			onclick="showAppnt();">同被保人</td>
	</TR>
	<TR id="fullInput8" style="display: ''">
		<TD class=title>投保人类型&nbsp;</TD>
		<TD class=input><input class=codeno name=AppntCustType
			onDblClick="return showCodeList('CustType',[this,AppntCustTypeName],[0,1]);"
			onKeyUp="return showCodeListKey('CustType',[this,AppntCustTypeName],[0,1]);"
			verify="投保人类型|code:CustType"><input class=codename
			name=AppntCustTypeName readonly> <input type="button"
			class=cssButtonLong name="butAppntCus" value="查  询"
			onClick="queryCustomer(2);"></TD>
		<TD class=title>投保人姓名&nbsp;</TD>
		<TD class=input><Input class=common name=AppntName
			verify="投保人姓名|len<=60"></TD>
	</TR>
	<TR id="fullInput9" style="display: ''">
		<td class=title>投保人证件类型</td>
		<TD class=input><Input class=codeno name=AppntIDType
			ondblClick="showCodeList('IDType',[this,AppntIDTypeName],[0,1],null,fm.AppntCustType.value,fm.AppntCustType.value,'1');"
			onkeyup="showCodeListKey('IDType',[this,AppntIDTypeName],[0,1],null,fm.AppntCustType.value,fm.AppntCustType.value,'1');"
			verify="投保人证件类型|code:allidtype"><input class=codename
			name=AppntIDTypeName readonly></TD>
		<td class=title>投保人证件号码</td>
		<td class=input><input class=common name=AppntIDNo
			verify="投保人证件号码|len<=20"></td>
	</TR>
</table>
<div id="div6" style="display: ''">
<table class="common">
	<TR id="fullInput10" style="display: ''">
		<td class=title>地址</td>
		<td class=input><input class=common3 name=AppntPostalAddress
			verify="投保人地址|len<=40"></td>
		<td class=title>邮政编码</td>
		<td class=input><input class=common name=AppntZipCode
			verify="投保人邮政编码|ZIPCODE"></td>
	</TR>
	<TR id="fullInput6" style="display: ''">
		<td class=title>投保人手机</td>
		<td class=input><input class=common name=AppntMobile
			verify="投保人手机|len<=15&TELEPHONE"></td>
		<td class=title>投保人小灵通</td>
		<td class=input><input class=common name=AppntMobile2
			verify="投保人小灵通|len<=15&TELEPHONE"></td>
	</TR>
	<TR id="fullInput61" style="display: ''">
		<td class=title>投保人办公电话</td>
		<td class=input><input class=common name=AppntPhone
			verify="投保人办公电话|len<=15&TELEPHONE"></td>
		<td class=title>投保人家庭电话</td>
		<td class=input><input class=common name=AppntHomePhone
			verify="投保人家庭电话|len<=15&TELEPHONE"></td>
	</TR>
	<TR>
		<TD class=title>投保人会员类别</TD>
		<TD class=input><input class=codeno name=AppntMemberType
			verify="投保人会员类别|code:MemberType"
			ondblClick="showCodeList('MemberType',[this,AppntMemberTypeName],[0,1]);"
			onkeyup="showCodeListKey('MemberType',[this,AppntMemberTypeName],[0,1]);"><input
			class=codename name=AppntMemberTypeName readonly></td>
		<TD class=title></TD>
		<TD class=input></td>
	</TR>
</table>
</div>
<table class="common">
	<TR>
		<td><input type="checkbox" name="handling" value=""
			onclick="showHandling();">同被保人</td>
	</TR>
	<TR>
		<TD class=title>经办人姓名&nbsp;</TD>
		<TD class=input><Input class=common name=HandlingName
			verify="经办人姓名|len<=60"><input type="button"
			class=cssButtonLong name="butHandlingCus" value="查  询"
			onClick="queryCustomer(1);"></TD>
		<td class=title></td>
		<td class=input></td>
	</TR>
	<TR>
		<td class=title>经办人证件类型</td>
		<TD class=input><Input class=codeno name=HandlingIDType
			ondblClick="showCodeList('IDType',[this,HandlingIDTypeName],[0,1],null,'1','1','1');"
			onkeyup="showCodeListKey('IDType',[this,HandlingIDTypeName],[0,1],null,'1','1','1');"
			verify="经办人证件类型|code:allidtype"><input class=codename
			name=HandlingIDTypeName readonly></TD>
		<td class=title>经办人证件号码</td>
		<td class=input><input class=common name=HandlingIDNo
			verify="经办人证件号码|len<=20"></td>
	</TR>
</table>
<div id="div7" style="display: ''">
<table class="common">
	<TR>
		<td class=title>地址</td>
		<td class=input><input class=common3 name=HandlingPostalAddress
			verify="经办人地址|len<=40"></td>
		<td class=title>邮政编码</td>
		<td class=input><input class=common name=HandlingZipCode
			verify="经办人邮政编码|ZIPCODE"></td>
	</TR>
	<TR id="fullInput6" style="display: ''">
		<td class=title>经办人手机</td>
		<td class=input><input class=common name=HandlingMobile
			verify="经办人手机|len<=15&TELEPHONE"></td>
		<td class=title>经办人小灵通</td>
		<td class=input><input class=common name=HandlingMobile2
			verify="经办人小灵通|len<=15&TELEPHONE"></td>
	</TR>
	<TR id="fullInput61" style="display: ''">
		<td class=title>经办人办公电话</td>
		<td class=input><input class=common name=HandlingPhone
			verify="经办人办公电话|len<=15&TELEPHONE"></td>
		<td class=title>经办人家庭电话</td>
		<td class=input><input class=common name=HandlingHomePhone
			verify="经办人家庭电话|len<=15&TELEPHONE"></td>
	</TR>
	<TR>
		<TD class=title>经办人会员类别</TD>
		<TD class=input><input class=codeno name=HandlingMemberType
			verify="经办人会员类别|code:MemberType"
			ondblClick="showCodeList('MemberType',[this,HandlingMemberTypeName],[0,1]);"
			onkeyup="showCodeListKey('MemberType',[this,HandlingMemberTypeName],[0,1]);"><input
			class=codename name=HandlingMemberTypeName readonly></td>
		<TD class=title></TD>
		<TD class=input></td>
	</TR>

</table>
</div>
</div>
<br>
<table class="common">
	<tr style="display: 'none'">
		<TD class=title>代理人所在团队&nbsp;</TD>
		<TD class=input><input class=codeno name=AgentGroup
			onDblClick="return showCodeList('AgentToGroup',[this,AgentGroupName],[0,1],null,1,fm.AgentCode.value,null,300);"
			onKeyUp="return showCodeListKey('AgentToGroup',[this,AgentGroupName],[0,1],null,1,fm.AgentCode.value,null,300);"><input
			class=codename name=AgentGroupName readonly elementtype=nacessary
			onmouseover="showtitle(this);"></TD>
		<td class=title></td>
		<td class=input></td>
	</tr>
	<TR>
		<TD class=title>出单员编码</TD>
		<TD class=input><Input class=common name=SignMan
			onblur="showSignManName();" value="<%=tOperator%>"></TD>
		<TD class=title>出单员姓名</TD>
		<TD class=input><Input class=common name=SignManName></TD>
	</TR>
	<TR>
		<TD class=title>录单员编码</TD>
		<TD class=input><Input class=readonly name=ReceiveOperator
			value="<%=tOperator%>" readonly></TD>
		<TD class=title>录单员姓名</TD>
		<TD class=input><Input class=readonly name=ReceiveOperatorName
			readonly></TD>
	</TR>
	<TR>
		<TD class=title>录单日期</TD>
		<TD class=input><input class=readonly name=ReceiveDate
			value="<%=tCurrentDate%>" readonly></TD>
		<TD class=title>录单时间</TD>
		<TD class=input><Input class=readonly name=ReceiveTime
			value="<%=tCurrentHour%>:<%=tCurrentMinute%>:<%=tCurrentSecond%>"
			readonly></TD>
	</TR>
	<TR class=common>
		<td class=title></td>
		<td class=input></td>
		<td class=title></td>
		<td class=input></td>
	</TR>
	<TR class=common>
		<TD class=title>保单特约</TD>
		<TD colspan="3" class=input><textarea name="PolicyPli"
			class="common"></textarea> <input type=hidden name=hidePolicyPli>
		</TD>
	</TR>
	<TR class=common id="FullRemark" style="display: ''">
		<TD class=title>备注</TD>
		<TD colspan="3" class=input><textarea name="Remark"
			class="common"></textarea> <input type=hidden name=hideRemark>
		</TD>
	</TR>
</TABLE>
</Div>
</div>

<div id="div8" style="display: ''">
<table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divCarInfo);">
		车辆信息&nbsp;&nbsp;&nbsp;</TD>
	</TR>
</TABLE>
<Div id="divCarInfo" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title>车主</TD>
		<TD class=input><Input class=common name=CarOwner
			verify="车主|len<=30"><input type="button" class=cssButtonLong
			name="butCarInfo" value="车辆查询" onClick="queryCarInfo();"></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</TR>
	<TR class=common>
		<TD class=title>车辆种类</TD>
		<TD class=input><Input class=codeno name=CarKindCode
			verify="车辆种类|code:carkind"
			ondblClick="showCodeList('carkind',[this,CarKindName],[0,1]);"
			onkeyup="showCodeListKey('carkind',[this,CarKindName],[0,1]);"><input
			class=codename name=CarKindName readonly></TD>
		<TD class=title>使用性质</TD>
		<TD class=input><Input class=codeno name=UseNatureCode
			verify="使用性质|notnull&code:carusages"
			ondblClick="showCodeList('carusages',[this,UseNatureName],[0,1],null,'1','OtherSign');"
			onkeyup="showCodeListKey('carusages',[this,UseNatureName],[0,1],null,'1','OtherSign');"><input
			class=codename name=UseNatureName readonly elementtype=nacessary>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>车架号</TD>
		<TD class=input><Input class=common name=FrameNo
			verify="车架号|len<=30"></TD>
		<TD class=title>发动机号</TD>
		<TD class=input><Input class=common name=EngineNo
			verify="发动机号|len<=30"></TD>
	</TR>
	<TR class=common>
		<TD class=title>厂牌车型(车系+车型)</TD>
		<TD class=input><Input class=common name=ModelCode
			verify="厂牌车型|len<=50"></TD>
		<TD class=title>年审到期日</TD>
		<TD class=input><input name="YearExamDate" class=coolDatePicker
			dateFormat="short" verify="年审到期日|Date"></TD>
	</TR>
	<TR>
		<TD class=title>初次登记日期</TD>
		<TD class=input><input name="EnrollDate" class=coolDatePicker
			dateFormat="short" verify="初次登记日期|Date"></TD>
		<TD class=title>购置日期</TD>
		<TD class=input><input name="BuyDate" class=coolDatePicker
			dateFormat="short" verify="购置日期|Date"></TD>
	</TR>
	<TR id="fullInput12" style="display: ''">
		<TD class=title>VIN码</TD>
		<TD class=input><Input class=common name=VINNo
			verify="VIN码|len=17"></TD>
		<TD class=title>行驶区域</TD>
		<TD class=input><Input class=common name=RunAreaName
			verify="行驶区域|len<=30"></TD>
	</TR>
	<TR id="fullInput13" style="display: ''">
		<TD class=title>是否港澳车标志</TD>
		<TD class=input><Input class=codeno name=HKFlag
			verify="是否港澳车标志|code:yesorno"
			ondblClick="showCodeList('yesorno',[this,HKFlagName],[0,1]);"
			onkeyup="showCodeListKey('yesorno',[this,HKFlagName],[0,1]);"><input
			class=codename name=HKFlagName readonly></TD>
		<TD class=title>港澳车牌号码</TD>
		<TD class=input><Input class=common name=HKLicenseNo
			verify="港澳车牌号码|len<=10"></TD>
	</TR>
	<TR id="fullInput14" style="display: ''">
		<TD class=title>使用年限</TD>
		<TD class=input><Input class=common name=UseYears
			verify="使用年限|len<=12&num"></TD>
		<TD class=title>行驶里程(公里)</TD>
		<TD class=input><Input class=common name=RunMiles
			verify="行驶里程|len<=14&num"></TD>
	</TR>
	<TR id="fullInput15" style="display: ''">
		<TD class=title>国别性质</TD>
		<TD class=input><Input class=codeno name=CountryNature
			verify="国别性质|code:countrynature&notnull"
			ondblClick="showCodeList('countrynature',[this,CountryNatureName],[0,1]);"
			onkeyup="showCodeListKey('countrynature',[this,CountryNatureName],[0,1]);"><input
			class=codename name=CountryNatureName readonly></TD>
		<TD class=title>生产国家</TD>
		<TD class=input><Input class=codeno name=CountryCode1
			verify="生产国家|code:country&notnull"
			ondblClick="showCodeList('country',[this,CountryCode1Name],[0,1]);"
			onkeyup="showCodeListKey('country',[this,CountryCode1Name],[0,1]);"><input
			class=codename name=CountryCode1Name readonly></TD>
	</TR>
	<TR id="fullInput16" style="display: ''">
		<TD class=title>车牌底色</TD>
		<TD class=input><Input class=codeno name=LicenseColorCode
			verify="车牌底色|code:licensecolor&notnull"
			ondblClick="showCodeList('licensecolor',[this,LicenseColorName],[0,1]);"
			onkeyup="showCodeListKey('licensecolor',[this,LicenseColorName],[0,1]);"><input
			class=codename name=LicenseColorName readonly></TD>
		<TD class=title>座位数</TD>
		<TD class=input><Input class=common name=SeatCount1
			verify="座位数|len<=8&int"></TD>
	</TR>
	<TR id="fullInput17" style="display: ''">
		<TD class=title>吨位数</TD>
		<TD class=input><Input class=common name=TonCount
			verify="吨位数|len<=8&num"></TD>
		<TD class=title>排量</TD>
		<TD class=input><Input class=common name=ExhaustScale
			verify="排量|len<=4&num"></TD>
	</TR>
	<TR id="fullInput18" style="display: ''">
		<TD class=title>车身颜色</TD>
		<TD class=input><Input class=codeno name=ColorCode
			verify="车身颜色|code:carcolor&notnull"
			ondblClick="showCodeList('carcolor',[this,ColorName],[0,1]);"
			onkeyup="showCodeListKey('carcolor',[this,ColorName],[0,1]);"><input
			class=codename name=ColorName readonly></TD>
		<TD class=title>安全配置</TD>
		<TD class=input><Input class=common name=SafeDevice
			verify="安全配置|len<=15"></TD>
	</TR>
	<TR id="fullInput19" style="display: ''">
		<TD class=title>固定停放地点</TD>
		<TD class=input><Input class=common name=ParkSite
			verify="固定停放地点|len<=15"></TD>
		<TD class=title>购车人地址</TD>
		<TD class=input><Input class=common name=OwnerAddress
			verify="购车人地址|len<=20"></TD>
	</TR>
	<TR id="fullInput20" style="display: ''">
		<TD class=title>生产日期</TD>
		<TD class=input><input name="ProduceDate" class=coolDatePicker
			dateFormat="short" verify="生产日期|Date"></TD>
		<TD class=title>购车用途</TD>
		<TD class=input><Input class=common name=CarUsage
			verify="购车用途|len<=10"></TD>
	</TR>
	<TR id="fullInput21" style="display: ''">
		<TD class=title>新车购置价格(万元)</TD>
		<TD class=input><Input class=common name=PurchasePrice
			verify="新车购置价格(万元)|notnull&Money:8-4&value>=0&value<=1000"
			elementtype=nacessary></TD>
		<TD class=title>购车发票号</TD>
		<TD class=input><Input class=common name=InvoiceNo
			verify="购车发票号|len<=10"></TD>
	</TR>
	<TR id="fullInput22" style="display: ''">
		<TD class=title>验车人</TD>
		<TD class=input><Input class=common name=CarChecker
			verify="验车人|notnull&len<=30"></TD>
		<TD class=title>验车日期</TD>
		<TD class=input><input name="CarCheckTime" class=coolDatePicker
			dateFormat="short" verify="验车日期|Date"></TD>
	</TR>
	<TR id="fullInput23" style="display: ''">
		<TD class=title>验车情况</TD>
		<TD class=input><Input class=codeno name=CarCheckStatus
			verify="验车情况|code:carcheck&notnull"
			ondblClick="showCodeList('carcheck',[this,CarCheckStatusName],[0,1]);"
			onkeyup="showCodeListKey('carcheck',[this,CarCheckStatusName],[0,1]);"><input
			class=codename name=CarCheckStatusName readonly></TD>
		<TD class=title>经销商名称</TD>
		<TD class=input><Input class=common name=CarDealerName
			verify="经销商名称|notnull&len<=60"></TD>
	</TR>
</table>
</DIV>
</div>
<%@include file="FBProjRiskIns.jsp"%>
<div id="div9" style="display: ''">
<Table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divRiskDetail);">
		险种信息</TD>
	</TR>
</Table>

<Div id="divRiskDetail" style="display: ''">
<TABLE class="common">
	<TR class=common>
		<TD class=title>险种编码</TD>
		<TD class=input><input class="codeno" name=OutRiskCode
			verify="险种编码|notnull"
			ondblclick="showRiskClick(this,RiskName,RiskCode,KindCode,RuleID);"
			onKeyUp="showRiskKey(this,RiskName,RiskCode,KindCode,RuleID);"><input
			class=codename name=RiskName elementtype=nacessary readonly
			onmouseover="showtitle(this);"></TD>
		<TD class=title>总保费&nbsp;</TD>
		<TD class=input><Input class=common name=SumPrem
			verify="总保费|notnull&NUM&value>=0&Money:15-2" elementtype=nacessary></TD>
	</TR>
	<TR id="tax" style="display: ''">
		<TD class=title>车船税</TD>
		<TD class=input><input class=common name=tax1
			verify="车船税|NUM&value>=0&Money:15-2"></TD>
		<TD class=title>印花税</TD>
		<TD class=input><input class=common name=tax2
			verify="印花税|NUM&value>=0&Money:15-2"></TD>
	</TR>
	<TR class=common>
		<TD class=title>费率等级</TD>
		<TD class=input><input class="codeno" name=FYCType
			verify="费率等级|NOTNULL"
			ondblclick="showFYCTypeClick(this,FYCTypeName,FYCTypeReMark);"
			onKeyUp="showFYCTypeKey(this,FYCTypeName,FYCTypeReMark);"><input
			class=codename name=FYCTypeName readonly onmouseover="showtitleHidden(this , fm.FYCTypeReMark.value);"></TD>
		<TD class=title>费率等级描述</TD>
		<TD class=input><input class=readonly name=FYCTypeReMark></TD>
	</TR>
</table>
</Div>
</div>

<div id="div10" style="display: ''">
<div id="divKindInfo" style="display: ''">
<TABLE class="common">
	<TR class=common>
		<TD height="23" colspan="4" class=titleImg><IMG
			src="../common/images/butExpand.gif" style="cursor: hand;"
			OnClick="showPage(this,divKindGrid);"> 险别信息</TD>
	</TR>
</TABLE>

<Div id="divKindGrid" style="display: ''">
<table id=mytable1 class=common>
	<tr>
		<td text-align:left colSpan=1><span id="spanKindGrid"></span></td>
	</tr>
</table>
</Div>
</Div>
</Div>
<input name="Policy" type=hidden> <input name="hideOperate"
	type=hidden> <input name="FullFlag" type=hidden value="false">
<Input name="InnerContNo" type=hidden> <Input
	name="SupplierCode" type=hidden> <Input name="OldSupplierCode"
	type=hidden> <Input name="ProtocolNo" type=hidden> <Input
	name="Corporation" type=hidden> <input name="RiskCode"
	type=hidden> <input name="KindCode" type=hidden> <input
	name="RiskCons" type=hidden> <input name="FYCCons" type=hidden>
<input name="AgentFlag" type=hidden> <input name="SignTime"
	type=hidden> <input name="PolNo" type=hidden> <input
	name="ItemType" type=hidden> <input name="ItemNo" type=hidden>
<input name="ItemCode" type=hidden value="06"> <input
	name="ItemName" type=hidden value="车辆"> <input name="AppntNo"
	type=hidden> <input name="InsuredNo" type=hidden> <input
	name="HandlingNo" type=hidden> <input name="AppntAddressNo"
	type=hidden> <input name="InsuredAddressNo" type=hidden>
<input name="HandlingAddressNo" type=hidden> <input
	name="AgentRate" type=hidden value="100"> <input
	name="BrokerRate" type=hidden value="100"> <input
	name="AgentGrade" type=hidden value="#"> <input
	name="PKProtocolNo" type=hidden> <input name="TimePrem"
	type=hidden> <input name="hideCInValiDate" type=hidden>
<input name="FirstFeeFlag" type=hidden> <input
	name="FirstFeeFlagName" type=hidden> <input name="AgentFlag"
	type=hidden> <input name="PayIntv" type=hidden value="0">
<input name="InputType" type=hidden value="<%=tInputType%>"> <input
	name="AgentScore" type=hidden>
	<input name="RuleID" type=hidden> 
	<input name="IsSingle1" type=hidden> 
	<br>

<Div id="divCheckBox" style="display: ''">
<TABLE class="common">
	<TR class=common align="left">
		<TD>
		<div id="div12" style="display: ''"><INPUT TYPE=checkBox
			NAME=CopyFlag id=CopyFlag>复制保单</input></div>
		</TD>
		<TD>&nbsp;</TD>
		<TD><INPUT TYPE=checkBox NAME=ISSingle id=ISSingle   onClick="isSingle()">是否单出</input></TD>
		<TD width="70%">&nbsp;</TD>
	</TR>
</TABLE>
</Div>

<div id="div13" style="display: ''" align=center>
<table>
	<Input VALUE="保  存" onclick="saveContClick()" type=button
		class=cssButton>
	<input value="保单录入完毕" onClick="confirmClick();" id="butEnd"
		type="button" class=cssButtonLong>
</table>
</div>
<div id="div14" style="display: 'none'" align="center"><br>
<input value="关  闭" onClick="top.close()" type="button"
	class=cssButtonLong></div>
</form>
<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
<script LANGUAGE="JavaScript">
function document.onkeydown()
{
	if(event.keyCode==13)
	{
		if(window.document.activeElement.name=="LicenseNo")
		{
			getCont();
		}
		if(window.document.activeElement.name=="ContNo")
		{
			getPrintCont();
		}
	}
}
</script>
