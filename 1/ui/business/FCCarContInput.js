var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var mRiskCode="";

window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)   
  {
    try
    {
      showInfo.focus();   
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function isSingle()
{ 
	if(fm.ISSingle.checked)
	{
		fm.IsSingle1.value="Y";
	}
	else{ 
		fm.IsSingle1.value="N";
		
	} 
}


function isTrueName(s){
	if(!NullOrBlank(s)){
		var patrn=/^[a-zA-Z0-9\u4E00-\u9FA5]+$/;
		if (!patrn.exec(s)){
			return false
		}
	}
	return true;
}

function isTrueNo(s){
	if(!NullOrBlank(s)){
		var patrn=/^[a-zA-Z0-9]+(\_([a-zA-Z0-9])+)*$/;
		if (!patrn.exec(s)){
			return false
		}
	}
	return true;
}

function IsChar(s)
{ 
 var Number = "0123456789.abcdefghijklmnopqrstuvwxyz-\/ABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@#$%^&*()_公安盐";
 for (i = 0; i < s.length;i++)
    {
        var c = s.charAt(i);
        if (Number.indexOf(c) == -1) return false;
    }
 return true
}



//保单信息查询
function queryCont()
{
	openWindow("./FCPropContQueryMain.jsp?tPageFrom=fast");
}

//保单信息查询
function queryCopyCont()
{
  	showInfo = openWindow("./FCPropContQueryMain.jsp?tPageFrom=copy");
}

function afterQuery(tInnerContNo)
{
	initForm();
	initKindGrid();
	
	fm.InnerContNo.value=tInnerContNo;
	
	//查询保单信息
	var sql="select a.PolicyPrtNo,a.InvoicePrtNo,a.InsCardPrtNo,a.CInsFlagPrtNo,a.OutManageCom,a.GetManageCom,"
	+" a.BussNature,a.ComFirstFeeFlag,a.SupplierCode,a.ContNo,a.SignDate,a.SignTime,a.UWDate,a.CValiDate,a.CInValiDate,"
	+" a.InsuredNo,a.InsuredCustType,a.InsuredName,a.AgentCode,a.AgentGroup,a.PolicyPli,a.InnerContNo,"
	+" a.ProtocolNo,a.Corporation,a.RightSendFlag,a.SignMan,a.ChannelCode,a.ComFirstFeeFlag,a.POSNo,a.AccreditFlag,"
	+" a.InsuredIDType,a.InsuredIDNo,a.ReceiveOperator,a.ReceiveDate,a.ReceiveTime,a.ProjCode "
	+" ,(select shortname from FDCom where comcode=a.OutManageCom),"
	+" (select shortname from FDCom where comcode=a.GetManageCom),"
	+" (select CodeName from FDCode where CodeType='bussnature2' and code=a.BussNature),"
	+" (select codename from fdcode where codetype='yesorno' and code=a.ComFirstFeeFlag),"
	+" (select shortname from fdinscom where suppliercode=a.SupplierCode),"
	+" (select SupplierShowCode from fdinscom where SupplierCode=a.SupplierCode),"
	+" (select CodeName from FDCode where CodeType='custtype' and Code=a.InsuredCustType),"
	+" (select Name from FAAgentTree where AgentCode = a.AgentCode and Corporation = a.Corporation),"
	+" (select shortname from FDCom where comcode=a.AgentGroup),"
	+" (select CodeName from FDCode where CodeType='yesorno' and code=a.RightSendFlag),"
	+" (select UserName from FDUser where UserCode =a.SignMan),"
	+" (select UserName from FDUser where UserCode =a.ReceiveOperator),"
	+" (select CodeName from FDCode where CodeType='yesorno' and code=a.AccreditFlag),"
	+" (select codename from fdcode where codetype='idtype' and code=a.InsuredIDType),"
	+" a.SignManName,a.Remark"
	+" from FCCont a where a.InnerContNo='"+tInnerContNo+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	//是否有保单
	if(resultArr!=null)
	{
		fm.PolicyPrtNo.value=resultArr[0][0];
		fm.InvoicePrtNo.value=resultArr[0][1];
		fm.InsCardPrtNo.value=resultArr[0][2];
		fm.CInsFlagPrtNo.value=resultArr[0][3];
		fm.OutManageCom.value=resultArr[0][4]; 
		//fm.OutManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][4]);
		fm.OutManageComName.value = resultArr[0][36]; 
		
		fm.GetManageCom.value=resultArr[0][5];
		//fm.GetManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][5]);
		fm.GetManageComName.value = resultArr[0][37];
		
		fm.BussNature.value=resultArr[0][6];
		//fm.BussNatureName.value=getNameFromLDCode("BussNature2",resultArr[0][6]);
		fm.BussNatureName.value=resultArr[0][38];
		
		fm.ComFirstFeeFlag.value=resultArr[0][7];
		//fm.ComFirstFeeFlagName.value=getNameFromLDCode("yesorno",resultArr[0][7]);
		fm.ComFirstFeeFlagName.value = resultArr[0][39];
		
		fm.SupplierCode.value=resultArr[0][8];
		//fm.SupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",resultArr[0][8]);
		fm.SupplierName.value=resultArr[0][40];
		
		//fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",resultArr[0][8]);
		fm.SupplierShowCode.value=resultArr[0][41];
		
		fm.ContNo.value=resultArr[0][9];
		fm.SignDate.value=resultArr[0][10];
		fm.SignTime.value=resultArr[0][11];
		fm.hour.value = (fm.SignTime.value).substring(0,2);
		fm.minute.value = (fm.SignTime.value).substring(3,5);
		fm.UWDate.value=resultArr[0][12];
		fm.CValiDate.value=resultArr[0][13];
		showCInValiDate2(resultArr[0][14]);
		fm.InsuredNo.value=resultArr[0][15];
		fm.InsuredCustType.value=resultArr[0][16];
		//fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",resultArr[0][16]);
		fm.InsuredCustTypeName.value=resultArr[0][42];
		
		fm.InsuredName.value=resultArr[0][17];
		fm.AgentCode.value=resultArr[0][18];
		//fm.AgentName.value=getNameByCode("Name","FAAgent","AgentCode",resultArr[0][18]);
		fm.AgentName.value=resultArr[0][43];
		
		fm.AgentGroup.value=resultArr[0][19];
		//fm.AgentGroupName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][19]);
		fm.AgentGroupName.value=resultArr[0][44];
		
		fm.PolicyPli.value=unescapes(resultArr[0][20]);
		fm.InnerContNo.value=resultArr[0][21];
		//fm.ProtocolNo.value=resultArr[0][22];
		//fm.PKProtocolNo.value=resultArr[0][22];
		fm.Corporation.value=resultArr[0][23];
		fm.RightSendFlag.value=resultArr[0][24];
		//fm.RightSendFlagName.value=getNameFromLDCode("yesorno",resultArr[0][24]);
		fm.RightSendFlagName.value=resultArr[0][45];
		
		fm.SignMan.value=resultArr[0][25];
		//fm.SignManName.value=getNameByCode("UserName","FDUser","UserCode",resultArr[0][25]);
		fm.SignManName.value=resultArr[0][46];
		
		if(NullOrBlank(fm.SignMan.value))
		{
			fm.SignManName.value=resultArr[0][50];
		}
		
		fm.Remark.value=unescapes(resultArr[0][51]);
		
		fm.ReceiveOperator.value = resultArr[0][32];
		//fm.ReceiveOperatorName.value = getNameByCode("UserName","FDUser","UserCode",resultArr[0][32]);
		fm.ReceiveOperatorName.value =resultArr[0][47];
		
		fm.ReceiveDate.value = resultArr[0][33];
		fm.ReceiveTime.value = resultArr[0][34];
		
		queryFCProjRiskIns(resultArr[0][35]);
		
		fm.POSNo.value=resultArr[0][28];
		
		fm.AccreditFlag.value=resultArr[0][29];
		//fm.AccreditFlagName.value=getNameFromLDCode("yesorno",resultArr[0][29]);
		fm.AccreditFlagName.value=resultArr[0][48];
		
		fm.InsuredIDType.value=resultArr[0][30];
		//fm.InsuredIDTypeName.value=getNameFromLDCode("idtype",resultArr[0][30]);
		fm.InsuredIDTypeName.value=resultArr[0][49];
		fm.InsuredIDNo.value=resultArr[0][31];
		
		if(fm.BussNature.value=="02")
		{
			channel.style.display = "";
			fm.ChannelCode.value=resultArr[0][26];
			fm.ChannelName.value=getNameByCode("RepairShopName","FDRepairShop","RepairShopCode",resultArr[0][26]);
		}
		else
		{
			channel.style.display = "none";
			fm.ChannelCode.value="";
			fm.ChannelName.value="";
		}
		
		//mProtocolNo = fm.ProtocolNo.value;
		//mAgentGroup = fm.AgentGroup.value;
				
	    getCarByInnerContNo(tInnerContNo);
		queryRisk(tInnerContNo);
	}
}

function afterQueryCopy(tInnerContNo)
{
	initForm();
	initKindGrid();
	
	//查询保单信息
	var sql="select a.PolicyPrtNo,a.InvoicePrtNo,a.InsCardPrtNo,a.CInsFlagPrtNo,a.OutManageCom,a.GetManageCom,"
	+" a.BussNature,a.ComFirstFeeFlag,a.SupplierCode,a.ContNo,a.SignDate,a.SignTime,a.UWDate,a.CValiDate,a.CInValiDate,"
	+" a.InsuredNo,a.InsuredCustType,a.InsuredName,a.AgentCode,a.AgentGroup,a.PolicyPli,a.InnerContNo,"
	+" a.ProtocolNo,a.Corporation,a.POSNo,a.AccreditFlag"
	+" from FCCont a where a.InnerContNo='"+tInnerContNo+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	//是否有保单
	if(resultArr!=null)
	{
		//fm.PolicyPrtNo.value=resultArr[0][0];
		//fm.InvoicePrtNo.value=resultArr[0][1];
		//fm.InsCardPrtNo.value=resultArr[0][2];
		//fm.CInsFlagPrtNo.value=resultArr[0][3];
		fm.OutManageCom.value=resultArr[0][4]; 
		fm.OutManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][4]);
		fm.GetManageCom.value=resultArr[0][5];
		fm.GetManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][5]);
		fm.BussNature.value=resultArr[0][6];
		fm.BussNatureName.value=getNameFromLDCode("BussNature2",resultArr[0][6]);
		fm.ComFirstFeeFlag.value=resultArr[0][7];
		fm.ComFirstFeeFlagName.value=getNameFromLDCode("yesorno",resultArr[0][7]);
		fm.SupplierCode.value=resultArr[0][8];
		fm.SupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",resultArr[0][8]);
		fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",resultArr[0][8]);
		//fm.ContNo.value=resultArr[0][9];
		fm.SignDate.value=resultArr[0][10];
		fm.SignTime.value=resultArr[0][11];
		fm.hour.value = (fm.SignTime.value).substring(0,2);
		fm.minute.value = (fm.SignTime.value).substring(3,5);
		fm.UWDate.value=resultArr[0][12];
		fm.CValiDate.value=resultArr[0][13];
		showCInValiDate2(resultArr[0][14]);
		fm.InsuredNo.value=resultArr[0][15];
		fm.InsuredCustType.value=resultArr[0][16];
		fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",resultArr[0][16]);
		fm.InsuredName.value=resultArr[0][17];
		fm.AgentCode.value=resultArr[0][18];
		fm.AgentName.value=getNameByCode("Name","FAAgentTree","AgentCode",resultArr[0][18]);
		fm.AgentGroup.value=resultArr[0][19];
		fm.AgentGroupName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][19]);
		fm.PolicyPli.value=unescapes(resultArr[0][20]);
		//fm.InnerContNo.value=resultArr[0][21];
		//fm.ProtocolNo.value=resultArr[0][22];
		//fm.PKProtocolNo.value=resultArr[0][22];
		fm.Corporation.value=resultArr[0][23];
		fm.POSNo.value=resultArr[0][24];
		
		fm.AccreditFlag.value=resultArr[0][25];
		fm.AccreditFlagName.value=getNameFromLDCode("yesorno",resultArr[0][25]);
		
		//mProtocolNo = fm.ProtocolNo.value;
		//mAgentGroup = fm.AgentGroup.value;
	
	    getCarByInnerContNo(tInnerContNo);
		queryOldRisk(tInnerContNo);
		
	}
}

 function queryRisk(tInnerContNo)
{
	var sql="select a.PolNo,a.KindCode,a.RiskCode,a.OutRiskCode,"
	+" (select RiskShortName from FMRisk where RiskCode=a.RiskCode),"
	+" a.PayIntv,a.SumPrem,a.tax1,a.tax2,a.FYCType,"
	+" (select GradeName from FMProtocolgrade where indexcode=a.FYCType),"
	+" (select Remark from FMProtocolgrade where indexcode=a.FYCType),a.ISSINGLE "
	+" from FCPol a"
	+" where a.InnerContNo='"+tInnerContNo+"' "; 		
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.PolNo.value = resultArr[0][0];
		fm.KindCode.value = resultArr[0][1];
		fm.RiskCode.value = resultArr[0][2];
		mRiskCode = fm.RiskCode.value;
		fm.OutRiskCode.value = resultArr[0][3];
		fm.RiskName.value = resultArr[0][4];
		fm.PayIntv.value = resultArr[0][5];
		fm.SumPrem.value = resultArr[0][6];
		fm.tax1.value = resultArr[0][7];
		fm.tax2.value = resultArr[0][8];
		fm.FYCType.value = resultArr[0][9];
		fm.FYCTypeName.value = resultArr[0][10];
		fm.FYCTypeReMark.value = resultArr[0][11];
		fm.IsSingle1.value = resultArr[0][12];
		if(fm.IsSingle1.value=="Y"){
			fm.ISSingle.checked = true;
		}else{
			fm.ISSingle.checked = false;
		}
	    
	}
	showKindList(tInnerContNo);
}

function queryOldRisk(tInnerContNo)
{
	var sql="select a.PolNo,a.KindCode,a.RiskCode,a.OutRiskCode,"
	+" (select RiskShortName from FMRisk where RiskCode=a.RiskCode),"
	+" a.PayIntv,a.SumPrem,a.tax1,a.tax2,a.FYCType,"
	+" (select GradeName from FMProtocolgrade where indexcode=a.FYCType)"
	+" from FCPol a"
	+" where a.InnerContNo='"+tInnerContNo+"' "; 		
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.PolNo.value = resultArr[0][0];
		fm.KindCode.value = resultArr[0][1];
		fm.RiskCode.value = resultArr[0][2];
		mRiskCode = fm.RiskCode.value;
		fm.OutRiskCode.value = resultArr[0][3];
		fm.RiskName.value = resultArr[0][4];
		fm.PayIntv.value = resultArr[0][5];
		fm.SumPrem.value = resultArr[0][6];
		fm.tax1.value = resultArr[0][7];
		fm.tax2.value = resultArr[0][8];
		//fm.FYCType.value = resultArr[0][9];
		//fm.FYCTypeName.value = resultArr[0][10];
	}
	
	showKindList(tInnerContNo);
}

function submitContForm()
{
//	var i = 0;
//	var showStr="正在查询数据，请您稍候";
//	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;  
	
	
	//showInfo=window.showModelessDialog(urlStr,window,WINDOWSSIZE);   
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.hideOperate.value=mOperate;
	
	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	
	fm.action="FCCarContSave.jsp";
	fm.submit(); //提交  
}

//提交后操作,服务器数据返回后执行的操作
function afterContSubmit( FlagStr, content)
{
//	showInfo.close();
//	fm.Policy.value="";
//	if (FlagStr == "Fail" )
//	{             
//		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
//		showModalDialog(urlStr,window,WINDOWSSIZE);   
//	}
//	else
//	{
//		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
//		showModalDialog(urlStr,window,WINDOWSSIZE);
//		
//		if(fm.CopyFlag.checked)
//		{
//			fm.InnerContNo.value="";
//			fm.PolNo.value="";
//			fm.ItemNo.value="";
//			fm.InsuredNo.value="";
//			
//			fm.PolicyPrtNo.value="";
//			fm.InvoicePrtNo.value="";
//			fm.InsCardPrtNo.value="";
//			fm.CInsFlagPrtNo.value="";
//			fm.ContNo.value="";
//			fm.PolicyPli.value="";
//			
//			//把全局变量mRiskCode清空使得afterCodeSelect 使得能下拉出险别代码
//			mRiskCode="";
//			
//			initPolForm();
//			initKindGrid();
//			
//			
//			//fm.UseNatureCode.value = "200";
//			//fm.UseNatureName.value = "非营业";
//			fm.CopyFlag.checked = false;
//		}
//		else
//		{
//			fm.InnerContNo.value="";
//			fm.PolNo.value="";
//			fm.ItemNo.value="";
//			fm.InsuredNo.value="";
//			
//			fm.PolicyPrtNo.value="";
//			fm.InvoicePrtNo.value="";
//			fm.InsCardPrtNo.value="";
//			fm.CInsFlagPrtNo.value="";
//			fm.ContNo.value="";
//			fm.InsuredNo.value="";
//			fm.InsuredCustType.value="";
//			fm.InsuredCustTypeName.value="";
//			fm.InsuredName.value="";
//			
//			fm.InsuredIDType.value="";
//			fm.InsuredIDTypeName.value="";
//			fm.InsuredIDNo.value="";
//			
//			fm.PolicyPli.value="";
//			
//			
//			fm.CValiDate.value="";
//			fm.CInValiDate.value="";
//			
//			
//			
//			
//			//把全局变量mRiskCode清空使得afterCodeSelect 使得能下拉出险别代码
//			mRiskCode="";
//			
//			initPolForm();
//			initKindGrid();
//			initItemForm();
//			
//			fm.UseNatureCode.value = "200";
//			fm.UseNatureName.value = "非营业";
//			fm.CopyFlag.checked = false;
//		}
//	}
	
	  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }  
}


function show(tDiv)
{
	if(tDiv=="cont")
	{
		contDiv.style.display = "";
		riskDiv.style.display = "none";
	}
	else
	{
		riskDiv.style.display = "";
		contDiv.style.display = "none";
	}
}

//提交投保单信息前的校验、计算  
function beforeContSubmit()
{
	showDiv(divCarInfo,"true");
	if(!verifyInput2())
  	{
	    return false;
	}
	
	if(!showChannelName())
	{
	    return false;
	}
	if(!showAgentName())
	{
	    return false;
	}
	
	if(fm.BussNature.value=="02")
	{
		if(NullOrBlank(fm.ChannelCode.value))
		{
			showVerify("fm.ChannelCode","渠道编码不能为空！");
	  		return false;
		}
    }
	
	if(!isTrueNo(fm.PolicyPrtNo.value))
	{
		showVerify("fm.PolicyPrtNo","保单印刷号不合法！");
	  	return false;
    }
    if(!isTrueNo(fm.InvoicePrtNo.value))
	{
		showVerify("fm.InvoicePrtNo","发票印刷号不合法！");
	  	return false;
    }
	if(!isTrueNo(fm.InsCardPrtNo.value))
	{
		showVerify("fm.InsCardPrtNo","保险卡印刷号不合法！");
	  	return false;
    }
    if(!isTrueNo(fm.CInsFlagPrtNo.value))
	{
		showVerify("fm.CInsFlagPrtNo","交强险标志印刷号不合法！");
	  	return false;
    }
    if(!checkLicenseNo(fm.LicenseNo.value))
	{
		if(!confirm("车牌号不合法，是否继续？"))
		{
			return false;
		}
    }
	if(!isNo2(fm.ContNo.value))
	{
		showVerify("fm.ContNo","保单号不合法！");
	  	return false;
    }
    /*if(!_checkContNo(fm.SupplierCode.value.substring(0,4),fm.ContNo.value.length))
	{
		showVerify("fm.ContNo","保单号长度与系统中设置的不符！");
		return false;
	}*/
    if(!checkFDInsContNo()){
   		return false;
    }      
    if(!NullOrBlank(fm.InsuredIDType.value)&&!NullOrBlank(fm.InsuredIDNo.value))
    {
    	if(fm.InsuredIDType.value=="01")
    	{
    		var errMess = checkIdCard2(fm.InsuredIDNo.value);
    		if(!NullOrBlank(errMess))
    		{
    			showVerify("fm.InsuredIDNo","被保人"+errMess);
	  			return false;
    		}
    	}
    }
    
    //是否参与内部考核积分”选“是”，“是否放弃并授权”则不允许选择“是”
    if(fm.RightSendFlag.value=="Y")
    {
    	if(fm.AccreditFlag.value=="Y")
    	{
    		alert("是否参与内部考核积分”选“是”，“是否放弃并授权”则不允许选择“是”");
    		fm.AccreditFlag.focus();
    		return false;
    	}
    }
	   
   	if(!NullOrBlank(fm.hour.value))
   	{
	  	if(charToNum(fm.hour.value)> charToNum("23"))
	  	{
		    showVerify("fm.hour","出单时间小时大小应该在00-23之间！");
		    return false;
	    }
	    if(charToNum(fm.minute.value)> charToNum("60"))
	  	{
	  		showVerify("fm.minute","出单时间分钟大小应该在00-60之间！");
		    return false;
	    }
    }
    
	if(!NullOrBlank(fm.CValiDate.value)&&!NullOrBlank(fm.CInValiDate.value))
	{
		if(fm.CInValiDate.value<fm.CValiDate.value)
		{
			showVerify("fm.CValiDate","保单生效日期不能大于保单有效日期至！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.SignDate.value)&&!NullOrBlank(fm.UWDate.value))
	{
		if(fm.UWDate.value>fm.SignDate.value)
		{
			showVerify("fm.UWDate","签单日期不能大于出单日期！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.CValiDate.value)&&!NullOrBlank(fm.UWDate.value))
	{
		if(fm.UWDate.value>fm.CValiDate.value)
		{
			showVerify("fm.UWDate","签单日期不能大于保单生效日期！");
			return false;
		}
	}	
	
	if(!NullOrBlank(fm.SignDate.value))
	{
		if(fm.SignDate.value>tCurrentDate)
		{
			showVerify("fm.SignDate","出单日期不能大于当前日期！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.Remark.value))
	{
		if(fm.Remark.value.length>250)
		{
			alert("备注长度不能大于250！");
			return false;
		}
	}
	
	if(KindGrid.checkValue(null,KindGrid) == false)
	{
		return false;
	}
	var cache = "";
	var cache2 = "";
	var tAmnt = "0";
	var tPrem = "0";
	KindGrid.delBlankLine();
	var kindLen = KindGrid.mulLineCount;
	if(kindLen>0)
	{
		for(var i=0;i<kindLen;i++)
		{
			cache = KindGrid.getRowColData(i,1);
			sql = "select a.RiskKindName from FMKind a "
			+" where a.RiskCode='"+fm.RiskCode.value+"' and a.RiskKindCode='"+cache+"'";
			
			var result=easyQueryVer3(sql,1,0);
			if(!result)
			{
				alert("不存在对应的险别编码!");
				return false;
			}
			
			cache = KindGrid.getRowColData(i,3);
			cache2 = KindGrid.getRowColData(i,4);
			if(charToNum2(cache)>0&&charToNum2(cache2)>0)
			{	
				if(charToNum2(cache2)>=charToNum2(cache))
				{
					alert("第"+(i+1)+"行保额必须大于保费!");
					return false;
				}
			}
			tAmnt = fixMath(charToNum(tAmnt),charToNum2(cache),'+');
			tPrem = fixMath(charToNum3(tPrem),charToNum3(cache2),'+');
		}
		if(charToNum3(tPrem)!=charToNum3(fm.SumPrem.value))
		{
			if(!confirm("险别总保费不等于险种总保费，是否继续？"))
			{
				return false;
			}
		}
		
		if(kindLen>1)
		{
			for(var i=0;i<kindLen;i++)
			{
				for(var j=i+1;j<kindLen;j++)
				{
					if( KindGrid.getRowColData(i,1)==KindGrid.getRowColData(j,1) )
					{
						alert("险别不能重复!");
						return false;
					}
				}
			}
		}
	}
	
	if(!showSignManName())
	{
	    return false;
	}
	
	if(!IsChar(fm.FrameNo.value))
	{
		if(!confirm("车架号不合法,是否继续？"))
		{
			return false;
		}
		
    }
    if(!IsChar(fm.EngineNo.value))
	{
		if(!confirm("发动机号不合法,是否继续？"))
		{
			return false;
		}
    }
    if(!NullOrBlank(fm.EnrollDate.value)&&!NullOrBlank(fm.YearExamDate.value))
	{
		if(fm.YearExamDate.value<fm.EnrollDate.value)
		{
			showVerify("fm.EnrollDate","初次登记日期不能大于年审到期日！");
			return false;
		}
	}
	if(!NullOrBlank(fm.EnrollDate.value))
	{
		if(fm.EnrollDate.value>tCurrentDate)
		{
			showVerify("fm.EnrollDate","初次登记日期不能大于当前日期！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.PolicyPli.value))
	{
		if(fm.PolicyPli.value.length>250)
		{
			alert("保单特约长度不能大于250！");
			return false;
		}
	}
	
	if(!NullOrBlank(fm.ProjCode.value))
	{
		fm.ProtocolNo.value ="jingjixieyi";
	}else
	{
		if(!getProtocolNo())
		{
			alert("无法根据保单上的信息获取对应的协议,请确认是否录入信息准确！");
			return false;
		}
	}
	
  	fm.hidePolicyPli.value=escapes(fm.PolicyPli.value);
  	fm.hideRemark.value=escapes(fm.Remark.value);
  	fm.SignTime.value = fm.hour.value+":"+fm.minute.value+":"+fm.Second.value;
  	showHideCInValiDate();
  	
  	if(fm.ComFirstFeeFlag.value=="Y")
  	{
  		fm.FirstFeeFlag.value="N";
  	}
  	else
  	{
  		fm.FirstFeeFlag.value="Y";
  	}
  	
/*	if(!JudgeCertifyCard())
  	{
  		return false;
  	}
  	
  */
  	

	return true;
}

function initVerify()
{
	var formsNum = 0;	//窗口中的FORM数
	var elementsNum = 0;	//FORM中的元素数
	var passFlag = true;	//校验通过标志
	//遍历所有FORM
	for (formsNum=0; formsNum<window.document.forms.length; formsNum++)
	{
		//遍历FORM中的所有ELEMENT
		for (elementsNum=0; elementsNum<window.document.forms[formsNum].elements.length; elementsNum++)
		{
			window.document.forms[formsNum].elements[elementsNum].verify="";
		}		
	}
}

function saveClick()
{
	mOperate = "INSERT||MAIN";
	
//	initVerify();
//	addContVerify2();
//	addItemVerify2();
//	addPolVerify2();
//	//下面增加相应的代码
//	if(!beforeContSubmit())
//	{
//		showDiv(divCarInfo,"false");
//		return false;
//	}
//	showDiv(divCarInfo,"false");
	
	submitContForm();
}

function deleteContClick()
{
	if(NullOrBlank(fm.InnerContNo.value))
	{
		alert("请选择一条保单数据,再做删除!");
		return false;
	}
	if(!confirm("确定要删除该保单吗?"))
	{
		return false;
	}
    //下面增加相应的代码
  	mOperate="DELETE||MAIN"; 
  	
  	submitContForm();
}

function showKindList(tInnerContNo)
{
	KindGrid.clearData();
	initKindGrid();
	var sql=" select distinct a.RiskKindCode,a.RiskKindName,a.Amount,a.SumPrem"
	+" from FCItemKind a"
	+" where a.InnerContNo='"+tInnerContNo+"'";
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		displayMultiline(resultArr,KindGrid);
	}
}

function showAgentName()
{
	if(NullOrBlank(fm.AgentCode.value))
	{
		fm.AgentCode.value = "";
		fm.AgentName.value = "";
		fm.AgentGroup.value = "";
		fm.AgentGroupName.value = "";
		
		return true;
	}
	
	var sql = "select a.Name,a.AgentState,a.ApproveState,a.EmployDate,a.OfficeFlag "
	+" from FAAgentTree a "
	+" where a.AgentCode='"+fm.AgentCode.value+"'"
	+" and a.Corporation='"+Corporate+"'";
	
	var result=easyQueryVer3(sql,1,0);
	if(!result&&!NullOrBlank(fm.AgentCode.value))
	{
		showVerify("fm.AgentCode","代理人编码错误,不存在对应的代理人！");
		return false;
	}
	
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		if(resultArr[0][1]==null||resultArr[0][1]==""||resultArr[0][1]=="03"||resultArr[0][1]=="04")
		{
			showVerify("fm.AgentCode","代理人编码对应代理人不是在职代理人！");
			return false;
		}
		
		if(resultArr[0][2]==null||resultArr[0][2]==""||resultArr[0][2]=="N")
		{
			showVerify("fm.AgentCode","代理人编码对应代理人未经过审核！");
			return false;
		}

		if(!NullOrBlank(resultArr[0][3])&&!NullOrBlank(fm.SignDate.value))
		{
			if(resultArr[0][3]>fm.SignDate.value)
			{
				showVerify("fm.AgentCode","代理人的入司时间不能大于出单日期！");
				return false;
			}
		}
		
		if(!NullOrBlank(fm.ProjCode.value))
		{}else
		{
			if(NullOrBlank(resultArr[0][4])||resultArr[0][4]=="N")
			{
				showVerify("fm.AgentCode","无项目查询返回的须由工作室出单！");
		  		return false;
			}
		}
		
		fm.AgentName.value = resultArr[0][0];
	}
	
	var checkBlackList = " select 1 from Faagentblacklist bl,FAAgentTree a "
		+ " where bl.IDNO = a.IDNO and bl.IDNOType = a.IDNOType and a.agentcode = substr('"+fm.AgentCode.value+"',2) ";
	var inBlackList = easyQueryVer4(checkBlackList);	
		
	if(inBlackList == '1'){
		showVerify("fm.AgentCode","代理人已列入黑名单中，不能录单！");
		return false;
	}	
	
	var sqlPart="";
	var sqlPart1="";
	if(tComType=="00"&&tComType=="01"&&tComType=="02")
	{
		sqlPart=getManageComLimitlike3("b.Corporation",UserComCode);
		sqlPart1=getManageComLimitlike3("b.Corporation",UserComCode);
	}
	else
	{
		sqlPart=" and b.Corporation='"+Corporate+"'";
		sqlPart1=" and b.Corporation='"+Corporate+"'";
	}
	
	if(!NullOrBlank(fm.ProjCode.value))
	{
	}else
	{
		sqlPart+=" and b.OfficeFlag='Y' ";
		sqlPart1+=" and a.OfficeFlag='Y' ";
	}
	sql = "select b.AgentGroup,(select ComName from FDCom where ComCode=b.AgentGroup) from FAAgentTree b"
	+" where b.AgentCode='"+fm.AgentCode.value+"' and b.BranchType<>'3'"
	+sqlPart;
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);

	if(resultArr!=null)
	{
		if(resultArr.length==1)
		{
			fm.AgentGroup.value = resultArr[0][0];
			fm.AgentGroupName.value = resultArr[0][1];
			
			afterCodeSelect( 'AgentToGroup', '');
		}
	}
	else
	{
		showVerify("fm.AgentCode","代理人编码错误,您没有该代理人的权限！");
		return false;
	}
	return true;
}

function afterQueryAgent(tAgentCode,tAgentName,tAgentGroup,tAgentGroupName,tCorporation,tAgentGrade,tRightSendFlag,tRightSendFlagName,
                         tAccreditFlag,tAccreditFlagName )
{
	fm.AgentCode.value = tAgentCode;
	fm.AgentName.value = tAgentName;
	fm.AgentGroup.value = tAgentGroup;
	fm.AgentGroupName.value = tAgentGroupName;
	fm.Corporation.value = tCorporation;
	//是否参与内部考核积分
	if(!NullOrBlank(tRightSendFlag))
	{
		fm.RightSendFlag.value = tRightSendFlag;
		fm.RightSendFlagName.value = tRightSendFlagName;
	}else
	{
		fm.RightSendFlag.value = "N";
		fm.RightSendFlagName.value = "否";
	}
	//是否放弃并授权
	if(!NullOrBlank(tAccreditFlag))
	{
		fm.AccreditFlag.value = tAccreditFlag;
		fm.AccreditFlagName.value = tAccreditFlagName;
	}else
	{
		fm.AccreditFlag.value = "N";
		fm.AccreditFlagName.value = "否";
	}
	fm.RightSendFlag.focus();
}

function queryAgent(tAgentCode,tAgentName)
{
	var tProjCode = fm.ProjCode.value;
	var tPart = "";
	
	if(tAgentCode!=null)
	{
		tPart = tPart+"&tAgentCode="+tAgentCode;
	}
	if(tAgentName!=null)
	{
		tPart = tPart+"&tAgentName="+tAgentName;
	}
	
	openWindow("./FAAgentQuery.html?tPageFrom=prop&tProjCode="+tProjCode+tPart);
}

function afterCodeSelect( cName, Filed)
{	
	if(  cName == 'inscom5' || cName == 'inscom6' )
	{
		fm.KindCode.value = "";
		fm.RiskCode.value = "";
		fm.OutRiskCode.value = "";
		fm.RiskName.value = "";
		
		fm.FYCType.value = "";
		fm.FYCTypeName.value = "";
		fm.FYCTypeReMark.value = "";
		
		var sql="select ContNo,PolicyPrtNo,InvoicePrtNo,InsCardPrtNo,CInsFlagPrtNo"
		+" from FCContRecent where SupplierCode = '" + fm.SupplierCode.value + "' and Corporation='" + Corporate + "'";
		
		var result=easyQueryVer3(sql,1,0);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			fm.ContNo.value = resultArr[0][0];
			fm.PolicyPrtNo.value = resultArr[0][1];
			fm.InvoicePrtNo.value = resultArr[0][2];
			fm.InsCardPrtNo.value = resultArr[0][3];
			fm.CInsFlagPrtNo.value = resultArr[0][4];
		}
	}
	
	if( cName == 'AgentToGroup' )
	{
		var sql = "select a.ComCode from FDCom a "
		+" where ComType='03' and ComNature='Y' and  EndFlag = 'N'"
		+" and ComCode in(select ComCode from FDCom connect by PRIOR "
		+" UPComCode = ComCode start with  ComCode = '" + fm.AgentGroup.value + "')";
		
		var result=easyQueryVer3(sql,1,0);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			fm.Corporation.value = resultArr[0][0];
		}
	}
	
	if( cName == 'BussNature2' )
	{
		if(fm.BussNature.value=="02")
		{
			channel.style.display = "";
		}
		else
		{
			fm.ChannelCode.value = "";
			fm.ChannelName.value = "";
			channel.style.display = "none";
		}
	}
	
	if( cName == 'riskcode8' ||cName == 'riskcode11')
	{
		if(fm.KindCode.value!='112')
		{
			fm.CInsFlagPrtNo.value="";
		}
		var tRiskCode=fm.RiskCode.value;
		if(mRiskCode!=tRiskCode)
		{   
			mRiskCode = tRiskCode;
			initKindGrid();
			//根据险种判断手续费率是否是规制引擎来的
			if(!NullOrBlank(fm.RuleID.value)){
				fm.FYCType.disabled = true;
				fm.FYCTypeName.disabled = true;
			}else{//根据费率等级来
				getFYCType();
			}
		}
	}
}

function getFYCType()
{
	fm.FYCType.value = "";
	fm.FYCTypeName.value = "";
	fm.FYCTypeReMark.value = "";
	
	var sql="select distinct a.GradeCode,b.GradeName,b.Remark from FMEstateProtocolFeeRate a,FMProtocolgrade b "
	+"where b.indexcode=a.GradeCode "
	+"and a.ManageCom in(select ComCode from FDCom connect by PRIOR UPComCode = ComCode start with  ComCode = '"+fm.Corporation.value+"') "
	+"and a.SupplierCode ='" +fm.SupplierCode.value+ "' "
	+"and a.RiskCode ='" +fm.RiskCode.value+ "' "
	+"and a.Iseffect ='Y' "
	+"and a.StartDate <='" +fm.SignDate.value+ "' "
	+"and a.EndDate >='" +fm.SignDate.value+ "' "
	+"and (a.SctrlDate>='" +fm.SignDate.value+ "' or a.SctrlDate is null)";
			
	var result=easyQueryVer3(sql,1,0);
	
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		if(resultArr.length==1)
		{
			fm.FYCType.value = resultArr[0][0];
			fm.FYCTypeName.value = resultArr[0][1];
			fm.FYCTypeReMark.value = resultArr[0][2];
		}
	}
}

function getProtocolNo()
{
	var sql =" select a.ProtocolNo from FMProtocol a,FMProtocolRisk b "
	+" where a.ProtocolNo=b.ProtocolNo"
	+" and a.ManageCom='"+fm.Corporation.value+"'"
	+" and a.CorpCode='"+fm.SupplierCode.value+"' and a.CorpType='01'"
	+" and a.TypeKind='02' and a.PStatus='1'"
	+" and a.PStartDate<='"+fm.SignDate.value+"' and a.PEndDate>='"+fm.SignDate.value+"'"
	+" and b.RiskCode='"+fm.RiskCode.value+"'"
	+" and b.StartDate<='"+fm.SignDate.value+"' and b.EndDate>='"+fm.SignDate.value+"'";

	var result=easyQueryVer3(sql,1,0);

	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.ProtocolNo.value = resultArr[0][0];
		return true;
	}
	else
	{
		var sql =" select a.ProtocolNo from FMProtocol a,FMProtocolRisk b,FDCom c "
		+" where a.ProtocolNo=b.ProtocolNo"
		+" and c.ComCode='"+fm.Corporation.value+"'"
		+" and a.ManageCom=c.UpComCode"
		+" and a.CorpCode='"+fm.SupplierCode.value+"' and a.CorpType='01'"
		+" and a.TypeKind='02' and a.PStatus='1'"
		+" and a.PStartDate<='"+fm.SignDate.value+"' and a.PEndDate>='"+fm.SignDate.value+"'"
		+" and b.RiskCode='"+fm.RiskCode.value+"'"
		+" and b.StartDate<='"+fm.SignDate.value+"' and b.EndDate>='"+fm.SignDate.value+"'";
		
		var result=easyQueryVer3(sql,1,0);
		
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			fm.ProtocolNo.value = resultArr[0][0];
			return true;
		}
	}
	return false;
}


function fullClick()
{
	parent.fraInterface.location.href="./FCCarContFullInput.jsp?InputType=Full&ThisSYSPageType=query";	//转换连接，比直接用a的方式快一些
	parent.VD.location.reload();	//用来清除缓存的操作，看看能否不采用这个方式
}

//根据生效日期,加1年减1天显示失效日期
function showCInValiDate()
{
	fm.CInValiDate.value = calDate(fm.CValiDate.value,"1","","-1");
}

//根据失效日期,减1天显示失效日期
function showCInValiDate2(tCInValiDate)
{
	fm.CInValiDate.value = calDate(tCInValiDate,"","","-1");
}

//保存失效日期时,加1天保存
function showHideCInValiDate()
{
	fm.hideCInValiDate.value = calDate(fm.CInValiDate.value,"","","1");
}

function addContVerify()
{
	fm.PolicyPrtNo.verify="保单印刷号|len<=100";
	fm.InvoicePrtNo.verify="发票印刷号|len<=100";
	fm.InsCardPrtNo.verify="保险卡印刷号|len<=100";
	fm.CInsFlagPrtNo.verify="交强险标志印刷号|len<=100";
	fm.OutManageCom.verify="出单机构|notnull&code:SettleMgmtDivision";
	fm.GetManageCom.verify="接单机构|notnull&code:GetManageCom";
	fm.LicenseNo.verify="车牌号|notnull&len<=10";
	fm.BussNature.verify="业务性质|code:BussNature2&notnull";
	fm.ComFirstFeeFlag.verify="是否见费出单|code:yesorno&notnull";
	fm.RightSendFlag.verify="是否参与内部考核积分|code:yesorno&notnull";
	fm.AccreditFlag.verify="是否放弃并授权|code:yesorno&notnull";
	fm.SupplierShowCode.verify="保险公司|notnull";
	fm.ContNo.verify="保单号|notnull&len<=40";
	fm.SignDate.verify="出单日期|notnull&date";
	fm.hour.verify="小时|notnull&NUM&len=2";
	fm.minute.verify="分钟|notnull&NUM&len=2";
	fm.UWDate.verify="签单日期|notnull&date";
	fm.CValiDate.verify="保单生效日期|notnull&date";
	fm.CInValiDate.verify="保单有效日期至|notnull&date";
	fm.InsuredCustType.verify="被保人类型|notnull&code:CustType";
	fm.InsuredName.verify="被保人姓名|notnull&len<=60";	
	fm.AgentCode.verify="代理人编码|notnull";
	fm.AgentName.verify="代理人姓名|notnull";
	//fm.AgentGroup.verify="代理人所在团队|notnull";
	
	fm.InsuredIDType.verify="被保人证件类型|code:AllIDType";
	fm.InsuredIDNo.verify="被保人证件号码|len<=20";
	
	fm.POSNo.verify="POS机交易号|len<=20";
}

function addItemVerify()
{
	fm.PurchasePrice.verify="新车购置价格(万元)|notnull&Money:8-4&value>=0&value<=1000";
	fm.CarOwner.verify="车主|len<=30";
	fm.UseNatureCode.verify="使用性质|notnull&code:carusages";
	fm.FrameNo.verify="车架号|len<=30";
	fm.EngineNo.verify="发动机号|len<=30";
	fm.CarKindCode.verify="车辆种类|code:carkind";
	fm.ModelCode.verify="厂牌车型|len<=50";
	fm.EnrollDate.verify="初次登记日期|Date";
	fm.YearExamDate.verify="年审到期日|Date";
	fm.SeatCount1.verify="座位数|len<=8&int";
	fm.TonCount.verify="吨位数|len<=8&num";
}

function addPolVerify()
{
	if(NullOrBlank(fm.RuleID.value)){
		fm.FYCType.verify="费率等级|NOTNULL";
	}
	fm.OutRiskCode.verify="险种编码|NOTNULL";
	fm.PayIntv.verify="缴费方式|code:PayIntv&NOTNULL";
	fm.SumPrem.verify="总保费|notnull&NUM&value>=0&Money:15-2";
	fm.tax1.verify="车船税|NUM&value>=0&Money:15-2";
	fm.tax2.verify="印花税|NUM&value>=0&Money:15-2";
}

function addContVerify2()
{
	fm.PolicyPrtNo.verify="保单印刷号|len<=100";
	fm.InvoicePrtNo.verify="发票印刷号|len<=100";
	fm.InsCardPrtNo.verify="保险卡印刷号|len<=100";
	fm.CInsFlagPrtNo.verify="交强险标志印刷号|len<=100";
	fm.OutManageCom.verify="出单机构|notnull&code:SettleMgmtDivision";
	fm.GetManageCom.verify="接单机构|notnull&code:GetManageCom";
	fm.LicenseNo.verify="车牌号|notnull&len<=10";
	fm.BussNature.verify="业务性质|code:BussNature2";
	fm.ComFirstFeeFlag.verify="是否见费出单|code:yesorno";
	fm.SupplierShowCode.verify="保险公司|notnull";
	fm.ContNo.verify="保单号|notnull&len<=40";
	fm.SignDate.verify="出单日期|notnull&date";
	fm.hour.verify="小时|notnull&NUM&len=2";
	fm.minute.verify="分钟|notnull&NUM&len=2";
	fm.UWDate.verify="签单日期|date";
	fm.CValiDate.verify="保单生效日期|date";
	fm.CInValiDate.verify="保单有效日期至|date";
	fm.InsuredCustType.verify="被保人类型|notnull&code:CustType";
	fm.InsuredName.verify="被保人姓名|notnull&len<=60";	
	fm.AgentCode.verify="代理人编码|notnull";
	fm.AgentName.verify="代理人姓名|notnull";
	//fm.AgentGroup.verify="代理人所在团队|notnull";
	
	fm.InsuredIDType.verify="被保人证件类型|code:AllIDType";
	fm.InsuredIDNo.verify="被保人证件号码|len<=20";
	
	fm.POSNo.verify="POS机交易号|len<=20";
}

function addItemVerify2()
{
	fm.PurchasePrice.verify="新车购置价格(万元)|Money:8-4&value>=0&value<=1000";
	fm.CarOwner.verify="车主|len<=30";
	fm.UseNatureCode.verify="使用性质|code:carusages";
	fm.FrameNo.verify="车架号|len<=30";
	fm.EngineNo.verify="发动机号|len<=30";
	fm.CarKindCode.verify="车辆种类|code:carkind";
	fm.ModelCode.verify="厂牌车型|len<=50";
	fm.EnrollDate.verify="初次登记日期|Date";
	fm.YearExamDate.verify="年审到期日|Date";
	fm.SeatCount1.verify="座位数|len<=8&int";
	fm.TonCount.verify="吨位数|len<=8&num";
}

function addPolVerify2()
{
	fm.OutRiskCode.verify="险种编码|NOTNULL";
	if(NullOrBlank(fm.RuleID.value)){
		fm.FYCType.verify="费率等级|NOTNULL";
	}
	fm.PayIntv.verify="缴费方式|code:PayIntv";
	fm.SumPrem.verify="总保费|notnull&NUM&value>=0&Money:15-2";
	fm.tax1.verify="车船税|NUM&value>=0&Money:15-2";
	fm.tax2.verify="印花税|NUM&value>=0&Money:15-2";
}

function initPolForm()
{
	fm.PolNo.value = "";
	fm.KindCode.value = "";
	fm.RiskCode.value = "";
	fm.OutRiskCode.value = "";
	fm.RiskName.value = "";
	fm.SumPrem.value = "";
	fm.tax2.value = "";
	fm.tax1.value = "";
	fm.FYCType.value = "";
	fm.FYCTypeName.value = "";
	fm.FYCTypeReMark.value = "";
	
}

function initItemForm()
{	
	//车辆信息
	fm.PurchasePrice.value = "";
	fm.CarOwner.value = "";
	fm.LicenseNo.value = "";
	fm.CarKindCode.value = "";
	fm.CarKindName.value = "";
	fm.UseNatureCode.value = "";
	fm.UseNatureName.value = "";
	fm.FrameNo.value = "";
	fm.EngineNo.value = "";
	fm.ModelCode.value = "";
	fm.YearExamDate.value = "";
	fm.EnrollDate.value = "";
	fm.SeatCount1.value="";
	fm.TonCount.value="";
}

function getCarByInnerContNo(tInnerContNo)
{
	fm.ItemCode.value = "06";
	fm.ItemName.value = "车辆";
	fm.ItemType.value="CarInfo";
	
	var sql="select a.ItemNo,a.CarOwner,a.LicenseNo,a.CarKindCode,a.UseNatureCode,"
	+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,PurchasePrice,"
	+" a.SeatCount,a.TonCount"
	+" ,(select codename from fdcode where codetype='carkind' and code=a.CarKindCode)"
	+" ,(select codename from fdcode where codetype='carusages' and code=a.UseNatureCode),"
	+" CountryNature,(select codeName from fdcode where codetype='countrynature' and code = a.CountryNature),"
	+" LicenseColorCode,(select codeName from fdcode where codetype='licensecolor' and code = a.LicenseColorCode),"
	+" UseYears"
	+" from FCItemCar a where a.InnerContNo='"+tInnerContNo+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.ItemNo.value = resultArr[0][0];
		fm.CarOwner.value = resultArr[0][1];
		fm.LicenseNo.value = resultArr[0][2];
		fm.CarKindCode.value = resultArr[0][3];
		//fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][3]);
		fm.CarKindName.value =resultArr[0][13];
		
		fm.UseNatureCode.value = resultArr[0][4];
		fm.UseNatureName.value =resultArr[0][14];
		
		fm.FrameNo.value = resultArr[0][5];
		fm.EngineNo.value = resultArr[0][6];
		fm.ModelCode.value = resultArr[0][7];
		fm.YearExamDate.value = resultArr[0][8];
		fm.EnrollDate.value = resultArr[0][9];
		fm.PurchasePrice.value = resultArr[0][10];
		fm.SeatCount1.value = resultArr[0][11];
		fm.TonCount.value = resultArr[0][12];
		
		fm.CountryNature.value = resultArr[0][15];
		fm.CountryNatureName.value = resultArr[0][16];
		fm.LicenseColorCode.value = resultArr[0][17];
		fm.LicenseColorName.value = resultArr[0][18];
		fm.UseYears.value = resultArr[0][19];
	}
	else
	{
		alert("系统中查询不到车牌号相同的车辆信息");
	}
}


function confirmClick()
{
	mOperate = "CONFIRM||INSERT";
	
//	initVerify();
//	addContVerify();
//	addItemVerify();
//	addPolVerify();

//	车险，非车险，录入完毕的时候 产品定义不选择发放权益证类型的情况下，出单的 地方不 能够选发放权益证
//	if(fm.RightSendFlag.value=="Y")
//	{
//		var sql="select count(SendBenefitType) from FMRisk where RiskCode ='"+fm.RiskCode.value+"'";
//		var result=easyQueryVer3(sql,1,0);
		
//		var resultArr=new Array();
//		resultArr=decodeEasyQueryResult(result);
//		if(resultArr==0)
//		{
//			alert("产品定义未选择是否参与内部考核积分，是否参与内部考核积分不能选择【是】");
//			fm.RightSendFlag.focus();
//			return false;
//		}
		
//	}	
	
//	//下面增加相应的代码
//	if(!beforeContSubmit())
//	{
//		showDiv(divCarInfo,"false");
//		return false;
//	}
//	showDiv(divCarInfo,"false");
	
	submitContForm();
}

function showInsured()
{
	if(fm.insured.checked==true)
	{
		fm.all('AppntCustType').value=fm.all('InsuredCustType').value;
		fm.all('AppntCustTypeName').value=fm.all('InsuredCustTypeName').value;
	  	fm.all('AppntName').value=fm.all('InsuredName').value;
	  	fm.all('AppntIDType').value=fm.all('InsuredIDType').value;
	  	fm.all('AppntIDTypeName').value=fm.all('InsuredIDTypeName').value;
	  	fm.all('AppntIDNo').value=fm.all('InsuredIDNo').value;
	  	fm.all('AppntPostalAddress').value=fm.all('InsuredPostalAddress').value;
	  	fm.all('AppntZipCode').value=fm.all('InsuredZipCode').value;
	  	fm.all('AppntMobile').value=fm.all('InsuredMobile').value;
	  	
	  	fm.all('AppntCustType').disabled=true;
	  	fm.all('AppntCustTypeName').disabled=true;
	  	fm.all('AppntName').disabled=true;
	  	fm.all('AppntIDType').disabled=true;
	  	fm.all('AppntIDTypeName').disabled=true;
	  	fm.all('AppntIDNo').disabled=true;
	  	fm.all('AppntPostalAddress').disabled=true;
	  	fm.all('AppntZipCode').disabled=true;
	  	fm.all('AppntMobile').disabled=true;
	}
	else
	{
	  	fm.all('AppntCustType').disabled=false;
	  	fm.all('AppntCustTypeName').disabled=false;
	  	fm.all('AppntName').disabled=false;
	  	fm.all('AppntIDType').disabled=false;
	  	fm.all('AppntIDTypeName').disabled=false;
	  	fm.all('AppntIDNo').disabled=false;
	  	fm.all('AppntPostalAddress').disabled=false;
	  	fm.all('AppntZipCode').disabled=false;
	  	fm.all('AppntMobile').disabled=false;
	}
}

//查询被保人是否是已存在客户
function queryInsured()
{
	//客户类型,姓名,证件类型,证件号码都不为空时才做处理
	if(fm.InsuredCustType.value==null||fm.InsuredCustType.value==''
	 ||fm.InsuredName.value==null||fm.InsuredName.value==''
	 ||fm.InsuredIDType.value==null||fm.InsuredIDType.value==''
	 ||fm.InsuredIDNo.value==null||fm.InsuredIDNo.value=='')
	{
		
	}
	else
	{
		//个人客户且证件类型为身份证
		if(fm.InsuredCustType.value=="1"&&fm.InsuredIDType.value=="01")
		{
			var tName = fm.InsuredName.value;
			var tIDType = fm.InsuredIDType.value;
			var tIDNo = fm.InsuredIDNo.value;
			var tSex = calSexByIDNo(fm.InsuredIDNo.value,"0");
			var tBirthday = calBirthByIDNo(fm.InsuredIDNo.value);
		
			var sql="select CustomerNo from FDPerson where Name='"+tName+"' and Sex='"+tSex+"' and Birthday='"+tBirthday+"' and IDType='"+tIDType+"' and IDNo='"+tIDNo+"' ";
			
			var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			if(resultArr!=null)
			{
				fm.InsuredNo.value = resultArr[0][0];
				
				queryCustCar(fm.InsuredNo.value);
				queryInsuredAddress(fm.InsuredNo.value,1);
			}
		}
		else if(fm.InsuredCustType.value=="2"&&fm.InsuredIDType.value=="71")
		{
			var tName = fm.InsuredName.value;
			var tIDNo = fm.InsuredIDNo.value;
			
			var sql="select CustomerNo from FDGroup where GrpName='"+tName+"' and OrganComCode='"+tIDNo+"'";
			
			var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			if(resultArr!=null)
			{
				fm.InsuredNo.value = resultArr[0][0];
				
				queryCustCar(fm.InsuredNo.value);
				queryInsuredAddress(fm.InsuredNo.value,2);
			}
		}
	}
}

function queryCustCar(tCustomerNo)
{
	if(NullOrBlank(fm.ItemNo.value)&&NullOrBlank(fm.CarOwner.value)
	 &&NullOrBlank(fm.LicenseNo.value)&&NullOrBlank(fm.CarKindCode.value)
	 &&NullOrBlank(fm.UseNatureCode.value)&&NullOrBlank(fm.FrameNo.value)
	 &&NullOrBlank(fm.EngineNo.value)&&NullOrBlank(fm.ModelCode.value)
	 &&NullOrBlank(fm.YearExamDate.value)&&NullOrBlank(fm.EnrollDate.value)
	 &&NullOrBlank(fm.BuyDate.value)&&NullOrBlank(fm.PurchasePrice.value))
	{
		var sql="select '',a.CarOwner,a.LicenseNo,a.CarKindCode,a.UseNatureCode,"
		+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,a.BuyDate,"
		+" a.VINNo,a.RunAreaName,a.HKFlag,a.HKLicenseNo,a.UseYears,a.RunMiles,"
		+" a.CountryNature,a.CountryCode,a.LicenseColorCode,a.SeatCount,a.TonCount,"
		+" a.ExhaustScale,a.ColorCode,a.SafeDevice,a.ParkSite,a.OwnerAddress,"
		+" a.ProduceDate,a.CarUsage,a.PurchasePrice,a.InvoiceNo,a.CarChecker,"
		+" a.CarCheckTime,a.CarCheckStatus,a.CarDealerName,a.PurchasePrice"
		+" from FDCustObjectCar a where a.CustomerNo='"+tCustomerNo+"'";
		
		var result=easyQueryVer3(sql,1,0);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result);
		if(resultArr!=null)
		{
			fm.CarOwner.value = resultArr[0][1];
			fm.LicenseNo.value = resultArr[0][2];
			fm.CarKindCode.value = resultArr[0][3];
			fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][3]);
			fm.UseNatureCode.value = resultArr[0][4];
			fm.UseNatureName.value = getNameFromLDCode("carusages",resultArr[0][4]);
			fm.FrameNo.value = resultArr[0][5];
			fm.EngineNo.value = resultArr[0][6];
			fm.ModelCode.value = resultArr[0][7];
			fm.YearExamDate.value = resultArr[0][8];
			fm.EnrollDate.value = resultArr[0][9];
			fm.BuyDate.value = resultArr[0][10];
			fm.VINNo.value = resultArr[0][11];
			fm.RunAreaName.value = resultArr[0][12];
			fm.HKFlag.value = resultArr[0][13];
			fm.HKFlagName.value = getNameFromLDCode("yesorno",resultArr[0][13]);
			fm.HKLicenseNo.value = resultArr[0][14];
			fm.UseYears.value = resultArr[0][15];
			fm.RunMiles.value = resultArr[0][16];
			fm.CountryNature.value = resultArr[0][17];
			fm.CountryNatureName.value = getNameFromLDCode("countrynature",resultArr[0][17]);
			fm.CountryCode1.value = resultArr[0][18];
			fm.CountryCode1Name.value = getNameFromLDCode("country",resultArr[0][18]);
			fm.LicenseColorCode.value = resultArr[0][19];
			fm.LicenseColorName.value = getNameFromLDCode("licensecolor",resultArr[0][19]);
			fm.SeatCount1.value = resultArr[0][20];
			fm.TonCount.value = resultArr[0][21];
			fm.ExhaustScale.value = resultArr[0][22];
			fm.ColorCode.value = resultArr[0][23];
			fm.ColorName.value = getNameFromLDCode("carcolor",resultArr[0][23]);
			fm.SafeDevice.value = resultArr[0][24];
			fm.ParkSite.value = resultArr[0][25];
			fm.OwnerAddress.value = resultArr[0][26];
			fm.ProduceDate.value = resultArr[0][27];
			fm.CarUsage.value = resultArr[0][28];
			fm.PurchasePrice.value = resultArr[0][29];
			fm.InvoiceNo.value = resultArr[0][30];
			fm.CarChecker.value = resultArr[0][31];
			fm.CarCheckTime.value = resultArr[0][32];
			fm.CarCheckStatus.value = resultArr[0][33];
			fm.CarCheckStatusName.value = getNameFromLDCode("carcheck",resultArr[0][33]);
			fm.CarDealerName.value = resultArr[0][34];
			fm.PurchasePrice.value = resultArr[0][35];
		}
	}
}

var oldInnerContNo="";
function getCont()
{
	if(getContByLicenseNo(fm.LicenseNo.value))
	{
		getCarByInnerContNo(oldInnerContNo);
		//queryOldRisk(oldInnerContNo);
		afterCodeSelect('ManageCom2','');
		oldInnerContNo = "";
	}
	//没有保单则查询车辆信息和客户信息
	else
	{
		var temp = getCarByLicenseNo(fm.LicenseNo.value);
		if(temp==1)
		{
			fm.InsuredName.value=getNameByCode("Name","FDPerson","CustomerNo",fm.InsuredNo.value);
			if(NullOrBlank(fm.InsuredName.value))
			{
				fm.InsuredCustType.value = "2";
				fm.InsuredCustTypeName.value = getNameFromLDCode("custtype",fm.InsuredCustType.value);
				fm.InsuredName.value=getNameByCode("GrpName","FDGroup","CustomerNo",fm.InsuredNo.value);
			}
			else
			{
				fm.InsuredCustType.value = "1";
				fm.InsuredCustTypeName.value = getNameFromLDCode("custtype",fm.InsuredCustType.value);
			}
		}
		else if(temp>1)
		{
			openWindow("../customer/FDCustomerQueryMain.jsp?tType=InsuredCust&LicenseNo="+fm.LicenseNo.value);
		}
	}
}

function getContByLicenseNo(tLicenseNo)
{
	var sql="select a.PolicyPrtNo,a.InvoicePrtNo,a.InsCardPrtNo,a.CInsFlagPrtNo,a.OutManageCom,a.GetManageCom,"
	+" a.BussNature,a.ComFirstFeeFlag,a.SupplierCode,a.ContNo,a.SignDate,a.SignTime,a.UWDate,a.CValiDate,a.CInValiDate,"
	+" a.InsuredNo,a.InsuredCustType,a.InsuredName,a.AgentCode,a.AgentGroup,a.PolicyPli,a.InnerContNo,"
	+" a.ProtocolNo,a.Corporation,a.SignMan,a.ChannelCode,a.POSNo,a.RightSendFlag,a.AccreditFlag,a.InsuredIDType,a.InsuredIDNo"
	+" from FCCont a where a.LicenseNo='"+tLicenseNo+"' order by a.MakeDate desc,a.MakeTime desc";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	//是否有保单
	if(resultArr!=null)
	{
		//fm.PolicyPrtNo.value=resultArr[0][0];
		//fm.InvoicePrtNo.value=resultArr[0][1];
		//fm.InsCardPrtNo.value=resultArr[0][2];
		//fm.CInsFlagPrtNo.value=resultArr[0][3];
		//fm.OutManageCom.value=resultArr[0][4]; 
		//fm.OutManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][4]);
		//fm.GetManageCom.value=resultArr[0][5];
		//fm.GetManageComName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][5]);
		fm.BussNature.value=resultArr[0][6];
		fm.BussNatureName.value=getNameFromLDCode("BussNature2",resultArr[0][6]);
		fm.ComFirstFeeFlag.value=resultArr[0][7];
		fm.ComFirstFeeFlagName.value=getNameFromLDCode("yesorno",resultArr[0][7]);
		fm.SupplierCode.value=resultArr[0][8];
		fm.SupplierName.value=getNameByCode("ShortName","fdinscom","SupplierCode",resultArr[0][8]);
		fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",resultArr[0][8]);
		//fm.ContNo.value=resultArr[0][9];
		fm.SignDate.value=resultArr[0][10];
		fm.SignTime.value=resultArr[0][11];
		fm.hour.value = (fm.SignTime.value).substring(0,2);
		fm.minute.value = (fm.SignTime.value).substring(3,5);
		fm.UWDate.value=resultArr[0][12];
		fm.CValiDate.value=resultArr[0][13];
		showCInValiDate2(resultArr[0][14]);
		fm.InsuredNo.value=resultArr[0][15];
		fm.InsuredCustType.value=resultArr[0][16];
		fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",resultArr[0][16]);
		fm.InsuredName.value=resultArr[0][17];
		fm.AgentCode.value=resultArr[0][18];
		fm.AgentName.value=getNameByCode("Name","FAAgentTree","AgentCode",resultArr[0][18]);
		fm.AgentGroup.value=resultArr[0][19];
		fm.AgentGroupName.value=getNameByCode("ShortName","FDCom","ComCode",resultArr[0][19]);
		fm.PolicyPli.value=unescapes(resultArr[0][20]);
		//fm.InnerContNo.value=resultArr[0][21];
		oldInnerContNo=resultArr[0][21];
		//fm.ProtocolNo.value=resultArr[0][22];
		//fm.PKProtocolNo.value=resultArr[0][22];
		fm.Corporation.value=resultArr[0][23];
		
		fm.SignMan.value=resultArr[0][24];
		fm.SignManName.value=getNameByCode("UserName","FDUser","UserCode",resultArr[0][24]);
		fm.POSNo.value=resultArr[0][26];
		
		fm.RightSendFlag.value=resultArr[0][27];
		fm.RightSendFlagName.value=getNameFromLDCode("yesorno",resultArr[0][27]);
		fm.AccreditFlag.value=resultArr[0][28];
		fm.AccreditFlagName.value=getNameFromLDCode("yesorno",resultArr[0][28]);
		
		fm.InsuredIDType.value=resultArr[0][29];
		fm.InsuredIDTypeName.value=getNameFromLDCode("idtype",resultArr[0][29]);
		fm.InsuredIDNo.value=resultArr[0][30];
		
		if(fm.BussNature.value=="02")
		{
			channel.style.display = "";
			fm.ChannelCode.value=resultArr[0][25];
			fm.ChannelName.value=getNameByCode("RepairShopName","FDRepairShop","RepairShopCode",resultArr[0][25]);
		}
		else
		{
			channel.style.display = "none";
			fm.ChannelCode.value="";
			fm.ChannelName.value="";
		}
		
		//mProtocolNo = fm.ProtocolNo.value;
		//mAgentGroup = fm.AgentGroup.value;
		
		return true;
	}
	
	return false;
}

function getCarByLicenseNo(tLicenseNo)
{
	var sql="select a.CarOwner,a.CarKindCode,a.UseNatureCode,"
	+" a.FrameNo,a.EngineNo,a.ModelCode,a.YearExamDate,a.EnrollDate,a.CustomerNo,a.PurchasePrice"
	+" from FDCustObjectCar a where a.LicenseNo='"+tLicenseNo+"' order by a.MakeDate desc,a.MakeTime desc";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.CarOwner.value = resultArr[0][0];
		fm.CarKindCode.value = resultArr[0][1];
		fm.CarKindName.value = getNameFromLDCode("carkind",resultArr[0][1]);
		fm.UseNatureCode.value = resultArr[0][2];
		fm.UseNatureName.value = getNameFromLDCode("carusages",resultArr[0][2]);
		fm.FrameNo.value = resultArr[0][3];
		fm.EngineNo.value = resultArr[0][4];
		fm.ModelCode.value = resultArr[0][5];
		fm.YearExamDate.value = resultArr[0][6];
		fm.EnrollDate.value = resultArr[0][7];
		fm.InsuredNo.value = resultArr[0][8];
		fm.PurchasePrice.value = resultArr[0][9];
		
		return resultArr.length;
	}
	else
	{
		alert("系统中查询不到车牌号相同的车辆信息");
	}
	
	return 0;
}

//个人客户
function afterQueryPerCust(tCustomerNo,tType)
{
	var PerCustSQL ="";
	PerCustSQL = "select Name"
        	   	+" from FDPerson"
  				+" where customerno = '"+tCustomerNo+"' ";
  	var PerCustResult=easyQueryVer3(PerCustSQL,1,0,1);
	if(!PerCustResult){
	}
	else
	{
		var PerCustSelected = decodeEasyQueryResult(PerCustResult);
		//被保人信息
		if("InsuredCust"==tType)
		{
			fm.InsuredCustType.value ="1";
			fm.InsuredCustTypeName.value ="个人";
			fm.InsuredName.value=PerCustSelected[0][0];
			fm.InsuredNo.value=tCustomerNo;
		}
	}
}
//单位客户
function afterQueryGrpCust(tCustomerNo,tType)
{
	var GrpCustSQL =" select GrpName"
		        +" from FDGroup"
			    +" where customerno ='"+tCustomerNo+"' ";
	  	var PerCustResult=easyQueryVer3(GrpCustSQL,1,0,1);
	if(!PerCustResult){
	}
	else
	{
		var PerCustSelected = decodeEasyQueryResult(PerCustResult);
		if("InsuredCust"==tType)
		{
			fm.InsuredCustType.value ="2";
			fm.InsuredCustTypeName.value ="单位";
			fm.InsuredName.value=PerCustSelected[0][0];
			fm.InsuredNo.value=tCustomerNo;
		}
	}
}

function showSignManName()
{
	if(NullOrBlank(fm.SignMan.value))
	{
		return true;
	}
	var sql = "select a.UserName from FDUser a"
	+" where a.UserCode='"+fm.SignMan.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=decodeEasyQueryResult(result);
	if(!result)
	{
		alert("出单员编码错误,不存在对应的出单员!");
		fm.SignMan.value = "";
		fm.SignManName.value = "";		
		return false;
	}
	else
	{
		fm.SignManName.value = resultArr[0][0];
	}
	return true;
}

function showReceiveOperatorName()
{
	if(NullOrBlank(fm.ReceiveOperator.value))
	{
		fm.ReceiveOperator.value = "";
		return;
	}
	var sql = "select a.UserName from FDUser a"
	+" where a.UserCode='"+fm.ReceiveOperator.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=decodeEasyQueryResult(result);
	if(!result)
	{
		alert("录单员编码错误,不存在对应的录单员!");
		fm.ReceiveOperator.value = "";
		fm.ReceiveOperatorName.value = "";		
		return;
	}
	else
	{
		fm.ReceiveOperatorName.value = resultArr[0][0];
	}
}

function showChannelName()
{
	if(NullOrBlank(fm.ChannelCode.value))
	{
		fm.ChannelName.value = "";
		return true;
	}
	
	var sql = "select a.RepairShopName from FDRepairShop a"
	+" where a.RepairShopCode='"+fm.ChannelCode.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=decodeEasyQueryResult(result);
	if(!result)
	{
		alert("渠道编码错误,不存在对应的渠道!");
		fm.ChannelCode.value = "";
		fm.ChannelName.value = "";		
		return false;
	}
	else
	{
		fm.ChannelName.value = resultArr[0][0];
	}
	
	var sqlPart = "";
	if(tComType=="00"||tComType=="01"||tComType=="02")
	{
		sqlPart = getManageComLimitlike3("ComCode",""," and ComType = '03' ");
	}
	else
	{
		sqlPart = " and ComCode='"+Corporate+"'";
	}
	
	var sql="select RepairShopCode from FDRepairShopToCom"
	+" where RepairShopCode='"+fm.ChannelCode.value+"'"
	+ sqlPart;
	var result=easyQueryVer3(sql,1,0);
	if(!result)
	{
		alert("渠道编码错误,您登录的机构对与渠道【"+fm.ChannelName.value+"】没有关联关系！");
		fm.ChannelCode.value = "";
		fm.ChannelName.value = "";		
		return false;
	}
	return true;
}

//实现../common/Calendar/Calendar.js中调用的方法
function _afterSelectCalendar(obj)
{
	if(obj==fm.CValiDate)
	{
		showCInValiDate();
	}
}


function queryChannel()
{
  showInfo=openWindow("./FCChannelQueryInput.html");
}

function afterQueryChannel(arrQueryResult)
{
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
	    arrResult = arrQueryResult;	
		fm.ChannelCode.value = arrResult[0]; 
		fm.ChannelName.value = arrResult[1];
   }
}
//下拉保险公司
function showSupplierClick(str1,str2,str3)
{
	//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		showCodeList('inscom6',[str1,str2,str3],[0,1,2],null,'1',fm.ProjCode.value,'1',300);
	}
	//代理业务查询保险公司
	else
	{
		showCodeList('inscom5',[str1,str2,str3],[0,1,2],null,null,null,null,300);
	}
}

function showSupplierKey(str1,str2,str3)
{
	//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		showCodeListKey('inscom6',[str1,str2,str3],[0,1,2],null,'1',fm.ProjCode.value,'1',300);
	}
	//代理业务查询保险公司
	else
	{
		showCodeListKey('inscom5',[str1,str2,str3],[0,1,2],null,null,null,null,300);
	}
}

//下拉险种
function showRiskClick(str1,str2,str3,str4,str5)
{
	if(NullOrBlank(fm.SupplierCode.value))
	{
		alert("请先选择保险公司！");
		return false;
	}
	if(NullOrBlank(fm.SignDate.value))
	{
		alert("请先录入出单日期！");
		return false;
	}
	if(NullOrBlank(fm.AgentCode.value))
	{
		alert("请先选择代理人！");
		return false;
	}
	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
	{
		alert("代理人信息有误！");
		return false;
	}
	//车险标志
	var carflag ="Y";
	//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		fm.RiskCons.value=fm.SupplierCode.value+","+fm.ProjCode.value+","+carflag;
	    showCodeList('riskcode11',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}else//财险代理业务
	{
		fm.RiskCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.AgentCode.value+","+fm.SignDate.value;
	    showCodeList('riskcode8',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}	
	
}

function showRiskKey(str1,str2,str3,str4,str5)
{
	if(NullOrBlank(fm.SupplierCode.value))
	{
		alert("请先选择保险公司！");
		return false;
	}
	if(NullOrBlank(fm.SignDate.value))
	{
		alert("请先录入出单日期！");
		return false;
	}
	if(NullOrBlank(fm.AgentCode.value))
	{
		alert("请先选择代理人！");
		return false;
	}
	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
	{
		alert("代理人信息有误！");
		return false;
	}
		
	//车险标志
	var carflag ="Y";	
		//经济代理项目
	if(!NullOrBlank(fm.ProjCode.value))
	{
		fm.RiskCons.value=fm.SupplierCode.value+","+fm.ProjCode.value+","+carflag;
	    showCodeListKey('riskcode11',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}else//财险代理业务
	{
		fm.RiskCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.AgentCode.value+","+fm.SignDate.value;
	    showCodeListKey('riskcode8',[str1,str2,str3,str4,str5],[0,1,2,3,4],null,'1',fm.RiskCons.value,'1',300);
	}	
}

function showFYCTypeClick(str1,str2,str3)
{
//	if(NullOrBlank(fm.SupplierCode.value))
//	{
//		alert("请先选择保险公司！");
//		return false;
//	}
//	if(NullOrBlank(fm.SignDate.value))
//	{
//		alert("请先录入出单日期！");
//		return false;
//	}
//	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
//	{
//		alert("代理人信息有误！");
//		return false;
//	}
//	if(NullOrBlank(fm.RiskCode.value))
//	{
//		alert("请先选择险种！");
//		return false;
//	}
//		
//	fm.FYCCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.RiskCode.value+","+fm.SignDate.value;
	//showCodeList('fyctype2',[str1,str2,str3],[0,1,2],null,'1',fm.FYCCons.value,'1',800);
	
   
}

function showFYCTypeKey(str1,str2,str3)
{
	if(NullOrBlank(fm.SupplierCode.value))
	{
		alert("请先选择保险公司！");
		return false;
	}
	if(NullOrBlank(fm.SignDate.value))
	{
		alert("请先录入出单日期！");
		return false;
	}
	if(NullOrBlank(fm.AgentGroup.value)||NullOrBlank(fm.Corporation.value))
	{
		alert("代理人信息有误！");
		return false;
	}
	if(NullOrBlank(fm.RiskCode.value))
	{
		alert("请先选择险种！");
		return false;
	}
		
	fm.FYCCons.value=fm.Corporation.value+","+fm.SupplierCode.value+","+fm.RiskCode.value+","+fm.SignDate.value;
	showCodeListKey('fyctype2',[str1,str2,str3],[0,1,2],null,'1',fm.FYCCons.value,'1',800);
}

function getPrintCont()
{
	var sql="select a.Policy from FCContNew a where a.ContNo='"+fm.ContNo.value+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	//是否有保单
	if(resultArr!=null&&resultArr.length>1)
	{
		openWindow("./FCPropContPrintQueryInput.jsp?ContNo="+fm.ContNo.value);
		fm.isscanflag.value="1";
		var imgDivvar=document.getElementById("imgDiv");
        imgDivvar.style.display="";
	}
	if(resultArr!=null&&resultArr.length==1)
	{
		getPrintContDetail(resultArr[0][0]);
		fm.isscanflag.value="1";
		var imgDivvar=document.getElementById("imgDiv");
        imgDivvar.style.display="";
	}
	if(resultArr==null||resultArr.length==0)
	{
		fm.isscanflag.value="0";
	}
	
}

function getPrintContDetail(tPolicy)
{	
	fm.Policy.value = tPolicy;
	var sql="select a.SupplierCode,a.SupplierName,a.POSNo,a.UWDate,a.SignDate,"
	+" a.SignTime,a.PolicyPli,a.InsuredName,a.InsuredIDType,a.InsuredIDTypeName,a.InsuredIDNo,"
	+" a.InsuredPostalAddress,a.InsuredMobile,a.InsuredHomePhone,a.InsuredPhone,PolicyType,InsuredSex,InsuredSexName,InsuredBirthday,InsuredEMail  "
	+" from FCContNew a where a.Policy='"+tPolicy+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		if(NullOrBlank(fm.SupplierCode.value))
		{	
			fm.SupplierCode.value=resultArr[0][0];
			fm.SupplierName.value=resultArr[0][1];
			fm.SupplierShowCode.value=getNameByCode("SupplierShowCode","fdinscom","SupplierCode",resultArr[0][0]);
		}
		fm.POSNo.value=resultArr[0][2];
		fm.UWDate.value=resultArr[0][3];
		fm.SignDate.value=resultArr[0][4];
		//fm.SignTime.value=resultArr[0][5];
		//fm.hour.value = (fm.SignTime.value).substring(0,2);
		//fm.minute.value = (fm.SignTime.value).substring(3,5);
		fm.PolicyPli.value=resultArr[0][6];
		fm.InsuredName.value=resultArr[0][7];
		fm.InsuredIDType.value=resultArr[0][8];
		fm.InsuredIDTypeName.value=resultArr[0][9];
		fm.InsuredIDNo.value=resultArr[0][10];
		fm.PolicyType.value=resultArr[0][15];
		//被保人信息
		fm.InsuredSex1.value=resultArr[0][16];
		fm.InsuredSexName1.value=resultArr[0][17];
		fm.InsuredPostalAddress1.value=resultArr[0][11];
		fm.InsuredBirthday1.value=resultArr[0][18];
		fm.InsuredEMail1.value=resultArr[0][19];
		fm.InsuredMobile1.value=resultArr[0][12];
		fm.InsuredHomePhone1.value=resultArr[0][13];
		fm.InsuredPhone1.value=resultArr[0][14];
	}
	
	var sql="select a.CarOwner,a.LicenseNo,a.CarKindCode,a.CarKindName,a.UseNatureCode,a.UseNatureName,"
	+"a.VINNo,a.FrameNo,a.EngineNo,a.ModelCode,a.SeatCount,a.TonCount,a.ExhaustScale,a.EnrollDate,"
	+"a.UseYears,a.PurchasePrice,a.RunAreaName,a.ColorCode,a.ColorName"
	+" from FCCarNew a where a.Policy='"+tPolicy+"'";
	
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.CarOwner.value=resultArr[0][0];
		fm.LicenseNo.value=resultArr[0][1];
		fm.CarKindCode.value=resultArr[0][2];
		fm.CarKindName.value=resultArr[0][3];
		fm.UseNatureCode.value=resultArr[0][4];
		fm.UseNatureName.value=resultArr[0][5];
		fm.FrameNo.value=resultArr[0][7];
		fm.EngineNo.value=resultArr[0][8];
		fm.ModelCode.value=resultArr[0][9];
		fm.SeatCount1.value=resultArr[0][10];
		fm.TonCount.value=resultArr[0][11];
		fm.EnrollDate.value=resultArr[0][13];
		fm.PurchasePrice.value=resultArr[0][15];
	}
	
	var sql="select a.RiskCode,a.RiskName,a.CValiDate,a.EndDate,a.SumPrem,a.tax1"
	+" from FCRiskNew a where a.Policy='"+tPolicy+"'";
	var result=easyQueryVer3(sql,1,0);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result);
	if(resultArr!=null)
	{
		fm.RiskCode.value=resultArr[0][0];
		fm.KindCode.value=getNameByCode("MainFlag","fmrisk","RiskCode",resultArr[0][0]);
		fm.OutRiskCode.value=getNameByCode("OutRiskCode","fmrisk","RiskCode",resultArr[0][0]);
		fm.RiskName.value=resultArr[0][1];
		fm.CValiDate.value=resultArr[0][2];
		fm.CInValiDate.value = resultArr[0][3];
		fm.SumPrem.value=resultArr[0][4];
		fm.tax1.value=resultArr[0][5];
	}
	
	//initKindGrid.clearData();
	initKindGrid();
	var sql=" select a.RiskKindCode,a.RiskKindName,a.Amount,a.SumPrem"
	+" from FCRiskKindNew a where a.Policy='"+tPolicy+"'";
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		displayMultiline(resultArr,KindGrid);
	}
	
	//储存要显示随动录入图层的value
	getPrintInputValue();
}

function showCarOwnerName()
{
	if(fm.CarOwner.value!=fm.InsuredName.value)
	{
		fm.CarOwner.value=fm.InsuredName.value;
	}
}

//经代项目查询
function projQueryClick()
{
	showInfo =openWindow("../broker/FBProjectQuery.jsp?pagetype=Prop");
}

//经代项目查询
function afterQueryPro(arrQueryResult){
	initForm();
	var ProjCode = arrQueryResult[0][0];
	
	if(ProjCode == null||ProjCode == ""){
		return false;
	}
	
	//投保客户信息
	var getClientSQL = "select RoleFlag,ClientNo from FCProjMain where ProjCode = '" + ProjCode + "'";
	var clientResult=easyQueryVer3(getClientSQL,1,0);
	var clientResultArr=decodeEasyQueryResult(clientResult);
	
	//客户信息
	if(clientResultArr!=null){ //个人客户
		fm.InsuredCustType.value = clientResultArr[0][0];
		fm.InsuredCustTypeName.value=getNameFromLDCode("CustType",clientResultArr[0][0]);
		fm.InsuredNo.value = clientResultArr[0][1];
		fm.InsuredName.value=getNameByCode("CustomerName","V_FCCustomer","CustomerNo",clientResultArr[0][1]);
	}
	//查询项目下保险公司险种信息
	queryFCProjRiskIns(ProjCode);
}

function getAgent()
{
//	if(NullOrBlank(fm.AgentCode.value))
//	{
//		queryAgent();
//	}
//	else
//	{
//		if(isTrueNo(fm.AgentCode.value))
//		{
//			sql="select a.AgentCode,a.Name,a.AgentGroup,(select shortname from fdcom where comcode=a.AgentGroup),a.Corporation,"
//			+" a.IsAssessScore,(select codename from fdcode where codetype='yesorno' and code =a.IsAssessScore ),"
//			+" a.IsBackGra,(select codename from fdcode where codetype='yesorno' and code =a.IsBackGra ) "
//			+" from FAAgentTree a where a.AgentCode like '%"+fm.AgentCode.value+"%'"
//			+" and a.OfficeFlag='Y' and a.AgentState in ('01','02') and a.Corporation='"+Corporate+"'";
//			var result=easyQueryVer3(sql,1,0);
//			var resultArr=new Array();
//			resultArr=decodeEasyQueryResult(result);
//			
//			if(resultArr!=null&&resultArr.length==1)
//			{
//				fm.AgentCode.value = resultArr[0][0];
//				fm.AgentName.value = resultArr[0][1];
//				fm.AgentGroup.value = resultArr[0][2];
//				fm.AgentGroupName.value = resultArr[0][3];
//				fm.Corporation.value = resultArr[0][4];
//				//是否参与内部考核积分
//				if(!NullOrBlank(resultArr[0][5]))
//				{
//					fm.RightSendFlag.value = resultArr[0][5];
//					fm.RightSendFlagName.value = resultArr[0][6];
//				}else
//				{
//					fm.RightSendFlag.value = "N";
//					fm.RightSendFlagName.value = "否";
//				}
//				//是否放弃并授权
//				if(!NullOrBlank(resultArr[0][7]))
//				{
//					fm.AccreditFlag.value = resultArr[0][7];
//					fm.AccreditFlagName.value = resultArr[0][8];
//				}else
//				{
//					fm.AccreditFlag.value = "N";
//					fm.AccreditFlagName.value = "否";
//				}
//				fm.RightSendFlag.focus();
//			}
//			else
//			{
//				queryAgent(fm.AgentCode.value,'');
//			}
//		}
//		else
//		{
//			sql="select a.AgentCode,a.Name,a.AgentGroup,(select shortname from fdcom where comcode=a.AgentGroup),a.Corporation,"
//			+" a.IsAssessScore,(select codename from fdcode where codetype='yesorno' and code =a.IsAssessScore ),"
//			+" a.IsBackGra,(select codename from fdcode where codetype='yesorno' and code =a.IsBackGra ) "
//			+" from FAAgentTree a where a.Name like '%"+fm.AgentCode.value+"%'"
//			+" and a.OfficeFlag='Y' and a.AgentState in ('01','02') and a.Corporation='"+Corporate+"'";
//			var result=easyQueryVer3(sql,1,0);
//			var resultArr=new Array();
//			resultArr=decodeEasyQueryResult(result);
//			
//			if(resultArr!=null&&resultArr.length==1)
//			{
//				fm.AgentCode.value = resultArr[0][0];
//				fm.AgentName.value = resultArr[0][1];
//				fm.AgentGroup.value = resultArr[0][2];
//				fm.AgentGroupName.value = resultArr[0][3];
//				fm.Corporation.value = resultArr[0][4];
//				//是否参与内部考核积分
//				if(!NullOrBlank(resultArr[0][5]))
//				{
//					fm.RightSendFlag.value = resultArr[0][5];
//					fm.RightSendFlagName.value = resultArr[0][6];
//				}else
//				{
//					fm.RightSendFlag.value = "N";
//					fm.RightSendFlagName.value = "否";
//				}
//				//是否放弃并授权
//				if(!NullOrBlank(resultArr[0][7]))
//				{
//					fm.AccreditFlag.value = resultArr[0][7];
//					fm.AccreditFlagName.value = resultArr[0][8];
//				}else
//				{
//					fm.AccreditFlag.value = "N";
//					fm.AccreditFlagName.value = "否";
//				}
//				fm.RightSendFlag.focus();
//			}
//			else
//			{
//				queryAgent('',fm.AgentCode.value);
//			}
//		}
//	}
	
	 fm.AgentGroup.value = '0000000001';
	 fm.AgentGroupName.value = '渠道测试营销团队';
	 fm.RightSendFlag.value = 'N';
	 fm.RightSendFlagName.value = '否';
}

//判断单证是否已经被使用

function JudgeCertifyCard()
{
	var tPolicyPrtNo = fm.PolicyPrtNo.value;
	var tInvoicePrtNo = fm.InvoicePrtNo.value;
	var tInsCardPrtNo = fm.InsCardPrtNo.value;
	var tCInsFlagPrtNo = fm.CInsFlagPrtNo.value;
	var tSupplierCode = fm.SupplierCode.value;
	
	var tMasterInscom = tSupplierCode.substring(0,4);
	
	//先用险种关联查询，如果查到了，直接看使用状态；
	//先用险种关联查询，如果查不到，就不用险种关联查询；
	var sqlPart = " and exists(select 1 from fmcardrisk where CertifyCode = a.CertifyCode and riskcode = '"+fm.RiskCode.value+"') "; //看保险公司险种 是否 用险种关联
	if(!NullOrBlank(tPolicyPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'J'"
	        +" and length(StartNo) = '"+tPolicyPrtNo.length+"' and StartNo <= '"+tPolicyPrtNo+"' and EndNo >= '"+tPolicyPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.PolicyPrtNo","保单印刷号【"+tPolicyPrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'J'"
	        +" and length(StartNo) = '"+tPolicyPrtNo.length+"' and StartNo <= '"+tPolicyPrtNo+"' and EndNo >= '"+tPolicyPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
			var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.PolicyPrtNo","保单印刷号【"+tPolicyPrtNo+"】已经被使用！");
					return false;
				}
			}
		}
	}
	
	if(!NullOrBlank(tInvoicePrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'I'"
	        +" and length(StartNo) = '"+tInvoicePrtNo.length+"' and StartNo <= '"+tInvoicePrtNo+"' and EndNo >= '"+tInvoicePrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.InvoicePrtNo","发票印刷号【"+tInvoicePrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'I'"
	        +" and length(StartNo) = '"+tInvoicePrtNo.length+"' and StartNo <= '"+tInvoicePrtNo+"' and EndNo >= '"+tInvoicePrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
	        var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.InvoicePrtNo","发票印刷号【"+tInvoicePrtNo+"】已经被使用！");
					return false;
				}
			}
	        
		}
	}
	if(!NullOrBlank(tInsCardPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'H'"
	        +" and length(StartNo) = '"+tInsCardPrtNo.length+"' and StartNo <= '"+tInsCardPrtNo+"' and EndNo >= '"+tInsCardPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.InsCardPrtNo","保险卡印刷号【"+tInsCardPrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'H'"
	        +" and length(StartNo) = '"+tInsCardPrtNo.length+"' and StartNo <= '"+tInsCardPrtNo+"' and EndNo >= '"+tInsCardPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'";
	        var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.InsCardPrtNo","保险卡印刷号【"+tInsCardPrtNo+"】已经被使用！");
					return false;
				}
			}
		}
	}
	if(!NullOrBlank(tCInsFlagPrtNo))
	{
		var sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'G'"
	        +" and length(StartNo) = '"+tCInsFlagPrtNo.length+"' and StartNo <= '"+tCInsFlagPrtNo+"' and EndNo >= '"+tCInsFlagPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"
	        +sqlPart;//先关联险种查
		var result=easyQueryVer3(sql,1,0);
		var resultArr=decodeEasyQueryResult(result);
		if(resultArr)
		{
			if(resultArr[0][0]=="1")
			{
				showVerify("fm.CInsFlagPrtNo","交强险标志印刷号【"+tCInsFlagPrtNo+"】已经被使用！");
				return false;
			}
		}else//否则不关联险种查询
		{
			sql=" SELECT  stateflag  FROM FZCard a"
	        +" where ConfirmFlag = '11' and (select CertifyClass from FMCertifyDes  where CertifyCode = a.CertifyCode) = 'G'"
	        +" and length(StartNo) = '"+tCInsFlagPrtNo.length+"' and StartNo <= '"+tCInsFlagPrtNo+"' and EndNo >= '"+tCInsFlagPrtNo+"'"
	        +" and decode((select MasterInscom from FDInsCom where SupplierCode = a.SupplierCode), null,suppliercode,(select MasterInscom from FDInsCom"
	        +" where SupplierCode = a.SupplierCode)) = '"+tMasterInscom+"'"; 
	        var result=easyQueryVer3(sql,1,0);
			var resultArr=decodeEasyQueryResult(result);
			if(resultArr)
			{
				if(resultArr[0][0]=="1")
				{
					showVerify("fm.CInsFlagPrtNo","交强险标志印刷号【"+tCInsFlagPrtNo+"】已经被使用！");
					return false;
				}
			}
	        
		}
    }
    
    return true;
}

function checkFDInsContNo()
{
	var tMasterInscom = fm.SupplierCode.value.substring(0,4);
	var tRiskCode = fm.RiskCode.value;
	var tContNoLen = fm.ContNo.value.length;
	var strSql1 = "select 'Y' from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03'";    
    var tResult1 = easyQueryVer4(strSql1, 1, 0, 1);
	if(tResult1 == 'Y'){			
		//校验险种层的保单号长度
		if(!checkRiskCodeCont(tMasterInscom,tRiskCode,tContNoLen)){
			alert("保单号长度与系统 险种层 设置的长度不符！");
			return false;
		}	
	}
	else{
		var strSql2 = "select 'Y' from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02'";    
    	var tResult2 = easyQueryVer4(strSql2, 1, 0, 1);
		if(tResult2 == 'Y'){			
			//校验险类层的保单号长度
			if(!checkRiskKindCont(tMasterInscom,tRiskCode,tContNoLen)){
				alert("保单号长度与系统 险类层 设置的长度不符！");
				return false;
			}			
		}
		else{
			var strSql3 = "select 'Y' from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and DefineType = '01'";    
    		var tResult3 = easyQueryVer4(strSql3, 1, 0, 1);
			if(tResult3 == 'Y'){				
				//校验保险公司层的保单号长度
				if(!checkMasterInscomCont(tMasterInscom,tContNoLen)){
					alert("保单号长度与系统 保险公司层 设置的长度不符！");
					return false;
				}	
			}
			else{
				alert("保单号对应的保险公司未设置保单号长度，请联系系统管理员!");
				return false;
			}
		}
	}
	return true;
}

function checkRiskCodeCont(tMasterInscom,tRiskCode,tContNoLen){
	var sql="select ContNoLenMode from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03'";    
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	
	if(resultArr!=null)
	{
		if(resultArr[0][0]=="01")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03' and ContNoLen='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);			
		    if(!result)
			{
				return false;
			}
		}
		else if(resultArr[0][0]=="02")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskCode = '"+tRiskCode+"' and DefineType = '03' and DLen<='"+tContNoLen+"' and UpLen>='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
	}
	return true;	
}


function checkRiskKindCont(tMasterInscom,tRiskCode,tContNoLen){
	var sql="select ContNoLenMode from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02'";    
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		if(resultArr[0][0]=="01")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02' and ContNoLen='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
		else if(resultArr[0][0]=="02")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and RiskKind = (select RiskKind from fmrisk where RiskCode = '"+tRiskCode+"') and DefineType = '02' and DLen<='"+tContNoLen+"' and UpLen>='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
	}	
	return true;	
}

function checkMasterInscomCont(tMasterInscom,tContNoLen){
	var sql="select ContNoLenMode from FDInsContNo where  masterinscom = '"+tMasterInscom+"' and DefineType = '01'";    
	
	var result=easyQueryVer3(sql,1,0,1);
	var resultArr=new Array();
	resultArr=decodeEasyQueryResult(result,0,0);
	if(resultArr!=null)
	{
		if(resultArr[0][0]=="01")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and DefineType = '01' and ContNoLen='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
		else if(resultArr[0][0]=="02")
		{
			sql="select * from FDInsContNo where MasterInscom='"+tMasterInscom+"' and DefineType = '01' and DLen<='"+tContNoLen+"' and UpLen>='"+tContNoLen+"'";
			
			result=easyQueryVer3(sql,1,0,1);
			if(!result)
			{
				return false;
			}
		}
	}	
	return true;	
}

/*var iArray = new Array("LicenseNo","SupplierShowCode","ContNo","BussNature","UWDate","SignDate","CValiDate",
						"CInValiDate","POSNo","InsuredCustType","InsuredName","InsuredIDType","InsuredIDNo","CarOwner","PurchasePrice",
						"UseNatureCode","OutRiskCode","SumPrem","tax1","FrameNo","EngineNo","CarKindCode","ModelCode","EnrollDate","SeatCount1","TonCount","PolicyPli");
*/ 
var OldiArray = new Array("","","","","","","","","","","","","","","","","","","","","","","","","","","","");
//获取回车后百星传过来的的保单信息值
function getPrintInputValue(){
	//OldiArray[0]= "111";
	//OldiArray[1]= "222";
	//OldiArray[2]= "333";
	//OldiArray[3]= "444";
	OldiArray[0]= fm.LicenseNo.value;
	OldiArray[1]= fm.SupplierShowCode.value;
	OldiArray[2]= fm.ContNo.value;
	OldiArray[3]= fm.BussNature.value;
	OldiArray[4]= fm.UWDate.value;
	OldiArray[5]= fm.SignDate.value;
	OldiArray[6]= fm.CValiDate.value;
	OldiArray[7]= fm.CInValiDate.value;
	OldiArray[8]= fm.POSNo.value;
	OldiArray[9]= fm.InsuredCustType.value;
	OldiArray[10]= fm.InsuredName.value;
	OldiArray[11]= fm.InsuredIDType.value;
	OldiArray[12]= fm.InsuredIDNo.value;
	OldiArray[13]= fm.CarOwner.value;
	OldiArray[14]= fm.PurchasePrice.value;
	OldiArray[15]= fm.UseNatureCode.value;
	OldiArray[16]= fm.OutRiskCode.value;
	OldiArray[17]= fm.SumPrem.value;
	OldiArray[18]= fm.tax1.value;
	OldiArray[19]= fm.FrameNo.value;
	OldiArray[20]= fm.EngineNo.value;
	OldiArray[21]= fm.CarKindCode.value;
	OldiArray[22]= fm.ModelCode.value;
	OldiArray[23]= fm.EnrollDate.value;
	OldiArray[24]= fm.SeatCount1.value;
	OldiArray[25]= fm.TonCount.value;
	OldiArray[26]= fm.PolicyPli.value;
	if(0==KindGrid.mulLineCount)
	{
	   OldiArray[27]= "";	
	}
	else
	{
		OldiArray[27]= fm.KindGrid1.value;
	}
}

function getFYCTypeReMark()
{
  fm.FYCTypeReMark.value = fm.FYCTypeName.value;

}

function showCont()
{
 var sql = "select           "
+"InnerContNo     , "
+"ContNo          , "
+"ProposalContNo  , "
+"ProjCode        , "
+"SupplierCode    , "
+"PolicyPrtNo     , "
+"InvoicePrtNo    , "
+"InsCardPrtNo    , "
+"CInsFlagPrtNo   , "
+"BussNature      , "
+"AgentGroup      , "
+"OutManageCom    , "
+"GetManageCom    , "
+"POSNo           , "
+"YearExamDate    , "
+"InsuredCustType , "
+"InsuredNo       , "
+"InsuredName     , "
+"InsuredIDType   , "
+"InsuredIDNo     , "
+"PayBank         , "
+"PolicyPli       , "
+"AgentCode       , "
+"SignDate        , "
+"SignTime        , "
+"ReceiveOperator , "
+"ReceiveDate     , "
+"ReceiveTime     , "
+"CValiDate       , "
+"CInValiDate     , "
+"Remark          , "
+"SumPrem         , "
+"ContPrem        , "
+"ProtocolNo      , "
+"AgentFlag       , "
+"FirstFeeFlag    , "
+"LicenseNo       , "
+"ComFirstFeeFlag , "
+"UWDate          , "
+"SignMan         , "
+"InsuredHomePhone, "
+"RightSendFlag   , "
+"HandlingMobile2 , "
+"ChannelCode     , "
+"AccreditFlag    , "
+"SignManName,Amnt,(select name from ldcom where comcode =fccont.OutManageCom ) ,ReceiveCom,(select codename from ldcode where code =fccont.GetManageCom and codetype = 'getmanagecom' ),"
+"HandlingNo           ,"
+"HandlingAddressNo    ,"
+"HandlingName         ,"
+"HandlingSex          ,"
+"HandlingBirthday     ,"
+"YearExamDate         ,"
+"HandlingIDType       ,"
+"HandlingIDNo         ,"
+"HandlingOccup        ,"
+"HandlingPostalAddress,"
+"HandlingZipCode      ,"
+"PolicyPli ,(select codename from ldcode where codetype = 'carkind' and code =fccont.HandlingName),           "
+"(select codename from ldcode where codetype = 'countrynature' and code =fccont.HandlingOccup),   "
+"(select codename from ldcode where codetype = 'licensecolor' and code =fccont.HandlingPostalAddress)   "
+"    from fccont where PolicyPrtNo = '"+fm.all('PolicyPrtNo').value+"' and state <= '"+fm.all('State').value+"' order by innercontno desc fetch first 1 rows only";
 var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
 var arr = new Array();
 arr = decodeEasyQueryResult(strQueryResult);
  if(arr!=null)
 {
fm.all('InnerContNo').value= arr[0][0];                        
fm.all('ContNo').value= arr[0][1]; 
fm.all('ContNo').value= arr[0][2];                             
fm.all('ProjCode').value= arr[0][3];                           
fm.all('SupplierCode').value= arr[0][4];                       
fm.all('PolicyPrtNo').value= arr[0][5];                        
fm.all('InvoicePrtNo').value= arr[0][6];                       
fm.all('InsCardPrtNo').value= arr[0][7];                       
fm.all('CInsFlagPrtNo').value= arr[0][8];   
fm.all('BussNature').value= arr[0][9];                         
fm.all('AgentGroup').value= arr[0][10];                         
fm.all('OutManageCom').value= arr[0][11];                      
fm.all('GetManageCom').value= arr[0][12];                       
fm.all('POSNo').value= arr[0][13]; 
fm.all('YearExamDate').value= arr[0][14];                       
fm.all('InsuredCustType').value= arr[0][15];                    
fm.all('InsuredNo').value= arr[0][16];                          
fm.all('InsuredName').value= arr[0][17];              
fm.all('InsuredIDType').value= arr[0][18];                      
fm.all('InsuredIDNo').value= arr[0][19];                        
fm.all('Policy').value= arr[0][20];    
fm.all('hidePolicyPli').value= arr[0][21];                      
fm.all('AgentCode').value= arr[0][22];                          
fm.all('SignDate').value= arr[0][23];                           
fm.all('SignTime').value= arr[0][24];  
fm.all('ReceiveOperator').value= arr[0][25];                    
fm.all('ReceiveDate').value= arr[0][26];    
//fm.all('ReceiveTime').value= arr[0][27];   
fm.all('CValiDate').value= arr[0][28];  
fm.all('CInValiDate').value= arr[0][29];        
fm.all('Remark').value= arr[0][30];    
fm.all('SumPrem').value= arr[0][31];                            
fm.all('SumPrem').value= arr[0][32];                            
fm.all('ProtocolNo').value= arr[0][33];                         
fm.all('AgentFlag').value= arr[0][34]; 
fm.all('FirstFeeFlag').value= arr[0][35];                       
fm.all('LicenseNo').value= arr[0][36];                  
fm.all('ComFirstFeeFlag').value= arr[0][37];                    
fm.all('UWDate').value= arr[0][38];      
fm.all('SignMan').value= arr[0][39];     
fm.all('InsuredHomePhone1').value= arr[0][40]; 
fm.all('RightSendFlag').value= arr[0][41];                      
// fm.all('HandlingMobile2').value= arr[0][42];  
fm.all('ChannelCode').value= arr[0][43];                        
fm.all('AccreditFlag').value= arr[0][44];     
//fm.all('SignManName').value= arr[0][45];      
fm.all('PurchasePrice').value = arr[0][46]; 

// add new OutManageCom
fm.all('ManageComName').value = arr[0][47];
fm.all('SupplierShowCode').value = arr[0][48];
fm.all('GetManageComName').value = arr[0][49];
fm.all('AgentGroupName').value = '渠道测试营销团队';
fm.all('SupplierName').value = '中国人民健康保险股份有限公司';
fm.all('FrameNo').value = arr[0][50];          
fm.all('EngineNo').value = arr[0][51];         
fm.all('CarKindCode').value = arr[0][52];      
fm.all('ModelCode').value = arr[0][53]  ;      
fm.all('EnrollDate').value = arr[0][54];       
fm.all('YearExamDate').value = arr[0][55];     
fm.all('SeatCount1').value = arr[0][56];       
fm.all('TonCount').value = arr[0][57];         
fm.all('CountryNature').value = arr[0][58];    
fm.all('LicenseColorCode').value = arr[0][59]; 
fm.all('UseYears').value = arr[0][60];         
fm.all('hidePolicyPli').value = arr[0][61];    
fm.all('CarKindName').value = arr[0][62];   
fm.all('CountryNatureName').value = arr[0][63];   
fm.all('LicenseColorName').value = arr[0][64];   


}
 sql = "select                     "+
"ContNo       ,             "+
"KindCode     ,             "+
"RiskCode     ,             "+
"OutRiskCode  ,             "+
"PayIntv      ,             "+
"SumPrem      ,             "+
"ContPrem     ,             "+
"tax1         ,             "+
"tax2         ,             "+
"SignDate     ,             "+
"SignTime     ,             "+
"AgentCode    ,             "+
"SupplierCode ,             "+
"LicenseNo    ,             "+
"ProtocolNo   ,             "+
"FYCType      ,             "+
"ISSingle     ,             "+
"RuleID       ,              "+
"(case OutRiskCode when '130101' then '普通车险' when '130102' then '意外车险' end ) ,"+
"(case FYCType when '000212' then 'A等级' when '000213' then 'B等级' end )  "+
"from FCPOL where  ProtocolNo = '"+fm.all('PolicyPrtNo').value+"' and state <= '"+fm.all('State').value+"' order by innercontno desc fetch first 1 rows only";

var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
 var arr = new Array();
 arr = decodeEasyQueryResult(strQueryResult);
  if(arr!=null)
 {
fm.all('ContNo').value=arr[0][0];                 
fm.all('KindCode').value=arr[0][1];               
fm.all('RiskCode').value=arr[0][2];               
fm.all('OutRiskCode').value=arr[0][3];            
fm.all('PayIntv').value=arr[0][4];                
fm.all('SumPrem').value=arr[0][5];                
fm.all('SumPrem').value=arr[0][6]
fm.all('tax1').value=arr[0][7];                   
fm.all('tax2').value=arr[0][8];                   
fm.all('SignDate').value=arr[0][9];               
fm.all('SignTime').value=arr[0][10];               
fm.all('AgentCode').value=arr[0][11];              
fm.all('SupplierCode').value=arr[0][12];           
fm.all('LicenseNo').value=arr[0][13];       
fm.all('ProtocolNo').value=arr[0][14];             
fm.all('FYCType').value=arr[0][15];                
fm.all('IsSingle1').value=arr[0][16];   
fm.all('RuleID').value=arr[0][17];  
fm.all('RiskName').value = arr[0][18];  
fm.all('FYCTypeName').value = arr[0][19];
fm.all('FYCTypeReMark').value = arr[0][19]; 
 }

}