<%
//Name    ：LZCertifyUserSave.jsp
//Function：对单证管理岗位人员管理的save程序
//Author   :zhangbin
//Date：2006-04-17 @ PICCH
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

  String FlagStr = "Succ";
  String Content = "操作成功";
  String tEndDate = request.getParameter("EndDate");
  String tStartDate = request.getParameter("StartDate");
  
//传参
TransferData tTransferData= new TransferData();
  tTransferData.setNameAndValue("tEndDate",tEndDate);
  tTransferData.setNameAndValue("tStartDate",tStartDate);
 
  VData tVData = new VData();

  tVData.addElement(tTransferData);
  LPJKXServiceTaskadd tLPJKXServiceTaskadd = new LPJKXServiceTaskadd();
	try
	{
		tLPJKXServiceTaskadd.run(tVData);
	}
	catch(Exception ex)
	{
	  Content = "失败" ;
	  FlagStr = "Fail";
	}
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>