/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package test;

/**
 * <p>
 * ClassName: JdbcUrl
 * </p>
 * <p>
 * Description: 构建 Jdbc 的 url
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author: HST
 * @version: 1.0
 * @date: 2002-05-31
 */
public class JdbcUrl {
	// @Field
	private String DBType;
	private String IP;
	private String Port;
	private String DBName;
	private String ServerName;
	private String UserName;
	private String PassWord;

	// @Constructor
	public JdbcUrl() {

		// 70.73
		DBType = "ORACLE";
		IP = "10.136.5.125";
		Port = "1521";
		DBName = "orcl";
		UserName = "cbspdp";
		PassWord = "cbspdp";

		// DBType = "DB2";
		// IP = "10.136.1.5";
		// Port = "50000";
		// DBName = "lis";
		// UserName = "cbstishu";
		// PassWord = "TiShu!1109";

//		 DBType = "DB2";
//		 IP = "10.136.1.5";
//		 Port = "50000";
//		 DBName = "lis";
////		 UserName = "cbstishu";
////		 PassWord = "TiShu!1109";
//		 UserName = "ghc";
//		 PassWord = "ghc&nzx71";

	}

	// @Method
	public String getDBType() {
		return DBType;
	}

	public String getIP() {
		return IP;
	}

	public String getPort() {
		return Port;
	}

	public String getDBName() {
		return DBName;
	}

	public String getServerName() {
		return ServerName;
	}

	public String getUserName() {
		return UserName;
	}

	public String getPassWord() {
		return PassWord;
	}

	public void setDBType(String aDBType) {
		DBType = aDBType;
	}

	public void setIP(String aIP) {
		IP = aIP;
	}

	public void setPort(String aPort) {
		Port = aPort;
	}

	public void setDBName(String aDBName) {
		DBName = aDBName;
	}

	public void setServerName(String aServerName) {
		ServerName = aServerName;
	}

	public void setUser(String aUserName) {
		UserName = aUserName;
	}

	public void setPassWord(String aPassWord) {
		PassWord = aPassWord;
	}

	/**
	 * 获取连接句柄
	 * 
	 * @return String
	 */
	public String getJdbcUrl() {
		String sUrl = "";

		if (DBType.trim().toUpperCase().equals("ORACLE")) {
			sUrl = "jdbc:oracle:thin:@" + IP + ":" + Port + ":" + DBName;
		}

		if (DBType.trim().toUpperCase().equals("INFORMIX")) {
			sUrl = "jdbc:informix-sqli://" + IP + ":" + Port + "/" + DBName + ":" + "informixserver=" + ServerName + ";"
					+ "user=" + UserName + ";" + "password=" + PassWord + ";";
		}

		if (DBType.trim().toUpperCase().equals("SQLSERVER")) {
			sUrl = "jdbc:inetdae:" + IP + ":" + Port + "?sql7=true&" + "database=" + DBName + "&" + "charset=gbk";
		}
		if (DBType.trim().toUpperCase().equals("WEBLOGICPOOL")) {
			sUrl = "jdbc:weblogic:pool:" + DBName;
		}

		if (DBType.trim().toUpperCase().equals("DB2")) {
			sUrl = "jdbc:db2://" + IP + ":" + Port + "/" + DBName;
		}
		return sUrl;
	}

}
