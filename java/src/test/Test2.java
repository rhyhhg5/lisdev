/*
 * servlet调用测试类
 * */

package test;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

public class Test2 {
	public static void main(String[] args) throws Exception {
//		String url = "http://10.252.4.99:8080/uapws/service/nc.itf.yyitf.synchdatac.IPfxxWs";
//		String url = "http://10.252.4.99:8080/uapws/service/nc.itf.yyitf.synchdata.IPfxxWs";
//		String url ="http://10.136.4.2:8080/ybkpre/servlet/httpservercon";
//		String url ="http://10.136.5.12:8080/ybkpre/servlet/httpservercon";//上海医保卡前置机外测
		String url ="http://localhost:8081/ui/servlet/httpserver";//上海医保卡前置机本地
//		String url ="http://localhost:8380/ui/servlet/httpserverHXYW";//上海医保卡前置机本地核心运维功能
//		String url ="http://114.255.117.72:8080/httpMedical";//沈阳医保外网地址
//		String url ="http://localhost:8580/ui/servlet/httpserver";//aclife_query查询接口本地
//		String url ="http://10.252.126.2:8080/httpserver";//aclife_query查询接口正式转发服务器
//		String url = "http://10.252.4.99:8080/uapws/service/IExoSysQueryService";
//		String url = "http://localhost:8680/RBJK/RBJKService";
//		String xml = "<?xml version='1.0' encoding='UTF-8'?><ufinterface account='develop' billtype='ftmcst' filename='' groupcode='' isexchange='Y' replace='Y' roottag='' sender='default'><bill id='45064BfbBuybi3OrderD'><billhead><pk_jiashuifl>32111111111111111111</pk_jiashuifl><!--业务流水号,最大长度为50,类型为:String--><id>a</id><!--保单号,最大长度为50,类型为:String--><baodanno>a</baodanno><!--收支类型,最大长度为50,类型为:String--><shouzhitype>a</shouzhitype><!--业务险种编码,最大长度为50,类型为:String--><xianzhongcode>2222222</xianzhongcode><!--出单机构编码,最大长度为50,类型为:String--><jigoucode>a</jigoucode><!--含税金额,最大长度为50,类型为:String--><hanshuiamount>2</hanshuiamount><!--自定义项1,最大长度为50,类型为:String--><def1>a</def1><!--自定义项2,最大长度为50,类型为:String--><def2>a</def2><!--自定义项3,最大长度为50,类型为:String--><def3>a</def3><!--自定义项4,最大长度为50,类型为:String--><def4>a</def4><!--自定义项5,最大长度为50,类型为:String--><def5>a</def5><!--自定义项6,最大长度为50,类型为:String--><def6>a</def6><!--自定义项7,最大长度为50,类型为:String--><def7>a</def7><!--自定义项8,最大长度为50,类型为:String--><def8>a</def8><!--自定义项9,最大长度为50,类型为:String--><def9>a</def9><!--自定义项10,最大长度为50,类型为:String--><def10>a</def10><!--税率,最大长度为50,类型为:String--><rate>2</rate><!--无税金额,最大长度为50,类型为:String--><wushuiamount>2</wushuiamount><!--税金,最大长度为50,类型为:String--><taxes>2</taxes></billhead></bill></ufinterface>";
		String xml = "";
		
		
		
		String mInFilePath = "E:/3/6/测试.xml";
		String mOutFilePath = "E:/fanhui1.xml";

		InputStream mInputStream = new FileInputStream(mInFilePath);
		ByteArrayOutputStream   baos   =   new   ByteArrayOutputStream(); 
        int   i=-1; 
        while((i=mInputStream.read())!=-1){ 
        baos.write(i); 
        } 
        System.out.println("报文读取完毕！");
        
        //GBK编码用于核心
		xml = new String(baos.toString().getBytes(),"GBK"); 
		
		//UTF-8编码用于对外接口
//		xml = new String(baos.toString().getBytes(),"UTF-8"); 

		System.out.println("发送报文" + xml);
		// 建立连接
		PostMethod post = new PostMethod(url);

		// 传递参数
		RequestEntity requestEntity = new StringRequestEntity(xml, "application/xml", "GBK");
//		RequestEntity requestEntity = new StringRequestEntity(xml, "application/xml", "UTF-8");
		post.setRequestEntity(requestEntity);
		HttpClient httpClient = new HttpClient();
		int statusCode = httpClient.executeMethod(post);
		if (HttpStatus.SC_OK == statusCode) {
			String responseXml = post.getResponseBodyAsString();
//			InputStream responseXml = post.getResponseBodyAsStream();
			
			System.out.println("响应报文：" + responseXml);
		} else {
			throw new Exception("通信异常，返回码为：" + statusCode);
		}
	}

}
