package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.CDATA;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

public class JDomXml {

    public void createXMLByJDOM(File dest) {
        // 创建根节点
        Element rss = new Element("rss");
        // 为根节点设置属性
        rss.setAttribute("version", "2.0");
        // 创建Document对象，并为其设置根节点
        Document document = new Document(rss);

        Element channel = new Element("channel");
        Element title = new Element("title");

//      设置节点内容，使用此方法会自动对特殊符号进行转义
//      title.setText("<![CDATA[上海移动互联网产业促进中心正式揭牌 ]]>");


//      设置CDATA类型的节点内容，使用此方法会自动在内容两边加上CDATA的格式
        CDATA cdata = new CDATA("上海移动互联网产业促进中心正式揭牌");
        title.setContent(cdata);

        channel.setContent(title);
        rss.setContent(channel);

        // 创建XMLOutputter对象
        XMLOutputter outputter = new XMLOutputter();
        try {

//          方法一：创建Format对象（自动缩进、换行）
            Format format = Format.getPrettyFormat();
//          为XMLOutputter设置Format对象
            outputter.setFormat(format);

//          方法二：创建Format对象，并设置其换行
//          Format format = Format.getCompactFormat();
//          format.setIndent("");

            // 将Document转换成XML
            outputter.output(document, new FileOutputStream(dest));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
