/*
 * 保单验真二期数据补提工具类
 * 
 * */
package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sinosoft.lis.schema.IAC_LDPERSONSchema;
import com.sinosoft.lis.vschema.IAC_LDPERSONSet;
import com.sinosoft.utility.RSWrapper;

public class CreateFileUtil {

    /**
     * 生成.TXT格式文件,行数几乎无上限
     */
    public static boolean createTxtFile(List<Object[]> rows, String filePath, String fileName) {
        // 标记文件生成是否成功
        boolean flag = true;

        try {
            // 含文件名的全路径
            String fullPath = filePath + File.separator + fileName + ".txt";

            File file = new File(fullPath);
            if (file.exists()) { // 如果已存在,删除旧文件
                file.delete();
            }
            file = new File(fullPath);
            file.createNewFile();
            System.out.println("文件创建成功！");

            // 格式化浮点数据
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMaximumFractionDigits(10); // 设置最大小数位为10

            // 格式化日期数据
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            // 遍历输出每行
            PrintWriter pfp = new PrintWriter(file, "UTF-8"); //设置输出文件的编码为utf-8
            for (Object[] rowData : rows) {
                StringBuffer thisLine = new StringBuffer("");
                for (int i = 0; i < rowData.length; i++) {
                    Object obj = rowData[i]; // 当前字段

                    // 格式化数据
                    String field = "";
                    if (null != obj) {
                        if (obj.getClass() == String.class) { // 如果是字符串
                            field = (String) (obj+",0101"+"\n");
                        } else if (obj.getClass() == Double.class || obj.getClass() == Float.class) { // 如果是浮点型
                            field = formatter.format(obj); // 格式化浮点数,使浮点数不以科学计数法输出
                        } else if (obj.getClass() == Integer.class || obj.getClass() == Long.class
                                || obj.getClass() == Short.class || obj.getClass() == Byte.class) { // 如果是整形
                            field += obj;
                        } else if (obj.getClass() == Date.class) { // 如果是日期类型
                            field = sdf.format(obj);
                        }
                    } else {
                        field = " "; // null时给一个空格占位
                    }

                    // 拼接所有字段为一行数据，用tab键分隔
                    if (i < rowData.length - 1) { // 不是最后一个元素
                        thisLine.append(field).append("");
                    } else { // 是最后一个元素
                        thisLine.append(field);
                    }
                }
                pfp.print(thisLine.toString());
            }
            pfp.close();

        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 生成.csv格式文件,行数几乎无上限
     */
    public static boolean createCsvFile(List<Object[]> rows, String filePath, String fileName) {
        // 标记文件生成是否成功
        boolean flag = true;

        // 文件输出流
        BufferedWriter fileOutputStream = null;

        try {
            // 含文件名的全路径
            String fullPath = filePath + File.separator + fileName + ".csv";

            File file = new File(fullPath);
            if (!file.getParentFile().exists()) { // 如果父目录不存在，创建父目录
                file.getParentFile().mkdirs();
            }
            if (file.exists()) { // 如果已存在,删除旧文件
                file.delete();
            }
            file = new File(fullPath);
            file.createNewFile();

            // 格式化浮点数据
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMaximumFractionDigits(10); // 设置最大小数位为10

            // 格式化日期数据
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            // 实例化文件输出流
            fileOutputStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "GB2312"), 1024);

            // 遍历输出每行
            Iterator<Object[]> ite = rows.iterator();
            while (ite.hasNext()) {
                Object[] rowData = (Object[]) ite.next();
                for (int i = 0; i < rowData.length; i++) {
                    Object obj = rowData[i]; // 当前字段
                    // 格式化数据
                    String field = "";
                    if (null != obj) {
                        if (obj.getClass() == String.class) { // 如果是字符串
                            field = (String)obj;
                        } else if (obj.getClass() == Double.class || obj.getClass() == Float.class) { // 如果是浮点型
                            field = formatter.format(obj); // 格式化浮点数,使浮点数不以科学计数法输出
                        } else if (obj.getClass() == Integer.class || obj.getClass() == Long.class
                                || obj.getClass() == Short.class || obj.getClass() == Byte.class) { // 如果是整形
                            field += obj;
                        } else if (obj.getClass() == Date.class) { // 如果是日期类型
                            field = sdf.format(obj);
                        }
                    } else {
                        field = " "; // null时给一个空格占位
                    }
                    // 拼接所有字段为一行数据
                    if (i < rowData.length - 1) { // 不是最后一个元素
                        fileOutputStream.write(field + "\r");
                    } else { // 是最后一个元素
                        fileOutputStream.write(field + "\r");
                    }
                }
                // 创建一个新行
                if (ite.hasNext()) {
                    fileOutputStream.newLine();
                }
            }
            fileOutputStream.flush();
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }
    
    public static void main(String[] args) {
    	final int max = 200000;
    	final String tBatchno = "20180410001";
    	//这部分可分别按年月日提数
//    	String year = "2018";
//    	String month = "05";
//    	int day = 0;
//    	String lastno = "001";
//    	for (int a = 1;a <= 9;a++){
//    	String tBatchno = year+month+"0"+String.valueOf(day+a)+lastno;
    	System.out.println("tBatchno:"+tBatchno);
    	HashMap<String,String> tHashMap = new HashMap<String,String>(); 
    	tHashMap.put("0","0101");
    	tHashMap.put("1","0102");
    	tHashMap.put("2","0103");
    	tHashMap.put("3","0106");
    	tHashMap.put("4","0115");
    	tHashMap.put("5","0104");
    	tHashMap.put("6","0108");
    	tHashMap.put("7","0115");
    	//这个for循环是按分组提数
    	//for(int groupno = 1; groupno <=36; groupno++ ){
	    	MD5new md5 = new MD5new();
	    	List<Object[]> list = new ArrayList<Object[]>();
	    	//String sql="select * from iac_ldperson where groupno='"+groupno+"' fetch first "+max+" rows only with ur";
	    	String sql="select * from iac_ldperson where batchno = '"+tBatchno+"' fetch first "+max+" rows only with ur";
	    	String entity[] = new String[max];
	    	IAC_LDPERSONSet tIAC_LDPERSONSet = new IAC_LDPERSONSet();
			RSWrapper tRSWrapper = new RSWrapper();
			tRSWrapper.prepareData(tIAC_LDPERSONSet, sql);
			int j =0;
			do
			{
				tRSWrapper.getData();
				for(int i =1;i<=tIAC_LDPERSONSet.size();i++)
				{
					j ++;
					IAC_LDPERSONSchema tIAC_LDPERSONSchema=tIAC_LDPERSONSet.get(i);
					String tIdNo=tIAC_LDPERSONSchema.getIdNo();
		    		entity[j-1] = md5.toDigest(md5.toDigest(tIdNo.trim().toUpperCase()).toUpperCase()+"BDYZEQ".toUpperCase()).toUpperCase();
		    		System.out.println("添加第"+j+"成功!");
				}
			}while(tIAC_LDPERSONSet.size()>0);
	    	list.add(entity);
	    	//生成.txt 格式文件后放入保协提供的.cvs文件中去
//	    	createTxtFile(list, "D:\\保单验真提数", String.valueOf(groupno));
	    	createTxtFile(list, "D:\\保单验真提数", tBatchno);
	    	System.out.println("处理完成！");
    	//}
	}
//    }

}