package test;

import java.util.Random;

public class RedPackage {
	int remainSize;
	double remainMoney;

	public static double getRandomMoney(RedPackage _redPackage) {
		// remainSize 剩余的红包数量
		// remainMoney 剩余的钱
		if (_redPackage.remainSize == 1) {
			_redPackage.remainSize--;
			return (double) Math.round(_redPackage.remainMoney * 100) / 100;
		}
		Random r = new Random();
		double min = 0.01; //
		double max = _redPackage.remainMoney / _redPackage.remainSize * 2;
		double money = r.nextDouble() * max;
		money = money <= min ? 0.01 : money;
		money = Math.floor(money * 100) / 100;
		_redPackage.remainSize--;
		_redPackage.remainMoney -= money;
		return money;
	}

	public static void main(String[] args) {
		RedPackage redpackage = new RedPackage();
		redpackage.remainMoney = 5;
		redpackage.remainSize = 20;
		Double sum = 0.0;
		for (int i = 0; i < 20; i++) {
			Double a = getRandomMoney(redpackage);
			System.out.println(a);
			sum = sum + a;
		}
		System.out.println(sum);
	}
}
