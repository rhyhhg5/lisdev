package test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.cbsws.xml.ctrl.complexpds.WxUW;
import com.certicom.net.ssl.a.c;
import com.certicom.net.ssl.a.p;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPersonTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.tb.RelatedTransactionBL;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

import examples.post;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utils.system;

public class Test {

	static LCAddressSchema schema = new LCAddressSchema();
	public CErrors mErrors = new CErrors();
	private FTPTool tFTPTool;
	
	 // 定义连接所需的字符串
    // 192.168.0.X是本机地址(要改成自己的IP地址)，1521端口号，XE是精简版Oracle的默认数据库名
    private static String USERNAMR = "cbspdp";
    private static String PASSWORD = "cbspdp";
    private static String DRVIER = "oracle.jdbc.OracleDriver";
    private static String URL = "jdbc:oracle:thin:@10.136.5.125:1521:orcl";
    private MMap resultMMap = new MMap();
    
    private static  LBPolSet mLCPolSet;

    // 创建一个数据库连接
    Connection connection = null;
    // 创建预编译语句对象，一般都是用这个而不用Statement
    PreparedStatement pstm = null;
    // 创建一个结果集对象
    ResultSet rs = null;
    
    public static  int i;
    public static  String s;

	public static void main(String[] args) throws Exception {
//		 String checkSQL = "select ScanCheckFlag from lccontsub where prtno = 'PD10000000815' and exists(select 1 from lccont where prtno = lccontsub.prtno and losttimes < 1 ) with ur";
//			SSRS result = new ExeSQL().execSQL(checkSQL);
//			if(result.getMaxRow()>0){
//			if (!result.GetText(1,1).equals("1")) {
//			System.out.println(111);
//				}
//			}
//		String idno = "Z1231321";
//		if(idno.matches("^[\\d|[A-Z]]*$")){
//			System.out.println("111");
//		}else{
//			System.out.println("222");
//		}
		
//		System.out.println("/d");
		//modify by zxs 删除发送报文
//		String filePath = "E:/3/7/104753000104.xml";
		//String filePath =requestURL + mBatchNo + ".xml";
//		FileOutputStream fos = new FileOutputStream(new File(filePath));
//		 System.out.println("乘法口诀表：");
//	        for(int i=1;i<=9;i++)
//	        {
//	            for(int j=1;j<=i;j++)
//	            {
//	                System.out.print(j+"*"+i+"="+j*i+"\t");
//	            }
//	            System.out.println();
//	        }
		
//		System.out.println(PubFun.getInsuredAppAge("2019-03-01", "1988-02-29")); 
		
//		  VData tVData = new VData();
//		  tVData.add("TB1028");
//		  tVData.add("TB1027");
//		  
//		  System.out.println((String)tVData.getObject(0));
//		  System.out.println((String)tVData.getObject(1));
		
//		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
//		System.out.println("classLoader="+classLoader);
		
//		String str = "AaAa··阿道·Aaa";
//		String pattern ="^[\u4E00-\u9FA5A-Za-z][\u4E00-\u9FA5A-Za-z·]+[\u4E00-\u9FA5A-Za-z]$";
////		String parrern = "^[\u4e00-\u9fa5]+(·[\u4e00-\u9fa5]+)*$";
//		Pattern r = Pattern.compile(pattern);
//		Matcher m = r.matcher(str);
//		System.out.println(m.matches());
		
//		String checkSQL = "select ScanCheckFlag from lccontsub where prtno ='PD00000006778' ";
//    	SSRS result = new ExeSQL().execSQL(checkSQL);
//    	if(result.getMaxRow()>0){
//    		if (!result.GetText(1,1).equals("1")){
//    			System.out.println(result.GetText(1,1));
//    			System.out.println(1111);
//    		}
//    	}
//		StrTool.isGBKString("·");
		
//		//延时执行任务
//		Timer timer = new Timer();
////		timer.schedule(task(任务), delay(延时时间));
//		timer.schedule(new TimerTask() {	
//			@Override
//			public void run() {
//				
//				System.out.println("定时计划任务执行");
//			}
//		}, 10000);
//		
//		//重复执行
////		timer.schedule(task(任务), delay(延时时间), period(重复周期));
//		timer.schedule(new TimerTask() {
//			@Override
//			public void run() {
//			System.out.println("指定的任务从指定的延迟开始重复性执行任务");
//				
//			}
//		}, 1000, 1000);
//		
//		//任务从某一固定时间点后周期重复性执行
////		timer.schedule(task(任务), firstTime(固定时间点), period(重复周期));
//		timer.schedule(new TimerTask() {	
//			@Override
//			public void run() {
//				System.out.println("任务从某一时间点后延时重复执行");
//				
//			}
//		}, new Date(), 1000);
//		
//		//任务从某一时间点后执行一次
////		timer.schedule(task(任务), time(固定时间点));
//		timer.schedule(new TimerTask() {	
//			@Override
//			public void run() {
//				System.out.println("任务从某一固定时间点后执行一次");	
//			}
//		}, new Date());
//		
//		//任务延时重复性速率执行
////		timer.scheduleAtFixedRate(task, delay, period);
//		timer.scheduleAtFixedRate(new TimerTask() {	
//			@Override
//			public void run() {
//			  System.out.println("任务延时重复性速率执行！");
//				
//			}
//		}, 1000, 1000);
//		
//		//任务指定某个时间点后重复性速率执行
////		timer.scheduleAtFixedRate(task, firstTime, period);
//	    timer.scheduleAtFixedRate(new TimerTask() {
//			@Override
//			public void run() {
//				System.out.println("任务指定某个时间点后重复性速率执行");
//				
//			}
//		}, new Date(), 1000); 
//	    
//	    String str = " 123";
//	    System.out.println(str.trim());
	    
//	    CommonBL.isBriefContPrtNoOfRule("161904300004", "6");
		
//		LCPersonTraceSchema appntLcPersonTrace  = new LCPersonTraceSchema();
//		LCPersonTraceSchema insuredLcPersonTrace  = new LCPersonTraceSchema();
//		
//		appntLcPersonTrace.setAuthorization("1");
//		appntLcPersonTrace.setAuthType("1.0");
//		appntLcPersonTrace.setCustomerContact("微信税优");
//		appntLcPersonTrace.setSharedMark("1");
//		appntLcPersonTrace.setBusinessLink("承保");
//		
//		insuredLcPersonTrace.setAuthorization("1");
//		insuredLcPersonTrace.setAuthType("1.0");
//		insuredLcPersonTrace.setCustomerContact("微信税优");
//		insuredLcPersonTrace.setSharedMark("1");
//		insuredLcPersonTrace.setBusinessLink("承保");
//		
//		if(appntLcPersonTrace.equals(insuredLcPersonTrace)){
//			System.out.println("122333");
//		}
		
//		VData vData = new VData();
//		 String tybt ="";
//		TransferData mTransferData = new TransferData();
//		//mTransferData.setNameAndValue("YBT", "YBT");
//		vData.add(mTransferData);
//		 TransferData tTransferData = (TransferData) vData
//                 .getObjectByObjectName("TransferData", 0);
//		    tybt = (String) tTransferData.getValueByName("YBT");
//		    if(!"ybt".equals(tybt)){
//		    	System.out.println("123");
//		    }
//		   System.out.println(tybt);
			
//		
//		int i = 123 ;
//		Integer j = 123 ;
//		Integer r = 123;
//		Integer k = new Integer(123);
//		Integer a = new Integer(129);
//		System.out.println(k==r);

//	    HttpClient client = new HttpClient();
//	    String OrderId_url ="http://api.t.sina.com.cn/";
//	    GetMethod get = new GetMethod(OrderId_url);   
//	    int status = client.executeMethod(get);
//	    String res = get.getResponseBodyAsString().trim();
//	    System.out.println("返回回来的数据："+res+ "  状态值：" + status);
//	    //返回回来的数据：[{"url_short":"http://t.cn/RONu69s","url_long":"http://dev7.89t.cn/ebusiness/wx/showWxOrderDetail.do?orderId=12451","type":0}]  状态值：200                      
//	    JSONArray result=JSONArray.fromObject(res);//转json对象
//	    JSONObject getJsonObj = result.getJSONObject(0);
//	    String url_short = getJsonObj.getString("url_short");

//
//		String str = "86320500";
//		System.out.println(str.indexOf("863205"));
		
//		HttpClient client = new HttpClient();
//
//        PostMethod method = new PostMethod("http://10.136.4.130:8180/RecordDev/DemoAction?type=2&value=PD21000009426");
//        
//            method.addRequestHeader(new Header("Content-Type", "application/x-www-form-urlencoded;charset=utf-8") );    
//            int result = client.executeMethod(method);
//            System.out.println("result="+result);
//            if (HttpStatus.SC_OK == result) {
//    			String responseXml = method.getResponseBodyAsString();
//    			System.out.println("响应报文：" + responseXml);
//    			if("[]".equals(responseXml)){
//    				System.out.println("该保单双录质检未通过！");
//    			}else{
//    				JSONArray resultArray=JSONArray.fromObject(responseXml);//转json对象
//        		    JSONObject jsonObject = resultArray.getJSONObject(0);
//        			System.out.println(jsonObject.get("auditingresult"));	
//    			}
//    			
//    			
//    		} else {
//    			throw new Exception("通信异常，返回码为：" + result);
//    		}
		
//	        SimpleDateFormat sDateFormat=new SimpleDateFormat("yyyy-MM-dd"); //加上时间
//
//			Date  baseDate =sDateFormat.parse("1985-08-05") ;
//			 int interval = 106;
//			String unit = "Y";
//			Date compDate = sDateFormat.parse("2019-07-23");
//		
//			Date payEndDate = PubFun.calDate(baseDate, interval, unit, compDate);
//			
//			System.out.println(payEndDate);
//		
//		String agentCom = new ExeSQL().getOneValue("select agentcom from lccont where prtno = 'XG000081435'");
//		
//		if(agentCom.indexOf("PY028")!=0){
//			System.out.println("11111");
//		}else{
//			System.out.println("22222");
//		}
		
		
//		   // 计算
//        Calculator mCalculator = new Calculator();
//        mCalculator.setCalCode("111111");
//        //增加基本要素
//
//        mCalculator.addBasicFactor("Amnt", "60");
//        mCalculator.addBasicFactor("OccuType", "6");
//        mCalculator.addBasicFactor("Age", "45");
//        String tStr = "";
//        tStr = mCalculator.calculate();
//        
//        Double mValue;
//		if (tStr == null || tStr.trim().equals("")) {
//            mValue = 0.00;
//        } else {
//            mValue = Double.parseDouble(tStr);
//        }
//
//        //System.out.println(mValue);
//        System.out.println(PubFun.setPrecision(mValue, "0.00")) ;
		
		LCAppntDB tAppntDB = new LCAppntDB();
		tAppntDB.setContNo("111");
		LCAppntSet tAppntSet = tAppntDB.query();
		
		
		
		
		
	}
	
	public static String checkResult(String prtno) throws Exception{
		HttpClient client = new HttpClient();
		String address = new ExeSQL().getOneValue("select sysvarvalue from LDSYSVAR  where Sysvar='DoubleResult'");
		String url = address+"?type=2&value="+prtno+"";
		PostMethod method = new PostMethod(url);
		//"http://10.136.4.130:8180/RecordDev/DemoAction?type=2&value=PD21000009426"
		method.addRequestHeader(new Header("Content-Type", "application/x-www-form-urlencoded;charset=utf-8") );    
		int result = client.executeMethod(method);
		System.out.println("result="+result);
		if (HttpStatus.SC_OK == result) {
			String responseXml = method.getResponseBodyAsString();
			System.out.println("响应报文：" + responseXml);
			if("[]".equals(responseXml)){
				System.out.println("该保单双录质检未通过！");
				return "0";
			}else{
				JSONArray resultArray=JSONArray.fromObject(responseXml);//转json对象
				JSONObject jsonObject = resultArray.getJSONObject(0);
				System.out.println(jsonObject.get("auditingresult"));
				String jsonResult = (String) jsonObject.get("auditingresult").toString();
				return jsonResult;
			}
		} else {	
			throw new Exception("通信异常，返回码为：" + result);
		}

	}
	
	 /**
     * 获取Connection对象
     * 
     * @return
     */
    public Connection getConnection() {
        try {
            Class.forName(DRVIER);
            connection = DriverManager.getConnection(URL, USERNAMR, PASSWORD);
            System.out.println("成功连接数据库");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("class not find !", e);
        } catch (SQLException e) {
            throw new RuntimeException("get connection error!", e);
        }

        return connection;
    }

//	public MMap dealData() {
//		MMap map = new MMap();
//		map.put("name", "zxs");
//		
//		return map;
//	}
	private static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}
	
	
	public int getByteLen(String val) {
	    int len = 0;
	    for (int i = 0; i < val.length(); i++) {
	         char a = val.charAt(i);
	         String s = "[^/x00-/xff]";

//	         String s2 ="\x00-\xff";
	         
	         if (String.valueOf(a).matches(s)) 
	        {
	            len += 2;
	        }
	        else
	        {
	            len += 1;
	        }
	    }
	    return len;
	}
}
