package test;

public class MyThread implements Runnable {

	@Override
	public void run() {
		System.out.println("新启动线程"+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		
		for (int i=0;i<5;i++){
			MyThread thread  = new MyThread();
			Thread t= new Thread(thread);
			t.start();
			System.out.println("主线程"+Thread.currentThread().getName());
		}
	}
	
	public void test(){
		
	}

}
