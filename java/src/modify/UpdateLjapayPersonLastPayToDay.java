import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


public class UpdateLjapayPersonLastPayToDay {
	
    private MMap mMap = new MMap();
	
	private VData mResult = new VData();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
      new UpdateLjapayPersonLastPayToDay().dealData();
	}
	
	
	private boolean dealData()
	{
		if(getData())
		{
			submitMap() ;
		}
	
		 return true;
	}
	
	public boolean getData()
	{
		String getDataSql="select "
	                     +"b.actugetno,b.endorsementno,b.contno,b.polno,a.paytodate, " 
	                     +"(select edorvalidate from lpedoritem where edorno = b.endorsementno and edortype = b.feeoperationtype) "
	                     +"from lppol a ,ljagetendorse b  "
	                     +"where endorsementno = a.edorno and a.polno = b.polno "
	                     +"and edortype = 'FX' and feeoperationtype = 'FX' and feefinatype = 'BF' "
	                     +"and getmoney <>0 "
	                     +"and exists (select 1 from lmriskapp where riskperiod = 'L' and riskcode = a.riskcode) "
	                     +"and (select edorvalidate from lpedoritem where edorno = b.endorsementno and edortype = b.feeoperationtype) > paytodate + 1 year "
	//                     +" and a.contno='000913527000001'"
	                     +"with ur "
                     ;
                     

		SSRS getDataSqlSSRS =new ExeSQL().execSQL(getDataSql); 
		System.out.println("++++++"+getDataSqlSSRS.getMaxRow());
		if(getDataSqlSSRS!= null && getDataSqlSSRS.getMaxRow() > 0 )
		{
			for(int i=1;i< getDataSqlSSRS.getMaxRow()+1;i++)
			{
				String payno=getDataSqlSSRS.GetText(i, 1);
				String edorno=getDataSqlSSRS.GetText(i, 2);;
				String contNo=getDataSqlSSRS.GetText(i, 3);
				String polNo=getDataSqlSSRS.GetText(i, 4);
				String payToDay=getDataSqlSSRS.GetText(i, 5);
				
				String sqlLjaPayPerson="select * from ljapayperson where payno='"+payno
				                      +"' and contno='"+contNo
				                      +"' and polno='"+polNo+"'";
				LJAPayPersonDB tLJAPayPersonDB =new LJAPayPersonDB();				
				LJAPayPersonSet tLJAPayPersonSet=tLJAPayPersonDB.executeQuery(sqlLjaPayPerson);
				String sqlLPPol="select * from lppol where edorno='"+edorno+"' and contno='"+contNo+"' and polno='"+polNo+"' and edortype='FX' ";
				LPPolDB tLPPolDB=new LPPolDB();
				LPPolSet tLPPolSet=tLPPolDB.executeQuery(sqlLPPol);
				if(tLJAPayPersonSet.size()>0)
				{
					for(int j=1;j<=tLJAPayPersonSet.size();j++)
					{
						LJAPayPersonSchema tLJAPayPersonSchema= new LJAPayPersonSchema();
						tLJAPayPersonSchema=tLJAPayPersonSet.get(j);
						tLJAPayPersonSchema.setLastPayToDate(tLPPolSet.get(1).getPaytoDate());
						mMap.put(tLJAPayPersonSchema, "UPDATE");
					}
				}

			}
		}
			
		mResult.add(mMap);
		return true;
	}
	
	   private boolean submitMap() 
	    {        
	         PubSubmit tPubsubmit = new PubSubmit();
	         if(!tPubsubmit.submitData(mResult, SysConst.INSERT))
	         {
	         	System.out.println("提交数据失败！"); 
	         	return false;
	         }
	         this.mMap = new MMap();
	         System.out.println("提交数据成功！");
	         return true;
	     }  

}
