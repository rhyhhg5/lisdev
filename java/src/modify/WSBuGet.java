package modify;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class WSBuGet {
	private MMap mMap = new MMap();	
	private VData mResult = new VData();
	private String liContNo=null;
//	每个保单改一个
	private String mContNo=null;
	private String mInsuredNo=null;
	private LDPersonSchema mLDPersonSchema=null;
	private LCDutySet mLCDutySet=null;
	
	public boolean submitData()
	{
		RSWrapper rswrapper = new RSWrapper();
		String sql="select * from temp_texu_tishu ";
		rswrapper.prepareData(null, sql);
        SSRS tSSRS = null;
        do
        {
	            tSSRS = rswrapper.getSSRS();
		    for (int i=1;i<tSSRS.getMaxRow()+1;i++)
					{
			    	String contNo=tSSRS.GetText(i,1);
			    	//校验团单表是否存在
					if(!CheckGRPContNoByContno(contNo))
					{
						continue;
					}
					//缺失duty表中的保单号
					mContNo=contNo;
					LCPolDB tLCPolDB=new LCPolDB();
					tLCPolDB.setContNo(mContNo);
					LCPolSet tLCPolSet=tLCPolDB.query();
					if(tLCPolSet.size()==0)
					{
						continue;
					}
					
					String grpContNo=tLCPolSet.get(1).getGrpContNo();
					String mainPolNo=tLCPolSet.get(1).getMainPolNo();
					mInsuredNo=tLCPolSet.get(1).getInsuredNo();
		//			得到相应的无名单的lcpol的对应项
					LCPolDB grpLCPolDB=new LCPolDB();
					grpLCPolDB.setGrpContNo(grpContNo);
					grpLCPolDB.setPolTypeFlag("1");
					grpLCPolDB.setPolNo(mainPolNo);
					LCPolSet grpLCPolSet=grpLCPolDB.query();
					if(grpLCPolSet.size()==0)
					{
						System.out.println("团单无名单险种信息不存在");
						return false;
					}
					//例子的保单号
					liContNo=grpLCPolSet.get(1).getContNo();
					
					//处理数据
					if (!dealData())
			        {
			            return false;
			        }
				
					if(!submitMap())
					{
					    return false;
					}
				}
        } 
        while(tSSRS.getMaxRow()>0);
				
	        return true;
	}
	
//	 通过保单号校验团单号
	private boolean CheckGRPContNoByContno(String contNo)
	{
		String sqlGRPContNo="select grpcontno from lcgrpcont where grpcontno = (select grpContNo from LPCont where contNo='"+contNo+"' fetch first 1 row only) with ur";
		String GRPContNo =new ExeSQL().getOneValue(sqlGRPContNo);
		
        if(GRPContNo.equals(""))
        {
        	System.out.println("该个单对应的团单号不存在");
        	return false;
        }
        return true;
	}
	private boolean dealData()
	{
		mLDPersonSchema=getLDPersonSchema(mInsuredNo);
		mLCDutySet=getLCDutySet();
//		buLCCont();
//		buLCInsured();
//		buLCPrem();
		buLCGet();	  
		return true;
	}
	
	 private boolean submitMap() 
	    {   
		     mResult.clear();
		     mResult.add(mMap);
	         PubSubmit tPubsubmit = new PubSubmit();
	         if(!tPubsubmit.submitData(mResult, SysConst.INSERT))
	         {
	         	System.out.println("提交数据失败！"); 
	         	return false;
	         }
	         this.mMap = new MMap();
	         System.out.println("提交数据成功！");
	         return true;
	     }  
	private void buLCGet()
	{
		LCGetSet  tLCGetSet=getLCGetSet();
		LCGetSet zongLCGetSet=new LCGetSet();
		for(int i=1;i<=tLCGetSet.size();i++)
		{
			LCGetSchema tLCGetSchema=tLCGetSet.get(i);
			tLCGetSchema.setContNo(mContNo);
			for(int j=1;j<=mLCDutySet.size();j++)
			{
				//用lcduty表中的polno去填补lcget中的lcpolno
				if(mLCDutySet.get(j).getDutyCode().equals(tLCGetSchema.getDutyCode()))
				{
					tLCGetSchema.setPolNo(mLCDutySet.get(j).getPolNo());
					tLCGetSchema.setInsuredNo(mInsuredNo);
					tLCGetSchema.setContNo(mContNo);
					zongLCGetSet.add(tLCGetSchema);
				}
			}
		}
		mMap.put(zongLCGetSet, "INSERT");
		
	}
	private void buLCPrem()
	{
		LCPremSet  tLCPremSet=getLCPremSet();
		LCPremSet zongLCPremSet=new LCPremSet();
		for(int i=1;i<=tLCPremSet.size();i++)
		{
			LCPremSchema tLCPremSchema=tLCPremSet.get(i);
			tLCPremSchema.setContNo(mContNo);
			for(int j=1;j<=mLCDutySet.size();j++)
			{
				//用lcduty表中的polno去填补lcprem中的lcpolno
				if((mLCDutySet.get(j).getDutyCode().equals(tLCPremSchema.getDutyCode()))&&(mLCDutySet.get(j).getContNo().equals(tLCPremSchema.getContNo())))
				{
					tLCPremSchema.setPolNo(mLCDutySet.get(j).getPolNo());
					zongLCPremSet.add(tLCPremSchema);
				}
			}
		}
		mMap.put(zongLCPremSet, "INSERT");
		
	}
	
	
	private void buLCCont()
	{
		LCContSchema tLCContSchema=new LCContSchema();
		tLCContSchema=getLCContSchema();
		tLCContSchema.setContNo(mContNo);
		tLCContSchema.setInsuredBirthday(mLDPersonSchema.getBirthday());
		tLCContSchema.setInsuredNo(mLDPersonSchema.getCustomerNo());
		tLCContSchema.setInsuredName(mLDPersonSchema.getName());
		tLCContSchema.setInsuredSex(mLDPersonSchema.getSex());
		tLCContSchema.setInsuredIDType(mLDPersonSchema.getIDType());
		tLCContSchema.setInsuredIDNo(mLDPersonSchema.getIDNo());
		mMap.put(tLCContSchema, "INSERT");
	}
	
	private void buLCInsured()
	{
		LCInsuredSchema tLCInsuredSchema=new LCInsuredSchema();
		tLCInsuredSchema=getLCInsuredSchema();
		tLCInsuredSchema.setInsuredNo(mInsuredNo);
		tLCInsuredSchema.setContNo(mContNo);
		tLCInsuredSchema.setName(mLDPersonSchema.getName());
		tLCInsuredSchema.setSex(mLDPersonSchema.getSex());
		tLCInsuredSchema.setBirthday(mLDPersonSchema.getBirthday());
		tLCInsuredSchema.setIDType(mLDPersonSchema.getIDType());
		tLCInsuredSchema.setIDNo(mLDPersonSchema.getIDNo());
		mMap.put(tLCInsuredSchema, "INSERT");
	}
	private LCDutySet getLCDutySet()
	{
		String tSql="select * from LCDuty where contno in ('"+mContNo+"')";
		LCDutyDB tLCDutyDB = new LCDutyDB();
		return tLCDutyDB.executeQuery(tSql);
		
	}
	private LCPremSet getLCPremSet()
	{
		String tSql="select * from LCPrem where contno in ('"+liContNo+"')";
		LCPremDB tLCPremDB = new LCPremDB();
		return tLCPremDB.executeQuery(tSql);
		
	}
	private LCGetSet getLCGetSet()
	{
		String tSql="select * from LCGet where contno in ('"+liContNo+"')";
		LCGetDB tLCGetDB = new LCGetDB();
		return tLCGetDB.executeQuery(tSql);
		
	}
	
	
	
	private LDPersonSchema getLDPersonSchema(String customerNo)
	{
		String tSql="select * from LDPerson where customerNo in ('"+customerNo+"')"
                    +" order by makedate desc,maketime desc fetch first 1 row only ";
		LDPersonDB tLDPersonDB = new LDPersonDB();
		LDPersonSet tLDPersonSet=new LDPersonSet();
		tLDPersonSet = tLDPersonDB.executeQuery(tSql);
		if(tLDPersonSet!=null)
		{
			return tLDPersonSet.get(1);
		}
		return null;
		
	}
	
	private LCInsuredSchema getLCInsuredSchema()
	{
		String tSql="select * from LCinsured where contno in ('"+liContNo+"')";
		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		LCInsuredSet tLCInsuredSet=new LCInsuredSet();
		tLCInsuredSet = tLCInsuredDB.executeQuery(tSql);
		if(tLCInsuredSet!=null)
		{
			return tLCInsuredSet.get(1);
		}
		return null;
		
	}
	private LCContSchema getLCContSchema()
	{
		String tSql="select * from LCCont where contno in ('"+liContNo+"')" ;
		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet=new LCContSet();
		tLCContSet = tLCContDB.executeQuery(tSql);
		if(tLCContSet!=null)
		{
			return tLCContSet.get(1);
		}
		return null;
	} 
	
	public static void main(String[] args) {
         new WSBuGet().submitData();
	}
}
