package modify;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Date;

import com.sinosoft.lis.easyscan.ProposalDownloadBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LRComRiskResultSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import com.sinosoft.utility.StrTool;



public class SQLExecuteBL {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
   
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    
    private FileWriter mFileWriter ;
    private BufferedWriter mBufferedWriter ;
    private TransferData mTransferData = new TransferData();
    private String strOperate;
    private String mSQL;
    private String tSysPath;
    
    //业务处理相关变量
    /** 全局数据 */

    public SQLExecuteBL() {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        
        if(!check()){
        	return false;
        }
        //生成查询数据
        if(strOperate.equals("CREATE")){
            if (!createData()) {
                return false;
            }
        }
        //下载数据
        if(strOperate.equals("DOWNLOAD")){
           if (!downloadData()) {
               return false;
           }
       }
        //更新数据
        if(strOperate.equals("UPDATE")){
           if (!updateData()) {
               return false;
           }
       }
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
       
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if(mTransferData==null){
             buildError("getInputData().mTransferData","数据信息不足");
            return false;
        }
        
        mSQL = (String)mTransferData.getValueByName("SQL");
        tSysPath = (String) mTransferData.getValueByName("SysPath"); //获取地址;
        if(StrTool.cTrim(mSQL).equals("") || StrTool.cTrim(mSQL).equals("")){
        
           buildError("getInputData","传入的语句出错");
           return false;
       }
        else{
        	mSQL=mSQL.replaceAll(";", "");
        	mSQL=mSQL.replaceAll("；", "");
        	mSQL=mSQL.replaceAll("？", "?");
        	mSQL=mSQL.replaceAll("‘", "\\\'");
        	mSQL=mSQL.replaceAll("’", "\\\'");
        	
        }
        return true;
    }
    
    private boolean check(){

		String upperSql = mSQL.toUpperCase();
		if(strOperate.equals("CREATE")){
			if ( upperSql.indexOf("UPDATE") > -1) {
				return false;
			}
			if ( upperSql.indexOf("INSERT") > -1) {
				return false;
			}
			if ( upperSql.indexOf("DELETE") > -1) {
				return false;
			}
			if ( upperSql.indexOf("SELECT") > -1) {
				return true;
			}
			else
				return false;
        }
		if(strOperate.equals("UPDATE")){
			if ( upperSql.indexOf("UPDATE") > -1) {
				return true;
			}
			else if ( upperSql.indexOf("INSERT") > -1) {
				return true;
			}
			else if ( upperSql.indexOf("DELETE") > -1) {
				return true;
			}
			
			else
				return false;
        }
		
		
		
		
	
    	return true;
    }
    /**
     * 查询语句生成文件
     * @return
     */
    private boolean createData(){
    	 
         tSysPath +="/vtsfile/";
         String tPath="";
         try{
        	 //tPath = PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
        	 tPath="SQLResult";
             mFileWriter = new FileWriter(tSysPath+tPath+".txt");
             mBufferedWriter = new BufferedWriter(mFileWriter);
         }catch(Exception ex1){
             ex1.printStackTrace();
         }
         int start = 1;
         int nCount = 10000;
           while (true) {
                SSRS tSSRS = new ExeSQL().execSQL(mSQL, start, nCount);
                int count=0;
                if (tSSRS.getMaxRow() <= 0) {
                     break;
                 }else{
                     count=tSSRS.getMaxRow();
                 }
               if (tSSRS != null && count > 0) {
                 for (int i = 1; i <= count; i++) {
                   try{
                       String result="";
                       for(int m=1;m<=tSSRS.getMaxCol();m++){
                             result += tSSRS.GetText(i,m)+" || ";
                       }
                        mBufferedWriter.write(result+"\r\n");
                        mBufferedWriter.flush();
                    } catch (IOException ex2) {
                           ex2.printStackTrace();
                     }
                    }
                }
                start += nCount;
              }
            try {
               mBufferedWriter.close();
             } catch (IOException ex3) {
             }
             String[] FilePaths = new String[1];
             FilePaths[0] = tSysPath+tPath+".txt";
             String[] FileNames = new String[1];
             FileNames[0] =tPath+".txt";
             String newPath = tSysPath +tPath+".zip";
             String FullPath = tPath+".zip";
             CreateZip(FilePaths,FileNames,newPath);
             try{
                 File fd = new File(FilePaths[0]);
                 fd.delete();
             }catch(Exception ex){
                 ex.printStackTrace();
             }
             mResult.add(FullPath);
    	return true;
    }
    
    private boolean downloadData(){
    	return true;
    }
    
    /**
     * 更改插入语句
     * @return
     */
    private boolean updateData(){
    	 ExeSQL tExeSQL = new ExeSQL();
  		
    	 if(!tExeSQL.execUpdateSQL(mSQL)){
    		 buildError("updateData","传入的更新插入语句出错");
    		 return false;
			 }
    	return true;
    }
    
    //生成压缩文件
  public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                           String tZipPath) {
      ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
      if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
          System.out.println("生成压缩文件失败");
          CError.buildErr(this, "生成压缩文件失败");
          return false;
      }
      return true;
  }
  
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "SQLExecuteBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public VData getResult()
    {
        return this.mResult;
    }
}
