package chat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * 利用UDP实现聊天功能
 * @author Administrator
 *
 */
public class UdpChatServer {
    public static void main(String[] args) {
        System.out.println("---------客服---------");
        try {
            //1.创建数据报套接字
            DatagramSocket socket = new DatagramSocket(8888);
            Scanner input = new Scanner(System.in);
            while(true){
                //接收数据
                byte[] bs2 = new byte[1024];
                DatagramPacket packet2 = new DatagramPacket(bs2, bs2.length);
                socket.receive(packet2);//接收数据
                byte[] serverMesage = packet2.getData();
                String str=new String(serverMesage,0,serverMesage.length);
                System.out.println("顾客说:"+str);
                
                //2.获取用户输入
                String message = input.next();
                byte[] bs = message.getBytes();
                //3.创建数据报包
                DatagramPacket packet = new DatagramPacket(bs, bs.length, InetAddress.getByName("127.0.0.1"),6666);
                //4.发送数据
                socket.send(packet);
                if(message.equals("bye")){
                    break;
                }
            }
            //释放资源
            socket.close();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
}