/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package dzsw;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAComECTypeSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LICertifyImportLogSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LAComECTypeSet;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:单证管理回收操作
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author kevin
 * @version 1.0
 */
public class InterBL {
	// 错误处理类，每个需要错误处理的类中都放置该类
	public CErrors mErrors = new CErrors();

	/* 业务相关的数据 */
	private LAComECTypeSet mLAComECTypeSet = new LAComECTypeSet();
	private VData mResult = new VData();
	private GlobalInput globalInput = new GlobalInput();
	private String mszOperate = "";

	// 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。

	public InterBL() {
	}

	public static void main(String[] args) {
	}

	// 传输数据的公共方法
	public boolean submitData(VData cInputData, String cOperate) {
		mszOperate = cOperate;
		if (mszOperate.equals("")) {
			buildError("verifyOperate", "不支持的操作字符串");
			return false;
		}

		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			return false;
		}

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	// 根据前面的输入数据，进行逻辑处理
	// 如果在处理过程中出错，则返回false,否则返回true
	private boolean dealData() {
		boolean flag = true;
		MMap tMMap = new MMap();
		String tErrorInfo = "";
		for (int i = 1; i <= mLAComECTypeSet.size(); i++) {
			LAComECTypeSchema tLAComECTypeSchema = mLAComECTypeSet.get(i);
			String tSQL = "";
			LAComECTypeSchema tLAComECTypeSchema1 = new LAComECTypeSchema();

			if ("Guishu".equals(mszOperate)) {
				tErrorInfo = "归属：" + tLAComECTypeSchema.getAgentCom();
				tLAComECTypeSchema.setAgentComType("01");

				tSQL = "update LAComECType set AgentComType='03'  where agentcom= '"
						+ tLAComECTypeSchema.getAgentCom() + "'";
				tMMap.put(tSQL, SysConst.UPDATE);
			} else if ("QGuishu".equals(mszOperate)) {
				tErrorInfo = "取消归属：" + tLAComECTypeSchema.getAgentCom();
				tLAComECTypeSchema.setAgentComType("02");
				tSQL = "update LAComECType set AgentComType ='04'  where agentcom= '"
						+ tLAComECTypeSchema.getAgentCom() + "'";
				tMMap.put(tSQL, SysConst.UPDATE);
			}

			// 下面这个是否就是登陆人员的操作日志信息
			String aSerNo = PubFun1.CreateMaxNo("STOPWRAP", "");
			tLAComECTypeSchema1.setSerNo(aSerNo);
			tLAComECTypeSchema1.setAgentCom(tLAComECTypeSchema.getAgentCom());
			tLAComECTypeSchema1.setSalechnl(tLAComECTypeSchema.getSalechnl());
			tLAComECTypeSchema1.setAcType(tLAComECTypeSchema.getAcType());
			tLAComECTypeSchema1.setAgentComType(tLAComECTypeSchema.getAgentComType());
			tLAComECTypeSchema1.setManageCom(globalInput.ManageCom);
			tLAComECTypeSchema1.setOperator(globalInput.Operator);
			tLAComECTypeSchema1.setMakeDate(PubFun.getCurrentDate());
			tLAComECTypeSchema1.setMakeTime(PubFun.getCurrentTime());
			tLAComECTypeSchema1.setModifyDate(PubFun.getCurrentDate());
			tLAComECTypeSchema1.setModifyTime(PubFun.getCurrentTime());
			tMMap.put(tLAComECTypeSchema1, SysConst.INSERT);
		}
		// 保存数据
		VData data = new VData();
		data.add(tMMap);

		PubSubmit p = new PubSubmit();
		if (!p.submitData(data, "")) {
			System.out.println("提交数据失败");
			buildError("submitData", "提交数据失败");
			return false;
		}
		tMMap = null;
		return flag;
	}

	// 从输入数据中得到所有对象
	// 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	private boolean getInputData(VData vData) {

		globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
				"GlobalInput", 0));
		mLAComECTypeSet.set((LAComECTypeSet) vData.getObjectByObjectName(
				"LAComECTypeSet", 0));
		if (mLAComECTypeSet == null || mLAComECTypeSet.size() <= 0) {
			buildError("getInputData", "获取中介机构数据失败！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "CertifyTakeBackUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
}
