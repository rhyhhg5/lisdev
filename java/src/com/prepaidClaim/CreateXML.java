package com.prepaidClaim;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.contract.obj.MessageHead;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class CreateXML {

	/**
	 * 
	 * 生成返回报文，errorValue为收集的处理信息
	 * 
	 * @param data
	 *            报文数据
	 * @param errorValue
	 *            处理时的信息
	 * @return
	 * @throws Exception
	 */
	public static String createXML(VData data, String errorValue, String xml)
			throws Exception {
		System.out.println("***********开始createXML生成返回报文*********");
		ExeSQL tExeSQL = new ExeSQL();
		MessageHead vMessHead = (MessageHead) data.getObjectByObjectName(
				"MessageHead", 0);
		LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表

		mLCGrpContSchema = (LCGrpContSchema) data.getObjectByObjectName(
				"LCGrpContSchema", 0);
		System.out.println("错误信息：" + errorValue);
		Element root = new Element("DataSet");
		Element eMsgHead = new Element("head");
		Element eHeadnode = new Element("Item");
		Element eHeadBatchNo = new Element("BatchNo");
		eHeadBatchNo.setText(vMessHead.getBatchNo());
		eHeadnode.addContent(eHeadBatchNo);
		Element eHeadSendDate = new Element("SendDate");
		eHeadSendDate.setText(vMessHead.getSendDate());
		eHeadnode.addContent(eHeadSendDate);
		Element eHeadSendTime = new Element("SendTime");
		eHeadSendTime.setText(vMessHead.getSendTime());
		eHeadnode.addContent(eHeadSendTime);
		Element eHeadBranchCode = new Element("BranchCode");
		eHeadBranchCode.setText(vMessHead.getBranchCode());
		eHeadnode.addContent(eHeadBranchCode);
		Element eHeadSendOperator = new Element("SendOperator");
		eHeadSendOperator.setText(vMessHead.getSendOperator());
		eHeadnode.addContent(eHeadSendOperator);
		Element eHeadMsgType = new Element("MsgType");
		eHeadMsgType.setText(vMessHead.getMsgType());
		eHeadnode.addContent(eHeadMsgType);

		if (!"".equals(errorValue) && null != errorValue) { // 返回报文失败--提示错误信息
			Element eHeadState = new Element("State");
			eHeadState.setText("01");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrCode = new Element("ErrCode"); // 错误信息代码
			Element eHeadErrInfo = new Element("ErrInfo"); // 错误信息
			eHeadErrInfo.setText(errorValue);
			eHeadnode.addContent(eHeadErrCode);
			eHeadnode.addContent(eHeadErrInfo);
		} else {
			Element eHeadState = new Element("State");
			eHeadState.setText("00");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrInfo = new Element("ErrInfo");
			eHeadErrInfo.setText("操作成功！");
			eHeadnode.addContent(eHeadErrInfo);
		}

		Element ebody = new Element("body");
		Element eOutput = new Element("Output");
		Element eManageCom = new Element("ManageCom");
		Element ePrepaidNo = new Element("PrepaidNo");
		Element eActugetNo = new Element("ActugetNo");
		String GrpContNo = mLCGrpContSchema.getGrpContNo();

		String PrepaidNoSQL = "select * from llprepaidclaim where grpcontno='"+GrpContNo+"' and rgttype='1'";
		SSRS arr3 = tExeSQL.execSQL(PrepaidNoSQL);
		int i = arr3.getMaxRow();
			String PrepaidNo = arr3.GetText(i, 1);
			ePrepaidNo.setText(PrepaidNo);
			String ActugetnoSQL = "select Actugetno from ljaget where otherno ='"
					+ PrepaidNo + "' AND Othernotype = 'Y'";
			SSRS ARR4 = tExeSQL.execSQL(ActugetnoSQL);
			String ActugetNo;
			ActugetNo = ARR4.GetText(1, 1);
			eActugetNo.setText(ActugetNo);
		
		ebody.addContent(eOutput);
		eManageCom.setText(mLCGrpContSchema.getManageCom());
		eOutput.addContent(eManageCom);
		eOutput.addContent(ePrepaidNo);
		eOutput.addContent(eActugetNo);
		eMsgHead.addContent(eHeadnode);
		root.addContent(eMsgHead);
		root.addContent(ebody);
		XMLOutputter out = new XMLOutputter();
		out.setEncoding("GBK");
		Document doc = new Document(root);
		String responseXml = out.outputString(doc);
		return responseXml;
	}
}
