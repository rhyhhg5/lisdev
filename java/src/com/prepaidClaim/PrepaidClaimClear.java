package com.prepaidClaim;

import utils.system;

import com.sinosoft.lis.llcase.LLPrepaidClaimConfirmUI;
import com.sinosoft.lis.llcase.LLPrepaidClaimInputUI;
import com.sinosoft.lis.llcase.LLPrepaidClaimUnderWriteUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimDetailSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimSchema;
import com.sinosoft.lis.schema.LLPrepaidUWMainSchema;
import com.sinosoft.lis.vschema.LLPrepaidClaimDetailSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class PrepaidClaimClear {
	public static void main(String[] args) {
		PrepaidClaimClear pcc = new PrepaidClaimClear();
		VData vdata = new VData();

		pcc.dealData(vdata);

	}

	// 错误的信息
	String Content;
	String ManageCom; // 管理机构
	String GrpName; // 投保单位名称
	String GrpContNo; // 团体保单号
	String BankCode; // 银行编码
	double money;
	double SumPrem;
	String RiskCode;
	String BankAccNo;
	String PayMode;
	String AccName;
	String PrepaidNo;
	String RgtType;
	boolean chkFlag;

	LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
	LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
	LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();// 预付赔款信息表
	// LLPrepaidClaimSet mLLPrepaidClaimSet = new LLPrepaidClaimSet();
	// LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new
	// LLPrepaidClaimDetailSchema();
	LLPrepaidClaimDetailSet mLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
	CErrors tError = null;
	ExeSQL tExeSQL = new ExeSQL();

	public String getContent() {
		return Content;
	}

	public boolean dealData(VData vdata) {
		System.out.println("**************************************预付赔款清账********************************");
//		System.out.println("**************************************");
		
		if (!getInputData(vdata)) {
			return false;
		}
		
		if (!ClearCheck()) {
			return false;
		}
		
		if(!approveCheck()){
			return false;
		}
		if(!informCheck()){
			return false;
		}
		return true;
	}

	private boolean getInputData(VData vdata) {
		mLCGrpContSchema = null;
		mLLPrepaidClaimSchema = null;
		// mLLPrepaidClaimSet = null;
		mLLPrepaidClaimDetailSet = null;

		mLCGrpContSchema = (LCGrpContSchema) vdata.getObjectByObjectName(
				"LCGrpContSchema", 0);

		mLLPrepaidClaimSchema = (LLPrepaidClaimSchema) vdata
				.getObjectByObjectName("LLPrepaidClaimSchema", 0);
		// mLLPrepaidClaimSet = (LLPrepaidClaimSet)
		// vdata.getObjectByObjectName("LLPrepaidClaimSet", 0);

		mLLPrepaidClaimDetailSet = (LLPrepaidClaimDetailSet) vdata
				.getObjectByObjectName("LLPrepaidClaimDetailSet", 0);

		return true;
	}

	/**
	 * 清账前台，后台逻辑处理
	 * @return
	 */
	public boolean ClearCheck() {
		System.out
				.println("=================================预付赔款清账前台页面===================");
		ExeSQL tExeSQL = new ExeSQL();
		 ManageCom = mLCGrpContSchema.getManageCom();
		 GrpName = mLCGrpContSchema.getGrpName();
		 GrpContNo = mLCGrpContSchema.getGrpContNo();
		 BankCode = mLLPrepaidClaimSchema.getBankCode();

//		GrpContNo = "00003112000107";
//		ManageCom = "86110000";
//		GrpName = "营业四部555";

		// 页面预付赔款查询按钮 对应的
		if (ManageCom == null || ManageCom == "") {
			Content = ("请选择管理机构");
			return false;
		}

		if ((GrpName == null || GrpName == "")
				&& (GrpContNo == null || GrpContNo == "")) {
			Content = ("请录入投保单位名称或团体保单号");
			return false;
		}

		String GrpContSQL = " SELECT b.managecom, b.grpcontno, b.grpname, b.cvalidate, b.cinvalidate, a.applyamount+a.regainamount "
				+ " FROM llprepaidgrpcont a, lcgrpcont b "
				+ " WHERE a.grpcontno=b.grpcontno AND b.managecom LIKE '"
				+ ManageCom
				+ "%'"
				+ " AND a.state='1' "
				+ "and b.GrpContNo = '" + GrpContNo + "'";
		SSRS arr1 = tExeSQL.execSQL(GrpContSQL);
		String gGrpContNo = arr1.GetText(1, 2);

		// 页面选中保单号后生成的险种信息
		String GrpPolSQL = " SELECT a.riskcode, b.riskname, "
				+ " (decimal(NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
				+ " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
				+ " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0),12,2)), "
				+ " a.applyamount+a.regainamount, "
				+ " (SELECT NVL(SUM(pay),0) FROM ljagetclaim WHERE othernotype='5' AND grpcontno=a.grpcontno AND grppolno=a.grppolno), "
				+ " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
				+ " AND m.rgtstate not in ('11','12') AND n.grpcontno=a.grpcontno AND n.grppolno=a.grppolno), "
				+ " a.prepaidbala, a.prepaidbala, a.grppolno "
				+ " FROM llprepaidgrppol a,lmrisk b "
				+ " WHERE a.riskcode=b.riskcode " + " AND a.grpcontno='"
				+ GrpContNo + "' order by a.riskcode ";
		String RiskCode = "";
		SSRS arr2 = tExeSQL.execSQL(GrpPolSQL);
		for (int i = 1; i <= mLLPrepaidClaimDetailSet.size(); i++) {
			LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
			mLLPrepaidClaimDetailSchema.setSchema(mLLPrepaidClaimDetailSet
					.get(i));
			money = mLLPrepaidClaimDetailSet.get(i).getMoney();
			RiskCode = mLLPrepaidClaimDetailSet.get(i).getRiskCode();
		}

		// 回收预付赔款申请----清账
		// 险种层次查询回收款项的实际金额
		String sql1 = "select prepaidbala from llprepaidgrppol where grpcontno='"
				+ GrpContNo + "' and riskcode = '" + RiskCode + "' ";

		String sql2 = "select (a.money + b.money) from("
				+ "select sum(money) money from llprepaidtrace "
				+ "where grpcontno='"
				+ GrpContNo
				+ "' and riskcode = '"
				+ RiskCode
				+ "' and money >= 0.00 and state = '1' ) a,"
				+ "(select sum(money) money from llprepaidtrace "
				+ "where grpcontno='"
				+ GrpContNo
				+ "' and riskcode = '"
				+ RiskCode
				+ "' and money < 0.00 and moneytype = 'PK' ) b,"
				+ " (select nvl(sum(money),0) money from llprepaidtrace q,LLPrepaidClaim z"
				+ " where q.grpcontno='"
				+ GrpContNo
				+ "' and q.riskcode = '"
				+ RiskCode
				+ "' and q.money < 0.00 and  q.otherno = z.PrepaidNo and z.rgtstate <> '07' and q.moneytype = 'HS'"
				+ " ) c";
		SSRS Sql1 = tExeSQL.execSQL(sql1);
		SSRS Sql2 = tExeSQL.execSQL(sql2);

		// 实际的金额
		double tTraceMoney1 = 0;
		double tMoney1 = 0;
		double tGRPPolMoney1 = Double.valueOf(Sql1.GetText(1, 1)); // 预付赔款余额
		System.out.println("=====================================预付的金额："+ tGRPPolMoney1);
		String ResultSQL = Sql2.GetText(1, 1);
		if("".equals(ResultSQL)){
			ResultSQL = "0.0";
			tTraceMoney1 = Double.valueOf(ResultSQL);
		}
		
		// 险种层次判断以哪个金额为准
		if (tGRPPolMoney1 - tTraceMoney1 >= 0) {
			tMoney1 = tTraceMoney1;
		} else {
			tMoney1 = tGRPPolMoney1;
		}

		// 保单层次查询回收款项的实际金额
		String sql3 = "select prepaidbala from llprepaidgrpcont where grpcontno = '"
				+ GrpContNo + "' ";
		String sql4 = "select (a.money + b.money + c.money) from("
				+ " select nvl(sum(money),0)  money from llprepaidtrace "
				+ " where grpcontno='"
				+ GrpContNo
				+ "' and money >= 0.00 and state = '1' ) a,"
				+ " (select nvl(sum(money),0) money from llprepaidtrace "
				+ " where grpcontno='"
				+ GrpContNo
				+ "' and money < 0.00 and moneytype = 'PK'"
				+ " ) b,"
				+ " (select nvl(sum(money),0) money from llprepaidtrace q,LLPrepaidClaim z"
				+ " where q.grpcontno='"
				+ GrpContNo
				+ "' and q.money < 0.00 and  q.otherno = z.PrepaidNo and z.rgtstate <> '07' and q.moneytype = 'HS'"
				+ " ) c";
		SSRS Sql3 = tExeSQL.execSQL(sql3);
		SSRS Sql4 = tExeSQL.execSQL(sql4);

		// 实际的金额tMoney1
		double tMoney2 = 0;
		double tGRPPolMoney2 = Double.valueOf(Sql3.GetText(1, 1));
		double tTraceMoney2 = Double.valueOf(Sql4.GetText(1, 1));
		// 保单层次判断以哪个金额为准
		if (tGRPPolMoney2 - tTraceMoney2 >= 0) {
			tMoney2 = tTraceMoney2;
		} else {
			tMoney2 = tGRPPolMoney2;
		}

		for (int i = 1; i <= mLLPrepaidClaimDetailSet.size(); i++) {
			LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
			mLLPrepaidClaimDetailSchema.setSchema(mLLPrepaidClaimDetailSet
					.get(i));
			double money = mLLPrepaidClaimDetailSet.get(i).getMoney();
			RiskCode = mLLPrepaidClaimDetailSet.get(i).getRiskCode();

			if (money <= 0) {
				Content = ("实际回收赔款不能为空");

				return false;
			}

			if (!isNumeric(money)) {
				Content = ("实际回收赔款必须为数字");

				return false;
			}

			if (money <= 0) {
				Content = ("实际回收赔款必须大于0");

				return false;
			}

			if (Number(tMoney2) < Number(money)) {
				Content = ("保单下回收金额不能大于预付赔款余额");

				return false;
			}

			if (Number(money) > Number(tMoney1)) {
				Content = ("险种下回收金额不能大于预付赔款余额");

				return false;
			}
			chkFlag = true;
		}

		if (chkFlag == false) {
			Content = ("请选择回收预付赔款险种");
			return false;
		}

		if (PayMode == "1" && PayMode == "2") {
			if (BankCode == null || BankCode == "") {
				Content = ("请选择银行");
				return false;
			}
		}

		if (PayMode == "4" || PayMode == "11") {
			if (BankAccNo == null || BankAccNo == "") {
				Content = ("请录入银行账号");
				return false;
			}

			if (AccName == null || AccName == "") {
				Content = ("请录入账户名");
				return false;
			}
		}
		// 调用后台处理类
		if (!correctionCommit()) {
			return false;
		}
		

		return true;
	}

	public boolean correctionCommit() {
		// 输出参数
		CErrors tError = null;
		String tRela = "";
		String FlagStr = "";
		String fmtransact = "";
		VData tResult = new VData();
		GlobalInput tG = new GlobalInput();
		tG.Operator = "ss1113";

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		String transact = "Prepaid||Apply";
		System.out.println("**************transact=" + transact);

		LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
		LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
		LLPrepaidClaimInputUI tLLPrepaidClaimInputUI = new LLPrepaidClaimInputUI();

		// *********************************************
		String tPrepaidNo = mLLPrepaidClaimSchema.getPrepaidNo();// 预付赔款号
		String tGrpContNo = mLCGrpContSchema.getGrpContNo();// 团体保单号
		// String tRgtType = mLLPrepaidClaimSchema.getRgtType(); // 预付赔款状态
		String tRgtType = "2";
		String tPayMode = mLLPrepaidClaimSchema.getPayMode(); // 收付方式
		String tBankCode = mLLPrepaidClaimSchema.getBankAccNo(); //
		String tAccNo = mLLPrepaidClaimSchema.getBankAccNo();
		String tAccName = mLLPrepaidClaimSchema.getAccName();

		// 从url中取出参数付给相应的schema
		tLLPrepaidClaimSchema.setGrpContNo(tGrpContNo);
		tLLPrepaidClaimSchema.setPrepaidNo(tPrepaidNo);
		tLLPrepaidClaimSchema.setRgtType(tRgtType);
		tLLPrepaidClaimSchema.setBankCode(tBankCode);
		tLLPrepaidClaimSchema.setBankAccNo(tAccNo);
		tLLPrepaidClaimSchema.setAccName(tAccName);
		tLLPrepaidClaimSchema.setPayMode(tPayMode);

		for (int i = 1; i <= mLLPrepaidClaimDetailSet.size(); i++) {
			LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
			mLLPrepaidClaimDetailSchema.setSchema(mLLPrepaidClaimDetailSet
					.get(i));
			money = mLLPrepaidClaimDetailSet.get(i).getMoney();
			RiskCode = mLLPrepaidClaimDetailSet.get(i).getRiskCode();
			System.out.println("*********************************第" + i
					+ "个险种编码为" + RiskCode + "的预付赔款金额：" + money);
			String grppolnosql = "select grppolno from LLPrepaidGrpPol where  GrpContNo='"
					+ tGrpContNo + "' and riskcode='" + RiskCode + "'";
			SSRS grppolnoarr = tExeSQL.execSQL(grppolnosql);
			// grppolno = grppolnoarr.GetText(1, 1);

			LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
			tLLPrepaidClaimDetailSchema.setRiskCode(mLLPrepaidClaimDetailSet
					.get(i).getRiskCode());
			tLLPrepaidClaimDetailSchema.setMoney(mLLPrepaidClaimDetailSet
					.get(i).getMoney());
			tLLPrepaidClaimDetailSchema.setGrpPolNo(grppolnoarr.GetText(1, 1));
			tLLPrepaidClaimDetailSet.add(tLLPrepaidClaimDetailSchema);

		}

		try {
			// 准备传输数据VData
			VData tVData = new VData();

			// 传输schema
			tVData.addElement(tLLPrepaidClaimSchema);
			tVData.addElement(tLLPrepaidClaimDetailSet);

			tVData.add(tG);
			tLLPrepaidClaimInputUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			System.out.println("==================核心校验失败，原因是：" + Content);
			return false;
		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tLLPrepaidClaimInputUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Success";
				tResult = tLLPrepaidClaimInputUI.getResult();
				tLLPrepaidClaimSchema = (LLPrepaidClaimSchema) tResult
						.getObjectByObjectName("LLPrepaidClaimSchema", 0);
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				System.out.println("==================核心校验失败，原因是：" + Content);
				return false;
			}
		}

		// 添加各种预处理

		System.out.println("操作预付赔款号：" + tLLPrepaidClaimSchema.getPrepaidNo());

		return true;
	}

	
	
	
	
	/**
	 * 核心清账操作之后进行审批，
	 * 直接调用审批后台类
	 * @return
	 */
	public boolean approveCheck(){
		System.out.println("*************************************预付赔款第二次审批***********************");
		GlobalInput tG = new GlobalInput();
		tG.Operator = "sic001";  //暂定操作员为sic001  
		tG.ComCode = "86110000";
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String transact = "";
		LLPrepaidClaimUnderWriteUI tLLPrepaidClaimUnderWriteUI = new LLPrepaidClaimUnderWriteUI();
		LLPrepaidUWMainSchema tLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		transact = "SPSDUW";
		String PrepaidNoSql = " select prepaidno  from LLPrepaidClaim  where '1'='1' and  1=1 and ManageCom like '8611%' and" +
				" GrpContNo='"+GrpContNo+"' and rgttype='2' and rgtstate='01' order by GrpContNo,PrepaidNo ";
		SSRS arr1 = tExeSQL.execSQL(PrepaidNoSql);
		String tPrepaidNo = arr1.GetText(1, 1);
		System.out.println("*********************回收的预付赔款号：" + tPrepaidNo);
		mLLPrepaidClaimDetailSchema.setGrpContNo(tPrepaidNo); //后台校验 明细表里插入数据
		tLLPrepaidUWMainSchema.setPrepaidNo(tPrepaidNo);
		try {
			// 准备传输数据VData
			VData tVData = new VData();
			tVData.add(tG);
			tVData.addElement(tLLPrepaidUWMainSchema);
			tVData.addElement(mLLPrepaidClaimDetailSchema);
			tLLPrepaidClaimUnderWriteUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			return false;
		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tLLPrepaidClaimUnderWriteUI.mErrors;
			if (!tError.needDealError()) {
				VData tResultData = tLLPrepaidClaimUnderWriteUI.getResult();
				String strResult = (String) tResultData.getObjectByObjectName(
						"String", 0);
				Content = strResult;
				FlagStr = "Success";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				return false;
			}
		}

		return true;
	
		
	
	}
	
	/**
	 * 清帐之后的 通知操作
	 * @param money
	 * @return
	 */
	public boolean informCheck() {
		System.out.println("**************************************预付赔款第二次通知***************************");
		// 接收信息，并作校验处理。
		LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
		LLPrepaidClaimConfirmUI tLLPrepaidClaimConfirmUI = new LLPrepaidClaimConfirmUI();
		LLPrepaidUWMainSchema tLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
		LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
		// 输出参数
		CErrors tError = null;
		String FlagStr = "";
		String transact = "";
		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		transact = "CONFIRM";
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = ManageCom; // gy 后台校验tG里面的managecom
		tG.Operator = "ss1113";
		
		String PrepaidNoSql = " select prepaidno  from LLPrepaidClaim  where '1'='1'and ManageCom like '8611%' and " +
				"GrpContNo='"+GrpContNo+"' and rgttype='2' and rgtstate='04' order by maketime desc";
		SSRS arr1 = tExeSQL.execSQL(PrepaidNoSql);
		String tPrepaidNo = arr1.GetText(1, 1);
		System.out.println("*********************回收的预付赔款号：" + tPrepaidNo);

		tLLPrepaidUWMainSchema.setPrepaidNo(tPrepaidNo);
		String preSql = "select ManageCom,PrepaidNo,GrpContNo,GrpName,RgtDate,SumMoney,"
				+ " codename('llprepaidstate',RgtState),PayMode,codename('paymode',PayMode),bankcode,"
				+ " (select bankname from ldbank where bankcode=a.bankcode),"
				+ " BankAccNo,AccName,RgtState"
				+ " from LLPrepaidClaim a"
				+ " where 1=1 and PrepaidNo='" + tPrepaidNo + "'";

		SSRS arr = tExeSQL.execSQL(preSql);
		// String ManageCom = arr.GetText(1, 1);
		// String GrpContNo = arr.GetText(1, 3);
		String tAccName = arr.GetText(1, 13);
		String tPayMode = arr.GetText(1, 8);
		String tBankCode = arr.GetText(1, 10);
		String tBankAccNo = arr.GetText(1, 12);
		tPrepaidNo = arr.GetText(1, 2);
//		System.out.println("=================================预付赔款获取到的保单信息="+ GrpContNo);
		// String tPrepaidNo = arr.GetText(1, 1);
		tLLPrepaidClaimSchema.setPayMode(tPayMode);
		tLLPrepaidClaimSchema.setBankCode(tBankCode);
		tLLPrepaidClaimSchema.setBankAccNo(tBankAccNo);
		tLLPrepaidClaimSchema.setAccName(tAccName);
		tLLPrepaidClaimSchema.setPrepaidNo(tPrepaidNo);

		try {
			// 准备传输数据VData
			VData tVData = new VData();
			tVData.add(tG);
			tVData.addElement(tLLPrepaidUWMainSchema);
			tVData.addElement(tLLPrepaidClaimSchema);
			
			
			//程序等待30秒再调用通知后台
			System.out.println("********************SleepBegin............");
			Thread.currentThread().sleep(30000);
			System.out.println("********************SleepEnd..............");
			tLLPrepaidClaimConfirmUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			System.out.println("==================原因是：" + Content);
			return false;
		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if ("".equals(FlagStr)) {
			tError = tLLPrepaidClaimConfirmUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Success";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				System.out.println("==================原因是：" + Content);
				return false;
			}
		}

		return true;
	}
	

	private int Number(double money) {

		return 0;
	}

	private boolean isNumeric(double sumMoney) {

		// 使用正则表达式进行判断
		// String chkExp=/^\d+(\.\d+)?$/;
		// return (chkExp.test(strValue));
		return true;
	}
}
