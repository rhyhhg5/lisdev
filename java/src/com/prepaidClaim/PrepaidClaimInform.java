package com.prepaidClaim;

import utils.system;

import com.sinosoft.lis.llcase.LLPrepaidClaimConfirmUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimDetailSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimSchema;
import com.sinosoft.lis.schema.LLPrepaidUWMainSchema;
import com.sinosoft.lis.vschema.LLPrepaidClaimDetailSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class PrepaidClaimInform {
//	public static void main(String[] args) {
//		PrepaidClaimInform pcc = new PrepaidClaimInform();
//		VData vdata = new VData();
//		pcc.dealData(vdata );
////		pcc.getContent();
//		System.out.println(pcc.getContent());
//	}

	// 错误信息
	String Content;
	String ManageCom; // 管理机构
	String RgtState; // 申请日期
	String GrpName; // 投保单位名称
	String GrpContNo; // 团体保单号
	String BankCode; // 银行编码
	double SumMoney;
	double SumPrem;
	String BankAccNo;
	String PayMode;
	String AccName;
	String PrepaidNo;
	String RgtType;
	String Handler;

	LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
	LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();// 预付赔款信息表
	LLPrepaidClaimDetailSet mLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
	LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
	CErrors tError = null;
	ExeSQL tExeSQL = new ExeSQL();
	public String getContent() {
		return Content;
	}
	public boolean dealData(VData vdata) {
		System.out.println("**********************************预付赔款通知********************************");
		System.out.println("**********************************通知结束后，进行付费操作");
		// 获取数据
		if (!getInputData(vdata)) {
			return false;
		}
		if (!informCheck()) {
			return false;
		}
		return true;
	}

	private boolean getInputData(VData vdata) {
		mLCGrpContSchema = null;
		mLLPrepaidClaimSchema = null;
		mLLPrepaidClaimDetailSet = null;
		mLCGrpContSchema = (LCGrpContSchema) vdata.getObjectByObjectName(
				"LCGrpContSchema", 0);

		mLLPrepaidClaimSchema = (LLPrepaidClaimSchema) vdata
				.getObjectByObjectName("LLPrepaidClaimSchema", 0);

		mLLPrepaidClaimDetailSet = (LLPrepaidClaimDetailSet) vdata
				.getObjectByObjectName("LLPrepaidClaimDetailSet", 0);

		return true;
	}

	private boolean informCheck() {
		String RgtDateS;
		String RgtDateE;
		ExeSQL tExeSQL = new ExeSQL();
	
		GrpContNo = mLCGrpContSchema.getGrpContNo();
		ManageCom = mLCGrpContSchema.getManageCom();
//		GrpContNo = "00003112000038";
//		ManageCom = "86110000";
		System.out.println("==============================:"+GrpContNo);
		
		String strSQL= "select prepaidno from llprepaidclaim where GRPCONTNO='"+GrpContNo+"' and rgtstate =4";
		SSRS arrSQL = tExeSQL.execSQL(strSQL);
		PrepaidNo = arrSQL.GetText(1, 1);
		System.out.println("**********************预付赔款录入生成的预付赔款号：" + PrepaidNo);
		
/*		=============================测试时 暂时不用======================
//		RgtState= mLLPrepaidClaimSchema.getRgtState();// 
//		GrpName = mLLPrepaidClaimSchema.getGrpName();
//		SumPrem = mLCGrpContSchema.getSumPrem();
//		Handler = mLLPrepaidClaimSchema.getHandler();
//		RgtType = mLLPrepaidClaimSchema.getRgtType();
//		SumMoney = mLLPrepaidClaimSchema.getSumMoney();
*/
		RgtDateS = "2000-1-1"; // 页面上的申请起期
		RgtDateE = "2020-1-1";

		System.out.println("***************管理机构：" + ManageCom);
		if (ManageCom == "" || ManageCom == null) {// ||RgtDateS==""||RgtDateS==null||RgtDateE==""||RgtDateE==null
			Content = ("管理机构、申请起期、申请止期不能为空!");
			return false;
		}
		// 通知页面查询 ------预付赔款信息
		String preSql="select ManageCom,PrepaidNo,GrpContNo,GrpName,RgtDate,SumMoney,"
			+" codename('llprepaidstate',RgtState),PayMode,codename('paymode',PayMode),bankcode,"
			+" (select bankname from ldbank where bankcode=a.bankcode),"
			+" BankAccNo,AccName,RgtState"
			+" from LLPrepaidClaim a"
			+" where 1=1 and ManageCom like '"+ManageCom+"%'"
			+" and RgtState in ('04','05','06') "
			+" and RgtDate>='"+RgtDateS+"' and RgtDate<='"+RgtDateE+"'"
			+"and PrepaidNo='"+PrepaidNo+"'";
		
		SSRS arr1 = tExeSQL.execSQL(preSql);
		String tRgtState = arr1.GetText(1, 14);
		System.out.println("=============预付赔款通知状态=============================="+tRgtState);
		if (!"04".equals(tRgtState.trim())) {
			Content = ("预付案件只有在审定状态才能进行给付确认!");
			return false;
		}
		String tPrepaidNo = arr1.GetText(1, 2);
		String tBankCode = arr1.GetText(1, 10);
		String tBankAccNo = arr1.GetText(1, 12);
		String tAccName = arr1.GetText(1, 13);
		String tTRgtType = arr1.GetText(1, 14);
		double tPayMode = Double.valueOf(arr1.GetText(1, 8));
		double tRgtType = Double.valueOf(tTRgtType);

		// if(tPayMode == null || tPayMode == "")
		// {
		// Content = ("收付方式不能为空");
		// return false;
		// }
		
		// 校验给付确认权限
		PrepaidNo = tPrepaidNo;
		if (!checkDealPopedom()) {

			return false;
		}
		
		if (tRgtType == 1
				&& (tBankCode == null || tBankCode == "" || tBankAccNo == null
						|| tBankAccNo == "" || tAccName == null || tAccName == "")) {
			Content = ("预付赔款时银行账户信息银行名称、银行账号、账户名称不能为空");
			return false;
		}
		if (tRgtType == 2 && (tPayMode == 3 || tPayMode == 4 || tPayMode == 11)
				&& (tBankCode == null || tBankCode == "")) {
			Content = ("收费方式为转账支票、银行转账或银行汇款时，银行名称不能为空");
			return false;
		}
		if (tRgtType == 2
				&& (tPayMode == 4 || tPayMode == 11)
				&& (tBankAccNo == null || tBankAccNo == "" || tAccName == null || tAccName == "")) {
			Content = ("收费方式为银行转账或银行汇款时，银行账户信息银行账号、账户名称不能为空");
			return false;
		}
		
		if (!correctionCommit()) {
			return false;
		}

		return true;

	}

	private boolean checkDealPopedom() {
		String operator = "ss0001";
		String RegisterSQL = "select Register from LLPrepaidClaim where PrepaidNo='"
				+ PrepaidNo + "'";
		SSRS arr1 = tExeSQL.execSQL(RegisterSQL);

		String handler = arr1.GetText(1, 1); // 预付赔款录入员：ss1113
		if (arr1.getMaxRow() < 0) {
			Content = ("预付案件申请人查询失败");
			return false;
		}

		String strSQL = "select 1 From llclaimuser a where a.usercode='"
				+ handler + "' and stateflag='1' "
				+ "union select 1 From llsocialclaimuser a where a.usercode='"
				+ handler + "' and stateflag='1' with ur";
		SSRS arr2 = tExeSQL.execSQL(strSQL);

		String upUser = handler;
		/*
		 * 
		 */
		
			String rightSQL = "select a.upusercode,a.stateFlag"
					+ " From llclaimuser a where a.usercode='"
					+ handler
					+ "' "
					+ " union select a.upusercode,a.stateflag from llsocialclaimuser a where a.usercode='"
					+ handler + "' with ur";
			SSRS arr4 = tExeSQL.execSQL(rightSQL);
			String upusercode = arr4.GetText(1, 1);   //ss0001
			String stateFlag = arr4.GetText(2, 2);    // 1 

			if (arr4.getMaxRow() < 0) {
				Content = ("没有权限进行操作");
				return false;
			}

			if ("1" == upusercode) {
				return false;
			}

			upUser = upusercode;
			handler = upUser;
		

		if (operator.equals(upUser)) {
			return true;
		} else {
			Content = ("您没有预付案件给付通知权限，请用" + upUser + "进行给付确认");
			return false;
		}
	}

	/**
	 * 通知操作调用后台处理
	 * @return
	 */
	private boolean correctionCommit() {
		// 接收信息，并作校验处理。
		// ××××Schema t××××Schema = new ××××Schema();
		LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
		LLPrepaidClaimConfirmUI tLLPrepaidClaimConfirmUI = new LLPrepaidClaimConfirmUI();
		LLPrepaidUWMainSchema tLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
		LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
		// 输出参数
		CErrors tError = null;
		String FlagStr = "";
		String transact = "";
		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		transact = "CONFIRM";
		
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = ManageCom;  //   gy 后台校验tG里面的managecom
		tG.Operator="ss1113";
		
		String tPrepaidNo = PrepaidNo;// 预付赔款号
		System.out.println("================================通知预付赔款获取预付赔款号="+tPrepaidNo);
		tLLPrepaidUWMainSchema.setPrepaidNo(tPrepaidNo);
		
		
		String preSql = "select ManageCom,PrepaidNo,GrpContNo,GrpName,RgtDate,SumMoney,"
				+ " codename('llprepaidstate',RgtState),PayMode,codename('paymode',PayMode),bankcode,"
				+ " (select bankname from ldbank where bankcode=a.bankcode),"
				+ " BankAccNo,AccName,RgtState"
				+ " from LLPrepaidClaim a"
				+ " where 1=1 and PrepaidNo='" + tPrepaidNo + "'";

		SSRS arr = tExeSQL.execSQL(preSql);
		//String ManageCom = arr.GetText(1, 1);
		//String GrpContNo = arr.GetText(1, 3);
		String tAccName = arr.GetText(1, 13);
		String tPayMode = arr.GetText(1, 8);
		String tBankCode = arr.GetText(1, 10);
		String tBankAccNo = arr.GetText(1, 12);
		tPrepaidNo = arr.GetText(1, 2);
		System.out.println("=================================预付赔款获取到的保单信息="+GrpContNo);
		// String tPrepaidNo = arr.GetText(1, 1);
		 tLLPrepaidClaimSchema.setPayMode(tPayMode);
		 tLLPrepaidClaimSchema.setBankCode(tBankCode);
		 tLLPrepaidClaimSchema.setBankAccNo(tBankAccNo);
		 tLLPrepaidClaimSchema.setAccName(tAccName);
		 tLLPrepaidClaimSchema.setPrepaidNo(tPrepaidNo);
		
		try {
			// 准备传输数据VData
			VData tVData = new VData();
			tVData.add(tG);
			tVData.addElement(tLLPrepaidUWMainSchema);
			tVData.addElement(tLLPrepaidClaimSchema);
			tLLPrepaidClaimConfirmUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			System.out.println("==================原因是："+Content);
			return false;
		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if ("".equals(FlagStr)) {
			tError = tLLPrepaidClaimConfirmUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Success";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				System.out.println("==================原因是："+Content);
				return false;
			}
		}

		return true;

	}

}
