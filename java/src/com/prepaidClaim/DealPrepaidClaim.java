package com.prepaidClaim;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.contract.obj.MessageHead;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimDetailSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimSchema;
import com.sinosoft.lis.vschema.LLPrepaidClaimDetailSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * 预付赔款： 得到数据 校验数据 调用后台处理类 返回报文
 * 
 * @author GnegYe
 * 
 */
public class DealPrepaidClaim {

	String MsgType;
	String SendOperator;
	String BranchCode;
	String BatchNo;
	String PrtNo;
	String SendDate;
	String SendTime;
	// 返回报文
	String returnxml;
	// 错误信息
	private String errorValue;
	private VData data = new VData();
	
	String Content = "";
	
	
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
	private LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();// 预付赔款信息表
//	 LLPrepaidClaimSet mLLPrepaidClaimSet = new LLPrepaidClaimSet();
//	 
	 LLPrepaidClaimDetailSet mLLPrepaidClaimDetailSet  = new LLPrepaidClaimDetailSet();

	
	// 保存报文
	String mUrl = "";
	String aOutXmlStr = "";
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String fileName = sdf.format(date);

	public String getErrorValue() {
		return errorValue;
	}

	public String getReturnxml() {

		return returnxml;

	}

	/**
	 * 接口类调用的方法，用于处理业务逻辑
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public boolean deal(String xml) throws Exception {
		try {
			if (null == xml || "".equals(xml)) {
				errorValue = "接收的报文为空";
				return false;
			}
			// 解析报文得到数据
			if (!parseXml(xml)) {
				return false;
			}

			// 调用后台 业务处理类
			DealPrepaidClaimDetail dealDetail = new DealPrepaidClaimDetail();
			if (!dealDetail.deal(data)) {
				errorValue = dealDetail.getErrorValue();
				System.out.println("******************************************错误信息："+errorValue);
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(errorValue);
			returnxml = CreateXML.createXML(data, errorValue, xml);
			System.out.println("======================"+returnxml);
		} finally {
			System.out.println(errorValue);
			returnxml = CreateXML.createXML(data, errorValue, xml);
			System.out.println("======================"+returnxml);
//			// 保存返回报文
//			getUrlName("2");
//			String outFilePath = mUrl + "/" + fileName + MsgType + ".xml";
//			FileWriter fw;
//			try {
//				fw = new FileWriter(outFilePath);
//				fw.write(returnxml);
//				fw.flush();
//				fw.close();
//			} catch (IOException e) {
//				errorValue = e.toString();
//				e.printStackTrace();
//			}
//			if (!"".equals(errorValue) && null != errorValue) {
//
//				// 错误日志
//				ExeSQL ExeSQL = new ExeSQL();
//				String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
//						+ SendOperator
//						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','"
//						+ MsgType
//						+ "','"
//						+ BranchCode
//						+ "','"
//						+ BatchNo
//						+ "','" + PrtNo + "')";
//				ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
//				String insertErrorInsertLogSql = "insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
//						+ SendOperator
//						+ "','"
//						+ xml
//						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
//						+ returnxml
//						+ "','"
//						+ errorValue
//						+ "','"
//						+ MsgType
//						+ "','" + BranchCode + "','" + BatchNo + "')";
//				ExeSQL.execUpdateSQL(insertErrorInsertLogSql);
//			} else {
//				// 日志
//				ExeSQL ExeSQL = new ExeSQL();
//				String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
//						+ SendOperator
//						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'1','"
//						+ MsgType
//						+ "','"
//						+ BranchCode
//						+ "','"
//						+ BatchNo
//						+ "','" + PrtNo + "')";
//				ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
//			}
		}

		return true;
	}

	/*
	 * 解析报文
	 */

	public boolean parseXml(String xml) {
		
		System.out.println("****************getdata解析报文******************");

	try {
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			
			// 报文头
			Element msgHead = rootElement.getChild("head");
			Element elehead = msgHead.getChild("Item");
			
			BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			BranchCode = elehead.getChildText("BranchCode");
			SendOperator = elehead.getChildText("SendOperator");
			MsgType = elehead.getChildText("MsgType");

			// 保存请求报文 1是请求报文
			getUrlName("1");
			String InFilePath = mUrl + "/" + fileName + MsgType + ".xml";
			System.out.println(InFilePath + "路径");
			FileWriter fw;
			try {
				fw = new FileWriter(InFilePath);
				fw.write(xml);
				fw.flush();
				fw.close();
			} catch (IOException e) {
				errorValue = e.toString();
				e.printStackTrace();
			}

			// 保存报文头
			MessageHead head = new MessageHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			
			// 解析报文体
			Element tBody = rootElement.getChild("body");
			Element tInput = tBody.getChild("Input");
			
			// 团体保单表
			mLCGrpContSchema.setManageCom(tInput.getChildText("ManageCom"));// 管理机构
			mLCGrpContSchema.setGrpContNo(tInput.getChildText("GrpContNo"));
			mLCGrpContSchema.setGrpName(tInput.getChildText("GrpName"));
			mLCGrpContSchema.setSumPrem(tInput.getChildText("SumPrem"));
			System.out.println("*******************************实收保费："+tInput.getChildText("SumPrem"));
	
			//预付赔款表
			mLLPrepaidClaimSchema.setHandler(tInput.getChildText("Handler"));
			mLLPrepaidClaimSchema.setPrepaidNo(tInput.getChildText("PrepaidNo"));
			mLLPrepaidClaimSchema.setBankCode(tInput.getChildText("BankCode"));
			System.out.println("================银行编码======"+tInput.getChildText("BankCode"));
			mLLPrepaidClaimSchema.setAccName(tInput.getChildText("AccName"));
			String BankName = tInput.getChildText("BankName");
			System.out.println("银行名称*************" + BankName);
			mLLPrepaidClaimSchema.setBankAccNo(tInput.getChildText("BankAccNo"));
			System.out.println("***********************************+"+tInput.getChildText("BankAccNo"));
			mLLPrepaidClaimSchema.setPayMode(tInput.getChildText("PayMode"));
			mLLPrepaidClaimSchema.setAccName(tInput.getChildText("AccName"));
			mLLPrepaidClaimSchema.setPayMode(tInput.getChildText("PayMode"));
			mLLPrepaidClaimSchema.setRgtState(tInput.getChildText("RgtState"));
			mLLPrepaidClaimSchema.setHandler(tInput.getChildText("Handler"));
			mLLPrepaidClaimSchema.setRgtType(tInput.getChildText("RgtType"));
			mLLPrepaidClaimSchema.setRgtState(tInput.getChildText("RgtState"));  //申请日期
		
//			mLLPrepaidClaimSchema.setSumMoney(tInput.getChildText("SumMoney"));
//			System.out.println("**********************预付金额："+tInput.getChildText("SumMoney"));
		//	mLLPrepaidClaimSet.add(mLLPrepaidClaimSchema);
			
			Element triskdetail = tInput.getChild("riskdetail");
			List<Element> tItems = triskdetail.getChildren();
			for(int i=0;i<tItems.size();i++){
				Element item = tItems.get(i);
				//预付赔款明细表
				LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema= new LLPrepaidClaimDetailSchema();
				System.out.println("*****************************************第"+i+"个险种预付赔款金额："+item.getChildText("money"));
				mLLPrepaidClaimDetailSchema.setMoney(item.getChildText("money"));

//				mLLPrepaidClaimDetailSchema.setGrpContNo(item.getChildText("GrpContNo"));
//				mLLPrepaidClaimDetailSchema.setPrepaidNo(item.getChildText("PrepaidNo"));
				mLLPrepaidClaimDetailSchema.setRiskCode(item.getChildText("RiskCode"));
				mLLPrepaidClaimDetailSchema.setOperator(item.getChildText("Operator"));   //  操作员
				
				mLLPrepaidClaimDetailSet.add(mLLPrepaidClaimDetailSchema);
				
				double money = mLLPrepaidClaimDetailSchema.getMoney(); // 预付赔款金额
				System.out.println("====================="+money);
			}
			
			//保存数据
			data.add(mLLPrepaidClaimSchema);
			data.add(mLCGrpContSchema);
			data.add(mLLPrepaidClaimDetailSet);
		//	data.add(mLLPrepaidClaimSet);
			
	} catch (Exception e) {
			e.printStackTrace();
			errorValue = "处理报文出错：" + e.toString();
			return false;
		}
		return true;

	}

	/**
	 * getUrlName 获取文件存放路径
	 * 
	 * @return boolean
	 */
	private boolean getUrlName(String type) {
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";// 发盘报文的存放路径
		String fileFolder = new ExeSQL().getOneValue(sqlurl);

		if (fileFolder == null || fileFolder.equals("")) {
			aOutXmlStr = "getFileUrlName,获取文件存放路径出错";
			return false;
		}
		// fileFolder ="E://QY//";
		if (type.equals("1")) {
			mUrl = fileFolder + "SYYB/prepaidClaimRequest/"
					+ PubFun.getCurrentDate2();
		} else {
			mUrl = fileFolder + "SYYB/prepaidClaimResponse/"
					+ PubFun.getCurrentDate2();
		}
		if (!newFolder(mUrl)) {
			return false;
		}
		return true;
	}

	/**
	 * newFolder 新建报文存放文件夹，以便对发盘报文查询
	 * 
	 * @return boolean
	 */
	public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在,文件路径为:" + myFilePath);
				return true;
			} else {
				myFilePath.mkdirs();
				System.out.println("新建目录成功,文件路径为:" + myFilePath);
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败!!");
			e.printStackTrace();
			return false;
		}
	}
}
