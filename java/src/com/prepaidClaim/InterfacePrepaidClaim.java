package com.prepaidClaim;


/**
 * 预付赔款接口
 * 
 * @author GengYe
 *
 */
public class InterfacePrepaidClaim {
	/**
	 * 接口的直接调用类
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public String service(String xml) throws Exception {
		System.out.println("***********service接口的直接调用类*********");
		System.out.println("获取的请求报文：" + xml);
		DealPrepaidClaim dealPrepaidClaim = new DealPrepaidClaim();
		try {
			//预付赔款后台处理类
			dealPrepaidClaim.deal(xml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String returnXML = dealPrepaidClaim.getReturnxml();
		System.out.println("返回的报文：" + returnXML);
		return returnXML;
		// 返回报文
	}
}
