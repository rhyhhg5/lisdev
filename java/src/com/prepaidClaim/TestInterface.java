package com.prepaidClaim;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.namespace.QName;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

public class TestInterface {
	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		try {
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return mByteArrayOutputStream.toByteArray();
	}

	public void service(String aInXmlStr) {

	// String tStrTargetEendPoint = "http://10.136.10.101:900/services/InterfacePrepaidClaim";
		String tStrTargetEendPoint = "http://localhost:8080/ui/services/InterfacePrepaidClaim";
		String tStrNamespace = "http://prepaidClaim.com";
		try {
			RPCServiceClient client = new RPCServiceClient();
			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
			option.setAction("service");
			option.setTimeOutInMilliSeconds(3000000L);
			QName name = new QName(tStrNamespace, "service");
			Object[] object = new Object[] { aInXmlStr };
			Class[] returnTypes = new Class[] { String.class };
			Object[] response = client
					.invokeBlocking(name, object, returnTypes);
			String result = (String) response[0];
			System.out.println("UI return:" + result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			TestInterface tPadClient = new TestInterface();
			String mInFilePath = "D:\\PICC\\prepaidClaim\\prepaidClaim2.xml";
			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = tPadClient.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "GBK");
			System.out.println(mInXmlStr);
			tPadClient.service(mInXmlStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
