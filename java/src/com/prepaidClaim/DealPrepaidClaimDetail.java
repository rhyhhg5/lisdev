package com.prepaidClaim;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/**
 * 
 * @author GengYe
 * 预付赔款
 *
 */
public class DealPrepaidClaimDetail {
//	public static void main(String[] args) {
//		DealPrepaidClaimDetail pcc = new DealPrepaidClaimDetail();
//		VData vdata = new VData();
//		pcc.deal(vdata );
////		pcc.getContent();
//		
//	}
	
	// 错误信息
	private String errorValue;
	private boolean rollbackFlag = false;
	VData data = new VData();
	private GlobalInput GI = new GlobalInput();
	ExeSQL tExeSQL = new ExeSQL();
	LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();// 预付赔款信息表
	LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
	String PrepaidNo;
	//LLPrepaidClaimSchema.getPrepaidNo()
	public String getErrorValue() {
		return errorValue;
	}
	public boolean deal(VData vdata){
		//预付赔款录入
		PrepaidClaimInput tPrepaidClaimInput = new PrepaidClaimInput();
		if (!tPrepaidClaimInput.dealData(vdata)) {
			rollbackFlag = true;
			errorValue = tPrepaidClaimInput.getContent();
			System.out.println("==================================获取到的错误信息:"+errorValue);
			return false;
		}

		
		//预付赔款审批
		PrepaidClaimApprove tPrepaidClaimApprove = new PrepaidClaimApprove();
		if (!tPrepaidClaimApprove.dealData(vdata)) {
			rollbackFlag = true;
			errorValue = tPrepaidClaimApprove.getContent();
			System.out.println("==================================获取到的错误信息:"+errorValue);
			return false;
		}
		
		//预付赔款通知
		PrepaidClaimInform tPrepaidClaimInform  = new PrepaidClaimInform();
		if (!tPrepaidClaimInform.dealData(vdata)) {
			rollbackFlag = true;
			errorValue = tPrepaidClaimInform.getContent();
			System.out.println("==================================获取到的错误信息:"+errorValue);
			return false;
		}
		
		//预付赔款清账
		PrepaidClaimClear tPrepaidClaimClear = new PrepaidClaimClear();
		if (!tPrepaidClaimClear.dealData(vdata)) {
			rollbackFlag = true;
			errorValue = tPrepaidClaimClear.getContent();
			return false;
		}
		return true;
	}
}
