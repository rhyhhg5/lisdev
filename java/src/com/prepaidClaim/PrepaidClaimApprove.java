package com.prepaidClaim;

import com.sinosoft.lis.llcase.LLPrepaidClaimUnderWriteUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimDetailSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimSchema;
import com.sinosoft.lis.schema.LLPrepaidUWMainSchema;
import com.sinosoft.lis.vschema.LLPrepaidClaimDetailSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class PrepaidClaimApprove {
	
	public static void main(String[] args) {
		PrepaidClaimApprove pca = new PrepaidClaimApprove();
		pca.approveCheck();
		
	}

	// 错误信息
	String Content;
	String ManageCom; // 管理机构
	String RgtState; // 申请日期
	String GrpName; // 投保单位名称
	String GrpContNo; // 团体保单号
	String BankCode; // 银行编码
	double money;         //预付金额
	double paymoney;	//应付金额
	String grppolno;
	String RiskCode;
	double SumPrem;
	String BankAccNo;
	String PayMode;
	String AccName;
	String PrepaidNo;
	String RgtType;
	String Handler;

	LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
	LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();// 预付赔款信息表
	LLPrepaidClaimDetailSet mLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
	LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
	CErrors tError = null;
	ExeSQL tExeSQL = new ExeSQL();
	public String getContent() {
		return Content;
	}

	public boolean dealData(VData vdata) {
		System.out
				.println("************************************预付赔款审批********************************");
		// 获取数据
		if (!getInputData(vdata)) {
			return false;
		}
		if (!approveCheck()) {
			return false;
		}
		return true;
	}

	private boolean getInputData(VData vdata) {
		mLCGrpContSchema = null;
		mLLPrepaidClaimSchema = null;
		mLLPrepaidClaimDetailSet = null;
		mLCGrpContSchema = (LCGrpContSchema) vdata.getObjectByObjectName("LCGrpContSchema", 0);
		mLLPrepaidClaimSchema = (LLPrepaidClaimSchema) vdata.getObjectByObjectName("LLPrepaidClaimSchema", 0);
		mLLPrepaidClaimDetailSet = (LLPrepaidClaimDetailSet) vdata.getObjectByObjectName("LLPrepaidClaimDetailSet", 0);
		return true;
	}

	public boolean approveCheck() {
		String RgtDateS;
		String RgtDateE;
		// 审批查询按钮
		ExeSQL tExeSQL = new ExeSQL();
		GrpContNo = mLCGrpContSchema.getGrpContNo();
//		GrpContNo = "00003112000058";
//		ManageCom = "86110000";
		ManageCom = mLCGrpContSchema.getManageCom();
		BankCode = mLLPrepaidClaimSchema.getBankCode();
//		System.out.println("==============================:"+GrpContNo);
		String strSQL= "select prepaidno from llprepaidclaim where GRPCONTNO='"+GrpContNo+"' and rgtstate =1";
		SSRS arrSQL = tExeSQL.execSQL(strSQL);
		PrepaidNo = arrSQL.GetText(1, 1);
		System.out.println("**********************预付赔款录入生成的预付赔款号：" + PrepaidNo);
		RgtState= mLLPrepaidClaimSchema.getRgtState();// 
		GrpName = mLLPrepaidClaimSchema.getGrpName();
//		SumPrem = mLCGrpContSchema.getSumPrem();
		Handler = mLLPrepaidClaimSchema.getHandler();
		RgtType = mLLPrepaidClaimSchema.getRgtType();
		

		RgtDateS = "2000-1-1"; // 页面上的申请起期
		RgtDateE = "2020-1-1";
		System.out.println("***************管理机构：" + ManageCom);
		if (ManageCom == "" || ManageCom == null) {// ||RgtDateS==""||RgtDateS==null||RgtDateE==""||RgtDateE==null
			Content = ("管理机构、申请起期、申请止期不能为空!");
			return false;
		}
//		// 查询 页面预付赔款信息
////		StringBuffer sb = new StringBuffer();
//		String preSql = "select ManageCom,PrepaidNo,GrpContNo,GrpName,RgtDate,case RgtType when '1' then '申请类' else '回收类' end,SumMoney,"
//				+ " codename('llprepaidstate',RgtState),Handler ,RgtState"
//				+ " from LLPrepaidClaim"
//				+ " where 1=1 and ManageCom like '"
//				+ ManageCom
//				+ "%'"
//				+ " and RgtDate>='"
//				+ RgtDateS
//				+ "' and RgtDate<='" + RgtDateE + "'";
//		StringBuffer sb = new StringBuffer(preSql);
//		String strSQL1 = " and GrpName = '" + GrpName + "'";//GrpName
//		String strSQL2 = " and GrpContNo = '" + GrpContNo + "'";
//		String strSQL3 = " and PrepaidNo = '" + PrepaidNo + "'";
//		String strSQL4 = " and RgtState = '" + RgtState + "'";
//		String strSQL5 = " and Handler = '" + Handler + "'";
//		String strSQL6 = " and RgtType = '" + RgtType + "'"
//				+ " order by GrpContNo,PrepaidNo";
////		sb.append(preSql);
//		
//		if (!("".equals(GrpName.trim()) || GrpName == null)) {
//			sb.append(strSQL1);
//		}
//		if (!("".equals(GrpContNo.trim()) || GrpContNo == null)) {
//			sb.append(strSQL2);
//		}
//		if (!("".equals(PrepaidNo.trim()) || PrepaidNo == null)) {
//			sb.append(strSQL3);
//		}
//		if (!("".equals(RgtState.trim()) || RgtState == null)) {
//			sb.append(strSQL4);
//		}
//		if (!("".equals(Handler.trim()) || Handler == null)) {
//			sb.append(strSQL5);
//		}
//		if (!("".equals(RgtType.trim()) || RgtType == null)) {
//			sb.append(strSQL6);
//		}
//		SSRS arr2 = tExeSQL.execSQL(sb.toString());
		
		String preSql="select ManageCom,PrepaidNo,GrpContNo,GrpName,RgtDate,case RgtType when '1' then '申请类' else '回收类' end,SumMoney,"
			+" codename('llprepaidstate',RgtState),Handler ,RgtState"
			+" from LLPrepaidClaim "
			+" where 1=1 and ManageCom like '"+ManageCom+"%'"
			+" and RgtDate>='"+RgtDateS+"' and RgtDate<='"+RgtDateE+"'"
			+"and PrepaidNo='"+PrepaidNo+"'";
		SSRS arr1 = tExeSQL.execSQL(preSql);
		

		// 页面上单击保单  ---------------生成预付赔款明细
		String preDetailSql = "select llde.RiskCode,(select riskname from lmrisk where riskcode=llde.riskcode), "
				+ " ( SELECT (decimal(NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
				+ " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
				+ " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0),12,2))"
				+ " FROM lcgrppol a, lmrisk b, llprepaidgrppol c"
				+ " WHERE a.grppolno=c.grppolno AND a.riskcode=b.riskcode "
				+ " AND a.GrpPolNo=llde.GrpPolNo),"
				+ " decimal(nvl(sum(llpreg.ApplyAmount+llpreg.RegainAmount),0),12,2), "
				+ " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
				+ " AND m.rgtstate in ('09','11','12') AND n.grpcontno=llde.grpcontno AND n.grppolno=llde.grppolno), "
				+ " nvl(llde.Money,0),llde.grppolno"
				+ " from LLPrepaidClaimDetail llde,LLPrepaidGrpPol llpreg"
				+ " where llde.GrpPolNo=llpreg.GrpPolNo "
				+ " and llde.PrepaidNo='"
				+ PrepaidNo
				+ "' group by llde.RiskCode,llde.GrpContNo,llde.GrpPolNo,llpreg.SumPay,llde.Money order by llde.RiskCode";
		
		
//		System.out.println("=======================================预付赔款审批====================");
		SSRS arrpredetail = tExeSQL.execSQL(preDetailSql);
		String tRgtState = arr1.GetText(1, 10); // 保单的状态    01申请状态
//		System.out.println("==================================保单状态=============："+tRgtState);
		String tPrepaidNo = PrepaidNo; //
		if (!"01".equals(tRgtState.trim()) && !"02".equals(tRgtState.trim()) &&!"03".equals(tRgtState.trim())) {
			Content = ("预付案件只有在申请扫描审批状态才能进行审批操作！");
			return false;
		} else {
			String DealerSql = "select Dealer,Operator from LLPrepaidClaim where PrepaidNo='"
					+ tPrepaidNo + "' ";
			SSRS arr22 = tExeSQL.execSQL(DealerSql);
			String arr22Result = arr22.GetText(1, 1); //sic001
			String Operator = arr22.GetText(1, 2);     //ss1113
			Operator = "sic001";
			arr22Result = "sic001";
			if (arr22Result != Operator) {
				Content = ("预付案件的审批人为" + arr22Result);
				return false;
			}
		}
		
		for(int i =1;i<=arrpredetail.getMaxRow();i++){
			boolean chkFlag = false;
			money = Double.valueOf(arrpredetail.GetText(i, 6));
			paymoney = Double.valueOf(arrpredetail.GetText(i, 3));
			grppolno = arrpredetail.GetText(i, 7);
			RiskCode = arrpredetail.GetText(i, 1);
			String checkSQL = " select " + paymoney + "-(" + money
						+ ") - (a.PrepaidBala) -" 
						+ " nvl((select sum(pay) from ljagetclaim where grppolno=a.grppolno and othernotype='5'),0) " 
						+ " from LLPrepaidGrpPol a where a.grppolno='"
						+ grppolno + "'";
			ExeSQL arr5 = new ExeSQL();
			SSRS sqlSSRSS = tExeSQL.execSQL(checkSQL);
			String result = sqlSSRSS.GetText(1, 1);
			double result1 = Double.valueOf(result);
			if (sqlSSRSS.getMaxNumber() > 0) {
				if (result1 < 0) {
					Content = ("险种" + RiskCode + "本次预付金额超过实收保费扣除 该险种理赔结案通知赔款与未回销预付赔款 之和，是否继续？");
					return false;
				}
			} else {
				Content = ("险种" + RiskCode + "预付信息查询失败，请与开发人员联系");
				return false;
		}
	}
		
		if (!correctionCommit()) {
			return false;
		}
		return true;
	}

	/**
	 * 审批调用后台处理类
	 * @return
	 */
	public boolean correctionCommit() {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "sic001";  //暂定操作员为sic001  
		tG.ComCode = "86110000";
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String transact = "";
		LLPrepaidClaimUnderWriteUI tLLPrepaidClaimUnderWriteUI = new LLPrepaidClaimUnderWriteUI();
		LLPrepaidUWMainSchema tLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		transact = "SPSDUW";
		String tPrepaidNo = PrepaidNo;// 预付赔款号
		System.out.println("*********************预付赔款号：" + tPrepaidNo);
		mLLPrepaidClaimDetailSchema.setGrpContNo(tPrepaidNo); //后台校验 明细表里插入数据
		tLLPrepaidUWMainSchema.setPrepaidNo(tPrepaidNo);
		try {
			// 准备传输数据VData
			VData tVData = new VData();
			tVData.add(tG);
			tVData.addElement(tLLPrepaidUWMainSchema);
			tVData.addElement(mLLPrepaidClaimDetailSchema);
			tLLPrepaidClaimUnderWriteUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			return false;
		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tLLPrepaidClaimUnderWriteUI.mErrors;
			if (!tError.needDealError()) {
				VData tResultData = tLLPrepaidClaimUnderWriteUI.getResult();
				String strResult = (String) tResultData.getObjectByObjectName(
						"String", 0);
				Content = strResult;
				FlagStr = "Success";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				return false;
			}
		}

		return true;
	}

}
