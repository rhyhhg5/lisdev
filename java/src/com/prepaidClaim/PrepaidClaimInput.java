package com.prepaidClaim;

import com.sinosoft.lis.llcase.LLPrepaidClaimInputUI;
import com.sinosoft.lis.llcase.LLPrepaidGrpContInputUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimDetailSchema;
import com.sinosoft.lis.schema.LLPrepaidClaimSchema;
import com.sinosoft.lis.schema.LLPrepaidGrpContSchema;
import com.sinosoft.lis.vschema.LLPrepaidClaimDetailSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class PrepaidClaimInput {

	// 错误的信息
	String Content;
	String ManageCom; // 管理机构
	String GrpName; // 投保单位名称
	String GrpContNo; // 团体保单号
	String BankCode; // 银行编码
	double money;
	double SumPrem;
	String BankAccNo;
	String PayMode;
	String AccName;
	String PrepaidNo;
	String RgtType;
	String RiskCode;
	String grppolno;
	LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); // 团体保单表
	LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();// 预付赔款信息表
	// LLPrepaidClaimSet mLLPrepaidClaimSet = new LLPrepaidClaimSet();
	LLPrepaidClaimDetailSet mLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
	CErrors tError = null;
	ExeSQL tExeSQL = new ExeSQL();

	public boolean dealData(VData vdata) {
		System.out
				.println("**************************************预付赔款录入********************************");
		// 获取数据
		if (!getInputData(vdata)) {
			return false;
		}
		if (!inputCheck()) {
			return false;
		}

		return true;
	}

	private boolean getInputData(VData vdata) {
		mLCGrpContSchema = null;
		mLLPrepaidClaimSchema = null;
		mLLPrepaidClaimDetailSet = null;
		mLCGrpContSchema = (LCGrpContSchema) vdata.getObjectByObjectName(
				"LCGrpContSchema", 0);
		mLLPrepaidClaimSchema = (LLPrepaidClaimSchema) vdata
				.getObjectByObjectName("LLPrepaidClaimSchema", 0);
		mLLPrepaidClaimDetailSet = (LLPrepaidClaimDetailSet) vdata
				.getObjectByObjectName("LLPrepaidClaimDetailSet", 0);
		return true;
	}

	public boolean inputCheck() {
//		System.out.println("==================================预付赔款录入的查询按钮====================");
		ExeSQL tExeSQL = new ExeSQL();
		ManageCom = mLCGrpContSchema.getManageCom();
		GrpName = mLCGrpContSchema.getGrpName();
		GrpContNo = mLCGrpContSchema.getGrpContNo();
		BankAccNo = mLLPrepaidClaimSchema.getBankAccNo();
		BankCode = mLLPrepaidClaimSchema.getBankCode();
//		System.out.println("=======================================银行编码"+BankCode);
		PayMode = mLLPrepaidClaimSchema.getPayMode();
		AccName = mLLPrepaidClaimSchema.getAccName();
		String SumActuPay; // 获取到的实收金额
		// 管理机构非空校验
		if (ManageCom == null || ManageCom == "") {
			Content = ("请选择管理机构");
			System.out.println("错误信息：" + Content);
			return false;
		}

		// 投保单位 团体保单号至少填一项
		if (GrpContNo == null || GrpContNo == "") {
			Content = ("请录入投保单位名称或团体保单号");
			System.out.println("错误信息：" + Content);
			return false;

		}
		StringBuffer sb = new StringBuffer();
		String asql = " SELECT a.managecom, a.grpcontno, a.grpname, a.cvalidate, a.cinvalidate, "
				+ " b.state, "
				+ " (CASE when b.state='0' THEN '撤销' WHEN b.state='1' THEN '有预付' ELSE '没有预付' END ), "
				+ " nvl(b.applyamount,0), nvl(b.prepaidbala,0) "
				+ " FROM lcgrpcont a LEFT JOIN llprepaidgrpcont b ON a.grpcontno=b.grpcontno "
				+ " WHERE a.managecom LIKE '"
				+ ManageCom
				+ "%'"
				+ " AND a.appflag='1' and a.stateflag in ('1','2','3')";
		String strSQL1 = " and a.GrpName = '" + GrpName + "'";
		String strSQL2 = " and a.GrpContNo = '" + GrpContNo + "'"
				+ " order by a.grpcontno";
		sb.append(asql);
		System.out.println("**********:" + GrpName);
		if (!("".equals(GrpName.trim()) || GrpName == null)) {
			sb.append(strSQL1);
		}
		if (!("".equals(GrpContNo.trim()) || GrpContNo == null)) {
			sb.append(strSQL2);
		}
		SSRS arr2 = tExeSQL.execSQL(sb.toString());
		String state = arr2.GetText(1, 6); // 1 有预付
		System.out.println("****************判断是否预付：" + state);
		if ("".equals(state)) {
			if (!GrpContDeal()) {
				return false;
			}
		}
		// System.out.println("==========================预付确认之后的state："+state);
		// 页面上选中保单后操作
		if (GrpContNo != null && GrpContNo != "") {
			boolean chkFlag = false;
			//
			String ClaimAccSQL = " SELECT a.claimbankcode, b.bankname, "
					+ " (CASE WHEN b.cansendflag='1' then '是' else '否' END), "
					+ " a.claimbankaccno, a.claimaccname, a.grpcontno, c.grpname, "
					+ " decimal((NVL((SELECT cast(SUM(sumactupaymoney) as decimal(12,2)) FROM ljapaygrp WHERE endorsementno IS NULL "
					+ " AND grpcontno=c.grpcontno and paytype<>'YF'),0) + "
					+ " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=c.grpcontno),0)),12,2) "
					+ " FROM lcgrpcont c, lcgrpappnt a LEFT JOIN ldbank b ON a.claimbankcode = b.bankcode "
					+ " WHERE a.grpcontno=c.grpcontno and c.grpcontno='"
					+ GrpContNo + "'";

			SSRS arr3 = tExeSQL.execSQL(ClaimAccSQL);
			BankCode = mLLPrepaidClaimSchema.getBankCode();
			BankAccNo = mLLPrepaidClaimSchema.getBankAccNo();
			AccName = mLLPrepaidClaimSchema.getAccName();
			GrpContNo = arr3.GetText(1, 6);
			GrpName = arr3.GetText(1, 7);
			SumActuPay = arr3.GetText(1, 8); // 总实缴金额

			if (mLLPrepaidClaimDetailSet.size() >= 1) {
				for (int i = 1; i <= mLLPrepaidClaimDetailSet.size(); i++) {
					LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
					mLLPrepaidClaimDetailSchema.setSchema(mLLPrepaidClaimDetailSet.get(i));
					money = mLLPrepaidClaimDetailSet.get(i).getMoney();
					RiskCode = mLLPrepaidClaimDetailSet.get(i).getRiskCode();
					System.out.println("*********************************第" + i
							+ "个险种编码为" + RiskCode + "的预付赔款金额：" + money);
					String GrpPolSQL = " SELECT a.riskcode, b.riskname, "
							+ " decimal(NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
							+ " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
							+ " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0),12,2), "
							+ " c.applyamount, "
							+ " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
							+ " AND m.rgtstate in ('09','11','12') AND n.grpcontno=a.grpcontno AND n.grppolno=a.grppolno), "
							+ " '', a.grppolno "
							+ " FROM lcgrppol a, lmrisk b, llprepaidgrppol c"
							+ " WHERE a.grpcontno=c.grpcontno AND a.grppolno=c.grppolno AND a.riskcode=b.riskcode "
							+ " AND a.grpcontno='" + GrpContNo + "'"
							+ " AND a.riskcode = '" + RiskCode
							+ "' order by a.riskcode";
					SSRS arr4 = tExeSQL.execSQL(GrpPolSQL);
					grppolno = arr4.GetText(1, 7);

					String paymoney = arr4.GetText(1, 3);
					System.out.println("================================应付金额："
							+ paymoney);

					String strmoney = String.valueOf(money);
					if (strmoney == null || strmoney == "") {
						Content = ("本次预付金额不能为空");
						return false;
					}
					if (!isNumeric(money)) {
						Content = ("本次预付金额必须为数字");
						return false;
					}
					if (money <= 0) {
						Content = ("本次预付金额必须大于0");

						return false;
					}
					String checkSQL = " select "
							+ paymoney
							+ "-"
							+ money
							+ " - a.PrepaidBala -"
							+ " nvl((select sum(pay) from ljagetclaim where grppolno=a.grppolno and othernotype='5'),0) "
							+ " from LLPrepaidGrpPol a where a.grppolno='"
							+ grppolno + "'";
					ExeSQL arr5 = new ExeSQL();
					SSRS sqlSSRSS = tExeSQL.execSQL(checkSQL);
					String result = arr4.GetText(1, 1);
					double result1 = Double.valueOf(result);
					if (sqlSSRSS.getMaxNumber() > 0) {
						if (result1 < 0) {
							Content = ("险种" + RiskCode + "本次预付金额超过实收保费扣除 该险种理赔结案通知赔款与未回销预付赔款 之和，是否继续？");
							return true;
						}
					} else {
						Content = ("险种" + RiskCode + "预付信息查询失败，请与开发人员联系");
						return false;
					}
					chkFlag = true;
				}
				if (chkFlag == false) {
					Content = ("请选择预付赔款险种");
					return false;
				}
			}
		}

		// 申请前台页面校验---- 提交后操作,服务器数据返回后执行的操作
		if (!ApplyCheck()) {
			return false;
		}

		return true;

	}

	// 页面上保单预付确认
	public boolean GrpContDeal() {
		// 输入参数
		LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = new LLPrepaidGrpContSchema();
		LLPrepaidGrpContInputUI tLLPrepaidGrpContInputUI = new LLPrepaidGrpContInputUI();

		// 输出参数
		CErrors tError = null;
		String tRela = "";
		String FlagStr = "";
		String Content = "";
		String transact = "";

		GlobalInput tG = new GlobalInput();
		tG.Operator = "ss1113";

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		transact = "GrpCont||Confirm";

		String tGrpContNo = mLCGrpContSchema.getGrpContNo();
		System.out.println("===================================tGrpContNo"
				+ tGrpContNo);
		// 从url中取出参数付给相应的schema
		tLLPrepaidGrpContSchema.setGrpContNo(tGrpContNo);

		try {
			// 准备传输数据VData
			VData tVData = new VData();
			// 传输schema
			tVData.addElement(tLLPrepaidGrpContSchema);
			tVData.add(tG);

			tLLPrepaidGrpContInputUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tLLPrepaidGrpContInputUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Success";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}

		}
		return true;
	}

	public boolean ApplyCheck() {//
		if (null == BankCode || "".equals(BankCode)) {// 此处校验报文中未传值，查询也为空所以会返回错误信息
			Content = "银行编码不能为空！";
			// System.out.println("银行编码不能为空！！");
			return false;
		}
		if (null == BankAccNo || "".equals(BankAccNo)) {
			Content = "银行账号不能为空！";
			return false;
		}
		if (null == PayMode || "".equals(PayMode)) {
			Content = "给付方式不能为空！";
			return false;
		}
		if (null == AccName || "".equals(AccName)) {
			Content = "账户名不能为空！";
			return false;
		}

		// 调用后台处理类
		if (!correctionCommit()) {
			return false;
		}

		return true;
	}

	public boolean correctionCommit() {
		// 输出参数
		CErrors tError = null;
		String tRela = "";
		String FlagStr = "";
		String fmtransact = "";
		VData tResult = new VData();
		GlobalInput tG = new GlobalInput();
		tG.Operator = "ss1113";

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		String transact = "Prepaid||Apply";
		System.out.println("**************transact=" + transact);
		
		LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
		LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
		LLPrepaidClaimInputUI tLLPrepaidClaimInputUI = new LLPrepaidClaimInputUI();

		// *********************************************
		String tPrepaidNo = mLLPrepaidClaimSchema.getPrepaidNo();// 预付赔款号
		System.out.println("+++++++++++++++++++++++++++++++++++++++"+mLLPrepaidClaimSchema.getPrepaidNo());
		String tGrpContNo = mLCGrpContSchema.getGrpContNo();// 团体保单号
		String tRgtType = mLLPrepaidClaimSchema.getRgtType(); // 预付赔款状态
		String tPayMode = mLLPrepaidClaimSchema.getPayMode(); // 收付方式
		String tBankCode = mLLPrepaidClaimSchema.getBankCode(); //
		String tAccNo = mLLPrepaidClaimSchema.getBankAccNo();
		String tAccName = mLLPrepaidClaimSchema.getAccName();

		// 从url中取出参数付给相应的schema
		tLLPrepaidClaimSchema.setGrpContNo(tGrpContNo);
		tLLPrepaidClaimSchema.setPrepaidNo(tPrepaidNo);
		tLLPrepaidClaimSchema.setRgtType(tRgtType);
		tLLPrepaidClaimSchema.setBankCode(tBankCode);
		tLLPrepaidClaimSchema.setBankAccNo(tAccNo);
		tLLPrepaidClaimSchema.setAccName(tAccName);
		tLLPrepaidClaimSchema.setPayMode(tPayMode);
	
		for (int i = 1; i <= mLLPrepaidClaimDetailSet.size(); i++) {
			LLPrepaidClaimDetailSchema mLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
			mLLPrepaidClaimDetailSchema.setSchema(mLLPrepaidClaimDetailSet.get(i));
			money = mLLPrepaidClaimDetailSet.get(i).getMoney();
			RiskCode = mLLPrepaidClaimDetailSet.get(i).getRiskCode();
			System.out.println("*********************************第" + i+ "个险种编码为" + RiskCode + "的预付赔款金额：" + money);
			String grppolnosql = "select grppolno from LLPrepaidGrpPol where  GrpContNo='"+ tGrpContNo + "' and riskcode='" + RiskCode + "'";
			SSRS grppolnoarr = tExeSQL.execSQL(grppolnosql);
			//grppolno = grppolnoarr.GetText(1, 1);
			
			LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
			tLLPrepaidClaimDetailSchema.setRiskCode(mLLPrepaidClaimDetailSet.get(i).getRiskCode());
			tLLPrepaidClaimDetailSchema.setMoney(mLLPrepaidClaimDetailSet.get(i).getMoney());
			tLLPrepaidClaimDetailSchema.setGrpPolNo(grppolnoarr.GetText(1, 1));
			tLLPrepaidClaimDetailSet.add(tLLPrepaidClaimDetailSchema);
			
		}
		
		try {
			// 准备传输数据VData
			VData tVData = new VData();
			// 传输schema
			tVData.addElement(tLLPrepaidClaimSchema);
			tVData.addElement(tLLPrepaidClaimDetailSet);
			tVData.add(tG);
			tLLPrepaidClaimInputUI.submitData(tVData, transact);

			System.out.println("tG" + tLLPrepaidClaimSchema.getPrepaidNo());
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			System.out.println(Content);
			return false;

		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tLLPrepaidClaimInputUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Success";
				tResult = tLLPrepaidClaimInputUI.getResult();
				tLLPrepaidClaimSchema = (LLPrepaidClaimSchema) tResult
						.getObjectByObjectName("LLPrepaidClaimSchema", 0);
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				System.out.println(Content);
				return false;
			}
		}

		return true;
	}

	private boolean isNumeric(double sumMoney) {
		// 使用正则表达式进行判断
		// String chkExp=/^\d+(\.\d+)?$/;
		// return (chkExp.test(strValue));
		return true;
	}

	public String getContent() {
		return Content;
	}

	// public static void main(String[] args) {
	// PrepaidClaimInput pca = new PrepaidClaimInput();
	// pca.getContent();
	// System.out.println( pca.getContent());
	// }
}
