package com.preservation.obj;

public class LPFromData {

	private String EdorValiDate;
	private String PriorityNo;
	private String WorkTypeNo;
	private String AcceptWayNo;
	private String ApplyTypeNo;
	private String ApplyName;
	private String Remark;
	private String PlanCode;
	private String BankCode;
	private String AccName;
	private String BankAccNo;
	private String ProxyName;//代理人姓名
	private String ProxyIDType;//证件类型
	private String ProxyIDNo;//证件号码
	private String ProxyIDStartDate;//证件生效日期
	private String ProxyIDEndDate;//证件失效日期
	private String ProxyPhone;//代理人联系电话
	private String Excel;
	private String AgentFlag;//是否代理
	private String PayMode;//收付费方式
	private String Bank;//银行编码
	private String PayDate;//转账日期
	
	public String getPayMode() {
		return PayMode;
	}
	public void setPayMode(String payMode) {
		PayMode = payMode;
	}
	public String getBank() {
		return Bank;
	}
	public void setBank(String bank) {
		Bank = bank;
	}
	public String getPayDate() {
		return PayDate;
	}
	public void setPayDate(String payDate) {
		PayDate = payDate;
	}
	public String getAgentFlag() {
		return AgentFlag;
	}
	public void setAgentFlag(String agentFlag) {
		AgentFlag = agentFlag;
	}
	public String getExcel() {
		return Excel;
	}
	public void setExcel(String excel) {
		Excel = excel;
	}
	public String getProxyName() {
		return ProxyName;
	}
	public void setProxyName(String proxyName) {
		ProxyName = proxyName;
	}
	public String getProxyIDType() {
		return ProxyIDType;
	}
	public void setProxyIDType(String proxyIDType) {
		ProxyIDType = proxyIDType;
	}
	public String getProxyIDNo() {
		return ProxyIDNo;
	}
	public void setProxyIDNo(String proxyIDNo) {
		ProxyIDNo = proxyIDNo;
	}
	public String getProxyIDStartDate() {
		return ProxyIDStartDate;
	}
	public void setProxyIDStartDate(String proxyIDStartDate) {
		ProxyIDStartDate = proxyIDStartDate;
	}
	public String getProxyIDEndDate() {
		return ProxyIDEndDate;
	}
	public void setProxyIDEndDate(String proxyIDEndDate) {
		ProxyIDEndDate = proxyIDEndDate;
	}
	public String getProxyPhone() {
		return ProxyPhone;
	}
	public void setProxyPhone(String proxyPhone) {
		ProxyPhone = proxyPhone;
	}
	public String getEdorValiDate() {
		return EdorValiDate;
	}
	public void setEdorValiDate(String edorValiDate) {
		EdorValiDate = edorValiDate;
	}
	public String getPriorityNo() {
		return PriorityNo;
	}
	public void setPriorityNo(String priorityNo) {
		PriorityNo = priorityNo;
	}
	public String getWorkTypeNo() {
		return WorkTypeNo;
	}
	public void setWorkTypeNo(String workTypeNo) {
		WorkTypeNo = workTypeNo;
	}
	public String getAcceptWayNo() {
		return AcceptWayNo;
	}
	public void setAcceptWayNo(String acceptWayNo) {
		AcceptWayNo = acceptWayNo;
	}
	public String getApplyTypeNo() {
		return ApplyTypeNo;
	}
	public void setApplyTypeNo(String applyTypeNo) {
		ApplyTypeNo = applyTypeNo;
	}
	public String getApplyName() {
		return ApplyName;
	}
	public void setApplyName(String applyName) {
		ApplyName = applyName;
	}
	public String getRemark() {
		return Remark;
	}
	public void setRemark(String remark) {
		Remark = remark;
	}
	public String getPlanCode() {
		return PlanCode;
	}
	public void setPlanCode(String planCode) {
		PlanCode = planCode;
	}
	public String getBankCode() {
		return BankCode;
	}
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}
	public String getAccName() {
		return AccName;
	}
	public void setAccName(String accName) {
		AccName = accName;
	}
	public String getBankAccNo() {
		return BankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}
	
	
	
}
