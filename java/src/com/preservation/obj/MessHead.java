package com.preservation.obj;

import java.io.Serializable;

/**
 * 接收报文头的实体类
 * 
 * @author LiHao
 *
 */
public class MessHead implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String BatchNo;
	private String SendDate;
	private String SendTime;
	private String BranchCode;
	private String SendOperator;
	private String MsgType;

	public String getBatchNo() {
		return BatchNo;
	}

	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}

	public String getSendDate() {
		return SendDate;
	}

	public void setSendDate(String sendDate) {
		SendDate = sendDate;
	}

	public String getSendTime() {
		return SendTime;
	}

	public void setSendTime(String sendTime) {
		SendTime = sendTime;
	}

	public String getBranchCode() {
		return BranchCode;
	}

	public void setBranchCode(String branchCode) {
		BranchCode = branchCode;
	}

	public String getSendOperator() {
		return SendOperator;
	}

	public void setSendOperator(String sendOperator) {
		SendOperator = sendOperator;
	}

	public String getMsgType() {
		return MsgType;
	}

	public void setMsgType(String msgType) {
		MsgType = msgType;
	}
}
