package com.preservation.obj;

import java.io.Serializable;
import java.util.List;

import org.jdom.Element;
/**
 * WD操作过程中查询的数据，方便取用
 * 
 * @author LiHao
 *
 */
public class FormData implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private String EdorAcceptNo;//保全受理号
	private String EdorNo;
	private String GrpContNo;
	private String EdorAppDate;
	private String EdorValiDate;
	private String EdorType;
	private String ManageCom;
	private String ApplyTypeNo;//申请人类型
	private String ApplyName;//申请人姓名
	private String PriorityNo;//优先级别
	private String WorkTypeNo;//子业务类型
	private String AcceptWayNo;//受理途径
	private String Remark;//备注
	private String AgentFlag;//是否代理
	private String ProxyName;//代理人姓名
	private String ProxyIDType;//证件类型
	private String ProxyIDNo;//证件号码
	private String ProxyIDStartDate;//证件生效日期
	private String ProxyIDEndDate;//证件失效日期
	private String ProxyPhone;//代理人联系电话
	private Double PremWZ  = 0.0;//人均期交保费
	private int Peoples = 0;//增加、减少人数
	private String PlanCode;//保障计划
	private List<Element> PlanCodes;//保障计划
	private Double SumPrem = 0.0;//应交保费
	private String PayMode;//收付费方式
	private String Bank;//银行编码
	private String BankAccNo;//银行账号
	private String PayDate;//转账日期
	private String endDate;//截止日期
	private String cost;//工本费
	private String reason_tb;//退保原因

	private String grpPolNo;//团体保单号
	private String groupMoney;//追加管理金额

	private String riskCode;//险种
	private Double EdorValue;//结余返还金额

	private String excel;//excel文件地址
	
	private List<Element> Money;//团队固定账户金额集合
	
	private String RateType;//利息/利率的类型
	private String AccRate;//利息/利率
	
	private String BankCode;//银行编码
	private String AccName;//账户名
	private String SerialNo;//客户序号
	private String InsuredNo;//被保人客户号
	
	private  List<Element> element;//通用节点
	
	
	public List<Element> getElement() {
		return element;
	}
	public void setElement(List<Element> element) {
		this.element = element;
	}
	public List<Element> getPlanCodes() {
		return PlanCodes;
	}
	public void setPlanCodes(List<Element> planCodes) {
		PlanCodes = planCodes;
	}
	public String getInsuredNo() {
		return InsuredNo;
	}
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getBankCode() {
		return BankCode;
	}
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}
	public String getAccName() {
		return AccName;
	}
	public void setAccName(String accName) {
		AccName = accName;
	}
	public String getAccRate() {
		return AccRate;
	}
	public void setAccRate(String accRate) {
		AccRate = accRate;
	}
	public String getRateType() {
		return RateType;
	}
	public void setRateType(String rateType) {
		RateType = rateType;
	}
	public List<Element> getMoney() {
		return Money;
	}
	public void setMoney(List<Element> money) {
		Money = money;
	}
	public String getExcel() {
		return excel;
	}
	public void setExcel(String excel) {
		this.excel = excel;
	}
	public String getGrpPolNo() {
		return grpPolNo;
	}
	public void setGrpPolNo(String grpPolNo) {
		this.grpPolNo = grpPolNo;
	}
	public String getGroupMoney() {
		return groupMoney;
	}
	public void setGroupMoney(String groupMoney) {
		this.groupMoney = groupMoney;
	}
	
	
	public String getPayDate() {
		return PayDate;
	}
	public void setPayDate(String payDate) {
		PayDate = payDate;
	}
	public String getPayMode() {
		return PayMode;
	}
	public void setPayMode(String payMode) {
		PayMode = payMode;
	}
	public String getBank() {
		return Bank;
	}
	public void setBank(String bank) {
		Bank = bank;
	}
	public String getBankAccNo() {
		return BankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}
	public String getPlanCode() {
		return PlanCode;
	}
	public void setPlanCode(String planCode) {
		PlanCode = planCode;
	}
	public Double getSumPrem() {
		return SumPrem;
	}
	public void setSumPrem(Double sumPrem) {
		SumPrem = sumPrem;
	}
	public Double getPremWZ() {
		return PremWZ;
	}
	public void setPremWZ(Double premWZ) {
		PremWZ = premWZ;
	}
	public int getPeoples() {
		return Peoples;
	}
	public void setPeoples(int peoples) {
		Peoples = peoples;
	}
	public String getApplyName() {
		return ApplyName;
	}
	public void setApplyName(String applyName) {
		ApplyName = applyName;
	}
	public String getAgentFlag() {
		return AgentFlag;
	}
	public void setAgentFlag(String agentFlag) {
		AgentFlag = agentFlag;
	}
	public String getProxyName() {
		return ProxyName;
	}
	public void setProxyName(String proxyName) {
		ProxyName = proxyName;
	}
	public String getProxyIDType() {
		return ProxyIDType;
	}
	public void setProxyIDType(String proxyIDType) {
		ProxyIDType = proxyIDType;
	}
	public String getProxyIDNo() {
		return ProxyIDNo;
	}
	public void setProxyIDNo(String proxyIDNo) {
		ProxyIDNo = proxyIDNo;
	}
	public String getProxyIDStartDate() {
		return ProxyIDStartDate;
	}
	public void setProxyIDStartDate(String proxyIDStartDate) {
		ProxyIDStartDate = proxyIDStartDate;
	}
	public String getProxyIDEndDate() {
		return ProxyIDEndDate;
	}
	public void setProxyIDEndDate(String proxyIDEndDate) {
		ProxyIDEndDate = proxyIDEndDate;
	}
	public String getProxyPhone() {
		return ProxyPhone;
	}
	public void setProxyPhone(String proxyPhone) {
		ProxyPhone = proxyPhone;
	}
	public String getRemark() {
		return Remark;
	}
	public void setRemark(String remark) {
		Remark = remark;
	}
	public String getWorkTypeNo() {
		return WorkTypeNo;
	}
	public void setWorkTypeNo(String workTypeNo) {
		WorkTypeNo = workTypeNo;
	}
	public String getAcceptWayNo() {
		return AcceptWayNo;
	}
	public void setAcceptWayNo(String acceptWayNo) {
		AcceptWayNo = acceptWayNo;
	}

	
	
	public String getApplyTypeNo() {
		return ApplyTypeNo;
	}
	public void setApplyTypeNo(String applyTypeNo) {
		ApplyTypeNo = applyTypeNo;
	}

	public String getPriorityNo() {
		return PriorityNo;
	}

	public void setPriorityNo(String priorityNo) {
		PriorityNo = priorityNo;
	}

	

	public String getEdorAcceptNo() {
		return EdorAcceptNo;
	}

	public void setEdorAcceptNo(String edorAcceptNo) {
		EdorAcceptNo = edorAcceptNo;
	}

	public String getEdorNo() {
		return EdorNo;
	}

	public void setEdorNo(String edorNo) {
		EdorNo = edorNo;
	}

	public String getGrpContNo() {
		return GrpContNo;
	}

	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}

	public String getEdorAppDate() {
		return EdorAppDate;
	}

	public void setEdorAppDate(String edorAppDate) {
		EdorAppDate = edorAppDate;
	}

	public String getEdorValiDate() {
		return EdorValiDate;
	}

	public void setEdorValiDate(String edorValiDate) {
		EdorValiDate = edorValiDate;
	}

	public String getEdorType() {
		return EdorType;
	}

	public void setEdorType(String edorType) {
		EdorType = edorType;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getReason_tb() {
		return reason_tb;
	}
	public void setReason_tb(String reason_tb) {
		this.reason_tb = reason_tb;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getRiskCode()
	{
		return riskCode;
	}
	public void setRiskCode(String riskCode)
	{
		this.riskCode = riskCode;
	}
	public Double getEdorValue()
	{
		return EdorValue;
	}
	public void setEdorValue(Double edorValue)
	{
		EdorValue = edorValue;
	}
	
}
