package com.preservation.obj;

import java.io.Serializable;

/**
 * 接收报文体的第一个节点
 * 
 * @author LiHao
 *
 */
public class LCGrpContInfo implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private String CustomerNo;
	private String GrpContNo;
	private String GrpName;

	private String GrpBankCode;
	private String GrpAccName;
	private String GrpBankAccNo;
	
	public String getGrpBankCode() {
		return GrpBankCode;
	}

	public void setGrpBankCode(String grpBankCode) {
		GrpBankCode = grpBankCode;
	}

	public String getGrpAccName() {
		return GrpAccName;
	}

	public void setGrpAccName(String grpAccName) {
		GrpAccName = grpAccName;
	}

	public String getGrpBankAccNo() {
		return GrpBankAccNo;
	}

	public void setGrpBankAccNo(String grpBankAccNo) {
		GrpBankAccNo = grpBankAccNo;
	}

	public String getCustomerNo() {
		return CustomerNo;
	}

	public void setCustomerNo(String customerNo) {
		CustomerNo = customerNo;
	}

	public String getGrpContNo() {
		return GrpContNo;
	}

	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}

	public String getGrpName() {
		return GrpName;
	}

	public void setGrpName(String grpName) {
		GrpName = grpName;
	}
}
