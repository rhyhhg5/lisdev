package com.preservation.obj;

import java.io.Serializable;

public class CustomerInfo implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private String CustomerNo;
	private String WorkNo;//������

	public String getWorkNo() {
		return WorkNo;
	}

	public void setWorkNo(String workNo) {
		WorkNo = workNo;
	}

	public String getCustomerNo() {
		return CustomerNo;
	}

	public void setCustomerNo(String customerNo) {
		CustomerNo = customerNo;
	}
}
