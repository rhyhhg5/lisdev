package com.preservation.BB;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.BQBase.InterfaceBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;

public class InterfaceBB extends InterfaceBase{

	/**
	 * 保全保费回补逻辑处理
	 * @throws Exception 
	 */
	public boolean dealc(){
		DealBB dealBB = new DealBB();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		if (!dealBB.deal(data)) {
			errorValue = dealBB.getErrorValue();
			gongdanhao = dealBB.getEdorAcceptNo();
			try{
				String sql = "insert into ErrorInsertLog " +
				"(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ," +
				"Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ," +
				"transactionBatch ,transactionDescription ,transactionAddress ) " +
				"values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
				+GI.Operator+"','"
				+pxml+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), " +
				"to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), " +
				"'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+returnXML(pxml)+"','中间状态','"+errorValue+"','"+mMessHead.getMsgType()+"','"
				+mMessHead.getBranchCode()+"','"+mMessHead.getBatchNo()+"','交易描述','交易地址')";
				tExeSQL.execUpdateSQL(sql);
			}catch (Exception e) {
			}
			
			return false;
		}
		errorValue = "";
		gongdanhao = dealBB.getEdorAcceptNo();
		return true;
	}

	
	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement ){
		Element BB = rootElement.getChild("BB");
		// 客户号
		Element msgCustomer = BB.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = BB.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = BB.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

//		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorType("BB");
		System.out.println("==================================="+xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate")); //保全生效日期
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));       //优先级别
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));			//子业务类型
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));		//受理途径
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		tFormData.setCost(xxFormData.getChildText("cost"));
		tFormData.setReason_tb(xxFormData.getChildText("reason_tb"));
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

		tFormData.setEdorValue(Double.parseDouble(xxFormData.getChildText("EdorValue")));
		data.add(tFormData);
	}

	
	public static void main(String[] args) {

		InterfaceBB tInterfaceBB = new InterfaceBB();

		try {
			InputStream pIns = new FileInputStream("C:/Users/dongjl/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(),
					"GBK");
			tInterfaceBB.deal(mInXmlStr);
			tInterfaceBB.returnXML(mInXmlStr);
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}
