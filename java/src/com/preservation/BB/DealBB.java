package com.preservation.BB;

import com.preservation.BQBase.DealBase;
import com.sinosoft.lis.bq.GEdorBJDetailUI;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * 保费补回
 *
 */
public class DealBB extends DealBase
{
	/**
	 * 保全项目明细
	 * 
	 * @param data
	 * @return
	 */

	public boolean checkSave(){
		ExeSQL tExeSQL = new ExeSQL();

		Double num = mFormData.getEdorValue();
		if(num<=0){
			Content = "输入的数值必须大于0："+num;
			System.out.println(Content);
			return false;
		}
		//明细调用后台方法
		
		  CErrors tError = null; 
		  String tRela  = "";                
		  String FlagStr = "";
		  String transact = "INSERT";  

			String edorNo = EdorAcceptNo;
			String edorType = "BB";
			String edorValue = mFormData.getEdorValue().toString();
			
			LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
			tLPGrpEdorItemSchema.setEdorNo(edorNo);
			//tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
			tLPGrpEdorItemSchema.setGetMoney(edorValue);
			tLPGrpEdorItemSchema.setEdorType(edorType);

		  // 准备传输数据 VData
	  		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
	  		tLPEdorEspecialDataSchema.setEdorNo(edorNo);
	  		tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
	  		
	  		tLPEdorEspecialDataSchema.setEdorType(edorType);
	  		tLPEdorEspecialDataSchema.setEdorValue(edorValue);
	  		
	  		
		    tLPEdorEspecialDataSchema.setDetailType("BBMONEY");
		    tLPEdorEspecialDataSchema.setPolNo("000000");
			
			VData tVData = new VData();
			tVData.add(GI);
			tVData.add(tLPGrpEdorItemSchema);
			tVData.add(tLPEdorEspecialDataSchema);
			//

			GEdorBJDetailUI tGEdorBJDetailUI = new GEdorBJDetailUI();
			if (!tGEdorBJDetailUI.submitData(tVData, transact))
			{
				VData rVData = tGEdorBJDetailUI.getResult();
				System.out.println("Submit Failed! " + tGEdorBJDetailUI.mErrors.getErrContent());
				Content = transact + "失败，原因是:" + tGEdorBJDetailUI.mErrors.getFirstError();
				FlagStr = "Fail";
				return false;
			}
			else 
			{
				Content = "保存成功";
				FlagStr = "Success";
				System.out.println(Content);
			}
		
		return true;
	}

	

}
