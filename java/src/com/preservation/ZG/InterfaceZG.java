package com.preservation.ZG;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.BQBase.InterfaceBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;

/**
 * @author dongjiali
 * 
 * 	追加管理费
 *
 */
public class InterfaceZG extends InterfaceBase{


	
	/**
	 * 逻辑处理
	 */
	public boolean dealc(){
		// 处理逻辑
		DealZG dealZG  = new DealZG();
		if (!dealZG.deal(data)) {
			errorValue = dealZG.getErrorValue();
			gongdanhao = dealZG.getEdorAcceptNo();
			return false;
		}
		gongdanhao = dealZG.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	
	

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement ){
		Element ZG = rootElement.getChild("ZG");
		// 客户号
		
		Element msgCustomer = ZG.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = ZG.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = ZG.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		
		FormData tFormData = new FormData();

		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		
		tFormData.setGrpPolNo(xxFormData.getChildText("grpPolNo"));
		tFormData.setGroupMoney(xxFormData.getChildText("groupMoney"));
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		tFormData.setElement(xxFormData.getChildren("Pols"));
		data.add(tFormData);
	}

	public static void main(String[] args) {
		InterfaceZG tInterfaceZG = new InterfaceZG();
		
		try {
			InputStream pIns = new FileInputStream("C:/Users/dongjl/Desktop/ZG.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceZG.deal(mInXmlStr);
			System.out.println(tInterfaceZG.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
	
}
