package com.preservation.ZG;

import java.util.List;

import org.jdom.Element;

import com.preservation.BQBase.DealBase;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GEdorZGDetailUI;
import com.sinosoft.utility.ExeSQL;

/**
 * @author dongjiali
 * 
 * 	追加管理费
 *
 */
public class DealZG extends DealBase{

	/**
	 * 保全项目明细
	 * 保存按钮
	 * @return
	 */
	public boolean checkSave(){
		ExeSQL tExeSQL = new ExeSQL();
		List<Element> list=mFormData.getElement();
		/*
		String grpPolNo=mFormData.getGrpPolNo();
		String groupMoney=mFormData.getGroupMoney();//获取追加管理费
*/		
		String edorNo=EdorAcceptNo;//保全号
		String edorType=dMessHead.getMsgType();//保全类型
		String grpContNo=mLCGrpContInfo.getGrpContNo();//集体保单号
		
			EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo,
					edorType);
			for(int i=0;i<list.size();i++){
				tSpecialData.setGrpPolNo(list.get(i).getChildText("grpPolNo"));//险种号
				tSpecialData.add("GroupMoney",list.get(i).getChildText("groupMoney"));//追加管理费
			}
			GEdorZGDetailUI tGEdorZGDetailUI = new GEdorZGDetailUI(GI, edorNo,
					grpContNo, tSpecialData);
			if (!tGEdorZGDetailUI.submitData()) {
			
				Content = "数据保存失败！原因是:" + tGEdorZGDetailUI.getError();
				return false;
			} else {
				Content = "数据保存成功。";
				return true;
		}
		
		
	
	}

}
