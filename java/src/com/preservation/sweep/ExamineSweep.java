package com.preservation.sweep;

import java.io.IOException;
import java.io.StringReader;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.easyscan.EsModifyUI;
import com.sinosoft.lis.easyscan.LLEsModifyUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.Es_IssueDocSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.preservation.util.StringUtil;
import com.writeoff.GrpDueFeeSplit;

/**
 * 扫描修改审核接口
 * 
 * @author 姜士杰 2018-11-14
 *
 */

public class ExamineSweep {

	private Logger log = Logger.getLogger(GrpDueFeeSplit.class);
	private XMLOutputter out = new XMLOutputter();

	// 提示信息
	private String Content = "";
	// 问题流水号
	private String IssueDocID = "";
	// 业务号码
	private String BussNo = "";
	// 业务类型
	private String BussType = "";
	// 单证细类
	private String SubType = "";
	// 标志
	private String SuccessFlag = "";
	// 批次号
	private String BatchNo = "";
	// 管理机构
	private String BranchCode = "";
	// 操作人员
	private String PromptOperator = "";
	// 报文接口
	private String MsgType = "";
	// 返回报文
	private String responseXml = "";
	// 操作符
	private String tOperate = "ApplyPass";
	//状态一
	private String status = "";
	//状态二
	private String stateflag = "";

	public String getResponseXml() {
		return responseXml;
	}
	// 业务逻辑处理
	public boolean deal(String xml) throws Exception {
		// TODO Auto-generated method stub
		
		// 接收报文是否为空
		if (!StringUtil.isNull(xml)) {
			Content = "请求报文不能为空！";
			SuccessFlag = "0";
			getReturnxml();
			return false;
		}
		
		// 解析报文
		if (!getdata(xml)) {
			return false;
		}
		
		return true;
	}

	// 解析验证报文
	public boolean getdata(String xml) throws Exception {

		System.out.println("***********开始getdata解析报文*********");

		StringReader reader = new StringReader(xml);
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document doc = tSAXBuilder.build(reader);
		Element rootElement = doc.getRootElement();

		// 报文
		Element msgHead = rootElement.getChild("MsgHead");
		Element elehead = msgHead.getChild("Item");

		// 报文头
		BatchNo = elehead.getChildText("BatchNo");
		MsgType = elehead.getChildText("MsgType");

		// 报文体
		msgHead = rootElement.getChild("MsgBody");
		elehead = msgHead.getChild("Item");

		// 获取单证号码
		BussNo = elehead.getChildText("DocCode");
		BranchCode = elehead.getChildText("ManageCom");
		PromptOperator = elehead.getChildText("Operator");
		elehead.getChildText("IssueType");
		elehead.getChildText("IssueDesc");
		System.out.println(BussNo);

		// 验证单证号码是否为空
		if (BussNo == null || "".equals(BussNo)) {
			Content = "单证号码为空！";
			SuccessFlag = "0";
			getReturnxml();
			return false;
		}

		// 验证单证号码是否存在申请修改池
		ExeSQL tExeSQL = new ExeSQL();
		
        String  sql="select distinct SubType, busstype,DocID, DocCode  from\r\n" + 
        		" (  select edm.SubType , edm.busstype ,edm.docid, edm.doccode, edd.subtypename, edm.numpages, edm.makedate,  edm.scanoperator, edm.managecom,\r\n" + 
        		" ei.Status Status, ei.StateFlag StateFlag, ei.Result Result  from es_doc_main edm  inner join es_doc_def edd on edd.SubType = edm.SubType  \r\n" + 
        		"inner join es_doc_relation edr on edr.DocID = edm.DocID  left join es_issuedoc ei on ei.BussNo = edm.DocCode \r\n" + 
        		" where 1 = 1  and edm.doccode='"+BussNo+"'    \r\n" + 
        		" and (edm.State is null or edm.State != '03')  and ei.Status = '1' and ei.StateFlag = '1'  )";
		
        // 执行查询SQL获取返回结果
		SSRS sqlSSRS = tExeSQL.execSQL(sql);
		
		if (sqlSSRS.getMaxRow() < 1) {
			
			//查询问题件数据
			String sql2="select * from ES_Issuedoc where  bussno='"+BussNo+"' and ((status='1' and (stateflag !=2 and stateflag!=4 ))  or (status!='1') )";
			SSRS sql2SSRS = tExeSQL.execSQL(sql2);
			
			if(sql2SSRS.getMaxRow() <= 0 ){
				
				Content = "此单号："+BussNo+"未提交修改！";
				SuccessFlag = "0";
				getReturnxml();
				return false;
			}
			
			if(sql2SSRS.getMaxRow() > 1){
				Content = "在扫描修改池中,号码为:"+BussNo+"的单证的数目为:"+sql2SSRS.getMaxRow()+"而不是唯一!";
				SuccessFlag = "0";
				getReturnxml();
				return false;
			}
			
			//核对单证状态
			if(sql2SSRS.getMaxRow() == 1){
				
				status = sql2SSRS.GetText(1, 7);
				stateflag = sql2SSRS.GetText(1, 15);
				
				if("0".equals(status) && "2".equals(stateflag)){
					
					Content = "此单号："+BussNo+"已审核完成，待扫描修改！";
					SuccessFlag = "0";
					getReturnxml();
					return false;
				}
				
				if("1".equals(status) && "1".equals(stateflag)){
					
					Content = "此单号："+BussNo+"已提交修改申请，待审核！";
					SuccessFlag = "0";
					getReturnxml();
					return false;
				}
			}
			
			Content = "未找到符合条件的数据！";
			SuccessFlag = "0";
			getReturnxml();
			return false;
		} else {
			
			//获取数据
			IssueDocID = sqlSSRS.GetText(1, 3);
			BussType = sqlSSRS.GetText(1, 2);
			SubType = sqlSSRS.GetText(1, 1);
		}

		Es_IssueDocSchema es_IssueDocSchema = new Es_IssueDocSchema();
		VData vDate = new VData();
		
		//审核用户
		GlobalInput tGI = new GlobalInput();
		tGI.ManageCom = BranchCode;
		
		es_IssueDocSchema.setIssueDocID(IssueDocID);
		es_IssueDocSchema.setBussNo(BussNo);
		es_IssueDocSchema.setBussType(BussType);
		es_IssueDocSchema.setSubType(SubType);
		vDate.add(es_IssueDocSchema);
		
		System.out.println("****************单证细类："+SubType);
		
		//判断是否为理赔单号
		if("LP01".equals(SubType) || "LP02".equals(SubType) || "LP03".equals(SubType) || "LP04".equals(SubType)){
			
			LLEsModifyUI tLLEsModifyUI = new LLEsModifyUI();
			
			//判断审核人员
			String sql3="select handler from llcase where  rgtno='"+BussNo+"'";
			SSRS sql3SSRS = tExeSQL.execSQL(sql3);
			
			if(sql3SSRS.getMaxRow() < 1){
				
				String sql4 = "select handler from llcase where  caseno='"+BussNo+"'";
				SSRS sql4SSRS = tExeSQL.execSQL(sql4);
				
				if(sql4SSRS.getMaxRow() < 1){
					Content = "审核失败,未查询到数据！";
					SuccessFlag = "0";
					getReturnxml();
					return false;
				} else {
					tGI.Operator = sql4SSRS.GetText(1, 1);
				}
				
			} else {
				tGI.Operator = sql3SSRS.GetText(1, 1);
			}
			
			vDate.add(tGI);
			
			try {
				
				//提交理赔核心
				if (!tLLEsModifyUI.submitData(vDate, tOperate)) {
					
					Content = "操作失败，原因是:" + tLLEsModifyUI.mErrors.getFirstError();
					SuccessFlag = "0";
					getReturnxml();
					return false;
				}
			} catch (Exception ex) {
				
				Content = "操作失败，原因是:" + ex.toString();
				SuccessFlag = "0";
				getReturnxml();
				return false;
			}
			
			SuccessFlag = "1";
			Content = "您的审核已通过，待扫描修改。";
			getReturnxml();
			return true;
			
		} else {
			
			EsModifyUI tEsModifyUI = new EsModifyUI();
			tGI.Operator = "BAOQA";
			vDate.add(tGI);
			
			try {
				
				//提交扫描核心
				if (!tEsModifyUI.submitData(vDate, tOperate)) {
					
					Content = "操作失败，原因是:" + tEsModifyUI.mErrors.getFirstError();
					SuccessFlag = "0";
					getReturnxml();
					return false;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				Content = "操作失败，原因是:" + e.toString();
				SuccessFlag = "0";
				getReturnxml();
				return false;
			}
			
			SuccessFlag = "1";
			Content = "修改审核成功！";
			getReturnxml();
		}
		
		SuccessFlag = "0";
		Content = "单证号码错误！";
		getReturnxml();
		return false;
	}

	// 返回报文
	public void getReturnxml() throws IOException {
		// TODO Auto-generated method stub

		// 组装返回报文head节点
		Element head_Response = new Element("MsgHead");
		// 组装返回报文bady节点
		Element bady_Response = new Element("MsgBody");
		// head下节点
		Element Item_Response = new Element("Item");
		// body下节点
		Element Item2_Response = new Element("Item");
		// 创建返回报文
		Element DataSet_Response = new Element("DataSet_Response");

		// 组装返回报文
		Element BatchNo = new Element("BatchNo");
		BatchNo.setText(this.BatchNo);//
		Item_Response.addContent(BatchNo);

		Element SendDate = new Element("SendDate");
		SendDate.setText(PubFun.getCurrentDate());// 发送日期
		Item_Response.addContent(SendDate);

		Element SendTime = new Element("SendTime");
		SendTime.setText(PubFun.getCurrentTime());// 发送时间
		Item_Response.addContent(SendTime);

		Element BranchCode = new Element("BranchCode");
		BranchCode.setText(this.BranchCode);// 管理机构
		Item_Response.addContent(BranchCode);

		Element SendOperator = new Element("SendOperator");
		SendOperator.setText(this.PromptOperator);// 操作人员
		Item_Response.addContent(SendOperator);

		Element MsgType = new Element("MsgType");
		MsgType.setText(this.MsgType);// 报文接口
		Item_Response.addContent(MsgType);

		Element BussNo = new Element("DocCode");
		BussNo.setText(this.BussNo);// 单证号码
		Item2_Response.addContent(BussNo);

		Element Content = new Element("Content");
		Content.setText(this.Content);// 业务类型
		Item2_Response.addContent(Content);

		Element SuccessFlag = new Element("SuccessFlag");
		SuccessFlag.setText(this.SuccessFlag);// 单证细类
		Item2_Response.addContent(SuccessFlag);

		head_Response.addContent(Item_Response);
		bady_Response.addContent(Item2_Response);

		DataSet_Response.addContent(head_Response);
		DataSet_Response.addContent(bady_Response);
		responseXml = out.outputString(DataSet_Response);
		log.info("扫描修改申请返回报文<?xml version='1.0' encoding='GBK'?>" + responseXml);

		// System.out.println("***************");
		 responseXml = "<?xml version='1.0' encoding='GBK'?>\r\n" + responseXml;
		// System.out.println(responseXml);

	}

}
