/**
 * @Title: EsDocMain.java
 * @Package com.preservation.sweep
 */
package com.preservation.sweep;

/**
  * @ClassName: EsDocMain
  * @Description: TODO
  * @author liuzehong
  * @date 2018年11月23日 上午9:19:38
  * @version V1.0
 **/
public class EsDocMain {
	//流水号
	private String  docID;
	//单证号码
	private String  docCode;
	//单证类型
	private String  bussType;
	//单证细类
	private String  subType;
	//扫描文件数
	private String  numPages;
	
	private String  docFlag;
	
	private String  inputState;
	
	private String  makeDate;
	
	private String  makeTime;
	
	private String  modifyDate;
	
	private String  modifyTime;
	
	private String ManageCom;
	
	private String  scanNo;
	
	private String  printCode;
	//返回地址
	private String  pictureUrl;
	
	public String getDocID() {
		return docID;
	}

	public void setDocID(String docID) {
		this.docID = docID;
	}

	public String getDocCode() {
		return docCode;
	}

	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}

	public String getMakeDate() {
		return makeDate;
	}

	public void setMakeDate(String makeDate) {
		this.makeDate = makeDate;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getBussType() {
		return bussType;
	}

	public void setBussType(String bussType) {
		this.bussType = bussType;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getNumPages() {
		return numPages;
	}

	public void setNumPages(String numPages) {
		this.numPages = numPages;
	}

	public String getDocFlag() {
		return docFlag;
	}

	public void setDocFlag(String docFlag) {
		this.docFlag = docFlag;
	}

	public String getInputState() {
		return inputState;
	}

	public void setInputState(String inputState) {
		this.inputState = inputState;
	}

	public String getMakeTime() {
		return makeTime;
	}

	public void setMakeTime(String makeTime) {
		this.makeTime = makeTime;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	
	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getScanNo() {
		return scanNo;
	}

	public void setScanNo(String scanNo) {
		this.scanNo = scanNo;
	}

	public String getPrintCode() {
		return printCode;
	}

	public void setPrintCode(String printCode) {
		this.printCode = printCode;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

}
