package com.preservation.sweep;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;


import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 扫描申请查询接口
 * @author 姜士杰 2018-11-5
 *
 */
public class DealSweep {
	//流水号
	private String DocID = "";
	//业务类型
	private String BussTpye = "";
	//报文机构
	private String tManageCom = "";
	//返回报文
	private String responseXml = "";
	//报文操作员
	private String tOperator;
	//单证细类
	private String tSubType;
	//单证号码
	private String tDocCode;
	//提示信息
	private String content;
	//信息集合
	private List<EsDocMain> esDocMainlist=null;
	//单证号码
	private String tBatchNo;
	//发送时间
	private String tSendTime;
	//报文头机构
	private String tBranchCode;
	//报文头操作员
	private String tSendOperator;
	//发送日期
	private String tSendDate;
	//修改时间
	private String tMsgType;
	//返回报文机构
	private String ManageCom;
	
	public String getResponseXml() {
		return responseXml;
	}
	
	//业务逻辑处理
	public boolean deal(String xml) throws Exception{
		//接收报文是否为空
		if(xml == null || "".equals(xml)) {
			content = "报文为空！";
			getReturnxml();
			return false;
		}
		// 解析报文
		if (!getdata(xml)) {
			return false;
		}
		return true;
	}
	
	//解析验证报文
	public boolean getdata(String xml) throws Exception {
		System.out.println("***********开始getdata解析报文*********");
		Document doc;
		try {
			doc = DocumentHelper.parseText(xml);
			Element root = doc.getRootElement();
			Element msgHead = root.element("MsgHead");
			Element thItem = msgHead.element("Item");
			this.tBatchNo=thItem.elementText("BatchNo");
			this.tSendDate=thItem.elementText("SendDate");
			this.tSendTime=thItem.elementText("SendTime");
			this.tBranchCode=thItem.elementText("BranchCode");
			this.tSendOperator=thItem.elementText("SendOperator");
			this.tMsgType=thItem.elementText("MsgType");
			// 获取报文体
			Element msgBody = root.element("MsgBody");
			Element tItem = msgBody.element("Item");
			tManageCom = tItem.elementText("ManageCom");
			this.tOperator = tItem.elementText("Operator");//扫描操作员
			this.tSubType = tItem.elementText("SubType");//单证类型
			this.tDocCode = tItem.elementText("DocCode");  // 单证号码
			ExeSQL tExeSQL = new ExeSQL();
			//获取主机url
			String httpSql="select Sysvarvalue from LDSYSVAR WHERE Sysvar='ServerURL'";
			String url="";
			SSRS httpSqlSSRS = tExeSQL.execSQL(httpSql);
			//获取服务器地址
			if (httpSqlSSRS.getMaxRow() > 0) {
				url=httpSqlSSRS.GetText(1, 1);
			}else {
                content = "获取主机url失败";
				getReturnxml();
				return false;
			}
			
			System.out.println("***********开始数据验证*********");
			SSRS sqlSSRS ;
			if (stringNull(tDocCode)) {
				String sql="select  DocID, DocCode,BussType,SubType,NumPages,DocFlag,InputState,MakeDate,MakeTime,ModifyDate,ModifyTime,ManageCom,ScanNo,PrintCode from Es_Doc_Main where DocCode='"+tDocCode+"'";
				//执行查询SQL获取返回结果
				sqlSSRS = tExeSQL.execSQL(sql);
				if (sqlSSRS.getMaxRow() <= 0 ) {
					content = "单证号码错误,未查询到数据";
					getReturnxml();
					return false;
				}
				if (sqlSSRS.getMaxRow() > 0) {
					
					esDocMainlist = new ArrayList<EsDocMain>();
					for (int i = 1; i <= sqlSSRS.getMaxRow(); i++) {
						
						EsDocMain esDocMain=new EsDocMain();
						esDocMain.setDocID(sqlSSRS.GetText(i, 1));
						esDocMain.setDocCode(sqlSSRS.GetText(i, 2));
						esDocMain.setBussType(sqlSSRS.GetText(i, 3));
						esDocMain.setSubType(sqlSSRS.GetText(i, 4));
						esDocMain.setNumPages(sqlSSRS.GetText(i, 5));
						esDocMain.setDocFlag(sqlSSRS.GetText(i, 6));
						esDocMain.setInputState(sqlSSRS.GetText(i, 7));
						esDocMain.setMakeDate(sqlSSRS.GetText(i, 8));
						esDocMain.setMakeTime(sqlSSRS.GetText(i, 9));
						esDocMain.setModifyDate(sqlSSRS.GetText(i, 10));
						esDocMain.setModifyTime(sqlSSRS.GetText(i, 11));
						esDocMain.setManageCom(sqlSSRS.GetText(i, 12));
						esDocMain.setScanNo(sqlSSRS.GetText(i,13));
						esDocMain.setPrintCode(sqlSSRS.GetText(i, 14));
//					    esDocMain.setPictureUrl(url+"easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+DocID+"&DocCode="+tDocCode+"&BussTpye="+BussTpye+"&SubTpye="+tSubType);
					    esDocMain.setPictureUrl(url+"EasyScanQuery/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+DocID+"&DocCode="+tDocCode+"&BussTpye="+BussTpye+"&SubTpye="+tSubType);
					    esDocMainlist.add(esDocMain);  
					}
				}
			}else {
				
				StringBuffer sb = new StringBuffer();
				sb.append("select  DocID, DocCode,BussType,SubType,NumPages,DocFlag,InputState,MakeDate,MakeTime,ModifyDate,ModifyTime,ManageCom,ScanNo,PrintCode from Es_Doc_Main where 1=1");
				
				if (!(stringNull(tManageCom)&&stringNull(tSubType))) {
					content="管理机构或单证类型不能为空";
					getReturnxml();
					return false;
				}else {
					sb.append("  and managecom='"+tManageCom+"' and SubType='"+tSubType+"'");
					if (stringNull(tOperator)) {
						sb.append(" and scanoperator='"+tOperator+"'");
					}
				}
				sqlSSRS = tExeSQL.execSQL(sb.toString());
				if (sqlSSRS.getMaxRow() <= 0 ) {
					content = "未查询到数据";
					getReturnxml();
					return false;
				}
				if (sqlSSRS.getMaxRow() > 0) {
					
					esDocMainlist=new ArrayList<EsDocMain>();
					for (int i = 1; i <= sqlSSRS.getMaxRow(); i++) {
						
						EsDocMain esDocMain=new EsDocMain();
						esDocMain.setDocID(sqlSSRS.GetText(i, 1));
						esDocMain.setDocCode(sqlSSRS.GetText(i, 2));
						esDocMain.setBussType(sqlSSRS.GetText(i, 3));
						esDocMain.setSubType(sqlSSRS.GetText(i, 4));
						esDocMain.setNumPages(sqlSSRS.GetText(i, 5));
						esDocMain.setDocFlag(sqlSSRS.GetText(i, 6));
						esDocMain.setInputState(sqlSSRS.GetText(i, 7));
						esDocMain.setMakeDate(sqlSSRS.GetText(i, 8));
						esDocMain.setMakeTime(sqlSSRS.GetText(i, 9));
						esDocMain.setModifyDate(sqlSSRS.GetText(i, 10));
						esDocMain.setModifyTime(sqlSSRS.GetText(i, 11));
						esDocMain.setManageCom(sqlSSRS.GetText(i, 12));
						esDocMain.setScanNo(sqlSSRS.GetText(i,13));
						esDocMain.setPrintCode(sqlSSRS.GetText(i, 14));
					    esDocMain.setPictureUrl(url+"easyscan/QCManageInputMainShow.jsp?"
					    		+ "EASYWAY=1&DocID="+sqlSSRS.GetText(i, 1)+"&DocCode="+
					    		sqlSSRS.GetText(i, 2)+"&BussTpye="+sqlSSRS.GetText(i, 3)+
					    		"&SubTpye="+sqlSSRS.GetText(i, 4));
					    esDocMainlist.add(esDocMain);  
					}
				}
			}
			getReturnxml();
		} catch (Exception e) {
			content="解析报文出错";
			getReturnxml();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	//返回报文
	public void getReturnxml() throws IOException {
		StringWriter stringWriter = new StringWriter();  
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("DataSet_Response");
		document.setRootElement(tDateSet);
		//返回报文头样式
		Element tMsgResHead = tDateSet.addElement("MsgHead");
		Element tItem = tMsgResHead.addElement("Item");
		Element tBatchNo = tItem.addElement("BatchNo");
		tBatchNo.addText(this.tBatchNo);
		Element tSendDate = tItem.addElement("SendDate");
		tSendDate.addText(this.tSendDate);
		Element tSendTime = tItem.addElement("SendTime");
		tSendTime.addText(this.tSendTime);
		Element tBranchCode = tItem.addElement("BranchCode");
		tBranchCode.addText(this.tBranchCode);
		Element tSendOperator= tItem.addElement("SendOperator");
		tSendOperator.addText(this.tSendOperator);
		Element tMsgType = tItem.addElement("MsgType");
		tMsgType.addText(this.tMsgType);
		Element tMsgBody = tDateSet.addElement("MsgBody");
		Element tContent = tMsgBody.addElement("Content");
		if (stringNull(this.content)) {
			tContent.addText(content);
		}
		if (esDocMainlist!=null&&esDocMainlist.size()>0) {
			for (int i = 0; i < esDocMainlist.size(); i++) {
				EsDocMain esDocMain =esDocMainlist.get(i);
				
				Element tbItem = tMsgBody.addElement("Item");
				
				Element tbDocCode = tbItem.addElement("DocCode");
				tbDocCode.addText(esDocMain.getDocCode());
				
				Element tbBussType = tbItem.addElement("BussType");
				tbBussType.addText(esDocMain.getBussType());
				
				Element tbSubType = tbItem.addElement("SubType");
				tbSubType.addText(esDocMain.getSubType());
				
				Element tbNumPages = tbItem.addElement("NumPages");
				tbNumPages.addText(esDocMain.getNumPages());
				
				Element tbDocFlag = tbItem.addElement("DocFlag");
				tbDocFlag.addText(esDocMain.getDocFlag());
				
				Element tbInputState = tbItem.addElement("InputState");
				tbInputState.addText(esDocMain.getInputState());
				
				Element tMakeDate = tbItem.addElement("MakeDate");
				tMakeDate.addText(esDocMain.getMakeDate());
				
				Element tMakeTime = tbItem.addElement("MakeTime");
				tMakeTime.addText(esDocMain.getMakeTime());
				
				Element tModifyDate = tbItem.addElement("ModifyDate");
				tModifyDate.addText(esDocMain.getModifyDate());
				
				Element tModifyTime = tbItem.addElement("ModifyTime");
				tModifyTime.addText(esDocMain.getModifyTime());
				
				Element ManageCom = tbItem.addElement("ManageCom");
				ManageCom.addText(esDocMain.getManageCom());
				
				Element tbScanNo = tbItem.addElement("ScanNo");
				tbScanNo.addText(esDocMain.getScanNo());
				
				Element tbPrintCode = tbItem.addElement("PrintCode");
				tbPrintCode.addText(esDocMain.getPrintCode());
				
				Element tbPictureUrl= tbItem.addElement("PictureUrl");
				tbPictureUrl.addText(esDocMain.getPictureUrl());
			}
			
		}
		 XMLWriter xmlWriter = new XMLWriter(stringWriter); 
	        //写入文件  
	        try {
				xmlWriter.write(document);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}  
	        //关闭  
	        try {
				xmlWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(e.toString());
			} 
		    this.responseXml = stringWriter.toString();
			System.out.println(responseXml);
	}
	
	//读取报文
	
	
	public boolean stringNull(String str){
		if(!"".equals(str)&&null!=str){
			return true;
		}else{
			return false;
		}
	}
	
	public static void main(String[] args) {
	}

}
