package com.preservation.LP;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.Date;



import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.preservation.BQBase.DealBase;
import com.preservation.BQBase.Exceldeal;
import com.preservation.utilty.StringUtil;

import com.sinosoft.lis.bq.GEdorGrpLPDetailUI;

import com.sinosoft.lis.bq.GEdorLPDetailUI;
import com.sinosoft.lis.bq.PEdorLPDetailUI;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

import com.sinosoft.utility.VData;

public class DealLP extends DealBase {

	/**
	 * 当保全有单独逻辑的时候重写次方法
	 */
	@Override
	public boolean domake(){
		if(!checkupdata()){
			log.info("**************团体理赔金银行账户修改**************************");
			return false;
		}
		
		if(!checkupdatas()){
			log.info("***************客户理赔金账号变更（个人）**************************");
			return false;
		}
		if(StringUtil.StringNull(map.get("ExcelURL").toString())){
			getExcel(map.get("ExcelURL").toString());
			/*LQreturn lt=new LQreturn();
			lt.setMatData(map);*/
			Exceldeal ed=new Exceldeal();
			ed.setMatData(map);
			Thread t=new Thread(ed);
			t.start();
		}
		
		return true;
	} 
  
	/**
	 * 团体理赔金银行账户修改
	 */
	private boolean checkupdata(){
		String edorNo = EdorAcceptNo;
		String grpContNo = mLCGrpContInfo.getGrpContNo();
		LCGrpAppntSchema tLCGrpAppntSchema=new LCGrpAppntSchema();
		tLCGrpAppntSchema.setClaimBankCode(mFormData.getBankCode());
		tLCGrpAppntSchema.setClaimBankAccNo(mFormData.getBankAccNo());
		tLCGrpAppntSchema.setClaimAccName(mFormData.getAccName());
		//准备传输数据 VData
		VData tVData = new VData();  
		
		//保存个人保单信息(保全)
		tVData.addElement(tLCGrpAppntSchema);
		 GEdorGrpLPDetailUI tGEdorGrpLPDetailUI = new GEdorGrpLPDetailUI(GI, edorNo, grpContNo);
			if (!tGEdorGrpLPDetailUI.submitData(tVData))
			{
				
				Content = "数据保存失败！原因是:" + tGEdorGrpLPDetailUI.getError();
				return false;
			}else 
			{
				
				Content = "数据保存成功。";
				return true;
			}
	}
	/**
	 *
	 *客户理赔金账号变更（个人）
	 *
	 */
	private boolean checkupdatas(){
		String FlagStr = "";
		String Content = "";
		String transact = "";
		String Result = "";
	  PEdorLPDetailUI tPEdorLPDetailUI = new PEdorLPDetailUI();
	  try
		{
		
	  
		//理赔金帐号更改  add  by Lanjun 2005-5-24 10:53
		//---------------------------------------------------------------//	
		LPDiskImportSchema tLPDiskImportSchema  = new LPDiskImportSchema();
	  tLPDiskImportSchema.setEdorNo(EdorAcceptNo);
	  tLPDiskImportSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		tLPDiskImportSchema.setEdorType(dMessHead.getMsgType());
		tLPDiskImportSchema.setInsuredNo(mCustomerInfo.getCustomerNo());
		tLPDiskImportSchema.setBankCode(mFormData.getBankCode());
	  tLPDiskImportSchema.setBankAccNo(mFormData.getBankAccNo());                
	  tLPDiskImportSchema.setAccName(mFormData.getAccName());
	 	tLPDiskImportSchema.setSerialNo(mFormData.getSerialNo());

	  //---------------------------------------------------------------//
		

			VData data = new VData();
			data.addElement(GI);
			data.addElement(tLPDiskImportSchema);

			tPEdorLPDetailUI.submitData(data, transact);
		}
		catch(Exception ex)
		{
			Content = transact+"失败，原因是:" + ex.toString();
		   return  false;
		}
	
			if (!tPEdorLPDetailUI.mErrors.needDealError())
			{                          
				Content = " 保存成功";
				return true;
			}
			else                                                                           
			{
				Content = " 保存失败，原因是:" + tPEdorLPDetailUI.mErrors.getFirstError();
				return false;
			}
		
	}
	/**
	 * 保全项目明细
	 * 保存按钮
	 * @return
	 */
	public boolean checkSave(){
		ExeSQL tExeSQL = new ExeSQL();
		String customerno=mCustomerInfo.getCustomerNo();//客户号
		String edorNo=EdorAcceptNo;
		String edorType=dMessHead.getMsgType();
		String grpContNo=mLCGrpContInfo.getGrpContNo();
		//报文参数的非空校验
		if(!StringUtil.StringNull(edorNo)||!StringUtil.StringNull(edorType)||!StringUtil.StringNull(grpContNo)||!StringUtil.StringNull(customerno)){
			Content="保全号，保全类型，团体合同号,客户号不能为空";
			return false;
		}
		String custSql="select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday,  a.IDType, a.IDNo, a.OccupationCode, a.OccupationType from LCInsured a, LCCont b where '1489051947000'='1489051947000' and  a.ContNo = b.ContNo and not exists (select insuredno from LPDiskImport     where InsuredNo = a.InsuredNo     and EdorType = 'LP'     and EdorNo = '"+edorNo+"')and b.AppFlag = '1' and a.GrpContNo = '"+grpContNo+"'  and a.InsuredNo='"+customerno+"' order by InsuredNo fetch first 3000 rows only with ur ";
		SSRS ssrs=tExeSQL.execSQL(custSql);
		String  lPsql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo, ErrorReason " +
        "from LPDiskImport " +
        "where GrpContNo = '" + grpContNo+ "' " +
        "and EdorNo = '" +edorNo + "' " +
        "and EdorType = '" + edorType + "' " +
        "and State = '0' " +
        "order by Int(SerialNo) ";
		SSRS ssrs1=tExeSQL.execSQL(lPsql);
		if(ssrs.getMaxRow()<=0){//根据客户号查询被保人信息
			Content="没有查询到客户信息";
			return false;
		}else if(ssrs1.getMaxRow()>0){//跟就客户号，保全号，保全类型查询数据是否是有效数据（针对页面导入功能）
			Content="无效的数据";
			return false;
		}
		GEdorLPDetailUI tGEdorLPDetailUI = new GEdorLPDetailUI(GI, edorNo, grpContNo);
			if (!tGEdorLPDetailUI.submitData())
			{
				
				Content = "数据保存失败！原因是:" + tGEdorLPDetailUI.getError();
				return false;
			}
			else 
			{
				Content = "数据保存成功。";
				String message = tGEdorLPDetailUI.getMessage();
				if (message != null)
				{
				  Content = message;
				  return false;
				}
				return true;
			}
	}
	
	/**
	 * 
	 * 保全导入Excel时，
	 * 
	 * 解析Excel数据
	 * 
	 */
	public boolean getExcel(String  ExcelURL){
		
	/*	Element eBody = rootElement.getChild("MsgBody");
		Element eWS = eBody.getChild("WS");
		Element lCGrpContInfo = eWS.getChild("LCGrpContTable");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");
		String ExcelURL = eLCGrpContInfo.getChildText("Excel");*/
		String FileName = ExcelURL.split("/")[ExcelURL.split("/").length - 1];
		String FTPPath = ExcelURL.substring(0, ExcelURL.length() - FileName.length());
		// 人员信息
		// FTP下载人员文件.xls
		String sqlFTP = "select DESCRIPTION,CODE from ldconfig where codetype = 'WSInterface'";
		SSRS ssqlFTP = new ExeSQL().execSQL(sqlFTP);
		if (ssqlFTP.getMaxRow() != 4) {
			Content = "从下载WS无名单实名化人员信息文件的FTP信息 配置错误。检查数据库配置";
			return false;
		}
		String Ip = null;
		String AccName = null;
		String PassWord = null;
		String Port = null;
		String rootPath = getClass().getResource("/").getFile().toString();
		String BasePath = rootPath.replaceAll("WEB-INF/classes/", "");
		BasePath += "temp_lp/";
		
		for (int i = 1; i <= ssqlFTP.getMaxRow(); i++) {
			if ("IP".equals(ssqlFTP.GetText(i, 1))) {
				Ip = ssqlFTP.GetText(i, 2);
			}
			if ("AccName".equals(ssqlFTP.GetText(i, 1))) {
				AccName = ssqlFTP.GetText(i, 2);
			}
			if ("PassWord".equals(ssqlFTP.GetText(i, 1))) {
				PassWord = ssqlFTP.GetText(i, 2);
			}
			if ("Port".equals(ssqlFTP.GetText(i, 1))) {
				Port = ssqlFTP.GetText(i, 2);
			}
		}

		FTPTool tFTPTool = new FTPTool(Ip, AccName, PassWord, Integer.valueOf(Port));
		try {
			if (!tFTPTool.loginFTP()) {
				Content = "登陆FTP失败：" + tFTPTool.getErrContent(1);
				return false;
			} else {
				tFTPTool.downloadFile(FTPPath, BasePath, FileName);
			}
		} catch (Exception e) {
			Content = "FTP下载失败：";
			e.printStackTrace();
			return false;
		}
		// 读取xls 解析得到数据
		
		LPDiskImportSet mLPDiskImportSet = new LPDiskImportSet();
		try {
			FileInputStream pkg = new FileInputStream(BasePath + FileName);
			HSSFWorkbook book = new HSSFWorkbook(pkg);
			HSSFSheet sheet = book.getSheetAt(0);
			int firstRowNum = sheet.getFirstRowNum();// 最小行数
			int lastRowNum = sheet.getLastRowNum();// 最大行数
			short lastCellNum = sheet.getRow(firstRowNum).getLastCellNum();// 最大列数
			if (lastCellNum != (short) 7) {
				Content = "xls数据不正确。请检查列数";
				return false;
			}
			for (int i = firstRowNum + 1; i <= lastRowNum; i++) {
				DecimalFormat df = new DecimalFormat("0");
				String SerialNo=sheet.getRow(i).getCell((short) 0).getStringCellValue();
				if(!StringUtil.StringNull(SerialNo)){
					Content = "被保险人客户号不能为空:xls中第" + (i + 1) + "行 第1列";
					pkg.close();
					return false;
				}
				String InsuredName=sheet.getRow(i).getCell((short) 1).getStringCellValue();
				if(!StringUtil.StringNull(InsuredName)){
					Content = "被保险人姓名不能为空:xls中第" + (i + 1) + "行 第2列";
					pkg.close();
					return false;
				}
				double dSex = sheet.getRow(i).getCell((short) 3).getNumericCellValue();
				String Sex = df.format(dSex);
				if(StringUtil.StringNull(Sex)){
					Content = "被保人性别 不能为空:xls中第" + (i + 1) + "行 第3列";
					pkg.close();
					return false;
				}
				Date Birthday = sheet.getRow(i).getCell((short) 4).getDateCellValue();
				if (null == Birthday || "".equals(Birthday)) {
					Content = "被保人出生日期 不能为空:xls中第" + (i + 1) + "行 第4列";
					pkg.close();
					return false;
				}
				
				String IdType = sheet.getRow(i).getCell((short) 5).getStringCellValue();
				if (StringUtil.StringNull(IdType)) {
					Content = "被保人证件类型不能为空:xls中第" + (i + 1) + "行 第5列";
					pkg.close();
					return false;
				} 
				String IdNo = sheet.getRow(i).getCell((short) 6).getStringCellValue();
				if (StringUtil.StringNull(IdNo)) {
					Content = "被保人证件号号不能为空:xls中第" + (i + 1) + "行 第6列";
					pkg.close();
					return false;
				} 
				String BankCode = sheet.getRow(i).getCell((short) 7).getStringCellValue();
				if (StringUtil.StringNull(BankCode)) {
					Content = "被保人银行编码不能为空:xls中第" + (i + 1) + "行 第7列";
					pkg.close();
					return false;
				} 
				String AccName1 = sheet.getRow(i).getCell((short) 8).getStringCellValue();
				if (StringUtil.StringNull(AccName1)) {
					Content = "被保人账户名不能为空:xls中第" + (i + 1) + "行 第8列";
					pkg.close();
					return false;
				} 
				String BankAccNo = sheet.getRow(i).getCell((short) 9).getStringCellValue();
				if (StringUtil.StringNull(BankAccNo)) {
					Content = "被保人银行账号不能为空:xls中第" + (i + 1) + "行 第9列";
					pkg.close();
					return false;
				} 
				LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();
				mLPDiskImportSchema.setSerialNo(SerialNo);
				mLPDiskImportSchema.setInsuredName(InsuredName);
				mLPDiskImportSchema.setSex(Sex);
				mLPDiskImportSchema.setBirthday(Birthday);
				mLPDiskImportSchema.setIDType(IdType);
				mLPDiskImportSchema.setIDNo(IdNo);
				mLPDiskImportSchema.setBankCode(BankCode);
				mLPDiskImportSchema.setAccName(AccName1);
				mLPDiskImportSchema.setBankAccNo(BankAccNo);
				mLPDiskImportSchema.setEdorNo(EdorAcceptNo);
				mLPDiskImportSchema.setEdorType(dMessHead.getMsgType());
				mLPDiskImportSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
				mLPDiskImportSet.add(mLPDiskImportSchema);
			}
			tt.add(mLPDiskImportSet);
			map.put("mLPDiskImportSet", mLPDiskImportSet);
			map.put("VData", tt);
			pkg.close();
		} catch (Exception e) {
			Content = "解析xls错误";
			e.printStackTrace();
			return false;
		} finally {
			File file = new File(BasePath + FileName);
			file.delete();
		}
	return true;
	}
	/*
	public boolean diskImport(){
		
		String filepath=mFormData.getExcel();
		
		  LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
		  tLPDiskImportSchema.setEdorNo(EdorAcceptNo);
		  tLPDiskImportSchema.setEdorType("LP");
		  tLPDiskImportSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		  VData data = new VData();
		  data.add(GI);
		  data.add(tLPDiskImportSchema);
		  GEdorImportUI tGEdorImportUI = new GEdorImportUI(filepath, "BqImport.xml","LP");
		  if (!tGEdorImportUI.submitData(data)){
			  Content = "磁盘导入失败，原因是：" + tGEdorImportUI.getError() + "请检查导入模版的格式和数据！";
			  return  false;
		  }else{
			  return true;
		  }
		
	}*/
	
	
}
