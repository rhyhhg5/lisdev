package com.preservation.LP;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.BQBase.InterfaceBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;

public class InterfaceLP extends InterfaceBase{

	
	/**
	 * 逻辑处理
	 */
	public boolean dealc(){
		DealLP dealLP  = new DealLP();
		if (!dealLP.deal(data)) {
			errorValue = dealLP.getErrorValue();
			gongdanhao = dealLP.getEdorAcceptNo();
			return false;
		}
		gongdanhao = dealLP.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	
	

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement ){
		Element LP = rootElement.getChild("LP");
		// 客户号
		
		Element msgCustomer = LP.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = LP.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = LP.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		
	/*	tFormData.setGrpPolNo(xxFormData.getChildText("grpPolNo"));
		tFormData.setGroupMoney(xxFormData.getChildText("groupMoney"));*/
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		
		tFormData.setPlanCode(xxFormData.getChildText("BankCode"));//银行编码
		tFormData.setPlanCode(xxFormData.getChildText("AccName"));//账号名称
		tFormData.setPlanCode(xxFormData.getChildText("BankAccNo"));//银行账号
		tFormData.setPlanCode(xxFormData.getChildText("SerialNo"));//被保人序号
		tFormData.setPlanCode(xxFormData.getChildText("Excel"));//导入表格的url地址
		
		data.add(tFormData);
	}
	

	
	public static void main(String[] args) {
		InterfaceLP tInterfaceLP = new InterfaceLP();
		
		try {
			InputStream pIns = new FileInputStream("C:/Users/user/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceLP.deal(mInXmlStr);
			System.out.println(tInterfaceLP.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
}
