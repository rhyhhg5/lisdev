package com.preservation.ZB.obj;

import java.io.Serializable;
import java.util.List;



/**
 * ZB追加保费
 * 
 * @author LiHao
 *
 */
public class ZBLCInsuredInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Name;// 被保人姓名
	private String Sex;// 性别
	private String Birthday;// 生日
	private String IdType;// 证件类型
	private String IdNo;// 证件号码
	private String AddPrem;// 追加保费
	
	private  List<ZBLCInsuredInfo> ZBLCInsuredInfoSet;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getBirthday() {
		return Birthday;
	}
	public void setBirthday(String birthday) {
		Birthday = birthday;
	}
	public String getIdType() {
		return IdType;
	}
	public void setIdType(String idType) {
		IdType = idType;
	}
	public String getIdNo() {
		return IdNo;
	}
	public void setIdNo(String idNo) {
		IdNo = idNo;
	}
	public String getAddPrem() {
		return AddPrem;
	}
	public void setAddPrem(String addPrem) {
		AddPrem = addPrem;
	}
	public List<ZBLCInsuredInfo> getZBLCInsuredInfoSet() {
		return ZBLCInsuredInfoSet;
	}
	public void setZBLCInsuredInfoSet(List<ZBLCInsuredInfo> zBLCInsuredInfoSet) {
		ZBLCInsuredInfoSet = zBLCInsuredInfoSet;
	}

	
	
}
