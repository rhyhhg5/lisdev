package com.preservation.WS;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.WS.deal.WSForReturn;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * a无名单实名化
 * 
 * @author 李颢
 *
 */
public class WSInterface {
	private String errorValue;
	private String returnXML;
	private String GrpContNo;
	private String updata;
	private VData data = new VData();
	private TextTag tTextTag = new TextTag();
	private GlobalInput globalInput = new GlobalInput();
	private XmlExport dXmlExport = new XmlExport();

	public boolean deal(String xml) throws Exception {
		if (!gateAndCheckData(xml)) {
			returnXML = creatReturnXML();
			return false;
		}
		returnXML = creatReturnXML();
		// 启动线程，逻辑处理类，生成返回报文，调用东软接口
		if ("".equals(errorValue) || null == errorValue) {
			WSForReturn WSForReturn = new WSForReturn();
			WSForReturn.setData(data);
			WSForReturn.setXml(xml);
			Thread t = new Thread(WSForReturn);
			t.start();
		}
		return true;
	}

	private boolean gateAndCheckData(String xml) throws Exception {
		StringReader reader = new StringReader(xml);
		try {

			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = null;
			try {
				doc = tSAXBuilder.build(reader);
			} catch (JDOMException e1) {
				e1.printStackTrace();
			}
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");
			/*globalInput.ManageCom = BranchCode;
			globalInput.Operator = SendOperator;*/
			globalInput.ManageCom = "86";
			globalInput.Operator = "BAOQA";
			globalInput.ComCode = "86";
			MessHead head = new MessHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);
			data.add(globalInput);
			// 团单信息
			Element eBody = rootElement.getChild("MsgBody");
			Element eWS = eBody.getChild("WS");
			Element lCGrpContInfo = eWS.getChild("LCGrpContTable");
			Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
			String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
			String GrpName = eLCGrpContInfo.getChildText("GrpName");
			String ExcelURL = eLCGrpContInfo.getChildText("Excel");
			String FileName = ExcelURL.split("/")[ExcelURL.split("/").length - 1];
			String FTPPath = ExcelURL.substring(0, ExcelURL.length() - FileName.length());
			if (null == GrpContNo || "".equals(GrpContNo)) {
				errorValue = "申请报文中获取团单信息失败。";
				return false;
			}
			String str = "select 1 from LCCont  where appflag='1' and polType = '1' "
					+ " and conttype='2' and grpcontno='" + GrpContNo + "' ";
			String res = new ExeSQL().getOneValue(str);
			if (null == res || "".equals(str)) {
				errorValue = "非无名单保单不能申请无名单实名化";
				return false;
			}
			if (null == GrpName || "".equals(GrpName)) {
				errorValue = "团单组织名称不能为空";
				return false;
			}
			LCGrpContDB tLCGrpContDB = new LCGrpContDB();
			tLCGrpContDB.setGrpContNo(GrpContNo);
			if (!tLCGrpContDB.getInfo()) {
				errorValue = "传入的团单号错误，核心系统不存在团单“" + GrpContNo + "”";
				return false;
			}
			LCGrpContSchema mLCGrpContSchema = tLCGrpContDB.getSchema();
			data.add(mLCGrpContSchema);
			MMap tCekMap = null;
			tCekMap = lockLGWORK(mLCGrpContSchema);
			if (tCekMap == null) {
				errorValue = "团单" + mLCGrpContSchema.getGrpContNo() + "正在进行无名单实名化，请不要重复请求";
				return false;
			}
			if (!submit(tCekMap)) {
				errorValue = "团单" + mLCGrpContSchema.getGrpContNo() + "正在进行无名单实名化，请不要重复请求";
				return false;
			}

			LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
			iLCGrpContInfo.setGrpContNo(GrpContNo);
			iLCGrpContInfo.setGrpName(GrpName);
			data.add(iLCGrpContInfo);

			// 人员信息
			// FTP下载人员文件.xls
			String sqlFTP = "select DESCRIPTION,CODE from ldconfig where codetype = 'WSInterface'";
			SSRS ssqlFTP = new ExeSQL().execSQL(sqlFTP);
			if (ssqlFTP.getMaxRow() != 4) {
				errorValue = "从下载WS无名单实名化人员信息文件的FTP信息 配置错误。检查数据库配置";
				return false;
			}
			String Ip = null;
			String AccName = null;
			String PassWord = null;
			String Port = null;
			String rootPath = getClass().getResource("/").getFile().toString();
			String BasePath = rootPath.replaceAll("WEB-INF/classes/", "");
			BasePath += "temp_lp/";

			for (int i = 1; i <= ssqlFTP.getMaxRow(); i++) {
				if ("IP".equals(ssqlFTP.GetText(i, 1))) {
					Ip = ssqlFTP.GetText(i, 2);
				}
				if ("AccName".equals(ssqlFTP.GetText(i, 1))) {
					AccName = ssqlFTP.GetText(i, 2);
				}
				if ("PassWord".equals(ssqlFTP.GetText(i, 1))) {
					PassWord = ssqlFTP.GetText(i, 2);
				}
				if ("Port".equals(ssqlFTP.GetText(i, 1))) {
					Port = ssqlFTP.GetText(i, 2);
				}
			}

			FTPTool tFTPTool = new FTPTool(Ip, AccName, PassWord, Integer.valueOf(Port));
				if (!tFTPTool.loginFTP()) {
					errorValue = "登陆FTP失败：" + tFTPTool.getErrContent(1);
					return false;
				} else {
					if (!tFTPTool.downloadFile(FTPPath, BasePath, FileName)){
						errorValue = "登陆FTP失败：" + tFTPTool.getErrContent(1);
						return false;
					}
				}
			// 读取xls 解析得到数据
			LPDiskImportSet mLPDiskImportSet = new LPDiskImportSet();
			try {
				// OPCPackage pkg = OPCPackage.open(BasePath + FileName);
				try {
					FileInputStream in = new FileInputStream(BasePath + FileName);
					HSSFWorkbook book = new HSSFWorkbook(in);
					System.out.println("*************************HSSFWorkbook");
					HSSFSheet sheet = book.getSheetAt(0);
					int firstRowNum = sheet.getFirstRowNum();// 最小行数
					int lastRowNum = sheet.getLastRowNum();// 最大行数
					short lastCellNum = sheet.getRow(firstRowNum).getLastCellNum();// 最大列数
					if (lastCellNum != (short) 7) {
						//errorValue = "xls数据不正确。请检查列数";
						//return false;
						System.out.println("xls数据不正确。请检查列数");
					}
					for (int i = firstRowNum + 1; i <= lastRowNum; i++) {
						String Name = sheet.getRow(i).getCell((short) 0).getStringCellValue();
						System.out.println("***************:"+Name);
						if (null == Name || "".equals(Name)) {
						/*	errorValue = "被保人姓名 不能为空:xls中第" + (i + 1) + "行 第1列";
							 in.close();
							return false;*/	
							System.out.println("被保人姓名 不能为空:xls中第" + (i + 1) + "行 第1列");
							continue;
						}
						String Sex = sheet.getRow(i).getCell((short) 1).getStringCellValue();
						System.out.println("***************:"+Sex);
						if (null == Sex || "".equals(Sex)) {
							/*errorValue = "被保人性别 不能为空:xls中第" + (i + 1) + "行 第2列";
							in.close();
							return false;*/		
							System.out.println("被保人性别 不能为空:xls中第" + (i + 1) + "行 第2列");
							continue;
						}
						String Birthday = sheet.getRow(i).getCell((short) 2).getStringCellValue();
						System.out.println("***************:"+Birthday);
						if (null == Birthday || "".equals(Birthday)) {
							/*errorValue = "被保人出生日期 不能为空:xls中第" + (i + 1) + "行 第3列";
							in.close();
							return false;*/		
							System.out.println("被保人出生日期 不能为空:xls中第" + (i + 1) + "行 第3列");
							continue;
						}
						String IDType = sheet.getRow(i).getCell((short) 3).getStringCellValue();
						System.out.println("***************:"+IDType);
						if (null == IDType || "".equals(IDType)) {
							/*errorValue = "被保人证件类型 不能为空:xls中第" + (i + 1) + "行 第4列";
							in.close();
							return false;*/
							System.out.println("被保人证件类型 不能为空:xls中第" + (i + 1) + "行 第4列");
							 continue;
						}

						String IDNo = sheet.getRow(i).getCell((short) 4).getStringCellValue();
						System.out.println("***************:"+IDNo);
						if (null == IDNo || "".equals(IDNo)) {
							/*errorValue = "被保人证件号码 不能为空:xls中第" + (i + 1) + "行 第5列";
							in.close();
							return false;*/
							System.out.println("被保人证件号码 不能为空:xls中第" + (i + 1) + "行 第5列");
							 continue;
						}
						String OccupationType = sheet.getRow(i).getCell((short) 5).getStringCellValue();
						System.out.println("***************:"+OccupationType);
						if (null == OccupationType || "".equals(OccupationType)) {
							/*errorValue = "被保人职业类别 不能为空:xls中第" + (i + 1) + "行 第6列";
							in.close();
							return false;*/
							System.out.println("被保人职业类别 不能为空:xls中第" + (i + 1) + "行 第6列");
							 continue;
						}
						String ContPlanCode = sheet.getRow(i).getCell((short) 6).getStringCellValue();
						System.out.println("***************:"+ContPlanCode);
						if (null == ContPlanCode || "".equals(ContPlanCode)) {
							/*errorValue = "被保人保障计划 不能为空:xls中第" + (i + 1) + "行 第7列";
							in.close();
							return false;*/
							System.out.println("被保人保障计划 不能为空:xls中第" + (i + 1) + "行 第7列");
							 continue;
						}
						LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();
						mLPDiskImportSchema.setInsuredName(Name);
						mLPDiskImportSchema.setSex(Sex);
						mLPDiskImportSchema.setBirthday(Birthday);
						mLPDiskImportSchema.setIDType(IDType);
						mLPDiskImportSchema.setIDNo(IDNo);
						mLPDiskImportSchema.setContPlanCode(ContPlanCode);
						mLPDiskImportSchema.setOccupationType(OccupationType);
						mLPDiskImportSet.add(mLPDiskImportSchema);
						//tTextTag.add(updata, i);

						
					}
					data.add(mLPDiskImportSet);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					File file = new File(BasePath + FileName);
					file.delete();
				}
			} catch (Exception e) {
				e.printStackTrace();
				errorValue = "解析报文错误";
				return false;
			}
		} finally {
			reader.close();
		}
		return true;
	}

	/**
	 * 锁表
	 * 
	 * @param cLCGrpContSchema
	 * @return
	 */
	private MMap lockLGWORK(LCGrpContSchema cLCGrpContSchema) {
		MMap tMMap = null;
		String tLockNoType = "WS";
		String tAIS = "360";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", cLCGrpContSchema.getGrpContNo());
		tTransferData.setNameAndValue("LockNoType", tLockNoType);
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

		VData tVData = new VData();
		tVData.add(globalInput);
		tVData.add(tTransferData);

		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
		if (tMMap == null) {
			return null;
		}
		return tMMap;
	}

	private boolean submit(MMap map) {
		VData vdata = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(vdata, "")) {
			errorValue = "提交数据库发生错误" + tPubSubmit.mErrors;
			return false;
		}
		return true;
	}

	/**
	 * 生成第一次返回报文
	 */
	private String creatReturnXML() {
		String responseXml = null;
		try {
			MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
			Element root = new Element("DataSet");
			Document doc = new Document(root);
			Element eMsgHead = new Element("MsgHead");
			Element eHeadnode = new Element("Item");
			Element eHeadBatchNo = new Element("BatchNo");
			eHeadBatchNo.setText(vMessHead.getBatchNo());
			eHeadnode.addContent(eHeadBatchNo);
			Element eHeadSendDate = new Element("SendDate");
			eHeadSendDate.setText(vMessHead.getSendDate());
			eHeadnode.addContent(eHeadSendDate);
			Element eHeadSendTime = new Element("SendTime");
			eHeadSendTime.setText(vMessHead.getSendTime());
			eHeadnode.addContent(eHeadSendTime);
			Element eHeadBranchCode = new Element("BranchCode");
			eHeadBranchCode.setText(vMessHead.getBranchCode());
			eHeadnode.addContent(eHeadBranchCode);
			Element eHeadSendOperator = new Element("SendOperator");
			eHeadSendOperator.setText(vMessHead.getSendOperator());
			eHeadnode.addContent(eHeadSendOperator);
			Element eHeadMsgType = new Element("MsgType");
			eHeadMsgType.setText(vMessHead.getMsgType());
			eHeadnode.addContent(eHeadMsgType);
			if (!"".equals(errorValue) && null != errorValue) {
				Element eHeadState = new Element("State");
				eHeadState.setText("01");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("接收解析报文失败：" + errorValue);
				eHeadnode.addContent(eHeadErrInfo);
			} else {
				Element eHeadState = new Element("State");
				eHeadState.setText("00");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("接收并解析报文成功！");
				eHeadnode.addContent(eHeadErrInfo);
			}
			eMsgHead.addContent(eHeadnode);

			root.addContent(eMsgHead);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(doc);
		} catch (IOException e) {
			responseXml = "生成返回报文失败";
			e.printStackTrace();
		}
		return responseXml;
	}

	public String getReturnXML() {
		return returnXML;
	}

	public void setReturnXML(String returnXML) {
		this.returnXML = returnXML;
	}

	public VData getData() {
		return data;
	}

	public void setData(VData data) {

		this.data = data;
	}
	
	private boolean FTPSendFile() {
		/** 文件 */
		String fileName = this.GrpContNo;
		String filePath = "";

		try {
			String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";// 生成文件的存放路径
			// "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";
			filePath = new ExeSQL().getOneValue(sqlurl);// 生成文件的存放路径
			System.out.println("生成文件的存放路径   " + filePath);// 调试用－－－－－
			if (filePath == null || filePath.equals("")) {
				System.out.println("获取文件存放路径出错");
				return false;
			}
			String xmlDoc = this.dXmlExport.outputString();
			System.out.println("doc==" + xmlDoc);
			this.dXmlExport.outputDocumentToFile(filePath, fileName, "UTF-8");
			// filePath="E:\\";
			// 上传文件到FTP服务器
			if (!sendZip(filePath + fileName + ".xml")) {
				System.out.println("上传文件到FTP服务器失败！");
				return false;
			} else {
				// 上传成功后删除
				System.out.println("上传文件到FTP服务器成功！");
				File xmlFile = new File(filePath + fileName + ".txt");
				xmlFile.delete(); // 删除上传完的文件
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 *  * sendZip 上传文件至指定FTP（清单文件和单打的数据文件在同一个目录）
	 * 
	 * @param string String
	 * 
	 * @param flag String
	 * 
	 * @return boolean
	 */
	private boolean sendZip(String cFileUrlName) {
		// 登入前置机
		String ip = "";
		String user = "";
		String pwd = "";
		int port = 21;// 默认端口为21
		// String sql =
		// "select code,codename from LDCODE where codetype='ecprintserver'";
		String sql = "select DESCRIPTION,CODE from ldconfig where codetype = 'WSInterface'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS != null) {
			for (int m = 1; m <= tSSRS.getMaxRow(); m++) {
				if (tSSRS.GetText(m, 1).equals("IP")) {
					ip = tSSRS.GetText(m, 2);
				} else if (tSSRS.GetText(m, 1).equals("AccName")) {
					user = tSSRS.GetText(m, 2);
				} else if (tSSRS.GetText(m, 1).equals("PassWord")) {
					pwd = tSSRS.GetText(m, 2);
				} else if (tSSRS.GetText(m, 1).equals("Port")) {
					port = Integer.parseInt(tSSRS.GetText(m, 2));
				}
			}
		}

		FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
				return false;
			} else {
				if (!tFTPTool.upload(cFileUrlName)) {
					CError tError = new CError();
					tError.errorMessage = tFTPTool
							.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);

					System.out.println("上载文件失败!");
					return false;
				}

				tFTPTool.logoutFTP();
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
			CError tError = new CError();
			tError.errorMessage = tFTPTool
					.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);

			System.out.println("上载文件失败，可能是网络异常!");
			return false;
		} catch (IOException ex) {
			ex.printStackTrace();
			CError tError = new CError();
			tError.errorMessage = tFTPTool
					.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);

			System.out.println("上载文件失败，可能是无法写入文件");
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		WSInterface wdInterface = new WSInterface();
		
		try {
			InputStream pIns = new FileInputStream("D:\\Respmessage/BQWS.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			wdInterface.deal(mInXmlStr);
			System.out.println(wdInterface.creatReturnXML());
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
	
}
