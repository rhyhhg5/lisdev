package com.preservation.WJ;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.BQBase.InterfaceBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;

/**
 * 
 * WJ无名单减少被保人 接口 得到数据 调用处理类DealWJ.java
 * 
 * @author fq
 *
 */
public class InterfaceWJ extends InterfaceBase{

	
	
	/**
	 * 逻辑处理
	 */
	public boolean dealc(){
		DealWJ dealWJ = new DealWJ();
		if (!dealWJ.deal(data)) {
			errorValue = dealWJ.getErrorValue();
			gongdanhao = dealWJ.getEdorAcceptNo();
			return false;
		}
		gongdanhao = dealWJ.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement ){
		Element WJ = rootElement.getChild("WJ");
		// 客户号
		Element msgCustomer = WJ.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = WJ.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = WJ.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorType("WJ");
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		if(!"".equals(xxFormData.getChildText("PremWZ"))){
			tFormData.setPremWZ(Double.parseDouble(xxFormData.getChildText("PremWZ")));
		}
		if(!"".equals(xxFormData.getChildText("Peoples"))){
			tFormData.setPeoples(Integer.parseInt(xxFormData.getChildText("Peoples")));
		}
		if(!"".equals(xxFormData.getChildText("SumPrem"))){
			tFormData.setSumPrem(Double.parseDouble(xxFormData.getChildText("SumPrem")));
		}
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
		
		data.add(tFormData);
	}
	
	
	
	

	public static void main(String[] args) {
		InterfaceWJ tInterfaceWJ = new InterfaceWJ();
		
		try {
			InputStream pIns = new FileInputStream("C:/Users/Administrator/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceWJ.deal(mInXmlStr);
			System.out.println(tInterfaceWJ.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
}
