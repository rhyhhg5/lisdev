package com.preservation.WJ;

import com.preservation.BQBase.DealBase;
import com.sinosoft.lis.bq.GEdorWZDetailUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * @author fq
 *
 */
public class DealWJ extends DealBase{
	
	/**
	 * 保全项目明细
	 * 保存按钮 
	 * 前提校验
	 * @return
	 */
	private boolean checksave(){
		ExeSQL tExeSQL = new ExeSQL();

		Double premWJ = mFormData.getPremWZ();  //人均期交保费
		int peoples2WJ = mFormData.getPeoples();  //总人数
		Double sumPremWJ = mFormData.getSumPrem();  //总保费
		  if("".equals(mFormData.getPlanCode()))
		  {
		    Content = "请选择保障。";
		    System.out.println(Content);
		    return false;
		  }
		  String sql = "SELECT DISTINCT a.contPlanCode, a.contPlanName, a.peoples2, " +
			" ( SELECT SUM(prem)  FROM LCPol WHERE grpContNo = a.grpcontNo AND contPlanCode = a.contPlanCode AND polTypeFlag = '1' ),c.contno " +
			" FROM  LCContPlan a , LCInsured b, LCCont c " +
			" WHERE a.grpContNo = b.grpContNo " +
			" AND a.contPlanCode = b.contPlanCode " +
			" AND b.contNo = c.contNo " +
			" AND c.polType = '1' " +
			" AND a.grpContNo = '"+mLCGrpContInfo.getGrpContNo()+"' " +
			" AND a.contPlanCode != '11' and a.contPlanCode = '"+mFormData.getPlanCode()+"'";

		  SSRS result = tExeSQL.execSQL(sql);
		  String contPrem = result.GetText(1,4);  //保障计划总保费
		  String contPeoples2 = result.GetText(1,3);  //保障计划总人数
		  
		  if(premWJ < 0 || sumPremWJ < 0)
		  {
		    Content = "人均期交保费、减少被保险人数、本次应收保费均应为非负数";
		    System.out.println(Content);
		    return false;
		  }
		  if(Double.parseDouble(contPrem) < sumPremWJ)
		  {
			  Content = "录入总保费不能大于保障计划保费。";  
		    System.out.println(Content);
		    return false;
		  }
		  if(peoples2WJ < 0)
		  {
			  Content = "减少被保险人数应为正整数";
			  System.out.println(Content);
		    return false;
		  }
		  if(Integer.parseInt(contPeoples2) < peoples2WJ)
		  {
			  Content = "录入人数不能大于保障计划人数。";  
			  System.out.println(Content);
		    return false;
		  }

		  String edorValiDate = mFormData.getEdorValiDate();
//		  if(!isDate(edorValiDate))
//		  {
//		    alert("生效日期必须为日期格式'yyyy-mm-dd'");
//		    fm.EdorValiDate.value = currentDate;
//		    return false;
//		  }
		  String contno = result.GetText(1,5);
		  String sql2 = "select distinct lastPayToDate, curPayToDate, payCount from LJAPayPerson where contNo = '"+contno+"' order by curpaytodate";
		  result = tExeSQL.execSQL(sql2);
		  sql = "  select 1 "
		            + "from dual "
		            + "where days('" + result.GetText(1,1) + "') > days('" + edorValiDate + "') "
		            + "   or days('" + result.GetText(1,2) + "') < days('" + edorValiDate + "') ";
		  result = tExeSQL.execSQL(sql);

		  if(result.MaxRow>0)
		  {
		    Content = "本次减人生效日期必须在本交费期的应交日期和交至日之间。";
		    System.out.println(Content);
		    return false;
		  }

		  String planPeoples2 = contPeoples2;  
		  sql = "select count(distinct a.ContNo) from LCPol a ,LCCont b "
		          + "    where a.GrpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' "
		          + "        and exists (select 1 from LCPol c " 
		          + "                    where c.ContNo = b.ContNo  and c.PolNo = a.MasterPolNo "
		          + "                    and a.stateflag = '1') "
		          + "        and b.conttype ='2' and b.PolType = '1' and a.PolTypeFlag <> '1' "
		          + "        and a.ContPlanCode = '" + mFormData.getPlanCode() + "' with ur ";
		  String ContPeoples = "0";   
		  result = tExeSQL.execSQL(sql);
		  if(result.MaxRow>0)
		  {
		    ContPeoples = result.GetText(1,1);
		  }
		  if(Integer.parseInt(planPeoples2)-peoples2WJ < Integer.parseInt(ContPeoples))
		  {
		    Content = "减人后保障计划" + mFormData.getPlanCode() + "人数不能少于已经被实名化的人数,\n"
		                  + "保障计划" + mFormData.getPlanCode() + "已经实名化" + ContPeoples + "人!";
		    System.out.println(Content);
		    return false;
		  }
		//后台数据提交
		  String edorNo = EdorAcceptNo;
		  GlobalInput gi = GI;
		  
		  //集体批改信息
		  LPGrpEdorItemSchema tLPGrpEdorItemSchema  = new LPGrpEdorItemSchema();
		  tLPGrpEdorItemSchema.setEdorNo(edorNo);
		  tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		  tLPGrpEdorItemSchema.setEdorType(dMessHead.getMsgType());
		  tLPGrpEdorItemSchema.setEdorValiDate(mFormData.getEdorValiDate());
		  
		  TransferData td = new TransferData();
		  td.setNameAndValue("prem", "-" + String.valueOf(sumPremWJ));
		  td.setNameAndValue("peoples2", "-" + String.valueOf(peoples2WJ));
		  td.setNameAndValue("contPlanCode", mFormData.getPlanCode());
		  td.setNameAndValue("contNo", contno);
		  
		  VData data = new VData();
		  data.add(gi);
		  data.add(td);
		  data.add(tLPGrpEdorItemSchema);
		  
		  GEdorWZDetailUI ui = new GEdorWZDetailUI();
		  if(!ui.submitData(data, ""))
		  {
//		    flag = "Fail";
		    Content = ui.mErrors.getErrContent();
		    System.out.println(Content);
		    return false;
		  }
		  else
		  {
//		    flag = "Succ";
			  Content = "保存成功";
			  System.out.println(Content);
		  }
		  
		  Content = PubFun.changForHTML(Content);
		  System.out.println(Content);
		return true;
	}
}
