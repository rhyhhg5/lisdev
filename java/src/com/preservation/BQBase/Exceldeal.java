package com.preservation.BQBase;

import java.util.HashMap;
import java.util.Map;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.utility.VData;

public class Exceldeal implements Runnable{

	private Map<String,Object> matData;	
	
	private Map mMap=new HashMap();
	
	public Map<String, Object> getMatData() {
		return matData;
	}

	public void setMatData(Map<String, Object> matData) {
		this.matData = matData;
	}

	public void run() {
		if(matData!=null){
			deal(matData);
		}
		
	}
	
	public void deal(Map<String,Object> map){
		LPDiskImportSet  mLPDiskImportSet=(LPDiskImportSet) map.get("mLPDiskImportSet");
		mMap.put(mLPDiskImportSet, "INSERT");
		submit();
	}
	
	 private boolean submit()
	    {
	        VData data = new VData();
	        data.add(mMap);
	        PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(data, ""))
	        {
	           // mErrors.copyAllErrors(tPubSubmit.mErrors);
	            return false;
	        }
	        return true;
	    }
}
