package com.preservation.BQBase;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.preservation.edorCancel.GEdorCancel;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqConfirmUI;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.FeeNoticeGrpVtsUI;
import com.sinosoft.lis.bq.GEdorZFDetailUI;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.PEdorAppAccConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpContSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LPGrpContSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author dongjiali
 *
 *	中止缴费
 */
public class DealBase {
	public Logger log=Logger.getLogger(DealBase.class);
	// 错误信息
	public String Content;
	// 得到data封装的数据
	public MessHead dMessHead;
	public CustomerInfo mCustomerInfo;
	public FormData mFormData;
	public LCGrpContInfo mLCGrpContInfo;
	public GlobalInput GI = new GlobalInput();
	public String EdorAcceptNo = "";
	public VData tt = new VData();
	public String getEdorAcceptNo() {
		return EdorAcceptNo;
	}
	public Map<String, Object> map=new HashMap<String, Object>();
	String  ExcelURL="";
	
	public boolean deal(VData data) {
		
		dMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		mCustomerInfo = (CustomerInfo) data.getObjectByObjectName("CustomerInfo", 0);
		mFormData = (FormData) data.getObjectByObjectName("FormData", 0);
		mLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		ExcelURL=mFormData.getExcel();
	
	
		map.put("dMessHead", dMessHead);
		map.put("Content", Content);
		map.put("mCustomerInfo", mCustomerInfo);
		map.put("mFormData", mFormData);
		map.put("mLCGrpContInfo", mLCGrpContInfo);
		map.put("GI", GI);
		map.put("EdorAcceptNo", EdorAcceptNo);
		map.put("ExcelURL", ExcelURL);
		
		GEdorCancel tGEdorCancel = new GEdorCancel();
		
		VData tt = new VData();
		
		VData vdata = new VData();
		MMap mmap = new MMap();
		String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,betweenness ,transactionType ,transactionCode ,transactionBatch ,  transactionDescription,   transactionAddress, grpcontno, rgtno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"+GI.Operator+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','中间状态','"+dMessHead.getMsgType()+"','"+dMessHead.getBranchCode()+"','"+dMessHead.getBatchNo()+"','保全团单无名单新增被保险人','交易地址','"+mLCGrpContInfo.getGrpContNo()+"','')";
		mmap.put(insertClaimInsertLogSql, "INSERT");
		vdata.add(mmap);
		PubSubmit pub = new PubSubmit();
		pub.submitData(vdata, "INSERT");
		
		// 经办
		if (!doBusiness()) {
			System.out.println("=======经办失败");
			return false;
		}
		//添加保全项 前台
		if(!addRecord()){
			System.out.println("=======添加保全项失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		//添加保全项 后台
		if(!grpEdorItemSave()){
			System.out.println("=======添加保全项失败222222");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
	
		
		if(!domake()){
			return false;
		}
		//保全项目明细
		if(!checkSave()){
			System.out.println("=======保全项目明细失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		//保全理算
		if(!edorAppConfirm()){
			System.out.println("=======保全理算失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		//保全确认 前台
		if(!edorConfirm()){
			System.out.println("=======保全确认前台失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		//保全确认 后台
		if(!GEdorConfirmSubmit()){
			System.out.println("=======保全确认后台失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		
		/*if(StringUtil.StringNull(mFormData.getPayMode())){
			String sqledorse="select * from LJAGetEndorse where otherno='"+EdorAcceptNo+"'";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS ssrs=tExeSQL.execSQL(sqledorse);
			String actugetno = "";
			if (ssrs.getMaxRow() > 0) {
				actugetno = ssrs.GetText(1, 1);
			}
			vdata.clear();
			MMap amap = new MMap();
			String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"+EdorAcceptNo+"' WHERE transactionCode = '"+dMessHead.getBranchCode()+"' and transactionBatch='"+dMessHead.getBatchNo()+"' ";
			String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
//			String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', "+actugetno+", "+mFormData.getPayMode()+", otherno, '10','"+mFormData.getBankAccNo()+"','0', '1', '成功', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', '', '', '', '' FROM ljaget where otherno ='"+EdorAcceptNo+"' ";
			String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', "+actugetno+", "+mFormData.getPayMode()+", otherno, '10','"+mFormData.getBankAccNo()+"','0', '1', '成功', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', '', '', '', '' FROM ljspay where otherno ='"+EdorAcceptNo+"' ";
			amap.put(updateClaimInsertLogSql, "UPDATE");
			amap.put(insertReconcileInfoSql, "INSERT");
			vdata.add(amap);
			PubSubmit puba = new PubSubmit();
			puba.submitData(vdata, "UPDATE");
		}*/
		
		//无名单增人（收费）
		if("WZ".equals(mFormData.getEdorType())){	
				vdata.clear();
				MMap amap = new MMap();
				String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"+EdorAcceptNo+"' WHERE transactionCode = '"+dMessHead.getBranchCode()+"' and transactionBatch='"+dMessHead.getBatchNo()+"' ";
				String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
				String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', "+EdorAcceptNo+", "+mFormData.getPayMode()+", otherno, '10','"+mFormData.getBankAccNo()+"','0', '', '', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', 'WZ', 'pay', '', '' FROM ljspay where otherno ='"+EdorAcceptNo+"' ";
				amap.put(updateClaimInsertLogSql, "UPDATE");
				amap.put(insertReconcileInfoSql, "INSERT");
				vdata.add(amap);
				PubSubmit puba = new PubSubmit();
				puba.submitData(vdata, "UPDATE");
		}
		
		//保费回补（收费）
		if("BB".equals(mFormData.getEdorType())){
			vdata.clear();
			MMap amap = new MMap();
			String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"+EdorAcceptNo+"' WHERE transactionCode = '"+dMessHead.getBranchCode()+"' and transactionBatch='"+dMessHead.getBatchNo()+"' ";
			String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
			String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', "+EdorAcceptNo+", "+mFormData.getPayMode()+", otherno, '10','"+mFormData.getBankAccNo()+"','0', '', '', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', 'BB', 'pay', '', '' FROM ljspay where otherno ='"+EdorAcceptNo+"' ";
			amap.put(updateClaimInsertLogSql, "UPDATE");
			amap.put(insertReconcileInfoSql, "INSERT");
			vdata.add(amap);
			PubSubmit puba = new PubSubmit();
			puba.submitData(vdata, "UPDATE");
		}
		
		//结余返还（付费）
		if("BJ".equals(mFormData.getEdorType())){
			vdata.clear();
			MMap amap = new MMap();
			String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"+EdorAcceptNo+"' WHERE transactionCode = '"+dMessHead.getBranchCode()+"' and transactionBatch='"+dMessHead.getBatchNo()+"' ";
			String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
			String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', "+EdorAcceptNo+", "+mFormData.getPayMode()+", otherno, '10','"+mFormData.getBankAccNo()+"','0', '', '', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', 'BJ', 'get', '', '' FROM ljaget where otherno ='"+EdorAcceptNo+"' ";
			amap.put(updateClaimInsertLogSql, "UPDATE");
			amap.put(insertReconcileInfoSql, "INSERT");
			vdata.add(amap);
			PubSubmit puba = new PubSubmit();
			puba.submitData(vdata, "UPDATE");
		}
		
		
		//无名单减人（付费）
		if("WJ".equals(mFormData.getEdorType())){
			vdata.clear();
			MMap amap = new MMap();
			String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"+EdorAcceptNo+"' WHERE transactionCode = '"+dMessHead.getBranchCode()+"' and transactionBatch='"+dMessHead.getBatchNo()+"' ";
			String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
			String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', "+EdorAcceptNo+", "+mFormData.getPayMode()+", otherno, '10','"+mFormData.getBankAccNo()+"','0', '', '', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', 'WJ', 'get', '', '' FROM ljsget where otherno ='"+EdorAcceptNo+"' ";
			amap.put(updateClaimInsertLogSql, "UPDATE");
			amap.put(insertReconcileInfoSql, "INSERT");
			vdata.add(amap);
			PubSubmit puba = new PubSubmit();
			puba.submitData(vdata, "UPDATE");
		}
		
		//保费追加（收费）
		if("ZB".equals(mFormData.getEdorType())){
			vdata.clear();
			MMap amap = new MMap();
			String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"+EdorAcceptNo+"' WHERE transactionCode = '"+dMessHead.getBranchCode()+"' and transactionBatch='"+dMessHead.getBatchNo()+"' ";
			String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
			String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', "+EdorAcceptNo+", "+mFormData.getPayMode()+", otherno, '10','"+mFormData.getBankAccNo()+"','0', '', '', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', 'ZB', 'pay', '', '' FROM ljspay where otherno ='"+EdorAcceptNo+"' ";
			amap.put(updateClaimInsertLogSql, "UPDATE");
			amap.put(insertReconcileInfoSql, "INSERT");
			vdata.add(amap);
			PubSubmit puba = new PubSubmit();
			puba.submitData(vdata, "UPDATE");
		}
		return true;
	}


	/**
	 * 
	 * 经办
	 * 
	 * @return
	 */
	public boolean doBusiness() {
		//前台校验
//		dMessHead.getBatchNo();
//		String grpcontno = mLCGrpContInfo.getGrpContNo();
		ExeSQL tExeSQL = new ExeSQL();
		/*if(grpcontno!=null&&!"".equals(grpcontno))
		{
			String sql = "select StateType,State from LCGrpContState where GrpContNo='"
					+ grpcontno + "' and GrpPolNo='000000'";
			SSRS result = tExeSQL.execSQL(sql);
			if (result.MaxRow > 0) {
				// alert("result[0][0]===="+result[0][0]);
				if ("Pause".equals(result.GetText(1, 1))
						&& "2".equals(result.GetText(1, 2))) {
					Content = "保单" + grpcontno + "处于暂停状态!";
					System.out.println(Content);
					return false;
				}
				if ("Terminate".equals(result.GetText(1, 1))
						&& "1".equals(result.GetText(1, 2))) {
					Content = "保单" + grpcontno + "处于失满期终止状态!";
					System.out.println(Content);
					return false;
				}
			}
		}*/
		
		//后台save提交 mOption = "DOBUSINESS";
		String FlagStr;
		String tWorkNo = "";
//		String tAccpetNo = "";
		String tDetailWorkNo = "";
		
		//得到业务类型
		String tTypeNo = "03";//01-保全
		
		String statusNo = "2";
//		String temp = request.getParameter("statusNo");
//		if(temp != null && temp.equals("3"))
//		{
//			statusNo = "3";
//		}

		
		//输入参数
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(mCustomerInfo.getCustomerNo());
		tLGWorkSchema.setCustomerCardNo("");//团体客户号，没有IDNO
		tLGWorkSchema.setStatusNo(statusNo);
		tLGWorkSchema.setPriorityNo(mFormData.getPriorityNo());
		tLGWorkSchema.setTypeNo(tTypeNo);
		String DateLimit = "";
		if(!"".equals(mFormData.getWorkTypeNo())){
			String sql = "select DateLimit from LGWorkType where WorkTypeNo = '03'";
			SSRS ss = tExeSQL.execSQL(sql);
			DateLimit = ss.GetText(1, 1);
		}
		tLGWorkSchema.setDateLimit(DateLimit);
		tLGWorkSchema.setApplyTypeNo(mFormData.getApplyTypeNo());
		tLGWorkSchema.setApplyName(mFormData.getApplyName());//申请人姓名
		tLGWorkSchema.setAcceptWayNo("03");
		tLGWorkSchema.setAcceptDate("");//受理日期
		tLGWorkSchema.setRemark(mFormData.getRemark());
		//代理人标记
		String agentFlag=mFormData.getAgentFlag();
		if(null!=agentFlag && !"".equals(agentFlag)){
			tLGWorkSchema.setAgentFlag(agentFlag);
			if(agentFlag.equals("0")){
				tLGWorkSchema.setAgentName(mFormData.getProxyName());
				tLGWorkSchema.setAgentIDType(mFormData.getProxyIDType());
				tLGWorkSchema.setAgentIDNo(mFormData.getProxyIDNo());
				tLGWorkSchema.setAgentIDStartDate(mFormData.getProxyIDStartDate());
				tLGWorkSchema.setAgentIDEndDate(mFormData.getProxyIDEndDate());
				tLGWorkSchema.setAgentPhone(mFormData.getProxyPhone());
			}
		}
		System.out.println(agentFlag+"************代理标记");
		VData tVData = new VData();
		tVData.add(tLGWorkSchema);
		tVData.add(GI);
		
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (tTaskInputBL.submitData(tVData, "") == false)
		{
			FlagStr = "Fail";
			Content = "数据保存失败！";
			System.out.println(Content);
			return false;
		}
		else
		{
			
			
			//设置显示信息
			VData tRet = tTaskInputBL.getResult();
			LGWorkSchema mLGWorkSchema = new LGWorkSchema();
			mLGWorkSchema.setSchema((LGWorkSchema) tRet.getObjectByObjectName("LGWorkSchema", 0));
			
			tWorkNo = mLGWorkSchema.getWorkNo();
			EdorAcceptNo = mLGWorkSchema.getAcceptNo();
			tDetailWorkNo = mLGWorkSchema.getDetailWorkNo();
			
			
//			SimpleDateFormat sdf1 =   new SimpleDateFormat("yyyy-MM-dd");
//			SimpleDateFormat sdf2 =   new SimpleDateFormat("HH:mm:ss");
//			Date   curDate   =   new   Date(System.currentTimeMillis());//获取当前时间  
		
//			String LPEdorAppsql="insert into LPEdorApp ( EdorAcceptNo,OtherNo,OtherNoType,EdorAppName,ManageCom,ChgPrem,ChgAmnt,ChgGetAmnt,GetMoney,GetInterest,EdorAppDate,EdorState,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime ) values" +
//					" ( '"+EdorAcceptNo+"','"+mCustomerInfo.getCustomerNo()+"','2','"+mFormData.getApplyName()+"','"+mFormData.getManageCom()+"',0.0,0.0,0.0,0.0,0.0,'"+sdf1.format(curDate)+"','1','"+dMessHead.getSendOperator()+"','"+sdf1.format(curDate)+"','"+sdf2.format(curDate)+"','"+sdf1.format(curDate)+"','"+sdf2.format(curDate)+"' )";
//			boolean bl= tExeSQL.execUpdateSQL(LPEdorAppsql);
//			System.out.println(bl);
			
			//把WorkNo保存到session中
//			session.setAttribute("WORKNOINPUT", tWorkNo);
			
			FlagStr = "Succ";
			Content = "数据保存成功，录入工单的受理号为：" + EdorAcceptNo;
			System.out.println(Content);
		}
		return true;
	}
	
	/**
	 * 添加保全项目按钮
	 * 前台校验
	 * @return
	 */
	/*private boolean addRecord(){
		ExeSQL tExeSQL = new ExeSQL();
		String grpcontno = mLCGrpContInfo.getGrpContNo();
		//判断保单是否有万能险种
		String sql = "select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"+grpcontno+"' and RiskType4='4' with ur ";
		SSRS result = tExeSQL.execSQL(sql);
		if(result.MaxRow>0){
			 //解约保全生效日期必须大于或者等于保单最后一次月结日期
			String checkHaveEdor = "select edoracceptno from lpgrpedoritem where grpcontno='" + grpcontno + "' and exists ( " +
				" select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0' )  ";		   
			result = tExeSQL.execSQL(checkHaveEdor);
			if(result.MaxRow>0)
			{
				Content = "该团险万能的保单下存在其他未结案的保全项目，不能继续操作！";
				System.out.println(Content);
				return false;
			}
		}
		   
		if(mFormData.getEdorValiDate() == null || "".equals(mFormData.getEdorValiDate()))
		{
			Content = "保全生效日不能为空!";
			System.out.println(Content);
			return false ;
		}
			
	    if (dMessHead.getMsgType() == null || "".equals(dMessHead.getMsgType()))
	    {
	        Content = "请选择需要添加的保全项目";
	        System.out.println(Content);
	        return false;    
	    }
	    
	    String checkSQL = "select riskcode from lcgrppol where grpcontno='"+grpcontno+"' and " +
		" exists( select 1 from lmriskapp where riskcode=lcgrppol.riskcode and startdate>='2013-11-06' ) and " +
		" not exists (select 1 from lmriskedoritem where riskcode=lcgrppol.riskcode and edorcode='"+dMessHead.getMsgType()+"') ";
		
		SSRS riskCode = tExeSQL.execSQL(checkSQL);
		if(riskCode.MaxRow>0 && !"".equals(riskCode.GetText(1,1))){
			SSRS edorName = tExeSQL.execSQL("select edorname from lmedoritem where edorcode='"+dMessHead.getMsgType()+"' fetch first 1 rows only");
			Content = "您的保单投保有险种：'"+riskCode.GetText(1,1)+"'，该险种的["+edorName.GetText(1,1)+"]保全功能暂未上线，无法添加保全项目！";
			System.out.println(Content);
			return false;
		}
	    

	    sql = "select state from lcgrpbalplan where grpcontno = '"+grpcontno+"'";
	    SSRS arrResult = tExeSQL.execSQL(sql);    
	    if (arrResult.MaxRow > 0 && !"0".equals(arrResult.GetText(1,1))) 
	    {
	    	Content = "该集体合同目前正在进行定期结算!不能进行保全操作!";
	    	System.out.println(Content);
	        return false;  
	    }
	    
	    sql = "select edorno from lpgrpedoritem a where grpcontno = '"+grpcontno+"' and edortype='TZ' and " +
	    		" exists ( select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0' )";
	    arrResult = tExeSQL.execSQL(sql);    
	    if (arrResult.MaxRow > 0) 
	    {
	    	Content = "团体万能增人不能和其他保全项目一起操作!";
	    	System.out.println(Content);
	        return false;  
	    }
	    
	    String IsRSsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and ((enddate is null) or (startdate<=current date  and enddate>current date )) and grpcontno = '"+grpcontno+"'";
	    SSRS IsRSarrResult = tExeSQL.execSQL(IsRSsql);    
	    if (IsRSarrResult.MaxRow>0) 
	    {
	    	Content = "该集体合同目前正处于保单暂停状态!不能添加保单恢复外的任何保全操作!";
	    	System.out.println(Content);
	        return false;  
	    }
       
//		校验保全生效日是否在暂停期
	    String IsRSRRsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and enddate is not null and startdate<='"+mFormData.getEdorValiDate()+"'  and enddate>'"+mFormData.getEdorValiDate()+"'  and grpcontno = '"+grpcontno+"'";
	    SSRS IsRSRRResult = tExeSQL.execSQL(IsRSRRsql);    
	    if (IsRSarrResult.MaxRow>0) 
	    {
	    	Content = "生效日期不可选在该集体合同暂停区间内!"; 	  
	    	System.out.println(Content);
	        return false;  
	    }
	          

	    

	    sql = "select 1 from lpgrpedoritem a where grpcontno='"+grpcontno+"' and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and edortype='TY' "; 
	    arrResult = tExeSQL.execSQL(sql);
	    if (arrResult.MaxRow > 0)
	    {
	    	Content = "该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!";
	    	System.out.println(Content);
	        return false;
	    } 
	    
//	    fm.all('fmAction').value="INSERT||GRPEDORITEM";
//	    fm.action = "../bq/GrpEdorItemSave.jsp"
		return true;
	}*/

	// 判断团单是否有万能险种
	public boolean hasULIRisk(String grpContNo)
	{
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and RiskType4='4' with ur ";
		SSRS sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0)
		{
			return true;
		}
		return false;
	}
	public boolean checkGrpEdorType()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String checkSQL = "select riskcode from lcgrppol where grpcontno='"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and "
				+ " exists( select 1 from lmriskapp where riskcode=lcgrppol.riskcode and startdate>='2013-11-06' ) and "
				+ " not exists (select 1 from lmriskedoritem where riskcode=lcgrppol.riskcode and edorcode='"
				+ mFormData.getEdorType() + "') ";
		
		SSRS checkSQLSSRS = tExeSQL.execSQL(checkSQL);
		/*.SetText(checkSQLSSRS);*/
		if (checkSQLSSRS.getMaxRow() > 0)
		{
			System.out.println("jhkjdofj"+mFormData.getEdorType());
			SSRS edorName = tExeSQL
					.execSQL("select edorname from lmedoritem where edorcode='"
							+ mFormData.getEdorType()
							+ "' fetch first 1 rows only");
		
			System.out.println("您的保单投保有险种：'" + checkSQLSSRS.GetText(1, 1)
					+ "'，该险种的[" + edorName.GetText(1, 1)
					+ "]保全功能暂未上线，无法添加保全项目！");
			Content = "您的保单投保有险种：'" + checkSQLSSRS.GetText(1, 1) + "'，该险种的["
					+ edorName.GetText(1, 1) + "]保全功能暂未上线，无法添加保全项目！";
			return false;
		}
		return true;
	}
	/**
	 * 
	 * 添加保全项目
	 * 
	 */
	public boolean addRecord()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "";
		SSRS sqlSSRS = null;
		// 判断保单是否有万能险种
		if (hasULIRisk(mLCGrpContInfo.getGrpContNo()))
		{
			// 解约保全生效日期必须大于或者等于保单最后一次月结日期
			sql = "select edoracceptno from lpgrpedoritem where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and exists ( "
					+ " select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0' )  ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该团险万能的保单下存在其他未结案的保全项目，不能继续操作！");
				Content = "该团险万能的保单下存在其他未结案的保全项目，不能继续操作！";
				return false;
			}
		}
		if (null == mFormData.getEdorValiDate()
				|| "".equals(mFormData.getEdorValiDate()))
		{
			System.out.println("保全生效日不能为空!");
			Content = "保全生效日不能为空!";
			return false;
		}
		/*if (mFormData.getEdorType() == null|| "".equals(mFormData.getEdorType()))
		{
			System.out.println("保全项目类型不能为空！");
			Content = "保全项目类型不能为空！";
			return false;
		}*/
		if (!checkGrpEdorType())
		{
			return false;
		}
		// 做过无名单增减人的保单不能犹豫期退保
		if (mFormData.getEdorType() == "WT")
		{
			sql = "select 1 from lpgrpedoritem where grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo() + "' and edortype='WJ'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该集体合同做过无名单减少被保人，不能进行犹豫期退保操作!");
				Content = "该集体合同做过无名单减少被保人，不能进行犹豫期退保操作!";
				return false;
			}
			sql = "select 1 from lmriskapp where riskcode in (select riskcode from lcgrppol where grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo() + "') and risktype4='4'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				sql = "select 1 from ljagetendorse where  grpcontno = '"
						+ mLCGrpContInfo.getGrpContNo()
						+ "' and getmoney<>0 fetch first 1 rows only with ur";
				sqlSSRS = tExeSQL.execSQL(sql);
				if (sqlSSRS.getMaxRow() > 0)
				{
					System.out.println("该团体万能的集体合同做过补退费的保全项目，不能进行犹豫期退保!");
					Content = "该团体万能的集体合同做过补退费的保全项目，不能进行犹豫期退保!";
					return false;
				}
				String sqlLP = "select 1 from llclaimdetail where grpcontno ='"
						+ mLCGrpContInfo.getGrpContNo()
						+ "' fetch first 1 rows only  with ur  ";
				SSRS sqlLPSSRS = tExeSQL.execSQL(sqlLP);
				if (sqlLPSSRS.getMaxRow() > 0)
				{
					System.out.println("团险万能满期保单有过理赔操作,不能做犹豫期退保!");
					Content = "团险万能满期保单有过理赔操作,不能做犹豫期退保!";
					return false;

				}
			}
		}
		// 最后一次续期交费后，做过无名单增减人的保单不能退保
		if (mFormData.getEdorType() == "CT")
		{
			// 判断保单是否有万能险种
			if (hasULIRisk(mLCGrpContInfo.getGrpContNo()))
			{
				// 解约保全生效日期必须大于或者等于保单最后一次月结日期
				String checkDateSQL = " select 1 from lcinsureaccclass where "
						+ " grpcontno='"
						+ mLCGrpContInfo.getGrpContNo()
						+ "' "
						+ " and polno=(select polno from lcpol where grpcontno=lcinsureaccclass.grpcontno and poltypeflag='2') "
						+ " and baladate<='" + mFormData.getEdorValiDate()
						+ "' with ur ";
				SSRS checkDateSQLSSRS = tExeSQL.execSQL(checkDateSQL);
				if (checkDateSQLSSRS.getMaxRow() < 0)
				{
					System.out.println("团险万能解约项目的保全生效日期必须大于或者等于保单最后一次月结日期！");
					Content = "团险万能解约项目的保全生效日期必须大于或者等于保单最后一次月结日期！";
					return false;
				}
			}
			sql = "select 1 from lpgrpedoritem a where grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and edortype='WJ'"
					+ " and exists( select 1 from lpedorapp where edoracceptno=a.edorno and  confdate>= (select max(confdate) from ljapaygrp where a.grpcontno=grpcontno )) ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该集体合同最后一次续期缴费后做过无名单减少被保人，不能进行退保操作!");
				Content = "该集体合同最后一次续期缴费后做过无名单减少被保人，不能进行退保操作!";
				return false;
			}
			// 按照需求增加校验，对于约定交费的团单，只能申请协议退保，不能申请解约。
			sql = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo() + "'  and payintv = -1 ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该单缴费频次为约定缴费，不能申请解约保全项目");
				Content = "该单缴费频次为约定缴费，不能申请解约保全项目";
				return false;
			}
		}
		sql = "select state from lcgrpbalplan where grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo() + "'";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0 && sqlSSRS.GetText(1, 1) != "0")
		{
			System.out.println("该集体合同目前正在进行定期结算!不能进行保全操作!");
			Content = "该集体合同目前正在进行定期结算!不能进行保全操作!";
			return false;
		}
		sql = "select edorno from lpgrpedoritem a where grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and edortype='TZ' and "
				+ " exists ( select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0' )";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0)
		{
			System.out.println("团体万能增人不能和其他保全项目一起操作!");
			Content = "团体万能增人不能和其他保全项目一起操作!";
			return false;
		}
		String strSQL = "select EdorAcceptNo, EdorNo, EdorType, GrpContNo, "
                + "(select CustomerNo from LCGrpAppnt "
                + " where GrpContNo = LPGrpEdorItem.GrpContNo), "
                + " EdorValiDate, "
                + "	case EdorState "
                + "		when '1' then '录入完毕' "
                + "		when '2' then '理算确认' "
                + "		when '3' then '未录入' "
                + "		when '4' then '试算成功' "
                + "		when '0' then '保全确认' "
                + "	end "
                + "from LPGrpEdorItem "
                + "where EdorAcceptNo='" + EdorAcceptNo + "' "
                + "	and EdorNo='" + EdorAcceptNo + "' ";
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		if (strSQLSSRS.getMaxRow() > 0)
		{
			for (int h = 1; h <= strSQLSSRS.getMaxRow(); h++)
			{
				if (strSQLSSRS.GetText(h, 3) == "JM")
				{
					System.out.println("激活卡客户资料变更不能和其他保全项目一起操作!");
					Content = "激活卡客户资料变更不能和其他保全项目一起操作!";
					return false;
				}
				if (strSQLSSRS.GetText(h, 3) == "RS")
				{
					System.out.println("保单暂停功能不能和其他保全项目一起操作!");
					Content = "保单暂停功能不能和其他保全项目一起操作!";
					return false;
				}
				if (strSQLSSRS.GetText(h, 3) == "RR")
				{
					System.out.println("保单恢复功能不能和其他保全项目一起操作!");
					Content = "保单恢复功能不能和其他保全项目一起操作!";
					return false;
				}
			}
		}
		if (mFormData.getEdorType() == "RS")
		{
			boolean flag = true;
			// 是建工险种的不需要配置，可直接添加保单暂停的保全项目
			String checkJianGongXian = "select riskcode from lcgrppol where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo() + "'";
			SSRS checkJianGongXianSSRS = tExeSQL.execSQL(checkJianGongXian);
			if (checkJianGongXianSSRS.getMaxRow() > 0)
			{
				for (int i = 1; i <= checkJianGongXianSSRS.getMaxRow(); i++)
				{
					if (checkJianGongXianSSRS.GetText(1, i) == "190106"
							|| checkJianGongXianSSRS.GetText(1, i) == "590206"
							|| checkJianGongXianSSRS.GetText(1, i) == "5901")
					{
						String checkJianGongXianQiTa = "select 1 from lcgrppol where grpcontno='"
								+ mLCGrpContInfo.getGrpContNo()
								+ "' and riskcode not in ('190106','590206','5901')";
						SSRS checkJianGongXianQiTaSSRS = tExeSQL
								.execSQL(checkJianGongXianQiTa);
						if (checkJianGongXianQiTaSSRS.getMaxRow() > 0)
						{
							System.out.println("该保单不只是含有建工险种，还含有其它险种,无法添加此项目!");
							Content = "该保单不只是含有建工险种，还含有其它险种,无法添加此项目!";
							return false;
						}
					}
				}
				flag = false;
			}
			if (flag)
			{
				String RSSql = "select 1 from LCGrpEdor where ValidFlag='1' and state='0' and GrpContNo = '"
						+ mLCGrpContInfo.getGrpContNo() + "'";
				SSRS RSresult = tExeSQL.execSQL(RSSql);
				if (RSresult.getMaxRow() < 0)
				{
					System.out.println("该保单未在配置表中配置,或者该保单已经暂停期满,无法添加此项目!");
					Content = "该保单未在配置表中配置,或者该保单已经暂停期满,无法添加此项目!";
					return false;
				}
			}
		}
		String IsRSsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and ((enddate is null) or (startdate<=current date  and enddate>current date )) and grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo() + "'";
		SSRS IsRSarrResult = tExeSQL.execSQL(IsRSsql);
		if (IsRSarrResult.getMaxRow() > 0)
		{
			if (mFormData.getEdorType() != "RR")
			{
				System.out.println("该集体合同目前正处于保单暂停状态!不能添加保单恢复外的任何保全操作!");
				Content = "该集体合同目前正处于保单暂停状态!不能添加保单恢复外的任何保全操作!";
				return false;
			}
		} else
		{
			if (mFormData.getEdorType() == "RR")
			{
				System.out.println("该集体合同目前未处于保单暂停状态!不能添加保单恢复的保全操作!");
				Content = "该集体合同目前未处于保单暂停状态!不能添加保单恢复的保全操作!";
				return false;
			}
		}
		// 校验保全生效日是否在暂停期
		String IsRSRRsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and enddate is not null and startdate<='"
				+ mFormData.getEdorValiDate()
				+ "'  and enddate>'"
				+ mFormData.getEdorValiDate()
				+ "'  and grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo() + "'";
		SSRS IsRSRRResult = tExeSQL.execSQL(IsRSRRsql);
		if (IsRSRRResult.getMaxRow() > 0)
		{
			System.out.println("生效日期不可选在该集体合同暂停区间内!");
			Content = "生效日期不可选在该集体合同暂停区间内!";
			return false;
		}
		// add by 特需医疗险种的团单增减人不可同一个工单来做
		sql = "select 1 from lcgrppol a where  grpcontno ='"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype3='7')";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0 && strSQLSSRS.getMaxRow() > 0)
		{
			for (int j = 0; j < strSQLSSRS.getMaxRow(); j++)
			{
				if (strSQLSSRS.GetText(j, 3) == "NI"
						&& (mFormData.getEdorType() == "ZT"))
				{
					System.out.println("特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行减人操作");
					Content = "特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行减人操作";
					return false;
				}
				if (strSQLSSRS.GetText(j, 3) == "ZT"
						&& (mFormData.getEdorType() == "NI"))
				{
					System.out.println("特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行增人操作");
					Content = "特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行增人操作";
					return false;
				}
			}
		}
		if (mFormData.getEdorType() != "AC")
		{
			// 添加对于卡折业务类型保单的保全校验
			sql = "select 1 from lcgrpcont where cardflag ='2' and grpcontno ='"
					+ mLCGrpContInfo.getGrpContNo() + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				String sqlLccont = "select 1 from lccont where  grpcontno='"
						+ mLCGrpContInfo.getGrpContNo()
						+ "'  and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02')";
				SSRS sqlLccontSSRS = tExeSQL.execSQL(sqlLccont);
				if (sqlLccontSSRS.getMaxRow() > 0)
				{
					System.out.println("该保单为激活卡保单,不允许操作任何保全项目");
					Content = "该保单为激活卡保单,不允许操作任何保全项目";
					return false;
				} else
				{
					if (!(mFormData.getEdorType() == "AD")
							|| (mFormData.getEdorType() == "CM"))
					{
						System.out.println("卡折业务的保单只能操作客户资料变更或者联系方式变更的保全项目");
						Content = "卡折业务的保单只能操作客户资料变更或者联系方式变更的保全项目";
						return false;
					}
				}
			}
		}
		// 添加对 AC不可和NI、ZT同时做的校验
		if (mFormData.getEdorType() == "AC")
		{
			sql = "select 1 from lpgrpedoritem where  edorno='" + EdorAcceptNo
					+ "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和投保单位资料变更一起做。");
				Content = "其他保全项目不可以和投保单位资料变更一起做。";
				return false;

			}
		} else
		{
			sql = "select 1 from lpgrpedoritem where edortype='AC' and edorno='"
					+ EdorAcceptNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("投保单位资料变更不可以和其他保全一起做。");
				Content = "投保单位资料变更不可以和其他保全一起做。";
				return false;
			}
		}
		// 添加对CM不可和NI、ZT同时做的校验
		if (mFormData.getEdorType() == "CM")
		{
			sql = "select 1 from lpgrpedoritem where  edorno='" + EdorAcceptNo
					+ "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和团体客户资料变更一起做。");
				Content = "其他保全项目不可以和团体客户资料变更一起做。";
				return false;

			}
		} else
		{
			sql = "select 1 from lpgrpedoritem where edortype='CM' and edorno='"
					+ EdorAcceptNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("客户资料变更不可以和其他保全一起做。");
				Content = "客户资料变更不可以和其他保全一起做。";
				return false;

			}
		}
		sql = "select 1 from lpgrpedoritem a where grpcontno='"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and edortype='TY' ";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0)
		{
			System.out.println("该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!");
			Content = "该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!";
			return false;
		}

		// 添加对 AC不可和NI、ZT同时做的校验，杨天政 20110711
		if (mFormData.getEdorType() == "UM")
		{
			sql = "select 1 from lpgrpedoritem where  edorno='" + EdorAcceptNo
					+ "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和团险万能满期一起做。");
				Content = "其他保全项目不可以和团险万能满期一起做。";
				return false;

			}
			sql = "select 1 from lpgrpedoritem a where 1=1 and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and edorno!='"
					+ EdorAcceptNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和团险万能满期一起做。");
				Content = "其他保全项目不可以和团险万能满期一起做。";
				return false;
			}

		} else
		{
			sql = "select 1 from lpgrpedoritem where edortype='UM' and edorno='"
					+ EdorAcceptNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("团险万能满期不可以和其他保全一起做。");
				Content = "团险万能满期不可以和其他保全一起做。";
				return false;
			}
		}
		if (mFormData.getEdorType() == "ZF")
		{

			String sqlLcgrpcont = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and APPFLAG='1' and stateflag='1' and signdate is not null ";
			SSRS sqlLcgrpcontSSRS = tExeSQL.execSQL(sqlLcgrpcont);
			if (sqlLcgrpcontSSRS.getMaxRow() < 0)
			{
				System.out.println("该保单为非承保有效保单，不能做终止缴费项目");
				Content = "该保单为非承保有效保单，不能做终止缴费项目";
				return false;

			}

			sql = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo() + "'  and payintv <> 0 ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() < 0)
			{
				System.out.println("只有缴费频次为期缴或“约定缴费”的保单，才能申请终止缴费项目");
				Content = "只有缴费频次为期缴或“约定缴费”的保单，才能申请终止缴费项目";
				return false;

			}

			sql = "select max(dealstate) from ljspayb where otherno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and dealstate not in ('1','2','6') ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				if (sqlSSRS.GetText(1, 1) == "0")
				{
					System.out.println("该保单续期状态为已抽档待收费，不能做终止缴费项目");
					Content = "该保单续期状态为已抽档待收费，不能做终止缴费项目";
				} else if (sqlSSRS.GetText(1, 1) == "4")
				{
					System.out.println("该保单续期状态为已收费待核销，不能做终止缴费项目");
					Content = "该保单续期状态为已收费待核销，不能做终止缴费项目";
				} else
				{
					System.out.println("保单处在续期中间状态下，不能申请终止缴费项目");
					Content = "保单处在续期中间状态下，不能申请终止缴费项目";
				}
				return false;
			}

			sql = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "'  and state = '03050002' ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("满足上述条件的团单只要申请终止缴费结案后，不能再次申请终止缴费");
				Content = "满足上述条件的团单只要申请终止缴费结案后，不能再次申请终止缴费";
				return false;
			}
		}
		// add by 团体公共保额分配的生效日期必须为当天
		if (mFormData.getEdorType() == "GD")
		{
			if (mFormData.getEdorValiDate() != PubFun.getCurrentDate())
			{
				System.out.println("保额分配的保全生效日期必须为当前日期！");
				Content = "保额分配的保全生效日期必须为当前日期！";
				return false;
			}
		}
		/*
		 * 保单下有未结案理赔案件或者有未回销的预付赔款（保单预付赔款余额不为0）时
		 * 保全不能进行结余返还，待理赔案件处理完毕，没有未回销预付赔款时才能做结余返还。
		 */
		if (mFormData.getEdorType() == "BJ")
		{
			String sql_pay = "select sum(b.realpay) from "
					+ "llcase a,llclaimdetail b where " + " a.rgtno = b.rgtno "
					+ " and a.caseno = b.caseno "
					+ " and a.RgtState not in ('09','11','12','14') "
					+ " and b.grpcontno = '" + mLCGrpContInfo.getGrpContNo()
					+ "' " + " group by grpcontno ";

			String sql_bala = "select PrepaidBala from "
					+ " LLPrepaidGrpCont where " + " PrepaidBala > 0 "
					+ " and state = '1' " + " and grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo() + "'";
			SSRS pay_result = tExeSQL.execSQL(sql_pay);
			SSRS bala_result = tExeSQL.execSQL(sql_bala);

			if (pay_result.getMaxRow() > 0 || bala_result.getMaxRow() > 0)
			{
				double bala = 0.0;
				double pay = 0.0;
				if (bala_result.getMaxRow() < 0)
				{
					bala = 0.0;
				} else
				{
					bala = Double.parseDouble(bala_result.GetText(1, 1));
				}
				if (pay_result.getMaxRow() < 0)
				{
					pay = 0.0;
				} else
				{
					pay = Double.parseDouble(pay_result.GetText(1, 1));
				}
				System.out
						.println("该团单未结案赔款"
								+ pay
								+ "元，未回销预付赔款"
								+ bala
								+ "元，无法添加此保全项目。因“未结案赔款”“未回销预付赔款”数据实时更新，请及时根据提示内容进行核实后进行操作。");
				Content = "该团单未结案赔款"
						+ pay
						+ "元，未回销预付赔款"
						+ bala
						+ "元，无法添加此保全项目。因“未结案赔款”“未回销预付赔款”数据实时更新，请及时根据提示内容进行核实后进行操作。";
				return false;
			}
		}
		return true;
	}
	/**
	 * 添加保全项目按钮
	 * 后台
	 * @return
	 */
	public boolean grpEdorItemSave(){
		
		  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
		  LPGrpEdorItemSet mLPGrpEdorItemSet =new LPGrpEdorItemSet();
		 
		  TransferData tTransferData = new TransferData(); 
		  GrpEdorItemUI tGrpEdorItemUI   = new GrpEdorItemUI();
		  //输出参数
		  String FlagStr = "";
		  //String Content = "";
		 
		  GlobalInput tGI = new GlobalInput(); //repair:
		  tGI=GI;  //参见loginSubmit.jsp
		  
		  CErrors tError = null;
		  String tBmCert = "";
		  System.out.println("aaaa");
		  //后面要执行的动作：添加，修改，删除
		  String fmAction="INSERT||GRPEDORITEM";
		  System.out.println("fmAction:"+fmAction); 

		  if(fmAction.equals("INSERT||GRPEDORITEM"))
		    {
		        tLPGrpEdorMainSchema.setEdorAcceptNo(EdorAcceptNo); 
		        tLPGrpEdorMainSchema.setEdorNo(EdorAcceptNo);
		        tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		        tLPGrpEdorMainSchema.setEdorAppDate(PubFun.getCurrentDate()); //保全申请日期
		        tLPGrpEdorMainSchema.setEdorValiDate(mFormData.getEdorValiDate()); //生效日期
		     
		        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		        tLPGrpEdorItemSchema.setEdorAcceptNo(EdorAcceptNo);
		        tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
		        tLPGrpEdorItemSchema.setEdorAppNo(EdorAcceptNo); 
		        tLPGrpEdorItemSchema.setEdorType(dMessHead.getMsgType()); 
		        tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo()); 
		        tLPGrpEdorItemSchema.setEdorAppDate(PubFun.getCurrentDate()); 
		        tLPGrpEdorItemSchema.setEdorValiDate(mFormData.getEdorValiDate()); 
		        tLPGrpEdorItemSchema.setManageCom(GI.ManageCom);
		        mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);
		    }

		    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++"+fmAction);
		    
		    //写死打印次数
		    ExeSQL tExeSQL = new ExeSQL();
		    String LCGrpContsql="update LCGrpCont set PrintCount='1' where GrpContNo='"+mLCGrpContInfo.getGrpContNo()+"'";
		    tExeSQL.execUpdateSQL(LCGrpContsql);
		    
		    try
		    {
		        // 准备传输数据 VData
		         VData tVData = new VData();
		         
		         tVData.add(mLPGrpEdorItemSet);
		         tVData.add(tLPGrpEdorMainSchema);
		         tVData.add(tTransferData);
		         tVData.add(tGI);
		         
		          
		         //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		        if ( tGrpEdorItemUI.submitData(tVData,fmAction))
		        {
		            if (fmAction.equals("INSERT||GRPEDORITEM"))
			        {
			    	    System.out.println("11111------return");
			            	
			    	    tVData.clear();
			    	    tVData = tGrpEdorItemUI.getResult();
			    	    
			    	    LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet(); 
			            tLPGrpEdorItemSet = (LPGrpEdorItemSet)tVData.getObjectByObjectName("LPGrpEdorItemSet", 0);

			        }
			    }
			    else
			    {
			        Content = "保存失败，原因是:" + tGrpEdorItemUI.mErrors.getFirstError();
			        System.out.println(Content);
		    	    FlagStr = "Fail";
		    	    return false;
			    }
		    }
		    
		    catch(Exception ex)
		    {
		      Content = "保存失败，原因是:" + ex.toString();
		      System.out.println(Content);
		      FlagStr = "Fail";
		    }

		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		  if (FlagStr.equals(""))
		  {
		    tError = tGrpEdorItemUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		      FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getFirstError();
		    	System.out.println(Content);
		    	FlagStr = "Fail";
		    	return false;
		    }
		   }
		
		return true;
	}
	
	/**
	 * 当保全有单独逻辑的时候重写次方法
	 */
	public boolean domake(){
		return true;
	}
	/**
	 * 保全项目明细
	 * 保存按钮
	 * @return
	 */
	public boolean checkSave(){
		LPGrpContSet mLPGrpContSet=new LPGrpContSet();
		  
		  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		  	
/*
			String edorAcceptNo = request.getParameter("EdorAcceptNo");
			String edorNo = request.getParameter("EdorNo");
			String edorType = request.getParameter("EdorType");
			String GrpcontNo = request.getParameter("GrpContNo");*/
			
			
		    
		   // String tChk[] = request.getParameterValues("InpPolGridChk");
		    //String tGrpContNo[]= request.getParameterValues("PolGrid9");
		//   String tGrpPolNo[]= request.getParameterValues("PolGrid1");
//		    for (int i=0;i<tChk.length;i++)
//		    {
		 //       System.out.println("$$$$$$$$$$$44"+tChk[i]);
		 //       if (tChk[i].equals("1"))
//		        {
		            LPGrpContSchema tLPGrpContSchema=new LPGrpContSchema();
		            tLPGrpContSchema.setEdorNo(EdorAcceptNo);
		            tLPGrpContSchema.setEdorType(dMessHead.getMsgType());
		            tLPGrpContSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		            
		            mLPGrpContSet.add(tLPGrpContSchema);
//		        }
//		    }
		                
			
			LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
			tLPEdorItemSchema.setEdorAcceptNo(EdorAcceptNo);
			tLPEdorItemSchema.setEdorNo(EdorAcceptNo);
			tLPEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
			tLPEdorItemSchema.setContNo("000000");
			tLPEdorItemSchema.setEdorType(dMessHead.getMsgType());
			tLPEdorItemSchema.setInsuredNo("000000");
			tLPEdorItemSchema.setPolNo("000000");
			//tLPEdorItemSchema.setGetMoney(getMoney);
			
		        // 准备传输数据 VData		
			VData tVData = new VData();
			tVData.add(GI);
			tVData.add(tLPEdorItemSchema);
			tVData.add(mLPGrpContSet);

		    

		    GEdorZFDetailUI tGEdorZFDetailUI   = new GEdorZFDetailUI();  
				if (!tGEdorZFDetailUI.submitData(tVData, ""))
				{
					VData rVData = tGEdorZFDetailUI.getResult();
					System.out.println("Submit Failed! " + tGEdorZFDetailUI.mErrors.getErrContent());
					Content = "保存失败，原因是:" + tGEdorZFDetailUI.mErrors.getFirstError();
					return false;
				}
				else 
				{
					Content = "保存成功";
					return true;
				} 
	}
	
	/**
	 * 保全理算
	 * @return
	 */
	public boolean edorAppConfirm(){
		//前台校验
		ExeSQL tExeSQL = new ExeSQL();
		String strSQL=" select * from lpgrpedoritem where edoracceptno='"+EdorAcceptNo+"'";
		SSRS arrResult = tExeSQL.execSQL(strSQL);
		if(arrResult.MaxRow <= 0)
		{
			Content = "未添加保全项目，不能进行保全理算！";	
			System.out.println(Content);
			return false;
		}

		strSQL=" select 'x' from lpgrpedoritem where edoracceptno='"+EdorAcceptNo+"' and EdorState='3'";
		arrResult = tExeSQL.execSQL(strSQL);
		if(arrResult.MaxRow > 0)
		{
			Content = "本次申请，有保全项目处于未录入状态，不能进行保全理算！";	
			System.out.println(Content);
			return false;
		}
		if ("".equals(EdorAcceptNo))
		{
			Content = "请重新申请!";
			System.out.println(Content);
			return false;
		}
	
//	    fm.all('fmAction').value = "INSERT||GEDORAPPCONFIRM";	       
//	    fm.action='GEdorAppConfirmSubmit.jsp';
		//后台数据提交
	    
	    //集体批改信息
	    LPGrpEdorMainSchema tLPGrpEdorMainSchema  = new LPGrpEdorMainSchema();
	    tLPGrpEdorMainSchema.setEdorAcceptNo(EdorAcceptNo);
	    tLPGrpEdorMainSchema.setEdorNo(EdorAcceptNo);
	    tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
	    tLPGrpEdorMainSchema.setEdorValiDate(mFormData.getEdorValiDate());
	    tLPGrpEdorMainSchema.setEdorAppDate(PubFun.getCurrentDate());
	    VData data = new VData();  
	    data.add(GI);   
	    data.add(tLPGrpEdorMainSchema);
	    PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
	    if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM"))
	    {
//	      flag = "Fail";
	      Content = "保全理算失败！原因是：" + tPGrpEdorAppConfirmUI.getError().getErrContent();
	      System.out.println(Content);
	      return false;
	    }
	    else
	    {
//			flag = "Succ";
	    	Content = "保全理算成功。";
	    	System.out.println(Content);
	    }
	    Content = PubFun.changForHTML(Content);
		return true;
	}
	
	/**
	 * 保全确认
	 * 前台
	 * @return
	 */
	public boolean edorConfirm(){
		//前台校验
		ExeSQL tExeSQL = new ExeSQL();
		String strSql = "select statusno from lgwork where workno = '" + EdorAcceptNo + "' ";
		SSRS arrResult = tExeSQL.execSQL(strSql);
		if("5".equals(arrResult.GetText(1,1)))
		{
		    Content = "保全已经确认，请不要重复确认！";
		    System.out.println(Content);
		    return false;
		}
		if("88".equals(arrResult.GetText(1,1)))
		{
			Content = "保全审批意见为[不同意]，请重新录入保全明细或撤销保全工单！";
			System.out.println(Content);
		    return false;
		}

		String findSql="select serialno from LCUrgeVerifyLog where serialno='"+EdorAcceptNo+"' ";
		SSRS arrResult_1 = tExeSQL.execSQL(findSql);
		if( arrResult_1.MaxRow > 0 )
		{
			Content = "正在保全确认或者重复理算操作，请不要再次点击保全确认！";
			System.out.println(Content);
			return false;
		}
		String strSql2 = "select GetMoney from LPGrpEdorMain where EdorAcceptNo = '"
				+ EdorAcceptNo + "'";
		SSRS strSqlSSRS = tExeSQL.execSQL(strSql2);
		if (strSqlSSRS.getMaxRow() < 0) {
			Content = "查询保全主表信息出错！";
			System.out.println(Content);
			return false;
		}
		return true;
	}
	/**
	 * 保全确认
	 * 后台
	 * @return
	 */
	public boolean GEdorConfirmSubmit(){
		ExeSQL tExeSQL = new ExeSQL();
		String flag = "";
	    String edorAcceptNo = EdorAcceptNo;
		String payMode = mFormData.getPayMode();
		String balanceMethodValue = "1";
		String sql = "select OtherNo from LPEdorApp where EdorAcceptNo = '" + edorAcceptNo + "' ";
		SSRS result = tExeSQL.execSQL(sql);
		String customerNo = result.GetText(1,1);
		sql ="select getmoney from LPEdorApp where EdorAcceptNo='"+edorAcceptNo+"'";
		result = tExeSQL.execSQL(sql);
		if (result.MaxRow < 0) 
		  {
				Content = "查询保全申请主表时出错，保全受理号为"+edorAcceptNo+"！";
				System.out.println(Content);
				return false;
			}
		String accType = "";
		String destSource = "";
		String fmtransact = "";
		String fmtransact2 = "NOTUSEACC";
		if(Double.parseDouble(result.GetText(1,1)) > 0){
			accType = "0";
			destSource = "01";
			fmtransact = "1";//交费
//			fmtransact2 = "INSERT||TakeOut";
		}else{
			accType = "1";
			destSource = "11";
			fmtransact = "0";//退费
//			fmtransact2 = "INSERT||ShiftTo";
		}
		String contType= "";
	    GlobalInput gi = GI;
	    
	    String failType = null;
			String autoUWFail = null;
			
			System.out.println("fmtransact2:" + fmtransact2);
			
			//若使用账户冲抵保全收退费，则默认为即时结算
			if(!"NOTUSEACC".equals(fmtransact2)){
				balanceMethodValue="1";
			}
			//qulq 2007-4-5 
			String 	checkSQL = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType in ('LQ','ZB')";
	  	SSRS rs =new ExeSQL().execSQL(checkSQL);
	  	if(rs!= null && rs.getMaxRow() > 0 )
	  	{
		  	if(balanceMethodValue.equals("0"))
	  		{
	  			flag = "Fail";
				Content = "部分领取、追加保费项目不得进行定期结算，请单独操作并进行即时结算";
				System.out.println(Content);
	  		  	return false;
	  		}
	  	}
	  
	  	//xiep 2008-12-1 增加做NI项目时会增人重复的校验
			String 	checkNI = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'NI' ";
	  	SSRS mssrs =new ExeSQL().execSQL(checkNI);
	  	if(mssrs!= null && mssrs.getMaxRow() > 0 )
	  	{
	  		//增加理算后对于lcinsured是否缺失的校验 add by xp 100604
	  		String 	checkInsured = " select 1 from lccont where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"+
	  								edorAcceptNo+"' fetch first 1 row only) and contno not in "+
	  								"(select distinct contno from lcinsured where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"+
	  								edorAcceptNo+"' fetch first 1 row only) ) and contno in (select contno from lcinsuredlist where edorno='"+edorAcceptNo+"') ";
	  	  SSRS mssrs1 =new ExeSQL().execSQL(checkInsured); 
	  	  if(mssrs1!= null && mssrs1.getMaxRow() > 0 )  
	 	   	  {
	  			 flag = "Fail";
				Content = "保全项目增人时出现lcinsured丢失的问题,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
	  		  	System.out.println(Content);
	  		  	return false;
	 	   	  }     					
	       //------------------insured校验结束-------------------  
	          					
	  			String sql1 = "select getmoney from lpgrpedoritem "+
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'NI' ";
	            SSRS tSSRS1 =new ExeSQL().execSQL(sql1);
	            String itemmoney = tSSRS1.GetText(1,1);
	            String sql2 = "select sum(getmoney) from ljsgetendorse "+
	            					" where endorsementno = '" + edorAcceptNo + "' " +
	            					" and feeoperationtype='NI' and feefinatype='BF' with ur";
	            SSRS tSSRS2 =new ExeSQL().execSQL(sql2);
	            String endorsemoney = tSSRS2.GetText(1,1);
	  	
	  		if(!itemmoney.equals(endorsemoney))
		  	{
	  			flag = "Fail";
				Content = "保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
				System.out.println(Content);
	  		  	return false;
	  		}
	        //-----------------------关于增人总数和实际的理算的人数的比较-----------------------
	           String sql_check_ljagetendorse = 
	                      "  select count(distinct a.contno),count(distinct b.contno) "+
	                           "  from ljsgetendorse a, lcinsuredlist b "+
	                           "  where a.grpcontno = b.grpcontno "+
	                           "  and a.endorsementno = '"+edorAcceptNo+"'"+
	                           "  and a.feeoperationtype = 'NI' "+
	                           "  and a.endorsementno=b.edorno "+
	                           "  and b.state='1' "+
	                           "  with ur";
	                  //---------------------------------------------------------
	                    SSRS t_check_ljagendorse_SSRS1 =new ExeSQL().execSQL(sql_check_ljagetendorse);
	                 //---------------------------------------------------------
	                    System.out.println("sql_check_ljagetendorse:"+sql_check_ljagetendorse);
	                    int the_number_of_1 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1,1));
	                    int the_number_of_2 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1,2));
	                    System.out.println("the_number_of_1:"+the_number_of_1);
	                    System.out.println("the_number_of_2:"+the_number_of_2);
	                 if(the_number_of_1!=the_number_of_2)
	                 {
	                     flag = "Fail";
	                     Content = "实际理算完成的人数和模板录入的人数不一致，请点击[重复理算]回退到保全明细录入页面，并重新录入[NI]项目明细";
	                     System.out.println(Content);
	     	  		  	 return false;
	                 }
	  			
	  	}
	  	
	  	//xiep 2010-4-21 增加做ZT项目时会错误的校验
			String 	checkZT = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'ZT' ";
	  	SSRS mssrs1 =new ExeSQL().execSQL(checkZT);
	  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
	  	{
	  			String sql3 = "select sum(getmoney) from lpedoritem "+
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'ZT' ";
	            SSRS tSSRS3 =new ExeSQL().execSQL(sql3);
	            String itemmoneyZT = tSSRS3.GetText(1,1);
	            String sql4 = "select case when sum(getmoney) is null then 0 else sum(getmoney) end from ljsgetendorse "+
	            					" where endorsementno = '" + edorAcceptNo + "' " +
	            					" and feeoperationtype='ZT'  with ur";
	            SSRS tSSRS4 =new ExeSQL().execSQL(sql4);
	            String endorsemoneyZT = tSSRS4.GetText(1,1);
	  	
	  		if(!itemmoneyZT.equals(endorsemoneyZT))
		  	{
	  			flag = "Fail";
				Content = "保全项目减人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[ZT]项目明细重试";
				System.out.println(Content);
	  		  	return false;
	  		}
	  	}
	  	
	  	//xiep 2009-6-24 增加做WS项目时会实名化重复的校验
			String 	checkWS = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'WS' ";
	  	SSRS mrs =new ExeSQL().execSQL(checkWS);
	  	if(mrs!= null && mrs.getMaxRow() > 0 )
	  	{
	  			String wssql = "select 1 from lpcont where edorno='" + edorAcceptNo + "' group by insuredno having count(contno)>1";
	            SSRS wsSSRS =new ExeSQL().execSQL(wssql);
	        if(wsSSRS!= null && wsSSRS.getMaxRow() > 0 )
	  		{
	  			flag = "Fail";
				Content = "无名单实名化时增人出现重复数据,请点击[重复理算]回退到保全明细后重新添加[WS]项目明细";
				System.out.println(Content);
	  		  	return false;
			}
	  	}

		if(!flag.equals("Fail"))
			{
			EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorAcceptNo, "YE");
			tSpecialData.add("CustomerNo", customerNo);
			tSpecialData.add("AccType", accType);
			tSpecialData.add("OtherType", "3");
			tSpecialData.add("OtherNo", edorAcceptNo);
			tSpecialData.add("DestSource", destSource);
			tSpecialData.add("ContType", contType);
		  tSpecialData.add("Fmtransact2", fmtransact2);
	    
	   	LCAppAccTraceSchema tLCAppAccTraceSchema=new LCAppAccTraceSchema();
		  tLCAppAccTraceSchema.setCustomerNo(customerNo);
		  tLCAppAccTraceSchema.setAccType(accType);
		  tLCAppAccTraceSchema.setOtherType("3");//团单
		  tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
		  tLCAppAccTraceSchema.setDestSource(destSource);
		  tLCAppAccTraceSchema.setOperator(gi.Operator);
		  
	    VData tVData = new VData();
	    tVData.add(tLCAppAccTraceSchema);
	    tVData.add(tSpecialData);
			tVData.add(gi);
			PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
			if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param"))
			{
				flag = "Fail";
				Content = "处理帐户余额失败！";
				System.out.println(Content);
	  		  	return false;
			}
	  	else
	  	{
		    BqConfirmUI tBqConfirmUI = new BqConfirmUI(gi, edorAcceptNo, BQ.CONTTYPE_G,"");
		    if (!tBqConfirmUI.submitData())
		    {
		      flag = "Fail";
		      Content = tBqConfirmUI.getError();
		      System.out.println(Content);
	  		  return false;
		    }
		    else
		    {
		    	System.out.println("交退费通知书" + edorAcceptNo);
		    	String strSql = "select a.BankCode, a.BankAccno, a.AccName " +
							 "from LCGrpCont a, LPGrpEdorMain b " +
							 "where a.GrpContNo = b.GrpContNo " +
							 "and   b.EdorAcceptNo = '" + edorAcceptNo + "' and a.BankAccno ='"+mFormData.getBankAccNo()+"'";
		    	SSRS rss = new ExeSQL().execSQL(strSql);
		    	String accName = "";
		    	if(rss.MaxRow > 0){
		    		accName = rss.GetText(1, 3);
		    	}
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("payMode", payMode);	
					tTransferData.setNameAndValue("endDate", mFormData.getPayDate());
					tTransferData.setNameAndValue("payDate", mFormData.getPayDate());
					tTransferData.setNameAndValue("bank", mFormData.getBank());
					tTransferData.setNameAndValue("bankAccno", mFormData.getBankAccNo());
					tTransferData.setNameAndValue("accName", accName);
		  		
		  		//生成交退费通知书
					FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(edorAcceptNo);
					if (!tFeeNoticeGrpVtsUI.submitData(tTransferData))
					{
						flag = "Fail";
						Content = "生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError();
						System.out.println(Content);
			  		  	return false;
					} 		
					
					VData data = new VData();
					data.add(gi);
					data.add(tTransferData);
					SetPayInfo spi = new SetPayInfo(edorAcceptNo);
					try{
						if (!spi.submitDate(data, fmtransact))
						{
							System.out.println("设置转帐信息失败！");
							flag = "Fail";
							Content = "设置收退费方式失败！原因是：" + spi.mErrors.getFirstError();
							System.out.println(Content);
				  		  	return false;
						}
					}catch (Exception e) {
						System.out.println("Error");
						System.out.println("设置转帐信息失败！");
						flag = "Fail";
						Content = "设置收退费方式失败！原因是：" + spi.mErrors.getFirstError();
						System.out.println(Content);
			  		  	return false;
					}
					
		  		flag = "Succ";
					Content = "保全确认成功！";
					System.out.println(Content);
					String message = tBqConfirmUI.getMessage();
					if ((message != null) && (!message.equals("")))
					{
					  Content += "\n" + tBqConfirmUI.getMessage();
					}
		    }
			  Content = PubFun.changForHTML(Content);
		    failType = tBqConfirmUI.getFailType();  //是否审批通过
		    autoUWFail = tBqConfirmUI.autoUWFail();  //是否自核通过
		    autoUWFail = autoUWFail == null ? "" : autoUWFail;
		  }
		  }
		  System.out.println("end");
		  return true;
	}
	
	// 返回错误信息
	public String getErrorValue() {
		return Content;
	}

}
