package com.preservation.WZ;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.BQBase.InterfaceBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;


/**
 * 
 * WZ无名单增加被保人 接口 得到数据 调用处理类DealWZ.java
 * 
 * @author fq
 *
 */
public class InterfaceWZ extends InterfaceBase{

	

	

	/**
	 * 逻辑处理
	 */
	public boolean dealc(){
		DealWZ dealWZ = new DealWZ();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		if (!dealWZ.deal(data)) {
			errorValue = dealWZ.getErrorValue();
			gongdanhao = dealWZ.getEdorAcceptNo();
			String sql="";
			try {
				sql = "insert into ErrorInsertLog " +
				"(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ," +
				"Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ," +
				"transactionBatch ,transactionDescription ,transactionAddress ) " +
				"values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
				+GI.Operator+"','"
				+pxml+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), " +
				"to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), " +
				"'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+returnXML(pxml)+"','中间状态','"+errorValue+"','"+mMessHead.getMsgType()+"','"
				+mMessHead.getBranchCode()+"','"+mMessHead.getBatchNo()+"','交易描述','交易地址')";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(StringUtil.StringNull(sql)){
				tExeSQL.execUpdateSQL(sql);
			}
			
			return false;
		}
		gongdanhao = dealWZ.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	
	

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement ){
		Element WZ = rootElement.getChild("WZ");
		// 客户号
		
		Element msgCustomer = WZ.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = WZ.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = WZ.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType")); 
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		if(StringUtil.StringNull(xxFormData.getChildText("PremWZ"))){
			tFormData.setPremWZ(Double.parseDouble(xxFormData.getChildText("PremWZ")));
		}
		if(StringUtil.StringNull(xxFormData.getChildText("Peoples"))){
			tFormData.setPeoples(Integer.parseInt(xxFormData.getChildText("Peoples")));
		}
		if(StringUtil.StringNull(xxFormData.getChildText("SumPrem"))){
			tFormData.setSumPrem(Double.parseDouble(xxFormData.getChildText("SumPrem")));
		}
		
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
		tFormData.setElement(xxFormData.getChildren("plans"));
		data.add(tFormData);
	}
	
	
	
	public static void main(String[] args) {
		InterfaceWZ tInterfaceWZ = new InterfaceWZ();
		
		try {
			InputStream pIns = new FileInputStream("C:/Users/dongjl/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceWZ.deal(mInXmlStr);
			System.out.println(tInterfaceWZ.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
}
