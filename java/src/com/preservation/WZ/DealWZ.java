package com.preservation.WZ;

import java.util.List;

import org.jdom.Element;

import com.preservation.BQBase.DealBase;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.bq.GEdorWZDetailUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * @author fq
 *
 */
public class DealWZ extends DealBase{
	/**
	 * 保全项目明细
	 * 保存按钮
	 * @return
	 */
	public boolean checkSave(){
		ExeSQL tExeSQL = new ExeSQL();
		List<Element> list=mFormData.getElement();
		for(int i=0;i<list.size();i++){
			String PlanCode=list.get(i).getChildText("PlanCode");
			int peoples2WZ =Integer.parseInt(list.get(i).getChildText("Peoples"));
			double premWZ =Double.parseDouble(list.get(i).getChildText("PremWZ"));
			double sumPremWZ = Double.parseDouble(list.get(i).getChildText("SumPrem"));
		
		//保存按钮前台校验
	/*	int peoples2WZ = mFormData.getPeoples();  //总人数
		double premWZ = mFormData.getPremWZ();  //人均期交保费
		double sumPremWZ = mFormData.getSumPrem();//总保费	
			*/
			
			String ss = "";
		if(StringUtil.StringNull(PlanCode)){
			ss = " and a.contPlanCode = '"+PlanCode+"'";
		}
		String sql = "SELECT DISTINCT a.contPlanCode, a.contPlanName, a.peoples2, " +
					" ( SELECT SUM(prem)  FROM LCPol WHERE grpContNo = a.grpcontNo AND contPlanCode = a.contPlanCode AND polTypeFlag = '1' ),c.contno " +
					" FROM  LCContPlan a , LCInsured b, LCCont c " +
					" WHERE a.grpContNo = b.grpContNo " +
					" AND a.contPlanCode = b.contPlanCode " +
					" AND b.contNo = c.contNo " +
					" AND c.polType = '1' " +
					" AND a.grpContNo = '"+mLCGrpContInfo.getGrpContNo()+"' " +
					" AND a.contPlanCode != '11' "+ss;

		SSRS result = tExeSQL.execSQL(sql);
		
		//Double premWZ1;
		
		if(StringUtil.StringNull(PlanCode)){
			if(result.getMaxNumber()>0){
				//premWZ1 = Double.parseDouble(result.GetText(1, 4))/Double.parseDouble(result.GetText(1, 3));
				sumPremWZ = peoples2WZ*premWZ;
			}
			
		}
		
		
	/*	  if(premWZ>premWZ1)
		  {
		    Content = "人均期交保费不可大于";
		    System.out.println(Content);
		    return false;
		  }
		  */
		  
		  
		  if(sumPremWZ>peoples2WZ*premWZ)
		  {
		    Content = "本次应收保费均应小于等于人均期交保费、增加被保险人数的乘积";
		    System.out.println(Content);
		    return false;
		  }
		
		
		
	/*	if(sumPremWZ != 0 && sumPremWZ != sumPremWZ){
			Content = "应收保费录入错误，与计算结果不符！";
			System.out.println(Content);
			return false;
		}*/
		

		  if(premWZ < 0 || sumPremWZ < 0 || peoples2WZ < 0)
		  {
		    Content = "人均期交保费、增加被保险人数、本次应收保费均应为非负数";
		    System.out.println(Content);
		    return false;
		  }
//		  if(!isInteger(peoples2WZ) || parseFloat(peoples2WZ) == 0)
//		  {
//		    alert("增加被保险人数应为正整数");
//		    return false;
//		  }
		  String contno = result.GetText(1,5);
		  String edorValiDate = mFormData.getEdorValiDate();
		  String sql2 = "select distinct lastPayToDate, curPayToDate, payCount from LJAPayPerson where contNo = '"+contno+"' order by curpaytodate";
		  result = tExeSQL.execSQL(sql2);
		  String sql3 = "  select 1 "
		            + "from dual "
		            + "where days('" + result.GetText(1,1) + "') > days('" + edorValiDate + "') "
		            + "   or days('" + result.GetText(1,2) + "') < days('" + edorValiDate + "') ";
		  
		  result = tExeSQL.execSQL(sql3);
		  if(result.MaxRow>0)
		  {
		    Content = "本次增人生效日期必须在本交费期的应交日期和交至日之间。";
		    System.out.println(Content);
		    return false;
		  }
		  
		  GlobalInput gi = GI;
		  
		  //集体批改信息
		  LPGrpEdorItemSchema tLPGrpEdorItemSchema  = new LPGrpEdorItemSchema();
		  tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
		  tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		  tLPGrpEdorItemSchema.setEdorType(dMessHead.getMsgType());
		  tLPGrpEdorItemSchema.setEdorValiDate(mFormData.getEdorValiDate());
		  
		  TransferData td = new TransferData();
		  td.setNameAndValue("prem", String.valueOf(sumPremWZ));
		  td.setNameAndValue("peoples2", String.valueOf(peoples2WZ));
		  
		  td.setNameAndValue("contPlanCode", PlanCode);
		  td.setNameAndValue("contNo", contno);
		  
		  VData data = new VData();
		  data.add(gi);
		  data.add(td);
		  data.add(tLPGrpEdorItemSchema);
		  
		  GEdorWZDetailUI ui = new GEdorWZDetailUI();
		  if(!ui.submitData(data, ""))
		  {
		    Content = ui.mErrors.getErrContent();
		    System.out.println(Content);
		    return false;
		  }
		  else
		  {
		    Content = "保存成功";
		  }
		  
		  Content = PubFun.changForHTML(Content);
		  System.out.println(Content);
		}
		return true;
	}

}
