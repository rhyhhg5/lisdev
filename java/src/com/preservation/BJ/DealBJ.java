package com.preservation.BJ;

import com.preservation.BQBase.DealBase;
import com.sinosoft.lis.bq.GEdorBJDetailUI;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/**
 * BJ结余返还
 * @author fq
 *
 */
public class DealBJ extends DealBase{

	public boolean checkSave(){
		ExeSQL tExeSQL = new ExeSQL();

		Double num = mFormData.getEdorValue();
		if(num<=0){
			Content = "输入的数值必须大于0："+num;
			System.out.println(Content);
			return false;
		}
		//下面需要对输入的金额进行校验
		//保单返还总金额（本次录入金额和之前操作的结余返还金额的合计）不能大于保单实收保费（包括首期、续期和保全收付费的合计）。
		
		//之前操作的结余返还金额的合计
		String sql_back = "select nvl(sum(-getmoney),0) from "
						+" ljagetendorse where feeoperationtype='BJ' and grpcontno='"+mLCGrpContInfo.getGrpContNo()+"'";

		//保单实收保费,下面两者之和 
		String sql_get1 = "select nvl(sum(sumactupaymoney),0) from "
						+" ljapay where incomeno='"+mLCGrpContInfo.getGrpContNo()+"' and incometype='1'" ;
								
		String sql_get2 = 	"select nvl(sum(getmoney),0) from "
						+"ljagetendorse where feeoperationtype!='BJ' and grpcontno='"+mLCGrpContInfo.getGrpContNo()+"'";
		//执行上述三条语句，然后获取我们的要的结果，进行判断
		SSRS back_result = tExeSQL.execSQL(sql_back);
		SSRS get1_result = tExeSQL.execSQL(sql_get1);
		SSRS get2_result = tExeSQL.execSQL(sql_get2);
		double math_back = Double.parseDouble(back_result.GetText(1, 1));
		double math_get1 = Double.parseDouble(get1_result.GetText(1, 1));
		double math_get2 = Double.parseDouble(get2_result.GetText(1, 1));
		if(((num)+math_back)>(math_get1+math_get2)){
			Content = "本次录入金额应不能大于"+(math_get1+math_get2-math_back);
			System.out.println(Content);
			return false;
		}
		
//		fm.all('fmtransact').value = "INSERT";
//		fm.submit();
		  CErrors tError = null; 
		  String tRela  = "";                
		  String FlagStr = "";
		  String transact = "INSERT";  

//		  	GlobalInput tGlobalInput = new GlobalInput();
//			tGlobalInput = (GlobalInput)session.getValue("GI");

//			 transact = request.getParameter("fmtransact");
			String edorNo = EdorAcceptNo;
			String edorType = "BJ";
			String edorValue = mFormData.getEdorValue().toString();
			
			LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
			tLPGrpEdorItemSchema.setEdorNo(edorNo);
			//tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
			tLPGrpEdorItemSchema.setGetMoney(edorValue);
			tLPGrpEdorItemSchema.setEdorType(edorType);

		  // 准备传输数据 VData
		  		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		  		tLPEdorEspecialDataSchema.setEdorNo(edorNo);
		  		tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
		  		
		  		tLPEdorEspecialDataSchema.setEdorType(edorType);
		  		tLPEdorEspecialDataSchema.setEdorValue(edorValue);
		  		
		  		
			    tLPEdorEspecialDataSchema.setDetailType("BJMONEY");
			    tLPEdorEspecialDataSchema.setPolNo("000000");
			
			VData tVData = new VData();
			tVData.add(GI);
			tVData.add(tLPGrpEdorItemSchema);
			tVData.add(tLPEdorEspecialDataSchema);
			//

			GEdorBJDetailUI tGEdorBJDetailUI = new GEdorBJDetailUI();
			if (!tGEdorBJDetailUI.submitData(tVData, transact))
			{
				VData rVData = tGEdorBJDetailUI.getResult();
				System.out.println("Submit Failed! " + tGEdorBJDetailUI.mErrors.getErrContent());
				Content = transact + "失败，原因是:" + tGEdorBJDetailUI.mErrors.getFirstError();
				FlagStr = "Fail";
				return false;
			}
			else 
			{
				Content = "保存成功";
				FlagStr = "Success";
				System.out.println(Content);
			}
		
		return true;
	}
	
}
