package com.preservation.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
  * @ClassName: StringUtil
  * @Description: TODO
  * @author liuzehong
  * @date 2018年12月4日 上午9:42:08
  * @version V1.0
 **/
public class StringUtil {
	
	/**
	 * 判断字符串是否为null或者空字符串
	 * @param str
	 * @return
	 */
	public static boolean isNull(String str) {
		if (!"".equals(str) && null != str) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 存在字符串为null或者空字符串，则返回null
	 * @param str
	 * @return
	 */
	public static boolean isNull(String...  str) {
		if (str==null) 
			return false;
		for (int i = 0; i < str.length; i++) 
			if(!isNull(str[i])) 
				return false;
		return true;
	}
	
	/**
	 * 判断是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return !isNull(str);
	}

	public static boolean isEmpty(String... str) {
		return !isNull(str);
	}
	
	/**
	 * 将流转化成字节数据
	 * 
	 * @param pIns
	 * @return
	 */
	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		try {
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return mByteArrayOutputStream.toByteArray();
	}

	/**
	 * 判断字符串是不是数字组成
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		return pattern.matcher(str).matches();
	}

	/**
	 * 首字母大写
	 * 
	 * @param str
	 * @return
	 */
	public static String upperCase(String str) {
		return !isEmpty(str) ? str.substring(0, 1).toUpperCase() + str.substring(1) : null;
	}

	/**
	 * 首字母小写
	 * 
	 * @param str
	 * @return
	 */
	public static String lowerCase(String str) {
		return !isEmpty(str) ? str.substring(0, 1).toLowerCase() + str.substring(1) : null;
	}
	
	/**
	 * 将一个字符串转化为输入流
	 */
	public static InputStream getStringStream(String sInputString) {
		if (!isEmpty(sInputString)) {
			try {
				ByteArrayInputStream tInputStringStream 
				      = new ByteArrayInputStream(sInputString.getBytes());
				return tInputStringStream;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 将一个输入流转化为字符串
	 */
	public static String getStreamString(InputStream tInputStream) {
		if (tInputStream != null) {
			try {
				BufferedReader tBufferedReader = new BufferedReader(new InputStreamReader(tInputStream));
				StringBuffer tStringBuffer = new StringBuffer();
				String sTempOneLine = new String("");
				while ((sTempOneLine = tBufferedReader.readLine()) != null) {
					tStringBuffer.append(sTempOneLine);
				}
				return tStringBuffer.toString();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

}
