package com.preservation.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
/**
 * 处理xml报文工具类
  * @ClassName: XmlUtil
  * @Description: TODO
  * @author liuzehong
  * @date 2018年12月4日 下午6:34:39
  * @version V1.0
 *
 */
public class XmlUtil {
    /**
     * xml转Map
     * @param requestXML
     * @return
     */
	public Map<String,Object> xmlToMap(String requestXML){   
	       Document document = null;  
	       Map<String,Object> map=null;
		      try {  
		       document = DocumentHelper.parseText(requestXML);  
		      } catch (DocumentException e) {  
		       System.out.println("解析数据错误，请检查文本" + e.getMessage());  
		      }  
		      Element root = document.getRootElement();  
		    //子节点
		      @SuppressWarnings("unchecked")
			List<Element> childElements = root.elements();
		       map = new HashMap<String, Object>();
		      //遍历子节点
		      map = getAllElements(childElements,map);
			return map;  
	    }
	   /**
	    * 将所有节点转化成map
	    * @param childElements
	    * @param mapEle
	    * @return
	    */
	    @SuppressWarnings("unchecked")
		private Map<String, Object> getAllElements(List<Element> childElements,Map<String,Object> mapEle) {
	    	if(!childElements.isEmpty()) {  
		    	for (Element ele : childElements) {
		            mapEle.put(ele.getName(), ele.getText());
		            if(ele.elements().size()>0){
		                mapEle = getAllElements(ele.elements(), mapEle);
		            }
		        }
	    	}
	        return mapEle;
	    }
	    /**
	     * xml转JSONObject
	     * @param xml
	     * @return
	     * @throws JSONException
	     * @throws IOException
	     */
	    public static JSONObject xmlToJSONObject(String xml) throws JSONException, IOException {
	        JSONObject xmlJSONObj = XML.toJSONObject(xml);
	        return xmlJSONObj;
	    }
	    /**
	     * xml转JSO字符串
	     * @param xml
	     * @return
	     * @throws JSONException
	     * @throws IOException
	     */
	    public static String xmlToStrJSON(String xml) throws JSONException, IOException {
	        JSONObject xmlJSONObj = XML.toJSONObject(xml);
	        return xmlJSONObj.toString();
	    }
	    /**
		 * map转xml map中没有根节点的键
		 * @param map
		 * @param rootName 根节点名称<必录>
		 * @return
		 * @throws DocumentException
		 * @throws IOException
		 */
	    public static String mapToXml(Map<String, Object> map, String rootName) throws DocumentException, IOException {
	        Document doc = DocumentHelper.createDocument();// 创建一个Document对象
	        Element root = DocumentHelper.createElement(rootName); // 创建根节点
	        doc.add(root);
	        mapToXml(map, root);
	        return formatXml(doc);
	    }
	    /**
	     * map转xml
	     * @param map
	     * @param body
	     * @return
	     */
	    @SuppressWarnings({ "rawtypes", "unchecked" })
	    private static Element mapToXml(Map<String, Object> map, Element body) {
	        Set<Entry<String, Object>> entries = map.entrySet();
	        for (Entry<String, Object> entry : entries) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            if (key.startsWith("@")) {// 属性
	                body.addAttribute(key.substring(1, key.length()), value.toString());
	            } else if (key.equals("#text")) { // 有属性时的文本
	                body.setText(value.toString());
	            } else {
	                if (value instanceof java.util.List) {
	                    List list = (List) value;
	                    Object obj;
	                    for (int i = 0; i < list.size(); i++) {
	                        obj = list.get(i);
	                        // list里是map或String，不会存在list里直接是list的
	                        if (obj instanceof Map) {
	                            Element subElement = body.addElement(key);
	                            mapToXml((Map) list.get(i), subElement);
	                        } else {
	                            body.addElement(key).setText((String) list.get(i));
	                        }
	                    }
	                } else if (value instanceof Map) {
	                    Element subElement = body.addElement(key);
	                    mapToXml((Map) value, subElement);
	                } else {
	                    body.addElement(key).setText(value.toString());
	                }
	            }
	        }
	        return body;
	    }
	    /**
	     * 格式化输出xml
	     * 
	     * @param document
	     * @return
	     * @throws DocumentException
	     * @throws IOException
	     */
	    public static String formatXml(Document document) throws DocumentException, IOException {
	        // 格式化输出格式
	        OutputFormat format = OutputFormat.createPrettyPrint();
	        // format.setEncoding("UTF-8");
	        StringWriter writer = new StringWriter();
	        XMLWriter xmlWriter = new XMLWriter(writer, format);
	        xmlWriter.write(document);
	        xmlWriter.close();
	        return writer.toString();
	    }
	    
	    /**
	     * 测试
	     * @param args
	     * @throws DocumentException
	     * @throws IOException
	     * @throws JSONException 
	     */
		public static void main(String[] args) throws DocumentException, IOException, JSONException {
			//map转xml
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			Map<String, Object> head = new LinkedHashMap<String, Object>();
			head.put("BatchNo", "");
			head.put("SendDate", "");
			head.put("MsgType", "");
			head.put("SendTime", "");
			head.put("BranchCode", "");
			head.put("SendOperator", "");
			Map<String, Object> body = new LinkedHashMap<String, Object>();
			body.put("TypeFlag", "");
			body.put("ManageCom", "");
			body.put("ProjectName", "");
			body.put("ProjectNo", "");
			body.put("GrpContNo", "");
			body.put("StartDate", "");
			body.put("EndDate", "");
			body.put("StartSignDate", "");
			body.put("EndSignDate", "");
			body.put("EstimateRate", "");
			body.put("EstimateBalanceRate", "");
			body.put("ContEstimatePrem", "");
			body.put("BackEstimatePrem", "");
			body.put("Reason", "");
			map.put("head", head);
			map.put("body", body);
			
			
			String newXml = mapToXml(map, "DataSet");
			System.out.println("=================4444444444444444444================map转xml=================================================");
			System.out.println(newXml);
			/*XmlUtil xmlUtil=new XmlUtil();
			System.out.println("===================================xml转map=================================================");
			Map<String, Object> newMap = xmlUtil.xmlToMap(newXml);
			System.out.println(newMap.toString());
			System.out.println("======================================xml转JSONObject===================================");
			System.out.println(XmlUtil.xmlToJSONObject(newXml));
			System.out.println("======================================xml转json字符串===================================");
			System.out.println(XmlUtil.xmlToStrJSON(newXml));*/
		
		}
}