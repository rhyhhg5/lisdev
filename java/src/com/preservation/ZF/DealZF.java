package com.preservation.ZF;

import com.preservation.BQBase.DealBase;
import com.sinosoft.lis.bq.GEdorZFDetailUI;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpContSchema;
import com.sinosoft.lis.vschema.LPGrpContSet;
import com.sinosoft.utility.VData;

/**
 * @author dongjiali
 *
 *	中止缴费
 */
public class DealZF extends DealBase{


	/**
	 * 保全项目明细
	 * 保存按钮
	 * @return
	 */
	public boolean checkSave(){
		LPGrpContSet mLPGrpContSet=new LPGrpContSet();
		  
		  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		  	
/*
			String edorAcceptNo = request.getParameter("EdorAcceptNo");
			String edorNo = request.getParameter("EdorNo");
			String edorType = request.getParameter("EdorType");
			String GrpcontNo = request.getParameter("GrpContNo");*/
			
			
		    
		   // String tChk[] = request.getParameterValues("InpPolGridChk");
		    //String tGrpContNo[]= request.getParameterValues("PolGrid9");
		//   String tGrpPolNo[]= request.getParameterValues("PolGrid1");
//		    for (int i=0;i<tChk.length;i++)
//		    {
		 //       System.out.println("$$$$$$$$$$$44"+tChk[i]);
		 //       if (tChk[i].equals("1"))
//		        {
		            LPGrpContSchema tLPGrpContSchema=new LPGrpContSchema();
		            tLPGrpContSchema.setEdorNo(EdorAcceptNo);
		            tLPGrpContSchema.setEdorType(dMessHead.getMsgType());
		            tLPGrpContSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		            
		            mLPGrpContSet.add(tLPGrpContSchema);
//		        }
//		    }
		                
			
			LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
			tLPEdorItemSchema.setEdorAcceptNo(EdorAcceptNo);
			tLPEdorItemSchema.setEdorNo(EdorAcceptNo);
			tLPEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
			tLPEdorItemSchema.setContNo("000000");
			tLPEdorItemSchema.setEdorType(dMessHead.getMsgType());
			tLPEdorItemSchema.setInsuredNo("000000");
			tLPEdorItemSchema.setPolNo("000000");
			//tLPEdorItemSchema.setGetMoney(getMoney);
			
		        // 准备传输数据 VData		
			VData tVData = new VData();
			tVData.add(GI);
			tVData.add(tLPEdorItemSchema);
			tVData.add(mLPGrpContSet);

		    

		    GEdorZFDetailUI tGEdorZFDetailUI   = new GEdorZFDetailUI();  
				if (!tGEdorZFDetailUI.submitData(tVData, ""))
				{
					VData rVData = tGEdorZFDetailUI.getResult();
					System.out.println("Submit Failed! " + tGEdorZFDetailUI.mErrors.getErrContent());
					Content = "保存失败，原因是:" + tGEdorZFDetailUI.mErrors.getFirstError();
					return false;
				}
				else 
				{
					Content = "保存成功";
					return true;
				} 
	}
	
	
}
