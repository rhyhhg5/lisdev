package com.preservation.CM.obj;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LcinsuredObj {

	private String OldName;
	private String OldIDNo;
	private String Name;
	private String Sex;
	private String Birthday;
	private String IDType;
	private String IDNo;
	private String IDStartDate;
	private String IDEndDate;
	private String OccupationCode;
	private String OccupationType;
	private String Marriage;
	private String Relation;
	private String RelationToAppnt;
	private String InsuredState;
	private String nationality;
	private String position;
	private String salary;
	private String GrpInsuredPhone;
	private String BankCode;
	private String AccName;
	private String BankAccNo;
	private  List<LcinsuredObj> LcinsuredObjSet=new ArrayList<LcinsuredObj>();
	
	
	
	public String getOldName() {
		return OldName;
	}
	public void setOldName(String oldName) {
		OldName = oldName;
	}
	public String getOldIDNo() {
		return OldIDNo;
	}
	public void setOldIDNo(String oldIDNo) {
		OldIDNo = oldIDNo;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getBirthday() {
		return Birthday;
	}
	public void setBirthday(String birthday) {
		Birthday = birthday;
	}
	public String getIDType() {
		return IDType;
	}
	public void setIDType(String iDType) {
		IDType = iDType;
	}
	public String getIDNo() {
		return IDNo;
	}
	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
	public String getIDStartDate() {
		return IDStartDate;
	}
	public void setIDStartDate(String iDStartDate) {
		IDStartDate = iDStartDate;
	}
	public String getIDEndDate() {
		return IDEndDate;
	}
	public void setIDEndDate(String iDEndDate) {
		IDEndDate = iDEndDate;
	}
	public String getOccupationCode() {
		return OccupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		OccupationCode = occupationCode;
	}
	public String getOccupationType() {
		return OccupationType;
	}
	public void setOccupationType(String occupationType) {
		OccupationType = occupationType;
	}
	public String getMarriage() {
		return Marriage;
	}
	public void setMarriage(String marriage) {
		Marriage = marriage;
	}
	public String getRelation() {
		return Relation;
	}
	public void setRelation(String relation) {
		Relation = relation;
	}
	public String getRelationToAppnt() {
		return RelationToAppnt;
	}
	public void setRelationToAppnt(String relationToAppnt) {
		RelationToAppnt = relationToAppnt;
	}
	public String getInsuredState() {
		return InsuredState;
	}
	public void setInsuredState(String insuredState) {
		InsuredState = insuredState;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getGrpInsuredPhone() {
		return GrpInsuredPhone;
	}
	public void setGrpInsuredPhone(String grpInsuredPhone) {
		GrpInsuredPhone = grpInsuredPhone;
	}
	public String getBankCode() {
		return BankCode;
	}
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}
	public String getAccName() {
		return AccName;
	}
	public void setAccName(String accName) {
		AccName = accName;
	}
	public String getBankAccNo() {
		return BankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}
	public List<LcinsuredObj> getLcinsuredObjSet() {
		return LcinsuredObjSet;
	}
	public void setLcinsuredObjSet(List<LcinsuredObj> lcinsuredObjSet) {
		LcinsuredObjSet = lcinsuredObjSet;
	}
	
	
	
}
