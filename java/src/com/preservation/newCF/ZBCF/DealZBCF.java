package com.preservation.newCF.ZBCF;

import com.preservation.newCF.DealBaseCF;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GEdorZBDetailUI;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class DealZBCF extends DealBaseCF {

	// 项目明细 保存
	public boolean checkSave() {

		ExeSQL tExeSQL = new ExeSQL();

		String edorNo = EdorAcceptNo;// 保全号
		String edorType = mFormData.getEdorType();// 保全类型
		String grpContNo = mLCGrpContInfo.getGrpContNo();// 集体保单号
		
		// 报文参数的非空校验
		if (!StringUtil.StringNull(edorNo) || !StringUtil.StringNull(edorType) || !StringUtil.StringNull(grpContNo)) {
			Content = "保全号，保全类型，团体合同号不能为空";
			return false;
		}
		
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo, edorType);

		String sql = "select grppolno from lcgrppol where  grpContNo = '" + mLCGrpContInfo.getGrpContNo() + "'";
		SSRS ssrs = tExeSQL.execSQL(sql);

		String grppolno = (ssrs.GetText(1, 1));

		String groupMoney = mFormData.getGroupMoney().toString();

		tSpecialData.setGrpPolNo(grppolno);// 险种号
		tSpecialData.add("GroupMoney", groupMoney);// 追加管理费

		GEdorZBDetailUI tGEdorZBDetailUI = new GEdorZBDetailUI(GI, edorNo, grpContNo, tSpecialData);

		if (!tGEdorZBDetailUI.submitData()) {

			Content = "保存失败，原因是:" + tGEdorZBDetailUI.getError();
			System.out.println(Content);
			return false;
		} else {

			Content = "数据保存成功。";
			String message = tGEdorZBDetailUI.getMessage();
			if (message != null) {

				Content = message;
				return false;
			}
			return true;
		}

	}

}
