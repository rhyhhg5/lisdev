package com.preservation.newCF.ACCF;

import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.preservation.AC.CreatXMLAC;
import com.preservation.AC.obj.ACLCInsuredInfo;
import com.preservation.newCF.GEdorCancelBQ;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqConfirmUI;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.PEdorAppAccConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorACDetailUI;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.PGrpEdorCancelUI;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPGrpAddressSchema;
import com.sinosoft.lis.schema.LPGrpAppntSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.schema.LPGrpSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 保全接口追加保费CF
 * 
 * @author 姜士杰 2018-12-13
 *
 */

public class InterfaceACCF {

	private String Content;// 错误信息
	private VData data = new VData();
	private GlobalInput mGlobalInput = new GlobalInput();
	String mCurrDate = PubFun.getCurrentDate();
	String mCurrTime = PubFun.getCurrentTime();
	private String LCGrpContAppntNo;
	private String LCGrpContManageCom;
	private String PrtNo;
	private String GrpContNo;
	private String mEdorAcceptNo;// 生成的工单处理号
	// 返回报文用
	private String SendOperator;
	private String MsgType;
	private String BatchNo;
	private String BranchCode;
	private String returnXML;

	public boolean deal(String xml) throws Exception {
		System.out.println("**************deal**************");
		try {
			if (!getAndCheckData(xml)) {
				System.out.println("**************无名单实名化接口程序处理错误：撤销工单**************");
				GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
				VData tt = new VData();
				tt.add(mGlobalInput);
				tt.add(mEdorAcceptNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				return false;
			}
			if (!ACdetail()) {
				System.out.println("**************无名单实名化接口程序处理错误：撤销工单**************");
				GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
				VData tt = new VData();
				tt.add(mGlobalInput);
				tt.add(mEdorAcceptNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				return false;
			}
		} finally {
			CreatXMLAC creatXMLMyAC = new CreatXMLAC();
			returnXML = creatXMLMyAC.createXML(Content, mEdorAcceptNo, xml);
			if (!"".equals(Content) && null != Content) {
				// 错误日志
				ExeSQL ExeSQL = new ExeSQL();
				String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
						+ SendOperator
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','"
						+ MsgType
						+ "','"
						+ BranchCode
						+ "','"
						+ BatchNo
						+ "','" + mEdorAcceptNo + "')";
				ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
				String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
						+ SendOperator
						+ "','"
						+ xml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+ returnXML
						+ "','"
						+ Content
						+ "','"
						+ MsgType
						+ "','"
						+ BranchCode + "','" + mEdorAcceptNo + "')";
				ExeSQL.execUpdateSQL(insertErrorInsertLogSql);
			} else {
				// 日志
				ExeSQL ExeSQL = new ExeSQL();
				String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
						+ SendOperator
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'1','"
						+ MsgType
						+ "','"
						+ BranchCode
						+ "','"
						+ BatchNo
						+ "','" + mEdorAcceptNo + "')";
				ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
			}
		}

		return true;
	}

	/**
	 * 解析报文，非空校验
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	private boolean getAndCheckData(String xml) throws Exception {

		StringReader reader = new StringReader(xml);
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document doc = tSAXBuilder.build(reader);

		Element rootElement = doc.getRootElement();
		// 报文头
		Element msgHead = rootElement.getChild("MsgHead");
		Element elehead = msgHead.getChild("Item");
		BatchNo = elehead.getChildText("BatchNo");
		String SendDate = elehead.getChildText("SendDate");
		String SendTime = elehead.getChildText("SendTime");
		BranchCode = elehead.getChildText("BranchCode");
		SendOperator = elehead.getChildText("SendOperator");
		MsgType = elehead.getChildText("MsgType");

		// 团单信息
		Element eBody = rootElement.getChild("MsgBody");
		Element eAC = eBody.getChild("ACCF");
		Element lCGrpContInfo = eAC.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		mEdorAcceptNo = eLCGrpContInfo.getChildText("WorkNo");
		mGlobalInput.ManageCom = eLCGrpContInfo.getChildText("ManageCom");
		mGlobalInput.ManageCom = eLCGrpContInfo.getChildText("ManageCom");
//		mGlobalInput.Operator = "pa0001";
		 mGlobalInput.Operator = "BAOQA";
		data.add(mGlobalInput);

		if (null == GrpContNo || "".equals(GrpContNo)) {
			Content = "申请报文中获取团单信息GrpContNo失败。";
			return false;
		}

		// 获取客户号
		String sql1 = "select appntno from lcgrpcont where  grpcontno='" + GrpContNo + " '";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql1SSRS = tExeSQL.execSQL(sql1);
		if (sql1SSRS.getMaxRow() < 1) {
			Content = "此保单号" + GrpContNo + "未查询到客户号！";
			return false;
		}
		String customerno = sql1SSRS.GetText(1, 1);
		System.out.println("查询到客户号为" + customerno);

		// 查询作业表是否生成工单号
		String sql = "select * from LGWork where CustomerNo='" + customerno + "' and WorkNo='" + mEdorAcceptNo + "'";

		// 获取工单号
		SSRS sqlSSRS = tExeSQL.execSQL(sql);

		if (sqlSSRS.getMaxRow() > 0) {
			mEdorAcceptNo = sqlSSRS.GetText(1, 2);
			String str = sqlSSRS.GetText(1, 5);
			//查询工单是否结案
			String sql2 = "select edorstate from lpedorapp where edoracceptno ='" + mEdorAcceptNo + "'";
			SSRS sql2SSRS = tExeSQL.execSQL(sql2);
			if(sql2SSRS.getMaxRow() > 0){
				String str1 = sql2SSRS.GetText(1, 1);
				if("0".equals(str1)){
					Content = "此工单号" + mEdorAcceptNo + "已结案！";
					return false;
				}
			} else if(sql2SSRS.getMaxRow() == 0 && "8".equals(str)){
				Content = "此工单号" + mEdorAcceptNo + "已撤销！";
				return false;
			}
		} else if (sqlSSRS.getMaxRow() == 0){
			Content = "客户号" + customerno + "未生成工单号" + mEdorAcceptNo;
			return false;
		}
		System.out.println("********工单号EdorAcceptNo：" + mEdorAcceptNo);

		LCGrpContSchema sLCGrpContSchema = new LCGrpContSchema();
		sLCGrpContSchema.setGrpContNo(GrpContNo);
		data.add(sLCGrpContSchema);

		// 人员信息
		Element lCInsuredInfo = eAC.getChild("LCInsuredInfo");
		Element elelCInsured = lCInsuredInfo.getChild("Item");

		String GrpName = elelCInsured.getChildText("GrpName");// 投保团体名称

		String Phone = elelCInsured.getChildText("Phone");// 联系电话
		String GrpAddress = elelCInsured.getChildText("GrpAddress");// 投保单位地址
		String GrpZipCode = elelCInsured.getChildText("GrpZipCode");// 邮政编码
		String TaxNo = elelCInsured.getChildText("TaxNo");// 税务登记证号码
		String LegalPersonName = elelCInsured.getChildText("LegalPersonName");// 法人姓名
		String LegalPersonIDNo = elelCInsured.getChildText("LegalPersonIDNo");// 法人身份证号
		String BusinessType = elelCInsured.getChildText("BusinessType");// 行业性质
		String GrpNature = elelCInsured.getChildText("GrpNature");// 企业类型
		String Peoples = elelCInsured.getChildText("Peoples");// 员工总人数 大于 0
		String OnWorkPeoples = elelCInsured.getChildText("OnWorkPeoples");// 在职人数
		String OffWorkPeoples = elelCInsured.getChildText("OffWorkPeoples");// 退休人数
		String OtherPeoples = elelCInsured.getChildText("OtherPeoples");// 其他人员数
		String LinkMan1 = elelCInsured.getChildText("LinkMan1");// 联系人姓名
		String Phone1 = elelCInsured.getChildText("Phone1");// 联系电话
		String Fax1 = elelCInsured.getChildText("Fax1");// 传真
		String E_Mail1 = elelCInsured.getChildText("E_Mail1");// E_Mail1
		String Mobile1 = elelCInsured.getChildText("Mobile1");
		String IDType = elelCInsured.getChildText("IDType");// E_Mail1
		String IDNo = elelCInsured.getChildText("IDNo");//
		String IDStartDate = elelCInsured.getChildText("IDStartDate");//
		String IDEndDate = elelCInsured.getChildText("IDEndDate");//
		String IDLongEffFlag = elelCInsured.getChildText("IDLongEffFlag");//

		if (null == GrpName || "".equals(GrpName)) {
			Content = "申请报文中获取投保团体名称 GrpName 失败。";
			return false;
		}
		if (null == Phone || "".equals(Phone)) {
			Content = "申请报文中获取Phone 投保单位联系电话 失败。";
			return false;
		}
		if (null == GrpAddress || "".equals(GrpAddress)) {
			Content = "申请报文中获取GrpAddress 投保单位地址 失败。";
			return false;
		}
		if (null == GrpZipCode || "".equals(GrpZipCode)) {
			Content = "申请报文中获取GrpZipCode 邮政编码 失败。";
			return false;
		}
		if (null == BusinessType || "".equals(BusinessType)) {
			Content = "申请报文中获取BusinessType 行业性质 失败。";
			return false;
		}
		if (null == GrpNature || "".equals(GrpNature)) {
			Content = "申请报文中获取GrpNature 企业类型 失败。";
			return false;
		}

		/**
		 * 下面四组数据，页面校验不能为空。东软无数据，暂时为空。后续有关人员数量的问题，方便查找
		 */
		// if (null == Peoples || "".equals(Peoples)) {
		// Content = "申请报文中获取Peoples 员工总人数 失败。";
		// return false;
		// }if (null == OnWorkPeoples || "".equals(OnWorkPeoples)) {
		// Content = "申请报文中获取OnWorkPeoples 在职人数 失败。";
		// return false;
		// }
		// if (null == LinkMan1 || "".equals(LinkMan1)) {
		// Content = "申请报文中获取LinkMan1 联系人姓名 失败。";
		// return false;
		// }if (null == Phone1 || "".equals(Phone1)) {
		// Content = "申请报文中获取Phone1 联系人联系电话 失败。";
		// return false;
		// }

		ACLCInsuredInfo aCLCInsuredInfo = new ACLCInsuredInfo();
		aCLCInsuredInfo.setGrpName(GrpName);
		aCLCInsuredInfo.setPhone(Phone);
		aCLCInsuredInfo.setGrpAddress(GrpAddress);
		aCLCInsuredInfo.setGrpZipCode(GrpZipCode);
		aCLCInsuredInfo.setTaxNo(TaxNo);
		aCLCInsuredInfo.setLegalPersonName(LegalPersonName);
		aCLCInsuredInfo.setLegalPersonIDNo(LegalPersonIDNo);
		aCLCInsuredInfo.setBusinessType(BusinessType);
		aCLCInsuredInfo.setGrpNature(GrpNature);
		aCLCInsuredInfo.setPeoples(Peoples);
		aCLCInsuredInfo.setOnWorkPeoples(OnWorkPeoples);
		aCLCInsuredInfo.setOffWorkPeoples(OffWorkPeoples);
		aCLCInsuredInfo.setOtherPeoples(OtherPeoples);
		aCLCInsuredInfo.setLinkMan1(LinkMan1);
		aCLCInsuredInfo.setPhone1(Phone1);
		aCLCInsuredInfo.setFax1(Fax1);
		aCLCInsuredInfo.setE_Mail1(E_Mail1);
		aCLCInsuredInfo.setIDType(IDType);
		aCLCInsuredInfo.setIDNo(IDNo);
		aCLCInsuredInfo.setIDStartDate(IDStartDate);
		aCLCInsuredInfo.setIDEndDate(IDEndDate);
		aCLCInsuredInfo.setIDLongEffFlag(IDLongEffFlag);
		aCLCInsuredInfo.setMobile1(Mobile1);

		data.add(aCLCInsuredInfo);

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(GrpContNo);
		if (!tLCGrpContDB.getInfo()) {
			Content = "传入的团单号错误，核心系统不存在团单:" + GrpContNo;
			return false;
		}

		// 锁表
		MMap tCekMap = null;
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", GrpContNo);
		tTransferData.setNameAndValue("LockNoType", "AC");
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", "360");// 锁定360s

		VData tVData = new VData();
		tVData.add(mGlobalInput);
		tVData.add(tTransferData);

		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tCekMap = tLockTableActionBL.getSubmitMap(tVData, null);

		if (tCekMap == null) {
			Content = "团单:" + GrpContNo + "正在进行无名单删除，请不要重复请求";
			return false;
		}

		VData tVdata1 = new VData();
		tVdata1.add(tCekMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVdata1, "")) {
			Content = "团单:" + GrpContNo + "正在进行无名单删除，请不要重复请求";
			return false;
		}

		String str = "select 1 from LCCont  where appflag='1' and polType = '1'  and conttype='2' and grpcontno='"
				+ GrpContNo + "' ";
		String res = new ExeSQL().getOneValue(str);
		if (null == res || "".equals(str)) {
			Content = "非无名单保单不能申请无名单删除";
			return false;
		}

		LCGrpContDB tLCGrpContDB1 = new LCGrpContDB();
		tLCGrpContDB1.setGrpContNo(GrpContNo);
		LCGrpContSet tLCGrpContSet = tLCGrpContDB1.query();
		if (tLCGrpContSet.size() == 0) {
			Content = "查询团单信息失败！";
			return false;
		}
		LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
		mLCGrpContSchema = tLCGrpContSet.get(1);
		LCGrpContAppntNo = mLCGrpContSchema.getAppntNo();
		LCGrpContManageCom = mLCGrpContSchema.getManageCom();
		PrtNo = mLCGrpContSchema.getPrtNo();
		System.out.println("PrtNo*************************************************" + PrtNo);
		return true;
	}

	/**
	 * 
	 * 生成工单 添加保全项目
	 * 
	 * @return
	 */
	private boolean ACdetail() {

		// 添加保全项 前台
		if (!addRecord()) {
			System.out.println("=======添加保全项失败");

			return false;
		}
		// 添加保全项 后台
		if (!grpEdorItemSave()) {
			System.out.println("=======添加保全项失败222222");

			return false;
		}

		if (!domake()) {
			return false;
		}
		// 保全项目明细
		if (!checkSave()) {
			System.out.println("=======保全项目明细失败");

			return false;
		}
		// 保全理算
		if (!edorAppConfirm()) {
			System.out.println("=======保全理算失败");

			return false;
		}
		// 保全确认 前台
		if (!edorConfirm()) {

			System.out.println("=======保全确认前台失败");
			return false;
		}
		// 保全确认 后台
		if (!GEdorConfirmSubmit()) {
			System.out.println("=======保全确认后台失败");

			return false;
		}

		return true;
	}

	/**
	 * 
	 * 添加保全项目前台校验
	 * 
	 */
	public boolean addRecord() {

		ExeSQL tExeSQL = new ExeSQL();
		String sql = "";
		SSRS sqlSSRS = null;
		// 判断保单是否有万能险种
		if (hasULIRisk(GrpContNo)) {
			// 解约保全生效日期必须大于或者等于保单最后一次月结日期
			sql = "select edoracceptno from lpgrpedoritem where grpcontno='"
					+ GrpContNo
					+ "' and exists ( "
					+ " select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0' )  ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0) {
				System.out.println("该团险万能的保单下存在其他未结案的保全项目，不能继续操作！");
				Content = "该团险万能的保单下存在其他未结案的保全项目，不能继续操作！";
				return false;
			}
		}

		/*
		 * if (mFormData.getEdorType() == null ||
		 * "".equals(mFormData.getEdorType())) {
		 * System.out.println("保全项目类型不能为空！"); Content = "保全项目类型不能为空！"; return
		 * false; }
		 */
		if (!checkGrpEdorType()) {
			return false;
		}
		// 做过无名单增减人的保单不能犹豫期退保

		// 最后一次续期交费后，做过无名单增减人的保单不能退保

		sql = "select state from lcgrpbalplan where grpcontno = '" + GrpContNo
				+ "'";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0 && sqlSSRS.GetText(1, 1) != "0") {
			System.out.println("该集体合同目前正在进行定期结算!不能进行保全操作!");
			Content = "该集体合同目前正在进行定期结算!不能进行保全操作!";
			return false;
		}
		sql = "select edorno from lpgrpedoritem a where grpcontno = '"
				+ GrpContNo
				+ "' and edortype='TZ' and "
				+ " exists ( select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0' )";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0) {
			System.out.println("团体万能增人不能和其他保全项目一起操作!");
			Content = "团体万能增人不能和其他保全项目一起操作!";
			return false;
		}
		String strSQL = "select EdorAcceptNo, EdorNo, EdorType, GrpContNo, "
				+ "(select CustomerNo from LCGrpAppnt "
				+ " where GrpContNo = LPGrpEdorItem.GrpContNo), "
				+ " EdorValiDate, " + "	case EdorState "
				+ "		when '1' then '录入完毕' " + "		when '2' then '理算确认' "
				+ "		when '3' then '未录入' " + "		when '4' then '试算成功' "
				+ "		when '0' then '保全确认' " + "	end " + "from LPGrpEdorItem "
				+ "where EdorAcceptNo='" + mEdorAcceptNo + "' "
				+ "	and EdorNo='" + mEdorAcceptNo + "' ";
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		if (strSQLSSRS.getMaxRow() > 0) {
			for (int h = 1; h <= strSQLSSRS.getMaxRow(); h++) {
				if (strSQLSSRS.GetText(h, 3) == "JM") {
					System.out.println("激活卡客户资料变更不能和其他保全项目一起操作!");
					Content = "激活卡客户资料变更不能和其他保全项目一起操作!";
					return false;
				}
				if (strSQLSSRS.GetText(h, 3) == "RS") {
					System.out.println("保单暂停功能不能和其他保全项目一起操作!");
					Content = "保单暂停功能不能和其他保全项目一起操作!";
					return false;
				}
				if (strSQLSSRS.GetText(h, 3) == "RR") {
					System.out.println("保单恢复功能不能和其他保全项目一起操作!");
					Content = "保单恢复功能不能和其他保全项目一起操作!";
					return false;
				}
			}
		}

		// 校验保全生效日是否在暂停期

		// add by 特需医疗险种的团单增减人不可同一个工单来做

		// 添加对 AC不可和NI、ZT同时做的校验

		sql = "select 1 from lpgrpedoritem where  edorno='" + mEdorAcceptNo
				+ "'";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0) {
			System.out.println("其他保全项目不可以和投保单位资料变更一起做。");
			Content = "其他保全项目不可以和投保单位资料变更一起做。";
			return false;

		}
		sql = "select 1 from lpgrpedoritem where edortype='AC' and edorno='"
				+ mEdorAcceptNo + "'";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0) {
			System.out.println("投保单位资料变更不可以和其他保全一起做。");
			Content = "投保单位资料变更不可以和其他保全一起做。";
			return false;

		}

		sql = "select 1 from lpgrpedoritem a where grpcontno='"
				+ GrpContNo
				+ "' and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and edortype='TY' ";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0) {
			System.out.println("该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!");
			Content = "该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!";
			return false;
		}

		// 添加对 AC不可和NI、ZT同时做的校验，杨天政 20110711

		// add by 团体公共保额分配的生效日期必须为当天

		/*
		 * 保单下有未结案理赔案件或者有未回销的预付赔款（保单预付赔款余额不为0）时
		 * 保全不能进行结余返还，待理赔案件处理完毕，没有未回销预付赔款时才能做结余返还。
		 */

		return true;

	}
	
	// 判断团单是否有万能险种
	public boolean hasULIRisk(String grpContNo) {
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"
				+ GrpContNo + "' and RiskType4='4' with ur ";
		SSRS sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0) {
			return true;
		}
		return false;
	}

	public boolean checkGrpEdorType() {
		ExeSQL tExeSQL = new ExeSQL();

		String checkSQL = "select distinct b.EdorCode, b.EdorName from LMRiskEdoritem  a, LMEdorItem b where a.edorCode ="
				+ " b.edorCode and b.edorcode != 'XB'   and a.riskCode in  (select riskCode from LCGrpPol where "
				+ "grpContNo = '"
				+ GrpContNo
				+ "')  and (b.edorTypeFlag != 'N' or b.edorTypeFlag is null)order by EdorCode";
		SSRS checkSQLSSRS = tExeSQL.execSQL(checkSQL);

		System.out.println("================可添加:" + checkSQLSSRS.getMaxRow()
				+ "个保全项");
		int total = 0;
		for (int i = 1; i <= checkSQLSSRS.getMaxRow(); i++) {

			if ("AC".equals(checkSQLSSRS.GetText(i, 1))) {
				total = 1;
			}
		}
		if (total <= 0) {
			Content = "您的保单投保,有险种的【AC】保全功能暂未上线，无法添加保全项目！";
			return false;
		}
		return true;
	}

	/**
	 * 添加保全项后台
	 * 
	 * @return
	 */
	public boolean grpEdorItemSave() {

		// 添加保全项目
		System.out
				.println("======================================添加保全项===================================");
		LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
		mLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		mLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		mLPGrpEdorMainSchema.setGrpContNo(GrpContNo);
		mLPGrpEdorMainSchema.setEdorAppDate(mCurrDate);
		mLPGrpEdorMainSchema.setEdorValiDate(mCurrDate);
		LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		mLPGrpEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorType("AC");
		mLPGrpEdorItemSchema.setGrpContNo(GrpContNo);
		mLPGrpEdorItemSchema.setEdorAppDate(mCurrDate);
		mLPGrpEdorItemSchema.setEdorValiDate(mCurrDate);
		mLPGrpEdorItemSchema.setManageCom(LCGrpContManageCom);
		mLPGrpEdorItemSet.add(mLPGrpEdorItemSchema);

		VData tVData4 = new VData();
		tVData4.add(mLPGrpEdorItemSet);
		tVData4.add(mLPGrpEdorMainSchema);
		tVData4.add(mGlobalInput);
		GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();
		if (!tGrpEdorItemUI.submitData(tVData4, "INSERT||GRPEDORITEM")) {
			Content = "添加保全项目失败：" + tGrpEdorItemUI.mErrors.getFirstError();
			String error = tGrpEdorItemUI.mErrors.getFirstError();
			System.out.println("添加保全项目失败:" + error);
			return false;
		}

		return true;
	}

	/**
	 * 当保全有单独逻辑的时候重写次方法
	 */
	public boolean domake() {
		return true;
	}

	/**
	 * 保全项目明细 保存按钮
	 * 
	 * @return
	 */
	public boolean checkSave() {

		/*
		 * 
		 * 保全项目明细 ---GEdorTypeACInput.jsp 查询地址信息
		 */

		System.out
				.println("====================================添加保全项明细============================");
		String GrpAddressNo = "";
		String sqlAddressNo = "select AddressNo from LCGrpAppnt "
				+ "where GrpContNo = '" + GrpContNo + "'";
		SSRS ssrsAddressNo = new ExeSQL().execSQL(sqlAddressNo);
		if (ssrsAddressNo.getMaxRow() > 0) {
			GrpAddressNo = ssrsAddressNo.GetText(1, 1);
		} else {
			Content = "团单号：" + GrpContNo + "下没有地址信息";
			return false;
		}
		// 保存 修改的数据
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		LPGrpAppntSchema tLPGrpAppntSchema = new LPGrpAppntSchema();
		LPGrpSchema tLPGrpSchema = new LPGrpSchema();
		LPGrpAddressSchema tLPGrpAddressSchema = new LPGrpAddressSchema();
		PGrpEdorACDetailUI tPGrpEdorACDetailUI = new PGrpEdorACDetailUI();

		// 后面要执行的动作：添加，修改
		ACLCInsuredInfo lCInsuredInfo = (ACLCInsuredInfo) data
				.getObjectByObjectName("ACLCInsuredInfo", 0);
		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		tLPGrpEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorType("AC");
		tLPGrpEdorItemSchema.setGrpContNo(GrpContNo);

		// 团单投保人信息 LCGrpAppnt
		tLPGrpAppntSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpAppntSchema.setEdorType("AC");
		tLPGrpAppntSchema.setGrpContNo(GrpContNo); // 集体投保单号码
		tLPGrpAppntSchema.setCustomerNo(LCGrpContAppntNo); // 客户号码
		tLPGrpAppntSchema.setPrtNo(PrtNo); // 印刷号码
		tLPGrpAppntSchema.setName(lCInsuredInfo.getGrpName());
		tLPGrpAppntSchema.setPostalAddress(lCInsuredInfo.getGrpAddress());
		tLPGrpAppntSchema.setZipCode(lCInsuredInfo.getGrpZipCode());
		tLPGrpAppntSchema.setAddressNo(GrpAddressNo);
		tLPGrpAppntSchema.setPhone(lCInsuredInfo.getPhone());
		tLPGrpAppntSchema.setTaxNo(lCInsuredInfo.getTaxNo());
		tLPGrpAppntSchema
				.setLegalPersonName(lCInsuredInfo.getLegalPersonName()); // 法人姓名
		tLPGrpAppntSchema
				.setLegalPersonIDNo(lCInsuredInfo.getLegalPersonIDNo()); // 法人身份证号

		// 添加联系人相关信息
		tLPGrpAppntSchema.setIDType(lCInsuredInfo.getIDType());
		tLPGrpAppntSchema.setIDNo(lCInsuredInfo.getIDNo());
		tLPGrpAppntSchema.setIDStartDate(lCInsuredInfo.getIDStartDate());
		tLPGrpAppntSchema.setIDEndDate(lCInsuredInfo.getIDEndDate());
		tLPGrpAppntSchema.setIDLongEffFlag(lCInsuredInfo.getIDLongEffFlag());

		// 团体客户信息 LPGrp
		tLPGrpSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpSchema.setEdorType("AC");
		tLPGrpSchema.setCustomerNo(LCGrpContAppntNo); // 客户号码
		tLPGrpSchema.setGrpName(lCInsuredInfo.getGrpName()); // 单位名称
		tLPGrpSchema.setGrpNature(lCInsuredInfo.getGrpNature()); // 单位性质
		tLPGrpSchema.setBusinessType(lCInsuredInfo.getBusinessType()); // 行业类别
		tLPGrpSchema.setPeoples(lCInsuredInfo.getPeoples()); // 总人数
		tLPGrpSchema.setOnWorkPeoples(lCInsuredInfo.getOnWorkPeoples()); // 在职人数
		tLPGrpSchema.setOffWorkPeoples(lCInsuredInfo.getOffWorkPeoples()); // 退休人数
		tLPGrpSchema.setOtherPeoples(lCInsuredInfo.getOtherPeoples()); // 其它人员数
		tLPGrpSchema.setPhone(lCInsuredInfo.getPhone()); // 总机

		// 团体客户地址 LCGrpAddress
		tLPGrpAddressSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpAddressSchema.setEdorType("AC");
		tLPGrpAddressSchema.setCustomerNo(LCGrpContAppntNo); // 客户号码
		tLPGrpAddressSchema.setAddressNo(GrpAddressNo); // 地址号码
		tLPGrpAddressSchema.setGrpAddress(lCInsuredInfo.getGrpAddress()); // 单位地址
		tLPGrpAddressSchema.setGrpZipCode(lCInsuredInfo.getGrpZipCode()); // 单位邮编
		// tLPGrpAddressSchema.setPostalProvince(request.getParameter("Province"));
		// tLPGrpAddressSchema.setPostalCity(request.getParameter("City"));
		// tLPGrpAddressSchema.setPostalCounty(request.getParameter("County"));
		// tLPGrpAddressSchema.setDetailAddress(request.getParameter("appnt_DetailAdress"));
		// 保险联系人一
		tLPGrpAddressSchema.setLinkMan1(lCInsuredInfo.getLinkMan1());
		tLPGrpAddressSchema.setPhone1(lCInsuredInfo.getPhone1());
		tLPGrpAddressSchema.setFax1(lCInsuredInfo.getFax1());
		tLPGrpAddressSchema.setE_Mail1(lCInsuredInfo.getE_Mail1());//
		tLPGrpAddressSchema.setMobile1(lCInsuredInfo.getMobile1());

		try {
			// 准备传输数据 VData
			VData tVData = new VData();

			// 保存个人保单信息(保全)
			tVData.addElement("MOD");
			tVData.addElement(mGlobalInput);
			tVData.addElement(tLPGrpEdorItemSchema);
			tVData.addElement(tLPGrpAppntSchema);
			tVData.addElement(tLPGrpSchema);
			tVData.addElement(tLPGrpAddressSchema);
			tPGrpEdorACDetailUI.submitData(tVData, "INSERT||EDORAC");
		} catch (Exception ex) {
			Content = "INSERT||EDORAC" + "失败，原因是:"
					+ tPGrpEdorACDetailUI.mErrors.getFirstError();
			return false;
		}

		return true;
	}

	/**
	 * 保全理算
	 * 
	 * @return
	 */
	public boolean edorAppConfirm() {
		// 保全理算
		// 集体批改信息
		System.out
				.println("=======================================保全理算============================");
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setGrpContNo(GrpContNo);
		tLPGrpEdorMainSchema.setEdorValiDate(mCurrDate);
		tLPGrpEdorMainSchema.setEdorAppDate(mCurrDate);

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLPGrpEdorMainSchema);
		PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
		if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM")) {// tPGrpEdorAppConfirmUI
			Content = "保全理算失败！原因是："
					+ tPGrpEdorAppConfirmUI.getError().getErrContent();
			return false;
		}

		return true;
	}

	/**
	 * 保全确认 前台
	 * 
	 * @return
	 */
	public boolean edorConfirm() {

		// 前台校验
		ExeSQL tExeSQL = new ExeSQL();
		String strSql = "select statusno from lgwork where workno = '"
				+ mEdorAcceptNo + "' ";
		SSRS arrResult = tExeSQL.execSQL(strSql);
		if ("5".equals(arrResult.GetText(1, 1))) {
			Content = "保全已经确认，请不要重复确认！";
			System.out.println(Content);
			return false;
		}
		if ("88".equals(arrResult.GetText(1, 1))) {
			Content = "保全审批意见为[不同意]，请重新录入保全明细或撤销保全工单！";
			System.out.println(Content);
			return false;
		}

		String findSql = "select serialno from LCUrgeVerifyLog where serialno='"
				+ mEdorAcceptNo + "' ";
		SSRS arrResult_1 = tExeSQL.execSQL(findSql);
		if (arrResult_1.MaxRow > 0) {
			Content = "正在保全确认或者重复理算操作，请不要再次点击保全确认！";
			System.out.println(Content);
			return false;
		}
		String strSql2 = "select GetMoney from LPGrpEdorMain where EdorAcceptNo = '"
				+ mEdorAcceptNo + "'";
		SSRS strSqlSSRS = tExeSQL.execSQL(strSql2);
		if (strSqlSSRS.getMaxRow() < 0) {
			Content = "查询保全主表信息出错！";
			System.out.println(Content);
			return false;
		}
		return true;

	}

	/**
	 * 保全确认 后台
	 * 
	 * @return
	 */
	public boolean GEdorConfirmSubmit() {
		// 里算确认
		System.out.println("=========================================保全确认=============================");
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(
				mEdorAcceptNo, "YE");
		tSpecialData.add("CustomerNo", LCGrpContAppntNo);
		tSpecialData.add("AccType", "");
		tSpecialData.add("OtherType", "3");
		tSpecialData.add("OtherNo", mEdorAcceptNo);
		tSpecialData.add("DestSource", "");
		tSpecialData.add("ContType", "2");
		tSpecialData.add("Fmtransact2", "NOTUSEACC");

		LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
		tLCAppAccTraceSchema.setCustomerNo(LCGrpContAppntNo);
		tLCAppAccTraceSchema.setAccType("");
		tLCAppAccTraceSchema.setOtherType("3");// 团单
		tLCAppAccTraceSchema.setOtherNo(mEdorAcceptNo);
		tLCAppAccTraceSchema.setDestSource("");
		tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);

		VData tVData = new VData();
		tVData.add(tLCAppAccTraceSchema);
		tVData.add(tSpecialData);
		tVData.add(mGlobalInput);
		PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
		if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param")) {
			Content = "处理帐户余额失败！";
			System.out.println("保全后台确认"+Content);
			return false;
		} else {

			BqConfirmUI tBqConfirmUI = new BqConfirmUI(mGlobalInput,
					mEdorAcceptNo, BQ.CONTTYPE_G, "");
			if (!tBqConfirmUI.submitData()) {
				Content = tBqConfirmUI.getError();
				return false;
			}
		}

		return true;
	}

//	/**
//	 * 
//	 * 撤销工单
//	 * 
//	 * @param edorstate
//	 * @param transact
//	 * @return
//	 */
//	private boolean cancelEdorItem(String edorstate, String transact) {
//		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
//		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
//		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
//
//		TransferData tTransferData = new TransferData();
//		VData tVData = new VData();
//		tVData.addElement(mGlobalInput);
//
//		if ("G&EDORAPP".equals(transact)) {
//			tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
//			tLPEdorAppSchema.setEdorState(edorstate);
//
//			String delReason = "";
//			String reasonCode = "002";
//
//			tTransferData.setNameAndValue("DelReason", delReason);
//			tTransferData.setNameAndValue("ReasonCode", reasonCode);
//			tVData.addElement(tLPEdorAppSchema);
//
//			tVData.addElement(tTransferData);
//		} else if ("G&EDORMAIN".equals(transact)) {
//			tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
//			tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
//			tLPGrpEdorMainSchema.setEdorState(edorstate);
//			tLPGrpEdorMainSchema.setGrpContNo(GrpContNo);
//			String delReason = "";
//			String reasonCode = "002";
//			System.out.println(delReason);
//
//			tTransferData.setNameAndValue("DelReason", delReason);
//			tTransferData.setNameAndValue("ReasonCode", reasonCode);
//			tVData.addElement(tLPGrpEdorMainSchema);
//			tVData.addElement(tTransferData);
//		}
//		try {
//			tPGrpEdorCancelUI.submitData(tVData, transact);
//		} catch (Exception e) {
//
//			e.printStackTrace();
//			Content = "撤销工单失败" + e;
//			return false;
//		}
//		return true;
//	}

	public String getReturnXML() {
		return returnXML;
	}

}
