package com.preservation.newCF.TFCF;

import org.jdom.Element;

import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * 保全接口特权复效CF
 * 
 * @author 姜士杰 2018-12-13
 *
 */

public class InterfaceTFCF extends InterfaceBaseCF {

	public boolean deal(String xml) {

		this.pxml = xml;

		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}

		return dealc();
	}

	// 逻辑处理
	public boolean dealc() {

		DealTFCF dealTFCF = new DealTFCF();

		if (!dealTFCF.deal(data)) {
			errorValue = dealTFCF.getErrorValue();
			WorkNo = dealTFCF.getEdorAcceptNo();
			return false;
		}
		WorkNo = dealTFCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}
	
	public void getkDatabody(Element rootElement) {

		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element ZF = rootElement.getChild("TFCF");
		
		// 客户号
		Element msgCustomer = ZF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = ZF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = ZF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
		data.add(tFormData);
	}

}
