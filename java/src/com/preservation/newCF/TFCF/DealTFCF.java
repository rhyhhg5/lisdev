package com.preservation.newCF.TFCF;

import com.preservation.newCF.DealBaseCF;
import com.sinosoft.lis.bq.GrpEdorTFDetailUI;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealTFCF extends DealBaseCF {

	// 项目明细 保存
	public boolean checkSave() {

		// 传入复效日期
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("edorAppDate", null);
		System.out.print("\n before 传入保全项目");

		// 传入保全项目
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorAcceptNo(EdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
		tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		tLPGrpEdorItemSchema.setEdorType(mFormData.getEdorType());

		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(GI);
		tVData.add(tLPGrpEdorItemSchema);
		tVData.add(tTransferData);

		GrpEdorTFDetailUI tGrpEdorTFDetailUI = new GrpEdorTFDetailUI();
		if (!tGrpEdorTFDetailUI.submitData(tVData, "")) {
			System.out.println("Submit Failed! " + tGrpEdorTFDetailUI.mErrors.getErrContent());
			Content = "保存失败，原因是:" + tGrpEdorTFDetailUI.mErrors.getFirstError();
			return false;
		} else {
			Content = "保存成功";
			return true;
		}
	}

}
