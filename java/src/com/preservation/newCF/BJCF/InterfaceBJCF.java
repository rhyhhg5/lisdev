package com.preservation.newCF.BJCF;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.BJ.InterfaceBJ;
import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * 保全接口结余返还CF
 * 
 * @author 姜士杰 2018-12-13
 *
 */

public class InterfaceBJCF extends InterfaceBaseCF {

	public boolean deal(String xml) {

		this.pxml = xml;

		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}

		return dealc();
	}

	// 逻辑处理
	public boolean dealc() {

		DealBJCF dealBJCF = new DealBJCF();

		if (!dealBJCF.deal(data)) {

			errorValue = dealBJCF.getErrorValue();
			WorkNo = dealBJCF.getEdorAcceptNo();
			return false;
		}

		WorkNo = dealBJCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	// 解析报文body部分
	public void getkDatabody(Element rootElement) {

		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element BJ = rootElement.getChild("BJCF");

		// 客户号
		Element msgCustomer = BJ.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = BJ.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		//
		Element xFormData = BJ.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setEdorValue(Double.parseDouble(xxFormData.getChildText("EdorValue")));

		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

		data.add(tFormData);
	}
	
public static void main(String[] args) {
		
		InterfaceBJ tInterfaceBJ = new InterfaceBJ();
		
		try {
			
			InputStream pIns = new FileInputStream("D:\\JiangS丶杰\\Items1\\接口文档\\保全接口\\BQBJ结余返还.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceBJ.deal(mInXmlStr);
			System.out.println(tInterfaceBJ.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

}
