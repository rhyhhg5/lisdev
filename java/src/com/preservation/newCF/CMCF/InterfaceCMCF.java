package com.preservation.newCF.CMCF;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.preservation.CM.CreatXMLCM;
import com.preservation.CM.DealCM;
import com.preservation.CM.InterfaceCM;
import com.preservation.CM.obj.LcinsuredObj;
import com.preservation.newCF.CreateXMLBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * 保全接口客户资料变更CF
 * 
 * @author 姜士杰 2018-12-13
 *
 */

public class InterfaceCMCF {

	String errorValue; // 错误信息
	VData data = new VData();
	String WorkNo;
	String cValiDate;

	public boolean deal(String xml) throws ParseException {

		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}

		// 处理逻辑
		DealCMCF dealCMCF = new DealCMCF();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);

		if (!dealCMCF.deal(data)) {
			errorValue = dealCMCF.getContent();
			WorkNo = dealCMCF.getEdorAcceptNo();
			String sql = "insert into ErrorInsertLog "
					+ "(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,"
					+ "Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,"
					+ "transactionBatch ,transactionDescription ,transactionAddress ) "
					+ "values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'" + GI.Operator + "','" + xml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), "
					+ "to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), "
					+ "'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'" + returnXML(xml) + "','中间状态','" + errorValue
					+ "','" + mMessHead.getMsgType() + "','" + mMessHead.getBranchCode() + "','"
					+ mMessHead.getBatchNo() + "','交易描述','交易地址')";
			tExeSQL.execUpdateSQL(sql);
			return false;
		}
		WorkNo = dealCMCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}
	
	/**
	 * 得到报文数据 解析报文
	 * 
	 * @throws ParseException
	 * 
	 * @throws Exception
	 */
	public boolean getkData(String xml) throws ParseException {
		try {
			
			System.out.println("=========== star 解析报文 =========");
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");

			MessHead head = new MessHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			// 作为页面session值
			GlobalInput GI = new GlobalInput();
			
			Element body = rootElement.getChild("MsgBody");
			Element CM = body.getChild("CMCF");
			
			// 客户号
			Element msgCustomer = CM.getChild("CustomerInfo");
			Element eCustomerInfo = msgCustomer.getChild("Item");
			String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
			String WorkNo = eCustomerInfo.getChildText("WorkNo");
			GI.ComCode = eCustomerInfo.getChildText("ManageCom");;
			GI.ManageCom = eCustomerInfo.getChildText("ManageCom");;
//			GI.Operator = "pa0001";
			GI.Operator = "BAOQA";
			data.add(GI);

			CustomerInfo iCustomerInfo = new CustomerInfo();
			iCustomerInfo.setCustomerNo(CustomerNo);
			iCustomerInfo.setWorkNo(WorkNo);
			data.add(iCustomerInfo);

			// 团单信息
			Element lCGrpContInfo = CM.getChild("LCGrpContInfo");
			Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
			String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
			String GrpName = eLCGrpContInfo.getChildText("GrpName");

			LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
			iLCGrpContInfo.setGrpContNo(GrpContNo);
			iLCGrpContInfo.setGrpName(GrpName);
			data.add(iLCGrpContInfo);

			//
			Element xFormData = CM.getChild("FormData");
			Element xxFormData = xFormData.getChild("Item");
			FormData tFormData = new FormData();
			tFormData.setEdorType(xxFormData.getChildText("EdorType")); 
			tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
			cValiDate = xxFormData.getChildText("EdorValiDate");
			tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
			tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
			tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
			tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
			tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
			tFormData.setRemark(xxFormData.getChildText("Remark"));

			tFormData.setPayMode(xxFormData.getChildText("PayMode"));
			tFormData.setPayDate(xxFormData.getChildText("PayDate"));
			tFormData.setEndDate(xxFormData.getChildText("EndDate"));
			tFormData.setBank(xxFormData.getChildText("Bank"));
			tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

			Element xLcinsured = CM.getChild("Lcinsured");
			LcinsuredObj tLcinsuredObj = new LcinsuredObj();

			List<LcinsuredObj> LcinsuredObjSet = new ArrayList<LcinsuredObj>();
			List<Element> itemList = xLcinsured.getChildren("Item");
			for (int i = 0; i < itemList.size(); i++) {
				Element item = itemList.get(i);
				LcinsuredObj sLcinsuredObj = new LcinsuredObj();
				sLcinsuredObj.setOldName(item.getChildText("OldName"));
				
				sLcinsuredObj.setOldIDNo(item.getChildText("OldIDNo"));
				sLcinsuredObj.setName(item.getChildText("Name"));
				sLcinsuredObj.setSex(item.getChildText("Sex"));
				sLcinsuredObj.setBirthday(item.getChildText("Birthday"));
				sLcinsuredObj.setIDType(item.getChildText("IDType"));
				sLcinsuredObj.setIDNo(item.getChildText("IDNo"));
				// SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
				sLcinsuredObj.setIDStartDate(item.getChildText("IDStartDate"));
				sLcinsuredObj.setIDEndDate(item.getChildText("IDEndDate"));
				sLcinsuredObj.setOccupationCode(item.getChildText("OccupationCode"));
				sLcinsuredObj.setOccupationType(item.getChildText("OccupationType"));
				sLcinsuredObj.setMarriage(item.getChildText("Marriage"));
				sLcinsuredObj.setRelation(item.getChildText("Relation"));
				sLcinsuredObj.setRelationToAppnt(item.getChildText("RelationToAppnt"));
				sLcinsuredObj.setInsuredState(item.getChildText("InsuredState"));
				sLcinsuredObj.setNationality(item.getChildText("nationality"));
				sLcinsuredObj.setPosition(item.getChildText("position"));
				sLcinsuredObj.setSalary(item.getChildText("salary"));
				sLcinsuredObj.setGrpInsuredPhone(item.getChildText("GrpInsuredPhone"));
				sLcinsuredObj.setBankCode(item.getChildText("BankCode"));
				sLcinsuredObj.setAccName(item.getChildText("AccName"));
				sLcinsuredObj.setBankAccNo(item.getChildText("BankAccNo"));
				LcinsuredObjSet.add(sLcinsuredObj);

				// tLcinsuredObj.getLcinsuredObjSet().add(sLcinsuredObj);
				tLcinsuredObj.setLcinsuredObjSet(LcinsuredObjSet);
			}

			data.add(tFormData);
			data.add(tLcinsuredObj);

		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	//返回报文
	public String returnXML(String xml) {
		CreateXMLBaseCF createXMLCM = new CreateXMLBaseCF();
		String returnxml = null;
		try {
			returnxml = createXMLCM.createXML(data, errorValue, WorkNo,
					cValiDate, xml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnxml;

	}
	
	public static void main(String[] args) {
		InterfaceCMCF tInterfaceCM = new InterfaceCMCF();

		try {
			InputStream pIns = new FileInputStream("D:\\JiangS丶杰\\Items1\\接口文档\\保全接口CF\\BG.txt");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(),
					"gbk");
			System.out.println(mInXmlStr);
			tInterfaceCM.deal(mInXmlStr);
			System.out.println(tInterfaceCM.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
