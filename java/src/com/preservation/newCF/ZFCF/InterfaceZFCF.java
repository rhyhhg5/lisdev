package com.preservation.newCF.ZFCF;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.ZF.InterfaceZF;
import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;

public class InterfaceZFCF extends InterfaceBaseCF {

	/**
	 * 逻辑处理
	 */
	public boolean dealc() {
		DealZFCF dealZFCF = new DealZFCF();
		if (!dealZFCF.deal(data)) {
			errorValue = dealZFCF.getErrorValue();
			WorkNo = dealZFCF.getEdorAcceptNo();
			return false;
		}
		WorkNo = dealZFCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement) {

		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element ZFCF = rootElement.getChild("ZFCF");
		// 客户号

		Element msgCustomer = ZFCF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = ZFCF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = ZFCF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPayMode("1");
		tFormData.setPayDate(xxFormData.getChildText("EdorValiDate"));
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));

		/*
		 * tFormData.setGrpPolNo(xxFormData.getChildText("grpPolNo"));
		 * tFormData.setGroupMoney(xxFormData.getChildText("groupMoney"));
		 */
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));

		data.add(tFormData);

	}

	public static void main(String[] args) {
		InterfaceZF tInterfaceZG = new InterfaceZF();

		try {
			InputStream pIns = new FileInputStream("C:/Users/user/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceZG.deal(mInXmlStr);
			System.out.println(tInterfaceZG.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
