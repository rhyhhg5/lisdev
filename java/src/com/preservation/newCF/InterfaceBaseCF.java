package com.preservation.newCF;

import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.preservation.BQBase.CreateXMLBase;
import com.preservation.BQBase.DealBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.utility.VData;

public class InterfaceBaseCF {

	public String errorValue; // 错误信息
	public VData data = new VData();
	public String WorkNo;
	public String cValiDate;
	public String pxml;// 请求报文

	public boolean deal(String xml) {
		this.pxml = xml;
		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}
		return dealc();

	}
	
	//逻辑处理
	public boolean dealc(){
		DealBase dealZF  = new DealBase();
		if (!dealZF.deal(data)) {
			errorValue = dealZF.getErrorValue();
			WorkNo = dealZF.getEdorAcceptNo();
			return false;
		}
		WorkNo = dealZF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}
	
	//解析报文头
	public boolean getkData(String xml) {
		try {
			
			System.out.println("============= star getkData()");
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");

			MessHead head = new MessHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			getkDatabody(rootElement.getChild("MsgBody"));
			
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	//解析报文体
	public void getkDatabody(Element rootElement ){

		Element ZF = rootElement.getChild("ZF");
		// 客户号
		
		Element msgCustomer = ZF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = ZF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = ZF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		
	/*	tFormData.setGrpPolNo(xxFormData.getChildText("grpPolNo"));
		tFormData.setGroupMoney(xxFormData.getChildText("groupMoney"));*/
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		
		data.add(tFormData);

	}
	
	public String returnXML(String xml) throws Exception{
		CreateXMLBaseCF createXML= new CreateXMLBaseCF();
		String returnxml = createXML.createXML(data, errorValue, WorkNo, cValiDate, xml);
		return returnxml;
		
	}

}
