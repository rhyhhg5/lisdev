package com.preservation.newCF.BBCF;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 保全接口保费回补CF
 * @author 姜士杰 2018-12-13
 *
 */

public class InterfaceBBCF extends InterfaceBaseCF  {
	
	public boolean deal(String xml) {

		this.pxml = xml;

		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}

		return dealc();
	}
	
	//保费回补逻辑处理
	public boolean dealc() {

		DealBBCF dealBBCF = new DealBBCF();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);

		if (!dealBBCF.deal(data)) {

			errorValue = dealBBCF.getErrorValue();
			WorkNo = dealBBCF.getEdorAcceptNo();

			try {
				String sql = "insert into ErrorInsertLog "
						+ "(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,"
						+ "Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,"
						+ "transactionBatch ,transactionDescription ,transactionAddress ) "
						+ "values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'" + GI.Operator + "','" + pxml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), "
						+ "to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), "
						+ "'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'" + returnXML(pxml) + "','中间状态','"
						+ errorValue + "','" + mMessHead.getMsgType() + "','" + mMessHead.getBranchCode() + "','"
						+ mMessHead.getBatchNo() + "','交易描述','交易地址')";
				tExeSQL.execUpdateSQL(sql);
			} catch (Exception e) {

			}
			return false;
		}

		errorValue = "";
		WorkNo = dealBBCF.getEdorAcceptNo();
		return true;
	}
	
	//解析报文body部分
	public void getkDatabody(Element rootElement) {

		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element BBCF = rootElement.getChild("BBCF");
		// 客户号
		Element msgCustomer = BBCF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = BBCF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = BBCF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		System.out.println("===================================" + xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate")); // 保全生效日期
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo")); // 优先级别
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo")); // 子业务类型
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo")); // 受理途径
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		tFormData.setCost(xxFormData.getChildText("cost"));
		tFormData.setReason_tb(xxFormData.getChildText("reason_tb"));
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

		tFormData.setEdorValue(Double.parseDouble(xxFormData.getChildText("EdorValue")));
		data.add(tFormData);
	}
	
	public static void main(String[] args) {
		
		InterfaceBBCF tInterfaceBBCF = new InterfaceBBCF();

		try {
			InputStream pIns = new FileInputStream("D:\\JiangS丶杰\\Items1\\接口文档\\保全接口\\BQBB保费回补.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceBBCF.deal(mInXmlStr);
			tInterfaceBBCF.returnXML(mInXmlStr);
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
