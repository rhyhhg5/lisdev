package com.preservation.newCF.WSCF;

import java.io.IOException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.cbsws.obj.EdorItemInfo;
import com.preservation.newCF.GEdorCancelBQ;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.FinanceDataBL;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.GrpEdorWSDetailUI;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.PGrpEdorCancelUI;
import com.sinosoft.lis.bq.PGrpEdorConfirmBL;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 无名单实名化逻辑操作CF
 * 
 * @author 姜士杰 2018-12-20
 *
 */

public class WSCFDeal {

	String errorValue;
	String returnxml;

	private GlobalInput mGlobalInput = new GlobalInput();
	private String mEdorAcceptNo;

	public String getmEdorAcceptNo() {
		return mEdorAcceptNo;
	}

	public void setmEdorAcceptNo(String mEdorAcceptNo) {
		this.mEdorAcceptNo = mEdorAcceptNo;
	}

	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
	private LPGrpEdorMainSchema mLPGrpEdorMainSchema = null;
	private LCContSchema mLCContSchema;
	public CErrors mErrors = new CErrors();

	private String mCurrDate = PubFun.getCurrentDate();

	private String mCurrTime = PubFun.getCurrentTime();
	public LCGrpContInfo mLCGrpContTable;
	private LCGrpContSchema mLCGrpContSchema;
	private LPDiskImportSchema mLPDiskImportSchema;
	private LPDiskImportSet mLPDiskImportSet;
	public CustomerInfo mCustomerInfo;

	public String getErrorValue() {
		return errorValue;
	}

	public void setErrorValue(String errorValue) {
		this.errorValue = errorValue;
	}

	public WSCFDeal() {
	}

	public WSCFDeal(GlobalInput tGlobalInput, EdorItemInfo tEdorItemInfo, LCContSchema tLCContSchema,
			LCGrpContSchema tLCGrpContSchema, LCInsuredSchema tLCInsuredSchema) {
		this.mGlobalInput = tGlobalInput;
		this.mLCContSchema = tLCContSchema;
		this.mLCGrpContSchema = tLCGrpContSchema;
	}

	public boolean deal(VData data, String xml) {
		try {
			if (!getInputData(data)) {
				return false;
			}
			if (!deal()) {
				return false;
			}
		} finally {
			
			// 生成报文
			returnxml = ReturnXML(data);
			// 操作日志
			saveState(data, xml);
		}
		return true;
	}

	/**
	 * 获取数据
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mLCGrpContTable = (LCGrpContInfo) cInputData.getObjectByObjectName("LCGrpContInfo", 0);
		mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0);
		mLPDiskImportSet = (LPDiskImportSet) cInputData.getObjectByObjectName("LPDiskImportSet", 0);
		mCustomerInfo = (CustomerInfo) cInputData.getObjectByObjectName("CustomerInfo", 0);

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mLCGrpContTable.getGrpContNo());
		LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
		if (tLCGrpContSet.size() == 0) {
			errorValue = "查询团单信息失败！";
			return false;
		}
		mLCGrpContSchema = tLCGrpContSet.get(1);
		if (mGlobalInput == null) {
			errorValue = "请传入参数信息错误";
			return false;
		}
//		mGlobalInput.ComCode = mLCGrpContSchema.getManageCom();
//		mGlobalInput.ManageCom = mLCGrpContSchema.getManageCom();
		return true;
	}

	/**
	 * 逻辑处理
	 * @return
	 */
	private boolean deal() {

		try {

			if (!createWorkNo()) {
				return false;
			}

			if (!addEdorItem()) {

//				if (!cancelEdorItem("1", "G&EDORAPP")) {
//					return false;
//				}
				GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
				VData tt = new VData();
				tt.add(mGlobalInput);
				tt.add(mEdorAcceptNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				return false;
			}


			if (!saveDetail()) {

//				if (!cancelEdorItem("1", "G&EDORMAIN")) {
//					return false;
//				} else {
//					if (!cancelEdorItem("1", "G&EDORAPP")) {
//						return false;
//					}
//				}
				GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
				VData tt = new VData();
				tt.add(mGlobalInput);
				tt.add(mEdorAcceptNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				return false;
			}

			if (!appConfirm()) {

//				if (!cancelEdorItem("1", "G&EDORMAIN")) {
//					return false;
//				} else {
//					if (!cancelEdorItem("1", "G&EDORAPP")) {
//						return false;
//					}
//				}
				GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
				VData tt = new VData();
				tt.add(mGlobalInput);
				tt.add(mEdorAcceptNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				return false;
			}

			if (!edorConfirm()) {

//				if (!cancelEdorItem("1", "G&EDORMAIN")) {
//					return false;
//				} else {
//					if (!cancelEdorItem("1", "G&EDORAPP")) {
//						return false;
//					}
//				}
				GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
				VData tt = new VData();
				tt.add(mGlobalInput);
				tt.add(mEdorAcceptNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				return false;
			}
		} catch (Exception e) {
			errorValue = "保全处理异常，请求失败。";
//			cancelEdorItem("1", "G&EDORMAIN");
//			cancelEdorItem("1", "G&EDORAPP");
			GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
			VData tt = new VData();
			tt.add(mGlobalInput);
			tt.add(mEdorAcceptNo);
			tGEdorCancelBQ.cancelAppEdor(tt);
			return false;
		}

		return true;
	}

//	/**
//	 * 撤销工单
//	 * @param edorstate
//	 * @param transact
//	 * @return
//	 */
//	private boolean cancelEdorItem(String edorstate, String transact) {
//		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
//		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
//		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
//
//		TransferData tTransferData = new TransferData();
//		VData tVData = new VData();
//		tVData.addElement(mGlobalInput);
//
//		if ("G&EDORAPP".equals(transact)) {
//			tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
//			tLPEdorAppSchema.setEdorState(edorstate);
//
//			String delReason = "";
//			String reasonCode = "002";
//
//			tTransferData.setNameAndValue("DelReason", delReason);
//			tTransferData.setNameAndValue("ReasonCode", reasonCode);
//			tVData.addElement(tLPEdorAppSchema);
//
//			tVData.addElement(tTransferData);
//		} else if ("G&EDORMAIN".equals(transact)) {
//			tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
//			tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
//			tLPGrpEdorMainSchema.setEdorState(edorstate);
//			tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
//			String delReason = "";
//			String reasonCode = "002";
//			System.out.println(delReason);
//
//			tTransferData.setNameAndValue("DelReason", delReason);
//			tTransferData.setNameAndValue("ReasonCode", reasonCode);
//			tVData.addElement(tLPGrpEdorMainSchema);
//			tVData.addElement(tTransferData);
//		}
//		try {
//
//			tPGrpEdorCancelUI.submitData(tVData, transact);
//		} catch (Exception e) {
//
//			e.printStackTrace();
//			errorValue = "撤销工单失败" + e;
//			return false;
//		}
//		return true;
//	}

	/**
	 * 获取工单号
	 * @return
	 */
	private boolean createWorkNo() {

		mEdorAcceptNo = mCustomerInfo.getWorkNo();
		System.out.println("******获取工单号为：" + mEdorAcceptNo);
		return true;
	}

	/**
	 * 
	 * 添加保全项
	 * @return
	 */
	private boolean addEdorItem() {
		// 校验保单下的险种险种对应的保全项

		if (!checkGrpEdorType()) {
			return false;
		}
		mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
		mLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		mLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		mLPGrpEdorMainSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		mLPGrpEdorMainSchema.setEdorAppDate(mCurrDate);
		mLPGrpEdorMainSchema.setEdorValiDate(mCurrDate);

		mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		mLPGrpEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorType(BQ.EDORTYPE_WS);
		mLPGrpEdorItemSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		mLPGrpEdorItemSchema.setEdorAppDate(mCurrDate);
		mLPGrpEdorItemSchema.setEdorValiDate(mCurrDate);
		mLPGrpEdorItemSchema.setManageCom(mLCGrpContSchema.getManageCom());
		mLPGrpEdorItemSet.add(mLPGrpEdorItemSchema);

		VData tVData = new VData();
		tVData.add(mLPGrpEdorItemSet);
		tVData.add(mLPGrpEdorMainSchema);
		tVData.add(mGlobalInput);
		GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();
		if (!tGrpEdorItemUI.submitData(tVData, "INSERT||GRPEDORITEM")) {
			errorValue = "添加保全项目失败：" + tGrpEdorItemUI.mErrors.getFirstError();
			String error = tGrpEdorItemUI.mErrors.getFirstError();
			return false;
		}

		return true;

	}

	public boolean checkGrpEdorType() {

		ExeSQL tExeSQL = new ExeSQL();

		String checkSQL = "select distinct b.EdorCode, b.EdorName from LMRiskEdoritem  a, LMEdorItem b where a.edorCode ="
				+ " b.edorCode and b.edorcode != 'XB'   and a.riskCode in  (select riskCode from LCGrpPol where "
				+ "grpContNo = '" + mLCGrpContTable.getGrpContNo()
				+ "')  and (b.edorTypeFlag != 'N' or b.edorTypeFlag is null)order by EdorCode";
		SSRS checkSQLSSRS = tExeSQL.execSQL(checkSQL);

		System.out.println("================可添加:" + checkSQLSSRS.getMaxRow() + "个保全项");
		int total = 0;
		for (int i = 1; i <= checkSQLSSRS.getMaxRow(); i++) {
			System.out.println("=============" + checkSQLSSRS.GetText(i, 1) + "===" + "WS");
			if ("WS".equals(checkSQLSSRS.GetText(i, 1))) {
				total = 1;
			}
		}
		if (total <= 0) {
			errorValue = "您的保单投保,有险种的[WS]保全功能暂未上线，无法添加保全项目！";
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 保全项目明细录入
	 */
	private boolean saveDetail() {

		System.out.println("******************保全项目明细导入*******************");
		
		ExeSQL ExeSQL = new ExeSQL();
		String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ mGlobalInput.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WSCF" + "','" + mGlobalInput.ManageCom + "','" 
				+ "保全实名化开始导入','" + mEdorAcceptNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		
		MMap map = new MMap();
		for (int i = 1; i <= mLPDiskImportSet.size(); i++) {
			LPDiskImportSchema sLPDiskImportSchema = mLPDiskImportSet.get(i);
			mLPDiskImportSchema = new LPDiskImportSchema();
			mLPDiskImportSchema.setSerialNo("" + i);
			mLPDiskImportSchema.setEdorNo(mEdorAcceptNo);
			mLPDiskImportSchema.setEdorType(BQ.EDORTYPE_WS);
			mLPDiskImportSchema.setGrpContNo(mLCGrpContTable.getGrpContNo());
			mLPDiskImportSchema.setState("1");
			mLPDiskImportSchema.setInsuredName(sLPDiskImportSchema.getInsuredName());
			mLPDiskImportSchema.setSex(sLPDiskImportSchema.getSex());
			mLPDiskImportSchema.setBirthday(sLPDiskImportSchema.getBirthday());
			mLPDiskImportSchema.setIDType(sLPDiskImportSchema.getIDType());
			mLPDiskImportSchema.setIDNo(sLPDiskImportSchema.getIDNo());
			mLPDiskImportSchema.setContPlanCode(sLPDiskImportSchema.getContPlanCode());
			mLPDiskImportSchema.setOccupationType(sLPDiskImportSchema.getOccupationType());
			mLPDiskImportSchema.setOperator(mGlobalInput.Operator);
			mLPDiskImportSchema.setMakeDate(mCurrDate);
			mLPDiskImportSchema.setMakeTime(mCurrTime);
			mLPDiskImportSchema.setModifyDate(mCurrDate);
			mLPDiskImportSchema.setModifyTime(mCurrTime);
			map.put(mLPDiskImportSchema, "INSERT");
		}

		if (!submit(map)) {
			return false;
		}

		GrpEdorWSDetailUI tGrpEdorWSDetailUI = new GrpEdorWSDetailUI(mGlobalInput, mEdorAcceptNo,
				mLCGrpContTable.getGrpContNo());
		if (!tGrpEdorWSDetailUI.submitData()) {
			errorValue = "添加保全项目失败：" + tGrpEdorWSDetailUI.getError();
			return false;
		}
		
		String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ mGlobalInput.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WSCF" + "','" + mGlobalInput.ManageCom + "','" 
				+ "保全实名化导入结束','" + mEdorAcceptNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);

		return true;
	}

	/**
	 * 
	 * 保全理算
	 * @return
	 */
	private boolean appConfirm() {
		
		System.out.println("**************************准备理算");
		
		ExeSQL ExeSQL = new ExeSQL();
		String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ mGlobalInput.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WSCF" + "','" + mGlobalInput.ManageCom + "','" 
				+ "保全实名化开始理算','" + mEdorAcceptNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContTable.getGrpContNo());
		tLPGrpEdorMainSchema.setEdorValiDate(mCurrDate);
		tLPGrpEdorMainSchema.setEdorAppDate(mCurrDate);

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLPGrpEdorMainSchema);
		PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
		if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM")) {
			System.out.println(tPGrpEdorAppConfirmUI.getError().getErrContent());
			errorValue = "保全理算失败：" + tPGrpEdorAppConfirmUI.getError().getErrContent();
			return false;
		}
		
		String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ mGlobalInput.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WSCF" + "','" + mGlobalInput.ManageCom + "','" 
				+ "保全实名化理算结束','" + mEdorAcceptNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);
		
		return true;
	}

	/**
	 * 数据提交
	 * @param map
	 * @return
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			errorValue = "提交数据库发生错误" + tPubSubmit.mErrors;
			return false;
		}
		return true;
	}

	/**
	 * 
	 * baoqau 确认
	 * @return
	 */
	private boolean edorConfirm() {
		
		System.out.println("**************************准备确认");
		
		ExeSQL ExeSQL = new ExeSQL();
		String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ mGlobalInput.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WSCF" + "','" + mGlobalInput.ManageCom + "','" 
				+ "保全实名化开始确认','" + mEdorAcceptNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		
		MMap map = new MMap();

		FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput, mEdorAcceptNo, BQ.NOTICETYPE_G, "");
		if (!tFinanceDataBL.submitData()) {
			errorValue = "生成财务数据错误" + tFinanceDataBL.mErrors;
			return false;
		}

		String strTemplatePath = "";
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		VData data = new VData();
		data.add(strTemplatePath);
		data.add(tLPGrpEdorMainSchema);
		data.add(mGlobalInput);
		PGrpEdorConfirmBL tPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
		map = tPGrpEdorConfirmBL.getSubmitData(data, "INSERT||GRPEDORCONFIRM");
		if (map == null) {
			errorValue = "保全结案发生错误" + tFinanceDataBL.mErrors;
			return false;
		}

		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setTypeNo("03");

		data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (tmap == null) {
			errorValue = "工单结案失败" + tFinanceDataBL.mErrors;
			return false;
		}
		map.add(tmap);
		if (!submit(map)) {
			return false;
		}
		
		String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ mGlobalInput.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WSCF" + "','" + mGlobalInput.ManageCom + "','" 
				+ "保全实名化确认结束','" + mEdorAcceptNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);
		
		return true;
	}

	// 返回报文
	public String ReturnXML(VData data) {
		String responseXml;
		try {
			MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
			Element root = new Element("DataSet");
			Document doc = new Document(root);
			Element eMsgHead = new Element("MsgHead");
			Element eHeadnode = new Element("Item");
			Element eHeadBatchNo = new Element("BatchNo");
			eHeadBatchNo.setText(vMessHead.getBatchNo());
			eHeadnode.addContent(eHeadBatchNo);
			Element eHeadSendDate = new Element("SendDate");
			eHeadSendDate.setText(vMessHead.getSendDate());
			eHeadnode.addContent(eHeadSendDate);
			Element eHeadSendTime = new Element("SendTime");
			eHeadSendTime.setText(vMessHead.getSendTime());
			eHeadnode.addContent(eHeadSendTime);
			Element eHeadBranchCode = new Element("BranchCode");
			eHeadBranchCode.setText(vMessHead.getBranchCode());
			eHeadnode.addContent(eHeadBranchCode);
			Element eHeadSendOperator = new Element("SendOperator");
			eHeadSendOperator.setText(vMessHead.getSendOperator());
			eHeadnode.addContent(eHeadSendOperator);
			Element eHeadMsgType = new Element("MsgType");
			eHeadMsgType.setText(vMessHead.getMsgType());
			eHeadnode.addContent(eHeadMsgType);
			if (!"".equals(errorValue) && null != errorValue) {
				Element eHeadState = new Element("State");
				eHeadState.setText("01");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText(errorValue);
				eHeadnode.addContent(eHeadErrInfo);
			} else {
				Element eHeadState = new Element("State");
				eHeadState.setText("00");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("操作成功！");
				eHeadnode.addContent(eHeadErrInfo);
			}
			eMsgHead.addContent(eHeadnode);

			Element eMsgInfo = new Element("EndorsementInfo");
			Element eInfodnode = new Element("Item");
			Element eInfoEdorNo = new Element("EdorNo");
			eInfoEdorNo.setText(mEdorAcceptNo);
			System.out.println(mEdorAcceptNo);
			eInfodnode.addContent(eInfoEdorNo);
			Element eInfoEdorValidate = new Element("EdorValidate");
			eInfoEdorValidate.setText(mCurrDate);
			eInfodnode.addContent(eInfoEdorValidate);
			Element eInfoEdorConfdate = new Element("EdorConfdate");
			eInfoEdorConfdate.setText(mCurrDate);
			eInfodnode.addContent(eInfoEdorConfdate);
			eMsgInfo.addContent(eInfodnode);

			root.addContent(eMsgHead);
			root.addContent(eMsgInfo);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(doc);
		} catch (IOException e) {
			responseXml = "生成返回报文失败。。。";
			e.printStackTrace();
		}
		return responseXml;
	}

	public String getReturnxml() {
		return returnxml;
	}

	// 保存日志
	public void saveState(VData vdata, String xml) {
		MessHead vMessHead = (MessHead) vdata.getObjectByObjectName("MessHead", 0);

		if (!"".equals(errorValue) && null != errorValue) {
			// 错误日志
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ vMessHead.getSendOperator()
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','"
					+ vMessHead.getMsgType() + "','" + vMessHead.getBranchCode() + "','" + vMessHead.getBatchNo()
					+ "','" + mEdorAcceptNo + "')";
			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
			String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ vMessHead.getSendOperator() + "','" + xml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ returnxml + "','" + errorValue + "','" + vMessHead.getMsgType() + "','"
					+ vMessHead.getBranchCode() + "','" + mEdorAcceptNo + "')";
			ExeSQL.execUpdateSQL(insertErrorInsertLogSql);
		} else {
			// 日志
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ vMessHead.getSendOperator()
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'1','"
					+ vMessHead.getMsgType() + "','" + vMessHead.getBranchCode() + "','" + vMessHead.getBatchNo()
					+ "','" + mEdorAcceptNo + "')";
			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		}
	}

}
