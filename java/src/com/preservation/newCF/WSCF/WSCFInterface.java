package com.preservation.newCF.WSCF;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.net.SocketException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.obj.CustomerInfo;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.preservation.util.StringUtil;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * 无名单实名化CF
 * 
 * @author 姜士杰 2018-12-20
 *
 */

public class WSCFInterface {

	private String errorValue;
	private String returnXML;
	private String GrpContNo;
	private VData data = new VData();
	private GlobalInput globalInput = new GlobalInput();
	private XmlExport dXmlExport = new XmlExport();

	public boolean deal(String xml) throws Exception {
		
		if (!gateAndCheckData(xml)) {
			returnXML = creatReturnXML();
			return false;
		}
		returnXML = creatReturnXML();
		// 启动线程，逻辑处理类，生成返回报文，调用东软接口
		if ("".equals(errorValue) || null == errorValue) {
			WSCFForReturn wSCFForReturn = new WSCFForReturn();
			wSCFForReturn.setData(data);
			wSCFForReturn.setXml(xml);
			Thread t = new Thread(wSCFForReturn);
			t.start();
		}
		return true;
	}

	/**
	 * 解析报文
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	private boolean gateAndCheckData(String xml) throws Exception {
		StringReader reader = new StringReader(xml);
		try {

			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = null;
			try {
				doc = tSAXBuilder.build(reader);
			} catch (JDOMException e1) {
				e1.printStackTrace();
			}
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");

			MessHead head = new MessHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			// 获取客户号
			Element eBody = rootElement.getChild("MsgBody");
			Element eWS = eBody.getChild("WSCF");
			Element lCGrpContInfo = eWS.getChild("LCGrpContTable");
			Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
			String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
			String WorkNo = eLCGrpContInfo.getChildText("WorkNo");
			globalInput.ComCode = eLCGrpContInfo.getChildText("ManageCom");
			globalInput.ManageCom = eLCGrpContInfo.getChildText("ManageCom");
//			globalInput.Operator = "pa0001";
			globalInput.Operator = "BAOQA";
			data.add(globalInput);
			
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ globalInput.Operator
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ "WSCF" + "','" + globalInput.ManageCom + "','" 
					+ "保全实名化开始','" + WorkNo + "')";

			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
			
			//获取客户号
			String sql1 = "select appntno from lcgrpcont where  grpcontno='" + GrpContNo + " '";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS sql1SSRS = tExeSQL.execSQL(sql1);
			if(sql1SSRS.getMaxRow() < 1 ){
				errorValue = "此保单号" + GrpContNo + "未查询到客户号！";
				return false;
			} 
			String customerno = sql1SSRS.GetText(1, 1);
			System.out.println("查询到客户号为" + customerno);
			
			// 获取工单号
			String sql = "select * from LGWork where CustomerNo='" + customerno + "' and WorkNo='" + WorkNo + "'";
			
			SSRS sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0) {
				WorkNo = sqlSSRS.GetText(1, 2);
				String str1 = sqlSSRS.GetText(1, 5);
				if ("5".equals(str1)) {
					errorValue = "此工单号" + WorkNo + "已结案！";
					return false;
				}
			} else {
				errorValue = "客户号" + customerno + "未生成工单号！";
				return false;
			}
			System.out.println("********工单号EdorAcceptNo：" + WorkNo);

			CustomerInfo iCustomerInfo = new CustomerInfo();
			iCustomerInfo.setWorkNo(WorkNo);
			data.add(iCustomerInfo);

			// 团单信息
			String GrpName = eLCGrpContInfo.getChildText("GrpName");
			String ExcelURL = eLCGrpContInfo.getChildText("Excel");
			String FileName = ExcelURL.split("/")[ExcelURL.split("/").length - 1];
			String FTPPath = ExcelURL.substring(0, ExcelURL.length() - FileName.length());
			if (null == GrpContNo || "".equals(GrpContNo)) {
				errorValue = "申请报文中获取团单信息失败。";
				return false;
			}
			String str = "select 1 from LCCont  where appflag='1' and polType = '1' "
					+ " and conttype='2' and grpcontno='" + GrpContNo + "' ";
			String res = new ExeSQL().getOneValue(str);
			if (null == res || "".equals(str)) {
				errorValue = "非无名单保单不能申请无名单实名化";
				return false;
			}
			if (null == GrpName || "".equals(GrpName)) {
				errorValue = "团单组织名称不能为空";
				return false;
			}
			LCGrpContDB tLCGrpContDB = new LCGrpContDB();
			tLCGrpContDB.setGrpContNo(GrpContNo);
			if (!tLCGrpContDB.getInfo()) {
				errorValue = "传入的团单号错误，核心系统不存在团单“" + GrpContNo + "”";
				return false;
			}

			LCGrpContSchema mLCGrpContSchema = tLCGrpContDB.getSchema();
			data.add(mLCGrpContSchema);
			MMap tCekMap = null;
			tCekMap = lockLGWORK(mLCGrpContSchema);
			if (tCekMap == null) {
				errorValue = "团单" + mLCGrpContSchema.getGrpContNo() + "正在进行无名单实名化，请不要重复请求";
				return false;
			}
			if (!submit(tCekMap)) {
				errorValue = "团单" + mLCGrpContSchema.getGrpContNo() + "正在进行无名单实名化，请不要重复请求";
				return false;
			}

			LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
			iLCGrpContInfo.setGrpContNo(GrpContNo);
			iLCGrpContInfo.setGrpName(GrpName);
			data.add(iLCGrpContInfo);

			// 人员信息
			// FTP下载人员文件.xls
			ExeSQL exeSQL = new ExeSQL();
			String IpSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='IP'";
			String accNameSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='AccName'";
			String passSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='PassWord'";
			String portSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='Port'";
			String IP = exeSQL.getOneValue(IpSql);
			String accName = exeSQL.getOneValue(accNameSql);
			String passWord = exeSQL.getOneValue(passSql);
			String port = exeSQL.getOneValue(portSql);
			if (!(StringUtil.isNull(IP, accName, passWord, port))) {
				errorValue = "FTP信息 配置错误,检查数据库配置";
				return false;
			}
			String rootPath = getClass().getResource("/").getFile().toString();
			String BasePath = rootPath.replaceAll("WEB-INF/classes/", "");
			BasePath += "temp_lp/";

			FTPTool tFTPTool = new FTPTool(IP, accName, passWord, Integer.valueOf(port));
			if (!tFTPTool.loginFTP()) {
				errorValue = "登陆FTP失败：" + tFTPTool.getErrContent(1);
				return false;
			} else {
				if (!tFTPTool.downloadFile(FTPPath, BasePath, FileName)) {
					errorValue = "登陆FTP失败：" + tFTPTool.getErrContent(1);
					return false;
				}
			}
			// 解析excel
			LPDiskImportSet mLPDiskImportSet = new LPDiskImportSet();
			FileInputStream in = null;
			try {
				in = new FileInputStream(BasePath + FileName);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				errorValue = "系统错误：" + e.toString();
				return false;
			}
			HSSFWorkbook work = null;
			try {
				work = new HSSFWorkbook(in);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				errorValue = "系统错误：" + e.toString();
				return false;
			}
			System.out.println("HSSFWorkbook读取excel");
			HSSFSheet sheet = work.getSheetAt(0); // 得到第一个sheet
			int rowNo = sheet.getLastRowNum(); // 得到行数
			System.out.println("================================="+rowNo);
			for (int i = 1; i <= rowNo; i++) {
				String Name = sheet.getRow(i).getCell((short) 0).getStringCellValue();
				String Sex = sheet.getRow(i).getCell((short) 1).getStringCellValue();
				String Birthday = sheet.getRow(i).getCell((short) 2).getStringCellValue();
				String IDType = sheet.getRow(i).getCell((short) 3).getStringCellValue();
				String IDNo = sheet.getRow(i).getCell((short) 4).getStringCellValue();
				String OccupationType = sheet.getRow(i).getCell((short) 5).getStringCellValue();
				String ContPlanCode = sheet.getRow(i).getCell((short) 6).getStringCellValue();
				LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();
				mLPDiskImportSchema.setInsuredName(Name);
				mLPDiskImportSchema.setSex(Sex);
				mLPDiskImportSchema.setBirthday(Birthday);
				mLPDiskImportSchema.setIDType(IDType);
				mLPDiskImportSchema.setIDNo(IDNo);
				mLPDiskImportSchema.setContPlanCode(ContPlanCode);
				mLPDiskImportSchema.setOccupationType(OccupationType);
				mLPDiskImportSet.add(mLPDiskImportSchema);
			}
			data.add(mLPDiskImportSet);
			
			String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ globalInput.Operator
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ "WSCF" + "','" + globalInput.ManageCom + "','" 
					+ "保全实名化读取报文结束','" + WorkNo + "')";

			ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);
		} finally {
			reader.close();
		}
		return true;
	}

	// 锁表
	private MMap lockLGWORK(LCGrpContSchema cLCGrpContSchema) {

		MMap tMMap = null;
		String tLockNoType = "WS";
		String tAIS = "360";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", cLCGrpContSchema.getGrpContNo());
		tTransferData.setNameAndValue("LockNoType", tLockNoType);
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

		VData tVData = new VData();
		tVData.add(globalInput);
		tVData.add(tTransferData);

		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
		if (tMMap == null) {
			return null;
		}
		return tMMap;
	}

	private boolean submit(MMap map) {

		VData vdata = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(vdata, "")) {
			errorValue = "提交数据库发生错误" + tPubSubmit.mErrors;
			return false;
		}
		return true;
	}

	// 第一次返回报文
	private String creatReturnXML() {

		String responseXml = null;
		try {
			MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
			Element root = new Element("DataSet");
			Document doc = new Document(root);
			Element eMsgHead = new Element("MsgHead");
			Element eHeadnode = new Element("Item");
			Element eHeadBatchNo = new Element("BatchNo");
			eHeadBatchNo.setText(vMessHead.getBatchNo());
			eHeadnode.addContent(eHeadBatchNo);
			Element eHeadSendDate = new Element("SendDate");
			eHeadSendDate.setText(vMessHead.getSendDate());
			eHeadnode.addContent(eHeadSendDate);
			Element eHeadSendTime = new Element("SendTime");
			eHeadSendTime.setText(vMessHead.getSendTime());
			eHeadnode.addContent(eHeadSendTime);
			Element eHeadBranchCode = new Element("BranchCode");
			eHeadBranchCode.setText(vMessHead.getBranchCode());
			eHeadnode.addContent(eHeadBranchCode);
			Element eHeadSendOperator = new Element("SendOperator");
			eHeadSendOperator.setText(vMessHead.getSendOperator());
			eHeadnode.addContent(eHeadSendOperator);
			Element eHeadMsgType = new Element("MsgType");
			eHeadMsgType.setText(vMessHead.getMsgType());
			eHeadnode.addContent(eHeadMsgType);

			if (!"".equals(errorValue) && null != errorValue) {
				Element eHeadState = new Element("State");
				eHeadState.setText("01");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("接收解析报文失败：" + errorValue);
				eHeadnode.addContent(eHeadErrInfo);
			} else {
				Element eHeadState = new Element("State");
				eHeadState.setText("00");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("接收并解析报文成功！");
				eHeadnode.addContent(eHeadErrInfo);
			}
			eMsgHead.addContent(eHeadnode);

			root.addContent(eMsgHead);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(doc);
		} catch (IOException e) {
			responseXml = "生成返回报文失败";
			e.printStackTrace();
		}
		return responseXml;
	}

	public String getReturnXML() {
		return returnXML;
	}

	public void setReturnXML(String returnXML) {
		this.returnXML = returnXML;
	}

	public VData getData() {
		return data;
	}

	public void setData(VData data) {

		this.data = data;
	}

	private boolean FTPSendFile() {

		/** 文件 */
		String fileName = this.GrpContNo;
		String filePath = "";

		try {
			String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";// 生成文件的存放路径
			// "select sysvarvalue from LDSYSVAR where Sysvar='BatchSendPath'";
			filePath = new ExeSQL().getOneValue(sqlurl);// 生成文件的存放路径
			System.out.println("生成文件的存放路径   " + filePath);// 调试用－－－－－
			if (filePath == null || filePath.equals("")) {
				System.out.println("获取文件存放路径出错");
				return false;
			}
			String xmlDoc = this.dXmlExport.outputString();
			System.out.println("doc==" + xmlDoc);
			this.dXmlExport.outputDocumentToFile(filePath, fileName, "UTF-8");
			// filePath="E:\\";

			// 上传文件到FTP服务器
			if (!sendZip(filePath + fileName + ".xml")) {
				System.out.println("上传文件到FTP服务器失败！");
				return false;
			} else {
				// 上传成功后删除
				System.out.println("上传文件到FTP服务器成功！");
				File xmlFile = new File(filePath + fileName + ".txt");
				xmlFile.delete(); // 删除上传完的文件
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean sendZip(String cFileUrlName) {

		// 登入前置机
		String ip = "";
		String user = "";
		String pwd = "";
		int port = 21;// 默认端口为21
		// String sql =
		// "select code,codename from LDCODE where codetype='ecprintserver'";
		String sql = "select DESCRIPTION,CODE from ldconfig where codetype = 'WSInterface'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS != null) {
			for (int m = 1; m <= tSSRS.getMaxRow(); m++) {
				if (tSSRS.GetText(m, 1).equals("IP")) {
					ip = tSSRS.GetText(m, 2);
				} else if (tSSRS.GetText(m, 1).equals("AccName")) {
					user = tSSRS.GetText(m, 2);
				} else if (tSSRS.GetText(m, 1).equals("PassWord")) {
					pwd = tSSRS.GetText(m, 2);
				} else if (tSSRS.GetText(m, 1).equals("Port")) {
					port = Integer.parseInt(tSSRS.GetText(m, 2));
				}
			}
		}

		FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
				return false;
			} else {
				if (!tFTPTool.upload(cFileUrlName)) {
					CError tError = new CError();
					tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);

					System.out.println("上载文件失败!");
					return false;
				}

				tFTPTool.logoutFTP();
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
			CError tError = new CError();
			tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);

			System.out.println("上载文件失败，可能是网络异常!");
			return false;
		} catch (IOException ex) {
			ex.printStackTrace();
			CError tError = new CError();
			tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);

			System.out.println("上载文件失败，可能是无法写入文件");
			return false;
		}
		return true;
	}

}
