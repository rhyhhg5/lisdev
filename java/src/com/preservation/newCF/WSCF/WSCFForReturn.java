package com.preservation.newCF.WSCF;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

import com.preservation.WS.deal.WSDeal;
import com.preservation.WS.deal.WSForReturn;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 
 * 保全 无名单实名化CF 调用东软接口 返回操作后的报文
 * 
 * @author 姜士杰 2018-12-20
 *
 */

public class WSCFForReturn implements Runnable {

	VData data = new VData();
	String xml;

	public VData getData() {
		return data;
	}

	public void setData(VData data) {
		this.data = data;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public void run() {
		
		System.out.println("***************************开始线程***************************");
		
		WSCFDeal wSCFDeal = new WSCFDeal();
		try {
			wSCFDeal.deal(data, xml);
		} finally {
			
			String returnXML = wSCFDeal.getReturnxml();
			// 调用接口发送报文
			Returnservice(returnXML);
		}
	}

	// 调用东软接口发送报文
	public void Returnservice(String aInXmlStr) {
		String sql = "select TransDetail,TransDetail2,remark from WFTransInfo where transtype = 'WSInterface' and transcode = '1'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql1SSRS = tExeSQL.execSQL(sql);
		if (sql1SSRS.getMaxRow() < 1) {
			System.out.println("无法查询到发送报文的接口信息！");
			return;
		} else if ("".equals(sql1SSRS.GetText(1, 2)) || null == sql1SSRS.GetText(1, 2)) {
			System.out.println("查询到发送报文的接口 的 路径 为空！");
			return;
		} else if ("".equals(sql1SSRS.GetText(1, 1)) || null == sql1SSRS.GetText(1, 1)) {
			System.out.println("查询到发送报文的接口 的 命名空间 为空！");
			return;
		} else if ("".equals(sql1SSRS.GetText(1, 3)) || null == sql1SSRS.GetText(1, 3)) {
			System.out.println("查询到发送报文的接口 的 主方法 为空！");
			return;
		}
		String tStrTargetEendPoint = sql1SSRS.GetText(1, 2).trim();
		String tStrNamespace = sql1SSRS.GetText(1, 1).trim();
		try {
			RPCServiceClient client = new RPCServiceClient();
			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
			option.setAction(sql1SSRS.GetText(1, 3).trim());
			option.setTimeOutInMilliSeconds(3000000L);
			QName name = new QName(tStrNamespace, sql1SSRS.GetText(1, 3).trim());
			Object[] object = new Object[] { aInXmlStr };
			client.invokeRobust(name, object);
			System.out.println("调用接口成功：");
			System.out.println(aInXmlStr);
		} catch (Exception e) {
			System.out.println("调用接口失败：");
			System.out.println(aInXmlStr);
			e.printStackTrace();
		}
	}

	public static byte[] InputStreamToBytes(InputStream pIns) {
		
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
				
			}
			
		} catch (IOException ex) {
			
			ex.printStackTrace();
			return null;
			
		} finally {
			
			try {
				
				pIns.close();
				
			} catch (IOException ex) {
				
				ex.printStackTrace();
			}
		}

		return mByteArrayOutputStream.toByteArray();
	}

	public static void main(String[] args) {
		
		WSForReturn WSForReturn = new WSForReturn();
		WSForReturn.Returnservice("aaaa");
	}

}
