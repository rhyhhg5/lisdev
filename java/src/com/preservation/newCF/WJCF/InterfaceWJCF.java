package com.preservation.newCF.WJCF;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.WJ.InterfaceWJ;
import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * BBQWJCF 无名单减少被保人
 * @author 姜士杰 2018-12-20
 *
 */
public class InterfaceWJCF extends InterfaceBaseCF{

	/**
	 * 逻辑处理
	 */
	public boolean dealc() {
		DealWJCF dealWJCF = new DealWJCF();
		if (!dealWJCF.deal(data)) {
			errorValue = dealWJCF.getErrorValue();
			WorkNo = dealWJCF.getEdorAcceptNo();
			return false;
		}
		WorkNo = dealWJCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement) {
		
		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element WJCF = rootElement.getChild("WJCF");
		// 客户号
		Element msgCustomer = WJCF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = WJCF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = WJCF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		Element xxxFormData = xxFormData.getChild("plans");
		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		if (StringUtil.StringNull(xxxFormData.getChildText("PlanCode"))) {
			tFormData.setPlanCode(xxxFormData.getChildText("PlanCode"));
		}
		
		if (StringUtil.StringNull(xxxFormData.getChildText("PremWZ"))) {
			tFormData.setPremWZ(Double.parseDouble(xxxFormData.getChildText("PremWZ")));
		}

		if (StringUtil.StringNull(xxxFormData.getChildText("Peoples"))) {
			tFormData.setPeoples(Integer.parseInt(xxxFormData.getChildText("Peoples")));
		}

		if (StringUtil.StringNull(xxxFormData.getChildText("SumPrem"))) {
			tFormData.setSumPrem(Double.parseDouble(xxxFormData.getChildText("SumPrem")));
		}

		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

		data.add(tFormData);
	}

	public static void main(String[] args) {
		InterfaceWJ tInterfaceWJ = new InterfaceWJ();

		try {
			InputStream pIns = new FileInputStream("C:/Users/Administrator/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceWJ.deal(mInXmlStr);
			System.out.println(tInterfaceWJ.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
