package com.preservation.newCF.LQCF;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Element;

import com.preservation.LQ.InterfaceLQ;
import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.newCF.XTCF.obj.RiskCodeObjCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * LQ部分领取CF 姜士杰 2019-01-18
 * 
 * @author Administrator
 *
 */
public class InterfaceLQCF extends InterfaceBaseCF {

	/**
	 * 逻辑处理
	 */
	public boolean dealc() {
		// 处理逻辑
		DealLQCF dealLQCF = new DealLQCF();
		if (!dealLQCF.deal(data)) {
			errorValue = dealLQCF.getErrorValue();
			WorkNo = dealLQCF.getEdorAcceptNo();
			return false;
		}
		WorkNo = dealLQCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement) {

		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element LQCF = rootElement.getChild("LQCF");
		List<Element> list ;

		// 客户号
		Element msgCustomer = LQCF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = LQCF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = LQCF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
//		tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
		tFormData.setCost(xxFormData.getChildText("cost"));
		tFormData.setExcel(xxFormData.getChildText("Excel"));
		
		Element Money = LQCF.getChild("Money");
		
		RiskCodeObjCF XTRiskCodeObj = new RiskCodeObjCF();
		List<RiskCodeObjCF> RiskCodeObjSet = new ArrayList<RiskCodeObjCF>();
		
		List<Element> itemList = Money.getChildren("Item");
		for(int i = 0;i < itemList.size();i++){
			Element money = itemList.get(i);
			RiskCodeObjCF XtRiskCodeObj = new RiskCodeObjCF();
			String GrpPolNo = "";
			String RiskCode = money.getChildText("RiskCode");
			String groupMoney = money.getChildText("groupMoney");
			String groupFixMoney = money.getChildText("GroupFixMoney");
			String sql = "select GrpPolNo from LCPol where grpcontno='" + GrpContNo + "'" + "and RiskCode='" + RiskCode + "'";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS sqlSSRS = tExeSQL.execSQL(sql);
			if(sqlSSRS.getMaxRow() < 1){
				GrpPolNo = "0";
			}else {
				GrpPolNo = sqlSSRS.GetText(1, 1);
			}
			System.out.println("==============================解析到报文险种代码为1：" + GrpPolNo);
			System.out.println("==============================解析到报文险种代码为1：" + groupMoney);
			System.out.println("==============================解析到报文险种代码为1：" + groupFixMoney);
			XtRiskCodeObj.setRiskCode(GrpPolNo);
			XtRiskCodeObj.setGetMoney(groupMoney);
			XtRiskCodeObj.setGroupFixMoney(groupFixMoney);
			RiskCodeObjSet.add(XtRiskCodeObj);
			XTRiskCodeObj.setRiskCodeObjCF(RiskCodeObjSet);
		}
//		xtFormData.setGroupMoney(groupMoney);
//		tFormData.setMoney(list);
		data.add(tFormData);
		data.add(XTRiskCodeObj);
	}

	public static void main(String[] args) {
		InterfaceLQ tInterfaceLQ = new InterfaceLQ();

		try {
			InputStream pIns = new FileInputStream("C:/Users/user/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceLQ.deal(mInXmlStr);
			System.out.println(tInterfaceLQ.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
