package com.preservation.newCF.LQCF;

import java.util.List;
import com.preservation.newCF.DealBaseCF;
import com.preservation.newCF.XTCF.obj.RiskCodeObjCF;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GrpEdorLQDetailUI;

public class DealLQCF extends DealBaseCF {

	/**
	 * 保全项目明细 保存按钮
	 * 姜士杰 2019-01-18
	 * @return
	 */
	public boolean checkSave() {

		String edorNo = EdorAcceptNo;
		String edorType = mFormData.getEdorType();
		String grpContNo = mLCGrpContInfo.getGrpContNo();

		// 报文参数的非空校验
		if (!StringUtil.StringNull(edorNo) || !StringUtil.StringNull(edorType) || !StringUtil.StringNull(grpContNo)) {
			Content = "保全号，保全类型，团体合同号不能为空";
			return false;
		}
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo, edorType);
		List<RiskCodeObjCF> riskcodelist = mRiskCodeObj.getRiskCodeObj();
		for (int i = 0; i < riskcodelist.size(); i++) {
			String groupMoney = riskcodelist.get(i).getGetMoney();
			System.out.println(groupMoney);
			String groupFixMoney = riskcodelist.get(i).getGroupFixMoney();
			System.out.println(groupFixMoney);
			if ("0".equals(groupMoney) && "0".equals(groupFixMoney)) {
				Content = "团体和个人账户领取金不能为空";
				return false;
			}
			if("0".equals(riskcodelist.get(i).getRiskCode())){
				Content = "传入保单号或险种号错误！";
				return false;
			}
			tSpecialData.setGrpPolNo(riskcodelist.get(i).getRiskCode());
			tSpecialData.add("GrpMoney", groupMoney);
			tSpecialData.add("GrpFixMoney", groupFixMoney);
		}
		GrpEdorLQDetailUI tGrpEdorLQDetailUI = new GrpEdorLQDetailUI(GI, edorNo, grpContNo, tSpecialData);
		if (!tGrpEdorLQDetailUI.submitData()) {

			Content = "数据保存失败！原因是:" + tGrpEdorLQDetailUI.getError();
			return false;
		} else {

			Content = "数据保存成功。";
			String message = tGrpEdorLQDetailUI.getMessage();
			if (message != null) {

				Content = message;
				return false;
			}
			return true;
		}
	}

	/**
	 * 当保全有单独逻辑的时候重写次方法
	 */
	@Override
	public boolean domake() {

		if(!"0".equals(mFormData.getCost())){
			Content = "领取类型错误！";
			return false;
		}
		return true;
	}

}
