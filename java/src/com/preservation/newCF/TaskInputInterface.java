package com.preservation.newCF;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;



import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.VData;
/**
  * 经办 
  * @ClassName: TaskInputInterface
  * @Description: TODO
  * @author liuzehong
  * @date 2018年11月30日 下午5:14:43
  * @version V1.0
 **/
public class TaskInputInterface {
	
	private String content="";
	
	private String tSendDate="";
	
	private String tSendTime="";
	
	private String tBranchCode="";
	
	private String tSendOperator="";
	
	private String tMsgType="";

	private String CustomerNo="";

	private String ApplyTypeNo="";
	
	private GlobalInput tG = new GlobalInput();
	
	private String ManageCom ="";//操作机构

	private String WorkNo="";  //工单号码

	private String responseXml="";  //返回报文

	public String getResponseXml() {
		return responseXml;
	}

	public boolean deal(String xml) throws Exception{
		
		if(!isNull(xml)) {
			this.content = "请求报文不能为空";
			createReturnXml("01");
			return false;
		}
		// 解析报文
		if (!getdata(xml)) {
			return false;
		}
		if (!server()) {
			return false;
		}
		createReturnXml("00");
		return true;
	}

	/**
	 * @param xml
	 * @return
	 */
	private boolean getdata(String xml) {
		// TODO Auto-generated method stub
		System.out.println("***********开始getdata解析报文*********");
		Document doc;
		try {
			doc = DocumentHelper.parseText(xml);
			Element root = doc.getRootElement();
			Element msgHead = root.element("MsgHead");
			Element tItem = msgHead.element("Item");
			tSendDate = tItem.elementText("SendDate");
			tSendTime = tItem.elementText("SendTime");
			tBranchCode = tItem.elementText("BranchCode");
			tSendOperator = tItem.elementText("SendOperator");
			tMsgType=tItem.elementText("MsgType");
			Element msgBody = root.element("MsgBody");
			ManageCom = msgBody.elementText("ManageCom");
			CustomerNo = msgBody.elementText("CustomerNo");
			ApplyTypeNo = msgBody.elementText("ApplyTypeNo");
			if (!isNull(ManageCom)) {
				content="管理机构不能为空！";
				createReturnXml("01");
				return false;
			}
			if (!isNull(CustomerNo)) {
				content="客户号不能为空！";
				createReturnXml("01");
				return false;
			}
			if (CustomerNo.length() != 8){
				content="客户号不正确！";
				createReturnXml("01");
				return false;
			}
			if (!isNull(ApplyTypeNo)) {
				content="申请人类型不能为空！";
				createReturnXml("01");
				return false;
			}
			int i = Integer.parseInt(ApplyTypeNo);
			if (i < 0 || i > 4){
				content="申请人类型错误！";
				createReturnXml("01");
				return false;
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			content="解析报文出错";
			createReturnXml("01");
			return false;
		}
		return true;
	}
	
	/**
	 * 调用核心处理类
	 */
	private boolean server() {
		// TODO Auto-generated method stub
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(CustomerNo);//客户号
		tLGWorkSchema.setTypeNo(ApplyTypeNo);//申请人类型
		tLGWorkSchema.setTypeNo("03");
		VData data = new VData();
		tG.AgentCom = ManageCom;
		System.out.println(tG.AgentCom);
		tG.Operator = "BAOQA";
		System.out.println(tG.Operator);
		data.add(tG);
		data.add(tLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			content= "生成保全工单失败" + tTaskInputBL.mErrors.getFirstError();
			return false;
		}
		this.WorkNo = tTaskInputBL.getWorkNo();
		System.out.println("工单号码："+WorkNo);
		content="操作成功";
		return true;
	}
	/**
	 *  拼接返回报文
	 * @param succFlag
	 */
	private void createReturnXml(String succFlag) {
		// TODO Auto-generated method stub
		StringWriter stringWriter = new StringWriter();  
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("TaskRe_Response");
		document.setRootElement(tDateSet);
		//返回报文头样式
		Element tMsgResHead = tDateSet.addElement("MsgHead");
		Element tItem = tMsgResHead.addElement("Item");
		Element tSendDate = tItem.addElement("SendDate");
		if (isNull(this.tSendDate)) {
			tSendDate.addText(this.tSendDate);
		}
		Element tSendTime = tItem.addElement("SendTime");
		if (isNull(this.tSendTime)) {
			tSendTime.addText(this.tSendTime);
		}
		Element tBranchCode = tItem.addElement("BranchCode");
		if (isNull(this.tBranchCode)) {
			tBranchCode.addText(this.tBranchCode);
		}
		Element tSendOperator= tItem.addElement("SendOperator");
		if (isNull(this.tSendOperator)) {
			tSendOperator.addText(this.tSendOperator);
		}
		Element tMsgType = tItem.addElement("MsgType");
		 tMsgType.addText("TASK");
		Element tMsgBody = tDateSet.addElement("MsgBody");
		
		Element tState = tMsgBody.addElement("State");
		tState.addText(succFlag);
		Element tErrInfo = tMsgBody.addElement("ErrInfo");
		tErrInfo.addText(this.content);
		Element tWokeNO = tMsgBody.addElement("WorkNo");
		if (isNull(WorkNo)) {
			tWokeNO.addText(this.WorkNo);
		}
		
		//创建写文件方法  
        XMLWriter xmlWriter = new XMLWriter(stringWriter); 
        //写入文件  
        try {
			xmlWriter.write(document);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}  
        //关闭  
        try {
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		} 
        this.responseXml = stringWriter.toString();
	}
	
	
	public  boolean isNull(String str){
		if(!"".equals(str)&&null!=str){
			return true;
		}else{
			return false;
		}
	}
	
	
	public static void main(String[] args) {
		
	}

}
