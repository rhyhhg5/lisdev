package com.preservation.newCF.XTCF;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Element;
import com.preservation.XT.DealXT;
import com.preservation.XT.obj.RiskCodeObj;
import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.newCF.XTCF.obj.RiskCodeObjCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;

/**
 * 协议退保CF
 * 
 * @author 姜士杰 2019-1-14
 *
 */
public class InterfaceXTCF extends InterfaceBaseCF {

	public boolean deal(String xml) {
		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}
		// 处理逻辑
		DealXTCF dealXTCF = new DealXTCF();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		if (!dealXTCF.deal(data)) {
			errorValue = dealXTCF.getErrorValue();
			WorkNo = dealXTCF.getEdorAcceptNo();
			String sql = "";
			try {
				sql = "insert into ErrorInsertLog "
						+ "(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,"
						+ "Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,"
						+ "transactionBatch ,transactionDescription ,transactionAddress ) "
						+ "values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'" + GI.Operator + "','" + pxml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), "
						+ "to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), "
						+ "'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'" + returnXML(pxml) + "','中间状态','"
						+ errorValue + "','" + mMessHead.getMsgType() + "','" + mMessHead.getBranchCode() + "','"
						+ mMessHead.getBatchNo() + "','交易描述','交易地址')";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tExeSQL.execUpdateSQL(sql);
			return false;
		}
		WorkNo = dealXTCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	/**
	 * 解析报文体
	 * 
	 * @throws ParseException
	 * 
	 * @throws Exception
	 */
	public void getkDatabody(Element rootElement) {

		System.out.println("====================================解析报文体========================");

		// 作为页面session值
		GlobalInput GI = new GlobalInput();

		Element XTCF = rootElement.getChild("XTCF");
		// 客户号
		Element msgCustomer = XTCF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = XTCF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = XTCF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();
		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setReason_tb(xxFormData.getChildText("reason_tb"));

		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

		Element XTriskCodeObj = XTCF.getChild("riskCodeObj");
		RiskCodeObjCF XTRiskCodeObj = new RiskCodeObjCF();

		List<RiskCodeObjCF> RiskCodeObjSet = new ArrayList<RiskCodeObjCF>();
		
		List<Element> itemList = XTriskCodeObj.getChildren("Item");
		for (int i = 0; i < itemList.size(); i++) {
			Element item = itemList.get(i);
			RiskCodeObjCF XtRiskCodeObj = new RiskCodeObjCF();
			String RiskCode = item.getChildText("riskCode");
			System.out.println("==============================解析到报文险种代码为1：" + RiskCode);
			XtRiskCodeObj.setPlanCode(item.getChildText("PlanCode"));

			XtRiskCodeObj.setRiskCode(item.getChildText("riskCode"));
			XtRiskCodeObj.setGetMoney(item.getChildText("GetMoney"));
			XtRiskCodeObj.setXTFeeRate(item.getChildText("XTFeeRate"));
			XtRiskCodeObj.setXTFee(item.getChildText("XTFee"));
			RiskCodeObjSet.add(XtRiskCodeObj);
			XTRiskCodeObj.setRiskCodeObjCF(RiskCodeObjSet);
		}
		data.add(tFormData);
		data.add(XTRiskCodeObj);
	}

}
