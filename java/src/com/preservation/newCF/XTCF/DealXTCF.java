package com.preservation.newCF.XTCF;

import java.util.List;

import com.preservation.XT.obj.RiskCodeObj;
import com.preservation.newCF.DealBaseCF;
import com.preservation.newCF.XTCF.obj.RiskCodeObjCF;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.EdorCTTestBL;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GrpEdorXTDetailUI;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LGErrorLogSet;
import com.sinosoft.lis.vschema.LPBudgetResultSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/**
 * 协议退保CF 姜士杰 2019-01-15
 * @author Administrator
 *
 */
public class DealXTCF extends DealBaseCF {

	public boolean checkSave() {

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(EdorAcceptNo, "XT");
		String fmAction = "";
		String FlagStr = "";
		String GrpContNo = mLCGrpContInfo.getGrpContNo();
		ExeSQL tExeSQL = new ExeSQL();
		// 保全项目明细退保试算校验
		System.out.println("================================******退保试算*******====================");
		String sqlLCGrpPol = "select 1 from LCGrpPol where GrpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' "
				+ "and riskcode = '590206' with ur ";
		SSRS sqlLCGrpPolSSRS = tExeSQL.execSQL(sqlLCGrpPol);
		if (sqlLCGrpPolSSRS.getMaxRow() > 0) {
			System.out.println("该保单含有建工险种，不需要退保试算！");
			Content = "该保单含有建工险种，不需要退保试算！";
			return true;
		}

		String sql = "  select grpContNo, grpName, appntNo, HandlerDate, cValiDate, " + "   (select max(payToDate) "
				+ "   from LCPol " + "   where grpContNo = a.grpContNo), cInValiDate, '缴费频次', prem "
				+ "from LCGrpCont a " + "where appFlag = '1' " + " and grpContNo = '" + mLCGrpContInfo.getGrpContNo()
				+ "'" + " and appntNo = '" + mCustomerInfo.getCustomerNo() + "' ";
		SSRS LCGrpContGrid = tExeSQL.execSQL(sql);
		String grpName = LCGrpContGrid.GetText(1, 2);
		String appntNo = LCGrpContGrid.GetText(1, 3);
		String polApplyDate = LCGrpContGrid.GetText(1, 4); // 投保单填写日期
		String cValiDate = LCGrpContGrid.GetText(1, 5); // 保单生效日
		String payToDate = LCGrpContGrid.GetText(1, 6); // 保单交至日期
		String cInValiDate = LCGrpContGrid.GetText(1, 7); // 保单失效日期
		String payToDateLongPol = payToDate; // 长期险预计交至日期

		// 得到保单保全状态
		String edorsql = "  select b.edortype " + "from LPEdorApp a, LPGrpEdorItem b "
				+ "where a.edorAcceptNo = b.edorNo " + "   and b.grpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' "
				+ "   and a.edorState != '0' ";
		SSRS resultSql = tExeSQL.execSQL(edorsql);

		if (resultSql.getMaxRow() > 1) {
			for (int i = 1; i <= resultSql.getMaxRow(); i++) {
				String sttt = resultSql.GetText(i, 1);
				if (!"XT".equals(sttt)) {
					Content = "有正在进行的其它保全作业:" + sttt;
					return false;
				}
			}
		}

		String llcasesql = " select 1 " + " from llregister "
				+ " where customerno in (select insuredno from lcinsured where grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo() + "') and rgtstate != '12' and  rgtstate != '14'";
		SSRS resultsql = tExeSQL.execSQL(llcasesql);
		if (resultsql.getMaxRow() > 0) {
			Content = "有正在进行理赔作业";
			return false;
		}

		// 得到保单险种信息
		String sqlLCGrpPolGrid = "  select a.contPlanCode, "
				+ "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
				+ "   a.riskCode, (select riskName from LMRisk where riskCode = a.riskCode), "
				+ "   count(distinct c.insuredNo), min(CValidate), "
				+ "   sum(c.amnt), min(c.mult), sum(c.prem), (select sum(SumActuPayMoney) from LJAPayPerson where GrpPolNo = c.GrpPolNo), max(c.endDate) ,"
				+ "	case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
				+ ", '', '', '' " + "from LCContPlanRisk a, LCPol c " + "where a.grpContNo = c.grpContNo "
				+ "   and a.contPlanCode = c.contPlanCode " + "   and a.riskCode = c.riskCode "
				+ " and c.riskcode not in (select code from ldcode where codetype='tdbc') " + "   and a.grpContNo = '"
				+ GrpContNo + "' " + "group by c.grpContNo, c.GrpPolNo, a.contPlanCode, a.riskCode, "
				+ "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType ,payintv,payendyear,payendyearflag " + " union "
				+ " select a.contPlanCode, "
				+ "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
				+ "   a.riskCode, (select riskName from LMRisk where riskCode = a.riskCode), "
				+ "   count(distinct c.insuredNo), min(CValidate), " + "   sum(c.amnt), min(c.mult), sum(c.prem), "
				+ " (select sum(SumActuPayMoney) from LJAPayPerson d,lcpol e where d.GrpPolNo = c.GrpPolNo  and d.PolNo=e.PolNo and e.contPlanCode = a.ContPlanCode), max(c.endDate) ,"
				+ "	case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
				+ ", '', '', '' " + "from LCContPlanRisk a, LCPol c " + "where a.grpContNo = c.grpContNo "
				+ "   and a.contPlanCode = c.contPlanCode " + "   and a.riskCode = c.riskCode "
				+ " and c.riskcode in (select code from ldcode where codetype='tdbc') " + "   and a.grpContNo = '"
				+ GrpContNo + "' " + "group by c.grpContNo, c.GrpPolNo, a.contPlanCode, a.riskCode, "
				+ "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType ,payintv,payendyear,payendyearflag ";
		SSRS sqlLCGrpPolGridSSRS = tExeSQL.execSQL(sqlLCGrpPolGrid);

		// 页面上险种信息-mulline选择
		List<RiskCodeObjCF> riskcodelist = mRiskCodeObj.getRiskCodeObj();
		if (sqlLCGrpPolGridSSRS.getMaxRow() > 0) {
			int alse = 0;
			for (int a = 0; a < riskcodelist.size(); a++) {

				String RiskCode = riskcodelist.get(a).getRiskCode();
				String ContPlanCode = riskcodelist.get(a).getPlanCode();

				if ("".equals(RiskCode) || null == RiskCode) {
					System.out.println("险种编码不能为空！");
					Content = "险种编码不能为空！";
					return false;
				}
				if("".equals(ContPlanCode) || null == ContPlanCode){
					System.out.println("保障计划不能为空！");
					Content = "保障计划不能为空！";
					return false;
				}
				
				// 计算理赔额
				String sqlSum = "  select sum(a.pay) " + "from LJAGetClaim a, llcase b, LCPol c "
						+ "where a.otherNo = b.caseNo " + "   and a.polNo = c.polNo " + "   " + "and c.contPlanCode = '"
						+ ContPlanCode + "'  and c.riskCode = '" + RiskCode + "'  and b.endCaseDate >= '" + cValiDate
						+ "'  and b.endCaseDate <= '" + cInValiDate + "' ";
				SSRS result = tExeSQL.execSQL(sqlSum);
				String claimPay = "0";
				if (result.getMaxRow() > 0) {
					claimPay = result.GetText(1, 1);
				}
				for (int i = 1; i <= sqlLCGrpPolGridSSRS.getMaxRow(); i++) {
					String baozhang = sqlLCGrpPolGridSSRS.GetText(i, 1);
					String xianzhong = sqlLCGrpPolGridSSRS.GetText(i, 3);
					System.out.println("=====================================查询到的险种" + i + "：" + xianzhong);

					if (xianzhong.equals(RiskCode) && baozhang.equals(ContPlanCode)) {
						alse++;
					}
					System.out.println("=====================================报文传入险种:" + RiskCode);
				}
			}

			if (alse != riskcodelist.size()) {
				Content = "传入险种或对应保障计划错误！";
				return false;
			}
		}
		// 页面上险种信息-mulline选择

		// SSRS sqlExeSSRS = tExeSQL.execSQL(sqlExe);
		// String[] riskCodes = mFormData.getRiskCode().split(",");
		// String riskCode = "";
		// for (int i = 0; i < riskCodes.length; i++)
		// {
		// riskCode = riskCode + "'" + riskCodes[i] + "',";
		// }
		// riskCode = riskCode.substring(0, riskCode.length() - 1);
		//
		// System.out.println("===========险种个数："+
		// sqlLCGrpPolGridSSRS.getMaxRow());
		// for (int i = 1; i <= sqlLCGrpPolGridSSRS.getMaxRow(); i++)
		// {
		// // 计算理赔额
		// String sqlSum = " select sum(a.pay) "
		// + "from LJAGetClaim a, llcase b, LCPol c "
		// + "where a.otherNo = b.caseNo "
		// + " and a.polNo = c.polNo " + " "
		// + "and c.contPlanCode = '"
		// + sqlLCGrpPolGridSSRS.GetText(i, 1)
		// + "' and c.riskCode = '"
		// + sqlLCGrpPolGridSSRS.GetText(i, 3)
		// + "' and b.endCaseDate >= '"
		// + sqlLCGrpPolGridSSRS.GetText(i, 6)
		// + "' and b.endCaseDate <= '"
		// + sqlLCGrpPolGridSSRS.GetText(i, 11) + "' ";
		// SSRS result = tExeSQL.execSQL(sqlSum);
		// String claimPay = "0";
		// if (result.getMaxRow() > 0)
		// {
		// claimPay = result.GetText(1, 1);
		// }
		//
		// }
		// 保全项目明细退保试算提交后台
		// -------------GEdorBudgetSave.jsp
		String remark = "无";
		String Operator = "86";
		LCPolDB tLCPolDB = new LCPolDB();
		LCPolSet tLCPolSet = null;
		long startTime = System.currentTimeMillis();

		String grpContNo = mLCGrpContInfo.getGrpContNo();
		String edorValiDate = mFormData.getEdorValiDate();
		if (edorValiDate == null || edorValiDate.equals("") || edorValiDate.equals("null")) {
			edorValiDate = mFormData.getEdorValiDate();
		}

		String edorType = mFormData.getEdorType();
		String edorNo = EdorAcceptNo;

		boolean needResultFlag = (edorNo != null && !edorNo.equals("") && edorType != null && !edorType.equals(""));
		EdorCTTestBL tEdorCTTestBL = new EdorCTTestBL();
		tEdorCTTestBL.setEdorValiDate(edorValiDate);
		tEdorCTTestBL.setCurPayToDateLongPol(payToDateLongPol);
		tEdorCTTestBL.setGrpContNo(grpContNo);
		tEdorCTTestBL.setOperator(Operator);
		System.out.println("\n\n\n\n\n\n\n\n" + edorValiDate + " " + payToDateLongPol + " " + grpContNo);

		LPBudgetResultSet tLPBudgetResultSet = new LPBudgetResultSet();
		LGErrorLogSet tLGErrorLogSet = new LGErrorLogSet();
		if (sqlLCGrpPolGridSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= sqlLCGrpPolGridSSRS.getMaxRow(); i++) {
				VData tVData = tEdorCTTestBL.budget(sqlLCGrpPolGridSSRS.GetText(i, 1),
						sqlLCGrpPolGridSSRS.GetText(i, 3), needResultFlag);

				if (tVData == null && tEdorCTTestBL.mErrors.needDealError()) {
					Content = tEdorCTTestBL.mErrors.getErrContent();
					remark += Content;
				} else {
					double getMoney = Double.parseDouble((String) tVData.getObjectByObjectName("String", 0));
					System.out.println("===========-=-=-=-=====-=" + getMoney);
					if (getMoney != 0) {
						getMoney = -getMoney;
					}
					tLPBudgetResultSet.add((LPBudgetResultSet) tVData.getObjectByObjectName("LPBudgetResultSet", 0));
					// qulq 061205
					tLGErrorLogSet.add((LGErrorLogSet) tVData.getObjectByObjectName("LGErrorLogSet", 0));
					if (tLGErrorLogSet.size() > 0) {
						remark = "";
						for (int j = 1; j <= tLGErrorLogSet.size(); j++) {
							remark += tLGErrorLogSet.get(j).getDescribe();
						}
						System.out.println("==============================================" + remark);
					}
				}
			}
			if (needResultFlag) {
				for (int i = 1; i <= tLPBudgetResultSet.size(); i++) {
					tLPBudgetResultSet.get(i).setEdorNo(edorNo);
					tLPBudgetResultSet.get(i).setEdorType(edorType);
					tLPBudgetResultSet.get(i).setOperator(Operator);
				}
				MMap map = new MMap();
				map.put("delete from LPBudgetResult where edorNo = '" + edorNo + "' and edorType = '" + edorType
						+ "' and grpContNo = '" + grpContNo + "' ", "DELETE");
				map.put(tLPBudgetResultSet, "INSERT");

				VData data = new VData();
				data.add(map);
				PubSubmit p = new PubSubmit();
				if (!p.submitData(data, "")) {
					Content += ", 存储试算结果出错。";
				}
			}
		}
		System.out.println(System.currentTimeMillis() - startTime);
		Content = PubFun.changForHTML(Content);

		/**
		 * 保全项目明细中 算费 算费前台校验及提交后台
		 * 
		 */
		System.out.println("=============================******保全明细算费项******=====================");
		String contPlansCode = "";
		for (int i = 0; i < mRiskCodeObj.getRiskCodeObj().size(); i++) {
			contPlansCode += "'" + mRiskCodeObj.getRiskCodeObj().get(i).getPlanCode() + "',";

		}
		contPlansCode.substring(0, contPlansCode.length() - 1);
		// System.out.println("contPlansCode[i]:" + contPlansCode[i]);
		// 算费前台校验及提交后台
		String SQLLCGrpPolGrid = "  select a.contPlanCode, "
				+ "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
				+ "   a.riskCode, (select riskName from LMRisk where riskCode = a.riskCode), "
				+ "   sum(c.prem), sum(c.prem), '', "
				+ "   min(c.CValiDate), max(c.EndDate), min(c.PayToDate), '', '', '' "
				+ "from LCContPlanRisk a, LCPol c " + "where a.grpContNo = c.grpContNo "
				+ "   and a.contPlanCode = c.contPlanCode " + "   and a.riskCode = c.riskCode "
				+ "   and a.grpContNo = '" + GrpContNo + "' " + "group by c.grpContNo, a.contPlanCode, a.riskCode, "
				+ "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType " + "order by a.contPlanCode, a.RiskCode ";
		SSRS sqlContListSSRS = tExeSQL.execSQL(SQLLCGrpPolGrid);
		if (sqlContListSSRS.getMaxRow() < 1) {
			System.out.println("查询列表为空，不可以进行操作！");
			Content = "查询列表为空，不可以进行操作！";
			return false;
		} else {
			for (int i = 1; i <= sqlContListSSRS.getMaxRow(); i++) {
				String xianzhong = sqlContListSSRS.GetText(i, 3);
				System.out.println("=====================================查询到的险种" + i + "：" + xianzhong);
			}
		}

		double[] getMoney = new double[sqlContListSSRS.getMaxRow()];
		double[] XTFee = new double[sqlContListSSRS.getMaxRow()];
		double[] XTFeeRate = new double[sqlContListSSRS.getMaxRow()];
		String[] contPlansCodes = new String[sqlContListSSRS.getMaxRow()];
		String[] riskCodess = new String[sqlContListSSRS.getMaxRow()];
		System.out.println("====================" + sqlContListSSRS.getMaxRow());
		List<RiskCodeObjCF> riskcodelist2 = mRiskCodeObj.getRiskCodeObj();

		System.out.println("//////////////////" + riskcodelist2.size());
		for (int i = 0; i < riskcodelist2.size(); i++) {

			String RiskCode = riskcodelist2.get(i).getRiskCode();

			// String riskcode =
			// mRiskCodeObj.getRiskCodeObj().get(i).getRiskCode();
			String plancode = riskcodelist2.get(i).getPlanCode();
			String GetMoney = riskcodelist2.get(i).getGetMoney();
			getMoney[i] = Double.parseDouble(GetMoney); // 正常解约金额
			XTFee[i] = Double.parseDouble(riskcodelist2.get(i).getXTFee()); // 协议解约金额

			System.out.println("=======================================" + riskcodelist2.get(i).getXTFeeRate());
			if ("".equals(riskcodelist2.get(i).getXTFeeRate())) {
				XTFeeRate[i] = XTFee[i] / getMoney[i]; // 协议解约比例
				System.out.println("===================================" + XTFeeRate[i]);
			} else {
				XTFeeRate[i] = Double.parseDouble(riskcodelist2.get(i).getXTFeeRate()); // 协议节约比例

			}

			contPlansCodes[i] = sqlContListSSRS.GetText(i + 1, 1);
			riskCodess[i] = sqlContListSSRS.GetText(i + 1, 3);

			Double xtProportionCheck = XTFee[i] / getMoney[i];
			Double xtMoneyCheck = getMoney[i] * XTFeeRate[i];
			if (XTFeeRate[i] != xtProportionCheck && XTFee[i] != xtMoneyCheck) {
				System.out.println(
						sqlContListSSRS.GetText(i + 1, 1) + " " + sqlContListSSRS.GetText(i + 1, 3) + "算费结果被修改，请再次算费");
				Content = sqlContListSSRS.GetText(i + 1, 1) + " " + sqlContListSSRS.GetText(i + 1, 3) + "算费结果被修改，请再次算费";
				return false;
			}
			if ("".equals(getMoney) || null == getMoney) {
				System.out.println("请先退保试算");
				Content = "请先退保试算";
				return false;
			}

			if (("".equals(XTFee) || null == XTFee) && ("".equals(XTFeeRate) || null == XTFeeRate)) {
				System.out.println("必须填协议退费金额和协议退费比例其中一项");
				Content = "必须填协议退费金额和协议退费比例其中一项";
				return false;
			}
			// 得到险种总实交保费
			Double sumActuPayMoney = Double.parseDouble(sqlContListSSRS.GetText(i + 1, 6));
			if (!"".equals(XTFee) && null != XTFee) {
				double proportion = XTFee[i] / sumActuPayMoney;
				if (proportion > 1.1) {
					System.out.println("协议退保比例不能超过总保费的110%");
					Content = "协议退保比例不能超过总保费的110%";
					return false;
				}
				XTFeeRate[i] = XTFee[i] / getMoney[i];
			} else if (!"".equals(XTFeeRate) && null != XTFeeRate) {
				double money = XTFeeRate[i] * getMoney[i];

				if (money > sumActuPayMoney * 1.1) {
					System.out.println("协议退保比例不能超过总保费的110%");
					Content = "协议退保比例不能超过总保费的110%";
					return false;
				}
				XTFee[i] = getMoney[i] * XTFeeRate[i];
			}

			// 保存按钮的校验
			if ("".equals(XTFee) || null == XTFee || "".equals(XTFeeRate) || null == XTFeeRate) {
				System.out.println("请先算费");
				Content = "请先算费";
				return false;
			}
			if ("".equals(mFormData.getReason_tb()) || null == mFormData.getReason_tb()) {
				System.out.println("请填写退保原因");
				Content = "请填写退保原因";
				return false;
			}

			// 保存按钮调用方法
			fmAction = "INSERT||EDORXT";
			tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
			tLPGrpEdorItemSchema.setEdorType("XT");
			tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
			tLPGrpEdorItemSchema.setReasonCode(mFormData.getReason_tb());

			// 按照险种退保
			tEdorItemSpecialData.setGrpPolNo(contPlansCodes[i] + "," + riskCodess[i]);
			tEdorItemSpecialData.add(BQ.DETAILTYPE_XTFEEG, "-" + XTFee[i]);
			tEdorItemSpecialData.add(BQ.XTFEERATEG, String.valueOf(XTFeeRate[i]));

		}

		GrpEdorXTDetailUI tPGrpEdorXTDetailUI = new GrpEdorXTDetailUI();
		try {
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.addElement(GI);
			tVData.addElement(tLPGrpEdorItemSchema);
			tVData.add(tEdorItemSpecialData);
			tPGrpEdorXTDetailUI.submitData(tVData, fmAction);
		} catch (Exception ex) {
			Content = fmAction + "失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			if (!tPGrpEdorXTDetailUI.mErrors.needDealError()) {
				Content = " 保存成功";
				FlagStr = "Success";
			} else {
				Content = " 保存失败，原因是:" + tPGrpEdorXTDetailUI.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}

		return true;

	}

}
