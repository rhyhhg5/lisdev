package com.preservation.newCF.XTCF.obj;

import java.io.Serializable;
import java.util.List;

public class RiskCodeObjCF implements Serializable{
	private String PlanCode;
	private String riskCode;//
	private String GetMoney;//正常解约金额
	private String XTFee;//协议解约金额
	private String XTFeeRate;//协议解约比例
	private String groupFixMoney;//团体账户领取金
	private List<RiskCodeObjCF> riskCodeObj;
	public String getPlanCode()
	{
		return PlanCode;
	}
	public void setPlanCode(String planCode)
	{
		PlanCode = planCode;
	}
	public String getRiskCode()
	{
		return riskCode;
	}
	public void setRiskCode(String riskCode)
	{
		this.riskCode = riskCode;
	}
	public String getGetMoney()
	{
		return GetMoney;
	}
	public void setGetMoney(String getMoney)
	{
		GetMoney = getMoney;
	}
	public String getXTFee()
	{
		return XTFee;
	}
	public void setXTFee(String xTFee)
	{
		XTFee = xTFee;
	}
	public String getXTFeeRate()
	{
		return XTFeeRate;
	}
	public void setXTFeeRate(String xTFeeRate)
	{
		XTFeeRate = xTFeeRate;
	}
	public String getGroupFixMoney() {
		return groupFixMoney;
	}
	public void setGroupFixMoney(String groupFixMoney) {
		this.groupFixMoney = groupFixMoney;
	}
	public List<RiskCodeObjCF> getRiskCodeObj() {
		return riskCodeObj;
	}
	public void setRiskCodeObjCF(List<RiskCodeObjCF> riskCodeObjSet) {
		this.riskCodeObj = riskCodeObjSet;
	}
	
}
