package com.preservation.newCF.WDCF;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.obj.CustomerInfo;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class WDCFInterface {

	String errorValue;
	private MessHead head = new MessHead();
	VData data = new VData();
	private String returnXML;

	// 处理请求报文
	public boolean deal(String xml) {
		// 得到并解析数据
		if (!getkData(xml)) {
			returnXML = creatReturnXML();
			return false;
		}
		returnXML = creatReturnXML();
		// 启动线程，逻辑处理类，生成返回报文，调用东软接口
		if ("".equals(errorValue) || null == errorValue) {
			WDCFForReturn WDForReturn = new WDCFForReturn();
			WDForReturn.setData(data);
			WDForReturn.setXml(xml);
			Thread t = new Thread(WDForReturn);
			t.start();
		}
		return true;
	}

	/**
	 * 解析报文
	 * @param xml
	 * @return
	 */
	public boolean getkData(String xml) {
		StringReader reader = new StringReader(xml);
		try {
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);

			// 作为页面session值
			GlobalInput GI = new GlobalInput();
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");

			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");

			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			Element eBody = rootElement.getChild("MsgBody");
			Element eWD = eBody.getChild("WDCF");
			// 团单信息
			Element lCGrpContInfo = eWD.getChild("LCGrpContTable");

			Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");

			String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
			String WorkNo = eLCGrpContInfo.getChildText("WorkNo");
			GI.ComCode = eLCGrpContInfo.getChildText("ManageCom");
			GI.ManageCom = eLCGrpContInfo.getChildText("ManageCom");
//			GI.Operator = "pa0001";
			GI.Operator = "BAOQA";
			data.add(GI);
			
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ GI.Operator
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ "WDCF" + "','" + GI.ManageCom + "','" 
					+ "保全实名化撤销开始','" + WorkNo + "')";

			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);

			String SQLCustomerNo = "select AppntNo from LCGrpCont where GrpContNo = '" + GrpContNo + "'";
			SSRS sSQLCustomerNo = new ExeSQL().execSQL(SQLCustomerNo);
			if (sSQLCustomerNo.getMaxRow() < 1) {
				errorValue = "保单号：" + GrpContNo + "下没有信息";
				return false;
			}
			String CustomerNo = sSQLCustomerNo.GetText(1, 1);
			String GrpName = eLCGrpContInfo.getChildText("GrpName");
			String ExcelURL = eLCGrpContInfo.getChildText("Excel");
			if ("".equals(CustomerNo) || null == CustomerNo) {
				errorValue = "查询的客户号码为空";
				return false;
			}
			if ("".equals(GrpContNo) || null == GrpContNo) {
				errorValue = "集体合同号码不能为空";
				return false;
			}
			if ("".equals(GrpName) || null == GrpName) {
				errorValue = "机构名称不能为空";
				return false;
			}
			if ("".equals(ExcelURL) || null == ExcelURL) {
				errorValue = "FTP地址不能为空";
				return false;
			}

			// 查询作业表是否生成工单号
			String sql = "select * from LGWork where CustomerNo='" + CustomerNo + "' and WorkNo='" + WorkNo + "'";
			// 获取工单号
			ExeSQL tExeSQL = new ExeSQL();
			SSRS sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0) {
				WorkNo = sqlSSRS.GetText(1, 2);
				String str = sqlSSRS.GetText(1, 5);
				if ("5".equals(str)) {
					errorValue = "此工单号" + WorkNo + "已结案！";
					return false;
				}
			} else {
				errorValue = "客户号" + CustomerNo + "未生成工单号！";
				return false;
			}
			System.out.println("********工单号EdorAcceptNo：" + WorkNo);

			CustomerInfo iCustomerInfo = new CustomerInfo();
			iCustomerInfo.setWorkNo(WorkNo);
			data.add(iCustomerInfo);

			String FileName = ExcelURL.split("/")[ExcelURL.split("/").length - 1];
			String FTPPath = ExcelURL.substring(0, ExcelURL.length() - FileName.length());
			// FTP下载人员文件.xls
			String sqlFTP = "select DESCRIPTION,CODE from ldconfig where codetype = 'WSInterface'";
			SSRS ssqlFTP = new ExeSQL().execSQL(sqlFTP);
			if (ssqlFTP.getMaxRow() != 4) {
				errorValue = "从下载WS无名单实名化人员信息文件的FTP信息 配置错误。检查数据库配置";
				return false;
			}
			String Ip = null;
			String AccName = null;
			String PassWord = null;
			String Port = null;
			String rootPath = getClass().getResource("/").getFile().toString();
			String BasePath = rootPath.replaceAll("WEB-INF/classes/", "");
			BasePath += "temp_lp/";

			for (int i = 1; i <= ssqlFTP.getMaxRow(); i++) {
				if ("IP".equals(ssqlFTP.GetText(i, 1))) {
					Ip = ssqlFTP.GetText(i, 2);
				}
				if ("AccName".equals(ssqlFTP.GetText(i, 1))) {
					AccName = ssqlFTP.GetText(i, 2);
				}
				if ("PassWord".equals(ssqlFTP.GetText(i, 1))) {
					PassWord = ssqlFTP.GetText(i, 2);
				}
				if ("Port".equals(ssqlFTP.GetText(i, 1))) {
					Port = ssqlFTP.GetText(i, 2);
				}
			}

			FTPTool tFTPTool = new FTPTool(Ip, AccName, PassWord, Integer.valueOf(Port));
			try {
				if (!tFTPTool.loginFTP()) {
					errorValue = "登陆FTP失败：" + tFTPTool.getErrContent(1);
					return false;
				} else {
					tFTPTool.downloadFile(FTPPath, BasePath, FileName);
				}
			} catch (Exception e) {
				errorValue = "FTP下载失败：";
				e.printStackTrace();
				return false;
			}
			// 读取xls 解析得到数据
			LCInsuredSet tLCInsuredSet = new LCInsuredSet();
			try {
				// OPCPackage pkg = OPCPackage.open(BasePath + FileName);
				try {

					FileInputStream in = new FileInputStream(BasePath + FileName);
					HSSFWorkbook book = new HSSFWorkbook(in);
					HSSFSheet sheet = book.getSheetAt(0);
					System.out.println("*************************XSSFWorkbook");
					int firstRowNum = sheet.getFirstRowNum();// 最小行数
					int lastRowNum = sheet.getLastRowNum();// 最大行数
					short lastCellNum = sheet.getRow(firstRowNum).getLastCellNum();// 最大列数
					if (lastCellNum != (short) 3) {
						errorValue = "xls数据不正确。请检查列数";
						return false;
					}
					for (int i = firstRowNum + 1; i <= lastRowNum; i++) {
						String Name = sheet.getRow(i).getCell((short) 0).getStringCellValue();
						System.out.println("***************:" + Name);
						if (null == Name || "".equals(Name)) {
							errorValue = "姓名 不能为空:xls中第" + (i + 1) + "行 第1列";
							return false;
						}
						String IDType = sheet.getRow(i).getCell((short) 1).getStringCellValue();
						System.out.println("***************:" + IDType);
						if (null == IDType || "".equals(IDType)) {
							errorValue = "证件类型 不能为空:xls中第" + (i + 1) + "行 第2列";
							return false;
						}
						String IDNo = sheet.getRow(i).getCell((short) 2).getStringCellValue();
						System.out.println("***************:" + IDNo);
						if (null == IDNo || "".equals(IDNo)) {
							errorValue = "证件号码 不能为空:xls中第" + (i + 1) + "行 第3列";
							return false;
						}
						String SQLInsuredNo = " select ContNo,InsuredNo from LCInsured where GrpContNo='" + GrpContNo
								+ "'  and Name='" + Name + "' and IDType='" + IDType + "' and IDNo='" + IDNo + "'";
						SSRS ssrscount = new ExeSQL().execSQL(SQLInsuredNo);
						if (ssrscount.getMaxRow() > 0) {
							LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
							tLCInsuredSchema.setGrpContNo(GrpContNo);
							tLCInsuredSchema.setContNo(ssrscount.GetText(1, 1));
							tLCInsuredSchema.setInsuredNo(ssrscount.GetText(1, 2));
							tLCInsuredSet.add(tLCInsuredSchema);
						}
					}
					data.add(tLCInsuredSet);

				} catch (Exception e) {
					FileInputStream in = new FileInputStream(BasePath + FileName);

					// String[] headers2 = { "被保人姓名", "证件类型", "证件号" };
					HSSFWorkbook book = new HSSFWorkbook(in);
					System.out.println("*************************HSSFWorkbook");
					HSSFSheet sheet = book.getSheetAt(0);
					int firstRowNum = sheet.getFirstRowNum();// 最小行数
					int lastRowNum = sheet.getLastRowNum();// 最大行数
					short lastCellNum = sheet.getRow(firstRowNum).getLastCellNum();// 最大列数
					if (lastCellNum != (short) 3) {
						errorValue = "xls数据不正确。请检查列数";
						return false;
					}
					for (int i = firstRowNum + 1; i <= lastRowNum; i++) {
						String Name = sheet.getRow(i).getCell((short) 0).getStringCellValue();
						System.out.println("***************:" + Name);
						if (null == Name || "".equals(Name)) {
							errorValue = "姓名 不能为空:xls中第" + (i + 1) + "行 第1列";
							in.close();
							return false;
						}
						String IDType = sheet.getRow(i).getCell((short) 1).getStringCellValue();
						System.out.println("***************:" + IDType);
						if (null == IDType || "".equals(IDType)) {
							errorValue = "证件类型 不能为空:xls中第" + (i + 1) + "行 第2列";
							in.close();
							return false;
						}
						String IDNo = sheet.getRow(i).getCell((short) 2).getStringCellValue();
						System.out.println("***************:" + IDNo);
						if (null == IDNo || "".equals(IDNo)) {
							errorValue = "证件号码 不能为空:xls中第" + (i + 1) + "行 第3列";
							in.close();
							return false;
						}
						String SQLInsuredNo = " select ContNo,InsuredNo from LCInsured where GrpContNo='" + GrpContNo
								+ "' and Name='" + Name + "' and IDType='" + IDType + "' and IDNo='" + IDNo + "'";
						SSRS ssrscount = new ExeSQL().execSQL(SQLInsuredNo);
						LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
						tLCInsuredSchema.setGrpContNo(GrpContNo);
						tLCInsuredSchema.setContNo(ssrscount.GetText(1, 1));
						tLCInsuredSchema.setInsuredNo(ssrscount.GetText(1, 2));
						tLCInsuredSet.add(tLCInsuredSchema);
					}

					data.add(tLCInsuredSet);
					in.close();
				} finally {
					File file = new File(BasePath + FileName);
					file.delete();
				}
			} catch (Exception e) {
				e.printStackTrace();
				errorValue = "解析报文错误";
				return false;
			}

			LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
			iLCGrpContInfo.setCustomerNo(CustomerNo);
			iLCGrpContInfo.setGrpContNo(GrpContNo);
			iLCGrpContInfo.setGrpName(GrpName);
			data.add(iLCGrpContInfo);
			
			String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ GI.Operator
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ "WDCF" + "','" + GI.ManageCom + "','" 
					+ "保全实名化撤销读取报文结束','" + WorkNo + "')";

			ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);

		} catch (Exception e) {
			e.printStackTrace();
			errorValue = "解析报文错误";
			return false;
		} finally {
			reader.close();
		}
		
		return true;

	}

	// 第一次返回报文
	private String creatReturnXML() {

		String responseXml = null;

		try {
			MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
			Element root = new Element("DataSet");
			Document doc = new Document(root);
			Element eMsgHead = new Element("MsgHead");
			Element eHeadnode = new Element("Item");
			Element eHeadBatchNo = new Element("BatchNo");
			eHeadBatchNo.setText(vMessHead.getBatchNo());
			eHeadnode.addContent(eHeadBatchNo);
			Element eHeadSendDate = new Element("SendDate");
			eHeadSendDate.setText(vMessHead.getSendDate());
			eHeadnode.addContent(eHeadSendDate);
			Element eHeadSendTime = new Element("SendTime");
			eHeadSendTime.setText(vMessHead.getSendTime());
			eHeadnode.addContent(eHeadSendTime);
			Element eHeadBranchCode = new Element("BranchCode");
			eHeadBranchCode.setText(vMessHead.getBranchCode());
			eHeadnode.addContent(eHeadBranchCode);
			Element eHeadSendOperator = new Element("SendOperator");
			eHeadSendOperator.setText(vMessHead.getSendOperator());
			eHeadnode.addContent(eHeadSendOperator);
			Element eHeadMsgType = new Element("MsgType");
			eHeadMsgType.setText(vMessHead.getMsgType());
			eHeadnode.addContent(eHeadMsgType);

			if (!"".equals(errorValue) && null != errorValue) {
				Element eHeadState = new Element("State");
				eHeadState.setText("01");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("接收解析报文失败：" + errorValue);
				eHeadnode.addContent(eHeadErrInfo);
			} else {
				Element eHeadState = new Element("State");
				eHeadState.setText("00");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("接收并解析报文成功！");
				eHeadnode.addContent(eHeadErrInfo);
			}

			eMsgHead.addContent(eHeadnode);

			root.addContent(eMsgHead);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(doc);

		} catch (IOException e) {
			responseXml = "生成返回报文失败";
			e.printStackTrace();
		}
		return responseXml;
	}

	public String getReturnXML() {
		return returnXML;
	}

	public void setReturnXML(String returnXML) {
		this.returnXML = returnXML;
	}

}
