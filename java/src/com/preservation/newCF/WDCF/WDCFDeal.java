package com.preservation.newCF.WDCF;

import java.io.IOException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.preservation.newCF.GEdorCancelBQ;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqConfirmUI;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.FeeNoticeGrpVtsUI;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.GrpEdorWDDetailUI;
import com.sinosoft.lis.bq.PEdorAppAccConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 无名单删除 返回解析报文成功后的线程操作CF
 * 
 * @author 姜士杰 2018-12-20
 *
 */

public class WDCFDeal {

	String errorValue;
	String returnxml;
	VData data;
	String EdorValiDate = "";
	private String mCurrDate = PubFun.getCurrentDate();
	GlobalInput GI = new GlobalInput();
	public CustomerInfo mCustomerInfo;
	// 生成的工单号
	String tWorkNo = "";
	String tAccpetNo = "";
	String tDetailWorkNo = "";
	String GrpContNo = "";
	FormData dFormData = new FormData();
	String ContType = "2";// GEdorInput.jsp页面隐藏赋值为 2

	public boolean deal(VData vdata, String xml) {

		data = vdata;
		boolean flagBack = false;
		GEdorCancelBQ tGEdorCancelBQ = new GEdorCancelBQ();
		VData tt = new VData();

		try {

			if (!gateAndData()) {
				return false;
			} 

			if (!doBusiness()) {
				tt.add(GI);
				tt.add(tAccpetNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				flagBack = true;
				return false;
			} 

			if (!addRecord()) {
				tt.add(GI);
				tt.add(tAccpetNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				flagBack = true;
				return false;
			} 

			if (!saveRecord()) {
				tt.add(GI);
				tt.add(tAccpetNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				flagBack = true;
				return false;
			} 
			
			if (!edorDetail()) {
				tt.add(GI);
				tt.add(tAccpetNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				flagBack = true;
				return false;
			} 

			if (!edorAppConfirm()) {
				tt.add(GI);
				tt.add(tAccpetNo);
				tGEdorCancelBQ.cancelAppEdor(tt);
				flagBack = true;
				return false;
			} 
			
		} finally {

//			// 回滚
//			if (flagBack) {
//				rollBack();
//				if ("".equals(errorValue) || null == errorValue) {
//					errorValue = "未知错误！";
//				}
//			}

			saveLog(flagBack, xml);
			returnxml = creatReturnxml();
		}

		return true;
	}

	/**
	 * 获取报文工单号
	 * @return
	 */
	private boolean gateAndData() {

		MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		mCustomerInfo = (CustomerInfo) data.getObjectByObjectName("CustomerInfo", 0);
		// 获取工单号
		tAccpetNo = mCustomerInfo.getWorkNo();
		return true;
	}

	/**
	 * 经办验证
	 * @return
	 */
	public boolean doBusiness() {

		System.out.println("**************经办 ****************");
		LCGrpContInfo vLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
	    String customerNo = vLCGrpContInfo.getCustomerNo();// 客户号
		GrpContNo = vLCGrpContInfo.getGrpContNo();// 集体合同号码
		System.out.println("customerNo:"+customerNo);
		System.out.println("GrpContNo:"+GrpContNo);

		// 有效保单或可回退保单 判断
		StringBuffer sql = new StringBuffer();
		if (customerNo.length() == 8) {
			sql.append(" select grpContNo  from LCGrpCont  where appntNo = '" + customerNo + "' ");
			sql.append(" and appFlag = '1'  and (StateFlag is null or StateFlag not in('0')) ");
			// 保全回退添加
			sql.append(" union select grpcontno from LBGrpCont b where appntNo = '" + customerNo + "' ");
			sql.append(
					" and exists (select 1 from lpgrpedoritem where grpcontno =b.grpcontno and edorno = b.edorno and edortype in ('WT','CT',''))");
		} else {
			sql.append("  select contNo " + "from LCCont " + "where appntNo = '" + customerNo + "' "
					+ "   and appFlag = '1' ");
			sql.append("   and (StateFlag is null or StateFlag not in('0')) ");
			// 保全回退添加
			sql.append(" union select contno from lbcont b where appntNo = '" + customerNo + "' ");
			sql.append(
					" and exists (select 1 from lpedoritem where contno = b.contno and edorno = b.edorno and edortype in ('WT','CT','XT'))");
		}

		ExeSQL tExeSQL1 = new ExeSQL();
		SSRS ssrssql = tExeSQL1.execSQL(sql.toString());

		if (ssrssql.getMaxRow() < 1) {
			errorValue = "客户" + customerNo + "没有有效保单或可回退保单。";
			return false;
		}

		// 判断客户号和保单号是否一致
		StringBuffer sqlCusnoGrpconNo = new StringBuffer();
		sqlCusnoGrpconNo.append(
				"select 1 from LCGrpCont where AppntNo ='" + customerNo + "' and GrpContNo = '" + GrpContNo + "' ");
		ExeSQL exeCusnoGrpconNo = new ExeSQL();
		SSRS ssrsCusnoGrpconNo = exeCusnoGrpconNo.execSQL(sqlCusnoGrpconNo.toString());

		if (ssrsCusnoGrpconNo.getMaxRow() < 1) {
			errorValue = "客户号" + customerNo + "下没有团体保单号为" + GrpContNo + "的信息";
			return false;
		}

		// 分投保人，主被保人和其它被保人三种情况
		// 保单状态判断
		StringBuffer strSQL = new StringBuffer();
		strSQL.append("select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, a.cinvalidate, ");
		strSQL.append(
				"a.PaytoDate, a.Prem, a.Amnt, codeName('stateflag', StateFlag), b.PostalAddress, case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end ");
		strSQL.append("from   LCCont a, LCAddress b ");
		strSQL.append("where  a.AppntNo = b.CustomerNo ");
		strSQL.append("and    (a.AppntNo = '" + customerNo + "' ");
		strSQL.append(" or    a.InsuredNo = '" + customerNo + "') ");
		strSQL.append("and    a.AppFlag = '1' ");
		strSQL.append("and    b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ");
		strSQL.append("and   (a.StateFlag is null or a.StateFlag not in('0')) "); // 未终止
		// 保全回退需要查询退保保单
		strSQL.append(" union ");
		strSQL.append(
				" select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, (select edorvalidate from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno),");
		strSQL.append(
				" a.PaytoDate, a.Prem, a.Amnt,(select case edortype when 'WT' THEN '犹豫期退保' when 'CT' then '解约' end from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno), b.PostalAddress, ");
		strSQL.append(" case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end ");
		strSQL.append(" from LBCont a, LCAddress b ");
		strSQL.append(" where  a.AppntNo = b.CustomerNo and a.appntno = '" + customerNo + "' ");
		strSQL.append(" and b.AddressNo = (select AddressNo from LBAppnt where ContNo = a.ContNo) and exists ( ");
		strSQL.append(
				" select 1 from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno)");

		ExeSQL tExeSQL = new ExeSQL();
		SSRS ssrsstrSQL = tExeSQL.execSQL(strSQL.toString());

		if (ssrsstrSQL.MaxRow > 0) {
			String ContNo = ssrsstrSQL.GetText(1, 1);
			System.out.println("ContNo:" + ContNo);
			System.out.println("查询sql的值：" + ContNo);
			if (ContNo != null && ContNo != "") {
				StringBuffer sqlContNo = new StringBuffer();
				sqlContNo.append(
						"select StateType,State from LCContState where contno='" + ContNo + "' and PolNo='000000'");
				ExeSQL exeContNo = new ExeSQL();
				SSRS ssrssContNo = exeContNo.execSQL(sqlContNo.toString());
				if (ssrssContNo.getMaxRow() > 0) {
					if (ssrssContNo.GetText(1, 1) == "Available" && ssrssContNo.GetText(1, 2) == "1") {
						errorValue = "保单" + ContNo + "处于失效状态!";
						return false;
					}
					if (ssrssContNo.GetText(1, 1) == "Terminate" && ssrssContNo.GetText(1, 2) == "1") {
						errorValue = "保单" + ContNo + "处于失满期终止状态!";
						return false;
					}
				}
			}
		}

		// 保单状态判断
		StringBuffer sqlGrpCont = new StringBuffer();
		sqlGrpCont.append(" select a.GrpContNo, a.PrtNo, a.GrpName, a.Peoples2, ");
		sqlGrpCont.append(" a.PolApplyDate, a.CValiDate, a.PayMode, a.Prem, a.Amnt, ");
		sqlGrpCont.append(" codeName('stateflag', a.StateFlag),  b.GrpAddress, ");
		sqlGrpCont.append(
				" case when exists (select 1 from LCCont where grpContNo = a.grpContNo and polType = '1') then '无名单' else '非无名单' end ");
		sqlGrpCont.append(" from LCGrpCont a, LCGrpAddress b ");
		sqlGrpCont.append(" where a.AppntNo = b.CustomerNo ");
		sqlGrpCont.append(" and   a.AddressNo = b.AddressNo ");
		sqlGrpCont.append(" and   a.AppntNo ='" + customerNo + "' ");
		sqlGrpCont.append(" and   (a.StateFlag is null or a.StateFlag not in('0')) ");// 未终止
		sqlGrpCont.append(" and   a.AppFlag = '1'");
		sqlGrpCont.append(" and (state is null or state not in('03030002')) ");// 非续保终止
		sqlGrpCont.append(" and not exists ");
		sqlGrpCont.append(" (select 1 from LPEdorEspecialData a, LGWork b ");
		sqlGrpCont.append(" where a.EdorNo = b.WorkNo and b.ContNo = a.GrpContNo ");
		sqlGrpCont.append(" and a.EdorType = 'MJ' ");
		sqlGrpCont.append(" and a.DetailType = 'MJSTATE' ");
		sqlGrpCont.append(" and a.EdorValue != '1' and a.EdorValue != '0')"); // 已满期理算

		ExeSQL exeContNo = new ExeSQL();
		SSRS ssrssContNo = exeContNo.execSQL(sqlGrpCont.toString());

		if (ssrssContNo.getMaxRow() > 0) {

			// 生效日期
			EdorValiDate = ssrssContNo.GetText(1, 6);
			System.out.println("EdorValiDate=" + EdorValiDate);

			String grpcontno = ssrssContNo.GetText(1, 1);
			System.out.println("grpcontno:" + grpcontno);

			if (grpcontno != null && grpcontno != "") {
				StringBuffer sqlgrpcontno = new StringBuffer();
				sqlgrpcontno.append("select StateType,State from LCGrpContState where GrpContNo='" + grpcontno
						+ "' and GrpPolNo='000000'");
				ExeSQL exegrpcontno = new ExeSQL();
				SSRS ssrssgrpcontno = exegrpcontno.execSQL(sqlgrpcontno.toString());

				if (ssrssgrpcontno.getMaxRow() > 0) {
					if (ssrssgrpcontno.GetText(1, 1) == "Terminate" && ssrssgrpcontno.GetText(1, 2) == "1") {
						errorValue = "保单" + grpcontno + "处于满期终止状态!";
						return false;
					}
					if (ssrssgrpcontno.GetText(1, 1) == "Pause" && ssrssgrpcontno.GetText(1, 2) == "2") {
						errorValue = "保单" + grpcontno + "处于暂停状态!";
						return false;
					}
				}
			}
		}

		String strsql = "select * from LGWork where CustomerNo='" + customerNo + "' and WorkNo='" + tAccpetNo + "'";

		ExeSQL stSQL = new ExeSQL();
		SSRS sqlSSRS = tExeSQL.execSQL(strsql);

		if (sqlSSRS.getMaxRow() > 0) {
			tWorkNo = sqlSSRS.GetText(1, 1);
			String str = sqlSSRS.GetText(1, 5);
			if ("5".equals(str)) {
				errorValue = "此工单号" + tAccpetNo + "已结案！";
				return false;
			}
			tDetailWorkNo = sqlSSRS.GetText(1, 6);
		} else {
			errorValue = "此客户号" + customerNo + "未生成工单号！";
			return false;
		}

		return true;
	}

	/**
	 * 添加保全项 前台
	 * @return
	 */
	public boolean addRecord() {

		System.out.println("**************添加保全项目 ****************");

		// 添加保全项目 前 查询数据
		LCGrpContInfo vLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
//		String GrpContNo = vLCGrpContInfo.getGrpContNo();

		// 查询团体保单信息
		StringBuffer sqlhascontno = new StringBuffer();
		sqlhascontno.append(
				"select a.grpcontno,a.grpname,b.LinkMan1,a.CValiDate,a.prem,a.amnt,a.Peoples2,a.Operator,a.Managecom,a.agentcode,a.AppntNo");
		sqlhascontno.append(" from lcgrpcont a,LCGrpAddress b where a.grpcontno='" + GrpContNo
				+ "' and a.appflag='1' and a.AppntNo=b.CustomerNo and a.AddressNo = b.AddressNo");
		ExeSQL exehasgrpcontno = new ExeSQL();
		SSRS ssrscontno = exehasgrpcontno.execSQL(sqlhascontno.toString());

		if (ssrscontno.getMaxRow() < 1) {

			StringBuffer sqlhascontno2 = new StringBuffer();
			sqlhascontno2.append(
					"select a.grpcontno,a.grpname,a.grpname,a.CValiDate,a.prem,a.amnt,a.Peoples2,a.Operator,a.Managecom,a.agentcode,a.AppntNo");
			sqlhascontno2.append(" from lcgrpcont a where a.grpcontno='" + GrpContNo + "' and a.appflag='1'");

			ExeSQL exehasgrpcontno2 = new ExeSQL();
			SSRS ssrscontno2 = exehasgrpcontno2.execSQL(sqlhascontno2.toString());

			if (ssrscontno2.getMaxRow() < 1) {
				errorValue = "未查到团体保单号为" + GrpContNo + "的团体保单！";
			}
		}

		// 查询保全项目，
		String tEdorAcceptNo = tAccpetNo;
		String tEdorNo = tAccpetNo;

		// 险种信息初始化
		if (("".equals(tEdorAcceptNo) && null != tEdorAcceptNo) || ("".equals(tEdorNo) && null != tEdorNo)) {
			errorValue = "生成保全受理号失败！";
			return false;
		}

		// 判断保单是否有万能险种
		StringBuffer sqlhasULIRisk = new StringBuffer();
		sqlhasULIRisk.append("select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"
				+ GrpContNo + "' and RiskType4='4' with ur ");

		ExeSQL exehasULIRisk = new ExeSQL();
		SSRS ssrshasULIRisk = exehasULIRisk.execSQL(sqlhasULIRisk.toString());

		if (ssrshasULIRisk.getMaxRow() > 0) {

			// 解约保全生效日期必须大于或者等于保单最后一次月结日期
			StringBuffer sqlcheckHaveEdor = new StringBuffer();
			sqlcheckHaveEdor
					.append("select edoracceptno from lpgrpedoritem where grpcontno='" + GrpContNo + "' and exists ( ");
			sqlcheckHaveEdor.append(
					" select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0' )  ");

			ExeSQL execheckHaveEdor = new ExeSQL();
			SSRS ssrscheckHaveEdor = execheckHaveEdor.execSQL(sqlcheckHaveEdor.toString());

			if (ssrscheckHaveEdor.getMaxRow() > 0) {
				errorValue = "该团险万能的保单下存在其他未结案的保全项目，不能继续操作！";
				return false;
			}

		}

		if (!checkGrpEdorType()) {
			return false;
		}

		StringBuffer sqldealing = new StringBuffer();
		sqldealing.append("select state from lcgrpbalplan where grpcontno ='" + GrpContNo + "'");

		ExeSQL exedealing = new ExeSQL();
		SSRS ssrsdealing = exedealing.execSQL(sqldealing.toString());

		if (ssrsdealing.getMaxRow() > 0) {
			if (ssrsdealing.GetText(1, 1) != "0") {
				errorValue = "该集体合同目前正在进行定期结算!不能进行保全操作!";
				return false;
			}
		}

		// 校验保全生效日是否在暂停期
		if (!"".equals(EdorValiDate) && null != EdorValiDate) {
			StringBuffer sqlIsRSRR = new StringBuffer();
			sqlIsRSRR
					.append("select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and enddate is not null and startdate<='"
							+ EdorValiDate + "' and enddate>'" + EdorValiDate + "' and grpcontno ='" + GrpContNo + "'");

			ExeSQL exeIsRSRR = new ExeSQL();
			SSRS ssrsIsRSRR = exeIsRSRR.execSQL(sqlIsRSRR.toString());

			if (ssrsIsRSRR.getMaxRow() > 0) {
				errorValue = "生效日期不可选在该集体合同暂停区间内!";
				return false;
			}
		} else {
			System.out.println("EdorValiDate 为空");
			EdorValiDate = mCurrDate;
		}

		// AddRecordSave.java使用
		dFormData.setEdorValiDate(EdorValiDate);
		dFormData.setEdorAppDate(EdorValiDate);
		dFormData.setEdorType("WD");

		// AddRecordSave.java使用
		dFormData.setEdorNo(tEdorNo);
		dFormData.setEdorAcceptNo(tEdorNo);
		data.add(dFormData);

		return true;
	}

	/**
	 * 添加保全项 后台
	 * @return
	 */
	public boolean checkGrpEdorType() {

		ExeSQL tExeSQL = new ExeSQL();

		String checkSQL = "select distinct b.EdorCode, b.EdorName from LMRiskEdoritem  a, LMEdorItem b where a.edorCode ="
				+ " b.edorCode and b.edorcode != 'XB'   and a.riskCode in  (select riskCode from LCGrpPol where "
				+ "grpContNo = '" + GrpContNo
				+ "')  and (b.edorTypeFlag != 'N' or b.edorTypeFlag is null)order by EdorCode";

		SSRS checkSQLSSRS = tExeSQL.execSQL(checkSQL);

		System.out.println("================可添加:" + checkSQLSSRS.getMaxRow() + "个保全项");

		int total = 0;
		for (int i = 1; i <= checkSQLSSRS.getMaxRow(); i++) {
			System.out.println("=============" + checkSQLSSRS.GetText(i, 1) + "===" + "WD");
			if ("WD".equals(checkSQLSSRS.GetText(i, 1))) {
				total = 1;
			}
		}

		if (total <= 0) {
			errorValue = "您的保单投保,有险种的[WD]保全功能暂未上线，无法添加保全项目！";
			return false;
		}
		return true;
	}

	/**
	 * 保全明细录入
	 * @return
	 */
	public boolean saveRecord() {

		System.out.println("**************保全项目明细 ****************");
		
		ExeSQL ExeSQL = new ExeSQL();
		String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ GI.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WDCF" + "','" + GI.ManageCom + "','" 
				+ "保全实名化撤销开始导入','" + tAccpetNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		
		// 输入参数
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
		LCGrpContInfo vLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		String GrpContNo = vLCGrpContInfo.getGrpContNo();
		TransferData tTransferData = new TransferData();
		GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();

		// 后面要执行的动作：添加，修改，删除
		String fmAction = "INSERT||GRPEDORITEM";
		FormData dFormData = (FormData) data.getObjectByObjectName("FormData", 0);
		tLPGrpEdorMainSchema.setEdorAcceptNo(dFormData.getEdorAcceptNo());
		tLPGrpEdorMainSchema.setEdorNo(dFormData.getEdorNo());
		tLPGrpEdorMainSchema.setGrpContNo(GrpContNo);
		tLPGrpEdorMainSchema.setEdorAppDate(dFormData.getEdorAppDate());
		tLPGrpEdorMainSchema.setEdorValiDate(dFormData.getEdorValiDate());

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorAcceptNo(dFormData.getEdorAcceptNo());
		tLPGrpEdorItemSchema.setEdorNo(dFormData.getEdorNo());
		tLPGrpEdorItemSchema.setEdorAppNo(dFormData.getEdorNo());
		tLPGrpEdorItemSchema.setEdorType(dFormData.getEdorType());
		tLPGrpEdorItemSchema.setGrpContNo(GrpContNo);
		tLPGrpEdorItemSchema.setEdorAppDate(dFormData.getEdorAppDate());
		tLPGrpEdorItemSchema.setEdorValiDate(dFormData.getEdorValiDate());
		tLPGrpEdorItemSchema.setManageCom(GI.ManageCom);
		mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);

		try {

			// 准备传输数据 VData
			VData tVData = new VData();

			tVData.add(mLPGrpEdorItemSet);
			tVData.add(tLPGrpEdorMainSchema);
			tVData.add(tTransferData);
			tVData.add(GI);

			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			if (!tGrpEdorItemUI.submitData(tVData, fmAction)) {
				// 得不到未结案的错误信息
				System.out.println("11111------return");
				tVData.clear();
				tVData = tGrpEdorItemUI.getResult();
				// LPGrpEdorItemSet tLPGrpEdorItemSet = (LPGrpEdorItemSet)
				// tVData.getObjectByObjectName("LPGrpEdorItemSet", 0);
				errorValue = tGrpEdorItemUI.mErrors.getFirstError();
				return false;
			}
		} catch (Exception ex) {
			System.out.println("aaaa" + ex.toString());
			errorValue = ex.toString();
			return false;
		}
		
		String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ GI.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WDCF" + "','" + GI.ManageCom + "','" 
				+ "保全实名化撤销导入结束','" + tAccpetNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);
		
		return true;
	}

	/**
	 * 保全理算
	 * @return
	 */
	public boolean edorDetail() {

		System.out.println("**************保全项目理算****************");
		ExeSQL ExeSQL = new ExeSQL();
		String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ GI.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WDCF" + "','" + GI.ManageCom + "','" 
				+ "保全实名化撤销开始理算','" + tAccpetNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		
		LCGrpContInfo vLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		String GrpContNo = vLCGrpContInfo.getGrpContNo();

		String edorNo = tAccpetNo;
		StringBuffer sql = new StringBuffer();
		sql.append("select 1 from lccont where appflag in ('0','2') ");
		sql.append("and contno in (select contno from lcinsuredlist where edorno = '" + edorNo + "' and state = '1') ");

		ExeSQL exesql = new ExeSQL();
		SSRS ssrs = exesql.execSQL(sql.toString());

		if (ssrs.getMaxRow() > 0) {
			errorValue = "本次增人已经进行理算操作！\n请点【保全理算】按钮完成本次操作，或者把本次增人的保全项目删除后重新添加。";
			return false;
		}

		// detailEdorType();////////通过PEdor.js跳转到GEdorTypeWDInput.jsp
		StringBuffer sqlAPPOBJ = new StringBuffer();
		sqlAPPOBJ.append(" select NeedDetail from LMEdorItem where edorcode = 'WD' ");

		if (ContType == "1") {
			sqlAPPOBJ.append(" and APPOBJ='I'");
		} else {
			sqlAPPOBJ.append(" and APPOBJ='G'");
		}

		ExeSQL exeAPPOBJ = new ExeSQL();
		SSRS ssrsAPPOBJ = exeAPPOBJ.execSQL(sqlAPPOBJ.toString());

		if (ssrsAPPOBJ.getMaxRow() > 0) {
			if (ssrsAPPOBJ.GetText(1, 1) == "0") {
				errorValue = "该项目不需要录入明细信息！";
				return false;
			}
		} else {
			errorValue = "该项目定义不完整！";
			return false;
		}

		StringBuffer sqlcount = new StringBuffer();
		if (ContType == "1") {
			sqlcount.append("  select count(*) from LPEdorItem where edorno = '" + edorNo + "' and edortype = 'WD'");
		} else {
			sqlcount.append(
					"  select count(*) from LPGrpEdorItem where edorno = '" + edorNo + "' and edortype = 'WD' ");
		}

		ExeSQL execount = new ExeSQL();
		SSRS ssrscount = execount.execSQL(sqlcount.toString());

		// 判断是否查询成功
		if ("0".equals(ssrscount.GetText(1, 1))) {
			errorValue = "操作失败！";
			return false;
		} else {
			// GEdorTypeWDSubmit.jsp
			// 校验报文的客户是否为数据库的数据
			// 报文中客户号不存在数据库中时：不做操作。
			StringBuffer sqlLCInsuredGrid = new StringBuffer();
			sqlLCInsuredGrid.append("select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, ");
			sqlLCInsuredGrid.append(" a.IDType, a.IDNo, a.OccupationCode, a.OccupationType ");
			sqlLCInsuredGrid.append("from LCInsured a, LCCont b ");
			sqlLCInsuredGrid.append("where a.ContNo = b.ContNo ");
			sqlLCInsuredGrid.append("and b.PolType <> '1' ");
			sqlLCInsuredGrid.append("and not exists (select * from LPDiskImport ");
			sqlLCInsuredGrid.append("    where InsuredNo = a.InsuredNo ");
			sqlLCInsuredGrid.append("    and GrpContNo = '" + GrpContNo + "' ");
			sqlLCInsuredGrid.append("    and EdorType = 'WD' ");
			sqlLCInsuredGrid.append("    and EdorNo = '" + edorNo + "')");
			sqlLCInsuredGrid.append("and b.AppFlag = '1' ");
			sqlLCInsuredGrid.append("and a.GrpContNo = '" + GrpContNo + "' ");
			sqlLCInsuredGrid.append("order by InsuredNo");

			String operator = "ADD";
			// 人员信息
			LCInsuredSet vLCInsuredSet = (LCInsuredSet) data.getObjectByObjectName("LCInsuredSet", 0);

			LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
			tLPGrpEdorItemSchema.setEdorNo(edorNo);
			tLPGrpEdorItemSchema.setEdorType("WD");
			tLPGrpEdorItemSchema.setGrpContNo(GrpContNo);

			VData data = new VData();
			TransferData mTransferData = new TransferData();
			mTransferData.setNameAndValue("operator", operator);
			data.add(GI);
			data.add(vLCInsuredSet);
			data.add(tLPGrpEdorItemSchema);
			data.add(mTransferData);

			System.out.println("fff");
			GrpEdorWDDetailUI tGrpEdorWDDetailUI = new GrpEdorWDDetailUI();
			if (!tGrpEdorWDDetailUI.submitData(data, operator)) {
				errorValue = "数据保存失败！原因是:" + tGrpEdorWDDetailUI.getError();
				return false;
			}
		}

		// GEdorTypeWDSubmit.jsp 保存
		String operator = "SAVE";

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorNo(edorNo);
		tLPGrpEdorItemSchema.setEdorType("WD");
		tLPGrpEdorItemSchema.setGrpContNo(GrpContNo);

		VData data = new VData();
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("operator", operator);
		data.add(GI);
		data.add(tLPGrpEdorItemSchema);
		data.add(mTransferData);

		GrpEdorWDDetailUI tGrpEdorWDDetailUI = new GrpEdorWDDetailUI();
		if (!tGrpEdorWDDetailUI.submitData(data, operator)) {
			errorValue = "数据保存失败！原因是:" + tGrpEdorWDDetailUI.getError();
			return false;
		}
		
		String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ GI.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WDCF" + "','" + GI.ManageCom + "','" 
				+ "保全实名化撤销理算结束','" + tAccpetNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);
		
		return true;
	}

	/**
	 * 保全理算 确认
	 * @return
	 */
	public boolean edorAppConfirm() {

		System.out.println("*************************。。保全确认。。*****************");
		ExeSQL ExeSQL = new ExeSQL();
		String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ GI.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WDCF" + "','" + GI.ManageCom + "','" 
				+ "保全实名化撤销开始确认','" + tAccpetNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql);

		LCGrpContInfo vLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		String CustomerNo = vLCGrpContInfo.getCustomerNo();
		String GrpContNo = vLCGrpContInfo.getGrpContNo();
		FormData dFormData = (FormData) data.getObjectByObjectName("FormData", 0);
		String EdorAcceptNo = dFormData.getEdorAcceptNo();
		String edorNo = dFormData.getEdorNo();
		String EdorValiDate = dFormData.getEdorValiDate();

		// 集体批改信息
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(EdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(edorNo);
		tLPGrpEdorMainSchema.setGrpContNo(GrpContNo);
		tLPGrpEdorMainSchema.setEdorValiDate(EdorValiDate);
		tLPGrpEdorMainSchema.setEdorAppDate(EdorValiDate);// 待确认

		VData data = new VData();
		data.add(GI);
		data.add(tLPGrpEdorMainSchema);

		PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();

		if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM")) {
			errorValue = "保全理算失败！原因是：" + tPGrpEdorAppConfirmUI.getError().getErrContent();
			System.out.println(tPGrpEdorAppConfirmUI.getError().getErrContent());
			return false;
		}

		// GEdorConfirmSubmit.jsp
		String edorAcceptNo = EdorAcceptNo;
		String fmtransact2 = "NOTUSEACC";
		String balanceMethodValue = "0";

		String checkSQL = "select 1 from LPGrpEdorItem " + "where EdorNo = '" + edorAcceptNo + "' "
				+ "and EdorType in ('LQ','ZB')";

		SSRS rs = new ExeSQL().execSQL(checkSQL);

		if (rs != null && rs.getMaxRow() > 0) {
			if (balanceMethodValue.equals("0")) {
				errorValue = "部分领取、追加保费项目不得进行定期结算，请单独操作并进行即时结算";
			}

		}
		boolean flag = false;

		// 增加做NI项目时会增人重复的校验
		String checkNI = "select 1 from LPGrpEdorItem " + "where EdorNo = '" + edorAcceptNo + "' "
				+ "and EdorType = 'NI' ";

		SSRS mssrs = new ExeSQL().execSQL(checkNI);

		if (mssrs != null && mssrs.getMaxRow() > 0) {
			// 增加理算后对于lcinsured是否缺失的校验
			String checkInsured = " select 1 from lccont where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"
					+ edorAcceptNo + "' fetch first 1 row only) and contno not in "
					+ "(select distinct contno from lcinsured where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"
					+ edorAcceptNo
					+ "' fetch first 1 row only) ) and contno in (select contno from lcinsuredlist where edorno='"
					+ edorAcceptNo + "') ";

			SSRS mssrs1 = new ExeSQL().execSQL(checkInsured);

			if (mssrs1 != null && mssrs1.getMaxRow() > 0) {
				flag = false;
				errorValue = "保全项目增人时出现lcinsured丢失的问题,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
			}

			// ------------------insured校验结束-------------------
			String sql1 = "select getmoney from lpgrpedoritem " + "where EdorNo = '" + edorAcceptNo + "' "
					+ "and EdorType = 'NI' ";

			SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
			String itemmoney = tSSRS1.GetText(1, 1);

			String sql2 = "select sum(getmoney) from ljsgetendorse " + " where endorsementno = '" + edorAcceptNo + "' "
					+ " and feeoperationtype='NI' and feefinatype='BF' with ur";

			SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
			String endorsemoney = tSSRS2.GetText(1, 1);

			if (!itemmoney.equals(endorsemoney)) {
				flag = false;
				errorValue = "保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
			}

			// -----------------------关于增人总数和实际的理算的人数的比较-----------------------
			String sql_check_ljagetendorse = "  select count(distinct a.contno),count(distinct b.contno) "
					+ "  from ljsgetendorse a, lcinsuredlist b " + "  where a.grpcontno = b.grpcontno "
					+ "  and a.endorsementno = '" + edorAcceptNo + "'" + "  and a.feeoperationtype = 'NI' "
					+ "  and a.endorsementno=b.edorno " + "  and b.state='1' " + "  with ur";

			// ---------------------------------------------------------
			SSRS t_check_ljagendorse_SSRS1 = new ExeSQL().execSQL(sql_check_ljagetendorse);

			// ---------------------------------------------------------
			System.out.println("sql_check_ljagetendorse:" + sql_check_ljagetendorse);

			int the_number_of_1 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1, 1));
			int the_number_of_2 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1, 2));
			System.out.println("the_number_of_1:" + the_number_of_1);
			System.out.println("the_number_of_2:" + the_number_of_2);

			if (the_number_of_1 != the_number_of_2) {
				flag = false;
				errorValue = "实际理算完成的人数和模板录入的人数不一致，请点击[重复理算]回退到保全明细录入页面，并重新录入[NI]项目明细";
			}

		}

		// xiep 2010-4-21 增加做ZT项目时会错误的校验
		String checkZT = "select 1 from LPGrpEdorItem " + "where EdorNo = '" + edorAcceptNo + "' "
				+ "and EdorType = 'ZT' ";

		SSRS mssrs1 = new ExeSQL().execSQL(checkZT);

		if (mssrs1 != null && mssrs1.getMaxRow() > 0) {
			String sql3 = "select sum(getmoney) from lpedoritem " + "where EdorNo = '" + edorAcceptNo + "' "
					+ "and EdorType = 'ZT' ";

			SSRS tSSRS3 = new ExeSQL().execSQL(sql3);
			String itemmoneyZT = tSSRS3.GetText(1, 1);

			String sql4 = "select case when sum(getmoney) is null then 0 else sum(getmoney) end from ljsgetendorse "
					+ " where endorsementno = '" + edorAcceptNo + "' " + " and feeoperationtype='ZT'  with ur";

			SSRS tSSRS4 = new ExeSQL().execSQL(sql4);
			String endorsemoneyZT = tSSRS4.GetText(1, 1);

			if (!itemmoneyZT.equals(endorsemoneyZT)) {
				flag = false;
				errorValue = "保全项目减人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[ZT]项目明细重试";
			}
		}

		// xiep 2009-6-24 增加做WS项目时会实名化重复的校验
		String checkWS = "select 1 from LPGrpEdorItem " + "where EdorNo = '" + edorAcceptNo + "' "
				+ "and EdorType = 'WS' ";

		SSRS mrs = new ExeSQL().execSQL(checkWS);

		if (mrs != null && mrs.getMaxRow() > 0) {
			String wssql = "select 1 from lpcont where edorno='" + edorAcceptNo
					+ "' group by insuredno having count(contno)>1";

			SSRS wsSSRS = new ExeSQL().execSQL(wssql);

			if (wsSSRS != null && wsSSRS.getMaxRow() > 0) {
				flag = false;
				errorValue = "无名单实名化时增人出现重复数据,请点击[重复理算]回退到保全明细后重新添加[WS]项目明细";
			}
		}

		if (!flag) {
			EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorAcceptNo, "YE");
			tSpecialData.add("CustomerNo", CustomerNo);
			tSpecialData.add("AccType", "");
			tSpecialData.add("OtherType", "3");
			tSpecialData.add("OtherNo", edorAcceptNo);
			tSpecialData.add("DestSource", "");
			tSpecialData.add("ContType", ContType);
			tSpecialData.add("Fmtransact2", fmtransact2);

			LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
			tLCAppAccTraceSchema.setCustomerNo(CustomerNo);
			tLCAppAccTraceSchema.setAccType("");
			tLCAppAccTraceSchema.setOtherType("3");// 团单
			tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
			tLCAppAccTraceSchema.setDestSource("");
			tLCAppAccTraceSchema.setOperator(GI.Operator);

			VData tVData = new VData();
			tVData.add(tLCAppAccTraceSchema);
			tVData.add(tSpecialData);
			tVData.add(GI);

			PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();

			if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param")) {
				flag = false;
				errorValue = "处理帐户余额失败！";
			} else {

				BqConfirmUI tBqConfirmUI = new BqConfirmUI(GI, edorAcceptNo, BQ.CONTTYPE_G, balanceMethodValue);

				if (!tBqConfirmUI.submitData()) {
					flag = false;
					errorValue = tBqConfirmUI.getError();
				} else {
					System.out.println("交退费通知书" + edorAcceptNo);
					String mCurrDate = PubFun.getCurrentDate();
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("payMode", "0");
					tTransferData.setNameAndValue("endDate", mCurrDate);
					tTransferData.setNameAndValue("payDate", mCurrDate);
					tTransferData.setNameAndValue("bank", "");
					tTransferData.setNameAndValue("bankAccno", "");
					tTransferData.setNameAndValue("accName", "");

					// 生成交退费通知书
					FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(edorAcceptNo);
					if (!tFeeNoticeGrpVtsUI.submitData(tTransferData)) {
						flag = false;
						errorValue = "生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError();
						return false;
					}

					VData data1 = new VData();
					data1.add(GI);
					data1.add(tTransferData);
					SetPayInfo spi = new SetPayInfo(edorAcceptNo);
					if (!spi.submitDate(data1, "")) {
						System.out.println("设置转帐信息失败！");
						flag = false;
						errorValue = "设置收退费方式失败！原因是：" + spi.mErrors.getFirstError();
						return false;
					}
					flag = true;
					// errorValue = "保全确认成功！";
					String message = tBqConfirmUI.getMessage();
					if ((message != null) && (!message.equals(""))) {
						errorValue += "\n" + tBqConfirmUI.getMessage();
					}
				}
			}
		}
		System.out.println("--------------------------end--------------------");
		
		String insertClaimInsertLogSql1 = "insert into ClaimInsertLog (Id ,Caller ,endDate ,endtime ,transactionType ,transactionCode ,transactiondescription , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ GI.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
				+ "WDCF" + "','" + GI.ManageCom + "','" 
				+ "保全实名化撤销确认结束','" + tAccpetNo + "')";

		ExeSQL.execUpdateSQL(insertClaimInsertLogSql1);
		
		return true;
	}

//	/**
//	 * 回滚
//	 * @return
//	 */
//	public boolean rollBack() {
//
//		System.out.println("********************开始回滚*******************");
//
//		VData vdata = new VData();
//		MMap map = new MMap();
//
//		if ("".equals(tAccpetNo) || null == tAccpetNo) {
//			return false;
//		}
//
//		StringBuffer sql1 = new StringBuffer();
//		sql1.append("delete from LGWorkTrace where WorkNo = '" + tAccpetNo + "'");
//		map.put(sql1.toString(), "DELETE");
//
//		StringBuffer sql2 = new StringBuffer();
//		sql2.append("delete from LGTraceNodeOp where WorkNo = '" + tAccpetNo + "'");
//		map.put(sql2.toString(), "DELETE");
//
//		StringBuffer sql3 = new StringBuffer();
//		sql3.append("delete from LPEdorApp where EdorAcceptNo = '" + tAccpetNo + "'");
//		map.put(sql3.toString(), "DELETE");
//
//		StringBuffer sql4 = new StringBuffer();
//		sql4.append("delete from LGWork where WorkNo = '" + tAccpetNo + "'");
//		map.put(sql4.toString(), "DELETE");
//
//		StringBuffer sql5 = new StringBuffer();
//		sql5.append("delete from LPGrpEdorMain where EdorAcceptNo='" + tAccpetNo + "'");
//		map.put(sql5.toString(), "DELETE");
//
//		StringBuffer sql6 = new StringBuffer();
//		sql6.append("delete from LPGrpEdorItem where EdorAcceptNo='" + tAccpetNo + "'");
//		map.put(sql6.toString(), "DELETE");
//
//		StringBuffer sql7 = new StringBuffer();
//		sql7.append("delete from LPDiskImport where EdorNo='" + tAccpetNo + "'");
//		map.put(sql7.toString(), "DELETE");
//
//		StringBuffer sql8 = new StringBuffer();
//		sql8.append("delete from LPInsured where EdorNo='" + tAccpetNo + "'");
//		map.put(sql8.toString(), "DELETE");
//
//		StringBuffer sql9 = new StringBuffer();
//		sql9.append("delete from LPEdorItem where EdorAcceptNo = '" + tAccpetNo + "'");
//		map.put(sql9.toString(), "DELETE");
//
//		StringBuffer sql10 = new StringBuffer();
//		sql10.append("delete from lpedorprint where edorno='" + tAccpetNo + "'");
//		map.put(sql10.toString(), "DELETE");
//
//		StringBuffer sql11 = new StringBuffer();
//		sql11.append("delete from LPEdorEspecialData where EdorAcceptNo='" + tAccpetNo + "'");
//		map.put(sql11.toString(), "DELETE");
//
//		StringBuffer sql12 = new StringBuffer();
//		sql12.append(" Delete From LockTable Where Notype='BQ' And Nolimit='" + tAccpetNo + "'");
//		map.put(sql12.toString(), "DELETE");
//
//		StringBuffer sql13 = new StringBuffer();
//		sql13.append(" select count(*) from LBCont where EdorNo = '" + tAccpetNo + "'");
//
//		ExeSQL tExeSQL13 = new ExeSQL();
//		SSRS ssrssql13 = tExeSQL13.execSQL(sql13.toString());
//
//		System.out.println("count=" + (ssrssql13.GetText(1, 1)));
//		if (!"0".equals(ssrssql13.GetText(1, 1))) {
//			StringBuffer sql14 = new StringBuffer();
//			sql14.append("insert into LCCont ");
//			sql14.append(
//					" (GrpContNo,ContNo,ProposalContNo,PrtNo,ContType,FamilyType,FamilyID,PolType,CardFlag,ManageCom,ExecuteCom,AgentCom,AgentCode,AgentGroup,AgentCode1,AgentType,SaleChnl,Handler,Password,AppntNo,AppntName,AppntSex,AppntBirthday,AppntIDType,AppntIDNo,InsuredNo,InsuredName,InsuredSex,InsuredBirthday,InsuredIDType,InsuredIDNo,PayIntv,PayMode,PayLocation,DisputedFlag,OutPayFlag,GetPolMode,SignCom,SignDate,SignTime,ConsignNo,BankCode,BankAccNo,AccName,PrintCount,LostTimes,Lang,Currency,Remark,Peoples,Mult,Prem,Amnt,SumPrem,Dif,PaytoDate,FirstPayDate,CValiDate,InputOperator,InputDate,InputTime,ApproveFlag,ApproveCode,ApproveDate,ApproveTime,UWFlag,UWOperator,UWDate,UWTime,AppFlag,PolApplyDate,GetPolDate,GetPolTime,CustomGetPolDate,State,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,FirstTrialOperator,FirstTrialDate,FirstTrialTime,ReceiveOperator,ReceiveDate,ReceiveTime,TempFeeNo,ProposalType,SaleChnlDetail,ContPrintLoFlag,ContPremFeeNo,CustomerReceiptNo,CInValiDate,Copys,DegreeType,HandlerDate,HandlerPrint,StateFlag,PremScope,IntlFlag,UWConfirmNo,PayerType,GrpAgentCom,GrpAgentCode,GrpAgentName,AccType,Crs_SaleChnl,Crs_BussType,GrpAgentIDNo,DueFeeMsgFlag,PayMethod) ");
//			sql14.append(
//					" (select GrpContNo,ContNo,ProposalContNo,PrtNo,ContType,FamilyType,FamilyID,PolType,CardFlag,ManageCom,ExecuteCom,AgentCom,AgentCode,AgentGroup,AgentCode1,AgentType,SaleChnl,Handler,Password,AppntNo,AppntName,AppntSex,AppntBirthday,AppntIDType,AppntIDNo,InsuredNo,InsuredName,InsuredSex,InsuredBirthday,InsuredIDType,InsuredIDNo,PayIntv,PayMode,PayLocation,DisputedFlag,OutPayFlag,GetPolMode,SignCom,SignDate,SignTime,ConsignNo,BankCode,BankAccNo,AccName,PrintCount,LostTimes,Lang,Currency,Remark,Peoples,Mult,Prem,Amnt,SumPrem,Dif,PaytoDate,FirstPayDate,CValiDate,InputOperator,InputDate,InputTime,ApproveFlag,ApproveCode,ApproveDate,ApproveTime,UWFlag,UWOperator,UWDate,UWTime,AppFlag,PolApplyDate,GetPolDate,GetPolTime,CustomGetPolDate,State,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,FirstTrialOperator,FirstTrialDate,FirstTrialTime,ReceiveOperator,ReceiveDate,ReceiveTime,TempFeeNo,ProposalType,SaleChnlDetail,ContPrintLoFlag,ContPremFeeNo,CustomerReceiptNo,CInValiDate,Copys,DegreeType,HandlerDate,HandlerPrint,StateFlag,PremScope,IntlFlag,UWConfirmNo,PayerType,GrpAgentCom,GrpAgentCode,GrpAgentName,AccType,Crs_SaleChnl,Crs_BussType,GrpAgentIDNo,DueFeeMsgFlag,PayMethod");
//			sql14.append(" from LBCont where EdorNo = '" + tAccpetNo + "')");
//			map.put(sql14.toString(), "INSERT");
//			StringBuffer sql15 = new StringBuffer();
//			sql15.append("delete from LBCont where EdorNo = '" + tAccpetNo + "'");
//			map.put(sql5.toString(), "DELETE");
//		}
//
//		StringBuffer sql16 = new StringBuffer();
//		sql16.append("select count(*) from LBInsured where EdorNo='" + tAccpetNo + "'");
//
//		ExeSQL tExeSQL16 = new ExeSQL();
//		SSRS ssrssql6 = tExeSQL16.execSQL(sql16.toString());
//
//		if (!"0".equals(ssrssql6.GetText(1, 1))) {
//			StringBuffer sql17 = new StringBuffer();
//			sql17.append("insert into LCInsured");
//			sql17.append(
//					" (GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo,Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade,BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType,SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount,DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone) ");
//			sql17.append(
//					" (select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo,Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade,BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType,SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount,DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone ");
//			sql17.append(" from LBInsured where EdorNo = '" + tAccpetNo + "')");
//			map.put(sql17.toString(), "INSERT");
//			StringBuffer sql18 = new StringBuffer();
//			sql18.append("delete from LBInsured where EdorNo='" + tAccpetNo + "'");
//			map.put(sql18.toString(), "DELETE");
//		}
//
//		StringBuffer sql19 = new StringBuffer();
//		sql19.append("select count(*) from LBPol where EdorNo='" + tAccpetNo + "'");
//
//		ExeSQL tExeSQL19 = new ExeSQL();
//		SSRS ssrssql19 = tExeSQL19.execSQL(sql19.toString());
//
//		if (!"0".equals(ssrssql19.GetText(1, 1))) {
//			StringBuffer sql20 = new StringBuffer();
//			sql20.append("insert into LCPol");
//			sql20.append(
//					" (GrpContNo,GrpPolNo,ContNo,PolNo,ProposalNo,PrtNo,ContType,PolTypeFlag,MainPolNo,MasterPolNo,KindCode,RiskCode,RiskVersion,ManageCom,AgentCode,AgentGroup,SaleChnl,InsuredNo,InsuredName,InsuredSex,InsuredBirthday,InsuredAppAge,InsuredPeoples,OccupationType,AppntNo,AppntName,CValiDate,SignCom,SignDate,SignTime,PayEndDate,PaytoDate,GetStartDate,EndDate,GetYearFlag,GetYear,PayEndYearFlag,PayEndYear,InsuYearFlag,InsuYear,AcciYear,SpecifyValiDate,PayMode,PayIntv,PayYears,Years,ManageFeeRate,FloatRate,Mult,StandPrem,Prem,SumPrem,Amnt,RiskAmnt,LeavingMoney,EndorseTimes,ClaimTimes,LiveTimes,RenewCount,LastRevDate,RnewFlag,AutoPayFlag,BnfFlag,ImpartFlag,ApproveFlag,ApproveCode,ApproveDate,ApproveTime,UWFlag,UWCode,UWDate,UWTime,PolApplyDate,AppFlag,PolState,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,WaitPeriod,RiskSeqNo,Copys,ComFeeRate,BranchFeeRate,ProposalContNo,ContPlanCode,CessAmnt,StateFlag,SupplementaryPrem,InitFeeRate) ");
//			sql20.append(
//					" (select GrpContNo,GrpPolNo,ContNo,PolNo,ProposalNo,PrtNo,ContType,PolTypeFlag,MainPolNo,MasterPolNo,KindCode,RiskCode,RiskVersion,ManageCom,AgentCode,AgentGroup,SaleChnl,InsuredNo,InsuredName,InsuredSex,InsuredBirthday,InsuredAppAge,InsuredPeoples,OccupationType,AppntNo,AppntName,CValiDate,SignCom,SignDate,SignTime,PayEndDate,PaytoDate,GetStartDate,EndDate,GetYearFlag,GetYear,PayEndYearFlag,PayEndYear,InsuYearFlag,InsuYear,AcciYear,SpecifyValiDate,PayMode,PayIntv,PayYears,Years,ManageFeeRate,FloatRate,Mult,StandPrem,Prem,SumPrem,Amnt,RiskAmnt,LeavingMoney,EndorseTimes,ClaimTimes,LiveTimes,RenewCount,LastRevDate,RnewFlag,AutoPayFlag,BnfFlag,ImpartFlag,ApproveFlag,ApproveCode,ApproveDate,ApproveTime,UWFlag,UWCode,UWDate,UWTime,PolApplyDate,AppFlag,PolState,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,WaitPeriod,RiskSeqNo,Copys,ComFeeRate,BranchFeeRate,ProposalContNo,ContPlanCode,CessAmnt,StateFlag,SupplementaryPrem,InitFeeRate ");
//			sql20.append(" from LBPol where EdorNo='" + tAccpetNo + "') ");
//			map.put(sql20.toString(), "INSERT");
//			StringBuffer sql21 = new StringBuffer();
//			sql21.append(" delete from LBPol where EdorNo='" + tAccpetNo + "'");
//			map.put(sql21.toString(), "DELETE");
//		}
//
//		StringBuffer sql22 = new StringBuffer();
//		sql22.append("select count(*) from LBDuty where EdorNo='" + tAccpetNo + "'");
//
//		ExeSQL tExeSQL22 = new ExeSQL();
//		SSRS ssrssql22 = tExeSQL22.execSQL(sql22.toString());
//
//		if (!"0".equals(ssrssql22.GetText(1, 1))) {
//			StringBuffer sql23 = new StringBuffer();
//			sql23.append("insert into LCDuty ");
//			sql23.append(
//					" (PolNo,DutyCode,ContNo,Mult,StandPrem,Prem,SumPrem,Amnt,RiskAmnt,PayIntv,PayYears,Years,FloatRate,FirstPayDate,FirstMonth,PaytoDate,PayEndDate,PayEndYearFlag,PayEndYear,GetYearFlag,GetYear,InsuYearFlag,InsuYear,AcciYearFlag,AcciYear,EndDate,AcciEndDate,FreeFlag,FreeRate,FreeStartDate,FreeEndDate,GetStartDate,GetStartType,LiveGetMode,DeadGetMode,BonusGetMode,SSFlag,PeakLine,GetLimit,GetRate,CalRule,PremToAmnt,StandbyFlag1,StandbyFlag2,StandbyFlag3,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,Copys) ");
//			sql23.append(
//					" (select PolNo,DutyCode,ContNo,Mult,StandPrem,Prem,SumPrem,Amnt,RiskAmnt,PayIntv,PayYears,Years,FloatRate,FirstPayDate,FirstMonth,PaytoDate,PayEndDate,PayEndYearFlag,PayEndYear,GetYearFlag,GetYear,InsuYearFlag,InsuYear,AcciYearFlag,AcciYear,EndDate,AcciEndDate,FreeFlag,FreeRate,FreeStartDate,FreeEndDate,GetStartDate,GetStartType,LiveGetMode,DeadGetMode,BonusGetMode,SSFlag,PeakLine,GetLimit,GetRate,CalRule,PremToAmnt,StandbyFlag1,StandbyFlag2,StandbyFlag3,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,Copys ");
//			sql23.append(" from LBDuty where EdorNo='" + tAccpetNo + "') ");
//			map.put(sql23.toString(), "INSERT");
//			StringBuffer sql24 = new StringBuffer();
//			sql24.append("delete from LBDuty where EdorNo='" + tAccpetNo + "'");
//			map.put(sql24.toString(), "DELETE");
//		}
//
//		StringBuffer sql25 = new StringBuffer();
//		sql25.append("select count(*) from LBPrem where EdorNo='" + tAccpetNo + "'");
//
//		ExeSQL tExeSQL25 = new ExeSQL();
//		SSRS ssrssql25 = tExeSQL25.execSQL(sql25.toString());
//
//		if (!"0".equals(ssrssql25.GetText(1, 1))) {
//			StringBuffer sql26 = new StringBuffer();
//			sql26.append("insert into LCPrem");
//			sql26.append(
//					" (GrpContNo,ContNo,PolNo,DutyCode,PayPlanCode,PayPlanType,AppntType,AppntNo,UrgePayFlag,NeedAcc,PayTimes,Rate,PayStartDate,PayEndDate,PaytoDate,PayIntv,StandPrem,Prem,SumPrem,SuppRiskScore,FreeFlag,FreeRate,FreeStartDate,FreeEndDate,State,ManageCom,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime) ");
//			sql26.append(
//					" (select GrpContNo,ContNo,PolNo,DutyCode,PayPlanCode,PayPlanType,AppntType,AppntNo,UrgePayFlag,NeedAcc,PayTimes,Rate,PayStartDate,PayEndDate,PaytoDate,PayIntv,StandPrem,Prem,SumPrem,SuppRiskScore,FreeFlag,FreeRate,FreeStartDate,FreeEndDate,State,ManageCom,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime ");
//			sql26.append(" from LBPrem where EdorNo='" + tAccpetNo + "') ");
//			map.put(sql26.toString(), "INSERT");
//			StringBuffer sql27 = new StringBuffer();
//			sql27.append("delete from LBPrem where EdorNo='" + tAccpetNo + "'");
//			map.put(sql27.toString(), "DELETE");
//		}
//
//		StringBuffer sql28 = new StringBuffer();
//		sql28.append("select count(*) from LBGet where EdorNo='" + tAccpetNo + "'");
//
//		ExeSQL tExeSQL28 = new ExeSQL();
//		SSRS ssrssql28 = tExeSQL28.execSQL(sql28.toString());
//
//		if (!"0".equals(ssrssql28.GetText(1, 1))) {
//			StringBuffer sql29 = new StringBuffer();
//			sql29.append("insert into LCGet");
//			sql29.append(
//					" (GrpContNo,ContNo,PolNo,DutyCode,GetDutyCode,GetDutyKind,InsuredNo,GetMode,GetLimit,GetRate,UrgeGetFlag,LiveGetType,AddRate,CanGet,NeedAcc,NeedCancelAcc,StandMoney,ActuGet,SumMoney,GetIntv,GettoDate,GetStartDate,GetEndDate,BalaDate,State,ManageCom,Operator,MakeDate,MakeTime,ModifyTime,ModifyDate,AfterDie) ");
//			sql29.append(
//					" (select GrpContNo,ContNo,PolNo,DutyCode,GetDutyCode,GetDutyKind,InsuredNo,GetMode,GetLimit,GetRate,UrgeGetFlag,LiveGetType,AddRate,CanGet,NeedAcc,NeedCancelAcc,StandMoney,ActuGet,SumMoney,GetIntv,GettoDate,GetStartDate,GetEndDate,BalaDate,State,ManageCom,Operator,MakeDate,MakeTime,ModifyTime,ModifyDate,AfterDie");
//			sql29.append(" from LBGet where EdorNo='" + tAccpetNo + "')");
//			map.put(sql29.toString(), "INSERT");
//			StringBuffer sql30 = new StringBuffer();
//			sql30.append("delete from LBGet where EdorNo='" + tAccpetNo + "'");
//			map.put(sql30.toString(), "DELETE");
//		}
//		vdata.add(map);
//		PubSubmit pubSubmit = new PubSubmit();
//		pubSubmit.submitData(vdata, null);
//		System.out.println("回滚完成！");
//		return true;
//	}

	/**
	 * 操作保存信息
	 * @param flagBack
	 * @param xml
	 */
	private void saveLog(boolean flagBack, String xml) {

		MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		if (flagBack) {
			// 错误日志
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ vMessHead.getSendOperator()
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','"
					+ vMessHead.getMsgType() + "','" + vMessHead.getBranchCode() + "','" + vMessHead.getBatchNo()
					+ "','" + tAccpetNo + "')";

			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);

			String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ vMessHead.getSendOperator() + "','" + xml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ returnxml + "','" + errorValue + "','" + vMessHead.getMsgType() + "','"
					+ vMessHead.getBranchCode() + "','" + tAccpetNo + "')";

			ExeSQL.execUpdateSQL(insertErrorInsertLogSql);
		} else {
			// 日志
			ExeSQL ExeSQL = new ExeSQL();

			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ vMessHead.getSendOperator()
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'1','"
					+ vMessHead.getMsgType() + "','" + vMessHead.getBranchCode() + "','" + vMessHead.getBatchNo()
					+ "','" + tAccpetNo + "')";

			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		}
	}

	/**
	 * 生成返回报文
	 * @return
	 */
	public String creatReturnxml() {

		System.out.println("*****************开始生成返回报文**********");
		System.out.println("传入工单号为tAccpetNo：" + tAccpetNo);
		String responseXml;

		try {
			MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
			Element root = new Element("DataSet");
			Document doc = new Document(root);
			Element eMsgHead = new Element("MsgHead");
			Element eHeadnode = new Element("Item");
			Element eHeadBatchNo = new Element("BatchNo");
			eHeadBatchNo.setText(vMessHead.getBatchNo());
			eHeadnode.addContent(eHeadBatchNo);
			Element eHeadSendDate = new Element("SendDate");
			eHeadSendDate.setText(vMessHead.getSendDate());
			eHeadnode.addContent(eHeadSendDate);
			Element eHeadSendTime = new Element("SendTime");
			eHeadSendTime.setText(vMessHead.getSendTime());
			eHeadnode.addContent(eHeadSendTime);
			Element eHeadBranchCode = new Element("BranchCode");
			eHeadBranchCode.setText(vMessHead.getBranchCode());
			eHeadnode.addContent(eHeadBranchCode);
			Element eHeadSendOperator = new Element("SendOperator");
			eHeadSendOperator.setText(vMessHead.getSendOperator());
			eHeadnode.addContent(eHeadSendOperator);
			Element eHeadMsgType = new Element("MsgType");
			eHeadMsgType.setText(vMessHead.getMsgType());
			eHeadnode.addContent(eHeadMsgType);
			if (!"".equals(errorValue) && null != errorValue) {
				Element eHeadState = new Element("State");
				eHeadState.setText("01");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText(errorValue);
				eHeadnode.addContent(eHeadErrInfo);
			} else {
				Element eHeadState = new Element("State");
				eHeadState.setText("00");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("操作成功！");
				eHeadnode.addContent(eHeadErrInfo);
			}
			eMsgHead.addContent(eHeadnode);

			Element eMsgInfo = new Element("EndorsementInfo");
			Element eInfodnode = new Element("Item");
			Element eInfoEdorNo = new Element("EdorNo");
			eInfoEdorNo.setText(tAccpetNo);
			eInfodnode.addContent(eInfoEdorNo);
			Element eInfoEdorValidate = new Element("EdorValidate");
			eInfoEdorValidate.setText(mCurrDate);
			eInfodnode.addContent(eInfoEdorValidate);
			Element eInfoEdorConfdate = new Element("EdorConfdate");
			eInfoEdorConfdate.setText(mCurrDate);
			eInfodnode.addContent(eInfoEdorConfdate);
			eMsgInfo.addContent(eInfodnode);

			root.addContent(eMsgHead);
			root.addContent(eMsgInfo);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(doc);
		} catch (IOException e) {
			responseXml = "生成返回报文失败。。。";
			e.printStackTrace();
		}
		return responseXml;
	}

	public String getReturnxml() {
		return returnxml;
	}

	public void setReturnxml(String returnxml) {
		this.returnxml = returnxml;
	}

}
