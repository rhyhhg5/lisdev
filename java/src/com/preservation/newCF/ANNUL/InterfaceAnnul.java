package com.preservation.newCF.ANNUL;

import org.jdom.Element;

import com.preservation.newCF.GEdorCancelBQ;
import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.util.StringUtil;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * 保全撤销撤销（保全项/工单号）
 * 
 * @author 姜士杰 2018-12-13
 *
 */

public class InterfaceAnnul extends InterfaceBaseCF {

	// 处理类型，00撤销保全项，01撤销工单受理号
	private String WorkType = "";
	// 管理结构
	private String ManageCom;

	public boolean deal(String xml) {

		this.pxml = xml;

		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}

		return dealc();
	}

	/**
	 * 保全撤销逻辑处理
	 */
	public boolean dealc() {

		GEdorCancelBQ tGEdorCancel = new GEdorCancelBQ();

		if (!StringUtil.isNull(WorkType)) {
			System.out.println(WorkType);
			errorValue = "操作类型不能为空!";
			System.out.println(errorValue);
			return false;
		}
		if (!StringUtil.isNull(ManageCom)) {
			System.out.println(ManageCom);
			errorValue = "管理机构不能为空!";
			System.out.println(errorValue);
			return false;
		}
		if (!StringUtil.isNull(WorkNo)) {
			System.out.println(WorkNo);
			errorValue = "工单受理号不能为空!";
			System.out.println(errorValue);
			return false;
		}

		if ("1".equals(WorkType)) {
			tGEdorCancel.cancelAppEdor(data);
			// errorValue = "保全项撤销成功";
			// System.out.println(errorValue);
			return true;
		} else if ("2".equals(WorkType)) {
			tGEdorCancel.cancelAppEdorb(data);
			// errorValue = "工单项撤销成功";
			// System.out.println(errorValue);
			return true;
		} else {
			errorValue = "操作类型错误错误!";
			System.out.println(errorValue);
			return false;
		}
	}

	/**
	 * 解析报文体
	 */
	public void getkDatabody(Element rootElement) {

		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element ANNUL = rootElement.getChild("ANNUL");

		// 获取工单号
		WorkType = ANNUL.getChildText("WorkType");
		WorkNo = ANNUL.getChildText("WorkNo");
		ManageCom = ANNUL.getChildText("ManageCom");
		GI.ComCode = ANNUL.getChildText("ManageCom");
		GI.ManageCom = ANNUL.getChildText("ManageCom");
		// GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		System.out.println(WorkType);
		System.out.println(WorkNo);
		System.out.println(ManageCom);
		data.add(GI);
		data.add(WorkNo);
	}

}
