package com.preservation.newCF.LPCF;

import org.jdom.Element;

import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * 
 * @author jiangshijie 2018-12-28
 * 理赔金账户变更LPCF
 */

public class InterfaceLPCF extends InterfaceBaseCF {
	
	/**
	 * 逻辑处理
	 */
	public boolean dealc(){
		System.out.println("==========进入InterfaceLPCF.dealc子类方法===================");
		DealLPCF dealLPCF  = new DealLPCF();
		if (!dealLPCF.deal(data)) {
			errorValue = dealLPCF.getErrorValue();
			WorkNo = dealLPCF.getEdorAcceptNo();
			return false;
		}
		WorkNo = dealLPCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}
	
	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement){
		System.out.println("==========进入InterfaceLPCF.getkDatabody子类方法===================");
		// 解析团体客户节点
		Element LP = rootElement.getChild("LPCF");
		Element msgCustomer = LP.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		
		GlobalInput GI = new GlobalInput();
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
		//GI.Operator = "pa0001";
		 GI.Operator = "BAOQA";
		data.add(GI);
		
		CustomerInfo tCustomerInfo = new CustomerInfo();
		tCustomerInfo.setCustomerNo(eCustomerInfo.getChildText("CustomerNo"));
		tCustomerInfo.setWorkNo(eCustomerInfo.getChildText("WorkNo"));
		WorkNo = eCustomerInfo.getChildText("WorkNo");
		data.add(tCustomerInfo);

		// 解析团单信息节点
		Element lCGrpContInfo = LP.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		
		LCGrpContInfo tLCGrpContInfo = new LCGrpContInfo();
		tLCGrpContInfo.setGrpContNo(eLCGrpContInfo.getChildText("GrpContNo"));
		tLCGrpContInfo.setGrpAccName(eLCGrpContInfo.getChildText("GrpAccName"));
		tLCGrpContInfo.setGrpBankAccNo(eLCGrpContInfo.getChildText("GrpBankAccNo"));
		tLCGrpContInfo.setGrpBankCode(eLCGrpContInfo.getChildText("GrpBankCode"));
		//tLCGrpContInfo.setGrpName(eLCGrpContInfo.getChildText("GrpName"));
		data.add(tLCGrpContInfo);

		// 解析表单数据信息
		Element xFormData = LP.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		
		FormData tFormData = new FormData();
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		//tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		//tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		//tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		//tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));//必填，申请人类型  0-投保人，1-被保人，2-业务员，3-内部交办，4-其他委托代办 
		//tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		//tFormData.setRemark(xxFormData.getChildText("Remark"));
		
	/*	tFormData.setGrpPolNo(xxFormData.getChildText("grpPolNo"));
		tFormData.setGroupMoney(xxFormData.getChildText("groupMoney"));*/
		//tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		//tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorType("LP");
		tFormData.setInsuredNo(xxFormData.getChildText("InsuredNo"));//被保人客户号
		System.out.println(xxFormData.getChildText("InsuredNo"));
		tFormData.setBankCode(xxFormData.getChildText("BankCode"));//银行编码
		System.out.println(xxFormData.getChildText("BankCode"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));//银行账号
		tFormData.setAccName(xxFormData.getChildText("AccName"));//账号名称
		//tFormData.setSerialNo(xxFormData.getChildText("SerialNo"));//被保人序号
		tFormData.setExcel(xxFormData.getChildText("Excel"));//导入表格的url地址
		
		data.add(tFormData);
	}

}
