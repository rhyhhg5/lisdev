package com.preservation.newCF.LPCF;

import com.preservation.newCF.DealBaseCF;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.FinanceDataBL;
import com.sinosoft.lis.bq.GEdorGrpLPDetailUI;
import com.sinosoft.lis.bq.GEdorImportUI;
import com.sinosoft.lis.bq.GEdorLPDetailUI;
import com.sinosoft.lis.bq.PEdorLPDetailUI;
import com.sinosoft.lis.bq.PGrpEdorConfirmBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class DealLPCF extends DealBaseCF {

    private String flag = null;
	/**
	 * 当保全有单独逻辑的时候重写次方法
	 */
	@Override
	public boolean checkSave() {
		System.out.println("==========进入DealLPCF.domake子类方法===================");
		if (!checkupdata()) {
			return false;
		}

		if (!checkupdatas()) {
			return false;
		}
		String excel = mFormData.getExcel();
		if (StringUtil.StringNull(excel)) {
			System.out.println("==========进入DealLPCF.Excel处理开始===================");
			if(!getExcel(excel.toString())){
				System.out.println("处理Excle失败");
				return false;
			}
			}else{
				System.out.println("没有批量数据不执行");
			}
		if ("Succ".equals(flag)){
			System.out.println("保存开始==========");
			String sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo " +
		             "from LPDiskImport " +
		             "where GrpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' " +
		             "and EdorNo = '" + EdorAcceptNo + "' " +
		             "and EdorType = '" + mFormData.getEdorType() + "' " +
		             "and State = '1' " +
		             "order by Int(SerialNo) ";
			ExeSQL t1ExeSQL = new ExeSQL();
			SSRS sqlSSRS = t1ExeSQL.execSQL(sql);
			if(sqlSSRS.getMaxRow() > 0){}else{
				Content = "没有有效的导入数据！";
				System.out.println(Content);
				return false;	
			}
			String sql2 = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo, ErrorReason " +
		             "from LPDiskImport " +
		             "where GrpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' " +
		             "and EdorNo = '" + EdorAcceptNo + "' " +
		             "and EdorType = '" + mFormData.getEdorType() + "' " +
		             "and State = '0' " +
		             "order by Int(SerialNo) ";
			ExeSQL t0ExeSQL = new ExeSQL();
			SSRS sql2SSRS = t0ExeSQL.execSQL(sql2);
			if(sql2SSRS.getMaxRow() > 0){
				Content = "有错误数据，请重新导入！";
				System.out.println(Content);
				return false;	
			}
			
			 GEdorLPDetailUI tGEdorLPDetailUI = new GEdorLPDetailUI(GI, EdorAcceptNo, mLCGrpContInfo.getGrpContNo());
				if (!tGEdorLPDetailUI.submitData())
				{
					Content = "数据保存失败！原因是:" + tGEdorLPDetailUI.getError();
					String message = tGEdorLPDetailUI.getMessage();
					Content = Content+ message;
					return false;
				}else 
				{
					String message = tGEdorLPDetailUI.getMessage();
					if (message != null)
					{
					  Content = "数据保存异常，原因是:"+message;
					  System.out.println(Content);
					  String sql3 = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo, ErrorReason " +
					             "from LPDiskImport " +
					             "where GrpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' " +
					             "and EdorNo = '" + EdorAcceptNo + "' " +
					             "and EdorType = '" + mFormData.getEdorType() + "' " +
					             "and State = '0' " +
					             "order by Int(SerialNo) ";
						ExeSQL t3ExeSQL = new ExeSQL();
						SSRS sql3SSRS = t0ExeSQL.execSQL(sql3);
						if(sql3SSRS.getMaxRow() > 0){
							Content = Content+"序列号："+sql3SSRS.GetText(1, 1)+"客户名："+sql3SSRS.GetText(1, 4);
							System.out.println(Content);
						}
					  return false;
					}
				}
				
		}
		return true;
	}


	/**
	 * 团体理赔金银行账户修改
	 */
	private boolean checkupdata() {
		System.out.println("==========进入DealLPCF.checkupdata子类方法===================");
		String edorNo = EdorAcceptNo;
		String grpContNo = mLCGrpContInfo.getGrpContNo();
		LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();
		tLCGrpAppntSchema.setClaimBankCode(mLCGrpContInfo.getGrpBankCode());
		tLCGrpAppntSchema.setClaimBankAccNo(mLCGrpContInfo.getGrpBankAccNo());
		tLCGrpAppntSchema.setClaimAccName(mLCGrpContInfo.getGrpAccName());
		//为空校验
		
		if( !StringUtil.StringNull(edorNo)
			 ||!StringUtil.StringNull(grpContNo) 
				){
			Content = "团体保单号为空或受理号为空";
			System.out.println(Content);
			return false;	
		}
		if(!StringUtil.StringNull(mLCGrpContInfo.getGrpBankCode())
			||!StringUtil.StringNull(mLCGrpContInfo.getGrpBankAccNo())
			||!StringUtil.StringNull(mLCGrpContInfo.getGrpAccName())
				){
			Content = Content +"团体理赔金账户信息未填写完整，不做修改";
			System.out.println(Content);
			return true;	
		}
		// 准备传输数据 VData
		VData tVData = new VData();

		// 保存个人保单信息(保全)
		tVData.addElement(tLCGrpAppntSchema);
		GEdorGrpLPDetailUI tGEdorGrpLPDetailUI = new GEdorGrpLPDetailUI(GI, edorNo, grpContNo);
		if (!tGEdorGrpLPDetailUI.submitData(tVData)) {

			Content = "团体数据保存失败！原因是:" + tGEdorGrpLPDetailUI.getError();
			return false;
		} else {

			Content = "团体数据保存成功";
			return true;
		}
	}

	/**
	 *
	 * 客户理赔金账号变更（个人）
	 *
	 */
	private boolean checkupdatas() {
		System.out.println("==========进入DealLPCF.checkupdatas子类方法===================");
		String FlagStr = "";
		String Content = "";
		String transact ="INSERT||MAIN";
		String Result = "";
		PEdorLPDetailUI tPEdorLPDetailUI = new PEdorLPDetailUI();
		//验空
		if( !StringUtil.StringNull(EdorAcceptNo)
			||!StringUtil.StringNull(mLCGrpContInfo.getGrpContNo()) 
				){
			Content = Content + "团体保单号为空或受理号为空";
			return false;	
		}
		System.out.println(mFormData.getInsuredNo());
		if( !StringUtil.StringNull(mFormData.getInsuredNo())
				||!StringUtil.StringNull(mFormData.getBankCode())
				||!StringUtil.StringNull(mFormData.getBankAccNo())
				||!StringUtil.StringNull(mFormData.getAccName())
				){
			Content = Content +"被保人理赔金账户信息未填写完整，不做修改";
			System.out.println(Content);
			return true;	
			
		}
		try {
            System.out.println("开始处理被保人");
			// 理赔金帐号更改 add by Lanjun 2005-5-24 10:53
			// ---------------------------------------------------------------//
			LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
			tLPDiskImportSchema.setEdorNo(EdorAcceptNo);
			tLPDiskImportSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
			tLPDiskImportSchema.setEdorType(mFormData.getEdorType());
			tLPDiskImportSchema.setInsuredNo(mFormData.getInsuredNo());
			tLPDiskImportSchema.setBankCode(mFormData.getBankCode());
			tLPDiskImportSchema.setBankAccNo(mFormData.getBankAccNo());
			tLPDiskImportSchema.setAccName(mFormData.getAccName());
			String sql = "select a.InsuredNo from LCInsured a, LCCont b " +
					"where '1547103727000'='1547103727000' and  a.ContNo = b.ContNo " +
					" and b.AppFlag = '1' and a.GrpContNo = '"+mLCGrpContInfo.getGrpContNo()+"'" +
					"and a.InsuredNo='"+mFormData.getInsuredNo()+"'" +
					"order by InsuredNo fetch first 3000 rows only with ur ";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS sqlSSRS = tExeSQL.execSQL(sql);
			if(sqlSSRS.getMaxRow() > 0){
				tLPDiskImportSchema.setSerialNo("");
			}else{
				Content = Content +"该被保人客户号未处于承保状态或未实名化到本保单";
				return false;
			}
			
			VData data = new VData();
			data.addElement(GI);
			data.addElement(tLPDiskImportSchema);

			tPEdorLPDetailUI.submitData(data, transact);
		} catch (Exception ex) {
			Content = transact + "被保人保存失败，原因是:" + ex.toString();
			return false;
		}

		if (!tPEdorLPDetailUI.mErrors.needDealError()) {
			flag = "Succ";
			Content =  Content +" 被保人保存成功";
			return true;
		} else {
			Content =  " 被保人保存失败，原因是:" + tPEdorLPDetailUI.mErrors.getFirstError();
			return false;
		}

	}


	/**
	 * 
	 * 保全导入Excel时，
	 * 
	 * 解析Excel数据
	 * 
	 */
	public boolean getExcel(String ExcelURL) {
		System.out.println("==========进入DealLPCF.getExcel===================");

		String tfileName = null;
		String tconfigFileName = null;
		String tedorType = "LP";
		//ftp下载保存的文件路径
		String BasePath = null;
		//ftp下载保存的文件名
		String FileName = null;
		try {
		            FileName = ExcelURL.split("/")[ExcelURL.split("/").length - 1];
		            String FTPPath = ExcelURL.substring(0, ExcelURL.length() - FileName.length());
					ExeSQL exeSQL = new ExeSQL();
					String IpSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='IP'";
					String accNameSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='AccName'";
					String passSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='PassWord'";
					String portSql = "select CODE from ldconfig where codetype = 'WSInterface' and description='Port'";
					String IP = exeSQL.getOneValue(IpSql);
					String accName = exeSQL.getOneValue(accNameSql);
					String passWord = exeSQL.getOneValue(passSql);
					String port = exeSQL.getOneValue(portSql);
					if (!StringUtil.StringNull(IP)
							||!StringUtil.StringNull(accName)
							||!StringUtil.StringNull(passWord)
							||!StringUtil.StringNull(port)
							) {
						Content = Content +"FTP信息 配置错误,检查数据库配置";
						return false;
					}
					String rootPath = getClass().getResource("/").getFile().toString();
					BasePath = rootPath.replaceAll("WEB-INF/classes/", "");
					BasePath += "temp/";

					FTPTool tFTPTool = new FTPTool(IP, accName, passWord, Integer.valueOf(port));
					if (!tFTPTool.loginFTP()) {
						Content = Content + "登陆FTP失败：" + tFTPTool.getErrContent(1);
						return false;
					} else {
						if (!tFTPTool.downloadFile(FTPPath, BasePath, FileName)) {
							Content = Content + "登陆FTP失败：" + tFTPTool.getErrContent(1);
							return false;
						}
					}
			}catch (Exception e) {
				Content = Content+"ftp异常";
						System.out.println(e.toString());
						e.printStackTrace();
						return false;
					} 
		System.out.println("开始解析本地数据本地下载文件不好使的话现在换");
		System.out.println("替换的文件是："+BasePath + FileName);	
		
		//得到excel文件的保存路径
	 	System.out.println("得到Excle文件的保存路径"+BasePath);
			
	  LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
	  tLPDiskImportSchema.setEdorNo(EdorAcceptNo);
	  tLPDiskImportSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
	  tLPDiskImportSchema.setEdorType(mFormData.getEdorType());
	  
	  VData data = new VData();
	  data.add(GI);
	  data.add(tLPDiskImportSchema);
	
		tfileName = BasePath + FileName;
		tconfigFileName = BasePath + BQ.FILENAME_IMPORT;	
		System.out.println("第一个参数："+tfileName+"第二个参数："+tconfigFileName+"第三个参数"+tedorType);
		try{
		GEdorImportUI tGEdorImportUI = new GEdorImportUI(tfileName, tconfigFileName,tedorType);
		
		if (!tGEdorImportUI.submitData(data))
		{
		  flag = "Fail";
		  Content =  "批量磁盘导入失败，原因是：" + tGEdorImportUI.getError() +
			          "\n请检查导入模版的格式和数据！";
		}
		else
		{
		  flag = "Succ";
		  Content =  Content+"批量磁盘导入成功！";
		}	
       }catch (Exception e){
			System.out.println(e.toString());
			Content =  Content+"系统下载的文件异常，无法读取";
			return false;
		}
		return true;
	}

	@Override
	public boolean GEdorConfirmSubmit() {
		// TODO Auto-generated method stub
		System.out.println("子类保全确认后台"); 
		MMap map = new MMap();

		FinanceDataBL tFinanceDataBL = new FinanceDataBL(GI,EdorAcceptNo, BQ.NOTICETYPE_G, "");
		if (!tFinanceDataBL.submitData()) {
			Content = Content += "生成财务数据错误" + tFinanceDataBL.mErrors;
			System.out.println(Content); 
			return false;
		}

		String strTemplatePath = "";
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(EdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(EdorAcceptNo);
		VData data = new VData();
		data.add(strTemplatePath);
		data.add(tLPGrpEdorMainSchema);
		data.add(GI);
		PGrpEdorConfirmBL tPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
		map = tPGrpEdorConfirmBL.getSubmitData(data, "INSERT||GRPEDORCONFIRM");
		if (map == null) {
			Content = Content +"保全结案发生错误" + tFinanceDataBL.mErrors;
			System.out.println(Content); 
			return false;
		}

		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setDetailWorkNo(EdorAcceptNo);
		tLGWorkSchema.setTypeNo("03");

		data = new VData();
		data.add(GI);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (tmap == null) {
			Content = Content +"工单结案失败" + tFinanceDataBL.mErrors;
			System.out.println(Content); 
			return false;
		}
		map.add(tmap);
		if (!submit(map)) {
			return false;
		}
		
		return true;
	}
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			Content = Content + "提交数据库发生错误" + tPubSubmit.mErrors;
			System.out.println(Content); 
			return false;
		}
		return true;
	}
}
