package com.preservation.newCF.LRCF;

import com.preservation.newCF.DealBaseCF;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.GrpEdorLRDetailUI;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * LR保单遗失补发CF 逻辑处理 姜士杰 2019-01-17
 * 
 * @author Administrator
 *
 */
public class DealLRCF extends DealBaseCF {

	public boolean checkSave() {

		String transact = "INSERT";
		String edorNo = dMessHead.getBatchNo();
		String edorType = mFormData.getEdorType();
		String grpContNo = mLCGrpContInfo.getGrpContNo();
		String cost = mFormData.getCost();
		String getMoney = mFormData.getGroupMoney();
		System.out.println("=========================================================" + cost);

		// 准备传输数据 VData
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		// 工本费
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = null;
		tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
		tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
		tLPGrpEdorItemSchema.setEdorType(edorType);
		
		if ("1".equals(cost)) {// 判断是否需要交费
			tLPGrpEdorItemSchema.setGetMoney(getMoney);
			tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema.setEdorAcceptNo(EdorAcceptNo);
			tLPEdorEspecialDataSchema.setEdorNo(EdorAcceptNo);
			tLPEdorEspecialDataSchema.setEdorType(edorType);
			tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
			tLPEdorEspecialDataSchema.setEdorValue(getMoney);
			tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
		} else if ("0".equals(cost)) {
			tLPGrpEdorItemSchema.setGetMoney(0);
		} else {
			Content = "是否收取工本费设置错误!";
			return false;
		}

		VData tVData = new VData();
		tVData.add(GI);
		tVData.add(tLPGrpEdorItemSchema);
		tVData.add(tLPEdorEspecialDataSchema);

		GrpEdorLRDetailUI tGrpEdorLRDetailUI = new GrpEdorLRDetailUI();
		if (!tGrpEdorLRDetailUI.submitData(tVData, transact)) {
			VData rVData = tGrpEdorLRDetailUI.getResult();
			System.out.println("Submit Failed! " + tGrpEdorLRDetailUI.mErrors.getErrContent());
			Content = transact + "失败，原因是:" + tGrpEdorLRDetailUI.mErrors.getFirstError();
			return false;
		} else {
			Content = "保存成功";
		}

		// 写死打印次数
		ExeSQL tExeSQL = new ExeSQL();
		String LCGrpContsql = "update LCGrpCont set PrintCount='0' where GrpContNo='" + mLCGrpContInfo.getGrpContNo()
				+ "'";
		tExeSQL.execUpdateSQL(LCGrpContsql);

		return true;
	}

}
