package com.preservation.newCF.ZGCF;

import com.preservation.newCF.DealBaseCF;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GEdorZGDetailUI;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 追加管理费CF 姜士杰 2019-01-16
 * 
 * @author Administrator
 *
 */
public class DealZGCF extends DealBaseCF {

	/**
	 * 保全项目明细 保存按钮
	 * 
	 * @return
	 */
	public boolean checkSave() {
		ExeSQL tExeSQL = new ExeSQL();
		// List<Element> list = mFormData.getElement();
		/*
		 * String grpPolNo=mFormData.getGrpPolNo(); String
		 * groupMoney=mFormData.getGroupMoney();//获取追加管理费
		 */
		String edorNo = EdorAcceptNo;// 保全号
		String edorType = mFormData.getEdorType();// 保全类型
		String grpContNo = mLCGrpContInfo.getGrpContNo();// 集体保单号

		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo, edorType);

		String sql = "select grppolno from lcgrppol where riskcode='" + mFormData.getGrpPolNo() + "' and grpContNo = '"
				+ grpContNo + "'";
		SSRS ssrs = tExeSQL.execSQL(sql);

		if (ssrs.getMaxRow() < 1) {
			Content = "该保单下未查询到险种！";
			return false;
		}

		String grppolno = "";
		for (int i = 1; i <= ssrs.getMaxRow(); i++) {
			grppolno = (ssrs.GetText(i, 1));
		}

		String groupMoney = mFormData.getGroupMoney().toString();
		System.out.println(groupMoney);

		tSpecialData.setGrpPolNo(grppolno);// 险种号
		tSpecialData.add("GroupMoney", groupMoney);// 追加管理费
		Content = "传入险种不正确！";

		GEdorZGDetailUI tGEdorZGDetailUI = new GEdorZGDetailUI(GI, edorNo, grpContNo, tSpecialData);
		if (!tGEdorZGDetailUI.submitData()) {

			Content = "数据保存失败！原因是:" + tGEdorZGDetailUI.getError();
			return false;
		} else {
			Content = "数据保存成功。";
			return true;
		}

	}

}
