package com.preservation.newCF.ZGCF;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.ZG.DealZG;
import com.preservation.ZG.InterfaceZG;
import com.preservation.newCF.InterfaceBaseCF;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * 追加管理费CF 姜士杰 2019-01-16
 * 
 * @author Administrator
 *
 */
public class InterfaceZGCF extends InterfaceBaseCF {

	/**
	 * 逻辑处理
	 */
	public boolean dealc() {
		// 处理逻辑
		DealZGCF dealZGCF = new DealZGCF();
		if (!dealZGCF.deal(data)) {
			errorValue = dealZGCF.getErrorValue();
			WorkNo = dealZGCF.getEdorAcceptNo();
			return false;
		}
		WorkNo = dealZGCF.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement) {

		System.out.println("====================================解析报文体========================");
		
		// 作为页面session值
		GlobalInput GI = new GlobalInput();
		Element ZGCF = rootElement.getChild("ZGCF");
		// 客户号

		Element msgCustomer = ZGCF.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");
		String WorkNo = eCustomerInfo.getChildText("WorkNo");
		GI.ComCode = eCustomerInfo.getChildText("ManageCom");
		GI.ManageCom = eCustomerInfo.getChildText("ManageCom");
//		GI.Operator = "pa0001";
		GI.Operator = "BAOQA";
		data.add(GI);

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		iCustomerInfo.setWorkNo(WorkNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = ZGCF.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = ZGCF.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		Element xxxFormData = xxFormData.getChild("Pols");

		FormData tFormData = new FormData();

		tFormData.setEdorType(xxFormData.getChildText("EdorType"));
		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));

		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
		
		tFormData.setGrpPolNo(xxxFormData.getChildText("GrpPolNo"));
		tFormData.setGroupMoney(xxxFormData.getChildText("GroupMoney"));
		
		data.add(tFormData);
	}

	public static void main(String[] args) {
		InterfaceZG tInterfaceZG = new InterfaceZG();

		try {
			InputStream pIns = new FileInputStream("C:/Users/dongjl/Desktop/ZG.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceZG.deal(mInXmlStr);
			System.out.println(tInterfaceZG.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
