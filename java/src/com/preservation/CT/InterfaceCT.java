package com.preservation.CT;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.preservation.CT.obj.specialRiskObj;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class InterfaceCT {

	String errorValue; // 错误信息
	VData data = new VData();
	String gongdanhao;
	String cValiDate;

	public boolean deal(String xml) {
		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}
		// 处理逻辑
		DealCT dealCT = new DealCT();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		if (!dealCT.deal(data)) {
			errorValue = dealCT.getContent();
			gongdanhao = dealCT.gettAccpetNo();
			String sql = "insert into ErrorInsertLog " +
					"(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ," +
					"Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ," +
					"transactionBatch ,transactionDescription ,transactionAddress ) " +
					"values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+GI.Operator+"','"
					+xml+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), " +
					"to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), " +
					"'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+returnXML(xml)+"','中间状态','"+errorValue+"','"+mMessHead.getMsgType()+"','"
					+mMessHead.getBranchCode()+"','"+mMessHead.getBatchNo()+"','交易描述','交易地址')";
					tExeSQL.execUpdateSQL(sql);
			return false;
		}
		gongdanhao = dealCT.gettAccpetNo();
		errorValue = "";
		return true;
	}

	/**
	 * 得到报文数据 解析报文
	 * 
	 * @throws Exception
	 */
	public boolean getkData(String xml) {
		try {
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");

			MessHead head = new MessHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			// 作为页面session值
			GlobalInput GI = new GlobalInput();
			GI.Operator = SendOperator;
			GI.ComCode = BranchCode;
			GI.ManageCom = BranchCode;
			data.add(GI);

			// WT团体犹豫期退保
			Element CT = rootElement.getChild("CT");
			// 客户号
			Element msgCustomer = CT.getChild("CustomerInfo");
			Element eCustomerInfo = msgCustomer.getChild("Item");
			String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

			CustomerInfo iCustomerInfo = new CustomerInfo();
			iCustomerInfo.setCustomerNo(CustomerNo);
			data.add(iCustomerInfo);

			// 团单信息
			Element lCGrpContInfo = CT.getChild("LCGrpContInfo");
			Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
			String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
			String GrpName = eLCGrpContInfo.getChildText("GrpName");

			LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
			iLCGrpContInfo.setGrpContNo(GrpContNo);
			iLCGrpContInfo.setGrpName(GrpName);
			data.add(iLCGrpContInfo);

			// 人员信息
			Element xFormData = CT.getChild("FormData");
			Element xxFormData = xFormData.getChild("Item");
			FormData tFormData = new FormData();

			tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
			cValiDate = xxFormData.getChildText("EdorValiDate");
			tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
			tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
			tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
			tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
			tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
			tFormData.setRemark(xxFormData.getChildText("Remark"));
			tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
			
			tFormData.setReason_tb(xxFormData.getChildText("reason_tb"));
			tFormData.setPayMode(xxFormData.getChildText("PayMode"));
			tFormData.setPayDate(xxFormData.getChildText("PayDate"));
			tFormData.setEndDate(xxFormData.getChildText("EndDate"));
			tFormData.setBank(xxFormData.getChildText("Bank"));
			tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

			data.add(tFormData);
			Element xspecialRiskObj = CT.getChild("specialRiskObj");
			specialRiskObj tspecialRiskObj = new specialRiskObj();
			List<Element> itemList = xspecialRiskObj.getChildren("item");
			for (int i = 0; i < itemList.size(); i++) {
				Element item = itemList.get(i);
				specialRiskObj mspecialRiskObj = new specialRiskObj();
				mspecialRiskObj.setRiskcode(item.getChildText("riskcode"));
				mspecialRiskObj.setRateSelect(item.getChildText("rateSelect"));
				mspecialRiskObj.setRealrate(item.getChildText("realrate"));
				tspecialRiskObj.getSpecialTiskRate().add(mspecialRiskObj);
			}
			tspecialRiskObj.setRateMoney(Double.parseDouble(xspecialRiskObj.getChildText("RateMoney")));
			data.add(tspecialRiskObj);
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return true;
	}

	public String returnXML(String xml){
		CreateXMLCT createXMLCT= new CreateXMLCT();
		String returnxml = null;
		try
		{
			returnxml = createXMLCT.createXML(data, errorValue, gongdanhao, cValiDate, xml);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return returnxml;
	}
	
	public static void main(String[] args) {

		InterfaceCT tInterfaceCT = new InterfaceCT();

		try {
			InputStream pIns = new FileInputStream("D:/llll/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(),
					"GBK");
			tInterfaceCT.deal(mInXmlStr);
			tInterfaceCT.returnXML(mInXmlStr);
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}
