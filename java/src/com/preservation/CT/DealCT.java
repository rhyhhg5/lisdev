package com.preservation.CT;

import java.text.DecimalFormat;

import com.preservation.CT.obj.specialRiskObj;
import com.preservation.edorCancel.GEdorCancel;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqConfirmUI;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.FeeNoticeGrpVtsUI;
import com.sinosoft.lis.bq.GrpEdorCTDetailUI;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.PEdorAppAccConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPContPlanRiskSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LPContPlanRiskSet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealCT
{
	private String Content;// 错误信息
	private String tWorkNo = "";
	private String tAccpetNo = "";
	private String tDetailWorkNo = "";
	private String mCurrDate = PubFun.getCurrentDate();

	// 得到data封装的数据
	private MessHead dMessHead;
	private CustomerInfo mCustomerInfo;
	private FormData mFormData;
	private LCGrpContInfo mLCGrpContInfo;
	private specialRiskObj mspecialRiskObj;
	GlobalInput GI = new GlobalInput();

	public boolean deal(VData data)
	{
		dMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		mCustomerInfo = (CustomerInfo) data.getObjectByObjectName(
				"CustomerInfo", 0);
		mFormData = (FormData) data.getObjectByObjectName("FormData", 0);
		mLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName(
				"LCGrpContInfo", 0);
		mspecialRiskObj = (specialRiskObj) data.getObjectByObjectName(
				"specialRiskObj", 0);
		GEdorCancel gEdorCancel = new GEdorCancel();
		VData tt = new VData();

		VData vdata = new VData();
		MMap mmap = new MMap();
		String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,betweenness ,transactionType ,transactionCode ,transactionBatch ,  transactionDescription,   transactionAddress, grpcontno, rgtno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ GI.Operator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','中间状态','"
				+ dMessHead.getMsgType()
				+ "','"
				+ dMessHead.getBranchCode()
				+ "','"
				+ dMessHead.getBatchNo()
				+ "','保全保单解约','交易地址','"
				+ mLCGrpContInfo.getGrpContNo() + "','')";
		mmap.put(insertClaimInsertLogSql, "INSERT");
		vdata.add(mmap);
		PubSubmit pub = new PubSubmit();
		pub.submitData(vdata, "INSERT");
		// 经办
		if (!doBusiness())
		{
			System.out.println("=======经办失败");
			return false;
		}
		// 添加保全项 前台
		if (!addRecord())
		{
			System.out.println("=======添加保全项失败");
			tt.add(tAccpetNo);
			tt.add(GI);
			gEdorCancel.cancelAppEdor(tt);
			return false;
		}
		// 添加保全项 后台
		if (!saveRecord())
		{
			System.out.println("=======添加保全项失败222222");
			tt.add(tAccpetNo);
			tt.add(GI);
			gEdorCancel.cancelAppEdor(tt);
			return false;
		}

		// 保全项目明细校验及调用后台方法
		if (!edorTypeCTSave())
		{
			System.out.println("=======保全项目明细失败");
			tt.add(tAccpetNo);
			tt.add(GI);
			gEdorCancel.cancelAppEdor(tt);
			return false;
		}
		// 保全理算
		if (!edorAppConfirm())
		{
			System.out.println("=======保全理算失败");
			tt.add(tAccpetNo);
			tt.add(GI);
			gEdorCancel.cancelAppEdor(tt);
			return false;
		}
		// 保全确认 前台
		if (!edorConfirm())
		{
			System.out.println("=======保全确认前台失败");
			tt.add(tAccpetNo);
			tt.add(GI);
			gEdorCancel.cancelAppEdor(tt);
			return false;
		}
		// 保全确认 后台
		if (!GEdorConfirmSubmit())
		{
			System.out.println("=======保全确认后台失败");
			tt.add(tAccpetNo);
			tt.add(GI);
			gEdorCancel.cancelAppEdor(tt);
			return false;
		}

		VData adata = new VData();
		MMap amap = new MMap();
		String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"
				+ tAccpetNo
				+ "' WHERE transactionCode = '"
				+ dMessHead.getBranchCode()
				+ "' and transactionBatch='"
				+ dMessHead.getBatchNo() + "' ";
		String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
		String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"
				+ recoinfoId
				+ "', actugetno, paymode, otherno, 'CT','','0', '', '', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"
				+ tAccpetNo
				+ "'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', '', '', '', '' FROM ljaget where otherno ='"
				+ tAccpetNo + "' ";
		amap.put(updateClaimInsertLogSql, "UPDATE");
		amap.put(insertReconcileInfoSql, "INSERT");
		adata.add(amap);
		PubSubmit puba = new PubSubmit();
		puba.submitData(adata, "UPDATE");
		Content = "successful";
		return true;
	}

	/**
	 * 经办 前 信息查询 customerNo GrpCont
	 * 
	 * 经办 wdsave.save(data);
	 */
	public boolean doBusiness()
	{
		String customerNo = mCustomerInfo.getCustomerNo();
		String GrpContNo = mLCGrpContInfo.getGrpContNo();

		// 有效保单或可回退保单 判断
		StringBuffer sql = new StringBuffer();
		if (customerNo.length() == 8)
		{
			sql.append(" select grpContNo  from LCGrpCont  where appntNo = '"
					+ customerNo + "' ");
			sql.append(" and appFlag = '1'  and (StateFlag is null or StateFlag not in('0')) ");
			// 保全回退添加
			sql.append(" union select grpcontno from LBGrpCont b where appntNo = '"
					+ customerNo + "' ");
			sql.append(" and exists (select 1 from lpgrpedoritem where grpcontno =b.grpcontno and edorno = b.edorno and edortype in ('WT','CT',''))");
		} else
		{
			sql.append("  select contNo " + "from LCCont "
					+ "where appntNo = '" + customerNo + "' "
					+ "   and appFlag = '1' ");
			sql.append("   and (StateFlag is null or StateFlag not in('0')) ");
			// 保全回退添加
			sql.append(" union select contno from lbcont b where appntNo = '"
					+ customerNo + "' ");
			sql.append(" and exists (select 1 from lpedoritem where contno = b.contno and edorno = b.edorno and edortype in ('WT','CT','XT'))");
		}
		ExeSQL tExeSQL1 = new ExeSQL();
		SSRS ssrssql = tExeSQL1.execSQL(sql.toString());
		if (ssrssql.getMaxRow() < 1)
		{
			Content = "客户" + customerNo + "没有有效保单或可回退保单。";
			return false;
		}
		// 判断客户号和保单号是否一致
		StringBuffer sqlCusnoGrpconNo = new StringBuffer();
		sqlCusnoGrpconNo.append("select 1 from LCGrpCont where AppntNo ='"
				+ customerNo + "' and GrpContNo = '" + GrpContNo + "' ");
		ExeSQL exeCusnoGrpconNo = new ExeSQL();
		SSRS ssrsCusnoGrpconNo = exeCusnoGrpconNo.execSQL(sqlCusnoGrpconNo
				.toString());
		if (ssrsCusnoGrpconNo.getMaxRow() < 1)
		{
			Content = "客户号" + customerNo + "下没有团体保单号为" + GrpContNo + "的信息";
			return false;
		}
		// 分投保人，主被保人和其它被保人三种情况
		// 保单状态判断
		StringBuffer strSQL = new StringBuffer();
		strSQL.append("select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, a.cinvalidate, ");
		strSQL.append("a.PaytoDate, a.Prem, a.Amnt, codeName('stateflag', StateFlag), b.PostalAddress, case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end ");
		strSQL.append("from   LCCont a, LCAddress b ");
		strSQL.append("where  a.AppntNo = b.CustomerNo ");
		strSQL.append("and    (a.AppntNo = '" + customerNo + "' ");
		strSQL.append(" or    a.InsuredNo = '" + customerNo + "') ");
		strSQL.append("and    a.AppFlag = '1' ");
		strSQL.append("and    b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ");
		strSQL.append("and   (a.StateFlag is null or a.StateFlag not in('0')) "); // 未终止
		// 保全回退需要查询退保保单
		strSQL.append(" union ");
		strSQL.append(" select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, (select edorvalidate from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno),");
		strSQL.append(" a.PaytoDate, a.Prem, a.Amnt,(select case edortype when 'WT' THEN '犹豫期退保' when 'CT' then '解约' end from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno), b.PostalAddress, ");
		strSQL.append(" case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end ");
		strSQL.append(" from LBCont a, LCAddress b ");
		strSQL.append(" where  a.AppntNo = b.CustomerNo and a.appntno = '"
				+ customerNo + "' ");
		strSQL.append(" and b.AddressNo = (select AddressNo from LBAppnt where ContNo = a.ContNo) and exists ( ");
		strSQL.append(" select 1 from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno)");
		ExeSQL tExeSQL = new ExeSQL();
		SSRS ssrsstrSQL = tExeSQL.execSQL(strSQL.toString());
		if (ssrsstrSQL.MaxRow > 0)
		{
			String ContNo = ssrsstrSQL.GetText(1, 1);
			System.out.println("ContNo:" + ContNo);
			System.out.println("查询sql的值：" + ContNo);
			if (ContNo != null && ContNo != "")
			{
				StringBuffer sqlContNo = new StringBuffer();
				sqlContNo
						.append("select StateType,State from LCContState where contno='"
								+ ContNo + "' and PolNo='000000'");
				ExeSQL exeContNo = new ExeSQL();
				SSRS ssrssContNo = exeContNo.execSQL(sqlContNo.toString());
				if (ssrssContNo.getMaxRow() > 0)
				{
					if (ssrssContNo.GetText(1, 1) == "Available"
							&& ssrssContNo.GetText(1, 2) == "1")
					{
						Content = "保单" + ContNo + "处于失效状态!";
						return false;
					}
					if (ssrssContNo.GetText(1, 1) == "Terminate"
							&& ssrssContNo.GetText(1, 2) == "1")
					{
						Content = "保单" + ContNo + "处于失满期终止状态!";
						return false;
					}
				}
			}
		}
		// 保单状态判断
		StringBuffer sqlGrpCont = new StringBuffer();
		sqlGrpCont
				.append(" select a.GrpContNo, a.PrtNo, a.GrpName, a.Peoples2, ");
		sqlGrpCont
				.append(" a.PolApplyDate, a.CValiDate, a.PayMode, a.Prem, a.Amnt, ");
		sqlGrpCont
				.append(" codeName('stateflag', a.StateFlag),  b.GrpAddress, ");
		sqlGrpCont
				.append(" case when exists (select 1 from LCCont where grpContNo = a.grpContNo and polType = '1') then '无名单' else '非无名单' end ");
		sqlGrpCont.append(" from LCGrpCont a, LCGrpAddress b ");
		sqlGrpCont.append(" where a.AppntNo = b.CustomerNo ");
		sqlGrpCont.append(" and   a.AddressNo = b.AddressNo ");
		sqlGrpCont.append(" and   a.AppntNo ='" + customerNo + "' ");
		sqlGrpCont
				.append(" and   (a.StateFlag is null or a.StateFlag not in('0')) ");// 未终止
		sqlGrpCont.append(" and   a.AppFlag = '1'");
		sqlGrpCont.append(" and (state is null or state not in('03030002')) ");// 非续保终止
		sqlGrpCont.append(" and not exists ");
		sqlGrpCont.append(" (select 1 from LPEdorEspecialData a, LGWork b ");
		sqlGrpCont
				.append(" where a.EdorNo = b.WorkNo and b.ContNo = a.GrpContNo ");
		sqlGrpCont.append(" and a.EdorType = 'MJ' ");
		sqlGrpCont.append(" and a.DetailType = 'MJSTATE' ");
		sqlGrpCont.append(" and a.EdorValue != '1' and a.EdorValue != '0')"); // 已满期理算
		ExeSQL exeContNo = new ExeSQL();
		SSRS ssrssContNo = exeContNo.execSQL(sqlGrpCont.toString());
		if (ssrssContNo.getMaxRow() > 0)
		{
			// 生效日期
			String edorValiDate = ssrssContNo.GetText(1, 6);
			System.out.println("edorValiDate=" + edorValiDate);

			String grpcontno = ssrssContNo.GetText(1, 1);
			System.out.println("grpcontno:" + grpcontno);
			if (grpcontno != null && grpcontno != "")
			{
				StringBuffer sqlgrpcontno = new StringBuffer();
				sqlgrpcontno
						.append("select StateType,State from LCGrpContState where GrpContNo='"
								+ grpcontno + "' and GrpPolNo='000000'");
				ExeSQL exegrpcontno = new ExeSQL();
				SSRS ssrssgrpcontno = exegrpcontno.execSQL(sqlgrpcontno
						.toString());

				if (ssrssgrpcontno.getMaxRow() > 0)
				{
					if (ssrssgrpcontno.GetText(1, 1) == "Terminate"
							&& ssrssgrpcontno.GetText(1, 2) == "1")
					{
						Content = "保单" + grpcontno + "处于满期终止状态!";
						return false;
					}
					if (ssrssgrpcontno.GetText(1, 1) == "Pause"
							&& ssrssgrpcontno.GetText(1, 2) == "2")
					{
						Content = "保单" + grpcontno + "处于暂停状态!";
						return false;
					}
				}
			}
		}
		if (!save())
		{
			return false;
		}

		return true;
	}

	/**
	 * 
	 * 经办 保存数据
	 * 
	 * @param data
	 * @return
	 */
	public boolean save()
	{
		VData data = new VData();
		ExeSQL tExeSQL = new ExeSQL();
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		// 业务类型 TypeNo 03(保全)
		tLGWorkSchema.setApplyTypeNo("0");
		// 代理人标记
		tLGWorkSchema.setAgentFlag("1");

		// 输入参数
		tLGWorkSchema.setCustomerNo(mCustomerInfo.getCustomerNo());
		tLGWorkSchema.setCustomerCardNo("");// 团体客户号，没有IDNO
		tLGWorkSchema.setStatusNo("3");
		tLGWorkSchema.setPriorityNo(mFormData.getPriorityNo());
		tLGWorkSchema.setTypeNo("03");
		String DateLimit = "";
		if (!"".equals(mFormData.getWorkTypeNo()))
		{
			String sql = "select DateLimit from LGWorkType where WorkTypeNo = '"
					+ mFormData.getWorkTypeNo() + "'";
			SSRS ss = tExeSQL.execSQL(sql);
			DateLimit = ss.GetText(1, 1);
		}
		tLGWorkSchema.setDateLimit(DateLimit);
		tLGWorkSchema.setApplyTypeNo(mFormData.getApplyTypeNo());
		tLGWorkSchema.setApplyName(mFormData.getApplyName());// 申请人姓名
		tLGWorkSchema.setAcceptWayNo(mFormData.getAcceptWayNo());
		tLGWorkSchema.setAcceptDate("");// 受理日期
		tLGWorkSchema.setRemark(mFormData.getRemark());
		// 代理人标记
		String agentFlag = mFormData.getAgentFlag();
		if (null != agentFlag && !"".equals(agentFlag))
		{
			tLGWorkSchema.setAgentFlag(agentFlag);
			if (agentFlag.equals("0"))
			{
				tLGWorkSchema.setAgentName(mFormData.getProxyName());
				tLGWorkSchema.setAgentIDType(mFormData.getProxyIDType());
				tLGWorkSchema.setAgentIDNo(mFormData.getProxyIDNo());
				tLGWorkSchema.setAgentIDStartDate(mFormData
						.getProxyIDStartDate());
				tLGWorkSchema.setAgentIDEndDate(mFormData.getProxyIDEndDate());
				tLGWorkSchema.setAgentPhone(mFormData.getProxyPhone());
			}
		}

		System.out.println("************代理标记");

		data.add(tLGWorkSchema);
		data.add(GI);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (tTaskInputBL.submitData(data, "") == false)
		{
			System.out.println("数据保存失败！");
		} else
		{
			// 设置显示信息
			VData tRet = tTaskInputBL.getResult();
			LGWorkSchema mLGWorkSchema = new LGWorkSchema();
			mLGWorkSchema.setSchema((LGWorkSchema) tRet.getObjectByObjectName(
					"LGWorkSchema", 0));

			tWorkNo = mLGWorkSchema.getWorkNo();
			tAccpetNo = mLGWorkSchema.getAcceptNo();
			tDetailWorkNo = mLGWorkSchema.getDetailWorkNo();

			System.out.println("数据保存成功，录入工单的受理号为：" + tAccpetNo);
		}
		return true;
	}

	/**
	 * 
	 * 添加保全项目
	 * 
	 */
	public boolean addRecord()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "";
		SSRS sqlSSRS = null;
		// 判断保单是否有万能险种
		if (hasULIRisk(mLCGrpContInfo.getGrpContNo()))
		{
			// 解约保全生效日期必须大于或者等于保单最后一次月结日期
			sql = "select edoracceptno from lpgrpedoritem where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and exists ( "
					+ " select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0' )  ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该团险万能的保单下存在其他未结案的保全项目，不能继续操作！");
				Content = "该团险万能的保单下存在其他未结案的保全项目，不能继续操作！";
				return false;
			}
		}
		if (null == mFormData.getEdorValiDate()
				|| "".equals(mFormData.getEdorValiDate()))
		{
			System.out.println("保全生效日不能为空!");
			Content = "保全生效日不能为空!";
			return false;
		}
		if (mFormData.getEdorType() == null
				|| "".equals(mFormData.getEdorType()))
		{
			System.out.println("保全项目类型不能为空！");
			Content = "保全项目类型不能为空！";
			return false;
		}
		if (!checkGrpEdorType())
		{
			return false;
		}
		// 做过无名单增减人的保单不能犹豫期退保
		if (mFormData.getEdorType() == "WT")
		{
			sql = "select 1 from lpgrpedoritem where grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo() + "' and edortype='WJ'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该集体合同做过无名单减少被保人，不能进行犹豫期退保操作!");
				Content = "该集体合同做过无名单减少被保人，不能进行犹豫期退保操作!";
				return false;
			}
			sql = "select 1 from lmriskapp where riskcode in (select riskcode from lcgrppol where grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo() + "') and risktype4='4'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				sql = "select 1 from ljagetendorse where  grpcontno = '"
						+ mLCGrpContInfo.getGrpContNo()
						+ "' and getmoney<>0 fetch first 1 rows only with ur";
				sqlSSRS = tExeSQL.execSQL(sql);
				if (sqlSSRS.getMaxRow() > 0)
				{
					System.out.println("该团体万能的集体合同做过补退费的保全项目，不能进行犹豫期退保!");
					Content = "该团体万能的集体合同做过补退费的保全项目，不能进行犹豫期退保!";
					return false;
				}
				String sqlLP = "select 1 from llclaimdetail where grpcontno ='"
						+ mLCGrpContInfo.getGrpContNo()
						+ "' fetch first 1 rows only  with ur  ";
				SSRS sqlLPSSRS = tExeSQL.execSQL(sqlLP);
				if (sqlLPSSRS.getMaxRow() > 0)
				{
					System.out.println("团险万能满期保单有过理赔操作,不能做犹豫期退保!");
					Content = "团险万能满期保单有过理赔操作,不能做犹豫期退保!";
					return false;

				}
			}
		}
		// 最后一次续期交费后，做过无名单增减人的保单不能退保
		if (mFormData.getEdorType() == "CT")
		{
			// 判断保单是否有万能险种
			if (hasULIRisk(mLCGrpContInfo.getGrpContNo()))
			{
				// 解约保全生效日期必须大于或者等于保单最后一次月结日期
				String checkDateSQL = " select 1 from lcinsureaccclass where "
						+ " grpcontno='"
						+ mLCGrpContInfo.getGrpContNo()
						+ "' "
						+ " and polno=(select polno from lcpol where grpcontno=lcinsureaccclass.grpcontno and poltypeflag='2') "
						+ " and baladate<='" + mFormData.getEdorValiDate()
						+ "' with ur ";
				SSRS checkDateSQLSSRS = tExeSQL.execSQL(checkDateSQL);
				if (checkDateSQLSSRS.getMaxRow() < 0)
				{
					System.out.println("团险万能解约项目的保全生效日期必须大于或者等于保单最后一次月结日期！");
					Content = "团险万能解约项目的保全生效日期必须大于或者等于保单最后一次月结日期！";
					return false;
				}
			}
			sql = "select 1 from lpgrpedoritem a where grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and edortype='WJ'"
					+ " and exists( select 1 from lpedorapp where edoracceptno=a.edorno and  confdate>= (select max(confdate) from ljapaygrp where a.grpcontno=grpcontno )) ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该集体合同最后一次续期缴费后做过无名单减少被保人，不能进行退保操作!");
				Content = "该集体合同最后一次续期缴费后做过无名单减少被保人，不能进行退保操作!";
				return false;
			}
			// 按照需求增加校验，对于约定交费的团单，只能申请协议退保，不能申请解约。
			sql = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo() + "'  and payintv = -1 ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("该单缴费频次为约定缴费，不能申请解约保全项目");
				Content = "该单缴费频次为约定缴费，不能申请解约保全项目";
				return false;
			}
		}
		sql = "select state from lcgrpbalplan where grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo() + "'";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0 && sqlSSRS.GetText(1, 1) != "0")
		{
			System.out.println("该集体合同目前正在进行定期结算!不能进行保全操作!");
			Content = "该集体合同目前正在进行定期结算!不能进行保全操作!";
			return false;
		}
		sql = "select edorno from lpgrpedoritem a where grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and edortype='TZ' and "
				+ " exists ( select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0' )";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0)
		{
			System.out.println("团体万能增人不能和其他保全项目一起操作!");
			Content = "团体万能增人不能和其他保全项目一起操作!";
			return false;
		}
		String strSQL = "select EdorAcceptNo, EdorNo, EdorType, GrpContNo, "
                + "(select CustomerNo from LCGrpAppnt "
                + " where GrpContNo = LPGrpEdorItem.GrpContNo), "
                + " EdorValiDate, "
                + "	case EdorState "
                + "		when '1' then '录入完毕' "
                + "		when '2' then '理算确认' "
                + "		when '3' then '未录入' "
                + "		when '4' then '试算成功' "
                + "		when '0' then '保全确认' "
                + "	end "
                + "from LPGrpEdorItem "
                + "where EdorAcceptNo='" + tAccpetNo + "' "
                + "	and EdorNo='" + tAccpetNo + "' ";
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		if (strSQLSSRS.getMaxRow() > 0)
		{
			for (int h = 1; h <= strSQLSSRS.getMaxRow(); h++)
			{
				if (strSQLSSRS.GetText(h, 3) == "JM")
				{
					System.out.println("激活卡客户资料变更不能和其他保全项目一起操作!");
					Content = "激活卡客户资料变更不能和其他保全项目一起操作!";
					return false;
				}
				if (strSQLSSRS.GetText(h, 3) == "RS")
				{
					System.out.println("保单暂停功能不能和其他保全项目一起操作!");
					Content = "保单暂停功能不能和其他保全项目一起操作!";
					return false;
				}
				if (strSQLSSRS.GetText(h, 3) == "RR")
				{
					System.out.println("保单恢复功能不能和其他保全项目一起操作!");
					Content = "保单恢复功能不能和其他保全项目一起操作!";
					return false;
				}
			}
		}
		if (mFormData.getEdorType() == "RS")
		{
			boolean flag = true;
			// 是建工险种的不需要配置，可直接添加保单暂停的保全项目
			String checkJianGongXian = "select riskcode from lcgrppol where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo() + "'";
			SSRS checkJianGongXianSSRS = tExeSQL.execSQL(checkJianGongXian);
			if (checkJianGongXianSSRS.getMaxRow() > 0)
			{
				for (int i = 1; i <= checkJianGongXianSSRS.getMaxRow(); i++)
				{
					if (checkJianGongXianSSRS.GetText(1, i) == "190106"
							|| checkJianGongXianSSRS.GetText(1, i) == "590206"
							|| checkJianGongXianSSRS.GetText(1, i) == "5901")
					{
						String checkJianGongXianQiTa = "select 1 from lcgrppol where grpcontno='"
								+ mLCGrpContInfo.getGrpContNo()
								+ "' and riskcode not in ('190106','590206','5901')";
						SSRS checkJianGongXianQiTaSSRS = tExeSQL
								.execSQL(checkJianGongXianQiTa);
						if (checkJianGongXianQiTaSSRS.getMaxRow() > 0)
						{
							System.out.println("该保单不只是含有建工险种，还含有其它险种,无法添加此项目!");
							Content = "该保单不只是含有建工险种，还含有其它险种,无法添加此项目!";
							return false;
						}
					}
				}
				flag = false;
			}
			if (flag)
			{
				String RSSql = "select 1 from LCGrpEdor where ValidFlag='1' and state='0' and GrpContNo = '"
						+ mLCGrpContInfo.getGrpContNo() + "'";
				SSRS RSresult = tExeSQL.execSQL(RSSql);
				if (RSresult.getMaxRow() < 0)
				{
					System.out.println("该保单未在配置表中配置,或者该保单已经暂停期满,无法添加此项目!");
					Content = "该保单未在配置表中配置,或者该保单已经暂停期满,无法添加此项目!";
					return false;
				}
			}
		}
		String IsRSsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and ((enddate is null) or (startdate<=current date  and enddate>current date )) and grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo() + "'";
		SSRS IsRSarrResult = tExeSQL.execSQL(IsRSsql);
		if (IsRSarrResult.getMaxRow() > 0)
		{
			if (mFormData.getEdorType() != "RR")
			{
				System.out.println("该集体合同目前正处于保单暂停状态!不能添加保单恢复外的任何保全操作!");
				Content = "该集体合同目前正处于保单暂停状态!不能添加保单恢复外的任何保全操作!";
				return false;
			}
		} else
		{
			if (mFormData.getEdorType() == "RR")
			{
				System.out.println("该集体合同目前未处于保单暂停状态!不能添加保单恢复的保全操作!");
				Content = "该集体合同目前未处于保单暂停状态!不能添加保单恢复的保全操作!";
				return false;
			}
		}
		// 校验保全生效日是否在暂停期
		String IsRSRRsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and enddate is not null and startdate<='"
				+ mFormData.getEdorValiDate()
				+ "'  and enddate>'"
				+ mFormData.getEdorValiDate()
				+ "'  and grpcontno = '"
				+ mLCGrpContInfo.getGrpContNo() + "'";
		SSRS IsRSRRResult = tExeSQL.execSQL(IsRSRRsql);
		if (IsRSRRResult.getMaxRow() > 0)
		{
			System.out.println("生效日期不可选在该集体合同暂停区间内!");
			Content = "生效日期不可选在该集体合同暂停区间内!";
			return false;
		}
		// add by 特需医疗险种的团单增减人不可同一个工单来做
		sql = "select 1 from lcgrppol a where  grpcontno ='"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype3='7')";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0 && strSQLSSRS.getMaxRow() > 0)
		{
			for (int j = 0; j < strSQLSSRS.getMaxRow(); j++)
			{
				if (strSQLSSRS.GetText(j, 3) == "NI"
						&& (mFormData.getEdorType() == "ZT"))
				{
					System.out.println("特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行减人操作");
					Content = "特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行减人操作";
					return false;
				}
				if (strSQLSSRS.GetText(j, 3) == "ZT"
						&& (mFormData.getEdorType() == "NI"))
				{
					System.out.println("特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行增人操作");
					Content = "特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行增人操作";
					return false;
				}
			}
		}
		if (mFormData.getEdorType() != "AC")
		{
			// 添加对于卡折业务类型保单的保全校验
			sql = "select 1 from lcgrpcont where cardflag ='2' and grpcontno ='"
					+ mLCGrpContInfo.getGrpContNo() + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				String sqlLccont = "select 1 from lccont where  grpcontno='"
						+ mLCGrpContInfo.getGrpContNo()
						+ "'  and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02')";
				SSRS sqlLccontSSRS = tExeSQL.execSQL(sqlLccont);
				if (sqlLccontSSRS.getMaxRow() > 0)
				{
					System.out.println("该保单为激活卡保单,不允许操作任何保全项目");
					Content = "该保单为激活卡保单,不允许操作任何保全项目";
					return false;
				} else
				{
					if (!(mFormData.getEdorType() == "AD")
							|| (mFormData.getEdorType() == "CM"))
					{
						System.out.println("卡折业务的保单只能操作客户资料变更或者联系方式变更的保全项目");
						Content = "卡折业务的保单只能操作客户资料变更或者联系方式变更的保全项目";
						return false;
					}
				}
			}
		}
		// 添加对 AC不可和NI、ZT同时做的校验
		if (mFormData.getEdorType() == "AC")
		{
			sql = "select 1 from lpgrpedoritem where  edorno='" + tAccpetNo
					+ "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和投保单位资料变更一起做。");
				Content = "其他保全项目不可以和投保单位资料变更一起做。";
				return false;

			}
		} else
		{
			sql = "select 1 from lpgrpedoritem where edortype='AC' and edorno='"
					+ tAccpetNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("投保单位资料变更不可以和其他保全一起做。");
				Content = "投保单位资料变更不可以和其他保全一起做。";
				return false;
			}
		}
		// 添加对CM不可和NI、ZT同时做的校验
		if (mFormData.getEdorType() == "CM")
		{
			sql = "select 1 from lpgrpedoritem where  edorno='" + tAccpetNo
					+ "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和团体客户资料变更一起做。");
				Content = "其他保全项目不可以和团体客户资料变更一起做。";
				return false;

			}
		} else
		{
			sql = "select 1 from lpgrpedoritem where edortype='CM' and edorno='"
					+ tAccpetNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("客户资料变更不可以和其他保全一起做。");
				Content = "客户资料变更不可以和其他保全一起做。";
				return false;

			}
		}
		sql = "select 1 from lpgrpedoritem a where grpcontno='"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and edortype='TY' ";
		sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0)
		{
			System.out.println("该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!");
			Content = "该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!";
			return false;
		}

		// 添加对 AC不可和NI、ZT同时做的校验，杨天政 20110711
		if (mFormData.getEdorType() == "UM")
		{
			sql = "select 1 from lpgrpedoritem where  edorno='" + tAccpetNo
					+ "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和团险万能满期一起做。");
				Content = "其他保全项目不可以和团险万能满期一起做。";
				return false;

			}
			sql = "select 1 from lpgrpedoritem a where 1=1 and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and edorno!='"
					+ tAccpetNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("其他保全项目不可以和团险万能满期一起做。");
				Content = "其他保全项目不可以和团险万能满期一起做。";
				return false;
			}

		} else
		{
			sql = "select 1 from lpgrpedoritem where edortype='UM' and edorno='"
					+ tAccpetNo + "'";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("团险万能满期不可以和其他保全一起做。");
				Content = "团险万能满期不可以和其他保全一起做。";
				return false;
			}
		}
		if (mFormData.getEdorType() == "ZF")
		{

			String sqlLcgrpcont = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and APPFLAG='1' and stateflag='1' and signdate is not null ";
			SSRS sqlLcgrpcontSSRS = tExeSQL.execSQL(sqlLcgrpcont);
			if (sqlLcgrpcontSSRS.getMaxRow() < 0)
			{
				System.out.println("该保单为非承保有效保单，不能做终止缴费项目");
				Content = "该保单为非承保有效保单，不能做终止缴费项目";
				return false;

			}

			sql = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo() + "'  and payintv <> 0 ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() < 0)
			{
				System.out.println("只有缴费频次为期缴或“约定缴费”的保单，才能申请终止缴费项目");
				Content = "只有缴费频次为期缴或“约定缴费”的保单，才能申请终止缴费项目";
				return false;

			}

			sql = "select max(dealstate) from ljspayb where otherno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and dealstate not in ('1','2','6') ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				if (sqlSSRS.GetText(1, 1) == "0")
				{
					System.out.println("该保单续期状态为已抽档待收费，不能做终止缴费项目");
					Content = "该保单续期状态为已抽档待收费，不能做终止缴费项目";
				} else if (sqlSSRS.GetText(1, 1) == "4")
				{
					System.out.println("该保单续期状态为已收费待核销，不能做终止缴费项目");
					Content = "该保单续期状态为已收费待核销，不能做终止缴费项目";
				} else
				{
					System.out.println("保单处在续期中间状态下，不能申请终止缴费项目");
					Content = "保单处在续期中间状态下，不能申请终止缴费项目";
				}
				return false;
			}

			sql = "select 1 from lcgrpcont where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "'  and state = '03050002' ";
			sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				System.out.println("满足上述条件的团单只要申请终止缴费结案后，不能再次申请终止缴费");
				Content = "满足上述条件的团单只要申请终止缴费结案后，不能再次申请终止缴费";
				return false;
			}
		}
		// add by 团体公共保额分配的生效日期必须为当天
		if (mFormData.getEdorType() == "GD")
		{
			if (mFormData.getEdorValiDate() != PubFun.getCurrentDate())
			{
				System.out.println("保额分配的保全生效日期必须为当前日期！");
				Content = "保额分配的保全生效日期必须为当前日期！";
				return false;
			}
		}
		/*
		 * 保单下有未结案理赔案件或者有未回销的预付赔款（保单预付赔款余额不为0）时
		 * 保全不能进行结余返还，待理赔案件处理完毕，没有未回销预付赔款时才能做结余返还。
		 */
		if (mFormData.getEdorType() == "BJ")
		{
			String sql_pay = "select sum(b.realpay) from "
					+ "llcase a,llclaimdetail b where " + " a.rgtno = b.rgtno "
					+ " and a.caseno = b.caseno "
					+ " and a.RgtState not in ('09','11','12','14') "
					+ " and b.grpcontno = '" + mLCGrpContInfo.getGrpContNo()
					+ "' " + " group by grpcontno ";

			String sql_bala = "select PrepaidBala from "
					+ " LLPrepaidGrpCont where " + " PrepaidBala > 0 "
					+ " and state = '1' " + " and grpcontno = '"
					+ mLCGrpContInfo.getGrpContNo() + "'";
			SSRS pay_result = tExeSQL.execSQL(sql_pay);
			SSRS bala_result = tExeSQL.execSQL(sql_bala);

			if (pay_result.getMaxRow() > 0 || bala_result.getMaxRow() > 0)
			{
				double bala = 0.0;
				double pay = 0.0;
				if (bala_result.getMaxRow() < 0)
				{
					bala = 0.0;
				} else
				{
					bala = Double.parseDouble(bala_result.GetText(1, 1));
				}
				if (pay_result.getMaxRow() < 0)
				{
					pay = 0.0;
				} else
				{
					pay = Double.parseDouble(pay_result.GetText(1, 1));
				}
				System.out
						.println("该团单未结案赔款"
								+ pay
								+ "元，未回销预付赔款"
								+ bala
								+ "元，无法添加此保全项目。因“未结案赔款”“未回销预付赔款”数据实时更新，请及时根据提示内容进行核实后进行操作。");
				Content = "该团单未结案赔款"
						+ pay
						+ "元，未回销预付赔款"
						+ bala
						+ "元，无法添加此保全项目。因“未结案赔款”“未回销预付赔款”数据实时更新，请及时根据提示内容进行核实后进行操作。";
				return false;
			}
		}
		return true;
	}
	// 判断团单是否有万能险种
		public boolean hasULIRisk(String grpContNo)
		{
			ExeSQL tExeSQL = new ExeSQL();
			String sql = "select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and RiskType4='4' with ur ";
			SSRS sqlSSRS = tExeSQL.execSQL(sql);
			if (sqlSSRS.getMaxRow() > 0)
			{
				return true;
			}
			return false;
		}

		public boolean checkGrpEdorType()
		{
			ExeSQL tExeSQL = new ExeSQL();
			String checkSQL = "select riskcode from lcgrppol where grpcontno='"
					+ mLCGrpContInfo.getGrpContNo()
					+ "' and "
					+ " exists( select 1 from lmriskapp where riskcode=lcgrppol.riskcode and startdate>='2013-11-06' ) and "
					+ " not exists (select 1 from lmriskedoritem where riskcode=lcgrppol.riskcode and edorcode='"
					+ mFormData.getEdorType() + "') ";
			SSRS checkSQLSSRS = tExeSQL.execSQL(checkSQL);
			if (checkSQLSSRS.getMaxRow() > 0)
			{
				SSRS edorName = tExeSQL
						.execSQL("select edorname from lmedoritem where edorcode='"
								+ mFormData.getEdorType()
								+ "' fetch first 1 rows only");
				System.out.println("您的保单投保有险种：'" + checkSQLSSRS.GetText(1, 1)
						+ "'，该险种的[" + edorName.GetText(1, 1)
						+ "]保全功能暂未上线，无法添加保全项目！");
				Content = "您的保单投保有险种：'" + checkSQLSSRS.GetText(1, 1) + "'，该险种的["
						+ edorName.GetText(1, 1) + "]保全功能暂未上线，无法添加保全项目！";
				return false;
			}
			return true;
		}

	/**
	 * 添加保全后保存
	 * 
	 * @param data
	 * @return
	 */
	public boolean saveRecord()
	{
		// 接收信息，并作校验处理。
		// 输入参数
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();

		TransferData tTransferData = new TransferData();
		GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();
		// 后面要执行的动作：添加，修改，删除
		String fmAction = "INSERT||GRPEDORITEM";
		tLPGrpEdorMainSchema.setEdorAcceptNo(mFormData.getEdorAcceptNo());
		tLPGrpEdorMainSchema.setEdorNo(mFormData.getEdorNo());
		tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		tLPGrpEdorMainSchema.setEdorAppDate(mFormData.getEdorAppDate());
		tLPGrpEdorMainSchema.setEdorValiDate(mFormData.getEdorValiDate());

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorAcceptNo(mFormData.getEdorAcceptNo());
		tLPGrpEdorItemSchema.setEdorNo(mFormData.getEdorNo());
		tLPGrpEdorItemSchema.setEdorAppNo(mFormData.getEdorNo());
		tLPGrpEdorItemSchema.setEdorType(mFormData.getEdorType());
		tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		tLPGrpEdorItemSchema.setEdorAppDate(mFormData.getEdorAppDate());
		tLPGrpEdorItemSchema.setEdorValiDate(mFormData.getEdorValiDate());
		tLPGrpEdorItemSchema.setManageCom(mFormData.getManageCom());
		mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);
		try
		{
			// 准备传输数据 VData
			VData tVData = new VData();

			tVData.add(mLPGrpEdorItemSet);
			tVData.add(tLPGrpEdorMainSchema);
			tVData.add(tTransferData);
			tVData.add(GI);

			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			if (!tGrpEdorItemUI.submitData(tVData, fmAction))
			{
				// 得不到未结案的错误信息
				System.out.println("11111------return");
				tVData.clear();
				tVData = tGrpEdorItemUI.getResult();
				// LPGrpEdorItemSet tLPGrpEdorItemSet = (LPGrpEdorItemSet)
				// tVData.getObjectByObjectName("LPGrpEdorItemSet", 0);
				Content = tGrpEdorItemUI.mErrors.getFirstError();
				return false;
			}
		} catch (Exception ex)
		{
			System.out.println("aaaa" + ex.toString());
			Content = ex.toString();
			return false;
		}
		return true;
	}

	/**
	 * 保全项目明细
	 * 
	 * @param data
	 * @return
	 */

	private boolean edorTypeCTSave()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String sqlLMRiskApp = "select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' and RiskType4='4' with ur ";
		SSRS sqlLMRiskAppSSRS = tExeSQL.execSQL(sqlLMRiskApp);
		String riskFlag = "";
		if (sqlLMRiskAppSSRS.getMaxRow() < 0)
		{
			if ("".equals(mFormData.getRiskCode())
					|| null == mFormData.getRiskCode())
			{
				System.out.println("险种编码不能为空！");
				Content = "险种编码不能为空！";
				return false;
			}
		} else
		{
			riskFlag = "ULIRisk_CT";
		}
		String[] riskCodes = mFormData.getRiskCode().split(",");
		String riskCode = "";
		for (int i = 0; i < riskCodes.length; i++)
		{
			riskCode += "'" + riskCodes[i] + "',";
		}
		riskCode = riskCode.substring(0, riskCodes.length - 1);
		String sqlLCGrpPolGrid = "  select a.contPlanCode, f.riskSeqNo, a.riskCode, b.riskName, "
				+ "   (select count(1) "
				+ "   from LCInsured c, LCPol d "
				+ "   where c.contNo = d.contNo "
				+ "       and c.insuredNo = d.insuredNo "
				+ "       and c.contPlanCode = a.ContPlanCode "
				+ "       and d.grpPolNo = f.grpPolNo), "
				+ "   (select sum(prem) "
				+ "   from LCInsured c, LCPol d "
				+ "   where c.contNo = d.contNo "
				+ "       and c.insuredNo = d.insuredNo "
				+ "       and c.contPlanCode = a.ContPlanCode "
				+ "       and d.grpPolNo = f.grpPolNo), "
				+ "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType "
				+ "from LCContPlanRisk a, LMRiskApp b, LCGrpCont e, LCGrpPol f "
				+ "where a.grpContNo = e.grpContNo "
				+ "   and a.riskCode = b.riskCode "
				+ "   and a.grpContNo = f.grpContNo "
				+ "   and a.riskCode = f.riskCode "
				+ "   and a.contPlanCode not in('00', '11') "
				+ "   and b.riskType3 != '7' " + "   and a.grpContNo = '"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' "
				+ " and a.riskCode in ("
				+ riskCode
				+ ") "

				+ "union "

				+ "  select a.contPlanCode, f.riskSeqNo, a.riskCode, b.riskName, "
				+ "   (select count(1) "
				+ "   from LCInsured c, LCPol d "
				+ "   where c.contNo = d.contNo "
				+ "       and c.insuredNo = d.insuredNo "
				+ "       and c.contPlanCode = a.ContPlanCode "
				+ "       and d.grpPolNo = f.grpPolNo), "
				+ "   (select sum(prem) "
				+ "   from LCInsured c, LCPol d "
				+ "   where c.contNo = d.contNo "
				+ "       and c.insuredNo = d.insuredNo "
				+ "       and c.contPlanCode = a.ContPlanCode "
				+ "       and d.grpPolNo = f.grpPolNo), "
				+ "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType "
				+ "from LCContPlanRisk a, LMRiskApp b, LCGrpCont e, LCGrpPol f "
				+ "where a.grpContNo = e.grpContNo "
				+ "   and a.riskCode = b.riskCode "
				+ "   and a.grpContNo = f.grpContNo "
				+ "   and a.riskCode = f.riskCode "
				+ "   and (e.cardFlag = '0' or riskType3 = '7' or risktype8='6' ) "
				+ "   and a.grpContNo = '"
				+ mLCGrpContInfo.getGrpContNo()
				+ "' " + " and a.riskCode in (" + riskCode + ") ";
		SSRS sqlLCGrpPolGridSSRS = tExeSQL.execSQL(sqlLCGrpPolGrid);
		Double selectPrem = 0.0;
		String[] tContPlanCode = null;
		String[] tRiskCode = null;
		String[] tProposalGrpContNo = null;
		String[] tMainRiskCode = null;
		String[] tPlanType = null;
		for (int i = 1; i <= sqlLCGrpPolGridSSRS.MaxRow; i++)
		{
			  tContPlanCode[i-1]= sqlLCGrpPolGridSSRS.GetText(i, 1);
			  tRiskCode[i-1]= sqlLCGrpPolGridSSRS.GetText(i, 3);
			  tProposalGrpContNo[i-1] = sqlLCGrpPolGridSSRS.GetText(i, 7);
			  tMainRiskCode[i-1] = sqlLCGrpPolGridSSRS.GetText(i, 8);
			  tPlanType[i-1] = sqlLCGrpPolGridSSRS.GetText(i, 9);
		}
		
		// 接收信息，并作校验处理。
		// 输入参数
		// 个人批改信息
		System.out.println("-----ACsubmit---");
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

		GrpEdorCTDetailUI tGrpEdorCTDetailUI = new GrpEdorCTDetailUI();

		CErrors tError = null;
		// 后面要执行的动作：添加，修改

		String fmAction = "INSERT||EDORAC";

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		tLPGrpEdorItemSchema.setEdorAcceptNo(tAccpetNo);
		tLPGrpEdorItemSchema.setEdorNo(tAccpetNo);
		tLPGrpEdorItemSchema.setEdorType("CT");
		tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		tLPGrpEdorItemSchema.setReasonCode(mFormData.getReason_tb());
		System.out.println("-----ACsubmit-----riskFlag--" + riskFlag);
		VData tVData = new VData();
		LPContPlanRiskSet tLPContPlanRiskSet = new LPContPlanRiskSet();
		if (riskFlag != null && !"".equals(riskFlag)
				&& "ULIRisk_CT".equals(riskFlag))
		{
			System.out.println("-----ACsubmit---万能保单不需要录入保障计划--riskFlag--");
		} else
		{
			for(int i = 0; i < sqlLCGrpPolGridSSRS.MaxRow; i++){
				LPContPlanRiskSchema tLPContPlanRiskSchema = new LPContPlanRiskSchema();
			      tLPContPlanRiskSchema.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
			      tLPContPlanRiskSchema.setEdorType(tLPGrpEdorItemSchema.getEdorType());
			      tLPContPlanRiskSchema.setGrpContNo(tLPGrpEdorItemSchema.getGrpContNo());
			      tLPContPlanRiskSchema.setProposalGrpContNo(tProposalGrpContNo[i]);
			      tLPContPlanRiskSchema.setContPlanCode(tContPlanCode[i]);
			      tLPContPlanRiskSchema.setMainRiskCode(tMainRiskCode[i]);
			      tLPContPlanRiskSchema.setRiskCode(tRiskCode[i]);
			      tLPContPlanRiskSchema.setPlanType(tPlanType[i]);
			      tLPContPlanRiskSet.add(tLPContPlanRiskSchema);

			      System.out.println(tContPlanCode[i] + ", " + tRiskCode[i]);
				
				selectPrem = selectPrem + Double.parseDouble(sqlLCGrpPolGridSSRS.GetText(i, 6));
			}
		}
	
		
		boolean specialRiskFlag1 = false;
		String edorNo1 = tAccpetNo;
		String edorType1 = "CT";
		String grpContNo1 = mLCGrpContInfo.getGrpContNo();
		
		String bankrate = "";
		String zh[][] = new String [3][6];
		if ((grpContNo1 != null) && (!grpContNo1.equals("")))
		{
			specialRiskFlag1 = CommonBL.hasSpecialRisk(grpContNo1);
			// 判断合同下是否有特需的险种
			if (specialRiskFlag1)
			{
				// 得到帐户信息
				String rate = "";
				String cValiDate = "";
				String edorValiDate = "";
				String accNum = null;
				String accMoney = null;
				String grpAccMoney = null;
				String fixedAccMoney = null;
				String remark = null;
				bankrate = "0.0";
				LCGrpContDB tLCGrpContDB = new LCGrpContDB();
				tLCGrpContDB.setGrpContNo(grpContNo1);
				if (tLCGrpContDB.getInfo())
				{
					cValiDate = StrTool.cTrim(tLCGrpContDB.getCValiDate());

				}
				LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
				tLPGrpEdorItemDB.setEdorAcceptNo(edorNo1);
				tLPGrpEdorItemDB.setEdorNo(edorNo1);
				tLPGrpEdorItemDB.setEdorType(edorType1);
				tLPGrpEdorItemDB.setGrpContNo(grpContNo1);
				if (tLPGrpEdorItemDB.getInfo())
				{
					edorValiDate = tLPGrpEdorItemDB.getEdorValiDate();
				}

				int year = PubFun.calInterval(cValiDate, edorValiDate, "Y");

				String claimsql = "select givetypedesc from LLClaim where rgtNo = (select rgtno from LLRegister a where RgtObjNo ='"
						+ grpContNo1
						+ "' and  rgtno =(select max(rgtno) from LLRegister where RgtObjNo='"
						+ grpContNo1 + "'))";

				System.out.println("\n\n\n" + claimsql);
				SSRS tSSRS = tExeSQL.execSQL(claimsql);
				String claimResult = "";
				String riskcodesql = "select distinct riskcode from lccontplanrisk where grpcontno ='"
						+ grpContNo1 + "'";
				SSRS riskSSRS = tExeSQL.execSQL(riskcodesql);

				if (tSSRS == null || tSSRS.getMaxRow() < 1)
				{
					claimResult = "无记录";
				} else
				{
					claimResult = tSSRS.GetText(1, 1);
					if (claimResult == null || claimResult.equals(""))
					{
						claimResult = "无记录";
					}
				}
				// 查找银行活期利率
				String rateSql = "select rate from ldbankrate where startdate< '"
						// + StrTool.cTrim(tLCGrpContDB.getCValiDate())
						+ edorValiDate
						+ "' and enddate >'"
						// +StrTool.cTrim(tLCGrpContDB.getCValiDate())
						+ edorValiDate
						+ "'		and RateIntv = '0' " // 活期利率
						+ "   and RateType = 'S' "
						+ "   and RateIntvFlag = 'M' ";
				SSRS bankSSRS = tExeSQL.execSQL(rateSql);
				System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx" + rateSql);
				if (bankSSRS == null || bankSSRS.getMaxRow() < 1)
				{
					bankrate = "0.0";
				} else
				{
					bankrate = bankSSRS.GetText(1, 1);
				}
				String specialriskcodeSQL = "select a.riskcode from lcgrppol a,lmriskapp b where grpcontno = '"
						+ grpContNo1
						+ "' and a.riskcode = b.riskcode and (b.RiskType3 = '7' or b.RiskType8='6') ";
				SSRS specialRiskcode = tExeSQL.execSQL(specialriskcodeSQL);
				String insuAccNo = "";
				String sql = "";
				String ratetype1 = "";
				String rate1 = "";
				String ratetype2 = "";
				String rate2 = "";
				String ratetype3 = "";
				String rate3 = "";
				String disable1 = "false";
				String disable2 = "false";
				String disable3 = "false";
				int count = 0;
				
				// 个人
				insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,
						specialRiskcode.GetText(1, 1));

				if (insuAccNo == null || insuAccNo.equals(""))
				{
					// 禁止个人录入
					disable1 = "true";
				} else
				{
					System.out.println(insuAccNo
							+ "wwwwwwwwwwwwwwwwwwwwwwwwwwwww");
					disable1 = "false";
					for (int i = 1; i <= specialRiskcode.getMaxRow(); i++)
					{
						sql = " SELECT '个人帐户',a.riskcode,A.DEFAULTCALTYPE,A.DEFAULTRATE,A.GRPPOLNO,'PersonalRate' FROM LCGRPINTEREST A "
								+ " WHERE A.GRPCONTNO='"
								+ grpContNo1
								+ "' and a.RISKCODE='"
								+ specialRiskcode.GetText(i, 1)
								+ "' and  a.INSUACCNO='" + insuAccNo + "'";
						System.out.println(sql
								+ "wwwwwwwwwwwwwwwwwwwwwwwwwwwww");
						SSRS tSSRS1 = tExeSQL.execSQL(sql);
						if (tSSRS1 == null || tSSRS1.getMaxRow() == 0)
						{
							String defSQL = "select '个人帐户',a.riskcode,'-',0,A.GRPPOLNO,'PersonalRate' from  lcgrppol a"
									+ " where A.GRPCONTNO='"
									+ grpContNo1
									+ "' and a.RISKCODE='"
									+ specialRiskcode.GetText(i, 1) + "'";
							tSSRS1 = tExeSQL.execSQL(defSQL);
						}

						
						for (int j = 1; j <= tSSRS1.getMaxCol(); j++)
						{
							zh[count][j - 1] = tSSRS1.GetText(1, j);
						}
						count++;
					}
				}
				insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,
						specialRiskcode.GetText(1, 1));
				if (insuAccNo == null || insuAccNo.equals(""))
				{
					disable2 = "true";
				} else
				{

					disable2 = "false";
					for (int i = 1; i <= specialRiskcode.getMaxRow(); i++)
					{
						sql = " SELECT '团体帐户',a.riskcode,A.DEFAULTCALTYPE,A.DEFAULTRATE,A.GRPPOLNO,'GroupRate' FROM LCGRPINTEREST A "
								+ " WHERE A.GRPCONTNO='"
								+ grpContNo1
								+ "' and a.RISKCODE='"
								+ specialRiskcode.GetText(i, 1)
								+ "' and  a.INSUACCNO='" + insuAccNo + "'";
						SSRS tSSRS2 = tExeSQL.execSQL(sql);
						if (tSSRS2 == null || tSSRS2.getMaxRow() == 0)
						{
							String defSQL = "select '团体帐户',a.riskcode,'-',0,A.GRPPOLNO,'GroupRate' from  lcgrppol a"
									+ " where A.GRPCONTNO='"
									+ grpContNo1
									+ "' and a.RISKCODE='"
									+ specialRiskcode.GetText(i, 1) + "'";
							tSSRS2 = tExeSQL.execSQL(defSQL);
						}
//						String zh[][] = null;
						for (int j = 1; j <= tSSRS2.getMaxCol(); j++)
						{
							zh[count][j - 1] = tSSRS2.GetText(1, j);
						}
						count++;
					}
				}
				insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_FIXED,
						specialRiskcode.GetText(1, 1));
				if (insuAccNo == null || insuAccNo.equals(""))
				{
					// 禁止团体固定录入
					disable3 = "true";
				} else
				{
					disable3 = "false";
					for (int i = 1; i <= specialRiskcode.getMaxRow(); i++)
					{
						sql = " SELECT '团体固定帐户',a.riskcode,A.DEFAULTCALTYPE,A.DEFAULTRATE,A.GRPPOLNO,'FixedRate' FROM LCGRPINTEREST A "
								+ " WHERE A.GRPCONTNO='"
								+ grpContNo1
								+ "' and a.RISKCODE='"
								+ specialRiskcode.GetText(i, 1)
								+ "' and  a.INSUACCNO='" + insuAccNo + "'";
						SSRS tSSRS3 = tExeSQL.execSQL(sql);
						if (tSSRS3 == null || tSSRS3.getMaxRow() == 0)
						{
							String defSQL = "select '团体固定帐户',a.riskcode,'-',0,A.GRPPOLNO,'FixedRate' from  lcgrppol a"
									+ " where A.GRPCONTNO='"
									+ grpContNo1
									+ "' and a.RISKCODE='"
									+ specialRiskcode.GetText(i, 1) + "'";
							tSSRS3 = tExeSQL.execSQL(defSQL);
						}
//						String zh[][] = null;
						for (int j = 1; j <= tSSRS3.getMaxCol(); j++)
						{
							zh[count][j - 1] = tSSRS3.GetText(1, j);
						}
						count++;
					}
				}
			}
			
		}
		

		 boolean specialRiskFlag = false;
		  String edorNo = tAccpetNo;
		  String edorType = dMessHead.getMsgType(); 
		  String grpContNo = mLCGrpContInfo.getGrpContNo();
		  if ((grpContNo != null) && (!grpContNo.equals("")))
		  {
		      specialRiskFlag  = CommonBL.hasSpecialRisk(grpContNo);
		      //判断合同下是否有特需的险种
		      if (specialRiskFlag)
		      {
		      System.out.println("Specialrisk start!");
		          //得到帐户信息
		          String rate = "";
		          String cValiDate = "";
		          String accNum = null;
		          String accMoney = null;
		          String grpAccMoney = null;
		          String fixedAccMoney = null;
		          String remark = null;
		        
		          LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		          tLCGrpContDB.setGrpContNo(grpContNo);
		          if (tLCGrpContDB.getInfo())
		          {
		            cValiDate = StrTool.cTrim(tLCGrpContDB.getCValiDate());
		            remark = StrTool.cTrim(tLCGrpContDB.getRemark());
		          }						
		          String mngFeeRateSql = "  select b.feeName, a.feeValue "
		                              + "from LCGrpFee a, LMRiskFee b "
		                              + "where a.feeCode = b.feeCode "
		                              + "   and a.insuAccNo = b.insuAccNo "
		                              + "   and a.payPlanCode = b.payPlanCode "
		                              + "   and a.grpContNo = '" + grpContNo + "' "
		                              + "order by a.feeCode ";
//		           ExeSQL tExeSQL = new ExeSQL();
		          SSRS feeRate = tExeSQL.execSQL(mngFeeRateSql);
		          if(feeRate != null && feeRate.getMaxRow() > 0)
		          {
		            for(int i = 1; i <= feeRate.getMaxRow(); i++)
		            {
		              rate += feeRate.GetText(i, 1) + "率：" 
		                + Double.parseDouble(feeRate.GetText(i, 2)) + " ";
		            }
		          }
		          String specialriskcodeSQL = "select b.riskcode,b.risktype3 from lcgrppol a,lmriskapp b where grpcontno = '"
		          							+ grpContNo +"' and a.riskcode = b.riskcode and (risktype3='7' or risktype8='6') with ur ";	
		          SSRS specialRiskcode =  tExeSQL.execSQL(specialriskcodeSQL);
					System.out.println("查询险种:"+specialriskcodeSQL);      					
		          //得到人账户总金额及账户数目
		          String sql = "select count(*), sum(insuAccBala) from LCPol a, LCInsureAcc b " +
		                       "where a.polNo = b.polNo " + 
		                       "and a.grpContNo = b.grpContNo " + 
		                       "and a.polTypeFlag != '" + BQ.POLTYPEFLAG_PUBLIC + "' " +
		                       "and InsuAccNo = '" + CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,specialRiskcode.GetText(1,1)) +"' " +
		                       "and a.GrpContNo = '" + grpContNo + "'";
		                       System.out.println("得到人账户总金额及账户数目:"+sql);
		          SSRS tSSRS = tExeSQL.execSQL(sql);
		          if(tSSRS==null||tSSRS.getMaxRow()<1)
		          {
		          	accNum ="0";
		          	accMoney ="null";
		          }else{
		          accNum = StrTool.cTrim(tSSRS.GetText(1, 1));
		          accMoney = StrTool.cTrim(tSSRS.GetText(1, 2));
		          }
		          
		          if(accMoney.equals("null")||accMoney.equals(""))
		          {
		            accMoney = "0";
		          }
		          //得到团体账户金额
		          sql = "select insuAccBala from LCPol a, LCInsureAcc b " +
		                "where a.PolNo = b.PolNo " +
		                "and a.GrpContNo = b.GrpContNo " +
		                "and a.PolTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC + 
		                "' and a.acctype = '" + BQ.ACCTYPE_PUBLIC + 
		                "' and b.InsuAccNo = '" + CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,specialRiskcode.GetText(1,1)) + "' " +
		                "and a.grpContNo = '" + grpContNo + "' ";
		          tExeSQL = new ExeSQL();
		          grpAccMoney = tExeSQL.getOneValue(sql);
		          
		          if(grpAccMoney==null||grpAccMoney.equals("null")||grpAccMoney.equals(""))
		          {
		            grpAccMoney = "0";
		          }
		          //得到团体固定帐户金额
		          
		          String tRiskType3 = specialRiskcode.GetText(1,2);
		          String acctypePart = null;
		          if(tRiskType3.equals("7"))
		          {
		              acctypePart = " and a.acctype = '"+BQ.ACCTYPE_PUBLIC+"' ";
		          }
		          else
		          {
		              acctypePart = " and (a.acctype <> '"+BQ.ACCTYPE_PUBLIC+"' or a.acctype is null) ";
		          }
		          sql = "select insuAccBala from LCPol a, LCInsureAcc b " +
		                "where a.PolNo = b.PolNo " +
		                "and a.GrpContNo = b.GrpContNo " +
		                "and a.PolTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC + "' " + 
		                acctypePart +
		                "and b.InsuAccNo = '" + CommonBL.getInsuAccNo(BQ.ACCTYPE_FIXED,specialRiskcode.GetText(1,1)) + "' " +
		                "and a.GrpContNo = '" + grpContNo + "' ";
		          tExeSQL = new ExeSQL();
		          fixedAccMoney = tExeSQL.getOneValue(sql);
		          
		          if(fixedAccMoney==null||fixedAccMoney.equals("null")||fixedAccMoney.equals(""))
		          {
		            fixedAccMoney = "0";
		          }
		          DecimalFormat tDecimalFormat = new DecimalFormat("0.00");
//		          System.out.println(Double.parseDouble(grpAccMoney));	
		          String totalAccMoney = tDecimalFormat.format(
		              Double.parseDouble(String.valueOf((Double.parseDouble(accMoney) 
		                + Double.parseDouble(grpAccMoney) 
		                + Double.parseDouble(fixedAccMoney)))));
		          System.out.println("qulq sdkfjsdklfjdsaklfjsadlk;fjafkjasf;jdkfas;");	
		          if(totalAccMoney==null||totalAccMoney.equals("null")) 
		          {
		          	totalAccMoney = "0";
		          }
		          System.out.println("\n\n\n\n\n\n" + totalAccMoney);
		          String s = (new DecimalFormat("0.00")).format(Double.parseDouble(totalAccMoney)); 
		          System.out.println("\n\n\n\n\nfffff: "+s);
		            
		        //得到特需险种号grppolno
		                   
		          String  polnosql="select grppolno "
		          									+	"from lcgrppol "
		          									+ "where riskcode='"
		          									+specialRiskcode.GetText(1,1) 
															  + "' and grpContNo = '" + grpContNo + "' ";
						
						 String GrpPolNo=tExeSQL.getOneValue(polnosql);    

		          

		            if ((edorType != null) && (edorType.equals(BQ.EDORTYPE_NI)))
		            {

		       
		            }
		            if ((edorType != null) && (edorType.equals(BQ.EDORTYPE_ZT)))
		            {
	
		            } 
		        } 
		    }
		//数据提交
		  LPEdorEspecialDataSet tLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
		  String specialFlag = "";
		  if(specialRiskFlag1){
			  specialFlag = "true";
		  }else{
			  specialFlag = "false";
		  }
			if(specialFlag!=null && specialFlag.equals("true"))
			{
				EdorItemSpecialData tEdorItemSpecialData =
					new EdorItemSpecialData(tAccpetNo,tAccpetNo,dMessHead.getMsgType());
				
//				String bankrate = request.getParameter("bankrate");
		     String tGridNo[] = null;  //得到序号列的所有值
		     String tGrid1 [] = null; //得到第1列的所有值
		     String tGrid2 [] = null; //得到第2列的所有值
		     String tGrid3 [] = null;
		     String tGrid4 [] = null;
		     String tGrid5 [] = null;
		     String tGrid6 [] = null;
		     String tGrid7 [] = null;
		     String tGrid8 [] = null;
		     
		     for(int i = 0;i<zh.length;i++){
		    	 tGridNo[i] = Integer.toString(i);
		    	 tGrid1 [i] = zh[i][0];
		    	 tGrid2 [i] = zh[i][1];
		    	 tGrid3 [i] = zh[i][3];
		    	 tGrid4 [i] = zh[i][2];
		    	 tGrid7 [i] = zh[i][4];
		    	 tGrid8 [i] = zh[i][5];
		    	 mspecialRiskObj.getSpecialTiskRate();
		    	 for (int j = 0; j < mspecialRiskObj.getSpecialTiskRate().size(); j++) {
		    		specialRiskObj tspecialRiskObj = new specialRiskObj();
		    		tspecialRiskObj = mspecialRiskObj.getSpecialTiskRate().get(j);
					if(zh[i][1].equals(tspecialRiskObj.getRiskcode())){
						tGrid5 [i] = tspecialRiskObj.getRateSelect();
						tGrid6 [i] = tspecialRiskObj.getRealrate();
					}
				}
		     }
		     
			if(tGridNo!=null)	
			{
				int  count = tGridNo.length; //得到接受到的记录数
				for(int index=0;index< count;index++)
				{
		     		System.out.println("qulquqluquluqluqluq " +count);
						tEdorItemSpecialData.setGrpPolNo(tGrid7[index]);
						System.out.println("fsdajfhnsadjkfhasdjkfhasdjk"+tGrid7[index]);
						if(tGrid5 [index]==null||tGrid5 [index].equals(""))
						{
							tGrid5 [index] = "1";
						}
						if(tGrid5 [index].equals("1"))
						{
						tEdorItemSpecialData.add(tGrid8[index],"0.0");

						}
						if(tGrid5 [index].equals("2"))
						{
							tEdorItemSpecialData.add(tGrid8[index],tGrid3 [index]);
						
						}
						if(tGrid5 [index].equals("3"))
						{
							if(bankrate==null||bankrate.equals(""))
									bankrate = "0.0";
							tEdorItemSpecialData.add(tGrid8[index],bankrate);
						}
						if(tGrid5 [index].equals("4"))
						{
							tEdorItemSpecialData.add(tGrid8[index],tGrid6 [index]);
						
						}
						tEdorItemSpecialData.add(tGrid8[index]+"type",tGrid5 [index]);

		     }
		}
				tEdorItemSpecialData.add("RateMoney",String.valueOf(mspecialRiskObj.getRateMoney()));
				tLPEdorEspecialDataSet=tEdorItemSpecialData.getSpecialDataSet();
				System.out.println(tLPEdorEspecialDataSet.size());
				
			}
		    tVData.add(tLPContPlanRiskSet);
		    tVData.add(tLPEdorEspecialDataSet);
			
		    String FlagStr = "";
		  try
		  {
		    // 准备传输数据 VData
		    tVData.addElement(GI);
		    tVData.addElement(tLPGrpEdorItemSchema);
		    tGrpEdorCTDetailUI.submitData(tVData,fmAction);
			}
			catch(Exception ex)
			{
				Content = fmAction+"失败，原因是:" + ex.toString();
				FlagStr = "Fail";
				System.out.println(Content);
				return false;
			}
		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		  if (FlagStr=="")
		  {
		    tError = tGrpEdorCTDetailUI.mErrors;
		    if (!tError.needDealError())
		    {
		        Content = " 保存成功";
		        FlagStr = "Succ";
		        System.out.println(Content);
		    }
		    else
		    {
		    	Content = " 保存失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    	System.out.println(Content);
		    	return false;
		    }
		  }
		

		return true;
	}

	/**
	 * 保全理算
	 * 
	 * @param data
	 * @return
	 */

	public boolean edorAppConfirm()
	{

		ExeSQL tExeSQL = new ExeSQL();
		String strSQL = " select * from lpgrpedoritem where edoracceptno='"
				+ tAccpetNo + "'";
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		if (strSQLSSRS.getMaxRow() < 0)
		{
			System.out.println("没有可以进行保全理算的保全项目！");
			return false;
		}
		String sqlLpgrp = " select 'x' from lpgrpedoritem where edoracceptno='"
				+ tAccpetNo + "' and EdorState='3'";
		SSRS sqlLpgrpSSRS = tExeSQL.execSQL(sqlLpgrp);
		if (sqlLpgrpSSRS.getMaxRow() > 0)
		{
			System.out.println("本次申请，有保全项目处于未录入状态，不能进行保全理算！");
			return false;
		}
		if ("".equals(tAccpetNo) || null == tAccpetNo)
		{
			System.out.println("请重新申请!");
			return false;
		} else
		{

		}

		// 保全理算调用后台方法
		// 集体批改信息
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(tAccpetNo);
		tLPGrpEdorMainSchema.setEdorNo(tAccpetNo);
		tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		tLPGrpEdorMainSchema.setEdorValiDate(mFormData.getEdorValiDate());
		tLPGrpEdorMainSchema.setEdorAppDate(mCurrDate);

		VData data = new VData();
		data.add(GI);
		data.add(tLPGrpEdorMainSchema);
		PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
		if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM"))
		{
			Content = "保全理算失败！原因是："
					+ tPGrpEdorAppConfirmUI.getError().getErrContent();
			System.out
					.println(tPGrpEdorAppConfirmUI.getError().getErrContent());
		} else
		{
			Content = "保全理算成功。";
		}
		return true;

	}

	/**
	 * 保全确认
	 * 
	 * @return
	 */

	public boolean edorConfirm()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String strSql = "select statusno from lgwork where workno = '"
				+ tAccpetNo + "' ";
		SSRS strSqlSSRS = tExeSQL.execSQL(strSql);
		if ("5".equals(strSqlSSRS.GetText(1, 1)))
		{
			System.out.println("保全已经确认，不可以重复确认！");
			return false;
		}
		if ("88".equals(strSqlSSRS.GetText(1, 1)))
		{
			System.out.println("保全审批意见为[不同意]，请重新录入保全明细或撤销保全工单！");
			return false;
		}
		String findSql = "select serialno from LCUrgeVerifyLog where serialno='"
				+ tAccpetNo + "' ";
		SSRS findSqlSSRS = tExeSQL.execSQL(findSql);
		if (findSqlSSRS.getMaxRow() > 0)
		{
			System.out.println("正在保全确认或者重复理算操作，请不要再次点击保全确认！");
			return false;
		}
		String strSql2 = "select GetMoney from LPGrpEdorMain where EdorAcceptNo = '"
				+ tAccpetNo + "'";
		SSRS strSql2SSRS = tExeSQL.execSQL(strSql2);
		if (strSql2SSRS.getMaxRow() < 0)
		{
			Content = "查询保全主表信息出错！";
			System.out.println(Content);
			return false;
		}
		String getMoney = strSql2SSRS.GetText(1, 1);
		String balanceMethodValue = "1";
		String payMode = mFormData.getPayMode();// 交付费方式报文中传值
		if (!"".equals(getMoney) && "1".equals(balanceMethodValue))
		{
			if ("".equals(payMode) || null == payMode)
			{
				Content = "交费/付费方式不能为空。";
				System.out.println(Content);
				return false;
			}
			if (Double.parseDouble(getMoney) > 0)
			{
				if ("1".equals(payMode) || "2".equals(payMode))
				{
					String endDate = mFormData.getEndDate();// 截止日期报文传值
					if ("".equals(endDate))
					{
						Content = "截止日期不能为空";
						System.out.println(Content);
						return false;
					}
				}
			}
		}

		if ("4".equals(payMode))
		{
			String bank = mFormData.getBank();// 转账银行报文传值
			String bankAccno = mFormData.getBankAccNo();// 转账账号报文传值
			if ("".equals(bank))
			{
				Content = "转帐银行为空不能选择银行转帐！";
				System.out.println(Content);
				return false;
			}
			if ("".equals(bankAccno))
			{
				Content = "转帐帐号为空不能选择银行转帐！";
				System.out.println(Content);
				return false;
			}
		}

		return true;
	}

	/**
	 * 保全确认调用后台
	 * 
	 * @return
	 */
	public boolean GEdorConfirmSubmit()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String flag = "";
		String edorAcceptNo = tAccpetNo;
		String payMode = mFormData.getPayMode();
		String balanceMethodValue = "1";
		String sql = "select OtherNo from LPEdorApp where EdorAcceptNo = '"
				+ edorAcceptNo + "' ";
		SSRS result = tExeSQL.execSQL(sql);
		String customerNo = result.GetText(1, 1);
		sql = "select getmoney from LPEdorApp where EdorAcceptNo='"
				+ edorAcceptNo + "'";
		result = tExeSQL.execSQL(sql);
		if (result.MaxRow < 0)
		{
			Content = "查询保全申请主表时出错，保全受理号为" + edorAcceptNo + "！";
			System.out.println(Content);
			return false;
		}
		String accType = "";
		String destSource = "";
		String fmtransact = "";
		String fmtransact2 = "NOTUSEACC";
		if (Double.parseDouble(result.GetText(1, 1)) > 0)
		{
			accType = "0";
			destSource = "01";
			fmtransact = "1";// 交费
			// fmtransact2 = "INSERT||TakeOut";
		} else
		{
			accType = "1";
			destSource = "11";
			fmtransact = "0";// 退费
			// fmtransact2 = "INSERT||ShiftTo";
		}
		String contType = "";
		GlobalInput gi = GI;

		String failType = null;
		String autoUWFail = null;

		System.out.println("fmtransact2:" + fmtransact2);

		// 若使用账户冲抵保全收退费，则默认为即时结算
		if (!"NOTUSEACC".equals(fmtransact2))
		{
			balanceMethodValue = "1";
		}
		// qulq 2007-4-5
		String checkSQL = "select 1 from LPGrpEdorItem " + "where EdorNo = '"
				+ edorAcceptNo + "' " + "and EdorType in ('LQ','ZB')";
		SSRS rs = new ExeSQL().execSQL(checkSQL);
		if (rs != null && rs.getMaxRow() > 0)
		{
			if (balanceMethodValue.equals("0"))
			{
				flag = "Fail";
				Content = "部分领取、追加保费项目不得进行定期结算，请单独操作并进行即时结算";
				System.out.println(Content);
				return false;
			}
		}

		// xiep 2008-12-1 增加做NI项目时会增人重复的校验
		String checkNI = "select 1 from LPGrpEdorItem " + "where EdorNo = '"
				+ edorAcceptNo + "' " + "and EdorType = 'NI' ";
		SSRS mssrs = new ExeSQL().execSQL(checkNI);
		if (mssrs != null && mssrs.getMaxRow() > 0)
		{
			// 增加理算后对于lcinsured是否缺失的校验 add by xp 100604
			String checkInsured = " select 1 from lccont where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"
					+ edorAcceptNo
					+ "' fetch first 1 row only) and contno not in "
					+ "(select distinct contno from lcinsured where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"
					+ edorAcceptNo
					+ "' fetch first 1 row only) ) and contno in (select contno from lcinsuredlist where edorno='"
					+ edorAcceptNo + "') ";
			SSRS mssrs1 = new ExeSQL().execSQL(checkInsured);
			if (mssrs1 != null && mssrs1.getMaxRow() > 0)
			{
				flag = "Fail";
				Content = "保全项目增人时出现lcinsured丢失的问题,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
				System.out.println(Content);
				return false;
			}
			// ------------------insured校验结束-------------------

			String sql1 = "select getmoney from lpgrpedoritem "
					+ "where EdorNo = '" + edorAcceptNo + "' "
					+ "and EdorType = 'NI' ";
			SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
			String itemmoney = tSSRS1.GetText(1, 1);
			String sql2 = "select sum(getmoney) from ljsgetendorse "
					+ " where endorsementno = '" + edorAcceptNo + "' "
					+ " and feeoperationtype='NI' and feefinatype='BF' with ur";
			SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
			String endorsemoney = tSSRS2.GetText(1, 1);

			if (!itemmoney.equals(endorsemoney))
			{
				flag = "Fail";
				Content = "保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
				System.out.println(Content);
				return false;
			}
			// -----------------------关于增人总数和实际的理算的人数的比较-----------------------
			String sql_check_ljagetendorse = "  select count(distinct a.contno),count(distinct b.contno) "
					+ "  from ljsgetendorse a, lcinsuredlist b "
					+ "  where a.grpcontno = b.grpcontno "
					+ "  and a.endorsementno = '"
					+ edorAcceptNo
					+ "'"
					+ "  and a.feeoperationtype = 'NI' "
					+ "  and a.endorsementno=b.edorno "
					+ "  and b.state='1' "
					+ "  with ur";
			// ---------------------------------------------------------
			SSRS t_check_ljagendorse_SSRS1 = new ExeSQL()
					.execSQL(sql_check_ljagetendorse);
			// ---------------------------------------------------------
			System.out.println("sql_check_ljagetendorse:"
					+ sql_check_ljagetendorse);
			int the_number_of_1 = Integer.parseInt(t_check_ljagendorse_SSRS1
					.GetText(1, 1));
			int the_number_of_2 = Integer.parseInt(t_check_ljagendorse_SSRS1
					.GetText(1, 2));
			System.out.println("the_number_of_1:" + the_number_of_1);
			System.out.println("the_number_of_2:" + the_number_of_2);
			if (the_number_of_1 != the_number_of_2)
			{
				flag = "Fail";
				Content = "实际理算完成的人数和模板录入的人数不一致，请点击[重复理算]回退到保全明细录入页面，并重新录入[NI]项目明细";
				System.out.println(Content);
				return false;
			}

		}

		// xiep 2010-4-21 增加做ZT项目时会错误的校验
		String checkZT = "select 1 from LPGrpEdorItem " + "where EdorNo = '"
				+ edorAcceptNo + "' " + "and EdorType = 'ZT' ";
		SSRS mssrs1 = new ExeSQL().execSQL(checkZT);
		if (mssrs1 != null && mssrs1.getMaxRow() > 0)
		{
			String sql3 = "select sum(getmoney) from lpedoritem "
					+ "where EdorNo = '" + edorAcceptNo + "' "
					+ "and EdorType = 'ZT' ";
			SSRS tSSRS3 = new ExeSQL().execSQL(sql3);
			String itemmoneyZT = tSSRS3.GetText(1, 1);
			String sql4 = "select case when sum(getmoney) is null then 0 else sum(getmoney) end from ljsgetendorse "
					+ " where endorsementno = '"
					+ edorAcceptNo
					+ "' "
					+ " and feeoperationtype='ZT'  with ur";
			SSRS tSSRS4 = new ExeSQL().execSQL(sql4);
			String endorsemoneyZT = tSSRS4.GetText(1, 1);

			if (!itemmoneyZT.equals(endorsemoneyZT))
			{
				flag = "Fail";
				Content = "保全项目减人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[ZT]项目明细重试";
				System.out.println(Content);
				return false;
			}
		}

		// xiep 2009-6-24 增加做WS项目时会实名化重复的校验
		String checkWS = "select 1 from LPGrpEdorItem " + "where EdorNo = '"
				+ edorAcceptNo + "' " + "and EdorType = 'WS' ";
		SSRS mrs = new ExeSQL().execSQL(checkWS);
		if (mrs != null && mrs.getMaxRow() > 0)
		{
			String wssql = "select 1 from lpcont where edorno='" + edorAcceptNo
					+ "' group by insuredno having count(contno)>1";
			SSRS wsSSRS = new ExeSQL().execSQL(wssql);
			if (wsSSRS != null && wsSSRS.getMaxRow() > 0)
			{
				flag = "Fail";
				Content = "无名单实名化时增人出现重复数据,请点击[重复理算]回退到保全明细后重新添加[WS]项目明细";
				System.out.println(Content);
				return false;
			}
		}

		if (!flag.equals("Fail"))
		{
			EdorItemSpecialData tSpecialData = new EdorItemSpecialData(
					edorAcceptNo, "YE");
			tSpecialData.add("CustomerNo", customerNo);
			tSpecialData.add("AccType", accType);
			tSpecialData.add("OtherType", "3");
			tSpecialData.add("OtherNo", edorAcceptNo);
			tSpecialData.add("DestSource", destSource);
			tSpecialData.add("ContType", contType);
			tSpecialData.add("Fmtransact2", fmtransact2);

			LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
			tLCAppAccTraceSchema.setCustomerNo(customerNo);
			tLCAppAccTraceSchema.setAccType(accType);
			tLCAppAccTraceSchema.setOtherType("3");// 团单
			tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
			tLCAppAccTraceSchema.setDestSource(destSource);
			tLCAppAccTraceSchema.setOperator(gi.Operator);

			VData tVData = new VData();
			tVData.add(tLCAppAccTraceSchema);
			tVData.add(tSpecialData);
			tVData.add(gi);
			PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
			if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param"))
			{
				flag = "Fail";
				Content = "处理帐户余额失败！";
				System.out.println(Content);
				return false;
			} else
			{
				BqConfirmUI tBqConfirmUI = new BqConfirmUI(gi, edorAcceptNo,
						BQ.CONTTYPE_G, balanceMethodValue);
				if (!tBqConfirmUI.submitData())
				{
					flag = "Fail";
					Content = tBqConfirmUI.getError();
					System.out.println(Content);
					return false;
				} else
				{
					System.out.println("交退费通知书" + edorAcceptNo);
					String strSql = "select a.BankCode, a.BankAccno, a.AccName "
							+ "from LCGrpCont a, LPGrpEdorMain b "
							+ "where a.GrpContNo = b.GrpContNo "
							+ "and   b.EdorAcceptNo = '"
							+ edorAcceptNo
							+ "' and a.BankAccno ='"
							+ mFormData.getBankAccNo()
							+ "'";
					SSRS rss = new ExeSQL().execSQL(strSql);
					String accName = "";
					if (rss.MaxRow > 0)
					{
						accName = rss.GetText(1, 3);
					}
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("payMode", payMode);
					tTransferData.setNameAndValue("endDate",
							mFormData.getPayDate());
					tTransferData.setNameAndValue("payDate",
							mFormData.getPayDate());
					tTransferData.setNameAndValue("bank", mFormData.getBank());
					tTransferData.setNameAndValue("bankAccno",
							mFormData.getBankAccNo());
					tTransferData.setNameAndValue("accName", accName);

					// 生成交退费通知书
					FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(
							edorAcceptNo);
					if (!tFeeNoticeGrpVtsUI.submitData(tTransferData))
					{
						flag = "Fail";
						Content = "生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError();
						System.out.println(Content);
						return false;
					}

					VData data = new VData();
					data.add(gi);
					data.add(tTransferData);
					SetPayInfo spi = new SetPayInfo(edorAcceptNo);
					if (!spi.submitDate(data, fmtransact))
					{
						System.out.println("设置转帐信息失败！");
						flag = "Fail";
						Content = "设置收退费方式失败！原因是："
								+ spi.mErrors.getFirstError();
						System.out.println(Content);
						return false;
					}
					flag = "Succ";
					Content = "保全确认成功！";
					System.out.println(Content);
					String message = tBqConfirmUI.getMessage();
					if ((message != null) && (!message.equals("")))
					{
						Content += "\n" + tBqConfirmUI.getMessage();
					}
				}
				Content = PubFun.changForHTML(Content);
				failType = tBqConfirmUI.getFailType(); // 是否审批通过
				autoUWFail = tBqConfirmUI.autoUWFail(); // 是否自核通过
				autoUWFail = autoUWFail == null ? "" : autoUWFail;
			}
		}
		System.out.println("end");
		return true;
	}

	public String getContent()
	{
		return Content;
	}

	public String gettAccpetNo()
	{
		return tAccpetNo;
	}

	public static void main(String[] args)
	{
		DealCT ct = new DealCT();
		ct.doBusiness();
	}

}
