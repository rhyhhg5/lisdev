package com.preservation.CT.obj;

import java.util.List;

public class specialRiskObj {
	private String riskcode;
	private double RateMoney;//约定利息金额
	private String rateSelect;//1|无息^2|约定满期利率^3|活期利率^4|约定其他利率
	private String realrate;//其他利率
	private List<specialRiskObj> specialTiskRate;
	
	
	
	public String getRiskcode() {
		return riskcode;
	}
	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}
	public double getRateMoney() {
		return RateMoney;
	}
	public void setRateMoney(double rateMoney) {
		RateMoney = rateMoney;
	}
	public String getRateSelect() {
		return rateSelect;
	}
	public void setRateSelect(String rateSelect) {
		this.rateSelect = rateSelect;
	}
	public String getRealrate() {
		return realrate;
	}
	public void setRealrate(String realrate) {
		this.realrate = realrate;
	}
	public List<specialRiskObj> getSpecialTiskRate() {
		return specialTiskRate;
	}
	public void setSpecialTiskRate(List<specialRiskObj> specialTiskRate) {
		this.specialTiskRate = specialTiskRate;
	}
	
	
}
