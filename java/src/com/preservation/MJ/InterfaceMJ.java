package com.preservation.MJ;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.Element;

import com.preservation.BQBase.InterfaceBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;

/**
 * BJ结余返还
 * @author fq
 *
 */
public class InterfaceMJ extends InterfaceBase{

	/**
	 * 逻辑处理
	 */
	public boolean dealc(){
		DealMJ dealMJ = new DealMJ();
		if (!dealMJ.deal(data)) {
			errorValue = dealMJ.getErrorValue();
			gongdanhao = dealMJ.getEdorAcceptNo();
			
			return false;
		}
		gongdanhao = dealMJ.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	
	

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement ){
		Element MJ = rootElement.getChild("MJ");
		// 客户号
		
		Element msgCustomer = MJ.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = MJ.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 
		Element xFormData = MJ.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		tFormData.setEdorValue(Double.parseDouble(xxFormData.getChildText("EdorValue")));
		
		tFormData.setPayMode(xxFormData.getChildText("AccRate"));
		tFormData.setPayMode(xxFormData.getChildText("RateType"));
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
		
		data.add(tFormData);
	}
	
	public static void main(String[] args) {
		InterfaceMJ tInterfaceBJ = new InterfaceMJ();
		
		try {
			InputStream pIns = new FileInputStream("C:/Users/Administrator/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceBJ.deal(mInXmlStr);
			System.out.println(tInterfaceBJ.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}


}
