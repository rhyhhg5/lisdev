package com.preservation.MJ;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;

public class CreateXMLMJ {


	/**
	 * 
	 * 生成返回报文，errorValue为收集的处理信息
	 * 
	 * @param data
	 *            报文数据
	 * @param errorValue
	 *            处理时的信息
	 * @return
	 * @throws Exception
	 */
	public String createXML(VData data, String errorValue, String EdorAcceptNo, String mCurrDate, String xml)
			throws Exception {
		System.out.println("创建报文时的值：" + errorValue + ":" + EdorAcceptNo + ":" + mCurrDate);
		MessHead vMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		LCGrpContInfo mLCGrpContTable = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		Element root = new Element("DataSet");
		Document doc = new Document(root);
		Element eMsgHead = new Element("MsgHead");
		Element eHeadnode = new Element("Item");
		Element eHeadBatchNo = new Element("BatchNo");
		eHeadBatchNo.setText(vMessHead.getBatchNo());
		eHeadnode.addContent(eHeadBatchNo);
		Element eHeadSendDate = new Element("SendDate");
		eHeadSendDate.setText(PubFun.getCurrentDate());
		eHeadnode.addContent(eHeadSendDate);
		Element eHeadSendTime = new Element("SendTime");
		eHeadSendTime.setText(PubFun.getCurrentTime());
		eHeadnode.addContent(eHeadSendTime);
		Element eHeadBranchCode = new Element("BranchCode");
		eHeadBranchCode.setText(vMessHead.getBranchCode());
		eHeadnode.addContent(eHeadBranchCode);
		Element eHeadSendOperator = new Element("SendOperator");
		eHeadSendOperator.setText(vMessHead.getSendOperator());
		eHeadnode.addContent(eHeadSendOperator);
		Element eHeadMsgType = new Element("MsgType");
		eHeadMsgType.setText(vMessHead.getMsgType());
		eHeadnode.addContent(eHeadMsgType);
		if (!"".equals(errorValue) && null != errorValue) {
			Element eHeadState = new Element("State");
			eHeadState.setText("01");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrCode = new Element("ErrCode");
			eHeadErrCode.setText("01");
			eHeadnode.addContent(eHeadErrCode);
			Element eHeadErrInfo = new Element("ErrInfo");
			eHeadErrInfo.setText(errorValue);
			eHeadnode.addContent(eHeadErrInfo);
		} else {
			Element eHeadState = new Element("State");
			eHeadState.setText("00");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrCode = new Element("ErrCode");
			eHeadErrCode.setText("00");
			eHeadnode.addContent(eHeadErrCode);
			Element eHeadErrInfo = new Element("ErrInfo");
			eHeadErrInfo.setText("操作成功！");
			eHeadnode.addContent(eHeadErrInfo);
		}
		eMsgHead.addContent(eHeadnode);

		Element eMsgInfo = new Element("EndorsementInfo");
		Element eInfodnode = new Element("Item");
		Element eInfoEdorNo = new Element("EdorNo");
		eInfoEdorNo.setText(EdorAcceptNo);
		eInfodnode.addContent(eInfoEdorNo);
		Element eInfoEdorValidate = new Element("EdorValidate");
		eInfoEdorValidate.setText(mCurrDate);
		eInfodnode.addContent(eInfoEdorValidate);
		Element eInfoEdorConfdate = new Element("EdorConfdate");
		eInfoEdorConfdate.setText(PubFun.getCurrentDate());
		eInfodnode.addContent(eInfoEdorConfdate);
		eMsgInfo.addContent(eInfodnode);

		Element eMsgLCGrpContInfo = new Element("LCGrpContInfo");
		Element eLCGrpContInfonode = new Element("Item");
		Element eMsgGrpContNo = new Element("GrpContNo");
		eMsgGrpContNo.setText(mLCGrpContTable.getGrpContNo());
		eLCGrpContInfonode.addContent(eMsgGrpContNo);
		Element eMsgGrpName = new Element("GrpName");
		eMsgGrpName.setText(mLCGrpContTable.getGrpName());
		eLCGrpContInfonode.addContent(eMsgGrpName);
		eMsgLCGrpContInfo.addContent(eLCGrpContInfonode);

//		Element eMsgInfo1 = new Element("LCInsuredTable");
//		LCInsuredList lLCInsuredList = (LCInsuredList) data.getObjectByObjectName("LCInsuredList", 0);
//		List tLCInsuredList = lLCInsuredList.getLCInsuredList();
//		eMsgInfo1.setChildren(tLCInsuredList);
		root.addContent(eMsgHead);
		root.addContent(eMsgInfo);
		root.addContent(eMsgLCGrpContInfo);
//		root.addContent(eMsgInfo1);
		XMLOutputter out = new XMLOutputter();
		String responseXml = out.outputString(doc);
		return responseXml;
	}


}
