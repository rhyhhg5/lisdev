package com.preservation.MJ;

import com.preservation.BQBase.DealBase;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GEdorMJDetailUI;
import com.sinosoft.lis.schema.LPGrpPolSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * MJ续保续期—特需险满期结
 * 
 * @author hy
 * 
 */
public class DealMJ extends DealBase
{
	

	

	/**
	 * 保全项目明细
	 * 
	 * @param data
	 * @return
	 */
	public boolean checkSave()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String[] riskCodes = mFormData.getRiskCode().split(",");
		String riskCode = "";
		for(int i=0; i<riskCodes.length;i++){
			riskCode = riskCode + "'" + riskCodes[i] + "',";
		}
		riskCode = riskCode.substring(0,riskCode.length()-1);
		String sqlLCGrpPolGrid = "  select a.grpPolNo, a.riskCode, b.riskName "
	            + "from LCGrpPol a, LMRisk b "
	            + "where a.riskCode = b.riskCode "
	            +" and a.riskCode in "+ riskCode +" "
	            + "   and a.grpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' ";
		SSRS sqlLCGrpPolGridSSRS = tExeSQL.execSQL(sqlLCGrpPolGrid);
		if ("".equals(mFormData.getAccRate()) || null == mFormData.getAccRate())
		{
			
			System.out.println("利息/利率不能为空！");
			Content = "利息/利率不能为空！";
			return false;
		}
		if ("".equals(mFormData.getRateType())
				|| null == mFormData.getRateType())
		{
			System.out.println("利息/利率的类型不能为空！");
			Content = "利息/利率的类型不能为空！";
			return false;
		}
		EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(EdorAcceptNo, "MJ");
		LPGrpPolSchema tLPGrpPolSchema = new LPGrpPolSchema();
		for(int i=1; i <= sqlLCGrpPolGridSSRS.getMaxRow(); i++){
			// 默认利率
			if (mFormData.getRateType() == "3"){
				String sqlDefaultRate = "  select DefaultRate "
						+ "from LCGrpInterest " + "where GrpPolNo = '"
						+ sqlLCGrpPolGridSSRS.GetText(i, 1) + "' ";
				SSRS sqlDefaultRateSSRS = tExeSQL.execSQL(sqlDefaultRate);
				if (sqlDefaultRateSSRS.getMaxRow() > 0)
				{
					if (mFormData.getAccRate() != sqlDefaultRateSSRS.GetText(1, 1))
					{
						System.out.println("您选择了默认利率，不能修改默认利率值");
						Content = "您选择了默认利率，不能修改默认利率值";
						return false;
					}
				}
			}
			String interestGrp = "0";
			String interestInsured = "0";
			
			String edorNo = EdorAcceptNo;
			String edorType = "MJ";
			String grpPolNo = sqlLCGrpPolGridSSRS.GetText(i, 1);
			
			
			tEdorItemSpecialData.setPolNo(grpPolNo);
			tEdorItemSpecialData.add(BQ.DETAILTYPE_ACCRATE, mFormData.getAccRate());
			tEdorItemSpecialData.add(BQ.DETAILTYPE_ENDTIME_RATETYPE, mFormData.getRateType());
			System.out.println("\n\n\n\n" + mFormData.getAccRate() + " "
					+ mFormData.getRateType());
			
			
			tLPGrpPolSchema.setEdorNo(edorNo);
			tLPGrpPolSchema.setEdorType(edorType);
			tLPGrpPolSchema.setGrpPolNo(grpPolNo);
		}
		
		  VData data = new VData();
		  data.add(GI);
		  data.add(tLPGrpPolSchema);
		  data.add(tEdorItemSpecialData);
		  
		  GEdorMJDetailUI tGEdorMJDetailUI = new GEdorMJDetailUI();
		  if(!tGEdorMJDetailUI.submitData(data, ""))
		  {
		    Content = tGEdorMJDetailUI.mErrors.getLastError();
		  }
		  else
		  {
		    Content = "满期理算成功";
		  }
		  
		  
		return true;
	}

	

}
