package com.preservation.AC;

import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;

public class CreatXMLAC {

	/**
	 * 
	 * 生成返回报文，errorValue为收集的处理信息
	 * 
	 * @param data
	 *            报文数据
	 * @param errorValue
	 *            处理时的信息
	 * @return
	 * @throws Exception
	 */
	public static String createXML(String errorValue, String EdorNo, String xml) throws Exception {

		StringReader reader = new StringReader(xml);
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document readdoc = tSAXBuilder.build(reader);

		Element rootElement = readdoc.getRootElement();
		// 报文头
		Element msgHead = rootElement.getChild("MsgHead");
		Element elehead = msgHead.getChild("Item");
		String BatchNo = elehead.getChildText("BatchNo");
		String SendDate = elehead.getChildText("SendDate");
		String SendTime = elehead.getChildText("SendTime");
		String BranchCode = elehead.getChildText("BranchCode");
		String SendOperator = elehead.getChildText("SendOperator");
		String MsgType = elehead.getChildText("MsgType");

		Element root = new Element("DataSet");
		Document doc = new Document(root);
		Element eMsgHead = new Element("MsgHead");
		Element eHeadnode = new Element("Item");
		Element eHeadBatchNo = new Element("BatchNo");
		eHeadBatchNo.setText(BatchNo);
		eHeadnode.addContent(eHeadBatchNo);
		Element eHeadSendDate = new Element("SendDate");
		eHeadSendDate.setText(SendDate);
		eHeadnode.addContent(eHeadSendDate);
		Element eHeadSendTime = new Element("SendTime");
		eHeadSendTime.setText(SendTime);
		eHeadnode.addContent(eHeadSendTime);
		Element eHeadBranchCode = new Element("BranchCode");
		eHeadBranchCode.setText(BranchCode);
		eHeadnode.addContent(eHeadBranchCode);
		Element eHeadSendOperator = new Element("SendOperator");
		eHeadSendOperator.setText(SendOperator);
		eHeadnode.addContent(eHeadSendOperator);
		Element eHeadMsgType = new Element("MsgType");
		eHeadMsgType.setText(MsgType);
		eHeadnode.addContent(eHeadMsgType);
		if (!"".equals(errorValue) && null != errorValue) {
			Element eHeadState = new Element("State");
			eHeadState.setText("01");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrInfo = new Element("ErrInfo");
			eHeadErrInfo.setText(errorValue);
			eHeadnode.addContent(eHeadErrInfo);
		} else {
			Element eHeadState = new Element("State");
			eHeadState.setText("00");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrInfo = new Element("ErrInfo");
			eHeadErrInfo.setText("操作成功！");
			eHeadnode.addContent(eHeadErrInfo);
		}
		eMsgHead.addContent(eHeadnode);

		Element eMsgInfo = new Element("EndorsementInfo");
		Element eInfodnode = new Element("Item");
		Element eInfoEdorNo = new Element("EdorNo");
		eInfoEdorNo.setText(EdorNo);
		eInfodnode.addContent(eInfoEdorNo);
		Element eInfoEdorValidate = new Element("EdorValidate");
		eInfoEdorValidate.setText(PubFun.getCurrentDate());
		eInfodnode.addContent(eInfoEdorValidate);
		Element eInfoEdorConfdate = new Element("EdorConfdate");
		eInfoEdorConfdate.setText(PubFun.getCurrentDate());
		eInfodnode.addContent(eInfoEdorConfdate);
		eMsgInfo.addContent(eInfodnode);

		root.addContent(eMsgHead);
		root.addContent(eMsgInfo);
		XMLOutputter out = new XMLOutputter();
		String responseXml = out.outputString(doc);
		System.out.println("1111111111" + responseXml);
		return responseXml;
	}
}
