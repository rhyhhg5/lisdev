package com.preservation.AC.obj;

import java.io.Serializable;

/**
 * AC非无名单投保资料变更
 * 
 * @author LiHao
 *
 */
public class ACLCInsuredInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String GrpName;// 投保团体名称
	private String Phone;// 联系电话
	private String GrpAddress;// 投保单位地址
	private String GrpZipCode;// 邮政编码
	private String TaxNo;// 税务登记证号码
	private String LegalPersonName;// 法人姓名
	private String LegalPersonIDNo;// 法人身份证号
	private String BusinessType;// 行业性质
	private String GrpNature;// 企业类型
	private String Peoples;// 员工总人数 大于 0
	private String OnWorkPeoples;// 在职人数 大于 0
	private String OffWorkPeoples;// 退休人数
	private String OtherPeoples;// 其他人员数 员工总人数 = 在职人数 + 退休人数 + 其他人员数
	private String LinkMan1;// 联系人姓名
	private String Phone1;// 联系电话
	private String Fax1;// 传真
	private String E_Mail1;// E_Mail1
	
	private String IDType;// 
	private String IDNo;// 
	private String IDStartDate;// 
	private String IDEndDate;// 
	private String IDLongEffFlag;// 
	private String Mobile1;
	public String getGrpName() {
		return GrpName;
	}
	public void setGrpName(String grpName) {
		GrpName = grpName;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getGrpAddress() {
		return GrpAddress;
	}
	public void setGrpAddress(String grpAddress) {
		GrpAddress = grpAddress;
	}
	public String getGrpZipCode() {
		return GrpZipCode;
	}
	public void setGrpZipCode(String grpZipCode) {
		GrpZipCode = grpZipCode;
	}
	public String getTaxNo() {
		return TaxNo;
	}
	public void setTaxNo(String taxNo) {
		TaxNo = taxNo;
	}
	public String getLegalPersonName() {
		return LegalPersonName;
	}
	public void setLegalPersonName(String legalPersonName) {
		LegalPersonName = legalPersonName;
	}
	public String getLegalPersonIDNo() {
		return LegalPersonIDNo;
	}
	public void setLegalPersonIDNo(String legalPersonIDNo) {
		LegalPersonIDNo = legalPersonIDNo;
	}
	public String getBusinessType() {
		return BusinessType;
	}
	public void setBusinessType(String businessType) {
		BusinessType = businessType;
	}
	public String getGrpNature() {
		return GrpNature;
	}
	public void setGrpNature(String grpNature) {
		GrpNature = grpNature;
	}
	public String getPeoples() {
		return Peoples;
	}
	public void setPeoples(String peoples) {
		Peoples = peoples;
	}
	public String getOnWorkPeoples() {
		return OnWorkPeoples;
	}
	public void setOnWorkPeoples(String onWorkPeoples) {
		OnWorkPeoples = onWorkPeoples;
	}
	public String getOffWorkPeoples() {
		return OffWorkPeoples;
	}
	public void setOffWorkPeoples(String offWorkPeoples) {
		OffWorkPeoples = offWorkPeoples;
	}
	public String getOtherPeoples() {
		return OtherPeoples;
	}
	public void setOtherPeoples(String otherPeoples) {
		OtherPeoples = otherPeoples;
	}
	public String getLinkMan1() {
		return LinkMan1;
	}
	public void setLinkMan1(String linkMan1) {
		LinkMan1 = linkMan1;
	}
	public String getPhone1() {
		return Phone1;
	}
	public void setPhone1(String phone1) {
		Phone1 = phone1;
	}
	public String getFax1() {
		return Fax1;
	}
	public void setFax1(String fax1) {
		Fax1 = fax1;
	}
	public String getE_Mail1() {
		return E_Mail1;
	}
	public void setE_Mail1(String e_Mail1) {
		E_Mail1 = e_Mail1;
	}
	public String getIDType() {
		return IDType;
	}
	public void setIDType(String iDType) {
		IDType = iDType;
	}
	public String getIDNo() {
		return IDNo;
	}
	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
	public String getIDStartDate() {
		return IDStartDate;
	}
	public void setIDStartDate(String iDStartDate) {
		IDStartDate = iDStartDate;
	}
	public String getIDEndDate() {
		return IDEndDate;
	}
	public void setIDEndDate(String iDEndDate) {
		IDEndDate = iDEndDate;
	}
	public String getIDLongEffFlag() {
		return IDLongEffFlag;
	}
	public void setIDLongEffFlag(String iDLongEffFlag) {
		IDLongEffFlag = iDLongEffFlag;
	}
	public String getMobile1() {
		return Mobile1;
	}
	public void setMobile1(String mobile1) {
		Mobile1 = mobile1;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
