package com.preservation.XT;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.preservation.XT.obj.RiskCodeObj;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * ceiyi 
 * 
 * @author fq
 *
 */
public class InterfaceXT {

	String errorValue; // 错误信息
	VData data = new VData();
	String gongdanhao;
	String cValiDate;
//com.preservation.XT.DealXT
	public boolean deal(String xml) throws ParseException {
		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}
		// 处理逻辑
		DealXT dealXT = new DealXT();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		if (!dealXT.deal(data)) {
			errorValue = dealXT.getContent();
			gongdanhao = dealXT.getEdorAcceptNo();
			String sql = "insert into ErrorInsertLog "
					+ "(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,"
					+ "Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,"
					+ "transactionBatch ,transactionDescription ,transactionAddress ) "
					+ "values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ GI.Operator
					+ "','"
					+ xml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), "
					+ "to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), "
					+ "'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ returnXML(xml) + "','中间状态','" + errorValue + "','"
					+ mMessHead.getMsgType() + "','"
					+ mMessHead.getBranchCode() + "','"
					+ mMessHead.getBatchNo() + "','交易描述','交易地址')";
			tExeSQL.execUpdateSQL(sql);
			return false;
		}
		gongdanhao = dealXT.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	/**
	 * 得到报文数据 解析报文
	 * 
	 * @throws ParseException
	 * 
	 * @throws Exception
	 */
	public boolean getkData(String xml) throws ParseException {
		try {
			
			System.out.println("====================================解析报文========================");
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");

			MessHead head = new MessHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			// 作为页面session值
			GlobalInput GI = new GlobalInput();
			GI.Operator = "pa0001";
			GI.ComCode = "86360500";
			GI.ManageCom ="86360500";
			data.add(GI);
			Element body = rootElement.getChild("MsgBody");

			Element XT = body.getChild("XT");
			// 客户号
			Element msgCustomer = XT.getChild("CustomerInfo");
			Element eCustomerInfo = msgCustomer.getChild("Item");
			String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

			CustomerInfo iCustomerInfo = new CustomerInfo();
			iCustomerInfo.setCustomerNo(CustomerNo);
			data.add(iCustomerInfo);

			// 团单信息
			Element lCGrpContInfo = XT.getChild("LCGrpContInfo");
			Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
			String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
			String GrpName = eLCGrpContInfo.getChildText("GrpName");

			LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
			iLCGrpContInfo.setGrpContNo(GrpContNo);
			iLCGrpContInfo.setGrpName(GrpName);
			data.add(iLCGrpContInfo);

			// 人员信息
			Element xFormData = XT.getChild("FormData");
			Element xxFormData = xFormData.getChild("Item");
			FormData tFormData = new FormData();
			tFormData.setEdorType("XT");
			tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
			cValiDate = xxFormData.getChildText("EdorValiDate");
			tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
			tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
			tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
			tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
			tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
			tFormData.setRemark(xxFormData.getChildText("Remark"));
			tFormData.setReason_tb(xxFormData.getChildText("reason_tb"));

			tFormData.setPayMode(xxFormData.getChildText("PayMode"));
			tFormData.setPayDate(xxFormData.getChildText("PayDate"));
			tFormData.setEndDate(xxFormData.getChildText("EndDate"));
			tFormData.setBank(xxFormData.getChildText("Bank"));
			tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));

			Element XTriskCodeObj = XT.getChild("riskCodeObj");
			RiskCodeObj XTRiskCodeObj = new RiskCodeObj();
			
			List<RiskCodeObj> RiskCodeObjSet = new ArrayList<RiskCodeObj>();
			List<Element> itemList = XTriskCodeObj.getChildren("Item");
			for (int i = 0; i < itemList.size(); i++) {
				Element item = itemList.get(i);
				RiskCodeObj XtRiskCodeObj = new RiskCodeObj();
				String RiskCode = item.getChildText("riskCode");
				System.out.println("==============================解析到报文险种代码为："
						+ RiskCode);
				XtRiskCodeObj.setPlanCode(item.getChildText("PlanCode"));

				XtRiskCodeObj.setRiskCode(item.getChildText("riskCode"));
				XtRiskCodeObj.setGetMoney(item.getChildText("GetMoney"));
				XtRiskCodeObj.setXTFeeRate(item.getChildText("XTFeeRate"));
				XtRiskCodeObj.setXTFee(item.getChildText("XTFee"));
				RiskCodeObjSet.add(XtRiskCodeObj);
				XTRiskCodeObj.setRiskCodeObj(RiskCodeObjSet);

			}
			data.add(tFormData);
			data.add(XTRiskCodeObj);
		
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return true;
	}

	public String returnXML(String xml) {
		CreateXMLXT createXMLXT = new CreateXMLXT();
		String returnxml = null;
		try {
			returnxml = createXMLXT.createXML(data, errorValue, gongdanhao,
					cValiDate, xml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnxml;

	}

	public static void main(String[] args) {
		InterfaceXT tInterfaceCM = new InterfaceXT();

		try {
			InputStream pIns = new FileInputStream(
					"C:/Users/dongjl/Desktop/CM.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(),
					"GBK");
			tInterfaceCM.deal(mInXmlStr);
			System.out.println(tInterfaceCM.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
