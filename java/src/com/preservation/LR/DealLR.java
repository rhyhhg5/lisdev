package com.preservation.LR;

import com.preservation.BQBase.DealBase;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.GrpEdorLRDetailUI;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * LR保单遗失补发
 * @author fq
 *
 */
public class DealLR extends DealBase{

	

	public boolean checkSave(){
		
		CErrors tError = null; 
		String tRela  = "";                
		String FlagStr = "";

		String transact = "INSERT";  

		String edorNo = EdorAcceptNo;
		String edorType = "LR";
		String grpContNo = mLCGrpContInfo.getGrpContNo();
		String getMoney = mFormData.getCost();
		String needGetMoney="Yes";	


		  // 准备传输数据 VData
		  		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
				tLPGrpEdorItemSchema.setEdorNo(edorNo);
				tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
				tLPGrpEdorItemSchema.setEdorType(edorType);
				if("Yes".equals(needGetMoney)){//判断是否需要交费
				tLPGrpEdorItemSchema.setGetMoney(getMoney);
				}else{
				tLPGrpEdorItemSchema.setGetMoney(0);
				}
			//工本费 
			LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = null;
			String costCheck = "1";
			if(costCheck != null && costCheck.equals("1"))
			{
			    tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			    tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
			    tLPEdorEspecialDataSchema.setEdorNo(edorNo);
			    tLPEdorEspecialDataSchema.setEdorType(edorType);
			    tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
			    tLPEdorEspecialDataSchema.setEdorValue(mFormData.getCost());
			    tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
			}	
			VData tVData = new VData();
			tVData.add(GI);
			tVData.add(tLPGrpEdorItemSchema);
			
			tVData.add(tLPEdorEspecialDataSchema);

			GrpEdorLRDetailUI tGrpEdorLRDetailUI = new GrpEdorLRDetailUI();
			if (!tGrpEdorLRDetailUI.submitData(tVData, transact))
			{
				VData rVData = tGrpEdorLRDetailUI.getResult();
				System.out.println("Submit Failed! " + tGrpEdorLRDetailUI.mErrors.getErrContent());
				Content = transact + "失败，原因是:" + tGrpEdorLRDetailUI.mErrors.getFirstError();
				FlagStr = "Fail";
				return false;
			}
			else 
			{
				Content = "保存成功";
				FlagStr = "Success";
			}
		
		return true;
	}
	
	
	
}
