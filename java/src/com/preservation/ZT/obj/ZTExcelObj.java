package com.preservation.ZT.obj;

public class ZTExcelObj {
	private String CalTime;//计算方式
	private String FeeRate;//手续费比例
	private String Excel;//人员清单地址
	public String getCalTime() {
		return CalTime;
	}
	public void setCalTime(String calTime) {
		CalTime = calTime;
	}
	public String getFeeRate() {
		return FeeRate;
	}
	public void setFeeRate(String feeRate) {
		FeeRate = feeRate;
	}
	public String getExcel() {
		return Excel;
	}
	public void setExcel(String excel) {
		Excel = excel;
	}
	
	
}
