package com.preservation.ZT;

import com.preservation.ZT.obj.ZTExcelObj;
import com.preservation.edorCancel.GEdorCancel;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqConfirmUI;
import com.sinosoft.lis.bq.DiskImportZTUI;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.FeeNoticeGrpVtsUI;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.GrpEdorZTDetailUI;
import com.sinosoft.lis.bq.PEdorAppAccConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealZT {
	private String Content;
	GlobalInput GI = new GlobalInput();
	String EdorAcceptNo = "";
	private MessHead dMessHead;
	private CustomerInfo mCustomerInfo;
	private FormData mFormData;
	private LCGrpContInfo mLCGrpContInfo;
	private ZTExcelObj mZTExcelObj;
	
	public boolean deal(VData data) {
		dMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		mCustomerInfo = (CustomerInfo) data.getObjectByObjectName("CustomerInfo", 0);
		mFormData = (FormData) data.getObjectByObjectName("FormData", 0);
		mLCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		mZTExcelObj = (ZTExcelObj) data.getObjectByObjectName("ZTExcelObj", 0);
		GEdorCancel tGEdorCancel = new GEdorCancel();
		VData tt = new VData();
		
		VData vdata = new VData();
		MMap mmap = new MMap();
		String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,betweenness ,transactionType ,transactionCode ,transactionBatch ,  transactionDescription,   transactionAddress, grpcontno, rgtno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"+GI.Operator+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','中间状态','"+dMessHead.getMsgType()+"','"+dMessHead.getBranchCode()+"','"+dMessHead.getBatchNo()+"','保全团单减少被保险人','交易地址','"+mLCGrpContInfo.getGrpContNo()+"','')";
		mmap.put(insertClaimInsertLogSql, "INSERT");
		vdata.add(mmap);
		PubSubmit pub = new PubSubmit();
		pub.submitData(vdata, "INSERT");
		
		// 经办
		if (!doBusiness()) {
			System.out.println("=======经办失败");
			return false;
		}
		//添加保全项 前台
		if(!addRecord()){
			System.out.println("=======添加保全项失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		//添加保全项 后台
		if(!grpEdorItemSave()){
			System.out.println("=======添加保全项失败222222");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		
		//保全项明细  导入减人清单
		if(!importFile()){
			System.out.println("=======减人清单导入失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		
		//保全理算
		if(!edorAppConfirm()){
			System.out.println("=======保全理算失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		//保全确认 前台
		if(!edorConfirm()){
			System.out.println("=======保全确认前台失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		//保全确认 后台
		if(!GEdorConfirmSubmit()){
			System.out.println("=======保全确认后台失败");
			tt.add(GI);
			tt.add(EdorAcceptNo);
			tGEdorCancel.cancelAppEdor(tt);
			return false;
		}
		vdata.clear();
		MMap amap = new MMap();
		String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"+EdorAcceptNo+"' WHERE transactionCode = '"+dMessHead.getBranchCode()+"' and transactionBatch='"+dMessHead.getBatchNo()+"' ";
		String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
		String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT '"+recoinfoId+"', actugetno, paymode, otherno, 'ZT','','0', '', '', '1', '成功', (select EdorValiDate from LPGrpEdorMain where EdorAcceptNo='"+EdorAcceptNo+"'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', '', '', '', '' FROM ljaget where otherno ='"+EdorAcceptNo+"' ";
		amap.put(updateClaimInsertLogSql, "UPDATE");
		amap.put(insertReconcileInfoSql, "INSERT");
		vdata.add(amap);
		PubSubmit puba = new PubSubmit();
		puba.submitData(vdata, "UPDATE");
		return true;
	}
	
	/**
	 * 
	 * 经办
	 * 
	 * @return
	 */
	private boolean doBusiness() {
		//前台校验
//		dMessHead.getBatchNo();
		String grpcontno = mLCGrpContInfo.getGrpContNo();
		ExeSQL tExeSQL = new ExeSQL();
		if(grpcontno!=null&&!"".equals(grpcontno))
		{
		  String sql = "select StateType,State from LCGrpContState where GrpContNo='"+grpcontno+"' and GrpPolNo='000000'";
		  SSRS result = tExeSQL.execSQL(sql);
			if(result.MaxRow>0){
			//alert("result[0][0]===="+result[0][0]);
			  if("Pause".equals(result.GetText(1, 1))&&"2".equals(result.GetText(1, 2))){
				  Content = "保单"+grpcontno+"处于暂停状态!";
				  System.out.println(Content);
				  return false;
			  }
			  if("Terminate".equals(result.GetText(1, 1))&&"1".equals(result.GetText(1, 2))){
				  Content = "保单"+grpcontno+"处于失满期终止状态!";
				  System.out.println(Content);
				  return false;
			  }
			}
		}
		
		//后台save提交 mOption = "DOBUSINESS";
		String FlagStr;
		String tWorkNo = "";
		String tDetailWorkNo = "";
		
		//得到业务类型
		String tTypeNo = "03";//01-保全
		
		String statusNo = "2";
		
		//输入参数
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(mCustomerInfo.getCustomerNo());
		tLGWorkSchema.setCustomerCardNo("");//团体客户号，没有IDNO
		tLGWorkSchema.setStatusNo(statusNo);
		tLGWorkSchema.setPriorityNo(mFormData.getPriorityNo());
		tLGWorkSchema.setTypeNo(tTypeNo);
		String DateLimit = "";
		if(!"".equals(mFormData.getWorkTypeNo())){
			String sql = "select DateLimit from LGWorkType where WorkTypeNo = '"+mFormData.getWorkTypeNo()+"'";
			SSRS ss = tExeSQL.execSQL(sql);
			DateLimit = ss.GetText(1, 1);
		}
		tLGWorkSchema.setDateLimit(DateLimit);
		tLGWorkSchema.setApplyTypeNo(mFormData.getApplyTypeNo());
		tLGWorkSchema.setApplyName(mFormData.getApplyName());//申请人姓名
		tLGWorkSchema.setAcceptWayNo(mFormData.getAcceptWayNo());
		tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());//受理日期
		tLGWorkSchema.setRemark(mFormData.getRemark());
		//代理人标记
		String agentFlag=mFormData.getAgentFlag();
		if(null!=agentFlag && !"".equals(agentFlag)){
			tLGWorkSchema.setAgentFlag(agentFlag);
			if(agentFlag.equals("0")){
				tLGWorkSchema.setAgentName(mFormData.getProxyName());
				tLGWorkSchema.setAgentIDType(mFormData.getProxyIDType());
				tLGWorkSchema.setAgentIDNo(mFormData.getProxyIDNo());
				tLGWorkSchema.setAgentIDStartDate(mFormData.getProxyIDStartDate());
				tLGWorkSchema.setAgentIDEndDate(mFormData.getProxyIDEndDate());
				tLGWorkSchema.setAgentPhone(mFormData.getProxyPhone());
			}
		}
		System.out.println(agentFlag+"************代理标记");
		VData tVData = new VData();
		tVData.add(tLGWorkSchema);
		tVData.add(GI);
		
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (tTaskInputBL.submitData(tVData, "") == false)
		{
			FlagStr = "Fail";
			Content = "数据保存失败！";
			System.out.println(Content);
			return false;
		}
		else
		{
			//设置显示信息
			VData tRet = tTaskInputBL.getResult();
			LGWorkSchema mLGWorkSchema = new LGWorkSchema();
			mLGWorkSchema.setSchema((LGWorkSchema) tRet.getObjectByObjectName("LGWorkSchema", 0));
			
			tWorkNo = mLGWorkSchema.getWorkNo();
			EdorAcceptNo = mLGWorkSchema.getAcceptNo();
			tDetailWorkNo = mLGWorkSchema.getDetailWorkNo();
			
			//把WorkNo保存到session中
//			session.setAttribute("WORKNOINPUT", tWorkNo);
			
			FlagStr = "Succ";
			Content = "数据保存成功，录入工单的受理号为：" + EdorAcceptNo;
			System.out.println(Content);
		}
		return true;
	}
	
	/**
	 * 
	 * 添加保全项目
	 * 
	 */
	public boolean addRecord()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String grpcontno = mLCGrpContInfo.getGrpContNo();
		//判断保单是否有万能险种
		String sql = "select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"+grpcontno+"' and RiskType4='4' with ur ";
		SSRS result = tExeSQL.execSQL(sql);
		if(result.MaxRow>0){
			 //解约保全生效日期必须大于或者等于保单最后一次月结日期
			String checkHaveEdor = "select edoracceptno from lpgrpedoritem where grpcontno='" + grpcontno + "' and exists ( " +
				" select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0' )  ";		   
			result = tExeSQL.execSQL(checkHaveEdor);
			if(result.MaxRow>0)
			{
				Content = "该团险万能的保单下存在其他未结案的保全项目，不能继续操作！";
				System.out.println(Content);
				return false;
			}
		}
		   
		if(mFormData.getEdorValiDate() == null || "".equals(mFormData.getEdorValiDate()))
		{
			Content = "保全生效日不能为空!";
			System.out.println(Content);
			return false ;
		}
			
	    if (dMessHead.getMsgType() == null || "".equals(dMessHead.getMsgType()))
	    {
	        Content = "请选择需要添加的保全项目";
	        System.out.println(Content);
	        return false;    
	    }
	    
	    String checkSQL = "select riskcode from lcgrppol where grpcontno='"+grpcontno+"' and " +
		" exists( select 1 from lmriskapp where riskcode=lcgrppol.riskcode and startdate>='2013-11-06' ) and " +
		" not exists (select 1 from lmriskedoritem where riskcode=lcgrppol.riskcode and edorcode='"+dMessHead.getMsgType()+"') ";
		
		SSRS riskCode = tExeSQL.execSQL(checkSQL);
		if(riskCode.MaxRow>0 && !"".equals(riskCode.GetText(1,1))){
			SSRS edorName = tExeSQL.execSQL("select edorname from lmedoritem where edorcode='"+dMessHead.getMsgType()+"' fetch first 1 rows only");
			Content = "您的保单投保有险种：'"+riskCode.GetText(1,1)+"'，该险种的["+edorName.GetText(1,1)+"]保全功能暂未上线，无法添加保全项目！";
			System.out.println(Content);
			return false;
		}
	    

	    sql = "select state from lcgrpbalplan where grpcontno = '"+grpcontno+"'";
	    SSRS arrResult = tExeSQL.execSQL(sql);    
	    if (arrResult.MaxRow > 0 && !"0".equals(arrResult.GetText(1,1))) 
	    {
	    	Content = "该集体合同目前正在进行定期结算!不能进行保全操作!";
	    	System.out.println(Content);
	        return false;  
	    }
	    
	    sql = "select edorno from lpgrpedoritem a where grpcontno = '"+grpcontno+"' and edortype='TZ' and " +
	    		" exists ( select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0' )";
	    arrResult = tExeSQL.execSQL(sql);    
	    if (arrResult.MaxRow > 0) 
	    {
	    	Content = "团体万能增人不能和其他保全项目一起操作!";
	    	System.out.println(Content);
	        return false;  
	    }
	    
	    String IsRSsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and ((enddate is null) or (startdate<=current date  and enddate>current date )) and grpcontno = '"+grpcontno+"'";
	    SSRS IsRSarrResult = tExeSQL.execSQL(IsRSsql);    
	    if (IsRSarrResult.MaxRow>0) 
	    {
	    	Content = "该集体合同目前正处于保单暂停状态!不能添加保单恢复外的任何保全操作!";
	    	System.out.println(Content);
	        return false;  
	    }
       
//		校验保全生效日是否在暂停期
	    String IsRSRRsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and enddate is not null and startdate<='"+mFormData.getEdorValiDate()+"'  and enddate>'"+mFormData.getEdorValiDate()+"'  and grpcontno = '"+grpcontno+"'";
	    SSRS IsRSRRResult = tExeSQL.execSQL(IsRSRRsql);    
	    if (IsRSarrResult.MaxRow>0) 
	    {
	    	Content = "生效日期不可选在该集体合同暂停区间内!"; 	  
	    	System.out.println(Content);
	        return false;  
	    }


	    sql = "select 1 from lpgrpedoritem a where grpcontno='"+grpcontno+"' and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and edortype='TY' "; 
	    arrResult = tExeSQL.execSQL(sql);
	    if (arrResult.MaxRow > 0)
	    {
	    	Content = "该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!";
	    	System.out.println(Content);
	        return false;
	    } 
	    // 特需医疗险种的团单增减人不可同一个工单来做
	    String sqlss = "select 1 from lcgrppol a where  grpcontno ='"+grpcontno+"' and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype3='7')";
	    arrResult = tExeSQL.execSQL(sqlss);
	    sqlss = "select EdorAcceptNo, EdorNo, EdorType, GrpContNo, "
            + "(select CustomerNo from LCGrpAppnt "
            + " where GrpContNo = LPGrpEdorItem.GrpContNo), "
            + " EdorValiDate, "
            + "	case EdorState "
            + "		when '1' then '录入完毕' "
            + "		when '2' then '理算确认' "
            + "		when '3' then '未录入' "
            + "		when '4' then '试算成功' "
            + "		when '0' then '保全确认' "
            + "	end "
            + "from LPGrpEdorItem "
            + "where EdorAcceptNo='" + EdorAcceptNo + "' "
            + "	and EdorNo='" + EdorAcceptNo + "' ";
	    SSRS GrpEdorItem = tExeSQL.execSQL(sqlss);
	    if (arrResult.MaxRow > 0 && GrpEdorItem.MaxRow > 0)
	    {
	    int Linecount = GrpEdorItem.MaxRow;
	    for(int j=1;j<=Linecount;j++)
	        {        
	          if("NI".equals(GrpEdorItem.GetText(j, 3))&&("ZT".equals(dMessHead.getMsgType())))
	          {
	        	  Content = "特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行减人操作";
	        	  System.out.println(Content);
	        	  return false;
	          }
	          if("ZT".equals(GrpEdorItem.GetText(j, 3))&&("NI".equals(dMessHead.getMsgType())))
	          {
	        	  Content = "特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行增人操作";
	        	  System.out.println(Content);
	        	  return false;
	          }
	        }        
	    }
		return true;
	}
	
	/**
	 * 添加保全项目按钮
	 * 后台
	 * @return
	 */
	private boolean grpEdorItemSave(){
		
		  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
		  LPGrpEdorItemSet mLPGrpEdorItemSet =new LPGrpEdorItemSet();
		 
		  TransferData tTransferData = new TransferData(); 
		  GrpEdorItemUI tGrpEdorItemUI   = new GrpEdorItemUI();
		  //输出参数
		  String FlagStr = "";
		  String Content = "";
		 
		  GlobalInput tGI = new GlobalInput(); //repair:
		  tGI=GI;  //参见loginSubmit.jsp
		  
		  CErrors tError = null;
		  System.out.println("aaaa");
		  //后面要执行的动作：添加，修改，删除
		  String fmAction="INSERT||GRPEDORITEM";
		  System.out.println("fmAction:"+fmAction); 

		  if(fmAction.equals("INSERT||GRPEDORITEM"))
		    {
		        tLPGrpEdorMainSchema.setEdorAcceptNo(EdorAcceptNo); 
		        tLPGrpEdorMainSchema.setEdorNo(EdorAcceptNo);
		        tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		        tLPGrpEdorMainSchema.setEdorAppDate(PubFun.getCurrentDate()); //保全申请日期
		        tLPGrpEdorMainSchema.setEdorValiDate(mFormData.getEdorValiDate()); //生效日期
		     
		        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		        tLPGrpEdorItemSchema.setEdorAcceptNo(EdorAcceptNo);
		        tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
		        tLPGrpEdorItemSchema.setEdorAppNo(EdorAcceptNo); 
		        tLPGrpEdorItemSchema.setEdorType(dMessHead.getMsgType()); 
		        tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo()); 
		        tLPGrpEdorItemSchema.setEdorAppDate(PubFun.getCurrentDate()); 
		        tLPGrpEdorItemSchema.setEdorValiDate(mFormData.getEdorValiDate()); 
		        tLPGrpEdorItemSchema.setManageCom(GI.ManageCom);
		        mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);
		    }

		    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++"+fmAction);

		    try
		    {
		        // 准备传输数据 VData
		         VData tVData = new VData();
		         
		         tVData.add(mLPGrpEdorItemSet);
		         tVData.add(tLPGrpEdorMainSchema);
		         tVData.add(tTransferData);
		         tVData.add(tGI);
		         
		          
		         //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		        if ( tGrpEdorItemUI.submitData(tVData,fmAction))
		        {
		            if (fmAction.equals("INSERT||GRPEDORITEM"))
			        {
			    	    System.out.println("11111------return");
			            	
			    	    tVData.clear();
			    	    tVData = tGrpEdorItemUI.getResult();
			    	    
			    	    LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet(); 
			            tLPGrpEdorItemSet = (LPGrpEdorItemSet)tVData.getObjectByObjectName("LPGrpEdorItemSet", 0);

			        }
			    }
			    else
			    {
			        Content = "保存失败，原因是:" + tGrpEdorItemUI.mErrors.getFirstError();
			        System.out.println(Content);
		    	    FlagStr = "Fail";
		    	    return false;
			    }
		    }
		    
		    catch(Exception ex)
		    {
		      Content = "保存失败，原因是:" + ex.toString();
		      System.out.println(Content);
		      FlagStr = "Fail";
		    }

		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		  if (FlagStr.equals(""))
		  {
		    tError = tGrpEdorItemUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		      FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getFirstError();
		    	System.out.println(Content);
		    	FlagStr = "Fail";
		    	return false;
		    }
		   }
		
		return true;
	}
	/**
	 * 导入减人清单
	 * @return
	 */
	private boolean importFile(){
		ExeSQL tExeSQL = new ExeSQL();
		String path = "";      //文件上传路径
		String fileName = "";  //上传文件名
		
		//得到全局变量
//		GlobalInput tGI = (GlobalInput) session.getValue("GI");
		
	    //得到excel文件的保存路径
	    String sql = "select SysvarValue "
	                + "from ldsysvar "
	                + "where sysvar='XmlPath'";
	    String subPath = tExeSQL.getOneValue(sql);
	    String rootPath=getClass().getResource("/").getFile().toString(); 
		System.out.println(rootPath);
		path = rootPath.replaceAll("WEB-INF/classes/", ""); 
	 	path = path + subPath;
	 	System.out.println("-----------------"+path);
	 
	 	sql = "select description,code from ldconfig where codetype = 'ClaimInterface'";
		SSRS sql1SSRS = tExeSQL.execSQL(sql);
		String ip = "";
		String acc = "";
		String pwd = "";
		String port = "";
		for (int i = 1; i <= sql1SSRS.MaxRow; i++) {
			if("IP".equals(sql1SSRS.GetText(i, 1))){
				ip = sql1SSRS.GetText(i,2);
			}else if("AccName".equals(sql1SSRS.GetText(i, 1))){
				acc = sql1SSRS.GetText(i,2);
			}else if("PassWord".equals(sql1SSRS.GetText(i, 1))){
				pwd = sql1SSRS.GetText(i,2);
			}else if("Port".equals(sql1SSRS.GetText(i, 1))){
				port = sql1SSRS.GetText(i,2);
			}
		}
		System.out.println("ftp地址： " + ip + "账号： " + acc +"密码： " + pwd +"端口： " + port);
		FTPTool tFTPTool = new FTPTool(ip, acc, pwd, Integer.parseInt(port));
		
		try {
			String [] s = mZTExcelObj.getExcel().split("/");
			System.out.println("excel地址："+mZTExcelObj.getExcel());
			System.out.println("文件名： " + s[s.length-1]);
			fileName = s[s.length-1];
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
				Content = "ftp下载失败，原因是:" + tFTPTool.getErrContent(1);
                System.out.println("Content=========="+Content);
                return false;
			} else {
				String ftpPath = "";
				for(int i = 0;i<s.length-1; i++){
					ftpPath += s[i] + "/";
				}
				System.out.println("服务器路径ftp======= " + ftpPath);
				if(!tFTPTool.downloadFile(ftpPath, path,
						s[s.length-1]	)){
                    Content = "ftp下载失败，原因是:" + tFTPTool.getErrContent(1);
                    System.out.println("Content=========="+Content);
					return false;
                }
			}
		
		}catch(Exception ex){
			ex.printStackTrace();
			Content = "ftp下载失败，原因是:" + ex.toString();
			System.out.println(Content);
			return false;
		}

		
		System.out.println("______________fileName:"+fileName);	
		
		
		
		
		TransferData t = new TransferData();
		t.setNameAndValue("edorNo", EdorAcceptNo);
		t.setNameAndValue("grpContNo", mLCGrpContInfo.getGrpContNo());
		t.setNameAndValue("edorType", dMessHead.getMsgType());
		t.setNameAndValue("path", path);
		t.setNameAndValue("fileName", fileName);
		
		VData v = new VData();
		v.add(t);
		v.add(GI);

		//从磁盘导入被保人清单
		DiskImportZTUI tDiskImportZTUI = new DiskImportZTUI();
		if (!tDiskImportZTUI.submitData(v, "INSERT||EDOR"))
		{
//	        flag = "Fail";
	        Content = tDiskImportZTUI.mErrors.getErrContent();
	        System.out.println(tDiskImportZTUI.mErrors.getFirstError());
	        return false;
		}
		else
		{
//	        flag = "Succ";
	        if(tDiskImportZTUI.getImportPersons() != 0)
	        {
	            int unMainInsuredCount = tDiskImportZTUI.getImportPersons() 
	                  - tDiskImportZTUI.getImportPersonsForZT();
	            int notImportPersons =tDiskImportZTUI.getNotImportPersons();
	            Content = "成功导入" + tDiskImportZTUI.getImportPersons() + "人"
	              +(notImportPersons > 0 ? "，其中有" +  notImportPersons+ "人是重复输入的。<br>信息如下：<br>"
	              + tDiskImportZTUI.getDiskImportName()+" " : " ")
	              + (unMainInsuredCount > 0 ? "，其中有" +  unMainInsuredCount+ "人是连带被保人。" 
	                : "");
	        }
	        else
	        {
	            Content = "清单中没有本保单的客户";
	            System.out.println(Content);
	            return false;
	        }
	        
	        //执行成功，但是部分人员不是本保单客户
	        if(tDiskImportZTUI.mErrors.needDealError())
	        {
	            Content += tDiskImportZTUI.mErrors.getErrContent();
	        }
		}
		System.out.println(Content);
		Content = PubFun.changForHTML(Content);
		return true;
	}
	
	/**
	 * 保全项目明细页面  保存申请按钮
	 * @return
	 */
	private boolean ZTsave(){
		ExeSQL tExeSQL = new ExeSQL();
		 //若是减人，则需要存储算法类别
	    if("ZT".equals(dMessHead.getMsgType()))
	    {
	    	String sql = "  select edorAcceptNo "
	              + "from LPEdorItem "
	              + "where edorAcceptNo = '" + EdorAcceptNo + "' "
	              + "    and edorType = '" + dMessHead.getMsgType() + "' ";
	    	SSRS result = tExeSQL.execSQL(sql);
	    	if(result == null)
	    	{
	    		Content = "您还没有录入需退保的被保人";
	    		System.out.println(Content);
	    		return false;
	    	}   
	    	LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	        tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
	        tLPGrpEdorItemSchema.setEdorType(dMessHead.getMsgType());
	        tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
	        
	        String feeRate = mZTExcelObj.getFeeRate();
	        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLPGrpEdorItemSchema);
	        String calTime = mZTExcelObj.getCalTime();
	        tEdorItemSpecialData.add("CalTime", calTime); 
	        tEdorItemSpecialData.add("FeeRate", String.valueOf(Double.parseDouble(feeRate) / 100));
	        //request.getParameter("BalaPayWay") 找不到这个标签
	        tEdorItemSpecialData.add(BQ.DETAILTYPE_BALAPAYTYPE, "");
	        
	    		VData data = new VData();
	    		data.add(GI);
	    		data.add(tLPGrpEdorItemSchema);
	    		data.add(tEdorItemSpecialData);
	    		
	    		GrpEdorZTDetailUI tGrpEdorZTDetailUI = new GrpEdorZTDetailUI();
	    		if (!tGrpEdorZTDetailUI.submitData(data))
	    		{
//	    			flag = "Fail";
	    			Content = "数据保存失败！原因是:" + tGrpEdorZTDetailUI.getError();
	    			System.out.println(Content);
	    			return false;
	    		}
	    		else
	    		{
//	    			flag = "Succ";
	    			Content = "数据保存成功。";
	    			System.out.println(Content);
	    		}
	    		String message = tGrpEdorZTDetailUI.getMessage();
	    		if ((message != null) && (!message.equals("")))
	    		{
	    			Content += message;
	    		}
	        Content = PubFun.changForHTML(Content);
	    }
	    
		return true;
	}
	
	
	/**
	 * 保全理算
	 * @return
	 */
	private boolean edorAppConfirm(){
		//前台校验
		ExeSQL tExeSQL = new ExeSQL();
		String strSQL=" select * from lpgrpedoritem where edoracceptno='"+EdorAcceptNo+"'";
		SSRS arrResult = tExeSQL.execSQL(strSQL);
		if(arrResult.MaxRow <= 0)
		{
			Content = "未添加保全项目，不能进行保全理算！";	
			System.out.println(Content);
			return false;
		}

		strSQL=" select 'x' from lpgrpedoritem where edoracceptno='"+EdorAcceptNo+"' and EdorState='3'";
		arrResult = tExeSQL.execSQL(strSQL);
		if(arrResult.MaxRow > 0)
		{
			Content = "本次申请，有保全项目处于未录入状态，不能进行保全理算！";	
			System.out.println(Content);
			return false;
		}
		if ("".equals(EdorAcceptNo))
		{
			Content = "请重新申请!";
			System.out.println(Content);
			return false;
		}
	
//	    fm.all('fmAction').value = "INSERT||GEDORAPPCONFIRM";	       
//	    fm.action='GEdorAppConfirmSubmit.jsp';
		//后台数据提交
	    
	    //集体批改信息
	    LPGrpEdorMainSchema tLPGrpEdorMainSchema  = new LPGrpEdorMainSchema();
	    tLPGrpEdorMainSchema.setEdorAcceptNo(EdorAcceptNo);
	    tLPGrpEdorMainSchema.setEdorNo(EdorAcceptNo);
	    tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
	    tLPGrpEdorMainSchema.setEdorValiDate(mFormData.getEdorValiDate());
	    tLPGrpEdorMainSchema.setEdorAppDate(PubFun.getCurrentDate());
	             
	    VData data = new VData();  
	    data.add(GI);   
	    data.add(tLPGrpEdorMainSchema);
	    PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
	    if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM"))
	    {
//	      flag = "Fail";
	      Content = "保全理算失败！原因是：" + tPGrpEdorAppConfirmUI.getError().getErrContent();
	      System.out.println(Content);
	      return false;
	    }
	    else
	    {
//			flag = "Succ";
	    	Content = "保全理算成功。";
	    	System.out.println(Content);
	    }
	    Content = PubFun.changForHTML(Content);
		return true;
	}
	
	/**
	 * 保全确认
	 * 前台
	 * @return
	 */
	private boolean edorConfirm(){
		//前台校验
		ExeSQL tExeSQL = new ExeSQL();
		String strSql = "select statusno from lgwork where workno = '" + EdorAcceptNo + "' ";
		SSRS arrResult = tExeSQL.execSQL(strSql);
		if("5".equals(arrResult.GetText(1,1)))
		{
		    Content = "保全已经确认，请不要重复确认！";
		    System.out.println(Content);
		    return false;
		}
		if("88".equals(arrResult.GetText(1,1)))
		{
			Content = "保全审批意见为[不同意]，请重新录入保全明细或撤销保全工单！";
			System.out.println(Content);
		    return false;
		}

		String findSql="select serialno from LCUrgeVerifyLog where serialno='"+EdorAcceptNo+"' ";
		SSRS arrResult_1 = tExeSQL.execSQL(findSql);
		if( arrResult_1.MaxRow > 0 )
		{
			Content = "正在保全确认或者重复理算操作，请不要再次点击保全确认！";
			System.out.println(Content);
			return false;
		}
		String strSql2 = "select GetMoney from LPGrpEdorMain where EdorAcceptNo = '"
				+ EdorAcceptNo + "'";
		SSRS strSqlSSRS = tExeSQL.execSQL(strSql2);
		if (strSqlSSRS.getMaxRow() < 0) {
			Content = "查询保全主表信息出错！";
			System.out.println(Content);
			return false;
		}
		String getMoney = strSqlSSRS.GetText(1, 1);
		String balanceMethodValue = "1";
		String payMode = mFormData.getPayMode();//交付费方式报文中传值
		if (!"".equals(getMoney) && "1".equals(balanceMethodValue)) {
			if ("".equals(payMode) || null == payMode) {
				Content = "交费/付费方式不能为空。";
				System.out.println(Content);
				return false;
			}
			if (Double.parseDouble(getMoney) > 0) {
				if ("1".equals(payMode) || "2".equals(payMode)) {
					String endDate = mFormData.getEndDate();//截止日期报文传值
					if ("".equals(endDate)) {
						Content = "截止日期不能为空";
						System.out.println(Content);
						return false;
					}
				}
			}
		}

		if ("4".equals(payMode)) {
			String bank = mFormData.getBank();//转账银行报文传值
			String bankAccno = mFormData.getBankAccNo();//转账账号报文传值
			if ("".equals(bank)) {
				Content = "转帐银行为空不能选择银行转帐！";
				System.out.println(Content);
				return false;
			}
			if ("".equals(bankAccno)) {
				Content = "转帐帐号为空不能选择银行转帐！";
				System.out.println(Content);
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * 保全确认
	 * 后台
	 * @return
	 */
	public boolean GEdorConfirmSubmit(){
		ExeSQL tExeSQL = new ExeSQL();
		String flag = "";
	    String edorAcceptNo = EdorAcceptNo;
		String payMode = mFormData.getPayMode();
		String balanceMethodValue = "1";
		String sql = "select OtherNo from LPEdorApp where EdorAcceptNo = '" + edorAcceptNo + "' ";
		SSRS result = tExeSQL.execSQL(sql);
		String customerNo = result.GetText(1,1);
		sql ="select getmoney from LPEdorApp where EdorAcceptNo='"+edorAcceptNo+"'";
		result = tExeSQL.execSQL(sql);
		if (result.MaxRow < 0) 
		  {
				Content = "查询保全申请主表时出错，保全受理号为"+edorAcceptNo+"！";
				System.out.println(Content);
				return false;
			}
		String accType = "";
		String destSource = "";
		String fmtransact = "";
		String fmtransact2 = "NOTUSEACC";
		if(Double.parseDouble(result.GetText(1,1)) > 0){
			accType = "0";
			destSource = "01";
			fmtransact = "1";//交费
//			fmtransact2 = "INSERT||TakeOut";
		}else{
			accType = "1";
			destSource = "11";
			fmtransact = "0";//退费
//			fmtransact2 = "INSERT||ShiftTo";
		}
		String contType= "";
	    GlobalInput gi = GI;
	    
	    String failType = null;
			String autoUWFail = null;
			
			System.out.println("fmtransact2:" + fmtransact2);
			
			//若使用账户冲抵保全收退费，则默认为即时结算
			if(!"NOTUSEACC".equals(fmtransact2)){
				balanceMethodValue="1";
			}
			//qulq 2007-4-5 
			String 	checkSQL = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType in ('LQ','ZB')";
	  	SSRS rs =new ExeSQL().execSQL(checkSQL);
	  	if(rs!= null && rs.getMaxRow() > 0 )
	  	{
		  	if(balanceMethodValue.equals("0"))
	  		{
	  			flag = "Fail";
				Content = "部分领取、追加保费项目不得进行定期结算，请单独操作并进行即时结算";
				System.out.println(Content);
	  		  	return false;
	  		}
	  	}
	  
	  	//xiep 2008-12-1 增加做NI项目时会增人重复的校验
			String 	checkNI = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'NI' ";
	  	SSRS mssrs =new ExeSQL().execSQL(checkNI);
	  	if(mssrs!= null && mssrs.getMaxRow() > 0 )
	  	{
	  		//增加理算后对于lcinsured是否缺失的校验 add by xp 100604
	  		String 	checkInsured = " select 1 from lccont where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"+
	  								edorAcceptNo+"' fetch first 1 row only) and contno not in "+
	  								"(select distinct contno from lcinsured where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"+
	  								edorAcceptNo+"' fetch first 1 row only) ) and contno in (select contno from lcinsuredlist where edorno='"+edorAcceptNo+"') ";
	  	  SSRS mssrs1 =new ExeSQL().execSQL(checkInsured); 
	  	  if(mssrs1!= null && mssrs1.getMaxRow() > 0 )  
	 	   	  {
	  			 flag = "Fail";
				Content = "保全项目增人时出现lcinsured丢失的问题,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
	  		  	System.out.println(Content);
	  		  	return false;
	 	   	  }     					
	       //------------------insured校验结束-------------------  
	          					
	  			String sql1 = "select getmoney from lpgrpedoritem "+
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'NI' ";
	            SSRS tSSRS1 =new ExeSQL().execSQL(sql1);
	            String itemmoney = tSSRS1.GetText(1,1);
	            String sql2 = "select sum(getmoney) from ljsgetendorse "+
	            					" where endorsementno = '" + edorAcceptNo + "' " +
	            					" and feeoperationtype='NI' and feefinatype='BF' with ur";
	            SSRS tSSRS2 =new ExeSQL().execSQL(sql2);
	            String endorsemoney = tSSRS2.GetText(1,1);
	  	
	  		if(!itemmoney.equals(endorsemoney))
		  	{
	  			flag = "Fail";
				Content = "保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
				System.out.println(Content);
	  		  	return false;
	  		}
	        //-----------------------关于增人总数和实际的理算的人数的比较-----------------------
	           String sql_check_ljagetendorse = 
	                      "  select count(distinct a.contno),count(distinct b.contno) "+
	                           "  from ljsgetendorse a, lcinsuredlist b "+
	                           "  where a.grpcontno = b.grpcontno "+
	                           "  and a.endorsementno = '"+edorAcceptNo+"'"+
	                           "  and a.feeoperationtype = 'NI' "+
	                           "  and a.endorsementno=b.edorno "+
	                           "  and b.state='1' "+
	                           "  with ur";
	                  //---------------------------------------------------------
	                    SSRS t_check_ljagendorse_SSRS1 =new ExeSQL().execSQL(sql_check_ljagetendorse);
	                 //---------------------------------------------------------
	                    System.out.println("sql_check_ljagetendorse:"+sql_check_ljagetendorse);
	                    int the_number_of_1 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1,1));
	                    int the_number_of_2 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1,2));
	                    System.out.println("the_number_of_1:"+the_number_of_1);
	                    System.out.println("the_number_of_2:"+the_number_of_2);
	                 if(the_number_of_1!=the_number_of_2)
	                 {
	                     flag = "Fail";
	                     Content = "实际理算完成的人数和模板录入的人数不一致，请点击[重复理算]回退到保全明细录入页面，并重新录入[NI]项目明细";
	                     System.out.println(Content);
	     	  		  	 return false;
	                 }
	  			
	  	}
	  	
	  	//xiep 2010-4-21 增加做ZT项目时会错误的校验
			String 	checkZT = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'ZT' ";
	  	SSRS mssrs1 =new ExeSQL().execSQL(checkZT);
	  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
	  	{
	  			String sql3 = "select sum(getmoney) from lpedoritem "+
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'ZT' ";
	            SSRS tSSRS3 =new ExeSQL().execSQL(sql3);
	            String itemmoneyZT = tSSRS3.GetText(1,1);
	            String sql4 = "select case when sum(getmoney) is null then 0 else sum(getmoney) end from ljsgetendorse "+
	            					" where endorsementno = '" + edorAcceptNo + "' " +
	            					" and feeoperationtype='ZT'  with ur";
	            SSRS tSSRS4 =new ExeSQL().execSQL(sql4);
	            String endorsemoneyZT = tSSRS4.GetText(1,1);
	  	
	  		if(!itemmoneyZT.equals(endorsemoneyZT))
		  	{
	  			flag = "Fail";
				Content = "保全项目减人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[ZT]项目明细重试";
				System.out.println(Content);
	  		  	return false;
	  		}
	  	}
	  	
	  	//xiep 2009-6-24 增加做WS项目时会实名化重复的校验
			String 	checkWS = "select 1 from LPGrpEdorItem " +
	            					"where EdorNo = '" + edorAcceptNo + "' " +
	            					"and EdorType = 'WS' ";
	  	SSRS mrs =new ExeSQL().execSQL(checkWS);
	  	if(mrs!= null && mrs.getMaxRow() > 0 )
	  	{
	  			String wssql = "select 1 from lpcont where edorno='" + edorAcceptNo + "' group by insuredno having count(contno)>1";
	            SSRS wsSSRS =new ExeSQL().execSQL(wssql);
	        if(wsSSRS!= null && wsSSRS.getMaxRow() > 0 )
	  		{
	  			flag = "Fail";
				Content = "无名单实名化时增人出现重复数据,请点击[重复理算]回退到保全明细后重新添加[WS]项目明细";
				System.out.println(Content);
	  		  	return false;
			}
	  	}

		if(!flag.equals("Fail"))
			{
			EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorAcceptNo, "YE");
			tSpecialData.add("CustomerNo", customerNo);
			tSpecialData.add("AccType", accType);
			tSpecialData.add("OtherType", "3");
			tSpecialData.add("OtherNo", edorAcceptNo);
			tSpecialData.add("DestSource", destSource);
			tSpecialData.add("ContType", contType);
		  tSpecialData.add("Fmtransact2", fmtransact2);
	    
	   	LCAppAccTraceSchema tLCAppAccTraceSchema=new LCAppAccTraceSchema();
		  tLCAppAccTraceSchema.setCustomerNo(customerNo);
		  tLCAppAccTraceSchema.setAccType(accType);
		  tLCAppAccTraceSchema.setOtherType("3");//团单
		  tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
		  tLCAppAccTraceSchema.setDestSource(destSource);
		  tLCAppAccTraceSchema.setOperator(gi.Operator);
		  
	    VData tVData = new VData();
	    tVData.add(tLCAppAccTraceSchema);
	    tVData.add(tSpecialData);
			tVData.add(gi);
			PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
			if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param"))
			{
				flag = "Fail";
				Content = "处理帐户余额失败！";
				System.out.println(Content);
	  		  	return false;
			}
	  	else
	  	{
		    BqConfirmUI tBqConfirmUI = new BqConfirmUI(gi, edorAcceptNo, BQ.CONTTYPE_G,balanceMethodValue);
		    if (!tBqConfirmUI.submitData())
		    {
		      flag = "Fail";
		      Content = tBqConfirmUI.getError();
		      System.out.println(Content);
	  		  return false;
		    }
		    else
		    {
		    	System.out.println("交退费通知书" + edorAcceptNo);
		    	String strSql = "select a.BankCode, a.BankAccno, a.AccName " +
							 "from LCGrpCont a, LPGrpEdorMain b " +
							 "where a.GrpContNo = b.GrpContNo " +
							 "and   b.EdorAcceptNo = '" + edorAcceptNo + "' and a.BankAccno ='"+mFormData.getBankAccNo()+"'";
		    	SSRS rss = new ExeSQL().execSQL(strSql);
		    	String accName = "";
		    	if(rss.MaxRow > 0){
		    		accName = rss.GetText(1, 3);
		    	}
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("payMode", payMode);	
					tTransferData.setNameAndValue("endDate", mFormData.getPayDate());
					tTransferData.setNameAndValue("payDate", mFormData.getPayDate());
					tTransferData.setNameAndValue("bank", mFormData.getBank());
					tTransferData.setNameAndValue("bankAccno", mFormData.getBankAccNo());
					tTransferData.setNameAndValue("accName", accName);
		  		
		  		//生成交退费通知书
					FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(edorAcceptNo);
					if (!tFeeNoticeGrpVtsUI.submitData(tTransferData))
					{
						flag = "Fail";
						Content = "生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError();
						System.out.println(Content);
			  		  	return false;
					} 		
					
					VData data = new VData();
					data.add(gi);
					data.add(tTransferData);
					SetPayInfo spi = new SetPayInfo(edorAcceptNo);
					if (!spi.submitDate(data, fmtransact))
					{
						System.out.println("设置转帐信息失败！");
						flag = "Fail";
						Content = "设置收退费方式失败！原因是：" + spi.mErrors.getFirstError();
						System.out.println(Content);
			  		  	return false;
					}
		  		flag = "Succ";
					Content = "保全确认成功！";
					System.out.println(Content);
					String message = tBqConfirmUI.getMessage();
					if ((message != null) && (!message.equals("")))
					{
					  Content += "\n" + tBqConfirmUI.getMessage();
					}
		    }
			  Content = PubFun.changForHTML(Content);
		    failType = tBqConfirmUI.getFailType();  //是否审批通过
		    autoUWFail = tBqConfirmUI.autoUWFail();  //是否自核通过
		    autoUWFail = autoUWFail == null ? "" : autoUWFail;
		  }
		  }
		  System.out.println("end");
		  return true;
	}

	public String getContent() {
		return Content;
	}

	public String getEdorAcceptNo() {
		return EdorAcceptNo;
	}
	
	
}
