package com.preservation.ZT;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.preservation.ZT.obj.ZTExcelObj;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class InterfaceZT {
	
	String errorValue; // 错误信息
	VData data = new VData();
	String gongdanhao;
	String cValiDate;
	
	public boolean deal(String xml) {
		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}
		// 处理逻辑
		DealZT dealZT = new DealZT();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		if (!dealZT.deal(data)) {
			errorValue = dealZT.getContent();
			gongdanhao = dealZT.getEdorAcceptNo();
			String sql = "insert into ErrorInsertLog " +
			"(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ," +
			"Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ," +
			"transactionBatch ,transactionDescription ,transactionAddress ) " +
			"values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
			+GI.Operator+"','"
			+xml+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), " +
			"to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), " +
			"'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
			+returnXML(xml)+"','中间状态','"+errorValue+"','"+mMessHead.getMsgType()+"','"
			+mMessHead.getBranchCode()+"','"+mMessHead.getBatchNo()+"','交易描述','交易地址')";
			tExeSQL.execUpdateSQL(sql);
			return false;
		}
		gongdanhao = dealZT.getEdorAcceptNo();
		errorValue = "";
		return true;
	}
	
	/**
	 * 得到报文数据 解析报文
	 * 
	 * @throws Exception
	 */
	public boolean getkData(String xml) {
		try {
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");

			MessHead head = new MessHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			// 作为页面session值
			GlobalInput GI = new GlobalInput();
			GI.Operator = SendOperator;
			GI.ComCode = BranchCode;
			GI.ManageCom = BranchCode;
			data.add(GI);
			// 无名单的增人
			Element BJ = rootElement.getChild("BJ");
			// 客户号
			
			Element msgCustomer = BJ.getChild("CustomerInfo");
			Element eCustomerInfo = msgCustomer.getChild("Item");
			String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

			CustomerInfo iCustomerInfo = new CustomerInfo();
			iCustomerInfo.setCustomerNo(CustomerNo);
			data.add(iCustomerInfo);

			// 团单信息
			Element lCGrpContInfo = BJ.getChild("LCGrpContInfo");
			Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
			String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
			String GrpName = eLCGrpContInfo.getChildText("GrpName");

			LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
			iLCGrpContInfo.setGrpContNo(GrpContNo);
			iLCGrpContInfo.setGrpName(GrpName);
			data.add(iLCGrpContInfo);

			// 
			Element xFormData = BJ.getChild("FormData");
			Element xxFormData = xFormData.getChild("Item");
			FormData tFormData = new FormData();

			tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
			cValiDate = xxFormData.getChildText("EdorValiDate");
			tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
			tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
			tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
			tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
			tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
			tFormData.setRemark(xxFormData.getChildText("Remark"));
			tFormData.setPayMode(xxFormData.getChildText("PayMode"));
			tFormData.setPayDate(xxFormData.getChildText("PayDate"));
			tFormData.setEndDate(xxFormData.getChildText("EndDate"));
			tFormData.setBank(xxFormData.getChildText("Bank"));
			tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
			
			ZTExcelObj tZTExcelObj = new ZTExcelObj();
			tZTExcelObj.setCalTime(xxFormData.getChildText("CalTime"));
			tZTExcelObj.setFeeRate(xxFormData.getChildText("FeeRate"));
			tZTExcelObj.setExcel(xxFormData.getChildText("Excel"));
			
			data.add(tFormData);
			data.add(tZTExcelObj);

		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public String returnXML(String xml){
		CreateXMLZT createXMLZT= new CreateXMLZT();
		String returnxml = null;
		try {
			returnxml = createXMLZT.createXML(data, errorValue, gongdanhao, cValiDate, xml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnxml;
		
	}
	
	public static void main(String[] args) {
		InterfaceZT tInterfaceZT = new InterfaceZT();
		
		try {
			InputStream pIns = new FileInputStream("C:/Users/Administrator/Desktop/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
			tInterfaceZT.deal(mInXmlStr);
			System.out.println(tInterfaceZT.returnXML(mInXmlStr));
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
}
