package com.preservation.edorCancel;

import com.sinosoft.lis.bq.PGrpEdorCancelUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 团体保全申请撤销
 * @author Administrator
 *
 */
public class GEdorCancel {
	private String Content;
	private String edorno;
	GlobalInput tGI = new GlobalInput(); 
	
	public static void main(String[] args) {
		GEdorCancel tGEdorCancel = new GEdorCancel();
		VData tt = new VData();
		String edorNo = "20180723000057";
		GlobalInput tg = new GlobalInput(); 
		tg.Operator = "pa0001";
		tg.ManageCom = "86";
		tt.add(edorNo);
		tt.add(tg);
		tGEdorCancel.cancelAppEdor(tt);
	}
	/**
	 * 保全申请撤销
	 * @return
	 */
	public boolean cancelAppEdor(VData tVData){
		tGI = (GlobalInput) tVData.getObjectByObjectName("GlobalInput", 0);
		edorno = (String) tVData.getObjectByObjectName("String", 0);
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select 1 from lpgrpedormain where edorno = '"+edorno+"'";
		SSRS ss = tExeSQL.execSQL(sql);
		if(ss.MaxRow > 0){
			if(!cancelEdor()){
				System.out.println("保全项目撤销失败：" + Content);
				return false;
			}
		}
		if(!cancelEdorMain()){
			System.out.println("保全申请撤销失败：" + Content);
			return false;
		}
		return true;
	}
	
	
	
	/**
	 * 保全项目撤销
	 * @return
	 */
	public boolean cancelEdor(){
		//前台校验
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select 1 from LPEdorApp where UWState in ('5') and EdorAcceptNo = '" + edorno + "' with ur";
		SSRS result = tExeSQL.execSQL(sql);
		if(result.MaxRow > 0)
		  {
			Content = "保全申请 " + edorno + " 现在是核保状态，不能撤销！";
		    System.out.println(Content);
		    return false;
		  }

		//后台数据提交
		if(!edorAppCancelSubmit("G&EDORITEM","1")){
			System.out.println(Content);
			return false;
		}
		return true;
	}
	
	/**
	 * 批单撤销
	 * @return
	 */
	public boolean cancelEdorMain(){
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select 1 from LPEdorApp where UWState in ('5') and EdorAcceptNo = '" + edorno + "' with ur";
		SSRS result = tExeSQL.execSQL(sql);
		if(result.MaxRow > 0)
		  {
			Content = "保全申请 " + edorno + " 现在是核保状态，不能撤销！";
		    System.out.println(Content);
		    return false;
		  }

		           		      
		//后台数据提交
		if(!edorAppCancelSubmit("G&EDORMAIN","2")){
			System.out.println(Content);
			return false;
		}

		return true;
	}
	
	/**
	 * 保全申请撤销后台
	 * @param transact
	 * @return
	 */
	public boolean edorAppCancelSubmit(String transact , String delFlag){
		ExeSQL tExeSQL = new ExeSQL();
		  LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		  LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		  PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
		  TransferData tTransferData = new  TransferData(); 

		  //输出参数
		  String FlagStr = "";
//		  tGI=(GlobalInput)session.getValue("GI");  

		String Operator  = tGI.Operator ;  //保存登陆管理员账号
		String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom内存中登陆区站代码

		CErrors tError = null;
//		String tBmCert = "";
		String delReason = "系统错误";
		String reasonCode = "201";
		if(delFlag.equals("1")){        //删除项目
			System.out.println("=============删除项目==============");	
			String sql = "select a.edorNo,a.grpcontno,a.edortype,a.edorstate,a.MakeDate,a.MakeTime " +
						"from lpgrpedorItem a where a.edorNo='"+edorno+"' and a.edorstate<>'0' " +
						"order by a.MakeDate desc,a.MakeTime desc fetch first 3000 rows only with ur ";
			SSRS ss = tExeSQL.execSQL(sql);
			tLPGrpEdorItemSchema.setEdorNo(edorno);
			tLPGrpEdorItemSchema.setGrpContNo(ss.GetText(1,2));
			tLPGrpEdorItemSchema.setEdorType(ss.GetText(1,3));
			tLPGrpEdorItemSchema.setEdorState(ss.GetText(1,4));
			tLPGrpEdorItemSchema.setMakeDate(ss.GetText(1,5));
		    tLPGrpEdorItemSchema.setMakeTime(ss.GetText(1,6));
		      
		    tTransferData.setNameAndValue("DelReason",delReason);
		    tTransferData.setNameAndValue("ReasonCode",reasonCode);
//		    System.out.println("页面中的数据"+delReason);   
		     try
		     {
		    // 准备传输数据 VData
		     VData tVData = new VData();
		     tVData.addElement(tGI);
		     tVData.addElement(tLPGrpEdorItemSchema);
		     tVData.addElement(tTransferData);
		     
		     
		     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		      tPGrpEdorCancelUI.submitData(tVData,transact);
		    }
		    catch(Exception ex)
		    {
		      Content = "保存失败，原因是:" + ex.toString();
		      FlagStr = "Fail";
		      System.out.println(Content);
		      return false;
		    }
		  if (FlagStr=="")
		  {
		    tError = tPGrpEdorCancelUI.mErrors;
		    if (!tError.needDealError())
		    {
		      Content ="撤销成功！";
		      System.out.println(Content);
		    	FlagStr = "Succ";
		    }
		    else
		    {
		    	Content = "撤销失败，原因是:" + tError.getFirstError();
		    	System.out.println(Content);
		    	FlagStr = "Fail";
		    	return false;
		    }
		   }
		    

		  }
		    if (delFlag.equals("2")){  //删除批单
		      System.out.println("=============删除批单==============");
		      String sql = "select a.edorNo,a.grpcontno,a.edorState,a.makedate,a.maketime " +
		      		"from lpgrpedorMain a, LGWork b, LDUser c,lpedorapp d " +
		      		"where  a.edorNo = b.workNo   and d.edoracceptno = a.edorno  and b.acceptorNo = c.userCode   " +
		      		"and d.edorstate<>'0'    and b.typeno not in ('0601','0602')   and b.statusno not in ('5','8')  " +
		      		"and a.EdorNo='"+edorno+"' order by a.MakeDate desc,a.MakeTime desc fetch first 3000 rows only with ur ";
		      SSRS ss = tExeSQL.execSQL(sql);
		     tLPGrpEdorMainSchema.setEdorNo(edorno);
		     tLPGrpEdorMainSchema.setEdorState(ss.GetText(1,3));     
		     tLPGrpEdorMainSchema.setGrpContNo(ss.GetText(1,2));
//		     delReason = request.getParameter("delMainReason");
//		     reasonCode = request.getParameter("CancelMainReasonCode");  
		     
		     tTransferData.setNameAndValue("DelReason",delReason);
		     tTransferData.setNameAndValue("ReasonCode",reasonCode);  
		      System.out.println("页面中的数据"+delReason);
		     try
		     {
		    // 准备传输数据 VData
		     VData tVData = new VData();
		     tVData.addElement(tGI);
		     tVData.addElement(tLPGrpEdorMainSchema);
		     tVData.addElement(tTransferData);//撤销申请原因
		     //tVData.addElement(reasonCode);   
		     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		      tPGrpEdorCancelUI.submitData(tVData,transact);
		    }
		    catch(Exception ex)
		    {
		      Content = "保存失败，原因是:" + ex.toString();
		      FlagStr = "Fail";
		    }
		   if (FlagStr=="")
		   {
			    tError = tPGrpEdorCancelUI.mErrors;
			    if (!tError.needDealError())
			    {
			      Content ="撤销成功！";
			    	FlagStr = "Succ";
			    }
			    else
			    {
			    	Content = "撤销失败，原因是:" + tError.getFirstError();
			    	FlagStr = "Fail";
			    	System.out.println(Content);
			    	return false;
			    }
			}
		  
		  }
		return true;
	}
	
	
	
}
