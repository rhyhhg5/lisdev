package com.preservation.LQ;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;


import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jdom.Element;

import com.preservation.BQBase.DealBase;
import com.preservation.BQBase.Exceldeal;

import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.GrpEdorLQDetailUI;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


/**
 * @author user
 *
 *部分领取
 */
public class DealLQ extends DealBase {



	/**
	 * 保全项目明细
	 * 保存按钮
	 * @return
	 */
	public boolean checkSave(){
		
		String edorNo=EdorAcceptNo;
		String edorType=dMessHead.getMsgType();
		String grpContNo=mLCGrpContInfo.getGrpContNo();

		//报文参数的非空校验
		if(!StringUtil.StringNull(edorNo)||!StringUtil.StringNull(edorType)||!StringUtil.StringNull(grpContNo)){
			Content="保全号，保全类型，团体合同号不能为空";
			return false;
		}
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo, edorType);
		List<Element> list= mFormData.getMoney();
		for(int i=0;i<list.size();i++){
			String groupMoney= list.get(i).getChildTextTrim("groupMoney");
			String groupFixMoney=list.get(i).getChildTextTrim("groupFixMoney");
			if("0".equals(groupMoney)&&"0".equals(groupFixMoney)){
				Content="团体和个人账户领取金不能为空";
				return false;
			}
			 tSpecialData.setGrpPolNo(list.get(i).getChildTextTrim("grpPolNo"));
			tSpecialData.add("groupMoney",groupMoney);
			tSpecialData.add("groupFixMoney", groupFixMoney);
		}
		GrpEdorLQDetailUI tGrpEdorLQDetailUI = 
	          new GrpEdorLQDetailUI(GI, edorNo, grpContNo, tSpecialData);
		if (!tGrpEdorLQDetailUI.submitData())
		{
		
			Content = "数据保存失败！原因是:" + tGrpEdorLQDetailUI.getError();
			return false;
		}
		else 
		{
			
			Content = "数据保存成功。";
			String message = tGrpEdorLQDetailUI.getMessage();
			if (message != null)
			{
			
			  Content = message;
			  return false;
			}
			return true;
		}
	}
	
	/**
	 * 
	 * 保全导入Excel时，
	 * 
	 * 解析Excel数据
	 * 
	 */
	public boolean getExcel(String  ExcelURL){
		
	/*	Element eBody = rootElement.getChild("MsgBody");
		Element eWS = eBody.getChild("WS");
		Element lCGrpContInfo = eWS.getChild("LCGrpContTable");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");
		String ExcelURL = eLCGrpContInfo.getChildText("Excel");*/
		String FileName = ExcelURL.split("/")[ExcelURL.split("/").length - 1];
		String FTPPath = ExcelURL.substring(0, ExcelURL.length() - FileName.length());
		// 人员信息
		// FTP下载人员文件.xls
		String sqlFTP = "select DESCRIPTION,CODE from ldconfig where codetype = 'WSInterface'";
		SSRS ssqlFTP = new ExeSQL().execSQL(sqlFTP);
		if (ssqlFTP.getMaxRow() != 4) {
			Content = "从下载LQ无名单实名化人员信息文件的FTP信息 配置错误。检查数据库配置";
			return false;
		}
		String Ip = null;
		String AccName = null;
		String PassWord = null;
		String Port = null;
		String rootPath = getClass().getResource("/").getFile().toString();
		String BasePath = rootPath.replaceAll("WEB-INF/classes/", "");
		BasePath += "temp_lp/";
		
		for (int i = 1; i <= ssqlFTP.getMaxRow(); i++) {
			if ("IP".equals(ssqlFTP.GetText(i, 1))) {
				Ip = ssqlFTP.GetText(i, 2);
			}
			if ("AccName".equals(ssqlFTP.GetText(i, 1))) {
				AccName = ssqlFTP.GetText(i, 2);
			}
			if ("PassWord".equals(ssqlFTP.GetText(i, 1))) {
				PassWord = ssqlFTP.GetText(i, 2);
			}
			if ("Port".equals(ssqlFTP.GetText(i, 1))) {
				Port = ssqlFTP.GetText(i, 2);
			}
		}

		FTPTool tFTPTool = new FTPTool(Ip, AccName, PassWord, Integer.valueOf(Port));
		try {
			if (!tFTPTool.loginFTP()) {
				Content = "登陆FTP失败：" + tFTPTool.getErrContent(1);
				return false;
			} else {
				tFTPTool.downloadFile(FTPPath, BasePath, FileName);
			}
		} catch (Exception e) {
			Content = "FTP下载失败：";
			e.printStackTrace();
			return false;
		}
		// 读取xls 解析得到数据
		LPDiskImportSet mLPDiskImportSet = new LPDiskImportSet();
		try {
			
			FileInputStream pkg = new FileInputStream(BasePath + FileName);
			HSSFWorkbook book = new HSSFWorkbook(pkg);
			HSSFSheet sheet = book.getSheetAt(0);
			int firstRowNum = sheet.getFirstRowNum();// 最小行数
			int lastRowNum = sheet.getLastRowNum();// 最大行数
			short lastCellNum = sheet.getRow(firstRowNum).getLastCellNum();// 最大列数
			if (lastCellNum != (short) 7) {
				Content = "xls数据不正确。请检查列数";
				return false;
			}
			for (int i = firstRowNum + 1; i <= lastRowNum; i++) {
				DecimalFormat df = new DecimalFormat("0");
				String SerialNo=sheet.getRow(i).getCell((short) 0).getStringCellValue();
				if(!StringUtil.StringNull(SerialNo)){
					Content = "被保险人客户号不能为空:xls中第" + (i + 1) + "行 第1列";
					pkg.close();
					return false;
				}
				String InsuredName=sheet.getRow(i).getCell((short) 1).getStringCellValue();
				if(!StringUtil.StringNull(InsuredName)){
					Content = "被保险人姓名不能为空:xls中第" + (i + 1) + "行 第2列";
					pkg.close();
					return false;
				}
				double dSex = sheet.getRow(i).getCell((short) 3).getNumericCellValue();
				String Sex = df.format(dSex);
				if(StringUtil.StringNull(Sex)){
					Content = "被保人性别 不能为空:xls中第" + (i + 1) + "行 第3列";
					pkg.close();
					return false;
				}
				Date Birthday = sheet.getRow(i).getCell((short) 4).getDateCellValue();
				if (null == Birthday || "".equals(Birthday)) {
					Content = "被保人出生日期 不能为空:xls中第" + (i + 1) + "行 第4列";
					pkg.close();
					return false;
				}
				String IdType = sheet.getRow(i).getCell((short) 5).getStringCellValue();
				if (StringUtil.StringNull(IdType)) {
					Content = "被保人银行编码不能为空:xls中第" + (i + 1) + "行 第5列";
					pkg.close();
					return false;
				} 
				String IdNo = sheet.getRow(i).getCell((short) 6).getStringCellValue();
				if (StringUtil.StringNull(IdNo)) {
					Content = "被保人银行账号不能为空:xls中第" + (i + 1) + "行 第6列";
					pkg.close();
					return false;
				} 
				String Money = sheet.getRow(i).getCell((short) 7).getStringCellValue();
				if (StringUtil.StringNull(Money)) {
					Content = "被保人领取金额 不能为空:xls中第" + (i + 1) + "行 第7列";
					pkg.close();
					return false;
				}
				
				LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();
				mLPDiskImportSchema.setSerialNo(SerialNo);
				mLPDiskImportSchema.setInsuredName(InsuredName);
				mLPDiskImportSchema.setSex(Sex);
				mLPDiskImportSchema.setBirthday(Birthday);
				mLPDiskImportSchema.setIDType(IdType);
				mLPDiskImportSchema.setIDNo(IdNo);
				mLPDiskImportSchema.setMoney(Money);
				mLPDiskImportSchema.setEdorNo(EdorAcceptNo);
				mLPDiskImportSchema.setEdorType(dMessHead.getMsgType());
				mLPDiskImportSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
				mLPDiskImportSet.add(mLPDiskImportSchema);
			}
			tt.add(mLPDiskImportSet);
			map.put("mLPDiskImportSet", mLPDiskImportSet);
			map.put("VData", tt);
			pkg.close();
		} catch (Exception e) {
			Content = "解析xls错误";
			e.printStackTrace();
			return false;
		} finally {
			File file = new File(BasePath + FileName);
			file.delete();
		}
	return true;
	}
	
	/**
	 * 当保全有单独逻辑的时候重写次方法
	 */
	@Override
	public boolean domake(){
		
		if(map.get("ExcelURL")!=null){
			getExcel(map.get("ExcelURL").toString());
			/*LQreturn lt=new LQreturn();
			lt.setMatData(map);*/
			Exceldeal ed=new Exceldeal();
			ed.setMatData(map);
			Thread t=new Thread(ed);
			t.start();
			return true;
		}
		
		return false;
	}

	

}
