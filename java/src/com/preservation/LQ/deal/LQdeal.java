package com.preservation.LQ.deal;

import java.util.HashMap;
import java.util.Map;

import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.utility.VData;

/**
 * @author dongjl
 *
 *导入excel开启多线程的业务类
 */
public class LQdeal {

	private Map mMap=new HashMap();
	public void deal(Map<String,Object> map){
		LPDiskImportSet  mLPDiskImportSet=(LPDiskImportSet) map.get("mLPDiskImportSet");
		mMap.put(mLPDiskImportSet, "INSERT");
		submit();
	}
	
	 private boolean submit()
	    {
	        VData data = new VData();
	        data.add(mMap);
	        PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(data, ""))
	        {
	           // mErrors.copyAllErrors(tPubSubmit.mErrors);
	            return false;
	        }
	        return true;
	    }
	 
	
}
