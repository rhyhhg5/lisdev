package com.preservation.WT;

import java.util.List;

import org.jdom.Element;

import com.preservation.BQBase.DealBase;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.GrpEdorWTDetailUI;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LPContPlanRiskSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.vschema.LPContPlanRiskSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class DealWT extends DealBase
{
	/**
	 * 保全项目明细录入界面保存调用后台方法
	 * 
	 * @return
	 */
	public boolean checkSave() {

		System.out.println("\n\n\n GEdorTypeWTSubmit.jsp " + GI.Operator + " "
				+ PubFun.getCurrentDate() + " " + PubFun.getCurrentTime());

		// 团体项目信息
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorAcceptNo(EdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorNo(EdorAcceptNo);
		tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
		tLPGrpEdorItemSchema.setEdorType("WT");
		tLPGrpEdorItemSchema.setReasonCode(mFormData.getReason_tb());

		// 下面为险种选择列表中的数据，根据报文中传值直接查询到需要进行退保的保单
		ExeSQL tExeSQL = new ExeSQL();
		
		// 团体保障下险种信息
		LPContPlanRiskSet tLPContPlanRiskSet = new LPContPlanRiskSet();
		//保障计划编码从报文中获取值，为逗号隔开的字符串
		//String[] contPlansCode = mFormData.getPlanCode().split(",");
		List<Element> list= mFormData.getPlanCodes();
		for(int i=0;i<list.size();i++){
			String sqlLCGrpPolGrid = " select a.contPlanCode, "
		            + "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
		            + "   a.riskCode, b.riskName, count(distinct c.insuredNo), sum(c.prem), "
		            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType, min(c.cValiDate) "
		            + "from LCContPlanRisk a, LMRiskApp b, LCPol c "
		            + "where a.riskCode = b.riskCode "
		            + "   and a.grpContNo = c.grpContNo "
		            + "   and a.contPlanCode = c.contPlanCode "
		            + "   and a.riskCode = c.riskCode "
		            + "   and a.grpContNo = '" + mLCGrpContInfo.getGrpContNo() + "' "
		            + " and a.ContPlanCode = '"+list.get(i).getChildText("PlanCode")+"' "
		            + "group by c.grpContNo, a.contPlanCode, a.riskCode, b.riskName, "
		            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType "
		            + "order by a.contPlanCode ";

			SSRS sqlLCGrpPolGridSSRS = tExeSQL.execSQL(sqlLCGrpPolGrid);
			
			LPContPlanRiskSchema tLPContPlanRiskSchema = new LPContPlanRiskSchema();
			tLPContPlanRiskSchema.setGrpContNo(mLCGrpContInfo.getGrpContNo());
			tLPContPlanRiskSchema.setContPlanCode(list.get(i).getChildText("PlanCode"));
			tLPContPlanRiskSchema.setRiskCode(sqlLCGrpPolGridSSRS.GetText(i+1,3));
			tLPContPlanRiskSet.add(tLPContPlanRiskSchema);
		}
		// 工本费
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = null;
		String costCheck = "1";
		if (costCheck != null && costCheck.equals("1")) {
			tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema.setEdorAcceptNo(EdorAcceptNo);
			tLPEdorEspecialDataSchema.setEdorNo(EdorAcceptNo);
			tLPEdorEspecialDataSchema.setEdorType("WT");
			tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
			//此为工本费用，字段类型需要重新确定
			tLPEdorEspecialDataSchema.setEdorValue(mFormData.getCost());
			tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
		}

		VData tVData = new VData();
		tVData.add(GI);
		tVData.add(tLPGrpEdorItemSchema);
		tVData.add(tLPContPlanRiskSet);
		tVData.add(tLPEdorEspecialDataSchema);
		// tVData.add(tLPPENoticeSet);

		GrpEdorWTDetailUI tGrpEdorWTDetailUI = new GrpEdorWTDetailUI();
		if (!tGrpEdorWTDetailUI.submitData(tVData, "")) {
			Content = "保存失败，原因是:" + tGrpEdorWTDetailUI.mErrors.getFirstError();
			System.out.println(Content);
			return false;
		} else {
			Content = "保存成功";
			System.out.println(Content);
		}
		return true;
	}

}
