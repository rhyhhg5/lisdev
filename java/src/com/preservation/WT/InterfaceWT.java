package com.preservation.WT;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.jdom.Element;

import com.preservation.BQBase.InterfaceBase;
import com.preservation.obj.CustomerInfo;
import com.preservation.obj.FormData;
import com.preservation.obj.LCGrpContInfo;
import com.preservation.obj.MessHead;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;


public class InterfaceWT extends InterfaceBase{


	public boolean deal(String xml) {
		this.pxml=xml;
		// 得到并解析数据
		if (!getkData(xml)) {
			return false;
		}
		return dealc();
	}
	/**
	 * 逻辑处理
	 * @throws Exception 
	 */
	public boolean dealc(){
		DealWT dealWT = new DealWT();
		ExeSQL tExeSQL = new ExeSQL();
		MessHead mMessHead = new MessHead();
		GlobalInput GI = new GlobalInput();
		mMessHead = (MessHead) data.getObjectByObjectName("MessHead", 0);
		GI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		if (!dealWT.deal(data)) {
			errorValue = dealWT.getErrorValue();
			gongdanhao = dealWT.getEdorAcceptNo();
			
			String sql="";
			try {
				sql = "insert into ErrorInsertLog " +
						"(Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ," +
						"Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ," +
						"transactionBatch ,transactionDescription ,transactionAddress ) " +
						"values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
						+GI.Operator+"','"
						+pxml+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), " +
						"to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), " +
						"'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+returnXML(pxml)+"','中间状态','"+errorValue+"','"+mMessHead.getMsgType()+"','"
						+mMessHead.getBranchCode()+"','"+mMessHead.getBatchNo()+"','交易描述','交易地址')";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(StringUtil.StringNull(sql)){
				tExeSQL.execUpdateSQL(sql);
			}
					
			return false;
		}
		gongdanhao = dealWT.getEdorAcceptNo();
		errorValue = "";
		return true;
	}

	

	/**
	 * 解析保全报文的body部分
	 * 
	 */
	public void getkDatabody(Element rootElement ){
		
		Element WT = rootElement.getChild("WT");
		// 客户号
		Element msgCustomer = WT.getChild("CustomerInfo");
		Element eCustomerInfo = msgCustomer.getChild("Item");
		String CustomerNo = eCustomerInfo.getChildText("CustomerNo");

		CustomerInfo iCustomerInfo = new CustomerInfo();
		iCustomerInfo.setCustomerNo(CustomerNo);
		data.add(iCustomerInfo);

		// 团单信息
		Element lCGrpContInfo = WT.getChild("LCGrpContInfo");
		Element eLCGrpContInfo = lCGrpContInfo.getChild("Item");
		String GrpContNo = eLCGrpContInfo.getChildText("GrpContNo");
		String GrpName = eLCGrpContInfo.getChildText("GrpName");

		LCGrpContInfo iLCGrpContInfo = new LCGrpContInfo();
		iLCGrpContInfo.setGrpContNo(GrpContNo);
		iLCGrpContInfo.setGrpName(GrpName);
		data.add(iLCGrpContInfo);

		// 人员信息
		Element xFormData = WT.getChild("FormData");
		Element xxFormData = xFormData.getChild("Item");
		FormData tFormData = new FormData();

		tFormData.setEdorValiDate(xxFormData.getChildText("EdorValiDate"));
		cValiDate = xxFormData.getChildText("EdorValiDate");
		tFormData.setPriorityNo(xxFormData.getChildText("PriorityNo"));
		tFormData.setWorkTypeNo(xxFormData.getChildText("WorkTypeNo"));
		tFormData.setAcceptWayNo(xxFormData.getChildText("AcceptWayNo"));
		tFormData.setApplyTypeNo(xxFormData.getChildText("ApplyTypeNo"));
		tFormData.setApplyName(xxFormData.getChildText("ApplyName"));
		tFormData.setRemark(xxFormData.getChildText("Remark"));
		//tFormData.setPlanCode(xxFormData.getChildText("PlanCode"));
		tFormData.setCost(xxFormData.getChildText("cost"));
		tFormData.setReason_tb(xxFormData.getChildText("reason_tb"));
		tFormData.setPayMode(xxFormData.getChildText("PayMode"));
		tFormData.setPayDate(xxFormData.getChildText("PayDate"));
		tFormData.setEndDate(xxFormData.getChildText("EndDate"));
		tFormData.setBank(xxFormData.getChildText("Bank"));
		tFormData.setBankAccNo(xxFormData.getChildText("BankAccNo"));
		tFormData.setEdorType(xxFormData.getChildText("EdorType")); 

		Element riskCodeObj =WT.getChild("riskCodeObj");
		List<Element> list= riskCodeObj.getChildren("Item");
		tFormData.setPlanCodes(list);
		
		data.add(tFormData);
		
	}
	
	
	public static void main(String[] args) {

		InterfaceWT tInterfaceWT = new InterfaceWT();

		try {
			InputStream pIns = new FileInputStream("D:/llll/BQ001.xml");
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
			String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(),
					"GBK");
			tInterfaceWT.deal(mInXmlStr);
			tInterfaceWT.returnXML(mInXmlStr);
			pIns.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}
