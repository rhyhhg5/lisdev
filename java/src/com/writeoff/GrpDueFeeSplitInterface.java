package com.writeoff;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import org.jdom.JDOMException;

import com.claim.serviceTest.TestSentXMLThrowWebservice;
/**
 * 最后一期拆分
 * @author Administrator
 *
 */
public class GrpDueFeeSplitInterface {
	
	public String service(String xml) throws JDOMException, IOException{
		GrpDueFeeSplit tfq=new GrpDueFeeSplit();
		//业务操作,返回boolean
		System.out.println("..........................刘泽宏......................................");
		tfq.parseXml(xml);                    //parseXml()中会进行判空校验,此处不必做任何校验
		String responseXml = tfq.responseXml();
		//保存返回报文
		if(!tfq.getUrlName("2","CF")) {
			return tfq.responseXml();
		}
	    String  inFilePath = tfq.getStrUrl()+tfq.getDateTime()+"CF"+".xml";
		FileWriter fw=new FileWriter(inFilePath);
		fw.write(responseXml);         
		fw.flush();         
		fw.close();
		return responseXml;
	}
	
	public static void main(String[] args) {
    	try {
			TestSentXMLThrowWebservice test = new TestSentXMLThrowWebservice();
			GrpDueFeeSplitInterface grpDueFeeSplitInterface=new GrpDueFeeSplitInterface();
			String mInFilePath = "D:\\renewal\\scan//拆分.xml";
			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = test.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "gbk");
			System.out.println(mInXmlStr);
			String service = grpDueFeeSplitInterface.service(mInXmlStr);
			System.out.println(service);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}