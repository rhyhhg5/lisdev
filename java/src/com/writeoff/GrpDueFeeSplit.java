package com.writeoff;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.operfee.GrpDueFeeSplitUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpPayPlanSchema;
import com.sinosoft.lis.vschema.LCGrpPayPlanSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * (拆分)
 * 解析校验，调用核心处理。
 * @author lx
 *
 */
public class GrpDueFeeSplit {

	private Logger log = Logger.getLogger(GrpDueFeeSplit.class);
	private XMLOutputter out = new XMLOutputter();
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private String batchNo="";                               //批次号
	private String sendDate="";                              //发送日期
	private String sendTime="";                              //发送时间
	private String branchCode="";                            //交易编码
	private String sendOperator="";                          //交易人员
	private String msgType="";                               //固定值
	private LCGrpPayPlanSet  tLCGrpPayPlanSet  =new LCGrpPayPlanSet ();
	private String ProposalGrpContNo = "";	//集体保单号码
	private String tPlanCode ="";
	private String Content = "";            //处理信息
	private String GrpContNo="";
	private String PrtNo="";
	private String responseXml = "";
	private String strUrl = "";
	
	public String getStrUrl() {   //供外部获取url
		return strUrl;
	}
	// 组装返回报文head节点
	Element head_Response = new Element("head");
	// 组装返回报文bady节点
	Element bady_Response = new Element("body");
	Element State = new Element("State");// 处理状态：00－成功；01－失败
	Element ErrInfo = new Element("ErrInfo");// 错误信息
	// 创建返回报文
	Element Tempfee_Response = new Element("Tempfee_Respond");
	ExeSQL exeSQL = new ExeSQL();

	public boolean parseXml(String xml) throws JDOMException, IOException {
		System.out.println("..........................begin......................................");
		if (!StringUtil.StringNull(xml)) {
			Content = "请求报文不能为空";
			State.setText("01");
			return false;
		}
		try {
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			Element head = rootElement.getChild("head");
			batchNo = head.getChildText("BatchNo");
			sendDate = head.getChildText("SendDate");
			sendTime = head.getChildText("SendTime");
			branchCode = head.getChildText("BranchCode");
			sendOperator = head.getChildText("SendOperator");
			msgType = head.getChildText("MsgType");
			Element body = rootElement.getChild("body");
			String PayDate = body.getChildText("PayDate");
			GrpContNo = body.getChildText("GrpContNo");
			PrtNo = body.getChildText("PrtNo");
			if (!(StringUtil.StringNull(batchNo) && StringUtil.StringNull(sendDate) && StringUtil.StringNull(sendTime)
					&& StringUtil.StringNull(branchCode) && StringUtil.StringNull(sendOperator)
					&& StringUtil.StringNull(msgType))) {
				Content = "报文解析失败，报文头信息缺失！";
				State.setText("01");
				return false;
			}
			if (!"CF".equalsIgnoreCase(msgType)) {
				Content = "请核对MsgType节点";
				State.setText("01");
				return false;
			}
			//保存请求报文
			if (!getUrlName("1","CF")) {
				return false;
			}
		    String  InFilePath = strUrl+getDateTime()+"CF"+".xml";
			FileWriter fw=new FileWriter(InFilePath);
			fw.write(xml);         
			fw.flush();         
			fw.close();
			
			if (!StringUtil.StringNull(batchNo)) {
				Content = "最后一期缴费日期不能为空!";
				State.setText("01");
				return false;
			}
			
			Element PayFeeInfo = body.getChild("PayFeeInfo"); // 本次缴费信息
			Element PlanCodeObj = PayFeeInfo.getChild("PlanCodeObj"); // 内部为缴费循环
			@SuppressWarnings("unchecked")
			List<Element> items = PlanCodeObj.getChildren();
			if (!CheckData(GrpContNo, PrtNo, items)) {
				return false;
			}
			String ManageCom = body.getChildText("ManageCom");
			//String AgentCode = body.getChildText("AgentCode");
			GlobalInput tGI = new GlobalInput();
			//tGI.Operator = "ceshi9";
			tGI.Operator = "BAOQA";
			tGI.ManageCom = ManageCom;
			TransferData tempTransferData = new TransferData();
				tempTransferData.setNameAndValue("PrtNo", PrtNo);
				tempTransferData.setNameAndValue("GrpContNo", GrpContNo);
				tempTransferData.setNameAndValue("ProposalGrpContNo",ProposalGrpContNo);
				tempTransferData.setNameAndValue("PlanCode", tPlanCode);
				tempTransferData.setNameAndValue("PayDate", PayDate);
			VData tVData = new VData();
			tVData.add(tGI);
			tVData.add(tempTransferData);
			tVData.add(tLCGrpPayPlanSet);
			GrpDueFeeSplitUI tGrpDueFeeSplitUI = new GrpDueFeeSplitUI();
			tGrpDueFeeSplitUI.submitData(tVData, "");
			if (!tGrpDueFeeSplitUI.mErrors.needDealError()) {
				State.setText("00");
				Content = "Successful";
			} else {
				Content = " 失败，原因是:"
					+tGrpDueFeeSplitUI.mErrors.getFirstError();
				State.setText("01");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Content = "解析报文出错,请核对报文";
			State.setText("01");
			return false;
		}
		System.out.println(".............................end......................................");
		return true;
	}

	public boolean CheckData(String grpContNo, String prtNo, List<Element> items) throws IOException {
		// 非空校验
		if (!(null != grpContNo && !"".equals(grpContNo.trim())) && (null != prtNo && !"".equals(prtNo.trim()))) {
			State.setText("01");
			Content="保单号和印刷号不能同时为空！";
			return false;
		}
		//判断该团单是不是约定缴费的团单或该保单是否已失效
		String sql = "select grpcontno,prtno from lcgrpcont a "
				+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') ";
		String sql1 = " and GrpContNo = '" + grpContNo + "' ";
		String sql2 = "and PrtNo = '" + prtNo + "' ";
		String sql3 = " and exists (select 1 from lcgrppayplan where prtno=a.prtno) with ur ";
		StringBuffer sbBuffer = new StringBuffer(sql);
		if (!(grpContNo == null||"".equals(grpContNo.trim()))) {
			sbBuffer.append(sql1);
		}
		if (!(prtNo == null||"".equals(prtNo.trim()))) {
			sbBuffer.append(sql2);
		}
		sbBuffer.append(sql3);
		SSRS ssrs = exeSQL.execSQL(sbBuffer.toString());
		if (ssrs.getMaxRow() <= 0) {
			State.setText("01");
			Content="该团单不是约定缴费的团单或该保单已失效,请核对!";
			return false;
		} else {
			GrpContNo = ssrs.GetText(1, 1);
			PrtNo = ssrs.GetText(1, 2);
		}
		// 判断该团单是否已做过团单终止保全项目，以判断能否做续期抽档
		String Sql = "select grpcontno from lcgrpcont a "
				+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') ";
		String Sql1 = " and GrpContNo = '" + grpContNo + "' ";
		String Sql2 = "and PrtNo = '" + prtNo + "' ";
		String Sql3 = " and exists (select 1 from lcgrppayplan where prtno=a.prtno) and state = '03050002' with ur ";
		StringBuffer SbBuffer = new StringBuffer(Sql);
		if (!(grpContNo == null||"".equals(grpContNo.trim()))) {
			SbBuffer.append(Sql1);
		}
		if (!(prtNo == null||"".equals(prtNo.trim()))) {
			SbBuffer.append(Sql2);
		}
		SbBuffer.append(Sql3);
		SSRS ssrs1 = exeSQL.execSQL(SbBuffer.toString());
		if (ssrs1.getMaxRow() > 0) {
			State.setText("01");
			Content="该团单已做过团单终止保全项目，不能做续期抽档!";
			return false;
		}
		String GrpContSQL1 = "select contplancode,(select contplanname from lccontplan where proposalgrpcontno=a.proposalgrpcontno and contplancode=a.contplancode), "
				+ " prem,(select varchar(sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and plancode=a.plancode and state = '2'),"
				+ " paytodate,plancode,(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2'),proposalgrpcontno"
				+ " from lcgrppayplan a " + " where prtno= '" + PrtNo + "'  "
				+ " and int(plancode)=(select max(int(plancode)) from lcgrppayplan b where prtno= '" + PrtNo + "'  )"
				+ " and not exists (select 1 from lcgrppayplandetail c where c.prtno=a.prtno and int(c.plancode)<int(a.plancode) and c.state='2') "
				+ " and exists (select 1 from lcgrppayplandetail c where c.prtno=a.prtno and c.plancode=a.plancode and c.state='2') "
				+ " and not exists (select 1 from ljspay c where c.getnoticeno=(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2')) "
				+ " order by contPlanCode";
		SSRS noFeeSSRS = exeSQL.execSQL(GrpContSQL1);
		ProposalGrpContNo = noFeeSSRS.GetText(1, 8); // 集体保单号码
		int maxRow = noFeeSSRS.getMaxRow(); // 未缴费信息总条数
		if (maxRow <= 0) {
			State.setText("01");
			Content="保单最后一期已经抽档或者之前有未核销的期次！";
			return false;
		} else {
			if (maxRow == items.size()) {
				int n = 0; // 计数，判断期次和保障计划编码是否一致
				for (int i = 0; i < items.size(); i++) { // 缴费循环
					Element item = items.get(i);
					// 获取报文中的保障计划编码和缴费金额
					String contPlanCode = item.getChildText("PlanCode");// 保障计划编码
					String prem = item.getChildText("GetMoney");
					for (int k = 1; k <= maxRow; k++) {
						// 获取查询到的缴费信息
						String contPlanCode1 = noFeeSSRS.GetText(k, 1); // 保障计划编码
						if (contPlanCode1.equals(contPlanCode)) {
							n++;
						}
					}
					// 缴费金额不能为空且不能为空
					if ("".equals(prem) || prem == null || "0".equals(prem)) {
						State.setText("01");
						Content="第"+(i+1)+"个缴费金额（GetMoney）不能为空且不能为0！";
						return false;
					}
				}
				// 校验报文中的期次和保障计划编码是否一致
				if (n != items.size()) {
					State.setText("01");
					Content="请核对报文保障计划！";
					return false;
				}
				// 调后台，进核心
				tPlanCode = noFeeSSRS.GetText(1, 6);
				for (int i = 1; i <= maxRow; i++) // 数据库循环
				{
					for (int j = 0; j < items.size(); j++) { // 报文循环
						if (noFeeSSRS.GetText(i, 1).equals(items.get(j).getChildText("PlanCode"))) {
							LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
							tLCGrpPayPlanSchema.setPrtNo(PrtNo);
							tLCGrpPayPlanSchema.setProposalGrpContNo(ProposalGrpContNo);
							tLCGrpPayPlanSchema.setContPlanCode(noFeeSSRS.GetText(i, 1));
							tLCGrpPayPlanSchema.setPrem(items.get(j).getChildText("GetMoney"));
							tLCGrpPayPlanSchema.setPaytoDate(noFeeSSRS.GetText(i, 5));
							tLCGrpPayPlanSchema.setPlanCode(noFeeSSRS.GetText(i, 6));
							tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
							System.out.println("ContPlanCode:" + noFeeSSRS.GetText(i, 1));
							System.out.println("Prem:" + items.get(j).getChildText("GetMoney"));
							System.out.println("PaytoDate:" + noFeeSSRS.GetText(i, 5));
							System.out.println("PlanCode:" + noFeeSSRS.GetText(i, 6));
						}
					}
				}
			}else {
				State.setText("01");
				Content="Item标签数据条数应该等于'"+ maxRow +"'！";
				return false;
			}
		}
		return true;
	}
	/**
	 * 返回报文信息
	 * @return
	 * @throws IOException
	 */
	public String responseXml() throws IOException {
		//组装返回报文
		Element BatchNo = new Element("BatchNo");
		BatchNo.setText(this.batchNo);// 批次号
		head_Response.addContent(BatchNo);

		Element SendDate = new Element("SendDate");
		SendDate.setText(this.sendDate);// 发送日期
		head_Response.addContent(SendDate);

		Element SendTime = new Element("SendTime");
		SendTime.setText(this.sendTime);// 发送时间
		head_Response.addContent(SendTime);

		Element BranchCode = new Element("BranchCode");
		BranchCode.setText(this.branchCode);// 交易编码
		head_Response.addContent(BranchCode);

		Element SendOperator = new Element("SendOperator");
		SendOperator.setText(this.sendOperator);// 交易人员
		head_Response.addContent(SendOperator);

		Element MsgType = new Element("MsgType");
		MsgType.setText(this.msgType);// 固定值CF
		head_Response.addContent(MsgType);

		Tempfee_Response.addContent(head_Response);
		Tempfee_Response.addContent(bady_Response);
		ErrInfo.setText(Content);
		head_Response.addContent(State);
		head_Response.addContent(ErrInfo);
		responseXml = out.outputString(Tempfee_Response);
		log.info("最后一期拆分接口返回报文<?xml version='1.0' encoding='GBK'?>" + responseXml);
		return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
	}
	
	 /**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
     */
	public boolean getUrlName(String type,String msgType){//type=1:请求报文  type=2：返回报文
    	 //String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";//发盘报文的存放路径
    	 String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='SYYBPath'";//发盘报文的存放路径
    	// String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='SYYB'";//正式接口请求报文地址
    	 String fileFolder = new ExeSQL().getOneValue(sqlurl);
         if (fileFolder == null || fileFolder.equals("")){
        	 Content="获取文件存放路径出错";
        	 State.setText("01");
             return false;
         }
         if(type.equals("1")){
        	 strUrl =fileFolder +"XQRequest/"+ PubFun.getCurrentDate2()+"/";
         }else{
        	 strUrl =fileFolder+"XQRsponse/"+ PubFun.getCurrentDate2()+"/";
         }
         if (!newFolder(strUrl)){
        	 return false;
         }
         return true;
    }
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public  boolean newFolder(String folderPath){
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try{
            if (myFilePath.isDirectory()){
                System.out.println("目录已存在,文件路径为:"+myFilePath);
                return true;
            }else{
                myFilePath.mkdirs();
                System.out.println("新建目录成功,文件路径为:"+myFilePath);
                return true;
            }
        }catch (Exception e){
            System.out.println("新建目录失败");
            Content="新建目录失败";
            State.setText("01");
            e.printStackTrace();
            return false;
        }
    }
    /**
	 * 获得服务器当前日期及时间，以格式为：yyyyMMddHHmmss的日期字符串形式返回,用来拼接存储报文的名称
	 */
	public  String getDateTime() {
		try {
			Calendar cale = Calendar.getInstance();
			return sdf.format(cale.getTime());
		} catch (Exception e) {
			return "";
		}
	}
	
}
