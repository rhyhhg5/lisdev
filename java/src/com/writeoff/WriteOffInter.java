package com.writeoff;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.operfee.GrpLjsPlanConfirmUI;
import com.sinosoft.lis.operfee.NormPayCollOperUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 续期核销
 * @author lx
 *
 */
public class WriteOffInter {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private String Return = "";
	String returnxml = "";
	GlobalInput tGI = new GlobalInput();
	String loadFlag = "";
	CErrors tError = new CErrors();
	private String Content = "";
	String responseXml = "";
	String BatchNo="";
	String SendDate="";
	String SendTime="";
	String BranchCode="";
	String SendOperator="";
	String MsgType="";
	String GrpContNo="";
	String GetNoticeNo="";
	String ManageCom="";
	private String strUrl="";
	
	public String getStrUrl() {
		return strUrl;
	}
	
	public String getContent() {
		return Content;
	}

	VData tVData = new VData();

	public String parseXml(String xml) throws JDOMException, IOException {
		ExeSQL exeSQL = new ExeSQL();
		StringReader reader = new StringReader(xml);
		
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document doc = null;
		try {
			doc = tSAXBuilder.build(reader);
		} catch (JDOMException e1) {
			e1.printStackTrace();
			Content = e1.toString();
			return ReturnInfo("01", Content);
		}
		
		Element rootElement = doc.getRootElement();
		// 报文头
		Element msgHead = rootElement.getChild("head");
		BatchNo = msgHead.getChildText("BatchNo");
		SendDate = msgHead.getChildText("SendDate");
		SendTime = msgHead.getChildText("SendTime");
		BranchCode = msgHead.getChildText("BranchCode");
		SendOperator = msgHead.getChildText("SendOperator");
		MsgType = msgHead.getChildText("MsgType");
		//保存请求报文
		if (!getUrlName("1")) {
			return  ReturnInfo("01", Content);
		}
	    String  InFilePath = strUrl+"/"+getDateTime()+"HX"+".xml";
		FileWriter fw=new FileWriter(InFilePath);
		fw.write(xml);         
		fw.flush();         
		fw.close();
		Element msgBody = rootElement.getChild("body");
		GrpContNo = msgBody.getChildText("GrpContNo");
		String TypeFlag = msgBody.getChildText("TypeFlag");
		ManageCom = msgBody.getChildText("ManageCom");
		//GetNoticeNo = msgBody.getChildText("GetNoticeNo");
		if ("1".equalsIgnoreCase(TypeFlag)) { // 约定
			// 生成批处理表reconcileInfo
			String strSql = "select 1 from LJSPayGrp where grpcontno = '" + GrpContNo + "' and paytype='YET'";
			SSRS arcodeSQL = exeSQL.execSQL(strSql);
			// 取缴费类型，判断paytype是否为YET
			if (arcodeSQL.getMaxRow() > 0) {
				strSql = "select RiskCode,getnoticeno,SumActuPayMoney from LJSPayGrp where grpcontno = '" + GrpContNo
						+ "' and paytype='YET'";
			} else {
				strSql = "select RiskCode,getnoticeno,SumActuPayMoney from LJSPayGrp where grpcontno = '" + GrpContNo
						+ "' and paytype='ZC'";
			}
			arcodeSQL = exeSQL.execSQL(strSql);
			if (arcodeSQL.getMaxRow() > 0) {
				String bankSql = "select BankAccNo,PayMode,EnterAccDate from LJTempFeeClass a where TempFeeNo = '"
						+ arcodeSQL.GetText(1, 2) + "'";
				SSRS bankexecSQL = exeSQL.execSQL(bankSql);
				if (bankexecSQL.getMaxRow()<1) {
					Content = "没有找到暂交费用信息！";
					return ReturnInfo("01", Content);
				}
				String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
				String insertReconcileInfoSql = " INSERT INTO reconcileInfo values( '" + recoinfoId + "', '"
						+ arcodeSQL.GetText(1, 2) + "', '" + bankexecSQL.GetText(1, 2) + "','" + arcodeSQL.GetText(1, 2)
						+ "','2','" + bankexecSQL.GetText(1, 1) + "','0', '', '', '1', '成功', '"
						+ bankexecSQL.GetText(1, 3)
						+ "', '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'pa0001', '86', '', 'PlanCS', 'pay', '', '')";
				exeSQL.execUpdateSQL(insertReconcileInfoSql);
			}

			String strSQL = " select a.grpcontno,sum(a.prem),sum(b.prem),a.paytodate, "
					+ " (select min(paytodate) from lcgrppayplan where prtno=a.prtno and int(plancode)=(int(a.plancode)+1)),a.getnoticeno "
					+ " from lcgrppayactu a,lcgrppayplan b where a.prtno=b.prtno "
					+ " and a.contplancode=b.contplancode and a.paytodate=b.paytodate "
					+ " and a.plancode=b.plancode and a.state='2' "
					+ " and a.getnoticeno in (select getnoticeno from ljspayb where dealstate='4' and othernotype='1' and otherno='"
					+ GrpContNo + "') " + " group by a.prtno,a.getnoticeno,a.grpcontno,a.paytodate,a.plancode with ur ";
			SSRS GrpContGrid = exeSQL.execSQL(strSQL);
			if (GrpContGrid.getMaxRow() <= 0) {
				Content = "没有可核销的数据！";
				return ReturnInfo("01", Content);
			}
			GetNoticeNo = GrpContGrid.GetText(1, 6);
			TransferData tempTransferData = new TransferData();
			tempTransferData.setNameAndValue("GetNoticeNo", GetNoticeNo);
			// tempTransferData.setNameAndValue("GrpContNo", GrpContNo);
			tGI.ManageCom = ManageCom;
		//	tGI.Operator = "ceshi9";
			tGI.Operator = "BAOQA";
			tVData.add(tGI);
			tVData.add(tempTransferData);
			GrpLjsPlanConfirmUI tGrpLjsPlanConfirmUI = new GrpLjsPlanConfirmUI();
			tGrpLjsPlanConfirmUI.submitData(tVData, "");

			if (!tGrpLjsPlanConfirmUI.mErrors.needDealError()) {
				Content = " 处理成功";
				Return = ReturnInfo("00", Content);
			} else {
				Content = " 失败，原因是:" + tGrpLjsPlanConfirmUI.mErrors.getFirstError();
				Return = ReturnInfo("01", Content);
			}
		} else { // 期缴
			String strSql = "select a.GrpContNo,b.GrpName,a.GetNoticeNo,sum(a.SumDuePayMoney), '',value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType <> 'YEL'), 0),"
					+ " value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType = 'YEL'), 0), "
					+ "(select max(EnterAccDate) from LJTempFee where TempFeeNo= a.GetNoticeNo and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode),min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator,'',min(a.LastPayToDate) "
					+ " from LJSPayGrp a ,LCGrpCont  b,LJSPay c " + " where a.GrpContNo='" + GrpContNo + "' "
					+ " and b.PayIntv>0 and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1') and c.othernotype='1' and c.OtherNo=a.GrpContNo"
					+ " and exists(select RiskCode from LMRiskPay where UrgePayFlag='Y' and RiskCode=a.RiskCode)"
					+ " and b.GrpContNo=a.GrpContNo and b.managecom like '" + ManageCom + "%' "
					+ " and ((select count(*) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
					+ " group by a.GrpContNo,b.GrpName,a.GetNoticeNo,a.AgentCode,b.GrpContNo,b.PayMode,c.Operator ";
		//	SSRS GrpContGrid = exeSQL.execSQL(strSql);
			
			LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
			tLCGrpContSchema.setGrpContNo(GrpContNo);
			NormPayCollOperUI tNormPayCollOperUI = new NormPayCollOperUI();
			try {
				tVData.add(tGI);
				tVData.addElement(tLCGrpContSchema);
				tVData.add(loadFlag);
				tNormPayCollOperUI.submitData(tVData, "VERIFY");
				tError = tNormPayCollOperUI.mErrors;

				if (tError.needDealError()) {
					Content = " 核销失败，原因是: " + tNormPayCollOperUI.mErrors.getFirstError();
					Return = ReturnInfo("01", Content);
				} else {
					Content = " 数据处理完毕";
					Return = ReturnInfo("00", Content);
				}
			} catch (Exception ex) {
				Content = "核销失败，原因是:" + ex.toString();
				Return = ReturnInfo("01", Content);
			}
		}
		return Return;
	}

	public String ReturnInfo(String state, String errorInfo) {
		
		Element Dhead_Response = new Element("Tempfee_Respond");
		Document doc = new Document(Dhead_Response);
		Element head_Response = new Element("head");
		Element BatchNo1 = new Element("BatchNo");
		BatchNo1.setText(BatchNo);
		head_Response.addContent(BatchNo1);

		Element SendDate1 = new Element("SendDate");
		SendDate1.setText(SendDate);
		head_Response.addContent(SendDate1);

		Element SendTime1 = new Element("SendTime");
		SendTime1.setText(SendTime);
		head_Response.addContent(SendTime1);

		Element BranchCode1 = new Element("BranchCode");
		BranchCode1.setText(BranchCode);
		head_Response.addContent(BranchCode1);

		Element SendOperator1 = new Element("SendOperator");
		SendOperator1.setText(SendOperator);
		head_Response.addContent(SendOperator1);

		Element MsgType1 = new Element("MsgType");
		MsgType1.setText(MsgType);
		head_Response.addContent(MsgType1);

		Element body_Response = new Element("body");

		Element GrpContNo1 = new Element("GrpContNo");
		GrpContNo1.setText(GrpContNo);
		body_Response.addContent(GrpContNo1);
		Element State1 = new Element("State");
		State1.setText(state);
		body_Response.addContent(State1);
		Element ErrInfo1 = new Element("ErrInfo");
		ErrInfo1.setText(errorInfo);
		body_Response.addContent(ErrInfo1);
		Dhead_Response.addContent(head_Response);
		Dhead_Response.addContent(body_Response);
		XMLOutputter out = new XMLOutputter();
		try {
			responseXml = out.outputString(doc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseXml;
	}

	/**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
	 * @throws IOException 
     */
    public boolean getUrlName(String type) throws IOException{
    	 //String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";//发盘报文的存放路径
    	 String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='SYYBPath'";//发盘报文的存放路径
         String fileFolder = new ExeSQL().getOneValue(sqlurl);

         if (fileFolder == null || fileFolder.equals("")){
        	Content = "getFileUrlName,获取文件存放路径出错";
            return false;
         }
         
         if(type.equals("1")){
        	 strUrl =fileFolder + "XQRequest/" + PubFun.getCurrentDate2();
         }else{
        	 strUrl =fileFolder + "XQRsponse/" + PubFun.getCurrentDate2();
         }
         
         if (!newFolder(strUrl)){
        	 Content = "新建目录失败";
        	 return false;
         }
         return true;
    }
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     * @throws IOException 
     */
    public  boolean newFolder(String folderPath) throws IOException{
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try{
            if (myFilePath.isDirectory()){
                System.out.println("目录已存在,文件路径为:"+myFilePath);
                return true;
            }else{
                myFilePath.mkdirs();
                System.out.println("新建目录成功,文件路径为:"+myFilePath);
                return true;
            }
        }catch (Exception e){
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
    /**
   	 * 获得服务器当前日期及时间，以格式为：yyyyMMddHHmmss的日期字符串形式返回
   	 */
   	public  String getDateTime() {
   		try {
   			Calendar cale = Calendar.getInstance();
   			return sdf.format(cale.getTime());
   		} catch (Exception e) {
   			return "";
   		}
   	}
	
	
	
}
