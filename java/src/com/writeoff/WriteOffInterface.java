package com.writeoff;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.claim.serviceTest.TestSentXMLThrowWebservice;
import com.preservation.utilty.StringUtil;
/**
 * 续期核销
 * @author lx
 *
 */
public class WriteOffInterface {
	
	public String service(String xml) throws JDOMException, IOException {
		WriteOffInter writeOffInter=new WriteOffInter();
		SAXBuilder tSAXBuilder = new SAXBuilder();
		StringReader reader = new StringReader(xml);
		Document doc = null;
		try {
			doc = tSAXBuilder.build(reader);
		} catch (JDOMException e1) {
			e1.printStackTrace();
			return writeOffInter.ReturnInfo("01", e1.toString());
		}
		System.out.println("..........................刘泽宏......................................");
		Element rootElement = doc.getRootElement();
		Element msgHead = rootElement.getChild("head");
		String msgType = msgHead.getChildText("MsgType");
		if (!StringUtil.StringNull(msgType)) {
			return writeOffInter.ReturnInfo("01","MsgType不能为空");
		}
		if ("XQHX".equalsIgnoreCase(msgType)) {
			String returnXml = writeOffInter.parseXml(xml);
			//保存返回报文
			if(!writeOffInter.getUrlName("2")) {
				return writeOffInter.ReturnInfo("01", writeOffInter.getContent());
			}
		    String  inFilePath = writeOffInter.getStrUrl()+"/"+writeOffInter.getDateTime()+"HX"+".xml";
			FileWriter fw=new FileWriter(inFilePath);
			fw.write(returnXml);         
			fw.flush();         
			fw.close();
			return returnXml;
		}else if ("CF".equalsIgnoreCase(msgType)) {
			GrpDueFeeSplitInterface grpDueFeeSplitInterface=new GrpDueFeeSplitInterface();
			return grpDueFeeSplitInterface.service(xml);
		}
		return writeOffInter.ReturnInfo("01","请核对报文节点MsgType");
	}
	public static void main(String[] args) {
    	try {
			TestSentXMLThrowWebservice test = new TestSentXMLThrowWebservice();
			WriteOffInterface writeOffInterface=new WriteOffInterface();
			String mInFilePath = "D:\\renewal\\scan//HX.xml";
			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = test.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "gbk");
			System.out.println(mInXmlStr);
			String service = writeOffInterface.service(mInXmlStr);
			System.out.println(service);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}