package com.privateInterface;

import java.io.IOException;

import org.jdom.JDOMException;

import com.finfee.FinFeePay;

public class FinFeePayInterface {

	public String service(String xml) throws JDOMException, IOException{
		FinFeePay ffp =new FinFeePay();
		return ffp.parse(xml);
	}
}
