package com.privateInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.preservation.AC.InterfaceAC;
import com.preservation.BB.InterfaceBB;
import com.preservation.BJ.InterfaceBJ;
import com.preservation.CM.InterfaceCM;
import com.preservation.CT.InterfaceCT;
import com.preservation.LP.InterfaceLP;
import com.preservation.LQ.InterfaceLQ;
import com.preservation.LR.InterfaceLR;
import com.preservation.MJ.InterfaceMJ;
import com.preservation.TF.InterfaceTF;
import com.preservation.WD.WDInterface;
import com.preservation.WJ.InterfaceWJ;
import com.preservation.WS.WSInterface;
import com.preservation.WT.InterfaceWT;
import com.preservation.WZ.InterfaceWZ;
import com.preservation.XT.InterfaceXT;
import com.preservation.ZB.InterfaceZB;
import com.preservation.ZF.InterfaceZF;
import com.preservation.ZG.InterfaceZG;
import com.preservation.ZT.InterfaceZT;
import com.preservation.newCF.BBCF.InterfaceBBCF;
import com.preservation.newCF.BJCF.InterfaceBJCF;
import com.preservation.newCF.CMCF.InterfaceCMCF;
import com.preservation.newCF.LPCF.InterfaceLPCF;
import com.preservation.newCF.LQCF.InterfaceLQCF;
import com.preservation.newCF.LRCF.InterfaceLRCF;
import com.preservation.newCF.TFCF.InterfaceTFCF;
import com.preservation.newCF.WDCF.WDCFInterface;
import com.preservation.newCF.WJCF.InterfaceWJCF;
import com.preservation.newCF.WSCF.WSCFInterface;
import com.preservation.newCF.WTCF.InterfaceWTCF;
import com.preservation.newCF.WZCF.InterfaceWZCF;
import com.preservation.newCF.XTCF.InterfaceXTCF;
import com.preservation.newCF.ZBCF.InterfaceZBCF;
import com.preservation.newCF.ZFCF.InterfaceZFCF;
import com.preservation.newCF.ZGCF.InterfaceZGCF;
import com.preservation.newCF.TaskInputInterface;
import com.preservation.newCF.ACCF.InterfaceACCF;
import com.preservation.newCF.ANNUL.InterfaceAnnul;
import com.preservation.sweep.ApplySweep;
import com.preservation.sweep.DealSweep;
import com.preservation.sweep.ExamineSweep;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

/**
 * 接口类：接收报文，调用处理类，返回报文 私有接口，包含多个调用接口 1.理赔 2.保全 无名单实名化，无名单删除，新增，减少，撤销 3.新契约
 * 
 * @author LiHao
 *
 */
public class PrivateInterface {
	/**
	 * 
	 * 根据报文头信息类型，调用不同的处理类 WD无名单的删除，WJ无名单减少被保人，WZ无名单增加被保人
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */

	String mUrl = "";
	String aOutXmlStr = "";
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String fileName = sdf.format(date);
	String returnXML = "";

	public byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		try {
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return mByteArrayOutputStream.toByteArray();
	}

	public String service(String xml) throws Exception {
		System.out.println("接收到的报文内容为：");
		System.out.println(xml);
		StringReader reader = new StringReader(xml);
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document doc = tSAXBuilder.build(reader);
		Element rootElement = doc.getRootElement();
		// 报文头
		Element msgHead = rootElement.getChild("MsgHead");
		Element elehead = msgHead.getChild("Item");
		String MsgType = elehead.getChildText("MsgType");
		// 保存请求报文
		getUrlName("1");
		String InFilePath = mUrl + fileName + MsgType + ".xml";
		FileWriter fw = new FileWriter(InFilePath);
		fw.write(xml);
		fw.flush();
		fw.close();

  		System.out.println("============================***Start****=======================" + MsgType);
		
		if ("TASK".equals(MsgType)) {// 经办

			TaskInputInterface jb = new TaskInputInterface();
			jb.deal(xml);
			returnXML = jb.getResponseXml();
			System.out.println("TASK返回报文：" + returnXML);
			
		} else if ("ANNUL".equals(MsgType)) {// 投保人信息修改
			
			InterfaceAnnul interfaceAnnul = new InterfaceAnnul();
			interfaceAnnul.deal(xml);
			returnXML = interfaceAnnul.returnXML(xml);
			System.out.println("AC返回报文：" + returnXML);
			
		} else if ("AC".equals(MsgType)) {// 投保人信息修改
			
			InterfaceAC interfaceAC = new InterfaceAC();
			interfaceAC.deal(xml);
			returnXML = interfaceAC.getReturnXML();
			System.out.println("AC返回报文：" + returnXML);
			
		} else if ("ACCF".equals(MsgType)) {// 投保人信息修改
			
			InterfaceACCF interfaceACCF = new InterfaceACCF();
			interfaceACCF.deal(xml);
			returnXML = interfaceACCF.getReturnXML();
			System.out.println("ACCF返回报文：" + returnXML);
			
		} else if ("BB".equals(MsgType)) {//保费回补
			
			InterfaceBB interfaceBB = new InterfaceBB();
			interfaceBB.deal(xml);
			returnXML = interfaceBB.returnXML(xml);
			System.out.println("BB返回报文：" + returnXML);
			
		} else if ("BBCF".equals(MsgType)) {//保费回补CF
			
			InterfaceBBCF interfaceBBCF = new InterfaceBBCF();
			interfaceBBCF.deal(xml);
			returnXML = interfaceBBCF.returnXML(xml);
			System.out.println("BBCF返回报文：" + returnXML);
			
		} else if ("BJ".equals(MsgType)) {//结余返还
			
			InterfaceBJ interfaceBJ = new InterfaceBJ();
			interfaceBJ.deal(xml);
			returnXML = interfaceBJ.returnXML(xml);
			System.out.println("BJ返回报文：" + returnXML);
			
		} else if ("BJCF".equals(MsgType)) {//结余返还CF
			
			InterfaceBJCF interfaceBJCF = new InterfaceBJCF();
			interfaceBJCF.deal(xml);
			returnXML = interfaceBJCF.returnXML(xml);
			System.out.println("BJCF返回报文：" + returnXML);
			
		} else if ("CM".equals(MsgType)) {//客户资料修改
			
			InterfaceCM interfaceCM = new InterfaceCM();
			interfaceCM.deal(xml);
			returnXML = interfaceCM.returnXML(xml);
			System.out.println("CM返回报文：" + returnXML);
			
		} else if ("CMCF".equals(MsgType)) {//客户资料修改
			
			InterfaceCMCF interfaceCMCF = new InterfaceCMCF();
			interfaceCMCF.deal(xml);
			returnXML = interfaceCMCF.returnXML(xml);
			System.out.println("CMCF返回报文：" + returnXML);
			
		} else if ("LP".equals(MsgType)) {//理赔金账户变更
			
			InterfaceLP interfaceLP = new InterfaceLP();
			interfaceLP.deal(xml);
			returnXML = interfaceLP.returnXML(xml);
			System.out.println("LP返回报文：" + returnXML);
			
		} else if ("LPCF".equals(MsgType)) {//理赔金账户变更
			
			InterfaceLPCF interfaceLPCF = new InterfaceLPCF();
			interfaceLPCF.deal(xml);
			returnXML = interfaceLPCF.returnXML(xml);
			System.out.println("LP返回报文：" + returnXML);
			
		} else if ("LQ".equals(MsgType)) {//部分领取
			
			InterfaceLQ interfaceLQ = new InterfaceLQ();
			interfaceLQ.deal(xml);
			returnXML = interfaceLQ.returnXML(xml);
			System.out.println("LQ返回报文：" + returnXML);
			
		} else if ("LQCF".equals(MsgType)) {//部分领取
			
			InterfaceLQCF interfaceLQCF = new InterfaceLQCF();
			interfaceLQCF.deal(xml);
			returnXML = interfaceLQCF.returnXML(xml);
			System.out.println("LQ返回报文：" + returnXML);
			
		} else if ("LR".equals(MsgType)) {// LR保单遗失补发
			
			InterfaceLR interfaceLR = new InterfaceLR();
			interfaceLR.deal(xml);
			returnXML = interfaceLR.returnXML(xml);
			System.out.println("LR返回报文：" + returnXML);
			
		} else if ("LRCF".equals(MsgType)) {// LR保单遗失补发
			
			InterfaceLRCF interfaceLRCF = new InterfaceLRCF();
			interfaceLRCF.deal(xml);
			returnXML = interfaceLRCF.returnXML(xml);
			System.out.println("LR返回报文：" + returnXML);
			
		} else if ("TF".equals(MsgType)) {//特权复效
			
			InterfaceTF interfaceTF = new InterfaceTF();
			interfaceTF.deal(xml);
			returnXML = interfaceTF.returnXML(xml);
			System.out.println("TF返回报文：" + returnXML);
			
		} else if ("TFCF".equals(MsgType)) {//特权复效
			
			InterfaceTFCF interfaceTFCF = new InterfaceTFCF();
			interfaceTFCF.deal(xml);
			returnXML = interfaceTFCF.returnXML(xml);
			System.out.println("TFCF返回报文：" + returnXML);
			
		} else if ("WD".equals(MsgType)) {//WD无名单的删除
			
			WDInterface WDInterface = new WDInterface();
			WDInterface.deal(xml);
			returnXML = WDInterface.getReturnXML();
			System.out.println("WD返回报文：" + returnXML);
			
		} else if ("WDCF".equals(MsgType)) {//WD无名单的删除
			
			WDCFInterface WDCFInterface = new WDCFInterface();
			WDCFInterface.deal(xml);
			returnXML = WDCFInterface.getReturnXML();
			System.out.println("WDCF返回报文：" + returnXML);
			
		} else if ("WJ".equals(MsgType)) {// WJ无名单减少被保人
			
			InterfaceWJ interfaceWJ = new InterfaceWJ();
			interfaceWJ.deal(xml);
			returnXML = interfaceWJ.returnXML(xml);
			System.out.println("WJ返回报文：" + returnXML);
			
		} else if ("WJCF".equals(MsgType)) {// WJ无名单减少被保人
			
			InterfaceWJCF interfaceWJCF = new InterfaceWJCF();
			interfaceWJCF.deal(xml);
			returnXML = interfaceWJCF.returnXML(xml);
			System.out.println("WJCF返回报文：" + returnXML);
			
		} else if ("WT".equals(MsgType)) {//WT保单犹豫期退保
			
			InterfaceWT interfaceWT = new InterfaceWT();
			interfaceWT.deal(xml);
			returnXML = interfaceWT.returnXML(xml);
			System.out.println("WT返回报文：" + returnXML);

		} else if ("WTCF".equals(MsgType)) {//WT保单犹豫期退保CF
			
			InterfaceWTCF interfaceWTCF = new InterfaceWTCF();
			interfaceWTCF.deal(xml);
			returnXML = interfaceWTCF.returnXML(xml);
			System.out.println("WTCF返回报文：" + returnXML);

		} else if ("WS".equals(MsgType)) {//无名单实名化
			
			WSInterface ws = new WSInterface();
			ws.deal(xml);
			returnXML = ws.getReturnXML();
			System.out.println("WS返回报文：" + returnXML);
			
		} else if ("WSCF".equals(MsgType)) {//无名单实名化
			
			WSCFInterface wscf = new WSCFInterface();
			wscf.deal(xml);
			returnXML = wscf.getReturnXML();
			System.out.println("WSCF返回报文：" + returnXML);
			
		} else if ("WZ".equals(MsgType)) {//WZ无名单增加被保人
			
			InterfaceWZ interfaceWZ = new InterfaceWZ();
			interfaceWZ.deal(xml);
			returnXML = interfaceWZ.returnXML(xml);
			System.out.println("WZ返回报文：" + returnXML);
			
		} else if ("WZCF".equals(MsgType)) {//WZ无名单增加被保人CF
			
			InterfaceWZCF interfaceWZCF = new InterfaceWZCF();
			interfaceWZCF.deal(xml);
			returnXML = interfaceWZCF.returnXML(xml);
			System.out.println("WZCF返回报文：" + returnXML);
			
		} else if ("XT".equals(MsgType)) {//协议退保保
			
			InterfaceXT interfaceXT = new InterfaceXT();
			interfaceXT.deal(xml);
			returnXML = interfaceXT.returnXML(xml);
			
		} else if ("XTCF".equals(MsgType)) {//协议退保保CF
			
			InterfaceXTCF interfaceXTCF = new InterfaceXTCF();
			interfaceXTCF.deal(xml);
			returnXML = interfaceXTCF.returnXML(xml);
			
		} else if ("ZB".equals(MsgType)) {//追加保费
			
			InterfaceZB interfaceZB = new InterfaceZB();
			interfaceZB.deal(xml);
			returnXML = interfaceZB.returnXML(xml);
			System.out.println("ZB返回报文：" + returnXML);
			
		} else if ("ZBCF".equals(MsgType)) {//追加保费CF
			
			InterfaceZBCF interfaceZBCF = new InterfaceZBCF();
			interfaceZBCF.deal(xml);
			returnXML = interfaceZBCF.returnXML(xml);
			System.out.println("ZBCF返回报文：" + returnXML);
			
		} else if ("ZG".equals(MsgType)) {//追加管理费
			
			InterfaceZG interfaceZG = new InterfaceZG();
			interfaceZG.deal(xml);
			returnXML = interfaceZG.returnXML(xml);
			
		} else if ("ZGCF".equals(MsgType)) {//追加管理费
			
			InterfaceZGCF interfaceZGCF = new InterfaceZGCF();
			interfaceZGCF.deal(xml);
			returnXML = interfaceZGCF.returnXML(xml);
			
		} else if ("ZT".equals(MsgType)) {//减少被保险人
			
			InterfaceZT interfaceZT = new InterfaceZT();
			interfaceZT.deal(xml);
			returnXML = interfaceZT.returnXML(xml);
			
		} else if ("CT".equals(MsgType)) {
			
			InterfaceCT interfaceCT = new InterfaceCT();
			interfaceCT.deal(xml);
			returnXML = interfaceCT.returnXML(xml);
			System.out.println("CT返回报文：" + returnXML);
			
		} else if ("MJ".equals(MsgType)) {
			
			InterfaceMJ interfaceMJ = new InterfaceMJ();
			interfaceMJ.deal(xml);
			returnXML = interfaceMJ.returnXML(xml);
			
		} else if ("ZF".equals(MsgType)) {
			
			InterfaceZF interfaceZF = new InterfaceZF();
			interfaceZF.deal(xml);
			returnXML = interfaceZF.returnXML(xml);
			
		} else if ("ZFCF".equals(MsgType)) {//终止缴费
			
			InterfaceZFCF interfaceZFCF = new InterfaceZFCF();
			interfaceZFCF.deal(xml);
			returnXML = interfaceZFCF.returnXML(xml);
			
		} else if ("SMXGCX".equals(MsgType)) {//扫描查询
			
			DealSweep dealSweep = new DealSweep();
			dealSweep.deal(xml);
			returnXML = dealSweep.getResponseXml();
			System.out.println("SMXGCX返回报文：" + returnXML);
			
		} else if ("SMXGSQ".equals(MsgType)) {//扫描修改申请
			
			ApplySweep applySweep = new ApplySweep();
			applySweep.deal(xml);
			returnXML = applySweep.getResponseXml();
			System.out.println("SMXGSQ返回报文：" + returnXML);
			
		} else if ("SMXGSH".equals(MsgType)) {//扫描修改审核
			
			ExamineSweep examineSweep = new ExamineSweep();
			examineSweep.deal(xml);
			returnXML = examineSweep.getResponseXml();
			System.out.println("SMXGSH返回报文：" + returnXML);
			
		} else {
			return "操作失败，请检查报文头的MsgType信息！MsgType：" + MsgType;
		}
		// 保存返回报文
		getUrlName("2");
		String OutFilePath = mUrl + fileName + MsgType + ".xml";
		FileWriter fw1 = new FileWriter(OutFilePath);
		fw1.write(returnXML);
		fw1.flush();
		fw1.close();

		return returnXML;
	}

	/**
	 * newFolder 新建报文存放文件夹，以便对发盘报文查询
	 * 
	 * @return boolean
	 */
	public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在,文件路径为:" + myFilePath);
				return true;
			} else {
				myFilePath.mkdirs();
				System.out.println("新建目录成功,文件路径为:" + myFilePath);
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败");
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * getUrlName 获取文件存放路径
	 * 
	 * @return boolean
	 */
	private boolean getUrlName(String type) {
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='SYYBPath'";// 发盘报文的存放路径
		String fileFolder = new ExeSQL().getOneValue(sqlurl);
		if (fileFolder == null || fileFolder.equals("")) {
			aOutXmlStr = "getFileUrlName,获取文件存放路径出错";
			return false;
		}

		if (type.equals("1")) {
			mUrl = fileFolder + "BQRequest/"+PubFun.getCurrentDate2()+"/";
		} else {
			mUrl = fileFolder + "BQRsponse/"+PubFun.getCurrentDate2()+"/";
		}

		if (!newFolder(mUrl)) {
			return false;
		}
		return true;
	}
}