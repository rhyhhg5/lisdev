package com.privateInterface;


import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCEstimateSchema;
import com.sinosoft.lis.tb.EstimateUI;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class Dyevaluation {
	CErrors tError = null;
	String transact = "INSERT||MAIN";
	String FlagStr = "";
	String Content = "";
	GlobalInput tG = new GlobalInput();
	String responseXml = "";
	String BatchNo;
	String SendDate;
	String SendTime;
	String BranchCode;
	String SendOperator;
	String MsgType;
	String GrpContNo;
	String ManageCom;
	String ProjectNo;
	String StartDate;
	String EndDate;
	String StartSignDate;
	String EndSignDate;
	String ProjectName;
	String EstimateRate;
	String EstimateBalanceRate;
	String ContEstimatePrem;
	String BackEstimatePrem;
	String Reason;
	String TypeFlag;
	public String service(String xml) {
		StringReader reader = new StringReader(xml);
		String Return = "";
		SAXBuilder tSAXBuilder = new SAXBuilder();
		org.jdom.Document doc = null;
		try {
			doc = tSAXBuilder.build(reader);
		} catch (JDOMException e1) {
			e1.printStackTrace();
		}
		Element rootElement = doc.getRootElement();
		// 报文头
		Element Date = rootElement.getChild("Tempfee_Request");
		Element msgHead = rootElement.getChild("head");
		BatchNo = msgHead.getChildText("BatchNo");
		SendDate = msgHead.getChildText("SendDate");
		SendTime = msgHead.getChildText("SendTime");
		BranchCode = msgHead.getChildText("BranchCode");
		SendOperator = msgHead.getChildText("SendOperator");
		MsgType = msgHead.getChildText("MsgType");

		Element msgBody = rootElement.getChild("body");
		TypeFlag = msgBody.getChildText("TypeFlag");
		ManageCom = msgBody.getChildText("ManageCom");
		ProjectName = msgBody.getChildText("ProjectName");
		ProjectNo = msgBody.getChildText("ProjectNo");

		GrpContNo = msgBody.getChildText("GrpContNo");
		StartDate = msgBody.getChildText("StartDate");
		EndDate = msgBody.getChildText("EndDate");
		StartSignDate = msgBody.getChildText("StartSignDate");
		EndSignDate = msgBody.getChildText("EndSignDate");

		EstimateRate = msgBody.getChildText("EstimateRate");
		EstimateBalanceRate = msgBody.getChildText("EstimateBalanceRate");
		ContEstimatePrem = msgBody.getChildText("ContEstimatePrem");
		BackEstimatePrem = msgBody.getChildText("BackEstimatePrem");
		Reason = msgBody.getChildText("Reason");

		if (GrpContNo == null || "".equals(GrpContNo)) {
			System.out.println("获取保单号码失败！");
			FlagStr = "Fail";
			Content = "获取保单号码失败！";
			Return = ReturnInfo("01", Content);
		}
		System.out.println("保单号:" + GrpContNo);

		if (ManageCom == null || "".equals(ManageCom)) {
			System.out.println("管理机构不能为空，且必须为两位或四位机构！");
			FlagStr = "Fail";
			Content = "管理机构不能为空，且必须为两位或四位机构！";
			Return = ReturnInfo("01", Content);
		}

		
		if (EstimateRate == null || "".equals(EstimateRate)) {
			System.out.println("预估赔付率不可以为空！");
			FlagStr = "Fail";
			Content = "预估赔付率不可以为空！";
			Return = ReturnInfo("01", Content);
		}
		
		
		if (!isNumeric(EstimateRate)) {
			
			System.out.println("预估赔付率必须为数字！");
			FlagStr = "Fail";
			Content = ("预估赔付率必须为数字");
			Return = ReturnInfo("01", Content);
		}
		
		
		
	/*	var tStrs = new Array();
		var tEstimateRate = parseFloat(fm.EstimateRate.value)+"";  
		tStrs = tEstimateRate.split(".");
		if(tStrs.length>1){
			if(tStrs[1].length>4){
				alert("预估赔付率最多保留四位小数！");
				fm.EstimateRate.focus();
				return false;
			}
		}*/
		
		
	if (!isNumeric(EstimateBalanceRate)) {
			
			System.out.println("预估赔付率（含结余返还/保费回补）必须为数字！");
			FlagStr = "Fail";
			Content = ("预估赔付率（含结余返还/保费回补）必须为数字！");
			Return = ReturnInfo("01", Content);
		}
	
	
/*	tStrs = new Array();
	var tEstimateBalanceRate = parseFloat(fm.EstimateBalanceRate.value)+"";  
	tStrs = tEstimateBalanceRate.split(".");
	if(tStrs.length>1){
		if(tStrs[1].length>4){
			alert("预估赔付率（含结余返还/保费回补）最多保留四位小数！");
			fm.EstimateBalanceRate.focus();
			return false;
		}
	}
	}*/
	
	
	
	if (!isInteger(ContEstimatePrem)) {
		
		System.out.println("预估结余返还金额必须为非负整数！");
		FlagStr = "Fail";
		Content = ("预估结余返还金额必须为非负整数！");
		Return = ReturnInfo("01", Content);
	}
		
	if (Reason == null || "".equals(Reason)) {
		System.out.println("录入原因不可以为空！");
		FlagStr = "Fail";
		Content = "录入原因不可以为空！";
		Return = ReturnInfo("01", Content);
	}
	
	
	
/*	Date now = new Date(); 
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");//可以方便地修改日期格式
	String tCurrentDate = dateFormat.format( now ); 
	String tSQL = "select 1 from LCEstimate where grpcontno = '"+GrpContNo+"' and makedate = '"+tCurrentDate+"' ";
	 ExeSQL exeSQL1 = new ExeSQL();
	 SSRS GrpContGrid1 = exeSQL1.execSQL(tSQL);
	
	if(GrpContGrid1.getMaxRow() <= 0){
		System.out.println("今天已录入社保业务动态评估信息，请进行修改！");
		FlagStr = "Fail";
		Content = "今天已录入社保业务动态评估信息，请进行修改！";
		Return = ReturnInfo("01", Content);
	}*/
	
	
		
		
		 StringBuffer sbBuffer = new StringBuffer();
			String strSql1 = "select lgc.managecom,lgc.grpcontno,lgcs.projectname,lgcs.projectno,lgc.grpname, "
					+ "(select EstimateBalanceRate from LCEstimate where grpcontno = lgc.grpcontno order by makedate desc fetch first 1 rows only), "
					+ "lgc.cvalidate,lgc.signdate "
					+ "from lcgrpcont lgc "
					+ "inner join lcgrpcontsub lgcs on lgc.prtno = lgcs.prtno "
					+ "where lgc.appflag = '1' "
					+ "and lgcs.projectname is not null and lgcs.projectname != ''";
			
			
			// EstimateRate, EstimateBalanceRate,ContEstimatePrem,Reason,SerNo
			
			 String strSq2 = "and lgc.ManageCom like '"+ ManageCom +"%' ";
			 String strSq3 = "and  ProjectName like '"+ ProjectName +"'";
			 String strSq4 = "and ProjectNo = '"+ ProjectNo +"'";
			 String strSq5 = "and lgc.GrpContNo = '"+ GrpContNo +"'";
			 String strSq6 = "and CvaliDate = '"+ StartDate +"'";
			 String strSq7 = "and CvaliDate = '"+ EndDate +"'";
			 String strSq8 = "and lgc.SignDate = '"+ StartSignDate +"'";
			 String strSq9 = "and CvaliDate = '"+ EndSignDate +"'";
			 String strSq10 =  " order by lgc.managecom,lgc.grpcontno ";
			 
			 sbBuffer.append(strSql1);
				 if (!("".equals(ManageCom.trim()) || ManageCom ==null)) {
						sbBuffer.append(strSq2);
					}
				 if (!("".equals(ProjectName.trim()) || ProjectName ==null)) {
						sbBuffer.append(strSq3);
					}
				 if (!("".equals(ProjectNo.trim()) || ProjectNo ==null)) {
						sbBuffer.append(strSq4);
					}
				 if (!("".equals(GrpContNo.trim()) || GrpContNo ==null)) {
						sbBuffer.append(strSq5);
					}
				 if (!("".equals(StartDate.trim()) || StartDate ==null)) {
						sbBuffer.append(strSq6);
					}
				 if (!("".equals(EndDate.trim()) || EndDate ==null)) {
						sbBuffer.append(strSq7);
					}
				 if (!("".equals(StartSignDate.trim()) || StartSignDate ==null)) {
						sbBuffer.append(strSq8);
					}
				 if (!("".equals(EndSignDate.trim()) || EndSignDate ==null)) {
						sbBuffer.append(strSq9);
					}
				 sbBuffer.append(strSq10);
				 ExeSQL exeSQL = new ExeSQL();
				 
			
					
					
					
					
					
					
				 
				 
				 
			SSRS GrpContGrid = exeSQL.execSQL(sbBuffer.toString());
			if (GrpContGrid.getMaxRow() <= 0){
				Content = " 保存失败，原因是:未查询到相关社保业务动态评估信息 " ;
				FlagStr = "Fail";
				Return = ReturnInfo("01", Content);
			}
			
			
			tG.Operator = SendOperator;
			tG.ManageCom= ManageCom;
		if ("1".equalsIgnoreCase(TypeFlag)) {
		
		
			if (!"Fail".equals(FlagStr)) {
				LCEstimateSchema tLCEstimateSchema = new LCEstimateSchema();
				tLCEstimateSchema.setGrpContNo(GrpContNo);
				tLCEstimateSchema.setProjectName(GrpContGrid.GetText(1, 3));// 项目名称
		        tLCEstimateSchema.setProjectNo(GrpContGrid.GetText(1, 4));// 项目编码
				tLCEstimateSchema.setEstimateRate(EstimateRate);// 预估赔付率
				tLCEstimateSchema.setEstimateBalanceRate(EstimateBalanceRate);// 预估赔付率（含结余返还）
				tLCEstimateSchema.setContEstimatePrem(ContEstimatePrem);// 保单预估结余返还金额
				tLCEstimateSchema.setBackEstimatePrem(BackEstimatePrem);
				// 预计保费回补金额和保单预估结余返还金额，二者只需录其一
				tLCEstimateSchema.setReason(Reason);// 录入原因
				// 准备向后台传输数据 VData
				VData tVData = new VData();
				FlagStr = "";
				tVData.add(tG);
				tVData.addElement(tLCEstimateSchema);
				EstimateUI mEstimateUI = new EstimateUI();
				try {
					mEstimateUI.submitData(tVData, transact);
				} catch (Exception ex) {
					Content = "保存失败，原因是:" + ex.toString();
					FlagStr = "Fail";
					Return = ReturnInfo("01", Content);
				}
				if (!FlagStr.equals("Fail")) {
					tError = mEstimateUI.mErrors;
					if (!tError.needDealError()) {
						Content = " 保存成功! ";
						FlagStr = "Succ";
						Return = ReturnInfo("00", Content);
					} else {
						Content = " 保存失败，原因是:" + tError.getFirstError();
						FlagStr = "Fail";
						Return = ReturnInfo("01", Content);
					}
				}
			}

		} else {
		
				
			transact = "UPDATE||MAIN";
			
			
			//String mGrpContNo = GrpContGrid.GetText(1, 2);
			//String mProjectName = GrpContGrid.GetText(1, 3);
			//String mProjectNo = GrpContGrid.GetText(1, 4);
			//String EstimateRate = GrpContGrid.GetText(1, 6);
			//String EstimateBalanceRate = GrpContGrid.GetText(1, 7);
			//String ContEstimatePrem = GrpContGrid.GetText(1, 8);
			//String Reason = GrpContGrid.GetText(1, 9);
			//String SerNo = GrpContGrid.GetText(1, 10);
			
			// String BackEstimatePrem = GrpContGrid.GetText(1, 6);

			if (!"Fail".equals(FlagStr)) {
				LCEstimateSchema tLCEstimateSchema = new LCEstimateSchema();
				//tLCEstimateSchema.setSerNo(SerNo);//流水号
				tLCEstimateSchema.setGrpContNo(GrpContNo);
				tLCEstimateSchema.setProjectName(ProjectName);// 项目名称
				tLCEstimateSchema.setProjectNo(ProjectNo);// 项目编码
				tLCEstimateSchema.setEstimateRate(EstimateRate);// 预估赔付率
				tLCEstimateSchema.setEstimateBalanceRate(EstimateBalanceRate);// 预估赔付率（含结余返还）
				tLCEstimateSchema.setContEstimatePrem(ContEstimatePrem);// 保单预估结余返还金额
				tLCEstimateSchema.setBackEstimatePrem(BackEstimatePrem);
				// 预计保费回补金额和保单预估结余返还金额，二者只需录其一
				tLCEstimateSchema.setReason(Reason);// 录入原因
				// 准备向后台传输数据 VData
				VData tVData = new VData();
				FlagStr = "";
				tVData.add(tG);
				tVData.addElement(tLCEstimateSchema);
				EstimateUI mEstimateUI = new EstimateUI();
				try {
					mEstimateUI.submitData(tVData, transact);
				} catch (Exception ex) {
					Content = "保存失败，原因是:" + ex.toString();
					FlagStr = "Fail";
					Return = ReturnInfo("01", Content);
				}
				if (!FlagStr.equals("Fail")) {
					tError = mEstimateUI.mErrors;
					if (!tError.needDealError()) {
						Content = " 保存成功! ";
						FlagStr = "Succ";
						Return = ReturnInfo("00", Content);
					} else {
						Content = " 保存失败，原因是:" + tError.getFirstError();
						FlagStr = "Fail";
						Return = ReturnInfo("01", Content);
					}
				}
			}

		}

		return Return;

	}

	private boolean isInteger(String contEstimatePrem2) {
		// TODO Auto-generated method stub
		return true;
	}

	private boolean isNumeric(String estimateRate2) {
		
		return true;
	}

	public String ReturnInfo(String state, String errorInfo) {

		Element Dhead_Response = new Element("Tempfee_Respond");
		Document doc = new Document(Dhead_Response);
		Element head_Response = new Element("head");
		Element BatchNo1 = new Element("BatchNo");
		BatchNo1.setText(BatchNo);
		head_Response.addContent(BatchNo1);

		Element SendDate1 = new Element("SendDate");
		SendDate1.setText(SendDate);
		head_Response.addContent(SendDate1);

		Element SendTime1 = new Element("SendTime");
		SendTime1.setText(SendTime);
		head_Response.addContent(SendTime1);

		Element BranchCode1 = new Element("BranchCode");
		BranchCode1.setText(BranchCode);
		head_Response.addContent(BranchCode1);

		Element SendOperator1 = new Element("SendOperator");
		SendOperator1.setText(SendOperator);
		head_Response.addContent(SendOperator1);

		Element MsgType1 = new Element("MsgType");
		MsgType1.setText(MsgType);
		head_Response.addContent(MsgType1);

		Element body_Response = new Element("body");

		Element GrpContNo1 = new Element("GrpContNo");
		GrpContNo1.setText(GrpContNo);
		body_Response.addContent(GrpContNo1);
		Element State1 = new Element("State");
		State1.setText(state);
		body_Response.addContent(State1);
		Element ErrInfo1 = new Element("ErrInfo");
		ErrInfo1.setText(errorInfo);
		body_Response.addContent(ErrInfo1);
		Dhead_Response.addContent(head_Response);
		Dhead_Response.addContent(body_Response);
		XMLOutputter out = new XMLOutputter();
		try {
			responseXml = out.outputString(doc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseXml;

	}
}
