package com.privateInterface;

import java.io.IOException;

import org.jdom.JDOMException;

import com.finfee.TempFeeSave;

public class TempFeeSaveInterface {

	public String service(String strxml) throws JDOMException, IOException{
		TempFeeSave tfs=new TempFeeSave();
		return tfs.parse(strxml);
	}
	
	
}
