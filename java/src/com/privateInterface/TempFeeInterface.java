package com.privateInterface;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jdom.JDOMException;

import com.finfee.TempFeeQuery;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

public class TempFeeInterface {
	/**
	 * 
	 * 接口方法，接收报文，返回报文
	 * 
	 * @param xml
	 * @return
	 */
	//创建报文保存地址，根据参数1为请求文件夹，2为响应文件夹
	String mUrl = "";
	//返回报文
	String aOutXmlStr = "";
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String fileName = sdf.format(date);
	
	public String service(String xml) throws JDOMException, IOException{
		//解析报文头
				try {
					
					Document doc = DocumentHelper.parseText(xml);
					
					Element root = doc.getRootElement();
					
					Element tHead = root.element("head");
					
					String tMsgType = tHead.elementText("MsgType");
					
					//保存请求报文
					 getUrlName("1");
					 String InFilePath = mUrl + "/" + fileName + tMsgType + ".xml";
					 System.out.println();
					 System.out.println(InFilePath+"====================请求报文保存地址");
					 FileWriter fw=new FileWriter(InFilePath);
					 fw.write(xml);         
					 fw.flush();          
					 fw.close(); 
		
					 
		//	 if("contract".equals(tMsgType)||"TempFee".equals(tMsgType)){
				 //创建报文处理对象
		       TempFeeQuery tfq=new TempFeeQuery();
		       //执行处理，获取返回报文
		       aOutXmlStr=tfq.TempInfoQuery(xml);
		       //打印保存返回报文
		       getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + "/"+ fileName + tMsgType + ".xml";
				System.out.println();
				System.out.println(OutFilePath+"====================返回报文保存地址");
				
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
		       
		       
		       return aOutXmlStr;
		//	 }
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				return "操作失败，请检查报文！";
	}
				
	/**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
     */
    private  boolean getUrlName(String type){
    	
    	 String sqlurl =  "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";// 发盘报文的存放路径
         String fileFolder = new ExeSQL().getOneValue(sqlurl);
         if (fileFolder == null || fileFolder.equals("")){
        	 aOutXmlStr = "getFileUrlName,获取文件存放路径出错";
             return false;
         }
          if(type.equals("1")){
        	  mUrl = fileFolder + "SYYB/QYRequest/" + PubFun.getCurrentDate2();
         }else{
        	 mUrl = fileFolder + "SYYB/QYRsponse/" + PubFun.getCurrentDate2();
         }
         if (!newFolder(mUrl)){
        	 return false;
         }
         return true;
         
    }
    
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath){
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try{
            if (myFilePath.isDirectory()){
                System.out.println("目录已存在,文件路径为:"+myFilePath);
                return true;
            }else{
                myFilePath.mkdirs();
                System.out.println("新建目录成功,文件路径为:"+myFilePath);
                return true;
            }
        }catch (Exception e){
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
	
}
