package com.contract.obj;

import java.io.Serializable;

/**
 * 
 * 团体客户地址表
 * 
 * @author LiHao
 *
 */
public class LCGrpAddressInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String LinkMan1;// 联系人1VARCHAR(60)
	private String Department1;// 部门1VARCHAR(120)
	private String HeadShip1;// 职务1VARCHAR(30)
	private String Phone1;// 联系电话1VARCHAR(30)
	private String E_Mail1;// E_Mail1 VARCHAR(60)
	private String Fax1;// 传真1VARCHAR(30)
	private String Mobile1;// 联系人1手机号VARCHAR(30)
	private String LinkMan2;// 联系人2VARCHAR(60)
	private String Department2;// 部门2 VARCHAR(120)
	private String HeadShip2;// 职务2 VARCHAR(30)
	private String Phone2;// 联系电话2VARCHAR(30)
	private String E_Mail2;// E_Mail2 VARCHAR(60)
	private String Fax2;// 传真1VARCHAR(30)

	public String getLinkMan1() {
		return LinkMan1;
	}

	public void setLinkMan1(String linkMan1) {
		LinkMan1 = linkMan1;
	}

	public String getDepartment1() {
		return Department1;
	}

	public void setDepartment1(String department1) {
		Department1 = department1;
	}

	public String getHeadShip1() {
		return HeadShip1;
	}

	public void setHeadShip1(String headShip1) {
		HeadShip1 = headShip1;
	}

	public String getPhone1() {
		return Phone1;
	}

	public void setPhone1(String phone1) {
		Phone1 = phone1;
	}

	public String getE_Mail1() {
		return E_Mail1;
	}

	public void setE_Mail1(String e_Mail1) {
		E_Mail1 = e_Mail1;
	}

	public String getFax1() {
		return Fax1;
	}

	public void setFax1(String fax1) {
		Fax1 = fax1;
	}

	public String getMobile1() {
		return Mobile1;
	}

	public void setMobile1(String mobile1) {
		Mobile1 = mobile1;
	}

	public String getLinkMan2() {
		return LinkMan2;
	}

	public void setLinkMan2(String linkMan2) {
		LinkMan2 = linkMan2;
	}

	public String getDepartment2() {
		return Department2;
	}

	public void setDepartment2(String department2) {
		Department2 = department2;
	}

	public String getHeadShip2() {
		return HeadShip2;
	}

	public void setHeadShip2(String headShip2) {
		HeadShip2 = headShip2;
	}

	public String getPhone2() {
		return Phone2;
	}

	public void setPhone2(String phone2) {
		Phone2 = phone2;
	}

	public String getE_Mail2() {
		return E_Mail2;
	}

	public void setE_Mail2(String e_Mail2) {
		E_Mail2 = e_Mail2;
	}

	public String getFax2() {
		return Fax2;
	}

	public void setFax2(String fax2) {
		Fax2 = fax2;
	}
}
