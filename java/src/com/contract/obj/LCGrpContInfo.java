package com.contract.obj;

import java.io.Serializable;

/**
 * 
 * 集体保单表
 * 
 * @author LiHao
 *
 */
public class LCGrpContInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String PrtNo;// 印刷号码 VARCHAR(20) 不为空
	private String ManageCom;// 管理机构 VARCHAR(10)
	private String SaleChnl;// 销售渠道 VARCHAR(2)
	private String AgentCom;// 代理机构VARCHAR(20)
	private String AgentType;// 代理机构内部分类VARCHAR(20)
	private String AgentCode;// 代理人编码VARCHAR(10)
	private String AgentGroup;// 代理人组别VARCHAR(12)
	private String AgentCode1;// 联合代理人代码VARCHAR(10)
	private String AgentSaleCode;// 代理销售人员编码
	private String GrpSpec;// 集体特约VARCHAR(255)
	private String PayMode;// 交费方式
	private String GrpName;// 单位名称VARCHAR(150)
	private String GetFlag;// 付款方式VARCHAR(1)
	private String GrpContPayIntv;// 交费间隔SMALLINT
	private String BankCode;// 银行编码VARCHAR(10)
	private String BankAccNo;// 银行帐号VARCHAR(40)
	private String AccName;// 银行帐户名VARCHAR(120)
	private String Currency;// 币别VARCHAR(2)
	private String CValiDate;// 保单生效日期DATE
	private String OutPayFlag;// 溢交处理方式VARCHAR(1)
	private String EnterKind;// 参保形式VARCHAR(1)
	private String AmntGrade;// 保额等级VARCHAR(1)
	private String Peoples3;// 单位可投保人数INTEGER
	private String OnWorkPeoples;// 在职投保人数INTEGER
	private String OffWorkPeoples;// 退休投保人数INTEGER
	private String OtherPeoples;// 其它投保人数INTEGER
	private String RelaPeoples;// 连带投保人数INTEGER
	private String RelaMatePeoples;// 连带配偶投保人数INTEGER
	private String RelaYoungPeoples;// 连带子女投保人数INTEGER
	private String RelaOtherPeoples;// 连带其它投保人数INTEGER
	private String FirstTrialOperator;// 初审人VARCHAR(30)
	private String ReceiveDate;// 收单日期DATE
	private String Prem;// 总保费NUMERIC(16,2)
	private String Amnt;// 总保额NUMERIC(16,2)
	private String TempFeeNo;// 暂收据号VARCHAR(20)
	private String BusinessBigType;// 行业大类VARCHAR(2)
	private String CoInsuranceFlag;// 共保保单标志VARCHAR(1) 0|^0|非共保保单^1|共保保单
	private String Crs_SaleChnl;// 集团交叉销售渠道VARCHAR(2) 01-产助健；02-寿助健
	private String Crs_BussType;// 集团销售业务类型VARCHAR(2)
	private String GrpNature;// 单位性质VARCHAR(10)
	private String BusinessType;// 行业分类VARCHAR(20)
	private String Peoples;// 总人数INTEGER
	private String RgtMoney;// 注册资本NUMERIC(16,2)
	private String Asset;// 资产总额NUMERIC(16,2)
	private String NetProfitRate;// 净资产收益率NUMERIC(16,4)
	private String MainBussiness;// 主营业务VARCHAR(60)
	private String Corporation;// 法人VARCHAR(20)
	private String ComAera;// 机构分布区域VARCHAR(30)
	private String Phone;// 单位电话VARCHAR(18)
	private String Fax;// 单位传真VARCHAR(18)
	private String FoundDate;// 成立日期DATE
	private String Remark;// 备注VARCHAR(3000)
	private String HandlerName;// 投保经办人VARCHAR(60)
	private String HandlerDate;// 投保单填写日期DATE
	private String HandlerPrint;// 投保单位章VARCHAR(120)
	private String AgentDate;// 业务员填写日期DATE
	private String MarketType;// 市场类型VARCHAR(2)
	private String CInValiDate;// 合同终止日期DATE
	private String AskGrpContNo;// 询价合同号VARCHAR(20)
	private String GrpContSumPrem;// 资金规模NUMERIC(16,2)
	private String BigProjectFlag;// 大项目标识VARCHAR(2)
	private String ContPrintType;// 保单打印类型VARCHAR(2)
									// ^0|常规普通型^1|健管简易型^2|委托管理型^3|大团单^4|简易团单^5|汇交件
	private String GrpAgentCom;// 集团代理机构VARCHAR(20)
	private String GrpAgentIDNo;// 集团代理人身份证VARCHAR(20)
	private String GrpAgentCode;// 集团代理人VARCHAR(20)
	private String GrpAgentName;// 集团代理人姓名VARCHAR(120)
	private String InputOperator;// 录单人VARCHAR(10)
	private String HandlerIDNo;// 业务经办人身份证号
	private String MixComFlag;// 是否为交叉销售
	private String ImpartParam1;
	private String ImpartParam2;
	public String getImpartParam1() {
		return ImpartParam1;
	}

	public void setImpartParam1(String impartParam1) {
		ImpartParam1 = impartParam1;
	}

	public String getImpartParam2() {
		return ImpartParam2;
	}

	public void setImpartParam2(String impartParam2) {
		ImpartParam2 = impartParam2;
	}

	public String getMixComFlag() {
		return MixComFlag;
	}

	public void setMixComFlag(String mixComFlag) {
		MixComFlag = mixComFlag;
	}

	public String getPrtNo() {
		return PrtNo;
	}

	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getSaleChnl() {
		return SaleChnl;
	}

	public void setSaleChnl(String saleChnl) {
		SaleChnl = saleChnl;
	}

	public String getAgentCom() {
		return AgentCom;
	}

	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}

	public String getAgentType() {
		return AgentType;
	}

	public void setAgentType(String agentType) {
		AgentType = agentType;
	}

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	public String getAgentGroup() {
		return AgentGroup;
	}

	public void setAgentGroup(String agentGroup) {
		AgentGroup = agentGroup;
	}

	public String getAgentCode1() {
		return AgentCode1;
	}

	public void setAgentCode1(String agentCode1) {
		AgentCode1 = agentCode1;
	}

	public String getAgentSaleCode() {
		return AgentSaleCode;
	}

	public void setAgentSaleCode(String agentSaleCode) {
		AgentSaleCode = agentSaleCode;
	}

	public String getGrpSpec() {
		return GrpSpec;
	}

	public void setGrpSpec(String grpSpec) {
		GrpSpec = grpSpec;
	}

	public String getPayMode() {
		return PayMode;
	}

	public void setPayMode(String payMode) {
		PayMode = payMode;
	}

	public String getGrpName() {
		return GrpName;
	}

	public void setGrpName(String grpName) {
		GrpName = grpName;
	}

	public String getGetFlag() {
		return GetFlag;
	}

	public void setGetFlag(String getFlag) {
		GetFlag = getFlag;
	}

	public String getGrpContPayIntv() {
		return GrpContPayIntv;
	}

	public void setGrpContPayIntv(String grpContPayIntv) {
		GrpContPayIntv = grpContPayIntv;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getBankAccNo() {
		return BankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}

	public String getAccName() {
		return AccName;
	}

	public void setAccName(String accName) {
		AccName = accName;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getCValiDate() {
		return CValiDate;
	}

	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}

	public String getOutPayFlag() {
		return OutPayFlag;
	}

	public void setOutPayFlag(String outPayFlag) {
		OutPayFlag = outPayFlag;
	}

	public String getEnterKind() {
		return EnterKind;
	}

	public void setEnterKind(String enterKind) {
		EnterKind = enterKind;
	}

	public String getAmntGrade() {
		return AmntGrade;
	}

	public void setAmntGrade(String amntGrade) {
		AmntGrade = amntGrade;
	}

	public String getPeoples3() {
		return Peoples3;
	}

	public void setPeoples3(String peoples3) {
		Peoples3 = peoples3;
	}

	public String getOnWorkPeoples() {
		return OnWorkPeoples;
	}

	public void setOnWorkPeoples(String onWorkPeoples) {
		OnWorkPeoples = onWorkPeoples;
	}

	public String getOffWorkPeoples() {
		return OffWorkPeoples;
	}

	public void setOffWorkPeoples(String offWorkPeoples) {
		OffWorkPeoples = offWorkPeoples;
	}

	public String getOtherPeoples() {
		return OtherPeoples;
	}

	public void setOtherPeoples(String otherPeoples) {
		OtherPeoples = otherPeoples;
	}

	public String getRelaPeoples() {
		return RelaPeoples;
	}

	public void setRelaPeoples(String relaPeoples) {
		RelaPeoples = relaPeoples;
	}

	public String getRelaMatePeoples() {
		return RelaMatePeoples;
	}

	public void setRelaMatePeoples(String relaMatePeoples) {
		RelaMatePeoples = relaMatePeoples;
	}

	public String getRelaYoungPeoples() {
		return RelaYoungPeoples;
	}

	public void setRelaYoungPeoples(String relaYoungPeoples) {
		RelaYoungPeoples = relaYoungPeoples;
	}

	public String getRelaOtherPeoples() {
		return RelaOtherPeoples;
	}

	public void setRelaOtherPeoples(String relaOtherPeoples) {
		RelaOtherPeoples = relaOtherPeoples;
	}

	public String getFirstTrialOperator() {
		return FirstTrialOperator;
	}

	public void setFirstTrialOperator(String firstTrialOperator) {
		FirstTrialOperator = firstTrialOperator;
	}

	public String getReceiveDate() {
		return ReceiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		ReceiveDate = receiveDate;
	}

	public String getPrem() {
		return Prem;
	}

	public void setPrem(String prem) {
		Prem = prem;
	}

	public String getAmnt() {
		return Amnt;
	}

	public void setAmnt(String amnt) {
		Amnt = amnt;
	}

	public String getTempFeeNo() {
		return TempFeeNo;
	}

	public void setTempFeeNo(String tempFeeNo) {
		TempFeeNo = tempFeeNo;
	}

	public String getBusinessBigType() {
		return BusinessBigType;
	}

	public void setBusinessBigType(String businessBigType) {
		BusinessBigType = businessBigType;
	}

	public String getCoInsuranceFlag() {
		return CoInsuranceFlag;
	}

	public void setCoInsuranceFlag(String coInsuranceFlag) {
		CoInsuranceFlag = coInsuranceFlag;
	}

	public String getCrs_SaleChnl() {
		return Crs_SaleChnl;
	}

	public void setCrs_SaleChnl(String crs_SaleChnl) {
		Crs_SaleChnl = crs_SaleChnl;
	}

	public String getCrs_BussType() {
		return Crs_BussType;
	}

	public void setCrs_BussType(String crs_BussType) {
		Crs_BussType = crs_BussType;
	}

	public String getGrpNature() {
		return GrpNature;
	}

	public void setGrpNature(String grpNature) {
		GrpNature = grpNature;
	}

	public String getBusinessType() {
		return BusinessType;
	}

	public void setBusinessType(String businessType) {
		BusinessType = businessType;
	}

	public String getPeoples() {
		return Peoples;
	}

	public void setPeoples(String peoples) {
		Peoples = peoples;
	}

	public String getRgtMoney() {
		return RgtMoney;
	}

	public void setRgtMoney(String rgtMoney) {
		RgtMoney = rgtMoney;
	}

	public String getAsset() {
		return Asset;
	}

	public void setAsset(String asset) {
		Asset = asset;
	}

	public String getNetProfitRate() {
		return NetProfitRate;
	}

	public void setNetProfitRate(String netProfitRate) {
		NetProfitRate = netProfitRate;
	}

	public String getMainBussiness() {
		return MainBussiness;
	}

	public void setMainBussiness(String mainBussiness) {
		MainBussiness = mainBussiness;
	}

	public String getCorporation() {
		return Corporation;
	}

	public void setCorporation(String corporation) {
		Corporation = corporation;
	}

	public String getComAera() {
		return ComAera;
	}

	public void setComAera(String comAera) {
		ComAera = comAera;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getFoundDate() {
		return FoundDate;
	}

	public void setFoundDate(String foundDate) {
		FoundDate = foundDate;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}

	public String getHandlerName() {
		return HandlerName;
	}

	public void setHandlerName(String handlerName) {
		HandlerName = handlerName;
	}

	public String getHandlerDate() {
		return HandlerDate;
	}

	public void setHandlerDate(String handlerDate) {
		HandlerDate = handlerDate;
	}

	public String getHandlerPrint() {
		return HandlerPrint;
	}

	public void setHandlerPrint(String handlerPrint) {
		HandlerPrint = handlerPrint;
	}

	public String getAgentDate() {
		return AgentDate;
	}

	public void setAgentDate(String agentDate) {
		AgentDate = agentDate;
	}

	public String getMarketType() {
		return MarketType;
	}

	public void setMarketType(String marketType) {
		MarketType = marketType;
	}

	public String getCInValiDate() {
		return CInValiDate;
	}

	public void setCInValiDate(String cInValiDate) {
		CInValiDate = cInValiDate;
	}

	public String getAskGrpContNo() {
		return AskGrpContNo;
	}

	public void setAskGrpContNo(String askGrpContNo) {
		AskGrpContNo = askGrpContNo;
	}

	public String getGrpContSumPrem() {
		return GrpContSumPrem;
	}

	public void setGrpContSumPrem(String grpContSumPrem) {
		GrpContSumPrem = grpContSumPrem;
	}

	public String getBigProjectFlag() {
		return BigProjectFlag;
	}

	public void setBigProjectFlag(String bigProjectFlag) {
		BigProjectFlag = bigProjectFlag;
	}

	public String getContPrintType() {
		return ContPrintType;
	}

	public void setContPrintType(String contPrintType) {
		ContPrintType = contPrintType;
	}

	public String getGrpAgentCom() {
		return GrpAgentCom;
	}

	public void setGrpAgentCom(String grpAgentCom) {
		GrpAgentCom = grpAgentCom;
	}

	public String getGrpAgentIDNo() {
		return GrpAgentIDNo;
	}

	public void setGrpAgentIDNo(String grpAgentIDNo) {
		GrpAgentIDNo = grpAgentIDNo;
	}

	public String getGrpAgentCode() {
		return GrpAgentCode;
	}

	public void setGrpAgentCode(String grpAgentCode) {
		GrpAgentCode = grpAgentCode;
	}

	public String getGrpAgentName() {
		return GrpAgentName;
	}

	public void setGrpAgentName(String grpAgentName) {
		GrpAgentName = grpAgentName;
	}

	public String getInputOperator() {
		return InputOperator;
	}

	public void setInputOperator(String inputOperator) {
		InputOperator = inputOperator;
	}

	public String getHandlerIDNo() {
		return HandlerIDNo;
	}

	public void setHandlerIDNo(String handlerIDNo) {
		HandlerIDNo = handlerIDNo;
	}
}
