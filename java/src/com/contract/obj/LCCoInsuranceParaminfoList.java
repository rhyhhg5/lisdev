package com.contract.obj;

import java.io.Serializable;
import java.util.List;

public class LCCoInsuranceParaminfoList implements Serializable {
	private static final long serialVersionUID = 1L;
	private List list;

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
}
