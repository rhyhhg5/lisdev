package com.contract.obj;

import java.io.Serializable;

public class LCCoInsuranceParaminfo implements Serializable {
	private static final long serialVersionUID = 1L;
	//共保机构代码
	private String AgentCom;
	//共保机构名称
	private String Name;
	//负担比例
	private String UpAgentCom;
	public String getAgentCom() {
		return AgentCom;
	}
	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getUpAgentCom() {
		return UpAgentCom;
	}
	public void setUpAgentCom(String upAgentCom) {
		UpAgentCom = upAgentCom;
	}
	
}
