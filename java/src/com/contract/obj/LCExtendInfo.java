package com.contract.obj;

import java.io.Serializable;

/**
 * 
 * 综合开拓标示 选中时的实体类
 * 
 * @author LiHao
 *
 */
public class LCExtendInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String AssistSaleChnl;// 协助销售渠道
	private String AssistAgentCode;// 协助销售人员代码
	private String ExtendFlag;// 是否为综合开拓

	public String getExtendFlag() {
		return ExtendFlag;
	}

	public void setExtendFlag(String extendFlag) {
		ExtendFlag = extendFlag;
	}

	public String getAssistSaleChnl() {
		return AssistSaleChnl;
	}

	public void setAssistSaleChnl(String assistSaleChnl) {
		AssistSaleChnl = assistSaleChnl;
	}

	public String getAssistAgentCode() {
		return AssistAgentCode;
	}

	public void setAssistAgentCode(String assistAgentCode) {
		AssistAgentCode = assistAgentCode;
	}
}
