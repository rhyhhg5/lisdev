package com.contract.obj;

import java.io.Serializable;
import java.util.List;
/**
 * 定义公共账户
 * @author GengYe
 *
 */

public class PubAccInfoList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List list;

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
}
