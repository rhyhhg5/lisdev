package com.contract.obj;

import java.io.Serializable;

/**
 * 
 * ContPlanGrid表單的值（Item1的值）
 * 
 * @author LiHao
 *
 */
public class ContPlanGridInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String DutyCode;// 责任代码
	private String Prem;// 保费
	private String Amnt;// 保额/限额
	private String DutyAmnt;// 保额
	private String GetLimit;// 免赔额
	private String FloatRate;// 折扣
	private String StandbyFlag1;// 旅行类型
	private String WaitPeriod;// 等待期
	private String GetRate;// 给付比例

	public String getDutyCode() {
		return DutyCode;
	}

	public void setDutyCode(String dutyCode) {
		DutyCode = dutyCode;
	}

	public String getPrem() {
		return Prem;
	}

	public void setPrem(String prem) {
		Prem = prem;
	}

	public String getAmnt() {
		return Amnt;
	}

	public void setAmnt(String amnt) {
		Amnt = amnt;
	}

	public String getDutyAmnt() {
		return DutyAmnt;
	}

	public void setDutyAmnt(String dutyAmnt) {
		DutyAmnt = dutyAmnt;
	}

	public String getGetLimit() {
		return GetLimit;
	}

	public void setGetLimit(String getLimit) {
		GetLimit = getLimit;
	}

	public String getFloatRate() {
		return FloatRate;
	}

	public void setFloatRate(String floatRate) {
		FloatRate = floatRate;
	}

	public String getStandbyFlag1() {
		return StandbyFlag1;
	}

	public void setStandbyFlag1(String standbyFlag1) {
		StandbyFlag1 = standbyFlag1;
	}

	public String getWaitPeriod() {
		return WaitPeriod;
	}

	public void setWaitPeriod(String waitPeriod) {
		WaitPeriod = waitPeriod;
	}

	public String getGetRate() {
		return GetRate;
	}

	public void setGetRate(String getRate) {
		GetRate = getRate;
	}

}
