package com.contract.obj;

import java.io.Serializable;

/**
 * 人工核保
 * 
 * @author LiHao
 *
 */
public class LCGCUWMasterInfo implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private String GUWState;// 团体保单核保结论 只可传固定值9 9-正常承保
	private String GUWIdea;// 团体保单核保意见

	public String getGUWState() {
		return GUWState;
	}

	public void setGUWState(String gUWState) {
		GUWState = gUWState;
	}

	public String getGUWIdea() {
		return GUWIdea;
	}

	public void setGUWIdea(String gUWIdea) {
		GUWIdea = gUWIdea;
	}
}
