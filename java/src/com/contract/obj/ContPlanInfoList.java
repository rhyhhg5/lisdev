package com.contract.obj;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 保障计划定制
 * 
 * @author LiHao
 *
 */
public class ContPlanInfoList implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private List list;

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
}
