package com.contract.obj;

import java.io.Serializable;

/**
 * 定义社保要素
 * 
 * @author LiHao
 *
 */
public class LCGrpContSubInfo implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private String ProjectName;/// ProjectName> 项目名称 VARCHAR(900) 不可为空 30字以内
	private String BalanceTermFlag;/// BalanceTermFlag>是否为结余返还 VARCHAR(2) 不可为空
									/// Y——是 N——否
	private String StopLine;/// StopLine> 止损线 VARCHAR(20) 不可为空 止损线必须为非负数字
	private String SharedLine;/// SharedLine>共担线 VARCHAR(20)不可为空 共担线必须为非负数字
	private String RevertantLine;/// RevertantLine> 回补线 VARCHAR(20)不可为空
									/// 回补线必须为非负数字！
	private String AccountCycle;/// AccountCycle> 核算周期 VARCHAR(20) 核算周期必须为非负数字
	private String BalanceLine;/// BalanceLine> 结余线 VARCHAR(20) 结余线必须大于等于0，且小于1！
	private String CostRate;/// CostRate> 成本占比 VARCHAR(20) 成本占比必须大于等于0，且小于1！
	private String BalanceRate;/// BalanceRate>返还比例 VARCHAR(20)
								/// 返还比例必须大于等于0，且小于1！
	private String SharedRate;/// SharedRate>共担比例 VARCHAR(20) 共担比例必须大于等于0，且小于1
	private String RevertantRate;/// RevertantRate>回补比例 VARCHAR(20)
									/// 回补比例必须大于等于0，且小于1！
	private String BalanceTxt;/// BalanceTxt>风险调节文本 VARCHAR(3000) 风险调节文本最多1000字
	private String XBFlag;/// XBFlag>是否为新建项目 VARCHAR(20)不可为空 1——新建项目 2——已有项目
	private String BalanceType;/// BalanceType>返还类型 VARCHAR(2) 0——常规 1——特殊

	public String getProjectName() {
		return ProjectName;
	}

	public void setProjectName(String projectName) {
		ProjectName = projectName;
	}

	public String getBalanceTermFlag() {
		return BalanceTermFlag;
	}

	public void setBalanceTermFlag(String balanceTermFlag) {
		BalanceTermFlag = balanceTermFlag;
	}

	public String getStopLine() {
		return StopLine;
	}

	public void setStopLine(String stopLine) {
		StopLine = stopLine;
	}

	public String getSharedLine() {
		return SharedLine;
	}

	public void setSharedLine(String sharedLine) {
		SharedLine = sharedLine;
	}

	public String getRevertantLine() {
		return RevertantLine;
	}

	public void setRevertantLine(String revertantLine) {
		RevertantLine = revertantLine;
	}

	public String getAccountCycle() {
		return AccountCycle;
	}

	public void setAccountCycle(String accountCycle) {
		AccountCycle = accountCycle;
	}

	public String getBalanceLine() {
		return BalanceLine;
	}

	public void setBalanceLine(String balanceLine) {
		BalanceLine = balanceLine;
	}

	public String getCostRate() {
		return CostRate;
	}

	public void setCostRate(String costRate) {
		CostRate = costRate;
	}

	public String getBalanceRate() {
		return BalanceRate;
	}

	public void setBalanceRate(String balanceRate) {
		BalanceRate = balanceRate;
	}

	public String getSharedRate() {
		return SharedRate;
	}

	public void setSharedRate(String sharedRate) {
		SharedRate = sharedRate;
	}

	public String getRevertantRate() {
		return RevertantRate;
	}

	public void setRevertantRate(String revertantRate) {
		RevertantRate = revertantRate;
	}

	public String getBalanceTxt() {
		return BalanceTxt;
	}

	public void setBalanceTxt(String balanceTxt) {
		BalanceTxt = balanceTxt;
	}

	public String getXBFlag() {
		return XBFlag;
	}

	public void setXBFlag(String xBFlag) {
		XBFlag = xBFlag;
	}

	public String getBalanceType() {
		return BalanceType;
	}

	public void setBalanceType(String balanceType) {
		BalanceType = balanceType;
	}
}
