package com.contract.obj;

import java.io.Serializable;

/**
 * 
 * 团单投保人表
 * 
 * @author LiHao
 *
 */
public class LCGrpAppntInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String OrgancomCode;// 组织机构代码 VARCHAR(10) 长度等于10
	private String GrpAddress;// 投保人地址（联系地址）VARCHAR(120)不可为空 长度小于60
	private String GrpZipCode;// 邮政编码 CHARACTER(6) 不可为空
	private String UnifiedSocialCreditNo;// 统一社会信用代码 VARCHAR(20)
	private String TaxNo;// 税务登记证号码 VARCHAR(120)
	private String LegalPersonName;// 法人姓名 VARCHAR(120)
	private String LegalPersonIDNo;// 法人身份证号 VARCHAR(50)
	private String AppntOnWorkPeoples;// 在职人数 INTEGER
	private String AppntOffWorkPeoples;// 退休人数 INTEGER
	private String AppntOtherPeoples;// 其他人员人数 INTEGER
	private String ClaimBankCode;// 单位代领理赔金转账银行 VARCHAR(10)
	private String ClaimAccName;// 理赔金转账银行户名 VARCHAR(120)
	private String ClaimBankAccNo;// 理赔金转账银行账号 VARCHAR(40)
	private String BusinessScope;// 经营范围 VARCHAR(1200)
	private String IDStartDate;// 证件生效日期 DATE
	private String IDEndDate;// 证件失效日期 DATE
	private String IDNo;// 联系人证 VARCHAR(50)
	private String IDType;// 证件类型 VARCHAR(1) 0-身份证 4-其他
	private String IDLongEffFlag;// >是否长期有效标志 VARCHAR(1)该字段只可传Y或空 Y是长期有效 空不是长期有效

	public String getOrgancomCode() {
		return OrgancomCode;
	}

	public void setOrgancomCode(String organcomCode) {
		OrgancomCode = organcomCode;
	}

	public String getGrpAddress() {
		return GrpAddress;
	}

	public void setGrpAddress(String grpAddress) {
		GrpAddress = grpAddress;
	}

	public String getGrpZipCode() {
		return GrpZipCode;
	}

	public void setGrpZipCode(String grpZipCode) {
		GrpZipCode = grpZipCode;
	}

	public String getUnifiedSocialCreditNo() {
		return UnifiedSocialCreditNo;
	}

	public void setUnifiedSocialCreditNo(String unifiedSocialCreditNo) {
		UnifiedSocialCreditNo = unifiedSocialCreditNo;
	}

	public String getTaxNo() {
		return TaxNo;
	}

	public void setTaxNo(String taxNo) {
		TaxNo = taxNo;
	}

	public String getLegalPersonName() {
		return LegalPersonName;
	}

	public void setLegalPersonName(String legalPersonName) {
		LegalPersonName = legalPersonName;
	}

	public String getLegalPersonIDNo() {
		return LegalPersonIDNo;
	}

	public void setLegalPersonIDNo(String legalPersonIDNo) {
		LegalPersonIDNo = legalPersonIDNo;
	}

	public String getAppntOnWorkPeoples() {
		return AppntOnWorkPeoples;
	}

	public void setAppntOnWorkPeoples(String appntOnWorkPeoples) {
		AppntOnWorkPeoples = appntOnWorkPeoples;
	}

	public String getAppntOffWorkPeoples() {
		return AppntOffWorkPeoples;
	}

	public void setAppntOffWorkPeoples(String appntOffWorkPeoples) {
		AppntOffWorkPeoples = appntOffWorkPeoples;
	}

	public String getAppntOtherPeoples() {
		return AppntOtherPeoples;
	}

	public void setAppntOtherPeoples(String appntOtherPeoples) {
		AppntOtherPeoples = appntOtherPeoples;
	}

	public String getClaimBankCode() {
		return ClaimBankCode;
	}

	public void setClaimBankCode(String claimBankCode) {
		ClaimBankCode = claimBankCode;
	}

	public String getClaimAccName() {
		return ClaimAccName;
	}

	public void setClaimAccName(String claimAccName) {
		ClaimAccName = claimAccName;
	}

	public String getClaimBankAccNo() {
		return ClaimBankAccNo;
	}

	public void setClaimBankAccNo(String claimBankAccNo) {
		ClaimBankAccNo = claimBankAccNo;
	}

	public String getBusinessScope() {
		return BusinessScope;
	}

	public void setBusinessScope(String businessScope) {
		BusinessScope = businessScope;
	}

	public String getIDStartDate() {
		return IDStartDate;
	}

	public void setIDStartDate(String iDStartDate) {
		IDStartDate = iDStartDate;
	}

	public String getIDEndDate() {
		return IDEndDate;
	}

	public void setIDEndDate(String iDEndDate) {
		IDEndDate = iDEndDate;
	}

	public String getIDNo() {
		return IDNo;
	}

	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getIDLongEffFlag() {
		return IDLongEffFlag;
	}

	public void setIDLongEffFlag(String iDLongEffFlag) {
		IDLongEffFlag = iDLongEffFlag;
	}
}
