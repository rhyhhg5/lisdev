package com.contract;

import com.contract.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableUI;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LDSysTraceSchema;
import com.sinosoft.lis.tb.NoNameContUI;
import com.sinosoft.lis.tb.ParseGuideInUI;
import com.sinosoft.lis.vschema.LDSysTraceSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 被保人清单导入
 * @author Administrator
 *
 */
public class DealGrpInsuList {
	//错误信息
	private String Content = "";
	//从报文中获取
	//印刷号码  
	private String PrtNo="";
	//页面隐藏字段
	private String GrpContNo="";
	//查询出来的值 不需要报文传的值
	private String BatchNo = "";
	
	private GlobalInput tGI = new GlobalInput(); 
public boolean dealData(VData data){
	System.out.println("*****************************************************被保人清单导入********************************");
	LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
	tGI.Operator = "CENA";
	tGI.ManageCom = lCGrpContInfo.getManageCom();
	//原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
	tGI.ComCode = tGI.ManageCom;
	PrtNo = lCGrpContInfo.getPrtNo();
	if(!dealGrpInsuList()){
		return false;
	}
	return true;
}
	/**
	 * 调用被保人清单导入处理
	 * @return
	 */
public boolean dealGrpInsuList(){
	ExeSQL tExeSQL =new ExeSQL();
	String tGrpContNoSql="select * from LCGrpCont where  PrtNo = '"+PrtNo+"' fetch first 3000 rows only with ur ";
	SSRS tGrpContNoSSRS=tExeSQL.execSQL(tGrpContNoSql);
	String tGrpContNo="";
	if(tGrpContNoSSRS.getMaxNumber()<=0){
		Content="被保人清单操作失败，失败原因:团体信息查询失败!";
		System.out.println(Content);
		return false;
	}else{
		tGrpContNo=	tGrpContNoSSRS.GetText(1, 1);
		System.out.println("团体信息有数据，grpcontno："+tGrpContNo);
	}
	String inforSql="select c.*,getUniteCode(agentcode) from LCGrpCont c where GrpContNo = '" +tGrpContNo+ "' ";
	SSRS inforSqlSSRS=tExeSQL.execSQL(inforSql);
	GrpContNo=inforSqlSSRS.GetText(1, 1);
	String ProposalGrpContNo=inforSqlSSRS.GetText(1, 2);
	  if ("".equals(ProposalGrpContNo)||null==ProposalGrpContNo) {
		  	Content="被保人清单操作失败，失败原因:必须保存合同信息才能进入〔被保人清单〕信息界面！!";
			System.out.println(Content);
			return false;
		  }
	  String tSql = "select 1 from lcgrpcont where prtno='" +PrtNo+ "' and ContPrintType = '3'";
	  SSRS arrResult =tExeSQL.execSQL(tSql);
	  System.out.println("======================="+(arrResult.getMaxNumber()>0)+"只要有一个true就是走的不是无名单");
	  if(arrResult.getMaxNumber()>0){
		    //处理团体保单属性为大团单
			//  dealLGrpInsuredMain();
		  	//window.open("../app/LGrpInsuredMain.jsp?PrtNo="+fm.PrtNo.value+"&GrpContNo="+fm.GrpContNo.value+"&Resource="+Resource+"&LoadFlag="+LoadFlag);
		  	Content="被保人清单操作失败，失败原因:不支持团体保单属性为大团单的处理!";
			System.out.println(Content);
			return false;
			  } else {
		  if(!noBigGrpgetinCheck()){
			  return false;
		  }
		  if(!noBigGrpgetin()){
			  return false;
		  }
		  if(!noBigGrpCalPremCheck()){
			  return false;
		  }
		  if(!noBigGrpCalPrem()){
			  return false;
		  }
	  }
	return true;
}
/**
 * 团体保单属性为非大团单  无名单录入前台校验
 * @return
 */
public boolean noBigGrpgetinCheck(){
	//被保人清单导入 无名单录入前台校验
	// 保单存在实名被保人，则不能录入无名单
    String tStrSql = ""
        + " select 1 "
        + " from LCInsuredList "
        + " where 1 = 1 "
        + " and GrpContNo = '"+GrpContNo+"' "
        + " and (Publicacctype is null or Publicacctype = '') "
        + " and InsuredId not in ('C', 'G') "
        + " union all "
        + " select 1 "
        + " from LCCont lcc "
        + " where 1 = 1 "
        + " and lcc.GrpContNo = '"+GrpContNo+"' "
        + " and lcc.PolType = '0' ";
    ExeSQL tExeSQL=new ExeSQL();
    SSRS tResult =tExeSQL.execSQL(tStrSql);
    System.out.println("======================="+(tResult.getMaxNumber()>0)+"只要有一个true就是走的不是无名单");
    if(tResult.getMaxNumber()>0)
    {
    	Content="无名单录入失败，失败原因:保单已存在实名被保人，因此不能再录入无名单。";
		System.out.println(Content);
		return false;
    }
	return true;
}
/**
 * 被保人清单导入 无名单录入save中的代码调用后台处理
 * @return
 */
public boolean noBigGrpgetin(){
	//接收信息，并作校验处理。
	  //输入参数
	 
	  //输出参数
	  CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
//	  GlobalInput tG = new GlobalInput(); 
	  //人员再定 xu
//	  tG.Operator ="CP3613";
//	  tG.Operator ="CENA";
//	  tG.ComCode="86360500";
	  NoNameContUI tNoNameContUI = new NoNameContUI();
	    TransferData tTransferData = new TransferData();
	    tTransferData.setNameAndValue("GrpContNo",GrpContNo);
	   
	    VData tVData = new VData();
			
	    tVData.add(tTransferData);
			tVData.add(tGI);	
			try
			  {
				System.out.println("开始执行无名单ui");
					tNoNameContUI.submitData(tVData,"");
				}
			  catch(Exception ex)
			  {
			  	ex.printStackTrace();
			    Content = "无名单录入保存失败，原因是:" + tNoNameContUI.mErrors.getFirstError();
			    System.out.println(Content);
			    FlagStr = "Fail";
			    return false;
			  }
		    tError = tNoNameContUI.mErrors;
		    if (!tError.needDealError())
		    {
		    	Content = " 保存成功! ";
		    	FlagStr = "Success";
		    }
		    else                                                                           
		    {
		    	Content = " 无名单录入保存失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
			    System.out.println(Content);
		    	return false;
		    }
	return true;
}
/**
 * 团体保单属性为非大团单 计算保费前台校验
 * @return
 */
public boolean noBigGrpCalPremCheck(){
	//计算保费的操作人员xu  以后定
	String CalPrem0perator="CENA";
	String sql = "select * from lccontplan where grpcontno = '"+GrpContNo+"' and contplancode <>'11'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr =tExeSQL.execSQL(sql);
	if( arr.getMaxNumber()<=0 ) {
		Content="计算保费失败，失败原因:保障计划没有填写，请先返回保单界面录入保障计划信息！";
		System.out.println(Content);
		return false;
		
	}
	String strSql = "select * from ldsystrace where PolNo='" +PrtNo+ "' and PolState=1007 ";
	SSRS arrResult =tExeSQL.execSQL(strSql);
	if(arrResult.getMaxNumber()>0){
		if (!CalPrem0perator.equals(arrResult.GetText(1, 2).trim())) {
			Content="计算保费失败，失败原因:该"+PrtNo+"印刷号的投保单已经被操作员（" + arrResult.GetText(1, 2).trim() + "）在（" +arrResult.GetText(1, 6).trim()+ "）位置锁定！您不能操作，请选其它的印刷号！";
			System.out.println(Content);
			return false;
		}	
	}
// GlobalInput tG = new GlobalInput();	
	 //人员再定 xu
//	  tG.Operator ="ly";
//	  tG.ComCode="86";
//	  tG.ManageCom="86";	
//	  tG.Operator ="CENA";
//	  tG.ComCode="86360500";
//	  tG.ManageCom="86360500";	
	  //锁印刷号
	  LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
	  tLDSysTraceSchema.setPolNo(PrtNo);
	  tLDSysTraceSchema.setCreatePos("保费计算");
	  tLDSysTraceSchema.setPolState("1007");
	  LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
	  inLDSysTraceSet.add(tLDSysTraceSchema);
	  VData VData3 = new VData();
	  VData3.add(tGI);
	  VData3.add(inLDSysTraceSet);
	  
	  LockTableUI LockTableUI1 = new LockTableUI();
	  if (!LockTableUI1.submitData(VData3, "INSERT")) {
	    VData rVData = LockTableUI1.getResult();
	    System.out.println("LockTable Failed! " + (String)rVData.get(0));
	  }
	  else {
	    System.out.println("UnLockTable Succed!");
	  }
	//fm.fmtransact.value="INSERT||DATABASE";
	String BatchNoSql = "select  BatchNo from LCInsuredList where GrpContNo='"
								+GrpContNo+"'  and insuredid not in ('C','G') order by BatchNo";
	//prompt('',strSql);
	SSRS BatchNoarr =tExeSQL.execSQL(BatchNoSql);
	if(BatchNoarr.getMaxNumber()>0)
	{
		BatchNo=BatchNoarr.GetText(1, 1);
	}else{
		BatchNo = "";
	}
	return true;
}
/**
 * 团体保单属性为非大团单 计算保费save中的代码
 * @return
 */
public boolean noBigGrpCalPrem(){
	//接收信息，并作校验处理。
	  //输入参数
	  LCInsuredListSchema tLCInsuredListSchema   = new LCInsuredListSchema();
	  //OLCInuredListUI tOLCInuredListUI   = new OLCInuredListUI();
	  //输出参数
	  CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
//	  GlobalInput tG = new GlobalInput(); 
	  //人员再定 xu
//    tG.Operator ="CP3613";
//	  tG.Operator ="CENA";
//	  tG.ComCode="86360500";
//	  tG.ManageCom="86360500";
	  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	  String transact = "INSERT||DATABASE";
	  String mBatchNo =BatchNo;
	  System.out.println(transact);
	 	if(transact.equals("INSERT||DATABASE"))
	 	  {
	 	  	TransferData tTransferData = new TransferData();
	 	  	ParseGuideInUI tParseGuideInUI = new ParseGuideInUI();
	 	 /* 	if(request.getParameter("EdorType")!=null) {
	 		    tTransferData.setNameAndValue("EdorType",request.getParameter("EdorType"));
	 		    tTransferData.setNameAndValue("EdorValiDate",request.getParameter("EdorValiDate"));
	 		    tTransferData.setNameAndValue("Flag", request.getParameter("Flag"));
	 	  	}*/
	 	    tTransferData.setNameAndValue("FileName", mBatchNo);	
	 	    tTransferData.setNameAndValue("GrpContNo", GrpContNo);	
	 			System.out.println("23234234234qweqwe  "+mBatchNo);
	 		//tTransferData.setNameAndValue("FilePath", ImportPath);
	 			VData tVData = new VData();
	 			
	 		  tVData.add(tTransferData);
	 			tVData.add(tGI);
	 				
	 				System.out.println("12313123123123123123213");
	 				try
	 			  {
	 					tParseGuideInUI.submitData(tVData,transact);
	 				}
	 			  catch(Exception ex)
	 			  {
	 			  	ex.printStackTrace();
	 			    Content = "计算保费保存失败，原因是:" + tParseGuideInUI.mErrors.getFirstError();
	 			    FlagStr = "Fail";
	 			    System.out.println(Content);
	 			    return false;
	 			  }
	 			
	 			    tError = tParseGuideInUI.mErrors;
	 			    if (!tError.needDealError())
	 			    {
	 			    	Content = " 保存成功! ";
	 			    	FlagStr = "Success";
	 			    }
	 			    else                                                                           
	 			    {
	 			    	Content = " 保存失败，原因是:" + tError.getFirstError();
	 			    	FlagStr = "Fail";
	 			    	System.out.println(Content);
		 			    return false;
	 			    }
	 	   }
	return true;
}
public static void main(String[] args) {
	DealGrpInsuList a=new DealGrpInsuList();
	a.dealGrpInsuList();
}
public String getContent() {
	return Content;
}
}
