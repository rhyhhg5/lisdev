package com.contract;

import com.contract.obj.LCGrpContInfo;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
 * 团单整单删除 用于数据回滚
 * @author Administrator
 *
 */
public class DealDeleteGrpCont {
	//错误信息
	private String Content = "";
	//从报文保存节点中获取
	//印刷号码  
	private String PrtNo="";
	public boolean deal(VData data){
		System.out.println("************************ 团单整单删除 用于数据回滚*******************************");
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		PrtNo = lCGrpContInfo.getPrtNo();
		if(!deleteGrpContCheck()){
			return false;
		}
		if(!deleteGrpCont()){
			return false;
		}
		return true;
	}
	/**
	 * 团单整单删除 前台校验
	 * @return
	 */
	public boolean deleteGrpContCheck(){
		String strsql="select 1 from ljtempfee where otherno='"+PrtNo+"' "
				+ "and tempfeeno not in (select tempfeeno from LJAGetTempFee) and enteraccdate is not null";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arr = tExeSQL.execSQL(strsql);
		if(arr.getMaxNumber()>0){
			Content="数据回滚操作失败，失败原因:财务已交费！不能进行新单删除！";
			System.out.println(Content);
			return false;
		}
		return true;
	}
	/**
	 * 团单整单删除 GroupContDeleteChk.jsp调用后台代码
	 * @return
	 */
	public boolean deleteGrpCont(){
		 //输出参数
		  CErrors tError = null;
		  String FlagStr = "Fail";
		  GlobalInput tG = new GlobalInput();
		  //人员再定 xu
		  tG.Operator ="001";
		  tG.ComCode="86";
		  tG.ManageCom="86";
		  String strsql = "select a.ProposalGrpContNo,a.GrpContNo,a.PrtNo,a.GrpName,a.ManageCom from LCGrpCont a where 1=1 and a.PrtNo='"+PrtNo+"'";
		  // 投保单列表
		  ExeSQL tExeSQL=new ExeSQL();
		  SSRS strsqlSSRS=tExeSQL.execSQL(strsql);
		  if(strsqlSSRS.getMaxNumber()>0){
				String tGrpContNo = strsqlSSRS.GetText(1, 2);
				String tPrtNo = PrtNo;
				System.out.println("pfousdfsdfjsldfjlsdfjlsdfjlsdjflsdjflsdkjf"+tPrtNo);
				boolean flag = false;
				System.out.println("ready for UWCHECK GrpContNo: " + tGrpContNo);
				
				LCGrpContSet tLCGrpContSet = null;
				LCGrpContDB tLCGrpContDB = new LCGrpContDB();
				if(!StrTool.cTrim(tGrpContNo).equals("")){
					tLCGrpContDB.setGrpContNo(tGrpContNo);
					tLCGrpContSet = tLCGrpContDB.query();
				}else{
					LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
					tLCGrpContSchema.setPrtNo(tPrtNo);
					tLCGrpContSet = new LCGrpContSet();
					tLCGrpContSet.add(tLCGrpContSchema);
				}
				if (tLCGrpContSet == null)
				{
					Content="数据回滚操作失败，失败原因:集体（投）保单号为" + tGrpContNo + "的合同查找失败！";
					System.out.println(Content);
					return false;
				}
				LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
				try{
					// 准备传输数据 VData
					VData tVData = new VData();
					tVData.add( tLCGrpContSchema );
					tVData.add( tG );
					
					// 数据传输
					ContractGroupContDeleteUI tGroupContDeleteUI = new ContractGroupContDeleteUI();
					if (tGroupContDeleteUI.submitData(tVData,"DELETE") == false)
					{
						FlagStr = "Fail";
					}
					else
						FlagStr = "Succ";
					//如果在Catch中发现异常，则不从错误类中提取错误信息
					if (FlagStr == "Fail")
					{
					    tError = tGroupContDeleteUI.mErrors;
					    System.out.println("tError.getErrorCount:"+tError.getErrorCount());
					    if (!tError.needDealError())
					    {                          
					    	Content = " 整单删除成功! ";
					    	FlagStr = "Succ";
					    }
					    else                                                                           
					    {
					    	Content = "数据回滚中的自动核保失败，原因是:";
					    	int n = tError.getErrorCount();
			    			if (n > 0)
			    			{
						      for(int i = 0;i < n;i++)
						      {
						        //tError = tErrors.getError(i);
						        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
						      }
							}
					    	FlagStr = "Fail";
					    	System.out.println(Content);
					    	return false;
					    }
					}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Content = Content.trim()+".提示：异常终止!";
				return false;
			}
		  }else{
			  ExeSQL tExeSQL1 =new ExeSQL();
			  tExeSQL1.execUpdateSQL("delete lwmission where MissionProp1='"+PrtNo+"'");
			  tExeSQL1.execUpdateSQL("delete ldsystrace where PolNo='"+PrtNo+"'");
		  }
	
		return true;
	}
	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public static void main(String[] args) {
		DealDeleteGrpCont a=new DealDeleteGrpCont();
		//a.deal();
	}

}
