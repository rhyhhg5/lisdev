package com.contract;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class ContractGroupContDeleteBL {
    
    private VData mInputData = new VData();

    
    private VData mResult = new VData();

    
    private MMap mMap = new MMap();

    
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    
    public CErrors mErrors = new CErrors();

    
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    
    private boolean delScan = false;

    public ContractGroupContDeleteBL() {
    }

    
    public boolean submitData(VData cInputData, String cOperate) {
        
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        
        if (this.getInputData() == false) {
            return false;
        }

        System.out.println("---getInputData successful---");

        if (this.dealData() == false) {
            return false;
        }

        System.out.println("---dealdata successful---");

        
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mResult, cOperate)) {
            
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            return false;
        }

        return true;
    }

    
    private boolean getInputData() {
        
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null) {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        
        mLCGrpContSchema.setSchema((LCGrpContSchema) mInputData.
                                   getObjectByObjectName("LCGrpContSchema", 0));

        if (mLCGrpContSchema == null) {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = null;
        if (!StrTool.cTrim(mLCGrpContSchema.getGrpContNo()).equals("")) {
            tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLCGrpContSet = tLCGrpContDB.query();
        } else if (this.mLCGrpContSchema.getPrtNo() != null) {
            delScan = true;
        }

        if (tLCGrpContSet != null && tLCGrpContSet.size() > 0) {
            mLCGrpContSchema.setSchema(tLCGrpContSet.get(1));
        }
        System.out.println("完成数据解析 结果为 " + mLCGrpContSchema.getPrtNo() + "  " +
                           mLCGrpContSchema.getGrpContNo());
        return true;
    }

    
    private boolean dealData() {
        String tPrtNo = mLCGrpContSchema.getPrtNo();
        
        if(StrTool.cTrim(mLCGrpContSchema.getGrpContNo()).equals("")){
            System.out.println("单独删除投保书扫描件!!!!!");
                
               mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                        tPrtNo + "' and subtype in ('TB02','TB07'))", "DELETE");
               mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                        tPrtNo + "'and subtype in ('TB02','TB07'))", "DELETE");
               mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                        "'and subtype in ('TB02','TB07')", "DELETE");
               
               
               mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                       tPrtNo + "' and subtype='TB08')", "DELETE");
               mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                       tPrtNo + "'and subtype='TB08')", "DELETE");
               mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                       "'and subtype='TB08'", "DELETE");
               
               mMap.put("delete from lwmission where MissionProp1 = '" +
                        tPrtNo + "' and activityid in ('0000002098','0000002099') ",
                        "DELETE");
               
               
               mMap.put("delete from lwmission where MissionProp1 = '" +
                       tPrtNo + "' and activityid = '0000012001' ",
                       "DELETE");
                   return true;
        }
        String tGrpContNo = mLCGrpContSchema.getGrpContNo();

         tPrtNo = mLCGrpContSchema.getPrtNo();
        mLCDelPolLog.setOtherNo(StrTool.cTrim(tGrpContNo));
        mLCDelPolLog.setOtherNoType("0");
        mLCDelPolLog.setPrtNo(mLCGrpContSchema.getPrtNo());

        if ("1".equals(mLCGrpContSchema.getAppFlag())) {
            mLCDelPolLog.setIsPolFlag("1");
        } else {
            mLCDelPolLog.setIsPolFlag("0");
        }

        mLCDelPolLog.setOperator(mOperator);
        mLCDelPolLog.setManageCom(mManageCom);
        mLCDelPolLog.setMakeDate(theCurrentDate);
        mLCDelPolLog.setMakeTime(theCurrentTime);
        mLCDelPolLog.setModifyDate(theCurrentDate);
        mLCDelPolLog.setModifyTime(theCurrentTime);
        if (!this.delScan) {

            mMap.put("insert into LOBInsuredRelated (select * from LCInsuredRelated where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCInsuredRelated where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBDuty (select * from LCDuty where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put(
                    "delete from LCDuty where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                    tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBPrem_1 (select * from LCPrem_1 where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCPrem_1 where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBPremToAcc (select * from LCPremToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCPremToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBGetToAcc (select * from LCGetToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCGetToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put(
                    "insert into LOBPol (select * from LCPol where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPol where GrpContNo = '" + tGrpContNo + "'",
                     "DELETE");

            mMap.put("insert into LOBBnf (select * from LCBnf where ContNo in (select ContNo from LCCont where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCBnf where ContNo in (select ContNo from LCCont where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put(
                    "insert into LOBCont (select * from LCCont where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCont where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGrpPol (select * from LCGrpPol where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpPol where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBPrem (select * from LCPrem where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPrem where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGet (select * from LCGet where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGet where GrpContNo = '" + tGrpContNo + "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBInsureAccFee (select * from LCInsureAccFee where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccFee where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put("insert into LOBInsureAccClassFee (select * from LCInsureAccClassFee where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccClassFee where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBInsureAccClass (select * from LCInsureAccClass where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccClass where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put(
                    "insert into LOBInsureAcc (select * from LCInsureAcc where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAcc where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put("insert into LOBInsureAccTrace (select * from LCInsureAccTrace where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccTrace where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put(
                    "insert into LOBInsured (select * from LCInsured where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsured where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put("insert into LOBCustomerImpart (select * from LCCustomerImpart where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCustomerImpart where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBCustomerImpartParams (select * from LCCustomerImpartParams where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCustomerImpartParams where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");








            mMap.put(
                    "insert into LOBGrpAppnt (select * from LCGrpAppnt where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpAppnt where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGrpFee (select * from LCGrpFee where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpFee where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            



            mMap.put("delete from LCGrpInterest where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGrpFeeParam (select * from LCGrpFeeParam where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpFeeParam where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBContPlan (select * from LCContPlan where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlan where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGeneral (select * from LCGeneral where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGeneral where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put("insert into LOBGeneralToRisk (select * from LCGeneralToRisk where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGeneralToRisk where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBContPlanRisk (select * from LCContPlanRisk where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanRisk where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put("insert into LOBContPlanDutyParam (select * from LCContPlanDutyParam where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanDutyParam where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBContPlanFactory (select * from LCContPlanFactory where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanFactory where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBContPlanParam (select * from LCContPlanParam where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanParam where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBUWError (select * from LCUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWError where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBUWSub (select * from LCUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBUWMaster (select * from LCUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWMaster where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBCUWError (select * from LCCUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCUWError where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBCUWSub (select * from LCCUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBCUWMaster (select * from LCCUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCUWMaster where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGUWError (select * from LCGUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGUWError where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGUWSub (select * from LCGUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGUWMaster (select * from LCGUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGUWMaster where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGCUWError (select * from LCGCUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGCUWError where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGCUWSub (select * from LCGCUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGCUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGCUWMaster (select * from LCGCUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGCUWMaster where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBUWReport (select * from LCUWReport where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWReport where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBNotePad (select * from LCNotePad where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCNotePad where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBRReport (select * from LCRReport where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCRReport where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");




            mMap.put("delete from LCIssuePol where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGrpIssuePol (select * from LCGrpIssuePol where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpIssuePol where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBPENoticeItem (select * from LCPENoticeItem where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPENoticeItem where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBPENotice (select * from LCPENotice where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPENotice where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBCGrpSpec (select * from LCCGrpSpec where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCGrpSpec where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");







 
            mMap.put(
                    "insert into lobAscriptionrulefactory (select * from lcAscriptionrulefactory where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from lcAscriptionrulefactory where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            
            mMap.put(
                    "insert into Lobascriptionruleparams (select * from Lcascriptionruleparams where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from Lcascriptionruleparams where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            
            mMap.put(
                    "insert into lobriskztfee (select * from lcriskztfee where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from lcriskztfee where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            
            mMap.put("delete from lcgrpposition where GrpContNo = '" + tGrpContNo +
                    "'",
                    "DELETE");
            



            mMap.put("delete from LCSpec where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            
            mMap.put("delete from LCInsuredList where GrpContNo = '" + tGrpContNo + "'", "DELETE");
            mMap.put("delete from LCGrpBalPlan where GrpContNo = '" + tGrpContNo + "'", "DELETE");
            
            
            mMap.put("delete from loprtmanager where otherno = '" + mLCGrpContSchema.getProposalGrpContNo() +
                     "'",
                     "DELETE");
            

            mMap.put("delete from LCGrpPayplan where ProposalGrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() +
                     "'",
                     "DELETE");

            mMap.put("delete from LCGrpPayDue where ProposalGrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() +
                     "'",
                     "DELETE");

            mMap.put("delete from LCExtend where prtno = '" + tPrtNo +
                     "'",
                     "DELETE");
        }
        

        mMap.put("delete from LCGrpContSub where prtno = '" + tPrtNo + "'","DELETE");
        
        
        mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                 tPrtNo + "' and subtype in ('TB02','TB07'))", "DELETE");
        mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                 tPrtNo + "'and subtype in ('TB02','TB07'))", "DELETE");
        mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                 "'and subtype in ('TB02','TB07')", "DELETE");
        
        
        mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                tPrtNo + "' and subtype='TB08')", "DELETE");
        mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                tPrtNo + "'and subtype='TB08')", "DELETE");
        mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                "'and subtype='TB08'", "DELETE");
        
        
        mMap.put("delete from LDSysTrace where PolNo = '" + tPrtNo + "'", "DELETE");
        
        
        mMap.put("delete from LCGrpImportLog where GrpContNo = '" + tGrpContNo + "'", "DELETE");

        mMap.put("delete from lcnation where grpcontno='" + tGrpContNo + "'", "DELETE");

        
        String strSql = "select activityid from lwmission where (MissionProp1 = '" +
                tPrtNo + "' or MissionProp2 = '" + tPrtNo + "')";
        
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(strSql);
        System.out.println("tSSRS.getMaxRow()=" + tSSRS.getMaxRow());
        if (tSSRS.getMaxRow() != 0) {
            for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
                String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
                String activityid = tSSRS.GetText(row, 1);
                System.out.println("row==" + row);
                System.out.println("tSerielNo==" + tSerielNo);
                System.out.println("activityid==" + activityid);
                if (activityid.equals("0000001099") ||
                    activityid.equals("0000001098") ||
                    activityid.equals("0000001100") ||
                    activityid.equals("0000002099") ||
                    activityid.equals("0000002098") ||
                    activityid.equals("0000002004") ||
                    activityid.equals("0000012001")) {
                    mMap.put("insert into LBmission (select '" + tSerielNo +
                             "',lwmission.* from lwmission where MissionProp1 = '" +
                             tPrtNo + "' and activityid = '" + activityid +
                             "')", "INSERT");
                    mMap.put("delete from lwmission where MissionProp1 = '" +
                             tPrtNo + "' and activityid = '" + activityid + "'",
                             "DELETE");
                }
                if (activityid.equals("0000001001") ||
                    activityid.equals("0000002001") ||
                    activityid.equals("0000002004") ||
                    activityid.equals("0000002006") ||
                    activityid.equals("0000002005") ||
                    activityid.equals("0000012002")) {
                    mMap.put("insert into LBmission (select '" + tSerielNo +
                             "', lwmission.* from lwmission where MissionProp2 = '" +
                             tPrtNo + "' and activityid = '" + activityid +
                             "')", "INSERT");
                    mMap.put("delete from lwmission where MissionProp2 = '" +
                             tPrtNo + "' and activityid = '" + activityid + "'",
                             "DELETE");
                }
            }
        }
        
        
        MMap tTmpMap = null;
        tTmpMap = delCoInsuranceInfo(tGrpContNo);
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        

        mMap.put(
                "insert into LOBGrpCont (select * from LCGrpCont where GrpContNo = '" +
                tGrpContNo + "')", "INSERT");
        mMap.put("delete from LCGrpCont where GrpContNo = '" + tGrpContNo + "'",
                 "DELETE");
        if (!this.delScan) {
            mMap.put(mLCDelPolLog, "INSERT");
        }
        
        if("2".equals(mLCGrpContSchema.getCardFlag())){
        	mMap.put("update licertifyinsured set grpcontno = null,proposalgrpcontno = null,prtno = null," 
        			 + "state = '01', " 
        			 + "ModifyDate ='" + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                     + "' where prtno = '" + mLCGrpContSchema.getPrtNo() + "'" ,"UPDATE");
        	mMap.put("update licertify set grpcontno = null,proposalgrpcontno = null,prtno = null," 
       			 	+ "state = '01', " 
       			 	+ "ModifyDate ='" + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                    + "' where prtno = '" + mLCGrpContSchema.getPrtNo() + "'" ,"UPDATE");
            mMap.put("update libcertify set grpcontno = null,proposalgrpcontno = null,prtno = null," 
                    + "state = '01', " 
                    + "ModifyDate ='" + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                    + "' where prtno = '" + mLCGrpContSchema.getPrtNo() + "'" ,"UPDATE");
        }
        
        mMap.put("delete from LCGrpShareAmnt where GrpContNo = '" + tGrpContNo +
                "'",
                "DELETE");
        
        mMap.put("delete from LCGrpRiskShareAmnt where GrpContNo = '" + tGrpContNo +
                "'",
                "DELETE");
        
        mMap.put("delete from LCContPlanDutyDefGrade where GrpContNo = '" + tGrpContNo +
                "'",
                "DELETE");
        return true;
    }

    
    private void prepareOutputData() {
        
        mResult.clear();
        mResult.add(mMap);
    }
    
    
    private MMap delCoInsuranceInfo(String cGrpContNo)
    {
        MMap tMMap = null;

        ContractCoInsuranceGrpContBL tCoInsuranceGrpContBL = new ContractCoInsuranceGrpContBL();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpContNo", cGrpContNo);
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        tMMap = tCoInsuranceGrpContBL.getSubmitMap(tVData, "Delete");

        if (tMMap == null)
        {
            buildError("delCoInsuranceInfo", "共保要素处理失败。");
            return null;
        }

        return tMMap;
    }

    
    public VData getResult() {
        return mResult;
    }
    
    
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
