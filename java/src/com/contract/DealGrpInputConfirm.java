package com.contract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Element;

import com.contract.obj.LCCoInsuranceParaminfoList;
import com.contract.obj.LCGrpContInfo;
import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCCoInsuranceParamSchema;
import com.sinosoft.lis.tb.CoInsuranceGrpContUL;
import com.sinosoft.lis.vschema.LCCoInsuranceParamSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;

/**
 * 录入完毕
 * @author Administrator
 *
 */
public class DealGrpInputConfirm {
//错误信息
private String Content = "";
//从报文保存节点中获取
//印刷号码  
private String PrtNo="";
// 销售渠道
private String SaleChnl="";
//市场类型 
private String MarketType="";
//大项目标识
private String BigProjectFlag="";
//投保人名称（汇交人名称）
private String GrpName="";
//保险责任生效日 
private String CValiDate="";
//集团交叉销售渠道
private String Crs_SaleChnl="";
//页面隐藏字段
private String GrpContNo="";
private String ProposalGrpContNo="";
private String MissionProp20="";
private String WorkFlowFlag="";
//查询出来来的数据
// 业务员代码
private String AgentCode="";
//业务员组别
private String AgentGroup="";
//管理机构
private String ManageCom="";
private GlobalInput tGI = new GlobalInput(); 
public boolean dealData(VData data){
	System.out.println("***************************录入完毕*****************************************");
	LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
	tGI.Operator = "CENA";
	tGI.ManageCom = lCGrpContInfo.getManageCom();
	//原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
	tGI.ComCode = tGI.ManageCom;
	
	PrtNo = lCGrpContInfo.getPrtNo();
	SaleChnl=lCGrpContInfo.getSaleChnl();
	MarketType=lCGrpContInfo.getMarketType();
	BigProjectFlag=lCGrpContInfo.getBigProjectFlag();
	GrpName=lCGrpContInfo.getGrpName();
	CValiDate=lCGrpContInfo.getCValiDate();
	Crs_SaleChnl=lCGrpContInfo.getCrs_SaleChnl();
	String CoInsuranceFlag=lCGrpContInfo.getCoInsuranceFlag();
	//录入共保要素信息 
	if("1".equals(CoInsuranceFlag)){
		LCCoInsuranceParamSet mLCCoInsuranceParamSet = new LCCoInsuranceParamSet();
		LCCoInsuranceParaminfoList tlCCoInsuranceParaminfoList = (LCCoInsuranceParaminfoList) data.getObjectByObjectName("LCCoInsuranceParaminfoList", 0);
		List list = tlCCoInsuranceParaminfoList.getList();
		double m=0.0;
		if(list.size()>0){
			 for(int i=0;i<list.size();i++ ){
				 LCCoInsuranceParamSchema tLCCoInsuranceParamSchema = new LCCoInsuranceParamSchema();
				 Element ele = (Element) list.get(i);
				 if(ele.getChildText("AgentCom")==null ||"".equals(ele.getChildText("AgentCom"))){
					 	Content="录入共保要素信息失败，失败原因:共保机构代码不可为空！";
						System.out.println(Content);
						return false; 
				 }
				 if(ele.getChildText("UpAgentCom")==null ||"".equals(ele.getChildText("UpAgentCom"))){
					 	Content="录入共保要素信息失败，失败原因:负担比例不可为空！";
						System.out.println(Content);
						return false; 
				 }
				 tLCCoInsuranceParamSchema.setAgentCom(ele.getChildText("AgentCom"));
				 tLCCoInsuranceParamSchema.setAgentComName(ele.getChildText("Name"));
				 tLCCoInsuranceParamSchema.setRate(ele.getChildText("UpAgentCom"));
				 mLCCoInsuranceParamSet.add(tLCCoInsuranceParamSchema);
				 System.out.println("**************************************************"+ele.getChildText("AgentCom")+"=="+ele.getChildText("Name")+"=="+ele.getChildText("UpAgentCom"));
				 if(!"".equals(ele.getChildText("UpAgentCom"))&& null!=ele.getChildText("UpAgentCom")){
					 m=m+Double.valueOf(ele.getChildText("UpAgentCom"));
					 System.out.println("++++++++++++++++ "+m);
				 }
			 }
			 if(m>1){
				 Content="录入共保要素信息失败，失败原因: 所有负担比例累加之和不能超过1。";
				 System.out.println(Content);
				 return false;
			 }
				String tSQL = "select managecom,agentcom,agentcode,salechnl,grpcontno,proposalgrpcontno,agentcode ,agentgroup from lcgrpcont where prtno = '"+PrtNo+"'";
				ExeSQL tExeSQL=new ExeSQL();
				SSRS arr =tExeSQL.execSQL(tSQL);
				if(arr.getMaxNumber()<=0){
					Content="录入共保要素信息失败，失败原因:获取保单数据失败！";
					System.out.println(Content);
					return false;
				}
				GrpContNo=arr.GetText(1, 5);
				ProposalGrpContNo=arr.GetText(1, 6);
			// fm.fmtransact.value = "Create";
			 System.out.println("start CoInsuranceParamSave.jsp...");
			    
			    CErrors tError = null;
			    String FlagStr = "";
//			    GlobalInput tG = new GlobalInput();
				//人员再定 xu
//			    tG.Operator ="ly";
//			    tG.ComCode="86";
//			    tG.ManageCom="86";
//			    tG.Operator ="CENA";
//			    tG.ComCode="86360500";
//			    tG.ManageCom="86360500";
			 // 获取保单关键信息。
			    String tPrtNo =PrtNo;
			    String tGrpContNo = GrpContNo;
			    String tProposalGrpContNo = ProposalGrpContNo;
			  // --------------------------
			  LCCoInsuranceParamSet tLCCoInsuranceParamSet = new LCCoInsuranceParamSet();
			  for(int j = 1; j <= mLCCoInsuranceParamSet.size(); j++)
			    {
			        LCCoInsuranceParamSchema tTmpLCCoInsuranceParamSchema = new LCCoInsuranceParamSchema();
			        
			        tTmpLCCoInsuranceParamSchema.setGrpContNo(tGrpContNo);
			        tTmpLCCoInsuranceParamSchema.setProposalGrpContNo(tProposalGrpContNo);
			        tTmpLCCoInsuranceParamSchema.setPrtNo(tPrtNo);
			        
			        tTmpLCCoInsuranceParamSchema.setAgentCom(mLCCoInsuranceParamSet.get(j).getAgentCom());
			        tTmpLCCoInsuranceParamSchema.setAgentComName(mLCCoInsuranceParamSet.get(j).getAgentComName());
			        tTmpLCCoInsuranceParamSchema.setRate(mLCCoInsuranceParamSet.get(j).getRate());
			        
			        tLCCoInsuranceParamSet.add(tTmpLCCoInsuranceParamSchema);
			    }
			  TransferData tTransferData = new TransferData();
			    tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
			    
			    VData tVData = new VData();
			    tVData.add(tGI);
			    tVData.add(tLCCoInsuranceParamSet);
			    tVData.add(tTransferData);
			    String tOperate = "Create";
			    
			    CoInsuranceGrpContUL tCoInsuranceGrpContUL = new CoInsuranceGrpContUL();
			    try
			    {        
			        if (!tCoInsuranceGrpContUL.submitData(tVData, tOperate))
			        {
			            Content = " 录入共保要素信息处理失败! 原因是: " + tCoInsuranceGrpContUL.mErrors.getLastError();
			            FlagStr = "Fail";
			            System.out.println(Content);
			            return false;
			        }
			        else
			        {
			            Content = " 处理成功！";
			            FlagStr = "Succ";
			        }
			    }
			    catch(Exception ex)
			    {
			        Content = "录入共保要素信息处理失败，原因是:" + ex.toString();
			        FlagStr = "Fail";
			        System.out.println(Content);
		            return false;
			    }
			    System.out.println("end CoInsuranceParamSave.jsp...");
		}else{
			Content="录入共保要素信息失败，失败原因: 共保要素信息为空！";
			System.out.println(Content);
			return false;
		}
		
	}
	if(!deal()){
		return false;
	}
	return true;
}
public boolean deal(){
	if(!GrpInputConfirmCheck()){
		return false;
	}
	if(!GrpInputConfirm()){
		return false;
	}
	return true;
}
/**
 * 
 *	录入完毕  前台校验
 * @return
 */
public boolean GrpInputConfirmCheck(){
	  if(null==PrtNo||"".equals(PrtNo)){
		  Content="录入完毕操作失败，失败原因:印刷号码为空！ ";
			System.out.println(Content);
			return false;
	  }
	  
	  
	 String wFlag="1";
	  if(!checkSaleChnlInfo()){
	    	return false;
	    }
	  
	  //**************gy by 2017-11-15
//	  if(!checkMarkettypeSalechnl()){
//	    	return false;
//	    }
	  if(!checkRiskSalechnl()){
	    	return false;
	    }
	  //校验保单类型与险种
	if(!checkRiskContPrintType()){
		return false;
	} 
	if(!checkCoInsuranceParams())
	    {
	        return false;
	    }
	 //by gzh 20110413  若存在没有算费的被保人，则阻断。
    if(!checkAllCalPrem()){
    	return false;
    }
   
    //校验集团交叉销售相互代理业务的销售渠道与中介机构是否符合描述
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    // *****by  gy --2017-11-15
//    //缴费频次payintv=-1时，市场类型必须是 2,4,5,6,8,10,11
//    if(!checkPayIntv()){
//    	return false;
//    }
 
  //****************by  gy ---2017-11-25
    //*******by lrx data:20190214开放续期首期校验
    if(!checkFirstPayPlan()){
    	return false;
    }
  
    //校验建工险
    if(!check590301()){
    	return false;
    }
    
    //校验健享全球表定
    if(!checkB162001()){
    	return false;
    }
    
    /*//反洗钱
    if(!checkFXQ()){
    	return false;
    }*/
 /*   
    //联系人联系电话对应不同投保人
    if(!checkPhone1()){
    	return false;
    }*/
    
    //大团单校验
 /*   if(!checkLGrp()){
    	return false;
    }*/
    
    //校验简易团单的缴费方式
    if(!checkBriefGrp()){
    	return false;
    }
    
    //校验简易团单是否与险种匹配
    if(!checkBriefGrpRisk()){
    	return false;
    }
    
    //校验结余返还---社保项目要素
 // GY-by  2018-03-13

    String tGYSQL = "select salechnl,riskcode from lcgrppol where prtno = '"
				+ PrtNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS arr = tExeSQL.execSQL(tGYSQL);
		if (arr.getMaxNumber() <= 0) {
			Content = "团体复核通过操作失败，失败原因:获取保单险种数据失败！";
			System.out.println(Content);
			return false;
		}
		for (int i = 1; i <= arr.getMaxRow(); i++) {
			String tSaleChnl = arr.GetText(i, 1);
			String tRiskCode = arr.GetText(i, 2);
			if (!"690201".equals(tRiskCode)) {
				if (!checkBalance()) {
					return false;
				}
			}
		}
  
    
    //商团大项目必录校验
    if(!checkBigProject()){
    	return false;
    }
    
    //被保险人数校验
    if(!checkPeople()){
    	return false;
    }
    String mWFlag ="1";
    String tStr= "	select * from lwmission where 1=1 "
                +" and lwmission.processid = '0000000004'"
                +" and lwmission.activityid = '0000002001'"
                +" and lwmission.missionprop1 = '"+ProposalGrpContNo+"'";
  	ExeSQL tExeSQL1=new ExeSQL();
  	SSRS strQueryResult1 = tExeSQL1.execSQL(tStr);
      if (strQueryResult1.getMaxNumber()>0) {
    	  	Content="录入完毕操作失败，失败原因:该团单合同已经做过保存！";
  			System.out.println(Content);
  			return false;
      }

      String strsql = "";
      strsql="select InsuredNo from LCInsured where PrtNo ='"+PrtNo+"'"	;
  	  SSRS strQueryResult2 = tExeSQL1.execSQL(strsql);
      if (strQueryResult2.getMaxNumber()<=0) {
    	  	Content="录入完毕操作失败，失败原因:请添加被保人";
			System.out.println(Content);
			return false;
      }
      
      String strsql2 = "";
      strsql2="select PolNo from LcPol where PrtNo ='"+PrtNo+"'";
      SSRS strQueryResult3 = tExeSQL1.execSQL(strsql2);
      if (strQueryResult3.getMaxNumber()<=0) {
    	  	Content="录入完毕操作失败，失败原因:请添加险种信息";
			System.out.println(Content);
			return false;
      }

      if("".equals(ProposalGrpContNo)) {
    	  	Content="录入完毕操作失败，失败原因:团单合同信息未保存,不容许您进行 [录入完毕] 确认！";
			System.out.println(Content);
			return false;
      }
      
      // 如果是特需险种，进行公共帐户是否录入的判断。
      String tTmpGrpPrtNo = PrtNo;
      if(!checkPubAccData(tTmpGrpPrtNo))
      {
          return false;
      }
      
      String strsql3="select count(1) from LCGrpIssuePol where grpcontno in (select grpcontno from lcgrpcont where PrtNo ='"+PrtNo+"') and state<>'5' ";
      SSRS arr20=tExeSQL1.execSQL(strsql3);
   //   alert(arr20);
      if(arr20.getMaxNumber()>0)
      {
      	if(!"0".equals(arr20.GetText(1, 1))){
      	MissionProp20="N";
//      	alert(fm.all('MissionProp20').value);
      	}else{
      	MissionProp20="";
//      	alert(fm.all('MissionProp20').value);
  			}
      }
//          return;
      String ScanFlag="0";
      if("0".equals(ScanFlag)) {
        WorkFlowFlag = "0000002098";
      }
      /*if(ScanFlag=="1") {
        fm.WorkFlowFlag.value = "0000002099";
      }*/
    //对约定缴费计划信息进行校验
    if(!checkPayPlan()){
    	return false;
    }
	return true;
}
/**
 * 录入完毕 GrpInputConfirm.jsp save中的代码
 * @return
 */
public boolean GrpInputConfirm(){
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr="";
	String tAction = "";
	String tOperate = "";
	String wFlag = "";
//	GlobalInput tG = new GlobalInput();
	//人员再定 xu
//    tG.Operator ="CP3613";
//	tG.Operator ="CENA";
//    tG.ComCode="86360500";
//    tG.ManageCom="86360500";
    String strSQL = "select * "
			+" from( "				     
			+" select lwmission.missionprop1,lwmission.missionprop3,lwmission.missionprop2, "
			+" lwmission.missionprop4,lwmission.missionid,lwmission.submissionid, "
			+" (select case when count(1)<>0 then '有问题件' else '没有问题件' end from LCGrpIssuePol where grpcontno in (select grpcontno from lcgrpcont where PrtNo =lwmission.missionprop1) and state<>'5' ) "
			+" from lwmission where 1=1 "
			+" and activityid = '0000002098' "
			+" and processid = '0000000004' "
			+" and missionprop5 = '0'  and missionprop1='"+PrtNo+"' "
			+ " order by lwmission.missionprop1  ) as X";
    ExeSQL tExeSQL=new ExeSQL();
    SSRS infoSSRS=tExeSQL.execSQL(strSQL);
    String MissionID=infoSSRS.GetText(1, 5);
    String SubMissionID=infoSSRS.GetText(1, 6);
    System.out.println("MissionID==="+MissionID);
    System.out.println("SubMissionID==="+SubMissionID);
    VData tVData = new VData();
    //工作流操作型别，根据此值检索活动ID，取出服务类执行具体业务逻辑
    wFlag = WorkFlowFlag;
    TransferData mTransferData = new TransferData();
    mTransferData.setNameAndValue("ProposalGrpContNo", ProposalGrpContNo);
    mTransferData.setNameAndValue("PrtNo", PrtNo);
    mTransferData.setNameAndValue("SaleChnl",SaleChnl);
    mTransferData.setNameAndValue("AgentCode",AgentCode);    
    mTransferData.setNameAndValue("AgentGroup",AgentGroup);
    mTransferData.setNameAndValue("ManageCom", ManageCom);
    mTransferData.setNameAndValue("GrpName", GrpName);
    mTransferData.setNameAndValue("CValiDate",CValiDate);
    mTransferData.setNameAndValue("Operator",tGI.Operator);
    mTransferData.setNameAndValue("MakeDate",PubFun.getCurrentDate());
    mTransferData.setNameAndValue("MissionID",MissionID);  
    mTransferData.setNameAndValue("SubMissionID",SubMissionID);
    mTransferData.setNameAndValue("MissionProp20",MissionProp20);
    
    System.out.println(PrtNo);
    tVData.add(mTransferData);
    tVData.add(tGI);
    System.out.println("wFlag="+wFlag);
    System.out.println("-------------------start workflow---------------------");
    GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
  	//江苏团险校验
    boolean tJSFlag = false;
    {
    	  String tSaleChnl = SaleChnl;
    	  String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
    			  	  + "and code = '"+tSaleChnl+"'";
    	  SSRS tSSRS = new ExeSQL().execSQL(tSql);
    	  String tSubManageCom = ManageCom.substring(0,4);
    	  if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
    		  ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
        	  TransferData tJSTransferData = new TransferData();
        	  tJSTransferData.setNameAndValue("ContNo",GrpContNo);
        	  VData tJSVData = new VData();
        	  tJSVData.add(tJSTransferData);
        	  if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
        		  Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
        		  FlagStr = "Fail";
        		  tJSFlag = true;
        	  }
    	  }
      }
    if(!tJSFlag){
	  	if( !tGrpTbWorkFlowUI.submitData( tVData, wFlag ) ) {
	      	Content = " 处理失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getError(0).errorMessage;
	      	FlagStr = "Fail";
	      	return false;
	  	}else {
	      	Content = " 处理成功！";
	      	FlagStr = "Succ";
	  	}
    }else{
    	System.out.println(Content);
    	return false;
    }
 	System.out.println("-------------------end workflow---------------------");
	return true;
}

public boolean checkSaleChnlInfo(){
	String tSQL = "select managecom,agentcom,agentcode,salechnl,grpcontno,proposalgrpcontno,agentcode ,agentgroup from lcgrpcont where prtno = '"+PrtNo+"'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr =tExeSQL.execSQL(tSQL);
	if(arr.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取保单数据失败！";
		System.out.println(Content);
		return false;
	}
	String tManageCom = arr.GetText(1, 1);
	String tAgentCom = arr.GetText(1,2);
	String tAgentCode = arr.GetText(1, 3);
	String tSaleChnl = arr.GetText(1, 4);
	GrpContNo=arr.GetText(1, 5);
	ProposalGrpContNo=arr.GetText(1, 6);
	AgentCode=arr.GetText(1, 7);
	AgentGroup=arr.GetText(1, 8);
	ManageCom=tManageCom;
	String tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	SSRS arrCode =tExeSQL.execSQL(tSQLCode);
	if(arrCode.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:业务员与管理机构不匹配！";
		System.out.println(Content);
		return false;
	}
	if(tSaleChnl == "02" || tSaleChnl == "03"){
		String agentCodeSql = " select 1 from LAAgent a where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = '2' and a.BranchType2 in ('01','02') "
	                 + " union all "
					 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					 + " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
	    SSRS arrAgentCode =tExeSQL.execSQL(agentCodeSql) ;
	    if(arrAgentCode.getMaxNumber()<=0){
	    	Content="录入完毕操作失败，失败原因:业务员和销售渠道不匹配！";
			System.out.println(Content);
			return false;
	    }
	}else{
		String agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                     + " union all "
					 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					 + " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
	    SSRS arrAgentCode =tExeSQL.execSQL(agentCodeSql);
	    if(arrAgentCode.getMaxNumber()<=0){
	    	Content="录入完毕操作失败，失败原因:业务员和销售渠道不匹配！";
			System.out.println(Content);
			return false;
	    }
	}
	if(tAgentCom != "" && tAgentCom != null && tAgentCom != "null"){
		String tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
		SSRS arrCom =tExeSQL.execSQL(tSQLCom);
		if(arrCom.getMaxNumber()<=0){
			Content="录入完毕操作失败，失败原因:中介机构与管理机构不匹配！";
			System.out.println(Content);
			return false;
		}
		String tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
		SSRS arrComCode = tExeSQL.execSQL(tSQLComCode);
		if(arrComCode.getMaxNumber()<=0){
			Content="录入完毕操作失败，失败原因:业务员与中介机构不匹配！";
			System.out.println(Content);
			return false;
		}
	}
	return true;
}
public boolean checkMarkettypeSalechnl(){
	String tSQL = "select salechnl,markettype from lcgrpcont where prtno = '"+PrtNo+"'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS  arr =tExeSQL.execSQL(tSQL);
	if(arr.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取保单数据失败！";
		System.out.println(Content);
		return false;
	}
	String tSaleChnl = arr.GetText(1, 1);
	String tMarketType = arr.GetText(1, 2);
	String tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+tMarketType+"' and code1 = '"+tSaleChnl+"' ";
	SSRS arrCheck = tExeSQL.execSQL(tcheckSQL);
	if(arrCheck.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:市场类型和销售渠道不匹配，请核查！";
		System.out.println(Content);
		return false;
	}
	return true;
}
public boolean checkRiskSalechnl(){

	String tSQL = "select salechnl,riskcode from lcgrppol where prtno = '"+PrtNo+"'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr = tExeSQL.execSQL(tSQL);
	if(arr.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取保单险种数据失败！";
		System.out.println(Content);
		return false;
	}
	for(int i=1;i<=arr.getMaxRow();i++){
		String tSaleChnl = arr.GetText(i, 1);
		String tRiskCode = arr.GetText(i, 2);
		String tcheckSQL = "select codename from ldcode where codetype = 'unitesalechnlrisk' and code = '"+tRiskCode+"' ";
		SSRS arrCheck = tExeSQL.execSQL(tcheckSQL);
		if(arrCheck.getMaxNumber()<=0){
			if( "16".equals(tSaleChnl)){
				Content="录入完毕操作失败，失败原因:险种"+tRiskCode+"为非社保险种，与销售渠道"+tSaleChnl+"不匹配！";
				System.out.println(Content);
				return false;
			}
		}else{
			if(!"all".equals(arrCheck.GetText(1, 1)) && !tSaleChnl.equals(arrCheck.GetText(1, 1))){
				Content="录入完毕操作失败，失败原因:险种"+tRiskCode+"与销售渠道"+tSaleChnl+"不匹配！";
				System.out.println(Content);
				return false;
			}
		}
		//add by zjd 特需险校验赔付顺序为必录
		String triskclaimnumsql=" select riskcode from lmriskapp where risktype3='7' and riskcode='"+tRiskCode+"'  ";
	    SSRS arrclaim = tExeSQL.execSQL(triskclaimnumsql);
	    if(arrclaim.getMaxNumber()>0){
	    	String tlcgrpfeesql=" select 1 from lcgrpfee lgf,lcgrppol lgp  where lgf.grppolno=lgp.grppolno and lgp.prtno='"+PrtNo+"' and lgp.riskcode='"+tRiskCode+"' and ClaimNum is not null ";
	    	SSRS arrclaimnum = tExeSQL.execSQL(tlcgrpfeesql);
	    	if(arrclaimnum.getMaxNumber()<=0){
	    		Content="录入完毕操作失败，失败原因:险种"+tRiskCode+"为特需险，赔付顺序必录！\n 请到账户定义界面定义管理下录入赔付顺序！";
				System.out.println(Content);
				return false;
	    	}
	    }
	    //add by zjd 补充工伤团体失能损失保险B款 伤残等级给付比例
	    String tgrpSQL = "select grpcontno from lcgrpcont where prtno = '"+PrtNo+"'";
		SSRS grparr =tExeSQL.execSQL(tgrpSQL);
	    if("460301".equals(tRiskCode)){
	    	String tgraderatesql=" Select Distinct a.contplancode,a.Dutycode From Lccontplandutyparam a Where a.riskcode='460301' and grpcontno ='"+grparr.GetText(1, 1)+"' and ContPlanCode not in ('00','11') ";
	    	SSRS tgraderate = tExeSQL.execSQL(tgraderatesql);
	    	if(tgraderate.getMaxNumber()>0){
	    		for(int j=1;j<=tgraderate.getMaxRow();j++){
	    			String tratechksql=" select 1 from LCContPlanDutyDefGrade where grpcontno='"+grparr.GetText(1, 1)+"' and contplancode='"+tgraderate.GetText(1, 1)+"' and dutycode='"+tgraderate.GetText(1, 2)+"' and GetRate > 0 ";
	    			SSRS tratechk = tExeSQL.execSQL(tratechksql);
	    			if(tratechk.getMaxNumber()<=0){
	    				Content="录入完毕操作失败，失败原因:保障计划"+tgraderate.GetText(1, 1)+"下责任"+tgraderate.GetText(1, 2)+" 未录入伤残等级对应的给付比例。\n 请到保障计划下的制定伤残等级给付比例中录入给付比例！";
	    				System.out.println(Content);
	    				return false;
	    			}
	    		}
	    	}
	    }
	}
	return true;
}
public boolean checkRiskContPrintType(){

	String tSQL = "select salechnl,riskcode from lcgrppol where prtno = '"+PrtNo+"'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr = tExeSQL.execSQL(tSQL);
	if(arr.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取保单险种数据失败！";
		System.out.println(Content);
		return false;
	}
	String tContPrintType = "";
	String tContPrintTypeSQL = "select ContPrintType,ManageCom from lcgrpcont where prtno = '"+PrtNo+"' ";
	SSRS tContPrintTypeArr = tExeSQL.execSQL(tContPrintTypeSQL);
	if(tContPrintTypeArr.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取保单数据失败！";
		System.out.println(Content);
		return false;
	}else{
		tContPrintType = tContPrintTypeArr.GetText(1, 1);
	}
	for(int i=1;i<=arr.getMaxRow();i++){
		String tSaleChnl = arr.GetText(i, 1);
		String tRiskCode = arr.GetText(i, 2);
		String grpBriefSql = "select code,codename,'Risk',comcode from ldcode where codetype = 'grphjrisk' and code ='" + tRiskCode +"'  and othersign = '1' ";
		SSRS grpBriefArr = tExeSQL.execSQL(grpBriefSql);
		if("5".equals(tContPrintType)){
			if(grpBriefArr.getMaxNumber()<=0){
				Content="录入完毕操作失败，失败原因:汇交件不支持险种"+tRiskCode+"，请修改保单属性或险种代码";
				System.out.println(Content);
				return false;
			}
		}
		if(!"5".equals(tContPrintType)){
			if(grpBriefArr.getMaxNumber()>0){
				if(!"3".equals(grpBriefArr.GetText(1, 4))){
					Content="录入完毕操作失败，失败原因:非汇交件不支持险种"+tRiskCode+"，请修改保单属性或险种代码";
					System.out.println(Content);
					return false;
				}
				
			}
		}
	}
	return true;
}
/**
 * 校验共保保单的相关要素是否齐全。
 */
public boolean checkCoInsuranceParams()
{
    String tStrSql = ""
        + " select 1 "
        + " from LCGrpCont lgc "
        + " left join LCIGrpCont lcigc on lgc.GrpContNo = lcigc.GrpContNo "
        + " left join LCCoInsuranceParam lcip on lcip.GrpContNo = lcigc.GrpContNo "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" +GrpContNo+ "' "
        + " and lgc.CoInsuranceFlag = '1' "
        + " and lcigc.GrpContNo is null "
        + " and lcip.GrpContNo is null "
        ;
    ExeSQL tExeSQL=new ExeSQL();
    SSRS arr = tExeSQL.execSQL(tStrSql);
    if(arr.getMaxNumber()>0)
    {
    	Content="录入完毕操作失败，失败原因:该单为共保保单，但共保要素信息不全，请进行确认。";
		System.out.println(Content);
		return false;
    }
    
    return true;
}
//by  gzh 20110413  团单被保人上载后，若有错误信息，可选中修改，修改功能只修改list表记录，不再调用算费功能
//所以需在录入完毕及复核完毕时，校验是否所有上载被保人都计算过保费，避免修改数据后，未计算保费就录入完毕的问题

public boolean checkAllCalPrem(){
  String strSQL = "select insuredname,sex,birthday,idtype,idno from lcinsuredlist where grpcontno = '"+GrpContNo+"' and state = '0'";  
  ExeSQL tExeSQL=new ExeSQL();
  SSRS arrResult = tExeSQL.execSQL(strSQL);
  if (arrResult.getMaxNumber()>0 )
  {
  	for(int i=1;i<=arrResult.getMaxRow();i++){
  		String strSQL1 = "select * from lcinsured where grpcontno = '+mGrpContNo+'"
  		            + " and name = '"+arrResult.GetText(i, 1)+"' "
  		            + " and sex = '"+arrResult.GetText(i, 2)+"' "
  		            + " and birthday = '"+arrResult.GetText(i, 3)+"' "
  		            + "and idtype = '"+arrResult.GetText(i, 4)+"' "
  		            + "and idno = '"+arrResult.GetText(i, 5)+"'";
  		SSRS arrResult1 = tExeSQL.execSQL(strSQL1);
  		if(arrResult1.getMaxNumber()<=0) {
  			Content="录入完毕操作失败，失败原因:该保单存在未计算保费的被保人，请先进行计算保费操作！";
  			System.out.println(Content);
  			return false;
  		}
  	}
  }
  return true;
}
//校验标准团险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   by 赵庆涛 2016-06-28
public boolean checkCrsBussSaleChnl()
{
	if( null!= Crs_SaleChnl && !"".equals(Crs_SaleChnl))
	{
		String strSQL = "select 1 from lcgrpcont lgc where 1 = 1"  
			+ " and lgc.Crs_SaleChnl is not null " 
			+ " and lgc.Crs_BussType = '01' " 
			+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			+ " and lgc.PrtNo = '" +PrtNo+ "' ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrResult = tExeSQL.execSQL(strSQL);
      if (arrResult.getMaxNumber()>0)
      {
    	  	Content="录入完毕操作失败，失败原因:选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！";
			System.out.println(Content);
			return false;
      }
	}
	return true;
}
public boolean checkPayIntv()
{
	String tSql = "select 1 from lcgrpcont where prtno = '"+PrtNo+"' and payintv = -1 and markettype not in (select code from ldcode where codetype = 'markettypeyd') ";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr = tExeSQL.execSQL(tSql);
	if(arr.getMaxNumber()>0){
		Content="录入完毕操作失败，失败原因:缴费频次与市场类型不匹配，请核查！";
		System.out.println(Content);
		return false;
	}
	return true;
}
public boolean checkFirstPayPlan()
{
	String tSql = "select 1 from lcgrpcont where prtno = '"+PrtNo+"' and payintv = -1 ";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr = tExeSQL.execSQL(tSql);
	if(arr.getMaxNumber()>0){
		String tStrSql = "select contplancode, prem from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' and plancode = '1' order by contplancode " ;
		SSRS arrPayPlan = tExeSQL.execSQL(tStrSql);
		if(arrPayPlan.getMaxNumber()<=0){
			Content="录入完毕操作失败，失败原因:该单缴费频次为约定缴费，请录入约定缴费计划信息！";
			System.out.println(Content);
			return false;
		}
		String tPolSql = "select contplancode,sum(prem) from lcpol where prtno = '"+PrtNo+"' group by contplancode ";
		SSRS arrPol = tExeSQL.execSQL(tPolSql);
		if(arrPol.getMaxNumber()<=0){
			Content="录入完毕操作失败，失败原因:获取险种信息失败！";
			System.out.println(Content);
			return false;
		}
		for(int i=1;i<=arrPayPlan.getMaxRow();i++){
			for(int j=1;j<=arrPol.getMaxRow();j++){
				if(arrPayPlan.GetText(i, 1).equals(arrPol.GetText(j, 1)) && !Float.valueOf(arrPayPlan.GetText(i, 2)).equals(Float.valueOf(arrPol.GetText(j, 2))))
				{
					Content="录入完毕操作失败，失败原因:保障计划["+arrPayPlan.GetText(i, 1)+"]的首期保费["+arrPol.GetText(j, 2)+"]与约定缴费计划的首期保费["+arrPayPlan.GetText(i, 2)+"]不符！";
					System.out.println(Content);
					return false;
				}
			}
		}
	}
	return true;
}
//by gzh 20120910 
//建工险590301保费计算方式为按被保险人人数计算时
//1、保险期间不可以大于1年
//2、必须实名投保
public boolean check590301(){
	String tSql = " select 1 from LCContPlanDutyParam "
			 + " where ProposalGrpContNo = '"+ProposalGrpContNo+"' "
			 + " and riskcode = '590301' "
			 + " and calfactor = 'StandbyFlag1' "
			 + " and calfactorvalue = '3' ";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr = tExeSQL.execSQL(tSql);
	if(arr.getMaxNumber()>0){
		//保险期间不可以大于1年1400365686 
		String tSqlGrpCont = " select Cvalidate,Cinvalidate from LCGrpCont "
			 + " where ProposalGrpContNo = '"+ProposalGrpContNo+"' ";
		SSRS arrGrpCont = tExeSQL.execSQL(tSqlGrpCont);
		if(arrGrpCont.getMaxNumber()>0){
			if(null!=arrGrpCont.GetText(1, 2)&&!"".equals(arrGrpCont.GetText(1, 2))&&null!=arrGrpCont.GetText(1, 1)&&!"".equals(arrGrpCont.GetText(1, 1)) ){
			    String Cvalidate = arrGrpCont.GetText(1, 1);
			    String Cinvalidate=arrGrpCont.GetText(1, 2);
		        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		        Date tCvalidate;
		        Date tCinvalidate;
		        long diff = 0;
				try {
					tCvalidate =  df.parse(Cvalidate);
			        tCinvalidate = df.parse(Cinvalidate);
			        diff = (tCinvalidate.getTime() - tCvalidate.getTime())/(1000*3600*24);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Content=e.getMessage();
				   	return false;
				}
		        if(diff<365){
		        	Content="录入完毕操作失败，失败原因:承保产品新版建工险590301，保费计算方式为按被保人方式计算，保险期间应小于等于1年！";
					System.out.println(Content);
					return false;
		        }
			}
		}else{
			Content="录入完毕操作失败，失败原因:获取保单保险期间失败！";
			System.out.println(Content);
			return false;
		}
		//必须实名投保
		String tSqlCont = " select 1 from lccont where prtno = '"+PrtNo+"' and poltype = '1'";
		SSRS arrCont = tExeSQL.execSQL(tSqlCont);
		if(arrCont.getMaxNumber()>0){
			Content="录入完毕操作失败，失败原因:承保产品新版建工险590301，保费计算方式为按被保人方式计算，必须实名承保！ ";
			System.out.println(Content);
			return false;
		}
	}
	return true;
}
//by gzh 20121031
//健享全球表定费率必须实名承保
public boolean checkB162001(){
	 String tSQL= " select 1 from LCContPlanDutyParam where ProposalGrpContNo='"
				+ProposalGrpContNo+ "' and contplancode='11' and riskcode='162001' " 
         	+ " and calfactor = 'CalRule' and calfactorvalue = '0'" ;
	 ExeSQL tExeSQL=new ExeSQL();
	 SSRS tArr=tExeSQL.execSQL(tSQL);
		if(tArr.getMaxNumber()>0){
			//必须实名投保
			String tSqlCont = " select 1 from lccont where prtno = '"+PrtNo+"' and poltype = '1'";
			SSRS arrCont = tExeSQL.execSQL(tSqlCont);
			if(arrCont.getMaxNumber()>0){
				Content="录入完毕操作失败，失败原因:承保产品健享全球162001，保费计算方式为表定费率，必须实名承保！ ";
				System.out.println(Content);
				return false;
			}
		}
	return true;
}
public boolean checkBriefGrp(){
	String tContPrintTypeSQL = "select payintv from lcgrpcont where prtno = '"+PrtNo+"' and ContPrintType = '4' ";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS tContPrintTypeArr = tExeSQL.execSQL(tContPrintTypeSQL);
	if(tContPrintTypeArr.getMaxNumber()>0){
		if(!"0".equals(tContPrintTypeArr.GetText(1, 1))){
			Content="录入完毕操作失败，失败原因:简易团单的缴费方式必须为趸交！ ";
			System.out.println(Content);
			return false;
		}
	}
	return true;
}
public boolean checkBriefGrpRisk(){
	String tFlagSQL = "select grpcontno from lcgrpcont where prtno = '"+PrtNo+"' and ContPrintType = '4' ";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS tFlagSQLArr = tExeSQL.execSQL(tFlagSQL);
	if(tFlagSQLArr.getMaxNumber()>0){
		String tRiskSQL = "select 1 from lcgrppol where prtno = '"+PrtNo+"' ";
		SSRS tRiskArr = tExeSQL.execSQL(tRiskSQL);
		if(tRiskArr.getMaxNumber()>0){
			String tRiskFlagSQL = "select 1 from lcgrppol lgp where lgp.prtno = '"+PrtNo+"' "
							 + " and not exists (select 1 from ldcode where codetype = 'grpbriefrisk' and code = lgp.riskcode and codename = substr(lgp.managecom,1,4) ) ";
			SSRS tRiskFlagArr = tExeSQL.execSQL(tRiskFlagSQL);
			if(tRiskFlagArr.getMaxRow()>0){
				Content="录入完毕操作失败，失败原因:该单为简易团单，简易团单不支持已添加的险种，请修改保单属性或险种代码！";
				System.out.println(Content);
				return false;
			}
			String tContPlanSQL = "select distinct contplancode,riskcode from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and contplancode != '11' ";
			SSRS tContPlanArr = tExeSQL.execSQL(tContPlanSQL);
			if(tContPlanArr.getMaxNumber()>0){
				for(int i=1;i<=tContPlanArr.getMaxRow();i++){
					String tContPlanCode = tContPlanArr.GetText(i, 1);
					String tRiskCode = tContPlanArr.GetText(i, 2);
					String tInsuPlanCodeSQL = "select distinct calfactorvalue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and contplancode = '"+tContPlanCode+"' and riskcode = '"+tRiskCode+"' and calfactor like 'InsuPlanCode%' ";
					SSRS tInsuPlanCodeArr = tExeSQL.execSQL(tInsuPlanCodeSQL);
					if(tInsuPlanCodeArr.getMaxNumber()<=0){
						Content="录入完毕操作失败，失败原因:该单为简易团单，保障计划"+tContPlanCode+"中,险种["+tRiskCode+"]必须选择方案编码，请修改保障计划！";
						System.out.println(Content);
						return false;
					}
				}
			}else{
				Content="录入完毕操作失败，失败原因:获取保障计划信息失败！";
				System.out.println(Content);
				return false;
			}
		}else{
			Content="录入完毕操作失败，失败原因:获取险种信息失败！";
			System.out.println(Content);
			return false;
		}
		
		String tCalRuleSQL = "select distinct CalFactorValue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and CalFactor = 'CalRule' ";
		SSRS tCalRuleArr = tExeSQL.execSQL(tCalRuleSQL);
		if(tCalRuleArr.getMaxNumber()>0){
			if(tCalRuleArr.getMaxRow() >1){
				Content="录入完毕操作失败，失败原因:该单为简易团单，保费计算方式必须为[3-约定费率]！";
				System.out.println(Content);
				return false;
			}
			if(!"3".equals(tCalRuleArr.GetText(1, 1))){
				Content="录入完毕操作失败，失败原因:该单为简易团单，保费计算方式必须为[3-约定费率]！";
				System.out.println(Content);
				return false;
			}
		}else{
			Content="录入完毕操作失败，失败原因:获取保障计划算费要素失败！";
			System.out.println(Content);
			return false;
		}
		
		String tInsuPlanSQL = "select distinct contplancode,CalFactorValue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and CalFactor like 'InsuPlanCode%' ";
		SSRS tInsuPlanSArr = tExeSQL.execSQL(tInsuPlanSQL);
		if(tInsuPlanSArr.getMaxNumber()>0){
			for(int i=1;i<=tInsuPlanSArr.getMaxRow();i++){
				String tAllInsuRiskSQL = "select code1 from ldcode1 where codetype = 'grpbrieinsuplan' and code = '"+tInsuPlanSArr.GetText(i, 2)+"' ";
				SSRS tAllInsuRiskArr = tExeSQL.execSQL(tAllInsuRiskSQL);
				if(tAllInsuRiskArr.getMaxNumber()<=0){
					Content="录入完毕操作失败，失败原因:简易团单，获取方案对应的险种信息失败！";
					System.out.println(Content);
					return false;
				}
				String tAllRiskSQL = "select distinct riskcode from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and contplancode = '"+tInsuPlanSArr.GetText(i, 1)+"' ";
				SSRS tAllRiskArr =tExeSQL.execSQL(tAllRiskSQL);
				if(tAllRiskArr.getMaxNumber()<=0){
					Content="录入完毕操作失败，失败原因:简易团单，获取保障计划对应的险种信息失败！";
					System.out.println(Content);
					return false;
				}
				for(int m=1;m<=tAllInsuRiskArr.getMaxRow();m++){
					String tFlag = "0";
					for(int n=1;n<=tAllRiskArr.getMaxRow();n++){
						if(tAllInsuRiskArr.GetText(m, 1).equals(tAllRiskArr.GetText(n, 1))){
							tFlag = "1";
							break;
						}
					}
					if(!"1".equals(tFlag)){
						Content="录入完毕操作失败，失败原因:简易团单，方案编码["+tInsuPlanSArr.GetText(i, 2)+"]对应的险种["+tAllInsuRiskArr.GetText(m, 1)+"]在保障计划["+tInsuPlanSArr.GetText(i, 1)+"]中不存在，请核查！";
						System.out.println(Content);
						return false;
					}
				}
			}
		}else{
			Content="录入完毕操作失败，失败原因:该单为简易团单，必须选择方案编码，请修改保障计划！";
			System.out.println(Content);
			return false;
		}
	}
	return true;
}
public boolean checkBalance(){

	String tSqlCode = "select markettype from LCGrpCont where prtno='" +PrtNo+ "'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arrCodeResult = tExeSQL.execSQL(tSqlCode);
	if(arrCodeResult.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取保单市场类型失败！";
		System.out.println(Content);
		return false;
	}
	if("".equals(arrCodeResult.GetText(1, 1))||null==arrCodeResult.GetText(1, 1)){
		Content="录入完毕操作失败，失败原因:获取保单市场类型失败！";
		System.out.println(Content);
		return false;
	}
	String tSQL = "select ProjectName,BalanceTermFlag,StopLine,SharedLine,RevertantLine from LCGrpContSub where prtno = '"+PrtNo+"' ";
	SSRS arrBalanceResult = tExeSQL.execSQL(tSQL);
		if( !"1".equals(arrCodeResult.GetText(1, 1)) && !"9".equals(arrCodeResult.GetText(1, 1))){
			if(arrBalanceResult.getMaxNumber()<=0){
				Content="录入完毕操作失败，失败原因:根据保单市场类型，需录入社保项目要素信息！";
				System.out.println(Content);
				return false;
			}
			if(null==arrBalanceResult.GetText(1, 1) || "".equals(arrBalanceResult.GetText(1, 1))
			 ||null==arrBalanceResult.GetText(1, 2) || "".equals(arrBalanceResult.GetText(1, 2))
			 ||null==arrBalanceResult.GetText(1, 3) || "".equals(arrBalanceResult.GetText(1, 3))
			 ||null==arrBalanceResult.GetText(1, 4) || "".equals(arrBalanceResult.GetText(1, 4))
			 ||null==arrBalanceResult.GetText(1, 5)|| "".equals(arrBalanceResult.GetText(1, 5))){
				Content="录入完毕操作失败，失败原因:根据保单市场类型，需录入社保项目要素信息！";
				System.out.println(Content);
				return false;
			}
		}else{
			if(arrBalanceResult.getMaxNumber()>0){
				Content="录入完毕操作失败，失败原因:根据保单市场类型，不需录入社保项目要素信息！";
				System.out.println(Content);
				return false;
			}
		}	
	
	return true;
}
public boolean checkBigProject(){
	String tMarketType = MarketType;
	String tBigProjectFlag =BigProjectFlag;
	String tPrtNo = PrtNo;
	String tSQL = "select 1 from lcbigprojectcont where prtno='"+tPrtNo+"'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arr = tExeSQL.execSQL(tSQL);
	if("1".equals(tMarketType) ||"9".equals(tMarketType)){
		if("1".equals(tBigProjectFlag)){
			if(arr.getMaxNumber()<=0){
				Content="录入完毕操作失败，失败原因:印刷号为："+tPrtNo+"的保单必须选择或录入一个大项目！";
				System.out.println(Content);
				return false;
			}
		}
	}
	return true;
}
public boolean checkPeople(){
	String tPrtNo = PrtNo;
	String tSqlCount="select peoples2 from LCGrpCont where PrtNo='" + tPrtNo + "'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS arrCount = tExeSQL.execSQL(tSqlCount);
	if(Integer.valueOf(arrCount.GetText(1, 1))<3){
		Content="录入完毕操作失败，失败原因:被保险人数应大于等于3！";
		System.out.println(Content);
		return false;
	}
	return true;
}

/**
 * 判断特需险种是否已经录入了帐户信息。
 */
public boolean checkPubAccData(String tGrpPrtNo)
{
    boolean tBResult = true;
    String tStrSql = " select distinct lgp.grppolno, lgp.riskcode "
        + " from lcgrppol lgp "
        + " where prtno = '" + tGrpPrtNo + "' "
        + " and exists (select 1 from LMRisktoAcc lmrta where lmrta.riskcode = lgp.riskcode) "
        + " and not exists (select 1 from LCGrpFee lgf where lgf.riskcode = lgp.riskcode and lgf.grppolno = lgp.grppolno) ";
    ExeSQL tExeSQL=new ExeSQL();
	SSRS arr = tExeSQL.execSQL(tStrSql);
    if(arr.getMaxNumber()>0)
    {
        String tComment = "";
        for(int i=1;i<=arr.getMaxRow();i++)
        {
            if((!"".equals(arr.GetText(i, 1))&&null!=arr.GetText(i, 1))||(!"".equals(arr.GetText(i, 2))&&null!=arr.GetText(i, 2)))
            {
                tComment += "[" + arr.GetText(i, 2)+ "]";
            }
        }
        tComment += "并未添加相应帐户信息。";
        Content="录入完毕操作失败，失败原因:"+tComment;
		System.out.println(Content);
        tBResult = false; 
    }
    return tBResult;
}
//by gzh 增加约定缴费计划首期和总保费的校验,若缴费频次非约定缴费，则不可存在约定缴费计划
public boolean checkPayPlan()
{
	String tPayIntvSql = "select 1 from lcgrpcont where prtno = '"+PrtNo+"' and payintv = -1 ";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS tPayIntvArr = tExeSQL.execSQL(tPayIntvSql);
	if(tPayIntvArr.getMaxNumber()>0){//约定缴费
		String tPremScopeSql = "select PremScope from lcgrpcont where prtno = '"+PrtNo+"' ";
		SSRS tPremScopeArr = tExeSQL.execSQL(tPremScopeSql);
		String sumPrem = tPremScopeArr.GetText(1, 1);
		if(null==sumPrem ||"".equals(sumPrem) ||"0".equals(sumPrem)){
			Content="录入完毕操作失败，失败原因:缴费频次为约定缴费，必须录入保费合计！";
			System.out.println(Content);
			return false;
		}
		if(!checkPayAndCin()){
			return false;
		}
		String tGetPolPremSql = "select sum(prem) from lcpol where prtno = '"+PrtNo+"' ";
		SSRS tGetPolPremArr = tExeSQL.execSQL(tGetPolPremSql);
		if(tGetPolPremArr.getMaxNumber()<=0){
			Content="录入完毕操作失败，失败原因:查询首期保费失败！";
			System.out.println(Content);
			return false;
		}
		String tGetPlanPremSql = "select sum(prem) from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' ";
		SSRS tGetPlanPremArr = tExeSQL.execSQL(tGetPlanPremSql);
		if(tGetPlanPremArr.getMaxNumber()<=0){
			Content="录入完毕操作失败，失败原因:查询约定缴费计划总保费失败！";
			System.out.println(Content);
			return false;
		}
		//********gy 2017-11-15
		//********by lrx data 20190214 开放保费合计与缴费计划总金额相等校验
		if(!sumPrem.equals(tGetPlanPremArr.GetText(1, 1))){
			Content="录入完毕操作失败，失败原因:缴费资料->保费合计”与约定缴费计划中录入的总金额不符,请核对!";
			System.out.println(Content);
			return false;
		}
 }else{
 		String tPayPlanSql = "select 1 from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"'";
		SSRS tPayPlanArr = tExeSQL.execSQL(tPayPlanSql);
		if(tPayPlanArr.getMaxNumber()>0){
			String tGetPanIntvSQL = "select payintv,(select codename from ldcode where codetype = 'grppayintv' and code = char(lgc.payintv)) from lcgrpcont lgc where lgc.prtno = '"+PrtNo+"' ";
			SSRS tGetPanIntvArr = tExeSQL.execSQL(tGetPanIntvSQL);
			if(tGetPanIntvArr.getMaxNumber()<0){
				Content="录入完毕操作失败，失败原因:获取缴费频次名称失败  ！";
				System.out.println(Content);
				return false;
			}
			Content="录入完毕操作失败，失败原因:缴费方式为["+tGetPanIntvArr.GetText(1, 2)+"],请先删除约定缴费计划！";
			System.out.println(Content);
			return false;
		}
 }
 return true;
}
public boolean checkPayAndCin()
{
	String tCinSql = "select cinvalidate from lcgrpcont where prtno = '"+PrtNo+"'";
	ExeSQL tExeSQL=new ExeSQL();
	SSRS tCinArr = tExeSQL.execSQL(tCinSql);
	if(tCinArr.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取保单终止日期失败！";
		System.out.println(Content);
		return false;
	}
	if(null==tCinArr.GetText(1, 1)||"".equals(tCinArr.GetText(1, 1))){
		Content="录入完毕操作失败，失败原因:获取保单终止日期失败！";
		System.out.println(Content);
		return false;
	}
	String tSql = "select plancode,paytodate from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' group by plancode,paytodate ";
	SSRS arr = tExeSQL.execSQL(tSql);
	if(arr.getMaxNumber()<=0){
		Content="录入完毕操作失败，失败原因:获取约定缴费时间失败！";
		System.out.println(Content);
		return false;
	}
	for(int i=1;i<=arr.getMaxRow();i++){
		    String paytodate = arr.GetText(i, 2);
		    String cinvalidate = tCinArr.GetText(1, 1);
	        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	        Date tpaytodate;
	        Date tcinvalidate;
	        long diff = 0;
			try {
				tpaytodate =  df.parse(paytodate);
				tcinvalidate = df.parse(cinvalidate);
		        diff = tcinvalidate.getTime() - tpaytodate.getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Content=e.getMessage();
			   	return false;
			}
	        if(diff<0){
	        	Content="录入完毕操作失败，失败原因:第"+arr.GetText(i, 1)+"期的约定缴费时间["+arr.GetText(i, 2)+"]晚于保单终止日期["+tCinArr.GetText(1, 1)+"]，请核查！";
	    		System.out.println(Content);
	    		return false;
	        }
	}
	return true;
}
public String getContent() {
	return Content;
}
public static void main(String[] args) {
	DealGrpInputConfirm a=new DealGrpInputConfirm();
	a.deal();
}
}
