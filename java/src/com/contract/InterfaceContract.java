package com.contract;

/**
 * 新契约接口
 * 
 * @author LiHao
 *
 */
public class InterfaceContract {
	/**
	 * 接口的直接调用类
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public String service(String xml) throws Exception {
		System.out.println("***********开始service接口的直接调用类*********");
		System.out.println("接受的报文：" + xml);
		DealContract dealContract = new DealContract();
		try {
			// 业务逻辑，后端处理
			dealContract.deal(xml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String returnXML = dealContract.getReturnxml();
		System.out.println("返回的报文：" + returnXML);
		return returnXML;
		// 返回报文
	}
}
