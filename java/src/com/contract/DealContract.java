package com.contract;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.contract.obj.ContPlanInfoList;
import com.contract.obj.LCCoInsuranceParaminfoList;
import com.contract.obj.LCExtendInfo;
import com.contract.obj.LCGCUWMasterInfo;
import com.contract.obj.LCGrpAddressInfo;
import com.contract.obj.LCGrpAppntInfo;
import com.contract.obj.LCGrpContInfo;
import com.contract.obj.LCGrpContSubInfo;
import com.contract.obj.MessageHead;
import com.contract.obj.PayPlanInfoList;
import com.contract.obj.PubAccInfoList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 新契约：得到数据校验数据 创建返回报文
 * 
 * @author LiHao
 * 
 */
public class DealContract {
	// 错误信息
	private String errorValue;
	private VData data = new VData();
	private boolean rollbackFlag = false;

	public String getErrorValue() {
		return errorValue;
	}

	String SendOperator;
	String MsgType;
	String BranchCode;
	String BatchNo;
	String PrtNo;
	String returnxml;
	String RiskCode;
	String mUrl = "";
	String aOutXmlStr = "";
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String fileName = sdf.format(date);

	public String getReturnxml() {
		return returnxml;
	}

	/**
	 * 业务逻辑处理
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public boolean deal(String xml) throws Exception {
		try {
			if (null == xml || "".equals(xml)) {
				errorValue = "接收的报文为空";
				return false;
			}
			// 解析报文
			if (!getdata(xml)) {
				return false;
			}

			// 校验数据 锁表
			if (!cheakdata()) {
				return false;
			}
			DealContractDetail dealDetail = new DealContractDetail();
			if (!dealDetail.deal(data)) {
				errorValue = dealDetail.getErrorValue();
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("=============================程序错误信息:"
					+ errorValue);
		} finally {
			returnxml = CreateXML.createXML(data, errorValue, xml);
			// 保存返回报文
			getUrlName("2");
			String outFilePath = mUrl + "/" + fileName + MsgType + ".xml";
			FileWriter fw;
			try {
				fw = new FileWriter(outFilePath);
				fw.write(returnxml);
				fw.flush();
				fw.close();
			} catch (IOException e) {
				errorValue = e.toString();
				e.printStackTrace();
			}
			if (!"".equals(errorValue) && null != errorValue) {

				// 错误日志
				ExeSQL ExeSQL = new ExeSQL();
				String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
						+ SendOperator
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','"
						+ MsgType
						+ "','"
						+ BranchCode
						+ "','"
						+ BatchNo
						+ "','" + PrtNo + "')";
				ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
				String insertErrorInsertLogSql = "insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
						+ SendOperator
						+ "','"
						+ xml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+ returnxml
						+ "','"
						+ errorValue
						+ "','"
						+ MsgType
						+ "','" + BranchCode + "','" + BatchNo + "')";
				ExeSQL.execUpdateSQL(insertErrorInsertLogSql);
			} else {
				// 日志
				ExeSQL ExeSQL = new ExeSQL();
				String insertClaimInsertLogSql = "insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
						+ SendOperator
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'1','"
						+ MsgType
						+ "','"
						+ BranchCode
						+ "','"
						+ BatchNo
						+ "','" + PrtNo + "')";
				ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
			}
		}

		return true;
	}

	/**
	 * 
	 * 解析报文，封装数据
	 * 
	 * @param xml
	 * @return
	 */
	public boolean getdata(String xml) {
		System.out.println("***********开始getdata解析报文*********");
		try {
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();

			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			BranchCode = elehead.getChildText("BranchCode");
			SendOperator = "CENA";// elehead.getChildText("SendOperator");
			MsgType = elehead.getChildText("MsgType");

			// 保存请求报文
			getUrlName("1");
			String InFilePath = mUrl + "/" + fileName + MsgType + ".xml";
			System.out.println(InFilePath + "=============================================路径");
			FileWriter fw;
			try {
				fw = new FileWriter(InFilePath);
				fw.write(xml);
				fw.flush();
				fw.close();
			} catch (IOException e) {
				errorValue = e.toString();
				e.printStackTrace();
			}

			MessageHead head = new MessageHead();
			head.setBatchNo(BatchNo);
			head.setSendDate(SendDate);
			head.setSendTime(SendTime);
			head.setBranchCode(BranchCode);
			head.setSendOperator(SendOperator);
			head.setMsgType(MsgType);
			data.add(head);

			Element msgLCGrpContInfo = rootElement.getChild("LCGrpContInfo");
			Element eleLCGrpContInfo = msgLCGrpContInfo.getChild("Item");

			LCGrpContInfo lCGrpContInfo = new LCGrpContInfo();
			PrtNo = eleLCGrpContInfo.getChildText("PrtNo");
			lCGrpContInfo.setPrtNo(eleLCGrpContInfo.getChildText("PrtNo"));// 印刷号码
			lCGrpContInfo.setManageCom(eleLCGrpContInfo
					.getChildText("ManageCom"));// 管理机构
			lCGrpContInfo
					.setSaleChnl(eleLCGrpContInfo.getChildText("SaleChnl"));// 销售渠道
			// lCGrpContInfo.setSaleChnlDetail(eleLCGrpContInfo.getChildText("SaleChnlDetail"));//
			// 销售渠道明细VARCHAR(2)
			lCGrpContInfo
					.setAgentCom(eleLCGrpContInfo.getChildText("AgentCom"));// 代理机构VARCHAR(20)
			lCGrpContInfo.setAgentType(eleLCGrpContInfo
					.getChildText("AgentType"));// 代理机构内部分类VARCHAR(20)
			lCGrpContInfo.setAgentCode(eleLCGrpContInfo
					.getChildText("AgentCode"));// 代理人编码VARCHAR(10)
			lCGrpContInfo.setAgentGroup(eleLCGrpContInfo
					.getChildText("AgentGroup"));// 代理人组别VARCHAR(12)
			lCGrpContInfo.setAgentCode1(eleLCGrpContInfo
					.getChildText("AgentCode1"));// 联合代理人代码VARCHAR(10)
			lCGrpContInfo.setAgentSaleCode(eleLCGrpContInfo
					.getChildText("AgentSaleCode"));// 代理销售人员编码
			lCGrpContInfo.setGrpSpec(eleLCGrpContInfo.getChildText("GrpSpec"));// 集体特约VARCHAR(255)

			lCGrpContInfo.setPayMode(eleLCGrpContInfo.getChildText("PayMode"));// 交费方式
			lCGrpContInfo.setGrpName(eleLCGrpContInfo.getChildText("GrpName"));// 单位名称VARCHAR(150)
			lCGrpContInfo.setGetFlag(eleLCGrpContInfo.getChildText("GetFlag"));// 付款方式VARCHAR(1)

			// System.out.println("***********************************gy缴费频次："+eleLCGrpContInfo.getChildText("GrpContPayIntv"));
			String GrpContPayIntv = ""; // 缴费频次校验 除了0 默认为-1
			if (!"0".equals(eleLCGrpContInfo.getChildText("GrpContPayIntv"))) {
				GrpContPayIntv = "-1";
				lCGrpContInfo.setGrpContPayIntv(GrpContPayIntv);
			} else {
				lCGrpContInfo.setGrpContPayIntv("0");// 交费间隔SMALLINT
			}
			// System.out.println("*****************************gy****缴费频次："+GrpContPayIntv);
			lCGrpContInfo
					.setBankCode(eleLCGrpContInfo.getChildText("BankCode"));// 银行编码VARCHAR(10)
			lCGrpContInfo.setBankAccNo(eleLCGrpContInfo
					.getChildText("BankAccNo"));// 银行帐号VARCHAR(40)
			lCGrpContInfo.setAccName(eleLCGrpContInfo.getChildText("AccName"));// 银行帐户名VARCHAR(120)
			lCGrpContInfo
					.setCurrency(eleLCGrpContInfo.getChildText("Currency"));// 币别VARCHAR(2)
			lCGrpContInfo.setCValiDate(eleLCGrpContInfo
					.getChildText("CValiDate"));// 保单生效日期DATE
			lCGrpContInfo.setOutPayFlag(eleLCGrpContInfo
					.getChildText("OutPayFlag"));// 溢交处理方式VARCHAR(1)
			lCGrpContInfo.setEnterKind(eleLCGrpContInfo
					.getChildText("EnterKind"));// 参保形式VARCHAR(1)
			lCGrpContInfo.setAmntGrade(eleLCGrpContInfo
					.getChildText("AmntGrade"));// 保额等级VARCHAR(1)
			lCGrpContInfo
					.setPeoples3(eleLCGrpContInfo.getChildText("Peoples3"));// 单位可投保人数INTEGER
			lCGrpContInfo.setOnWorkPeoples(eleLCGrpContInfo
					.getChildText("OnWorkPeoples"));// 在职投保人数INTEGER
			lCGrpContInfo.setOffWorkPeoples(eleLCGrpContInfo
					.getChildText("OffWorkPeoples"));// 退休投保人数INTEGER
			lCGrpContInfo.setOtherPeoples(eleLCGrpContInfo
					.getChildText("OtherPeoples"));// 其它投保人数INTEGER
			lCGrpContInfo.setRelaPeoples(eleLCGrpContInfo
					.getChildText("RelaPeoples"));// 连带投保人数INTEGER
			lCGrpContInfo.setRelaMatePeoples(eleLCGrpContInfo
					.getChildText("RelaMatePeoples"));// 连带配偶投保人数INTEGER
			lCGrpContInfo.setRelaYoungPeoples(eleLCGrpContInfo
					.getChildText("RelaYoungPeoples"));// 连带子女投保人数INTEGER
			lCGrpContInfo.setRelaOtherPeoples(eleLCGrpContInfo
					.getChildText("RelaOtherPeoples"));// 连带其它投保人数INTEGER
			lCGrpContInfo.setFirstTrialOperator(eleLCGrpContInfo
					.getChildText("FirstTrialOperator"));// 初审人VARCHAR(30)
			lCGrpContInfo.setReceiveDate(eleLCGrpContInfo
					.getChildText("ReceiveDate"));// 收单日期DATE
			lCGrpContInfo.setPrem(eleLCGrpContInfo.getChildText("Prem"));// 总保费NUMERIC(16,2)
			lCGrpContInfo.setAmnt(eleLCGrpContInfo.getChildText("Amnt"));// 总保额NUMERIC(16,2)
			lCGrpContInfo.setTempFeeNo(eleLCGrpContInfo
					.getChildText("TempFeeNo"));// 暂收据号VARCHAR(20)
			lCGrpContInfo.setBusinessBigType(eleLCGrpContInfo
					.getChildText("BusinessBigType"));// 行业大类VARCHAR(2)
			lCGrpContInfo.setCoInsuranceFlag(eleLCGrpContInfo
					.getChildText("CoInsuranceFlag"));// 共保保单标志VARCHAR(1)
			lCGrpContInfo.setCrs_SaleChnl(eleLCGrpContInfo
					.getChildText("Crs_SaleChnl"));// 集团交叉销售渠道VARCHAR(2)
			lCGrpContInfo.setCrs_BussType(eleLCGrpContInfo
					.getChildText("Crs_BussType"));// 集团销售业务类型VARCHAR(2)
			lCGrpContInfo.setGrpNature(eleLCGrpContInfo
					.getChildText("GrpNature"));// 单位性质VARCHAR(10)
			lCGrpContInfo.setBusinessType(eleLCGrpContInfo
					.getChildText("BusinessType"));// 行业分类VARCHAR(20)
			lCGrpContInfo.setPeoples(eleLCGrpContInfo.getChildText("Peoples"));// 总人数INTEGER
			lCGrpContInfo
					.setRgtMoney(eleLCGrpContInfo.getChildText("RgtMoney"));// 注册资本NUMERIC(16,2)
			lCGrpContInfo.setAsset(eleLCGrpContInfo.getChildText("Asset"));// 资产总额NUMERIC(16,2)
			lCGrpContInfo.setNetProfitRate(eleLCGrpContInfo
					.getChildText("NetProfitRate"));// 净资产收益率NUMERIC(16,4)
			lCGrpContInfo.setMainBussiness(eleLCGrpContInfo
					.getChildText("MainBussiness"));// 主营业务VARCHAR(60)
			lCGrpContInfo.setCorporation(eleLCGrpContInfo
					.getChildText("Corporation"));// 法人VARCHAR(20)
			lCGrpContInfo.setComAera(eleLCGrpContInfo.getChildText("ComAera"));// 机构分布区域VARCHAR(30)
			lCGrpContInfo.setPhone(eleLCGrpContInfo.getChildText("Phone"));// 单位电话VARCHAR(18)
			lCGrpContInfo.setFax(eleLCGrpContInfo.getChildText("Fax"));// 单位传真VARCHAR(18)
			lCGrpContInfo.setFoundDate(eleLCGrpContInfo
					.getChildText("FoundDate"));// 成立日期DATE
			lCGrpContInfo.setRemark(eleLCGrpContInfo.getChildText("Remark"));// 备注VARCHAR(3000)
			lCGrpContInfo.setHandlerName(eleLCGrpContInfo
					.getChildText("HandlerName"));// 投保经办人VARCHAR(60)
			lCGrpContInfo.setHandlerDate(eleLCGrpContInfo
					.getChildText("HandlerDate"));// 投保单填写日期DATE
			lCGrpContInfo.setHandlerPrint(eleLCGrpContInfo
					.getChildText("HandlerPrint"));// 投保单位章VARCHAR(120)
			lCGrpContInfo.setAgentDate(eleLCGrpContInfo
					.getChildText("AgentDate"));// 业务员填写日期DATE
			lCGrpContInfo.setMarketType(eleLCGrpContInfo
					.getChildText("MarketType"));// 市场类型VARCHAR(2)
			lCGrpContInfo.setCInValiDate(eleLCGrpContInfo
					.getChildText("CInValiDate"));// 合同终止日期DATE
			lCGrpContInfo.setAskGrpContNo(eleLCGrpContInfo
					.getChildText("AskGrpContNo"));// 询价合同号VARCHAR(20)
			lCGrpContInfo.setGrpContSumPrem(eleLCGrpContInfo
					.getChildText("GrpContSumPrem"));// 资金规模NUMERIC(16,2)
			lCGrpContInfo.setBigProjectFlag(eleLCGrpContInfo
					.getChildText("BigProjectFlag"));// 大项目标识VARCHAR(2)
			lCGrpContInfo.setContPrintType(eleLCGrpContInfo
					.getChildText("ContPrintType"));// 保单打印类型VARCHAR(2)
			lCGrpContInfo.setGrpAgentCom(eleLCGrpContInfo
					.getChildText("GrpAgentCom"));// 集团代理机构VARCHAR(20)
			lCGrpContInfo.setGrpAgentIDNo(eleLCGrpContInfo
					.getChildText("GrpAgentIDNo"));// 集团代理人身份证VARCHAR(20)
			lCGrpContInfo.setGrpAgentCode(eleLCGrpContInfo
					.getChildText("GrpAgentCode"));// 集团代理人VARCHAR(20)
			lCGrpContInfo.setGrpAgentName(eleLCGrpContInfo
					.getChildText("GrpAgentName"));// 集团代理人姓名VARCHAR(120)
			lCGrpContInfo.setInputOperator(eleLCGrpContInfo
					.getChildText("InputOperator"));// 录单人VARCHAR(10)
			lCGrpContInfo.setHandlerIDNo(eleLCGrpContInfo
					.getChildText("HandlerIDNo"));// 业务经办人身份证号
			lCGrpContInfo.setImpartParam1(eleLCGrpContInfo
					.getChildText("ImpartParam1"));//
			lCGrpContInfo.setImpartParam2(eleLCGrpContInfo
					.getChildText("ImpartParam2"));//
			data.add(lCGrpContInfo);

			Element msgLCGrpAppntInfo = rootElement.getChild("LCGrpAppntInfo");
			Element eleLCGrpAppntInfo = msgLCGrpAppntInfo.getChild("Item");

			LCGrpAppntInfo lCGrpAppntInfo = new LCGrpAppntInfo();
			lCGrpAppntInfo.setOrgancomCode(eleLCGrpAppntInfo
					.getChildText("OrgancomCode"));// 组织机构代码
			lCGrpAppntInfo.setGrpAddress(eleLCGrpAppntInfo
					.getChildText("GrpAddress"));// 投保人地址（联系地址）VARCHAR(120)不可为空
			lCGrpAppntInfo.setGrpZipCode(eleLCGrpAppntInfo
					.getChildText("GrpZipCode"));// 邮政编码
			lCGrpAppntInfo.setUnifiedSocialCreditNo(eleLCGrpAppntInfo
					.getChildText("UnifiedSocialCreditNo"));// 统一社会信用代码
			lCGrpAppntInfo.setTaxNo(eleLCGrpAppntInfo.getChildText("TaxNo"));// 税务登记证号码
			lCGrpAppntInfo.setLegalPersonName(eleLCGrpAppntInfo
					.getChildText("LegalPersonName"));// 法人姓名
			lCGrpAppntInfo.setLegalPersonIDNo(eleLCGrpAppntInfo
					.getChildText("LegalPersonIDNo"));// 法人身份证号
			lCGrpAppntInfo.setAppntOnWorkPeoples(eleLCGrpAppntInfo
					.getChildText("AppntOnWorkPeoples"));// 在职人数
			lCGrpAppntInfo.setAppntOffWorkPeoples(eleLCGrpAppntInfo
					.getChildText("AppntOffWorkPeoples"));// 退休人数
			lCGrpAppntInfo.setAppntOtherPeoples(eleLCGrpAppntInfo
					.getChildText("AppntOtherPeoples"));// 其他人员人数
			lCGrpAppntInfo.setClaimBankCode(eleLCGrpAppntInfo
					.getChildText("ClaimBankCode"));// 单位代领理赔金转账银行
			lCGrpAppntInfo.setClaimAccName(eleLCGrpAppntInfo
					.getChildText("ClaimAccName"));// 理赔金转账银行户名
			lCGrpAppntInfo.setClaimBankAccNo(eleLCGrpAppntInfo
					.getChildText("ClaimBankAccNo"));// 理赔金转账银行账号
			lCGrpAppntInfo.setBusinessScope(eleLCGrpAppntInfo
					.getChildText("BusinessScope"));// 经营范围
			lCGrpAppntInfo.setIDStartDate(eleLCGrpAppntInfo
					.getChildText("IDStartDate"));// 证件生效日期
			lCGrpAppntInfo.setIDEndDate(eleLCGrpAppntInfo
					.getChildText("IDEndDate"));// 证件失效日期
			lCGrpAppntInfo.setIDNo(eleLCGrpAppntInfo.getChildText("IDNo"));// 联系人证
			lCGrpAppntInfo.setIDType(eleLCGrpAppntInfo.getChildText("IDType"));// 证件类型
			lCGrpAppntInfo.setIDLongEffFlag(eleLCGrpAppntInfo
					.getChildText("IDLongEffFlag"));// >是否长期有效标志
			data.add(lCGrpAppntInfo);

			Element msgLCGrpAddressInfo = rootElement
					.getChild("LCGrpAddressInfo");
			Element eleLCGrpAddressInfo = msgLCGrpAddressInfo.getChild("Item");

			LCGrpAddressInfo lCGrpAddressInfo = new LCGrpAddressInfo();
			lCGrpAddressInfo.setLinkMan1(eleLCGrpAddressInfo
					.getChildText("LinkMan1"));// 联系人1VARCHAR(60)
			lCGrpAddressInfo.setDepartment1(eleLCGrpAddressInfo
					.getChildText("Department1"));// 部门1VARCHAR(120)
			lCGrpAddressInfo.setHeadShip1(eleLCGrpAddressInfo
					.getChildText("HeadShip1"));// 职务1VARCHAR(30)
			lCGrpAddressInfo.setPhone1(eleLCGrpAddressInfo
					.getChildText("Phone1"));// 联系电话1VARCHAR(30)
			lCGrpAddressInfo.setE_Mail1(eleLCGrpAddressInfo
					.getChildText("E_Mail1"));// E_Mail1
			lCGrpAddressInfo.setFax1(eleLCGrpAddressInfo.getChildText("Fax1"));// 传真1VARCHAR(30)
			lCGrpAddressInfo.setMobile1(eleLCGrpAddressInfo
					.getChildText("Mobile1"));// 联系人1手机号VARCHAR(30)
			lCGrpAddressInfo.setLinkMan2(eleLCGrpAddressInfo
					.getChildText("LinkMan2"));// 联系人2VARCHAR(60)
			lCGrpAddressInfo.setDepartment2(eleLCGrpAddressInfo
					.getChildText("Department2"));// 部门2
			lCGrpAddressInfo.setHeadShip2(eleLCGrpAddressInfo
					.getChildText("HeadShip2"));// 职务2
			lCGrpAddressInfo.setPhone2(eleLCGrpAddressInfo
					.getChildText("Phone2"));// 联系电话2VARCHAR(30)
			lCGrpAddressInfo.setE_Mail2(eleLCGrpAddressInfo
					.getChildText("E_Mail2"));// E_Mail2
			lCGrpAddressInfo.setFax2(eleLCGrpAddressInfo.getChildText("Fax2"));// 传真1VARCHAR(30)
			data.add(lCGrpAddressInfo);

			Element msgLCExtendInfo = rootElement.getChild("LCExtendInfo");
			Element eleLCExtendInfo = msgLCExtendInfo.getChild("Item");

			LCExtendInfo LCExtendInfo = new LCExtendInfo();
			LCExtendInfo.setAssistSaleChnl(eleLCExtendInfo
					.getChildText("AssistSaleChnl"));// 协助销售渠道
			LCExtendInfo.setAssistAgentCode(eleLCExtendInfo
					.getChildText("AssistAgentCode"));// 协助销售人员代码
			data.add(LCExtendInfo);

			// 未解析的集合
			// 保障计划定制
			Element msgContPlanInfo = rootElement.getChild("ContPlanInfo");
			List eleContPlanInfo = msgContPlanInfo.getChildren("Item");
			ContPlanInfoList ContPlanInfo = new ContPlanInfoList();
			ContPlanInfo.setList(eleContPlanInfo);
			data.add(ContPlanInfo);

			/**
			 * GY BY 2017-12-04 定义公共账户PubAccInfo
			 * ContPlanInfoListContPlanInfoList
			 */
			ContPlanInfoList riskCodeInfo = (ContPlanInfoList) data
					.getObjectByObjectName("ContPlanInfoList", 0);
			System.out.println("**********************************委托型险种解析报文*****");
			List list = riskCodeInfo.getList();
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					Element ele = (Element) list.get(i);
					RiskCode = ele.getChildText("RiskCode");
					// 若险种是690201，需要定义公共账户
					if ("690201".equals(RiskCode)) {
						Element msgPubAccInfo = rootElement
								.getChild("PubAccInfo");
						List elePubAccInfo = msgPubAccInfo.getChildren("Item");
						PubAccInfoList PubAccInfo = new PubAccInfoList();
						PubAccInfo.setList(elePubAccInfo);
						data.add(PubAccInfo);
					}
				}
			}

			// 约定缴费计划
			Element msgPayPlanInfoList = rootElement.getChild("PayPlanInfo");
			List elePayPlanInfoList = msgPayPlanInfoList.getChildren("Item");
			PayPlanInfoList payPlanInfoList = new PayPlanInfoList();
			payPlanInfoList.setList(elePayPlanInfoList);
			data.add(payPlanInfoList);

			// 共保要素 如果CoInsuranceFlag为1时解析下面信息 xu
			if ("1".equals(eleLCGrpContInfo.getChildText("CoInsuranceFlag"))) {
				Element msLCCoInsuranceParaminfo = rootElement
						.getChild("LCCoInsuranceParaminfo");
				List eleLCCoInsuranceParaminfo = msLCCoInsuranceParaminfo
						.getChildren("Item");
				LCCoInsuranceParaminfoList LCCoInsuranceParaminfo = new LCCoInsuranceParaminfoList();
				LCCoInsuranceParaminfo.setList(eleLCCoInsuranceParaminfo);
				data.add(LCCoInsuranceParaminfo);

			}

			Element msgLCGrpContSubInfo = rootElement
					.getChild("LCGrpContSubInfo");
			Element eleLCGrpContSubInfo = msgLCGrpContSubInfo.getChild("Item");
			LCGrpContSubInfo lCGrpContSubInfo = new LCGrpContSubInfo();
			lCGrpContSubInfo.setProjectName(eleLCGrpContSubInfo
					.getChildText("ProjectName"));
			lCGrpContSubInfo.setBalanceTermFlag(eleLCGrpContSubInfo
					.getChildText("BalanceTermFlag"));
			lCGrpContSubInfo.setStopLine(eleLCGrpContSubInfo
					.getChildText("StopLine"));
			lCGrpContSubInfo.setSharedLine(eleLCGrpContSubInfo
					.getChildText("SharedLine"));
			lCGrpContSubInfo.setRevertantLine(eleLCGrpContSubInfo
					.getChildText("RevertantLine"));
			lCGrpContSubInfo.setAccountCycle(eleLCGrpContSubInfo
					.getChildText("AccountCycle"));
			lCGrpContSubInfo.setBalanceLine(eleLCGrpContSubInfo
					.getChildText("BalanceLine"));
			lCGrpContSubInfo.setCostRate(eleLCGrpContSubInfo
					.getChildText("CostRate"));
			lCGrpContSubInfo.setBalanceRate(eleLCGrpContSubInfo
					.getChildText("BalanceRate"));
			lCGrpContSubInfo.setSharedRate(eleLCGrpContSubInfo
					.getChildText("SharedRate"));
			lCGrpContSubInfo.setRevertantRate(eleLCGrpContSubInfo
					.getChildText("RevertantRate"));
			lCGrpContSubInfo.setBalanceTxt(eleLCGrpContSubInfo
					.getChildText("BalanceTxt"));
			lCGrpContSubInfo.setXBFlag(eleLCGrpContSubInfo
					.getChildText("XBFlag"));
			lCGrpContSubInfo.setBalanceType(eleLCGrpContSubInfo
					.getChildText("BalanceType"));
			data.add(lCGrpContSubInfo);

			Element msgLCGCUWMasterInfo = rootElement
					.getChild("LCGCUWMasterInfo");
			Element eleLCGCUWMasterInfo = msgLCGCUWMasterInfo.getChild("Item");
			LCGCUWMasterInfo lCGCUWMasterInfo = new LCGCUWMasterInfo();
			lCGCUWMasterInfo.setGUWIdea(eleLCGCUWMasterInfo
					.getChildText("GUWIdea"));
			lCGCUWMasterInfo.setGUWState(eleLCGCUWMasterInfo
					.getChildText("GUWState"));
			data.add(lCGCUWMasterInfo);

			if ("".equals(eleLCGrpContInfo.getChildText("PayMode"))
					|| null == eleLCGrpContInfo.getChildText("PayMode")) {
				errorValue = "交费方式不能为空";
				return false;
			}
			if ("".equals(eleLCGrpContInfo.getChildText("GrpContPayIntv"))
					|| null == eleLCGrpContInfo.getChildText("GrpContPayIntv")) {
				errorValue = "交费（频次）间隔不能为空";
				return false;
			}

		} catch (JDOMException e) {
			e.printStackTrace();
			errorValue = "处理报文出错：" + e.toString();
			return false;
		}
		return true;

	}

	/**
	 * 对解析后的报文数据，进行校验 不为空数据校验
	 * 
	 * @return
	 */
	public boolean cheakdata() {
		System.out.println("***********开始cheakdata校验报文*********");
		// 锁表，防止多次提交
		MMap tCekMap = null;
		tCekMap = lockLGWORK();
		if (tCekMap == null) {
			errorValue = "印刷号：" + PrtNo + " 正在进行无名单实名化，请不要重复请求";
			return false;
		}
		if (!submit(tCekMap)) {
			errorValue = "印刷号：" + PrtNo + " 正在进行无名单实名化，请不要重复请求";
			return false;
		}
		return true;
	}

	/**
	 * 锁表，防止多次提交
	 */
	private MMap lockLGWORK() {
		MMap tMMap = null;
		String tLockNoType = "CO";
		String tAIS = "360";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", PrtNo);
		tTransferData.setNameAndValue("LockNoType", tLockNoType);
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
		MessageHead messageHead = (MessageHead) data.getObjectByObjectName(
				"MessageHead", 0);
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.Operator = messageHead.getSendOperator();
		mGlobalInput.ComCode = messageHead.getBranchCode();
		mGlobalInput.ManageCom = messageHead.getBranchCode();
		VData tVData = new VData();
		tVData.add(mGlobalInput);
		tVData.add(tTransferData);

		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
		if (tMMap == null) {
			return null;
		}
		return tMMap;
	}

	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			errorValue = "提交数据库发生错误" + tPubSubmit.mErrors;
			return false;
		}
		return true;
	}

	/**
	 * getUrlName 获取文件存放路径
	 * 
	 * @return boolean
	 */
	private boolean getUrlName(String type) {
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";// 发盘报文的存放路径
		String fileFolder = new ExeSQL().getOneValue(sqlurl);

		if (fileFolder == null || fileFolder.equals("")) {
			aOutXmlStr = "getFileUrlName,获取文件存放路径出错";
			return false;
		}
		// fileFolder ="E://QY//";
		if (type.equals("1")) {
			mUrl = fileFolder + "SYYB/QYRequest/" + PubFun.getCurrentDate2();
		} else {
			mUrl = fileFolder + "SYYB/QYRsponse/" + PubFun.getCurrentDate2();
		}
		if (!newFolder(mUrl)) {
			return false;
		}
		return true;
	}

	/**
	 * newFolder 新建报文存放文件夹，以便对发盘报文查询
	 * 
	 * @return boolean
	 */
	public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在,文件路径为:" + myFilePath);
				return true;
			} else {
				myFilePath.mkdirs();
				System.out.println("新建目录成功,文件路径为:" + myFilePath);
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败!!");
			e.printStackTrace();
			return false;
		}
	}
}
