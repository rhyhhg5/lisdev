package com.contract;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;



public class ContractGroupContDeleteUI
{
	
	private VData mResult = new VData();

	
	private String mOperate;

	
	public CErrors mErrors = new CErrors();

	public ContractGroupContDeleteUI()
	{
	}

	
	public boolean submitData(VData cInputData, String cOperate)
	{
		
		this.mOperate = cOperate;

		ContractGroupContDeleteBL tGroupContDeleteBL = new ContractGroupContDeleteBL();

		if (tGroupContDeleteBL.submitData(cInputData, mOperate) == false)
		{
			
			this.mErrors.copyAllErrors(tGroupContDeleteBL.mErrors);

			return false;
		}
		else
		{
			mResult = tGroupContDeleteBL.getResult();
		}

		return true;
	}

	
	public VData getResult()
	{
		return mResult;
	}

	public static void main(String[] agrs)
	{
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();    
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.ManageCom = "86";
		mGlobalInput.Operator = "UW0003";

		tLCGrpContSchema.setGrpContNo("1400006631");
		tLCGrpContSchema.setManageCom("86440000");

		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(mGlobalInput);

		ContractGroupContDeleteUI tgrlbl = new ContractGroupContDeleteUI();
		tgrlbl.submitData(tVData, "DELETE");

		if (tgrlbl.mErrors.needDealError())
		{
			System.out.println(tgrlbl.mErrors.getFirstError());
		}
	}
}
