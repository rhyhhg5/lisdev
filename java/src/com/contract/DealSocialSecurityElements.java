package com.contract;

import com.contract.obj.LCGrpContInfo;
import com.contract.obj.LCGrpContSubInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSubSchema;
import com.sinosoft.lis.tb.BalanceInfoUI;
import com.sinosoft.lis.vschema.LCGrpContSubSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 定义社保项目要素
 * @author Administrator
 *
 */
public class DealSocialSecurityElements {
	//错误信息
	private String Content = "";
	//从报文中获取
	//印刷号码  保存节点下  取值
	private String PrtNo="";
	//从报文 LCGrpContSubInfo(定义社保项目要素)节点下取值
	//项目名称
	private String ProjectName="";
	//是否为结余返还
	private String BalanceTermFlag="";
	//止损线
	private String StopLine="";
	//共担线
	private String SharedLine="";
	//回补线
	private String RevertantLine="";
	//核算周期
	private String AccountCycle="";
	//结余线
	private String BalanceLine="";
	//成本占比
	private String CostRate="";
	//返还比例
	private String BalanceRate="";
	//共担比例
	private String SharedRate="";
	//
	private String RevertantRate="";
	//风险调节文本
	private String BalanceTxt="";
	//是否为新建项目
	private String XBFlag="";
	//返还类型
	private String BalanceType="";
	private GlobalInput tGI = new GlobalInput(); 
	public boolean dealData(VData data){
		System.out.println("*******************************定义社保项目要素***************************************");
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		tGI.Operator = "CENA";
		tGI.ManageCom = lCGrpContInfo.getManageCom();
		//原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
		tGI.ComCode = tGI.ManageCom;
		
		PrtNo = lCGrpContInfo.getPrtNo();
		LCGrpContSubInfo tLCGrpContSubInfo=(LCGrpContSubInfo)data.getObjectByObjectName("LCGrpContSubInfo", 0);
		ProjectName=tLCGrpContSubInfo.getProjectName();
		BalanceTermFlag=tLCGrpContSubInfo.getBalanceTermFlag();
		StopLine=tLCGrpContSubInfo.getStopLine();
		SharedLine=tLCGrpContSubInfo.getSharedLine();
		RevertantLine=tLCGrpContSubInfo.getRevertantLine();
		AccountCycle=tLCGrpContSubInfo.getAccountCycle();
		BalanceLine=tLCGrpContSubInfo.getBalanceLine();
		CostRate=tLCGrpContSubInfo.getCostRate();
		BalanceRate=tLCGrpContSubInfo.getBalanceRate();
		SharedRate=tLCGrpContSubInfo.getSharedRate();
		RevertantRate=tLCGrpContSubInfo.getRevertantRate();
		BalanceTxt=tLCGrpContSubInfo.getBalanceTxt();
		XBFlag=tLCGrpContSubInfo.getXBFlag();
		BalanceType=tLCGrpContSubInfo.getBalanceType();
		if(!deal()){
			return false;
		}
		return true;
	}
	public boolean deal(){
		if(!initBalanceCheck()){
			return false;
		}
		if(!initBalance()){
			return false;
		}
		return true;
	}
	/**
	 * 定义社保项目要素 保存的前台校验
	 * @return
	 */
	public boolean initBalanceCheck(){
		String tSqlCode = "select markettype from LCGrpCont where prtno='" +PrtNo+"'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrCodeResult = tExeSQL.execSQL(tSqlCode);
		if(arrCodeResult.getMaxNumber()<=0){
			Content="定义社保项目要素操作失败，失败原因:保单未保存，不允许录入社保项目要素信息!";
			System.out.println(Content);
			return false;
		}
		if("".equals(arrCodeResult.GetText(1, 1))||null==arrCodeResult.GetText(1, 1)){
			Content="定义社保项目要素保存操作失败，失败原因:获取保单市场类型失败！";
			System.out.println(Content);
			return false;
		}
		// 不阻断下面调用 当市场类型为1或9 调用定义社保项目要素保存
		if( "1" .equals(arrCodeResult.GetText(1, 1))|| "9".equals(arrCodeResult.GetText(1, 1))){
			System.out.println("该保单对应的市场类型，不需录入社保项目要素！");
			return true;
		}
	 	if( "".equals(ProjectName) ||null==ProjectName)
	   	{
	 		Content="定义社保项目要素保存操作失败，失败原因:项目名称不能为空！";
			System.out.println(Content);
			return false;
	   	}
//	 	if(ProjectName.length() > 30)
//	   	{
//	 		Content="定义社保项目要素保存操作失败，失败原因:项目名称必须在30字以内！接口校验";
//			System.out.println(Content);
//			return false;
//	   	}
	 	if("".equals(BalanceTermFlag) ||null==BalanceTermFlag)
	   	{
	 		Content="定义社保项目要素保存操作失败，失败原因:是否为结余返还不能为空！";
			System.out.println(Content);
			return false;
	   	}
	 	if("".equals(StopLine) ||null==StopLine)
	   	{
	 		Content="定义社保项目要素保存操作失败，失败原因:止损线不能为空！";
			System.out.println(Content);
			return false;
	   	}
	 	if("".equals(SharedLine) ||  null==SharedLine)
	   	{
	 		Content="定义社保项目要素保存操作失败，失败原因:共担线不能为空！";
			System.out.println(Content);
			return false;
	   	}
	 	if("".equals(RevertantLine) || null==RevertantLine)
	   	{
	 		Content="定义社保项目要素保存操作失败，失败原因:回补线不能为空！";
			System.out.println(Content);
			return false;
	   	}
	 	if( !"".equals(AccountCycle) &&  null!=AccountCycle)
	   	{
	   		if(!isNumeric(AccountCycle))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:核算周期必须为非负数字！";
				System.out.println(Content);
				return false;
			}
	   		
			if(Double.valueOf(AccountCycle)<0)
			{
				Content="定义社保项目要素保存操作失败，失败原因:核算周期不能为负数！";
				System.out.println(Content);
				return false;
			}
	   	}
	   	if(!"".equals(BalanceLine) && null!=BalanceLine)
	   	{
	   		if(!isNumeric(BalanceLine))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:结余线必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(BalanceLine)<0 || Double.valueOf(BalanceLine)>1)
			{
				Content="定义社保项目要素保存操作失败，失败原因:结余线必须大于等于0，且小于1！";
				System.out.println(Content);
				return false;
			}
	   	}
	  	if(!"".equals(CostRate) &&  null!=CostRate)
	   	{
	   		if(!isNumeric(CostRate))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:成本占比必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(CostRate)<0 || Double.valueOf(CostRate)>1)
			{
				Content="定义社保项目要素保存操作失败，失败原因:成本占比必须大于等于0，且小于1！";
				System.out.println(Content);
				return false;
			}
	   	}
	 	if(!"".equals(BalanceRate) && null!=BalanceRate)
	   	{
	   		if(!isNumeric(BalanceRate))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:返还比例必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(BalanceRate)<0 || Double.valueOf(BalanceRate)>1)
			{
				Content="定义社保项目要素保存操作失败，失败原因:返还比例必须大于等于0，且小于1！";
				System.out.println(Content);
				return false;
			}
	   	}
	 /*	if(!"".equals(BalanceAmnt) && != null!=BalanceAmnt)
	   	{
	   		if(!isNumeric(fm.BalanceAmnt.value))
			{
				alert("预估返还金额必须为非负数字！");
				fm.BalanceAmnt.focus();
				return false;
			}
			if(fm.BalanceAmnt.value<0)
			{
				alert("预估返还金额不能为负数！");
				fm.BalanceAmnt.focus();
				return false;
			}
	   	}*/
	  	if(!"".equals(StopLine) && null!=StopLine)
	   	{
	   		if(!isNumeric(StopLine))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:止损线必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(StopLine)<0)
			{
				Content="定义社保项目要素保存操作失败，失败原因:止损线不能为负数！";
				System.out.println(Content);
				return false;
			}
	   	}
		if(!"".equals(SharedLine) && null!=SharedLine)
	   	{
	   		if(!isNumeric(SharedLine))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:共担线必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(SharedLine)<0)
			{
				Content="定义社保项目要素保存操作失败，失败原因:共担线不能为负数！";
				System.out.println(Content);
				return false;
			}
	   	}
	 	if(!"".equals(SharedRate) &&  null!=SharedRate)
	   	{
	   		if(!isNumeric(SharedRate))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:共担比例必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(SharedRate)<0 || Double.valueOf(SharedRate)>1)
			{
				Content="定义社保项目要素保存操作失败，失败原因:共担比例必须大于等于0，且小于1！ ";
				System.out.println(Content);
				return false;
			}
	   	}
		if(!"".equals(RevertantLine) && null!=RevertantLine)
	   	{
	   		if(!isNumeric(RevertantLine))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:回补线必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(RevertantLine) <0)
			{
				Content="定义社保项目要素保存操作失败，失败原因:回补线不能为负数！";
				System.out.println(Content);
				return false;
			}
	   	}
	 	if(!"".equals(RevertantRate) && null!=RevertantRate)
	   	{
	   		if(!isNumeric(RevertantRate))
			{
	   			Content="定义社保项目要素保存操作失败，失败原因:回补比例必须为非负数字！";
				System.out.println(Content);
				return false;
			}
			if(Double.valueOf(RevertantRate)<0 || Double.valueOf(RevertantRate)>1)
			{
				Content="定义社保项目要素保存操作失败，失败原因:回补比例必须大于等于0，且小于1！";
				System.out.println(Content);
				return false;
			}
	   	}
	  	if(!"".equals(BalanceTxt) && null!=BalanceTxt)
	   	{
	   		if(BalanceTxt.length() >1000){
	   			Content="定义社保项目要素保存操作失败，失败原因:风险调节文本最多1000字！";
				System.out.println(Content);
				return false;
	   		}
	   	}
		if("".equals(XBFlag)|| null==XBFlag)
	   	{
			Content="定义社保项目要素保存操作失败，失败原因:是否为新建项目不能为空！";
			System.out.println(Content);
			return false;
	   	}
		//---------------新添-----------------
	   	//  从数据库中获取 "是否为新建项目" 的值 v1
		//by lrx data:20190214
		//开放查询项目是否已存在，已存在项目不允许选1新建，不存在项目不允许选2已有项目
		String tSQL = "select 1 from lcgrpcontsub "
			     + "where 1=1 "
			     + "and projectname = '"+ProjectName+"' "
			     +" union select 1 from LCProjectInfo lcp where lcp.projectname = '"+ProjectName+"' ";
		SSRS tResult =tExeSQL.execSQL(tSQL) ;
		if(tResult.getMaxNumber()>0){
			if("1".equals(XBFlag)){
				Content="定义社保项目要素保存操作失败，失败原因:请检查项目是否是新建项目！";
				System.out.println(Content);
				return false;
			}
		}else{
			if("2".equals(XBFlag)){
				Content="定义社保项目要素保存操作失败，失败原因:新建项目，不能作为已有项目录入！";
				System.out.println(Content);
				return false;
			}
		}
		if("2".equals(XBFlag)){
			String tSQL1 = "select 1 from lcgrpcontsub where projectname is not null and projectname != '' and projectname = '"+ProjectName+"' union select 1 from LCProjectInfo lcp where  lcp.projectname is not null and lcp.projectname != '' and lcp.projectname = '"+ProjectName+"' ";
			SSRS strQueryResult =tExeSQL.execSQL(tSQL1);
			if(strQueryResult.getMaxNumber()<=0){
				Content="定义社保项目要素保存操作失败，失败原因:当为续保时，只能选择原有的项目编码及项目名称！";
				System.out.println(Content);
				return false;
			}
		}
		//fm.fmtransact.value = "INSERT||MAIN";
		return true;
	}
	/**
	 * 定义社保项目要素 保存BalanceInputSave.jsp中的代码
	 * @return
	 */
	public boolean initBalance(){
	//	GlobalInput tG = new GlobalInput();
		//人员再定 xu
	//    tG.Operator ="CP3613";
//	    tG.Operator ="CENA";
//	    tG.ComCode="86360500";
//	    tG.ManageCom="86360500";
		String CurrentDate = PubFun.getCurrentDate();
		String CurrentTime = PubFun.getCurrentTime();
		//输入参数
		LCGrpContSubSet tLCGrpContSubSet = new LCGrpContSubSet();
		BalanceInfoUI mBalanceInfoUI = new BalanceInfoUI();
		//输出参数
		CErrors tError = null;
		String tOperate = "";
		String transact = "";
		String FlagStr = "";
		
		String mPrtNo = PrtNo;
		//执行动作：insert 添加纪录 delete 删除记录 
		transact ="INSERT||MAIN";
		if(mPrtNo == null || "".equals(mPrtNo))
		{
			FlagStr = "Fail";
			Content = "获取保单印刷号码失败！";
			System.out.println(Content);
			return false;
		}
		System.out.println("保单印刷号:" + mPrtNo);
		if(!"Fail".equals(FlagStr)){
			LCGrpContSubSchema tLCGrpContSubSchema = new LCGrpContSubSchema();
			tLCGrpContSubSchema.setPrtNo(mPrtNo);
			tLCGrpContSubSchema.setProjectName(ProjectName);
			tLCGrpContSubSchema.setBalanceTermFlag(BalanceTermFlag);
			tLCGrpContSubSchema.setAccountCycle(AccountCycle);
			tLCGrpContSubSchema.setBalanceLine(BalanceLine);
			tLCGrpContSubSchema.setCostRate(CostRate);
			tLCGrpContSubSchema.setBalanceRate(BalanceRate);
			tLCGrpContSubSchema.setBalanceType(BalanceType);
			//页面该BalanceAmnt字段为隐藏的 没有处理 现给该字段赋值为空 xu
			tLCGrpContSubSchema.setBalanceAmnt("");
			tLCGrpContSubSchema.setStopLine(StopLine);
			tLCGrpContSubSchema.setSharedLine(SharedLine);
			tLCGrpContSubSchema.setSharedRate(SharedRate);
			tLCGrpContSubSchema.setRevertantLine(RevertantLine);
			tLCGrpContSubSchema.setRevertantRate(RevertantRate);
			tLCGrpContSubSchema.setBalanceTxt(BalanceTxt);
			tLCGrpContSubSchema.setbak1(XBFlag);
			// 准备向后台传输数据 VData
			VData tVData = new VData();
			FlagStr = "";
			tVData.add(tGI);
			tVData.addElement(tLCGrpContSubSchema);
			try {
				mBalanceInfoUI.submitData(tVData, transact);
			} catch (Exception ex) {
				Content = "保存失败，原因是:" + ex.toString();
				FlagStr = "Fail";
				System.out.println(Content);
				return false;
			}
			if (!FlagStr.equals("Fail")) {
				tError = mBalanceInfoUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 保存成功! ";
					FlagStr = "Succ";
				} else {
					Content = " 保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					System.out.println(Content);
					return false;
				}
			}
		}
		return true;
	}
	public boolean isNumeric(String strValue)
	{
	  String NUM="0123456789.";
	  if(null== strValue||"".endsWith(strValue)) return false;
	  for(int i=0;i<strValue.length();i++)
	  {
	    if(NUM.indexOf(strValue.charAt(i))<0) return false;
	  }
	  if(strValue.indexOf(".")!=strValue.lastIndexOf(".")) return false;
	  return true;
	}
	public String getContent() {
		return Content;
	}
	public static void main(String[] args) {
		DealSocialSecurityElements a=new DealSocialSecurityElements();
		a.deal();
	}
}
