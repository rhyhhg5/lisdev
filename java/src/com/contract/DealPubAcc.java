package com.contract;

import java.util.List;

import org.jdom.Element;

import com.contract.obj.LCGrpContInfo;
import com.contract.obj.PubAccInfoList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCGrpFeeSchema;
import com.sinosoft.lis.schema.LCGrpInterestSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.tb.GrpPubAccUI;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LCGrpInterestSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author GengYe 2017-12-01
 */
public class DealPubAcc {

	private GlobalInput tGI = new GlobalInput(); 
	// 错误信息
	private String errorValue;

	public String getErrorValue() {
		return errorValue;
	}
	
	// 错误信息
	String Content;

	public String getContent() {
		return Content;
	}

	static VData data;
	static String RiskCode;
	String GrpContNo;
	String GrpPolNo;
	String PrtNo;
	String InsuAccNo;
	String ClaimNum;
	String Houston;
	String EntrustMoney;
	String InsuAccName;
	String PublicAccType;
	String InsuredName;
	String ftransact;
	
	List elelist;// Item1节点的集合
	List manageFeeList; // DefManageFee节点的集合
	ExeSQL tExeSQL = new ExeSQL();
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// DealPubAcc dealPubAcc = new DealPubAcc();
		// dealPubAcc.deal(data, RiskCode);
	}

	public boolean deal(VData vdata, String tRiskCode) {
		System.out
				.println("==================================定义公共账户======================");
		data = vdata;

		// String RiskCode = "690201";
		// String PrtNo = "18171201114";

		RiskCode = tRiskCode;
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data
				.getObjectByObjectName("LCGrpContInfo", 0);
		tGI.Operator = "CENA";
		tGI.ManageCom = lCGrpContInfo.getManageCom();
		//原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
		tGI.ComCode = tGI.ManageCom;
		
		PrtNo = lCGrpContInfo.getPrtNo();
		String GrpContNoSQL = "SELECT GrpContNo,GrpPolNo FROM lcgrppol WHERE PrtNo = '"
				+ PrtNo + "'";
		SSRS GrpContNoArr = tExeSQL.execSQL(GrpContNoSQL);
		GrpContNo = GrpContNoArr.GetText(1, 1);
		GrpPolNo = GrpContNoArr.GetText(1, 2);

		PubAccInfoList tPubAccInfoList = (PubAccInfoList) data
				.getObjectByObjectName("PubAccInfoList", 0);
		List list = tPubAccInfoList.getList();
		System.out.println("====================ceshi=====" + list.size());
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Element ele = (Element) list.get(i);
				InsuAccNo = ele.getChildText("InsuAccNo"); // Item 账户选择 理賠001
															// 固定004
				elelist = ele.getChildren("Item1");
				manageFeeList = ele.getChildren("DefManageFee");
				ClaimNum = ele.getChildText("ClaimNum"); // 赔付顺序 1仅个人 2先个人在公共
				Houston = ele.getChildText("Houston");
				EntrustMoney = ele.getChildText("EntrustMoney");
				String acctypeSql = "select acctype from LMRiskInsuAcc where '1'='1' and InsuAccNo='"+ InsuAccNo + "'fetch first 3000 rows only with ur";
				SSRS tacctypeArr = tExeSQL.execSQL(acctypeSql);
				if (tacctypeArr.getMaxNumber() > 0) {
					if ("001".equals(tacctypeArr.GetText(1, 1))) {
						PublicAccType = "C";
						InsuredName = "团体理赔帐户";
					} else if ("004".equals(tacctypeArr.GetText(1, 1))) {
						PublicAccType = "G";
						InsuredName = "团体固定账户";
					} else {
						errorValue = ("没有指定帐户类型!");
						return false;
					}
				} else {
					errorValue = ("没有指定帐户类型!");
					return false;
				}
				
				/*
				     	暂时不校验， 需求委托型险种只有一种
							if (RiskCode == "370301") {
				 */

				
				if (!submitForm()) {
					return false;
				}
				// 定义管理费 保存
				if (!saveManageFee()) {
					return false;
				}
				if (!saveHouston()) {
					return false;
				}
				/*
				 * 委托型险种暂时不需要定义利息 定义利息信息 -----
				 */
				// if(!saveInterest()){
				// return false;
				// }
			}
		}
		return true;
	}

	

	/**
	 * PubAccInputSave.jsp
	 * 
	 * 团体帐户定义
	 * 
	 * @return
	 */
	public boolean PubAccSave() {

		// 接收信息，并作校验处理。
		// 输入参数
		LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
		LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
		LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
		GrpPubAccUI tGrpPubAccUI = new GrpPubAccUI();

		// 输出参数
		CErrors tError = null;
		String tRela = "";
		String FlagStr = "";
		String Content = "";
		String transact = "";
		String tCalFactorType;
		String tPublicAcc = null;
		System.out.println("begin............");
	//	GlobalInput tG = new GlobalInput();
	//	tG.Operator = "CP3613";
//		tG.Operator = "CENA";
//		tG.ComCode = "86360500";
//		tG.ManageCom = "86360500";
		String tFeeValue = "";
		String tFeeCode = "";
		String cIndFeeValue = "";
		String claimnum = "";
		String tGrpContNo = GrpContNo;
		String tGrpPolNo = GrpPolNo;
		String tRiskCode = RiskCode;
		String tInsuAccNo = InsuAccNo;
		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		transact = ftransact;

		if ("INSERT||MAIN".equals(transact)) {
			// 处理要素信息
			System.out.println("======================ceshi==============="
					+ elelist.size());
			for (int i = 0; i < elelist.size(); i++) {
				Element eleItem1 = (Element) elelist.get(i);
				List listItem2 = eleItem1.getChildren("Item2");
				System.out.println("================要素节点: " + listItem2.size());
				for (int j = 0; j < listItem2.size(); j++) {
					Element eleItem2 = (Element) listItem2.get(j);
					String tCalFactor = eleItem2.getChildText("CalFactor");
					String tCalFactorValue = eleItem2
							.getChildText("CalFactorValue");

					StringBuffer SqlContPlanGrid = new StringBuffer();
					SqlContPlanGrid
							.append("select distinct CalFactor,FactorName,FactorNoti,CalFactorType");
					// SqlContPlanGrid.append(" and CalFactor = '" + tCalFactor
					// + "'");
					// SqlContPlanGrid.append(" and CalFactorValue='"+tCalFactorValue+"')");
					SqlContPlanGrid
							.append(" from lmriskdutyfactor where  RiskCode='"
									+ tRiskCode + "' and InsuAccNo='"
									+ InsuAccNo + "' and chooseflag ='1'");
					ExeSQL exeSQL = new ExeSQL();
					SSRS ssrsSqlContPlanGrid = exeSQL.execSQL(SqlContPlanGrid
							.toString());
					if (ssrsSqlContPlanGrid.getMaxRow() > 0) {
						tCalFactorType = ssrsSqlContPlanGrid.GetText(1, 4);
						LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
						tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
						tLCContPlanDutyParamSchema
								.setProposalGrpContNo(tGrpContNo);
						tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
						tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
						tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
						tLCContPlanDutyParamSchema.setContPlanCode("11");
						tLCContPlanDutyParamSchema.setDutyCode("000000");
						tLCContPlanDutyParamSchema.setCalFactor(tCalFactor);
						tLCContPlanDutyParamSchema
								.setCalFactorType(tCalFactorType);
						tLCContPlanDutyParamSchema
								.setCalFactorValue(tCalFactorValue);
						tLCContPlanDutyParamSchema.setPlanType("0");
						tLCContPlanDutyParamSchema.setPayPlanCode("000000");
						tLCContPlanDutyParamSchema.setGetDutyCode("000000");
						tLCContPlanDutyParamSchema.setInsuAccNo(tInsuAccNo);
						tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
					} else {
						errorValue = "账户信息：" + InsuAccNo + "下 没有" + tCalFactor
								+ "的要素信息错误";
						return false;
					}
					//
					if ("PublicAcc".equals(tCalFactor)) {
						tPublicAcc = tCalFactorValue;
					}
				}
				// 实际是存储在职/退休 但是应数据被保险人状态，因此用词字段存储账户类型
				String tPublicAccType = PublicAccType;
				// 如果是公共账户的话存储公共账户名称
				String tInsuredName = InsuredName;
				LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
				tLCInsuredListSchema.setGrpContNo(tGrpContNo);
				tLCInsuredListSchema.setInsuredName(tInsuredName);
				tLCInsuredListSchema.setEmployeeName(tInsuredName);
				tLCInsuredListSchema.setRelation("00");
				tLCInsuredListSchema.setRiskCode(tRiskCode);
				tLCInsuredListSchema.setPublicAcc(tPublicAcc); // 保费
				tLCInsuredListSchema.setPublicAccType(tPublicAccType);
				tLCInsuredListSet.add(tLCInsuredListSchema);
				// String tPublicAcc = Prem;
			}
		} else if ("MANAGEFEE||MAIN".equals(transact)||"HOUSTO||MAIN".equals(ftransact)) {

			// 管理费保存
			if (manageFeeList.size() > 0) {
				System.out.println("=========================:"
						+ manageFeeList.size());
				for (int i = 0; i < manageFeeList.size(); i++) {
					Element eleItem2 = (Element) manageFeeList.get(i);
					List Item1 = eleItem2.getChildren("Item1");
					System.out.println("================要素节点: " + Item1.size());
					for (int j = 0; j < Item1.size(); j++) {

						Element itemlist = (Element) Item1.get(j);
						tFeeCode = itemlist.getChildText("FeeCode");
						tFeeValue = itemlist.getChildText("FeeValue");
						String strSql = "select distinct GrpPolNo,GrpContNo,RiskCode,"
								+ "FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
								+ "FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
								+ "FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
								+ "CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
								+ "MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"
								+ GrpPolNo
								+ "' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"
								+ tRiskCode
								+ "' and AccType<>'002' union select '000000' from dual)";
						ExeSQL tExeSQL = new ExeSQL();
						SSRS AAArr = tExeSQL.execSQL(strSql);

						LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
						tLCGrpFeeSchema.setGrpPolNo(AAArr.GetText(j + 1, 1));
						tLCGrpFeeSchema.setGrpContNo(AAArr.GetText(j + 1, 2));
						tLCGrpFeeSchema.setRiskCode(AAArr.GetText(j + 1, 3));
						tLCGrpFeeSchema.setFeeCode(tFeeCode); // 报文获取的
						tLCGrpFeeSchema.setInsuAccNo(AAArr.GetText(j + 1, 5));
						tLCGrpFeeSchema.setPayPlanCode(AAArr.GetText(j + 1, 6));
						tLCGrpFeeSchema.setPayInsuAccName(AAArr.GetText(j + 1,
								7));
						tLCGrpFeeSchema.setFeeCalMode(AAArr.GetText(j + 1, 8));
						tLCGrpFeeSchema.setFeeCalModeType(AAArr.GetText(j + 1,
								9));
						tLCGrpFeeSchema.setFeeCalCode(AAArr.GetText(j + 1, 10));

						tLCGrpFeeSchema.setFeeValue(tFeeValue); // 报文获取的
						tLCGrpFeeSchema.setCompareValue(AAArr
								.GetText(j + 1, 13));
						tLCGrpFeeSchema.setFeePeriod(AAArr.GetText(j + 1, 14));
						tLCGrpFeeSchema.setMaxTime(AAArr.GetText(j + 1, 15));
						tLCGrpFeeSchema
								.setDefaultFlag(AAArr.GetText(j + 1, 16));
						tLCGrpFeeSchema.setOperator(AAArr.GetText(j + 1, 17));
						tLCGrpFeeSchema.setMakeDate(AAArr.GetText(j + 1, 18));
						tLCGrpFeeSchema.setMakeTime(AAArr.GetText(j + 1, 19));
						tLCGrpFeeSchema.setModifyDate(AAArr.GetText(j + 1, 20));
						tLCGrpFeeSchema.setModifyTime(AAArr.GetText(j + 1, 21));
						tLCGrpFeeSchema.setHouston(Houston);
						tLCGrpFeeSchema.setEntrustMoney(EntrustMoney);
						tLCGrpFeeSchema.setClaimNum(claimnum);
						tLCGrpFeeSet.add(tLCGrpFeeSchema);

						System.out.println("========================"
								+ AAArr.GetText(j + 1, 12));
						if ("理赔帐户管理费".equals(AAArr.GetText(j + 1, 11))) {
							cIndFeeValue = tFeeValue;
							System.out.println("================ceshi:"
									+ cIndFeeValue);
						}
					}
				}
			}
			// 处理个人管理费信息
			String strSql = "select distinct GrpPolNo,GrpContNo,RiskCode,"
					+ "FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
					+ "FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
					+ "FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
					+ "CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
					+ "MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"
					+ GrpPolNo
					+ "' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"
					+ tRiskCode + "' and AccType='002')";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS AAArr = tExeSQL.execSQL(strSql);

			LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
			tLCGrpFeeSchema.setGrpPolNo(AAArr.GetText(1, 1));
			tLCGrpFeeSchema.setGrpContNo(AAArr.GetText(1, 2));
			tLCGrpFeeSchema.setRiskCode(AAArr.GetText(1, 3));
			tLCGrpFeeSchema.setFeeCode(AAArr.GetText(1, 4)); // 报文获取的
			tLCGrpFeeSchema.setInsuAccNo(AAArr.GetText(1, 5));
			tLCGrpFeeSchema.setPayPlanCode(AAArr.GetText(1, 6));
			tLCGrpFeeSchema.setPayInsuAccName(AAArr.GetText(1, 7));
			tLCGrpFeeSchema.setFeeCalMode(AAArr.GetText(1, 8));
			tLCGrpFeeSchema.setFeeCalModeType(AAArr.GetText(1, 9));
			tLCGrpFeeSchema.setFeeCalCode(AAArr.GetText(1, 10));
			tLCGrpFeeSchema.setFeeValue(cIndFeeValue); // 报文获取的
			System.out.println("================ceshi:" + cIndFeeValue);
			tLCGrpFeeSchema.setCompareValue(AAArr.GetText(1, 13));
			tLCGrpFeeSchema.setFeePeriod(AAArr.GetText(1, 14));
			tLCGrpFeeSchema.setMaxTime(AAArr.GetText(1, 15));
			tLCGrpFeeSchema.setDefaultFlag(AAArr.GetText(1, 16));
			tLCGrpFeeSchema.setOperator(AAArr.GetText(1, 17));
			tLCGrpFeeSchema.setMakeDate(AAArr.GetText(1, 18));
			tLCGrpFeeSchema.setMakeTime(AAArr.GetText(1, 19));
			tLCGrpFeeSchema.setModifyDate(AAArr.GetText(1, 20));
			tLCGrpFeeSchema.setModifyTime(AAArr.GetText(1, 21));
			tLCGrpFeeSchema.setHouston(Houston);
			tLCGrpFeeSchema.setEntrustMoney(EntrustMoney);
			tLCGrpFeeSchema.setClaimNum(claimnum);
			tLCGrpFeeSet.add(tLCGrpFeeSchema);

			// ==================================================赔付顺序
			// =================================
			claimnum = ClaimNum;
			System.out
					.println("===============================================赔付顺序:"
							+ claimnum);

		}/* 
		else if ("HOUSTO||MAIN".equals(ftransact)) {
			String houston = Houston;
			String entrustM = EntrustMoney;
			String entrustMoney;
			if (entrustM == null || entrustM.equals("")) {
				entrustMoney = null;
			} else {
				int entrustM1 = Integer.parseInt(entrustM);
				entrustMoney = entrustM1 + "";
			}

		}*/
		 else if ("INTEREST||MAIN".equals(ftransact)) {
			/*
			 * 定义利息，暂时委托型险种不支持利息定义
			 */
			String strSql = "SELECT A.GRPPOLNO,A.GRPCONTNO,A.RISKCODE,A.INTERESTCODE,A.INSUACCNO,B.INSUACCNAME,A.INTERESTTYPE,A.DEFAULTCALTYPE,A.DEFAULTRATE FROM LCGRPINTEREST A,LMRISKINSUACC B WHERE A.GRPCONTNO='"
					+ tGrpContNo
					+ "' and a.RISKCODE='"
					+ tRiskCode
					+ "' and  a.INSUACCNO=b.INSUACCNO ";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS AAAAArr = tExeSQL.execSQL(strSql);
			if (AAAAArr.getMaxNumber() > 0) {
				for (int i = 0; i < AAAAArr.getMaxRow(); i++) {
					LCGrpInterestSet tLCGrpInterestSet = new LCGrpInterestSet();
					LCGrpInterestSchema tLCGrpInterestSchema = new LCGrpInterestSchema();
					tLCGrpInterestSchema.setGrpPolNo(AAAAArr.GetText(i + 1, 1));
					tLCGrpInterestSchema
							.setGrpContNo(AAAAArr.GetText(i + 1, 2));
					tLCGrpInterestSchema.setRiskCode(AAAAArr.GetText(i + 1, 3));
					tLCGrpInterestSchema.setInterestCode(AAAAArr.GetText(i + 1,
							4));
					tLCGrpInterestSchema
							.setInsuAccNo(AAAAArr.GetText(i + 1, 5));
					tLCGrpInterestSchema.setInterestType(AAAAArr.GetText(i + 1,
							8));
					tLCGrpInterestSchema.setDefaultRate(AAAAArr.GetText(i + 1,
							9));
					tLCGrpInterestSet.add(tLCGrpInterestSchema);
				}
			}
		}

		try {
			// 准备传输数据 VData
			VData tVData = new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
			tTransferData.setNameAndValue("RiskCode", tRiskCode);
			tTransferData.setNameAndValue("GrpPolNo", tGrpPolNo);
			tTransferData.setNameAndValue("InsuAccNo", tInsuAccNo);
			tTransferData.setNameAndValue("ClaimNum", claimnum);
			tVData.add(tTransferData);
			tVData.add(tLCInsuredListSet);
			tVData.add(tLCContPlanDutyParamSet);
			tVData.add(tLCGrpFeeSet);
			// tVData.add(tLCGrpInterestSet);
			tVData.add(tGI);
			tGrpPubAccUI.submitData(tVData, transact);
		} catch (Exception ex) {
			errorValue = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}

		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tGrpPubAccUI.mErrors;
			if (!tError.needDealError()) {
				errorValue = " 保存成功! ";
				FlagStr = "Success";
			} else {
				errorValue = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}

		return true;
	}
	/**
	 * 保存账户信息
	 * @return
	 */
	public boolean submitForm() {
		if (!checkDate()) {
			return false;
		}
		ftransact = "INSERT||MAIN";
		PubAccSave();
		return true;
	}
	
	/**
	 * 页面上管理费保存按钮
	 */
	public boolean saveManageFee() {
		if (!checkDate()) {
			return false;
		}

		// 调用后台
		ftransact = "MANAGEFEE||MAIN";
		PubAccSave();
		return true;

	}
	/**
	 *   委托基金信息定义  
	 *   是否进账
	 * @return
	 */
	public boolean saveHouston() {
		System.out
				.println("=================================================进账信息");
		//
		if (!checkDate()) {
			return false;
		}

		if (Houston == "" || Houston == null) {
			errorValue = ("险种" + RiskCode + "，是否进账必录！");
			return false;
		}
		if (EntrustMoney == "" || EntrustMoney == null) {
			errorValue = ("险种" + RiskCode + "，委托基金规模必录！");
			return false;
		}

		ftransact = "HOUSTO||MAIN";
		PubAccSave();
		return true;
	}

	public boolean saveInterest() {

		ftransact = "INTEREST||MAIN";
		PubAccSave();
		return true;
	}

	/**
	 * 校验保单下面是否有被保人
	 * 
	 * @return
	 */
	public boolean checkDate() {
		String StrSQL11 = "select 1 from lccont where Grpcontno='" + GrpContNo
				+ "' with ur ";
		SSRS resultArr = tExeSQL.execSQL(StrSQL11);
		if (resultArr.getMaxNumber() > 0) {
			errorValue = ("该保单已算费,请首先删除被保人、保障计划和账户信息后，再重新录入账户信息!");
			return false;
		}
		return true;

	}
}
