package com.contract;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.contract.obj.LCGrpContInfo;
import com.contract.obj.MessageHead;
import com.sinosoft.utility.VData;


/**
 * 
 * 生成返回报文，errorValue为收集的处理信息
 * 
 * @author LiHao
 *
 */
public class CreateXML {
	/**
	 * 
	 * 生成返回报文，errorValue为收集的处理信息
	 * 
	 * @param data
	 *            报文数据
	 * @param errorValue
	 *            处理时的信息
	 * @return
	 * @throws Exception
	 */
	public static String createXML(VData data, String errorValue, String xml) throws Exception {
		System.out.println("***********开始createXML生成返回报文*********");
		MessageHead vMessHead = (MessageHead) data.getObjectByObjectName("MessageHead", 0);
		LCGrpContInfo LCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		
		System.out.println("错误信息：" + errorValue);
		Element root = new Element("DataSet");
		
		Element eMsgHead = new Element("MsgHead");
		Element eHeadnode = new Element("Item");
		Element eHeadBatchNo = new Element("BatchNo");
		eHeadBatchNo.setText(vMessHead.getBatchNo());
		eHeadnode.addContent(eHeadBatchNo);
		Element eHeadSendDate = new Element("SendDate");
		eHeadSendDate.setText(vMessHead.getSendDate());
		eHeadnode.addContent(eHeadSendDate);
		Element eHeadSendTime = new Element("SendTime");
		eHeadSendTime.setText(vMessHead.getSendTime());
		eHeadnode.addContent(eHeadSendTime);
		Element eHeadBranchCode = new Element("BranchCode");
		eHeadBranchCode.setText(vMessHead.getBranchCode());
		eHeadnode.addContent(eHeadBranchCode);
		Element eHeadSendOperator = new Element("SendOperator");
		eHeadSendOperator.setText(vMessHead.getSendOperator());
		eHeadnode.addContent(eHeadSendOperator);
		Element eHeadMsgType = new Element("MsgType");
		eHeadMsgType.setText(vMessHead.getMsgType());
		eHeadnode.addContent(eHeadMsgType);
		if (!"".equals(errorValue) && null != errorValue) {
			Element eHeadState = new Element("State");
			eHeadState.setText("01");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrInfo = new Element("ActionInfo");
			eHeadErrInfo.setText(errorValue);
			eHeadnode.addContent(eHeadErrInfo);
		} else {
			Element eHeadState = new Element("State");
			eHeadState.setText("00");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrInfo = new Element("ActionInfo");
			eHeadErrInfo.setText("操作成功！");
			eHeadnode.addContent(eHeadErrInfo);
		}
		Element eHeadPrtNo = new Element("PrtNo");
		eHeadPrtNo.setText(LCGrpContInfo.getPrtNo());
		eHeadnode.addContent(eHeadPrtNo);
		
		eMsgHead.addContent(eHeadnode);
		root.addContent(eMsgHead);
		XMLOutputter out  = new XMLOutputter();
		out.setEncoding("GBK");
		Document doc = new Document(root);
		String responseXml = out.outputString(doc);
		return responseXml;

	}

}
