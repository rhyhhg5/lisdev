package com.contract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.contract.obj.LCGrpContInfo;
import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableUI;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDSysTraceSchema;
import com.sinosoft.lis.vschema.LDSysTraceSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;

/**
 * 团体复核通过
 * @author Administrator
 *
 */
public class DealGroupPolCheck {
	//错误信息
	private String Content = "";
	//从报文保存节点中获取
	//印刷号码  
	private String PrtNo="";
	//缴费频次
	private String GrpContPayIntv="";
	//大项目标识
	private String BigProjectFlag="";
	//市场类型 
	private String MarketType="";
	// 销售渠道
	private String SaleChnl="";
	//投保人名称（汇交人名称）
	private String GrpName="";
	//保险责任生效日 
	private String CValiDate="";
	//集团交叉销售渠道
	private String Crs_SaleChnl="";
	
	//查询出字段
	private String GrpContNo="";
	private String ProposalGrpContNo="";
	private String MissionID="";
	private String SubMissionID="";
	private String ManageCom="";
	private GlobalInput tGI = new GlobalInput();
	public boolean dealData(VData data){
		System.out.println("***********************************团体复核通过******************************");
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		tGI.Operator = "CENB";
		tGI.ManageCom = lCGrpContInfo.getManageCom();
		//原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
		tGI.ComCode = tGI.ManageCom;
		
		PrtNo = lCGrpContInfo.getPrtNo();
		GrpContPayIntv=lCGrpContInfo.getGrpContPayIntv();
		BigProjectFlag=lCGrpContInfo.getBigProjectFlag();
		MarketType=lCGrpContInfo.getMarketType();
		SaleChnl=lCGrpContInfo.getSaleChnl();
		GrpName=lCGrpContInfo.getGrpName();
		CValiDate=lCGrpContInfo.getCValiDate();
		Crs_SaleChnl=lCGrpContInfo.getCrs_SaleChnl();
		if(!deal()){
			return false;
		}
		return true;
	}
	public boolean deal(){
		if(!gmanuchk()){
			return false;
		}
		if(!dealGmanuchk()){
			return false;
		}
		return true;
	}
	/**
	 * 团体复核通过 前台校验
	 * @return
	 */
	public boolean gmanuchk(){
		//操作团体复核通过的人员  
	//	String operator="UW3624";
		String operator="CENB";
		String strSQL = "select lwmission.missionprop1,lwmission.missionprop2,(select codename from ldcode where codetype='salechnl' and code=lwmission.missionprop3),lwmission.missionprop7,lwmission.missionprop8,lwmission.missionid,lwmission.submissionid,case lwmission.missionprop20 when 'Y' then '问题件未修改' when  'N' then '问题件已修改' when 'N1' then '有新的问题件' else '没有问题件' end from lwmission where 1=1 "
				 + " and activityid = '0000002001' "
				 + " and  not exists (select GrpProposalNo  from lcgrppol a where a.prtno=missionprop2 and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G')) "
				 + " and processid = '0000000004' and missionprop2='"+PrtNo+"' "
				 + " order by lwmission.missionprop2" ;	
		ExeSQL tExeSQL=new ExeSQL();
		SSRS strSQLSSRS= tExeSQL.execSQL(strSQL);
		if(strSQLSSRS.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:没有查询到印刷号"+PrtNo+"可以进行团体复核通过的信息！";
			System.out.println(Content);
			return false;
		}
		String polNo = strSQLSSRS.GetText(1, 1);
		String prtNo = strSQLSSRS.GetText(1, 2);
		String cMissionID=strSQLSSRS.GetText(1, 6);
		String cSubMissionID =strSQLSSRS.GetText(1, 7);
		MissionID=cMissionID;
		SubMissionID=cSubMissionID;
		//by gzh 增加保单录入人员与复核人员不能为同一人的校验
		String OperatorSql = "select inputoperator from lcgrpcont where prtno = '"+prtNo+"' ";
		SSRS OperatorResult =tExeSQL.execSQL(OperatorSql);
		  if(OperatorResult.getMaxNumber()<=0){
			  	Content="团体复核通过操作失败，失败原因:获取保单录入人员信息失败！";
				System.out.println(Content);
				return false;
		  }
		  if("".equals(OperatorResult.GetText(1, 1))||null==OperatorResult.GetText(1, 1)){
			    Content="团体复核通过操作失败，失败原因:获取保单录入人员信息失败！";
				System.out.println(Content);
				return false;
		  }else if(operator.equals(OperatorResult.GetText(1, 1))){
			  	Content="团体复核通过操作失败，失败原因:该印刷号的投保单由您（"+OperatorResult.GetText(1, 1)+"）录入，您不能进行复核操作!";
				System.out.println(Content);
				return false;
		  }
		  String strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and  CreatePos='承保复核' and  PolState=1003 ";
		  SSRS arrResult = tExeSQL.execSQL(strSql);
		  if(arrResult.getMaxNumber()>0){
			  if(!operator.equals(arrResult.GetText(1, 2))){
				  Content="团体复核通过操作失败，失败原因:该印刷号的投保单已经被操作员（" + arrResult.GetText(1, 2) + "）在（" + arrResult.GetText(1, 6) + "）位置锁定！您不能操作，请选其它的印刷号！";
				  System.out.println(Content);
				  return false;
			  }
		  }
	//	  GlobalInput tG = new GlobalInput();	
		  //人员再定 xu
	//	  tG.Operator ="UW3624";
	//	  tG.ComCode="8636";
	//	  tG.ManageCom="8636";
		  
//		  tG.Operator ="CENB";
//		  tG.ComCode="86360500";
//		  tG.ManageCom="86360500";	
		  //锁印刷号
		  LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
		  tLDSysTraceSchema.setPolNo(PrtNo);
		  tLDSysTraceSchema.setCreatePos("承保复核");
		  tLDSysTraceSchema.setPolState("1003");
		  LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
		  inLDSysTraceSet.add(tLDSysTraceSchema);
		  VData VData3 = new VData();
		  VData3.add(tGI);
		  VData3.add(inLDSysTraceSet);
		  
		  LockTableUI LockTableUI1 = new LockTableUI();
		  if (!LockTableUI1.submitData(VData3, "INSERT")) {
		    VData rVData = LockTableUI1.getResult();
		    System.out.println("LockTable Failed! " + (String)rVData.get(0));
		  }
		  else {
		    System.out.println("UnLockTable Succed!");
		  }
		  
		  if(!checkSaleChnlInfo()){
		    	return false;
		    }
		    //gy  -2017-11-16
//		    if(!checkMarkettypeSalechnl()){
//		    	return false;
//		    }
		    
		    if(!checkRiskSalechnl()){
		    	return false;
		    }
		    //销售渠道和业务员的校验
		/*	if(!checkSaleChnl())
			{
				return false;
			}*/
			
			//中介机构的校验
			/*if(!checkAgentCom())
			{
				return false;
			}*/
			
		    //开办市县数和开办市县内容校验
//			if(!checkCity(2))
//			{
//				return false;
//			}
		    if(!checkCoInsuranceParams())
		    {
		        return false;
		    }
		    //gy BY -2018-03-14
		    if(!ChkPayIntv())
		    {
		        return false;
		    }
		    //校验被保人职业类别与职业代码
		    if(!checkOccTypeOccCode())
		    {
		    	return false;
		    }
		    //校验被保人性别
		    if(!CheckSex())
		    {
		    	return false;
		    }
		    
		    //校验集团交叉销售相互代理业务的销售渠道与中介机构是否符合描述
		    if(!checkCrsBussSaleChnl())
		    {
		    	return false;
		    }
		   
		    //by gzh 20110413  若存在没有算费的被保人，则阻断。
		    if(!checkAllCalPrem()){
		    	return false;
		    }
		  /*  //非阻断校验，校验缴费频次与保险区间是否匹配
		    if(!checkPayIntvMatchInsuYear())
		    {
		    	return false;
		    }
		    */
		    // gy 2017- 11-15
		    //缴费频次payintv=-1时，市场类型必须是 2,4,5,6,8,10,11
//		    if(!checkPayIntv()){
//		    	return false;
//		    }
		    
//		    if(!checkFirstPayPlan()){
//		    	return false;
//		    }
		    
		   /* //校验代理机构及代理机构业务员
		    if(!checkAgentComAndSaleCdoe()){
		    	return false;
		    }*/
		    
		    //校验建工险
		    if(!check590301()){
		    	return false;
		    }
		    
		    //校验健享全球表定
		    if(!checkB162001()){
		    	return false;
		    }
		    
		   /* //反洗钱
		    if(!checkFXQ()){
		    	return false;
		    }
		    */
		   /* //联系人联系电话对应不同投保人
		    if(!checkPhone1()){
		    	return false;
		    }*/
		    
		   /* if(!checkLGrp()){
		    	return false;
		    }*/
		    
		    //校验简易团单的缴费方式
		    if(!checkBriefGrp()){
		    	return false;
		    }
		    
		    //校验简易团单是否与险种匹配
		    if(!checkBriefGrpRisk()){
		    	return false;
		    }
		    
		    //商团大项目必录校验
		    if(!checkBigProject()){
		    	return false;
		    }
		    
		    //被保险人数校验
		    if(!checkPeople()){
		    	return false;
		    }
		    if(  null==ProposalGrpContNo ||"".equals(ProposalGrpContNo) ){
		    	Content="团体复核通过操作失败，失败原因:请选择团体主险投保单后，再进行复核操作";
				System.out.println(Content);
				return false;
		    }else{
		    	  //对约定缴费计划信息进行校验
		        if(!checkPayPlan()){
		        	return false;
		        }
		      
		        //校验结余返还---社保项目要素
		        // GY-by  2018-03-13
			String tGYSQL = "select salechnl,riskcode from lcgrppol where prtno = '"
					+ PrtNo + "'";
			ExeSQL tExeSQL1 = new ExeSQL();
			SSRS arr = tExeSQL1.execSQL(tGYSQL);
			if (arr.getMaxNumber() <= 0) {
				Content = "团体复核通过操作失败，失败原因:获取保单险种数据失败！";
				System.out.println(Content);
				return false;
			}
			for (int i = 1; i <= arr.getMaxRow(); i++) {
				String tSaleChnl = arr.GetText(i, 1);
				String tRiskCode = arr.GetText(i, 2);
				if (!"690201".equals(tRiskCode)) {
					if (!checkBalance()) {
						return false;
					}
				}
			}
		}
		        
		  
		  
		return true;
	}
	/**
	 * 团体复核通过 GroupPolApproveSave.jsp 调用后台处理代码
	 * @return
	 */
	public boolean dealGmanuchk(){
		 CErrors tError = null;
		 String FlagStr = "Fail";
		 
//		GlobalInput tG = new GlobalInput();
		//人员再定 xu
//	    tG.Operator ="UW3624";
//	    tG.ComCode="8636";
//	    tG.ManageCom="8636";
//	    tG.Operator ="CENB";
//	    tG.ComCode="86360500";
//	    tG.ManageCom="86360500";
		TransferData mTransferData=new TransferData();
		  mTransferData.setNameAndValue("GrpContNo",ProposalGrpContNo);
		  mTransferData.setNameAndValue("PrtNo",PrtNo);
		  mTransferData.setNameAndValue("SaleChnl",SaleChnl);
		  mTransferData.setNameAndValue("ManageCom",ManageCom);
		  mTransferData.setNameAndValue("GrpName",GrpName);
		  mTransferData.setNameAndValue("CValiDate",CValiDate);
		  mTransferData.setNameAndValue("MissionID",MissionID);
		  mTransferData.setNameAndValue("SubMissionID",SubMissionID);
			System.out.println("GrpContNo   =="+ProposalGrpContNo);
			System.out.println("PrtNo       =="+PrtNo);
			System.out.println("SaleChnl    =="+SaleChnl);
			System.out.println("ManageCom   =="+ManageCom);
			System.out.println("GrpName     =="+GrpName);
			System.out.println("CValiDate   =="+CValiDate);
			System.out.println("MissionID   =="+MissionID);
			System.out.println("SubMissionID=="+SubMissionID);
			//接收信息
			LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		    String tProposalGrpContNo = ProposalGrpContNo;
		    String flag="5";
		  	
			if( (null !=tProposalGrpContNo && !"".equals(tProposalGrpContNo))&(null!=flag && !"".equals(flag)) )
			{    
			 System.out.println("jsp中GrpPolNo:"+tProposalGrpContNo);

			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add( mTransferData );
			tVData.add( tGI );
			
			// 数据传输
			GrpTbWorkFlowUI tGrpTbWorkFlowUI= new GrpTbWorkFlowUI();
			//江苏团险校验
			boolean tJSFlag = false;
			{
				String tSaleChnl = SaleChnl;
				String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
						  	+ "and code = '"+tSaleChnl+"'";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				String tSubManageCom = ManageCom.substring(0,4);
				if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
					ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
			    	TransferData tJSTransferData = new TransferData();
			    	tJSTransferData.setNameAndValue("ContNo",GrpContNo);
			    	VData tJSVData = new VData();
			    	tJSVData.add(tJSTransferData);
			    	if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
			    		Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
			    		FlagStr = "Fail";
			    		tJSFlag = true;
			    	}
				}
			}
			if(!tJSFlag){
				if (tGrpTbWorkFlowUI.submitData( tVData, "0000002001" ) == false){
					Content = " 复核失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getError(0).errorMessage;
					FlagStr = "Fail";
				}
				//如果在Catch中发现异常，则不从错误类中提取错误信息
				if (FlagStr == "Fail"){
			    	tError = tGrpTbWorkFlowUI.mErrors;
			    	if (!tError.needDealError()){                          
			    		Content = " 复核成功! ";
			    		FlagStr = "Succ";
			    	}else{
			    		Content = " 复核失败，原因是:" + tError.getFirstError();
			    		FlagStr = "Fail";
			    		return false;
			    	}
				}
			}
		  }
		return true;
 
	}
	public boolean checkSaleChnlInfo(){
		String tSQL = "select managecom,agentcom,agentcode,salechnl,grpcontno,proposalgrpcontno,agentcode ,agentgroup from lcgrpcont where prtno = '"+PrtNo+"'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arr =tExeSQL.execSQL(tSQL);
		if(arr.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:获取保单数据失败！";
			System.out.println(Content);
			return false;
		}
		String tManageCom = arr.GetText(1, 1);
		String tAgentCom = arr.GetText(1,2);
		String tAgentCode = arr.GetText(1, 3);
		String tSaleChnl = arr.GetText(1, 4);
		GrpContNo=arr.GetText(1, 5);
		ProposalGrpContNo=arr.GetText(1, 6);
		//AgentCode=arr.GetText(1, 7);
		//AgentGroup=arr.GetText(1, 8);
		ManageCom=tManageCom;
		String tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
		SSRS arrCode =tExeSQL.execSQL(tSQLCode);
		if(arrCode.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:业务员与管理机构不匹配！";
			System.out.println(Content);
			return false;
		}
		if(tSaleChnl == "02" || tSaleChnl == "03"){
			String agentCodeSql = " select 1 from LAAgent a where a.AgentCode = '"+ tAgentCode+ "'" 
		                 + " and a.BranchType = '2' and a.BranchType2 in ('01','02') "
		                 + " union all "
						 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
						 + " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
		    SSRS arrAgentCode =tExeSQL.execSQL(agentCodeSql) ;
		    if(arrAgentCode.getMaxNumber()<=0){
		    	Content="团体复核通过操作失败，失败原因:业务员和销售渠道不匹配！";
				System.out.println(Content);
				return false;
		    }
		}else{
			String agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
		                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
	                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
	                     + " union all "
						 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
						 + " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
		    SSRS arrAgentCode =tExeSQL.execSQL(agentCodeSql);
		    if(arrAgentCode.getMaxNumber()<=0){
		    	Content="团体复核通过操作失败，失败原因:业务员和销售渠道不匹配！";
				System.out.println(Content);
				return false;
		    }
		}
		if(tAgentCom != "" && tAgentCom != null && tAgentCom != "null"){
			String tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
			SSRS arrCom =tExeSQL.execSQL(tSQLCom);
			if(arrCom.getMaxNumber()<=0){
				Content="团体复核通过操作失败，失败原因:中介机构与管理机构不匹配！";
				System.out.println(Content);
				return false;
			}
			String tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
			SSRS arrComCode = tExeSQL.execSQL(tSQLComCode);
			if(arrComCode.getMaxNumber()<=0){
				Content="团体复核通过操作失败，失败原因:业务员与中介机构不匹配！";
				System.out.println(Content);
				return false;
			}
		}
		return true;
	}
	public boolean checkMarkettypeSalechnl(){
		String tSQL = "select salechnl,markettype from lcgrpcont where prtno = '"+PrtNo+"'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS  arr =tExeSQL.execSQL(tSQL);
		if(arr.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:获取保单数据失败！";
			System.out.println(Content);
			return false;
		}
		String tSaleChnl = arr.GetText(1, 1);
		String tMarketType = arr.GetText(1, 2);
		String tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+tMarketType+"' and code1 = '"+tSaleChnl+"' ";
		SSRS arrCheck = tExeSQL.execSQL(tcheckSQL);
		if(arrCheck.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:市场类型和销售渠道不匹配，请核查！";
			System.out.println(Content);
			return false;
		}
		return true;
	}
	public boolean checkRiskSalechnl(){

		String tSQL = "select salechnl,riskcode from lcgrppol where prtno = '"+PrtNo+"'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arr = tExeSQL.execSQL(tSQL);
		if(arr.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:获取保单险种数据失败！";
			System.out.println(Content);
			return false;
		}
		for(int i=1;i<=arr.getMaxRow();i++){
			String tSaleChnl = arr.GetText(i, 1);
			String tRiskCode = arr.GetText(i, 2);
			String tcheckSQL = "select codename from ldcode where codetype = 'unitesalechnlrisk' and code = '"+tRiskCode+"' ";
			SSRS arrCheck = tExeSQL.execSQL(tcheckSQL);
			if(arrCheck.getMaxNumber()<=0){
				if( "16".equals(tSaleChnl)){
					Content="团体复核通过操作失败，失败原因:险种"+tRiskCode+"为非社保险种，与销售渠道"+tSaleChnl+"不匹配！";
					System.out.println(Content);
					return false;
				}
			}else{
				if(!"all".equals(arrCheck.GetText(1, 1)) && !tSaleChnl.equals(arrCheck.GetText(1, 1))){
					Content="团体复核通过操作失败，失败原因:险种"+tRiskCode+"与销售渠道"+tSaleChnl+"不匹配！";
					System.out.println(Content);
					return false;
				}
			}
			//add by zjd 特需险校验赔付顺序为必录
			String triskclaimnumsql=" select riskcode from lmriskapp where risktype3='7' and riskcode='"+tRiskCode+"'  ";
		    SSRS arrclaim = tExeSQL.execSQL(triskclaimnumsql);
		    if(arrclaim.getMaxNumber()>0){
		    	String tlcgrpfeesql=" select 1 from lcgrpfee lgf,lcgrppol lgp  where lgf.grppolno=lgp.grppolno and lgp.prtno='"+PrtNo+"' and lgp.riskcode='"+tRiskCode+"' and ClaimNum is not null ";
		    	SSRS arrclaimnum = tExeSQL.execSQL(tlcgrpfeesql);
		    	if(arrclaimnum.getMaxNumber()<=0){
		    		Content="团体复核通过操作失败，失败原因:险种"+tRiskCode+"为特需险，赔付顺序必录！\n 请到账户定义界面定义管理下录入赔付顺序！";
					System.out.println(Content);
					return false;
		    	}
		    }
		    //add by zjd 补充工伤团体失能损失保险B款 伤残等级给付比例
		    String tgrpSQL = "select grpcontno from lcgrpcont where prtno = '"+PrtNo+"'";
			SSRS grparr =tExeSQL.execSQL(tgrpSQL);
		    if("460301".equals(tRiskCode)){
		    	String tgraderatesql=" Select Distinct a.contplancode,a.Dutycode From Lccontplandutyparam a Where a.riskcode='460301' and grpcontno ='"+grparr.GetText(1, 1)+"' and ContPlanCode not in ('00','11') ";
		    	SSRS tgraderate = tExeSQL.execSQL(tgraderatesql);
		    	if(tgraderate.getMaxNumber()>0){
		    		for(int j=1;j<=tgraderate.getMaxRow();j++){
		    			String tratechksql=" select 1 from LCContPlanDutyDefGrade where grpcontno='"+grparr.GetText(1, 1)+"' and contplancode='"+tgraderate.GetText(1, 1)+"' and dutycode='"+tgraderate.GetText(1, 2)+"' and GetRate > 0 ";
		    			SSRS tratechk = tExeSQL.execSQL(tratechksql);
		    			if(tratechk.getMaxNumber()<=0){
		    				Content="团体复核通过操作失败，失败原因:保障计划"+tgraderate.GetText(1, 1)+"下责任"+tgraderate.GetText(1, 2)+" 未录入伤残等级对应的给付比例。\n 请到保障计划下的制定伤残等级给付比例中录入给付比例！";
		    				System.out.println(Content);
		    				return false;
		    			}
		    		}
		    	}
		    }
		}
		return true;
	}
	/**
	 * 校验共保保单的相关要素是否齐全。
	 */
	public boolean checkCoInsuranceParams()
	{
	    String tStrSql = ""
	        + " select 1 "
	        + " from LCGrpCont lgc "
	        + " left join LCIGrpCont lcigc on lgc.GrpContNo = lcigc.GrpContNo "
	        + " left join LCCoInsuranceParam lcip on lcip.GrpContNo = lcigc.GrpContNo "
	        + " where 1 = 1 "
	        + " and lgc.GrpContNo = '" +GrpContNo+ "' "
	        + " and lgc.CoInsuranceFlag = '1' "
	        + " and lcigc.GrpContNo is null "
	        + " and lcip.GrpContNo is null "
	        ;
	    ExeSQL tExeSQL=new ExeSQL();
	    SSRS arr = tExeSQL.execSQL(tStrSql);
	    if(arr.getMaxNumber()>0)
	    {
	    	Content="团体复核通过操作失败，失败原因:该单为共保保单，但共保要素信息不全，请进行确认。";
			System.out.println(Content);
			return false;
	    }
	    
	    return true;
	}
	//添加缴费频次的校验
	public boolean ChkPayIntv()
	{
		String tPayInv = GrpContPayIntv;
		String strSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
				 +" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+GrpContNo+"' "
				 +" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
				 +" (select sum(Peoples2) from lccontplan where GrpContNo='"+GrpContNo+"' "
				 +" and ContPlancode in (select distinct contplancode from lccontplanrisk where "
				 +" grpcontno='"+GrpContNo+"' and riskcode=a.riskcode)), "
				 +" a.peoples2, "
				 +" to_zero((select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno )) "
				 +" From lcgrppol a,LMRiskApp b "
				 +" Where b.riskcode not in (select Riskcode from LCRiskWrap where Grpcontno=a.GrpContNo) and  a.riskcode=b.riskcode   and  a.grpcontno='" +GrpContNo+"'  "
				 +" union "
				 +" select a,b,c,d,e, "                                                                                                         
				 +" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
				 +"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+GrpContNo+"' "                                      
				 +"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
				 +" (select sum(Peoples2) from lccontplan where GrpContNo='"+GrpContNo+"'  and ContPlancode "                                      
				 +" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+GrpContNo+"' "                                        
				 +" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
				 +" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
				 +"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
				 +" and a.Grpcontno='"+GrpContNo+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  ;
		ExeSQL tExeSQL=new ExeSQL();
		SSRS RiskGrid=tExeSQL.execSQL(strSql);
		if(RiskGrid.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:险种信息查询失败。";
			System.out.println(Content);
			return false;
		}
		for (int m=1; m<=RiskGrid.getMaxRow(); m++)
		{
		    String tRiskCode = RiskGrid.GetText(m, 1);
		    String strSQL = "select 1 from LMRiskPayIntv where riskcode = '"+tRiskCode+"'";
		    SSRS arrResult = tExeSQL.execSQL(strSQL);
		    if(arrResult.getMaxNumber()>0)
		    {
		        String strSql1 = "select 1 from LMRiskPayIntv where "
								+" riskcode ='"+tRiskCode+"'"
								+" and PayIntv='"+tPayInv+"'";
				  
				SSRS arr = tExeSQL.execSQL(strSql1);
				if(arr.getMaxNumber()<=0)
				{
					String sql="select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where 1 = 1 and codetype = 'grppayintv' and Code= '"+GrpContPayIntv+"'order by Code fetch first 3000 rows only with ur ";
					SSRS sqlSSRS=tExeSQL.execSQL(sql);
					String GrpContPayIntvName=sqlSSRS.GetText(1, 2);
					Content="团体复核通过操作失败，失败原因:险种"+tRiskCode+"的缴费频次不支持"+GrpContPayIntvName+",请修改缴费频次并且保存至数据库.";
					System.out.println(Content);
					return false;
				}
		    }
		}
		return true;
	}
	//校验被保人职业类别与职业代码
	public boolean checkOccTypeOccCode()
	{
		String sql = "select 1 from lcpol where poltypeflag = '0' and prtno = '" +PrtNo+ "'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS result = tExeSQL.execSQL(sql);
		if(result.getMaxNumber()>0)
		{
			String strSql = "select distinct OccupationType,OccupationCode from lcinsured "
						+ " where prtno = '" +PrtNo+ "'";
			SSRS arrResult = tExeSQL.execSQL(strSql);
			
			if(arrResult.getMaxNumber()>0 ) 
			{
				for(int i = 0; i <= arrResult.getMaxRow(); i++)
				{
					String OccupationType = arrResult.GetText(i, 1);
					String OccupationCode = arrResult.GetText(i, 2);
					
					if(null != OccupationType && !"".equals(OccupationType))
					{
						if(!CheckOccupationType(OccupationType))
						{
							return false;
						}
					}
					
					if( null != OccupationCode && !"".equals(OccupationCode) )
					{
						
						if(!CheckOccupationCode(OccupationType,OccupationCode))
						{
							return false;
						}
						
					}
				}
			}
		}
		return true;
	}
	//校验职业类别
	public boolean CheckOccupationType(String Type)
	{
		String strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrResult = tExeSQL.execSQL(strSql);
		if(arrResult.getMaxNumber()<=0 )
		{
			Content="团体复核通过操作失败，失败原因:被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6";
			System.out.println(Content);
			return false;
		} 
		return true;
	}
	//校验职业代码
	public boolean CheckOccupationCode(String Type,String Code)
	{
		String strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrResult = tExeSQL.execSQL(strSql);
		if(arrResult == null)
		{
			Content="团体复核通过操作失败，失败原因:被保人有职业类别和职业代码与系统描述不一致，请查看！";
			System.out.println(Content);
			return false;
		} 
		return true;
	}
	//校验被保人性别
	public boolean CheckSex()
	{
		String strSql = "select distinct Sex from lcinsured where prtno = '" +PrtNo+ "'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrResult = tExeSQL.execSQL(strSql);
		if(arrResult.getMaxNumber()>0)
		{
			for(int i = 1; i <= arrResult.getMaxRow(); i++)
			{
				String sex = arrResult.GetText(i, 1);
				if(!"0".equals(sex) && !"1".equals(sex) && !"2".equals(sex))
				{
					Content="团体复核通过操作失败，失败原因:被保人的性别填写有误，请查看。\n性别必须是半角数字的0、1、2 ，规则是0-男，1-女，2-其他！";
					System.out.println(Content);
					return false;
				}
			}
		}
		return true;
	}
	//校验标准团险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   by 赵庆涛 2016-06-28
	public boolean checkCrsBussSaleChnl()
	{
		if( null!= Crs_SaleChnl && !"".equals(Crs_SaleChnl))
		{
			String strSQL = "select 1 from lcgrpcont lgc where 1 = 1"  
				+ " and lgc.Crs_SaleChnl is not null " 
				+ " and lgc.Crs_BussType = '01' " 
				+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
				+ " and lgc.PrtNo = '" +PrtNo+ "' ";
			ExeSQL tExeSQL=new ExeSQL();
			SSRS arrResult = tExeSQL.execSQL(strSQL);
	      if (arrResult.getMaxNumber()>0)
	      {
	    	  	Content="录入完毕操作失败，失败原因:选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！";
				System.out.println(Content);
				return false;
	      }
		}
		return true;
	}
	public boolean checkAllCalPrem(){
		  String strSQL = "select insuredname,sex,birthday,idtype,idno from lcinsuredlist where grpcontno = '"+GrpContNo+"' and state = '0'";  
		  ExeSQL tExeSQL=new ExeSQL();
		  SSRS arrResult = tExeSQL.execSQL(strSQL);
		  if (arrResult.getMaxNumber()>0 )
		  {
		  	for(int i=1;i<=arrResult.getMaxRow();i++){
		  		String strSQL1 = "select * from lcinsured where grpcontno = '+mGrpContNo+'"
		  		            + " and name = '"+arrResult.GetText(i, 1)+"' "
		  		            + " and sex = '"+arrResult.GetText(i, 2)+"' "
		  		            + " and birthday = '"+arrResult.GetText(i, 3)+"' "
		  		            + "and idtype = '"+arrResult.GetText(i, 4)+"' "
		  		            + "and idno = '"+arrResult.GetText(i, 5)+"'";
		  		SSRS arrResult1 = tExeSQL.execSQL(strSQL1);
		  		if(arrResult1.getMaxNumber()<=0) {
		  			Content="团体复核通过操作失败，失败原因:该保单存在未计算保费的被保人，请先进行计算保费操作！";
		  			System.out.println(Content);
		  			return false;
		  		}
		  	}
		  }
		  return true;
		}
	public boolean checkPayIntv()
	{
		String tSql = "select 1 from lcgrpcont where prtno = '"+PrtNo+"' and payintv = -1 and markettype not in (select code from ldcode where codetype = 'markettypeyd') ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arr = tExeSQL.execSQL(tSql);
		if(arr.getMaxNumber()>0){
			Content="团体复核通过操作失败，失败原因:缴费频次与市场类型不匹配，请核查！";
			System.out.println(Content);
			return false;
		}
		return true;
	}
	public boolean checkFirstPayPlan()
	{
		String tSql = "select 1 from lcgrpcont where prtno = '"+PrtNo+"' and payintv = -1 ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arr = tExeSQL.execSQL(tSql);
		if(arr.getMaxNumber()>0){
			String tStrSql = "select contplancode, prem from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' and plancode = '1' order by contplancode " ;
			SSRS arrPayPlan = tExeSQL.execSQL(tStrSql);
			if(arrPayPlan.getMaxNumber()<=0){
				Content="团体复核通过操作失败，失败原因:该单缴费频次为约定缴费，请录入约定缴费计划信息！";
				System.out.println(Content);
				return false;
			}
			String tPolSql = "select contplancode,sum(prem) from lcpol where prtno = '"+PrtNo+"' group by contplancode ";
			SSRS arrPol = tExeSQL.execSQL(tPolSql);
			if(arrPol.getMaxNumber()<=0){
				Content="团体复核通过操作失败，失败原因:获取险种信息失败！";
				System.out.println(Content);
				return false;
			}
			for(int i=1;i<=arrPayPlan.getMaxRow();i++){
				for(int j=1;j<=arrPol.getMaxRow();j++){
					if(arrPayPlan.GetText(i, 1).equals(arrPol.GetText(j, 1)) && !Float.valueOf(arrPayPlan.GetText(i, 2)).equals(Float.valueOf(arrPol.GetText(j, 2))))
					{
						Content="团体复核通过操作失败，失败原因:保障计划["+arrPayPlan.GetText(i, 1)+"]的首期保费["+arrPol.GetText(j, 2)+"]与约定缴费计划的首期保费["+arrPayPlan.GetText(i, 2)+"]不符！";
						System.out.println(Content);
						return false;
					}
				}
			}
		}
		return true;
	}
	//by gzh 20120910 
	//建工险590301保费计算方式为按被保险人人数计算时
	//1、保险期间不可以大于1年
	//2、必须实名投保
	public boolean check590301(){
		String tSql = " select 1 from LCContPlanDutyParam "
				 + " where ProposalGrpContNo = '"+ProposalGrpContNo+"' "
				 + " and riskcode = '590301' "
				 + " and calfactor = 'StandbyFlag1' "
				 + " and calfactorvalue = '3' ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arr = tExeSQL.execSQL(tSql);
		if(arr.getMaxNumber()>0){
			//保险期间不可以大于1年1400365686 
			String tSqlGrpCont = " select Cvalidate,Cinvalidate from LCGrpCont "
				 + " where ProposalGrpContNo = '"+ProposalGrpContNo+"' ";
			SSRS arrGrpCont = tExeSQL.execSQL(tSqlGrpCont);
			if(arrGrpCont.getMaxNumber()>0){
				if(null!=arrGrpCont.GetText(1, 2)&&!"".equals(arrGrpCont.GetText(1, 2))&&null!=arrGrpCont.GetText(1, 1)&&!"".equals(arrGrpCont.GetText(1, 1)) ){
				    String Cvalidate = arrGrpCont.GetText(1, 1);
				    String Cinvalidate=arrGrpCont.GetText(1, 2);
			        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			        Date tCvalidate;
			        Date tCinvalidate;
			        long diff = 0;
					try {
						tCvalidate =  df.parse(Cvalidate);
				        tCinvalidate = df.parse(Cinvalidate);
				        diff = (tCinvalidate.getTime() - tCvalidate.getTime())/(1000*3600*24);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Content=e.getMessage();
					   	return false;
					}
			        if(diff<365){
			        	Content="团体复核通过操作失败，失败原因:承保产品新版建工险590301，保费计算方式为按被保人方式计算，保险期间应小于等于1年！";
						System.out.println(Content);
						return false;
			        }
				}
			}else{
				Content="团体复核通过操作失败，失败原因:获取保单保险期间失败！";
				System.out.println(Content);
				return false;
			}
			//必须实名投保
			String tSqlCont = " select 1 from lccont where prtno = '"+PrtNo+"' and poltype = '1'";
			SSRS arrCont = tExeSQL.execSQL(tSqlCont);
			if(arrCont.getMaxNumber()>0){
				Content="团体复核通过操作失败，失败原因:承保产品新版建工险590301，保费计算方式为按被保人方式计算，必须实名承保！ ";
				System.out.println(Content);
				return false;
			}
		}
		return true;
	}
	//by gzh 20121031
	//健享全球表定费率必须实名承保
	public boolean checkB162001(){
		 String tSQL= " select 1 from LCContPlanDutyParam where ProposalGrpContNo='"
					+ProposalGrpContNo+ "' and contplancode='11' and riskcode='162001' " 
	         	+ " and calfactor = 'CalRule' and calfactorvalue = '0'" ;
		 ExeSQL tExeSQL=new ExeSQL();
		 SSRS tArr=tExeSQL.execSQL(tSQL);
			if(tArr.getMaxNumber()>0){
				//必须实名投保
				String tSqlCont = " select 1 from lccont where prtno = '"+PrtNo+"' and poltype = '1'";
				SSRS arrCont = tExeSQL.execSQL(tSqlCont);
				if(arrCont.getMaxNumber()>0){
					Content="团体复核通过操作失败，失败原因:承保产品健享全球162001，保费计算方式为表定费率，必须实名承保！ ";
					System.out.println(Content);
					return false;
				}
			}
		return true;
	}
	public boolean checkBriefGrp(){
		String tContPrintTypeSQL = "select payintv from lcgrpcont where prtno = '"+PrtNo+"' and ContPrintType = '4' ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS tContPrintTypeArr = tExeSQL.execSQL(tContPrintTypeSQL);
		if(tContPrintTypeArr.getMaxNumber()>0){
			if(!"0".equals(tContPrintTypeArr.GetText(1, 1))){
				Content="团体复核通过操作失败，失败原因:简易团单的缴费方式必须为趸交！ ";
				System.out.println(Content);
				return false;
			}
		}
		return true;
	}
	public boolean checkBriefGrpRisk(){
		String tFlagSQL = "select grpcontno from lcgrpcont where prtno = '"+PrtNo+"' and ContPrintType = '4' ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS tFlagSQLArr = tExeSQL.execSQL(tFlagSQL);
		if(tFlagSQLArr.getMaxNumber()>0){
			String tRiskSQL = "select 1 from lcgrppol where prtno = '"+PrtNo+"' ";
			SSRS tRiskArr = tExeSQL.execSQL(tRiskSQL);
			if(tRiskArr.getMaxNumber()>0){
				String tRiskFlagSQL = "select 1 from lcgrppol lgp where lgp.prtno = '"+PrtNo+"' "
								 + " and not exists (select 1 from ldcode where codetype = 'grpbriefrisk' and code = lgp.riskcode and codename = substr(lgp.managecom,1,4) ) ";
				SSRS tRiskFlagArr = tExeSQL.execSQL(tRiskFlagSQL);
				if(tRiskFlagArr.getMaxRow()>0){
					Content="团体复核通过操作失败，失败原因:该单为简易团单，简易团单不支持已添加的险种，请修改保单属性或险种代码！";
					System.out.println(Content);
					return false;
				}
				String tContPlanSQL = "select distinct contplancode,riskcode from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and contplancode != '11' ";
				SSRS tContPlanArr = tExeSQL.execSQL(tContPlanSQL);
				if(tContPlanArr.getMaxNumber()>0){
					for(int i=1;i<=tContPlanArr.getMaxRow();i++){
						String tContPlanCode = tContPlanArr.GetText(i, 1);
						String tRiskCode = tContPlanArr.GetText(i, 2);
						String tInsuPlanCodeSQL = "select distinct calfactorvalue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and contplancode = '"+tContPlanCode+"' and riskcode = '"+tRiskCode+"' and calfactor like 'InsuPlanCode%' ";
						SSRS tInsuPlanCodeArr = tExeSQL.execSQL(tInsuPlanCodeSQL);
						if(tInsuPlanCodeArr.getMaxNumber()<=0){
							Content="团体复核通过操作失败，失败原因:该单为简易团单，保障计划"+tContPlanCode+"中,险种["+tRiskCode+"]必须选择方案编码，请修改保障计划！";
							System.out.println(Content);
							return false;
						}
					}
				}else{
					Content="团体复核通过操作失败，失败原因:获取保障计划信息失败！";
					System.out.println(Content);
					return false;
				}
			}else{
				Content="团体复核通过操作失败，失败原因:获取险种信息失败！";
				System.out.println(Content);
				return false;
			}
			
			String tCalRuleSQL = "select distinct CalFactorValue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and CalFactor = 'CalRule' ";
			SSRS tCalRuleArr = tExeSQL.execSQL(tCalRuleSQL);
			if(tCalRuleArr.getMaxNumber()>0){
				if(tCalRuleArr.getMaxRow() >1){
					Content="团体复核通过操作失败，失败原因:该单为简易团单，保费计算方式必须为[3-约定费率]！";
					System.out.println(Content);
					return false;
				}
				if(!"3".equals(tCalRuleArr.GetText(1, 1))){
					Content="团体复核通过操作失败，失败原因:该单为简易团单，保费计算方式必须为[3-约定费率]！";
					System.out.println(Content);
					return false;
				}
			}else{
				Content="团体复核通过操作失败，失败原因:获取保障计划算费要素失败！";
				System.out.println(Content);
				return false;
			}
			
			String tInsuPlanSQL = "select distinct contplancode,CalFactorValue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and CalFactor like 'InsuPlanCode%' ";
			SSRS tInsuPlanSArr = tExeSQL.execSQL(tInsuPlanSQL);
			if(tInsuPlanSArr.getMaxNumber()>0){
				for(int i=1;i<=tInsuPlanSArr.getMaxRow();i++){
					String tAllInsuRiskSQL = "select code1 from ldcode1 where codetype = 'grpbrieinsuplan' and code = '"+tInsuPlanSArr.GetText(i, 2)+"' ";
					SSRS tAllInsuRiskArr = tExeSQL.execSQL(tAllInsuRiskSQL);
					if(tAllInsuRiskArr.getMaxNumber()<=0){
						Content="团体复核通过操作失败，失败原因:简易团单，获取方案对应的险种信息失败！";
						System.out.println(Content);
						return false;
					}
					String tAllRiskSQL = "select distinct riskcode from lccontplandutyparam where grpcontno = '"+tFlagSQLArr.GetText(1, 1)+"' and contplancode = '"+tInsuPlanSArr.GetText(i, 1)+"' ";
					SSRS tAllRiskArr =tExeSQL.execSQL(tAllRiskSQL);
					if(tAllRiskArr.getMaxNumber()<=0){
						Content="团体复核通过操作失败，失败原因:简易团单，获取保障计划对应的险种信息失败！";
						System.out.println(Content);
						return false;
					}
					for(int m=1;m<=tAllInsuRiskArr.getMaxRow();m++){
						String tFlag = "0";
						for(int n=1;n<=tAllRiskArr.getMaxRow();n++){
							if(tAllInsuRiskArr.GetText(m, 1).equals(tAllRiskArr.GetText(n, 1))){
								tFlag = "1";
								break;
							}
						}
						if(!"1".equals(tFlag)){
							Content="团体复核通过操作失败，失败原因:简易团单，方案编码["+tInsuPlanSArr.GetText(i, 2)+"]对应的险种["+tAllInsuRiskArr.GetText(m, 1)+"]在保障计划["+tInsuPlanSArr.GetText(i, 1)+"]中不存在，请核查！";
							System.out.println(Content);
							return false;
						}
					}
				}
			}else{
				Content="团体复核通过操作失败，失败原因:该单为简易团单，必须选择方案编码，请修改保障计划！";
				System.out.println(Content);
				return false;
			}
		}
		return true;
	}
	public boolean checkPeople(){
		String tPrtNo = PrtNo;
		String tSqlCount="select peoples2 from LCGrpCont where PrtNo='" + tPrtNo + "'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrCount = tExeSQL.execSQL(tSqlCount);
		if(Integer.valueOf(arrCount.GetText(1, 1))<3){
			Content="团体复核通过操作失败，失败原因:被保险人数应大于等于3！";
			System.out.println(Content);
			return false;
		}
		return true;
	}
	public boolean checkBigProject(){
		String tMarketType = MarketType;
		String tBigProjectFlag =BigProjectFlag;
		String tPrtNo = PrtNo;
		String tSQL = "select 1 from lcbigprojectcont where prtno='"+tPrtNo+"'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arr = tExeSQL.execSQL(tSQL);
		if("1".equals(tMarketType) ||"9".equals(tMarketType)){
			if("1".equals(tBigProjectFlag)){
				if(arr.getMaxNumber()<=0){
					Content="团体复核通过操作失败，失败原因:印刷号为："+tPrtNo+"的保单必须选择或录入一个大项目！";
					System.out.println(Content);
					return false;
				}
			}
		}
		return true;
	}
	//by gzh 增加约定缴费计划首期和总保费的校验,若缴费频次非约定缴费，则不可存在约定缴费计划
	public boolean checkPayPlan()
	{
		String tPayIntvSql = "select 1 from lcgrpcont where prtno = '"+PrtNo+"' and payintv = -1 ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS tPayIntvArr = tExeSQL.execSQL(tPayIntvSql);
		if(tPayIntvArr.getMaxNumber()>0){//约定缴费
			String tPremScopeSql = "select PremScope from lcgrpcont where prtno = '"+PrtNo+"' ";
			SSRS tPremScopeArr = tExeSQL.execSQL(tPremScopeSql);
			String sumPrem = tPremScopeArr.GetText(1, 1);
			if(null==sumPrem ||"".equals(sumPrem) ||"0".equals(sumPrem)){
				Content="团体复核通过操作失败，失败原因:缴费频次为约定缴费，必须录入保费合计！";
				System.out.println(Content);
				return false;
			}
			if(!checkPayAndCin()){
				return false;
			}
			String tGetPolPremSql = "select sum(prem) from lcpol where prtno = '"+PrtNo+"' ";
			SSRS tGetPolPremArr = tExeSQL.execSQL(tGetPolPremSql);
			if(tGetPolPremArr.getMaxNumber()<=0){
				Content="团体复核通过操作失败，失败原因:查询首期保费失败！";
				System.out.println(Content);
				return false;
			}
			String tGetPlanPremSql = "select sum(prem) from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' ";
			SSRS tGetPlanPremArr = tExeSQL.execSQL(tGetPlanPremSql);
			if(tGetPlanPremArr.getMaxNumber()<=0){
				Content="团体复核通过操作失败，失败原因:查询约定缴费计划总保费失败！";
				System.out.println(Content);
				return false;
			}
//			if(!sumPrem.equals(tGetPlanPremArr.GetText(1, 1))){
//				Content="团体复核通过操作失败，失败原因:缴费资料->保费合计”与约定缴费计划中录入的总金额不符,请核对!";
//				System.out.println(Content);
//				return false;
//			}
	 }else{
	 		String tPayPlanSql = "select 1 from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"'";
			SSRS tPayPlanArr = tExeSQL.execSQL(tPayPlanSql);
			if(tPayPlanArr.getMaxNumber()>0){
				String tGetPanIntvSQL = "select payintv,(select codename from ldcode where codetype = 'grppayintv' and code = char(lgc.payintv)) from lcgrpcont lgc where lgc.prtno = '"+PrtNo+"' ";
				SSRS tGetPanIntvArr = tExeSQL.execSQL(tGetPanIntvSQL);
				if(tGetPanIntvArr.getMaxNumber()<0){
					Content="团体复核通过操作失败，失败原因:获取缴费频次名称失败  ！";
					System.out.println(Content);
					return false;
				}
				Content="团体复核通过操作失败，失败原因:缴费方式为["+tGetPanIntvArr.GetText(1, 2)+"],请先删除约定缴费计划！";
				System.out.println(Content);
				return false;
			}
	 }
	 return true;
	}
	public boolean checkPayAndCin()
	{
		String tCinSql = "select cinvalidate from lcgrpcont where prtno = '"+PrtNo+"'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS tCinArr = tExeSQL.execSQL(tCinSql);
		if(tCinArr.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:获取保单终止日期失败！";
			System.out.println(Content);
			return false;
		}
		if(null==tCinArr.GetText(1, 1)||"".equals(tCinArr.GetText(1, 1))){
			Content="团体复核通过操作失败，失败原因:获取保单终止日期失败！";
			System.out.println(Content);
			return false;
		}
		String tSql = "select plancode,paytodate from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' group by plancode,paytodate ";
		SSRS arr = tExeSQL.execSQL(tSql);
		if(arr.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:获取约定缴费时间失败！";
			System.out.println(Content);
			return false;
		}
		for(int i=1;i<=arr.getMaxRow();i++){
			    String paytodate = arr.GetText(i, 2);
			    String cinvalidate = tCinArr.GetText(1, 1);
		        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		        Date tpaytodate;
		        Date tcinvalidate;
		        long diff = 0;
				try {
					tpaytodate =  df.parse(paytodate);
					tcinvalidate = df.parse(cinvalidate);
			        diff = tcinvalidate.getTime() - tpaytodate.getTime();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Content=e.getMessage();
				   	return false;
				}
		        if(diff<0){
		        	Content="团体复核通过操作失败，失败原因:第"+arr.GetText(i, 1)+"期的约定缴费时间["+arr.GetText(i, 2)+"]晚于保单终止日期["+tCinArr.GetText(1, 1)+"]，请核查！";
		    		System.out.println(Content);
		    		return false;
		        }
		}
		return true;
	}
	public boolean checkBalance(){
		String tSqlCode = "select markettype from LCGrpCont where prtno='" +PrtNo+ "'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrCodeResult = tExeSQL.execSQL(tSqlCode);
		if(arrCodeResult.getMaxNumber()<=0){
			Content="团体复核通过操作失败，失败原因:获取保单市场类型失败！";
			System.out.println(Content);
			return false;
		}
		if("".equals(arrCodeResult.GetText(1, 1))||null==arrCodeResult.GetText(1, 1)){
			Content="团体复核通过操作失败，失败原因:获取保单市场类型失败！";
			System.out.println(Content);
			return false;
		}
		String tSQL = "select ProjectName,BalanceTermFlag,StopLine,SharedLine,RevertantLine from LCGrpContSub where prtno = '"+PrtNo+"' ";
		SSRS arrBalanceResult = tExeSQL.execSQL(tSQL);
			if( !"1".equals(arrCodeResult.GetText(1, 1)) && !"9".equals(arrCodeResult.GetText(1, 1))){
				if(arrBalanceResult.getMaxNumber()<=0){
					Content="团体复核通过操作失败，失败原因:根据保单市场类型，需录入社保项目要素信息！";
					System.out.println(Content);
					return false;
				}
				if(null==arrBalanceResult.GetText(1, 1) || "".equals(arrBalanceResult.GetText(1, 1))
				 ||null==arrBalanceResult.GetText(1, 2) || "".equals(arrBalanceResult.GetText(1, 2))
				 ||null==arrBalanceResult.GetText(1, 3) || "".equals(arrBalanceResult.GetText(1, 3))
				 ||null==arrBalanceResult.GetText(1, 4) || "".equals(arrBalanceResult.GetText(1, 4))
				 ||null==arrBalanceResult.GetText(1, 5)|| "".equals(arrBalanceResult.GetText(1, 5))){
					Content="团体复核通过操作失败，失败原因:根据保单市场类型，需录入社保项目要素信息！";
					System.out.println(Content);
					return false;
				}
			}else{
				if(arrBalanceResult.getMaxNumber()>0){
					Content="团体复核通过操作失败，失败原因:根据保单市场类型，不需录入社保项目要素信息！";
					System.out.println(Content);
					return false;
				}
			}	
		
		return true;
	}
	public String getContent() {
		return Content;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DealGroupPolCheck a=new DealGroupPolCheck();
		a.deal();

	}

}
