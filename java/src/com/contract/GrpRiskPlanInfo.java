package com.contract;

import java.util.HashMap;
import java.util.List;

import org.jdom.Element;

import com.contract.obj.ContPlanInfoList;
import com.contract.obj.LCGrpContInfo;
import com.contract.obj.MessageHead;
import com.contract.obj.PayPlanInfoList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCGrpPayPlanSchema;
import com.sinosoft.lis.tb.GrpPayPlanUI;
import com.sinosoft.lis.tb.LCContPlanUI;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LCGrpPayPlanSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 保障计划定制
 * 
 * @author LiHao
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class GrpRiskPlanInfo {
	// 错误信息
	private String errorValue;

	public String getErrorValue() {
		return errorValue;
	}

	private VData data;
	private String ProposalGrpContNo;
	private String GrpContNo;
	private String RiskFlag;
	private String ContPlanCode = "";// 保障计划
	private String InsuPlanCode = "";// 方案编码
	private String RiskCode = "";// 险种代码
	private String MainRiskCode = "";// 主险种信息
	private String Peoples3 = "";
	private String SumPrem;// 总保费
	private String ContPlanName;
	private String Peoples2 = "";
	private String RiskAmnt;
	private String InsuArea;
	private String SpecialGetRate;
	private String YearAmnt;// 年度限额
	private String getQDWZTable = "planqdwz";
	private List elelist;// Item1节点的集合
    private String GrpContPayIntv;
    private GlobalInput tGI = new GlobalInput(); 
	public boolean grpRiskPlanInfo(VData vdata, String pProposalGrpContNo, String gGrpContNo) {
		System.out.println("======================================保障计划制定======================");
		data = vdata;
		ProposalGrpContNo = pProposalGrpContNo;
		GrpContNo = gGrpContNo;
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		
		tGI.Operator = "CENA";
		tGI.ManageCom = lCGrpContInfo.getManageCom();
		//原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
		tGI.ComCode = tGI.ManageCom;
		
		GrpContPayIntv = lCGrpContInfo.getGrpContPayIntv();
		if ("".equals(ProposalGrpContNo) || null == ProposalGrpContNo) {
			errorValue = "必须保存合同信息才能进行〔保险计划制定〕！";
			return false;
		}
		ContPlanInfoList riskCodeInfo = (ContPlanInfoList) data.getObjectByObjectName("ContPlanInfoList", 0);
		List list = riskCodeInfo.getList();
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Element ele = (Element) list.get(i);
				RiskCode = ele.getChildText("RiskCode");
				// CalRule = ele.getChildText("CalRule");
				ContPlanCode = ele.getChildText("ContPlanCode");
				ContPlanName = ele.getChildText("ContPlanName");
				Peoples3 = ele.getChildText("Peoples3");
				Peoples2 = ele.getChildText("Peoples2");
				SumPrem = ele.getChildText("SumPrem");
				RiskAmnt = ele.getChildText("RiskAmnt");
				InsuPlanCode = ele.getChildText("InsuPlanCode");
				InsuArea = ele.getChildText("InsuArea");
				SpecialGetRate = ele.getChildText("SpecialGetRate");
				MainRiskCode = ele.getChildText("MainRiskCode");
				YearAmnt = ele.getChildText("YearAmnt");
				elelist = ele.getChildren("Item1");

				for (int j = 0; j < elelist.size(); j++) {
					Element eleItem1 = (Element) elelist.get(j);
					String DutyCode = eleItem1.getChildText("DutyCode");// 责任代码
					ExeSQL exeSQL = new ExeSQL();
					String sqlbDutyCode = "select choflag from lmriskduty where riskcode='" + RiskCode
							+ "' and dutycode='" + DutyCode + "'";
					SSRS ssrsbDutyCode = exeSQL.execSQL(sqlbDutyCode);
					if (ssrsbDutyCode.getMaxRow() < 1) {
						errorValue = "险种" + RiskCode + "下没有责任代码为" + DutyCode + "的信息";
						return false;
					}
				}
				// 保障计划定制前 数据的初始化
				if (!clearAreaCode()) {
					return false;
				}
				if (!ContPlanSave()) {
					return false;
				}
			}
		}
		// 缴费频次-1 则需要进行约定缴费计划的录入
		if("-1".equals(GrpContPayIntv)){
			if (!planCodeClick()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 保障计划定制前 初始化数据
	 * 
	 * @return
	 */
	public boolean clearAreaCode() {

		ExeSQL exeSQL = new ExeSQL();
		String tContPrintTypeSQL = "select ContPrintType from lcgrpcont where grpcontno = '" + GrpContNo + "' ";
		SSRS ssrstContPrintType = exeSQL.execSQL(tContPrintTypeSQL);
		if (ssrstContPrintType.getMaxRow() > 0) {
			if ("4".equals(ssrstContPrintType.GetText(1, 1))) {
				getQDWZTable = "planqdwzbrief";
			}
		}
		return true;
	}

	/**
	 * 
	 * ContPlanSave.jsp
	 */

	public boolean ContPlanSave() {
		LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
		LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
		LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
		LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
		LCContPlanUI tLCContPlanUI = new LCContPlanUI();

		// 输出参数
		CErrors tError = null;
		String FlagStr = "Fail";
		String mRiskFlag = "";

		// 全局变量
		System.out.println("begin ...");
//		GlobalInput tG = new GlobalInput();
		MessageHead vMessHead = (MessageHead) data.getObjectByObjectName("MessageHead", 0);
//		tG.Operator = vMessHead.getSendOperator();
//		tG.ComCode = vMessHead.getBranchCode();
//		tG.ManageCom = vMessHead.getBranchCode();
//		tG.Operator = "CP3613";
//		tG.Operator = "CENA";
//		tG.ComCode = "86360500";
//		tG.ManageCom = "86360500";
		String mSql = "";
		mSql = "select count(*) from lcriskwrap where grpcontno= '" + GrpContNo + "'";
		ExeSQL exeSQL = new ExeSQL();
		SSRS ssrs = exeSQL.execSQL(mSql);
		if (ssrs.getMaxRow() > 0) {
			mRiskFlag = ssrs.GetText(1, 1);
			if (mRiskFlag.equals("1") || mRiskFlag.equals("3")) {
				RiskFlag = "Wrap";
			}
			System.out.println("获取到的RiskFlag ：" + mRiskFlag);
		}

		TransferData tTransferData = new TransferData();
		System.out.println("看看有没有获取到RiskFlag ：" + RiskFlag);
		tTransferData.setNameAndValue("RiskFlag", RiskFlag);

		// 保险计划要素信息
		if (elelist.size() > 0) {
			HashMap tHashMap = new HashMap();

			for (int i = 0; i < elelist.size(); i++) {
				Element eleItem1 = (Element) elelist.get(i);   
				String DutyCode = eleItem1.getChildText("DutyCode");// 责任代码
				List listItem2 = eleItem1.getChildren("Item2");
				System.out.println("ceshi: " + listItem2.size());   //  7  
				int sum = 0;
				for (int j = 0; j < listItem2.size(); j++) {
					Element eleItem2 = (Element) listItem2.get(j);
					String CalFactor = eleItem2.getChildText("CalFactor");
					String CalFactorValue = eleItem2.getChildText("CalFactorValue");
					String PayPlanCode = "";
					String GetDutyCode = "";
					String InsuAccNo = "";
					/**
					 * BY GengYe 2017-12-15
					 */
					if("690201".equals(RiskCode)&&CalFactor.equals("AccGetRate")){
						 InsuAccNo =eleItem2.getChildText("InsuAccNo");
					}
					String RiskVer = "";
					String CalFactorType = "";
					String RiskWrapFlag = "";//
					String GrpPolNo = "";// 集体保单险种号码
					String SqlContPlanGrid1;
					StringBuffer SqlContPlanGrid = new StringBuffer();
					SqlContPlanGrid.append(
							" select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti, ");
					SqlContPlanGrid.append(
							" b.RiskVer,d.GrpPolNo,a.CalFactorType,c.CalMode,a.PayPlanCode,a.GetDutyCode,a.InsuAccNo  ");
					SqlContPlanGrid.append(" from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCGrpPol d where ");
					SqlContPlanGrid.append(
							" a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode and a.ChooseFlag in ('0','2') ");
					SqlContPlanGrid.append(" and a.CalFactor = '" + CalFactor + "' ");
					SqlContPlanGrid.append(" and a.DutyCode = '" + DutyCode + "' ");
					SqlContPlanGrid.append(" and GrpContNO = '" + GrpContNo + "' and a.RiskCode = '" + RiskCode+ "'  ");
					/**
					 * BY GengYe 2017-12-15
					 */
					SqlContPlanGrid1=" and a.InsuAccNo = '" + InsuAccNo + "' order by a.RiskCode,a.DutyCode,a.FactorOrder";
					if ("690201".equals(RiskCode)&&CalFactor.equals("AccGetRate")) {
						SqlContPlanGrid.append(SqlContPlanGrid1);
					}
					SSRS ssrsSqlContPlanGrid = exeSQL.execSQL(SqlContPlanGrid.toString());
					if (ssrsSqlContPlanGrid.getMaxRow() > 0) {
						RiskVer = ssrsSqlContPlanGrid.GetText(1, 8);
						GrpPolNo = ssrsSqlContPlanGrid.GetText(1, 9);
						CalFactorType = ssrsSqlContPlanGrid.GetText(1, 10);
						PayPlanCode = ssrsSqlContPlanGrid.GetText(1, 12);
						GetDutyCode = ssrsSqlContPlanGrid.GetText(1, 13);
						InsuAccNo = ssrsSqlContPlanGrid.GetText(1, 14);
						sum = sum + ssrsSqlContPlanGrid.getMaxRow();
					} else {
						errorValue = "责任代码：" + DutyCode + "下 没有" + CalFactor + "的要素信息错误";
						return false;
					}
					tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
					tLCContPlanDutyParamSchema.setGrpContNo(GrpContNo);
					tLCContPlanDutyParamSchema.setProposalGrpContNo(ProposalGrpContNo);
					tLCContPlanDutyParamSchema.setContPlanCode(ContPlanCode);
					tLCContPlanDutyParamSchema.setContPlanName(ContPlanName);
					tLCContPlanDutyParamSchema.setRiskCode(RiskCode);
					tLCContPlanDutyParamSchema.setDutyCode(DutyCode);// 有生成
					tLCContPlanDutyParamSchema.setCalFactor(CalFactor);// ContPlanGrid5计算要素
					tLCContPlanDutyParamSchema.setCalFactorValue(CalFactorValue);// ContPlanGrid8要素值
					tLCContPlanDutyParamSchema.setRemark("");// ContPlanGrid9特别说明
					tLCContPlanDutyParamSchema.setRiskVersion(RiskVer);// ContPlanGrid10险种版本
					tLCContPlanDutyParamSchema.setGrpPolNo(GrpPolNo);// 集体保单险种号码ContPlanGrid11
					if ("".equals(MainRiskCode) || null == MainRiskCode) {
						MainRiskCode = RiskCode;
					}
					tLCContPlanDutyParamSchema.setMainRiskCode(MainRiskCode);
					tLCContPlanDutyParamSchema.setMainRiskVersion(RiskVer);
					tLCContPlanDutyParamSchema.setCalFactorType(CalFactorType);
					tLCContPlanDutyParamSchema.setPlanType("0");
					tLCContPlanDutyParamSchema.setPayPlanCode(PayPlanCode);
					tLCContPlanDutyParamSchema.setGetDutyCode(GetDutyCode);
					tLCContPlanDutyParamSchema.setInsuAccNo(InsuAccNo);
					tLCContPlanDutyParamSchema.setOrder(i);

					tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
					// 总保费问题
					LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
					tLCContPlanRiskSchema.setRiskCode(RiskCode);// ContPlanGrid2
					tLCContPlanRiskSchema.setRiskPrem(SumPrem);
					tLCContPlanRiskSchema.setRiskAmnt(RiskAmnt);// ContPlanGrid19
					tLCContPlanRiskSchema.setRiskWrapCode("");// ContPlanGrid21
					if (RiskWrapFlag == null || RiskWrapFlag.equals("")) {
						RiskWrapFlag = "N";
					}
					tLCContPlanRiskSchema.setRiskWrapFlag(RiskWrapFlag);// ContPlanGrid20
					tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

					if ("160306".equals(RiskCode)) {
						if (InsuPlanCode.trim() != null && !InsuPlanCode.equals("") && !InsuPlanCode.equals("null")) {
							tHashMap.put("InsuPlanCode160306", InsuPlanCode);
						}
					}
				}
			//GY
				if ("160306".equals(RiskCode)) {
					return true;
				} else if (sum != listItem2.size()) {
					errorValue = "责任代码：" + DutyCode + "下 要素的个数：" + sum
							+ "，和报文中节点个数：" + listItem2.size() + "不一致";
					return false;

				}

			}
			if (InsuPlanCode.trim() != null && !InsuPlanCode.equals("") && !InsuPlanCode.equals("null")) {
				tHashMap.put("InsuPlanCode", InsuPlanCode);
			}
			if (InsuArea.trim() != null && !InsuArea.equals("") && !InsuArea.equals("null")) {
				tHashMap.put("InsuArea", InsuArea);
			}
			if (SpecialGetRate.trim() != null && !SpecialGetRate.equals("") && !SpecialGetRate.equals("null")) {
				tHashMap.put("SpecialGetRate", SpecialGetRate);
			}
			if (YearAmnt.trim() != null && !YearAmnt.equals("") && !YearAmnt.equals("null")) {
				tHashMap.put("YearAmnt", YearAmnt);
			}

			if ("planqdwzbrief".equals(getQDWZTable)) {
				if (InsuPlanCode.trim() != null && !InsuPlanCode.equals("") && !InsuPlanCode.equals("null")) {
					tHashMap.put("InsuPlanCodeQDWZ", InsuPlanCode);
				}

			}

			tTransferData.setNameAndValue("NewFactor", tHashMap);
		}
		System.out.println("end ...");

		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tGI);
		tVData.addElement(tLCContPlanDutyParamSet);
		tVData.addElement(ProposalGrpContNo);
		tVData.addElement(GrpContNo);
		tVData.addElement(ContPlanCode);
		tVData.addElement("0");
		tVData.addElement("");// PlanSql
		tVData.addElement(Peoples3);
		tVData.addElement(Peoples2);
		tVData.addElement(tLCContPlanRiskSet);
		tVData.addElement(tLCGrpFeeSet);
		tVData.addElement(tTransferData);
		try {
			System.out.println("this will save the data!!!");
			tLCContPlanUI.submitData(tVData, "INSERT||MAIN");
		} catch (Exception ex) {
			ex.printStackTrace();
			errorValue = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}

		if (!FlagStr.equals("Fail")) {
			tError = tLCContPlanUI.mErrors;
			if (!tError.needDealError()) {
				FlagStr = "Succ";
			} else {
				errorValue = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
		return true;
	}

	// 约定费率录入按钮
	public boolean planCodeClick() {
		ExeSQL exeSQL = new ExeSQL();
		String SqlContPlanCode = "select 1 from lccontplan where grpcontno = '" + GrpContNo
				+ "' and contplancode !='11' ";
		SSRS ssrsSqlContPlanCode = exeSQL.execSQL(SqlContPlanCode);
		if (ssrsSqlContPlanCode.getMaxRow() < 0) {
			errorValue = "请先录入保障计划后，再录入约定缴费计划！";
			return false;
		}

		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		String tSpecSQL = "select 1 from lcgrppol where prtno = '" + lCGrpContInfo.getPrtNo()
				+ "' and riskcode in (select riskcode from lmriskapp where risktype3 = '7' or risktype4 = '4')  ";
		SSRS ssrsSpecSQL = exeSQL.execSQL(tSpecSQL);
		if (ssrsSpecSQL.getMaxRow() > 0) {
			errorValue = "含有特需险或万能险种，不可添加约定缴费计划！";
			return false;
		}

		// 填写缴费计划
		// alert("请填写缴费计划！");
		// alert("第"+(i+2)+"期的约定缴费时间应晚于第"+(i+1)+"期的约定缴费时间!");
		MessageHead vMessHead = (MessageHead) data.getObjectByObjectName("MessageHead", 0);
		GlobalInput globalInput = new GlobalInput();
		globalInput.Operator = vMessHead.getSendOperator();
		globalInput.ComCode = vMessHead.getBranchCode();
		globalInput.ManageCom = vMessHead.getBranchCode();

		PayPlanInfoList payPlanInfoList = (PayPlanInfoList) data.getObjectByObjectName("PayPlanInfoList", 0);
		List list = payPlanInfoList.getList();
		// 保存缴费计划
		try {
			LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
			for (int i = 0; i < list.size(); i++) {
				Element ele = (Element) list.get(i);
				String PaytoDate = ele.getChildText("PaytoDate");
				LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
				// tLCGrpPayPlanSchema.setPlanCode("GrpPayPlanGrid1");
				tLCGrpPayPlanSchema.setPaytoDate(PaytoDate);
				tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
			}
			TransferData mTransferData = new TransferData();
			mTransferData.setNameAndValue("ProposalGrpContNo", ProposalGrpContNo);
			mTransferData.setNameAndValue("LCGrpPayPlanSet", tLCGrpPayPlanSet);

			// 准备传输数据 VData
			VData vData = new VData();

			vData.addElement(globalInput);
			vData.addElement(tLCGrpPayPlanSet);
			vData.add(mTransferData);

			// 数据传输
			GrpPayPlanUI tGrpPayPlanUI = new GrpPayPlanUI();

			if (!tGrpPayPlanUI.submitData(vData, "INSERT")) {
				errorValue = " 保存失败，原因是: " + tGrpPayPlanUI.mErrors.getFirstError();
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			errorValue = " 保存失败，原因是:" + ex.getMessage();
			return false;
		}

		//
		try {
			LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
			for (int i = 0; i < list.size(); i++) {
				Element ele = (Element) list.get(i);
				String PaytoDate = ele.getChildText("PaytoDate");
				List listItem1 = ele.getChildren("Item1");
				for (int j = 0; j < listItem1.size(); j++) {
					LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
					Element eleItem1 = (Element) listItem1.get(j);
					String ContPlanCode1 = eleItem1.getChildText("ContPlanCode");
					String Prems = eleItem1.getChildText("Prems");
					tLCGrpPayPlanSchema.setPlanCode(String.valueOf(i + 1));
					tLCGrpPayPlanSchema.setPaytoDate(PaytoDate);
					tLCGrpPayPlanSchema.setContPlanCode(ContPlanCode1);
					tLCGrpPayPlanSchema.setPrem(Prems);
					tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
				}
			}
			TransferData mTransferData = new TransferData();
			mTransferData.setNameAndValue("ProposalGrpContNo", ProposalGrpContNo);
			mTransferData.setNameAndValue("LCGrpPayPlanSet", tLCGrpPayPlanSet);

			// 准备传输数据 VData
			VData vData = new VData();

			vData.addElement(globalInput);
			vData.addElement(tLCGrpPayPlanSet);
			vData.add(mTransferData);

			// 数据传输
			GrpPayPlanUI tGrpPayPlanUI = new GrpPayPlanUI();
			if (!tGrpPayPlanUI.submitData(vData, "INSERTDetail")) {
				errorValue = " 保存失败，原因是: " + tGrpPayPlanUI.mErrors.getFirstError();
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			errorValue = " 保存失败，原因是:" + ex.getMessage();
			return false;
		}
		return true;
	}
}
