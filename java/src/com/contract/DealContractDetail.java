package com.contract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.jdom.Element;

import com.contract.obj.ContPlanInfoList;
import com.contract.obj.LCExtendInfo;
import com.contract.obj.LCGrpAddressInfo;
import com.contract.obj.LCGrpAppntInfo;
import com.contract.obj.LCGrpContInfo;
import com.contract.obj.MessageHead;
import com.sinosoft.lis.db.LockTableDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableUI;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LDSysTraceSchema;
import com.sinosoft.lis.schema.LockTableSchema;
import com.sinosoft.lis.tb.GroupContUI;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LDSysTraceSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;

/**
 * 新契约：核心处理过程
 * 
 * @author LiHao
 *
 */
/**
 * 
 * 定义社保项目要素解除对于690201险种的限制录入
 * 责任人：吕瑞祥
 * date：20190109
 */
public class DealContractDetail {
	// 当前日期
	private String nowDate = PubFun.getCurrentDate();

	private String Operator;
	// 错误信息
	private String errorValue;
    String PrtNo;
	public String getErrorValue() {
		return errorValue;
	}

	// 保存（contPolSave）后生成的数据
	private String ProposalGrpContNo;
	private String GrpContNo;
	private String GrpNo;
	String RiskCode;
	private VData data;
	private GlobalInput GI = new GlobalInput();
	private boolean rollbackFlag = false;

	public boolean deal(VData vdata) throws Exception {
		data = vdata;
		try {
			// 申请
			if (!ApplyInput()) {
				return false;
			}

			// 生成
			//修改当前操作员为CENA，机构也写死了
			if (!inputSave()) {
				rollbackFlag = true;
				return false;
			}
			// 开始录入,锁表
			if (!goToInput()) {
				rollbackFlag = true;
				return false;
			}
			// 保存前验证一些数据
			if (!submitForm()) {
				rollbackFlag = true;
				return false;
			}
			// 保存操作
			//修改录入人员为CENA
			if (!contPolSave()) {
				rollbackFlag = true;
				return false;
			}
			//添加险种
			DealAddRecord dealAddRecord = new DealAddRecord();
			if (!dealAddRecord.deal(data)) {
				rollbackFlag = true;
				errorValue = dealAddRecord.getContent();
				return false;
			}
			
			//定义公共账户
			ContPlanInfoList riskCodeInfo = (ContPlanInfoList) data.getObjectByObjectName("ContPlanInfoList", 0);
			List list = riskCodeInfo.getList();
			if(list.size()>0){
				 for(int i=0;i<list.size();i++ ){
					 Element ele = (Element) list.get(i);
					 RiskCode=ele.getChildText("RiskCode");
					 // 若险种是690201，需要定义公共账户
					 if("690201".equals(RiskCode)){
						 DealPubAcc dealPubAcc = new DealPubAcc();
						 if (!dealPubAcc.deal(data, RiskCode)) {
								rollbackFlag = true;
								errorValue = dealPubAcc.getErrorValue();
								return false;
							}
					 }
				 }
			}
			
			//缴费频次 不为年缴，执行保障计划定制
			LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
			String GrpContPayIntv = lCGrpContInfo.getGrpContPayIntv();
			if(!"12".equals(GrpContPayIntv)){
				GrpRiskPlanInfo grpRiskPlanInfo = new GrpRiskPlanInfo();
				if (!grpRiskPlanInfo.grpRiskPlanInfo(data, ProposalGrpContNo, GrpContNo)) {
					rollbackFlag = true;
					errorValue = grpRiskPlanInfo.getErrorValue();
					return false;
				}
			}
			//被保人清单导入
			//有一个算费的ly？
			DealGrpInsuList tDealGrpInsuList = new DealGrpInsuList();
			System.out.println("开始被保人清单导入");
				if (!tDealGrpInsuList.dealData(data)) {
					rollbackFlag = true;
					errorValue = tDealGrpInsuList.getContent();
					return false;
				}
				System.out.println("被保人清单导入结束");
			//定义社保项目要素
			DealSocialSecurityElements tDealSocialSecurityElements = new DealSocialSecurityElements();
			//if (!"690201".equals(RiskCode)) {
			//因理赔需要项目要素数据，故解除此对于690201险种限制date：20190109
			System.out.println("========社保项目要素定义开始========");
				if (!tDealSocialSecurityElements.dealData(data)) {
					rollbackFlag = true;
					errorValue = tDealSocialSecurityElements.getContent();
					return false;
				  }
			//}
			//录入完毕
			//有一个ly？
			DealGrpInputConfirm tDealGrpInputConfirm = new DealGrpInputConfirm();
			if (!tDealGrpInputConfirm.dealData(data)) {
				rollbackFlag = true;
				errorValue = tDealGrpInputConfirm.getContent();
				return false;
			}
			//团体复核通过
			DealGroupPolCheck tDealGroupPolCheck = new DealGroupPolCheck();
			if (!tDealGroupPolCheck.dealData(data)) {
				rollbackFlag = true;
				errorValue = tDealGroupPolCheck.getContent();
				return false;
			}
			//人工核保 团体保单整单确认
			DealGroupUW tDealGroupUW = new DealGroupUW();
			if (!tDealGroupUW.dealData(data)) {
				rollbackFlag = true;
				errorValue = tDealGroupUW.getContent();
				return false;
			}
		} finally {
			System.out.println("errorValue:::::::::::::::::::::::::::"+errorValue);
			if (rollbackFlag) {
				LockTableDB mLockTableDB = new LockTableDB();
				LockTableSchema mLockTableSchema = new LockTableSchema();
				mLockTableSchema.setNoLimit(PrtNo);
				mLockTableSchema.setNoType("CO");
				mLockTableDB.setSchema(mLockTableSchema);
				mLockTableDB.delete();
				// 回滚
				DealDeleteGrpCont DealDeleteGrpCont = new DealDeleteGrpCont();
				DealDeleteGrpCont.deal(data);
			}
		}
		return true;
	}

	/**
	 * NoScanContInput.jsp页面中的"申 请"
	 * 
	 * 无名单扫描申请
	 * 
	 * 申请时先校验是否存在该印刷号： 1.存在，走查询方法 easyQueryClick 2.不存在，走生成方法 inputSave
	 * 
	 * @return
	 */
	public boolean ApplyInput() {
		System.out.println("***********开始ApplyInput申请*********");
		// 团体印刷号
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		PrtNo = lCGrpContInfo.getPrtNo();
		// 判断是否为个体印刷号
		StringBuffer sqlCheckPrtNoIsSingle = new StringBuffer();
		sqlCheckPrtNoIsSingle.append("select missionprop1 from lwmission where 1=1 ");
		sqlCheckPrtNoIsSingle.append(" and activityid = '0000001098' ");
		sqlCheckPrtNoIsSingle.append(" and processid = '0000000003'");
		sqlCheckPrtNoIsSingle.append(" and missionprop1='" + PrtNo + "'");
		ExeSQL exeSQL = new ExeSQL();
		SSRS ssrssqlCheckPrtNoIsSingle = exeSQL.execSQL(sqlCheckPrtNoIsSingle.toString());
		if (ssrssqlCheckPrtNoIsSingle.getMaxRow() > 0) {
			errorValue = "印刷号：" + PrtNo + "已经存在，且为个体印刷号，填写正确的团体印刷号";
			return false;
		}
		// 判断团体印刷号是否存在
		StringBuffer sqlCheckPrtNo = new StringBuffer();
		sqlCheckPrtNo.append("select missionprop1 from lwmission where 1=1 ");
		sqlCheckPrtNo.append(" and activityid = '0000002098' ");
		sqlCheckPrtNo.append(" and processid = '0000000004'");
		sqlCheckPrtNo.append(" and missionprop1='" + PrtNo + "'");
		SSRS ssrsCheckPrtNo = exeSQL.execSQL(sqlCheckPrtNo.toString());

		StringBuffer sqlCheckPrtNo1 = new StringBuffer();
		sqlCheckPrtNo1.append("select prtno from lccont where prtno = '" + PrtNo + "'");
		sqlCheckPrtNo1.append("union select prtno from lbcont where prtno = '" + PrtNo + "'");
		SSRS ssrsCheckPrtNo1 = exeSQL.execSQL(sqlCheckPrtNo1.toString());
		if (ssrsCheckPrtNo.getMaxRow() > 0 || ssrsCheckPrtNo1.getMaxRow() > 0) {
			errorValue = "印刷号：" + PrtNo + "已经存在，填写正确的团体印刷号";
			return false;
		}
		return true;
	}

	/**
	 * NoScanContInputSave.jsp 不存在团体印刷号时： 保存印刷号 生成相关数据
	 * 
	 * @return
	 */
	private boolean inputSave() {
		System.out.println("***********开始inputSave生成相关数据*********");
		CErrors tError = null;
		String FlagStr = "Fail";
		MessageHead messageHead = (MessageHead) data.getObjectByObjectName("MessageHead", 0);
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		String PrtNo = lCGrpContInfo.getPrtNo();
//		GI.Operator = messageHead.getSendOperator();
		GI.ComCode = messageHead.getBranchCode();
//		GI.ManageCom = messageHead.getBranchCode();
		GI.ManageCom = lCGrpContInfo.getManageCom();
//		GI.Operator = "CP3613";
		GI.Operator = "CENA";
//		GI.ComCode = "86360500";
//		GI.ManageCom = "86360500"; 
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", PrtNo);
		tTransferData.setNameAndValue("ManageCom", GI.ManageCom);
		tTransferData.setNameAndValue("InputDate", nowDate);
		tTransferData.setNameAndValue("Operator", GI.Operator);
		tTransferData.setNameAndValue("ULIFlag", "0");
		tVData.add(tTransferData);
		tVData.add(GI);
		try {
			GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
			if (tGrpTbWorkFlowUI.submitData(tVData, "7699999999") == false) {
				int n = tGrpTbWorkFlowUI.mErrors.getErrorCount();
				for (int j = 0; j < n; j++) {
					errorValue = " 投保单申请失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getError(0).errorMessage;
				}
				FlagStr = "Fail";
				return false;
			}
			// 如果在Catch中发现异常，则不从错误类中提取错误信息
			if (FlagStr != "Fail") {
				tError = tGrpTbWorkFlowUI.mErrors;
				if (!tError.needDealError()) {
					int n = tError.getErrorCount();
					if (n > 0) {
						for (int j = 0; j < n; j++) {
							errorValue = errorValue.trim() + j + ". " + tError.getError(j).errorMessage.trim() + ".";
						}
					}
					FlagStr = "Succ";
				} else {
					int n = tError.getErrorCount();
					if (n > 0) {
						for (int j = 0; j < n; j++) {
							errorValue = errorValue.trim() + j + ". " + tError.getError(j).errorMessage.trim() + ".";
						}
					}
					FlagStr = "Fail";
					return false;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			errorValue = errorValue.trim() + " 提示:异常退出.";
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 开始录入
	 * 
	 * @return
	 */
	public boolean goToInput() {
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		String PrtNo = lCGrpContInfo.getPrtNo();
		System.out.println("***********开始goToInput开始录入*********");
		StringBuffer sqllock = new StringBuffer();
		sqllock.append("select * from ldsystrace where PolNo='" + PrtNo + "' and PolState=1002 ");
		ExeSQL exeSQL = new ExeSQL();
		SSRS ssrslock = exeSQL.execSQL(sqllock.toString());
		if (ssrslock.getMaxRow() > 0 && !ssrslock.GetText(1, 2).equals(Operator)) {
			errorValue = "该印刷号的投保单已经被操作员（" + ssrslock.GetText(1, 2) + "）在（" + ssrslock.GetText(1, 6)
					+ "）位置锁定！您不能操作，请选其它的印刷号！";
			return false;
		}

		// 锁印刷号
		LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
		tLDSysTraceSchema.setPolNo(PrtNo);
		tLDSysTraceSchema.setCreatePos("承保录单");
		tLDSysTraceSchema.setPolState("1002");
		LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
		inLDSysTraceSet.add(tLDSysTraceSchema);
		VData VData3 = new VData();
		VData3.add(GI);
		VData3.add(inLDSysTraceSet);

		LockTableUI LockTableUI1 = new LockTableUI();
		if (!LockTableUI1.submitData(VData3, "INSERT")) {
			VData rVData = LockTableUI1.getResult();
			System.out.println("LockTable Failed! " + (String) rVData.get(0));
		} else {
			System.out.println("UnLockTable Succed!");
		}
		return true;
	}

	/**
	 * 
	 * 保存 集体投保单的提交
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean submitForm() throws Exception {
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		LCGrpAddressInfo lCGrpAddressInfo = (LCGrpAddressInfo) data.getObjectByObjectName("LCGrpAddressInfo", 0);
		LCGrpAppntInfo lCGrpAppntInfo = (LCGrpAppntInfo) data.getObjectByObjectName("LCGrpAppntInfo", 0);
		String AppntOnWorkPeoples = lCGrpAppntInfo.getAppntOnWorkPeoples();
		String OrgancomCode = lCGrpAppntInfo.getOrgancomCode();// 客户组织机构代码
		String GrpName = lCGrpContInfo.getGrpName();// 投保人名称（汇交人名称）

		// 选择交叉销售后，对交叉销售各不为空项校验。
		String MixComFlag = lCGrpContInfo.getMixComFlag();
		String Crs_SaleChnl = lCGrpContInfo.getCrs_SaleChnl();
		String Crs_BussType = lCGrpContInfo.getCrs_BussType();
		String GrpAgentCom = lCGrpContInfo.getGrpAgentCom();
		String GrpAgentCode = lCGrpContInfo.getGrpAgentCode();
		String GrpAgentName = lCGrpContInfo.getGrpAgentName();
		String GrpAgentIDNo = lCGrpContInfo.getGrpAgentIDNo();
		if ("1".equals(MixComFlag)) {
			if (Crs_SaleChnl.equals("") || Crs_SaleChnl == null) {
				errorValue = "选择交叉销售时，交叉销售渠道不能为空，请核查！";
				return false;
			}
			if (Crs_BussType.equals("") || Crs_BussType == null) {
				errorValue = "选择交叉销售时，交叉销售业务类型不能为空，请核查！";
				return false;
			}
			if (GrpAgentCom.equals("") || GrpAgentCom == null) {
				errorValue = "选择交叉销售时，对方机构代码不能为空，请核查！";
				return false;
			}
			if (GrpAgentCode.equals("") || GrpAgentCode == null) {
				errorValue = "选择交叉销售时，对方业务员代码不能为空，请核查！";
				return false;
			}
			if (GrpAgentName.equals("") || GrpAgentName == null) {
				errorValue = "选择交叉销售时，对方业务员姓名不能为空，请核查！";
				return false;
			}
			if (GrpAgentIDNo.equals("") || GrpAgentIDNo == null) {
				errorValue = "选择交叉销售时，对方业务员身份证号码不能为空，请核查！";
				return false;
			}
		} else {
			return true;
		}

		// 是否为综合开拓
		LCExtendInfo lCExtendInfo = (LCExtendInfo) data.getObjectByObjectName("LCExtendInfo", 0);
		String ExtendFlag = lCExtendInfo.getExtendFlag();// ExtendFlag = 0 未选中
															// ExtendFlag = 1 选中
		String AssistSaleChnl = lCExtendInfo.getAssistSaleChnl();
		String AssistAgentCode = lCExtendInfo.getAssistAgentCode();
		if (ExtendFlag.equals("1")) {
			if (AssistSaleChnl.equals("") || AssistSaleChnl == null) {
				errorValue = "选择综合开拓时，协助销售渠道不能为空，请核查！";
				return false;
			}
			if (AssistAgentCode.equals("") || AssistAgentCode == null) {
				errorValue = "选择综合开拓时，协助销售人员代码不能为空，请核查！";
				return false;
			}
			StringBuffer sqlAssistSaleChnl = new StringBuffer();
			sqlAssistSaleChnl.append("select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"
					+ AssistSaleChnl + "' ");
			ExeSQL exeSQL = new ExeSQL();
			SSRS ssrsAssistSaleChnl = exeSQL.execSQL(sqlAssistSaleChnl.toString());
			if (ssrsAssistSaleChnl.getMaxRow() < 1) {
				errorValue = "销售渠道与业务员代码不匹配！";
				return false;
			} else {
				StringBuffer sqlAssistAgentCode = new StringBuffer();
				sqlAssistAgentCode.append("select count(*) from laagent where agentcode = '" + AssistAgentCode
						+ "' and BranchType = '" + ssrsAssistSaleChnl.GetText(1, 1) + "' and BranchType2 = '"
						+ ssrsAssistSaleChnl.GetText(1, 2) + "'");
				SSRS ssrsAssistAgentCode = exeSQL.execSQL(sqlAssistAgentCode.toString());
				if ("0".equals(ssrsAssistAgentCode.GetText(1, 1))) {
					errorValue = "销售渠道与业务员代码不匹配，请核查！";
					return false;
				}
			}
		} else {
			return true;
		}

		String PayMode = lCGrpContInfo.getPayMode();// 缴费方式 1.现金 3.支票 4.转帐）
		String BankCode = lCGrpContInfo.getBankCode();
		if (PayMode.equals("4") && (BankCode.equals("") || BankCode == null)) {
			errorValue = "请输入银行转帐的银行和该银行的帐号";
			return false;
		}
		String MarketType = lCGrpContInfo.getMarketType();// 市场类型
		if (MarketType.equals("") || MarketType == null) {
			errorValue = "市场类型不能为空,请录入市场类型";
			return false;
		}
		String SaleChnl = lCGrpContInfo.getSaleChnl(); // 销售渠道
		String AgentCom = lCGrpContInfo.getAgentCom();// 中介公司代码
		if (SaleChnl.equals("03") || SaleChnl.equals("04") || SaleChnl.equals("10") || SaleChnl.equals("15")) {
			if (AgentCom == null || AgentCom.equals("") || AgentCom.equals("null")) {
				errorValue = "销售渠道为中介，请录入中介公司代码！";
				return false;
			}
		}

		// 集团交叉业务要素校验
		String tCrs_SaleChnl = Crs_SaleChnl.trim();
		String tCrs_BussType = Crs_BussType.trim();
		String tGrpAgentCom = GrpAgentCom.trim();
		String tGrpAgentCode = GrpAgentCode.trim();
		String tGrpAgentName = GrpAgentName.trim();
		String tGrpAgentIDNo = GrpAgentIDNo.trim();

		if (tCrs_SaleChnl != null && tCrs_SaleChnl != "") {
			if (tCrs_BussType == null || tCrs_BussType.equals("")) {
				errorValue = "选择集团交叉渠道时，集团交叉业务类型不能为空。";
				return false;
			}
			if (tGrpAgentCom == null || tGrpAgentCom.equals("")) {
				errorValue = "选择集团交叉渠道时，对方业务员机构不能为空。";
				return false;
			}
			if (tGrpAgentCode == null || tGrpAgentCode.equals("")) {
				errorValue = "选择集团交叉渠道时，对方业务员代码不能为空。";
				return false;
			}
			if (tGrpAgentName == null || tGrpAgentName.equals("")) {
				errorValue = "选择集团交叉渠道时，对方业务员姓名不能为空。";
				return false;
			}
			if (tGrpAgentIDNo == null || tGrpAgentIDNo.equals("")) {
				errorValue = "选择集团交叉渠道时，对方业务员身份证不能为空。";
				return false;
			}
		} else {
			if (tCrs_BussType != null && tCrs_BussType != "") {
				errorValue = "未选择集团交叉渠道时，集团交叉业务类型不能填写。";
				return false;
			}
			if (tGrpAgentCom != null && tGrpAgentCom != "") {
				errorValue = "未选择集团交叉渠道时，对方业务员机构不能为填写。";
				return false;
			}
			if (tGrpAgentCode != null && tGrpAgentCode != "") {
				errorValue = "未选择集团交叉渠道时，对方业务员代码不能为填写。";
				return false;
			}
			if (tGrpAgentName != null && tGrpAgentName != "") {
				errorValue = "未选择集团交叉渠道时，对方业务员姓名不能为填写。";
				return false;
			}
			if (tGrpAgentIDNo != null && tGrpAgentIDNo != "") {
				errorValue = "未选择集团交叉渠道时，对方业务员身份证不能为填写。";
				return false;
			}
		}

		ExeSQL exeSQL = new ExeSQL();
		if (OrgancomCode != null && OrgancomCode != "") {
			StringBuffer sqltGrpName = new StringBuffer();
			sqltGrpName.append(
					"select 1 from ldgrp where grpname = '" + GrpName + "' and OrgancomCode = '" + OrgancomCode + "' ");
			SSRS ssrstGrpName = exeSQL.execSQL(sqltGrpName.toString());
			if (ssrstGrpName.getMaxRow() > 0) {
				errorValue = "该投保机构已存在客户数据，请查询客户数据或填写对应的客户号码！";
				return false;
			}
		}

		String Peoples = lCGrpContInfo.getPeoples();
		String ContPrintType = lCGrpContInfo.getContPrintType();// 团体保单属性
		String BusinessType = lCGrpContInfo.getBusinessType();
		String GrpNature = lCGrpContInfo.getGrpNature();
		String LinkMan1 = lCGrpAddressInfo.getLinkMan1();
		String Phone1 = lCGrpAddressInfo.getPhone1();
		String OnWorkPeoples = lCGrpContInfo.getOnWorkPeoples();

		if (ContPrintType.equals("5")) {
			if (BusinessType.equals("") || GrpNature.equals("")) {
				errorValue = "非汇交件保单的行业编码和企业类型编码不能为空！";
				return false;
			}
			if (LinkMan1.equals("") || Phone1.equals("")) {
				errorValue = "非汇交件保单的联系人姓名和联系电话不能为空！";
				return false;
			} else {
				if (LinkMan1.length() > 10) {
					errorValue = "联系人姓名太长，最大长度为10！";
					return false;
				}
				if (Phone1.length() > 30) {
					errorValue = "联系电话太长，最大长度为30！";
					return false;
				}
			}
			if (AppntOnWorkPeoples.equals("")) {
				errorValue = "非汇交件保单投保信息中的在职人数不能为空！";
				return false;
			} else {
				if (AppntOnWorkPeoples.matches("[0-9]+")) {
					errorValue = "在职人数必须是数字！";
					return false;
				}
			}
			if (OnWorkPeoples.equals("")) {
				errorValue = "非汇交件保单参保信息中的在职人数不能为空！";
				return false;
			} else {
				if (OnWorkPeoples.matches("[0-9]+")) {
					errorValue = "在职人数必须是数字！";
					return false;
				}
			}

			if (Peoples.equals("")) {
				errorValue = "非汇交件保单的员工总人数不能为空！";
				return false;
			} else {
				if (Peoples.matches("[0-9]+")) {
					errorValue = "员工总人数必须是数字！";
					return false;
				}
			}
		}

		// 约定缴费方式时进行的校验 --- END
		String HandlerDate = lCGrpContInfo.getHandlerDate();
		String CValiDate = lCGrpContInfo.getCValiDate();// 保险责任生效日
		String CInValiDate = lCGrpContInfo.getCInValiDate();// 保险责任终止日
		if (diffTime(HandlerDate, CValiDate) > 365) {
			errorValue = "保单生效日期最多为投保日期后一年";
			return false;
		}
		if (diffTime(CValiDate, CInValiDate) < 0) {
			errorValue = "保单生效日期不应在保单失效日期之前!";
			return false;
		}
		if (diffTime(CValiDate, HandlerDate) > 365) {
			errorValue = "保单生效日期最多为投保填写日期前一年";
			return false;
		}

		if (ContPrintType == null || ContPrintType.equals("")) {
			errorValue = "保单打印类型不能为空!";
			return false;
		}

		String AppntOffWorkPeoples = lCGrpAppntInfo.getAppntOffWorkPeoples();
		String AppntOtherPeoples = lCGrpAppntInfo.getAppntOtherPeoples();
		// 员工总人数=在职人数+退休人数+其它人员人数
		if (ContPrintType != "5") {
			if (Peoples == null || Peoples.equals("")) {
				Peoples = "0";
			}
			if (AppntOnWorkPeoples == null || AppntOnWorkPeoples.equals("")) {
				AppntOnWorkPeoples = "0";
			}
			if (AppntOffWorkPeoples == null || AppntOffWorkPeoples.equals("")) {
				AppntOffWorkPeoples = "0";
			}
			if (AppntOtherPeoples == null || AppntOtherPeoples.equals("")) {
				AppntOtherPeoples = "0";
			}
			int intPeoples = Integer.valueOf(Peoples);
			int intAppntOnWorkPeoples = Integer.valueOf(AppntOnWorkPeoples);
			int intAppntOffWorkPeoples = Integer.valueOf(AppntOffWorkPeoples);
			int intAppntOtherPeoples = Integer.valueOf(AppntOtherPeoples);

			if (intPeoples != intAppntOnWorkPeoples + intAppntOffWorkPeoples + intAppntOtherPeoples) {
				errorValue = "员工总人数应该为" + (intAppntOnWorkPeoples + intAppntOffWorkPeoples + intAppntOtherPeoples);
				return false;
			}
		}
		return true;
	}

	/**
	 * 两个日期之间的天数判断
	 * 
	 * @param sdate
	 * @param edate
	 * @return
	 * @throws Exception
	 */
	public long diffTime(String sdate, String edate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(sdate));
		long time1 = cal.getTimeInMillis();
		cal.setTime(sdf.parse(edate));
		long time2 = cal.getTimeInMillis();
		long betweenDates = (time2 - time1) / (1000 * 3600 * 24);
		return betweenDates;
	}
public static void main(String[] args) {
	
}
	/**
	 * 保存操作
	 * 
	 * @return
	 */
	public boolean contPolSave() {
		System.out.println("*********開始contPolSave保存操作************");
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		String PrtNo = lCGrpContInfo.getPrtNo();
		String ManageCom = lCGrpContInfo.getManageCom();
		String SaleChnl = lCGrpContInfo.getSaleChnl();
		String AgentCom = lCGrpContInfo.getAgentCom();
		String AgentType = lCGrpContInfo.getAgentType();
		String AgentCode = lCGrpContInfo.getAgentCode();
		String AgentGroup = lCGrpContInfo.getAgentGroup();
		String AgentCode1 = lCGrpContInfo.getAgentCode1();
		String AgentSaleCode = lCGrpContInfo.getAgentSaleCode();
		String GrpSpec = lCGrpContInfo.getGrpSpec();
		String PayMode = lCGrpContInfo.getPayMode();
		String GrpName = lCGrpContInfo.getGrpName();
		String GetFlag = lCGrpContInfo.getGetFlag();
		String GrpContPayIntv = lCGrpContInfo.getGrpContPayIntv();
		String BankCode = lCGrpContInfo.getBankCode();
		String BankAccNo = lCGrpContInfo.getBankAccNo();
		String AccName = lCGrpContInfo.getAccName();
		String Currency = lCGrpContInfo.getCurrency();
		String CValiDate = lCGrpContInfo.getCValiDate();
		String HandlerDate = lCGrpContInfo.getHandlerDate();
		String OutPayFlag = lCGrpContInfo.getOutPayFlag();
		String EnterKind = lCGrpContInfo.getEnterKind();
		String AmntGrade = lCGrpContInfo.getAmntGrade();
		String Peoples3 = lCGrpContInfo.getPeoples3();
		String OnWorkPeoples = lCGrpContInfo.getOnWorkPeoples();
		String OffWorkPeoples = lCGrpContInfo.getOffWorkPeoples();
		String OtherPeoples = lCGrpContInfo.getOtherPeoples();
		String RelaPeoples = lCGrpContInfo.getRelaPeoples();
		String RelaMatePeoples = lCGrpContInfo.getRelaMatePeoples();
		String RelaYoungPeoples = lCGrpContInfo.getRelaYoungPeoples();
		String RelaOtherPeoples = lCGrpContInfo.getRelaOtherPeoples();
		String FirstTrialOperator = lCGrpContInfo.getFirstTrialOperator();
		if("".equals(FirstTrialOperator) || null == FirstTrialOperator){
			errorValue = "初审人员不能为空";
			return false;
		}
		String ReceiveDate = lCGrpContInfo.getReceiveDate();
		String Prem = lCGrpContInfo.getPrem();
		String Amnt = lCGrpContInfo.getAmnt();
		String TempFeeNo = lCGrpContInfo.getTempFeeNo();
		String BusinessBigType = lCGrpContInfo.getBusinessBigType();
		String CoInsuranceFlag = lCGrpContInfo.getCoInsuranceFlag();
		String Crs_SaleChnl = lCGrpContInfo.getCrs_SaleChnl();
		String Crs_BussType = lCGrpContInfo.getCrs_BussType();
		String GrpNature = lCGrpContInfo.getGrpNature();
		String BusinessType = lCGrpContInfo.getBusinessType();
		String Peoples = lCGrpContInfo.getPeoples();
		String RgtMoney = lCGrpContInfo.getRgtMoney();
		String Asset = lCGrpContInfo.getAsset();
		String NetProfitRate = lCGrpContInfo.getNetProfitRate();
		String MainBussiness = lCGrpContInfo.getMainBussiness();
		String Corporation = lCGrpContInfo.getCorporation();
		String ComAera = lCGrpContInfo.getComAera();
		String Phone = lCGrpContInfo.getPhone();
		String Fax = lCGrpContInfo.getFax();
		String FoundDate = lCGrpContInfo.getFoundDate();
		String Remark = lCGrpContInfo.getRemark();
		String HandlerName = lCGrpContInfo.getHandlerName();
		String HandlerPrint = lCGrpContInfo.getHandlerPrint();
		String AgentDate = lCGrpContInfo.getAgentDate();
		String MarketType = lCGrpContInfo.getMarketType();
		String CInValiDate = lCGrpContInfo.getCInValiDate();
		String AskGrpContNo = lCGrpContInfo.getAskGrpContNo();
		String GrpContSumPrem = lCGrpContInfo.getGrpContSumPrem();
		String BigProjectFlag = lCGrpContInfo.getBigProjectFlag();
		String ContPrintType = lCGrpContInfo.getContPrintType();
		String GrpAgentCom = lCGrpContInfo.getGrpAgentCom();
		String GrpAgentIDNo = lCGrpContInfo.getGrpAgentIDNo();
		String GrpAgentCode = lCGrpContInfo.getGrpAgentCode();
		String GrpAgentName = lCGrpContInfo.getGrpAgentName();
		
		// 投保录入人员 写死
		//String InputOperator = "CP3613";
		String InputOperator = "CENA";
		String HandlerIDNo = lCGrpContInfo.getHandlerIDNo();
		String ImpartParam1 = lCGrpContInfo.getImpartParam1();
		String ImpartParam2 = lCGrpContInfo.getImpartParam2();
		String ImpartParamModle = "";
		if(!"".equals(ImpartParam1) && null != ImpartParam1 && !"".equals(ImpartParam2) && null != ImpartParam2){
			if(Integer.valueOf(ImpartParam1) + Integer.valueOf(ImpartParam2) > 100 || Integer.valueOf(ImpartParam1) + Integer.valueOf(ImpartParam2) < 100){
				errorValue = "投保人承担"+ImpartParam1+"與被保险人承担"+ImpartParam2+"的和 不是100 ";
				return false;
			}
			ImpartParamModle = "N,N,Y,"+ImpartParam1+","+ImpartParam2;
		}
		if(("".equals(ImpartParam1) || null == ImpartParam1)  && (!"".equals(ImpartParam2) && null != ImpartParam2)){
			if(!"100".equals(ImpartParam2)){
				errorValue = "被保险人承担:"+ImpartParam2+" 不是100 ";
				return false;
			}
			ImpartParamModle = "N,Y,N,N,N";
		}
		if(("".equals(ImpartParam2) || null == ImpartParam2)  && (!"".equals(ImpartParam1) && null != ImpartParam1)){
			if(!"100".equals(ImpartParam1)){
				errorValue = "投保人承担:"+ImpartParam1+" 不是100 ";
				return false;
			}
			ImpartParamModle = "Y,N,N,N,N";
		}
		
		LCGrpAppntInfo lCGrpAppntInfo = (LCGrpAppntInfo) data.getObjectByObjectName("LCGrpAppntInfo", 0);
		String OrgancomCode = lCGrpAppntInfo.getOrgancomCode();
		String GrpAddress = lCGrpAppntInfo.getGrpAddress();
		String GrpZipCode = lCGrpAppntInfo.getGrpZipCode();
		String UnifiedSocialCreditNo = lCGrpAppntInfo.getUnifiedSocialCreditNo();
		String TaxNo = lCGrpAppntInfo.getTaxNo();
		String LegalPersonName = lCGrpAppntInfo.getLegalPersonName();
		String LegalPersonIDNo = lCGrpAppntInfo.getLegalPersonIDNo();
		String AppntOnWorkPeoples = lCGrpAppntInfo.getAppntOnWorkPeoples();
		String AppntOffWorkPeoples = lCGrpAppntInfo.getAppntOffWorkPeoples();
		String AppntOtherPeoples = lCGrpAppntInfo.getAppntOtherPeoples();
		String ClaimBankCode = lCGrpAppntInfo.getClaimBankCode();
		String ClaimAccName = lCGrpAppntInfo.getClaimAccName();
		String ClaimBankAccNo = lCGrpAppntInfo.getClaimBankAccNo();
		String BusinessScope = lCGrpAppntInfo.getBusinessScope();
		String IDStartDate = lCGrpAppntInfo.getIDStartDate();
		String IDEndDate = lCGrpAppntInfo.getIDEndDate();
		String IDNo = lCGrpAppntInfo.getIDNo();
		String IDType = lCGrpAppntInfo.getIDType();
		String IDLongEffFlag = lCGrpAppntInfo.getIDLongEffFlag();

		LCGrpAddressInfo lCGrpAddressInfo = (LCGrpAddressInfo) data.getObjectByObjectName("LCGrpAddressInfo", 0);
		String LinkMan1 = lCGrpAddressInfo.getLinkMan1();
		String Department1 = lCGrpAddressInfo.getDepartment1();
		String HeadShip1 = lCGrpAddressInfo.getHeadShip1();
		String Phone1 = lCGrpAddressInfo.getPhone1();
		String E_Mail1 = lCGrpAddressInfo.getE_Mail1();
		String Fax1 = lCGrpAddressInfo.getFax1();
		String Mobile1 = lCGrpAddressInfo.getMobile1();
		String LinkMan2 = lCGrpAddressInfo.getLinkMan2();
		String Department2 = lCGrpAddressInfo.getDepartment2();
		String HeadShip2 = lCGrpAddressInfo.getHeadShip2();
		String Phone2 = lCGrpAddressInfo.getPhone2();
		String E_Mail2 = lCGrpAddressInfo.getE_Mail2();
		String Fax2 = lCGrpAddressInfo.getFax2();

		LCExtendInfo lCExtendInfo = (LCExtendInfo) data.getObjectByObjectName("LCExtendInfo", 0);
		String AssistSaleChnl = lCExtendInfo.getAssistSaleChnl();
		String AssistAgentCode = lCExtendInfo.getAssistAgentCode();

		VData tVData = new VData();
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema(); // 集体保单
		LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema(); // 团单投保人
		LDGrpSchema tLDGrpSchema = new LDGrpSchema(); // 团体客户
		LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema(); // 团体客户地址
		
		// 集体保单信息 LCGrpCont
		tLCGrpContSchema.setPrtNo(PrtNo); // 印刷号码
		tLCGrpContSchema.setManageCom(ManageCom); // 管理机构
		tLCGrpContSchema.setSaleChnl(SaleChnl); // 销售渠道
		tLCGrpContSchema.setSaleChnlDetail("01");// 销售渠道明细
		tLCGrpContSchema.setAgentCom(AgentCom); // 代理机构
		tLCGrpContSchema.setAgentType(AgentType); // 代理机构分类
		tLCGrpContSchema.setAgentCode(AgentCode); // 代理人编码
		tLCGrpContSchema.setAgentGroup(AgentGroup); // 代理人组别
		tLCGrpContSchema.setAgentCode1(AgentCode1); // 联合代理人代码
		tLCGrpContSchema.setAgentSaleCode(AgentSaleCode); // 代理销售人员编码
		tLCGrpContSchema.setGrpSpec(GrpSpec); // 集体特约
		tLCGrpContSchema.setPayMode(PayMode); // 缴费方式
		tLCGrpContSchema.setGrpName(GrpName); // 单位名称

		tLCGrpContSchema.setGetFlag(GetFlag); // 付款方式
		tLCGrpContSchema.setPayIntv(GrpContPayIntv);
		tLCGrpContSchema.setBankCode(BankCode); // 银行编码
		tLCGrpContSchema.setBankAccNo(BankAccNo); // 银行帐号
		tLCGrpContSchema.setAccName(AccName); // 银行帐号
		tLCGrpContSchema.setCurrency(Currency); // 币别
		tLCGrpContSchema.setCValiDate(CValiDate); // 保单生效日期
		tLCGrpContSchema.setPolApplyDate(HandlerDate); // 保单投保日期
		tLCGrpContSchema.setOutPayFlag(OutPayFlag); // 溢交处理方式
		tLCGrpContSchema.setEnterKind(EnterKind); // 参保形式
		tLCGrpContSchema.setAmntGrade(AmntGrade); // 保额等级
		tLCGrpContSchema.setPeoples3(Peoples3); // 单位可投保人数
		tLCGrpContSchema.setOnWorkPeoples(OnWorkPeoples); // 单位可投保人数
		tLCGrpContSchema.setOffWorkPeoples(OffWorkPeoples); // 单位可投保人数
		tLCGrpContSchema.setOtherPeoples(OtherPeoples); // 单位可投保人数
		tLCGrpContSchema.setRelaPeoples(RelaPeoples); // 连带被保人人数
		tLCGrpContSchema.setRelaMatePeoples(RelaMatePeoples); // 配偶人数
		tLCGrpContSchema.setRelaYoungPeoples(RelaYoungPeoples); // 子女人数
		tLCGrpContSchema.setRelaOtherPeoples(RelaOtherPeoples); // 连带其他人员数
		tLCGrpContSchema.setFirstTrialOperator(FirstTrialOperator);
		tLCGrpContSchema.setReceiveDate(ReceiveDate);
		
		//TODO 
		//JL
		tLCGrpContSchema.setPrem(Prem);
		tLCGrpContSchema.setAmnt(Amnt);
		tLCGrpContSchema.setTempFeeNo(TempFeeNo);
		tLCGrpContSchema.setBusinessBigType(BusinessBigType);

		// 共保标志
		tLCGrpContSchema.setCoInsuranceFlag(CoInsuranceFlag);

		// 集团交叉业务标识。
		tLCGrpContSchema.setCrs_SaleChnl(Crs_SaleChnl);
		tLCGrpContSchema.setCrs_BussType(Crs_BussType);

		tLCGrpContSchema.setGrpNature(GrpNature); // 单位性质
		tLCGrpContSchema.setBusinessType(BusinessType); // 行业类别
		tLCGrpContSchema.setPeoples(Peoples); // 总人数
		tLCGrpContSchema.setRgtMoney(RgtMoney); // 注册资本
		tLCGrpContSchema.setAsset(Asset); // 资产总额
		tLCGrpContSchema.setNetProfitRate(NetProfitRate); // 净资产收益率
		tLCGrpContSchema.setMainBussiness(MainBussiness); // 主营业务
		tLCGrpContSchema.setCorporation(Corporation); // 法人
		tLCGrpContSchema.setComAera(ComAera); // 机构分布区域
		tLCGrpContSchema.setPhone(Phone); // 总机
		tLCGrpContSchema.setFax(Fax); // 传真
		tLCGrpContSchema.setFoundDate(FoundDate); // 成立时间
		tLCGrpContSchema.setRemark(Remark); // 备注
		tLCGrpContSchema.setHandlerName(HandlerName);
		tLCGrpContSchema.setHandlerDate(HandlerDate);
		tLCGrpContSchema.setHandlerPrint(HandlerPrint);
		tLCGrpContSchema.setAgentDate(AgentDate);
		tLCGrpContSchema.setMarketType(MarketType); // 市场类型
		tLCGrpContSchema.setCInValiDate(CInValiDate);
		tLCGrpContSchema.setAskGrpContNo(AskGrpContNo);
		tLCGrpContSchema.setPremScope(GrpContSumPrem);
		tLCGrpContSchema.setBigProjectFlag(BigProjectFlag); // 大项目标识
		tLCGrpContSchema.setContPrintType(ContPrintType); // 保单打印类型
		tLCGrpContSchema.setGrpAgentCom(GrpAgentCom);
		tLCGrpContSchema.setGrpAgentIDNo(GrpAgentIDNo);
		tLCGrpContSchema.setGrpAgentCode(GrpAgentCode);
		tLCGrpContSchema.setGrpAgentName(GrpAgentName);
		String tInputOperator = InputOperator;
		if ("".equals(tInputOperator) || null == tInputOperator) {
			tLCGrpContSchema.setInputOperator(GI.Operator);
		} else {
			tLCGrpContSchema.setInputOperator(InputOperator);
		}
		
		tLCGrpContSchema.setHandlerIDNo(HandlerIDNo);// 业务经办人身份证号
		// 团单投保人信息 LCGrpAppnt
		tLCGrpAppntSchema.setPrtNo(PrtNo); // 印刷号码
		tLCGrpAppntSchema.setName(GrpName);
		tLCGrpAppntSchema.setOrganComCode(OrgancomCode); // 组织机构
		tLCGrpAppntSchema.setPostalAddress(GrpAddress);
		tLCGrpAppntSchema.setZipCode(GrpZipCode);
		tLCGrpAppntSchema.setUnifiedSocialCreditNo(UnifiedSocialCreditNo);
		tLCGrpAppntSchema.setPhone(Phone);
		// 反洗钱新增
		tLCGrpAppntSchema.setTaxNo(TaxNo);
		tLCGrpAppntSchema.setLegalPersonName(LegalPersonName);
		tLCGrpAppntSchema.setLegalPersonIDNo(LegalPersonIDNo);

		// 在团单投保人信息层，加入人员相关基本信息的采集。
		tLCGrpAppntSchema.setPeoples(Peoples);
		tLCGrpAppntSchema.setOnWorkPeoples(AppntOnWorkPeoples);
		tLCGrpAppntSchema.setOffWorkPeoples(AppntOffWorkPeoples);
		tLCGrpAppntSchema.setOtherPeoples(AppntOtherPeoples);

		// 理赔金账户
		tLCGrpAppntSchema.setClaimBankCode(ClaimBankCode);
		tLCGrpAppntSchema.setClaimAccName(ClaimAccName);
		tLCGrpAppntSchema.setClaimBankAccNo(ClaimBankAccNo);

		// #2445 投保书新增字段
		tLCGrpAppntSchema.setBusinessScope(BusinessScope);
		tLCGrpAppntSchema.setIDStartDate(IDStartDate);
		tLCGrpAppntSchema.setIDEndDate(IDEndDate);
		tLCGrpAppntSchema.setIDNo(IDNo);
		tLCGrpAppntSchema.setIDType(IDType);
		tLCGrpAppntSchema.setIDLongEffFlag(IDLongEffFlag);

		// 团体客户信息 LDGrp
		tLDGrpSchema.setGrpName(GrpName); // 单位名称
		tLDGrpSchema.setOrganComCode(OrgancomCode); // 组织机构
		tLDGrpSchema.setGrpNature(GrpNature); // 单位性质
		tLDGrpSchema.setBusinessType(BusinessType); // 行业类别
		tLDGrpSchema.setPeoples(Peoples); // 总人数
		tLDGrpSchema.setOnWorkPeoples(AppntOnWorkPeoples); // 总人数
		tLDGrpSchema.setOffWorkPeoples(AppntOffWorkPeoples); // 总人数
		tLDGrpSchema.setOtherPeoples(AppntOtherPeoples); // 总人数
		tLDGrpSchema.setRgtMoney(RgtMoney); // 注册资本
		tLDGrpSchema.setAsset(Asset); // 资产总额
		tLDGrpSchema.setNetProfitRate(NetProfitRate); // 净资产收益率
		tLDGrpSchema.setMainBussiness(MainBussiness); // 主营业务
		tLDGrpSchema.setCorporation(Corporation); // 法人
		tLDGrpSchema.setComAera(ComAera); // 机构分布区域
		tLDGrpSchema.setPhone(Phone); // 总机
		tLDGrpSchema.setFax(Fax); // 传真
		tLDGrpSchema.setFoundDate(FoundDate); // 成立时间
		tLDGrpSchema.setGrpAppntNum("0");// 记录当前客户投保次数，每签单一次投保次数加1，初始为0
		tLDGrpSchema.setUnifiedSocialCreditNo(UnifiedSocialCreditNo); // 统一社会信用代码
		// 团体客户地址 LCGrpAddress
		tLCGrpAddressSchema.setGrpAddress(GrpAddress); // 单位地址
		tLCGrpAddressSchema.setGrpZipCode(GrpZipCode); // 单位邮编

		// 保险联系人一
		tLCGrpAddressSchema.setLinkMan1(LinkMan1);
		tLCGrpAddressSchema.setDepartment1(Department1);
		tLCGrpAddressSchema.setHeadShip1(HeadShip1);
		tLCGrpAddressSchema.setPhone1(Phone1);
		tLCGrpAddressSchema.setE_Mail1(E_Mail1);
		tLCGrpAddressSchema.setFax1(Fax1);
		tLCGrpAddressSchema.setMobile1(Mobile1);
		// 保险联系人二
		tLCGrpAddressSchema.setLinkMan2(LinkMan2);
		tLCGrpAddressSchema.setDepartment2(Department2);
		tLCGrpAddressSchema.setHeadShip2(HeadShip2);
		tLCGrpAddressSchema.setPhone2(Phone2);
		tLCGrpAddressSchema.setE_Mail2(E_Mail2);
		tLCGrpAddressSchema.setFax2(Fax2);
		// 增加对综合开拓数据的处理
		LCExtendSchema tLCExtendSchema = new LCExtendSchema();
		tLCExtendSchema.setPrtNo(PrtNo);
		tLCExtendSchema.setAssistSalechnl(AssistSaleChnl);
		tLCExtendSchema.setAssistAgentCode(AssistAgentCode);
		tVData.add(tLCExtendSchema);
		System.out.println("end setSchema:");
		// 准备传输数据 VData
		tVData.add(tLCGrpContSchema);
		tVData.add(tLCGrpAppntSchema);
		tVData.add(tLDGrpSchema);
		tVData.add(tLCGrpAddressSchema);
		if(!"".equals(ImpartParamModle) && ImpartParamModle != null){
			LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
			LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
			tLCCustomerImpartSchema.setCustomerNoType("0");
			tLCCustomerImpartSchema.setImpartCode("010");
			tLCCustomerImpartSchema.setImpartContent("□ 投保人全额承担  □被保人全额承担  □双方共同承担,其中投保人承担______％，被保人承担______％");
			tLCCustomerImpartSchema.setImpartParamModle(ImpartParamModle);
			tLCCustomerImpartSchema.setImpartVer("021") ;
			tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
			tVData.add( tLCCustomerImpartSet );
		}
		tVData.add(GI);

		// 传递非SCHEMA信息
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LoadFlag", 2);
		tTransferData.setNameAndValue("SavePolType", 0); // 保全保存标记，默认为0，标识非保全
		System.out.println("SavePolType，BQ is 2，other is 0 : " + 0);
		tVData.addElement(tTransferData);

		GroupContUI tGroupContUI = new GroupContUI();
		if (tGroupContUI.submitData(tVData, "INSERT||GROUPPOL") == false) {
			errorValue = " 保存失败，原因是: " + tGroupContUI.mErrors.getError(0).errorMessage;
			return false;
		} else {
			VData dataResult = new VData();
			dataResult = tGroupContUI.getResult();
			// 保单信息
			LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
			mLCGrpContSchema.setSchema((LCGrpContSchema) dataResult.getObjectByObjectName("LCGrpContSchema", 0));
			ProposalGrpContNo = mLCGrpContSchema.getProposalGrpContNo();
			GrpContNo = mLCGrpContSchema.getGrpContNo();
			GrpNo = mLCGrpContSchema.getAppntNo();
		}
		return true;
	}
}
