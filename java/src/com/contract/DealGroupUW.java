package com.contract;

import com.contract.obj.LCGCUWMasterInfo;
import com.contract.obj.LCGrpContInfo;
import com.sinosoft.lis.cbcheck.UWSendTraceAllUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCCUWMasterSchema;
import com.sinosoft.lis.schema.LCGCUWMasterSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCUWSendTraceSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;

/**
 * 人工核保  团体保单整单确认
 * @author Administrator
 *
 */
public class DealGroupUW {
	//错误信息
	private String Content = "";
	//从报文保存节点中获取
	//印刷号码  
	private String PrtNo="";
	//从核保节点中获取值
	//团体保单核保结论
	private String GUWState="";
	//团体保单核保意见
	private String GUWIdea="";
	//查询出来的字段
	private String GrpContNo="";
	private String GrpSendUpFlag="";
	private String MissionID="";
	private String SubMissionID="";
	private GlobalInput tGI = new GlobalInput(); 
	public boolean dealData(VData data){
		System.out.println("***********************************人工核保  团体保单整单确认****************************************");
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		tGI.Operator = "CENA";
		tGI.ManageCom = lCGrpContInfo.getManageCom();
		//原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
		tGI.ComCode = tGI.ManageCom;
		
		PrtNo = lCGrpContInfo.getPrtNo();
		LCGCUWMasterInfo tLCGCUWMasterInfo=(LCGCUWMasterInfo) data.getObjectByObjectName("LCGCUWMasterInfo", 0);
		GUWState=tLCGCUWMasterInfo.getGUWState();
		GUWIdea=tLCGCUWMasterInfo.getGUWIdea();
		if(!deal()){
			return false;
		}
		return true;
	}
	public boolean deal(){
		if(!gmanuchk()){
			return false;
		}
		if(!dealGmanuchk()){
			return false;
		}
		return true;
	}
	/**
	 * 人工核保  团体保单整单确认 前台校验
	 * @return
	 */
	public boolean gmanuchk(){
		//暂时先这么处理 使得可以操作人工核保 xu
		String sql = "update lwmission set defaultoperator='CENA'  where missionprop2='"+PrtNo+"'";
	 	ExeSQL tExeSQL = new ExeSQL();
	 	tExeSQL.execUpdateSQL(sql);
		//团单--人工核保	
		String operFlag="2";
		if(operFlag == "2")
		{
		//核保的操作人员  以后再定 xu	
//		String operator="UW0057";
		String operator="CENA";
		String uwUser = "";
		if(!"group".equals(operator)){
			uwUser = "    and (t.defaultoperator ='" +operator+ "' or t.defaultoperator in (select usercode from lduwuser where agentusercode ='"+operator+"' and uwtype='02'))";
		}
			//待处理信箱		
		String strSQL =" select z.prtno,z.grpname,z.cvalidate,z.prem,case when t.missionprop12='0' then '上报' when t.missionprop12='1' "      
							+" then '核保回退' when t.missionprop12='2' then '抽检' when (select count(1) from lcuwsendtrace where sendtype in('3','4') and otherno=z.proposalgrpcontno)>0 then '问题件回退' when  (select count(1) from lbmission where "                    
							+" activityid = '0000002001' and missionprop2=z.prtno and missionprop20='N' and "                               
							+" t.missionprop12 is null and missionprop2 not in (select doccode from es_doc_main   where "                           
							+" (doccode = z.prtno || '101' or doccode = z.prtno || '102') and SubType='TB23' and busstype='TB')) = 1 "                                            
							+" then '问题件已修改'  when (select count(1) from es_doc_main   where (doccode = z.prtno || '101' or doccode = z.prtno || '102') "                     
							+" and SubType='TB23' and busstype='TB')=1 then '问题件回销'  else '新单' end ,z.managecom,z.grpcontno,t.MissionProp19,t.SubMissionID,t.activityid,t.MissionID " 
							+" from lcgrpcont z,lwmission t where  t.activityid='0000002004'  and "                                                 
							+" t.activitystatus in ('1','3')   and t.missionprop1=z.proposalgrpcontno "      
			 				+uwUser
			 + " and missionprop2='"+PrtNo+"'  order by t.MissionProp19 with ur ";
		SSRS strSQLSSRS=tExeSQL.execSQL(strSQL);	
		if(strSQLSSRS.getMaxNumber()<=0){
			Content="团体保单整单确认失败，失败原因:印刷号码"+PrtNo+"不能操作团体保单整单确认！ ";
			System.out.println(Content);
			return false;
		}
		GrpContNo=strSQLSSRS.GetText(1, 7);
		MissionID=strSQLSSRS.GetText(1, 11);
		SubMissionID=strSQLSSRS.GetText(1, 9);
		 /* strSQL1 = "select A,B,C,D,ChNote(E,F,G),H,L,J,K,N "   
								+"from"                                                                                                                                            
								+" ("                                                                                                                                               
								+"select z.prtno A,z.grpname B,z.cvalidate C,z.prem D,z.managecom H,z.grpcontno L,t.SubMissionID J,t.activityid K,t.MissionID N,"                                               
								+"(SELECT count(1) FROM LOPRTManager where Code = '74' and OtherNo=z.grpcontno  AND PrtType = '0') E,"           
								+"(SELECT count(1) FROM LOPRTManager where Code = '03' and standbyflag3=z.grpcontno  AND PrtType = '0') F,"           
								+"(SELECT count(1) FROM LOPRTManager where Code = '54' and OtherNo=z.grpcontno  AND PrtType = '0') G "            
								+" from lcgrpcont z,lwmission t where t.activityid='0000002004' and t.activitystatus in ('2') and t.missionprop1=z.grpcontno" 
										 				+uwUser         
							  + getWherePart("t.missionprop2", "PrtNo")
							  + getWherePart("z.GrpName", "AppntName","like")
							  + getWherePart("z.CValiDate", "CValiDate")
							  + getWherePart("z.ManageCom", "ManageCom","like")
							  + getWherePart("getUniteCode(t.missionprop5)", "AgentCode")
							  +")as X " 
								+"order by A"
								;	*/
		/*
		 * by``GY
		 * 2017-12-07
		 * 人工核保uwflag=9
		 */
		String lccontUwflagSQL = "update lccont set uwflag='9' where  grpcontno='"+GrpContNo+"'"; 
	 	tExeSQL.execUpdateSQL(lccontUwflagSQL);
		String lcgrpcotUwflag  = "update lcgrpcont set uwflag='9' where  grpcontno='"+GrpContNo+"'";
		tExeSQL.execUpdateSQL(lcgrpcotUwflag);
		String strSql = "select count(1) from lccont where uwflag not in ('1','4','9','a') and grpcontno='"+GrpContNo+"'";
		SSRS arr = tExeSQL.execSQL(strSql);
		if(Integer.valueOf(arr.GetText(1, 1))>0){
			Content="团体保单整单确认失败，失败原因:团单下还有个单没有核保结论！ ";
			System.out.println(Content);
			return false;
		} 
		String flag="0";
		if(!"9".equals(GUWState)){
			Content="团体保单整单确认失败，失败原因:该操作只支持团体保单核保结论为9（9代表正常承保） ";
			System.out.println(Content);
			return false;
		}
		flag = GUWState;	 	

		 String strSql3 = "select sum(a) from (select 1 a from lcuwsendtrace where sendtype='2' and otherno='"+GrpContNo+"' union all select 1 a from lcuwsendtrace where sendflag='0' and otherno='"+GrpContNo+"') as x  ";
		 SSRS arr1 = tExeSQL.execSQL(strSql3);
		 if(null==arr1 ||"".equals(arr1)){
			 GrpSendUpFlag="0";
		 }else{
			 GrpSendUpFlag=arr1.GetText(1, 1);
		 }
		  if(Integer.valueOf(arr1.GetText(1, 1))>=2){
			GrpSendUpFlag="";
		  	String strsql = "select operator d from lcuwsendtrace where sendtype='2' and otherno='"+GrpContNo+"'";
		  	SSRS arr_2 = tExeSQL.execSQL(strsql);
		  	if(arr_2.getMaxNumber()>0){
			  	for(int m = 1 ; m <= arr_2.getMaxRow() ; m++ ){
			  		if(operator == arr_2.GetText(m, 1)){
			  			GrpSendUpFlag = "3";
			  		}
			  	}
			  }
		  }
		}
		return true;
	}
	// 对应UWConfirm.jsp"
	public boolean dealGmanuchk(){
		//输出参数
		CErrors tError = null;
		String tRela  = "";                
		String FlagStr="Succ";
		String tAction = "";
		String tOperate = "";
		String wFlag = "";
//		GlobalInput tG = new GlobalInput();
		//人员再定 xu
//	    tG.Operator ="UW0057";
//	    tG.ComCode="86";
//	    tG.ManageCom="86";
//		tG.Operator ="CENA";
//		tG.ComCode="86360500";
//		tG.ManageCom="86360500";
	    //xu request.getParameter("YesOrNo")
	    String tYesOrNo = "";
	    VData tVData = new VData();
	    VData mVData = new VData();
	  //工作流操作型别，根据此值检索活动ID，取出服务类执行具体业务逻辑
	    wFlag = "0000002004";
	    TransferData mTransferData = new TransferData();
	    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	    LCGrpContSet tLCGrpContSet =new LCGrpContSet();

	    tLCGrpContSchema.setGrpContNo(GrpContNo);
	    System.out.println("=================================gy ceshi====GUWState="+GUWState);
	    tLCGrpContSchema.setUWFlag(GUWState);
	    tLCGrpContSchema.setRemark(GUWIdea);
	    tLCGrpContSet.add(tLCGrpContSchema);
	    
	    LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
	    tLCUWSendTraceSchema.setOtherNoType("2");        //团险上报
	    tLCUWSendTraceSchema.setOtherNo(GrpContNo);
	    tLCUWSendTraceSchema.setUWFlag(GUWState);
	    tLCUWSendTraceSchema.setUWIdea(GUWIdea);
	    //	 xu   tLCUWSendTraceSchema.setYesOrNo(request.getParameter("YesOrNo"));
	    tLCUWSendTraceSchema.setYesOrNo("");
	    
	    //团体核保最近结果表
	    LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
	    tLCGCUWMasterSchema.setGrpContNo(GrpContNo);
	    tLCGCUWMasterSchema.setPassFlag(GUWState);
	    tLCGCUWMasterSchema.setUWIdea(GUWIdea);
	    
	    
	    //  BY  ----GY ---
	    //2017-12-11
	    String passFlagSQL ="update LCCUWMaster  set  PassFlag = '9' where grpcontno='"+GrpContNo+"'";
	    ExeSQL tExeSQL = new ExeSQL();
	    tExeSQL.execUpdateSQL(passFlagSQL);
	    /*
	    LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
	    tLCCUWMasterSchema.setGrpContNo(GrpContNo);
	    tLCCUWMasterSchema.setPassFlag(GUWState);
	    tLCCUWMasterSchema.setUWIdea(GUWIdea);
	    mTransferData.setNameAndValue("LCGCUWMasterSchema",tLCCUWMasterSchema); 
	    */
	   
	    mTransferData.setNameAndValue("LCGCUWMasterSchema",tLCGCUWMasterSchema);
	    mTransferData.setNameAndValue("LCUWSendTraceSchema",tLCUWSendTraceSchema); 
	    mTransferData.setNameAndValue("GrpSendUpFlag",GrpSendUpFlag); 
	    System.out.println("GrpSendUpFlag======="+GrpSendUpFlag);
	    tVData.add(mTransferData);
	    tVData.add(tLCGrpContSet);
	    tVData.add(tGI);

	    System.out.println(tLCGrpContSet.get(1).getGrpContNo());
		UWSendTraceAllUI tUWSendTraceAllUI = new UWSendTraceAllUI();
		if (!tUWSendTraceAllUI.submitData(tVData, "submit"))
		{
			int n = tUWSendTraceAllUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			Content = " 团体保单整单确认操作失败，上报核保失败，原因是: " + tUWSendTraceAllUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
			return false;
		}
		else
		{
			int n = tUWSendTraceAllUI.mErrors.getErrorCount();
			if(n>0)
			{
				for (int i = 0; i < n; i++)
				Content = " 团体保单整单确认操作失败，上报核保失败，原因是: " + tUWSendTraceAllUI.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";	
				return false;
			}
			if(FlagStr.equals("Succ"))
			{
					Content = " 上报核保成功！ ";
					String tUWSendFlag;
					String tUserCode;
					TransferData tTransferData = new TransferData();
					tTransferData = (TransferData) tUWSendTraceAllUI.getResult().getObjectByObjectName("TransferData", 0);
					tUWSendFlag =(String)tTransferData.getValueByName("SendFlag");
					tUserCode = (String)tTransferData.getValueByName("UserCode");
					
					System.out.println("tUWSendFlag========="+tUWSendFlag);
					if(tUWSendFlag.equals("9"))	           //核保下报
					{
							Content = "核保已下发给核保师："+tUserCode;
					}
					else if(tUWSendFlag.equals("1"))
					{
							Content = "核保已上报给核保师："+tUserCode;
					}
					System.out.println("tUWSendFlag========="+tUWSendFlag);
					if(tUWSendFlag.equals("0")||(tUWSendFlag.equals("0")&&tYesOrNo.equals("Y")))
					{
  						System.out.println("-------------------start workflow---------------------");
							TransferData nTransferData = new TransferData();
							
	    				nTransferData.setNameAndValue("MissionID",MissionID);  
   						nTransferData.setNameAndValue("SubMissionID",SubMissionID);	
   	   			 
   						mVData.add(nTransferData);
    					mVData.add(tLCGrpContSet);
    					mVData.add(tGI);
    															
  						GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
  						System.out.println("adfadfadfafafd wFlag  "+wFlag);
  						if( !tGrpTbWorkFlowUI.submitData( mVData, wFlag ) ) {
  						    Content = " 核保确认失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getError(0).errorMessage;
  						    FlagStr = "Fail";
  						    return false;
  						}else {
  							String tUwflagSql = "select uwflag from lcgrpcont where grpcontno = '"+tLCGrpContSet.get(1).getGrpContNo()+"' ";
  							String uwflag = new ExeSQL().getOneValue(tUwflagSql);
  							if("c".equals(uwflag)){
  								Content = "核保确认失败，原因是: 该单不符合临分规则要求，需总公司再保或核保审核，请及时追踪审核状态，以免延误！";
  	  						    return false;
  							}else{
  								Content = " 核保确认成功！";
  							}
  						    FlagStr = "Succ";
  						}
  				}
  		 }
  	}
 System.out.println("-------------------end workflow---------------------");

		return true;
	}
	public String getContent() {
		return Content;
	}
	
	public static void main(String[] args) {
		DealGroupUW a =new DealGroupUW();
		a.getContent();
		System.out.println(a.getContent());
	}
}
