package com.contract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jdom.Element;

import com.contract.obj.ContPlanInfoList;
import com.contract.obj.LCGrpContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.tb.GroupRiskUI;
import com.sinosoft.lis.tb.TBGrpWrapUI;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCRiskWrapSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 添加险种
 * @author 李颢
 *
 */
public class DealAddRecord {
	//错误信息
	String Content;
	//印刷号码  
	private String PrtNo;
	//险种代码
	private String RiskCode;
	//保费计算方式
	private String CalRule;
	//缴费频次 在保存节点下取值
	private String GrpContPayIntv;
	//保险责任生效日  在保存节点下取值
	private String CValiDate;
	//销售渠道  在保存节点下取值
	private String SaleChnl;
	private GlobalInput tGI = new GlobalInput(); //repair:
	//页面隐藏字段
	private String RiskType="";
	private String GrpContNo="";
	private String ExpPeoples="";
	private String SaleChnlDetail="";
	public boolean deal(VData data){
		System.out.println("*******************************添加险种 DealAddRecord***********************************************");
		LCGrpContInfo lCGrpContInfo = (LCGrpContInfo) data.getObjectByObjectName("LCGrpContInfo", 0);
		tGI.Operator = "CENA";
	    tGI.ManageCom = lCGrpContInfo.getManageCom();
	    //原接口Comcode 总是跟ManageCom保持一致为86360500，所以动态也修改为一致。
	    tGI.ComCode = tGI.ManageCom;
		PrtNo = lCGrpContInfo.getPrtNo();
		GrpContPayIntv = lCGrpContInfo.getGrpContPayIntv();
		CValiDate = lCGrpContInfo.getCValiDate();
		SaleChnl = lCGrpContInfo.getSaleChnl();
		ContPlanInfoList riskCodeInfo = (ContPlanInfoList) data.getObjectByObjectName("ContPlanInfoList", 0);
		List list = riskCodeInfo.getList();
		System.out.println("=================解析到的节点个数========="+list.size()); //2
		//GY by - 2018-03-15 一个险种下面多个保障计划
		List<String> list2 = new ArrayList<String>();
		Set<String> set = new HashSet<String>();
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Element ele = (Element) list.get(i);
				RiskCode = ele.getChildText("RiskCode"); // 690201
				CalRule = ele.getChildText("CalRule");

				list2.add(RiskCode);
				set.addAll(list2);
				System.out.println("List长度："+list2.size());
				System.out.println("Set长度："+set.size());
				if (set.size() >= list2.size()) {  // 1 1
					if(!addRecordCheck()){
						return false;
					}
				//	return addRecordCheck();
					//return true ;
				} else {
					break;
				}
			}
			
		}else{
			Content="添加险种失败， 险种信息为空！";
			System.out.println(Content);
			return false;
		}
		return true;
	}
	/**
	 * 添加险种 前台校验
	 * @return
	 */
	public boolean addRecordCheck(){
		if(null==RiskCode||"".equals(RiskCode)){
			Content="添加险种失败，失败原因:险种代码不可为空！";
			System.out.println(Content);
			return false;
		}
		if(null==CalRule||"".equals(CalRule)){
			Content="添加险种失败，失败原因:保费计算方式不可为空！";
			System.out.println(Content);
			return false;
		}
		String tContPrintType = "";
		String tContPrintTypeSQL = "select ContPrintType,ManageCom from lcgrpcont where prtno = '"+PrtNo+"' ";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS tContPrintTypeArr = tExeSQL.execSQL(tContPrintTypeSQL);
		if(tContPrintTypeArr.getMaxNumber()<=0){
			Content="添加险种失败，失败原因:获取保单数据失败！";
			System.out.println(Content);
			return false;
		}else{
			tContPrintType = tContPrintTypeArr.GetText(1, 1);
		}
		if("3".equals(tContPrintType)){
			String lgrpSql = "select 1 from ldriskmanage where riskcode='" +RiskCode
					+ "' and checktype='lgrprisk' and state='0' " 
					+ " and startdate <= current date and (enddate is null or enddate > current date) ";
			SSRS lgrpArr =tExeSQL.execSQL(lgrpSql);
			if(lgrpArr.getMaxNumber()<=0){
				Content="添加险种失败，失败原因:大团单保单不支持"+RiskCode+"险种，请修改保单属性或险种代码";
				System.out.println(Content);
				return false;
			}
		}
		if("4".equals(tContPrintType)){
			String grpBriefSql = "select 1 from ldcode where codetype = 'grpbriefrisk' and code ='" +RiskCode+"' and codename = '"+tContPrintTypeArr.GetText(1, 2).substring(0,4)+"' ";
			SSRS grpBriefArr =tExeSQL.execSQL(grpBriefSql);
			if(grpBriefArr.getMaxNumber()<=0){
				Content="添加险种失败，失败原因:简易团单保单不支"+RiskCode+"该险种，请修改保单属性或险种代码";
				System.out.println(Content);
				return false;
			}
		}
		if("5".equals(tContPrintType)){
			String grpBriefSql = "select code,codename,'Risk' from ldcode where codetype = 'grphjrisk' and code ='" +RiskCode+"'  and othersign = '1' ";
			SSRS grpBriefArr =tExeSQL.execSQL(grpBriefSql);
			if(grpBriefArr.getMaxNumber()<=0){
				Content="添加险种失败，失败原因:汇交件不支持"+RiskCode+"险种，请修改保单属性或险种代码";
				System.out.println(Content);
				return false;
			}
		}
		if(!"5".equals(tContPrintType)){
			String grpBriefSql = "select code,codename,'Risk',comcode from ldcode where codetype = 'grphjrisk' and code ='" +RiskCode+"'  and othersign = '1' ";
			SSRS grpBriefArr =tExeSQL.execSQL(grpBriefSql);
			if(grpBriefArr.getMaxNumber()>=1){
				if(!"3".equals(grpBriefArr.GetText(1, 4))){
					Content="添加险种失败，失败原因:非汇交件不支持"+RiskCode+"险种，请修改保单属性或险种代码";
					System.out.println(Content);
					return false;
				}
			}
		}
		//首先区分是套餐险种还是普通险种
		String strSql = "select 1 from LMRiskApp where RiskCode='"+RiskCode+"'"
								+" union "
								+"select 2 from LDRiskWrap where RiskWrapCode='"+RiskCode+"'";
		SSRS arr =tExeSQL.execSQL(strSql); 
		if(arr.getMaxNumber()>=1){
			if("1".equals(arr.GetText(1, 1))){
				RiskType = "Risk";
			}else if("2".equals(arr.GetText(1, 1))){
				RiskType = "Wrap";
			}
		}else{
			Content="添加险种失败，失败原因:"+RiskCode+"险种代码或套餐代码系统中不存在，请确认是否录入正确！";
			System.out.println(Content);
			return false;
		}
		String tGrpContNo="";
		String tGrpContNoSql="select * from LCGrpCont where  PrtNo = '"+PrtNo+"' fetch first 3000 rows only with ur ";
		SSRS tGrpContNoSSRS=tExeSQL.execSQL(tGrpContNoSql);
		if(tGrpContNoSSRS.getMaxNumber()<=0){
			Content="添加险种失败，失败原因:团体信息查询失败!";
			System.out.println(Content);
			return false;
		}else{
			tGrpContNo=	tGrpContNoSSRS.GetText(1, 1);
		}
		String inforSql="select c.*,getUniteCode(agentcode) from LCGrpCont c where GrpContNo = '" +tGrpContNo+ "' ";
		SSRS inforSqlSSRS=tExeSQL.execSQL(inforSql);
		String sProposalGrpContNo=inforSqlSSRS.GetText(1, 2);
		GrpContNo=inforSqlSSRS.GetText(1, 1);
		ExpPeoples=inforSqlSSRS.GetText(1, 55);
		SaleChnlDetail=inforSqlSSRS.GetText(1, 115);
		String riskcodeSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
		+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+GrpContNo+"' "
		+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
		+" (select sum(Peoples2) from lccontplan where GrpContNo='"+GrpContNo+"' "
		+" and ContPlancode in (select distinct contplancode from lccontplanrisk where "
		+" grpcontno='"+GrpContNo+"' and riskcode=a.riskcode)), "
		+" a.peoples2, "
		+" to_zero((select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno )) "
		+" From lcgrppol a,LMRiskApp b "
		+" Where b.riskcode not in (select Riskcode from LCRiskWrap where Grpcontno=a.GrpContNo) and  a.riskcode=b.riskcode   and  a.grpcontno='" +GrpContNo+"'  "
		+" union "
		+" select a,b,c,d,e, "                                                                                                         
		+" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
		+"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+GrpContNo+"' "                                      
		+"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
		+" (select sum(Peoples2) from lccontplan where GrpContNo='"+GrpContNo+"'  and ContPlancode "                                      
		+" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+GrpContNo+"' "                                        
		+" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
		+" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
		+"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
		+" and a.Grpcontno='"+GrpContNo+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  ;
		SSRS riskcodeSqlSSRS=tExeSQL.execSQL(riskcodeSql);
		String riskcode="";
		if(riskcodeSqlSSRS.getMaxNumber()>=1){
			riskcode=riskcodeSqlSSRS.GetText(1, 1);
		}


		//170501险种 必须 单独承保
		String strSQL = "select 1 from lcgrppol where prtno = '"+PrtNo+"'";
		SSRS arrSSRS =tExeSQL.execSQL(strSQL); 

		if(arr.getMaxNumber()>=1){
			if("170501".equals(RiskCode)){
				Content="添加险种失败，失败原因:单独承保的险种不能添加到已存在险种的保单里！";
				System.out.println(Content);
				return false;
			}
			if("170501".equals(riskcode)){
				Content="添加险种失败，失败原因:该保单存在单独承保的险种，不能继续添加险种！";
				System.out.println(Content);
				return false;
			}
		}//End 170501 险种 必须 单独承保
		//170501险种 算费方式 为 表定费率
		if("170501".equals(RiskCode) && !"0".equals(CalRule)){
			Content="添加险种失败，失败原因:"+RiskCode+"险种的算费方式应为'表定费率'，请核实！";
			System.out.println(Content);
			return false;
		}//End 170501险种 算费方式 为 表定费率
		//170501险种 缴费方式 为 趸缴
		if("170501".equals(RiskCode) &&!"0".equals(GrpContPayIntv)){
			Content="添加险种失败，失败原因:"+RiskCode+"险种的缴费方式应为'趸缴'，请核实！";
			System.out.println(Content);
			return false;
		}//End 170501险种 缴费方式 为 趸缴
		if("122502".equals(RiskCode) && !"3".equals(CalRule)){
			Content="添加险种失败，失败原因:"+RiskCode+"险种的算费方式应为'约定费率'，请核实！";
			System.out.println(Content);
			return false;
		}//End 122502险种 算费方式 为 约定费率
		 if(!ChkRiskVer()){
			 return false; 
		 } 
		 if(null==GrpContNo ||"".equals(GrpContNo)) {
			    Content="添加险种失败，失败原因:团单合同信息未保存，不容许〔添加险种〕！";
				System.out.println(Content);
				return false;
			  }
		 if("Risk".equals(RiskType)){
			 	return groupRiskSave();
			}else if("Wrap".equals(RiskType)){
				return groupWrapSave();
			}
		return true;
	}
	public boolean groupRiskSave(){
		//fm.all('fmAction').value="INSERT||GROUPRISK";
		//接收信息，并作校验处理。
		//输入参数
		LCGrpContSchema tLCGrpContSchema   = new LCGrpContSchema();
		LCGrpPolSet mLCGrpPolSet =new LCGrpPolSet();
		LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
		LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();

		TransferData tTransferData = new TransferData();
		GroupRiskUI tGroupRiskUI   = new GroupRiskUI();
		//输出参数
		String FlagStr = "";
		String mLoadFlag = "2";

		tTransferData.setNameAndValue("LoadFlag",mLoadFlag);
		
		String tGrpPolNo = "";
	//	GlobalInput tGI = new GlobalInput(); //repair:
	//	tGI.Operator ="CP3613";
	//	tGI.Operator ="CENA";
	//	tGI.ComCode="86360500";
		System.out.println("tGI"+tGI);
		String Operator  = tGI.Operator ;  //保存登陆管理员账号
	//  String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
		String ManageCom = tGI.ManageCom;
		  CErrors tError = null;
		  String tBmCert = "";
		  System.out.println("aaaa");
		  //后面要执行的动作：添加，修改，删除
		  String fmAction="INSERT||GROUPRISK";
		  System.out.println("fmAction:"+fmAction);
		  if(fmAction.equals("INSERT||GROUPRISK"))
		  {
		    tLCGrpContSchema.setGrpContNo(GrpContNo);
		    tLCGrpContSchema.setPrtNo(PrtNo);

		    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
		    tLCGrpPolSchema.setRiskCode(RiskCode);
		    tLCGrpPolSchema.setCValiDate(CValiDate);
		    tLCGrpPolSchema.setPayIntv(GrpContPayIntv);
		    tLCGrpPolSchema.setExpPeoples(ExpPeoples);
		    tLCGrpPolSchema.setSaleChnl(SaleChnl);            //销售渠道
		    tLCGrpPolSchema.setSaleChnlDetail(SaleChnlDetail);
		    mLCGrpPolSet.add(tLCGrpPolSchema);
		    //将保费计算方式封装到要素表中，添加为一个全局计划要素，
		    //在添加保险计划时将会为同一险种的每一个责任添加此要素
		    if(!"".equals(StrTool.cTrim(CalRule))){
			    tLCContPlanDutyParamSchema.setGrpContNo(GrpContNo);
			    tLCContPlanDutyParamSchema.setRiskCode(RiskCode);
			    tLCContPlanDutyParamSchema.setProposalGrpContNo(GrpContNo);
			    tLCContPlanDutyParamSchema.setCalFactorValue(CalRule);
			    tLCContPlanDutyParamSchema.setCalFactor("CalRule");
			    tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
		    }
		  else{
		  		tLCContPlanDutyParamSet = null;
		  	}
		  }
		  try
		  {
		    // 准备传输数据 VData
		    VData tVData = new VData();
		    tVData.add(mLCGrpPolSet);
		    tVData.add(tLCContPlanDutyParamSet);
		    tVData.add(tLCGrpContSchema);
		    tVData.add(tTransferData);
		    tVData.add(tGI);
		    
		    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		    if ( tGroupRiskUI.submitData(tVData,fmAction))
		    {
		      if (fmAction.equals("INSERT||GROUPRISK"))
		      {
		        System.out.println("11111------return");

		        tVData.clear();
		        tVData = tGroupRiskUI.getResult();

		        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
		        LCContPlanDutyParamSet mLCContPlanDutyParamSet = (LCContPlanDutyParamSet)tVData.getObjectByObjectName("LCContPlanDutyParamSet",0);
		        tLCGrpPolSet=(LCGrpPolSet)tVData.getObjectByObjectName("LCGrpPolSet",0);
		        TransferData mTransferData = new TransferData();
		        mTransferData=(TransferData)tVData.getObjectByObjectName("TransferData",0);
		        String RiskName=(String)mTransferData.getValueByName("RiskName");
		        RiskCode = tLCGrpPolSet.get(1).getRiskCode();
		        tGrpPolNo = tLCGrpPolSet.get(1).getGrpPolNo();
		        System.out.println("RiskName"+RiskName);
		      }
		      
		    }
		  }
		  catch(Exception ex)
		  {
		    Content = "添加险种保存失败，原因是:" + ex.toString();
		    System.out.println(Content);
		    FlagStr = "Fail";
		    return false;
		  } 
		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		  if (FlagStr=="")
		  {
		    tError = tGroupRiskUI.mErrors;
		    if (!tError.needDealError())
		    {
		      Content ="保存成功！";
			  System.out.println(Content);
		      FlagStr = "Succ";
		    }
		    else
		    {
		      Content = "添加险种保存失败，原因是:" + tError.getFirstError();
			  System.out.println(Content);
		      FlagStr = "Fail";
		      return false;
		    }
		  }
			return true;
	}
	public boolean groupWrapSave(){
		//接收信息，并作校验处理。
		//输入参数

		LCRiskWrapSet tLCRiskWrapSet = new LCRiskWrapSet();
		TransferData tTransferData = new TransferData();
		TBGrpWrapUI tTBGrpWrapUI   = new TBGrpWrapUI();
		//输出参数
		String FlagStr = "";
		String mLoadFlag = "";
//		GlobalInput tGI = new GlobalInput(); //repair:
//		tGI.Operator ="CP3613";
//		tGI.Operator ="CENA";
//		tGI.ComCode="86360500";
		System.out.println("tGI"+tGI);
		String Operator  = tGI.Operator ;  //保存登陆管理员账号
//		String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
		String ManageCom = tGI.ManageCom;
		CErrors tError = null;
		//后面要执行的动作：添加，修改，删除
		String fmAction="INSERT||GROUPRISK";
		System.out.println("fmAction:"+fmAction);
		  if("INSERT||GROUPRISK".equals(fmAction)){
			  	tTransferData.setNameAndValue("GrpContNo",GrpContNo);
					tTransferData.setNameAndValue("WrapCode",RiskCode);
			  }
		  try
		  {
		    // 准备传输数据 VData
		    VData tVData = new VData();

		    tVData.add(tLCRiskWrapSet);
		    tVData.add(tTransferData);
		    tVData.add(tGI);



		    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
				tTBGrpWrapUI.submitData(tVData,fmAction);

		  }catch(Exception ex)
		  {
			    Content = " 添加险种保存失败，原因是:" + ex.toString();
			    System.out.println(Content);
			    FlagStr = "Fail";
			    return false;
			  }
		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		  if (FlagStr=="")
		  {
		    tError = tTBGrpWrapUI.mErrors;
		    if (!tError.needDealError())
		    {
		      Content ="保存成功！";
			  System.out.println(Content);
		      FlagStr = "Succ";
		    }
		    else
		    {
		      Content = "添加险种保存失败，原因是:" + tError.getFirstError();
		      System.out.println(Content);
		      FlagStr = "Fail";
		      return false;
		    }
		  }

		return true;
	}
	public boolean ChkRiskVer()
	{
		String strSql = "select enddate from LMRiskApp where "
								+" riskcode ='"+RiskCode+"'";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrResult=tExeSQL.execSQL(strSql);
	    if (arrResult.getMaxNumber()>=1)
	    {
	    	if(null!=arrResult.GetText(1, 1)&&!"".endsWith(arrResult.GetText(1, 1))){
		    	  boolean date=false;
		    	  String enddate =arrResult.GetText(1, 1);
			        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			        Date tCValiDate;
			        long diff = 0;
					try {
						tCValiDate =  df.parse(CValiDate);
				        Date tenddate = df.parse(enddate);
				         diff = tenddate.getTime() - tCValiDate.getTime();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Content=e.getMessage();
					   	return false;
					}
			        if(diff<0){
			        	date=true;
			        }
		      if(date && !"".equals(arrResult.GetText(1, 1)))
		      {
		    	  Content=RiskCode+"该产品已经停售! ";
				  System.out.println(Content);
				  return false;
		      }
		    else
		    {
		      return true;
		    }
	    	}
	    }
		return true;
	}
	public static void main(String[] args) {
		DealAddRecord a=new DealAddRecord();
		a.addRecordCheck();
	}
	public String getContent() {
		return Content;
	}
}
