
package com.contract;

import com.sinosoft.lis.db.LockTableDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LockTableSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class ContractLockTableActionBL
{
    
    public CErrors mErrors = new CErrors();

    
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    
    private MMap mMap = new MMap();

    
    private String mOperate = "";

    
    private String mLockNoKey = null;

    
    private String mLockNoType = null;

    
    private long mAvailabilityIntervalSecond = 0;
    
    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public ContractLockTableActionBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mLockNoKey = (String) mTransferData.getValueByName("LockNoKey");
        if (mLockNoKey == null)
        {
            buildError("getInputData", "参数[LockNoKey]不存在。");
            return false;
        }

        mLockNoType = (String) mTransferData.getValueByName("LockNoType");
        if (mLockNoType == null)
        {
            buildError("getInputData", "参数[mLockNoType]不存在。");
            return false;
        }

        String tAIS = (String) mTransferData
                .getValueByName("AvailabilityIntervalSecond");
        if (tAIS != null)
        {
            try
            {
                mAvailabilityIntervalSecond = Long.parseLong(tAIS);
            }
            catch (Exception e)
            {
                buildError("getInputData",
                        "参数[AvailabilityIntervalSecond]应该为整数。");
                return false;
            }
        }

        return true;
    }

    private boolean dealData()
    {
        MMap tMMap = null;

        
        tMMap = lockAction();
        if (tMMap == null)
        {
            return false;
        }
        mMap.add(tMMap);
        

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    
    private MMap lockAction()
    {
        MMap tMMap = new MMap();

        
        LockTableSchema tLockTableSchema = getLastLockTrace();
        

        MMap tTmpMap = new MMap();
        

        if (tLockTableSchema != null)
        {
            if (!checkLockAvailability(tLockTableSchema))
            {
                
                tTmpMap = delLockTrace(tLockTableSchema);
                if (tTmpMap == null)
                {
                    return null;
                }
                tMMap.add(tTmpMap);
                
            }
            else
            {
                buildError("lockAction", "为了避免重复提交，现控制同一数据处理在"
                        + mAvailabilityIntervalSecond + "秒不得重复操作。");
                return null;
            }
        }
        
        
        String DelSQL="Delete From LockTable Where Notype='"+mLockNoType+"' And Nolimit='"+mLockNoKey
                     +"' And timestampdiff(2, char( timestamp('" + mCurrentDate + "'||' '||'" + mCurrentTime 
                     + "' )	- 	timestamp(char(makedate)||' '||maketime)))>"+mAvailabilityIntervalSecond;        
        tTmpMap.put(DelSQL,"DELETE");

        
        tTmpMap.add(createLockTrace());
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        

        return tMMap;
    }

    
    private LockTableSchema getLastLockTrace()
    {
        LockTableDB tLockTableDB = new LockTableDB();
        tLockTableDB.setNoType(mLockNoType);
        tLockTableDB.setNoLimit(mLockNoKey);

        if (!tLockTableDB.getInfo())
        {
            return null;
        }

        return tLockTableDB.getSchema();
    }

    
    private boolean checkLockAvailability(LockTableSchema cLockTableSchema)
    {
		return false;
//        String date = cLockTableSchema.getMakeDate();
//        String time = cLockTableSchema.getMakeTime();
//
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
//                Locale.SIMPLIFIED_CHINESE);
//
//        Date bef = null;
//        Date now = new Date();
//
//        try
//        {
//            bef = df.parse(date + " " + time);
//        }
//        catch (ParseException e)
//        {
//            e.printStackTrace();
//        }
//
//        long between = (now.getTime() - bef.getTime()) / 1000;
//
//        return (0 == mAvailabilityIntervalSecond || between < mAvailabilityIntervalSecond);
    }

    
    private MMap delLockTrace(LockTableSchema cLockTableSchema)
    {
        MMap tMMap = new MMap();

        tMMap.put(cLockTableSchema, SysConst.DELETE);

        return tMMap;
    }

    
    private MMap createLockTrace()
    {
        MMap tMMap = new MMap();

        LockTableSchema tLockTableSchema = new LockTableSchema();

        tLockTableSchema.setNoType(mLockNoType);
        tLockTableSchema.setNoLimit(mLockNoKey);

        tLockTableSchema.setMakeDate(PubFun.getCurrentDate());
        tLockTableSchema.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tLockTableSchema, SysConst.INSERT);

        return tMMap;
    }

    
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LockTableActionBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
