package com.contract.chargeQuery;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.utilty.StringUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class FeeQuery {
	private Logger log=Logger.getLogger(FeeQuery.class);
	String BussType;
	public String ChargeInfoQuery(String xml) throws JDOMException, IOException{
		String responseXml="";
		//组装返回报文节点
		Element bady_Response=new Element("body");
		Element head_Response=new Element("head");
		//创建返回报文
		Element Tempfee_Response=new Element("Tempfee_Response");
		Element DataSet=new Element("DataSet");
		Tempfee_Response.addContent(head_Response);
		Tempfee_Response.addContent(bady_Response);
		Element bussType = new Element("BussType");
		Element bussNo = new Element("BussNo");
		Element SuccessFlag = new Element("SuccessFlag"); //-调用成功标志成功为1，失败返回0
		Element Description = new Element("Description"); //-调用失败异常信息，成功返回successful，失败返回具体异常，长度截取至4000
		Element Stateofcharge=new Element("Stateofcharge");// 收付费状态，已收付费1，未收付费2
		Element Chargefiledreasion=new Element("Chargefiledreasion");//已收费的话返回successful，未收费的话返回未收费
		try {
			log.info(xml + "+++++++++++++++++====");
			ExeSQL exeSQL = new ExeSQL();
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			Element head = rootElement.getChild("head");
			Element body = rootElement.getChild("body");
			
			//组装返回报文bead节点
			
			Element BatchNo=new Element("BatchNo");
			BatchNo.setText(head.getChildText("BatchNo"));//批次号
			head_Response.addContent(BatchNo);
			
			Element SendDate=new Element("SendDate");
			SendDate.setText(head.getChildText("SendDate"));//发送日期
			head_Response.addContent(SendDate);
			
			Element SendTime=new Element("SendTime");
			SendTime.setText(head.getChildText("SendTime"));//发送时间
			head_Response.addContent(SendTime);
			
			Element BranchCode=new Element("BranchCode");
			BranchCode.setText(head.getChildText("BranchCode"));//交易编码
			head_Response.addContent(BranchCode);
			
			Element SendOperator=new Element("SendOperator");
			SendOperator.setText(head.getChildText("SendOperator"));//交易人员
			head_Response.addContent(SendOperator);
			
			Element MsgType=new Element("MsgType");
			MsgType.setText(head.getChildText("MsgType"));//固定值
			head_Response.addContent(MsgType);
			
			List<Element> list=body.getChildren("detail");
			for (int i = 0; i < list.size(); i++) {
				XMLOutputter out = new XMLOutputter();
				String BussType = list.get(i).getChildText("BussType");//业务类型 1-新单收费 2-保全收费 3-续期收费 Y-预付赔款收费
				String BussNo=list.get(i).getChildTextTrim("BussNo");// 业务号码 印刷号、保全受理号、保单号 、预付赔款号
				if ("Y".equals(BussType)) {
					String TempFeeSql="select EnterAccDate,ManageCom from ljaget where otherno = '"+BussNo+"' and  othernotype = '"+BussType+"'"; 
					SSRS ssrs1=exeSQL.execSQL(TempFeeSql);
					if (ssrs1.getMaxRow() <= 0) {
						SuccessFlag.setText("0");
						Description.setText("该预付赔款号未能查到相关数据！");
					}else {
						if ("".equals(ssrs1.GetText(1, 1)) || ssrs1.GetText(1, 1) == null) {	
							Stateofcharge.setText("2");
							Chargefiledreasion.setText("未收费");
						}else {	
							Stateofcharge.setText("1");
							Chargefiledreasion.setText("successfull");
						}
						SuccessFlag.setText("1");
						Description.setText("successfull");
					}
					 responseXml = out.outputString(DataSet);
				} else {
					// 拼接sql
					String TempFeeSql = "select enteraccdate from LJTempFee where otherno='"
							+ BussNo + "'  and  TempFeeType='" + BussType + "'";
					SSRS ssrs1 = exeSQL.execSQL(TempFeeSql);// 查询结果返回TempFee对象.
					
					if (ssrs1.getMaxRow() <= 0) { // 从给定的印刷号中没查到相关数据
						SuccessFlag.setText("0");
						Description.setText("该业务号未能查到相关数据！");
					} else {
						String enteraccdate = ssrs1.GetText(1, 1);
						if ("".equals(enteraccdate.trim())
								|| enteraccdate == null) {
							Stateofcharge.setText("2");
							Chargefiledreasion.setText("未收费");
						} else {
							Stateofcharge.setText("1");
							Chargefiledreasion.setText("successfull");
						}
						SuccessFlag.setText("1");
						Description.setText("successfull");
					}
				}
				bussType.setText(BussType);
				bussNo.setText(BussNo);
				bady_Response.addContent(bussType);
				bady_Response.addContent(bussNo);
				bady_Response.addContent(SuccessFlag);
				bady_Response.addContent(Description);
				bady_Response.addContent(Stateofcharge);
				bady_Response.addContent(Chargefiledreasion);
				responseXml = out.outputString(Tempfee_Response);
			}
			log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
			return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
		} catch (Exception e) {
			SuccessFlag.setText("0");
			Description.setText("流程异常或者数据错误，请检测报文");
			Stateofcharge.setText("2");
			Chargefiledreasion.setText("流程异常或者数据错误，请检测报文");
			bady_Response.addContent(bussType);
			bady_Response.addContent(bussNo);
			bady_Response.addContent(SuccessFlag);
			bady_Response.addContent(Description);
			bady_Response.addContent(Stateofcharge);
			bady_Response.addContent(Chargefiledreasion);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(Tempfee_Response);
			log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
			return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
		}finally{
			if(!StringUtil.StringNull(responseXml)){
				SuccessFlag.setText("0");
				Description.setText("发生阻断异常,返回报文为空");
				Stateofcharge.setText("2");
				Chargefiledreasion.setText("发生阻断异常,返回报文为空");
				bady_Response.addContent(bussType);
				bady_Response.addContent(bussNo);
				bady_Response.addContent(SuccessFlag);
				bady_Response.addContent(Description);
				bady_Response.addContent(Stateofcharge);
				bady_Response.addContent(Chargefiledreasion);
				XMLOutputter out = new XMLOutputter();
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
				return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
			}
		}
	}
}