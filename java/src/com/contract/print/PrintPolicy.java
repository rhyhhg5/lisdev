package com.contract.print;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.preservation.utilty.StringUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class PrintPolicy {
	private Logger log = Logger.getLogger(PrintPolicy.class);

	public String print(String xml) throws JDOMException, IOException {
		String responseXml = "";

		// 组装返回报文bady节点
		Element bady_Response = new Element("body");
		Element Result = new Element("Result");// 处理结果 0-成功 1-失败
		Element ResultRemark = new Element("ResultRemark");// 失败原因说明

	
		Element Print_Response = new Element("Print_Response");
		try {
			log.info(xml + "+++++++++++++++++====");
			ExeSQL exeSQL = new ExeSQL();
			
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			//baowen tou 
			Element head = rootElement.getChild("head");
			Element body = rootElement.getChild("body");

			// 组装返回报文head节点
			Element head_Response = new Element("head");
			Element BatchNo = new Element("BatchNo");

			if (!StringUtil.StringNull(head.getChildText("BatchNo"))
					&& !StringUtil.StringNull(head.getChildText("SendDate"))
					&& !StringUtil.StringNull(head.getChildText("SendTime"))
					&& !StringUtil.StringNull(head.getChildText("BranchCode"))
					&& !StringUtil
							.StringNull(head.getChildText("SendOperator"))
					&& !StringUtil.StringNull(head.getChildText("MsgType"))) {

				Result.setText("1");
				ResultRemark.setText("报文解析失败，报文头信息缺失！");

				bady_Response.addContent(ResultRemark);
				bady_Response.addContent(Result);
				XMLOutputter out = new XMLOutputter();
				responseXml = out.outputString(Print_Response);
				log.info("保单打印接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}

			BatchNo.setText(head.getChildText("BatchNo"));// 批次号
			head_Response.addContent(BatchNo);

			Element SendDate = new Element("SendDate");
			SendDate.setText(head.getChildText("SendDate"));// 发送日期
			head_Response.addContent(SendDate);

			Element SendTime = new Element("SendTime");
			SendTime.setText(head.getChildText("SendTime"));// 发送时间
			head_Response.addContent(SendTime);

			Element BranchCode = new Element("BranchCode");
			BranchCode.setText(head.getChildText("BranchCode"));// 交易编码
			head_Response.addContent(BranchCode);

			Element SendOperator = new Element("SendOperator");
			SendOperator.setText(head.getChildText("SendOperator"));// 交易人员
			head_Response.addContent(SendOperator);

			Element MsgType = new Element("MsgType");
			MsgType.setText(head.getChildText("MsgType"));// 固定值
			head_Response.addContent(MsgType);

			Print_Response.addContent(head_Response);

			Print_Response.addContent(bady_Response);
			List<Element> list = body.getChildren("Item");

			for (int i = 0; i < list.size(); i++) {
				String PrtNo = list.get(i).getChildText("PrtNo");//
				String ManageCom = list.get(i).getChildTextTrim("ManageCom");//

				System.out.println("*************************"+PrtNo);
				// 拼接sql
				String PrintSql = "update lcgrpcont  set printcount='1' where prtno='"
						+ PrtNo + "'";
				boolean ssrs2 = exeSQL.execUpdateSQL(PrintSql);				
				//System.out.println("*****************保单打印完成******************");
				
				String PrintCountSql = "select printcount from lcgrpcont where  prtno='"
						+ PrtNo + "'";
				
				SSRS ssrs1 = exeSQL.execSQL(PrintCountSql);
				
				if (ssrs1.getMaxNumber()<0) {
					Result.setText("1");
					ResultRemark.setText("未打印");

					bady_Response.addContent(ResultRemark);
				} else {
					Result.setText("0");
					ResultRemark.setText("打印成功");
					bady_Response.addContent(ResultRemark);
				}
			}
			bady_Response.addContent(Result);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(Print_Response);
			log.info("保单打印接口返回报文<?xml version='1.0' encoding='GBK'?>"
					+ responseXml);
			return "<?xml version='1.0' encoding='GBK'?>" + responseXml;

		} catch (Exception e) {
			Result.setText("1");
			ResultRemark.setText("流程异常或者数据错误，请检测报文");
			bady_Response.addContent(ResultRemark);
			bady_Response.addContent(Result);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(Print_Response);
			log.info("保单打印接口返回报文<?xml version='1.0' encoding='GBK'?>"
					+ responseXml);
			return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
		} finally {
			if (!StringUtil.StringNull(responseXml)) {
				Result.setText("1");
				ResultRemark.setText("发生阻断异常,返回报文为空");
				bady_Response.addContent(ResultRemark);
				bady_Response.addContent(Result);
				XMLOutputter out = new XMLOutputter();
				responseXml = out.outputString(Print_Response);
				log.info("保单打印接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
		}
	}
}
