package com.contract.print;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

import com.contract.TestInterface;
import com.sinosoft.midplat.kernel.util.JdomUtil;

import javax.xml.namespace.QName;
public class PrintTestInterface {
	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}
	
	public void service(String aInXmlStr){


//		String  tStrTargetEendPoint = "http://10.136.10.101:900/services/InterfacePrintPolicy";
		String  tStrTargetEendPoint = "http://localhost:8080/ui/services/InterfacePrintPolicy";
		String  tStrNamespace = "http://print.contract.com";
		
		try {
			RPCServiceClient client = new RPCServiceClient();
			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
			option.setAction("service");
			option.setTimeOutInMilliSeconds(3000000L);
			QName name = new QName(tStrNamespace, "service");
			Object[] object = new Object[] { aInXmlStr };
			Class[] returnTypes = new Class[] { String.class };
			Object[] response = client.invokeBlocking(name, object, returnTypes);
			String result = (String) response[0];
			System.out.println("UI return:" + result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
	      try {
	    	  PrintTestInterface tPadClient = new PrintTestInterface();
	    	    
	    	    String mInFilePath = "F:/requestmsg/print.xml";
				InputStream mIs = new FileInputStream(mInFilePath);
				byte[] mInXmlBytes = tPadClient.InputStreamToBytes(mIs);
				String mInXmlStr = new String(mInXmlBytes, "GBK");
//				System.out.println(mInXmlStr);

				tPadClient.service(mInXmlStr);
	      } catch (Exception e) {
	          e.printStackTrace();
	      }
	  }
}
