package com.grouppol;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableUI;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDSysTraceSchema;
import com.sinosoft.lis.tb.QySendMessgeMBL;
import com.sinosoft.lis.vschema.LDSysTraceSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;

/**
 * @author dongjiali
 * 
 *         签单录入接口
 */
public class GroupPolSave {

	private Logger log = Logger.getLogger(GroupPolSave.class);

	public String groupsave(String xml) throws JDOMException, IOException {

		log.info(xml);
		ExeSQL exeSQL = new ExeSQL();
		String workType = "";
		String Content = "";
		String FlagStr = "";
		StringReader reader = new StringReader(xml);
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document doc = tSAXBuilder.build(reader);
		Element rootElement = doc.getRootElement();
		Element msgHead = rootElement.getChild("MsgHead");
		Element item = msgHead.getChild("Item");
		String Operator = item.getChildTextTrim("SendOperator");
		String BranchCode = item.getChildTextTrim("BranchCode");
		Element parame = rootElement.getChild("Parame");
		String prtno = parame.getChildTextTrim("prtno");// 获取保单号
		String tActivityId = "0000002006";
		// 根据保单查询数据
		String strinsql = "select A,B,C,D,E,F,G,H,I ,J, M from( select missionprop1 A,missionprop2 B,missionprop9 C,missionprop7 D,missionprop4 E,getUniteCode(missionprop5) F,missionid G,submissionid H,(case when exists (select 1  from ljtempfee where '1487758483000'='1487758483000' and  otherno = missionprop2 and enteraccdate is not null) then  '已交费'  when exists (select 1  from ljtempfeeclass a  left join ljtempfee b  on a.tempfeeno = b.tempfeeno   where b.otherno = missionprop2  and a.paymode in ('2', '3') and b.enteraccdate is null) then '已交费，但未做到账确认'else '未交费' end) I, ( case when (select sum(paymoney) from ljtempfee where otherno=missionprop2 and enteraccdate is not null) is null then 0  else (select sum(paymoney) from ljtempfee where otherno=missionprop2 and enteraccdate is not null and confflag = '0') end ) J  ,(select stateflag from loprtmanager where otherno = missionprop1 and code = '76') M from lwmission  where processid='0000000004' and activityid in ('0000002006', '0000002007')  and missionprop2='"
				+ prtno
				+ "'  and lwmission.missionprop4 like '86%' ) as x fetch first 3000 rows only with ur ";
		SSRS ssrs3 = exeSQL.execSQL(strinsql);
		String tMissionID = "";
		String tSubMissionID = "";
		String tGrpContNo = "";
		if (ssrs3.getMaxRow() <= 0) {
			return returnXml(doc, "false", "没有查到相关数据");
		} else {
			tMissionID = ssrs3.GetText(1, 7);
			tSubMissionID = ssrs3.GetText(1, 8);
			tGrpContNo = ssrs3.GetText(1, 1);

		}

		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		TransferData tTransferData = new TransferData();
		tLCGrpContSchema.setGrpContNo(tGrpContNo);
		tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
		tTransferData.setNameAndValue("MissionID", tMissionID);
		tTransferData.setNameAndValue("SubMissionID", tSubMissionID);

		// 查询保单状态
		String tSRSQL = "select lcp.riskcode from lcpol lcp where lcp.prtno = '"
				+ prtno
				+ "' "
				+ "and lcp.riskcode in (select code from ldcode where codetype = 'stoprisk') "
				+ "and current date >= (select CODEALIAS from ldcode where codetype = 'stoprisk' and code = lcp.riskcode) ";
		SSRS ssrs1 = exeSQL.execSQL(tSRSQL);
		GlobalInput tGI = new GlobalInput();
		tGI.Operator = Operator;
		tGI.ComCode = BranchCode;
		tGI.ManageCom = BranchCode;
		log.info(ssrs1.getMaxNumber());

		if (ssrs1.getMaxRow() > 0) {
			log.info("险种" + ssrs1.GetText(1, 1) + "已停售，不能签单！");
			return returnXml(doc, "false", "险种" + ssrs1.GetText(1, 1)
					+ "已停售，不能签单！");
		}
		String strSql = "select * from ldsystrace where PolNo='" + prtno
				+ "' and PolState=1006 ";
		SSRS ssrs2 = exeSQL.execSQL(strSql);
		if (ssrs2.getMaxRow() > 0 && ssrs2.GetText(1, 2) != Operator) {
			log.info("该印刷号的投保单已经被操作员（" + ssrs2.GetText(1, 2) + "）在（"
					+ ssrs2.GetText(1, 6) + "）位置锁定！您不能操作，请选其它的印刷号！");
			return returnXml(
					doc,
					"false",
					"该印刷号的投保单已经被操作员（" + ssrs2.GetText(1, 2) + "）在（"
							+ ssrs2.GetText(1, 6) + "）位置锁定！您不能操作，请选其它的印刷号！");
		}

		// 锁定该印刷号
		String lwmission = "select ActivityStatus from lwmission where activityid='0000002006' and missionid='"
				+ tMissionID
				+ "' and submissionid='"
				+ tSubMissionID
				+ "' with ur";
		SSRS ssrs4 = exeSQL.execSQL(lwmission);
		if ("0".equals(ssrs4.GetText(1, 1))) {
			String lccont = "select count(1) from lccont where appflag in ('1','9') and PrtNo='"
					+ prtno + "' with ur";
			SSRS ssrs5 = exeSQL.execSQL(lccont);
			returnXml(doc, "false", "保单正在签单过程中！已完成" + ssrs5.GetText(1, 1)
					+ "个被保险人，请稍等");
		}

		LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
		tLDSysTraceSchema.setPolNo(prtno);
		tLDSysTraceSchema.setCreatePos("承保签单");
		tLDSysTraceSchema.setPolState("1006");
		LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
		inLDSysTraceSet.add(tLDSysTraceSchema);
		VData VData3 = new VData();
		VData3.add(tGI);
		VData3.add(inLDSysTraceSet);

		LockTableUI LockTableUI1 = new LockTableUI();
		if (!LockTableUI1.submitData(VData3, "INSERT")) {
			VData rVData = LockTableUI1.getResult();
			System.out.println("LockTable Failed! " + (String) rVData.get(0));
		} else {
			System.out.println("UnLockTable Succed!");
		}

		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(tTransferData);
		tVData.add(tGI);
		//int n = tLCGrpContSignBL.mErrors.getErrorCount();
		GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
		boolean bl = tTbWorkFlowUI.submitData(tVData, tActivityId);
		log.info(bl);
		int n = tTbWorkFlowUI.mErrors.getErrorCount();
		if (n == 0) {
			if (bl) {
				if (!"PREVIEW".equals(workType)) {
					String tSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"
							+ tGrpContNo
							+ "' "
							+ " and substr(managecom,1,4) in ( select code from ldcode where codetype = 'qysendmessgem' ) ";
					String tPrtNo = new ExeSQL().getOneValue(tSQL);
					if (tPrtNo != null && !"".equals(tPrtNo)) {
						VData tSMSVData = new VData();
						TransferData tSMSTransferData = new TransferData();
						tSMSTransferData.setNameAndValue("PrtNo", tPrtNo);
						tSMSTransferData.setNameAndValue("ContTypeFlag", "2");
						tSMSVData.add(tSMSTransferData);
						tSMSVData.add(tGI);
						QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
						if (tQySendMessgeMBL.submitData(tSMSVData, "QYMSG")) {
							System.out.println(tPrtNo + "签单后即时发送短信成功！");
						} else {
							System.out.println(tPrtNo + "签单后即时发送短信失败！");
						}
					}
				}
				Content = " 签单成功! ";
				FlagStr = "Succ";
				if ("PREVIEW".equals(workType)) {
					Content = " 预打保单成功! ";
				}
			} else {
				Content = "集体投保单签单失败";
				FlagStr = "Fail";
				if ("PREVIEW".equals(workType)) {
					Content = " 预打保单失败! ";
				}
			}
		} else {

			String strErr = "";
			for (int i = 0; i < n; i++) {
				strErr += (i + 1) + ": "
						+ tTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
				System.out
						.println(tTbWorkFlowUI.mErrors.getError(i).errorMessage);
			}
			if (bl) {
				Content = " 部分签单成功,但是有如下信息: " + strErr;
				FlagStr = "Succ";
				if ("PREVIEW".equals(workType)) {
					Content = " 预打保单成功! ";
				}
				if (!"PREVIEW".equals(workType)) {
					String tSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"
							+ tGrpContNo
							+ "' "
							+ " and substr(managecom,1,4) in ( select code from ldcode where codetype = 'qysendmessgem' ) ";
					String tPrtNo = new ExeSQL().getOneValue(tSQL);
					if (tPrtNo != null && !"".equals(tPrtNo)) {
						VData tSMSVData = new VData();
						TransferData tSMSTransferData = new TransferData();
						tSMSTransferData.setNameAndValue("PrtNo", tPrtNo);
						tSMSTransferData.setNameAndValue("ContTypeFlag", "2");
						tSMSVData.add(tSMSTransferData);
						tSMSVData.add(tGI);
						QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
						if (tQySendMessgeMBL.submitData(tSMSVData, "QYMSG")) {
							System.out.println(tPrtNo + "签单后即时发送短信成功！");
						} else {
							System.out.println(tPrtNo + "签单后即时发送短信失败！");
						}
					}
				}
			} else {
				Content = "集体投保单签单失败，原因是: " + strErr;
				FlagStr = "Fail";
				if ("PREVIEW".equals(workType)) {
					Content = " 预打保单失败! ";
				}
			}
		} // end of if
		return null;
	} // end of if

	/**
	 * @param Operator  交易人
	 * @param BranchCode 交易码
	 * @param prtno 保单号
	 * @return 返回结果
	 */
	public String groupsave(String Operator, String BranchCode, String prtno,String bussType,String msgType,String batchNo)
			throws JDOMException, IOException {
		Document doc=null;
		ExeSQL exeSQL = new ExeSQL();
		String workType = "";
		String Content = "true";
		String FlagStr = "";
		String tActivityId = "0000002006";
		// 根据保单查询数据
		String strinsql = "select A,B,C,D,E,F,G,H,I ,J, M from( select missionprop1 A,missionprop2 B,missionprop9 C,missionprop7 D,missionprop4 E,getUniteCode(missionprop5) F,missionid G,submissionid H,(case when exists (select 1  from ljtempfee where '1487758483000'='1487758483000' and  otherno = missionprop2 and enteraccdate is not null) then  '已交费'  when exists (select 1  from ljtempfeeclass a  left join ljtempfee b  on a.tempfeeno = b.tempfeeno   where b.otherno = missionprop2  and a.paymode in ('2', '3') and b.enteraccdate is null) then '已交费，但未做到账确认'else '未交费' end) I, ( case when (select sum(paymoney) from ljtempfee where otherno=missionprop2 and enteraccdate is not null) is null then 0  else (select sum(paymoney) from ljtempfee where otherno=missionprop2 and enteraccdate is not null and confflag = '0') end ) J  ,(select stateflag from loprtmanager where otherno = missionprop1 and code = '76') M from lwmission  where processid='0000000004' and activityid in ('0000002006', '0000002007')  and missionprop2='"
				+ prtno
				+ "'  and lwmission.missionprop4 like '86%' ) as x fetch first 3000 rows only with ur ";
		SSRS ssrs3 = exeSQL.execSQL(strinsql);
		String tMissionID = "";
		String tSubMissionID = "";
		String tGrpContNo = "";
		if (ssrs3.getMaxRow() <= 0) {
			return "false|没有查询到相关数据";
		} else {
			tMissionID = ssrs3.GetText(1, 7);
			tSubMissionID = ssrs3.GetText(1, 8);
			tGrpContNo = ssrs3.GetText(1, 1);

		}

		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		TransferData tTransferData = new TransferData();
		tLCGrpContSchema.setGrpContNo(tGrpContNo);
		tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
		tTransferData.setNameAndValue("MissionID", tMissionID);
		tTransferData.setNameAndValue("SubMissionID", tSubMissionID);

		// 查询保单状态
		String tSRSQL = "select lcp.riskcode from lcpol lcp where lcp.prtno = '"
				+ prtno
				+ "' "
				+ "and lcp.riskcode in (select code from ldcode where codetype = 'stoprisk') "
				+ "and current date >= (select CODEALIAS from ldcode where codetype = 'stoprisk' and code = lcp.riskcode) ";
		SSRS ssrs1 = exeSQL.execSQL(tSRSQL);
		GlobalInput tGI = new GlobalInput();
		tGI.Operator = "CENA";
		tGI.ComCode = BranchCode;
		tGI.ManageCom = BranchCode;
		log.info(ssrs1.getMaxNumber());

		if (ssrs1.getMaxRow() > 0) {
			log.info("险种" + ssrs1.GetText(1, 1) + "已停售，不能签单！");
			return "false|" +"险种" + ssrs1.GetText(1, 1)
					+ "已停售，不能签单！";
		}
		String strSql = "select * from ldsystrace where PolNo='" + prtno
				+ "' and PolState=1006 ";
		SSRS ssrs2 = exeSQL.execSQL(strSql);
		/*if (ssrs2.getMaxRow() > 0 && ssrs2.GetText(1, 2) != Operator) {
			log.info("该印刷号的投保单已经被操作员（" + ssrs2.GetText(1, 2) + "）在（"
					+ ssrs2.GetText(1, 6) + "）位置锁定！您不能操作，请选其它的印刷号！");
			return 
					"false|"+
					"该印刷号的投保单已经被操作员（" + ssrs2.GetText(1, 2) + "）在（"
							+ ssrs2.GetText(1, 6) + "）位置锁定！您不能操作，请选其它的印刷号！";
		}*/

		// 锁定该印刷号
		String lwmission = "select ActivityStatus from lwmission where activityid='0000002006' and missionid='"
				+ tMissionID
				+ "' and submissionid='"
				+ tSubMissionID
				+ "' with ur";
		SSRS ssrs4 = exeSQL.execSQL(lwmission);
		if ("0".equals(ssrs4.GetText(1, 1))) {
			String lccont = "select count(1) from lccont where appflag in ('1','9') and PrtNo='"
					+ prtno + "' with ur";
			SSRS ssrs5 = exeSQL.execSQL(lccont);
			returnXml(doc, "false", "保单正在签单过程中！已完成" + ssrs5.GetText(1, 1)
					+ "个被保险人，请稍等");
		}

		LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
		tLDSysTraceSchema.setPolNo(prtno);
		tLDSysTraceSchema.setCreatePos("承保签单");
		tLDSysTraceSchema.setPolState("1006");
		LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
		inLDSysTraceSet.add(tLDSysTraceSchema);
		VData VData3 = new VData();
		VData3.add(tGI);
		VData3.add(inLDSysTraceSet);

		LockTableUI LockTableUI1 = new LockTableUI();
		if (!LockTableUI1.submitData(VData3, "INSERT")) {
			VData rVData = LockTableUI1.getResult();
			System.out.println("LockTable Failed! " + (String) rVData.get(0));
		} else {
			System.out.println("UnLockTable Succed!");
		}

		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(tTransferData);
		tVData.add(tGI);
		// int n = tLCGrpContSignBL.mErrors.getErrorCount();
		GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
		boolean bl = tTbWorkFlowUI.submitData(tVData, tActivityId);
		log.info("返回结果"+bl);
		
		int n = tTbWorkFlowUI.mErrors.getErrorCount();
		SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		SimpleDateFormat hms = new SimpleDateFormat("HH:mm:ss");//设置日期格式
		if (n == 0) {
			
			if (bl) {
				
//				String updatasql="update reconcileInfo set OtherNo="+prtno+",OtherType="+bussType+",businessFailMark='1',businessFailDes='成功',effectiveDate='"+ymd.format(new Date())+"',effectiveTime='"+hms.format(new Date())+"' where otherno='"+ prtno +"'";
//				ExeSQL esql=new ExeSQL();
//				esql.execUpdateSQL(updatasql);
				
				VData vdata = new VData();
				MMap mmap = new MMap();
				String insertClaimInsertLogSql = "  insert into ClaimInsertLog" +
						" (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,betweenness ,transactionType ,transactionCode ,transactionBatch ,  transactionDescription,   transactionAddress, grpcontno, rgtno )" +
						" values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '1'),'"+Operator+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','中间状态','"+msgType+"','"+BranchCode+"','"+batchNo+"','交易描述','交易地址','"+prtno+"','')";
//				mmap.put(insertReconcileInfoSql, "INSERT");
				mmap.put(insertClaimInsertLogSql, "INSERT");
				vdata.add(mmap);
				PubSubmit pub = new PubSubmit();
				pub.submitData(vdata, "INSERT");
			} else {
				Content = "集体投保单签单失败";
//				String updatasql="update reconcileInfo set OtherNo="+prtno+",OtherType="+bussType+",businessFailMark='0',businessFailDes='失败',effectiveDate='"+ymd.format(new Date())+"',effectiveTime='"+hms.format(new Date())+"' where otherno='"+ prtno +"'";
//				ExeSQL esql=new ExeSQL();
//				esql.execUpdateSQL(updatasql);
				
				VData tdata=new VData();
				MMap map=new MMap();
				String insertErrorInsertLogSql="  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"+Operator+"','',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'','中间状态','"+Content+"','"+msgType+"','"+BranchCode+"','"+batchNo+"','交易描述','交易地址')";
				map.put(insertErrorInsertLogSql, "INSERT");
				tdata.add(map);
				PubSubmit pubt = new PubSubmit();
				pubt.submitData(tdata, "INSERT");
			}
		} else {

			String strErr = "";
			for (int i = 0; i < n; i++) {
				strErr += (i + 1) + ": "
						+ tTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
				System.out
						.println(tTbWorkFlowUI.mErrors.getError(i).errorMessage);
			}
			if (bl) {
//				String updatasql="update reconcileInfo set OtherNo="+prtno+",OtherType="+bussType+",businessFailMark='1',businessFailDes='成功',effectiveDate='"+ymd.format(new Date())+"',effectiveTime='"+hms.format(new Date())+"' where otherno='"+ prtno +"'";
//				ExeSQL esql=new ExeSQL();
//				esql.execUpdateSQL(updatasql);
				
				
				VData vdata = new VData();
				MMap mmap = new MMap();
				String insertClaimInsertLogSql = "  insert into ClaimInsertLog" +
						" (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,betweenness ,transactionType ,transactionCode ,transactionBatch ,  transactionDescription,   transactionAddress, grpcontno, rgtno )" +
						" values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '1'),'"+Operator+"',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','中间状态','"+msgType+"','"+BranchCode+"','"+batchNo+"','交易描述','交易地址','"+prtno+"','')";
				mmap.put(insertClaimInsertLogSql, "INSERT");
				vdata.add(mmap);
				PubSubmit pub = new PubSubmit();
				pub.submitData(vdata, "INSERT");
			} else {
//				String updatasql="update reconcileInfo set OtherNo="+prtno+",OtherType="+bussType+",businessFailMark='0',businessFailDes='失败',effectiveDate='"+ymd.format(new Date())+"',effectiveTime='"+hms.format(new Date())+"' where otherno='"+ prtno +"'";
//				ExeSQL esql=new ExeSQL();
//				esql.execUpdateSQL(updatasql);
				Content = "集体投保单签单失败，原因是: " + strErr;
				FlagStr = "Fail";
				
				
				VData tdata=new VData();
				MMap map=new MMap();
				String insertErrorInsertLogSql="  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"+Operator+"','',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'','中间状态','"+Content+"','"+msgType+"','"+BranchCode+"','"+batchNo+"','交易描述','交易地址')";
				map.put(insertErrorInsertLogSql, "INSERT");
				tdata.add(map);
				PubSubmit pubt = new PubSubmit();
				pubt.submitData(tdata, "INSERT");
			}
		} // end of if
		return bl+"|"+Content;
	} // end of if

	/**
	 * 处理返回报文格式
	 * 
	 * @param root
	 *            返回报文节点
	 * @param State
	 *            返回报文状态
	 * @param count
	 *            然后报文内容
	 * @throws IOException
	 */
	public String returnXml(Document doc, String state, String count)
			throws IOException {
		Element rootElement = doc.getRootElement();
		Element msgHead = rootElement.getChild("MsgHead");
		Element item = msgHead.getChild("Item");
		Element stateel = new Element("State").setText(state);
		item.addContent(stateel);
		Element countel = new Element("Count").setText(count);
		item.addContent(countel);

		XMLOutputter out = new XMLOutputter();
		String responseXml = out.outputString(doc);
		log.info(responseXml);
		return responseXml;
	}

	public static void main(String[] str) throws JDOMException, IOException {
		SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		SimpleDateFormat hms = new SimpleDateFormat("HH:mm:ss");//设置日期格式
		System.out.println(ymd.format(new Date()));// new Date()为获取当前系统时间
		System.out.println(hms.format(new Date()));// new Date()为获取当前系统时间
	}
}
