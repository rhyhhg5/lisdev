package com.grpLJSCancel;

import java.io.IOException;
import java.io.StringReader;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.operfee.GrpLJSCancelUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GrpLJSCancel {
	private Logger log = Logger.getLogger(GrpLJSCancel.class);
	private XMLOutputter out = new XMLOutputter();
	private GrpLJSCancelUI tGrpLJSCancelUI = new GrpLJSCancelUI();
	public String GrpLJSCancelInfo(String xml) throws JDOMException, IOException {
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String responseXml = "";
		// 组装返回报文bady节点
		Element head_Response = new Element("head");
		Element State = new Element("State");// 作废处理结果 00-成功 01-失败
		Element ResultRemark = new Element("ErrInfo");// 失败原因说明
		// 创建返回报文
		Element Tempfee_Response = new Element("Tempfee_Response");
		
		try {
			log.info(xml + ".........................begin............................................");
			ExeSQL exeSQL = new ExeSQL();
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			Element head = rootElement.getChild("head");
				String sendDate = head.getChildText("SendDate");
				String sendTime = head.getChildText("SendTime");
				String branchCode = head.getChildText("BranchCode");
				String sendOperator = head.getChildText("SendOperator");
				String msgType = head.getChildText("MsgType");
			    String batchNo = head.getChildText("BatchNo");
			Element body = rootElement.getChild("body");
			if (!(StringUtil.StringNull(batchNo)
					&& StringUtil.StringNull(sendDate)
					&& StringUtil.StringNull(sendTime)
					&& StringUtil.StringNull(branchCode)
					&& StringUtil.StringNull(sendOperator)
					&& StringUtil.StringNull(msgType))) {
				State.setText("01");
				ResultRemark.setText("报文解析失败，报文头信息缺失！");
				head_Response.addContent(State);
				head_Response.addContent(ResultRemark);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
			//拼装返回报文的报文头
			Element BatchNo = new Element("BatchNo");
			BatchNo.setText(batchNo);// 批次号
			head_Response.addContent(BatchNo);

			Element SendDate = new Element("SendDate");
			SendDate.setText(sendDate);// 发送日期
			head_Response.addContent(SendDate);

			Element SendTime = new Element("SendTime");
			SendTime.setText(sendTime);// 发送时间
			head_Response.addContent(SendTime);

			Element BranchCode = new Element("BranchCode");
			BranchCode.setText(branchCode);// 交易编码
			head_Response.addContent(BranchCode);

			Element SendOperator = new Element("SendOperator");
			SendOperator.setText(sendOperator);// 交易人员
			head_Response.addContent(SendOperator);

			Element MsgType = new Element("MsgType");
			MsgType.setText(msgType);// 固定值
			head_Response.addContent(MsgType);

			Tempfee_Response.addContent(head_Response);

			// 获取请求报文体数据
			String AppntNo = body.getChildText("AppntNo");
			String GrpContNo = body.getChildText("GrpContNo");
			String GetNoticeNo = body.getChildText("GetNoticeNo");
			String cancelMode = body.getChildText("cancelMode");

		    StringBuffer sb = new StringBuffer();
		    String strSQL = "select distinct a.GrpContNo,a.GrpName,a.CValiDate,min(b.PayToDate),'已签单',c.SumDuePayMoney,getAgentName(a.AgentCode)"
				   +" from LCGrpCont a,LCCont b,LJSPay c"
		           + " where 1=1";
		    String strSQL1 = " and a.GrpContNo = '"+ GrpContNo +"'";
		    String strSQL2 = " and a.AppntNo = '"+ AppntNo +"'";
		    String strSQL3 = " and c.GetNoticeNo = '"+ GetNoticeNo +"'";
		    String strSQL4 = " and c.OtherNoType='1'"	   
			   + " and a.GrpContNo=b.GrpContNo"
			   + " and c.OtherNo=b.GrpContNo"
			   + " and a.AppFlag='1' and b.AppFlag='1'"
			   + " group by a.GrpContNo,a.GrpName,a.CValiDate,a.AppFlag,c.SumDuePayMoney,a.AgentCode";
		    sb.append(strSQL);
			if (!(AppntNo == null||"".equals(AppntNo.trim()))) {
				sb.append(strSQL2);
			}
			if (!(GrpContNo == null||"".equals(GrpContNo.trim()))) {
				sb.append(strSQL1);
			}
			if (!(GetNoticeNo == null||"".equals(GetNoticeNo.trim()))) {
				sb.append(strSQL3);
			}
			sb.append(strSQL4);
			SSRS exe = exeSQL.execSQL(sb.toString());
			if (exe.getMaxRow() <= 0) { // 没查到数据,没有符合条件的可催收保单信息
				State.setText("01");
				ResultRemark.setText("没有符合条件的作废信息");
				head_Response.addContent(State);
				head_Response.addContent(ResultRemark);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			} else {
				String tGrpContNo = exe.GetText(1, 1);
				String strSQL5 ="	select distinct b.GetNoticeNo,min(c.LastPayToDate),b.SumDuePayMoney, b.MakeDate,(select b.codeName from LJSPayB a, LDCode b where a.dealState = b.code and b.codeType = 'dealstate' and b.GetNoticeNo=a.GetNoticeNo)"
			       + " from  ljspay b,LJSPayPerson c where  b.othernotype='1'  "
			       + " and b.OtherNo='" +tGrpContNo + "'"
				   + " and c.GetNoticeNo=b.GetNoticeNo"
			       + " and c.RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
				   + " group by b.GetNoticeNo,b.SumDuePayMoney,b.MakeDate,b.MakeDate,b.otherNo";
				SSRS exe1 = exeSQL.execSQL(strSQL5);
				//合同号
				String tGetNoticeNo = exe1.GetText(1, 1);
				
				try {
					GlobalInput tGI = new GlobalInput();
					tGI.Operator = "BAOQA";
					//查询管理机构
					ExeSQL tExeSQL = new ExeSQL();
					String managecom="";
					String mansql="select managecom from  lcgrpcont  where grpcontno='"+GrpContNo+"'";
					SSRS manSSRS = tExeSQL.execSQL(mansql);
					if (manSSRS.getMaxNumber() > 0) {
						managecom = manSSRS.GetText(1, 1);
					}
					//tGI.ManageCom ="86360500";
					tGI.ManageCom =managecom;
					LJSPaySchema tLJSPaySchema =new LJSPaySchema();
					tLJSPaySchema.setGetNoticeNo(tGetNoticeNo);

					TransferData tTransferData= new TransferData();
					tTransferData.setNameAndValue("CancelMode",cancelMode);

					VData tVData = new VData();
					tVData.addElement(tLJSPaySchema);
					tVData.addElement(tGI);
					tVData.addElement(tTransferData);
					System.out.println(tGetNoticeNo);
					System.out.println(cancelMode);
					tGrpLJSCancelUI.submitData(tVData,"INSERT");
				} catch (Exception ex) {
					Content = "失败，原因是:" + ex.toString();
					FlagStr = "01";
				}
				if (FlagStr=="")
				{
					tError = tGrpLJSCancelUI.mErrors;
					if (!tError.needDealError())
					{
						Content = "Successful";
						FlagStr = "00";
					}
					else
					{
						Content =" 失败，原因是:" + tError.getFirstError();
						FlagStr = "01";
					}
				}
				State.setText(FlagStr);
				ResultRemark.setText(Content);
				head_Response.addContent(State);
				head_Response.addContent(ResultRemark);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
		
		} catch (Exception e) {
			State.setText("01");
			ResultRemark.setText("流程异常或者数据错误，请检测报文");
			head_Response.addContent(State);
			head_Response.addContent(ResultRemark);
			responseXml = out.outputString(Tempfee_Response);
			log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
			return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
		}finally{
			if(!StringUtil.StringNull(responseXml)){
				State.setText("01");
				ResultRemark.setText("发生阻断异常,返回报文为空");
				head_Response.addContent(State);
				head_Response.addContent(ResultRemark);
				 responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
				return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
			}
		}
	}
}
