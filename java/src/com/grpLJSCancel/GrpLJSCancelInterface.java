package com.grpLJSCancel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jdom.JDOMException;
import org.jdom.output.XMLOutputter;

import com.claim.serviceTest.TestSentXMLThrowWebservice;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
/**
 * 续期作废
 * @author lx
 *
 */
public class GrpLJSCancelInterface {
	private XMLOutputter out = new XMLOutputter();
	// 组装返回报文节点
	 private org.jdom.Element head_Response = new org.jdom.Element("head");
	 private org.jdom.Element State = new org.jdom.Element("State");// 作废处理结果 00-成功 01-失败
	 private org.jdom.Element ErrInfo = new org.jdom.Element("ErrInfo");// 失败原因说明
	// 创建返回报文
	 private org.jdom.Element Tempfee_Response = new org.jdom.Element("Tempfee_Response");
	 private String responseXml="";
	String mUrl = "";
	String aOutXmlStr = "";
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String fileName = sdf.format(date);
	public String service(String xml) throws JDOMException, IOException, DocumentException{
		Document doc = DocumentHelper.parseText(xml);
		Element root = doc.getRootElement();
		Element body = root.element("body");
		String CancelState = body.elementText("CancelState");
		// 保存请求报文
		if(!getUrlName("1")) {
			return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
		}
		String InFilePath = mUrl+ fileName + CancelState + ".xml";
		FileWriter fw = new FileWriter(InFilePath);
		fw.write(xml);
		fw.flush();
		fw.close();
		String returnxml;
		if ("plan".equalsIgnoreCase(CancelState)) {	//约定作废
			GrpLJSPlanCancel gdp = new GrpLJSPlanCancel();
			returnxml = gdp.GrpLJSPlanCancelInfo(xml);
		}else {	//期缴
			GrpLJSCancel tfq=new GrpLJSCancel();
		    returnxml=tfq.GrpLJSCancelInfo(xml);
		}
		//保存返回 报文
		// 保存请求报文
		if(!getUrlName("2")) {
			return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
		}
		String OutFilePath = mUrl +fileName + CancelState + ".xml";
		FileWriter fw1 = new FileWriter(OutFilePath);
		fw1.write(returnxml);
		fw1.flush();
		fw1.close();
		return returnxml;
	}
	/**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
	 * @throws IOException 
     */
    private boolean getUrlName(String type) throws IOException{
    	// String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";//发盘报文的存放路径
    	 String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='SYYBPath'";//发盘报文的存放路径
         String fileFolder = new ExeSQL().getOneValue(sqlurl);

         if (fileFolder == null || fileFolder.equals("")){
        	 aOutXmlStr = "getFileUrlName,获取文件存放路径出错";
        	 State.setText("01");
			 ErrInfo.setText("报文解析失败，报文头信息缺失！");
			 head_Response.addContent(State);
			 head_Response.addContent(ErrInfo);
			 responseXml = out.outputString(Tempfee_Response);
             return false;
         }
         
         if(type.equals("1")){
        	 mUrl =fileFolder + "ZFRequest/" + PubFun.getCurrentDate2()+"/";
         }else{
        	 mUrl =fileFolder + "ZFRsponse/"+ PubFun.getCurrentDate2()+"/";
         }
         
         if (!newFolder(mUrl)){
        	 State.setText("01");
			 ErrInfo.setText("新建目录失败！");
			 head_Response.addContent(State);
			 head_Response.addContent(ErrInfo);
			 responseXml = out.outputString(Tempfee_Response);
        	 return false;
         }
         return true;
    }
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath){
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try{
            if (myFilePath.isDirectory()){
                System.out.println("目录已存在,文件路径为:"+myFilePath);
                return true;
            }else{
                myFilePath.mkdirs();
                System.out.println("新建目录成功,文件路径为:"+myFilePath);
                return true;
            }
        }catch (Exception e){
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
    
    
    public static void main(String[] args) {
		try {
			TestSentXMLThrowWebservice test = new TestSentXMLThrowWebservice();
			GrpLJSCancelInterface grpLJSCancelInterface=new GrpLJSCancelInterface();
			String mInFilePath = "D:\\renewal\\scan/作废.xml";
			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = test.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "gbk");
			System.out.println(mInXmlStr);
			String service = grpLJSCancelInterface.service(mInXmlStr);
			System.out.println(service);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
