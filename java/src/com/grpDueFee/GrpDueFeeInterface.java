package com.grpDueFee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jdom.JDOMException;

import com.claim.serviceTest.TestSentXMLThrowWebservice;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

public class GrpDueFeeInterface {
	String mUrl = "";
	String aOutXmlStr = "";
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String fileName = sdf.format(date);
	public String service(String xml) throws DocumentException, JDOMException, IOException {
		Document doc = DocumentHelper.parseText(xml);
		Element root = doc.getRootElement();
		Element body = root.element("body");
		String MsgType = body.elementText("MsgType");
		// 保存请求报文
		getUrlName("1");
		String InFilePath = mUrl+fileName+MsgType+".xml";
		System.out.println(InFilePath);
		FileWriter fw = new FileWriter(InFilePath);
		fw.write(xml);
		fw.flush();
		fw.close();
		String returnxml;
		if ("plan".equalsIgnoreCase(MsgType)) {	//约定缴
			GrpDueFeePlan gdp = new GrpDueFeePlan();
			returnxml = gdp.GrpDueFeePlanInfo(xml);
		}else {	//期缴
			GrpDueFee tfq=new GrpDueFee();
			returnxml=tfq.GrpDueFeeInfo(xml);
		}
		//保存返回 报文
		getUrlName("2");
		String OutFilePath = mUrl + "/" + fileName + MsgType + ".xml";
		FileWriter fw1 = new FileWriter(OutFilePath);
		fw1.write(returnxml);
		fw1.flush();
		fw1.close();
		return returnxml;
	}
	
	 /**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
     */
    private boolean getUrlName(String type){
    	 //String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";//发盘报文的存放路径
    	 String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='SYYBPath'";//发盘报文的存放路径
         String fileFolder = new ExeSQL().getOneValue(sqlurl);

         if (fileFolder == null || fileFolder.equals("")){
        	 aOutXmlStr = "getFileUrlName,获取文件存放路径出错";
             return false;
         }
         
         if(type.equals("1")){
        	 mUrl =fileFolder + "XQRequest/" + PubFun.getCurrentDate2()+"/";;
         }else{
        	 mUrl =fileFolder + "XQRequest/" + PubFun.getCurrentDate2()+"/";;
         }
         
         if (!newFolder(mUrl)){
        	 return false;
         }
         return true;
    }
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath){
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try{
            if (myFilePath.isDirectory()){
                System.out.println("目录已存在,文件路径为:"+myFilePath);
                return true;
            }else{
                myFilePath.mkdirs();
                System.out.println("新建目录成功,文件路径为:"+myFilePath);
                return true;
            }
        }catch (Exception e){
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
    
    
   
    
}
