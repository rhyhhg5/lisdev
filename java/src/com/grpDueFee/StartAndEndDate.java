package com.grpDueFee;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StartAndEndDate {
	//起始日期，当前日期前推三个月
	public static String StartDate(){
		Calendar ca = Calendar.getInstance();// 得到一个Calendar的实例  
		ca.setTime(new Date()); // 设置时间为当前时间  
		ca.add(Calendar.MONTH, -3);// 月份减3  
		Date resultDate = ca.getTime(); // 结果  
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		String format = sdf.format(resultDate);
		return format;
	}
	
	//结束日期，当前日期后推月
	public static String endDate(){
		Calendar ca = Calendar.getInstance();// 得到一个Calendar的实例  
		ca.setTime(new Date()); // 设置时间为当前时间  
		ca.add(Calendar.MONTH, +2);// 月份加2  
		Date resultDate = ca.getTime(); // 结果  
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		String format = sdf.format(resultDate);
		return format;
	}
}
