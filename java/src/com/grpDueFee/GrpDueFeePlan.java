package com.grpDueFee;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.operfee.GrpDueFeePlanUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPayActuSchema;
import com.sinosoft.lis.vschema.LCGrpPayActuSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GrpDueFeePlan {
	private Logger log = Logger.getLogger(GrpDueFeePlan.class);
	private XMLOutputter out = new XMLOutputter();
	String Content = "";
	String responseXml = "";
	// 组装返回报文head节点
	Element head_Response = new Element("head");
	// 组装返回报文bady节点
	Element bady_Response = new Element("body");
	Element State = new Element("State");// 处理状态：00－成功；01－失败
	Element ErrInfo = new Element("ErrInfo");// 错误信息
	// 创建返回报文
	Element Tempfee_Response = new Element("Tempfee_Respond");
	ExeSQL exeSQL = new ExeSQL();

	public String GrpDueFeePlanInfo(String xml) throws JDOMException, IOException {
		try {
			log.info(xml + ".......................begin....................................");
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			Element head = rootElement.getChild("head");
			    String sendDate = head.getChildText("SendDate");
				String sendTime = head.getChildText("SendTime");
				String branchCode = head.getChildText("BranchCode");
				String sendOperator = head.getChildText("SendOperator");
				String msgType = head.getChildText("MsgType");
			    String batchNo = head.getChildText("BatchNo");
			Element body = rootElement.getChild("body");
			Element PayFeeInfo = body.getChild("PayFeeInfo");	//本次缴费信息
			Element PlanCodeObj = PayFeeInfo.getChild("PlanCodeObj");	//内部为缴费循环
			List<Element> items = PlanCodeObj.getChildren();
			
			// 组装返回报文bead节点
			Element BatchNo = new Element("BatchNo");
			BatchNo.setText(batchNo);// 批次号
			head_Response.addContent(BatchNo);

			Element SendDate = new Element("SendDate");
			SendDate.setText(sendDate);// 发送日期
			head_Response.addContent(SendDate);

			Element SendTime = new Element("SendTime");
			SendTime.setText(sendTime);// 发送时间
			head_Response.addContent(SendTime);

			Element BranchCode = new Element("BranchCode");
			BranchCode.setText(branchCode);// 交易编码
			head_Response.addContent(BranchCode);

			Element SendOperator = new Element("SendOperator");
			SendOperator.setText(sendOperator);// 交易人员
			head_Response.addContent(SendOperator);

			Element MsgType = new Element("MsgType");
			MsgType.setText(msgType);// 固定值
			head_Response.addContent(MsgType);

			Tempfee_Response.addContent(head_Response);
			Tempfee_Response.addContent(bady_Response);
			// 获取请求报文体数据
			String StartDate = body.getChildText("StartDate");
			String EndDate = body.getChildText("EndDate");
			String GrpContNo = body.getChildText("GrpContNo");
			String PrtNo = body.getChildText("PrtNo");
			String ManageCom = body.getChildText("ManageCom");
			String AgentCode = body.getChildText("AgentCode");
			if (!(StringUtil.StringNull(batchNo)
					&& StringUtil.StringNull(sendDate)
					&& StringUtil.StringNull(sendTime)
					&& StringUtil.StringNull(branchCode)
					&& StringUtil.StringNull(sendOperator)
					&& StringUtil.StringNull(msgType))) {
				State.setText("01");
				ErrInfo.setText("报文解析失败，报文头信息缺失！");
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				responseXml = out.outputString(Tempfee_Response);
				log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
			//如果开始、结束日期为空，赋默认值
			if (StartDate == null||"".equals(StartDate.trim())) {
				StartDate = StartAndEndDate.StartDate();
			}
			if (EndDate == null||"".equals(EndDate.trim())) {
				EndDate = StartAndEndDate.endDate();
			}
			//调用校验方法进行校验并返回校验结果
				 String checkData = CheckData(GrpContNo,PrtNo);
				 if (checkData != null) {
					return checkData;
				}
				 //查询团单信息,通过团单号可获得印刷号，反之亦然
				 StringBuffer sbBuffer = new StringBuffer();
				 String GrpContSQL1 = " select b.GrpContNo,b.PrtNo,b.GrpName,'',b.CValiDate,(select sum(sumactupaymoney) from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC'),'约定缴费',ShowManageName(b.ManageCom),db2inst1.getUniteCode(b.AgentCode),b.ProposalGrpContNo "
			         +" from LCGrpCont b where  "
					 +" 1=1 ";
				 String GrpContSQL2 = "and GrpContNo = '"+ GrpContNo +"'";
				 String GrpContSQL3 = "and ManageCom like '"+ ManageCom +"%' ";
				 String GrpContSQL4 = "and PrtNo = '"+ PrtNo +"'";
				 String GrpContSQL5 = " and agentcode=db2inst1.getAgentCode('"+AgentCode+"') ";
				 sbBuffer.append(GrpContSQL1);
				 if (!(GrpContNo ==null||"".equals(GrpContNo.trim()))) {
					sbBuffer.append(GrpContSQL2);
				}
				 if (!(ManageCom ==null||"".equals(ManageCom.trim()))) {
						sbBuffer.append(GrpContSQL3);
					}
				 if (!(PrtNo ==null||"".equals(PrtNo.trim()))) {
						sbBuffer.append(GrpContSQL4);
					}
				 if (!(AgentCode ==null||"".equals(AgentCode.trim()))) {
						sbBuffer.append(GrpContSQL5);
					}
				 SSRS gcSSRC = exeSQL.execSQL(sbBuffer.toString());
				 PrtNo = gcSSRC.GetText(1, 2);
				 GrpContNo = gcSSRC.GetText(1, 1);
				 String ProposalGrpContNo = gcSSRC.GetText(1, 10);	//集体保单号码
			
			//团体保障计划未缴费信息查询
			String noFeesSQL = "select contplancode,(select contplanname from lccontplan where proposalgrpcontno=a.proposalgrpcontno and contplancode=a.contplancode), " 
				+ " prem,(select varchar(sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and plancode=a.plancode and state = '2'),"
				+ " paytodate,plancode,(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2')," +
						" (select nvl(sum(prem),0) from lcgrppaydue due where prtno=a.prtno and confstate='02' and contplancode=a.contplancode and " +
						" int(plancode)<=int(a.plancode) and exists (select 1 from lcgrpcont where prtno=due.prtno and cvalidate>='2013-1-1') ) "
	            + " from lcgrppayplan a "
	            + " where prtno= '" + PrtNo + "'  "
	            + " and int(plancode)=(select min(int(plancode)) from lcgrppayplan b where prtno= '" + PrtNo + "'  and paytodate between '"+StartDate+"' and '"+EndDate+"'" 
	            + " and exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='2') "  
	            + " and not exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='1')) "
	            + " order by contPlanCode";
			
			SSRS noFeeSSRS = exeSQL.execSQL(noFeesSQL);
			int maxRow = noFeeSSRS.getMaxRow();	//未缴费信息总条数
			if (maxRow <= 0) {
				State.setText("01");
				ErrInfo.setText("保单在该日期区间没有待抽档的续期!");

				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				responseXml = out.outputString(Tempfee_Response);
				log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}else {
					int n = 0;	//计数，判断期次和保障计划编码是否一致
					for (int i = 0; i < items.size() ; i++) { //缴费循环
						Element item = items.get(i);
						//获取报文中的保障计划编码和缴费金额
						String contPlanCode = item.getChildText("PlanCode");//保障计划编码
						String prem = item.getChildText("GetMoney");
						for (int k = 1; k <= maxRow ; k++) {
							//获取查询到的缴费信息
							String contPlanCode1 = noFeeSSRS.GetText(k, 1);	//保障计划编码
							if (contPlanCode1.equals(contPlanCode)) {
								n++;
							}
						}
						//缴费金额不能为空
						if ("".equals(prem) || prem == null ) {
							State.setText("01");
							ErrInfo.setText("第"+(i+1)+"个缴费金额（GetMoney）不能为空！");
							head_Response.addContent(State);
							head_Response.addContent(ErrInfo);
							responseXml = out.outputString(Tempfee_Response);
							log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
									+ responseXml);
							return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
						}
					}
					//校验报文中的期次和保障计划编码是否一致
					if (n != items.size()) {
						State.setText("01");
						ErrInfo.setText("计划编码（PlanNum）和保障计划编码（PlanCode）不匹配，请核对！");
						head_Response.addContent(State);
						head_Response.addContent(ErrInfo);
						responseXml = out.outputString(Tempfee_Response);
						log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
								+ responseXml);
						return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
					}
					
					//调后台，进核心
					  String tPlanCode=noFeeSSRS.GetText(1, 6);
					  LCGrpPayActuSet tLCGrpPayActuSet = new LCGrpPayActuSet();
					  List<String> PlanCodes = new ArrayList();
					  for (int i=0;i<maxRow;i++)
					  {
						  for(int k=0;k<items.size();k++){
							  if(noFeeSSRS.GetText(i+1, 1).equals(items.get(k).getChildText("PlanCode"))){
								  LCGrpPayActuSchema tLCGrpPayActuSchema = new LCGrpPayActuSchema();
								  tLCGrpPayActuSchema.setContPlanCode(noFeeSSRS.GetText(i+1, 1));
								  tLCGrpPayActuSchema.setPrem(items.get(k).getChildText("GetMoney"));
								  tLCGrpPayActuSchema.setPaytoDate(noFeeSSRS.GetText(i+1, 5));
								  tLCGrpPayActuSchema.setPlanCode(noFeeSSRS.GetText(i+1, 6));
								  tLCGrpPayActuSet.add(tLCGrpPayActuSchema);
								  PlanCodes.add(noFeeSSRS.GetText(i+1, 1));
								  System.out.println("ContPlanCode:"+noFeeSSRS.GetText(i+1, 1));
								  System.out.println("Prem:"+items.get(k).getChildText("GetMoney"));
								  System.out.println("PaytoDate:"+noFeeSSRS.GetText(i+1, 5));
								  System.out.println("PlanCode:"+noFeeSSRS.GetText(i+1, 6));
							  }
						  }
					  }
					  if (maxRow > items.size()) { // 报文中未显示的保障计划默认金额为0
							 for (int j = 0; j < maxRow; j++) {
								 if (!PlanCodes.contains(noFeeSSRS.GetText(j+1, 1))) {
									 LCGrpPayActuSchema tLCGrpPayActuSchema1 = new LCGrpPayActuSchema();
									 tLCGrpPayActuSchema1.setContPlanCode(noFeeSSRS.GetText(j+1, 1));
									 tLCGrpPayActuSchema1.setPrem("0");
									 tLCGrpPayActuSchema1.setPaytoDate(noFeeSSRS.GetText(j+1, 5));
									 tLCGrpPayActuSchema1.setPlanCode(noFeeSSRS.GetText(j+1, 6));
									  tLCGrpPayActuSet.add(tLCGrpPayActuSchema1);
									  System.out.println("ContPlanCode:"+noFeeSSRS.GetText(j+1, 1));
									  System.out.println("Prem:"+0);
									  System.out.println("PaytoDate:"+noFeeSSRS.GetText(j+1, 5));
									  System.out.println("PlanCode:"+noFeeSSRS.GetText(j+1, 6));
								 }
							}
						 }
					GlobalInput tGI = new GlobalInput(); 
					//tGI.Operator = sendOperator;
					tGI.Operator = "BAOQA";
				    TransferData tempTransferData=new TransferData();
				    tempTransferData.setNameAndValue("PrtNo",PrtNo);
				    tempTransferData.setNameAndValue("GrpContNo",GrpContNo);
				    tempTransferData.setNameAndValue("ProposalGrpContNo",ProposalGrpContNo);
				    tempTransferData.setNameAndValue("PlanCode",tPlanCode);
				    tempTransferData.setNameAndValue("Flag","");
				    System.out.println("PrtNo:GrpContNo:ProposalGrpContNo:PlanCode:Flag:"+PrtNo+":"+GrpContNo+":"+ProposalGrpContNo+":"+tPlanCode);
				    VData tVData = new VData(); 
				    tVData.add(tGI);
				    tVData.add(tempTransferData);
					tVData.add(tLCGrpPayActuSet);
					GrpDueFeePlanUI tGrpDueFeePlanUI = new GrpDueFeePlanUI();
					tGrpDueFeePlanUI.submitData(tVData, "");
					if (!tGrpDueFeePlanUI.mErrors.needDealError()) {
						State.setText("00");
						Content = "Successful";
					} else {
						State.setText("01");
						Content = "失败，原因是:" + tGrpDueFeePlanUI.mErrors.getFirstError();
						ErrInfo.setText(Content);
						head_Response.addContent(State);
						head_Response.addContent(ErrInfo);
						responseXml = out.outputString(Tempfee_Response);
						log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
								+ responseXml);
						return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
					}
				ErrInfo.setText(Content);
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				//全部成功，则封装返回报文报文体
				Element grpContNo = new Element("GrpContNo");
				grpContNo.setText(GrpContNo);
				bady_Response.addContent(grpContNo);
				//拼装返回报文数据
				String strSQL = "select 1 from LJSPayGrp where grpcontno = '" + GrpContNo + "' and paytype='YET'";
				SSRS arcodeSQL = exeSQL.execSQL(strSQL);
				//取缴费类型，判断paytype是否为YET
				if (arcodeSQL.getMaxRow() > 0) {
					strSQL = "select RiskCode,getnoticeno,SumActuPayMoney from LJSPayGrp where grpcontno = '" + GrpContNo + "' and paytype='YET'";
				}else {
					strSQL = "select RiskCode,getnoticeno,SumActuPayMoney from LJSPayGrp where grpcontno = '" + GrpContNo + "' and paytype='ZC'";
				}
				arcodeSQL = exeSQL.execSQL(strSQL);
				Element ArCode = new Element("ArCode");	//应收记录号
				ArCode.setText(arcodeSQL.GetText(1, 2));
				bady_Response.addContent(ArCode);
				
				Element PayFeeInfo1 = new Element("PayFeeInfo");
				Element RiskCodeObj = new Element("RiskCodeObj");
				PayFeeInfo1.addContent(RiskCodeObj);
				bady_Response.addContent(PayFeeInfo1);
				
				for (int j = 1; j <= arcodeSQL.getMaxRow(); j++) {
					Element Item = new Element("Item");
					Element RiskCode = new Element("RiskCode");	//险种
					Element GetMoney = new Element("GetMoney");
					RiskCode.setText(arcodeSQL.GetText(j, 1));
					GetMoney.setText(arcodeSQL.GetText(j, 3));
					Item.addContent(RiskCode);
					Item.addContent(GetMoney);
					RiskCodeObj.addContent(Item);
				}
				responseXml = out.outputString(Tempfee_Response);
				log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
		} catch (Exception e) {
			State.setText("01");
			ErrInfo.setText("流程异常或者数据错误，请检测报文");
			head_Response.addContent(State);
			head_Response.addContent(ErrInfo);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(Tempfee_Response);
			log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
			return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
		}finally{
			if(!StringUtil.StringNull(responseXml)){
				State.setText("01");
				ErrInfo.setText("发生阻断异常,返回报文为空");
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				XMLOutputter out = new XMLOutputter();
				 responseXml = out.outputString(Tempfee_Response);
				log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
				return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
			}
		}
	}
	
	//校验方法
	public String CheckData(String GrpContNo, String PrtNo) throws IOException{
		//非空校验
		if ((GrpContNo == null||"".equals(GrpContNo.trim())) && (PrtNo == null||"".equals(PrtNo.trim()))) {
			State.setText("01");
			ErrInfo.setText("保单号和印刷号不能同时为空！");
			head_Response.addContent(State);
			head_Response.addContent(ErrInfo);
			responseXml = out.outputString(Tempfee_Response);
			log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
					+ responseXml);
			return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
		}
		
		//判断该团单是不是约定缴费的团单或该保单是否已失效
		String sql = "select grpcontno from lcgrpcont a "
			+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') ";
		String sql1 =  " and GrpContNo = '"+ GrpContNo +"' ";
		String sql2 = "and PrtNo = '"+ PrtNo +"' ";
		String sql3 =  " and exists (select 1 from lcgrppayplan where prtno=a.prtno) with ur ";
		StringBuffer sbBuffer = new StringBuffer(sql);
		if (!(GrpContNo == null||"".equals(GrpContNo.trim()))) {
			sbBuffer.append(sql1);
		}
		if (!(PrtNo == null||"".equals(PrtNo.trim()))) {
			sbBuffer.append(sql2);
		}
		sbBuffer.append(sql3);
		SSRS ssrs = exeSQL.execSQL(sbBuffer.toString());
		if (ssrs.getMaxRow() <= 0) {
			State.setText("01");
			ErrInfo.setText("该团单不是约定缴费的团单或该保单已失效,请核对!");
			head_Response.addContent(State);
			head_Response.addContent(ErrInfo);
			responseXml = out.outputString(Tempfee_Response);
			log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
					+ responseXml);
			return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
		}
		
		//判断该团单是否已做过团单终止保全项目，以判断能否做续期抽档
		String Sql = "select grpcontno from lcgrpcont a "
			+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') ";
		String Sql1 =  " and GrpContNo = '"+ GrpContNo +"' ";
		String Sql2 = "and PrtNo = '"+ PrtNo +"' ";
		String Sql3 =  " and exists (select 1 from lcgrppayplan where prtno=a.prtno) and state = '03050002' with ur ";
		StringBuffer SbBuffer = new StringBuffer(Sql);
		if (!(GrpContNo == null||"".equals(GrpContNo.trim()))) {
			SbBuffer.append(Sql1);
		}
		if (!(PrtNo == null||"".equals(PrtNo.trim()))) {
			SbBuffer.append(Sql2);
		}
		SbBuffer.append(Sql3);
		SSRS ssrs1 = exeSQL.execSQL(SbBuffer.toString());
		if (ssrs1.getMaxRow() > 0) {
			State.setText("01");
			ErrInfo.setText("该团单已做过团单终止保全项目，不能做续期抽档!");
			head_Response.addContent(State);
			head_Response.addContent(ErrInfo);
			responseXml = out.outputString(Tempfee_Response);
			log.info("约定催收接口返回报文<?xml version='1.0' encoding='GBK'?>"
					+ responseXml);
			return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
		}
		return null;
	}
}
