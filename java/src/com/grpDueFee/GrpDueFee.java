package com.grpDueFee;

import java.io.IOException;
import java.io.StringReader;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.operfee.GrpDueFeeUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GrpDueFee {
	
	private Logger log = Logger.getLogger(GrpDueFee.class);
	
	private XMLOutputter out = new XMLOutputter();

	public String GrpDueFeeInfo(String xml) throws JDOMException, IOException {
		String Content = "";
		String responseXml = "";
		// 组装返回报文head节点
		Element head_Response = new Element("head");
		// 组装返回报文bady节点
		Element bady_Response = new Element("body");
		Element State = new Element("State");// 处理状态：00－成功；01－失败
		Element ErrInfo = new Element("ErrInfo");// 错误信息
		// 创建返回报文
		Element Tempfee_Response = new Element("Tempfee_Respond");
		try {
			log.info(xml + "...........................begin..........................................");
			ExeSQL exeSQL = new ExeSQL();
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			Element head = rootElement.getChild("head");
			    String sendDate = head.getChildText("SendDate");
				String sendTime = head.getChildText("SendTime");
				String branchCode = head.getChildText("BranchCode");
				String sendOperator = head.getChildText("SendOperator");
				String msgType = head.getChildText("MsgType");
			    String batchNo = head.getChildText("BatchNo");
			Element body = rootElement.getChild("body");
			
			// 组装返回报文bead节点
			Element BatchNo = new Element("BatchNo");
			BatchNo.setText(batchNo);// 批次号
			head_Response.addContent(BatchNo);

			Element SendDate = new Element("SendDate");
			SendDate.setText(sendDate);// 发送日期
			head_Response.addContent(SendDate);

			Element SendTime = new Element("SendTime");
			SendTime.setText(sendTime);// 发送时间
			head_Response.addContent(SendTime);

			Element BranchCode = new Element("BranchCode");
			BranchCode.setText(branchCode);// 交易编码
			head_Response.addContent(BranchCode);

			Element SendOperator = new Element("SendOperator");
			SendOperator.setText(sendOperator);// 交易人员
			head_Response.addContent(SendOperator);

			Element MsgType = new Element("MsgType");
			MsgType.setText(msgType);// 固定值
			head_Response.addContent(MsgType);

			Tempfee_Response.addContent(head_Response);
			Tempfee_Response.addContent(bady_Response);
            
			// 获取请求报文体数据
			String StartDate = body.getChildText("StartDate");
			String EndDate = body.getChildText("EndDate");
			String GrpContNo = body.getChildText("GrpContNo");
			String PrtNo = body.getChildText("PrtNo");
			String ManageCom = body.getChildText("ManageCom");
			String AgentCode = body.getChildText("AgentCode");
			if (!(StringUtil.StringNull(batchNo)
					&& StringUtil.StringNull(sendDate)
					&& StringUtil.StringNull(sendTime)
					&& StringUtil.StringNull(branchCode)
					&& StringUtil.StringNull(sendOperator)
					&& StringUtil.StringNull(msgType))) {
				State.setText("01");
				ErrInfo.setText("报文解析失败，报文头信息缺失！");

				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
			//如果开始、结束日期为空，赋默认值(暂时报文里未设置该节点,默认赋值)
			if (StartDate == null||"".equals(StartDate.trim())) {
				StartDate = StartAndEndDate.StartDate();  //当前日期前推三个月
			}
			if (EndDate == null||"".equals(EndDate.trim())) {
				EndDate = StartAndEndDate.endDate();      //当前日期后推两个月
			}
			//保单号、印刷号非空校验
			if ((GrpContNo == null||"".equals(GrpContNo.trim())) && ( PrtNo == null||"".equals(PrtNo.trim()))) {
				State.setText("01");
				ErrInfo.setText("保单号和印刷号不能同时为空！");
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
			
			StringBuffer sb = new StringBuffer();
			String sql1 = "select 1 from lcgrpcont where  state = '03050002'";
			String sql2	=" and GrpContNo = '"+ GrpContNo +"'";
			String sql3 = " and PrtNo = '" + PrtNo +"'";
			sb.append(sql1);
			//判断团单号是否为空
			if (!(GrpContNo == null||"".equals(GrpContNo.trim()))) {
				sb.append(sql2);
			}
			//判断印刷号是否为空
			if (!(PrtNo == null||"".equals(PrtNo.trim()))) {
				sb.append(sql3);
			}
			SSRS ssrs = exeSQL.execSQL(sb.toString());
			if (ssrs.getMaxRow() > 0) {
				State.setText("01");
				ErrInfo.setText("该保单已经终止缴费，无法进行抽档");
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
			
			StringBuffer sbGrp = new StringBuffer();
			String grpSQL = "select b.GrpContNo,b.PrtNo,b.GrpName,'',b.CValiDate,b.SumPrem,value((select accGetMoney from LCAppAcc where customerNo = b.appntNo), 0),(select sum(prem) from lccont where b.grpcontno = grpcontno  and PayIntv > 0),(SELECT min(paytodate) from lcgrppol a where a.grpcontno=b.grpcontno) payToDate, "
					+ "(select codename from ldcode where codetype='paymode' and code=b.PayMode),ShowManageName(b.ManageCom),db2inst1.getUniteCode(b.AgentCode),'',b.proposalGrpContNo "
					+ " from LCGrpCont b where (b.state is null or b.state <> '03050002') and  "
					+ " b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1')  and b.GrpContNo not in (select otherno from ljspay where othernotype='1')  and b.GrpContNo in(select GrpContNo from LCGrpPol c where"
					+ " (PaytoDate>='"+ StartDate
					+ "' and PaytoDate<='"+ EndDate
					+ "' and PaytoDate < PayEndDate )";
			String grpSQL1 =  " and managecom like '" + ManageCom + "%'";
			String grpSQL2 =  " and PayIntv>0 and AppFlag='1' and (StateFlag is null or StateFlag = '1')"
					+ " and GrpPolNo not in (select GrpPolNo from LJSPayGrp)"
					// add by xp 剔除一年期险种年缴情况的抽档
					+ " and not exists (select 1 from lcgrppol d where c.grppolno=d.grppolno and payintv=12 and exists (select 1 from lmriskapp where riskcode=d.riskcode  and riskperiod in ('M','S')))"
					+ " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y'))" ;
			String grpSQL3 =  " and GrpContNo = '" + GrpContNo +"'";
			String grpSQL4 = " and ManageCom like '" + ManageCom + "%'";
			String grpSQL5 = " and agentcode = db2inst1.getAgentCode('" + AgentCode + "')";
			String grpSQL6 = " and PrtNo = '" + PrtNo +"'";
			sbGrp.append(grpSQL);
			//判断机构是否为空
			if (!(ManageCom == null||"".equals(ManageCom.trim()))) {
				sbGrp.append(grpSQL1);
			}
			sbGrp.append(grpSQL2);
			//判断团单号是否为空
			if (!(GrpContNo == null||"".equals(GrpContNo.trim()))) {
				sbGrp.append(grpSQL3);
			}
			//判断机构是否为空
			if (!(ManageCom == null||"".equals(ManageCom.trim()))) {
				sbGrp.append(grpSQL4);
			}
			//判断代理人编码是否为空
			if (!(AgentCode == null||"".equals(AgentCode.trim()))) {
				sbGrp.append(grpSQL5);
			}
			//判断印刷号是否为空
			if (!(PrtNo == null||"".equals(PrtNo.trim()))) {
				sbGrp.append(grpSQL6);
			}
			SSRS exe = exeSQL.execSQL(sbGrp.toString());
			if (exe.getMaxRow() <= 0) { // 没查到数据,没有符合条件的可催收保单信息
				State.setText("01");
				ErrInfo.setText("没有符合条件的可催收保单信息");
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			} else {
				GrpContNo = exe.GetText(1, 1);
				String ProposalGrpContNo = exe.GetText(1, 14);
				GlobalInput tGI = new GlobalInput();
				//tGI.Operator = "pa3650";
				tGI.Operator = "BAOQA";
				tGI.Operator =sendOperator;
				LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
				tLCGrpContSchema.setGrpContNo(GrpContNo);
				TransferData tempTransferData = new TransferData();
				tempTransferData.setNameAndValue("StartDate", StartDate);
				tempTransferData.setNameAndValue("EndDate", EndDate);
				tempTransferData.setNameAndValue("ManageCom", ManageCom);
				tempTransferData.setNameAndValue("LoadFlag",null);

				VData tVData = new VData();
				tVData.add(tLCGrpContSchema);
				tVData.add(tGI);
				tVData.add(tempTransferData);
				
				GrpDueFeeUI tGrpDueFeeUI = new GrpDueFeeUI();
				tGrpDueFeeUI.submitData(tVData, "INSERT");
				if (!tGrpDueFeeUI.mErrors.needDealError()) {
					State.setText("00");
					Content = "Successful";
					//封装返回报文报文体
					Element proposalGrpContNo = new Element("ProposalGrpContNo");
					proposalGrpContNo.setText(ProposalGrpContNo);
					bady_Response.addContent(proposalGrpContNo);
					
					//催收完成后查询保单，查询应收记录号
//					String arCode = "select b.GetNoticeNo from LCGrpCont  a, ljspay b,ljspaygrp c where  "
//						 + " a.AppFlag='1' and (a.StateFlag is null or a.StateFlag = '1') and b.OtherNo=a.GrpContNo and  b.othernotype='1' and c.GetNoticeNo=b.GetNoticeNo and c.GrpContNo=b.otherno and  b.MakeDate='" + tempCurrentTime + "'  "
//						 + " and a.ManageCom like '" + ManageCom + "%' " 
//						 + " and a.GrpContNo='" + GrpContNo +"'"
//						 + " group by a.GrpContNo,a.PrtNo,a.GrpName,a.CValiDate,a.SumPrem,a.appntNo,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode";
					
					//拼装返回报文数据
					String strSQL = "select 1 from LJSPayGrp where grpcontno = '" + GrpContNo + "' and paytype='YET'";
					SSRS arcodeSQL = exeSQL.execSQL(strSQL);
					//取缴费类型，判断paytype是否为YET
					if (arcodeSQL.getMaxRow() > 0) {
						strSQL = "select RiskCode,getnoticeno,SumActuPayMoney from LJSPayGrp where grpcontno = '" + GrpContNo + "' and paytype='YET'";
					}else {
						strSQL = "select RiskCode,getnoticeno,SumActuPayMoney from LJSPayGrp where grpcontno = '" + GrpContNo + "' and paytype='ZC'";
					}
					SSRS exearCode = exeSQL.execSQL(strSQL);
					
					Element ArCode = new Element("ArCode");
					ArCode.setText(exearCode.GetText(1, 2));
					bady_Response.addContent(ArCode);
					Element PayFeeInfo = new Element("PayFeeInfo");
					Element RiskCodeObj = new Element("RiskCodeObj");
					bady_Response.addContent(PayFeeInfo);
					PayFeeInfo.addContent(RiskCodeObj);
					Element Item = new Element("Item");
					Element RiskCode = new Element("RiskCode");	//险种编码
					Element GetMoney = new Element("GetMoney");	//缴费金额
						//催收成功后，查看该保单的险种信息
						/*String PolSQL = "select contPlanCode, "
				            + " count(1), "
				            + "   (select riskname  from lmrisk where   riskcode=a.riskcode), "
				            + "   a.riskCode, "
				            + "   (select codeName from LDCode where code=char(a.payIntv) and codeType='payintv'), "
				            + "   sum(prem), min(cValiDate), min(payToDate), min(endDate) "
				            + "from LCPol a "
				            + "where grpContNo = '" + GrpContNo + "' and (a.StateFlag is null or a.StateFlag = '1') "
				            + " and payintv != 0 "
				            + "group by grpContNo, contPlanCode, riskCode, riskCode, payIntv "
				            + "order by contPlanCode, riskCode ";
						SSRS exePol = exeSQL.execSQL(PolSQL);*/
					for (int j = 1; j <= arcodeSQL.getMaxRow(); j++) {
						RiskCode.setText(arcodeSQL.GetText(j, 1));//险种编码
						GetMoney.setText(arcodeSQL.GetText(j, 3));//缴费金额
						Item.addContent(RiskCode);
						Item.addContent(GetMoney);
						RiskCodeObj.addContent(Item);
					}
				} else {
					Content = " 失败，原因是:" + tGrpDueFeeUI.mErrors.getFirstError();
					State.setText("01");
					ErrInfo.setText(Content);
					//回滚
				}
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"
						+ responseXml);
				return "<?xml version='1.0' encoding='GBK'?>" + responseXml;
			}
		} catch (Exception e) {
			State.setText("01");
			ErrInfo.setText("流程异常或者数据错误，请检测报文");
			head_Response.addContent(State);
			head_Response.addContent(ErrInfo);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(Tempfee_Response);
			log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
			return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
		}finally{
			if(!StringUtil.StringNull(responseXml)){
				State.setText("01");
				ErrInfo.setText("发生阻断异常,返回报文为空");
				head_Response.addContent(State);
				head_Response.addContent(ErrInfo);
				XMLOutputter out = new XMLOutputter();
				 responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
				return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
			}
		}
	}
}