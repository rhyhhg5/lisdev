package com.MsgXmlUntil.serviceTest;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

import javax.xml.namespace.QName;

public class TestSentXMLThrowWebservice {
	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		try {
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return mByteArrayOutputStream.toByteArray();
	}

	@SuppressWarnings("rawtypes")
	public void service(String aInXmlStr) {
		// webservice 地址
		String sql = "select TransDetail,TransDetail2 from WFTransInfo where transtype = 'DealClaimsInterface' and transcode = '1'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql1SSRS = tExeSQL.execSQL(sql);
		
//		String tStrTargetEendPoint = sql1SSRS.GetText(1, 2);
//		String tStrNamespace = sql1SSRS.GetText(1, 1);
		String tStrTargetEendPoint = "http://10.253.33.168:8080/picc/services/DealClaimsInterface/service";
		String tStrNamespace = "http://claim.com";
		try {
			RPCServiceClient client = new RPCServiceClient();
			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
//			option.setAction("service");// 调用方法名
			QName name = new QName(tStrNamespace, "service");// 调用方法名
			Object[] object = new Object[] { aInXmlStr };
			Class[] returnTypes = new Class[] { String.class };
			Object[] response = client.invokeBlocking(name, object, returnTypes);
			String result = (String) response[0];
			System.out.println("UI return:" + result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		try {
			TestSentXMLThrowWebservice test = new TestSentXMLThrowWebservice();

			String mInFilePath = "C:/Users/Administrator/Desktop/12.xml";
			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = test.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "GBK");
			System.out.println(mInXmlStr);
			test.service(mInXmlStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
