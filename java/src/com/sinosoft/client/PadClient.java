package com.sinosoft.client;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

public class PadClient {
	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}
	
	public void service(String aInXmlStr){
//		webservice 地址
//		String  tStrTargetEendPoint = "http://10.252.130.99:8080/wasforwardxml/services/ComplexServiceProxy";
//		String tStrTargetEendPoint = "http://10.136.1.3:9907services/PubServiceProxy";//正式机微信电商接口！！！！
//		String tStrNamespace = "http://services.ws.lis.sinosoft.com";
//	    String tStrTargetEendPoint = "http://localhost:8080/ui/services/WasForwardXmlProxy";
		String tStrTargetEendPoint = "http://10.136.10.101:900/services/WasForwardXmlProxy";//PAD接口
//		String tStrTargetEendPoint = "http://localhost:8080/ui/services/PubServiceProxy";//微信、电商接口.、
//		String tStrTargetEendPoint = "http://10.136.10.101:900/services/PubServiceProxy";
		
		String tStrNamespace = "http://services.wasforwardxml.sinosoft.com";
//		String tStrNamespace = "http://services.core.cbsws.com";
//		E:\lisdev\java\src\com\sinosoft\wasforwardxml\services\WasForwardXmlProxy.java
		
		try {
			RPCServiceClient client = new RPCServiceClient();
			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
			option.setAction("service");//调用方法名
			QName name = new QName(tStrNamespace, "service");//调用方法名
			Object[] object = new Object[] { aInXmlStr };
			Class[] returnTypes = new Class[] { String.class };
			Object[] response = client.invokeBlocking(name, object, returnTypes);
			String result = (String) response[0];
			System.out.println("UI return:" + result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
	      try {
	    	  PadClient tPadClient = new PadClient();
	    	  String mInFilePath="";
	    		 mInFilePath = "E:/3/7/PD21000008352_TB.xml";

				InputStream mIs = new FileInputStream(mInFilePath);
				byte[] mInXmlBytes = tPadClient.InputStreamToBytes(mIs);
				String mInXmlStr = new String(mInXmlBytes, "GBK");
				System.out.println(mInXmlStr);
				tPadClient.service(mInXmlStr);
//				PubServiceProxy mm=new PubServiceProxy();
//				WasForwardXmlProxy mm=new WasForwardXmlProxy();
//				mm.service(mInXmlStr);
	      } catch (Exception e) {
	          e.printStackTrace();
	      }
	}
}