package com.sinosoft.test;

import com.sinosoft.lis.certifybusiness.CertifyStateTakeBackBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CertifyBack {
	
	 private TransferData mTransferData = new TransferData();

	    /** 全局数据 */
	    private GlobalInput mGlobalInput = new GlobalInput();
	
	public void CertifyBackRun() {

		String tPrtNo = (String) mTransferData.getValueByName("PrtNo");

		if (tPrtNo == null || tPrtNo.equals("")) {
			System.out.println("印刷号为空！");
		}
		
		String tSql = "select 1 from lcgrpcont where prtno = '"+tPrtNo+"'";
		String tIsCont = new ExeSQL().getOneValue(tSql);
		if(!"1".equals(tIsCont)){
			System.out.println("未查到保单信息，到此截止！");
		}

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", tPrtNo);

		VData vData = new VData();

		vData.addElement(mGlobalInput);
		vData.addElement(tTransferData);

		// 对单证系统中的对应单证进行正常核销。
		System.out.println("单证回销，开始时间是" +PubFun.getCurrentTime());
		try {
			CertifyStateTakeBackBL tCertifyStateTakeBackBL = new CertifyStateTakeBackBL();
			if (!tCertifyStateTakeBackBL.submitData(vData, "ConfBack")) {
				System.out.println("单证回销失败！");
			}else{
				System.out.println("单证回销成功！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		System.out.println("单证回销，结束时间是" +PubFun.getCurrentTime());
	}
	
	public static void main(String[] args){
		CertifyBack tCertifyBack = new CertifyBack();
		tCertifyBack.mGlobalInput.Operator = "000";
		tCertifyBack.mGlobalInput.ManageCom = "86";
		tCertifyBack.mTransferData.setNameAndValue("PrtNo", "19000131311");
		tCertifyBack.CertifyBackRun();
	}
}
