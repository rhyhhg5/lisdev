package com.sinosoft.test;

import com.sinosoft.lis.certifybusiness.CertActBatchImportBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CardActiveBatch {
	
	public static void main(String[] args){
		try
		{
			GlobalInput tGI = new GlobalInput();
			
			tGI.ManageCom = "86";
			tGI.Operator = "001";
			String Dates = "2011-01-10，2011-01-12，2011-01-13";
		    String[] tActiveDates = Dates.split(",");
		    
		    for(int i=0;i<tActiveDates.length;i++){
		    	
		    	String tActiveDate = tActiveDates[i];
		    	System.out.println("激活时间："+tActiveDate);
		    	VData tVData = new VData();
			    
			    TransferData tTransferData = new TransferData();
			    tTransferData.setNameAndValue("ActiveDate", tActiveDate);
			    
			    tVData.add(tTransferData);
			    tVData.add(tGI);
			    
			    CertActBatchImportBL tCertActBatchImportBL = new CertActBatchImportBL();
			    if(!tCertActBatchImportBL.submitData(tVData, "ActSyncBatch"))
			    {
			        System.out.println( " 卡激活数据处理失败，原因是: " + tCertActBatchImportBL.mErrors.getFirstError());
			    }
			    else
			    {
			        System.out.print( " 卡激活数据处理成功！");
			    }
		    }
		    System.out.println("执行OK。。。。。。");
		    
		}
		catch (Exception e)
		{
			System.out.print( "抛异常了！");
			e.printStackTrace();
		}
	}

}
