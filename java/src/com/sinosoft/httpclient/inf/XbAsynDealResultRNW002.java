package com.sinosoft.httpclient.inf;

import java.util.List;

import com.sinosoft.httpclient.dto.rnw002.request.Head;
import com.sinosoft.httpclient.dto.rnw002.request.RNW002Request;
import com.sinosoft.httpclient.dto.rnw002.request.RequestNode;
import com.sinosoft.httpclient.dto.rnw002.request.RequestNodes;
import com.sinosoft.httpclient.dto.rnw002.response.RNW002Response;
import com.sinosoft.httpclient.dto.rnw002.response.ResponseNode;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class XbAsynDealResultRNW002 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /**实收号*/
	  private String mPayNo;
	  /**保单号*/
	  private String mContNo;
	  /**业务类型*/
	  private String mBusinessType;
	  /**预约码*/
	  private String mBookingSequenceNo;
	  /**数据操作类型*/
	  private String mOperate;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  
	  /**
	   * 提供无参的构造方法
	   */
	  public XbAsynDealResultRNW002() {}
	
	/**
	 * 获取平台客户码的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    if(!dealData()){
	    	return false;
	    }
	    return true;
	}
	/**
	 * 获取传入的数据
	 */
	public boolean getInputData(){
		/**
  	 * 接收传入的数据
  	 */
  	tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
  	mBookingSequenceNo = (String) tempTransferData.getValueByName("BookingSequenceNo");
 		System.out.println(" ************* "  + mBookingSequenceNo);
 		if(null==mBookingSequenceNo || "".equals(mBookingSequenceNo)){
  		CError tCError = new CError();
  		tCError.moduleName = "XbAsynDealResult";
  		tCError.functionName = "getInputData";
  		tCError.errorMessage = "传入的预约码为空！";
  		this.mErrors.addOneError(tCError);
  		return false;
  	}
 		return true;
	}
	/**
	 * 数据处理方法
	 */
	public boolean dealData(){
		/**
 		 * 封装请求报文dto
 		 * 先上传一个客户号，获取信息校验平台的平台客户编码
 		 */
     	RNW002Request tRNW002Request = new RNW002Request();
     	RequestNode tRequestNode = new RequestNode();
     	tRequestNode.setBookingSequenceNo(mBookingSequenceNo);
     	tRNW002Request.getRequestNodes().getRequestNode().add(tRequestNode);
 		/**生成xml的头部信息*/
     	Head head=new Head();
     	try {
     		/**发送报文获取返回结果 */
			String responseXml = CommunicateServiceImpl.post(tRNW002Request,mOperate);
			
			/******************************************************************
			 ***********************交互之后响应部分******************************************/
			/**解析返回结果 */
			RNW002Response tRNW002Response = (RNW002Response)XmlParseUtil.xmlToDto(responseXml, RNW002Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tRNW002Response.getHead().getResponseCode();
			
			ResponseNode responseNode = tRNW002Response.getResponseNodes().getResponseNode();
			/**
			 * 添加上平台那边返回错误的详细信息
			 */
			String resultInfoDesc = tRNW002Response.getResponseNodes().getResponseNode().getResult().getResultInfoDesc();
			
			System.out.println("响应报文已返回。"+responseCode);
			/**
			 * 添加上平台那边返回错误的详细信息
			 */
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				String resultStatus = responseNode.getResult().getResultStatus();
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tRNW002Response,mErrorInfo);
					System.out.println("核心获取返回报文后的处理。。。");
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tRNW002Response,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "XbAsynDealResult";
					tCError.functionName = "XbAsynDealResult";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "XbAsynDealResult";
					tCError.functionName = "XbAsynDealResult";
					tCError.errorMessage = resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tRNW002Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "XbAsynDealResult";
					tCError.functionName = "XbAsynDealResult";
					tCError.errorMessage = resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tRNW002Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "XbAsynDealResult";
					tCError.functionName = "XbAsynDealResult";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tRNW002Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
     	return true;
	}
	/**
	 * 获取结果的公共方法
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(RNW002Response mRNW002Response,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_NAME_BATCHNO", 20);
		String TransType = mRNW002Response.getHead().getTransType();
		String TransNo = mRNW002Response.getHead().getTransNo();
		String responseCode = mRNW002Response.getHead().getResponseCode();
		String resultStatus = mRNW002Response.getResponseNodes().getResponseNode().getResult().getResultStatus();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"',null,'','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BookingSequenceNo", "32019055947");
		tVData.add(tTransferData);
		XbAsynDealResultRNW002 tXbAsynDealResult= new XbAsynDealResultRNW002();
		tXbAsynDealResult.submitData(tVData,"ZBXPT");
		
	}
}
