package com.sinosoft.httpclient.inf;

import com.sinosoft.httpclient.dto.nbu001.request.Head;
import com.sinosoft.httpclient.dto.nbu001.request.NBU001Request;
import com.sinosoft.httpclient.dto.nbu001.request.RequestNode;
import com.sinosoft.httpclient.dto.nbu001.response.NBU001Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSErrorListSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:  获取客户税优识别码</p>
 * <p>Description:根据保单号获取客户税优识别码</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class GetTaxCode {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /**用于存储传入的保单号*/
	  private String mContNo;
	  /**用于存储查询的客户号*/
//	  private String mAppntNo;
	  /**用于存储客户平台编码*/
	  private String mPTCustomerNo;
	  /**用于存储税优识别码*/
	  private String mTaxCode;
	  /**存储错误信息*/
	  private String mErrorInfo;
	  /**
	   * 提供无参的构造方法
	   */
	  public GetTaxCode() {}
	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    
   		if(!dealData()){
			return false;
		}
   		if(!prepareData()){
   			return false;
   		}
		return true;
	}
	/**
	 * 获取传入的方法
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
   		 */
   		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
   		mContNo = (String) tempTransferData.getValueByName("ContNo");
   		System.out.println(mContNo);
   		if(mContNo==null&&mContNo.equals("")){
   			CError tCError = new CError();
   			tCError.moduleName = "ConnectZBXServiceImpl";
   			tCError.functionName = "getMessage";
   			tCError.errorMessage = "传入的保单号为空！";
   			this.mErrors.addOneError(tCError);
   			return false;
		}
   		return true;
	}
	/**
	 * 校验平台客户号是否已经获取
	 */
	public boolean checkPlatformCustomerNo(){
		//根据保单号查询ldperson表中是否存在平台客户号
		String pSQL = "select a.PlatformCustomerNo from ldpseron a where customerno = (select b.appntno from lccont b where b.contno = '"+mContNo+"')";
		ExeSQL tExeSQL = new ExeSQL();
		String mResult = tExeSQL.getOneValue(pSQL);
		if(mResult!=null&&!mResult.equals("")){
			return true;
		}
		return false;
	}
	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 上传保单号，从中保信获取税优识别码
   		 */
       	NBU001Request tNBU001Request = new NBU001Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setPolicyNo(mContNo);
       	tNBU001Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
			String responseXml = CommunicateServiceImpl.post(tNBU001Request,mOperate);
			/**获取客户校验平台的平台客户编码 */
			NBU001Response tNBU001Response = (NBU001Response)XmlParseUtil.xmlToDto(responseXml, NBU001Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tNBU001Response.getHead().getResponseCode();
			String resultStatus = tNBU001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tNBU001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
					System.out.println("系统正常，业务成功处理！");
					this.mErrorInfo = "系统正常，业务成功处理！";
					saveIntoLSErrorList(tNBU001Response,mErrorInfo,responseCode,resultStatus);
					/**
					 * 上传 平台客户号和保单号码 从信息验证平台上获取税优识别码
					 */
					mTaxCode = tNBU001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().get(0).getTaxCode();
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = "系统正常，等待异步返回!";
					saveIntoLSErrorList(tNBU001Response,mErrorInfo,responseCode,resultStatus);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为："+resultInfoDesc;
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = tCError.errorMessage;
					saveIntoLSErrorList(tNBU001Response,mErrorInfo,responseCode,resultStatus);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："+resultInfoDesc;
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = tCError.errorMessage;
					saveIntoLSErrorList(tNBU001Response,mErrorInfo,responseCode,resultStatus);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："+resultInfoDesc;
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = tCError.errorMessage;
					saveIntoLSErrorList(tNBU001Response,mErrorInfo,responseCode,resultStatus);
					return false;
				}
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "GetTaxCode";
			tCError.functionName = "getTaxCode";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	/***********************************************************
	 * 向LSErrorList表中增加轨迹【无论成功与否都记录一条信息】
	 * *********************************************************
	 */
	private void saveIntoLSErrorList(NBU001Response tNBU001Response, String mErrorInfo, String responseCode, String resultStatus) {
		try {
			//准备参数
			String maxNo = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);
			String transType = tNBU001Response.getHead().getTransType();
			String transNo = tNBU001Response.getHead().getTransNo();
			String sql = "select prtno from lccont where contno = '"+mContNo+"' ";
			ExeSQL tExesql = new ExeSQL();
			String prtno = tExesql.getOneValue(sql);
			//封装数据到LSErrorListSchema
			LSErrorListSchema tLSErrorListSchema = new LSErrorListSchema();
			tLSErrorListSchema.setSerNo(maxNo);
			tLSErrorListSchema.setTransType(transType);
			tLSErrorListSchema.setTransNo(transNo);
			tLSErrorListSchema.setBusinessNo(prtno);
			tLSErrorListSchema.setResponseCode(responseCode);
			tLSErrorListSchema.setResultStatus(resultStatus);
			tLSErrorListSchema.setErrorInfo(mErrorInfo);
			tLSErrorListSchema.setMakeDate(PubFun.getCurrentDate());
			tLSErrorListSchema.setMakeTime(PubFun.getCurrentTime());
			tLSErrorListSchema.setModifyDate(PubFun.getCurrentDate());
			tLSErrorListSchema.setModifyTime(PubFun.getCurrentTime());
			//提交数据
			MMap map = new MMap();
			VData mResult = new VData();
			map.put(tLSErrorListSchema, "DELETE&INSERT");//先删再插
			mResult.add(map);
			PubSubmit tSubmit = new PubSubmit();
			if (!tSubmit.submitData(mResult, ""))
			{
				this.mErrors.copyAllErrors(tSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "GetTaxCode";
				tError.functionName = "submitData";
				tError.errorMessage = "向LSErrorList表提交数据失败!";
				this.mErrors.addOneError(tError);
			}
		}catch(Exception e) {
			System.out.println("向LSErrorList表中保存数据时出错！");
			e.printStackTrace();
		}
	}
	public boolean prepareData(){
		/**
		 * 将税优识别码放入到结果集中
		 */
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("TaxCode", mTaxCode);
		System.out.println(mTaxCode);
		mResult.add(tTransferData);
		return true;
	}
	/**
	 * 获取结果集
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	
	/**
	 * 获取结果集
	 * @param args
	 */
	public String getTaxCode(){
		return mTaxCode;
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", "13032593126");
		tVData.add(tTransferData);
		GetTaxCode tGetTaxCode= new GetTaxCode();
		tGetTaxCode.submitData(tVData, "HQSYSBM");
		
		
	}
}
