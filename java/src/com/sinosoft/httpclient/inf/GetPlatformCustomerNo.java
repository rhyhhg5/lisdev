package com.sinosoft.httpclient.inf;


import com.sinosoft.httpclient.dto.pty001.request.Head;
import com.sinosoft.httpclient.dto.pty001.request.PTY001Request;
import com.sinosoft.httpclient.dto.pty001.request.RequestNode;
import com.sinosoft.httpclient.dto.pty001.response.PTY001Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData; 


/**
 * <p>Title:  客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Description:客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class GetPlatformCustomerNo {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  
	  /**用于存储查询的客户号*/
	  private String mAppntNo;
	  /**数据操作类型*/
	  private String mOperate;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  /**批次号*/
	  private String mBatchNo;
	  
	  /**
	   * 提供无参的构造方法
	   */
	  public GetPlatformCustomerNo() {}
	
	/**
	 * 获取平台客户码的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    if(!dealData()){
	    	return false;
	    }
	   		
	    return true;
	}
	/**
	 * 获取传入的数据
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
    	 */
    	tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
    	mAppntNo = (String) tempTransferData.getValueByName("CustomerNo");
    	mBatchNo = (String) tempTransferData.getValueByName("BatchNo");
   		System.out.println(mAppntNo);
   		if(mAppntNo==null&&mAppntNo.equals("")){
    		CError tCError = new CError();
    		tCError.moduleName = "GetPlatformCustomerNo";
    		tCError.functionName = "getMessage";
    		tCError.errorMessage = "传入的客户号为空！";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
   		return true;
	}
	/**
	 * 数据处理方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 先上传一个客户号，获取信息校验平台的平台客户编码
   		 */
       	PTY001Request tPTY001Request = new PTY001Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setCustomerNo(mAppntNo);
       	tPTY001Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
			String responseXml = CommunicateServiceImpl.post(tPTY001Request,mOperate);
			/**获取客户校验平台的平台客户编码   mPTCustomerNo    */
			PTY001Response tPTY001Response = (PTY001Response)XmlParseUtil.xmlToDto(responseXml, PTY001Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tPTY001Response.getHead().getResponseCode();
			String resultStatus = tPTY001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			
			/**
			 * 添加上平台那边返回错误的详细信息
			 */
			String resultInfoDesc = tPTY001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tPTY001Response,mErrorInfo);
					String mPTCustomerNo = tPTY001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPlatformCustomerNo();
					String mCustomerNo = tPTY001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getCustomerNo();
					String updLDPerson = "update ldperson a set a.platformcustomerno ='"+mPTCustomerNo+"' where a.customerno='"+mCustomerNo+"'";
					boolean updLDPersonF=tExeSQL.execUpdateSQL(updLDPerson);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tPTY001Response,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tPTY001Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tPTY001Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tPTY001Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	return true;
	}
	/**
	 * 获取结果的公共方法
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(PTY001Response mPTY001Response,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);
		String TransType = mPTY001Response.getHead().getTransType();
		String TransNo = mPTY001Response.getHead().getTransNo();
		String responseCode = mPTY001Response.getHead().getResponseCode();
		String resultStatus = mPTY001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String resultInfo = mPTY001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"','"+mAppntNo+"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
		String updateSQL = "";
		if(resultStatus!=null && "00".equals(resultStatus)){
			updateSQL = "update lsinsuredlist set checkflag='00',errormessage='内检成功',modifydate=current date,modifytime=current time where batchno='"+mBatchNo+"' and customerno='"+mAppntNo+"'";
		}else{
			updateSQL = "update lsinsuredlist set checkflag='01',errormessage='"+resultInfo+"',modifydate=current date,modifytime=current time where batchno='"+mBatchNo+"' and customerno='"+mAppntNo+"'";
		}
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(updateSQL)){
			System.out.println("更新信息失败--"+TransType);
		}
		System.out.println("更新信息成功--"+TransType);
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CustomerNo", "013969261");
		tVData.add(tTransferData);
		GetPlatformCustomerNo tGetPlatformCustomerNo= new GetPlatformCustomerNo();
		tGetPlatformCustomerNo.submitData(tVData,"ZBXPT");
		
	}
}