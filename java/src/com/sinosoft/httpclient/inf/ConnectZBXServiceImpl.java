package com.sinosoft.httpclient.inf;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:  连接中保信的接口</p>
 * <p>Description:中保信     连接中保信的接口  为获取税优识别码等</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class ConnectZBXServiceImpl {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
//	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /** 数据操作字符串 */
	  private String mOperate;
//	  /**用于存储传入的保单号*/
	  private String mContNo;
//	  /**用于存储查询的客户号*/
//	  private String mAppntNo;
	  /**
	   * 提供无参的构造方法
	   */
	public ConnectZBXServiceImpl() {}
	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
		/**
    	 * 根据传入的操作类型去判断需要进行什么样的操作
    	 */
	    if(mOperate!=null&&!mOperate.equals("")){
	    	/**
	    	 * 获取税优识别码的操作方法
	    	 */
	    	if(mOperate.equals("ZBXPT")){
	    		GetTaxCode tGetTaxCode = new GetTaxCode();
	    		if(!tGetTaxCode.submitData(mInputData,mOperate)){
	    			 // @@错误处理
	    		      this.mErrors.copyAllErrors(tGetTaxCode.mErrors);
	    		      mResult.clear();
	    		      System.out.println("---------------------");
	    		      //将获取税优识别码的错误信息保存到hislcinsurelist表中
	    		      tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
	    		      mContNo = (String) tempTransferData.getValueByName("ContNo");
//	    		      String error = "获取税优识别码失败！原因为："+mErrors.getFirstError();
//	    		      String eSql = "update hislcinsuredlist a set a.errorinfo = '"+error+"' where a.contno = "
//	    		      		+ "(select b.proposalcontno from lccont b where b.contno = '"+mContNo+"')";
//	    		      tExeSQL.execUpdateSQL(eSql);
//	    		      System.out.println(mErrors.getFirstError());
	    		      System.out.println("---------------------");
	    		      mResult.add(mErrors.getFirstError());
	    		      return false;
	    		}
	    		mResult = tGetTaxCode.getResult();
	    	}
	    }
		return true;
	}
	/**
	 *	获取结果集
	 */
	public VData getResult(){
		
		return mResult;
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", "001012268000022");
		tVData.add(tTransferData);
		ConnectZBXServiceImpl tConnectZBXServiceImpl= new ConnectZBXServiceImpl();
		tConnectZBXServiceImpl.submitData(tVData, "ZBXPT");
		tVData = tConnectZBXServiceImpl.getResult();
		tTransferData = (TransferData) tVData.getObjectByObjectName("TransferData", 0);
		String taxCode = (String) tTransferData.getValueByName("TaxCode");
		System.out.println(taxCode);
		
	}
}
