package com.sinosoft.httpclient.inf;


import java.util.List;

import com.sinosoft.httpclient.dto.pty002.request.Head;
import com.sinosoft.httpclient.dto.pty002.request.PTY002Request;
import com.sinosoft.httpclient.dto.pty002.request.RequestNode;
import com.sinosoft.httpclient.dto.pty002.response.PTY002Response;
import com.sinosoft.httpclient.dto.pty002.response.ResponseNode;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData; 


/**
 * <p>Title:  客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Description:客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class GetPlatformCustomerNoPTY002 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  
	  /**用于存储异步预约码*/
	  private String bookingSequenceNo;
	  /**数据操作类型*/
	  private String mOperate;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  
	  /**
	   * 提供无参的构造方法
	   */
	  public GetPlatformCustomerNoPTY002() {}
	
	/**
	 * 获取平台客户码的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    if(!dealData()){
	    	return false;
	    }
	   		
	    return true;
	}
	/**
	 * 获取传入的数据
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
    	 */
    	tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
    	bookingSequenceNo = (String) tempTransferData.getValueByName("BookingSequenceNo");
   		System.out.println(bookingSequenceNo);
   		if(bookingSequenceNo==null&&bookingSequenceNo.equals("")){
    		CError tCError = new CError();
    		tCError.moduleName = "GetPlatformCustomerNo";
    		tCError.functionName = "getMessage";
    		tCError.errorMessage = "传入的异步预约码为空！";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
   		return true;
	}
	/**
	 * 数据处理方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 先上传一个客户号，获取信息校验平台的平台客户编码
   		 */
       	PTY002Request tPTY001Request = new PTY002Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setBookingSequenceNo(bookingSequenceNo);
       	tPTY001Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
			String responseXml = CommunicateServiceImpl.post(tPTY001Request,mOperate);
			/**获取客户校验平台的平台客户编码   mPTCustomerNo    */
			PTY002Response tPTY002Response = (PTY002Response)XmlParseUtil.xmlToDto(responseXml, PTY002Response.class);
			
			List<ResponseNode> responseNodeList = tPTY002Response.getResponseNodes().getResponseNode();
			
			
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tPTY002Response.getHead().getResponseCode();
//			String resultStatus = tPTY002Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			
			/**
			 * 添加上平台那边返回错误的详细信息
			 */
			String resultInfoDesc = tPTY002Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				for(ResponseNode responseNode:responseNodeList){
					String resultStatus = responseNode.getResult().getResultStatus();
					if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
						System.out.println("系统正常，业务成功处理！");
						mErrorInfo = "系统正常，业务成功处理!";
						saveErrorList(tPTY002Response,mErrorInfo);
//						String mPTCustomerNo = tPTY002Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPlatformCustomerNo();
//						String mCustomerNo = tPTY002Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getCustomerNo();
						String mPTCustomerNo = responseNode.getBusinessObject().getPlatformCustomerNo();
						String mCustomerNo = responseNode.getBusinessObject().getCustomerNo();
						String updLDPerson = "update ldperson a set a.platformcustomerno ='"+mPTCustomerNo+"' where a.customerno='"+mCustomerNo+"'";
						boolean updLDPersonF=tExeSQL.execUpdateSQL(updLDPerson);
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
						System.out.println("系统正常，等待异步返回!");
						mErrorInfo = "系统正常，等待异步返回!";
						saveErrorList(tPTY002Response,mErrorInfo);
						CError tCError = new CError();
						tCError.moduleName = "GetPlatformCustomerNo";
						tCError.functionName = "getPlatformCustomerNo";
						tCError.errorMessage = "系统正常，等待异步返回!";
						this.mErrors.addOneError(tCError);
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
						System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
						CError tCError = new CError();
						tCError.moduleName = "GetPlatformCustomerNo";
						tCError.functionName = "getPlatformCustomerNo";
						tCError.errorMessage = resultInfoDesc;
						mErrorInfo = tCError.errorMessage;
						saveErrorList(tPTY002Response,mErrorInfo);
						this.mErrors.addOneError(tCError);
						return false;
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
						System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
						CError tCError = new CError();
						tCError.moduleName = "GetPlatformCustomerNo";
						tCError.functionName = "getPlatformCustomerNo";
						tCError.errorMessage = resultInfoDesc;
						mErrorInfo = tCError.errorMessage;
						saveErrorList(tPTY002Response,mErrorInfo);
						this.mErrors.addOneError(tCError);
						return false;
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
						System.out.println("系统异常！");
						CError tCError = new CError();
						tCError.moduleName = "GetPlatformCustomerNo";
						tCError.functionName = "getPlatformCustomerNo";
						tCError.errorMessage = "系统异常!";
						mErrorInfo = tCError.errorMessage;
						saveErrorList(tPTY002Response,mErrorInfo);
						this.mErrors.addOneError(tCError);
						return false;
					}
					return true;
				}
			}
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	return true;
	}
	/**
	 * 获取结果的公共方法
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(PTY002Response mPTY002Response,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_NAME_BATCHNO", 20);
		String TransType = mPTY002Response.getHead().getTransType();
		String TransNo = mPTY002Response.getHead().getTransNo();
		String responseCode = mPTY002Response.getHead().getResponseCode();
		String resultStatus = mPTY002Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"','"+bookingSequenceNo+"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
//		String updateSQL = "";
//		if(resultStatus!=null && "00".equals(resultStatus)){
//			updateSQL = "update lsinsuredlist set checkflag='00',errormessage='内检成功',modifydate=current date,modifytime=current time where batchno='"+mBatchNo+"' and customerno='"+mAppntNo+"'";
//		}else{
//			updateSQL = "update lsinsuredlist set checkflag='01',errormessage='内检失败',modifydate=current date,modifytime=current time where batchno='"+mBatchNo+"' and customerno='"+mAppntNo+"'";
//		}
//		ExeSQL tExeSQL = new ExeSQL();
//		if(!tExeSQL.execUpdateSQL(updateSQL)){
//			System.out.println("更新信息失败--"+TransType);
//		}
//		System.out.println("更新信息成功--"+TransType);
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BookingSequenceNo", "13032593126");
		tVData.add(tTransferData);
		GetPlatformCustomerNoPTY002 tGetPlatformCustomerNo= new GetPlatformCustomerNoPTY002();
		tGetPlatformCustomerNo.submitData(tVData,"ZBXPT");
		
	}
}