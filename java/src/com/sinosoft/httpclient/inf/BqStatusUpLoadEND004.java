package com.sinosoft.httpclient.inf;


import java.util.List;

import com.sinosoft.httpclient.dto.end004.request.Head;
import com.sinosoft.httpclient.dto.end004.request.END004Request;
import com.sinosoft.httpclient.dto.end004.request.RequestNode;
import com.sinosoft.httpclient.dto.end004.response.END004Response;
import com.sinosoft.httpclient.dto.end004.response.ResponseNode;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData; 


/**
 * <p>Title:  客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Description:客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class BqStatusUpLoadEND004 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /**预约号*/
	  private String mBookingSequenceNo;
	  /**数据操作类型*/
	  private String mOperate;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  
	  /**
	   * 提供无参的构造方法
	   */
	  public BqStatusUpLoadEND004() {}
	
	/**
	 * 获取平台客户码的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    if(!dealData()){
	    	return false;
	    }
	   		
	    return true;
	}
	/**
	 * 获取传入的数据
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
    	 */
    	tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
    	mBookingSequenceNo = (String) tempTransferData.getValueByName("BookingSequenceNo");
    	
    	System.out.println(mBookingSequenceNo + " ** " + mBookingSequenceNo);
   		if(null==mBookingSequenceNo || "".equals(mBookingSequenceNo)){
    		CError tCError = new CError();
    		tCError.moduleName = "BqStatusUpLoad";
    		tCError.functionName = "getMessage";
    		tCError.errorMessage = "传入的异步预约码为空！";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
   		return true;
	}
	/**
	 * 数据处理方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 先上传一个客户号，获取信息校验平台的平台客户编码
   		 */
       	END004Request tEND004Request = new END004Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setBookingSequenceNo(mBookingSequenceNo);
       	tEND004Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
       		/**发送报文获取返回结果 */
			String responseXml = CommunicateServiceImpl.post(tEND004Request,mOperate);
			/**解析返回结果 */
			END004Response tEND004Response = (END004Response)XmlParseUtil.xmlToDto(responseXml, END004Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tEND004Response.getHead().getResponseCode();
			ResponseNode responseNode = tEND004Response.getResponseNodes().getResponseNode();
			/**
			 * 添加上平台那边返回错误的详细信息
			 */
			String resultInfoDesc = tEND004Response.getResponseNodes().getResponseNode().getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				String resultStatus = responseNode.getResult().getResultStatus();
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tEND004Response,mErrorInfo,resultStatus);
					System.out.println("核心获取返回报文后的处理。。。");
					
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tEND004Response,mErrorInfo,resultStatus);
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND004Response,mErrorInfo,resultStatus);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND004Response,mErrorInfo,resultStatus);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND004Response,mErrorInfo,resultStatus);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	return true;
	}
	/**
	 * 获取结果的公共方法
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(END004Response mEND004Response,String mErrorInfo,String resultStatus){
		String ID = PubFun1.CreateMaxNo("SEQ_NAME_BATCHNO", 20);
		String TransType = mEND004Response.getHead().getTransType();
		String TransNo = mEND004Response.getHead().getTransNo();
		String responseCode = mEND004Response.getHead().getResponseCode();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"',null,'"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BookingSequenceNo", "13032593126");
		tVData.add(tTransferData);
		BqStatusUpLoadEND004 tBqStatusUpLoad= new BqStatusUpLoadEND004();
		tBqStatusUpLoad.submitData(tVData,"ZBXPT");
		
	}
}