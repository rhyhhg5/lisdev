package com.sinosoft.httpclient.inf;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.httpclient.dto.end006.request.END006Request;
import com.sinosoft.httpclient.dto.end006.request.Head;
import com.sinosoft.httpclient.dto.end006.request.RequestNode;
import com.sinosoft.httpclient.dto.end006.request.RequestNodes;
import com.sinosoft.httpclient.dto.end006.response.Client;
import com.sinosoft.httpclient.dto.end006.response.ClientList;
import com.sinosoft.httpclient.dto.end006.response.END006Response;
import com.sinosoft.httpclient.dto.end006.response.TransferIn;
import com.sinosoft.httpclient.dto.end006.response.TransferInList;
import com.sinosoft.httpclient.dto.end006.response.TransferOut;
import com.sinosoft.httpclient.dto.end006.response.TransferOutList;
import com.sinosoft.httpclient.dto.prm001.response.PRM001Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LSTransOutInfoDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LSErrorListSchema;
import com.sinosoft.lis.schema.LSTransOutInfoSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranferOut </p>
 * <p>Description: 保单转出的接口类 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class LCContTranferOut {
	/**公共的错误类*/
	public CErrors mErrors = new CErrors();
	/**返回结构的容器*/
	private VData mResult = new VData();
	/**存储保存外部传入的值*/
	private VData mInputData = new VData();
	/**携带数据的类型*/
	private TransferData tempTransferData = new TransferData();
	/**存储外部传入的操作类型*/
	private String mOperate;
	/**获取数据的开始日期*/
	private String mQueryStartDate;
	/**获取数据的结束日期*/
	private String mQueryEndDate;
	
    private String CurrentDate = PubFun.getCurrentDate();
    
    private String CurrentTime = PubFun.getCurrentTime();
    
	private MMap mMap = new MMap();
	
	private ExeSQL tExeSQL=new ExeSQL();
	/**
	 * 公共的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		/**
		 * 获取传入的值
		 */
		if(!getInputData()){
			return false;
		}
		/**
		 * 数据处理方法
		 */
		if(!dealData()){
			return false;
		}
		/**
		 * 准备往后台传输数据
		 */
		if(!preparedData()){
			return false;
		}
		/**
		 * 插入数据库
		 */
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("start LCContTranferOut pubsubmit!!!");
		if(!tPubSubmit.submitData(mInputData, mOperate)){
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContTranferOut";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("end LCContTranferOut pubsubmit!!!");
		mInputData = null;
		return true;
	}
	/**
	 * 获取传入的值
	 */
	private boolean getInputData(){
		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mQueryStartDate = (String) tempTransferData.getValueByName("QueryStartDate");
		mQueryEndDate = (String) tempTransferData.getValueByName("QueryEndDate");
		if(mQueryStartDate==null||mQueryStartDate.equals("")){
			CError tCError = new CError();
			tCError.moduleName = "LCContTranferOut";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的查询起始日期为空！！！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		if(mQueryEndDate==null||mQueryEndDate.equals("")){
			CError tCError = new CError();
			tCError.moduleName = "LCContTranferOut";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的查询终止日期为空！！！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	/**
	 * 数据处理方法
	 */
	private boolean dealData(){
		END006Request tEnd006Request = new END006Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setQueryStartDate(mQueryStartDate);
		tRequestNode.setQueryEndDate(mQueryEndDate);
		tEnd006Request.getRequestNodes().getRequestNode().add(tRequestNode);
		/**生成xml的头部信息*/
		Head head = new Head();
		
		try {
			/**
			 * 调用发送请求报文的方法，用responseXml 去接收返回的响应报文
			 */
			String responseXml = CommunicateServiceImpl.post(tEnd006Request, mOperate);
			/**
			 * 将接收到的xml字符串解析为bean类
			 */
			END006Response tEND006Response = (END006Response) XmlParseUtil.xmlToDto(responseXml, END006Response.class);
			/**获取返回的状态*/
			String tResponseCode = tEND006Response.getHead().getResponseCode();
			List<TransferInList> tTransferInList = new ArrayList<TransferInList>();
			List<TransferIn> tTransferIn = new ArrayList<TransferIn>();
			List<TransferOutList> tTransferOutList = new ArrayList<TransferOutList>();
			List<TransferOut> tTransferOut = new ArrayList<TransferOut>();
			List<ClientList> tClientList = new ArrayList<ClientList>();
			List<Client> tClient= new ArrayList<Client>();
			LSTransOutInfoSchema tLSTransOutInfoSchema=null;
//			LSTransOutInfoDB tLSTransOutInfoDB= new LSTransOutInfoDB();
			//根据返回的状态进行判断，0000时代表没有重大的异常
			if(tResponseCode!=null&&!tResponseCode.equals("")&&"0000".equals(tResponseCode)){
				//获取处理结果状态的值
				String tResultStatus = tEND006Response.getResponseNodes().getResponseNode().getResult().getResultStatus();
				String tResultInfoDesc = tEND006Response.getResponseNodes().getResponseNode().getResult().getResultInfoDesc();
				String tCiitcResultCode = tEND006Response.getResponseNodes().getResponseNode().getResult().getCiitcResultCode();
				String tCiitcResultMessage = tEND006Response.getResponseNodes().getResponseNode().getResult().getCiitcResultMessage();
				if("00".equals(tResultStatus)&&"1".equals(tCiitcResultCode)){
//					tLSTransOutInfoSchema.setRejectReson(tCiitcResultMessage);
					tTransferInList = tEND006Response.getResponseNodes().getResponseNode().getTransferInList();
					
					for(int i=0;i<tTransferInList.size();i++){
						//获取返回的保单号
						System.out.println(tTransferInList.size());
						tTransferIn = tTransferInList.get(i).getTransferIn();
						if(null != tTransferIn && tTransferIn.size()>0){
							for(int j=0;j<tTransferIn.size();j++){
								tLSTransOutInfoSchema=new LSTransOutInfoSchema();
								tLSTransOutInfoSchema.setInCompanyName(tTransferIn.get(j).getCompanyName());
								tLSTransOutInfoSchema.setInBankName(tTransferIn.get(j).getBankName());
								tLSTransOutInfoSchema.setInBankAccNo(tTransferIn.get(j).getAccountNo());
								tLSTransOutInfoSchema.setInContactName(tTransferIn.get(j).getContactName());
								tLSTransOutInfoSchema.setInContactTele(tTransferIn.get(j).getContactTele());
								tLSTransOutInfoSchema.setInContactEmail(tTransferIn.get(j).getContactEmail());
								tTransferOutList = tTransferIn.get(j).getTransferOutList();
								for(int m=0;m<tTransferOutList.size();m++){
								
									tTransferOut = tTransferOutList.get(m).getTransferOut();
									
									for(int n=0;n<tTransferOut.size();n++){
	//									tLSTransOutInfoSchema.setContNo(tTransferOut.get(n).getPolicyNo());
										tClientList=tTransferOut.get(n).getClientList();
										for(int k=0;k<tClientList.size();k++){
											tClient =tClientList.get(k).getClient();
										    for(int l=0;l<tClient.size();l++)
										    {
										    	String sql="select 1 FROM LSTransOutInfo where contno='"+ tClient.get(l).getSequenceNo()+"' ";
									            tExeSQL = new ExeSQL();
									            String strSql = tExeSQL.getOneValue(sql);
									            if(null==strSql || strSql.equals(""))
									            {
										           tLSTransOutInfoSchema.setContNo(tClient.get(l).getSequenceNo());
										           tLSTransOutInfoSchema.setApplyDate(tClient.get(l).getTransApplyDate());
										           tLSTransOutInfoSchema.setPlatTransNo(tClient.get(l).getTransferSequenceNo());
										           tLSTransOutInfoSchema.setPlatReceivDate(tClient.get(l).getTransReceivDate());
										           tLSTransOutInfoSchema.setTransOutState("01");
										    
										           tLSTransOutInfoSchema.setMakeDate(CurrentDate);
										           tLSTransOutInfoSchema.setMakeTime(CurrentTime);
										           tLSTransOutInfoSchema.setModifyDate(CurrentDate);
										           tLSTransOutInfoSchema.setModifyTime(CurrentTime);
										    
										           mMap.put(tLSTransOutInfoSchema, "DELETE&INSERT");
									           }else{
									        	   //已有申请记录的不再保存
									        	   continue;
									           }
										    }
										}
									}
								}
								
							}
						}
					}

				}
				//存储日志
				saveErrorList(tEND006Response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(END006Response mEND006Response){
		String ID = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);
		String TransType = mEND006Response.getHead().getTransType();
		String TransNo = mEND006Response.getHead().getTransNo();
		String responseCode = mEND006Response.getHead().getResponseCode();
		String resultStatus = mEND006Response.getResponseNodes().getResponseNode().getResult().getResultStatus();
		String mErrorInfo = mEND006Response.getResponseNodes().getResponseNode().getResult().getResultInfoDesc();
		if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
			mErrorInfo = "系统异常!";
		}
		LSErrorListSchema tLSErrorListSchema = new LSErrorListSchema();
		tLSErrorListSchema.setSerNo(ID);
		tLSErrorListSchema.setTransType(TransType);
		tLSErrorListSchema.setTransNo(TransNo);
		tLSErrorListSchema.setBusinessNo(mQueryStartDate + "&" + mQueryEndDate);
		tLSErrorListSchema.setResponseCode(responseCode);
		tLSErrorListSchema.setResultStatus(resultStatus);
		tLSErrorListSchema.setErrorInfo(mErrorInfo);
		tLSErrorListSchema.setMakeDate(PubFun.getCurrentDate());
		tLSErrorListSchema.setMakeTime(PubFun.getCurrentTime());
		tLSErrorListSchema.setModifyDate(PubFun.getCurrentDate());
		tLSErrorListSchema.setModifyTime(PubFun.getCurrentTime());
		
		mMap.put(tLSErrorListSchema, "DELETE&INSERT");
		
	}
	
	
	/**
	 * 准备往后台的数据
	 */
	private boolean preparedData(){
		this.mInputData.add(mMap);
		return true;
	}
	/**
	 * 获取结果集的公共get方法
	 */
	public VData getResult(VData result){
		return mResult;
	}
}
