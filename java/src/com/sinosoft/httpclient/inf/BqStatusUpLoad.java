package com.sinosoft.httpclient.inf;


import com.sinosoft.httpclient.dto.end001.response.END001Response;
import com.sinosoft.httpclient.dto.end003.request.Head;
import com.sinosoft.httpclient.dto.end003.request.END003Request;
import com.sinosoft.httpclient.dto.end003.request.RequestNode;
import com.sinosoft.httpclient.dto.end003.response.END003Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSErrorListSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData; 


/**
 * <p>Title:  客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Description:客户信息验证时从中保信上获取客户平台编码</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class BqStatusUpLoad {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  
	  /**状态生效时间*/
	  private String mStartDate;
	  
	  /**保单号*/
	  private String mContNo;
	  
	  /**数据操作类型*/
	  private String mOperate;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  
	  private MMap map = new MMap();
	  
	  /**
	   * 提供无参的构造方法
	   */
	  public BqStatusUpLoad() {}
	
	/**
	 * 获取平台客户码的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    if(!dealData()){
	    	return false;
	    }
	   		
	    return true;
	}
	/**
	 * 获取传入的数据
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
    	 */
    	tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
    	mStartDate = (String) tempTransferData.getValueByName("StartDate");
    	mContNo = (String) tempTransferData.getValueByName("ContNo");
   		System.out.println(mStartDate + " ** " + mContNo);
   		if(null==mStartDate || "".equals(mStartDate)){
    		CError tCError = new CError();
    		tCError.moduleName = "GetPlatformCustomerNo";
    		tCError.functionName = "getMessage";
    		tCError.errorMessage = "传入的状态生效时间为空！";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
   		if(null==mContNo || "".equals(mContNo)){
    		CError tCError = new CError();
    		tCError.moduleName = "GetPlatformCustomerNo";
    		tCError.functionName = "getMessage";
    		tCError.errorMessage = "传入的保单号为空！";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
   		return true;
	}
	/**
	 * 数据处理方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 先上传一个客户号，获取信息校验平台的平台客户编码
   		 */
       	END003Request tEND003Request = new END003Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setContNo(mContNo);
       	tRequestNode.setStartDate(mStartDate);
       	tEND003Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
       		/**发送报文获取返回结果 */
			String responseXml = CommunicateServiceImpl.post(tEND003Request,mOperate);
			/**解析返回结果 */
			END003Response tEND003Response = (END003Response)XmlParseUtil.xmlToDto(responseXml, END003Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tEND003Response.getHead().getResponseCode();
			String resultStatus = tEND003Response.getResponseNodes().getResponseNode().getResult().getResultStatus();
			
			/**
			 * 添加上平台那边返回错误的详细信息
			 */
			String resultInfoDesc = tEND003Response.getResponseNodes().getResponseNode().getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tEND003Response,mErrorInfo);
//					String mPTCustomerNo = tEND003Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPlatformCustomerNo();
//					String mCustomerNo = tEND003Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getCustomerNo();
//					String updLDPerson = "update ldperson a set a.platformcustomerno ='"+mPTCustomerNo+"' where a.customerno='"+mCustomerNo+"'";
//					boolean updLDPersonF=tExeSQL.execUpdateSQL(updLDPerson);
					System.out.println("核心获取返回报文后的处理。。。");
					
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tEND003Response,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND003Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND003Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "BqStatusUpLoad";
					tCError.functionName = "BqStatusUpLoad";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND003Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	return true;
	}
	/**
	 * 获取结果的公共方法
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(END003Response mEND003Response,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);
		String TransType = mEND003Response.getHead().getTransType();
		String TransNo = mEND003Response.getHead().getTransNo();
		String responseCode = mEND003Response.getHead().getResponseCode();
		String resultStatus = mEND003Response.getResponseNodes().getResponseNode().getResult().getResultStatus();
		
		LSErrorListSchema tLSErrorListSchema = new LSErrorListSchema();
		tLSErrorListSchema.setSerNo(ID);
		tLSErrorListSchema.setTransType(TransType);
		tLSErrorListSchema.setTransNo(mStartDate);
		tLSErrorListSchema.setBusinessNo(mContNo);
		tLSErrorListSchema.setResponseCode(responseCode);
		tLSErrorListSchema.setResultStatus(resultStatus);
		tLSErrorListSchema.setErrorInfo(mErrorInfo);
		tLSErrorListSchema.setMakeDate(PubFun.getCurrentDate());
		tLSErrorListSchema.setMakeTime(PubFun.getCurrentTime());
		tLSErrorListSchema.setModifyDate(PubFun.getCurrentDate());
		tLSErrorListSchema.setModifyTime(PubFun.getCurrentTime());
		
		
		map.put(tLSErrorListSchema, "DELETE&INSERT");
		mResult.add(map);
		
		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(mResult, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BqDataUpLoad";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
		}
		
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate", "2015-2-13");
		tTransferData.setNameAndValue("ContNo", "014026496000001");
		tVData.add(tTransferData);
		BqStatusUpLoad tBqStatusUpLoad= new BqStatusUpLoad();
		tBqStatusUpLoad.submitData(tVData,"ZBXPT");
		
	}
}