package com.sinosoft.httpclient.inf;


import java.util.Date;

import com.sinosoft.httpclient.dto.nbu003.request.Head;
import com.sinosoft.httpclient.dto.nbu003.request.NBU003Request;
import com.sinosoft.httpclient.dto.nbu003.request.RequestNode;
import com.sinosoft.httpclient.dto.nbu003.response.NBU003Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 撤销保单的接口  和中保信</p>
 * <p>Description:中保信     连接中保信的接口  撤销保单的接口</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class DrawLCContInf {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /**用于存储传入的保单号*/
	  private String mContNo;
	  /**用于存储红冲的具体原因*/
	  private String mHCRemark;
	  /**用于存储查询的客户号*/
	  private String mAppntNo;
	  private String result = "";
	  /**数据操作类型*/
	  private String mOperate;
	  
	  /**
	   * 提供无参的构造方法
	   */
	  public DrawLCContInf() {}
	
	/**
	 * 获取平台客户码的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    if(!dealData()){
	    	return false;
	    }
	   		
	    return true;
	}
	/**
	 * 获取传入的数据
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
    	 */
    	tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
    	mContNo = (String) tempTransferData.getValueByName("ContNo");
    	mHCRemark = (String) tempTransferData.getValueByName("HCRemark");
   		System.out.println(mContNo);
   		if(mContNo==null&&mContNo.equals("")){
    		CError tCError = new CError();
    		tCError.moduleName = "GetPlatformCustomerNo";
    		tCError.functionName = "getMessage";
    		tCError.errorMessage = "传入的保单号为空！";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
   		//通过保单号查询出投保人号码
   		return true;
	}
	/**
	 * 数据处理方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 先上传一个客户号，获取信息校验平台的平台客户编码
   		 */
       	NBU003Request tNBU003Request = new NBU003Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setPolicyNo(mContNo);
       	tRequestNode.setCancelDate(PubFun.getCurrentDate());
       	tRequestNode.setCancelReasonCode("1");
       	tRequestNode.setCancelReasonDesc(mHCRemark);
       	tNBU003Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
			String responseXml = CommunicateServiceImpl.post(tNBU003Request,mOperate);
			/**获取撤销保单返回的处理结果    */
			NBU003Response tNBU003Response = (NBU003Response)XmlParseUtil.xmlToDto(responseXml, NBU003Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tNBU003Response.getHead().getResponseCode();
			String resultStatus = tNBU003Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			
			/**
			 * 添加上平台那边返回错误的详细信息
			 */
			String resultInfoDesc = tNBU003Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			
		
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				/**
				 * 具体返回结果的处理   定义一个变量result用于存储处理结果   0代表撤销成功   1代表撤销失败
				 */
			
				
				if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
					if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
						System.out.println("系统正常，业务处理成功！");
						result = "1";
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("04")){
						System.out.println("上报未成功，无需撤销!");
						result = "1";
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
						System.out.println("实际报送失败!原因为："+resultInfoDesc);	
						CError tCError = new CError();
						tCError.moduleName = "DrawLCContInf";
						tCError.functionName = "dealData";
						tCError.errorMessage = "实际报送失败!原因为："+resultInfoDesc;
						this.mErrors.addOneError(tCError);
						result = "0";
						return false;
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
						System.out.println("系统异常！");
						CError tCError = new CError();
						tCError.moduleName = "DrawLCContInf";
						tCError.functionName = "dealData";
						tCError.errorMessage = "系统异常!";
						this.mErrors.addOneError(tCError);
						result = "0";
						return false;
					}
					return true;
				}
			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1001")){
				System.out.println("安全验证失败！");
				CError tCError = new CError();
				tCError.moduleName = "DrawLCContInf";
				tCError.functionName = "dealData";
				tCError.errorMessage = "安全验证失败!";
				this.mErrors.addOneError(tCError);
				result = "0";
				return false;
			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1002")){
				System.out.println("报文为空！");
				CError tCError = new CError();
				tCError.moduleName = "DrawLCContInf";
				tCError.functionName = "dealData";
				tCError.errorMessage = "报文为空!";
				this.mErrors.addOneError(tCError);
				result = "0";
				return false;
			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1003")){
				System.out.println("报文为空！");
				CError tCError = new CError();
				tCError.moduleName = "DrawLCContInf";
				tCError.functionName = "dealData";
				tCError.errorMessage = "报文为空!";
				this.mErrors.addOneError(tCError);
				result = "0";
				return false;
			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1004")){
				System.out.println("系统异常！");
				CError tCError = new CError();
				tCError.moduleName = "DrawLCContInf";
				tCError.functionName = "dealData";
				tCError.errorMessage = "系统异常!";
				this.mErrors.addOneError(tCError);
				result = "0";
				return false;
			}else {
				System.out.println("其他异常！");
				CError tCError = new CError();
				tCError.moduleName = "DrawLCContInf";
				tCError.functionName = "dealData";
				tCError.errorMessage = "其他异常!";
				this.mErrors.addOneError(tCError);
				result = "0";
				return false;
			}
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	return true;
	}
	/**
	 * 获取结果的公共方法
	 * @param args
	 */
	public VData getResult(){
		tempTransferData = new TransferData();
		tempTransferData.setNameAndValue("Result", result);
		mResult.add(tempTransferData);
		return mResult;
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", "014059606000001");
		tVData.add(tTransferData);
		DrawLCContInf tDrawLCContInf= new DrawLCContInf();
		tDrawLCContInf.submitData(tVData,"ZBXPT");
		tVData = tDrawLCContInf.getResult();
		tTransferData = (TransferData) tVData.getObjectByObjectName("TransferData", 0);
		String result = (String) tTransferData.getValueByName("Result");
		System.out.println(result);
		
	}
}
