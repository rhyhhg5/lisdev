package com.sinosoft.httpclient.inf;

import java.util.List;

import com.sinosoft.httpclient.dto.qry001.response.*;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.CustomerQueryClaimSchema;
import com.sinosoft.lis.schema.CustomerQueryRiskSchema;
import com.sinosoft.lis.schema.CustomerQuerySchema;
import com.sinosoft.lis.vschema.CustomerQueryClaimSet;
import com.sinosoft.lis.vschema.CustomerQueryRiskSet;
import com.sinosoft.lis.vschema.CustomerQuerySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.httpclient.util.XmlParseUtil;

public class SaveCustomerInfo {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /** 解析有报文 */
	  private QRY001Response mQRY001Response = null;
	  /**存放客户信息 */
	  private CustomerQuerySchema tCustomerQuerySchema = null;
	  private CustomerQuerySet tCustomerQuerySet = null;
	  /** 存放 客户赔款信息*/
	  private CustomerQueryClaimSchema tCustomerQueryClaimSchema = null;
	  private CustomerQueryClaimSet tCustomerQueryClaimSet = null;
	  /** 存放客户保单险种信息 */
	  private CustomerQueryRiskSchema tCustomerQueryRiskSchema = null;
	  private CustomerQueryRiskSet tCustomerQueryRiskSet = null;
	  private MMap mMap = new MMap();
	  private String mresponseXml ;
	  
	  private List<Coverage> tCoverage = null;
	  private List<Policy> tPolicy = null;
	  private List<Claim> tClaim = null;

	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    
  		if(!dealData()){
			return false;
		}
  		/**
		 * 准备往后台传输数据
		 */
		if(!preparedData()){
			return false;
		}
  		/**
		 * 插入数据库
		 */
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("start SaveCustomerInfo pubsubmit!!!");
		if(!tPubSubmit.submitData(mInputData, "")){
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContTranferOut";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("end SaveCustomerInfo pubsubmit!!!");
		mInputData = null;
		return true;
	}
	/**
	 * 获取传入的方法
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
   		 */
   		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
   		mresponseXml = (String) tempTransferData.getValueByName("responseXml");
   		System.out.println("mresponseXml---"+mresponseXml);
   		if(mresponseXml==null&&mresponseXml.equals("")){
   			CError tCError = new CError();
   			tCError.moduleName = "SaveCustomerInfo";
   			tCError.functionName = "getMessage";
   			tCError.errorMessage = "传入的查询报文为空！";
   			this.mErrors.addOneError(tCError);
   			return false;
		}
   		return true;
	}
	/**
	 * 准备往后台的数据
	 */
	private boolean preparedData(){
		this.mInputData.add(mMap);
		return true;
	}
	/**
	 * 解析报文数据，存储数据表
	 */
	public boolean dealData(){
		try {
			
		mQRY001Response = (QRY001Response)XmlParseUtil.xmlToDto(mresponseXml, QRY001Response.class);
		
		/**
		 * 获取返回结果，并对返回的结果进行判断
		 * 1：如果responseCode不等于"0000",表示处理失败,
		 */
		String responseCode = mQRY001Response.getHead().getResponseCode();
		String resultStatus = mQRY001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String ciitcResultCode = mQRY001Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultCode();
		String ciitcResultMessage = mQRY001Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultMessage();
		/**
		 * 添加上平台那边返回错误的详细信息
		 */
		String resultInfoDesc = mQRY001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
		if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
			if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
				Customer mCustomer = mQRY001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getCustomer();
				String mCustomerNo = mCustomer.getCustomerNo();
				String mNationality = mCustomer.getNationality();
				tPolicy = mCustomer.getPolicyList().getPolicy();
				if(tPolicy!=null && tPolicy.size()>0){
					tCustomerQuerySet = new CustomerQuerySet();
					for(int i=0;i<tPolicy.size();i++){
						tCustomerQuerySchema = new CustomerQuerySchema();
						tCustomerQuerySchema.setSerNo(PubFun1.CreateMaxNo("SEQ_NAME_Customer", 20));
						tCustomerQuerySchema.setCustomerNo(mCustomerNo);
						tCustomerQuerySchema.setNationality(mNationality);
						tCustomerQuerySchema.setResponseCode(responseCode);
						tCustomerQuerySchema.setResultStatus(resultStatus);
						tCustomerQuerySchema.setCiitcResultCode(ciitcResultCode);
						tCustomerQuerySchema.setCiitcResultMessage(ciitcResultMessage);
						tCustomerQuerySchema.setResultInfoDesc(resultInfoDesc);
						String mCompanyCode = tPolicy.get(i).getCompanyCode();
						String mPolicyType = tPolicy.get(i).getPolicyType();
						String mPolicyNo = tPolicy.get(i).getPolicyNo();
						String mSequenceNo = tPolicy.get(i).getSequenceNo();
						String mPolicySource = tPolicy.get(i).getPolicySource();
						String mPolicyStatus = tPolicy.get(i).getPolicyStatus();
						String mEffectiveDate = tPolicy.get(i).getEffectiveDate();
						String mExpireDate = tPolicy.get(i).getExpireDate();
						String mTerminationDate = tPolicy.get(i).getTerminationDate();
						String mTerminationReason = tPolicy.get(i).getTerminationReason();
						String mTaxFavorIndi = tPolicy.get(i).getTaxFavorIndi();
						String mPeriodPayment = tPolicy.get(i).getPeriodPayment();
						tCustomerQuerySchema.setCompanyCode(mCompanyCode);
						tCustomerQuerySchema.setPolicyType(mPolicyType);
						tCustomerQuerySchema.setPolicyNo(mPolicyNo);
						tCustomerQuerySchema.setSequenceNo(mSequenceNo);
						tCustomerQuerySchema.setPolicySource(mPolicySource);
						tCustomerQuerySchema.setPolicyStatus(mPolicyStatus);
						tCustomerQuerySchema.setEffectiveDate(mEffectiveDate);
						tCustomerQuerySchema.setExpireDate(mExpireDate);
						tCustomerQuerySchema.setTerminationDate(mTerminationDate);
						tCustomerQuerySchema.setTerminationReason(mTerminationReason);
						tCustomerQuerySchema.setTaxFavorIndi(mTaxFavorIndi);
						tCustomerQuerySchema.setPeriodPayment(mPeriodPayment);
						tCustomerQuerySchema.setMakeDate(PubFun.getCurrentDate());
						tCustomerQuerySchema.setMakeTime(PubFun.getCurrentTime());
						tCustomerQuerySchema.setModifyDate(PubFun.getCurrentDate());
						tCustomerQuerySchema.setModifyTime(PubFun.getCurrentTime());
						tCustomerQuerySet.add(tCustomerQuerySchema);
						mMap.put(tCustomerQuerySet, "INSERT");
						
						//获取保单险种信息，并且存储
						if(tPolicy.get(i).getCoverageList()!=null){
							tCoverage = tPolicy.get(i).getCoverageList().getCoverage();
							if(tCoverage!=null && tCoverage.size()>0){
								tCustomerQueryRiskSet = new CustomerQueryRiskSet();
								for(int j=0;j<tCoverage.size();j++){
									tCustomerQueryRiskSchema = new CustomerQueryRiskSchema();
									String mComCoverageName = tCoverage.get(j).getComCoverageName();
									String mSa = tCoverage.get(j).getSa();
									String mUnderwritingDecision = tCoverage.get(j).getUnderwritingDecision();
									tCustomerQueryRiskSchema.setSerNo(PubFun1.CreateMaxNo("SEQ_NAME_RISK", 20));
									tCustomerQueryRiskSchema.setPolicyNo(mPolicyNo);
									tCustomerQueryRiskSchema.setSequenceNo(mSequenceNo);
									tCustomerQueryRiskSchema.setComCoverageName(mComCoverageName);
									tCustomerQueryRiskSchema.setSa(mSa);
									tCustomerQueryRiskSchema.setUnderwritingDecision(mUnderwritingDecision);
									tCustomerQueryRiskSchema.setMakeDate(PubFun.getCurrentDate());
									tCustomerQueryRiskSchema.setMakeTime(PubFun.getCurrentTime());
									tCustomerQueryRiskSchema.setModifyDate(PubFun.getCurrentDate());
									tCustomerQueryRiskSchema.setModifyTime(PubFun.getCurrentTime());
									tCustomerQueryRiskSet.add(tCustomerQueryRiskSchema);
									mMap.put(tCustomerQueryRiskSet, "INSERT");
								}
							}
						}
					}
				}
				
				//获取客户理赔信息，并存储
				if(mCustomer.getClaimList()!=null){
					tClaim= mCustomer.getClaimList().getClaim();
					if(tClaim!=null && tClaim.size()>0){
						tCustomerQueryClaimSet = new CustomerQueryClaimSet();
						for(int n=0;n<tClaim.size();n++){
							tCustomerQueryClaimSchema = new CustomerQueryClaimSchema();
							tCustomerQueryClaimSchema.setSerNo(PubFun1.CreateMaxNo("SEQ_NAME_CLAIM", 20));
							tCustomerQueryClaimSchema.setCustomerNo(mCustomerNo);
							tCustomerQueryClaimSchema.setNationality(mNationality);
							String maccidentDate = tClaim.get(n).getAccidentDate();
							String mendcaseDate = tClaim.get(n).getEndcaseDate();
							String mclaimConclusionCode = tClaim.get(n).getClaimConclusionCode();
							String mwarningIndi = tClaim.get(n).getWarningIndi();
							String mwarningDesc = tClaim.get(n).getWarningDesc();
							tCustomerQueryClaimSchema.setAccidentDate(maccidentDate);
							tCustomerQueryClaimSchema.setEndcaseDate(mendcaseDate);
							tCustomerQueryClaimSchema.setClaimConclusionCode(mclaimConclusionCode);
							tCustomerQueryClaimSchema.setWarningIndi(mwarningIndi);
							tCustomerQueryClaimSchema.setWainingDesc(mwarningDesc);
							tCustomerQueryClaimSchema.setMakeDate(PubFun.getCurrentDate());
							tCustomerQueryClaimSchema.setMakeTime(PubFun.getCurrentTime());
							tCustomerQueryClaimSchema.setModifyDate(PubFun.getCurrentDate());
							tCustomerQueryClaimSchema.setModifyTime(PubFun.getCurrentTime());
							tCustomerQueryClaimSet.add(tCustomerQueryClaimSchema);
							mMap.put(tCustomerQueryClaimSet, "INSERT");
						}
					}
				}
			}
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
}
