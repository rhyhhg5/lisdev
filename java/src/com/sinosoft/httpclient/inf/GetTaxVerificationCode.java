package com.sinosoft.httpclient.inf;

import com.sinosoft.httpclient.dto.pty003.request.Head;
import com.sinosoft.httpclient.dto.pty003.request.PTY003Request;
import com.sinosoft.httpclient.dto.pty003.request.RequestNode;
import com.sinosoft.httpclient.dto.pty003.response.PTY003Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetTaxVerificationCode {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private String mResult = "";
	/**携带数据的类*/
	private TransferData tempTransferData = new TransferData();
	/** 数据操作字符串 */
	private String mOperate;
	
	/** 存储从前面封装发送过来的数据的字段们 */
	private String mName;
	private String mGerder;
	private String mBirthday;
	private String mCertiType;
	private String mCertiNo;
	private String mNationality;
	private String mProposalNo;
	private String mPolicySource;
	private String mCoverageEffectiveDate;
	
	/** 存储平台返回的字段们*/
	private String mCustomerNO;
	private String mTransferTimes;
	
	public GetTaxVerificationCode(){}
	
	/**
	 * 获取税优验证码
	 */
	public boolean submitData(VData cInputData,String cOperate){
		
		this.mInputData = (VData)cInputData.clone();
		this.mOperate = cOperate;
		
		if(!getInputData()){
			return false;
		}
		if(!dealData()){
			return false;
		}
		if(!prepareData()){
			return false;
		}
		
		return true;
	}
	
	private boolean getInputData(){
		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mName = (String)tempTransferData.getValueByName("Name");
		if(mName==null || "".equals(mName)){
			buildError("getInputData","客户姓名没有传入！");
			return false;
		}
		mGerder = (String)tempTransferData.getValueByName("Gender");
		if(mGerder==null || "".equals(mGerder)){
			buildError("getInputData","性别没有传入！");
			return false;
		}
		mBirthday = (String)tempTransferData.getValueByName("Birthday");
		if(mBirthday==null || "".equals(mBirthday)){
			buildError("getInputData","出生日期没有传入！");
			return false;
		}
		mCertiType = (String)tempTransferData.getValueByName("CertiType");
		if(mCertiType==null || "".equals(mCertiType)){
			buildError("getInputData","证件类型没有传入！");
			return false;
		}
		mCertiNo = (String)tempTransferData.getValueByName("CertiNo");
		if(mCertiNo==null || "".equals(mCertiNo)){
			buildError("getInputData","证件号码没有传入！");
			return false;
		}
		mNationality = (String)tempTransferData.getValueByName("Nationality");
		if(mNationality==null || "".equals(mNationality)){
			buildError("getInputData","国籍没有传入！");
			return false;
		}
		mProposalNo = (String)tempTransferData.getValueByName("ProposalNo");
		if(mProposalNo==null || "".equals(mProposalNo)){
			buildError("getInputData","投保单号没有传入！");
			return false;
		}
		mPolicySource = (String)tempTransferData.getValueByName("PolicySource");
		if(mPolicySource==null || "".equals(mPolicySource)){
			buildError("getInputData","保单投保来源没有传入！");
			return false;
		}
		mCoverageEffectiveDate = (String)tempTransferData.getValueByName("CoverageEffectiveDate");
		if(mCoverageEffectiveDate==null || "".equals(mCoverageEffectiveDate)){
			buildError("getInputData","险种生效日期没有传入！");
			return false;
		}
		return true;
	}
	
	private boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 上传必传项，从中保信获取税优验证码
   		 */
		PTY003Request tPTY003Request = new PTY003Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setName(mName);
		tRequestNode.setGender(mGerder);
		tRequestNode.setBirthday(mBirthday);
		tRequestNode.setCertiType(mCertiType);
		tRequestNode.setCertiNo(mCertiNo);
		tRequestNode.setNationality(mNationality);
		tRequestNode.setProposalNo(mProposalNo);
		tRequestNode.setPolicySource(mPolicySource);
		tRequestNode.setCoverageEffectiveDate(mCoverageEffectiveDate);
		tPTY003Request.getRequestNodes().getRequestNode().add(tRequestNode);
		/**生成xml的头部信息*/
		Head head = new Head();
		try{
			String responseXml = CommunicateServiceImpl.post(tPTY003Request,mOperate);
			
			PTY003Response tPTY003Response = (PTY003Response)XmlParseUtil.xmlToDto(responseXml, PTY003Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tPTY003Response.getHead().getResponseCode();
			String resultStatus = tPTY003Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			String ciitcResultMessage = tPTY003Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultMessage();
			
			if(responseCode!=null && !"".equals(responseCode) && "0000".equals(responseCode)){
				if(resultStatus!=null && !"".equals(resultStatus) && "00".equals(resultStatus)){
					System.out.println("系统正常,业务成功处理！");
					mCustomerNO = tPTY003Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getCustomerNo();
					mTransferTimes = tPTY003Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getTransferTimes();
					addCheckFlag("00","该投保人未承保税优保单！");
				}else if(resultStatus!=null && !"".equals(resultStatus) && "01".equals(resultStatus)){
					System.out.println("系统正常，等待异步返回！");
					buildError("dealData", "系统正常，等待异步返回！原因为："+ciitcResultMessage);
					addCheckFlag("99","系统正常，等待异步返回！原因为："+ciitcResultMessage);
				}else if(resultStatus!=null && !"".equals(resultStatus) && "02".equals(resultStatus)){
					System.out.println("系统正常，数据校验不通过！原因为："+ciitcResultMessage);
					buildError("dealData", "税优验证失败！原因为："+ciitcResultMessage);
					addCheckFlag("99","税优验证失败！原因为："+ciitcResultMessage);
					return false;
				}else if(resultStatus!=null && !"".equals(resultStatus) && "03".equals(resultStatus)){
					System.out.println("系统正常，业务处理失败！原因为："+ciitcResultMessage);
					buildError("dealData", "税优验证失败！原因为："+ciitcResultMessage);
					addCheckFlag("99","税优验证失败！原因为："+ciitcResultMessage);
					return false;
				}else if(resultStatus!=null && !"".equals(resultStatus) && "99".equals(resultStatus)){
					System.out.println("系统异常！");
					buildError("dealData", "系统异常！原因为："+ciitcResultMessage);
					addCheckFlag("99","系统异常！原因为："+ciitcResultMessage);
					return false;
				}
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
			buildError("dealData", "处理异常");
			return false;
		}
		return true;
	}
	
	private void addCheckFlag(String mCheckFlag,String mCheckInfo){
		String updateSQL = "update lccontsub set checkflag='"+mCheckFlag+"',checkInfo='"+mCheckInfo+"',modifydate=current date,modifytime=current time where prtno='"+mProposalNo+"'";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(updateSQL)){
			System.out.println("更新信息失败");
		}
		mResult = mCheckInfo;
		System.out.println("更新信息成功");
	}
	
	private boolean prepareData(){
		return true;
	}
	
	/**
	 * 获取结果
	 */
	public String getResult(){
		return mResult;
	}
	public String getCustomerNo(){
		return mCustomerNO;
	}
	public String getTransferTimes(){
		return mTransferTimes;
	}
	
	private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();
        cError.moduleName = "GetTaxVerificationCode";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("Name", "程州");
		tTransferData.setNameAndValue("Gender", "0");
		tTransferData.setNameAndValue("Birthday", "1984-07-02");
		tTransferData.setNameAndValue("CertiType", "0");
		tTransferData.setNameAndValue("CertiNo", "522501198407021633");
		tTransferData.setNameAndValue("Nationality", "CHN");
		tTransferData.setNameAndValue("ProposalNo", "16888888");
		tTransferData.setNameAndValue("PolicySource", "1");
		tTransferData.setNameAndValue("CoverageEffectiveDate", "2015-10-16");
		tVData.add(tTransferData);
		GetTaxVerificationCode tGetTaxVerificationCode = new GetTaxVerificationCode();
		tGetTaxVerificationCode.submitData(tVData, "ZBXPT");
	}
}
